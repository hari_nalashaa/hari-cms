#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/../cron') . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

// Setup WOTCID in Bulk
echo "\nStart: " . date("F j, Y, g:i a") . "\n\n";

// * new way to add wotcID's need to be developed  * //

$Nonprofit="N";
$CallCenter="Y";
$eSignature="Y";
$Active="Y";

$file = "NEW.xlsx";
$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
$spreadsheet = $reader->load($file);

$worksheet = $spreadsheet->getActiveSheet();

$i=1;
foreach ($worksheet->getRowIterator() as $row) {

        $wotcID = $worksheet->getCell("A$i")->getValue();
        $Address = $worksheet->getCell("C$i")->getValue();
        $City = $worksheet->getCell("D$i")->getValue();
        $State = $worksheet->getCell("E$i")->getValue();
        $Zip = $worksheet->getCell("F$i")->getValue();
        $EIN = $worksheet->getCell("G$i")->getValue();
        $EIN1=substr($EIN,0,2);
        $EIN2=substr($EIN,3);

	if (($wotcID != 'wotcid') && ($wotcID != '')) {

            $skip   =   array('wotcID', 'ClientSince');
            $info   =   array(
                        'wotcID'                =>  $wotcID,
                        'iRecruitOrgID'         =>  '',
                        'iRecruitMultiOrgID'    =>  '',
                        'CompanyIdentifier'     =>  '',
                        'Address'               =>  $Address,
                        'City'                  =>  $City,
                        'State'                 =>  $State,
                        'ZipCode'               =>  $Zip,
                        'EIN1'                  =>  $EIN1,
                        'EIN2'                  =>  $EIN2,
                        'Nonprofit'             =>  $Nonprofit,
                        'CallCenter'            =>  $CallCenter,
                        'eSignature'            =>  $eSignature,
                        'Active'                =>  $Active
                    );

            //G::Obj('WOTCOrganization')->insUpdOrgDataInfo($info, $skip);
	    echo print_r($info,true) . "\n";

	} // end if

$i++;
} // end foreach

echo "\n\nFinish: " . date("F j, Y, g:i a") . "\n\n";

?>
