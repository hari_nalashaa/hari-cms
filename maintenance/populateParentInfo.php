#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/../cron') . '/Configuration.inc';

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";

$query = "SELECT * from ParentCompany";

$query = "select OrgData.wotcID, OrgData.CompanyID, ParentCompany.Logo, ParentCompany.ParentID AS ParentLinkShortcode, ParentCompany.DisplayCode, ParentCompany.EmploymentConfirmation, ParentCompany.ParentCompany, CRMFEIN.CompanyID AS AltCompanyID from OrgData";
$query .= " JOIN ParentCompany ON ParentCompany.ParentID = OrgData.ParentID";
$query .= " LEFT JOIN CRMFEIN ON CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
$query .= " WHERE OrgData.Active = 'Y'";
$query .= " GROUP BY ParentCompany.ParentID";
$query .= " ORDER BY OrgData.CompanyID";
$PARENT =   G::Obj('GenericQueries')->getInfoByQuery($query);


$MATCH = array('AdvancedIndustrialMetals'=>'62f4170806ca71','RaleighEnterprises'=>'62b492b1812211','americanpostsllc'=>'62b492b3325101','keikochenterprises'=>'62f4170841b351','AeroheadGroupInc'=>'63c84b89c2b1e1');

$i=0;
foreach ($PARENT as $PC) {

	if ($PC['CompanyID'] != "") {
		$CompanyID = $PC['CompanyID'];
	} else {
		$CompanyID = $PC['AltCompanyID'];
	}

	if ($CompanyID == "") {
		$CompanyID=$MATCH[$PC['ParentLinkShortcode']];
	}

	if ($CompanyID != "") {
		$i++;

		echo $i . ' - ';
		echo $CompanyID;
		echo ' ';
		echo $PC['Logo'];
		echo ' ';
		$PARENTCONFIG=array();
		$PARENTCONFIG['ParentLinkShortcode']=$PC['ParentLinkShortcode'];
		$PARENTCONFIG['DisplayCode']=$PC['DisplayCode'];
		$PARENTCONFIG['EmploymentConfirmation']=$PC['EmploymentConfirmation'];
	        $Config=json_encode($PARENTCONFIG);
		echo $Config;
		echo "\n";

		$info   =   array(
                 	"CompanyID"    =>  $CompanyID,
                 	"Logo" 	       =>  $PC['Logo'],
                 	"ParentConfig" =>  $Config
                 );
        	$skip   =   array("CompanyID");

	        G::Obj('WOTCcrm')->insUpdCRM($info, $skip);


	} else {

		echo $PC['ParentLinkShortcode'] . ' - ' . $PC['ParentCompany'] . "\n";
		
	} // end if

} // end foreach

echo 'Done';
?>
