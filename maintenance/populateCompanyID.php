#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/../cron') . '/Configuration.inc';

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";

$query = "SELECT CompanyID, wotcIDs from CRM";
$CRM =   G::Obj('GenericQueries')->getInfoByQuery($query);

foreach ($CRM as $C) {

	$WOTCIDS = json_decode($C['wotcIDs']);

	foreach ($WOTCIDS AS $ID) {
	  echo $ID . " - ";
	  echo $C['CompanyID'] . "\n";

	  $query = "update OrgData set CompanyID = :CompanyID where wotcID = :wotcID";
	  $params = array(":wotcID"=>$ID,":CompanyID"=>$C['CompanyID']);

    	  $set_info   =   array("CompanyID = :CompanyID");
    	  $where_info =   array("wotcID = :wotcID");
	  $params = array(":wotcID"=>$ID,":CompanyID"=>$C['CompanyID']);
    	  G::Obj('GenericQueries')->updRowsInfo("OrgData", $set_info, $where_info, array($params));


	}
}



?>
