#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/../cron') . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

// Setup WOTCID in Bulk
echo "\nStart: " . date("F j, Y, g:i a") . "\n\n";

G::Obj('GenericQueries')->conn_string =   "WOTC";

$query = 'SELECT Billing.wotcID, ApplicantData.wotcID, ApplicantData.ApplicationID
 FROM ApplicantData
  LEFT JOIN Billing ON ApplicantData.wotcID = Billing.wotcID AND ApplicantData.ApplicationID = Billing.ApplicationID
   WHERE ApplicantData.wotcID IN ';
$query .= '("dycom10","dycom11","dycom14","dycom151","dycom152","dycom154","dycom155","dycom156","dycom157","dycom184","dycom223","dycom228","dycom229","dycom235","dycom25","dycom26","dycom292","dycom298","dycom299","dycom37","dycom48","dycom9","dycom1","dycom2","dycom3","dycom4","dycom5","dycom6","dycom169","dycom8","dycom12","dycom13","dycom143","dycom15","dycom16","dycom17","dycom18","dycom258","dycom153","dycom19","dycom20","dycom203","dycom21","dycom22","dycom23","dycom24","dycom241","dycom243","dycom240","dycom30","dycom31","dycom32","dycom33","dycom34","dycom35","dycom36","dycom38","dycom39","dycom40","dycom41","dycom42","dycom43","dycom108","dycom179","dycom283","dycom284","dycom285","dycom286","dycom293","dycom294","dycom296","dycom297","dycom45","dycom46","dycom47","dycom29","dycom268","dycom27","dycom279","dycom28","dycom280","dycom49","dycom50","dycom51","dycom52","dycom53","dycom167","dycom168","dycom233","dycom260","dycom261","dycom267","dycom290","dycom295","dycom55","dycom56","dycom57","dycom58","dycom59","dycom60","dycom61","dycom62","dycom63","dycom64","dycom65","dycom7","dycom201","dycom202","dycom204","dycom205","dycom207","dycom208","dycom209","dycom236","dycom237","dycom66","dycom67","dycom68","dycom69","dycom71","dycom72","dycom73","dycom230","dycom231","dycom242","dycom248","dycom250","dycom272","dycom74","dycom75","dycom76","dycom225","dycom257","dycom77","dycom78","dycom79","dycom80","dycom81","dycom82","dycom85","dycom86","dycom88","dycom89","dycom90","dycom91","dycom200","dycom224","dycom92","dycom93","dycom94","dycom95","dycom253","dycom96","dycom97","dycom98","dycom99","dycom100","dycom101","dycom102","dycom103","dycom172",';
$query .= '"dycom245","dycom246","dycom247","dycom291","dycom104","dycom105","dycom106","dycom107","dycom263","dycom44","dycom54","dycom83","dycom84","dycom109","dycom110","dycom244","dycom287","dycom288","dycom289","dycom254","dycom262","dycom111","dycom112","dycom113","dycom114","dycom115","dycom116","dycom117","dycom118","dycom119","dycom120","dycom121","dycom122","dycom123","dycom124","dycom125","dycom126","dycom127","dycom128","dycom193","dycom206","dycom249","dycom266","dycom273","dycom274","dycom275","dycom276","dycom277","dycom278","dycom129","dycom130","dycom131","dycom269","dycom270","dycom271","dycom132","dycom133","dycom134","dycom135","dycom136","dycom137","dycom138","dycom139","dycom140","dycom142","dycom158","dycom159","dycom160","dycom161","dycom162","dycom163","dycom164","dycom165","dycom166","dycom226","dycom227","dycom264","dycom265","dycom141","dycom170","dycom171","dycom173","dycom252","dycom216","dycom217","dycom218","dycom219","dycom220","dycom221","dycom222","dycom251","dycom174","dycom175","dycom238","dycom239","dycom144","dycom150","dycom176","dycom177","dycom178","dycom180","dycom181","dycom145","dycom182","dycom183","dycom255","dycom256","dycom185","dycom186","dycom187","dycom188","dycom189","dycom190","dycom191","dycom210","dycom211","dycom212","dycom213","dycom234","dycom259","dycom281","dycom282","dycom232","dycom192","dycom194","dycom195","dycom196","dycom197","dycom198","dycom199")
   AND ApplicantData.Qualifies = "Y"  AND Billing.wotcID IS NULL';

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

foreach ($RESULTS AS $R) {

	    $total          =   0;
	    $TaxCredit      =   0;

	    $query          =   "SELECT MAX(Year), SUM(TaxCredit) FROM Billing WHERE wotcID = '" . $R['wotcID'] . "' AND ApplicationID = '" . $R['ApplicationID'] . "'";
	    $billing_info   =   G::Obj('GenericQueries')->getRowInfoByQuery($query);

	    list ($year, $total) =  array_values($billing_info);

	    $year ++;

	    list ($TaxCredit, $active, $comment) = G::Obj('WOTCCalculator')->calculate_wotcCredit($R['wotcID'], $R['ApplicationID'], $year);

	    $TaxCredit += $total;

echo "WOTCID: " . $R['wotcID'] . ", ApplicationID: " . $R['ApplicationID'] . " - " . $TaxCredit . "\n";


}


echo "\n\nCredit: " . $TaxCredit . "\n\n";





echo "\n\nFinish: " . date("F j, Y, g:i a") . "\n";

?>
