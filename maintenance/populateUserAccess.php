#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/../cron') . '/Configuration.inc';

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";


$query = "SELECT year(Users.LastAccess) AS year, Users.UserID, OrgData.CompanyID, count(Access.wotcID) AS CNT, count(OrgData.CompanyID) AS CNT2,
group_concat(Access.wotcID) AS ACCESS, group_concat(OrgData.wotcID) AS OD,
Users.Partner, Users.LastAccess FROM Users
LEFT JOIN Access ON Access.UserID = Users.UserID
LEFT JOIN OrgData ON OrgData.wotcID = Access.wotcID
LEFT JOIN CRM ON CRM.CompanyID = OrgData.CompanyID
GROUP BY Users.UserID
ORDER BY Users.Partner, Users.LastAccess DESC";

$ACCESS =   G::Obj('GenericQueries')->getInfoByQuery($query);

$i=0;
$delete = "";
$d=0;
$noassign = "";
$n=0;
$update = "";
$u=0;
$except = "";
$e=0;

$DELUSERS=array('edgemaster','cmswotcadmin','potteradmin','irecruitadmin','aandmadmin');
$DYCOM=array('dycomindustries','dycomadmin');

foreach ($ACCESS as $A) {
	$i++;

	if (($A['CompanyID'] == null && $A['CNT'] == 0) || ($A['LastAccess'] == '0000-00-00 00:00:00') || ( in_array($A['UserID'],$DELUSERS)) || $A['year'] <= '2019') {
		$delete .= 'delete user: ' . $A['UserID'];
		$delete .= ' Partner: ' . $A['Partner'] . ' LastAccess: ' . $A['LastAccess'];
		$delete .= ' Access Count: ' . $A['CNT'];
	        $delete .= "\n";

		$where_info  =   array("UserID = :UserID");
		$params      =   array(":UserID"=>$A['UserID']);
		G::Obj('WOTCUsers')->delUsersInfo($where_info, array($params));

		$d++;
	} else if ($A['CompanyID'] != '' && ($A['CNT'] == $A['CNT2']) || ( in_array($A['UserID'],$DYCOM)) ) {
		$update .= 'update: ' . $A['UserID'] . ' with ' . $A['CompanyID'];
		$update .= ' Partner: ' . $A['Partner'] . ' LastAccess: ' . $A['LastAccess'];
	        $update .= "\n";
		$u++;

        	$set_info   =   array(
                            "CompanyID = :CompanyID"
                );
        	$where      =   array(
                            "UserID     =   :UserID"
                        );
        	$params     =   array(
                            ':CompanyID'    =>  $A['CompanyID'],
                            ':UserID'       =>  $A['UserID']
                        );

        	G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array($params));

	} else if ($A['CompanyID'] == '' && ($A['CNT'] == $A['CNT2'])) {
		$noassign .= 'Assign wotcID to CRM: ' . $A['UserID'];
		$noassign .= ' Partner: ' . $A['Partner'] . ' LastAccess: ' . $A['LastAccess'];
	        $noassign .= "\n";
		$ACCESS=explode(',',$A['ACCESS']);
		foreach ($ACCESS AS $AC) {
			$noassign .= " - wotcID: " . $AC . "\n";
		}
	        $noassign .= "\n";
		$n++;
	} else {

		// update these two
		$except .= 'user exception: ' . $A['UserID'] . '-' . $A['CNT'] . '=' .  $A['CNT2'];
		$except .= ' Partner: ' . $A['Partner'] . ' LastAccess: ' . $A['LastAccess'];
		$except .= ' Count: ' . $A['CNT'] . ' = ' . $A['CNT2'];
		$except .= ' CompanyID: ' . $A['CompanyID'];
		$except .= "\n";
		  $ACCESS=explode(',',$ACCESSLIST);
		  $OD=explode(',',$ODLIST);
		  $except .= print_r(array_diff($ACCESS,$OD)) . "\n";
		$e++;
	}

}

echo 'Total Users: ' . $i . "\n\n";

echo 'Delete: ' . $d . "\n";
echo $delete . "\n";

echo 'Update: ' . $u . "\n";
echo $update . "\n";

echo 'No Assign: ' . $n . "\n";
echo $noassign . "\n";

echo 'Except: ' . $e . "\n";
echo $except . "\n";

?>
