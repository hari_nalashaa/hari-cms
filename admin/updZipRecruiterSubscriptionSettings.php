<?php 
include 'Configuration.inc';

$columns = "OrgID, RequestID";
$subscribed_requisitions = $ZipRecruiterObj->getZipRecruiterSubscriptionsInfo($columns, $_REQUEST['OrgID']);

if($_REQUEST['subscriptions'] < $subscribed_requisitions['count']) {
    echo 0;
}
else {
    $ins_result = $ZipRecruiterObj->insZipRecruiterSubscriptionSettings($_REQUEST['OrgID'], $_REQUEST['subscriptions']);
    
    echo $ins_result['affected_rows'];
}
?>