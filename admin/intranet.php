<?php
include 'Configuration.inc';

require_once VENDOR_DIR . 'autoload.php';

$OPEN = 'N'; // != 'Y' restricts - 'Y' to assure server stays open for testing

$intra_info = $IntranetAccessObj->getIntranetAccessInfoByIpAddress("*", $_SERVER['REMOTE_ADDR']);
$ipck       = $intra_info['IntranetAccess'];

if ($ipck != "") {
  setcookie("AID","",time()-3600,"/");
  header("Location: " . ADMIN_HOME . "login.php");
  exit();
}

$AUTH = $AdminUsersObj->getUserInfoByUserID($_POST['user'], "UserID, Verification");

if (password_verify($_POST['password'],$AUTH['Verification'])) {
    if (password_needs_rehash($_POST['password'], PASSWORD_DEFAULT)) {
        //update user
        $set_info   = array("Verification = :Verification");
        $where_info = array("UserID = :UserID");
        $params     = array(":Verification"=>password_hash( $_POST['password'] , PASSWORD_DEFAULT ), ":UserID"=>$AUTH['UserID']);
        $AdminUsersObj->updUsersInfo($set_info, $where_info, array($params));
    }
} else {
    $AUTH   =   array();
}

if ($AUTH['UserID']) {

    //Insert Intranet Access Information
    $info = array("UserID"=>$AUTH['UserID'], "IPADDRESS"=>$_SERVER['REMOTE_ADDR']);
    $IntranetAccessObj->insUpdIntranetAccess($info);

    //set a new Session ID
    //randomize a session key
    $SESSIONID  =   $GenericLibraryObj->generateRandomSessionKey();

    if ($_COOKIE['AID']) {
      $SESSIONID = $_COOKIE['AID']; 
    }

    // update user
    $set_info   =   array("LastAccess = NOW()", "SessionID = :SessionID");
    $where      =   array("UserID = :UserID");
    $params     =   array(":SessionID"=>$SESSIONID, ":UserID"=>$AUTH['UserID']);
    if ($keep == "yes") $set_info[] = "Persist = 1";
    $AdminUsersObj->updUsersInfo($set_info, $where, array($params));

    // set session key on users browser
    setcookie("AID", $SESSIONID, time()+7200, "/");
    
    if ($OPEN != 'Y') {
      $ACCESS_RES   = $IntranetAccessObj->getIntranetAccessInfo("distinct(IPADDRESS) AS IPADDRESS", array("Area in ('cms','intranet')"));
      $ACCESS       = $ACCESS_RES['results'];
    }

    $where_info     = array("Area IN ('intranet','cms','callcenter')");
    $CALLACCESS_RES = $IntranetAccessObj->getIntranetAccessInfo("distinct(IPADDRESS) AS IPADDRESS", $where_info);
    $CALLACCESS     = $CALLACCESS_RES['results'];

$phpversion = <<<END

<Files ~ ".(cgi)$">
allow from all
satisfy any
</Files>

AddType application/x-httpd-php .html .htm

END;

$access = <<<END
RewriteEngine On
RewriteCond %{HTTPS} !=on
RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI} [R,L]

<FilesMatch "\.(mstr|inc)$">
   Order allow,deny
   Deny from all
</FilesMatch>

Options -Indexes

END;

$admin = $access;
$admin .= "\n\n";
$admin .= <<<END
<FilesMatch "(intranet.php|login.php)$">
   Order allow,deny
   Allow from all
</FilesMatch>
END;

$callcenter = $access;
$callcenter .= "\nDirectoryIndex admin.php";
$callcenter .= "\n\n";

$filename=realpath(__DIR__ . '/..') . '/.htaccess';
$fh = fopen($filename, 'w');
if (!$fh) { die('Cannot open file ' . $filename); }
fwrite($fh, $access . $phpversion);
fclose($fh);

$filename=realpath(__DIR__ . '/..') . '/wotc/.htaccess';
$fh = fopen($filename, 'w');
if (!$fh) { die('Cannot open file ' . $filename); }
fwrite($fh, $callcenter . $phpversion);
fclose($fh);

$filename=realpath(__DIR__ . '/..') . '/admin/.htaccess';
$fh = fopen($filename, 'w');
if (!$fh) { die('Cannot open file ' . $filename); }
fwrite($fh, $admin . $phpversion);
fclose($fh);


// lock down all sites on Development and Quality
if ($_SERVER ['SERVER_NAME'] != "irecruit.pairsite.com") {

$intranet = $access;

$intranet .= "\n\n";

if (sizeof($ACCESS) > 0) {

$intranet .= <<<END
order deny,allow
deny from all
END;

$intranet .= "\n";

}

    foreach ($ACCESS as $IA) {
      $intranet .= "allow from " . $IA['IPADDRESS'] . "\n";
    } // end while
    
    if ((preg_match("/david/i", realpath(__DIR__))) || (preg_match("/hari/i", realpath(__DIR__)))) {
      $filename=realpath(__DIR__ . '/..') . '/.htaccess';
      $fh = fopen($filename, 'w');
      if (!$fh) { die('Cannot open file ' . $filename); }
      fwrite($fh, $access . $phpversion); 
      fclose($fh);
    } else {
      $filename=realpath(__DIR__ . '/..') . '/.htaccess';
      $fh = fopen($filename, 'w');
      if (!$fh) { die('Cannot open file ' . $filename); }
      fwrite($fh, $intranet . $phpversion);
      fclose($fh);
    }

} // end Not Production


// lock down Call Center and Admin on Production
if ($_SERVER ['SERVER_NAME'] == "irecruit.pairsite.com") {

/* ADMIN */
$admin .= "\n\n";

if (sizeof($ACCESS) > 0) {

$admin .= <<<END
order deny,allow
deny from all
END;

$admin .= "\n";

}

foreach ($ACCESS as $IA) {
  $admin .= "allow from " . $IA['IPADDRESS'] . "\n";
} // end while

$filename=realpath(__DIR__ . '/..') . '/admin/.htaccess';
$fh = fopen($filename, 'w');
if (!$fh) { die('Cannot open file ' . $filename); }
fwrite($fh, $admin . $phpversion);
fclose($fh);
/* ADMIN */


/* CALLCENTER */
$callcenter .= "\n\n";

if (sizeof($CALLACCESS) > 0) {

$callcenter .= <<<END
<FilesMatch "(callcenter).php$">
<limit GET>
order deny,allow
deny from all
END;

$callcenter .= "\n";

}

foreach ($CALLACCESS as $CIA) {
  $callcenter .= "allow from " . $CIA['IPADDRESS'] . "\n";
} // end while

$callcenter .= <<<END
</limit>
</FilesMatch>

END;

$filename=realpath(__DIR__ . '/..') . '/wotc/.htaccess';
$fh = fopen($filename, 'w');
if (!$fh) { die('Cannot open file ' . $filename); }
fwrite($fh, $callcenter . $phpversion);
fclose($fh);
/* CALLCENTER */

} // end Production

$where_info     = array("UserID = :UserID");
$params = array(":UserID"=>$AUTH['UserID']);
$RESULTS = G::Obj('IntranetAccess')->getIntranetAccessInfo("*", $where_info, "", $params);

$IPADDRESS = $RESULTS['results'][0]['IPADDRESS'];
$SystemAccess = json_decode($RESULTS['results'][0]['SystemAccess'],true);

if ((is_array($SystemAccess)) && ($_POST['AWS'] != "N")) {
  foreach ($SystemAccess as $SA) {
  G::Obj('AWSSecurityGroups')->updateAWS ($SA['server'], $SA['region'], $SA['policy_grp_name'], $SA['port_from'], $SA['port_to'], $IPADDRESS, $AUTH['UserID']);
  }
}

// go to application
header("Location: " . ADMIN_HOME);
exit;

} // end if AUTH UserID 

echo $ADMINObj->adminHeader();
echo "<div style=\"line-height:210%;text-align:right;width:350px;margin-top:80px;padding:25px 20px 10px 10px;border:1px solid #000;border-radius:10px;\">\n";
echo "<form method=\"POST\" action=\"intranet.php\">\n";
echo "UserID: <input type=\"text\" name=\"user\" value=\"\" size=\"30\" maxlength=\"45\">\n";
echo "<br>";
echo "Password: <input type=\"password\" name=\"password\" value=\"\" size=\"30\" maxlength=\"45\">\n";
echo "<br><br>";
echo "<input type=\"hidden\" name = \"AWS\" value=\"" . $_GET['AWS'] . "\">\n";
echo "<input type=\"submit\" value=\"Access Intranet\">\n";
echo "</form>\n";
echo "<p><a href=\"javascript:ReverseDisplay('emailcheck')\">forgot your username or password?</a></p>\n";
echo "</div>\n";
?>
<div id="emailcheck" style="display:none;margin:0 0 0 30px;">
<p>Please enter either the email address or user name associated to this account.<br>We will send your login credentials to the email address on record.</p>
<form method="POST" action="intranet.php">
<?php if (!$type) { $type='email'; } ?>
<p><input type="radio" name="type" value="username"<?php if ($type == "username") { echo ' checked'; } ?>>Username
<input type="radio" name="type" value="email"<?php if ($type == "email") { echo ' checked'; } ?>>Email Address
<input type="text" name="lookup" size="30">
<input value="Lookup" name="submit" type="submit"></p>
</form>
</div>
<div style="margin:0 0 0 100px;">
<?php

if ($_POST['submit'] == "Lookup") {

  if ($_POST['type'] == "username") {
    $AUTH   =   G::Obj('AdminUsers')->getUserInfoByUserID($_POST['lookup'], "Email, UserID");
  } else {
    $AUTH   =   G::Obj('AdminUsers')->getUserInfoByEmail($_POST['lookup'], "Email, UserID");
  }

  if ($AUTH['Email']) {

    $tempsessionid  = G::Obj('GenericLibrary')->generateRandomSessionKey();

    // update user
    $set_info   = array("SessionID = :SessionID");
    $where_info = array("UserID = :UserID", "Email = :Email");
    $params     = array(":SessionID"=>$tempsessionid, ":UserID"=>$AUTH['UserID'], ":Email"=>$AUTH['Email']);
    $AdminUsersObj->updUsersInfo($set_info, $where_info, array($params));

    $to         =   $AUTH['Email'];
    $subject    =   "Intranet Reminder";
    
    $message    =   "\nYour user name is: " . $AUTH['UserID'] . "\n\n";
    $message   .=   "Please reset your password here: " . ADMIN_HOME . "reset.php";
    $message   .=   "?s=" . $tempsessionid;

    $PHPMailerObj->clearCustomProperties();

    // Set who the message is to be sent to
    $PHPMailerObj->addAddress ( $to );
    // Set the subject line
    $PHPMailerObj->Subject = $subject;
    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );
    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/plain';
    //Send email
    $PHPMailerObj->send ();
    
    echo "<p>Recovery email has been sent to your account.</p>";

  } else {

    if ($_POST['type'] == "username") {
      echo "<p>Information for that user name was not found.</p>";
    } else if ($_POST['type'] == "email") {
      echo "<p>Information for that email address was not found.</p>";
    }
  }

}

echo "</div>\n";

echo $ADMINObj->adminFooter();
?>
