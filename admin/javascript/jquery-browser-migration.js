(function(jQuery) {
    jQuery.browser || "1.3.2" == jQuery.fn.jquery || (jQuery.extend({
        browser: {}
    }), jQuery.browser.init = function() {
        var b = {};
        try {
            navigator.vendor ? /Chrome/.test(navigator.userAgent) ? (b.browser = "Chrome", b.version = parseFloat(navigator.userAgent.split("Chrome/")[1].split("Safari")[0])) : /Safari/.test(navigator.userAgent) ? (b.browser = "Safari", b.version = parseFloat(navigator.userAgent.split("Version/")[1].split("Safari")[0])) : /Opera/.test(navigator.userAgent) && (b.Opera = "Safari", b.version = parseFloat(navigator.userAgent.split("Version/")[1])) :
                /Firefox/.test(navigator.userAgent) ? (b.browser = "mozilla", b.version = parseFloat(navigator.userAgent.split("Firefox/")[1])) : (b.browser = "MSIE", /MSIE/.test(navigator.userAgent) ? b.version = parseFloat(navigator.userAgent.split("MSIE")[1]) : b.version = "edge")
        } catch (c) {
            b = c
        }
        jQuery.browser[b.browser.toLowerCase()] = b.browser.toLowerCase();
        jQuery.browser.browser = b.browser;
        jQuery.browser.version = b.version;
        jQuery.browser.chrome = "chrome" == jQuery.browser.browser.toLowerCase();
        jQuery.browser.safari = "safari" == jQuery.browser.browser.toLowerCase();
        jQuery.browser.opera =
            "opera" == jQuery.browser.browser.toLowerCase();
        jQuery.browser.msie = "msie" == jQuery.browser.browser.toLowerCase();
        jQuery.browser.mozilla = "mozilla" == jQuery.browser.browser.toLowerCase()
    }, jQuery.browser.init())
})(jQuery);