var tinymce_editor = function() {
	
	tinymce.init({
		selector: "textarea.mceEditor",
	    setup: function (editor) {
	        editor.on('change', function () {
	            editor.save();
	        });
	    },
	    browser_spellcheck : true, 
	    mode: "specific_textareas",
	    editor_selector: "mceEditor",
	    editor_deselector : "mceNoEditor",
	    plugins: ["searchreplace wordcount code print link lists anchor visualblocks media insertdatetime"],
	    toolbar: "insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code print preview",
	    style_formats: [
	            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
	            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
	            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
	            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
	            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
	            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
	            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
	            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
	            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
	            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
	            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
	            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
	            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
	        ],
	    theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
	    font_size_style_values: "12px,13px,14px,16px,18px,20px",
	    relative_urls : true,
	    toolbar_items_size: 'small',
	    resize: 'both',
	    height : 250
	});
}

tinymce_editor();


var tinymce_editor_advertise = function() {

	tinymce.init({
		selector: "textarea.AdvertisingOptionmceEditor",
	    setup: function (editor) {
	        editor.on('change', function () {
	            editor.save();
	        });
	    },
	    browser_spellcheck : true, 
	    mode: "specific_textareas",
	    editor_selector: "mceEditor",
	    editor_deselector : "mceNoEditor",
	    plugins: ["searchreplace wordcount code print link lists anchor visualblocks media insertdatetime"],
	    toolbar: "insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code print preview",
	    style_formats: [
	            {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
	            {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
	            {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
	            {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
	            {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
	            {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
	            {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
	            {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
	            {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
	            {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
	            {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
	            {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
	            {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
	        ],
	    theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px,20px",
	    font_size_style_values: "12px,13px,14px,16px,18px,20px",
	    relative_urls : true,
	    toolbar_items_size: 'small',
	    resize: 'both',
	    height : 100
	});
}

tinymce_editor_advertise();