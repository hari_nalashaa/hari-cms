function getTwilioUsersListByOrgID(OrgID) {
	
	var input_data = {"TwilioConversationOrgID":OrgID};
	$('#spnUsersListLoading').html(".... Please wait loading");
	
	$.ajax({
		method: "POST",
  		url: 'irecruit/ajax/getTwilioUsersListByOrgID.php', 
		type: "POST",
		data: input_data,
		dataType: 'json',
		success: function(data) {
			var users_length = data.length;

			$('#TwilioUsersList option').remove();
			$('#TwilioUsersList').append($("<option value=''></option>").attr("value",'').text('Select User'));
			
			for(var u = 0; u < users_length; u++) {

				$('#TwilioUsersList').
					append($("<option value=''></option>").
					attr("value", data[u].UserID).
					text(data[u].FirstName + " " + data[u].LastName));
				
			}
			//Set the loading message span as empty
			$('#spnUsersListLoading').html("");
    	}
	});
	
}

function exportTwilioConversations() {

	document.forms['frmTwilioConversationsReportExport'].action	=	"irecruit/getTwilioConversationsReportExport.php";
		 
	var OrgID		=	document.frmTwilioConversationsReport.TwilioOrgsList.value;
	var UserID		=	document.frmTwilioConversationsReport.TwilioUsersList.value;
	var FromDate	=	document.frmTwilioConversationsReport.txtFromDate.value;
	var ToDate		=	document.frmTwilioConversationsReport.txtToDate.value;

	if(OrgID == "") {
		alert("Please select organization");
		return false;
	}
	
	document.getElementById('hideTwilioOrgsList').value		=	OrgID;
	document.getElementById('hideTwilioUsersList').value	=	UserID;
	document.getElementById('hideFromDate').value			=	FromDate;
	document.getElementById('hideToDate').value				=	ToDate;

	document.forms['frmTwilioConversationsReportExport'].submit();
	
}

function setConversationsList(OrgID, UserID, FromDate, ToDate, IndexStart) {
	
	if(OrgID != "") {
		var input_data = {"TwilioConversationOrgID":OrgID, "TwilioConversationUserID":UserID, "FromDate":FromDate, "ToDate":ToDate};
		
		$('#divTwilioConversationsList').html("<br><br>.... Please wait loading");
		
		$.ajax({
			method: "POST",
			url: 'irecruit/ajax/getTwilioConversationsListByFilters.php?IndexStart='+IndexStart,
			type: "POST",
			data: input_data,
			dataType: 'json',
			success: function(data) {

				var json_data			=	data;
				var conversations_len	=	json_data.conversations_info.length;
				var conversations_res	=	json_data.conversations_info;
				
				var app_prev			=	json_data.previous;
				var app_next			=	json_data.next;
				var total_pages			=	json_data.total_pages;
				var current_page		=	json_data.current_page;
				var conversations_count	=	json_data.conversations_count;

				var conversations_list	=	"<br><br>";
				conversations_list	+=	"<h3>Conversations List</h3>";
				conversations_list	+=	"<table class='table table-bordered' cellspacing='0' cellpadding='10' border='1' width='95%' style='border-collapse: collapse; border:1px solid #ddd;'>";
				conversations_list	+=	"<tr>";
				conversations_list	+=	"<th align='left'>User ID</th>";
				conversations_list	+=	"<th align='left'>Conversation ID</th>";
				conversations_list	+=	"<th align='left'>Messages Count</th>";
				conversations_list	+=	"</tr>";
				
				if(conversations_len > 0) {
					for(c = 0; c < conversations_len; c++) {						
						var UserID 			= 	conversations_res[c].UserID;
						var ResourceID		=	conversations_res[c].ResourceID;
						var CreatedDateTime	=	conversations_res[c].CreatedDateTime;
						var MessagesCount	=	conversations_res[c].MessagesCount;

						conversations_list	+=	"<tr>";
						conversations_list	+=	"<td align='left'>"+UserID+"</td>";
						conversations_list	+=	"<td align='left'>"+ResourceID+"</td>";
						conversations_list	+=	"<td align='left'>"+MessagesCount+"</td>";
						conversations_list	+=	"</tr>";
					}

					if(total_pages > 1) {
						conversations_list	+=	"<tr>";
						conversations_list 	+=	"<td colspan='3' align='left'>";
						conversations_list 	+=	"<span>"+app_prev+"</span>&nbsp;&nbsp;";
						conversations_list 	+=	"<span>"+current_page+' - '+total_pages+"</span>&nbsp;&nbsp;";
						conversations_list 	+=	"<span>"+app_next+"</span>&nbsp;&nbsp;";
						conversations_list 	+=	"</td>";
						conversations_list	+=	"</tr>";
					}
				}
				else {
					conversations_list	+=	"<tr>";
					conversations_list	+=	"<td colspan='3' align='center'>There are no conversations for this user in this date range.</td>";
					conversations_list	+=	"</tr>";
				}
				
				conversations_list	+=	"</table>";
				
				$('#divTwilioConversationsList').html(conversations_list);
	    	}
		});
	}

}

function getRecordsByPage(IndexStart) {
	document.frmTwilioConversationsReport.IndexStart.value = IndexStart;
	
	var OrgID		=	document.frmTwilioConversationsReport.TwilioOrgsList.value;
	var UserID		=	document.frmTwilioConversationsReport.TwilioUsersList.value;
	var FromDate	=	document.frmTwilioConversationsReport.txtFromDate.value;
	var ToDate		=	document.frmTwilioConversationsReport.txtToDate.value;

	setConversationsList(OrgID, UserID, FromDate, ToDate, IndexStart);
}

function getTwilioConversationsList() {
	
	var OrgID		=	document.frmTwilioConversationsReport.TwilioOrgsList.value;
	var UserID		=	document.frmTwilioConversationsReport.TwilioUsersList.value;
	var FromDate	=	document.frmTwilioConversationsReport.txtFromDate.value;
	var ToDate		=	document.frmTwilioConversationsReport.txtToDate.value;
	var IndexStart	=	document.frmTwilioConversationsReport.IndexStart.value;
	
	setConversationsList(OrgID, UserID, FromDate, ToDate, IndexStart);
}