<?php
include 'Configuration.inc';

$where = array("MultiOrgID = ''");

if ($_POST['keyword'] != "") {
    $query_parts    =   array();
    $query_parts[]  =   "OrgID LIKE '%".trim($_POST['keyword'])."%'";
    $query_parts[]  =   "OrganizationName LIKE '%".trim($_POST['keyword'])."%'";

    $where[]     .=   "(" . implode(" OR ", $query_parts) . ")";
}

$ORGDATA_RES    =   $OrganizationsObj->getOrgDataInfo("OrgID, OrganizationName", $where, 'OrganizationName LIMIT 20');
$ORGDATA        =   $ORGDATA_RES['results'];

echo '<ul id="org-list">';
foreach ($ORGDATA as $OD) {
    $org_text = $OD['OrgID'] . ' - ' . $OD['OrganizationName'];
    echo '<li onClick=\'getSubscriptionInfo(this);\'>' . $org_text . '</li>';
}
echo '</ul>';
