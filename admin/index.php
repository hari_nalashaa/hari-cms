<?php 
include 'Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

if(isset($_REQUEST['make_hired']) && $_REQUEST['make_hired'] == 'yes')
{ 
	$set_info	=	array("ApplicationStatus = 'H'");
	$where_info	=	array("wotcID = :wotcID", "ApplicationID = :ApplicationID", "WotcFormID = :WotcFormID");
	$params		=	array(":wotcID"=>$_REQUEST['wotcID'], ":ApplicationID"=>$_REQUEST['ApplicationID'], "WotcFormID"=>$_REQUEST['WotcFormID']);
	G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
	
	$wotc_administration = ADMIN_HOME . "index.php?pg=wotc_administration&msg=suc_hired";
	
	header("Location:".$wotc_administration);
	exit();
}
 
if (($_REQUEST['pg'] == "wotc_hours") && ($_REQUEST['process'] == "Y")) {
            include "wotc/Hours.inc";
}

if ($_POST['wotcbulk'] == "Export Bulk File") {

    $DATA   =   G::Obj('WOTCAdmin')->queryStepTwo();
    include ADMIN_DIR . 'wotc/BulkFile.inc';

}
if ($_POST['export'] == "Export to Excel") {
    
    $APPLICANTDATA = G::Obj('WOTCAdmin')->queryApplicants();
    include ADMIN_DIR . 'wotc/Export.inc';
}

if ($_POST['exportCRM'] == "Export to Excel") {
        
    $APPLICANTDATA = G::Obj('WOTCAdmin')->queryApplicantsCRM();
    include ADMIN_DIR . 'wotc/Export.inc';
}

if ($_POST['export'] == "Needs Processing") {
    $APPLICANTDATA = G::Obj('WOTCAdmin')->queryApplicantsCRM();
    include ADMIN_DIR . 'wotc/ExportNeedsProcessing.inc';
}

if ($_GET['export'] == "exportrecent") { 
    include ADMIN_DIR . 'wotc/ExportRecent.inc';
}

if ($_GET['export'] == "exportdycomhires") {
    include ADMIN_DIR . 'wotc/ExportDycomHires.inc';
}

if ($_GET['export'] == "exportcredits") { 
    include ADMIN_DIR . 'wotc/ExportCredits.inc';
}

if ($_GET['export'] == "exportpayroll") { 
    include ADMIN_DIR . 'wotc/ExportPayroll.inc';
}

if ($_GET['export'] == "irecruitbillingcounts") {
    include ADMIN_DIR . 'irecruit/iRecruitBillingCounts.inc';
}
if($_POST['pg'] == "custom_webforms_save"){  
     $WebFormID= $_SESSION['WebFormID'];
     $OrgID= $_SESSION['OrgID'];



//Delete existing
 $webform = IRECRUIT_DIR . 'formsInternal/PDFForms/Custom/'.$OrgID.'-'.$WebFormID.'.pdf';

         //delete file
         if (file_exists($webform)) { 
             unlink($webform);

          }

	//file upload

   $pdfform1 = IRECRUIT_DIR . 'formsInternal/PDFForms';


         if (!file_exists($pdfform1 )) {  
            mkdir($pdfform1 , 0700);
            chmod($pdfform1 , 0777);
        }

     $pdfform2 = $pdfform1.'/Custom';

       if (!file_exists($pdfform2)) {  
            mkdir($pdfform2 , 0700);
            chmod($pdfform2 , 0777);
        }

        $target_file = $pdfform2.'/'.$OrgID.'-'.$WebFormID.'.pdf';
  	move_uploaded_file($_FILES["newfile"]["tmp_name"], $target_file);
  	header("Location: " . ADMIN_HOME . "index.php?pg=custom_webforms&search=".$OrgID);
 	exit();

}


if ($_REQUEST['pg'] == "custom_webforms_delete") { 
        $webform = IRECRUIT_DIR . 'formsInternal/PDFForms/Custom/'.$_REQUEST['OrgID'].'-'.$_REQUEST['WebFormID'].'.pdf';
        $OrgID = $_REQUEST['OrgID'];
        $WebformID = $_REQUEST ['WebFormID'];
         //delete file
         if (file_exists($webform)) { 
             unlink($webform);
          }
          
          //delete form data
          $delwhere_info =   array("OrgID = :OrgID","FormID= :FormID");
          $delparams    =   array(":OrgID" => $OrgID,":FormID" => $WebformID);
          $delete_record = G::Obj('GenericQueries')->delRows("CustomPDFForms", $delwhere_info, array($delparams));
            
          header("Location: " . ADMIN_HOME . "index.php?pg=custom_webforms&search=".$OrgID);
 	  exit();

  }

if($_GET['pg']=="custom_webforms_update"){ 
 if(!empty($_POST)){
        foreach($_POST['pdffield'] as $j=>$datum){    
                 

	//WebFormQuestions update	
	         // if(!empty($_POST['pdffield'][$j])){ 
                $query_pdffield ="select * from CustomPDFForms where FormID ='".$_POST['WebFormID']."' and QuestionID='".$_POST['QuestionID'][$j]."' and  OrgID='".$_POST['OrgID']."' ";
                $alldata = G::Obj('GenericQueries')->getInfoByQuery($query_pdffield, array());

                if(!empty($alldata)){ 

                  if(empty($datum)){
		     //delete record
			 $delwhere_info =   array("OrgID = :OrgID","QuestionID= :QuestionID","FormID= :FormID");
                         $delparams    =   array(":OrgID" => $_POST['OrgID'],":QuestionID" =>$_POST['QuestionID'][$j],":FormID" => $_POST['WebFormID']);
                         $delete_record = G::Obj('GenericQueries')->delRows("CustomPDFForms", $delwhere_info, array($delparams));
                        }else{
                     //CustomPDFForms update
                      $update_query   =   "UPDATE CustomPDFForms SET PDFField = :PDFField WHERE OrgID = :orgid and  FormID =:FormID and QuestionID=:QuestionID";
                     $params1         =   array(":PDFField" =>$datum, ":orgid" => $_POST['OrgID'],":QuestionID" => $_POST['QuestionID'][$j],":FormID" => $_POST['WebFormID']);
                     $pdffield_update   = G::Obj('GenericQueries')->updInfoByQuery($update_query, array($params1));
                     $_SESSION['status'] =1;
                     } 
          	}else{
                //CustomPDFForms insert
   		 $info       =   array("OrgID" => $_POST['OrgID'],"PDFField" => $datum,"QuestionID"=>$_POST['QuestionID'][$j],"FormID"=>$_POST['WebFormID']);
  		 $pdffield_insert = G::Obj('GenericQueries')->insRowsInfo("CustomPDFForms", $info);
                  $_SESSION['status'] =1;
                }
	//}
    }
   

header("Location: " . ADMIN_HOME . "index.php?pg=custom_webforms_edit&WebFormID=".$_POST['WebFormID']."&OrgID=".$_POST['OrgID']);
 	exit();
}

}


if ($_GET['mq'] != "") {
    list ($wotcID, $ApplicationID) = explode(":", $_GET['mq']);
    
    if (($wotcID != "") && ($ApplicationID != "")) {
        $set_info   =   array("Qualifies = 'Y'");
        $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
        $params     =   array(":wotcID" => $wotcID, ":ApplicationID" => $ApplicationID);
        G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
    }
    
    header("Location: " . ADMIN_HOME . "index.php?pg=wotc_processing");
    exit();
}

require_once 'Header.inc';
require_once 'Navigation.inc';
require_once 'PageWrapperStart.inc';

        echo  "<!DOCTYPE html>\n";
       echo  "<html>\n";
       echo "<head>\n";
        echo  "<meta charset=\"UTF-8\">\n";
        echo "<title>CMS Administration</title>\n";
        
        echo  "<!-- Favicons-->\n";
        echo  "<link rel=\"shortcut icon\" href=\"" . ADMIN_HOME . "images/fav.png?v=3\">\n";
        echo  "<link rel=\"icon\" href=\"" . ADMIN_HOME . "images/fav.png?v=3\">\n";
       echo  "<link rel=\"apple-touch-icon\" href=\"" . ADMIN_HOME . "images/fav.png?v=3\">\n";
        echo  "<link rel=\"apple-touch-icon-precomposed\" href=\"icon\">\n";
        
        $load_jquery = "false";

        // WOTC Specific
        if (strstr($_REQUEST["pg"], "wotc")) {

            echo  "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "wotc/css/masteradminwotc.css?v=1.0\">\n";

            // processing form
            echo  "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
           echo  "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-1.12.4.js\"></script>\n";
           echo "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-ui.min.js\"></script>\n";
            echo "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "wotc/javascript/masteradminwotc.js\"></script>\n";
            echo  "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "wotc/javascript/wotcfunctions.js\"></script>\n";
            echo "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "tiny_mce/tinymce.min.js\"></script>\n";
            echo  "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/irec_Textareas.js\"></script>\n";

            $load_jquery = "true";
        }
        // WOTC Specific
        
        // iRecruit Specific
        if (strstr($_REQUEST["pg"], "irecruit")) {
            echo  "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "css/jquery.simple-dtpicker.css\">\n";
           echo  "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
            echo  "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-1.7.2.min.js\"></script>\n";
            echo  "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "calendar/js/jquery-ui-1.8.4.custom.min.js\"></script>\n";
           echo  "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "jscolor/jscolor.js\"></script>\n";
            echo  "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery.simple-dtpicker.js\"></script>\n";
            
            $load_jquery = "true";
        }
        
        if($load_jquery == "false") {
            // processing form
            echo  "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
            echo  "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-1.12.4.js\"></script>\n";
            echo  "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-ui.min.js\"></script>\n";
        }
        
        // iRecruit Specific
        echo  "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "css/admin.css?v=2.0\">\n";
        echo  "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "css/modal.css?v=2.0\">\n";
        echo  "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "css/custom-styles.css?v=2.0\">\n";
        echo  "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/admin.js\"></script>\n";

        echo  "</head>\n";
        echo  "<body>\n";
        
        echo  "<div id=\"content\">\n";

if (($AUTH['iRecruitAccess'] != "N") || ($AUTH['wotcAccess'] != "N")) { // begin permissions

    if (empty($_REQUEST['pg'])) {
        $_REQUEST['pg'] = "cms";
    }
                                                                            
        // CMS
        if ($_REQUEST['pg'] == "cms") {
            include "cms/links.inc";
        }
    
        // iRecruit
        if ($_REQUEST['pg'] == "master_access") {
            include "irecruit/MasterAccess.inc";
        }
        if ($_REQUEST['pg'] == "remote_system_access") {
            include "irecruit/RemoteSystemAccess.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_orgsetup") {
            include "irecruit/OrganizationSetup.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_features") {
            include "irecruit/OrganizationFeatures.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_vault") {
            include "irecruit/OrganizationVault.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_roles") {
            include "irecruit/RoleAdministration.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_purchases") {
            include "irecruit/Purchases.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_icons") {
            include "irecruit/Icons.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_themes") {
            include "irecruit/Themes.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_usernotifications") {
            include "irecruit/UserNotifications.inc";
        }
        if ($_REQUEST['pg'] == "notification_reminders") {
            include "irecruit/NotificationReminders.inc";
        }
        if ($_REQUEST['pg'] == "manage_notification_reminders") {
            include "irecruit/ManageNotificationReminders.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_branding") {
            include "irecruit/IrecruitBranding.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_up_application_themes") {
            include "irecruit/UserportalApplicationThemes.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_req_highlight_icons") {
            include "irecruit/RequisitionHighlightIcons.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_production_logs") {
            include "irecruit/ProductionLogs.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_ziprecruiter_subscription") {
            include "irecruit/ZipRecruiterSubscription.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_twilio_conversations") {
        	include "irecruit/TwilioConversationReports.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_twilio_accounts") {
        	include "irecruit/TwilioAccounts.inc";
        }
        if ($_REQUEST['pg'] == "irecruit_twilio_usage") {
        	include "irecruit/TwilioUsage.inc";
        }
        if ($_REQUEST['pg'] == "custom_webforms") {
            include "irecruit/custom_webforms.inc";
        }
 	if ($_REQUEST['pg'] == "custom_webforms_create") {
              $_SESSION['WebFormID'] = $_REQUEST['WebFormID'];
              $_SESSION['OrgID'] = $_REQUEST['OrgID'];
            include "irecruit/custom_webforms_create.inc";
        }
	if ($_REQUEST['pg'] == "custom_webforms_edit") {  //echo "sdgfd";die;
              $_SESSION['WebFormID'] = $_REQUEST['WebFormID'];
              $_SESSION['OrgID'] = $_REQUEST['OrgID'];
            include "irecruit/custom_webforms_edit.inc";
        }

        // WOTC
        if ($_REQUEST['pg'] == "wotc_change_applicant") {
            include "wotc/ChangeApplicant.inc";
        }
        if ($_REQUEST['pg'] == "wotc_manage_forms") {
            include "wotc/ManageWotcForms.inc";
        }
        if ($_REQUEST['pg'] == "wotc_edit_form") {
            include "wotc/EditWotcForm.inc";
        }
        if ($_REQUEST['pg'] == "wotc_new_form") {
            include "wotc/NewWotcFormQuestions.inc";
        }
        if ($_REQUEST['pg'] == "create_agreement") {
            include "wotc/createOrgAgreement.inc";
        }
	if ($_REQUEST['pg'] == "poa_agreements") {
            include "wotc/POAagreements.inc";
        }
        if ($_REQUEST['pg'] == "state_portals") {
            include "wotc/StatePortals.inc";
        }
        if ($_REQUEST['pg'] == "wotccrm") {
            include "wotc/WOTC_CRM.inc";
        }
        if ($_REQUEST['pg'] == "wotc_edit_form_que") {
            if(isset($_REQUEST['btnSaveQueTypeHtml']) && $_REQUEST['btnSaveQueTypeHtml'] == "Save") {
                include "wotc/ProcessEditWotcFormQuestion.inc";
            }
            include "wotc/EditWotcFormQuestion.inc";
        }
	if ($_GET['pg'] == "exportshoeshowhours") {
    	    include ADMIN_DIR . 'wotc/wotcShoeShowReport.php';
	}
        if ($_REQUEST['pg'] == "wotc_hours") {
            include "wotc/Hours.inc";
        }
	if ($_GET['pg'] == "wotcrollupreport") {
    	    include ADMIN_DIR . 'wotc/wotcRollUpReport.inc';
	}

    if ($_REQUEST['pg'] == "wotc_step_two") {

        echo '<div style="font-size:12pt;font-weight:bold;margin-bottom:20px;">WOTC Process to State</div>';

        echo "<table>";
        echo "<tr><td>\n";
            echo G::Obj('WOTCAdmin')->lookupFormStepTwo();
	    if (($_REQUEST['process'] == "list") || ($_REQUEST['process'] == "bulk")) {
              $DATA   =   G::Obj('WOTCAdmin')->queryStepTwo();
              echo G::Obj('WOTCAdmin')->listStepTwo($DATA);
	    }
        echo "</td></tr>";
        echo "</table>\n";
        echo "</div>\n";
    }

    // Admin
    if ($_REQUEST['pg'] == "access") {    

        echo $ADMINObj->listUsers();

    }

    if ($_REQUEST['pg'] == "wotc_administration_cms") {
        echo G::Obj('WOTCAdmin')->lookupFormCRM();
	if ($_REQUEST['process'] == "Y") {
            $DATA   =   G::Obj('WOTCAdmin')->queryApplicantsCRM();
            echo G::Obj('WOTCAdmin')->displayApplicants($DATA,'lookupFormCRM');
	}
    }
    
    require_once 'PageWrapperEnd.inc';

    require_once 'Footer.inc'; 

    // Reports
    if ($_REQUEST['pg'] == "reports_overview") {
        include "reports/Overview.inc";
    }
     
    
} // end permissions

echo $ADMINObj->adminFooter();
?>
