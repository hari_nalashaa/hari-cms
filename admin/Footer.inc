    <footer class="footer">
		<div class="container">
		      <?php 
    		      if($brand_info['BrandID'] == "0") {
                     ?><a href="http://www.irecruit-software.com" target="_blank">Powered by iRecruit</a><?php
                  }
                  else {
                     if($brand_org_info['Url'] != "") {
                        ?><a href="<?php echo $brand_org_info['Url'];?>" target="_blank"><?php echo $brand_org_info['FooterText'];?></a><?php
                     }
                     else {
                     	echo $brand_org_info['FooterText'];
                     }
                  }
    		      
                  if(isset($bottomnav) && is_array($bottomnav)) {
                    foreach ($bottomnav as $section => $displaytitle) {
                        echo '<a href="index.php?navpg=' . $section . '">' . " | " . $displaytitle . '</a> ';
                    }	
                  }
		      ?>
		</div>
	</footer>
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo ADMIN_HOME?>js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo ADMIN_HOME?>js/plugins/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo ADMIN_HOME?>js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo ADMIN_HOME?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo ADMIN_HOME?>js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
</body>
</html>