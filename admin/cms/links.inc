<div style="margin-top:20px;"></div>

<div style="float:left;margin-right:40px;width:140px;">
    <div style="font-size:12pt;font-weight:bold;">Dashboards</div>
    <div style="line-height:150%;margin:5px 0 20px 10px;">
	<?php if($_SERVER['SERVER_NAME'] != "irecruit.pairsite.com"){ ?>
        <a href="https://irecruit.pairsite.com/admin/intranet.php" target="_blank">Production</a><br>
	<?php } ?>
	<?php if($_SERVER['SERVER_NAME'] != "quality.irecruit-us.com"){ ?>
        <a href="https://quality.irecruit-us.com/admin/intranet.php" target="_blank">Quality</a><br>
	<?php } ?>
    </div>
    <div style="font-size:12pt;font-weight:bold;">Documents</div>
    <div style="line-height:150%;margin:5px 0 20px 10px;">
	 <a href="<?php echo ADMIN_HOME ?>wotc/documents/WOTC-White_Label.pdf" target="_blank">WOTC White Label</a><br>
	 <a href="<?php echo ADMIN_HOME ?>wotc/documents/WOTC-REST_API.pdf" target="_blank">WOTC REST API</a><br>
	 <a href="<?php echo ADMIN_HOME ?>irecruit/documents/CMS-Infrastructure.pdf" target="_blank">CMS Infrastructure</a>
    </div>
</div>

<div style="float:left;margin-right:40px;width:200px;">
    <div style="font-size:12pt;font-weight:bold;">Application Sites</div>
    <div style="line-height:150%;margin:5px 0 20px 10px;">
        <a href="<?php echo IRECRUIT_HOME; ?>" target="_blank">iRecruit Application</a><br>
        <a href="<?php echo PUBLIC_HOME; ?>index.php?OrgID=B12345467" target="_blank">iRecruit Public Website</a><br>
        <a href="<?php echo USERPORTAL_HOME; ?>login.php?OrgID=B12345467" target="_blank">iRecruit User Portal</a><br>
        <a href="<?php echo WOTC_HOME; ?>admin.php?wotcID=DS062010" target="_blank">WOTC Portal Website</a><br>
        <a href="<?php echo WOTCADMIN_HOME; ?>login.php?wotcID=DS062010" target="_blank">WOTC Customer Admin</a><br>
    </div>
</div>

<div style="float:left;margin-right:50px;width:150px;">
    <div style="font-size:12pt;font-weight:bold;">Marketing Sites</div>
    <div style="line-height:150%;margin:5px 0 20px 10px;">
        <a href="http://www.cmshris.com" target="_blank">cmshris</a><br>
        <a href="http://www.cmswotc.com" target="_blank">cmswotc</a><br>
        <a href="http://www.cmshr.com" target="_blank">cmshr</a><br>
        <a href="http://www.cmsirecruit.com" target="_blank">cmsirecruit</a><br>
        <a href="http://www.staffinglink360.com" target="_blank">staffinglink360</a><br>
        <a href="http://www.irecruit-software.com" target="_blank">irecruit-software</a><br>
        <a href="http://www.irecruitforsagehrms.com" target="_blank">irecruitforsagehrms</a><br>
    </div>
</div>

<div>
    <div style="font-size:12pt;font-weight:bold;">Help Sites</div>
    <div style="line-height:150%;margin:5px  0 20px 10px;">
        <a href="http://help.myirecruit.com" target="_blank">User Portal</a><br>
        <a href="http://help.irecruit-us.com" target="_blank">iRecruit</a><br>
    </div>
</div>
<div style="clear:both;"></div>
<div style="font-size:12pt;font-weight:bold;">Supporting Sites</div>

<div style="line-height:150%;margin:20px 0 20px 10px;">
Email: <a href="https://portal.microsoftonline.com" target="_blank">Microsoft 365 Portal</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://mail.office365.com" target="_blank">Office Email</a>
<br>
Phone: <a href="http://www.8x8.com" target="_blank">8x8</a>
<br>
Utilities: <a href="https://www.dropbox.com/home" target="_blank">Drop Box</a>
<br>
Hosting: <a href="http://www.pair.com" target="_blank">Pair Networks</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://my-db.pair.com/phpMyAdminQS/" target="_blank">DB MyAdminQS</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.godaddy.com/" target="_blank">GoDaddy</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://aws.amazon.com/" target="_blank">AWS</a>
<br>
Monitoring: <a href="https://manager.alertbot.com/" target="_blank">AlertBot</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://www.google.com/analytics/" target="_blank">Google Analytics</a>
<br>
Security: <a href="https://web.analysiscenter.veracode.com/" target="_blank">Veracode</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://rules.sonarsource.com/" target="_blank">Sonar Source</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.sans.org/" target="_blank">Sans Institute</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://www.owasp.org/" target="_blank">OWASP</a>
<br>
Certification: <a href="https://www.fedramp.gov/" target="_blank">FedRAMP</a>
<br>
E-Verify: <a href="https://e-verify.uscis.gov/web/Home.aspx" target="_blank">E-Verify Portal</a>
<br>
Texting: <a href="https://www.twilio.com/" target="_blank">Twilio</a>
<br>
Gateway: <a href="https://merchants.innovativegateway.com/" target="_blank">Innovative Gateway</a>&nbsp;COSTMANAGEMENTSERVICELLC113106
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://www.pcisecuritystandards.org/" target="_blank">PCI Compliance</a>
<br>
Backup: <a href="https://www.iBackup.com/" target="_blank">iBackup</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://evs.ibackup.com/developers-guide.htm" target="_blank">iBackup Guide</a>
<br>
Code Development: <a href="https://bitbucket.org" target="_blank">Bitbucket</a>
&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://irecruit.atlassian.net/projects/PI/queues/custom/25" target="_blank">JIRA</a>
<br>
Help Desk: <a href="https://irecruit.atlassian.net/servicedesk/customer/portal" target="_blank">JIRA Service Desk</a>
</div>
