<?php
include 'Configuration.inc';

if ($_COOKIE['AID']) {
  header('Location: ' . ADMIN_HOME );
  exit;
}

if ($_POST['submit'] == "Please Login") {
    
    $AUTH = $AdminUsersObj->getUserInfoByUserID($_POST['user'], "UserID, Verification");
    
    if (password_verify($_POST['password'], $AUTH['Verification'])) {
        if (password_needs_rehash($_POST['password'], PASSWORD_DEFAULT)) {
          $set_info   = array("Verification = :Verification");
          $where_info = array("UserID = :UserID");
          $params     = array(":Verification" => password_hash( $_POST['password'] , PASSWORD_DEFAULT ), ":UserID" => $AUTH['UserID']);
          $AdminUsersObj->updUsersInfo($set_info, $where_info, array($params));
        }
    } else {
        $AUTH = array();
    }

    // if we have a good user and password match
    if ($AUTH['UserID']) {
        
        //  set a new Session ID
        $SESSIONID  = $GenericLibraryObj->generateRandomSessionKey();
        
        if ($_COOKIE['AID']) $SESSIONID = $_COOKIE['AID'];
        
        if ($_POST['keep'] == "yes") {
          $keeper = ", Persist = 1";
        } else {
          $keeper = '';
        }
        
        // update user
        $set_info   = array("LastAccess = NOW(), SessionID = :SessionID");
        $where_info = array("UserID = :UserID");
        $params     = array(":SessionID"=>$SESSIONID, ":UserID"=>$AUTH['UserID']);
        if ($_POST['keep'] == "yes") $set_info[] = "Persist = 1";
        $AdminUsersObj->updUsersInfo($set_info, $where_info, array($params));
        
        // set session key on users browser
        if ($_POST['keep'] == "yes") {
          setcookie("AID", $SESSIONID, time()+2592000, "/");
        } else {
          setcookie("AID", $SESSIONID, time()+7200, "/");
        }
        
        // go to application
        header('Location: ' . ADMIN_HOME );
        exit;
    
    } // end AUTH

    $ERROR = 'Login Failed';

} // end submit

echo $ADMINObj->adminHeader();
?>
<div style="margin:80px 0 0 0;">
<form method="POST" action="login.php">
<p>Username:
<input name="user" size="23" maxlength="15" type="text"></p>
<p>Password:
<input name="password" size="23" maxlength="15" type="password"></p>
<p><input type="checkbox" name="keep" value="yes">Remember this computer.</p>
<p><input value="Please Login" name="submit" type="submit">&nbsp;&nbsp;<?php echo $ERROR?></p>
</form>
<p><a href="javascript:ReverseDisplay('emailcheck')">forgot your username or password?</a></p>
</div>
<div id="emailcheck" style="display:none;margin:0 0 0 0;">
<p>Please enter either the email address or user name associated to this account.<br>We will send an email to the email address on record with a link to update your password.</p>
<form method="POST" action="login.php">
<?php if (!$_POST['type']) { $_POST['type']='email'; } ?>
<p><input type="radio" name="type" value="username"<?php if ($_POST['type'] == "username") { echo ' checked'; } ?>>Username
<input type="radio" name="type" value="email"<?php if ($_POST['type'] == "email") { echo ' checked'; } ?>>Email Address
<input type="text" name="lookup" size="30">
<input value="Lookup" name="submit" type="submit"></p>
</form>
</div>
<div style="margin:0 0 0 100px;">
<?php

if ($_POST['submit'] == "Lookup") {

  if ($_POST['type'] == "username") { 
    $AUTH = G::Obj('AdminUsers')->getUserInfoByEmail($_POST['lookup'], "Email, UserID");
  } else { 
    $AUTH = G::Obj('AdminUsers')->getUserInfoByEmail($_POST['lookup'], "Email, UserID");
  }

  if ($AUTH['Email']) {

    $tempsessionid  = $GenericLibraryObj->generateRandomSessionKey();

    // update user
    $set_info   = array("SessionID = :SessionID");
    $where_info = array("UserID = :UserID", "Email = :Email");
    $params     = array(":SessionID"=>$tempsessionid, ":UserID"=>$AUTH['UserID'], ":Email"=>$AUTH['Email']);
    $AdminUsersObj->updUsersInfo($set_info, $where_info, array($params));

    $to = $AUTH['Email'];
    $subject = "CMS Administration Reminder";
    $message = "\nYour user name is: " . $AUTH['UserID'] . "\n\n";
    $message .= "Please reset your password here: " . ADMIN_HOME . "reset.php";
    $message .= "?s=" . $tempsessionid;

    $PHPMailerObj->clearCustomProperties();

    // Set who the message is to be sent to
    $PHPMailerObj->addAddress ( $to );
    // Set the subject line
    $PHPMailerObj->Subject = $subject;
    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );
    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/plain';
    //Send email
    $PHPMailerObj->send ();

    echo "<p>Recovery email has been sent to your account.</p>";

  } else {

    if ($_POST['type'] == "username") {
      echo "<p>Information for that user name was not found.</p>";
    } else if ($_POST['type'] == "email") {
      echo "<p>Information for that email address was not found.</p>";
    }
  }

}

echo "</div>\n";

echo $ADMINObj->adminFooter();
?>