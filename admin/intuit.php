<?php
include 'Configuration.inc';

use QuickBooksOnline\API\DataService\DataService;
$dataservice_config = array(
    'auth_mode'     =>  'oauth2',
    'ClientID'      =>  $IntuitObj->client_id,
    'ClientSecret'  =>  $IntuitObj->client_secret,
    'RedirectURI'   =>  $IntuitObj->redirect_url,
    'scope'         =>  "com.intuit.quickbooks.payment",
    'baseUrl'       =>  "Development"
);
$dataService = DataService::Configure($dataservice_config);

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

if($authUrl != "") {
	header("Location: ".$authUrl);
	exit();
}
?>
