<?php
include '../Configuration.inc';

$forms_info	= $_REQUEST['forms_info'];

for($f = 0; $f < count($forms_info); $f++) {
	$WotcFormID 	=	$forms_info[$f]['WotcFormID'];
	$QuestionID 	=	$forms_info[$f]['QuestionID'];
	$SortOrder		=	$forms_info[$f]['SortOrder'];
	
	$set_info		=	array("QuestionOrder = :QuestionOrder");
	$where_info		=	array("WotcFormID = :WotcFormID", "QuestionID = :QuestionID");
	$params_info	=	array(":WotcFormID"=>$WotcFormID, ":QuestionID"=>$QuestionID,  ":QuestionOrder"=>$SortOrder);
	G::Obj('WOTCFormQuestions')->updWotcFormQuestionsInfo($set_info, $where_info, array($params_info));
}


echo "Successfully Updated.";
?>