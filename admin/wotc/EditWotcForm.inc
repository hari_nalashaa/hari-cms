<h3>Edit Wotc Form</h3>
<div id="wotc_form_questions_info" style="color:blue"></div>
<div>
<?php
$where_info     =   array("WotcID = :WotcID", "WotcFormID = :WotcFormID");
$params_info    =   array(":WotcID"=>$_GET['WotcID'], ":WotcFormID"=>$_GET['WotcFormID']);
$wotc_form_ques =   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsInfo("*", $where_info, "", "QuestionOrder ASC", array($params_info));
$form_ques      =   $wotc_form_ques['results'];
?>
<table border="0" cellpadding="3" cellspacing="3" width="100%" id="wotc_questions">
	<thead>
		<tr>
	        <td><strong>Wotc ID</strong></td>
	        <td><strong>Form ID</strong></td>
	        <td><strong>Question</strong></td>
	        <td width="5%"><strong>Action</strong></td>
	    </tr>
	</thead>
	<tbody>
    <?php
    for($wfq = 0; $wfq < count($form_ques); $wfq++) {
        $WotcID         =   $form_ques[$wfq]['WotcID'];
        $WotcFormID     =   $form_ques[$wfq]['WotcFormID'];
        $QuestionID     =   $form_ques[$wfq]['QuestionID'];
        $QuestionTypeID =   $form_ques[$wfq]['QuestionTypeID'];
        ?>
        <tr id="<?php echo $form_ques[$wfq]['WotcFormID'] . "*" . $QuestionID;?>">
            <td><?php echo $form_ques[$wfq]['WotcID'];?></td>
            <td><?php echo $form_ques[$wfq]['WotcFormID'];?></td>
            <td><?php echo $form_ques[$wfq]['Question'];?></td>
            <td>
                <a href="<?php echo ADMIN_HOME."index.php?pg=wotc_edit_form_que&WotcID=".$WotcID."&WotcFormID=".$WotcFormID."&QuestionID=".$QuestionID;?>">
                    <img src="<?php echo IRECRUIT_HOME . "images/icons/pencil.png";?>" alt="Edit">&nbsp;Edit&nbsp;
                </a>    
            </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
</div>

<script type="text/javascript">
$(document).ready(function() {
	var forms_data = {
    	forms_info: []
    };

	$( "#wotc_questions tbody" ).sortable({
    		cursor: 'move',
    		placeholder: 'ui-state-highlight',
    	    stop: function( event, ui ) {
    	        
    	    	forms_data = {
					forms_info: []
    	    	};
    	    	
    	    	$(this).find('tr').each(function(i) {

    	        	var SortOrder	=	i;
    	            SortOrder++;
    	            var tr_id		=	$(this).attr('id');
    				var tr_info		=	tr_id.split("*");
    	           
    	            var WotcFormID	=	tr_info[0];
    	            var QuestionID	=	tr_info[1];
    	        	
    				forms_data.forms_info.push({
    	           		 "WotcFormID"	: WotcFormID,
    	           		 "QuestionID"  	: QuestionID,
    	           		 "SortOrder" 	: SortOrder
    	            });
    	        });
    	        
    	    	updateWotcQuestionsSortOrder(forms_data);
    	    }
    });
});
</script>