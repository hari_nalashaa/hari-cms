#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/..') . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 300);

$USER = G::Obj('ADMIN')->getUserInformation($argv[1]);

if ($USER['Email'] != "") {

$ASSIGNMENT = G::Obj('WOTCcrm')->getAssignment();
$COMPANIES = G::Obj('WOTCcrm')->getCompanies();

$ALPHA = range('A', 'Z');
$ALPHA[]="AA";
$ALPHA[]="AB";
$ALPHA[]="AC";
$ALPHA[]="AD";
$ALPHA[]="AE";
$ALPHA[]="AF";
$ALPHA[]="AG";
$ALPHA[]="AH";
$ALPHA[]="AI";
$ALPHA[]="AJ";
$ALPHA[]="AK";
$ALPHA[]="AL";
$ALPHA[]="AM";
$ALPHA[]="AN";
$ALPHA[]="AO";
$ALPHA[]="AP";
$ALPHA[]="AQ";
$ALPHA[]="AR";
$ALPHA[]="AS";
$ALPHA[]="AT";
$ALPHA[]="AU";
$ALPHA[]="AV";
$ALPHA[]="AW";
$ALPHA[]="AX";
$ALPHA[]="AY";
$ALPHA[]="AZ";
$ALPHA[]="BA";
$ALPHA[]="BB";
$ALPHA[]="BC";
$ALPHA[]="BD";
$ALPHA[]="BE";
$ALPHA[]="BF";
$ALPHA[]="BG";
$ALPHA[]="BH";


// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("iRecruit WOTC")
       ->setLastModifiedBy("System")
       ->setTitle("CRM Export")
       ->setSubject("Excel")
       ->setDescription("Export File")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

foreach ($ASSIGNMENT AS $sheet=>$name) {
    
    $results = G::Obj('WOTCcrm')->getCRM('*', array(), '', 'CompanyName', array(array()));
    $CRMDATA = $results['results'];
    $CRMKEYS=array_keys($CRMDATA[0]);

    $flipped = array_flip($CRMKEYS);
    unset($flipped['Assignment']);
    $CRMKEYS=array_flip($flipped);

    $CRMKEYS[]="WOTCIDs";
    $CRMKEYS[]="Users";
    
    $CRMKEYSSTATUS[]="FEIN";
    $CRMKEYSSTATUS[]="AKA/DBA";
    $CRMKEYSSTATUS[]="POA State";
    $CRMKEYSSTATUS[]="POA Expiration Date";
    $CRMKEYSSTATUS[]="First Screen Date";
    $CRMKEYSSTATUS[]="Last Screen Date";
    $CRMKEYSSTATUS[]="Savings";
    $CRMKEYSSTATUS[]="CMS";
    $CRMKEYSSTATUS[]="BP Value";
    $CRMKEYSSTATUS[]="WEB";
    $CRMKEYSSTATUS[]="CALL CENTER";
    $CRMKEYSSTATUS[]="PAPER";
    
    if (($name != "Business Partners") && ($name != "BP Clients")) {

        $flipped = array_flip($CRMKEYS);
        unset($flipped['BP Value']);
        unset($flipped['BPRate']);
        $CRMKEYS=array_flip($flipped);
        $flipped = array_flip($CRMKEYSSTATUS);
        unset($flipped['BP Value']);
        $CRMKEYSSTATUS=array_flip($flipped);
        
        
    }
    if ($name == "Business Partners") {
        $flipped = array_flip($CRMKEYS);
        unset($flipped['Partner']);
        unset($flipped['WOTCIDs']);
        unset($flipped['Users']);
        $CRMKEYS=array_flip($flipped);
        $CRMKEYS[]="Clients";

        $CRMKEYSSTATUS = array();
    }

    $CRMKEYS = array_values($CRMKEYS);
    $CRMKEYSSTATUS = array_values($CRMKEYSSTATUS);

    $CRMKEYS=array_merge($CRMKEYS,$CRMKEYSSTATUS);
    
  // Create a new worksheet, after the default sheet
  if ($sheet > 0) { $objPHPExcel->createSheet($sheet); }

  // Add some data to the second sheet, resembling some different data types
  $objPHPExcel->setActiveSheetIndex($sheet);

  // Header Row One
  $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getFill()->getStartColor()->setRGB('0c00ac');
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getAlignment()->setHorizontal('center');
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getAlignment()->setVertical('center');


  foreach ($CRMKEYS AS $k=>$v) {

        $objPHPExcel->getActiveSheet()->getCell($ALPHA[$k].'1')->setValue($v);

  } // end foreach CRMKEYS

  for ($i = 'A'; $i != $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
  }
  $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);

  $where=array('Assignment = :Assignment');
  $params=array(":Assignment"=>$name);
  $results = G::Obj('WOTCcrm')->getCRM('*', $where, '', 'CompanyName', array($params));

  $n=2;
  $C=array();
  foreach ($results['results'] AS $C) {  // Assigned Data
      

	  foreach ($CRMKEYS AS $key=>$value) { // key

	      $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setVertical('top');

		  $V="";
		  if ($value == "Contacts") {

		    if ($C[$value] != "") {

		      $CONTACTS=json_decode($C[$value]);

	 	      for ($x=1; $x<=3; $x++) {
			       $Contact='Contact' . $x;
			       $Role='Role' . $x;
			       $Phone='Phone' . $x;
			       $Email='Email' . $x;
                          if ($CONTACTS->$Contact != "") {
                                  $V .= $CONTACTS->$Contact;
                                  $V .= " - ";
                          }
                          if ($CONTACTS->$Role != "") {
				  $V .= $CONTACTS->$Role;
                                  $V .= " - ";
			  }
                          if ($CONTACTS->$Phone != "") {
				  $V .= $CONTACTS->$Phone;
                                  $V .= " - ";
			  }
                          if ($CONTACTS->$Email != "") {
				  $V .= $CONTACTS->$Email;
			  }
                          $V .= "\r";


		      } // end for loop
                    } // end if value

		    $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } else if ($value == "ParentConfig") {

		    if ($C[$value] != "") {
		      $PC=json_decode($C[$value],true);
		      $V="";
		      if ($PC['DisplayCode'] != "") {
		        $V="Display Code: " . $PC['DisplayCode'] . "\r";
		      }
		      if ($PC['EmploymentConfirmation'] != "") {
		        $V.="Employment Confirmation: " . $PC['EmploymentConfirmation'] . "\r";
		      }
		      if ($PC['ParentLinkShortcode'] != "") {
		        $V.="Link Shortcode: " . $PC['ParentLinkShortcode'] . "\r";
		      }
		    }
		    $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } else if ($value == "Users") {

			$where      =   array("CompanyID = :CompanyID");
			$params     =   array(":CompanyID"=>$C['CompanyID']);
			$USERS_RES  =   G::Obj('WOTCUsers')->getUsersInfo("UserID", $where, 'UserID', array($params));

			$V="";
                        foreach ($USERS_RES['results'] AS $U) {
				$V.=$U['UserID'] . "\r";
                        }

	     		$objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } else if ($value == "WOTCIDs") {

			$WOTCIDS = G::Obj('WOTCcrm')->getWOTCIDs($C['CompanyID']);
			asort($WOTCIDS);
                        foreach ($WOTCIDS AS $ID) {
				$V.=$ID . "\r";
			}
	     		$objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } else if ($value == "Partner") {

	     		$V=$COMPANIES[$C[$value]];

		  } else if ($value == "Clients") {

			$V="";
                        $columns = 'CompanyName';
                        $where = array("Partner = :Partner","Active = 'Y'");
                        $params = array(":Partner"=>$C['CompanyID']);
                        $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
                        $DATA = $RESULTS['results'];
                        foreach($DATA AS $CL) {
                          $V.=$CL['CompanyName'] . "\r";
                        } // end foreach

                        $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } else if ($value == "APIKey") {

			  $V="";
			  if ($C[$value] != "") {
			    $V='Ending: ...' . substr($C[$value],-8);
			  } 

		  } else if ($value == "CompanyPhone") {
		    $V="";
		    if ($C[$value] != '') {
			    $CP = json_decode($C[$value], true);
			    if ($CP[0] != "") {
			      $V=implode("-",$CP);
			    }
		    }
		  } else {
		      
	     		$V=$C[$value];
	     		
		  }

		  if (in_array($value, $CRMKEYSSTATUS)) {
		      $V="";
		      $xx=1;
      		      $STATUSES = G::Obj('WOTCcrm')->getStatuses($C['CompanyID']);
		      foreach ($STATUSES AS $S) {
		          if ($value == "Savings") {
		              $savings=preg_replace("/[^0-9\.]/", '', $S[$value]);
		              $V.=number_format($savings, 2, '.', ',');
		              $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getNumberFormat()->setFormatCode("#,##0.00");
		              
		          } else if ($value == "CMS") {
		              $cms = (preg_replace("/[^0-9\.]/", '', $S['Savings']) * ($C['Rate']/100));
		              $V.=number_format($cms, 2, '.', ',');
		              $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getNumberFormat()->setFormatCode("#,##0.00");
		              
		          } else if ($value == "BP Value") {
	                  $bpvalue = ((preg_replace("/[^0-9\.]/", '', $S['Savings']) * ($C['Rate']/100)) * ($C['BPRate']/100));
	                  $V.=number_format($bpvalue, 2, '.', ',');
	                  $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getNumberFormat()->setFormatCode("#,##0.00");
		          } else {
		            $V.=$S[$value];
		          }
		          
		          if ($xx < count($STATUSES)) {
		              $V.="\r";
		          }

              	      $xx++;
		      }
		      
		      $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } // end if in CRMKEYSTATUS
		 

        	$objPHPExcel->getActiveSheet()->getCell($ALPHA[$key].$n)->setValue($V);

	} // end foreach Key	  

  $n++;
  } // end foreach Assigned Data

  // Rename sheet
  $objPHPExcel->getActiveSheet()->setTitle($name);

} // end foreach ASSIGNMENT

$objPHPExcel->setActiveSheetIndex(0);

    $file = "CRM-Export.xlsx";
    $tempfile = ROOT . "cron/temp/" . $file;
    $objWriter= IOFactory::createWriter($objPHPExcel , 'Xlsx');
    $objWriter->save($tempfile);

    $message .= "Here is your CRM export file.<br><br>";
    $subject = 'CRM Export';

    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();

    // Set who the message is to be sent to
    $PHPMailerObj->addAddress( $USER['Email'], $USER['UserID']);

    // Set an alternative reply-to address
    $PHPMailerObj->setFrom ( 'cmswotc@cmswotc.com', 'WOTC' );

    // Set an alternative reply-to address
    $PHPMailerObj->addReplyTo ( 'cmswotc@cmswotc.com', 'WOTC' );

    // Set the subject line
    $PHPMailerObj->Subject = $subject;

    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/html';

    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );

    $data = file_get_contents($tempfile);
    $PHPMailerObj->addStringAttachment($data, $file, 'base64', 'text/html');

    //Send email
    $PHPMailerObj->send ();

    unlink($tempfile);

} // end email


?>
