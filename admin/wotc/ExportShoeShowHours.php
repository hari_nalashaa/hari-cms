#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/..') . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$USER = G::Obj('ADMIN')->getUserInformation($argv[1]);

if ($USER['Email'] != "") {

G::Obj('GenericQueries')->conn_string =   "WOTC";

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Results from query")
       ->setKeywords("phpExcel")
       ->setCategory("Output");


// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('wotcID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Entry Date');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('First Name');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Last Name');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('SSN');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Confirmed');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Category');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Hired Date');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Termed');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('Max Hours');
$objPHPExcel->getActiveSheet()->getCell('L1')->setValue('Max Wages');
$objPHPExcel->getActiveSheet()->getCell('M1')->setValue('Prev Max Hours');
$objPHPExcel->getActiveSheet()->getCell('N1')->setValue('Prev Max Wages');
$objPHPExcel->getActiveSheet()->getCell('O1')->setValue('Total Max Hours');
$objPHPExcel->getActiveSheet()->getCell('P1')->setValue('Total Max Wages');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


$query = "SELECT concat(AD.Social1,'-',AD.Social2,'-',AD.Social3) AS SSN,
 OD.wotcID, AD.ApplicationID,
 DATE_FORMAT(AD.EntryDate,'%m/%d/%Y %I:%i %p') AS EntryDate, AD.FirstName, AD.LastName, DATE_FORMAT(P.Confirmed,'%m/%d/%Y') AS Confirmed, P.Category
 FROM ApplicantData AD
 JOIN OrgData OD ON OD.wotcID = AD.wotcID
 LEFT JOIN Processing P ON AD.wotcID = P.wotcID AND AD.ApplicationID = P.ApplicationID
 LEFT JOIN Billing B ON B.wotcID = AD.wotcID AND B.ApplicationID = AD.ApplicationID
 WHERE OD.CompanyID = '62f41708602251'
 AND AD.Qualifies = 'Y' AND P.Confirmed != '0000-00-00' AND P.Qualified = 'Y' AND B.ApplicationID IS NULL";
//$query .= " limit 4";

 $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

$n=1;
foreach ($RESULTS as $OD) {
$n++;

$SSN=preg_replace('/[^0-9]/','',$OD['SSN']);
$params     =   array(':SSN' =>  $SSN);

$query = "select DATE_FORMAT(SS.HireDate,'%m/%d/%Y') AS HireDate, DATE_FORMAT(SS.TerminationDate,'%m/%d/%Y') AS TerminationDate from ShoeShow SS WHERE SS.SSN = :SSN";
$DATA1 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS MaxHours, max(SSH.YTDWages) AS MaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = YEAR(CURDATE())";
$DATA2 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 1 YEAR))";
$DATA3 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 2 YEAR))";
$DATA4 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 3 YEAR))";
$DATA5 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 4 YEAR))";
$DATA6 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 5 YEAR))";
$DATA7 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 6 YEAR))";
$DATA8 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 7 YEAR))";
$DATA9 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "select max(SSH.YTDHours) AS PrevMaxHours, max(SSH.YTDWages) AS PrevMaxWages from ShoeShowHours SSH WHERE SSH.SSN = :SSN AND YEAR(SSH.PayPeriod) = (YEAR(CURDATE() - INTERVAL 8 YEAR))";
$DATA10 =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

 $MaxHours = $DATA2[0]['MaxHours'] != "" ? $DATA2[0]['MaxHours'] : 0;
 $MaxWages = $DATA2[0]['MaxWages'] != "" ? $DATA2[0]['MaxWages'] : 0;

 $PrevMaxHours = $DATA3[0]['PrevMaxHours'] != "" ? $DATA3[0]['PrevMaxHours'] : 0;
 $PrevMaxWages = $DATA3[0]['PrevMaxWages'] != "" ? $DATA3[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA4[0]['PrevMaxHours'] != "" ? $DATA4[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA4[0]['PrevMaxWages'] != "" ? $DATA4[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA5[0]['PrevMaxHours'] != "" ? $DATA5[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA5[0]['PrevMaxWages'] != "" ? $DATA5[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA6[0]['PrevMaxHours'] != "" ? $DATA6[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA6[0]['PrevMaxWages'] != "" ? $DATA6[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA7[0]['PrevMaxHours'] != "" ? $DATA7[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA7[0]['PrevMaxWages'] != "" ? $DATA7[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA8[0]['PrevMaxHours'] != "" ? $DATA8[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA8[0]['PrevMaxWages'] != "" ? $DATA8[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA9[0]['PrevMaxHours'] != "" ? $DATA9[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA9[0]['PrevMaxWages'] != "" ? $DATA9[0]['PrevMaxWages'] : 0;

 $PrevMaxHours += $DATA10[0]['PrevMaxHours'] != "" ? $DATA10[0]['PrevMaxHours'] : 0;
 $PrevMaxWages += $DATA10[0]['PrevMaxWages'] != "" ? $DATA10[0]['PrevMaxWages'] : 0;

 $TotalHours = $MaxHours + $PrevMaxHours;
 $TotalWages = $MaxWages + $PrevMaxWages;

 $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($OD['wotcID']);
 $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($OD['ApplicationID']);
 $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($OD['EntryDate']);
 $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($OD['FirstName']);
 $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($OD['LastName']);
 $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($OD['SSN']);
 $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($OD['Confirmed']);
 $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($OD['Category']);

 $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($DATA1[0]['HireDate']);
 $objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue(($DATA1[0]['TerminationDate'] == "00/00/0000" ? '' : $DATA1[0]['TerminationDate']));

 $objPHPExcel->getActiveSheet()->getCell('K'.$n)->setValue($MaxHours);
 $objPHPExcel->getActiveSheet()->getCell('L'.$n)->setValue($MaxWages);

 $objPHPExcel->getActiveSheet()->getCell('M'.$n)->setValue($PrevMaxHours);
 $objPHPExcel->getActiveSheet()->getCell('N'.$n)->setValue($PrevMaxWages);

 $objPHPExcel->getActiveSheet()->getCell('O'.$n)->setValue($TotalHours);
 $objPHPExcel->getActiveSheet()->getCell('P'.$n)->setValue($TotalWages);

} // end foreach

// Rename sheet
$title="Wages-Hours";
$objPHPExcel->getActiveSheet()->setTitle($title);

    $file = "ShoeShow_WOTC_Hours.xlsx";
    $tempfile = ROOT . "cron/temp/" . $file;
    $objWriter= IOFactory::createWriter($objPHPExcel , 'Xlsx');
    $objWriter->save($tempfile);

    $message = "Here is your Shoe Show Hours report.";
    $subject = 'Shoe Show Hours Report';

    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();

    // Set who the message is to be sent to
    $PHPMailerObj->addAddress( $USER['Email'], $USER['UserID']);
    //$PHPMailerObj->addBCC( 'dedgecomb@irecruit-software.com', 'David Edgecomb');

    // Set an alternative reply-to address
    $PHPMailerObj->setFrom ( 'cmswotc@cmswotc.com', 'WOTC' );

    // Set an alternative reply-to address
    $PHPMailerObj->addReplyTo ( 'cmswotc@cmswotc.com', 'WOTC' );

    // Set the subject line
    $PHPMailerObj->Subject = $subject;

    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/html';

    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );

    $data = file_get_contents($tempfile);
    $PHPMailerObj->addStringAttachment($data, $file, 'base64', 'text/html');

    //Send email
    $PHPMailerObj->send ();

    unlink($tempfile);

} // end email
?>
