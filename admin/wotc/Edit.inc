<?php
if (($_POST['ApplicationID']) && ($_POST['wotcID'])) {

if ($_POST['process'] == "P") {

  $set_info[]			=   "FirstName = :FirstName";
  $set_info[]			=   "MiddleName = :MiddleName";
  $set_info[]			=   "LastName = :LastName";
  $set_info[]			=   "Social1 = :Social1";
  $set_info[]			=   "Social2 = :Social2";
  $set_info[]			=   "Social3 = :Social3";
  $set_info[]			=   "Address = :Address";
  $set_info[]			=   "City= :City";
  $set_info[]			=   "State = :State";
  $set_info[]			=   "WorkState = :WorkState";
  $set_info[]			=   "ZipCode = :ZipCode";
  $set_info[]			=   "County = :County";
  $where_info[]	 		=   "wotcID = :wotcID";
  $where_info[]			=   "ApplicationID = :ApplicationID";
  $params[':FirstName']     	=   $_POST['FirstName'];
  $params[':MiddleName']     	=   $_POST['MiddleName'];
  $params[':LastName']     	=   $_POST['LastName'];
  $params[':Social1']     	=   $_POST['Social1'];
  $params[':Social2']     	=   $_POST['Social2'];
  $params[':Social3']     	=   $_POST['Social3'];
  $params[':Address']     	=   $_POST['Address'];
  $params[':City']	     	=   $_POST['City'];
  $params[':State']	     	=   $_POST['State'];
  $params[':WorkState']	     	=   $_POST['WorkState'];
  $params[':ZipCode']	     	=   $_POST['ZipCode'];
  $params[':County']	     	=   $_POST['County'];
  $params[':wotcID']     	=   $_POST['wotcID'];
  $params[':ApplicationID']     =   $_POST['ApplicationID'];

  if (($_POST['wotcID'] != "") && ($_POST['ApplicationID'] != "")
	&& ($_POST['FirstName'] != "") && ($_POST['LastName'] != "")
	&& ($_POST['Social1'] != "") && ($_POST['Social2'] != "") && ($_POST['Social3'] != "")
	&& ($_POST['Address'] != "") && ($_POST['City'] != "") && ($_POST['State'] != "")
	&& ($_POST['WorkState'] != "") && ($_POST['ZipCode'] != "")) {
    G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
    $msg = "<span style=\"color:red;font-style:italic;\">Applicant Information Updated</span>";
  }

} // end process

$PARENT = G::Obj('WOTCcrm')->getParentInfo($_POST['wotcID']);
$WotcFormID=$PARENT['WotcFormID'];
$AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($_POST['wotcID']);
$CompanyName = $AKADBA['AKADBA'];

$AD         =   G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $_POST['wotcID'], $WotcFormID, $_POST['ApplicationID']);

if (count($AD) > 0) {
?>
<strong style="font-size:14pt;">Edit Applicant</strong><br>

<div style="margin: 20px 0;">
<?php echo $AD['wotcID']; ?> | <?php echo $CompanyName; ?><br>
ApplicationID: <?php echo $AD['ApplicationID']; ?> | 
Entered: <?php echo date("m/d/Y H:i:s",strtotime($AD['EntryDate'])); ?><br>
</div>

<div style="margin: 20px 0;">
<?php echo $msg; ?>
</div>

<?php
$where_info                 =   array("WotcID = :WotcID", "WotcFormID = :WotcFormID", "Active = :Active", "QuestionOrder <= 11");
$params_info                =   array(":WotcID"=>"MASTER", ":WotcFormID"=>$WotcFormID, ":Active"=>'Y');
$wotc_form_ques_list        =   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsInfo("*", $where_info, "", "QuestionOrder ASC", array($params_info));
$wotc_form_ques             =   $wotc_form_ques_list['results'];

//Set APPDATA array in ApplicationForm instance
G::Obj('ApplicationForm')->APPDATA =    $AD;

for($wfq = 0; $wfq < $wotc_form_ques_list['count']; $wfq++) {

    if($ques_info['QuestionTypeID'] == 17) {
        $DATE_IDS       .=  "#".$ques_info['QuestionID'].", ";
    }

    $QuestionID         =   $wotc_form_ques[$wfq]['QuestionID'];
    $QuestionTypeID     =   $wotc_form_ques[$wfq]['QuestionTypeID'];
    $values         	=   explode ( "::", $wotc_form_ques[$wfq]['value']);

    $ques_info          =   $wotc_form_ques[$wfq];
    $ques_info['name']  =   $wotc_form_ques[$wfq]['QuestionID'];
    $que_types_list[$wotc_form_ques[$wfq]['QuestionID']]   =   $wotc_form_ques[$wfq]['QuestionTypeID'];

    if($wotc_form_ques[$wfq]['ChildQuestionsInfo'] != "") {
        $child_que_list[$wotc_form_ques[$wfq]['QuestionID']]   =   json_decode($wotc_form_ques[$wfq]['ChildQuestionsInfo'], true);
    }

    //Set errors related css
    $requiredck =   '';
    $highlight  =   'yellow';

    if(in_array($QuestionID, $ERROR_KEYS)) {
        $requiredck = ' style="background-color:' . $highlight . ';"';
    }

    $lbl_class = ' class="question_name"';
    if($QuestionTypeID == 99 || $QuestionTypeID == 5) {
        $lbl_class = " class='question_name labels_auto'";
    }

    $ques_info ['lbl_class']  =   $lbl_class;
    $ques_info ['requiredck'] =   $requiredck;


    if ($QuestionID == "State") {

    	    $que_view = '<div class="form-group row" id="divQue-State"><div class="col-lg-3 col-md-3 col-sm-3"><label for="State" class="question_name">State:&nbsp;<font style="color:red">*</font>&nbsp;</label></div><div class="col-lg-9 col-md-9 col-sm-9"><select name="State" id="State" onchange="getCounties(this.options[this.selectedIndex].value,\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\')" class="form-control width-auto-inline"><option value="">Select</option>';
	        foreach ( $values as $v => $q ) {
                list ( $vv, $qq ) = explode ( ":", $q, 2 );
                $vv = trim ( $vv );

                $sel = '';
                if (isset ( $AD['State'] ) && $AD['State'] == $vv) {
                        $sel = ' selected';
                }

                $que_view .= "<option value='$vv' $sel>" . $qq . "</option>";

        	}

	    $que_view .= '</select></div>';

    } else if ($QuestionID == "County") {

	    $que_view = '<div class="form-group row" id="divQue-County"><div class="col-lg-3 col-md-3 col-sm-3"><label for="County" class="question_name">County:&nbsp;</label></div><div class="col-lg-9 col-md-9 col-sm-9"><select name="County" id="County" class="form-control width-auto-inline" style="display: block;">';

	    $que_view .= G::Obj('WOTCCounties')->getCounties($AD['State'],$AD['wotcID'],$AD['ApplicationID']);

	    $que_view .= '</select></div>';

    } else {

    	//Print the Question Preview
    	$que_view = call_user_func(array(G::Obj('ApplicationForm'), 'getQuestionView' . $ques_info['QuestionTypeID']), $ques_info);

    }

    if(in_array($QuestionID, $special_ques)) {
        if ($_POST['Initiated'] == "callcenter") {
            if($QuestionID == "verify") {
                echo $que_view;
            }
        }
        else {
                if($QuestionID != "verify") {
                echo str_replace("{Organization}", $CompanyName, $que_view);
                }
        }
    }
    else {
        echo $que_view;
    }

}

	echo '<div style="margin-top:5px;" class="col-lg-3 col-med-3 col-sm-3">Work State:</div>';
	echo '<div style="margin-top:5px;" class="col-lg-3 col-med-3 col-sm-3"><select name="WorkState">';
        echo '<option value="">Please Select</option>';

            // Get Address States List
            $results    =   G::Obj('Address')->getAddressStateList ();
            $STATES     =   $results['results'];

	    if($AD['WorkState'] == "") { $AD['WorkState'] = $AD['State']; }
	    echo "<option value=\"VC\"";
            if ($AD['WorkState'] == "VC") echo " selected";
	    echo ">Veteran Paperwork</option>\n";
            foreach ($STATES as $ST) {
                echo "<option value=\"" . $ST['Abbr'] . "\"";
                if ($AD['WorkState'] == $ST['Abbr']) echo " selected";
                echo ">" . $ST['Description'] . "</option>\n";
            } // end foreach
        echo '</select>';
	echo '</div>';

?>
<div style="clear:both;" class="col-lg-3 col-med-3 col-sm-3">&nbsp;</div>
<div style="padding-top:20px;" class="col-lg-3 col-med-3 col-sm-3">
<input type="hidden" name="wotcID" value="<?php echo $_POST['wotcID']; ?>">
<input type="hidden" name="ApplicationID" value="<?php echo $_POST['ApplicationID']; ?>">
<button onclick="loadTrackBillForm('<?php echo $_POST['type']; ?>','<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>','P','<?php echo $_POST['editrow']; ?>');return false;">Edit Applicant</button>
</div>
<?php
} // end if

} // end count
?>
