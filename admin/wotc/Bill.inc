<?php
if (($_POST['ApplicationID']) && ($_POST['wotcID'])) {

// get WOTC OrgData
$columns                =   "WotcFormID";
$where                  =   array("wotcID = :wotcid");
$params                 =   array($_POST['wotcID']);
$ORGS_RES               =   G::Obj('WOTCOrganization')->getOrgDataInfo($columns, $where, '', '', array($params));
$ORGS                   =   $ORGS_RES['results'];

$AD         =   G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $_POST['wotcID'], $ORGS[0]['WotcFormID'], $_POST['ApplicationID']);

$columns    =   "IF(StartDate = '0000-00-00','',date_format(StartDate,'%m/%d/%Y')) StartDate, IF(Received = '0000-00-00','',date_format(Received,'%m/%d/%Y')) Received, ";
$columns    .=  "IF(Filed = '0000-00-00','',date_format(Filed,'%m/%d/%Y')) Filed, IF(Confirmed= '0000-00-00','',date_format(Confirmed,'%m/%d/%Y')) Confirmed, ";
$columns    .=  "Qualified, Category, Comments";
$PROCESSING =   G::Obj('WOTCProcessing')->getProcessingInfo($columns, $_POST['wotcID'], $_POST['ApplicationID']);

$year       =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);

$year++;
if ($_POST['editrow'] > 0) {
    $year   =   $_POST['editrow'];
}

$columns    =   "SUM(Hours) Hours, SUM(Wages) Wages, MAX(ASOF) ASOF, GROUP_CONCAT(Comments) Comments";
$where      =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID", "Year = :Year");
$params     =   array(":wotcID"=>$_POST['wotcID'], ":ApplicationID"=>$_POST['ApplicationID'], ":Year"=>$year);
$TTHRS_RES  =   G::Obj('WOTCHours')->getHoursInfo($columns, $where, 'wotcID, ApplicationID', '', array($params));
$TTHRS      =   $TTHRS_RES['results'][0];

if ($_POST['editrow'] > 0) {
    
    $columns    =   "IF(Termed= '0000-00-00', '', date_format(Termed,'%m/%d/%Y')) Termed, Reason, TaxCredit, IF(DateBilled= '0000-00-00','',date_format(DateBilled,'%m/%d/%Y')) DateBilled,";
    $columns   .=   "IF(DatePaid= '0000-00-00','',date_format(DatePaid,'%m/%d/%Y')) Comments";
    $BILL       =   G::Obj('WOTCBilling')->getBillingInfoByPrimaryKey($columns, $_POST['wotcID'], $_POST['ApplicationID'], $_POST['editrow']);

} else {
    $BILL       =   array();
}

if ($_POST['process'] == "P") {

    if ($_POST['termed'] == "") { $_POST['termed']="0000-00-00"; }
    if ($_POST['datebilled'] == "") { $_POST['datebilled']="0000-00-00"; }

    if ($_POST['editrow']) {
    
      $set_info     =   array(
                            "Termed         =   :termed", 
                            "Reason         =   :reason", 
                            "TaxCredit      =   :taxcredit", 
                            "DateBilled     =   :datebilled",
                            "Comments       =   :comments", 
                            "LastUpdated    =   NOW()"
                            );
      $where_info   =   array("wotcID = :wotcid", "ApplicationID = :applicationid", "Year = :year");
      $params       =   array(
                            ':termed'       =>  date("Y-m-d",strtotime($_POST['termed'])),
                            ':reason'       =>  $_POST['reason'],
                            ':taxcredit'    =>  $_POST['taxcredit'],
                            ':datebilled'   =>  date("Y-m-d",strtotime($_POST['datebilled'])),
                            ':comments'     =>  $_POST['comment'],
                            ':wotcid'       =>  $_POST['wotcID'],
                            ':applicationid'=>  $_POST['ApplicationID'],
                            ':year'         =>  $_POST['editrow']
                          );
      G::Obj('WOTCBilling')->updBillingInfo($set_info, $where_info, array($params));
    
    } else {
    
        $year   =   1;
        
        $yr     =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);
        
        if ($yr) $year = $yr + 1;
    
        $info     =   array(
                            'wotcID'        =>  $_POST['wotcID'],
                            'ApplicationID' =>  $_POST['ApplicationID'],
                            'Year'          =>  $year,
                            'Termed'        =>  date("Y-m-d",strtotime($_POST['termed'])),
                            'Reason'        =>  $_POST['reason'],
                            'TaxCredit'     =>  $_POST['taxcredit'],
                            'DateBilled'    =>  date("Y-m-d",strtotime($_POST['datebilled'])),
                            'Comments'      =>  $_POST['comment'],
                            'LastUpdated'   =>  "NOW()"
                    );
        
        G::Obj('WOTCBilling')->insBillingInfo($info);
      
    } 

    if (($_POST['reason'] == "Employment Termination") && ($_POST['taxcredit'] == 0)) {
        $set_info   =   array("Qualifies = 'N'");
        $where      =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
        $params     =   array(':wotcID'=>$_POST['wotcID'], ':ApplicationID'=>$_POST['ApplicationID']);
        G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where, array($params));
    }

    echo 'closewindow';
    exit;

} // end process
?>

<strong style="font-size:14pt;">Billing</strong><br>
<table border="0" cellspacing="0" cellpadding="5">

<tr><td align="right">
Application ID:
</td><td>
<?php echo $_POST['ApplicationID']; ?> (XXX-XX-<?php echo $AD['Social3']; ?>)<br>
</td></tr>

<tr><td align="right" valign="top">
Applicant:
</td><td valign="top">
<b><?php echo $AD['FirstName']; ?> <?php echo $AD['MiddleName']; ?> <?php echo $AD['LastName']; ?></b><br>
<?php echo $AD['Address']; ?><br>
<?php echo $AD['City']; ?>, <?php echo $AD['State']; ?> <?php echo $AD['ZipCode']; ?><br>
</td></tr>

<tr><td align="right" valign="top">
WOTC:
</td><td valign="top">
Start Date: <?php echo $PROCESSING['StartDate']; ?><br>
Category: <?php echo $PROCESSING['Category']; ?><br>
</td></tr>

<tr><td align="right" valign="top">
Earnings:
</td><td valign="top">
Hours: <?php echo number_format($TTHRS['Hours']); ?><br>
Wages: $ <?php echo number_format($TTHRS['Wages'],2,".",","); ?><br>
AS OF: <?php echo $TTHRS['ASOF']; ?><br>
</td></tr>

<tr><td align="right">
Tax Credit Earned:
</td><td>
<?php
list($TaxCredit, $active, $comment) = G::Obj('WOTCCalculator')->calculate_wotcCredit($_POST['wotcID'], $_POST['ApplicationID'], $year);

if ($_POST['editrow']) { 
echo '<input type="text" name="taxcredit" value="' . $BILL['TaxCredit'] . '" size="6">';
} else {
  echo '<input type="hidden" name="taxcredit" value="' . $TaxCredit . '">';
}
echo '&nbsp;&nbsp;';
echo '$' . number_format($TaxCredit ,2,".",",");
echo '&nbsp;&nbsp;';
echo $comment;

if($$TTHRS['Hours'] == 0) {
    $BILL['Termed'] =   $PROCESSING['StartDate'];
    $BILL['Reason'] =   "Employment Termination";
} else {
    $date_billed    =   date_create(date("Y-m-d"));
    if($BILL['DateBilled']=="") $BILL['DateBilled'] = date_format($date_billed,'m/d/Y');
}

 
?>
</td></tr>

<tr><td align="right">
Termed:
</td><td>
<input id="termed<?php echo htmlspecialchars($_POST['wotcID']); ?><?php echo htmlspecialchars($_POST['ApplicationID']); ?>" name="termed" type="text" size="10" value="<?php echo htmlspecialchars($BILL['Termed']); ?>" onfocus="clearDefault(this)" />
&nbsp;&nbsp;
Reason: <select name="reason">
<?php
$REASONS    =   array('Maxed Limit', 'Employment Termination');
foreach ($REASONS as $reason) {
    echo '<option value="' . $reason . '"';
    if ($reason == $BILL['Reason']) {
        echo ' selected';
    }
    echo '>' . $reason . '</option>' . "\n";
}
?>
</select>
</td></tr>

<tr><td align="right">
Date Billed:
</td><td>
<input id="datebilled<?php echo htmlspecialchars($_POST['wotcID']); ?><?php echo htmlspecialchars($_POST['ApplicationID']); ?>" name="datebilled" type="text" size="10" value="<?php echo $BILL['DateBilled']; ?>" onfocus="clearDefault(this)" />
</td></tr>

<tr><td align="right">
Comment:
</td><td>
<input type="text" name="comment" size="50" maxlength="255" value="<?php echo $BILL['Comments']; ?>">
</td></tr>

<tr><td colspan="100%" align="center" valign="bottom" height="40">
<input type="hidden" name="wotcID" value="<?php echo htmlspecialchars($_POST['wotcID']); ?>">
<input type="hidden" name="ApplicationID" value="<?php echo htmlspecialchars($_POST['ApplicationID']); ?>">
<button onclick="loadTrackBillForm('<?php echo htmlspecialchars($_POST['type']); ?>','<?php echo htmlspecialchars($_POST['wotcID']); ?>','<?php echo htmlspecialchars($_POST['ApplicationID']); ?>','P','<?php echo htmlspecialchars($_POST['editrow']); ?>');return false;">Bill Applicant</button>
</td></tr>

</table>
<?php
} // end if
?>
