<?php
require_once '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">Associate a wotcID</strong></div>";
echo '<a href="#openDocuments" onclick="updateWOTCIDs(\'' . $_REQUEST['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Assign/Edit wotcID\'s" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Go Back</b></a>';
echo '</div>';

if ($_REQUEST['process'] == 'assoc') {

    if ($_REQUEST['wotcID'] != "" && $_REQUEST['CompanyID'] != "") {

      G::Obj('GenericQueries')->conn_string =   "WOTC";
      $update_query = "UPDATE OrgData set CompanyID = :CompanyID WHERE wotcID = :wotcID";
      $params         =   array(":wotcID"=>$_REQUEST['wotcID'],":CompanyID"=>$_REQUEST['CompanyID']);
      G::Obj('GenericQueries')->updInfoByQuery($update_query, array($params));

      echo '<div style="text-align:center;margin:20px 0;">';
      echo 'The wotcID <strong>' . $_REQUEST['wotcID'] . '</strong> has been successfully assigned to this company.';
      echo '</div>';

    } else {

      echo '<div style="text-align:center;margin:20px 0;">';
      echo 'No change has been made.';
      echo '</div>';
    }

    exit;

} // end process

G::Obj('GenericQueries')->conn_string =   "WOTC";
$query      =   "SELECT";
$query     .=   " OrgData.wotcID, CRMFEIN.AKADBA, concat(OrgData.EIN1,'-',OrgData.EIN2) AS FEIN, OrgData.Address,";
$query     .=   " OrgData.City, OrgData.State, OrgData.ZipCode";
$query 	   .=   " FROM OrgData";
$query     .=   " LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
$query     .=   " WHERE OrgData.CompanyID = ''";
$query     .=   " ORDER BY OrgData.wotcID";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

echo '<div style="margin-top:20px;">';
echo '<select name="wotcID">';
foreach ($RESULTS AS $AVAIL) {
	echo '<option value="'.$AVAIL['wotcID'].'">';
	echo $AVAIL['State'] . ' (' . $AVAIL['FEIN'] . ')';
	echo ' ' . $AVAIL['AKADBA'];
	echo ', ' . $AVAIL['Address'];
	echo ', ' . $AVAIL['City'];
	echo ' ' . $AVAIL['ZipCode'];
	echo ' (' . $AVAIL['wotcID'] . ')';
     	echo '</option>';
}
echo '</select>';
echo '</div>';

echo "<div style=\"margin-top:20px;\">";
echo '<input type="hidden" name="process" value="assoc">';
echo '<button onclick="associateExistingWotcID(\'' . $_REQUEST['CompanyID'] . '\',\'submit\');return false;">Associate wotcID</button>';
echo "</div>";

?>
