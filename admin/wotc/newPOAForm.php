<?php
ini_set("allow_url_fopen", 1);
require_once '../Configuration.inc';

if (isset($_REQUEST['CompanyID'])) {

list($EIN1,$EIN2) = explode("-",$_REQUEST['FEIN']);

$columns = '*';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];

$FEINCONFIG = G::Obj('WOTCcrm')->getFEIN($CRM['CompanyID'],$EIN1,$EIN2);

$FORMDATA = array();
$FORMDATA['CompanyName']=$CRM['CompanyName'];
$FORMDATA['Address']=$FEINCONFIG[0]['Address'];
$FORMDATA['City']=$FEINCONFIG[0]['City'];
$FORMDATA['State']=$FEINCONFIG[0]['State'];
$FORMDATA['ZipCode']=$FEINCONFIG[0]['ZipCode'];
$FORMDATA['DBA']=$FEINCONFIG[0]['AKADBA'];
$FORMDATA['EIN1']=$FEINCONFIG[0]['EIN1'];
$FORMDATA['EIN2']=$FEINCONFIG[0]['EIN2'];
$FORMDATA['POAStartingDate']=$FEINCONFIG[0]['POAStartingDate'];
$FORMDATA['POAExpirationDate']=$FEINCONFIG[0]['POAExpirationDate'];
$FORMDATA['POAState']=$FEINCONFIG[0]['POAState'];

$FileID = preg_replace("/[^a-zA-Z1-9]/", "", $FEINCONFIG[0]['AKADBA']);

} else {
$FileID = isset($_REQUEST['AgreementID']) ? $_REQUEST['AgreementID'] : '';
$column = '*';
$FORMDATA = G::Obj('WOTCAgreements')->getAgreementsInfo($column,$FileID);
}

// PDF Form
$pdfform = ADMIN_DIR . 'wotc/PDFFiles/WOTC_POA.pdf';

// Temp file for merged data
$txfdffile = ADMIN_DIR . 'wotc/vault/' . $FileID . '_POA.xfdf';

// Applicants Output file
$filename = $FileID . '-POA.pdf';
$outputpdf = ADMIN_DIR . 'wotc/vault/' . $filename;


if($FORMDATA['POAState'] == "KS"){
    $RepresentativeOne = "Cost Management Services, LLC\r\n321 Main Street\r\nFarmington, CT 06032";
    $RepNoticeOne = $RepresentativeOne!="" ? "Yes" : "No";
    
    $RepresentativeTwo = "Brian Kelly - Cost Management Services, LLC\r\n321 Main Street\r\nFarmington, CT 06032";
    $RepNoticeTwo = $RepresentativeTwo!="" ? "Yes" : "No";
}else if($FORMDATA['POAState'] == "TX"){
    $RepresentativeOne = "Cost Management Services, LLC - Lisa Schneider\r\n321 Main Street\r\nFarmington, CT 06032";
    $RepNoticeOne = $RepresentativeOne!="" ? "Yes" : "No";
    
    $RepresentativeTwo = "";
    $RepNoticeTwo = $RepresentativeTwo!="" ? "Yes" : "No";
}else{
    $RepresentativeOne = "Cost Management Services, LLC\r\n321 Main Street\r\nFarmington, CT 06032";
    $RepNoticeOne = $RepresentativeOne!="" ? "Yes" : "No";
    
    $RepresentativeTwo = "Brian Kelly\r\n321 Main Street\r\nFarmington, CT 06032";
    $RepNoticeTwo = $RepresentativeTwo!="" ? "Yes" : "No";
}


$TelephoneOne = $RepresentativeOne ?"860-678-4401":"";
$FaxNumberOne = $RepresentativeOne ?"860-356-4382":"";

$TelephoneTwo = $RepresentativeTwo ?"860-678-4401":"";
$FaxNumberTwo = $RepresentativeTwo ?"860-356-4382":"";


	$XFDF = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$XFDF .= "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n";
	$XFDF .= "<f href=\"WOTC_POA.pdf\"/>\n";
	$XFDF .= "<fields>\n";

                $XFDF .= "<field name=\"TaxpayerName\">";
                $XFDF .= "<value>" . $FORMDATA['CompanyName'] . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Taxpayer\">";
		$XFDF .= "<value>";
		$XFDF .= $FORMDATA['DBA']."\n";
		$XFDF .= $FORMDATA['Address']."\n";
		$XFDF .= $FORMDATA['City'].", ".$FORMDATA['State']." " . $FORMDATA['ZipCode'];
		$XFDF .= "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"TaxpayerID\">";
                $XFDF .= "<value>" . $FORMDATA['EIN1'] ." - ".$FORMDATA['EIN2']. "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"TaxpayerPhone\">";
                $XFDF .= "<value>800-517-9099</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"RepresentativeOne\">";
                $XFDF .= "<value>" . $RepresentativeOne . "</value>";
                $XFDF .= "</field>\n";
                
                $XFDF .= "<field name=\"RepNoticeOne\">";
                $XFDF .= "<value>" . $RepNoticeOne . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"TeleRepOne\">";
                $XFDF .= "<value>" . $TelephoneOne. "</value>";
                $XFDF .= "</field>\n";
                
                $XFDF .= "<field name=\"FaxRepOne\">";
                $XFDF .= "<value>" . $FaxNumberOne. "</value>";
                $XFDF .= "</field>\n";
                
                $XFDF .= "<field name=\"RepresentativeTwo\">";
                $XFDF .= "<value>" . $RepresentativeTwo . "</value>";
                $XFDF .= "</field>\n";
                
                $XFDF .= "<field name=\"RepNoticeTwo\">";
                $XFDF .= "<value>" . $RepNoticeTwo . "</value>";
                $XFDF .= "</field>\n";
                
                $XFDF .= "<field name=\"TeleRepTwo\">";
                $XFDF .= "<value>" . $TelephoneTwo . "</value>";
                $XFDF .= "</field>\n";
                
                $XFDF .= "<field name=\"FaxRepTwo\">";
                $XFDF .= "<value>" . $FaxNumberTwo . "</value>";
                $XFDF .= "</field>\n";
 
                $XFDF .= "<field name=\"PeriodOne\">";
                $XFDF .= "<value>" . date('m/d/Y',strtotime($FORMDATA['POAStartingDate'])) ." - ".date('m/d/Y',strtotime($FORMDATA['POAExpirationDate'])). "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"PeriodTwo\">";
                $XFDF .= "<value>" .  date('m/d/Y',strtotime($FORMDATA['POAStartingDate'])) ." - ".date('m/d/Y',strtotime($FORMDATA['POAExpirationDate'])). "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"PeriodThree\">";
                $XFDF .= "<value>" .  date('m/d/Y',strtotime($FORMDATA['POAStartingDate'])) ." - ".date('m/d/Y',strtotime($FORMDATA['POAExpirationDate'])). "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Jurisdiction\">";
                $XFDF .= "<value>" . $FORMDATA['POAState'] . "</value>";
                $XFDF .= "</field>\n";

        $XFDF .= "</fields>\n";
        $XFDF .= "</xfdf>\n";

        $fh = fopen($txfdffile, 'w');
        if (!$fh) { die('Cannot open file ' . $txfdffile); }
        fwrite($fh, $XFDF);
        fclose($fh);

// Merge temp file with PDF and write to applicants file
$command = 'pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($txfdffile) . ' output ' . escapeshellarg($outputpdf);

system($command);
// remove temp file
system('rm ' . escapeshellarg($txfdffile));
@chmod($outputpdf, 0666);


// Used for triggering download instead of display
if (file_exists ( $outputpdf )) {
    header ( 'Content-Description: File Transfer' );
    header ( 'Content-Type: application/octet-stream' );
    header ( 'Content-Disposition: attachment; filename=' . basename ( $filename ) );
    header ( 'Content-Transfer-Encoding: binary' );
    header ( 'Expires: 0' );
    header ( 'Cache-Control:' );
    header ( 'Pragma: cache' );
    header ( 'Content-Length: ' . filesize ( $outputpdf ) );
    readfile ( $outputpdf );
    exit ();
}


?>
