<?php
$columns =  "IF(StartDate = '0000-00-00','',date_format(StartDate,'%m/%d/%Y')) StartDate,";
$columns .= " IF(OfferDate = '0000-00-00','',date_format(OfferDate,'%m/%d/%Y')) OfferDate,";
$columns .= " IF(HiredDate = '0000-00-00','',date_format(HiredDate,'%m/%d/%Y')) HiredDate,";
$columns .= " IF(Received = '0000-00-00','',date_format(Received,'%m/%d/%Y')) Received,";
$columns .= " IF(Filed = '0000-00-00','',date_format(Filed,'%m/%d/%Y')) Filed,";
$columns .= " IF(Confirmed= '0000-00-00','',date_format(Confirmed,'%m/%d/%Y')) Confirmed,";
$columns .= " StartingWage, Position, Qualified, Category, Comments";
$PROCESSING =   G::Obj('WOTCProcessing')->getProcessingInfo($columns, $_POST['wotcID'], $_POST['ApplicationID']);

// get WOTC OrgData
$columns                =   "WotcFormID";
$where                  =   array("wotcID = :wotcid");
$params                 =   array($_POST['wotcID']);
$ORGS_RES               =   G::Obj('WOTCOrganization')->getOrgDataInfo($columns, $where, '', '', array($params));
$ORGS                   =   $ORGS_RES['results'];

$AD         =   G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $_POST['wotcID'], $ORGS[0]['WotcFormID'], $_POST['ApplicationID']);

if ($_POST['process'] == "P") {
    
    if ($_POST['offerdatep'] == "") {
        $_POST['offerdatep'] = "0000-00-00";
    }
    if ($_POST['hireddatep'] == "") {
        $_POST['hireddatep'] = "0000-00-00";
    }
    if ($_POST['startdatep'] == "") {
        $_POST['startdatep'] = "0000-00-00";
    }
    if ($_POST['receivedp'] == "") {
        $_POST['receivedp'] = "0000-00-00";
    }
    if ($_POST['filedp'] == "") {
        $_POST['filedp'] = "0000-00-00";
    }
    if ($_POST['confirmedp'] == "") {
        $_POST['confirmedp'] = "0000-00-00";
    }
    
    $category = "";
    
    if ($_POST['qualifiedp'] == "N") {

        $category   =   $_POST['categorydeniedp'];
        $set_info   =   array("Qualifies = 'N'");
        $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
        $params     =   array(':wotcID'=>$_POST['wotcID'], ':ApplicationID'=>$_POST['ApplicationID']);
        G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
        
    } else {
        
        $set_info   =   array("Qualifies = 'Y'");
        $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
        $params     =   array(':wotcID'=>$_POST['wotcID'], ':ApplicationID'=>$_POST['ApplicationID']);
        G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
        
        $category   =   $_POST['categoryacceptedp'];
    }
    
    if ($_POST['qualifiedp'] == "U") {
        $category = "U";
    }
    
    //Skip fields on duplicate key
    $skip   =   array("wotcID", "ApplicationID");
    //Information to insert
    $info   =   array(
                    "wotcID"            =>  $_POST['wotcID'],
                    "ApplicationID"     =>  $_POST['ApplicationID'],  
                    "OfferDate"         =>  date("Y-m-d", strtotime($_POST['offerdatep'])), 
                    "HiredDate"         =>  date("Y-m-d", strtotime($_POST['hireddatep'])),  
                    "StartDate"         =>  date("Y-m-d", strtotime($_POST['startdatep'])), 
                    "StartingWage"      =>  $_POST['startingwagep'], 
                    "Position"          =>  $_POST['positionp'], 
                    "Received"          =>  date("Y-m-d", strtotime($_POST['receivedp'])), 
                    "Filed"             =>  date("Y-m-d", strtotime($_POST['filedp'])), 
                    "Confirmed"         =>  date("Y-m-d", strtotime($_POST['confirmedp'])), 
                    "Qualified"         =>  $_POST['qualifiedp'], 
                    "Category"          =>  $category, 
                    "Comments"          =>  $_POST['commentp'], 
                    "LastUpdated"       =>  "NOW()"
                );
    G::Obj('WOTCProcessing')->insUpdProcessing($info, $skip);
    
    if (($_POST['receivedp'] != "0000-00-00") 
        && ($_POST['offerdatep'] != "0000-00-00") 
        && ($_POST['hireddatep'] != "0000-00-00") 
        && ($_POST['startdatep'] != "0000-00-00") 
        && ($_POST['startingwagep'] > 0) 
        && ($_POST['positionp'] != "")) {
        G::Obj('WOTCPdf')->createPDFs($_POST['wotcID'], $_POST['ApplicationID']);
    }
    
    echo "closewindow";
    exit();
} // end process

if (($_POST['ApplicationID']) && ($_POST['wotcID'])) {
    ?>
<strong style="font-size: 14pt;">Processing</strong>
<br>
<table border="0" cellspacing="0" cellpadding="5">

	<tr>
		<td colspan="100%" height="10"></td>
	</tr>

	<tr>
		<td align="right">Application ID:</td>
		<td>
		<?php echo $_POST['ApplicationID']; ?> (XXX-XX-<?php echo $AD['Social3']; ?>)<br>
		</td>
	</tr>

	<tr>
		<td align="right" valign="top">Applicant:</td>
		<td valign="top"><b><?php echo $AD['FirstName']; ?> <?php echo $AD['MiddleName']; ?> <?php echo $AD['LastName']; ?></b><br>
        <?php echo $AD['Address']; ?><br>
        <?php echo $AD['City']; ?>, <?php echo $AD['State']; ?> <?php echo $AD['ZipCode']; ?><br>
		</td>
	</tr>

	<tr>
		<td colspan="2">1. After receiving qualified applicants hire
			information
			<hr size="1">
		</td>
	</tr>

	<tr>
		<td align="right">Received at CMS:</td>
		<td><input
			id="receivedp<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="receivedp" type="text"
			value="<?php echo $PROCESSING['Received']; ?>" size="10"
			onBlur="validate_date(this.value,'Received at CMS Date');" />
	    <button style="font-size:8pt;font-face:italic;padding: 1px 5px;margin-left:20px;" onclick="replicateDate('<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>');return false;">Apply date to all</button> </td>
	</tr>

	<tr>
		<td align="right">Offer Date:</td>
		<td><input
			id="offerdatep<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="offerdatep" type="text"
			value="<?php echo $PROCESSING['OfferDate']; ?>" size="10"
			onBlur="validate_date(this.value,'Offer Date');" /></td>
	</tr>

	<tr>
		<td align="right">Hired Date:</td>
		<td><input
			id="hireddatep<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="hireddatep" type="text"
			value="<?php echo $PROCESSING['HiredDate']; ?>" size="10"
			onBlur="validate_date(this.value,'Hired Date');" /></td>
	</tr>

	<tr>
		<td align="right">Start Date:</td>
		<td><input
			id="startdatep<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="startdatep" type="text"
			value="<?php echo $PROCESSING['StartDate']; ?>" size="10"
			onBlur="validate_date(this.value,'Start Date');" /></td>
	</tr>

	<tr>
		<td align="right">Starting Wage:</td>
		<td><input
			id="startingwagep<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="startingwagep" type="text"
			value="<?php echo $PROCESSING['StartingWage']; ?>" size="10" /></td>
	</tr>
<?php
	if ($PROCESSING['Position'] == "") {
		$PROCESSING['Position'] = "Crew";
	}
?>
	<tr>
		<td align="right">Position:</td>
		<td><input
			id="positionp<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="positionp" size="25" maxlength="255" type="text"
			value="<?php echo $PROCESSING['Position']; ?>" size="10" /></td>
	</tr>

	<tr>
		<td colspan="2">2. After verifying 8850 and 9051 forms and filing with
			state
			<hr size="1">
		</td>
	</tr>

	<tr>
		<td align="right">Filed with State (<?php echo $AD['WorkState']; ?>):</td>
		<td><input
			id="filedp<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="filedp" type="text" value="<?php echo $PROCESSING['Filed']; ?>"
			size="10" onBlur="validate_date(this.value,'Filed with State Date');" />
		</td>
	</tr>

    <tr>
		<td colspan="2">3. Once confirmed from the state and prior to
			collecting payroll data
			<hr size="1">
		</td>
	</tr>

	<tr>
		<td align="right">Confirmed from State:</td>
		<td><input
			id="confirmedp<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>"
			name="confirmedp" type="text"
			value="<?php echo $PROCESSING['Confirmed']; ?>" size="10"
			onBlur="validate_date(this.value,'Confirmed from State Date');" />
        </td>
	</tr>

	<tr>
		<td align="right">Qualified:</td>
		<td>
		<select name="qualifiedp" onChange="CheckReason(this,'<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>')">
            <?php
            $columns            =   "QualificationCode, Description";
            $where_info         =   array("Type = 'Qualify'");
            $QUALIFYCODES_RES   =   G::Obj('WOTCQualificationCodes')->getQualificationCodesInfo($columns, $where_info, 'SortOrder');
            $QUALIFYCODES       =   $QUALIFYCODES_RES['results'];
            
            foreach ($QUALIFYCODES as $QC) {
                echo "<option value=\"" . $QC['QualificationCode'] . "\"";
                if ($PROCESSING['Qualified'] == $QC['QualificationCode']) {
                    echo " selected";
                }
                echo ">" . $QC['Description'] . "</option>\n";
            } // end foreach
            ?>
        </select>
        </td>
	</tr>

    <?php
    $accept_display = "none";
    if ($PROCESSING['Qualified'] == "Y") {
        $accept_display = "block";
    }
    ?>

    <tr>
		<td align="right">Category of Qualification:</td>
		<td>
			<div id="AcceptFilterQuestion<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>" style="display:<?php echo $accept_display; ?>;">
				<select name="categoryacceptedp" style="width: 250px;">
					<option value="">Please Select</option>
                    <?php
                    $columns            =   "QualificationCode, Description, CapYearOne, CapYearTwo";
                    $where              =   array("Type = 'Approval'");
                    $APPROVALCODES_RES  =   G::Obj('WOTCQualificationCodes')->getQualificationCodesInfo($columns, $where, 'SortOrder');
                    $APPROVALCODES      =   $APPROVALCODES_RES['results'];
                    
                    foreach ($APPROVALCODES as $AC) {
                        $value = $AC['CapYearOne'] + $AC['CapYearTwo'];
                        if ($AC['CapYearTwo'] > 0) {
                            $value .= " over 2 years";
                        }
                        echo "<option value=\"" . $AC['QualificationCode'] . "\"";
                        if ($PROCESSING['Category'] == $AC['QualificationCode']) {
                            echo " selected";
                        }
                        echo ">" . $AC['Description'] . " - $" . $value . "</option>\n";
                    } // end foreach
                    ?>
                </select>
			</div>
            <?php
            $deny_display = "none";
            if ($PROCESSING['Qualified'] == "N") {
                $deny_display = "block";
            }
            ?>
            <div id="DenialFilterQuestion<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>" style="display:<?php echo $deny_display; ?>;">
				<select name="categorydeniedp">
					<option value="">Please Select</option>
                    <?php
                    $columns            =   "QualificationCode, Description";
                    $where              =   array("Type = 'Denial'");
                    $DENIALCODES_RES    =   G::Obj('WOTCQualificationCodes')->getQualificationCodesInfo($columns, $where, 'SortOrder');
                    $DENIALCODES        =   $DENIALCODES_RES['results'];
                    
                    foreach ($DENIALCODES as $DC) {
                        echo "<option value=\"" . $DC['QualificationCode'] . "\"";
                        if ($PROCESSING['Qualified'] == $DC['QualificationCode']) {
                            echo " selected";
                        }
                        echo ">" . $DC['Description'] . "</option>\n";
                    } // end foreach
                    ?>
                </select>
			</div>
		</td>
	</tr>

	<tr>
		<td align="right">Comment:</td>
		<td><input type="text" name="commentp" size="40" maxlength="255" value="<?php echo $PROCESSING['Comments']; ?>"></td>
	</tr>

	<tr>
		<td colspan="100%" align="center" valign="bottom" height="40">
    		<input type="hidden" name="wotcID" value="<?php echo $_POST['wotcID']; ?>">
            <input type="hidden" name="ApplicationID" value="<?php echo $_POST['ApplicationID']; ?>">
            <button onclick="loadTrackBillForm('<?php echo $_POST['type']; ?>','<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>','P','');return false;">Process Applicant</button>
        </td>
	</tr>
</table>
<?php 
}  // end wotcID ApplicationID 

?>
