<?php 
include '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

list($EIN1,$EIN2) = explode("-",$_REQUEST['FEIN']);

echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">Update FEIN AKA/DBA - POA</strong></div>";
echo '<a href="#openDocuments" onclick="updateFEIN(\'' . $_REQUEST['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Update FEIN AKA/DBA - POA" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Go Back</b></a>';
echo '</div>';

if ($_REQUEST['process'] == 'poa') {

	$info   =   array(
                 "CompanyID"    =>  $_REQUEST['CompanyID'],
                 "EIN1"         =>  $EIN1,
                 "EIN2"         =>  $EIN2,
           	 "AKADBA"       =>  $_REQUEST['AKADBA'],
         	 "Address"      =>  $_REQUEST['Address'],
         	 "City"         =>  $_REQUEST['City'],
         	 "State"        =>  $_REQUEST['State'],
         	 "ZipCode"      =>  $_REQUEST['ZipCode'],
         	 "Phone"        =>  json_encode(array($_REQUEST['PhoneAreaCode'],$_REQUEST['PhonePrefix'],$_REQUEST['PhoneSuffix'])),
		 "POAStartingDate"    =>  date("Y-m-d",strtotime($_REQUEST['POAStartDate'])),
		 "POAExpirationDate"  =>  date("Y-m-d",strtotime($_REQUEST['POAEndDate'])),
                 "POAState"           =>  $_REQUEST['POAState']
        );

        $skip   =   array("CompanyID","EIN1","EIN2");

        G::Obj('WOTCcrm')->insUpdCRMFEIN($info, $skip);


    $MESSAGE = "<span style=\"color:red;\">Dates and State successfully updated.</span>";
    $MESSAGE .= "&nbsp;&nbsp;&nbsp;";

    $MESSAGE .= "<a style=\"text-decoration:none;font-weight:normal;color:#000;\" href=\"" . ADMIN_HOME . "wotc/newPOAForm.php?CompanyID=" . $_REQUEST['CompanyID'] . "&FEIN=" . $_REQUEST['FEIN'] . "\"><img src=\"" . ADMIN_HOME . "images/page_white_acrobat.png\" border=\"0\" title=\"View PDF Form\" style=\"margin: 0px 3px -4px 0px;\">View PDF Form</a>";

} // end process

$columns = '*';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];

$FCRES = G::Obj('WOTCcrm')->getFEIN($CRM['CompanyID'],$EIN1,$EIN2);
$FC=$FCRES[0];

 echo '<div style="line-height:320%;margin-top:20px;">';
 echo 'Organization Name: <strong>' . $CRM['CompanyName'] . '</strong><br>';
 echo '&nbsp;AKA/DBA: ' . '<input type="text" name="AKADBA" value="' . $FC['AKADBA'] . '" size="50"><br>';
 echo '&nbsp;&nbsp;&nbsp;';
 echo 'Address: ' . '<input type="text" name="Address" value="' . $FC['Address'] . '" size="50"><br>';
 echo '&nbsp;&nbsp;&nbsp;&nbsp;';
 echo '&nbsp;&nbsp;&nbsp;&nbsp;';
 echo '&nbsp;&nbsp;';
 echo 'City: ' . '<input type="text" name="City" value="' . $FC['City'] . '" size="28">&nbsp;&nbsp;';
 echo 'State: ';
 echo '<select size="1" name="State" style="max-width:300px;">';
 echo '<option value="">Please Select</option>';

 $STATES = $AddressObj->getAddressStateList ();
 $CP = json_decode($FC['Phone'], true);

 foreach ($STATES['results'] as $STATE) {
  if ($STATE['Abbr'] == $FC['State']) { $ckd = " selected"; } else { $ckd = ""; }
  echo "<option value=\"" . $STATE['Abbr'] . "\"" . $ckd . ">" . $STATE['Description'] . "</option>";
 } // end foreach

 echo '</select>';
 echo '&nbsp;Zip Code: ' . '<input type="text" name="ZipCode" value="' . $FC['ZipCode'] . '" size="6"><br>';
 echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
 echo 'Phone: ';
 echo '(<input type="text" name="PhoneAreaCode" value="';
 echo $CP[0];
 echo '" size="3">)&nbsp;';
 echo '<input type="text" name="PhonePrefix" value="';
 echo $CP[1];
 echo '" size="3">&nbsp;-&nbsp;';
 echo '<input type="text" name="PhoneSuffix" value="';
 echo $CP[2];
 echo '" size="4">';
echo '</div>';

 if ($_POST['POAStartDate'] == "" && $FC['POAStartDate'] == "") { $_POST['POAStartDate'] = "mm/dd/yyyy"; }
 if ($_POST['POAEndDate'] == "" && $FC['POAEndDate'] == "" ) { $_POST['POAEndDate'] = "mm/dd/yyyy"; }

 echo "<div style=\"margin-top:20px;line-height:260%;\">";
 echo '&nbsp;&nbsp;';
 echo 'POA Start Date: ';
 echo '<input id="POAStartDate" name="POAStartDate" class="input-date" data-msg-required="Please enter a POA End Date in the format mm/dd/yyyy" type="text" value="' . date('m/d/Y',strtotime($FC['POAStartingDate'])) . '" size="10" />';
 echo '<br>';
 echo '&nbsp;&nbsp;&nbsp;';
 echo 'POA End Date: ';
 echo '<input id="POAEndDate" name="POAEndDate" class="input-date" data-msg-required="Please enter a POA End Date in the format mm/dd/yyyy" type="text" value="' . date('m/d/Y',strtotime($FC['POAExpirationDate'])) . '" size="10" />';
 echo '<br>';
 echo 'POA Filing State: ';
 $POAState = isset($_POST['POAState']) ? htmlentities($_POST['POAState']) : $FC['POAState'];
 echo '<input name="POAState" id="POAState" class="input-val" data-msg-required="Please enter the POA State" type="text" value="' . $POAState . '" size="10" />';
 echo '&nbsp;&nbsp;';
 echo '<i style="color:#ababab">KS and TX cannot be a list.</i>';
 echo "</div>";

 echo "<div style=\"margin-top:20px;\">";
 echo '<input type="hidden" name="process" value="poa">';
 echo '<button onclick="processFEIN(\'' . $_REQUEST['CompanyID'] . '\',\'' . $FC['EIN1'] . '-' . $FC['EIN2'] . '\',\'submit\');return false;">Update FEIN</button>';
 if ($MESSAGE != "") {
 echo "&nbsp;&nbsp;&nbsp;";
 echo $MESSAGE;
 }
 echo "</div>";

?>
