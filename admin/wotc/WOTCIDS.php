<?php
require_once '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

/*
G::Obj('GenericQueries')->conn_string =   "WOTC";
$query      =   "SELECT concat(\"'\",group_concat(concat(iRecruitOrgID, iRecruitMultiOrgID) SEPARATOR \"','\"),\"'\") AS INUSE";
$query     .=   " FROM OrgData WHERE (iRecruitOrgID != '' OR iRecruitMultiOrgID != '')";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

G::Obj('GenericQueries')->conn_string =   "IRECRUIT";
$query      =   "SELECT OrgID, MultiOrgID, OrganizationName FROM OrgData";
$query     .=   " WHERE concat(OrgID,MultiOrgID) not in (:INUSE)";
$query     .=   " AND OrganizationName not like '%(inactive)%'";
$params  = array(":INUSE"=>$RESULTS[0]['INUSE']);
$IRECRUIT =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
 */

$wotc_forms = G::Obj('WOTCFormQuestions')->getActiveWotcFormIds();

echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;margin-bottom:20px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">Assign/Edit wotcID</strong></div>";
echo '<a href="#openDocuments" onclick="associateExistingWotcID(\''.$_REQUEST['CompanyID'] .'\',\'\');"><img src="'.IRECRUIT_HOME.'images/icons/arrow_refresh_small.png" title="Associate" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Associate an unassigned wotcID</b></a>';
echo '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
echo '<a href="#openDocuments" onclick="processWotcOrganization(\''.$_REQUEST['CompanyID'] .'\',\'\',\'\');"><img src="'.IRECRUIT_HOME.'images/icons/add.png" title="Create" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Create a new wotcID</b></a>';
echo '</div>';

$WOTCIDLS = G::Obj('WOTCcrm')-> getFirstLastScreened($_REQUEST['CompanyID'],'wotcID');
$WOTCIDS = G::Obj('WOTCcrm')->getWOTCIDs($_REQUEST['CompanyID']);

foreach ($WOTCIDS AS $wotcID) {

echo printDisplay($wotcID,$WOTCIDLS);

} // end foreach

function printDisplay($wotcID,$WOTCIDLS) {

   $AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($wotcID);
   $ORGDATA      =   G::Obj('WOTCOrganization')->getOrganizationInfo("*", $wotcID);

   $YN=array("Y"=>"Yes","N"=>"No");

   $rtn = "";

   $rtn .= '<div style="text-align:right;margin-top:6px;float:right;">';
   $rtn .= '<a href="#openDocuments" onclick="processWotcOrganization(\''.$ORGDATA['CompanyID'] .'\',\'' . $ORGDATA['wotcID'] . '\',\'\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Edit" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Edit</b></a>';
   $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   $rtn .= '<a href="#openDocuments" onclick="removeWotcID(\''.$ORGDATA['CompanyID'].'\',\'' . $ORGDATA['wotcID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/cross.png" title="Remove" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Remove</b></a>';
   $rtn .= '</div>';

   $rtn .= '<div style="line-height:200%;margin-top:10px;">';
   $rtn .= 'wotcID: <strong>' . $wotcID . '</strong>';
   $rtn .= '<br>';

   $rtn .= 'EIN#: <strong>' . $ORGDATA['EIN1'] . '-' . $ORGDATA['EIN2'] . '</strong> - ' . $ORGDATA['State'] . '&nbsp;&nbsp;&#149;&nbsp;&nbsp;' . $AKADBA['AKADBA'];
   $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   $rtn .= 'ApplicationID: <strong>' . $ORGDATA['ApplicationID'] . '</strong>';
   /*
   if ($ORGDATA['iRecruitOrgID'] != "") {
     $rtn .= '<br>';
     $rtn .= 'iRecruit Organization: <strong>' . G::Obj('Organizations')->getOrgTitle($ORGDATA['iRecruitOrgID'], $ORGDATA['iRecruitMultiOrgID']) . '</strong>';
   }
    */
   $rtn .= '<br>';
   $rtn .= 'Nonprofit: <strong>' . $YN[$ORGDATA['Nonprofit']] . '</strong>';
   $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   $rtn .= 'Call Center: <strong>' . $YN[$ORGDATA['CallCenter']] . '</strong>';
   $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   $rtn .= 'eSignature: <strong>' . $YN[$ORGDATA['eSignature']] . '</strong>';
   $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   $rtn .= 'Active: <strong>' . $YN[$ORGDATA['Active']] . '</strong>';
   $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   $rtn .= 'WOTC Form: <strong>' . $ORGDATA['WotcFormID'] . '</strong>';
   $rtn .= '<br>';

   if (count($WOTCIDLS[$wotcID]) > 0) {
     $rtn .= 'First Screened Date: <strong>' . $WOTCIDLS[$wotcID]['First Screen Date'] . "</strong>";
     $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
     $rtn .= 'Last Screened Date: <strong>' . $WOTCIDLS[$wotcID]['Last Screen Date'] . "</strong>";
     $rtn .= '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
     $rtn .= 'Total Screened: <strong>' . $WOTCIDLS[$wotcID]['WOTCScreened'] . "</strong><br>";
   } else {
     $rtn .= 'There are no applications for this wotcID.<br>';
   }


   $rtn .= '</div>';

   $rtn .= '<hr size="1">' . "\n";

   return $rtn;

} // end function

?>
