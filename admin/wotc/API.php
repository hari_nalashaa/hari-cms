<?php
require_once '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

$columns = 'CompanyID, APIKey';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results2 = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results2['results'][0];


if ($_REQUEST['Action'] == "generate") {
	$APIKEY = uniqid(rand(999, 99999).date('YmdHis', true));

} else if ($_REQUEST['Action'] == "revoke") {
	$APIKEY="";
}


if ($CRM['CompanyID'] != "") {
	$info   =   array(
                 "CompanyID"   =>  $CRM['CompanyID'],
                 "APIKey"      =>  $APIKEY
                 );
	$skip   =   array("CompanyID");

	G::Obj('WOTCcrm')->insUpdCRM($info, $skip);
}

?>
