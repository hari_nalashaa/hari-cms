<h3>Edit Wotc Form Question</h3>
<div>
	<form method="post" name="frmShowQuestionHtml" id="frmShowQuestionHtml">
    <?php
    //Get Question Information
    $ques_info = G::Obj('WOTCFormQuestionsDetails')->getWOTCFormQuestionsDetailsInfo("*", $_GET['WotcID'], $_GET['WotcFormID'], $_GET['QuestionID']);
    
    //Print the QuestionTypeHtml
    call_user_func(array(G::Obj('GenerateQuestionTypeHtml'), 'showQuestionType' . $ques_info['QuestionTypeID']), $ques_info);
    ?>
    <input type="submit" name="btnSaveQueTypeHtml" id="btnSaveQueTypeHtml" value="Save">
	</form>
</div>
<br>
<strong>Preview: </strong>
<br><br>
<div>
<?php
$ques_info['name'] =  $ques_info['QuestionID'];

//Print the Question Preview
$que_preview = call_user_func(array(G::Obj('ApplicationForm'), 'getQuestionView' . $ques_info['QuestionTypeID']), $ques_info);

echo $que_preview;
?>
</div>
<?php
if($ques_info['QuestionTypeID'] == 17) {
	?>
	<script type="text/javascript">
    var dates = $('#<?php echo $ques_info['QuestionID'];?>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: 'button',
                    buttonImage: '<?php echo WOTCADMIN_HOME; ?>calendar/css/smoothness/images/calendar.gif',
                    yearRange: '-80:+5',
                    buttonImageOnly: true,
                    buttonText: 'Select Date'
                });
    </script>
	<?php
}
?>