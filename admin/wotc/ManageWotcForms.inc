<h3>Wotc Manage Forms</h3>
<div>
<?php
$wotc_forms_res =   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsInfo("WotcID, WotcFormID", array(), "WotcFormID", "", array());
$wotc_forms     =   $wotc_forms_res['results'];
?>
<table cellpadding="3" cellspacing="3" width="100%">
    <tr>
        <td><strong>Wotc ID</strong></td>
        <td><strong>Form ID</strong></td>
        <td><strong>Action</strong></td>
    </tr>
    <?php
    for($wf = 0; $wf < count($wotc_forms); $wf++) {
        $wotc_id        =   $wotc_forms[$wf]['WotcID'];
        $wotc_form_id   =   $wotc_forms[$wf]['WotcFormID'];
        ?>
        <tr>
            <td><?php echo $wotc_id;?></td>
            <td><?php echo $wotc_form_id;?></td>
            <td>
                <a href="<?php echo ADMIN_HOME."index.php?pg=wotc_edit_form&WotcID=".$wotc_id."&WotcFormID=".$wotc_form_id;?>">
                    <img src="<?php echo IRECRUIT_HOME . "images/icons/pencil.png";?>" alt="Edit">&nbsp;Edit&nbsp;
                </a>    
            </td>
        </tr>
        <?php
    }
    ?>
</table>
</div>