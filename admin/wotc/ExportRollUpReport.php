#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/..') . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$USER = G::Obj('ADMIN')->getUserInformation($argv[1]);

if ($USER['Email'] != "") {

$report = "";

$REPORTS = array();
$REPORTS[]=array("ReportType"=>"Year","Active"=>"Y");
$REPORTS[]=array("ReportType"=>"Year","Active"=>"");

$REPORTS[]=array("ReportType"=>"Month","Active"=>"Y");
$REPORTS[]=array("ReportType"=>"Month","Active"=>"");

$REPORTS[]=array("ReportType"=>"Company","Active"=>"Y");
$REPORTS[]=array("ReportType"=>"Company","Active"=>"");

$REPORTS[]=array("ReportType"=>"Industry","Active"=>"Y");
$REPORTS[]=array("ReportType"=>"Industry","Active"=>"");

$REPORTS[]=array("ReportType"=>"Assignment","Active"=>"Y");
$REPORTS[]=array("ReportType"=>"Assignment","Active"=>"");

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Results from query")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

foreach ($REPORTS as $sheet=>$REPORT) {

$DATA = G::Obj('WOTCcrm')->getRollUpData($REPORT['Active'],$REPORT['ReportType']);

if ($sheet > 0) { $objPHPExcel->createSheet($sheet); }
$objPHPExcel->setActiveSheetIndex($sheet);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue($REPORT['ReportType']);
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Total Screens');

$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('PAPER');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('CALL CENTER');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('WEB');

$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Qualified');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Not Qualified');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Other Qualified');

$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Pending');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Certified');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('Awaiting Hours');

$objPHPExcel->getActiveSheet()->getCell('L1')->setValue('Realized Credit');

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);

$n=1;
foreach ($DATA as $RD) {
$n++;

 if ($RD['RealizedCredits'] == "") { $RD['RealizedCredits'] = 0; }
 $objPHPExcel->getActiveSheet()->getStyle('L'.$n)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
 $objPHPExcel->getActiveSheet()->getStyle('A'.$n)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
 $objPHPExcel->getActiveSheet()->getStyle('L'.$n)->getNumberFormat()->setFormatCode('#,##0.00');
 
 $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($RD['ReportType']);
 $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($RD['Screens']);

 $Paper=$RD['CMS'];
 $CallCenter=$RD['Callcenter'] + $RD['Phone'];
 $Web=$RD['Web'] + $RD['NotDefined'];

 $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($Paper);
 $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($CallCenter);
 $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($Web);

 $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($RD['Qualified']);
 $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($RD['NotQualified']);
 $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($RD['OtherQualified']);

 $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($RD['PendingwithState']);
 $objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($RD['CertifiedfromState']);
 $objPHPExcel->getActiveSheet()->getCell('K'.$n)->setValue($RD['AwaitingHoursWages']);

 $objPHPExcel->getActiveSheet()->getCell('L'.$n)->setValue($RD['RealizedCredits']);

} // end foreach

// Rename sheet
$title = $REPORT['ReportType'];
if ($REPORT['Active'] == "Y") { $title .= " Active"; } else { $title .= " Inactive"; }
$objPHPExcel->getActiveSheet()->setTitle($title);

} // end foreach report type

$objPHPExcel->setActiveSheetIndex(0);


    $file = "Roll-Up.xlsx";
    $tempfile = ROOT . "cron/temp/" . $file;
    $objWriter= IOFactory::createWriter($objPHPExcel , 'Xlsx');
    $objWriter->save($tempfile);

    $message .= "Here is your roll-up report.<br><br>";
    $message .= $report;
    $subject = 'Roll-Up Report';

    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();

    // Set who the message is to be sent to
    $PHPMailerObj->addAddress( $USER['Email'], $USER['UserID']);
    //$PHPMailerObj->addAddress( 'dedgecomb@irecruit-software.com', 'David Edgecomb');

    // Set an alternative reply-to address
    $PHPMailerObj->setFrom ( 'cmswotc@cmswotc.com', 'WOTC' );

    // Set an alternative reply-to address
    $PHPMailerObj->addReplyTo ( 'cmswotc@cmswotc.com', 'WOTC' );

    // Set the subject line
    $PHPMailerObj->Subject = $subject;

    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/html';

    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );

    $data = file_get_contents($tempfile);
    $PHPMailerObj->addStringAttachment($data, $file, 'base64', 'text/html');

    //Send email
    $PHPMailerObj->send ();

    unlink($tempfile);

} // end user email
?>
