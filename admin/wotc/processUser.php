<?php 
include '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

// PROCESSING
if (($_REQUEST['Action'] == "delete") && ($_REQUEST['UserID'] != "")) {

      $where_info  =   array("UserID = :UserID");
      $params      =   array(":UserID"=>$_REQUEST['UserID']);
      G::Obj('WOTCUsers')->delUsersInfo($where_info, array($params));

} else if (($_REQUEST['Action'] == "new" || $_REQUEST['Action'] == "edit") && ($_REQUEST['process'] == "Y") ){

    $ERROR      =   "";

    if (isset($_REQUEST['NewUserID'])) {
	    $_REQUEST['UserID'] = $_REQUEST['NewUserID'];
    }

    if ($_REQUEST['Action'] == "new") {

       $_REQUEST['UserID']= preg_replace('/[^A-Za-z0-9]/','',$_REQUEST['UserID']);
       if (strlen($_REQUEST['UserID']) < 8) {
          $ERROR .= "User name cannot be less than 8 or contain special characters." . "\\n";
       }
       $ERROR .= $ValidateObj->validate_verification($_REQUEST['pass'],"","N");

    } else {

       if ($_REQUEST['pass']) {
          $ERROR .= $ValidateObj->validate_verification($_REQUEST['pass'],"","N");
       }

    }

    if ($_REQUEST['Minutes'] < 10) {
        $ERROR .= "Minutes cannot be set less than 10." . "\\n";
    }


    if ($ERROR) {
        $MESSAGE = "<div style=\"margin-top:10px;margin-left:20px;\">";
        $MESSAGE .= "Please correct the following errors:";
        $MESSAGE .= "<div style=\"color:red;margin-left:10px;\">";
        $MESSAGE .= stripslashes(str_replace("\\n", '<br>', $ERROR));
        $MESSAGE .= "</div>";
        $MESSAGE .= "</div>";
    } else {

        $skip   =   array('UserID','CompanyID','ActivationDate');

        $info   =   array(
                    'UserID'            =>  $_REQUEST['UserID'],
                    'CompanyID'         =>  $_REQUEST['CompanyID'],
                    'Email'             =>  $_REQUEST['Email'],
                    'Minutes'           =>  $_REQUEST['Minutes'],
                    'ActivationDate'    =>  "NOW()"
                );

      	if ($_REQUEST['pass'] != "") {
	   $info['Verification']      = password_hash( $_REQUEST['pass'], PASSWORD_DEFAULT );
	}

        G::Obj('WOTCUsers')->insUpdUsersInfo($info, $skip);

	$MESSAGE = "<span style=\"color:red;\">Success</span>";

    } // end else ERROR

}
// END PROCESSING


echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">User Access</strong></div>";
echo '<a href="#openDocuments" onclick="userAccess(\'' . $_REQUEST['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="User Access" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Go Back</b></a>';
echo '</div>';


$where      =   array("UserID = :UserID");
$params     =   array(":UserID"=>$_REQUEST['UserID']);
$USERS_RES  =   G::Obj('WOTCUsers')->getUsersInfo("*", $where, 'UserID', array($params));
$USER      =   $USERS_RES['results'][0];

if ($ERROR) {
   $USER =   $_REQUEST;
}

if ($_REQUEST['Action'] == 'new') {
  $submit="Add New User";
} else {
  $submit="Edit User";
}

 echo "<div style=\"margin-top:20px;\">";
 echo "<table>";

 echo "<tr>";
 echo '<td align="right">User Name:</td>';
 echo "<td>";
 if ($_REQUEST['Action'] == "new") {
    echo "<input type=\"text\" name=\"NewUserID\" value=\"" . $_REQUEST['NewUserID'] . "\" size=\"10\" maxlength=\"45\">\n";
 } else {
    echo "<b>" . htmlspecialchars($USER['UserID']) . "</b>";
 }
 echo "</td>";
 echo "</tr>";

 echo "<tr>";
 echo '<td align="right">Password:</td>';
 echo "<td>";
 echo '<input type="password" name="pass" value="" size="20" maxlength="45"><br>';
 echo "</td>";
 echo "</tr>";

 echo "<tr>";
 echo '<td align="right">Email Address:</td>';
 echo "<td>";
 echo '<input type="text" name="Email" value="' . htmlspecialchars($USER['Email']) . '" size="40" maxlength="55">';
 echo "</td>";
 echo "</tr>";

 echo "<tr>";
 echo '<td align="right">Active Minutes:</td>';
 echo "<td>";
 echo '<input type="text" name="Minutes" value="' . htmlspecialchars($USER['Minutes']) . '" size="2" maxlength="3">';
 echo "</td>";
 echo "</tr>";

 echo "<tr><td colspan=\"2\">";
 echo "<div style=\"margin-top:20px;\">";
 echo '<input type="hidden" name="process" value="Y">';
 echo '<button onclick="processUser(\'' . $_REQUEST['CompanyID'] . '\',\'' . $_REQUEST['Action'] . '\',\'' . $USER['UserID'] . '\',\'submit\');return false;">' . $submit . '</button>';
 if ($MESSAGE != "") {
 echo "&nbsp;&nbsp;&nbsp;";
 echo $MESSAGE;
 }
 echo "</td></tr>";

 echo "</table>";
 echo "</div>";

?>
