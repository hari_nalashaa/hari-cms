<?php
include '../../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

      $DatePaid = ($_REQUEST['DatePaid'] != '') ? date("Y-m-d",strtotime($_REQUEST['DatePaid'])) : '0000-00-00';

      $set_info     =   array(
                            "InvoiceNumber  =   :invoicenumber",
                            "DatePaid       =   :datepaid",
                            "LastUpdated    =   NOW()"
                            );
      $where_info   =   array("wotcID = :wotcid", "ApplicationID = :applicationid", "Year = :year");
      $params       =   array(
                            ':invoicenumber'=>  $_REQUEST['InvoiceNumber'],
                            ':datepaid'     =>  $DatePaid,
                            ':wotcid'       =>  $_REQUEST['wotcID'],
                            ':applicationid'=>  $_REQUEST['ApplicationID'],
                            ':year'         =>  $_REQUEST['Year']
                          );
      if (($_REQUEST['InvoiceNumber'] != "") && ($_REQUEST['wotcID'] != "")
      		&& ($_REQUEST['ApplicationID'] != "") && ($_REQUEST['Year'] != "")) {
      	G::Obj('WOTCBilling')->updBillingInfo($set_info, $where_info, array($params));
      } // end if

?>
