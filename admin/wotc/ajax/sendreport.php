<?php
include '../../Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

$columns = '*';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['companyid']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];

$C=get_object_vars(json_decode($CRM['Contacts']));
$Contact=$C['Contact'.$_REQUEST['contact']];
$Email=$C['Email'.$_REQUEST['contact']];

if ($_REQUEST['process'] == "Y") {

	$info   =   array(
		 "ConfigID"     =>  "WOTCStatus",
                 "Subject"      =>  $_REQUEST['Subject'],
                 "Message"      =>  $_REQUEST['Message'],
                 "FromEmail"    =>  $_REQUEST['FromEmail'],
                 "FromName"     =>  $_REQUEST['FromName']
                 );

        $skip   =   array("ConfigID");

        if (($_REQUEST['Subject'] != "") && ($_REQUEST['Message'] != "")) {
          G::Obj('WOTCcrm')->insUpdWotcEmailConfig($info, $skip);
        }

	// read after insert to get latest config
	$columns = '*';
	$where = array("ConfigID = :ConfigID");
	$params = array(":ConfigID"=>"WOTCStatus");
	$results2 = G::Obj('WOTCcrm')->getWotcEmailConfig($columns, $where, '', '', array($params));
	$EMC = $results2['results'][0];

	$report = "";

	$subject        =   $EMC['Subject'];
	if ($_REQUEST['REPORT'] == "SR") {

		if ($CRM['Assignment'] == "Business Partners") {

        	   $columns = 'CompanyID, CompanyName';
        	   $where = array("Partner = :Partner","Active = 'Y'");
        	   $params = array(":Partner"=>$CRM['CompanyID']);
        	   $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
        	   $DATA = $RESULTS['results'];

        	   foreach($DATA AS $P) {

	  	   $report .= G::Obj('WOTCcrm')->getReport($P,'');
        	   $report .= '<br><br>';

        	   } // end foreach

		} else {
	  	   $report = G::Obj('WOTCcrm')->getReport($CRM,'');
		}

	} elseif ($_REQUEST['REPORT'] == "RCD") {

		if ($CRM['Assignment'] == "Business Partners") {

        	   $columns = 'CompanyID, CompanyName';
        	   $where = array("Partner = :Partner","Active = 'Y'");
        	   $params = array(":Partner"=>$CRM['CompanyID']);
        	   $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
        	   $DATA = $RESULTS['results'];

        	   foreach($DATA AS $P) {

	  	   $report .= G::Obj('WOTCcrm')->getBillingDataReport($P);
        	   $report .= '<br><br>';

        	   } // end foreach

		} else {
	  	   $report = G::Obj('WOTCcrm')->getBillingDataReport($CRM);
		}


	} elseif ($_REQUEST['REPORT'] == "APD") {

		if ($_REQUEST['APD-Attachment'] == "Y") {

			$objPHPExcel = new Spreadsheet();
			$objPHPExcel->getProperties()->setCreator("CMS WOTC")
			       ->setLastModifiedBy("System")
			       ->setTitle("WOTC Awaiting Payroll Data")
			       ->setSubject("Excel")
			       ->setDescription("Awaiting Payroll Data Export")
			       ->setKeywords("phpExcel")
			       ->setCategory("Output");

			// Create a first sheet, representing sales data
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setTitle("Awaiting Payroll Data");

			$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setRGB('0c00ac');
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
			$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setVertical('center');

			$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Employer');
			$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
			$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('ApplicantName');
			$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('SSN');
			$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Hired Date');
			$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Confirmed');
			$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Termed Date ');
			$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Gross Hours to Date');
			$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Gross Wages to Date');

			for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
			    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
			}
			    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);

		} // end APS-Attachment

		if ($CRM['Assignment'] == "Business Partners") {

        	   $columns = 'CompanyID, CompanyName';
        	   $where = array("Partner = :Partner","Active = 'Y'");
        	   $params = array(":Partner"=>$CRM['CompanyID']);
        	   $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
        	   $DATA = $RESULTS['results'];

		   $n=1;
        	   foreach($DATA AS $P) {

	  	      $report .= G::Obj('WOTCcrm')->getAwaitingPayrollDataReport($P);
        	      $report .= '<br><br>';

		      if ($_REQUEST['APD-Attachment'] == "Y") {

			  $PAYROLLDATA = G::Obj('WOTCcrm')->getAwaitingPayrollData($P['CompanyID']);

                	  foreach ($PAYROLLDATA AS $PD) {
			    $n++;

		            $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getNumberFormat()->setFormatCode("#,##0");
		            $objPHPExcel->getActiveSheet()->getStyle('G'.$n)->getNumberFormat()->setFormatCode("#,##0.00");
			    $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($PD['EIN'] . ', ' . $PD['AKADBA']);
			    $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($PD['ApplicationID']);
			    $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($PD['ApplicantName']);
			    $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($PD['SSN']);
			    $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($PD['HiredDate']);
			    $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($PD['Confirmed']);
			    $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue('');
			    $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue('');
			    $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue('');

			  } // end foreach PAYROLLDATA

		      } // end APS-Attachment

        	   } // end foreach

		} else {
	  	   $report = G::Obj('WOTCcrm')->getAwaitingPayrollDataReport($CRM);

		   if ($_REQUEST['APD-Attachment'] == "Y") {

			  $PAYROLLDATA = G::Obj('WOTCcrm')->getAwaitingPayrollData($CRM['CompanyID']);

			  $n=1;
                	  foreach ($PAYROLLDATA AS $PD) {
			    $n++;

		            $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getNumberFormat()->setFormatCode("#,##0");
		            $objPHPExcel->getActiveSheet()->getStyle('G'.$n)->getNumberFormat()->setFormatCode("#,##0.00");
			    $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($PD['EIN'] . ', ' . $PD['AKADBA']);
			    $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($PD['ApplicationID']);
			    $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($PD['ApplicantName']);
			    $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($PD['SSN']);
			    $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($PD['HiredDate']);
			    $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($PD['Confirmed']);
			    $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue('');
			    $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue('');
			    $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue('');

			  } // end foreach PAYROLLDATA

		   } // end APS-Attachment

		}
	}

	$style = '<style>';
	$style .= 'body {';
	$style .= '  font-family: Arial, Helvetica, sans-serif;';
	$style .= '  font-size: 14px;';
	$style .= '}';
	$style .= '</style>';

	$message = $style;
	$message .= '<body>';
	$message .= '<div style="margin-bottom:20px;">';

	$EMC['Message']    =   preg_replace('/\r/', "<br>\r\n", $EMC['Message']);
	$EMC['Message']    =   preg_replace('/{Contact}/', $Contact, $EMC['Message']);
	$EMC['Message']    =   preg_replace('/{Report}/', $report, $EMC['Message']);

	$message .= $EMC['Message'];

	$message .= "</div>";
	$message .= '</body>';


	if (($Email != "") && ($Contact != "") && ($EMC['FromEmail'] != "") && ($EMC['FromName'] != "")) {

	G::Obj('PHPMailer')->clearCustomProperties();
	G::Obj('PHPMailer')->clearCustomHeaders();
	G::Obj('PHPMailer')->setFrom ($EMC['FromEmail'], $EMC['FromName']);
	G::Obj('PHPMailer')->addAddress ($Email,$Contact);
	G::Obj('PHPMailer')->Subject = $subject;
	G::Obj('PHPMailer')->msgHTML ( $message );
	G::Obj('PHPMailer')->ContentType = 'text/html';

	if ($_REQUEST['APD-Attachment'] == "Y") {

	  $tempfile=ROOT.'cron/temp/tmpreport.xlsx';
	  $objWriter = IOFactory::createWriter($objPHPExcel , 'Xlsx');
	  $objWriter->save($tempfile);

	  $data = file_get_contents($tempfile);
	  G::Obj('PHPMailer')->addStringAttachment($data, 'WOTC-Hours_Wages.xlsx', 'base64', 'text/html');
	  unlink($tempfile);

	} // end APD-Attachment

	G::Obj('PHPMailer')->send ();

	echo $style;
        echo '<br><strong>Sent From:</strong> ' . $EMC['FromName'] . ' - ' . $EMC['FromEmail'] . "<br><br>";
        echo '<strong>Sent To:</strong> ' . $Contact . ' - ' . $Email . "<br><br>";
        echo '<strong>Subject:</strong> ' . $subject . "<br><br>";
        echo $message;

        $infohistory   =   array(
                 "HistoryID"    =>  uniqid() . rand(1, 1),
                 "CompanyID"    =>  $_REQUEST['companyid'],
                 "Date"         =>  'NOW()',
                 "UserID"       =>  $AUTH['UserID'],
                 "Notes"        =>  'Report sent to ' . $Email . ' - ' . $Contact
                 );

        $skiphistory   =   array("HistoryID");

        G::Obj('WOTCcrm')->insUpdCRMHistory($infohistory, $skiphistory);

	} else {

	echo $style;
        echo '<br>Email failed to send.<br>';

	}

}  else { // end process

$columns = '*';
$where = array("ConfigID = :ConfigID");
$params = array(":ConfigID"=>"WOTCStatus");
$results2 = G::Obj('WOTCcrm')->getWotcEmailConfig($columns, $where, '', '', array($params));
$EMC = $results2['results'][0];

echo '<div style="margin: 10px;line-height:250%;">';
echo "<b>To:</b> &#60;" . $Email . "&#62;&nbsp;" . $Contact . "<br>";
echo '<b>From:</b> ';
echo '<input type="text" name="FromEmail" value="'.$EMC['FromEmail'].'" size="30" maxlength="65">';
echo '&nbsp;&nbsp;<b>Name:</b> ';
echo '<input type="text" name="FromName" value="'.$EMC['FromName'].'" size="15" maxlength="45"><br>';
echo '<b>Subject:</b> ';
echo '<input type="text" name="Subject" value="'.$EMC['Subject'].'" size="58" maxlength="65"><br>';
echo '<b>Message:</b> {Contact}, {Report}<br>';
echo '<textarea name="Message" cols="115" rows="10">';
echo $EMC['Message'];
echo '</textarea><br>';
echo "<b>Report:</b><br>";
echo '<input type="radio" name="REPORT" value="SR" checked> Status Report<br>';
echo '<input type="radio" name="REPORT" value="RCD"> Realized Credit Details<br>';
echo '<input type="radio" name="REPORT" value="APD"> Awaiting Payroll Data';
if ($CRM['Assignment'] != "Business Partners") {
echo '&nbsp;&nbsp;&nbsp;';
echo '<input type="checkbox" name="APD-Attachment" value="Y"> Include Excel Attachment';
} // end if
echo '<br>';
echo '<br><button onclick="loadWOTCSendReport(\''.$_REQUEST['companyid'].'\',\''.$_REQUEST['contact'].'\',\'Y\');return false;">Submit</button>';
echo '</div>';

} // end else

?>
