<?php
include '../../Configuration.inc';

echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">Update Note</strong></div>";
echo '<a href="#openDocuments" onclick=loadHistory(\'' . $_POST['CompanyID'] . '\');><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="User Access" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Go Back</b></a>';
echo '</div>';

if ($_POST['process'] == "Y") {

	$infohistory   =   array(
                 "HistoryID"    =>  $_POST['HistoryID'],
                 "CompanyID"    =>  $_POST['CompanyID'],
                 "Date"         =>  'NOW()',
                 "UserID"       =>  $AUTH['UserID'],
                 "Notes"        =>  $_POST['Notes']
                 );

        $skiphistory   =   array("HistoryID","CompanyID");

        if ($_REQUEST['Notes'] != "") {
          G::Obj('WOTCcrm')->insUpdCRMHistory($infohistory, $skiphistory);
        }

	$message = '<span style="color:red;">Note successfully updated.</span>';

}

$where = array("CompanyID = :CompanyID","HistoryID = :HistoryID");
$params = array(":CompanyID"=>$_POST['CompanyID'],":HistoryID"=>$_POST['HistoryID']);
$HISTORY = G::Obj('WOTCcrm')->getCRMHistory('Notes', $where, '', '', array($params));

echo '<div style="margin:20px 0;">';
echo '<textarea name="Notes" cols="110" rows="5">';
echo $HISTORY[0]['Notes'];
echo '</textarea>';
echo '</div>';

echo '<div>';
echo '<input type="hidden" name="process" value="Y">';
echo '<button onclick="updateCRMNote(\'' . $_POST['CompanyID'] . '\',\'' . $_POST['HistoryID'] . '\',\'submit\');return false;">Update Note</button>';

echo '&nbsp;&nbsp;&nbsp;';
echo '&nbsp;&nbsp;&nbsp;';
echo $message;

echo '</div>';

?>
