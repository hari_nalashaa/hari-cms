<?php
include '../../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

if ($_POST['editrow']) {
    $where_info     =   array("rowid = :editrow", "wotcID = :wotcid", "ApplicationID = :applicationid");
    $params_info    =   array(
                            ':editrow'          =>  $_POST['editrow'],
                            ':wotcid'           =>  $_POST['wotcID'],
                            ':applicationid'    =>  $_POST['ApplicationID']
                        );
    $WOTCHoursObj->delHoursInfo($where_info, array($params_info));
}
?>
