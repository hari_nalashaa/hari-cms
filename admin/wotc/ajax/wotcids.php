<?php
include '../../Configuration.inc';

$AUTH   =   $ADMINObj->authenticate($_COOKIE['AID']);

if ($_REQUEST['PartnerID'] != "" && $_REQUEST['ParentID'] == "") {

   list ($PARTNERLIST,$PARENTLIST) = G::Obj('WOTCAdmin')->getPartnerParentList($_REQUEST['PartnerID']);

   $WOTCIDS=array();
   foreach ($PARENTLIST AS $P) {
      foreach (G::Obj('WOTCcrm')->getWOTCIDs($P['CompanyID']) AS $wotcID) {
         $WOTCIDS[]=$wotcID;
      }
   }

} else if ($_REQUEST['ParentID'] != "") {

   $WOTCIDS = G::Obj('WOTCcrm')->getWOTCIDs($_REQUEST['ParentID']);

} else {

   $WOTCIDS = G::Obj('WOTCcrm')->getWOTCIDs('');

}

echo "wotcID's: <select style=\"width:400px;\" id=\"wotcID\" name=\"wotcID\" onChange=\"loadwotcIDs('" . $_REQUEST['Partner'] . "','" . $_REQUEST['ParentID'] . "',this.value);\">\n";
echo '<option value="">ALL</option>';

if (count($WOTCIDS) > 0) {

  foreach ($WOTCIDS AS $wotcID) {

	$AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($wotcID);

        echo '<option value="' . $wotcID . '"';
        if ($_REQUEST['wotcID'] == $wotcID) {
            echo ' selected';
        }
	$desc = $AKADBA['State'];
	$desc .= ' (' . $AKADBA['EIN1'] . '-' . $AKADBA['EIN2'] . ')';
	$desc .= ' ' . $AKADBA['AKADBA'];
	$desc .= ', ' . $AKADBA['Address'] . ', ' . $AKADBA['City'] . ' ' . $AKADBA['ZipCode'];
        $desc .= ' (' . $wotcID . ')';
        echo '>' . $desc . '</option>' . "\n";
  } // end foreach

} // end count

echo "</select>\n";

if ($_REQUEST['wotcID'] != "") {
    echo "&nbsp;&nbsp;";
    echo "<a href=\"" . WOTC_HOME . "admin.php?wotcID=" . $_REQUEST['wotcID'] . "&Initiated=cms\" target=\"_blank\">";
    echo "Add New</a>\n";
}

?>
