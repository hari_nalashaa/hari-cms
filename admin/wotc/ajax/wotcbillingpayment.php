<?php
include '../../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

echo '<div style="text-align:left;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;margin-bottom:20px;">';
echo "<strong style=\"font-size:14pt;\">Billing/Payment</strong>";
echo '</div>';

$NEEDSDATA = G::Obj('WOTCcrm')->getNeedsBillingData();

echo '<div>';
echo '<select onchange="loadBillingPayment(this.value);">';
echo '<option value="">Please Select</option>';
foreach ($NEEDSDATA AS $ND) {

	echo '<option value="' . $ND['CompanyID'] . '"';
	if ($ND['CompanyID'] == $_REQUEST['CompanyID']) {
		echo ' selected';
	}
	echo '>' . $ND['CompanyName'] . '&nbsp;&nbsp;&nbsp;<i>(' . $ND['CNT'] . ')</i></option>';

} // end foreach
echo '</select>';
echo '</div>';

$BILLINGDATA = G::Obj('WOTCcrm')->getBillingPayment($_REQUEST['CompanyID']);

echo '</form>';

echo '<div style="min-height:200px;margin:10px 0 40px 20px;">';
foreach ($BILLINGDATA AS $BD) {

	echo '<div style="float:left;line-height:200%;padding:10px 0;">';
	echo 'Company: ';
	echo '<strong>' . $BD['EIN'] . '</strong>';
	echo '&nbsp;&#149;&nbsp;';
	echo '<strong>' . $BD['AKADBA'] . '</strong>';
	if ($BD['POState'] != "") {
	  echo '&nbsp;&#149;&nbsp;';
	  echo 'Filing State: <strong>' . $BD['POState'] . '</strong>';
	}
	echo '<br>';

	echo 'Applicant: ';
	echo '<strong>' . $BD['ApplicationID'] . '</strong>';
	echo '&nbsp;&#149;&nbsp;';
	echo '<strong>' . $BD['FullName'] . '</strong>';
	echo '&nbsp;&#149;&nbsp;';
	echo '<strong>' . $BD['SSN'] . '</strong>';
	echo '</strong>';
	echo '<br>';

	echo 'Earned Credit: ';
	echo '<strong>$' . number_format($BD['TaxCredit'], 2) . '</strong>';
	echo '&nbsp;on&nbsp;';
        echo '<strong>' . $BD['DateBilled'] . '</strong>';
	echo '&nbsp;&#149;&nbsp;';
	echo 'Reason: <strong>' . $BD['Reason'] . '</strong>';
	if ($BD['Termed'] != "00/00/0000") {
	  echo '&nbsp;on&nbsp;';
	  echo '<strong>' . $BD['Termed'] . '</strong>';
	}
	echo '</div>';

	echo '<div style="float:right;text-align:right;padding:10px 0;line-height:350%;">';
	echo '<form id="UpdatePayment' . $BD['wotcID'] . $BD['ApplicationID'] . $BD['Year'] . '">';
	echo 'Invoice Number: ';
        echo '<input type="text" name="InvoiceNumber" value = "'	. $BD['InvoiceNumber'] . '" size="10">';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo 'Date Paid: ';
        echo '<input type="text" id="DatePaid" name="DatePaid" class="input-date" value = "' . ($BD['DatePaid'] != '00/00/0000' ? $BD['DatePaid'] : date("m/d/Y"))  . '" size="10">';
	echo '<br>';
	echo '<input type="hidden" name="wotcID" value="' . $BD['wotcID'] . '">';
	echo '<input type="hidden" name="ApplicationID" value="' . $BD['ApplicationID'] . '">';
	echo '<input type="hidden" name="Year" value="' . $BD['Year'] . '">';
	echo '<input type="button" name="submit" value="Update Payment" onclick="processPayment(\'' . $BD['wotcID'] . $BD['ApplicationID'] . $BD['Year'] . '\',\'' .  $_REQUEST['CompanyID'] . '\');">';
	echo '</form>';
	echo '</div>';

	echo '<div style="clear:both;border-bottom:.5px solid #000;"></div>';


} // end foreach
echo '</div>';

echo '<form>';
?>
