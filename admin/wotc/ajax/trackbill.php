<?php
include '../../Configuration.inc';

$AUTH   =   $ADMINObj->authenticate($_COOKIE['AID']);

$rtn        =   '';

//$rtn .= '<hr style="border: 0; background-color: #fff; border-top: .5px dashed #000000;">';
$rtn .= "<a href=\"#openProcess" . $_POST['wotcID'] . $_POST['ApplicationID'] . "\" onclick=\"loadTrackBillForm('process','" . $_POST['wotcID'] . "','" . $_POST['ApplicationID'] . "','','');\">Process</a>\n";

$year   =   0;
$year   =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);
$year ++;

list ($value, $active, $comment) = G::Obj('WOTCCalculator')->calculate_wotcCredit($_POST['wotcID'], $_POST['ApplicationID'], $year);

$maxyear    =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);

$columns    =   "rowid, Year, Hours, Wages, if(ASOF = '0000-00-00','',ASOF) ASOF, Comments";
$where      =   array("wotcID = :wotcid", "ApplicationID = :applicationid");
$params     =   array(':wotcid'=>$_POST['wotcID'], ':applicationid'=>$_POST['ApplicationID']);
$HOURS_RES  =   G::Obj('WOTCHours')->getHoursInfo($columns, $where, '', 'Year, ASOF', array($params));
$HOURS      =   $HOURS_RES['results'];
$hrscnt     =   sizeof($HOURS);

if ($active == "Y") {

    $rtn .= '&nbsp;&nbsp;&nbsp;';
    $rtn .= '&#124;';
    $rtn .= '&nbsp;&nbsp;&nbsp;';
    $rtn .= '<a href="#openTrack' . $_POST['wotcID'] . $_POST['ApplicationID'] . '" onclick="loadTrackBillForm(\'track\',\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'\',\'\')">Add Hours</a>';

    if ($hrscnt > 0) {
    $rtn .= '&nbsp;&nbsp;&nbsp;';
    $rtn .= '&#124;';
    $rtn .= '&nbsp;&nbsp;&nbsp;';
    $rtn .= '<a href="#openBill' . $_POST['wotcID'] . $_POST['ApplicationID'] . '" onclick="loadTrackBillForm(\'bill\',\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'\',\'\')" style="font-size:9pt;">Bill/Term</a>';
    } // end hrscnt

} // end active = Y

$rtn .= '<br>';

$i          =   0;
$year       =   1;

if ($hrscnt > 0) {
$rtn .= '<table border="0" cellspacing="6" cellpadding="0">';
$rtn .= '<tr><td valign="top" width="40"></td><td></td>';
$rtn .= '<td valign="top" align="right"><u>Hours</u>:</td>';
$rtn .= '<td valign="top" align="right"><u>Wages</u>:</td>';
$rtn .= '<td valign="top" width="80"><u>AS OF</u>:</td>';
$rtn .= '<td valign="top"><u>Comment</u>:</td></tr>';

foreach ($HOURS as $HRS) {
    $i ++;

    if ($year != $HRS['Year']) {

        $THOURS  =   G::Obj('WOTCHours')->hoursTotal($_POST['wotcID'], $_POST['ApplicationID'], $year);

        foreach ($THOURS as $TTHRS) {
            $rtn .= '<tr><td valign="top" align="right" colspan="2" style="border-top:4px double #000;">Total Year ' . $year . '</td>';
            $rtn .= '<td valign="top" align="right" style="border-top:4px double #000;">' . number_format($TTHRS['Hours']) . '&nbsp;</td>';
            $rtn .= '<td valign="top" align="right" style="border-top:4px double #000;">$ ' . number_format($TTHRS['Wages'], 2, ".", ",") . '&nbsp;</td>';
            $rtn .= '<td valign="top" style="border-top:4px double #000;">' . $TTHRS['ASOF'] . '</td>';
            $rtn .= '<td valign="top">&nbsp;</td></tr>';
        }
        
        list ($value, $active, $comment) = G::Obj('WOTCCalculator')->calculate_wotcCredit($_POST['wotcID'], $_POST['ApplicationID'], $year);
        
        if ($value > 0) {
            $rtn .= '<tr><td valign="top" align="right" colspan="3">Estimated Value:</td>';
            $rtn .= '<td valign="top" align="right">$&nbsp;' . number_format($value, 2, ".", ",") . '&nbsp;</td>';
            $rtn .= '<td colspan="2">' . $comment . '</td></tr>';
        }
        
        $rtn .= '<tr><td valign="top" colspan="100%" height="10"></td></tr>';
        
    } // end year != year

    $rtn .= '<tr><td>';

    if ($HRS['Year'] > $maxyear) {
        $rtn .= '<a href="#openTrack' . $_POST['wotcID'] . $_POST['ApplicationID'] . '" onclick="loadTrackBillForm(\'track\',\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'E\',\'' . $HRS['rowid'] . '\')" style="font-size:9pt;">edit</a>';

        $rtn .= '&nbsp;&nbsp;';
        $rtn .= '<a href="#" onclick="deleteHours(\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'' . $HRS['rowid'] . '\',\'' . $HRS['Hours'] . '\',\'' . $HRS['Wages'] . '\',\'' . $HRS['ASOF'] . ' \')" style="font-size:9pt;">delete</a>';
    }

    $rtn .= '</td>';
    $rtn .= '<td align="center"></td>';

    $rtn .= '<td valign="top" align="right">' . number_format($HRS['Hours']) . '&nbsp;</td>';
    $rtn .= '<td valign="top" align="right">' . number_format($HRS['Wages'], 2, ".", ",") . '&nbsp;</td>';
    $rtn .= '<td valign="top">' . $HRS['ASOF'] . '</td>';
    $rtn .= '<td valign="top">' . $HRS['Comments'] . '</td></tr>';

    if ($i == $hrscnt) {
        
        $HOURS  =   G::Obj('WOTCHours')->hoursTotal($_POST['wotcID'], $_POST['ApplicationID'], $HRS['Year']);

        foreach ($HOURS as $TTHRS) {
            $rtn .= '<tr><td valign="top" align="right" colspan="2" style="border-top:4px double #000;">Total Year ' . $HRS['Year'] . '</td>';
            $rtn .= '<td valign="top" align="right" style="border-top:4px double #000;">' . number_format($TTHRS['Hours']) . '&nbsp;</td>';
            $rtn .= '<td valign="top" align="right" style="border-top:4px double #000;">$ ' . number_format($TTHRS['Wages'], 2, ".", ",") . '&nbsp;</td>';
            $rtn .= '<td valign="top" style="border-top:4px double #000;">' . $TTHRS['ASOF'] . '</td>';
            $rtn .= '<td valign="top">&nbsp;</td></tr>';
        }
        
        list ($value, $active, $comment) = G::Obj('WOTCCalculator')->calculate_wotcCredit($_POST['wotcID'], $_POST['ApplicationID'], $HRS['Year']);
        
        if ($value > 0) {
            $rtn .= '<tr><td valign="top" align="right" colspan="3">Estimated Value:</td>';
            $rtn .= '<td valign="top" align="right">$&nbsp;' . number_format($value, 2, ".", ",") . '&nbsp;</td>';
            $rtn .= '<td colspan="2">' . $comment . '</td></tr>';
        }
        
    }

    $year = $HRS['Year'];
} // end foreach

$rtn .= '</table>';
} // end hrscnt


  if (G::Obj('WOTCBilling')->checkBilling($_POST['wotcID'], $_POST['ApplicationID']) > 0) {
    
    $BILLING    =   G::Obj('WOTCBilling')->getBillingInfoByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);
    
    if (sizeof($BILLING) > 0 ) {
    //$rtn .= '<hr style="border: 0; background-color: #fff; border-top: .5px dashed #000000;">';
    $rtn .= "Billing";
    } // end size of BILLING

    foreach ($BILLING as $BILL) {
        $rtn .= '<table border="0" cellspacing="0" cellpadding="10"><tr>';
        $rtn .= '<td valign="top" width="60">';
    if (sizeof($BILLING) == $BILL['Year']) {
          $rtn .= '<a href="#openBill' . $_POST['wotcID'] . $_POST['ApplicationID'] . '" onclick="loadTrackBillForm(\'bill\',\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'E\',\'' . $BILL['Year'] . '\')" style="font-size:9pt;">edit</a>';
          $rtn .= '&nbsp;&nbsp;';
          $rtn .= '<a href="#" onclick="deleteBilling(\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'' . $BILL['Year'] . '\')" style="font-size:9pt;">delete</a>';
        }
	$rtn .= '</td>';
        $rtn .= '<td valign="top"><u>Termed</u><br>' . $BILL['Termed'] . '</td>';
        $rtn .= '<td valign="top" align="center"><u>Year</u><br>' . $BILL['Year'] . '</td>';
        $rtn .= '<td valign="top"><u>Reason</u><br>' . $BILL['Reason'] . '</td>';
        $rtn .= '<td valign="top" align="right"><u>Tax Credit</u><br>$ ' . number_format($BILL['TaxCredit'], 2, ".", ",") . '&nbsp;</td>';
        $rtn .= '<td valign="top"><u>Comment</u><br>' . $BILL['Comments'] . '</td>';
        $rtn .= '</tr></table>';
    
        if ($BILL['DateBilled']) {
            $rtn .= '<table border="0" cellspacing="0" cellpadding="10"><tr>';
            $rtn .= '<td valign="top" align="center" width="60"></td>';
            $rtn .= '<td valign="top"><u>Date Billed</u><br>' . $BILL['DateBilled'] . '</td>';
            $rtn .= '<td valign="top"><u>Invoice No</u><br>' . $BILL['InvoiceNumber'] . '</td>';
            $rtn .= '<td valign="top"><u>Date Paid</u><br>' . $BILL['DatePaid'] . '</td>';
            $rtn .= '</tr></table>';
            $rtn .= '<br>';
        }
    } // end foreach
    
  } // end Billing info

    echo $rtn;
?>
