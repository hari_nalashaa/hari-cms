<?php
include '../../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

if ($_POST['type'] == "track") {

    include ADMIN_DIR . 'wotc/Track.inc';

} else if ($_POST['type'] == "bill") {

    include ADMIN_DIR . 'wotc/Bill.inc';

} else if ($_POST['type'] == "process") {

    include ADMIN_DIR . 'wotc/Process.inc';

} else if ($_POST['type'] == "edit") {

    include ADMIN_DIR . 'wotc/Edit.inc';

} else if ($_POST['type'] == "term") {

    include ADMIN_DIR . 'wotc/Term.inc';

}
?>
