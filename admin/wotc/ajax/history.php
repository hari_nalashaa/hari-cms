<?php
include '../../Configuration.inc';

$AUTH   =   $ADMINObj->authenticate($_COOKIE['AID']);

echo '<div style="text-align:left;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;margin-bottom:20px;">';
echo "<strong style=\"font-size:14pt;\">Note History</strong>";
echo '</div>';

$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$HISTORY = G::Obj('WOTCcrm')->getCRMHistory('HistoryID, UserID, DATE_FORMAT(Date,"%m/%d/%Y at %I:%i %p") AS PrintDate, Notes', $where, '', 'Date DESC', array($params));

if (count($HISTORY) > 0) {
foreach ($HISTORY as $H) {
    echo '<div style="margin-bottom:20px;border-bottom:.5px solid #000000;">';

    if (($H['UserID'] != "System") && ($H['UserID'] != "API") && ($H['Notes'] != "Company Added.")) {
    echo '<div style="float:right;">';
    echo '<a href="#openDocuments" onclick="updateCRMNote(\'' . $_REQUEST['CompanyID']  . '\',\'' . $H['HistoryID'] . '\',\'\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Edit" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Edit</b></a>';
    echo '&nbsp;&nbsp;&nbsp;&nbsp;';
    echo '<a href="#openDocuments" onclick="deleteCRMNote(\'' . $H['HistoryID'] . '\',\'' . $_REQUEST['CompanyID'] . '\',\'' . $H['PrintDate'] . ' - ' . preg_replace('/[\'"]/','',substr(preg_replace('/\n/', ' ',$H['Notes']),0,30)) . '...' . '\')"><img src="'.IRECRUIT_HOME.'images/icons/cross.png" title="Delete" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Delete</b></a>';
    echo '</div>';
    }

    echo '<div style="margin:0 20px 10px 0;line-height:180%;">';
    echo preg_replace('/\n/', '<br>',$H['Notes']);
    echo '<br> ~ ' . $H['PrintDate'] . ", by: " . $H['UserID'];
    echo '</div>';

    echo '</div>';
} // end foreach

}

?>
