<?php
include '../../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

$columns = 'CompanyID, CompanyName, Assignment';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results2 = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results2['results'][0];

if ($CRM['Assignment'] == "Business Partners") {

	$columns = 'CompanyID, CompanyName';
	$where = array("Partner = :Partner","Active = 'Y'");
	$params = array(":Partner"=>$CRM['CompanyID']);
	$RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
	$DATA = $RESULTS['results'];

	foreach($DATA AS $P) {

   	   echo G::Obj('WOTCcrm')->getReport($P,'Y');
	   echo '<br><br>';

	} // end foreach

} else {
   echo G::Obj('WOTCcrm')->getReport($CRM,'Y');
}

?>
