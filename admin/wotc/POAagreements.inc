<div style="font-size:12pt;font-weight:bold;margin-bottom:20px;"><a href="index.php?pg=create_agreement">Create Agreement / POA</a></div>
<?php
$columns      =   "*";
$AGMTDataResults  =   G::Obj('WOTCAgreements')->getAllAgreementsData($columns);
$AGMTDATA         =   $AGMTDataResults['results'];
if($_REQUEST["update"]=="Success"){ ?>
	<div style="color:red;margin:20px 20px;">Update Complete</div>
<?php } ?>
<table>
	<thead>
    	<tr>
    		<th style="min-width:120px;">Organization</th>
    		<th style="min-width:120px;">Agreement</th>
    		<th style="min-width:120px;">Service Agreement</th>
    		<th style="min-width:120px;">POA</th>
    		<th style="min-width:120px;">Action</th>
    	</tr>
	</thead>
	<tbody>
		<?php
		foreach ($AGMTDATA as $AGMT) { ?>
        	<tr>
        		<td><?php echo $AGMT['OrganizationName']; ?></td>
        		<td><?php echo date('m/d/Y',strtotime($AGMT['AgmtStartDate'])); ?></td>
        		<td>
        			<a style="text-decoration:none;font-weight:normal;color:#000;" href="<?php echo ADMIN_HOME; ?>wotc/newAgreementForm.php?AgreementID=<?php echo $AGMT['AgreementID']; ?>">
        				<img src="<?php echo ADMIN_HOME; ?>images/page_white_acrobat.png" border="0" title="View PDF Form" style="margin: 0px 3px -4px 0px;">View PDF Form
        			</a>
        		</td>
        		<td>
        			<a style="text-decoration:none;font-weight:normal;color:#000;" href="<?php echo ADMIN_HOME; ?>wotc/newPOAForm.php?AgreementID=<?php echo $AGMT['AgreementID']; ?>">
        				<img src="<?php echo ADMIN_HOME; ?>images/page_white_acrobat.png" border="0" title="View PDF Form" style="margin: 0px 3px -4px 0px;">View PDF Form
        			</a>
        		</td>
        		<td>
        			<a href="index.php?pg=create_agreement&AgreementID=<?php echo $AGMT['AgreementID']; ?>">
        				<img src= "<?php echo ADMIN_HOME; ?>images/pencil.png" border="0" title="Edit" style="margin: 0px 3px -4px 0px;">
        			</a>
        			<img class="delete_agreement" id="delete_agreement_<?php echo $AGMT['AgreementID']; ?>" src="<?php echo ADMIN_HOME; ?>images/cross.png" border="0" title="Delete" style="margin: 0px 3px -4px 0px;">
        		</td>
        	</tr>
        <?php }// end foreach ?>
	</tbody>
</table>

<script type="text/javascript">
	$(".delete_agreement").click(function(){
		var get_delete_agmt_id = $(this).attr('id');
		var agmt_arr  = get_delete_agmt_id.split("_");
		var AgreementId      = agmt_arr[2];
		var result = confirm("Do you want to delete?");
        if (result) {
            $.ajax({
    		type: "POST",
    		url: "wotc/deleteAgreement.php",
    		data:'AgreementId='+ AgreementId,
    		success: function(data){
    			location.reload();
    		}
		});
        }
	});
</script>
