<?php
if (($_POST['ApplicationID']) && ($_POST['wotcID'])) {

// get WOTC OrgData
$columns                =   "WotcFormID";
$where                  =   array("wotcID = :wotcid");
$params                 =   array($_POST['wotcID']);
$ORGS_RES               =   G::Obj('WOTCOrganization')->getOrgDataInfo($columns, $where, '', '', array($params));
$ORGS                   =   $ORGS_RES['results'];

    $AD         =   G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $_POST['wotcID'], $ORGS[0]['WotcFormID'], $_POST['ApplicationID']);
    $year       =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);
    $year++;

    $columns    =   "SUM(Hours) Hours, SUM(Wages) Wages, MAX(ASOF) ASOF, GROUP_CONCAT(Comments) Comments";
    $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID", "Year = :Year");
    $params     =   array(':wotcID'=>$_POST['wotcID'], ':ApplicationID'=>$_POST['ApplicationID'], ':Year'=>$year);
    $TTHRS_RES  =   G::Obj('WOTCHours')->getHoursInfo($columns, $where_info, 'wotcID, ApplicationID', '', array($params));
    $TTHRS      =   $TTHRS_RES['results'][0];

    $PROC       =   G::Obj('WOTCProcessing')->getProcessingInfo("StartDate, now() AS CurrentDate", $_POST['wotcID'], $_POST['ApplicationID']);
    

    if ($_POST['editrow']) {
        $columns    =   "Hours, Wages, if(ASOF= '0000-00-00','',date_format(ASOF,'%m/%d/%Y')) ASOF, Comments";
        $where_info =   array("rowid = :editrow");
        $params     =   array(':editrow'=>$_POST['editrow']);
        $HRS_RES    =   G::Obj('WOTCHours')->getHoursInfo($columns, $where_info, '', '', array($params));
        $HRS        =   $HRS_RES['results'][0];
    } else {
        $HRS        =   array();
    }


    if ($_POST['process'] == "P") {

        if ($_POST['asof'] == "") { $_POST['asof']="0000-00-00"; }
        
        $ERROR  =   array();
        
        if (!is_numeric($_POST['hours'])) {
            $ERROR['hours']   =   'Hours are missing.';
        }
        
        if (!is_numeric($_POST['wages'])) {
            $ERROR['wages']   =   'Wages are missing.';
        }
        
        if ($_POST['asof'] == "") {
            $ERROR['asof']    =   'AS OF data is missing.';
        }
        
        if (sizeof($ERROR) == 0) {
        
            if ($_POST['editrow'] ) {

                $set_info     =   array("Hours = :Hours", "Wages = :Wages", "ASOF = :ASOF", "Comments = :Comments", "LastUpdated = NOW()");
                $params       =   array(":Hours"=>$_POST['hours'], ":Wages"=>$_POST['wages'], ":ASOF"=>date("Y-m-d",strtotime($_POST['asof'])), ":Comments"=>$_POST['comment'], ":rowid"=>$_POST['editrow']);
                $where_info   =   array("rowid = :rowid");
                G::Obj('WOTCHours')->updHoursInfo($set_info, $where_info, array($params));

            } else {
            
                $year   =   1;
                $yr     =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($_POST['wotcID'], $_POST['ApplicationID']);
            
                if ($yr) $year = $yr + 1;
            
                $info   =   array(
                                "wotcID"        =>  $_POST['wotcID'],
                                "ApplicationID" =>  $_POST['ApplicationID'],
                                "Year"          =>  $year,
                                "Hours"         =>  $_POST['hours'],
                                "Wages"         =>  $_POST['wages'],
                                "ASOF"          =>  date("Y-m-d",strtotime($_POST['asof'])),
                                "Comments"      =>  $_POST['comment'],
                                "LastUpdated"   =>  "NOW()"
                            );
                G::Obj('WOTCHours')->insHoursInfo($info);
            }
            
            echo 'closewindow';
            exit;
        
        } // end hours wages and asof

    } // end if process

    if (sizeof($ERROR) > 0) {
      $HRS['Hours'] =   $_POST['hours'];
      $HRS['Wages'] =   $_POST['wages'];
      $HRS['ASOF']  =   $_POST['asof'];
      
      foreach ($ERROR as $key => $value) {
        $ERROR[$key]    =   '&nbsp;&nbsp;&nbsp;<span style="color:red;font-size:10pt;">' . $value . '</span>';
      }
    }
    
    $proc_start_date    =   date_create($PROC['CurrentDate']);
    
    if($HRS['Hours'] == "") $HRS['Hours']   =   0;
    if($HRS['Wages'] == "") $HRS['Wages']   =   0;
    if($HRS['ASOF'] == "") $HRS['ASOF']     =   date_format($proc_start_date,'m/d/Y');
    
    ?>
    <strong style="font-size:14pt;">Track Hours</strong><br>
    <table border="0" cellspacing="0" cellpadding="5">
    <tr><td align="right">
    Application ID:
    </td><td>
    <?php echo $_POST['ApplicationID']; ?> (XXX-XX-<?php echo $AD['Social3']; ?>)<br>
    </td></tr>
    
    <tr><td align="right" valign="top">
    Applicant:
    </td><td valign="top">
    <b><?php echo $AD['FirstName']; ?> <?php echo $AD['MiddleName']; ?> <?php echo $AD['LastName']; ?></b><br>
    <?php echo $AD['Address']; ?><br>
    <?php echo $AD['City']; ?>, <?php echo $AD['State']; ?> <?php echo $AD['ZipCode']; ?><br>
    </td></tr>
    
    <tr><td align="right" valign="top">
    Earnings:
    </td><td valign="top">
    Hours: <?php echo number_format($TTHRS['Hours']); ?><br>
    Wages: $ <?php echo number_format($TTHRS['Wages'],2,".",","); ?><br>
    AS OF: <?php echo $TTHRS['ASOF']; ?><br>
    </td></tr>
    
    <tr><td align="right">
    Hours:
    </td><td>
    <input type="text" name="hours" value="<?php echo $HRS['Hours']; ?>" size="10" maxlength="15"><?php echo $ERROR['hours']; ?>
    </td></tr>
    
    <tr>
        <td align="right">Wages:</td>
        <td><input type="text" name="wages" value="<?php echo $HRS['Wages']; ?>" size="10" maxlength="15"><?php echo $ERROR['wages']; ?></td>
    </tr>
    
    <tr>
        <td align="right">AS OF:</td>
        <td><input id="asof<?php echo $_POST['wotcID']; ?><?php echo $_POST['ApplicationID']; ?>" name="asof" type="text" value="<?php echo $HRS['ASOF']; ?>" size="10" onfocus="clearDefault(this)" /><?php echo $ERROR['asof']; ?></td>
    </tr>
    
    <tr>
        <td align="right">Comment:</td>
        <td><input type="text" name="comment" size="50" maxlength="255" value="<?php echo $HRS['Comments']; ?>"></td>
    </tr>
    
    <?php
    if ($editrow) {
        $button =   "Update Data";
    } else {
        $button =   "Add New";
    }
    ?>
    <tr><td colspan="100%" align="center" valign="bottom" height="40">
    <input type="hidden" name="wotcID" value="<?php echo $_POST['wotcID']; ?>">
    <input type="hidden" name="ApplicationID" value="<?php echo $_POST['ApplicationID']; ?>">
    <button onclick="loadTrackBillForm('<?php echo $_POST['type']; ?>','<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>','P','<?php echo $_POST['editrow']; ?>');return false;"><?php echo $button; ?></button>
    <?php if ($editrow) { ?>
    <button onclick="loadTrackBillForm('<?php echo $_POST['type']; ?>','<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>','R','<?php echo $_POST['editrow']; ?>');return false;">Reset Form</button>
    <?php } ?>
    <button onclick="loadTrackBillForm('<?php echo $_POST['type']; ?>','<?php echo $_POST['wotcID']; ?>','<?php echo $_POST['ApplicationID']; ?>','R','');return false;">Clear Form</button>
    </td></tr>
    
    </table>

<?php 
} // end if
?>
