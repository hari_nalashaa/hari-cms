<?php
if ($_GET['del'] != "") {
    $params_info    =   array(":ApplicationID"=>$_GET['del']);
    $query          =   "UPDATE ApplicantData SET Qualifies = 'D', Processed_Date = NOW() WHERE wotcID = 'callcenter'";
    $query          .=  " AND ApplicationID = :ApplicationID";
    G::Obj('GenericQueries')->conn_string   =   "WOTC";
    $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params_info));
} // end del

$MOVEID = G::Obj('WOTCcrm')->getAllActiveWotcIDs();

echo '<div style="font-size:12pt;font-weight:bold;margin-bottom:20px;">WOTC Change Applicant</div>';
echo "<form id=\"changewotcid\">\n";
echo "ApplicationID: <input type=\"text\" name=\"ApplicationID\" size=\"12\" value=\"\">\n";
echo "&nbsp;&nbsp;&nbsp;";
echo " move from: <select name=\"wotcID\" style=\"width:300px;\">\n";
echo "<option value=\"\">Please Select</option>\n";
foreach ($MOVEID as $M) {
    if ($M['wotcID'] == 'callcenter') {
	$M['AKADBA']="Call Center";
	$M['FEIN']="00-000000";
    }
    echo "<option value=\"" . $M['wotcID'] . "\"";
    if ($M['wotcID'] == "callcenter") { echo "selected"; }
    echo ">";
    echo $M['AKADBA'] . ", " . $M['Address'] . ", "  . $M['City'] . " " . $M['ZipCode'];
    echo "&nbsp;&nbsp;" . $M['State'] . ' - (' . $M['FEIN'] . ') ';
    echo "&nbsp;&nbsp;(" . $M['wotcID'] . ")";
    echo "</option>\n";
} // end foreach
echo "</select>\n";
echo "&nbsp;&nbsp;&nbsp;";
echo " to: <select name=\"moveto\" style=\"width:300px;\">\n";
echo "<option value=\"\">Please Select</option>\n";
foreach ($MOVEID as $M) {
    if ($M['wotcID'] == 'callcenter') {
	$M['AKADBA']="Call Center";
	$M['FEIN']="00-000000";
    }
    echo "<option value=\"" . $M['wotcID'] . "\">";
    echo $M['AKADBA'] . ", " . $M['Address'] . ", "  . $M['City'] . " " . $M['ZipCode'];
    echo "&nbsp;&nbsp;" . $M['State'] . ' - (' . $M['FEIN'] . ') ';
    echo "&nbsp;&nbsp;(" . $M['wotcID'] . ")";
    echo "</option>\n";
} // end foreach
echo "</select>\n";
echo "&nbsp;&nbsp;&nbsp;";
echo "<button onclick=\"changeWOTCid('" . ADMIN_HOME . "','changewotcid');return false;\" style=\"padding:5px;\">Change Applicant</button>\n";
echo "</form>\n";

echo '<div style="font-size:10pt;font-weight:bold;margin-top:30px;margin-bottom:20px;">';
echo "Pending Call Center Applications (callcenter)";
echo '</div>';

$query = "SELECT * FROM ApplicantData";
$query .= " WHERE wotcID = 'callcenter'";
$query .= " AND Qualifies != 'D'";
$query .= " ORDER BY EntryDate DESC";
$params_info = array();

G::Obj('GenericQueries')->conn_string   =   "WOTC";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

if (sizeof($RESULTS) == 0) {
    echo '<div style="margin-bottom:10px;">';
    echo 'There are no pending call center applications at this time.';
    echo '</div>' . "\n";
}

foreach ($RESULTS AS $R) {
    echo '<div style="margin-bottom:10px;">';
    echo htmlspecialchars($R['ApplicationID']) . " - ";
    echo htmlspecialchars($R['EntryDate']) . " - ";
    $Name = $R['FirstName'];
    
    if ($R['MiddleName'] != "") {
        $Name .= ' ' . $R['MiddleName'];
    }

    $Name .= ' ' . $R['LastName'];
    echo htmlspecialchars($Name);
    echo ' -- ' . $R['City'] . ', ' . $R['State'] . ' ' . $R['Country'];
    echo ' - Qualifies: ' . $R['Qualifies'];
    echo '&nbsp;&nbsp;&nbsp;';
    echo "<a href=\"" . ADMIN_HOME . "index.php?pg=wotc_change_applicant&del=" . $R['ApplicationID'] . "\"";
    echo " onclick=\"return confirm('Are you sure you want to delete this applicant?\\n\\n" . $R['ApplicationID'] . ' - ' . $Name . "')\"";
    echo ">";
    echo "<img src=\"" . ADMIN_HOME . "images/cross.png\" border=\"0\" title=\"Delete\" style=\"margin:0px 3px -4px 0px;\"><b style=\"FONT-SIZE:8pt;COLOR:#000000\">Delete Applicant</b>";
    echo "</a>\n";
    echo '</div>' . "\n";
} // end foreach

?>
