<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$columns = 'CompanyID, CompanyName, Assignment';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];

			$objPHPExcel = new Spreadsheet();
                        $objPHPExcel->getProperties()->setCreator("CMS WOTC")
                               ->setLastModifiedBy("System")
                               ->setTitle("WOTC Credit Voucher Data")
                               ->setSubject("Excel")
                               ->setDescription("Credit Voucher Export")
                               ->setKeywords("phpExcel")
                               ->setCategory("Output");

                        // Create a first sheet, representing sales data
                        $objPHPExcel->setActiveSheetIndex(0);
                        $objPHPExcel->getActiveSheet()->setTitle("Credit Voucher Data");

                        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
                        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setRGB('0c00ac');
                        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
                        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
                        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setVertical('center');

                        $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Employer');
                        $objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Filed In');
                        $objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Realized Date');
                        $objPHPExcel->getActiveSheet()->getCell('D1')->setValue('ApplicationID');
                        $objPHPExcel->getActiveSheet()->getCell('E1')->setValue('ApplicantName');
                        $objPHPExcel->getActiveSheet()->getCell('F1')->setValue('SSN');
                        $objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Reason');
                        $objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Tax Credit');

                        for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
                        }
                            $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


                if ($CRM['Assignment'] == "Business Partners") {

                   $columns = 'CompanyID';
                   $where = array("Partner = :Partner","Active = 'Y'");
                   $params = array(":Partner"=>$CRM['CompanyID']);
                   $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
                   $DATA = $RESULTS['results'];

                   $n=1;
                   foreach($DATA AS $P) {

                          $BILLINGDATA = G::Obj('WOTCcrm')->getBillingData($P['CompanyID']);

                          foreach ($BILLINGDATA AS $BD) {
                            $n++;

                            $objPHPExcel->getActiveSheet()->getStyle('H'.$n)->getNumberFormat()->setFormatCode("#,##0.00");
                            $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($BD['EIN'] . ", " . $BD['AKADBA']);
                            $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($BD['WorkState']);
                            $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($BD['DateBilled']);
                            $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($BD['ApplicationID']);
                            $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($BD['ApplicantName']);
                            $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($BD['SSN']);
                            $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($BD['Reason']);
                            $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue(number_format($BD['TaxCredit'], 2, '.', ','));

                          } // end foreach PAYROLLDATA

                   } // end foreach

                } else {

                          $BILLINGDATA = G::Obj('WOTCcrm')->getBillingData($CRM['CompanyID']);

                          $n=1;
                          foreach ($BILLINGDATA AS $BD) {
                            $n++;

                            $objPHPExcel->getActiveSheet()->getStyle('H'.$n)->getNumberFormat()->setFormatCode("#,##0.00");
                            $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($BD['EIN'] . ", " . $BD['AKADBA']);
                            $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($BD['WorkState']);
                            $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($BD['DateBilled']);
                            $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($BD['ApplicationID']);
                            $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($BD['ApplicantName']);
                            $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($BD['SSN']);
                            $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($BD['Reason']);
                            $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue(number_format($BD['TaxCredit'], 2, '.', ','));

                          } // end foreach PAYROLLDATA

                }

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="VoucherExport.xlsx"');
header('Cache-Control: max-age=0');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
