function loadwotcPartner(partnerid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("Partner").innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/partner.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("PartnerID=" + partnerid + "&d=" + d);

} // end function

function loadwotcParent(partnerid,parentid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("Parent").innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/parent.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("PartnerID=" + partnerid + "&ParentID=" + parentid + "&d=" + d);

} // end function

function loadwotcIDs(partner,parentid,wotcid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("WotcIDs").innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcids.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("PartnerID=" + partner + "&ParentID=" + parentid + "&wotcID=" + wotcid + "&d=" + d);

} // end function

function loadHistory(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("DisplayDocuments").innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/history.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("CompanyID=" + companyid + "&d=" + d);

} // end function

function processVault(wotcID, ApplicationID, type, action, desc) {

	var fillwindow = "Vault" + wotcID + ApplicationID;

	var result = confirm(desc);
	if (result == true) {
		var xmlhttp;

		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera,
									// Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById(fillwindow).innerHTML = xmlhttp.responseText;
			}
		}

		var formData = new FormData();
		formData.append('wotcID', wotcID);
		formData.append('ApplicationID', ApplicationID);
		formData.append('type', type);
		formData.append('action', action);

		xmlhttp.open("POST", "wotc/ajax/vault.php", true);
		xmlhttp.send(formData);

	} // end confirm
} // end function

function loadTrackBillResults(wotcID, ApplicationID) {

	var xmlhttp;
	var fillwindow = "trackandbill" + wotcID + ApplicationID;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById(fillwindow).innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/trackbill.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("wotcID=" + wotcID + "&ApplicationID=" + ApplicationID + "&d="
			+ d);

} // end function

function loadCRMExport(userid) {

	var r = confirm("Are you sure you want to run the CRM Export.");
	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcCRMReport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("UserID=" + userid + "&d=" + d);

	} else { // end r == true
             document.getElementById('DisplayDocuments').innerHTML = "CRM Export not sent.";
	}

} // end function

function loadPatExport() {

	var r = confirm("Are you sure you want to send Pat the excel report.");
	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcPatReport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("d=" + d);

	} else { // end r == true
             document.getElementById('DisplayDocuments').innerHTML = "Pat's report not sent.";
	}

} // end function

function loadWOTCReport(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        document.getElementById('Report').innerHTML = xmlhttp.responseText;

                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcreport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send('CompanyID=' + companyid  + "&d=" + d);

} // end function

function loadPOAReport() {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        document.getElementById('Report').innerHTML = xmlhttp.responseText;

                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcpoareport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("d=" + d);

} // end function

function loadApplicantsAwaitingPayroll() {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        document.getElementById('Report').innerHTML = xmlhttp.responseText;

                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcapplicantsawaitingpayroll.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("d=" + d);

} // end function

function loadBillingPayment(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        document.getElementById('Report').innerHTML = xmlhttp.responseText;
			again_cal();

                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcbillingpayment.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);

} // end function

function processPayment(formid,companyid) {

	var data = $('#UpdatePayment' + formid).serialize();

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			loadBillingPayment(companyid);
                }
        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcupdatepayment.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&d=" + d);

} // end function

function loadAwaitingPayrollReport(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        document.getElementById('Report').innerHTML = xmlhttp.responseText;

                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcawaitingpayrollreport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);

} // end function

function loadBillingReport(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                        document.getElementById('Report').innerHTML = xmlhttp.responseText;

                }

        }
        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/wotcbillingreport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);

} // end function

function loadWOTCSendReport(companyid,contact,process) {

	var data = "N=";
	if (process == 'Y') {
	  data = $('#formReports').serialize();
	}

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                      document.getElementById('Report').innerHTML = xmlhttp.responseText;
		      $("#openReport").show();

                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/sendreport.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&companyid=" + companyid + "&contact=" + contact + "&process=" + process + "&d=" + d);

} // end function

function updateWOTCIDs(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      $("#openDocuments").show();

                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/WOTCIDS.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);


} // end function

function updateFEIN(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      $("#openDocuments").show();

                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/FEIN.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);


} // end function

function removeWotcID(companyid,wotcid) {

	var r = confirm("Are you sure you want to remove the following wotcID from this company?:\n\n" + wotcid);

	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      $("#openDocuments").show();

                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/removeWotcID.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&wotcID=" + wotcid + "&d=" + d);


	} // end r == true

} // end function

function associateExistingWotcID(companyid,process) {

	var data = "N=";
	if (process == "submit") {
	  data = $('#formDocuments').serialize();
	}

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      $("#openDocuments").show();

                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/associateWotcOrganization.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);


} // end function

function userAccess(companyid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/USERS.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&d=" + d);


} // end function

function processUser(companyid,action,userid,process) {

	var r = true;
	if (action == "delete") {
	   r = confirm("Are you sure you want to delete this user?\n\n" + userid);
	}

	if (r == true) {

	var data = "N=";
	if (process == "submit") {
	  data = $('#formDocuments').serialize();
	}

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			if (action == 'delete') {
			   userAccess(companyid);
			} else {
                           document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
			}
                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/processUser.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&CompanyID=" + companyid + "&Action=" + action + "&UserID=" + userid + "&d=" + d);

	} // end confirm


} // end function

function apiKey(companyid,action) {

	var r = true;
	if (action == "revoke") {
	   r = confirm("Are you sure you want to revoke this API key?\n\n");
	}

	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			userAccess(companyid);
                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/API.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&Action=" + action + "&d=" + d);

	}


} // end function

function removeWotcID(companyid,wotcid) {

	var r = confirm("Are you sure you want to remove the following wotcID from this company?:\n\n" + wotcid);

	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			updateWOTCIDs(companyid);
                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/removeWotcID.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + companyid + "&wotcID=" + wotcid + "&d=" + d);


	} // end r == true

} // end function

function associateExistingWotcID(companyid,process) {

	var data = "N=";
	if (process == "submit") {
	  data = $('#formDocuments').serialize();
	}

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      $("#openDocuments").show();
                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/associateWotcOrganization.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&CompanyID=" + companyid + "&d=" + d);

} // end function

function processWotcOrganization(companyid,wotcid,process) {

	var data = "N=";
	if (process == "submit") {
	  data = $('#formDocuments').serialize();
	}

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
        } else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      $("#openDocuments").show();

                }

        }

        var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/processWotcOrganization.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&CompanyID=" + companyid + "&wotcID=" + wotcid + "&d=" + d);

} // end function

function parentConfig(CompanyID,process) {

	var data = "N=";

	if (process == "submit") {

	  data = new FormData();
	  //Form data
	  var form_data = $('#formDocuments').serializeArray();
	  $.each(form_data, function (key, input) {
	    data.append(input.name, input.value);
	  });

	  //File data
	  var file_data = $('input[name="Logo"]')[0].files;
	  for (var i = 0; i < file_data.length; i++) {
	    data.append("Logo[]", file_data[i]);
	  }

	    data.append("CompanyID", CompanyID);


	  //Custom data
	  data.append('key', 'value');

	  $.ajax({
	    url: "wotc/PARENT.php",
	    method: "post",
	    processData: false,
	    contentType: false,
	    data: data,
	    success: function (data) {
		    $("#DisplayDocuments").html(data);
		    again_cal();
	    },
	    error: function (e) {
	        //error
	    }
	  });

	} else {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      again_cal();
		}

	}

        var d = new Date().getTime();
	xmlhttp.open("POST", "wotc/PARENT.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + CompanyID + "&d=" + d);

	} // end else

} // end function

function removeLogo(CompanyID) {

	var r = confirm("Are you sure you want to delete this Logo?");
	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		}

	}

        var d = new Date().getTime();
	xmlhttp.open("POST", "wotc/removeLogo.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("CompanyID=" + CompanyID + "&d=" + d);

	} // end r == true


} // end function


function processFEIN(CompanyID,FEIN,process) {

	var xmlhttp;

	var data = "N=";
	if (process == "submit") {
	  data = $('#formDocuments').serialize();
	}

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		      again_cal();
		}

	}

        var d = new Date().getTime();
	xmlhttp.open("POST", "wotc/processFEIN.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&CompanyID=" + CompanyID + "&FEIN=" + FEIN + "&d=" + d);


} // end function

function loadTrackBillForm(type, wotcID, ApplicationID, process, editrow) {

	var xmlhttp;

	var data = "";
	if (type == "track") {
		data = $('#TrackData' + wotcID + ApplicationID).serialize();
	}
	if (type == "bill") {
		data = $('#BillData' + wotcID + ApplicationID).serialize();
	}
	if (type == "process") {
		data = $('#ProcessData' + wotcID + ApplicationID).serialize();
	}
	if (type == "edit") {
		data = $('#EditData' + wotcID + ApplicationID).serialize();
	}

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

			if (xmlhttp.responseText.replace(/\s/g, '') == "closewindow") {

				loadTrackBillResults(wotcID, ApplicationID);
				loadProcessingResults(wotcID, ApplicationID);

				location.hash = '#closeTrackBill';
				return;

			} else {

				if (type == "track") {
					document.getElementById('Track' + wotcID + ApplicationID).innerHTML = xmlhttp.responseText;
				}
				if (type == "bill") {
					document.getElementById('Bill' + wotcID + ApplicationID).innerHTML = xmlhttp.responseText;
				}
				if (type == "process") {
					document.getElementById('Process' + wotcID + ApplicationID).innerHTML = xmlhttp.responseText;
				}
				if (type == "edit") {
					document.getElementById('Edit' + wotcID + ApplicationID).innerHTML = xmlhttp.responseText;
				}
				again_cal();
			}
		}

	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "wotc/ajax/trackbillforms.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("wotcID=" + wotcID + "&ApplicationID=" + ApplicationID
			+ "&type=" + type + "&process=" + process + "&editrow=" + editrow
			+ "&" + data + "&d=" + d);

} // end function

function CheckReason(obj, wotcID, ApplicationID) {

	if (obj.value == "N") {
		document
				.getElementById('AcceptFilterQuestion' + wotcID + ApplicationID).style.display = 'none';
		document
				.getElementById('DenialFilterQuestion' + wotcID + ApplicationID).style.display = 'block';
	} else if (obj.value == "Y") {
		document
				.getElementById('AcceptFilterQuestion' + wotcID + ApplicationID).style.display = 'block';
		document
				.getElementById('DenialFilterQuestion' + wotcID + ApplicationID).style.display = 'none';
	} else {
		document
				.getElementById('AcceptFilterQuestion' + wotcID + ApplicationID).style.display = 'none';
		document
				.getElementById('DenialFilterQuestion' + wotcID + ApplicationID).style.display = 'none';
	}

} // end function

function deleteHours(wotcID, ApplicationID, editrow, hours, wages, asof) {

	var r = confirm("Are you sure you want to delete these hours?\n\nHours: " + hours
			+ "\nWages: " + wages + "\nAS OF: " + asof);
	if (r == true) {

		var xmlhttp;

		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera,
									// Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				loadTrackBillResults(wotcID, ApplicationID);
			}
		}

		var d = new Date().getTime();

		xmlhttp.open("POST", "wotc/ajax/deletehours.php", true);
		xmlhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlhttp.send("wotcID=" + wotcID + "&ApplicationID=" + ApplicationID
				+ "&editrow=" + editrow + "&d=" + d);
	} // end r == true

} // end function

function deleteBilling(wotcID, ApplicationID, year) {

	var r = confirm("Are you sure you want to delete billing year " + year + "?");
	if (r == true) {

		var xmlhttp;

		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera,
									// Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				loadTrackBillResults(wotcID, ApplicationID);
			}
		}

		var d = new Date().getTime();

		xmlhttp.open("POST", "wotc/ajax/deletebilling.php", true);
		xmlhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlhttp.send("wotcID=" + wotcID + "&ApplicationID=" + ApplicationID
				+ "&editrow=" + year + "&d=" + d);
	} // end r == true

} // end function

function clearDefault(obj) {
	if (obj.defaultValue == obj.value)
		obj.value = ""
}

function changeWOTCid(url, form) {

	var sendData = $("#" + form).serialize();
	var formData = $("#" + form).serializeArray();

	len = formData.length, dataObj = {};

	for (i = 0; i < len; i++) {
		dataObj[formData[i].name] = formData[i].value;
	}

	var r = confirm("Are you sure you want to move this applicant?\n\n"
			+ dataObj['wotcID'] + " - " + dataObj['ApplicationID'] + " to "
			+ dataObj['moveto'] + "\n");
	if (r == true) {

		var xmlhttp;

		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera,
									// Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				document.getElementById(form).innerHTML = xmlhttp.responseText;
			}
		}

		var d = new Date().getTime();
		xmlhttp.open("POST", url + "wotc/ajax/moveapplicant.php", false);
		xmlhttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlhttp.send(sendData + "&d=" + d);

	} // end r

} // end function

function replicateDate(wotcID, ApplicationID) {

        var key_date = document.getElementById('receivedp' + wotcID + ApplicationID);

        document.getElementById('offerdatep' + wotcID + ApplicationID).value = key_date.value;
        document.getElementById('hireddatep' + wotcID + ApplicationID).value = key_date.value;
        document.getElementById('startdatep' + wotcID + ApplicationID).value = key_date.value;

} // end function

function deleteCRMNote(HistoryID,CompanyID,Note) {

	var r = confirm("Are you sure you want to delete this history record?\n\n" + Note);
	if (r == true) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		      loadHistory(CompanyID);
		}

	}

        var d = new Date().getTime();
	xmlhttp.open("POST", "wotc/ajax/deleteCRMNote.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send("HistoryID=" + HistoryID + "&d=" + d);

	} // end r == true


} // end function

function updateCRMNote(CompanyID,HistoryID,process) {

	var data = "N=";
	if (process == "submit") {
	  data = $('#formDocuments').serialize();
	}

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                      document.getElementById('DisplayDocuments').innerHTML = xmlhttp.responseText;
		}

	}

        var d = new Date().getTime();
	xmlhttp.open("POST", "wotc/ajax/updateCRMNote.php", true);
        xmlhttp.setRequestHeader("Content-type",
                        "application/x-www-form-urlencoded");
        xmlhttp.send(data + "&CompanyID=" + CompanyID + "&HistoryID=" + HistoryID + "&d=" + d);


} // end function
