<div style="font-size:14pt;font-weight:bold;margin-bottom:20px;">WOTC CRM</div>
<?php

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

$STATES =   G::Obj('WOTCStatePortals')->getStateList ();
$ASSIGNMENT = G::Obj('WOTCcrm')->getAssignment();

if ($_GET['Action'] == 'remove_logo') {
	G::Obj('WOTCcrm')->removeLogo($_REQUEST['CompanyID']);

}
if ($_GET['delete'] != '') {

  $columns = 'CompanyName, CompanyID';
  $where = array("CompanyID = :CompanyID");
  $params = array(":CompanyID"=>$_GET['delete']);
  $results2 = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
  $CRM = $results2['results'][0];

  if ($CRM['CompanyID'] != '') {
    G::Obj('WOTCcrm')->delCRM($CRM['CompanyID']);
    G::Obj('WOTCcrm')->delCRMFEIN($CRM['CompanyID']);

    G::Obj('GenericQueries')->conn_string =   "WOTC";

    $update_query = "UPDATE OrgData set CompanyID = '' WHERE CompanyID = :CompanyID";
    $params         =   array(":CompanyID"=>$CRM['CompanyID']);
    G::Obj('GenericQueries')->updInfoByQuery($update_query, array($params));

    if ($CRM['Assignment'] == "Business Partners") {
      $update_query = "UPDATE CRM set Partner = '' WHERE CompanyID = :CompanyID";
      $params         =   array(":CompanyID"=>$CRM['CompanyID']);
      G::Obj('GenericQueries')->updInfoByQuery($update_query, array($params));
    }

    $set_info       =   array("CompanyID = ''");
    $where_info     =   array("CompanyID = :CompanyID");
    $params         =   array(":CompanyID"=>$CRM['CompanyID']);
    G::Obj('WOTCOrganization')->updOrgDataInfo($set_info, $where_info, array($params));

?>
<script>
  setTimeout(function() { $(message).html('<span style="color:red;font-style:italic;"><?php echo $CRM['CompanyName']; ?> has been deleted.</span>'); }, 500);
</script>
<?php
  }

} // end delete

if ($_REQUEST['process'] == 'Y') {

	$CONTACTS=array();
	for ($x = 1; $x <= 3; $x++) {

		$CONTACTS['Contact' . $x]=$_REQUEST['Contact' . $x];	
		$CONTACTS['Role' . $x]=$_REQUEST['Role' . $x];		
		$CONTACTS['Phone' . $x]=$_REQUEST['Phone' . $x];		
		$CONTACTS['Email' . $x]=$_REQUEST['Email' . $x];		
	}
	$C = json_encode($CONTACTS);

	if ($_REQUEST['Assignment'] == "Business Partners") {
		$_REQUEST['Partner']="";
    		G::Obj('WOTCcrm')->delCRMFEIN($_REQUEST['CompanyID']);
    		$set_info       =   array("CompanyID = ''");
    		$where_info     =   array("CompanyID = :CompanyID");
    		$params         =   array(":CompanyID"=>$_REQUEST['CompanyID']);
    		G::Obj('WOTCOrganization')->updOrgDataInfo($set_info, $where_info, array($params));
	}

	$info   =   array(
                 "CompanyID"   	=>  $_REQUEST['CompanyID'],
                 "Active"   	=>  $_REQUEST['Active'],
                 "Assignment"   =>  $_REQUEST['Assignment'],
                 "Industry"   	=>  $_REQUEST['Industry'],
                 "Partner"   	=>  $_REQUEST['Partner'],
                 "CompanyName"  =>  $_REQUEST['CompanyName'],
                 "Address"      =>  $_REQUEST['Address'],
                 "City"    	=>  $_REQUEST['City'],
                 "State"    	=>  $_REQUEST['State'],
                 "ZipCode"    	=>  $_REQUEST['ZipCode'],
                 "CompanyPhone" =>  json_encode(array($_REQUEST['PhoneAreaCode'],$_REQUEST['PhonePrefix'],$_REQUEST['PhoneSuffix'])),
                 "QB"    	=>  $_REQUEST['QB'],
		 "Rate"    	=>  preg_replace("/[^0-9]/", "", $_REQUEST['Rate']),
	         "BPRate"    	=>  preg_replace("/[^0-9]/", "", $_REQUEST['BPRate']),
                 "SetupFee"    	=>  number_format(preg_replace("/[^0-9.]/", "", $_REQUEST['SetupFee']), 2, '.', ''),
                 "Contacts"    	=>  $C
                 );

	$skip   =   array("CompanyID");

	if ($_REQUEST['CompanyID'] == "") {
	  $info['CompanyID']=uniqid() . rand(1, 1);
	  $_REQUEST['CompanyID']=$info['CompanyID'];
	  $_REQUEST['Notes']="Company Added.";
	} 

	if (($_REQUEST['CompanyName'] != "Enter New Company") && ($_REQUEST['CompanyName'] != "")) {

	  if ($_FILES['logo']['name']) {

            $name = $_FILES ['logo'] ['name'];
            $name = preg_replace ( '/\s/', '_', $name );
            $name = preg_replace ( '/\&/', '', $name );
	    $name = $_REQUEST['CompanyID'] . '-' . $name;

            $data = file_get_contents ( $_FILES ['logo'] ['tmp_name'] );

            $filename = WOTC_DIR . "images/logos/" . $name;

            $fh = fopen ( $filename, 'w' );
            fwrite ( $fh, $data );
            fclose ( $fh );

            chmod ( $filename, 0666 );

	    $info['Logo']=$name;

	  } // end FILES

	  G::Obj('WOTCcrm')->insUpdCRM($info, $skip);
	}

	$infohistory   =   array(
                 "HistoryID"   	=>  uniqid() . rand(1, 1),
                 "CompanyID"   	=>  $_REQUEST['CompanyID'],
                 "Date"   	=>  'NOW()',
                 "UserID"   	=>  $AUTH['UserID'],
                 "Notes"  	=>  $_REQUEST['Notes']
                 );

	$skiphistory   =   array("HistoryID");

	if ($_REQUEST['Notes'] != "") {
	  G::Obj('WOTCcrm')->insUpdCRMHistory($infohistory, $skiphistory);
	}

// clear search data
$_REQUEST['CompanyName']="";
$_REQUEST['Assignment']="";
$_REQUEST['Active']="Y";

?>
<script>
setTimeout(function() { $(message).html('<span style="color:red;font-style:italic;">Company information successfully updated.</span>'); }, 500);
</script>
<?php
} // end processing

if ((!isset($_REQUEST['process'])) && (!isset($_REQUEST['search']))) {
$_REQUEST['Active']="Y";
}


if ($_REQUEST['search'] == 'Y') {

$columns = 'CompanyID, CompanyName';
$where = array();
$params = array();

$where[]="Active = :Active";
$params[':Active']=$_REQUEST['Active'];

if ($_REQUEST['Assignment'] != "") {
	$where[]="Assignment = :Assignment";
	$params[':Assignment']=$_REQUEST['Assignment'];
} 

if ($_REQUEST['CompanyName'] != "") {
	$where[]="CompanyName like :CompanyName";
	$params[':CompanyName']="%".$_REQUEST['CompanyName']."%";

}

$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', 'CompanyName', array($params));
$LIST = $results['results'];

if (count($LIST) == 0) {
?>
<script>
setTimeout(function() { $(message).html('<span style="color:red;font-style:italic;">No results match your query.</span>'); }, 500);
</script>
<?php
} else if (count($LIST) == 1) {

	$_REQUEST['CompanyID']=$LIST[0]['CompanyID'];

} // end else count

} // end search Y

echo '<div style="border:1px solid #000000;background:#ffffff;padding:10px;width:80%;margin: 10px 20px 10px 20px;min-width:1120px;">';

echo '<form method="post" action="index.php">';
echo '<input type="hidden" name="pg" value="wotccrm">';
echo '<input type="hidden" name="search" value="Y">';

echo 'Company Name: ';
echo '<input type="text" name="CompanyName" value="';
echo $_REQUEST['CompanyName'];
echo '" size="20">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'Assignment: ';
echo '<select name="Assignment">';
echo '<option value="">Please Select</option>';
foreach ($ASSIGNMENT AS $A) {
        echo '<option value="' . $A . '"';
	if ($_REQUEST['Assignment'] == $A) { echo ' selected'; }
        echo '>' . $A . '</option>';
}
echo '</select>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'Active: ';
echo '<input type="checkbox" name="Active" value="Y"';
if ($_REQUEST['Active'] == 'Y') { echo ' checked'; }
echo '>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo '<input type="submit" value="Find Company">';
echo '</form>';

echo '</div>';

if (count($LIST) > 1) {

echo '<div style="max-height: 150px;overflow-y:auto;border:1px solid #dddddd;background:#eeeeee;padding:10px;width:80%;margin: 0 20px 10px 20px;min-width:1120px;line-height:150%;">';
foreach ($LIST AS $L) {
	echo '<a href="'.ADMIN_HOME.'index.php?pg=wotccrm&CompanyID='.$L['CompanyID'].'">'.$L['CompanyName'].'</a><br>';
}
echo '</div>';

}

$columns = '*';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results2 = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results2['results'][0];

$REPORTACTIVE="N";
if ($CRM['Assignment'] == "Business Partners") {

        $columns = 'CompanyID, CompanyName';
        $where = array("Partner = :Partner","Active = 'Y'");
        $params = array(":Partner"=>$CRM['CompanyID']);
        $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));

	if ($RESULTS['count'] > 0) {
   	   $REPORTACTIVE="Y";
	}

} else {
  if (count(G::Obj('WOTCcrm')->getWOTCIDs($CRM['CompanyID'])) > 0 ) {
   $REPORTACTIVE="Y";
  }
}

$C=get_object_vars(json_decode($CRM['Contacts']));

echo '<div style="border:2px solid #dddddd;background:#dddddd;padding:10px;width:80%;margin: 0 20px;min-width:1120px;">';
if ($CRM['CompanyName'] == "") {
	$CRM['CompanyName']="Enter New Company";
}
echo '<table width="100%"><tr>';
echo '<td valign="top"><strong style="font-size:12pt;">' . $CRM['CompanyName'] . '</strong>';
echo '<br>';
echo '<font id="message" style="margin-top:10px;"></font>';
echo '</td><td valign="top" align="right">';
echo '<a href="'.ADMIN_HOME.'index.php?pg=wotccrm"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin: 0px 3px -4px 0px;"><b style="FONT-SIZE: 8pt; COLOR: #000000">Add New Company</b></a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openDocuments" onclick="loadCRMExport(\'' . $AUTH['UserID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/page_white_put.png" title="CRM Export" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">CRM Export</b></a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openDocuments" onclick="loadPatExport();"><img src="'.IRECRUIT_HOME.'images/icons/page_white_put.png" title="Pat Export" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Pat Export</b></a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openReport" onclick="loadPOAReport();"><img src="'.IRECRUIT_HOME.'images/icons/page_white_text.png" title="POA Expirations" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">POA Expirations</b></a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openReport" onclick="loadApplicantsAwaitingPayroll();"><img src="'.IRECRUIT_HOME.'images/icons/page_white_text.png" title="Awaiting Payroll" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Awaiting Payroll</b></a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openReport" onclick="loadBillingPayment(\'\');"><img src="'.IRECRUIT_HOME.'images/icons/page_white_text.png" title="Billing/Payment" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Billing/Payment</b></a>';
echo '</td></tr></table>';

echo '<hr size="1">';

echo '<div id="openReport" class="openReport"><div><a href="#closeReport" title="Close" class="closeReport">X</a>';
echo '<form id="formReports">';
echo '<div id="Report"></div>';
echo '</form>';
echo '</div></div>';

echo '<div id="openDocuments" class="openDocuments"><div><a href="#closeDocuments" title="Close" class="closeDocuments">X</a><form id="formDocuments" enctype="multipart/form-data"><div id="DisplayDocuments"></div></form></div></div>';

echo '<form method="post" action="index.php"  enctype="multipart/form-data">';
echo '<input type="hidden" name="pg" value="wotccrm">';
echo '<input type="hidden" name="CompanyID" value="' . $CRM['CompanyID']. '">';
echo '<input type="hidden" name="process" value="Y">';
echo '<div style="border:1px solid #aaaaaa;background:#efefef;padding:10px;line-height:280%;">';

echo 'Company Name: ';
echo '<input type="text" name="CompanyName" value="';
echo $CRM['CompanyName'];
echo '" size="45">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'Active: ';
echo '<input type="checkbox" name="Active" value="Y"';
if ($CRM['Active'] == 'Y') { echo ' checked'; }
echo '>';
echo '<br>';

echo 'Address: ';
echo '<input type="text" name="Address" value="';
echo $CRM['Address'];
echo '" size="40">';
echo '<br>';

echo 'City: ';
echo '<input type="text" name="City" value="';
echo $CRM['City'];
echo '" size="30">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'State: ';
echo '<select name="State">';
echo '<option value="">Please Select</option>';
foreach ($STATES AS $v=>$k) {
        echo '<option value="' . $v . '"';
        if ($CRM['State'] == $v) { echo ' selected';  }
        echo '>' . $k . '</option>';
}
echo '</select>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'Zip Code: ';
echo '<input type="text" name="ZipCode" value="';
echo $CRM['ZipCode'];
echo '" size="10">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

$CP = json_decode($CRM['CompanyPhone'], true);

echo 'Company Phone: ';
echo '(<input type="text" name="PhoneAreaCode" value="';
echo $CP[0];
echo '" size="3">)&nbsp;';
echo '<input type="text" name="PhonePrefix" value="';
echo $CP[1];
echo '" size="3">&nbsp;-&nbsp;';
echo '<input type="text" name="PhoneSuffix" value="';
echo $CP[2];
echo '" size="4">';
echo '<br>';

echo '<hr size="1">';

for ($x = 1; $x <= 3; $x++) {

echo 'Contact: ';
echo '<input type="text" name="Contact'.$x.'" value="';
echo $C['Contact' . $x];
echo '" size="30">';
echo '&nbsp;&nbsp;';

echo 'Role: ';
echo '<input type="text" name="Role'.$x.'" value="';
echo $C['Role' . $x];
echo '" size="25">';
echo '&nbsp;&nbsp;';

echo 'Phone: ';
echo '<input type="text" name="Phone'.$x.'" value="';
echo $C['Phone' . $x];
echo '" size="15">';
echo '&nbsp;&nbsp;';

echo 'Email: ';
echo '<input type="text" name="Email'.$x.'" value="';
echo $C['Email' . $x];
echo '" size="35">';


if (($REPORTACTIVE == "Y") && ($C['Contact' . $x] != "") && ($C['Email' . $x] != "")) {

    echo '&nbsp;&nbsp;&nbsp;&nbsp;';
    echo '<a href="#openReport" onclick="loadWOTCSendReport(\''.$CRM['CompanyID'].'\',\''.$x.'\',\'\');"><img src="'.IRECRUIT_HOME.'images/icons/email.png" title="Send Report" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Send Report</b></a>';

}

echo '<br>';

}

echo '<hr size="1">';

echo '<div style="line-height:300%;">';
echo 'Assignment: ';
echo '<select name="Assignment">';
echo '<option value="">Please Select</option>';
foreach ($ASSIGNMENT AS $A) {
        echo '<option value="' . $A . '"';
        if ($CRM['Assignment'] == $A) { echo ' selected';  }
        echo '>' . $A . '</option>';
}
echo '</select>';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

$QB = array("Y"=>"Yes", "N"=>"No");

echo 'QB: ';
echo '<select name="QB">';
echo '<option value="">Please Select</option>';
foreach ($QB AS $v=>$k) {
        echo '<option value="' . $v . '"';
        if ($CRM['QB'] == $v) { echo ' selected';  }
        echo '>' . $k . '</option>';
}
echo '</select>';
echo '<br>';

echo "Industry: ";
echo "<select name=\"Industry\">";
echo "<option value=\"\">None</option>";

$INDUSTRIES = G::Obj('WOTCAdmin')->getIndustries();

foreach ($INDUSTRIES as $ind) {
   echo "<option value=\"" . $ind['Industry'] . "\"";
   if ($CRM['Industry'] == $ind['Industry']) { echo " selected"; }
   echo ">" . $ind['Industry'] . "</option>\n";
} // end foreach
echo "</select>\n";

echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo 'Rate: ';
echo '<input type="text" name="Rate" value="';
echo $CRM['Rate'];
echo '" size="2"> %';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo '$etup Fee: ';
echo '<input type="text" name="SetupFee" value="';
echo number_format($CRM['SetupFee'], 2, '.', ',');
echo '" size="6">';

if (($CRM['Assignment'] == "Business Partners") || ($CRM['Assignment'] == "BP Clients")) {
echo '<br>';
echo 'BP Rate: ';
echo '<input type="text" name="BPRate" value="';
echo $CRM['BPRate'];
echo '" size="2"> %';
}

if ($CRM['Assignment'] != "Business Partners") {
echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo 'Partner: ';
echo '<select name="Partner">';
echo "<option value=\"\">None</option>";

$columns = 'CompanyID, CompanyName';
$where = array("Assignment = 'Business Partners'","Active = 'Y'");
$params = array();
$RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', 'CompanyName', array($params));
$PARTNERS = $RESULTS['results'];

foreach ($PARTNERS AS $BP) {
   echo '<option value="' . $BP['CompanyID'] . '"';
   if ($CRM['Partner'] == $BP['CompanyID']) { echo " selected"; }
   echo '>' . $BP['CompanyName'] . '</option>';
}
echo '</select>';
} // end Business Partners

echo '</div>';

if (($CRM['Assignment'] == "Business Partners") && ($CRM['CompanyID'] != "")) {

$columns = 'CompanyID, CompanyName';
$where = array("Partner = :Partner","Active = 'Y'");
$params = array(":Partner"=>$CRM['CompanyID']);
$RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$DATA = $RESULTS['results'];

echo '<hr size="1">';
echo '<div style="padding-left:20px;line-height:160%;">';
echo '<b>Partners:</b><br>';

echo "<div style=\"margin-right:100px;line-height:220%;float:right;\">\n";

if ($CRM['Logo'] != "") {
echo "<img src=\"" . WOTC_HOME . "images/logos/" . $CRM['Logo'] . "\" style=\"max-height:100px;\">";
echo "&nbsp;&nbsp;&nbsp;&nbsp;";

echo "<a href=\"" . ADMIN_HOME . "index.php?pg=wotccrm&CompanyID=" . $CRM['CompanyID'] . "&Action=remove_logo\"";
echo " onclick=\"return confirm('Are you sure you want to delete this logo?\\n\\n')\"";
echo ">";
echo "<img src=\"" . ADMIN_HOME . "images/cross.png\" border=\"0\" title=\"Delete\" style=\"margin:0px 3px -4px 0px;\"><b style=\"FONT-SIZE:8pt;COLOR:#000000\">Delete Logo</b>";
echo "</a>\n";

echo "<br>";
echo "Replace Logo Image: ";

} else {
  echo "Add Logo Image: ";
}
echo "<input type=\"file\" name=\"logo\">";
echo "</div>\n";

if (count($DATA) == 0) { echo '&nbsp;&nbsp;&nbsp;Currently there are no companies assigned to this business partner.'; }
foreach ($DATA AS $P) {

	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<a href="' . ADMIN_HOME . 'index.php?pg=wotccrm&CompanyID=' . $P['CompanyID'] . '">';
        echo $P['CompanyName'] . '</a><br>';

} // end foreach
echo '</div>';

echo '<div style="clear:both;"></div>';

echo '<div style="margin-top:10px;min-height: 150px;overflow-y:auto;line-height:260%;padding:6px;background-color:#efefef;">';

} 

// Begin Navigation Links
if ($CRM['CompanyID'] != "") {
echo '<div style="max-height: 150px;overflow-y:auto;line-height:260%;padding:6px;background-color:#efefef;">';


// Row One
if ($CRM['Assignment'] != "Business Partners") {

echo '<a href="#openDocuments" onclick="updateWOTCIDs(\'' . $CRM['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Assign/Edit wotcID\'s" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Assign/Edit wotcID\'s</b></a>';

echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openDocuments" onclick="parentConfig(\'' . $CRM['CompanyID'] . '\',\'Form\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Parent Config" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Parent Configuration</b></a>';

echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openDocuments" onclick="updateFEIN(\'' . $CRM['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Update FEIN AKA/DBA - POA" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Update FEIN AKA/DBA - POA</b></a>';

} // end Business Partners

if (($CRM['Assignment'] == "Business Partners") || (count(G::Obj('WOTCcrm')->getWOTCIDs($CRM['CompanyID'])) > 0 )) {

if ($CRM['Assignment'] != "Business Partners") {
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
}

echo '<a href="#openDocuments" onclick="userAccess(\'' . $CRM['CompanyID'] . '\');">';
echo '<img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="User Access" style="margin:0px 2px -4px 0px;" border="0">';
echo '<b style="FONT-SIZE: 8pt; COLOR: #000000">User Access</b></a>';

// Row Two
if ($CRM['Assignment'] != "Business Partners") {
echo '<br>';
echo '<a href="' . WOTCADMIN_HOME . 'adminAccess.php?uid=' . $_COOKIE['AID'] . '&cid=' . $_REQUEST['CompanyID'] . '" target="_blank">';
echo '<img src="'.IRECRUIT_HOME.'images/icons/html.png" title="Admin Portal" style="margin:0px 2px -4px 0px;" border="0">';
echo '<b style="FONT-SIZE: 8pt; COLOR: #000000">Go to WOTC Admin Portal</b></a>';

echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="'.ADMIN_HOME.'index.php?pg=wotc_administration_cms&ParentID='.$CRM['CompanyID'].'" target="_blank"><img src="' . IRECRUIT_HOME . 'images/icons/application_form_add.png" border="0" title="Process Data" style="margin: 0px 3px -4px 0px;"><b style="FONT-SIZE: 8pt; COLOR: #000000">Process Data</b></a>';
} // end Business Partners

} // end WOTCIDs

if ($REPORTACTIVE == "Y") {

echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openReport" onclick="loadWOTCReport(\''.$CRM['CompanyID'].'\');"><img src="'.IRECRUIT_HOME.'images/icons/page_white_text.png" title="Status Report" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Status Report</b></a>';

echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openReport" onclick="loadBillingReport(\''.$CRM['CompanyID'].'\');"><img src="'.IRECRUIT_HOME.'images/icons/page_white_text.png" title="Realized Credit Details" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Realized Credit Details</b></a>';

echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openReport" onclick="loadAwaitingPayrollReport(\''.$CRM['CompanyID'].'\');"><img src="'.IRECRUIT_HOME.'images/icons/page_white_text.png" title="Awaiting Payroll Data" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Awaiting Payroll Data</b></a>';

} // end REPORTACTIVE

echo '</div>';
} // end if CompanID
// end Links

echo '<hr size="1">';

echo '<div>';
echo '<strong>Notes:</strong><br>';
echo '<textarea name="Notes" cols="120" rows="5">';
echo '</textarea>';
if ($CRM['CompanyID'] != "") {
echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<a href="#openDocuments" onclick="loadHistory(\'' . $CRM['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/page_white_put.png" title="Note History" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Note History</b></a>';
}
echo '</div>';

echo '<div>';
echo '<input type="submit" value="Submit Changes">';
echo '</div>';
echo '</form>';

echo '</div>';

if ($CRM['CompanyID'] != "") {
echo '<div style="margin-top:10px;width:100%;text-align:right;">';
echo '<a href="'.ADMIN_HOME.'index.php?pg=wotccrm&delete='.$CRM['CompanyID'].'" onclick="return confirm(\'Are you sure you want to delete the following company information?\n\n' . preg_replace("/'/", "\'", $CRM['CompanyName']) . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin: 0px 3px -4px 0px;"><b style="FONT-SIZE: 8pt; COLOR: #000000">Delete Company</b></a>';
echo '</div>';
}
echo '</div>';

$LST   =   array("POAStartDate","POAEndDate","DatePaid");
echo G::Obj('DateHelper')->dynamicCalendar($LST,'modal');

?>
