<?php 
$cnt        =   $_POST['cnt'];
$Question   =   $_POST['Question'];
$sort       =   $_POST['sort'];

//Bind Parameters
$params     =   array(
                    ':WotcID'       =>  $_REQUEST['WotcID'],
                    ':WotcFormID'   =>  $_REQUEST['WotcFormID'],
                    ':QuestionID'   =>  $_REQUEST['QuestionID'],
                    ':Question'     =>  $_REQUEST['Question']
                );
$set_info   =   array ();

if(isset($_REQUEST['ddlPrefilledQueData']) && $_REQUEST['ddlPrefilledQueData'] != "") {

    if($_REQUEST['ddlPrefilledQueData'] ==  "States")
    {
        $address_states_results =   $AddressObj->getAddressStateList();
        $address_states_count   =   $address_states_results['count'];
        $address_states         =   $address_states_results['results'];

        $address_states_list    =   array();
        $address_states_list[]  =   "Select";
        $answers_info[]         =   "" . ":" . "Select";
        for($asl = 0; $asl < $address_states_count; $asl++) {
            $address_states_list[$address_states[$asl]['Abbr']] = $address_states[$asl]['Description'];
            if($address_states[$asl]['Description'] != "") {
                $answers_info[] =  $address_states[$asl]['Abbr'] . ':' . $address_states[$asl]['Description'];
            }
        }
    }
    else if($_REQUEST['ddlPrefilledQueData'] ==  "Days")
    {
        $days_list              =   G::Obj('QuestionPreFilledInfo')->getDays();
        $answers_info[]         =   "" . ":" . "Select";
        foreach($days_list as $day_key=>$day_value) {
            if($day_value != "") {
                $answers_info[] =   $day_key . ':' . $day_value;
            }
        }
    }
    else if($_REQUEST['ddlPrefilledQueData'] ==  "Months")
    {
        $months_list            =   G::Obj('QuestionPreFilledInfo')->getMonths();
        $answers_info[]         =   "" . ":" . "Select";
        foreach($months_list as $month_key=>$month_value) {
            if($month_value != "") {
                $answers_info[] =   $month_key . ':' . $month_value;
            }
        }
    }
    else if($_REQUEST['ddlPrefilledQueData'] == 'SocialMedia') {
        
        $social_media           =   G::Obj('QuestionPreFilledInfo')->getSocialMedia();
        $answers_info[]         =   "" . ":" . "Select";
        foreach($social_media as $socialmedia_key=>$socialmedia_value) {
            if($socialmedia_value != "") {
                $answers_info[] =   $socialmedia_key . ':' . $socialmedia_value;
            }
        }
    }

    $question_value = implode("::", $answers_info);
}
else {
    $ii     =   0;

    $question_value =   "";
    for($i = 1; $i <= $cnt; $i++) {

        $v  =   'value-' . $i;
        $d  =   'display-' . $i;
    
        if ($_POST [$d] != '') {
            
            $question_value .= $_POST [$v] . ':' . $_POST [$d] . '::';
            $ii ++;
            
            $data [] = array (
                'value' =>  $_POST [$v],
                'name'  =>  $_POST [$d]
            );
        }
    }

    if ($sort == 'Y') {
    
        if($_REQUEST ['QuestionTypeID'] != "100") {
            $question_value = "";
            	
            foreach ( $data as $key => $row ) {
                if($row ['name'] == "Select") $row ['name'] = "1Select";
                $value [$key] = $row ['value'];
                $name [$key] = $row ['name'];
            }
            array_multisort ( $name, SORT_ASC, $data );
            	
            foreach ( $data as $key => $row ) {
                if($row ['name'] == "1Select")  $row ['name'] = "Select";
                $question_value .= $row ['value'] . ':' . $row ['name'] . '::';
                $ii ++;
            }
        }
    
    }
    
    if ($ii > 0) {
        $question_value =   substr ( "$question_value", 0, - 2 );
    }
    
    if ($_REQUEST ['QuestionTypeID'] == "100") {
        $question_answers ['RVal'] = $_REQUEST ['RVal'];
        $question_answers ['LabelValRow'] = $_REQUEST ['LabelValRow'];
    
        if ($sort == 'Y') {
            //Sort based on alphabetical order
            asort($question_answers ['LabelValRow']);
    
            if(is_array($question_answers ['LabelValRow'])) {
                $q100_i = 1;
                foreach($question_answers ['LabelValRow'] as $lable_key_name=>$label_val_name) {
                    $question_answers ['LabelValRow']["LabelValRow".$q100_i] = $label_val_name;
    
                    $q100_i++;
                }
            }
        }
    
        if (is_array ( $question_answers )) {
            $question_value = serialize ( $question_answers );
        }
    
        if ($_REQUEST ['rows'] != "" && $_REQUEST ['cols'] != "") {
            $params [':rows'] = $_REQUEST ['rows'];
            $params [':cols'] = $_REQUEST ['cols'];
            $set_info [] = "rows = :rows";
            $set_info [] = "cols = :cols";
        }
    }
    
    if ($_REQUEST ['QuestionTypeID'] == "120") {
        $question_answers ['day_names'] = $_REQUEST ['day_names'];
        if (is_array ( $question_answers )) {
            $question_value = serialize ( $question_answers );
        }
    
        if ($_REQUEST ['number_of_days'] != "") {
            $params [':rows'] = $_REQUEST ['number_of_days'];
            $set_info [] = "rows = :rows";
        }
    }
} 
	
//Bind Parameters
$params[':value']           =   $question_value;
$params[':defaultValue']    =   $_POST[$_POST['default']];  //Set Default Value

//Set the parameters those are going to update
$set_info[]                 =   "Question       =   :Question";
$set_info[]                 =   "value          =   :value";
$set_info[]                 =   "defaultValue   =   :defaultValue";

//set where condition
$where                      =   array("WotcID = :WotcID", "WotcFormID = :WotcFormID", "QuestionID = :QuestionID");

//Update the table information based on bind and set values
$res_que_info               =   G::Obj('WOTCFormQuestions')->updWotcFormQuestionsInfo($set_info, $where, array($params));

echo '<script language="JavaScript" type="text/javascript">';
echo "alert('Update completed!')";
echo '</script>';
?>