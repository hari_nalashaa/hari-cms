<?php
set_time_limit(360);

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Today's Results")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('FEDID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('SSN');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('EmployeeID');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Position');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('State');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Hourly Wage');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('LastHired Date');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Termination Date');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('EmployeeName');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('LastModified');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);

G::Obj('GenericQueries')->conn_string   =   "WOTC";

$query = "SELECT * FROM DycomHires";
$APPLICANTDATA = G::Obj('GenericQueries')->getInfoByQuery($query);

$n  =   2;
foreach ($APPLICANTDATA as $AD) {
  
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($AD['FEDID']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($AD['SSN']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($AD['EmployeeID']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($AD['Position']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($AD['State']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($AD['HourlyWage']);
$objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($AD['LastHiredDate']);
$objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($AD['TerminationDate']);
$objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($AD['EmployeeName']);
$objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($AD['LastModified']);

    $n++;
} // end foreach

// Redirect output to a client's web browser (Excel5)

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="exportDycomHires.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
