<div style="font-size:14pt;font-weight:bold;margin-bottom:20px;">State Portals</div>
<?php

if ($_REQUEST['process'] == 'Y') {

$skip   =   array("State");
$info   =   array(
                 "State"             =>  $_REQUEST['state'],
                 "StatePortalURL"    =>  $_REQUEST['StatePortalURL'],
                 "UserID"    	     =>  $_REQUEST['UserID'],
                 "Password"    	     =>  $_REQUEST['Password'],
                 "RepID"    	     =>  $_REQUEST['RepID'],
                 "Email"    	     =>  $_REQUEST['Email'],
                 "POAContact"        =>  $_REQUEST['POAContact'],
                 "POAContactType"    =>  $_REQUEST['POAContactType'],
                 "SecurityAnswer"    =>  $_REQUEST['SecurityAnswer'],
                 "Notes"    	     =>  $_REQUEST['Notes']
                 );

G::Obj('WOTCStatePortals')->insUpdStatePortals($info, $skip);


?>
<script>
setTimeout(function() { $(message).html('<span style="color:red;font-style:italic;">State portal successfully updated.</span>'); }, 500);
</script>
<?php
} // end processing

$STATES =   G::Obj('WOTCStatePortals')->getStateList ();
$CONTACTTYPES = array('Email','Upload','Mail');


$columns = '*';
$where = array("State = :State");
$params = array(":State"=>$_REQUEST['state']);
$results = G::Obj('WOTCStatePortals')->getStatePortalsInfo($columns, $where, '', '', array($params));
$PORTAL = $results['results'][0];


if ($results['count'] > 0) {

echo '<div style="border:2px solid #dddddd;background:#dddddd;padding:10px;width:800px;margin: 0 20px;min-width:700px;">';

echo '<strong style="font-size:12pt;">' . $STATES[$PORTAL['State']] . '</strong>';
echo '<font id="message" style="margin-left:10px;"></font>';
echo '<hr size="1">';

echo '<form method="post" action="index.php">';
echo '<input type="hidden" name="pg" value="state_portals">';
echo '<input type="hidden" name="state" value="' . $PORTAL['State']. '">';
echo '<input type="hidden" name="process" value="Y">';
echo '<div style="border:1px solid #aaaaaa;background:#efefef;padding:10px;line-height:250%;">';

if ($PORTAL['StatePortalURL'] != '') {
  echo '<a href="' . $PORTAL['StatePortalURL'] . '" target="_blank" style="text-decoration:underline;color:blue;">';
}
echo 'Bulk Processing:';
if ($PORTAL['StatePortalURL'] != '') {
  echo '</a>';
}
echo ' URL: ';
echo '<input type="text" name="StatePortalURL" value="';
echo $PORTAL['StatePortalURL'];
echo '" size="65">';
echo '<br>';

echo 'User: ';
echo '<input type="text" name="UserID" value="';
echo $PORTAL['UserID'];
echo '" size="30">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'Password: ';
echo '<input type="text" name="Password" value="';
echo $PORTAL['Password'];
echo '" size="20">';
echo '<br>';

echo 'Representative: ';
echo '<input type="text" name="RepID" value="';
echo $PORTAL['RepID'];
echo '" size="30">';
echo '&nbsp;&nbsp;&nbsp;&nbsp;';

echo 'Email: ';
echo '<input type="text" name="Email" value="';
echo $PORTAL['Email'];
echo '" size="30">';
echo '<br>';

echo 'Security Answer: ';
echo '<input type="text" name="SecurityAnswer" value="';
echo $PORTAL['SecurityAnswer'];
echo '" size="40">';

echo '<hr size="1">';

echo '<table><tr><td valign="top">';
echo 'POA Type: ';
echo '<select name="POAContactType">';
foreach ($CONTACTTYPES AS $CT) {
	echo '<option value="' . $CT . '"';
	if ($PORTAL['POAContactType'] == $CT) { echo ' selected';  }
	echo '>' . $CT . '</option>';
}
echo '</select>';
echo '</td><td valign="top">';
echo '&nbsp;&nbsp;';
if (($PORTAL['POAContactType'] == 'Upload') || ($PORTAL['POAContactType'] == 'Email')) {
  echo '<a href="';
  if ($PORTAL['POAContactType'] == 'Email') {
	  echo 'mailto:';
  }
  echo $PORTAL['POAContact'] . '" target="_blank" style="text-decoration:underline;color:blue;">';
}
echo 'Information: ';
if (($PORTAL['POAContactType'] == 'Upload') || ($PORTAL['POAContactType'] == 'Email')) {
  echo '</a>';
}
echo '</td><td valign="top">';
echo '<textarea name="POAContact" cols="45" rows="6">';
echo $PORTAL['POAContact'];
echo '</textarea>';
echo '</td></tr>';
echo '</table>';

echo '<hr size="1">';

echo 'Notes:<br>';
echo '<textarea name="Notes" cols="80" rows="5">';
echo $PORTAL['Notes'];
echo '</textarea>';

echo '<br>';
echo '<input type="submit" value="Submit Changes">';

echo '</div>';
echo '</form>';
echo '</div>';
}



if ($_REQUEST['state'] == "") {


	$map = '
<p><map name="FPMap0">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=WA" shape="polygon" coords="71, 66, 91, 70, 97, 83, 179, 95, 195, 32, 111, 8, 75, 14, 71, 61, 87, 67, 87, 68">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=OR" shape="polygon" coords="37, 160, 42, 132, 71, 65, 90, 87, 108, 92, 134, 95, 176, 100, 183, 110, 163, 137, 167, 143, 157, 185">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=CA" shape="polygon" coords="37, 166, 109, 183, 90, 250, 170, 377, 154, 404, 113, 403, 62, 399, 44, 349, 36, 290, 28, 248, 23, 222, 20, 200, 25, 183">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=AK" shape="polygon" coords="112, 458, 70, 479, 56, 489, 29, 508, 16, 543, 61, 580, 0, 606, 6, 631, 105, 613, 138, 581, 165, 588, 197, 622, 215, 635, 248, 622, 175, 555, 172, 478, 115, 458">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=HI" shape="polygon" coords="243, 533, 242, 571, 280, 569, 306, 605, 357, 637, 386, 603, 324, 550">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=ID" shape="polygon" coords="199, 35, 183, 100, 188, 112, 171, 138, 171, 147, 164, 186, 257, 206, 265, 154, 240, 153, 237, 151, 227, 124, 217, 123, 220, 101, 206, 72, 203, 56, 210, 39, 203, 33, 204, 34">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NV" shape="polygon" coords="183, 335, 174, 332, 169, 357, 95, 250, 114, 181, 205, 201">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=UT" shape="polygon" coords="276, 333, 190, 317, 211, 204, 258, 211, 255, 234, 286, 241">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=AZ" shape="polygon" coords="191, 324, 275, 335, 255, 458, 223, 455, 156, 413, 178, 378, 171, 365, 176, 344, 193, 334">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MT" shape="polygon" coords="212, 37, 216, 79, 229, 97, 223, 114, 234, 117, 235, 132, 239, 142, 264, 148, 272, 135, 381, 151, 389, 64">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=WY" shape="polygon" coords="378, 248, 262, 232, 274, 143, 385, 156">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=CO" shape="polygon" coords="403, 343, 283, 330, 293, 242, 408, 253">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=ND" shape="polygon" coords="394, 68, 388, 134, 505, 136, 496, 71, 397, 65">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=SD" shape="polygon" coords="389, 139, 504, 141, 507, 157, 504, 192, 504, 212, 473, 204, 385, 201, 391, 142">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NE" shape="polygon" coords="523, 276, 411, 271, 414, 248, 384, 246, 387, 208, 474, 211, 490, 216, 499, 216, 508, 224">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=KS" shape="polygon" coords="540, 348, 407, 341, 412, 276, 525, 281, 531, 285, 537, 300">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=OK" shape="polygon" coords="544, 423, 540, 352, 392, 345, 392, 357, 444, 358, 440, 400, 458, 407, 472, 413, 498, 417, 514, 421">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NM" shape="polygon" coords="376, 455, 386, 342, 281, 333, 261, 455, 275, 459, 281, 448, 308, 450, 310, 441">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=TX" shape="polygon" coords="488, 606, 456, 592, 408, 510, 388, 507, 367, 529, 313, 456, 379, 459, 388, 365, 389, 363, 437, 362, 438, 406, 472, 421, 491, 425, 515, 427, 538, 427, 550, 434, 554, 462, 566, 481, 557, 515, 500, 552, 492, 578">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MN" shape="polygon" coords="620, 90, 532, 57, 522, 66, 503, 68, 511, 141, 512, 191, 595, 193, 560, 162, 570, 134, 617, 86, 605, 85, 609, 85">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=IA" shape="polygon" coords="595, 261, 589, 258, 526, 263, 509, 215, 508, 196, 594, 197, 598, 213, 607, 219, 614, 233, 603, 238, 599, 246, 600, 254">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MO" shape="polygon" coords="625, 355, 545, 358, 542, 299, 534, 275, 524, 268, 594, 269, 611, 303, 617, 302, 617, 318, 631, 328, 636, 344, 637, 350, 630, 368">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=AR" shape="polygon" coords="613, 434, 559, 433, 552, 427, 550, 422, 549, 364, 623, 362, 626, 373, 626, 389, 616, 408">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=LA" shape="polygon" coords="663, 528, 628, 529, 609, 515, 565, 513, 572, 480, 559, 460, 557, 436, 611, 437, 615, 452, 604, 477, 608, 478, 642, 482, 666, 529">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=WI" shape="polygon" coords="603, 126, 598, 114, 580, 134, 571, 143, 567, 161, 602, 188, 608, 210, 652, 210, 668, 143, 648, 164, 645, 142">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=IL" shape="polygon" coords="662, 232, 656, 214, 610, 212, 620, 226, 614, 240, 606, 241, 604, 249, 600, 264, 620, 300, 621, 314, 634, 326, 639, 337, 653, 336, 657, 321, 669, 301, 666, 293">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MS" shape="polygon" coords="670, 492, 668, 450, 668, 390, 633, 389, 622, 412, 620, 450, 615, 476, 643, 478, 654, 496, 673, 492, 670, 486, 672, 482">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=AL" shape="polygon" coords="685, 498, 676, 487, 670, 390, 715, 383, 729, 444, 730, 471, 683, 476">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MI" shape="polygon" coords="737, 219, 749, 193, 712, 107, 646, 89, 636, 78, 618, 91, 606, 116, 645, 136, 653, 153, 673, 134, 673, 186, 681, 206, 671, 229">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=IN" shape="polygon" coords="712, 288, 705, 229, 669, 234, 674, 300, 665, 320, 682, 318, 695, 313, 706, 296, 716, 285">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=KY" shape="polygon" coords="750, 341, 668, 346, 646, 349, 651, 341, 666, 322, 692, 318, 718, 290, 757, 295, 770, 315, 752, 337">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=OH" shape="polygon" coords="783, 212, 790, 252, 763, 291, 754, 287, 720, 285, 712, 226, 736, 223, 759, 226">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=TN" shape="polygon" coords="737, 380, 633, 385, 639, 355, 666, 351, 780, 338, 736, 374">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NY" shape="polygon" coords="800, 196, 803, 176, 847, 162, 842, 143, 864, 117, 888, 117, 901, 167, 902, 207, 933, 196, 928, 215, 906, 218, 895, 204, 872, 188, 798, 202, 801, 192, 801, 174, 803, 174">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=PA" shape="polygon" coords="886, 234, 870, 243, 798, 257, 787, 211, 793, 201, 798, 207, 872, 194, 883, 205, 875, 219">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=WV" shape="polygon" coords="790, 322, 777, 316, 767, 294, 795, 259, 839, 253, 848, 264, 836, 262, 821, 285, 807, 308">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=VA" shape="polygon" coords="885, 314, 761, 336, 776, 317, 790, 326, 815, 308, 837, 271, 847, 271, 853, 283, 872, 285">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NC" shape="polygon" coords="862, 388, 853, 383, 824, 363, 800, 362, 770, 365, 761, 373, 745, 373, 785, 338, 886, 318, 908, 339, 858, 387, 850, 381, 845, 379">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=SC" shape="polygon" coords="815, 442, 851, 390, 819, 368, 774, 370, 764, 379">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=GA" shape="polygon" coords="808, 470, 744, 472, 722, 388, 764, 379, 814, 449">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=FL" shape="polygon" coords="822, 615, 864, 583, 852, 539, 815, 473, 741, 476, 689, 481, 694, 494, 719, 490, 740, 503, 760, 494, 787, 516, 792, 541, 803, 562, 828, 587, 834, 599, 817, 617">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=ME" shape="polygon" coords="919, 102, 943, 148, 989, 97, 976, 74, 946, 39, 939, 45, 930, 76, 923, 102, 923, 104">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=VT" shape="polygon" coords="890, 117, 906, 165, 913, 128, 913, 107, 902, 69, 877, 72, 889, 120, 894, 115">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NH" shape="polygon" coords="915, 163, 940, 153, 920, 107">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MA" shape="polygon" coords="906, 173, 915, 177, 938, 173, 949, 187, 969, 177, 962, 163, 946, 157, 907, 168, 910, 172, 913, 175, 917, 174">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=CT" shape="polygon" coords="905, 187, 908, 203, 931, 195, 929, 179, 909, 183">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=RI" shape="polygon" coords="935, 182, 951, 220, 978, 212, 971, 204, 946, 192, 934, 186, 940, 185">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=NJ" shape="polygon" coords="886, 212, 883, 219, 893, 230, 887, 245, 899, 258, 905, 235, 947, 235, 947, 224, 930, 221, 914, 221, 887, 208, 884, 215">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=DC" shape="circle" coords="924, 323, 15">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MD" shape="polygon" coords="843, 255, 860, 277, 876, 281, 888, 300, 894, 287, 874, 249, 846, 253, 848, 258">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=MD" shape="circle" coords="926, 291, 14">
<area href="' . ADMIN_HOME . 'index.php?pg=state_portals&state=DE" shape="circle" coords="929, 259, 16">
</map>
<img src="images/WOTC_US_ImageMap.png" alt="United States Map with WOTC State Workforce Agency Links" usemap="#FPMap0" width="1000" height="648" border="0"></p>';
echo $map;

} else {
	echo '<div style="width:800px;margin:10px 20px 0 20px;line-height:140%;text-align:center;font-size:10pt;">';
	echo '<button style="font-size:8pt;font-face:italic;padding: 1px 5px;margin-left:20px;" onclick="window.location.href=\'' . ADMIN_HOME . '/index.php?pg=state_portals\';">Select State</button>';
	echo '</div>';
}



?>
