#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/..') . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 300);

$ALPHA = range('A', 'Z');
$ASSIGNMENT=array("Pat's List","CFC");

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("iRecruit WOTC")
       ->setLastModifiedBy("System")
       ->setTitle("Client Export")
       ->setSubject("Excel")
       ->setDescription("Export File")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

    $CRMKEYS[]="Active";
    $CRMKEYS[]="Industry";
    $CRMKEYS[]="CompanyName";
    $CRMKEYS[]="Contacts";

    $CRMKEYSSTATUS[]="Screened";	
    $CRMKEYSSTATUS[]="Last Screen Date";
    $CRMKEYSSTATUS[]="Savings";
    $CRMKEYSSTATUS[]="WEB";
    $CRMKEYSSTATUS[]="CALL CENTER";
    $CRMKEYSSTATUS[]="PAPER";

    $CRMKEYS=array_merge($CRMKEYS,$CRMKEYSSTATUS);

$sheet=0;
foreach ($ASSIGNMENT AS $assignment) {

  // Create a new worksheet, after the default sheet
  if ($sheet > 0) { $objPHPExcel->createSheet($sheet); }

  // Add some data to the second sheet, resembling some different data types
  $objPHPExcel->setActiveSheetIndex($sheet);

  $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getFill()->getStartColor()->setRGB('0c00ac');
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getAlignment()->setHorizontal('center');
  $objPHPExcel->getActiveSheet()->getStyle('A1:'.$ALPHA[count($CRMKEYS)-1].'1')->getAlignment()->setVertical('center');


  foreach ($CRMKEYS AS $k=>$v) {

        $objPHPExcel->getActiveSheet()->getCell($ALPHA[$k].'1')->setValue($v);

  } // end foreach CRMKEYS

  for ($i = 'A'; $i != $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
  }
  $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);

  $where=array('Assignment = :Assignment');
  $params=array(":Assignment"=>$assignment);
  $results = G::Obj('WOTCcrm')->getCRM('CompanyID, Active, Industry, CompanyName, Contacts', $where, '', 'CompanyName', array($params));

  $n=2;
  $C=array();
  foreach ($results['results'] AS $C) {  // Assigned Data
      

	  foreach ($CRMKEYS AS $key=>$value) { // key

	      $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setVertical('top');

		  $V="";
		  if ($value == "Contacts") {

		    if ($C[$value] != "") {

		      $CONTACTS=json_decode($C[$value]);

	 	      for ($x=1; $x<=3; $x++) {
			       $Contact='Contact' . $x;
			       $Phone='Role' . $x;
			       $Phone='Phone' . $x;
			       $Email='Email' . $x;
                          if ($CONTACTS->$Contact != "") {
                                  $V .= $CONTACTS->$Contact;
                                  $V .= " - ";
                          }
                          if ($CONTACTS->$Role != "") {
                                  $V .= $CONTACTS->$Role;
                                  $V .= " - ";
                          }
                          if ($CONTACTS->$Phone != "") {
                                  $V .= $CONTACTS->$Phone;
                                  $V .= " - ";
                          }
                          if ($CONTACTS->$Email != "") {
				  $V .= $CONTACTS->$Email;
			  }
                          $V .= "\r";


		      } // end for loop
                    } // end if value

		    $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } else {
		      
	     		$V=$C[$value];
	     		
		  }

		  if (in_array($value, $CRMKEYSSTATUS)) {

		      $V="";
		      $xx=1;
      		      $STATUSES = G::Obj('WOTCcrm')->getStatuses($C['CompanyID']);
		      foreach ($STATUSES AS $S) {
		          if ($value == "Savings") {
		              $savings=preg_replace("/[^0-9\.]/", '', $S[$value]);
		              $V.=number_format($savings, 2, '.', ',');
		              $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getNumberFormat()->setFormatCode("#,##0.00");
		          } else {

		            $V.=$S[$value];
		          }
		          
		          if ($xx < count($STATUSES)) {
		              $V.="\r";
		          }

              	      $xx++;
		      }
		      
		      $objPHPExcel->getActiveSheet()->getStyle($ALPHA[$key].$n)->getAlignment()->setWrapText(true);

		  } // end if in CRMKEYSTATUS

              $objPHPExcel->getActiveSheet()->getCell($ALPHA[$key].$n)->setValue($V);

	} // end foreach Key	  

  $n++;

  } // end foreach Assigned Data

  // Rename sheet
  $objPHPExcel->getActiveSheet()->setTitle($assignment);

$sheet++;
}

  $objPHPExcel->setActiveSheetIndex(0);

  $file = "Clien-Export.xlsx";
  $tempfile = ROOT . "cron/temp/" . $file;
  $objWriter= IOFactory::createWriter($objPHPExcel , 'Xlsx');
  $objWriter->save($tempfile);

  $message .= "Here is your report.<br><br>";
  $subject = 'Client Export';

  //Clear properties
  $PHPMailerObj->clearCustomProperties();
  $PHPMailerObj->clearCustomHeaders();

  // Set who the message is to be sent to
  $PHPMailerObj->addAddress( 'pmorrissey@cmswotc.com', 'Patrick Morrissey');
  $PHPMailerObj->addBCC( 'dedgecomb@irecruit-software.com', 'David Edgecomb');
  $PHPMailerObj->addBCC( 'skelly@cmswotc.com', 'Sean Kelly');

  // Set an alternative reply-to address
  $PHPMailerObj->setFrom ( 'cmswotc@cmswotc.com', 'WOTC' );

  // Set an alternative reply-to address
  $PHPMailerObj->addReplyTo ( 'cmswotc@cmswotc.com', 'WOTC' );

  // Set the subject line
  $PHPMailerObj->Subject = $subject;

  // Content Type Is HTML
  $PHPMailerObj->ContentType = 'text/html';

  // convert HTML into a basic plain-text alternative body
  $PHPMailerObj->msgHTML ( $message );

  $data = file_get_contents($tempfile);
  $PHPMailerObj->addStringAttachment($data, $file, 'base64', 'text/html');

  //Send email
  $PHPMailerObj->send ();

  unlink($tempfile);

?>
