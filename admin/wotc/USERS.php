<?php
require_once '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

$columns = 'CompanyID, APIKey, Assignment';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results2 = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results2['results'][0];

echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;margin-bottom:20px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">User Access</strong></div>";

if ($CRM['Assignment'] != "Business Partners") {
   echo '<a href="#openDocuments" onclick="processUser(\''.$_REQUEST['CompanyID'] .'\',\'new\',\'\',\'\');">';
   echo '<img src="'.IRECRUIT_HOME.'images/icons/add.png" title="Add User" style="margin:0px 2px -4px 0px;" border="0">';
   echo '<b style="FONT-SIZE: 8pt; COLOR: #000000">Add User</b></a>';
} else {
	echo '&nbsp;';
}
echo '</div>';

$where      =   array("CompanyID = :CompanyID");
$params     =   array(":CompanyID"=>$CRM['CompanyID']);
$USERS_RES  =   G::Obj('WOTCUsers')->getUsersInfo("*", $where, 'UserID', array($params));
$USERS      =   $USERS_RES['results'];

echo '<div style="clear:both;margin-bottom:20px;">';

if ($USERS_RES['count'] == 0) {
   echo "<div style=\"margin:20px 20px;line-height:180%;\">";
   if ($CRM['Assignment'] == "Business Partners") {
     echo "Business Partners do not have direct access to the WOTC Admin Portal.";
   } else {
     echo "There are no users associated with this company.";
   }
   echo "</div>\n";
} else {

  foreach ($USERS AS $USER) {

   echo "<div style=\"margin:20px 5px 20px 10px;line-height:180%;\">";

   echo "User: <b style=\"font-size:14pt;\">" . $USER['UserID'] . "</b>";
   echo "<span style=\"float:right;\">";
   echo "<a href=\"#openDocuments\" onclick=\"processUser('".$_REQUEST['CompanyID'] ."','edit','" . $USER['UserID'] . "','');\">";
   echo "<img src=\"" . ADMIN_HOME . "images/pencil.png\" border=\"0\" title=\"Change Info\" style=\"margin:0px 3px -4px 0px;\">";
   echo "<b style=\"FONT-SIZE:8pt;COLOR:#000000\">Edit User</b>";
   echo "</a>\n";
   echo "&nbsp;&nbsp;&nbsp;";
   echo "&#149;";
   echo "&nbsp;&nbsp;&nbsp;";
   echo "<a href=\"#openDocuments\" onclick=\"processUser('".$_REQUEST['CompanyID'] ."','delete','" . $USER['UserID'] . "','');\">";
   echo "<img src=\"" . ADMIN_HOME . "images/cross.png\" border=\"0\" title=\"Delete\" style=\"margin:0px 3px -4px 0px;\">";
   echo "<b style=\"FONT-SIZE:8pt;COLOR:#000000\">Delete User</b>";
   echo "</a>";
   echo "</span>";
   echo "<br>\n";

   echo "Activation Date: <b>" . $USER['ActivationDate'] . "</b>";
   echo "&nbsp;&nbsp;&nbsp;";
   echo "Last Accessed: <b>" . $USER['LastAccess'] . "</b>";
   echo '<br>';

   echo "Active Minutes: <b>" . $USER['Minutes'] . "</b>";
   echo "&nbsp;&nbsp;&nbsp;";
   if ($USER['Email'] == "") { $USER['Email'] = "N/A"; }
   echo "Email: <b>" . $USER['Email'] . "</b>";

   echo "</div>\n";

  } // end foreach

} // end if count

echo '</div>';


echo '<div style="text-align:center;">';
echo '</div>';

if ($CRM['Assignment'] == "Business Partners") {
// System Access
echo "<hr size=\"1\">";
echo '<div style="text-align:left;width:100%;background-color:#eeeeee;padding:4px;border-radius:5px;margin-bottom:20px;">';
echo "<strong style=\'font-size:12px;\'>System Access</strong> - For computer access to this companies data.";
echo '</div>';
echo '<div style="margin: 0 5px 20px 5px;">';

if ($CRM['APIKey'] != "") {

  echo "<strong>API Key</strong>: " . $CRM['APIKey'];

  echo "<span style=\"float:right;\">";
  if($CRM['APIKey'] == "109501969123119000163d00cdae9cb6") { // test key
    echo '<b>Test Key</b>';
  } else {
    echo '<a href="#openDocuments" onclick="apiKey(\'' . $CRM['CompanyID'] . '\',\'revoke\');">';
    echo "<img src=\"" . ADMIN_HOME . "images/cross.png\" border=\"0\" title=\"Revoke\" style=\"margin:0px 3px -4px 0px;\"><b style=\"FONT-SIZE:8pt;COLOR:#000000\">Revoke</b>";
    echo '</a>';
  }
  echo "</span>";

} else {

  echo "<strong>API Key</strong>: ";
  echo "<span style=\"float:right;\">";
  echo '<a href="#openDocuments" onclick="apiKey(\'' . $CRM['CompanyID'] . '\',\'generate\');">';
  echo "<img src=\"" . ADMIN_HOME . "images/add.png\" border=\"0\" title=\"Generate\" style=\"margin:0px 3px -4px 0px;\"><b style=\"FONT-SIZE:8pt;COLOR:#000000\">Generate Key</b>";
  echo '</a>';
  echo "</span>";

}
} // end Business Partners

echo '</div>';
?>
