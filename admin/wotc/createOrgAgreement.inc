<link rel="stylesheet" media="screen" href="wotc/css/screen.css">
<style>
	form.cmxform {
		width: 50em;
	}
	em.error {
		background:url("images/unchecked.gif") no-repeat 0px 0px;
		padding-left: 16px;
	}
	em.success {
		background:url("images/checked.gif") no-repeat 0px 0px;
		padding-left: 16px;
	}
	form.cmxform label.error {
		margin-left: auto;
		width: 250px;
	}
	em.error {
		color: black;
	}
	#warning {
		display: none;
	}
	#submit_button{padding:5px 10px;border-radius:3px;}
</style>
<?php 
if (isset($_POST['submit'])) {
    
    $post = $_POST;
    
    $skip   =   array('AgreementID');
    
    $now = date('Y-m-d H:i:s');
    $info   =   array(
                    'AgreementID'           =>  $_POST['AgreementID'],
                    'AgmtStartDate'         =>  date("Y-m-d",strtotime($_POST['AgmtStartDate'])),
                    'OrganizationName'      =>  $_POST['OrganizationName'], 
                    'DBA'                   =>  $_POST['DBA'],
                    'Address'               =>  $_POST['Address'], 
                    'City'                  =>  $_POST['City'], 
                    'State'                 =>  $_POST['State'], 
                    'ZipCode'               =>  $_POST['ZipCode'], 
                    'SetupFee'              =>  $_POST['SetupFee'], 
                    'ContingencyFee'        =>  $_POST['ContingencyFee'], 
                    'POAState'              =>  $_POST['POAState'],
                    'EIN1'                  =>  $_POST['EIN1'],
                    'EIN2'                  =>  $_POST['EIN2'],
                    'POAStartingDate'       =>  date("Y-m-d",strtotime($_POST['POAStartingDate'])),
                    'POAExpirationDate'     =>  date("Y-m-d",strtotime($_POST['POAExpirationDate'])),
                    'LastUpdated'           =>  $now
                );
    G::Obj('WOTCAgreements')->insUpdAgreementDataInfo($info, $skip);
    
    $MESSAGE = "Success";
    //header("location:" . ADMIN_HOME."index.php?pg=poa_agreements&update=".$MESSAGE,  true,  301 );  exit;
    echo "<script type='text/javascript'>window.top.location='". ADMIN_HOME."index.php?pg=poa_agreements&update=".$MESSAGE."';</script>"; exit;

} // end submit

// heading

$where_info		=	array("WotcID = 'MASTER'");
$wotc_frm_res	=	G::Obj('WOTCFormQuestions')->getWotcFormQuestionsInfo("WotcFormID", $where_info, "WotcFormID DESC");
$wotc_forms		=	$wotc_frm_res['results'];

// Get Address States List
$results =   $AddressObj->getAddressStateList ();
$STATES  =   $results['results'];
?>
<div style="font-size:12pt;font-weight:bold;margin-bottom:20px;">CMS WOTC Service Agreement</div>

<?php
if ($_REQUEST['AgreementID'] != "") {
    
    if ((!$ERROR) && ($_REQUEST['AgreementID'])) {
        $columns      =   "*";
        $ORGDATA      =   G::Obj('WOTCAgreements')->getAgreementsInfo($columns, $_REQUEST['AgreementID']);
    }// end not NEW
}
?>
<form id="AgreementForm" method="POST" action="index.php">
<input type="hidden" name="pg" value="create_agreement">
<?php if($_REQUEST["AgreementID"]){ ?>
<input type="hidden" name="AgreementID" value="<?php echo $_REQUEST["AgreementID"]; ?>">
<?php } ?>

<div style="margin-top:10px;">
<table border="0" cellspacing="3" cellpadding="3">

<?php
    $ASD   =   array("AgmtStartDate");
    echo G::Obj('DateHelper')->dynamicCalendar($ASD,'');
    
    if ($_POST['AgmtStartDate'] == "" && $ORGDATA['AgmtStartDate'] == "") { $_POST['AgmtStartDate'] = "mm/dd/yyyy"; }
    
    if ($_POST['POAStartingDate'] == "" && $ORGDATA['POAStartingDate'] == "") { $_POST['POAStartingDate'] = "mm/dd/yyyy"; }
    
    if ($_POST['POAExpirationDate'] == "" && $ORGDATA['POAExpirationDate'] == "" ) { $_POST['POAExpirationDate'] = "mm/dd/yyyy"; }
    
?>
<tr><td align="right">
Agreement Start Date:
</td><td>
<input id="AgmtStartDate" class="input-date" name="AgmtStartDate" data-msg-required="Please enter a Agreement Start Date in the format mm/dd/yyyy" type="text" value="<?php echo isset($_POST['AgmtStartDate']) ? htmlentities($_POST['AgmtStartDate']) : date('m/d/Y',strtotime($ORGDATA['AgmtStartDate'])); ?>" size="10" />
<span class="err"></span>
</td></tr>

<tr><td align="right" width="140">
Organization Name:
</td><td>
<input type="text" name="OrganizationName" id="OrganizationName" class="input-val" data-msg-required="Please enter the OrganizationName" value="<?php echo isset($_POST['OrganizationName']) ? htmlentities($_POST['OrganizationName']) : $ORGDATA['OrganizationName']; ?>" size="60" maxlength="60">
<span class="err"></span>
</td></tr>

<tr><td align="right">
DBA:
</td><td>
<input type="text" name="DBA" id="DBA" class="input-val" data-msg-required="Please enter the DBA" value="<?php echo isset($_POST['DBA']) ? htmlentities($_POST['DBA']) : $ORGDATA['DBA']; ?>" size="40" maxlength="45">
</td></tr>

<tr><td align="right">
Address:
</td><td>
<input type="text" name="Address" id="Address" class="input-val" data-msg-required="Please enter an Address" value="<?php echo isset($_POST['Address']) ? htmlentities($_POST['Address']) : $ORGDATA['Address']; ?>" size="40" maxlength="45">
</td></tr>

<tr><td align="right">
City:
</td><td>
<input type="text" name="City" id="City" class="input-val" data-msg-required="Please enter the City" value="<?php echo isset($_POST['City']) ? htmlentities($_POST['City']) : $ORGDATA['City']; ?>" size="30" maxlength="45">
</td></tr>

<tr><td align="right">
State:
</td><td>
<select size="1" name="State" id="State" class="input-val" data-msg-required="Please enter the State" style="max-width:300px;">
<option value="">Please Select
<?php
foreach ($STATES as $STATE) {
  if ($STATE['Abbr'] == $ORGDATA['State']) { $ckd = " selected"; } else { $ckd = ""; }
  echo "<option value=\"" . $STATE['Abbr'] . "\"" . $ckd . ">" . $STATE['Description'];
} // end foreach
?>
</select>
</td></tr>

<tr><td align="right">
<span style="color:<?php echo $color; ?>">Zip Code</span>:
</td><td>
<input type="text" name="ZipCode" id="ZipCode" class="input-val" data-msg-required="Please enter the Zip Code" value="<?php echo isset($_POST['ZipCode']) ? htmlentities($_POST['ZipCode']) : $ORGDATA['ZipCode']; ?>" size="10" maxlength="10">
</td></tr>

<tr><td align="right">
Setup Fee:
</td><td>
<input type="text" name="SetupFee" id="SetupFee" class="input-val" data-msg-required="Please enter the Setup Fee" value="<?php echo isset($_POST['SetupFee']) ? htmlentities($_POST['SetupFee']) : $ORGDATA['SetupFee']; ?>" size="10" maxlength="10">
</td></tr>

<tr><td align="right">
Contingency Fee:
</td><td>
<select name="ContingencyFee" id="ContingencyFee" class="input-val" data-msg-required="Please select the Contigency Fee">
	<option value="">Please Select</option>
	<?php 
	   $contingency_fee_arr = array(5,10,15,20,25,30);
	   foreach ($contingency_fee_arr AS $contingency_fee){ ?>
	      <option value="<?php echo $contingency_fee; ?>" <?php if($ORGDATA['ContingencyFee'] == $contingency_fee){ echo "Selected";} ?>><?php echo $contingency_fee; ?> %</option> 
	   <?php } ?>
	?>
</select>
</td></tr>

        <tr><td align="right">
        EIN #:
        </td><td>
        <input type="text" name="EIN1" id="EIN1" class="input-val" data-msg-required="Please enter the prefix EIN" value="<?php echo isset($_POST['EIN1']) ? htmlentities($_POST['EIN1']) : $ORGDATA['EIN1']; ?>" size="2" maxlength="5">-<input type="text" data-msg-required="Please enter the suffix EIN" name="EIN2" id="EIN2" class="input-val" value="<?php echo isset($_POST['EIN1']) ? htmlentities($_POST['EIN1']) : $ORGDATA['EIN2']; ?>" size="8" maxlength="20">
        </td></tr>
        <?php
            $LST1   =   array("POAStartingDate");
            echo G::Obj('DateHelper')->dynamicCalendar($LST1,'');
        ?>
        <tr><td align="right">
        POA Start Date:
        </td><td>
        <input id="POAStartingDate" name="POAStartingDate" class="input-date" data-msg-required="Please enter a POA Start Date in the format mm/dd/yyyy" type="text" value="<?php echo isset($_POST['POAStartingDate']) ? htmlentities($_POST['POAStartingDate']) : date('m/d/Y',strtotime($ORGDATA['POAStartingDate'])); ?>" size="10" />
        </td></tr>
        <tr><td align="right">
        <?php
            $POAED  =   array("POAExpirationDate");
            echo G::Obj('DateHelper')->dynamicCalendar($POAED,'');
        ?>
        POA End Date:
        </td><td>
        <input id="POAExpirationDate" name="POAExpirationDate" class="input-date" data-msg-required="Please enter a POA End Date in the format mm/dd/yyyy" type="text" value="<?php echo isset($_POST['POAExpirationDate']) ? htmlentities($_POST['POAExpirationDate']) : date('m/d/Y',strtotime($ORGDATA['POAExpirationDate'])); ?>" size="10" />
        </td></tr>
        <tr><td align="right">
        POA State:
        </td><td>
        <input type="text" name="POAState" id="POAState" class="input-val" data-msg-required="Please enter the POA State" value="<?php echo isset($_POST['POAState']) ? htmlentities($_POST['POAState']) : $ORGDATA['POAState']; ?>" size="10" maxlength="45"> KS and TX cannot be a list
        </td></tr>
         
        <tr>
        	<td>
        	</td>
        	<td height="30" valign="bottom">
        		<input type="submit" name="submit" id="submit_button" value="<?php echo 'Submit'; ?>">
        	</td>
        </tr>
        
        
        
        </table>
    </div>
</div>
</form>
<script src="wotc/javascript/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript">
	 $().ready(function() {
        
        $(".ui-datepicker-trigger").click(function(){
            $(this).prev().change(function(){
            	var get_id = $(this).attr("id");
            	var get_value = $(this).val();
            	if(get_value.match(/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/)){
            		$( "#"+get_id ).attr("aria-invalid","false").removeClass("error");
            		$("#"+get_id+"-error" ).text("ok!").addClass("success");
            	}else{
            		$( "#"+get_id ).rules( 'add', { dateFormat: true } );
            	}
    		});
    	});
    	
    	$(".input-val").change(function(){
        	var get_val = $(this).val();
        	var get_id = $(this).attr("id");
        	if(!get_val){
        		$("#"+get_id+"-error" ).removeClass("success");
        	}
		});
		
		$(".input-date").change(function(){
        	var get_val = $(this).val();
        	var get_id = $(this).attr("id");
        	if(!get_val.match(/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/)){
        		$("#"+get_id+"-error" ).removeClass("success");
        	}
		});
		
		
		
		$.validator.addMethod("dateFormat",
        function(value, element) {
            return value.match(/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/);
        },
        "Please enter a date in the format mm/dd/yyyy.");
        
		var validator = $("#AgreementForm").validate({
			debug: false,
			errorElement: "em",
			errorContainer: $("#warning, #summary"),
			errorPlacement: function(error, element) {
				error.appendTo(element.parent("td"));
			},
			success: function(label) {
				label.text("ok!").addClass("success");
			}, 
			rules: {
				AgmtStartDate: {
					required: true,
					dateFormat: true
				},
				OrganizationName: {
					required: true
				},
				DBA: {
					required: true
				},
				Address: {
					required: true
				},
				City: {
					required: true
				},
				State: {
					required: true
				},
				ZipCode: {
					required: true,
					minlength: 5,
					maxlength: 5,
					number: true
				},
				SetupFee: {
					required: true,
					number: true
				},
				ContingencyFee: {
					required: true,
					number: true
				},
				POAState: {
					required: true
				},
				EIN1: {
					required: true,
					minlength: 2,
					maxlength: 5,
					number: true
				},
				EIN2: {
					required: true,
					minlength: 5,
					maxlength: 5,
					number: true
				},
				POAStartingDate: {
					required: true,
					dateFormat: true
				},
				POAExpirationDate: {
					required: true,
					dateFormat: true
				}
			}
		});
		
		
		
	}); 
</script>

