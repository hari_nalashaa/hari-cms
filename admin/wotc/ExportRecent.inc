<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$METHODS = G::Obj('WOTCAdmin')->getEntryMethods();

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Today's Results")
       ->setKeywords("phpExcel")
       ->setCategory("Output");


// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('FEIN #');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Partner');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Parent Company');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Location');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('State');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Entry Date');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Name');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('SSN');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('DOB');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Qualifies');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('Start Date');
$objPHPExcel->getActiveSheet()->getCell('L1')->setValue('Received at CMS');
$objPHPExcel->getActiveSheet()->getCell('M1')->setValue('Filed to State');
$objPHPExcel->getActiveSheet()->getCell('N1')->setValue('Confirmed from State');
$objPHPExcel->getActiveSheet()->getCell('O1')->setValue('Approved by State');
$objPHPExcel->getActiveSheet()->getCell('P1')->setValue('Category');
$objPHPExcel->getActiveSheet()->getCell('Q1')->setValue('Comments');
$objPHPExcel->getActiveSheet()->getCell('R1')->setValue('Method');
$objPHPExcel->getActiveSheet()->getCell('S1')->setValue('Estimated Value');
$objPHPExcel->getActiveSheet()->getCell('T1')->setValue('Active Status');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);



$offset =   0;
if($_GET['offset']) $offset=$_GET['offset']; 

G::Obj('GenericQueries')->conn_string   =   "WOTC";
$query = "SELECT AD.* FROM ApplicantData AD, Processing P";
$query .= " WHERE AD.wotcID = P.wotcID";
$query .= " AND AD.ApplicationID = P.ApplicationID";
$query .= " AND DATE(P.LastUpdated) = DATE(DATE_SUB(NOW(), INTERVAL " . $offset . " DAY))";
$query .= " AND P.Qualified = 'Y'";
$APPLICANTDATA = G::Obj('GenericQueries')->getInfoByQuery($query);

G::Obj('GenericQueries')->conn_string   =   "IRECRUIT";

$n  =   2;
foreach ($APPLICANTDATA as $AD) {

    // Rename sheet
    $title      =   "Export Today's Changes";
    $objPHPExcel->getActiveSheet()->setTitle($title);
   
    $columns =  "IF(StartDate = '0000-00-00','',date_format(StartDate,'%m/%d/%Y')) StartDate,";
    $columns .= " IF(OfferDate = '0000-00-00','',date_format(OfferDate,'%m/%d/%Y')) OfferDate,";
    $columns .= " IF(HiredDate = '0000-00-00','',date_format(HiredDate,'%m/%d/%Y')) HiredDate,";
    $columns .= " IF(Received = '0000-00-00','',date_format(Received,'%m/%d/%Y')) Received,";
    $columns .= " IF(Filed = '0000-00-00','',date_format(Filed,'%m/%d/%Y')) Filed,";
    $columns .= " IF(Confirmed= '0000-00-00','',date_format(Confirmed,'%m/%d/%Y')) Confirmed,";
    $columns .= " StartingWage, Position, Qualified, Category, Comments";
    $PROC       =   G::Obj('WOTCProcessing')->getProcessingInfo($columns, $AD['wotcID'], $AD['ApplicationID']);
    
    $objPHPExcel->getActiveSheet()->getStyle('F'.$n.':V'.$n)->getAlignment()->setHorizontal('center');
  
    $PARENT = G::Obj('WOTCcrm')->getParentInfo($AD['wotcID']);
    $ADDPARENT=$PARENT['CompanyName'];
  //  . ', ' . $PARENT['Address'] . ', ' . $PARENT['City'] . ' ' . $PARENT['State'] . ' ' . $PARENT['ZipCode'];

    $PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($AD['wotcID']);
    $ADDPARTNER=$PARTNER['CompanyName'];

    $AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($AD['wotcID']);
    $ADDAKA =$AKADBA['AKADBA'] . ', ' . $AKADBA['Address'] . ', ' . $AKADBA['City'] . ' ' . $AKADBA['State'] . ' ' . $AKADBA['ZipCode'];


    $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($AKADBA['EIN1'] . '-' . $AKADBA['EIN2']);
    $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($ADDPARTNER);
    $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($ADDPARENT);
    $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($ADDAKA);
    $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($AKADBA['State']);
    $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($AD['EntryDate']);

    $NAME=$AD['FirstName'];
 
    if ($AD['MiddleName']) { $NAME .= ' ' . $AD['MiddleName']; }

    $NAME   .=  ' ' . $AD['LastName'];
    
    $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($NAME);
    $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue( $AD['Social1'] . '-' . $AD['Social2'] . '-' . $AD['Social3']);
    $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($AD['DOB_Date']);
    $objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($AD['Qualifies']);
    $objPHPExcel->getActiveSheet()->getCell('K'.$n)->setValue($PROC['StartDate']);
    $objPHPExcel->getActiveSheet()->getCell('L'.$n)->setValue($PROC['Received']);
    $objPHPExcel->getActiveSheet()->getCell('M'.$n)->setValue($PROC['Filed']);
    $objPHPExcel->getActiveSheet()->getCell('N'.$n)->setValue($PROC['Confirmed']);
    $objPHPExcel->getActiveSheet()->getCell('O'.$n)->setValue($PROC['Qualified']);
    $objPHPExcel->getActiveSheet()->getCell('P'.$n)->setValue($PROC['Category']);
    $objPHPExcel->getActiveSheet()->getCell('Q'.$n)->setValue($PROC['Comments']);
    $objPHPExcel->getActiveSheet()->getCell('R'.$n)->setValue($AD['Initiated']);

    $total  =   0;
    
    $billing_info   =   G::Obj('WOTCBilling')->getMaxYearAndTaxCredit($AD['wotcID'], $AD['ApplicationID']);
    list ($year, $total) =  array_values($billing_info);
    
    $year++;

    $TaxCredit  =   0;
    list($TaxCredit, $active, $comment)   =   G::Obj('WOTCCalculator')->calculate_wotcCredit($AD['wotcID'],$AD['ApplicationID'],$year);

    $TaxCredit  +=  $total;

    $objPHPExcel->getActiveSheet()->getCell('S'.$n)->setValue($TaxCredit);
    $objPHPExcel->getActiveSheet()->getCell('T'.$n)->setValue($active);

    $n++;
} // end foreach

// Redirect output to a client's web browser (Excel5)

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="exportRecent.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
