<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$columns = 'CompanyID, CompanyName, Assignment';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];


			$objPHPExcel = new Spreadsheet();
                        $objPHPExcel->getProperties()->setCreator("CMS WOTC")
                               ->setLastModifiedBy("System")
                               ->setTitle("WOTC Awaiting Payroll Data")
                               ->setSubject("Excel")
                               ->setDescription("Awaiting Payroll Data Export")
                               ->setKeywords("phpExcel")
                               ->setCategory("Output");

                        // Create a first sheet, representing sales data
                        $objPHPExcel->setActiveSheetIndex(0);
                        $objPHPExcel->getActiveSheet()->setTitle("Awaiting Payroll Data");

                        $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
                        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
                        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setRGB('0c00ac');
                        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
                        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
                        $objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setVertical('center');

                        $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Employer');
                        $objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
                        $objPHPExcel->getActiveSheet()->getCell('C1')->setValue('ApplicantName');
                        $objPHPExcel->getActiveSheet()->getCell('D1')->setValue('SSN');
                        $objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Hired Date');
                        $objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Confirmed');
                        $objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Termed Date ');
                        $objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Gross Hours to Date');
                        $objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Gross Wages to Date');

                        for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
                            $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
                        }
                            $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


                if ($CRM['Assignment'] == "Business Partners") {

                   $columns = 'CompanyID';
                   $where = array("Partner = :Partner","Active = 'Y'");
                   $params = array(":Partner"=>$CRM['CompanyID']);
                   $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
                   $DATA = $RESULTS['results'];

                   $n=1;
                   foreach($DATA AS $P) {

                          $PAYROLLDATA = G::Obj('WOTCcrm')->getAwaitingPayrollData($P['CompanyID']);

                          foreach ($PAYROLLDATA AS $PD) {
                            $n++;

                            $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getNumberFormat()->setFormatCode("#,##0");
                            $objPHPExcel->getActiveSheet()->getStyle('G'.$n)->getNumberFormat()->setFormatCode("#,##0.00");
                            $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($PD['EIN'] . ', ' . $PD['AKADBA']);
                            $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($PD['ApplicationID']);
                            $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($PD['ApplicantName']);
                            $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($PD['SSN']);
                            $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($PD['HiredDate']);
                            $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($PD['Confirmed']);
                            $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue('');
                            $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue('');
                            $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue('');

                          } // end foreach PAYROLLDATA

                   } // end foreach

                } else {

                          $PAYROLLDATA = G::Obj('WOTCcrm')->getAwaitingPayrollData($CRM['CompanyID']);

                          $n=1;
                          foreach ($PAYROLLDATA AS $PD) {
                            $n++;

                            $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getNumberFormat()->setFormatCode("#,##0");
                            $objPHPExcel->getActiveSheet()->getStyle('G'.$n)->getNumberFormat()->setFormatCode("#,##0.00");
                            $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($PD['EIN'] . ', ' . $PD['AKADBA']);
                            $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($PD['ApplicationID']);
                            $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($PD['ApplicantName']);
                            $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($PD['SSN']);
                            $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($PD['HiredDate']);
                            $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($PD['Confirmed']);
                            $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue('');
                            $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue('');
                            $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue('');

                          } // end foreach PAYROLLDATA

                }


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="PayrollExport.xlsx"');
header('Cache-Control: max-age=0');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
