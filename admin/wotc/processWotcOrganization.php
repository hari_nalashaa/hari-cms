<?php
require_once '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

/*
G::Obj('GenericQueries')->conn_string =   "WOTC";
$query      =   "SELECT concat(\"'\",group_concat(concat(iRecruitOrgID, iRecruitMultiOrgID) SEPARATOR \"','\"),\"'\") AS INUSE";
$query     .=   " FROM OrgData WHERE (iRecruitOrgID != '' OR iRecruitMultiOrgID != '')";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

G::Obj('GenericQueries')->conn_string =   "IRECRUIT";
$query      =   "SELECT OrgID, MultiOrgID, OrganizationName FROM OrgData";
$query     .=   " WHERE concat(OrgID,MultiOrgID) not in (:INUSE)";
$query     .=   " AND OrganizationName not like '%(inactive)%'";
$params  = array(":INUSE"=>$RESULTS[0]['INUSE']);
$IRECRUIT =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
 */

$wotc_forms = G::Obj('WOTCFormQuestions')->getActiveWotcFormIds();

$WOTCIDLS = G::Obj('WOTCcrm')-> getFirstLastScreened($_REQUEST['CompanyID'],'wotcID');
$WOTCIDS = G::Obj('WOTCcrm')->getWOTCIDs($_REQUEST['CompanyID']);

/*
list($iRecruitOrgID,$iRecruitMultiOrgID) = explode(":",$_REQUEST['iRecruitID']);

$_REQUEST['iRecruitOrgID'] = $iRecruitOrgID;
$_REQUEST['iRecruitMultiOrgID'] = $iRecruitMultiOrgID;
 */


if ($_REQUEST['process'] == 'wotcid') {

    $_REQUEST['wotcID'] = preg_replace('/[^A-Za-z0-9]/','',$_REQUEST['wotcID']);
    $ERROR="";

    if ($_REQUEST['NEW'] == "Y") {

       $org_info    =   G::Obj('WOTCOrganization')->getOrganizationInfo("wotcID", $_REQUEST['newID']);
       $WOTCIDCK    =   $org_info['wotcID'];

       if ($WOTCIDCK != "") {
          $ERROR    =   "<div style=\"color:red;margin:20px 20px;\">WOTC ID <span style=\"font-size:14pt;font-weight:bold;\">" . $WOTCIDCK . "</span> exists</div>";
          $MESSAGE  =   $ERROR;
       }

       if (strlen($_REQUEST['newID']) < 5) {
          $ERROR    .=  "<div style=\"color:red;margin:20px 20px;\">";
          $ERROR    .=  "wotcID needs to be at least 5 digits and contain no special characters.";
          $ERROR    .=  "</div>";
          $MESSAGE  =   $ERROR;
       }

       $_REQUEST['wotcID'] = $_REQUEST['newID'];

    }

    if (!$ERROR) {


        $skip   =   array('wotcID', 'ClientSince');
	$info   =   array(
                        'wotcID'                =>  $_REQUEST['wotcID'],
                        'CompanyID'             =>  $_REQUEST['CompanyID'],
                        'CompanyIdentifier'     =>  $_REQUEST['CompanyIdentifier'],
                        'Address'     		=>  $_REQUEST['Address'],
                        'City'     		=>  $_REQUEST['City'],
                        'State'     		=>  $_REQUEST['State'],
                        'ZipCode'     		=>  $_REQUEST['ZipCode'],
                        'EIN1'                  =>  $_REQUEST['EIN1'],
                        'EIN2'                  =>  $_REQUEST['EIN2'],
                        'ApplicationID'         =>  $_REQUEST['ApplicationID'],
                        'Nonprofit'             =>  $_REQUEST['Nonprofit'],
                        'CallCenter'            =>  $_REQUEST['CallCenter'],
                        'eSignature'            =>  $_REQUEST['eSignature'],
                        'Active'                =>  $_REQUEST['Active'],
                        'WotcFormID'            =>  $_REQUEST['ddlWotcFormID']
		);
	/*
                        'iRecruitOrgID'         =>  $iRecruitOrgID,
                        'iRecruitMultiOrgID'    =>  $iRecruitMultiOrgID,
	 */

	G::Obj('WOTCOrganization')->insUpdOrgDataInfo($info, $skip);
	$MESSAGE = "<span style=\"color:red;\">Successfully updated.</span>";

    } else { // else ERROR
	$MESSAGE = "<span style=\"color:red;\">" . $ERROR . "</span>";
	$_REQUEST['wotcID'] = "";
    } // end no ERROR

} // end process

if ($_REQUEST['wotcID'] != "") {
  $Title="Edit this wotcID";
  $Submit="Update wotcID";
  $new = "N";
} else {
  $Title="Create a new wotcID";
  $Submit="Create New WOTC Organization";
  $new = "Y";
}

echo '<div style="text-align:right;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;">';
echo "<div style=\"float:left;\"><strong style=\"font-size:14pt;\">" . $Title . "</strong></div>";
echo '<a href="#openDocuments" onclick="updateWOTCIDs(\'' . $_REQUEST['CompanyID'] . '\');"><img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Assign/Edit wotcID\'s" style="margin:0px 2px -4px 0px;" border="0"><b style="FONT-SIZE: 8pt; COLOR: #000000">Go Back</b></a>';
echo '</div>';

echo printForm($_REQUEST['wotcID'],$wotc_forms,$IRECRUIT,$MESSAGE,$Submit,$new,$WOTCIDS);

function printForm($wotcID,$wotc_forms,$IRECRUIT,$MESSAGE,$Submit,$new,$WOTCIDS) {

   if ($new == 'Y') {
      $ORGDATA      =   $_REQUEST;
   } else {
      $ORGDATA      =   G::Obj('WOTCOrganization')->getOrganizationInfo("*", $wotcID);
   }

   $rtn = '<div style="margin:10px 0;">';

   $rtn .= '<form method="POST" action="index.php">';
   $rtn .= '<input type="hidden" name="pg" value="wotc_orgsetup">';
   $rtn .= '<input type="hidden" name="NEW" value="' . $new . '">';
   $rtn .= '<div style="margin-top:10px;">';
   $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
   $rtn .= '<tr><td align="right" width="160">';
   $rtn .= 'wotcID:';
   $rtn .= '</td><td>';

if ($ORGDATA['wotcID'] != "") {

   $rtn .= '<input type="hidden" name="wotcID" value="' . $ORGDATA['wotcID'] . '">';
   $rtn .= $ORGDATA['wotcID'];
   $rtn .= '</td></tr>';

} else {

   $HIGHEST=array();
   $NAMES=array();
   foreach($WOTCIDS AS $ID) {
	$HIGHEST[]=preg_replace('/[^0-9]/', '', $ID);
	$NAMES[preg_replace('/[^a-z-A-Z]/', '', $ID)]=1;
   }
   sort($HIGHEST);
   $CNT = $HIGHEST[count($HIGHEST)-1];
   if ($CNT == "") { $CNT = "None"; }

   if ($_REQUEST['newID'] == "") { 
	   if (count($NAMES) == 1) {
	    $_REQUEST['newID'] = preg_replace("/[^a-zA-Z]/","",$WOTCIDS[0]); 
	   }
   }

   $rtn .= '<input type="text" name="newID" value="' . $_REQUEST['newID'] . '" size="20" maxlength="30">';
   $rtn .= '&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:8pt;">Highest ID number used:</span> <b>' . $CNT . '</b>';
   $rtn .= '</td></tr>';

}

   $rtn .= '<tr><td align="right">';
   $rtn .= 'Company Identifier:';
   $rtn .= '</td><td>';
   $rtn .= '<input type="text" name="CompanyIdentifier" value="' . $ORGDATA['CompanyIdentifier'] . '" size="30" maxlength="60">';
   $rtn .= '</td></tr>';

   $rtn .= '<tr>';
   $rtn .= '<td align="right">';
   $rtn .= 'Location Address:';
   $rtn .= '</td>';
   $rtn .= '<td>';
   $rtn .= '<input type="text" name="Address" value="' . $ORGDATA['Address'] . '" size="40" maxlength="45">';
   $rtn .= '</td></tr>';

   $rtn .= '<tr>';
   $rtn .= '<td align="right">';
   $rtn .= 'City:';
   $rtn .= '</td>';
   $rtn .= '<td>';
   $rtn .= '<input type="text" name="City" value="' . $ORGDATA['City'] . '" size="30" maxlength="45">';
   $rtn .= '</td></tr>';

   $rtn .= '<tr>';
   $rtn .= '<td align="right">';
   $rtn .= 'State:';
   $rtn .= '</td>';
   $rtn .= '<td>';
   $rtn .= '<select size="1" name="State" style="max-width:300px;">';
   $rtn .= '<option value="">Please Select</option>';

   // Get Address States List
   $results    =   G::Obj('Address')->getAddressStateList ();
   $STATES     =   $results['results'];

   foreach ($STATES as $STATE) {
     if ($STATE['Abbr'] == $ORGDATA['State']) { $ckd = " selected"; } else { $ckd = ""; }
     $rtn .= "<option value=\"" . $STATE['Abbr'] . "\"" . $ckd . ">" . $STATE['Description'] . '</option>';
   } // end foreach

   $rtn .= '</select>';
   $rtn .= '</td></tr>';

   $rtn .= '<tr>';
   $rtn .= '<td align="right">';
   $rtn .= 'Zip Code:';
   $rtn .= '</td>';
   $rtn .= '<td>';
   $rtn .= '<input type="text" name="ZipCode" value="' . $ORGDATA['ZipCode'] . '" size="10" maxlength="15">';
   $rtn .= '</td></tr>';

   $rtn .= '<tr><td align="right">';
   $rtn .= 'EIN#:';
   $rtn .= '</td><td>';
   $rtn .= '<input type="text" name="EIN1" value="' . $ORGDATA['EIN1'] . '" size="2" maxlength="5">-<input type="text" name="EIN2" value="' . $ORGDATA['EIN2'] . '" size="8" maxlength="20">';
   $rtn .= '</td></tr>';

   $rtn .= '<tr><td align="right" width="160">';
   $rtn .= 'ApplicationID:';
   $rtn .= '</td><td>';
if ($ORGDATA['ApplicationID'] == "") { $ORGDATA['ApplicationID'] = 0; }
   $rtn .= '<input type="text" name="ApplicationID" value="' . $ORGDATA['ApplicationID'] . '" size="6" maxlength="10">';
   $rtn .= '</td></tr>';

   $rtn .= '<tr><td align="right">';
   $rtn .= 'Nonprofit:';
   $rtn .= '</td><td>';
   $rtn .= '<select name="Nonprofit">';
   $rtn .= '<option value="N"';
   if ($ORGDATA['Nonprofit'] == "N") { $rtn .= ' selected'; }
   $rtn .= '>No</option>';
   $rtn .= '<option value="Y"';
   if ($ORGDATA['Nonprofit'] == "Y") { $rtn .= ' selected'; }
   $rtn .= '>Yes</option>';
   $rtn .= '</select>';
   $rtn .= '</td></tr>';

   $rtn .= '<tr><td align="right">';
   $rtn .= 'Call Center:';
   $rtn .= '</td><td>';
   $rtn .= '<select name="CallCenter">';
   $rtn .= '<option value="N"';
   if ($ORGDATA['CallCenter'] == "N") { $rtn .= ' selected'; }
   $rtn .= '>No</option>';
   $rtn .= '<option value="Y"';
   if ($ORGDATA['CallCenter'] == "Y") { $rtn .= ' selected'; }
   $rtn .= '>Yes</option>';
   $rtn .= '</select>';
   $rtn .= '</td></tr>';

   $rtn .= '<tr><td align="right">';
   $rtn .= 'eSignature:';
   $rtn .= '</td><td>';
   $rtn .= '<select name="eSignature">';
   $rtn .= '<option value="N"';
   if ($ORGDATA['eSignature'] == "N") { $rtn .= ' selected'; }
   $rtn .= '>No</option>';
   $rtn .= '<option value="Y"';
   if ($ORGDATA['eSignature'] == "Y") { $rtn .= ' selected'; } 
   $rtn .= '>Yes</option>';
   $rtn .= '</select>';
   $rtn .= '</td></tr>';

   $rtn .= '<tr><td align="right">';
   $rtn .= 'Active:';
   $rtn .= '</td><td>';
   $rtn .= '<select name="Active">';
   $rtn .= '<option value="N"';
   if ($ORGDATA['Active'] == "N") {    $rtn .= ' selected'; }
   $rtn .= '>No</option>';
   $rtn .= '<option value="Y"';
   if ($ORGDATA['Active'] == "Y") {    $rtn .= ' selected'; }
   $rtn .= '>Yes</option>';
   $rtn .= '</select>';
   $rtn .= '</td></tr>';

   $rtn .= '<tr>';
   $rtn .= '<td align="right">';
   $rtn .= 'WOTC Form:';
   $rtn .= '</td>';
   $rtn .= '<td>';
   $rtn .= '<select name="ddlWotcFormID" id="ddlWotcFormID">';

                if ($ORGDATA['WotcFormID'] == "") { $ORGDATA['WotcFormID'] = "STANDARD"; }
                for($w = 0; $w < count($wotc_forms); $w++) {
                      $rtn .= '<option value="' . $wotc_forms[$w]['WotcFormID'] . '"';
                      if ($ORGDATA['WotcFormID'] == $wotc_forms[$w]['WotcFormID']) { $rtn .= " selected"; }
                      $rtn .= '>' . $wotc_forms[$w]['WotcFormID'] . '</option>' . "\n";
                }

   $rtn .= '</select>';
   $rtn .= '</td>';
   $rtn .= '</tr>';

   /*
   $rtn .= '<tr>';
   $rtn .= '<td align="right">';
   $rtn .= 'iRecruit Organization:';
   $rtn .= '</td>';
   $rtn .= '<td>';

   $rtn .= '<select name="iRecruitID">';
   $rtn .= '<option value="">None</option>';
   foreach ($IRECRUIT AS $IR) {
	   $rtn .= '<option value="' . $IR['OrgID'] . ':' . $IR['MultiOrgID'] . '"';
	   if (($IR['OrgID'] . ':' . $IR['MultiOrgID']) == ($ORGDATA['iRecruitOrgID'] . ':' . $ORGDATA['iRecruitMultiOrgID'])) {
		   $rtn .= ' selected';
	   }
	   $rtn .= '>' . $IR['OrganizationName'] . '</option>';
   } // end foreach
   $rtn .= '</select>';
   $rtn .= '</td></tr>';
    */

   $rtn .= '</table>';

   $rtn .= '</div>' . "\n";

   $rtn .= "<div style=\"margin-top:20px;\">";
   $rtn .= '<input type="hidden" name="process" value="wotcid">';
   $rtn .= '<button onclick="processWotcOrganization(\'' . $_REQUEST['CompanyID'] . '\',\'' . $_REQUEST['wotcID'] . '\',\'submit\');return false;">' . $Submit . '</button>';
 if ($MESSAGE != "") {
   $rtn .= "&nbsp;&nbsp;&nbsp;";
   $rtn .= $MESSAGE;
 }
 echo "</div>";

   return $rtn;

} // end function
?>
