<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Needs Processing")
       ->setKeywords("phpExcel")
       ->setCategory("Output");


// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Location');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('NEW HIRE');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Start Date');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Starting Wage');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Supporting Documentation Pending');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);

$n  =   2;
foreach ($APPLICANTDATA as $AD) {

 $columns =  "IF(StartDate = '0000-00-00','',date_format(StartDate,'%m/%d/%Y')) StartDate,";
 $columns .= " IF(OfferDate = '0000-00-00','',date_format(OfferDate,'%m/%d/%Y')) OfferDate,";
 $columns .= " IF(HiredDate = '0000-00-00','',date_format(HiredDate,'%m/%d/%Y')) HiredDate,";
 $columns .= " IF(Received = '0000-00-00','',date_format(Received,'%m/%d/%Y')) Received,";
 $columns .= " IF(Filed = '0000-00-00','',date_format(Filed,'%m/%d/%Y')) Filed,";
 $columns .= " IF(Confirmed= '0000-00-00','',date_format(Confirmed,'%m/%d/%Y')) Confirmed,";
 $columns .= " StartingWage, Position, Qualified, Category, Comments";
 $PRS       =   G::Obj('WOTCProcessing')->getProcessingInfo($columns, $AD['wotcID'], $AD['ApplicationID']);

 list($filter,$determined) = G::Obj('WOTCAdmin')->filterStatus ( $PRS );

 if ($_POST['status'] == "NotProcessed") {
    $filter = $determined;
 }

 if ($filter == $_POST['status']) {

  $AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($AD['wotcID']);
  $ADDAKA =$AKADBA['State'] . ' - (' . $AKADBA['EIN1'] . '-' . $AKADBA['EIN2'] . ') ' . $AKADBA['AKADBA'] . ', ' . $AKADBA['Address'] . ', ' . $AKADBA['City'] . ' ' . $AKADBA['State'] . ' ' . $AKADBA['ZipCode'];
 
  $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue( $ADDAKA );
  $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue( $AD['FirstName'] . ' ' . $AD['LastName']);
  $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue( '');
  $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue( '');
  $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue( '');
    
    $n++;

 } // end status


} // end foreach

// Rename sheet
$title  =   "Needs Processing ";
$objPHPExcel->getActiveSheet()->setTitle($title);


// Redirect output to a client's web browser (Excel5)

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="wotcNeedsProcessing.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
