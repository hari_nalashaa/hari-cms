<?php
require_once '../Configuration.inc';

G::Obj('GenericQueries')->conn_string =   "WOTC";

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

echo '<div style="text-align:left;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;margin-bottom:20px;">';
echo "<strong style=\"font-size:14pt;\">FEIN AKA/DBA - POA</strong>";
echo '</div>';

$columns = '*';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];

$FEINCONFIG = G::Obj('WOTCcrm')->getFEIN($CRM['CompanyID'],'','');
$FEINLS= G::Obj('WOTCcrm')-> getFirstLastScreened($CRM['CompanyID'],'FEIN');

foreach ($FEINCONFIG AS $FC) {

 echo '<div style="float:right;text-align:right;margin-top:10px;">';
 echo "<a href=\"#openDocuments\" onclick=\"processFEIN('" . $CRM['CompanyID'] . "', '" . $FC['EIN1'] . '-' . $FC['EIN2'] . "','Form');\">";
 echo '<img src="' . ADMIN_HOME . 'images/pencil.png" title="Update/Renew POA" style="margin: 0px 3px -4px 0px;" border="0">';
 echo "Update/Renew POA</a>";
 echo '</div>';

 $date1 = date_create_from_format('m/d/Y', $FC['POAExpirationDate']);
 $date2 = date_create_from_format('Y-m-d', date('Y-m-d'));

 $diff = (array) date_diff($date1, $date2);
 if ($date1 < $date2) {
              $type="POA Expired: ";
              $typecolor="red";
 } else if ($diff['days'] < '90') {
              $type="POA Expiring soon: ";
              $typecolor="red";
 } else {
 	      $type="Expires: ";
              $typecolor="#000000";
 }

 echo "<div style=\"margin-top:10px;line-height:180%;\">";

 echo 'AKA/DBA: <strong>' . $FC['AKADBA'] . '</strong>';
 if ($FC['Address'] != "") {
   echo '<br>';
   echo '<strong>' . $FC['Address'];
   if ($FC['City'] != "") {
	   echo ', ' . $FC['City'] . ', ' . $FC['State'] . '&nbsp;&nbsp;' . $FC['ZipCode'];
   }
   echo '</strong>';
 }
 $CP = json_decode($FC['Phone'], true);

 if ($CP[0] != "") {
   echo '<br>';
   echo 'Phone: <strong>(' . $CP[0] . ') ' . $CP[1] . '-' . $CP['2'] . '</strong>';
 }
 echo '<br>';
 echo 'FEIN: <strong>' . $FC['EIN1'] . '-' . $FC['EIN2'] . '</strong>';
 echo '<br>';
 echo $type;
 echo '<strong><span style="color:'.$typecolor.';">' . $FC['POAExpirationDate'] . '</span></strong>';
 echo '&nbsp;&nbsp;';
 echo 'POA State: <strong>' . $FC['POAState'] . '</strong>';

 echo '<br>';
 if (count($FEINLS[$FC['EIN1'] . '-' . $FC['EIN2']]) > 0) {
   echo 'First Screened Date: <strong>' . $FEINLS[$FC['EIN1'] . '-' . $FC['EIN2']]['First Screen Date'] . "</strong>";
   echo '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   echo 'Last Screened Date: <strong>' . $FEINLS[$FC['EIN1'] . '-' . $FC['EIN2']]['Last Screen Date'] . "</strong>";
   echo '&nbsp;&nbsp;&#149;&nbsp;&nbsp;';
   echo 'Total Screened: <strong>' . $FEINLS[$FC['EIN1'] . '-' . $FC['EIN2']]['WOTCScreened'] . "</strong><br>";
 } else {
	 echo 'There are no applications for this FEIN.';
 }
 echo '</div>';

 echo '<hr size="1">';

} // end foreach

?>
