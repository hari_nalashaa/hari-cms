<?php
include '../Configuration.inc';

$AUTH				=	$ADMINObj->authenticate($_COOKIE['AID']);

echo $ADMINObj->adminHeader();

$wotcID				=	$_REQUEST['wotcID'];
$ApplicationID		=	$_REQUEST['ApplicationID'];

$PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
$WotcFormID=$PARENT['WotcFormID'];
$AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($wotcID);
$CompanyName = $AKADBA['AKADBA'];

//Get Applicant Data
$WOTCAPPDATA		=	G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $wotcID, $WotcFormID, $ApplicationID);

//Get WOTC Applicant Data
$WOTCQUEANS			=	G::Obj('WOTCFormQueAns')->getWotcFormQueAnswersInfo($wotcID, $WotcFormID, $ApplicationID);

$parent_ques		=	G::Obj('WOTCFormQuestions')->getWotcFormParentQuesForChildQues("MASTER", $WotcFormID);
?>
<link rel="stylesheet" type="text/css" href="<?php echo ADMIN_HOME;?>/css/bootstrap.min.css" />
<style>
#content {
	margin-left: 0px !important;
}
</style>
<script type="text/javascript" src="<?php echo ADMIN_HOME;?>/javascript/jquery-1.12.4.js"></script>
<div style="margin:0 20px;">
<h3>Application View:</h3>
<?php
echo "<div class='row' style='margin-bottom:1px;padding:5px;'>";
echo "<div class='col-lg-3 col-md-4 col-sm-3'>WotcID/Organization:&nbsp;&nbsp;</div>";
echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".htmlspecialchars($wotcID)." - " . htmlspecialchars($CompanyName) . "</strong></div>";
echo "<div class='col-lg-3 col-md-4 col-sm-3'>Application ID:&nbsp;&nbsp;</div>";
echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".htmlspecialchars($ApplicationID)."</strong></div>";
echo "<div class='col-lg-3 col-md-4 col-sm-3'>Entry Date: &nbsp;&nbsp;</div>";
echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$WOTCAPPDATA['EntryDate']."</strong></div>";
echo "</div>";

echo '<div style="background-color:#1076BB;color:#1076BB;width:600px;border:2px solid #1076BB;height:3px;font-size:0px;margin:0 0 5px 0;"></div>';

foreach ($WOTCQUEANS as $QuestionID=>$QuestionAnswerInfo) {

	$ParentQueID	=	$parent_ques[$QuestionID];

	if($QuestionAnswerInfo['QuestionTypeID'] != 99
		&& $QuestionAnswerInfo['QuestionTypeID'] != 45
		&& $QuestionID != 'Signature'
		&& $QuestionID != 'verify'
		&& $QuestionID != 'captcha') {

		if($ParentQueID != "") {

			$ChildQuestionsInfo		=	json_decode($WOTCQUEANS[$ParentQueID]['ChildQuestionsInfo'], true);
			$parent_que_val_info	=	$ChildQuestionsInfo[$WOTCAPPDATA[$ParentQueID]];
				
			if($parent_que_val_info[$QuestionID] == "show") {
				echo "<div class='row' style='margin-bottom:1px;padding:5px;'>";

				echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
				echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
				echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
				
				echo "</div>";
			}
		}
		else {

			if ($QuestionID == "County") {
				$QuestionAnswerInfo['Answer']=$WOTCAPPDATA['County'];
			}

			
			echo "<div class='row' style='margin-bottom:1px;padding:5px;'>";
			
			echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
			echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
			echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
			
			echo "</div>";
		}
	}

}

echo "</div>\n";

echo "</div>\n";

echo $ADMINObj->adminFooter();
?>
