<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

date_default_timezone_set('America/New_York');

if ($_POST['process'] == "Y") {

if ($_FILES['hoursfile']['name']) {

    $name = $_FILES ['hoursfile'] ['name'];
    $name = preg_replace ( '/\s/', '_', $name );
    $name = preg_replace ( '/\&/', '', $name );
    $data = file_get_contents ( $_FILES ['hoursfile'] ['tmp_name'] );

    $filename = ROOT . "cron/temp/" . $name;

    $fh = fopen ( $filename, 'w' );
    fwrite ( $fh, $data );
    fclose ( $fh );
    chmod ( $filename, 0666 );


//  Read your Excel workbook
try { 

$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
$objPHPExcelIN = $reader->load($filename);

} catch(Exception $e) {
    die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
}

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("CMS WOTC")
->setLastModifiedBy("System")
->setTitle("WOTC Export")
->setSubject("Excel")
->setDescription("Not Active")
->setKeywords("phpExcel")
->setCategory("Output");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('wotcID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Year');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Hours');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Wages');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('ASOF');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Termed');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Maxed Limit');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Tax Credit');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Comment');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('Updated Billing');


for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


$worksheet = $objPHPExcelIN->getActiveSheet();

//  Loop through each row of the worksheet in turn
$i=1;
foreach ($worksheet->getRowIterator() as $row) {
$i++;
if ($worksheet->getCell("A$i")->getValue() != "") {

    $Employee  = $worksheet->getCell("A$i")->getValue();

    $SSN = $worksheet->getCell("B$i")->getValue();
    $SSN = explode('-',$SSN);

    $value = $worksheet->getCell("D$i")->getValue();
    $value2 = $worksheet->getCell("C$i")->getValue();

    if (validateDate($value,'m/d/Y')) {
       $Termed = gmdate("Y-m-d",strtotime($value));
       $StartDate = gmdate("Y-m-d",strtotime($value2));
    } else {
       $Termed = gmdate("Y-m-d",\PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($value));
       $StartDate = gmdate("Y-m-d",\PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($value2));
    }

    $Hours = number_format($worksheet->getCell("E$i")->getValue(),2,'.','');
    $Wages = number_format($worksheet->getCell("F$i")->getValue(),2,'.','');

    $columns = "wotcID, ApplicationID, DATE_FORMAT(now(),'%Y-%m-%d') AS Date";
    $where_info =   array("Social1 = :ssn1", "Social2= :ssn2", "Social3 = :ssn3");
    $params     =   array(":ssn1" => $SSN[0], ":ssn2" => $SSN[1], ":ssn3" => $SSN[2]);
    $group_by = "";
    $order_by = "";

    $APPDATA = G::Obj('WOTCApplicantData')->getApplicantData($columns, $where_info, $group_by, $order_by, array($params));

    $wotcID = $APPDATA['results'][0]['wotcID'];
    $ApplicationID = $APPDATA['results'][0]['ApplicationID'];

    if (($wotcID != "") && ($ApplicationID != "")) {

    $Bill = "";
    if ($Termed != "1970-01-01") {
       $ASOF = $Termed;
       $Termed = "Y";
    }  else {
       $ASOF = $APPDATA['results'][0]['Date'];
       $Termed = "N";
    }

    $year   =   1;
    $yr     =   G::Obj('WOTCBilling')->getMaxYearByWotcIDANDApplicationID($wotcID, $ApplicationID);
    if ($yr == "1") $year = $yr + 1;

     $info   =   array(
                 "wotcID"        =>  $wotcID,
                 "ApplicationID" =>  $ApplicationID,
                 "Year"          =>  $year,
                 "Hours"         =>  $Hours,
                 "Wages"         =>  $Wages,
                 "ASOF"          =>  $ASOF,
                 "Comments"      =>  "",
                 "LastUpdated"   =>  "NOW()"
     );
     G::Obj('WOTCHours')->insHoursInfo($info);

     list ($TaxCredit, $active, $comment) = G::Obj('WOTCCalculator')->calculate_wotcCredit($wotcID, $ApplicationID, $year);

     $Maxed = "N";
     if (strpos($comment, 'Maxed Limit') !== false) {
	$Maxed = "Y";
     }

     $termed= "0000-00-00";
     $reason="";
     $taxcredit=$TaxCredit;
     $datebilled="0000-00-00";

     $action="";
     if (($Termed == "N") && ($Maxed == "N")) {

	$action.="Don't add Billing";

     } else {

       if ($Termed == "N") {
	  $action.="No Termed Date";
       } else {
	  $action.="Add Termed Date " . date("m/d/Y",strtotime($ASOF));
          $termed= $ASOF;
          if ($Maxed == "N") {
            $action.=", ";
	    $action.="Reason set to: 'Employment Termination'";
            $reason="Employment Termination";
          }
       }
       $action.=", ";
       if ($Maxed == "Y") {
	  $action.="Reason set to: 'Maxed Limit'";
          $reason="Maxed Limit";
       } 

       $action.=", ";
       if ($TaxCredit == "0") {
	  $action.="No Date Billed";
       } else {
   	  $action.="Add Date Billed " . $APPDATA['results'][0]['Date'];
          $datebilled=$APPDATA['results'][0]['Date'];
       }


       $info     =   array(
                     'wotcID'        =>  $wotcID,
                     'ApplicationID' =>  $ApplicationID,
                     'Year'          =>  $year,
                     'Termed'        =>  $termed,
                     'Reason'        =>  $reason,
                     'TaxCredit'     =>  $taxcredit,
                     'DateBilled'    =>  $datebilled,
                     'InvoiceNumber' =>  "",
                     'DatePaid'      =>  "",
                     'Comments'      =>  "",
                     'LastUpdated'   =>  "NOW()"
       );

       G::Obj('WOTCBilling')->insBillingInfo($info);

       if (($reason == "Employment Termination") && ($taxcredit == 0)) {
          $set_info   =   array("Qualifies = 'N'");
          $where      =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
          $params     =   array(':wotcID'=>$wotcID, ':ApplicationID'=>$ApplicationID);
          G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where, array($params));
       }


     } // end else Termed

    } // end if wotcID and ApplicationID 


    // Write to output excel
    $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getNumberFormat()->setFormatCode("#,##0.00");
    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getNumberFormat()->setFormatCode("#,##0.00");
    $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->getNumberFormat()->setFormatCode(\PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DATETIME);


$objPHPExcel->getActiveSheet()->getCell('A'.$i)->setValue($wotcID);
$objPHPExcel->getActiveSheet()->getCell('B'.$i)->setValue($ApplicationID);
$objPHPExcel->getActiveSheet()->getCell('C'.$i)->setValue($year);
$objPHPExcel->getActiveSheet()->getCell('D'.$i)->setValue($Hours);
$objPHPExcel->getActiveSheet()->getCell('E'.$i)->setValue($Wages);
$objPHPExcel->getActiveSheet()->getCell('F'.$i)->setValue(date("m/d/Y",strtotime($ASOF)));
$objPHPExcel->getActiveSheet()->getCell('G'.$i)->setValue($Termed);
$objPHPExcel->getActiveSheet()->getCell('H'.$i)->setValue($Maxed);
$objPHPExcel->getActiveSheet()->getCell('I'.$i)->setValue($TaxCredit);
$objPHPExcel->getActiveSheet()->getCell('J'.$i)->setValue($comment);
$objPHPExcel->getActiveSheet()->getCell('K'.$i)->setValue($action);


} // end blank row
} // end foreach

unlink($filename);

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Processed Hours");

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="processedHoursExport.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');

} // end FILES

exit();

} // end process

function validateDate($date, $format = 'Y-m-d') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}

?>

<div style="font-size:12pt;font-weight:bold;margin-bottom:20px;">WOTC Enter Hours</div>
<form method="POST" action="index.php" enctype="multipart/form-data">
Hours Excel File:&nbsp;&nbsp;<input type="file" name="hoursfile">
<input type="hidden" name="pg" value="wotc_hours">
<input type="hidden" name="process" value="Y">
<input type="submit" value="Upload and Process Excel">
</form>
<div style="margin:20px 0 0 0"></div>
