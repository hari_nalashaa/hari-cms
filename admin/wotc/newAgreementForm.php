<?php 
ini_set("allow_url_fopen", 1);
require_once '../Configuration.inc';

$AgreementID = isset($_REQUEST['AgreementID']) ? $_REQUEST['AgreementID'] : '';
$column = '*';
$AGMTFORMDATA = G::Obj('WOTCAgreements')->getAgreementsInfo($column,$AgreementID);

// PDF Form
$pdfform = ADMIN_DIR . 'wotc/PDFFiles/CMS_WOTC_Service_Agreement.pdf';

// Temp file for merged data
$txfdffile = ADMIN_DIR . 'wotc/vault/' . $AgreementID . '_Agreement.xfdf';

// Applicants Output file
$filename = $AgreementID . '-Agreement.pdf';
$outputpdf = ADMIN_DIR . 'wotc/vault/' . $filename;

	$XFDF = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$XFDF .= "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n";
	$XFDF .= "<f href=\"CMS_WOTC_Service_Agreement.pdf\"/>\n";
	$XFDF .= "<fields>\n";

            	$XFDF .= "<field name=\"AgreementDate\">";
            	$XFDF .= "<value>" .date('m/d/Y',strtotime($AGMTFORMDATA['AgmtStartDate'])) . "</value>";
            	$XFDF .= "</field>\n";
            	
                $XFDF .= "<field name=\"Organization\">";
                //$XFDF .= "<value>NalashaaCorporationLtd\r\nSampleDBA\r\nAlaska</value>";
                $XFDF .= "<value>" . $AGMTFORMDATA['OrganizationName'] ."\r\n". $AGMTFORMDATA['DBA']."\r\n".$AGMTFORMDATA['Address']."\r\n".$AGMTFORMDATA['City'].", ".$AGMTFORMDATA['State']. "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"SetupFee\">";
                $XFDF .= "<value> $" . $AGMTFORMDATA['SetupFee'] . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"ContingencyFee\">";
                $XFDF .= "<value>" . $AGMTFORMDATA['ContingencyFee'] . "%</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"OrgName\">";
                $XFDF .= "<value>" . $AGMTFORMDATA['OrganizationName'] . "</value>";
                $XFDF .= "</field>\n";

        $XFDF .= "</fields>\n";
        $XFDF .= "</xfdf>\n";

        $fh = fopen($txfdffile, 'w');
        if (!$fh) { die('Cannot open file ' . $txfdffile); }
        fwrite($fh, $XFDF);
        fclose($fh);

// Merge temp file with PDF and write to applicants file
$command = 'pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($txfdffile) . ' output ' . escapeshellarg($outputpdf);
//echo $command;
system($command);
// remove temp file
system('rm ' . escapeshellarg($txfdffile));
@chmod($outputpdf, 0666);

// Used for triggering download instead of display
if (file_exists ( $outputpdf )) {
    header ( 'Content-Description: File Transfer' );
    header ( 'Content-Type: application/octet-stream' );
    header ( 'Content-Disposition: attachment; filename=' . basename ( $filename ) );
    header ( 'Content-Transfer-Encoding: binary' );
    header ( 'Expires: 0' );
    header ( 'Cache-Control:' );
    header ( 'Pragma: cache' );
    header ( 'Content-Length: ' . filesize ( $outputpdf ) );
    readfile ( $outputpdf );
    exit ();
}


?>
