<?php
require_once '../Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

if ($_REQUEST['process'] == 'parent') {

	$PC=array();
	$PC['ParentLinkShortcode']=$_REQUEST['ParentLinkShortcode'];
	$PC['DisplayCode']=$_REQUEST['DisplayCode'];
	$PC['EmploymentConfirmation']=$_REQUEST['EmploymentConfirmation'];
	$Config=json_encode($PC);

	$info   =   array(
                 "CompanyID"    =>  $_REQUEST['CompanyID'],
		 "ParentConfig" =>  $Config
                 );
        $skip   =   array("CompanyID");

	if ($_FILES['Logo']['name'][0] != "") {

            $name = $_FILES ['Logo'] ['name'][0];
            $name = preg_replace ( '/\s/', '_', $name );
            $name = preg_replace ( '/\&/', '', $name );
            $name = $_REQUEST['CompanyID'] . '-' . $name;

            $data = file_get_contents ( $_FILES ['Logo'] ['tmp_name'] [0] );

            $filename = WOTC_DIR . "images/logos/" . $name;

            $fh = fopen ( $filename, 'w' );
            fwrite ( $fh, $data );
            fclose ( $fh );

            chmod ( $filename, 0666 );

	    $info['Logo']=$name;

        } // end FILES

        G::Obj('WOTCcrm')->insUpdCRM($info, $skip);

        $MESSAGE = "<span style=\"color:red;\">Successfully updated.</span>";

} // end process

$columns = 'CompanyID, Logo, ParentConfig';
$where = array("CompanyID = :CompanyID");
$params = array(":CompanyID"=>$_REQUEST['CompanyID']);
$results = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
$CRM = $results['results'][0];

$PC=json_decode($CRM['ParentConfig'],true);
$PARENT=array();
$PARENT['ParentLinkShortcode']=$PC['ParentLinkShortcode'];
$PARENT['DisplayCode']=$PC['DisplayCode'];
$PARENT['EmploymentConfirmation']=$PC['EmploymentConfirmation'];

echo '<div style="text-align:left;width:100%;background-color:#eeeeee;padding:8px;border-radius:5px;margin-bottom:20px;">';
echo "<strong style=\"font-size:14pt;\">Parent Configuration</strong>";
echo '</div>';

echo "<div style=\"margin:20px 20px;\">\n";

echo "<table border=\"0\" cellspacing=\"3\" cellpadding=\"3\">\n";

echo "<tr><td align=\"right\">";
echo "Parent Link Shortcode: ";
echo "</td><td>";
echo '<input type="text" name="ParentLinkShortcode" value="' . $PARENT['ParentLinkShortcode'] . '">';
echo "</td></tr>\n";

echo "<tr><td align=\"right\">";
echo "Display Confirmation Code: ";
echo "</td><td>";
echo "<select name=\"DisplayCode\">";
echo "<option value=\"N\"";
if ($PARENT['DisplayCode'] == "N") { echo " selected"; }
echo ">No</option>\n";
echo "<option value=\"Y\"";
if ($PARENT['DisplayCode'] == "Y") { echo " selected"; }
echo ">Yes</option>\n";
echo "</select>\n";
echo "</td></tr>\n";

echo "<tr><td align=\"right\">";
echo "Employment Confirmation: ";
echo "</td><td>";
echo "<select name=\"EmploymentConfirmation\">";
echo "<option value=\"N\"";
if ($PARENT['EmploymentConfirmation'] == "N") { echo " selected"; }
echo ">No</option>\n";
echo "<option value=\"Y\"";
if ($PARENT['EmploymentConfirmation'] == "Y") { echo " selected"; }
echo ">Yes</option>\n";
echo "</select> employer assisted callcenter calls\n";
echo "</td></tr>\n";

if ($CRM['Logo'] != "") {

echo "<tr><td colspan=\"2\" align=\"center\">";
echo "<img src=\"" . WOTC_HOME . "images/logos/" . $CRM['Logo'] . "\" style=\"max-height:100px;\">";
echo "&nbsp;&nbsp;&nbsp;&nbsp;";

echo "<a href=\"#openDocuments\"";
echo " onclick=\"removeLogo('" . $CRM['CompanyID'] . "');\"";
echo ">";
echo "<img src=\"" . ADMIN_HOME . "images/cross.png\" border=\"0\" title=\"Delete\" style=\"margin:0px 3px -4px 0px;\"><b style=\"FONT-SIZE:8pt;COLOR:#000000\">Delete Logo</b>";
echo "</a>\n";

echo "</td></tr>\n";

echo "<tr><td align=\"right\">";
echo "Replace Logo Image: ";

} else {
echo "<tr><td align=\"right\">";
echo "Add Logo Image: ";

}

echo "</td><td>";
echo "<input type=\"file\" id=\"Logo\" name=\"Logo\">";
echo "</td></tr>\n";

echo '</table>';
echo "</div>";

echo "<div style=\"margin-top:20px;margin-left:130px;\">";
echo '<input type="hidden" name="process" value="parent">';
echo '<button onclick="parentConfig(\'' . $CRM['CompanyID'] . '\',\'submit\');return false;">Update Parent Configuration</button>';
if ($MESSAGE != "") {
  echo "&nbsp;&nbsp;&nbsp;";
  echo $MESSAGE;
}
echo "</div>";

$link = WOTC_HOME . "parent.php?";
if ($PARENT['ParentLinkShortcode'] != "") {
  $link .= "c=" . $PARENT['ParentLinkShortcode'];
} else {
  $link .= "d=" . $CRM['CompanyID'];
}

echo "<div style=\"margin:40px 0 20px 0;text-align:center;\">";
echo "Page Link: ";
echo "<a href=\"" . $link . "\" target=\"_blank\">" . $link . "</a>";
echo "</td></tr>\n";

?>
