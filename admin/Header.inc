<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="description" content="CMS iRecruit User Portal for Applicant Processing.">
<meta name="keywords" content="Requisition, Job Postings, Collaboration">
<title><?php echo $page_title; ?></title>

<!-- Bootstrap Core CSS -->
<link href="<?php echo ADMIN_HOME?>css/bootstrap.nav.css"	rel="stylesheet">
<!-- MetisMenu CSS -->
<link href="<?php echo ADMIN_HOME?>css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
<!-- DataTables CSS -->
<link href="<?php echo ADMIN_HOME?>css/plugins/dataTables.bootstrap.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo ADMIN_HOME?>css/sb-admin-2.css" rel="stylesheet">

<link href="<?php echo ADMIN_HOME?>css/job-application-steps.css" rel="stylesheet" type="text/css">

<style type="text/css">
#users-list {
    margin-left:0px;
    padding:0px;
    background-color:#e0e1e2;
    width:400px;
    overflow: scroll-y;
    vertical-align: top;
    margin-top: 0px;
}
#users-list li {
    list-style: none;
    margin-left: 0px;
    padding: 5px;
    width: 400px;
}
#users-list li:hover {
    list-style: none;
    margin-left: 0px;
    padding: 5px;
    width:390px;
    color:white;
    background-color: blue;
}
#suggesstion-box {
    /*position: relative;*/
    z-index: 100;
    padding-top: 0px;
    left: 7%;
}
.navbar {
	background-color: #203864;
	box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
}
.application_steps li a.application_step_tabs {
    color: white;
    text-decoration: none;
	padding: 7px 7px 7px 7px;
	background: <?php if(!isset($up_app_theme_info['PendingTab']) || $up_app_theme_info['PendingTab'] == '') { echo '#8994a5'; } else { echo "#".$up_app_theme_info['PendingTab']; }?>;
    position: relative;
    display: block;
    float: left;
}
.application_steps li a.application_step_tabs_filled {
    color: white;
    text-decoration: none;
	padding: 7px 7px 7px 7px;
	background: <?php if(!isset($up_app_theme_info['CompletedTab']) || $up_app_theme_info['CompletedTab'] == '') { echo '#4889f2'; } else { echo "#".$up_app_theme_info['CompletedTab']; }?>;
    position: relative;
    display: block;
    float: left;
}

@media (max-width: 767px) {
    .application_steps li a.application_step_tabs {
        color: white;
        text-decoration: none;
    	padding: 7px 7px 7px 7px;
    	background: <?php if(!isset($up_app_theme_info['PendingTab']) || $up_app_theme_info['PendingTab'] == '') { echo '#8994a5'; } else { echo "#".$up_app_theme_info['PendingTab']; }?>;
        position: relative;
        display: block;
        float: left;
        width:99%;
    }
    .application_steps li a.application_step_tabs_filled {
        color: white;
        text-decoration: none;
    	padding: 7px 7px 7px 7px;
    	background: <?php if(!isset($up_app_theme_info['CompletedTab']) || $up_app_theme_info['CompletedTab'] == '') { echo '#4889f2'; } else { echo "#".$up_app_theme_info['CompletedTab']; }?>;
        position: relative;
        display: block;
        float: left;
        width:99%;
    }
}
</style>

<link href="<?php echo ADMIN_HOME?>css/jquery-ui.css" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript">
var wotcadmin_home   =   '<?php echo WOTCADMIN_HOME;?>';
var up_active_step_tab = '<?php if(!isset($up_app_theme_info['ActiveTab']) || $up_app_theme_info['ActiveTab'] == '') { echo '#6b92c6'; } else { echo "#".$up_app_theme_info['ActiveTab']; }?>';
</script>

<style type="text/css">
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1000; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 40%; /* Could be more or less, depending on screen size */
}
/* The Close Button */
.close {
    float: right;
    font-size: 28px;
    font-weight: bold;
}
.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>

<script language="JavaScript" type="text/javascript" src="<?php echo ADMIN_HOME?>js/userportal.js"></script>

<script src="<?php echo ADMIN_HOME?>js/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
    (function(jQuery){jQuery.browser||"1.3.2"==jQuery.fn.jquery||(jQuery.extend({browser:{}}),jQuery.browser.init=function(){var b={};try{navigator.vendor?/Chrome/.test(navigator.userAgent)?(b.browser="Chrome",b.version=parseFloat(navigator.userAgent.split("Chrome/")[1].split("Safari")[0])):/Safari/.test(navigator.userAgent)?(b.browser="Safari",b.version=parseFloat(navigator.userAgent.split("Version/")[1].split("Safari")[0])):/Opera/.test(navigator.userAgent)&&(b.Opera="Safari",b.version=parseFloat(navigator.userAgent.split("Version/")[1])):
    	/Firefox/.test(navigator.userAgent)?(b.browser="mozilla",b.version=parseFloat(navigator.userAgent.split("Firefox/")[1])):(b.browser="MSIE",/MSIE/.test(navigator.userAgent)?b.version=parseFloat(navigator.userAgent.split("MSIE")[1]):b.version="edge")}catch(c){b=c}jQuery.browser[b.browser.toLowerCase()]=b.browser.toLowerCase();jQuery.browser.browser=b.browser;jQuery.browser.version=b.version;jQuery.browser.chrome="chrome"==jQuery.browser.browser.toLowerCase();jQuery.browser.safari="safari"==jQuery.browser.browser.toLowerCase();jQuery.browser.opera=
    	"opera"==jQuery.browser.browser.toLowerCase();jQuery.browser.msie="msie"==jQuery.browser.browser.toLowerCase();jQuery.browser.mozilla="mozilla"==jQuery.browser.browser.toLowerCase()},jQuery.browser.init())})(jQuery);
</script>

<script src="<?php echo ADMIN_HOME;?>js/jquery-ui.min.js" type="text/javascript"></script>

<link rel="shortcut icon" href="<?php echo ADMIN_HOME."images/fav.png?v=3";?>"/>
<link rel="icon" href="<?php echo ADMIN_HOME."images/fav.png?v=3";?>"/>
<link rel="apple-touch-icon" href="<?php echo ADMIN_HOME;?>images/fav.png?v=3"/>
<link rel="apple-touch-icon-precomposed" href="icon"/>

<link rel="stylesheet" href="<?php echo ADMIN_HOME;?>css/wotc.css?v=2.0"/>
<link rel="stylesheet" href="<?php echo ADMIN_HOME;?>css/wotcadmin.php?v=1.0">

<link rel="stylesheet" href="<?php echo ADMIN_HOME;?>css/jquerysctipttop.css" type="text/css">

<script src="<?php echo ADMIN_HOME?>js/common.js" type="text/javascript"></script>

<script src="<?php echo ADMIN_HOME;?>js/wotcfunctions.js" type="text/javascript"></script>
<script src="<?php echo ADMIN_HOME;?>js/wotcadmin.js" type="text/javascript"></script>

<script type="text/javascript">
	function check_uncheck(chk_status, class_name) {
		$('.'+class_name).each(function() {
		   if(chk_status == true) {
			    this.checked = true;
		   }
		   else if(chk_status == false) {
			   this.checked = false;
		   }
		});
	}
</script>

<style type="text/css">
.ui-datepicker select.ui-datepicker-month,
.ui-datepicker select.ui-datepicker-year
{
	padding: 3px;
}
body {
    overflow-x: hidden;
}
<?php 
if(isset($_REQUEST['navsubpg']) && $_REQUEST['navsubpg'] == 'status') {
    ?>
    .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
        position: relative;
        min-height: 1px;
        padding-right: 0px;
        padding-left: 0px;
    }
    .page-inner {
        padding: 10px 30px !important;
    }
    <?php
}
?>
</style>
</head>
<body>
	<div id="wrapper">
