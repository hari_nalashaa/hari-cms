<?php
$RawFormIDs     =   explode(",", $FormIDs);
$FormIDs        =   "'" . implode ( "', '", explode(",", $FormIDs) ) . "'";
$FormIDsList    =   explode(",", $FormIDs);

if($FormType == "ApplicationForm") {
    //Get Form Questions List By
    $columns        =   "FormID, QuestionID, Question, SectionID";
    $where_info     =   array("OrgID = :OrgID", "Active = :Active");
    $params_info    =   array(":OrgID"=>$OrgID, ":Active"=>"Y");
    $where_info[]   =   "QuestionTypeID IN ('2', '3', '5', '6', '13', '14', '15', '17', '18', '1818')";
    
    if(!in_array("All", $RawFormIDs)) {
        $where_info[]   =   "FormID IN (".$FormIDs.")";
    }
    
    $form_que_res   =   G::Obj('FormQuestions')->getFormQuestionsAndTemplatesInfo("FormQuestions", $columns, $where_info, "QuestionID", "FormID, SectionID, QuestionOrder ASC", array($params_info));
    $form_que_list  =   $form_que_res['results'];
    
    //Get Form Sections List
    $where_info     =   array("OrgID = :OrgID");
    $params_info    =   array(":OrgID"=>$OrgID);
    
    if(!in_array("All", $RawFormIDs)) {
        $where_info[]   =   "FormID IN (".$FormIDs.")";
    }
    
    $form_sec_res       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsInfo("FormID, SectionID, SectionTitle", $where_info, "", "FormID, SectionID ASC", array($params_info));
    $form_sections_info =   $form_sec_res['results'];
    
    $form_sections      =   array();
    for($fsi = 0; $fsi < count($form_sections_info); $fsi++) {
        $form_sections[$form_sections_info[$fsi]['FormID']][$form_sections_info[$fsi]['SectionID']] =  $form_sections_info[$fsi];
    }
    
    for($fq = 0; $fq < count($form_que_list); $fq++) {
        $Que            =   $form_que_list[$fq]['Question'];
        $QueID          =   $form_que_list[$fq]['QuestionID'];
        $section_title  =   $form_sections[$form_que_list[$fq]['SectionID']]['SectionTitle'];
        $form_id        =   $form_que_list[$fq]['FormID'];
        $section_id     =   $form_que_list[$fq]['SectionID'];
        
        $form_que_list[$fq]['FormID']       =   $form_id;
        $form_que_list[$fq]['SectionTitle'] =   $form_sections[$form_id][$section_id]['SectionTitle'];
        $form_que_list[$fq]['Question']     =   (strlen($Que) > 100) ? substr(strip_tags($Que), 0, 100) . "..." : $Que;
        
        if(in_array($QueID, $data_to_track['QuestionIDs'])) {
            $form_que_list[$fq]['Selected'] =   "True";
        }
        else {
            $form_que_list[$fq]['Selected'] =   "False";
        }
    }
}
else if($FormType == "WebForm") {
    
    //Get Form Questions List By
    $columns        =   "WebFormID, QuestionID, Question";
    $where_info     =   array("OrgID = :OrgID", "Active = :Active");
    $params_info    =   array(":OrgID"=>$OrgID, ":Active"=>"Y");
    $where_info[]   =   "QuestionTypeID IN ('2', '3', '5', '6', '13', '14', '15', '17', '18', '1818')";
    
    if(!in_array("All", $RawFormIDs)) {
        $where_info[]   =   "WebFormID IN (".$FormIDs.")";
    }
    
    $form_que_res   =   G::Obj('FormQuestions')->getFormQuestionsAndTemplatesInfo("WebFormQuestions", $columns, $where_info, "QuestionID", "WebFormID, QuestionOrder ASC", array($params_info));
    $form_que_list  =   $form_que_res['results'];
    
    for($fq = 0; $fq < count($form_que_list); $fq++) {
        $Que            =   $form_que_list[$fq]['Question'];
        $QueID          =   $form_que_list[$fq]['QuestionID'];
        $web_form_id    =   $form_que_list[$fq]['WebFormID'];
        
        $form_que_list[$fq]['WebFormID']    =   $web_form_id;
        $form_que_list[$fq]['Question']     =   (strlen($Que) > 100) ? substr(strip_tags($Que), 0, 100) . "..." : $Que;
        
        if(in_array($QueID, $data_to_track['QuestionIDs'])) {
            $form_que_list[$fq]['Selected'] =   "True";
        }
        else {
            $form_que_list[$fq]['Selected'] =   "False";
        }
    }
}
else if($FormType == "PreFilledForm") {
    
    $form_que_list  =   array();
    
    //Get Form Questions List By
    $columns        =   "PreFilledFormID, QuestionID, Question";
    $where_info     =   array("OrgID = :OrgID", "Active = :Active");
    $params_info    =   array(":OrgID"=>"MASTER", ":Active"=>"Y");
    $where_info[]   =   "QuestionTypeID IN ('2', '3', '5', '6', '13', '14', '15', '17', '18', '1818')";
    
    if(!in_array("All", $RawFormIDs)) {
        $where_info[]   =   "PreFilledFormID IN (".$FormIDs.")";
    }
    
    $form_que_res   =   G::Obj('FormQuestions')->getFormQuestionsAndTemplatesInfo("PreFilledFormQuestions", $columns, $where_info, "QuestionID", "PreFilledFormID, QuestionOrder ASC", array($params_info));
    $form_que_list  =   $form_que_res['results'];
    
    for($fq = 0; $fq < count($form_que_list); $fq++) {
        $Que            =   $form_que_list[$fq]['Question'];
        $QueID          =   $form_que_list[$fq]['QuestionID'];
        $pre_form_id    =   $form_que_list[$fq]['PreFilledFormID'];
        
        $form_que_list[$fq]['PreFilledFormID']  =   $pre_form_id;
        $form_que_list[$fq]['Question']         =   (strlen($Que) > 100) ? substr(strip_tags($Que), 0, 100) . "..." : $Que;
        
        if(in_array($QueID, $data_to_track['QuestionIDs'])) {
            $form_que_list[$fq]['Selected'] =   "True";
        }
        else {
            $form_que_list[$fq]['Selected'] =   "False";
        }
    }
}
else if($FormType == "AgreementForm") {
    $form_que_list  =   array();
    
    //Get Form Questions List By
    $columns        =   "AgreementFormID, QuestionID, Question";
    $where_info     =   array("OrgID = :OrgID", "Active = :Active");
    $params_info    =   array(":OrgID"=>$OrgID, ":Active"=>"Y");
    $where_info[]   =   "QuestionTypeID IN ('2', '3', '5', '6', '13', '14', '15', '17', '18', '1818')";
    
    if(!in_array("All", $RawFormIDs)) {
        $where_info[]   =   "AgreementFormID IN (".$FormIDs.")";
    }
    
    $form_que_res   =   G::Obj('FormQuestions')->getFormQuestionsAndTemplatesInfo("AgreementFormQuestions", $columns, $where_info, "QuestionID", "AgreementFormID, QuestionOrder ASC", array($params_info));
    $form_que_list  =   $form_que_res['results'];
    
    for($fq = 0; $fq < count($form_que_list); $fq++) {
        $Que            =   $form_que_list[$fq]['Question'];
        $QueID          =   $form_que_list[$fq]['QuestionID'];
        $agr_form_id    =   $form_que_list[$fq]['AgreementFormID'];
        
        $form_que_list[$fq]['AgreementFormID']  =   $agr_form_id;
        $form_que_list[$fq]['Question']         =   (strlen($Que) > 100) ? substr(strip_tags($Que), 0, 100) . "..." : $Que;
        
        if(in_array($QueID, $data_to_track['QuestionIDs'])) {
            $form_que_list[$fq]['Selected'] =   "True";
        }
        else {
            $form_que_list[$fq]['Selected'] =   "False";
        }
    }
}
?>
