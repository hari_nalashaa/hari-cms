<?php
include 'Configuration.inc';

use QuickBooksOnline\API\DataService\DataService;
$dataservice_config = array(
    'auth_mode'     =>  'oauth2',
    'ClientID'      =>  $IntuitObj->client_id,
    'ClientSecret'  =>  $IntuitObj->client_secret,
    'RedirectURI'   =>  $IntuitObj->redirect_url,
    'scope'         =>  "com.intuit.quickbooks.payment",
    'baseUrl'       =>  "Development"
);
$dataService = DataService::Configure($dataservice_config);

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$accessTokenObj = $OAuth2LoginHelper->exchangeAuthorizationCodeForToken($_GET['code'], $_GET['realmId']);

if($accessTokenObj->getAccessToken() != "") {
    
    $_SESSION['IntuitAccessTokenValue']                 =   $accessTokenObj->getAccessToken();
    $_SESSION['IntuitRefreshTokenValue']                =   $accessTokenObj->getRefreshToken();
    $_SESSION['IntuitAccessTokenValidationPeriod']      =   $accessTokenObj->getAccessTokenValidationPeriodInSeconds();
    $_SESSION['IntuitRefreshTokenValidationPeriod']     =   $accessTokenObj->getRefreshTokenValidationPeriodInSeconds();
    $_SESSION['IntuitAccessTokenExpiresAt']             =   str_replace("/", "-", $accessTokenObj->getAccessTokenExpiresAt());
    $_SESSION['IntuitRefreshTokenExpiresAt']            =   str_replace("/", "-", $accessTokenObj->getRefreshTokenExpiresAt());
    $_SESSION['IntuitAuthorizationCode']                =   $_GET['code'];
    $_SESSION['IntuitRealmId']                          =   $_GET['realmId'];
    $_SESSION['IntuitClientID']                         =   $IntuitObj->client_id;
    $_SESSION['IntuitClientSecret']                     =   $IntuitObj->client_secret;

    $intuit_info = array(
                        "AccessTokenValue"              =>  $accessTokenObj->getAccessToken(),
                        "RefreshTokenValue"             =>  $accessTokenObj->getRefreshToken(),
                        "AccessTokenValidationPeriod"   =>  $accessTokenObj->getAccessTokenValidationPeriodInSeconds(),
                        "RefreshTokenValidationPeriod"  =>  $accessTokenObj->getRefreshTokenValidationPeriodInSeconds(),
                        "AccessTokenExpiresAt"          =>  str_replace("/", "-", $accessTokenObj->getAccessTokenExpiresAt()),
                        "RefreshTokenExpiresAt"         =>  str_replace("/", "-", $accessTokenObj->getRefreshTokenExpiresAt()),
                        "AuthorizationCode"             =>  $_GET['code'],
                        "RealmId"                       =>  $_GET['realmId'],
                        "ClientID"                      =>  $IntuitObj->client_id,
                        "ClientSecret"                  =>  $IntuitObj->client_secret,
                        "LastModifiedDateTime"          =>  "NOW()",
                        "CreatedDateTime"               =>  "NOW()"
                        );
    
    $IntuitObj->insIntuitAccessInformation($intuit_info);
    
    echo "<h2>Token has successfully updated. ";
    echo "<a href='".ADMIN_HOME."'>Click here to go back.</a></h2>";
}
?>
