<?php
include 'Configuration.inc';

$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

// remove the session key from the database
$set_info   =   array("SessionID = ''", "Persist = ''");
$where_info =   array("SessionID = :SessionID");
$params     =   array(":SessionID"=>$_COOKIE['AID']);
$AdminUsersObj->updUsersInfo($set_info, $where_info, array($params));

// remove the session key from the users browser
setcookie("AID", "", time() - 3600, "/");

echo $ADMINObj->adminHeader();
?>
<div style="font-family:Arial;margin:80px 0 0 0;">
<p>You have successfully logged out.<br>
Viewed pages during this session may still be seen in the browsers cache.<br>
Please be sure to close your browser completely to ensure data privacy.</p>
<p><i>Thank You</i></p>
<p>Click <a href="<?php echo ADMIN_HOME; ?>login.php">here</a> to log back in.</p>
</div>
<?php
echo $ADMINObj->adminFooter();
?>