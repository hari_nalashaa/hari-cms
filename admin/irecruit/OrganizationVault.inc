<?php
$dir = PUBLIC_DIR . 'images/';

/* Processing */
if (strlen($_GET['delete']) > 4 && strlen($_GET['OrgID']) > 7) {
    unlink ($dir . $_GET['OrgID'] . '/' . $_GET['delete']);
}

if ($_FILES['newfile']['name'] != "") {
	
	$ckdir = $dir . $_POST['OrgID'];

	if (! file_exists ( $ckdir )) {
		mkdir ( $ckdir, 0700 );
		chmod ( $ckdir, 0777 );
	}
	
	$name = $_FILES ['newfile'] ['name'];
	$name = preg_replace ( '/\s/', '_', $name );
	$name = preg_replace ( '/\&/', '', $name );

	$data = file_get_contents ( $_FILES ['newfile'] ['tmp_name'] );

	$filename = $ckdir . '/' . $name;

	$fh = fopen ( $filename, 'w' );
	fwrite ( $fh, $data );
	fclose ( $fh );

	chmod ( $filename, 0644 );	
}

$where          =   array("MultiOrgID = ''");
$org_list       =   $OrganizationsObj->getOrgDataInfo("OrgID, OrganizationName", $where, 'OrganizationName');
$ORGANIZATION   =   $org_list['results'];

foreach ($ORGANIZATION as $OrgData) {
    $ACTIVE[$OrgData['OrgID']]=$OrgData['OrganizationName'];
}


// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">iRecruit Organization Vault</div>\n";

echo '<form method="POST" action="index.php" enctype="multipart/form-data">';
echo '<input type="hidden" name="pg" value="irecruit_vault">';

echo '<div style="margin-bottom:10px;"><select name="OrgID" onchange="submit()">';
echo '<option value="">Please Select</option>';

foreach ($ACTIVE as $ActiveOrgID=>$OrganizationName) {
	echo '<option value="' . $ActiveOrgID . '"';
	if ($ActiveOrgID == $_REQUEST['OrgID']) { echo ' selected'; }
	echo '>' . $ActiveOrgID . ' - ' . $OrganizationName;
	echo '</option>';
} // end foreach
echo "</select>\n";
echo "</div>\n";


if ($handle = opendir($dir)) {
   while (false !== ($orgdir = readdir($handle))) {
        if (array_key_exists($orgdir, $ACTIVE) && ($orgdir == $_REQUEST['OrgID'])) {
        	if(is_dir($dir . '/' . $orgdir)) {
        		if ($files = opendir($dir . $orgdir)) {
        			while (false !== ($file = readdir($files))) {
        				if ($file != "." && $file != "..") {
        					$f = PUBLIC_HOME . 'images/' . $orgdir . '/' . $file;
        					echo '<div style="padding:5px;">';
        					echo '<a href="' . $f . '" title="' . $f . '">' .  $file . '</a>';
        					echo '<a href="index.php?pg=irecruit_vault&OrgID=' . $_REQUEST['OrgID'] . '&delete=' . $file . '" onclick="return confirm(\'Are you sure you want to delete the following file?\\n\\n' . $file . '\\n\\n\')">';
        					echo '<img src="'. IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 8px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Delete</b>';
        					echo '</a>';
        					echo '</div>';
        				}
        			}
        			
        		}
        	
        	}
   		}    
    }
    
    closedir($handle);
}

if (strlen($_REQUEST['OrgID']) > 7) {
    echo '<div style="margin:10px;">Add file: <input type="file" name="newfile">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Upload New File"></div>';
}

echo '</form>';
?>