<div style="font-size: 12pt; font-weight: bold; margin-bottom: 20px;">iRecruit
	Icon Usage</div>

<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/information.png"
			border="0" title="Help" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Help</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/door_out.png"
			border="0" title="Log Out" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Log Out</b></td>

	</tr>
</table>


<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/application_cascade.png"
			border="0" title="Copy" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Copy</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/arrow_rotate_anticlockwise.png"
			border="0" title="Replace" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Replace</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/pencil.png"
			border="0" title="Edit" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Edit</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/lock_edit.png"
			border="0" title="Lock Edit" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Lock Edit</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/cross.png"
			border="0" title="Delete" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Delete</b></td>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/arrow_switch.png"
			border="0" title="Change Status" style="margin: 0px 3px -4px 0px;"> <b
			style="FONT-SIZE: 8pt; COLOR: #000000">Change Status</b></td>

	</tr>
</table>



<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/resultset_previous.png"
			border="0" title="Reset" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Reset</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/bullet_go_up.png"
			width="20" height="16" title="Up" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Up</b> <img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/bullet_go_down.png"
			width="20" height="16" title="Down" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Down</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/add.png"
			border="0" title="Add" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Add</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/arrow_undo.png"
			border="0" title="Back" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Go Back</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/group_go.png"
			border="0" title="New Applicants" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">New Applicants</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/photo_delete.png"
			title="Close Window" border="0" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Close Window</b></td>

	</tr>
</table>


<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/status_offline.png"
			border="0" title="Assign Approver" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Assign Approver</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/status_online.png"
			border="0" title="Reassign" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Reassign</b></td>

		<td colspan="3"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/thumb_up.png"
			border="0" title="Approval Status" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Approval Status</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/status_busy.png"
			border="0" title="Approval Started" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Approval Started</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/application_edit.png"
			border="0" title="Assign Form" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Assign Form</b></td>

	</tr>
</table>


<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/application_form_add.png"
			border="0" title="Additional Information"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Additional Information</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/stop.png"
			border="0" title="On/Off" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">On/Off</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/go.png"
			border="0" title="On/Off" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">On/Off</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/inactive.png"
			border="0" title="Expired" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Expired</b></td>

	</tr>
</table>

<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/reqqon.png"
			border="0" width="18" title="Screen Questions"
			style="margin: 0px 3px -4px 0px;"> <img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/reqqoff.png" border="0"
			width="18" title="Screen Questions" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Screen Questions</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/user_green.png"
			border="0" title="Auto Forward" style="margin: 0px 3px -4px 0px;"> <img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/user_red.png"
			border="0" title="Auto Forward" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Auto Forward</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/money_dollar.png"
			border="0" width="18" title="Costs" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Costs</b></td>



		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/world_add.png"
			title="Publish Requisition" border="0" align="left"
			style="margin: 0px 3px -4px 0px;"></a><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Publish Requisition</b></td>


		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/key.png"
			title="Key Table" border="0" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Key Table</b></td>

	</tr>
</table>

<!--
<table border="0" cellspacing="3" cellpadding="3">
	<tr>
	<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/arrow_out.png"
			title="Export" border="0" align="left"
			style="margin: 0px 3px -4px 0px;"></a><b
			style="FONT-SIZE: 8pt; COLOR: #000000">XXXX- Export Requisition</b></td>
</tr>
</table>
-->

<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/briefcase.png"
			border="0" title="Vault" style="margin: 0px 3px -4px 0px;">Vault</td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/email.png"
			border="0" title="Tickler" style="margin: 0px 3px -4px 0px;">Tickler
		</td>

	</tr>
</table>


<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/page_edit.png"
			border="0" width="18" title="Notes" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Notes</b></td>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_acrobat.png"
			border="0" title="View PDF Form" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">View PDF Form</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_magnify.png"
			border="0" title="View" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">View</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/application_form.png"
			border="0" title="View" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">View Form</b></td>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/script.png" border="0"
			title="View History" style="margin: 0px 3px -4px 0px;"> <b
			style="FONT-SIZE: 8pt; COLOR: #000000">View History</b></td>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_picture.png"
			border="0" title="View External Link"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">View External Link</b></td>

	</tr>
</table>


<table border="0" cellspacing="3" cellpadding="3">
	<tr>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_text.png"
			border="0" title="Report" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Report</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/user_go.png"
			border="0" title="Applicant Summary"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Applicant Summary</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/chart_curve.png"
			border="0" width="18" title="Chart" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Status Charts</b></td>

		<td><img src="<?php echo IRECRUIT_HOME;?>images/icons/bug.png"
			border="0" width="18" title="Verify"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Check/Verify Fields</b></td>

	</tr>
</table>

<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/basket.png"
			border="0" title="Shopping Basket" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Shopping Basket</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>images/icons/basket_put.png"
			border="0" title="Add Item" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Add Item</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/basket_remove.png"
			border="0" title="Remove Item" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Remove Item</b></td>

	</tr>
</table>




<table border="0" cellspacing="5" cellpadding="5">
	<tr>

		<td><img src="<?php echo PUBLIC_HOME; ?>images/f_logo.png" width="16"
			height="16" title="Share on Facebook" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Share on Facebook</b></td>

		<td><img src="<?php echo PUBLIC_HOME; ?>images/twitter.png" width="16"
			height="16" title="Share on Twitter" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Share on Twitter</b></td>

		<td><img src="<?php echo PUBLIC_HOME; ?>images/linkedin.png"
			width="20" height="16" title="Share on LinkedIn" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Share on LinkedIn</b></td>


	</tr>
</table>

<table border="0" cellspacing="5" cellpadding="5">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>/images/icons/sitemap_color.png"
			width="16" height="16" title="Export Data" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Export Data</b></td>

		<td><img src="<?php echo IRECRUIT_HOME; ?>/images/icons/clock.png"
			width="16" height="16" title="Schedule" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Schedule</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>/images/icons/chart_curve_add.png"
			width="16" height="16" title="Select Data" border="0"
			style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Select Data</b></td>

	</tr>
</table>

<table border="0" cellspacing="5" cellpadding="5">
	<tr>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/application_form.png"
			border="0" title="Web Form" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Web Form</b></td>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_paste.png"
			border="0" title="Agreement Form" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Agreement Form</b></td>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/report.png"
			border="0" title="Federal & State Form" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Federal & State Form</b></td>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/link.png"
			title="External Link" border="0" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">External Link</b></td>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/attach.png"
			border="0" title="Attachment" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Attachment</b></td>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/html.png"
			border="0" title="HTML Page" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">HTML Page</b></td>
		<td><img
			src="<?php echo IRECRUIT_HOME; ?>/images/icons/tick.png"
			border="0" title="Mark Complete" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Mark Complete</b></td>

		<td><img
			src="<?php echo IRECRUIT_HOME; ?>/images/icons/arrow_right.png"
			border="0" title="Next Form" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Next Form</b></td>

	</tr>
</table>


<table border="0" cellspacing="3" cellpadding="3">
	<tr>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_text.png"
			border="0" title="Printable" style="margin: 0px 0px 4px 0px;"><br>
			<b style="FONT-SIZE: 8pt; COLOR: #000000">Printable<br>Version</b></td>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/email_go.png"
			border="0" title="Email" style="margin: 0px 0px 4px 0px;"><br>
			<b style="FONT-SIZE: 8pt; COLOR: #000000">Email<br>Report</b></td>

		<td align="center"><img
			src="<?php echo IRECRUIT_HOME; ?>images/icons/page_white_put.png"
			border="0" title="Export Report" style="margin: 0px 0px 4px 0px;"><br>
			<b style="FONT-SIZE: 8pt; COLOR: #000000">Export<br>Report</b></td>

	</tr>
</table>



<table border="0" cellspacing="5" cellpadding="5">
	<tr>
		<td><a href="<?php echo IRECRUIT_HOME; ?>images/readme-icons.html">more
				icons</a></td>
	</tr>
</table>
