<?php
// Get Organizations List
$orgs_list = G::Obj ( 'TwilioConversationUsersOrgs' )->getAllTwilioConversationOrgsInfo ($OrgID);

//$last_month_records_info	= G::Obj('TwilioUsageRecordsApi')->getLastMonthUsageForAllCategories();
//print_R($last_month_records_info);
?>

<link href="css/twilio-conversations-report.css" rel="stylesheet">

<div>
	
	<h3>iRecruit Text Report:</h3>

	<form name="frmTwilioConversationsReport" id="frmTwilioConversationsReport">
	
		<strong>Organizations List: </strong>
		
		<select name="TwilioOrgsList" id="TwilioOrgsList"
			onchange="getTwilioUsersListByOrgID(this.value)">
			<option value="">Select</option>
			<?php
			for($o = 0; $o < count ( $orgs_list ); $o ++) {
				?>
				<option value="<?php echo $orgs_list[$o]['OrgID'];?>">
					<?php echo $orgs_list[$o]['OrgName'];?>
			  	</option>
				<?php
			}
			?>
		</select>
		
		&nbsp;&nbsp;
		<strong>Users List: </strong>
		<select name="TwilioUsersList" id="TwilioUsersList">
			<option value="">Select</option>
		</select>
		
		<span id="spnUsersListLoading"></span>
		
		<?php
		$from_date = date ( 'm/d/Y', strtotime ( "-180 day" ) );
		$to_date = date ( 'm/d/Y' );
		?>
		<br>
		<br>
		From Date:
		<input type="text" name="txtFromDate" id="txtFromDate" size="10" value="<?php echo $from_date?>">
		
		&nbsp;To Date:
		<input type="text" name="txtToDate" id="txtToDate" size="10" value="<?php echo $to_date?>">
		
		<input type="hidden" name="IndexStart" id="IndexStart" value="0">
		<input type="button" name="btnGetConversations" id="btnGetConversations" size="10" value="Get Conversations" onclick="getTwilioConversationsList()">
	
	</form>
	<br>
	<form name="frmTwilioConversationsReportExport" id="frmTwilioConversationsReportExport" method="post">
		<input type="hidden" name="TwilioOrgsList" id="hideTwilioOrgsList">
		<input type="hidden" name="TwilioUsersList" id="hideTwilioUsersList">
		<input type="hidden" name="txtFromDate" id="hideFromDate">
		<input type="hidden" name="txtToDate" id="hideToDate">
		<input type="button" name="btnExportConversations" id="btnExportConversations" onclick="exportTwilioConversations()" value="Export Conversations">
	</form>
</div>

<!-- Display Twilio Conversations List -->
<div id="divTwilioConversationsList"></div>

<?php
$LST = array (
		"txtFromDate",
		"txtToDate" 
);
echo $DateHelperObj->dynamicCalendar ( $LST, '' );
?>

<script type="text/javascript" src="javascript/twilio-conversations-report.js"></script>