<?php
if($_REQUEST['defaultsettings'] == 'Yes') {

	G::Obj('IrecruitUserPreferences')->setAdminDefaultTheme($_REQUEST['DefaultTheme']);
	
	echo "<script>location.href='index.php?pg=irecruit_themes&msg=dts'</script>";
	exit;
}

$themeslist         =   G::Obj('IrecruitUserPreferences')->getThemes();

$query              =   "SELECT UP.* FROM UserPreferences UP, Themes T WHERE UP.UserID = 'MASTER' AND T.ThemeID = UP.ThemeID";
$up_res             =   G::Obj('GenericQueries')->getInfoByQuery($query);
$defaultsettings    =   $up_res[0];


if($_REQUEST['dba'] == 'fa' && $_REQUEST['tid'] != '') {
	$rowtheme  =   G::Obj('IrecruitUserPreferences')->getThemeInfoByThemeID($_REQUEST['tid']);
}

if($_REQUEST['dba'] == 'fu' && $_REQUEST['tid'] != '') {
    //Update themes information	
	$set_info      =   array("ThemeName = :ThemeName", "TableHeader = :TableHeader", "TableRowHeader = :TableRowHeader", "TableColumnColor1 = :TableColumnColor1", "TableColumnColor2 = :TableColumnColor2");
	$where_info    =   array("ThemeID = :ThemeID");
	$params_info   =   array(
	                       ":ThemeName"            =>  $_REQUEST['ThemeName'],
	                       ":TableHeader"          =>  '#'.$_REQUEST['TableHeader'],
	                       ":TableRowHeader"       =>  '#'.$_REQUEST['TableRowHeader'],
	                       ":TableColumnColor1"    =>  '#'.$_REQUEST['TableColumnColor1'],
	                       ":TableColumnColor2"    =>  '#'.$_REQUEST['TableColumnColor2'],
	                       ":ThemeID"              =>  $_REQUEST['tid']
	                   );
	G::Obj('IrecruitUserPreferences')->updThemesInfo($set_info, $where_info, array($params_info));

	echo "<script>location.href='index.php?pg=irecruit_themes&msg=upd'</script>";
	exit;
}

if($_REQUEST['dba'] == 'fi') {
    //Insert themes information
	$info          =   array(
                            "ThemeName"             =>  $_REQUEST['ThemeName'],
                            "TableHeader"           =>  '#'.$_REQUEST['TableHeader'],
                            "TableRowHeader"        =>  '#'.$_REQUEST['TableRowHeader'],
                            "TableColumnColor1"     =>  '#'.$_REQUEST['TableColumnColor1'],
                            "TableColumnColor2"     =>  '#'.$_REQUEST['TableColumnColor2']
        	           );
	
	G::Obj('IrecruitUserPreferences')->insThemesInfo($info);
	echo "<script>location.href='index.php?pg=irecruit_themes&msg=ins'</script>";
	exit;
}

if($_REQUEST['dba'] == 'delete' && $_REQUEST['tid'] != '') {
    //Delete theme by id
    G::Obj('IrecruitUserPreferences')->delThemeByThemeID($_REQUEST['tid']);
	
	echo "<script>location.href='index.php?pg=irecruit_themes&msg=del'</script>";
	exit;
}

if($_GET['msg'] == 'del') {
	$msg = "<span style='color:red;'>Successfully Deleted</span>";
}
if($_GET['msg'] == 'upd') {
	$msg = "<span style='color:red;'>Successfully Updated</span>";
}
if($_GET['msg'] == 'ins') {
	$msg = "<span style='color:red;'>Successfully Inserted</span>";
}
if($_GET['msg'] == 'dts') {
	$msg = "<span style='color:red;'>Default Theme Successfully Updated</span>";
}
?>
<div style="font-size:12pt;font-weight:bold;">iRecruit Themes</div>
<div style="width: 100%;overflow:auto">
<form method="post" name="frmTheme" id="frmTheme" action="index.php?pg=irecruit_themes">
<table border="0" cellspacing="3" cellpadding="5" width="770" align="left">
		<tr>
			<td colspan="3" style="color: red"><?php echo $msg;?></td>
		</tr>
		<tr>
			<td colspan="3">
				<strong>Note*</strong>:<br><br>
				<strong style='color:red'>TableHeader:</strong> This color will be applied to calendar main navigation bar as well as the theme top left navigation toggle option
				<strong style='color:red'>TableRowHeader:</strong> This color will be applied to calendar days bar as well as the theme top navigation bar<br>
				<strong style='color:red'>TableColumnColor1:</strong> This color will be applied to calendar days having sundays and to highlight the selected left navigation menu<br>
				<strong style='color:red'>TableColumnColor2:</strong> This color will be applied to events days in calendar<br>
			</td>
		</tr>
		<tr>
			<td colspan="3">Use this page to set the colors in Hexadecimal for
				your Calendar Themes.</td>
		</tr>
		<tr>
			<td width="20%">Theme Name: &nbsp;&nbsp;</td>
			<td width="1%">&nbsp;</td>
			<td><input type="text" name="ThemeName" id="ThemeName" value="<?php if($_REQUEST['tid'] != '') echo $rowtheme['ThemeName']?>" size="30"></td>
		</tr>
		<tr>
			<td>TableHeader:</td>
			<td>#</td>
			<td><input class="color" name="TableHeader" value="<?php if($_REQUEST['tid'] != '') echo $rowtheme['TableHeader']?>" size="6"></td>
		</tr>
		<tr>
			<td>TableRowHeader:</td>
			<td>#</td>
			<td><input class="color" name="TableRowHeader" value="<?php if($_REQUEST['tid'] != '') echo $rowtheme['TableRowHeader']?>" size="6"></td>
		</tr>
		<tr>
			<td>TableColumnColor1:</td>
			<td>#</td>
			<td><input class="color" name="TableColumnColor1" value="<?php if($_REQUEST['tid'] != '') echo $rowtheme['TableColumnColor1']?>" size="6"></td>
		</tr>
		<tr>
			<td>TableColumnColor2:</td>
			<td>#</td>
			<td><input class="color" name="TableColumnColor2" value="<?php if($_REQUEST['tid'] != '') echo $rowtheme['TableColumnColor2']?>" size="6"></td>
		</tr>
		<tr>
			<td colspan="3">
			<?php 
				if($_REQUEST['tid'] != "") {
					?><input type="submit" name="update" value="Update Theme">&nbsp;&nbsp;<?php		
				}
				else {
					?><input type="button" name="create" value="Create Theme" onclick="return validateTheme()">&nbsp;&nbsp;<?php
				}
			?>
			<input type="button" name="Reset" value="Reset" onclick='location.href="index.php?pg=irecruit_themes"'>
			</td>
		</tr>
</table>
	<input type="hidden" name="tid" id="tid" value="<?php echo $_REQUEST['tid']; ?>">
	<input type="hidden" name="dba" id="dba" value="fu">
</form>
</div>
<div>
<form name="frmDefaultSettings" id="frmDefaultSettings" method="post">
<input type="hidden" name="defaultsettings" id="defaultsettings" value="Yes">
<table width="100%" cellpadding="5" cellspacing="5">
	<tr>
		<td><strong>Theme</strong></td>
		<td><strong>TableHeader</strong></td>
		<td><strong>TableRowHeader</strong></td>
		<td><strong>TableColumnColor1</strong></td>
		<td><strong>TableColumnColor2</strong></td>
		<td><strong>Default</strong></td>
		<td><strong>Action</strong></td>
	</tr>
	<?php
		for($t = 0; $t < count($themeslist); $t++) {
			?>
			<tr>
				<td><?php echo $themeslist[$t]['ThemeName']?></td>
				<td align="left">
					<div style="float:left;background-color:<?php echo $themeslist[$t]['TableHeader']?>;width:20px;height:20px"></div>
					<div style="float:left;margin-left:10px"><?php echo strtoupper($themeslist[$t]['TableHeader']);?></div>
				</td>
				<td align="left">
					<div style="float:left;background-color:<?php echo $themeslist[$t]['TableRowHeader']?>;width:20px;height:20px"></div>
					<div style="float:left;margin-left:10px"><?php echo strtoupper($themeslist[$t]['TableRowHeader']);?></div>
				</td>
				<td align="left">
					<div style="float:left;background-color:<?php echo $themeslist[$t]['TableColumnColor1']?>;width:20px;height:20px"></div>
					<div style="float:left;margin-left:10px"><?php echo strtoupper($themeslist[$t]['TableColumnColor1']);?></div>
				</td>
				<td align="left">
					<div style="float:left;background-color:<?php echo $themeslist[$t]['TableColumnColor2']?>;width:20px;height:20px"></div>
					<div style="float:left;margin-left:10px"><?php echo strtoupper($themeslist[$t]['TableColumnColor2']);?></div>
				</td>
				<td>
					<input type="radio" name="DefaultTheme" id="DefaultTheme<?php echo $themeslist[$t]['ThemeID']?>" value="<?php echo $themeslist[$t]['ThemeID']?>" onclick="this.form.submit();" <?php if($themeslist[$t]['ThemeID'] == $defaultsettings['ThemeID']) echo 'checked="checked"';?>>
				</td>
				<td align="left">
					<a href="index.php?pg=irecruit_themes&dba=fa&tid=<?php echo $themeslist[$t]['ThemeID'];?>">
						<img border="0" title="Edit" src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png">
					</a>&nbsp;
					<a href="javascript:void(0);" onclick="deleteTheme('<?php echo $themeslist[$t]['ThemeID'];?>')">
						<img border="0" title="Delete" src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png">
					</a>&nbsp;
				</td>
			</tr>
			<?php
		}
	?>
</table>
</form>
</div>
<script>
	function deleteTheme(tid) {
		var c = confirm("Do you want to delete this theme");
		if (c == true) {
		    location.href = 'index.php?pg=irecruit_themes&dba=delete&tid='+tid;
		}
	}
	function validateTheme() {
		if($("#ThemeName").val() == '') {
			$("#ThemeName").css("border", "1px solid red");
			return false;
		}
		document.getElementById('dba').value = 'fi';
		document.frmTheme.submit();
	}
</script>