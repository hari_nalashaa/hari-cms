<?php
$pages_list =   array(
                    "All"=>"All", 
                    "index.php"=>"Dashboard",
                    "applicantsSearch.php"=>"Applicants Search",
                    "createRequest.php"=>"Create Request",
                    "createRequisition.php"=>"Create Requisition",
                    "requisitionsSearch.php"=>"Requisitions Search",
                    "forms.php"=>"Application Forms"
                );

$notification_message_types = array(
                    "alert alert-info"  =>  "Information",
                    "alert alert-warning-notification"  =>  "Warning Notification",
                    "alert alert-danger"    =>  "Danger"
                );

if(isset($_REQUEST['notification_action']) && $_REQUEST['notification_action'] == 'delete')
{
	//Delete notifications
	$UserNotificationsObj->delUserNotifications($_REQUEST['notfication_id']);
	
	echo "<script>location.href='index.php?pg=irecruit_usernotifications';</script>";
}
//Insert notification
if(isset($_REQUEST['create_notification']) && $_REQUEST['create_notification'] == "Create Notification") {

	//Insert Information
	$insert_info = array(
						"NotificationSubject"		=>	$_REQUEST['txtNotificationSubject'],
						"NotificationMessage"		=>	$_REQUEST['taNotificationMessage'],
						"NotificationPage"			=>	$_REQUEST['ddlPageName'],
						"NotificationMessageType"	=>	$_REQUEST['ddlNotificationMessageType'],
						"NotificationType"			=>	$_REQUEST['ddlNotificationType'],
						"NotificationStartDateTime"	=>	$_REQUEST['start_time'],
						"NotificationEndDateTime"	=>	$_REQUEST['end_time'],
						"CreatedDateTime"			=>	"NOW()"
						);
	//Insert UserNotification Status
	$UserNotificationsObj->insUserNotification($insert_info);
	
	echo "<script>location.href='index.php?pg=irecruit_usernotifications';</script>";
}

$user_notifications_info = $UserNotificationsObj->getUserNotifications("*", array(), "CreatedDateTime DESC LIMIT 20", array());
$user_notifications_list = $user_notifications_info['results'];
?>
<div style="font-size:12pt;font-weight:bold;">User Notification Messages</div>
<br><br>
<div style="width: 95%;overflow:auto; border: 1px solid #f5f5f5">
<br>
<style>
.user_notification_msg_footer {
    position: fixed;
    width: 35%;
    right: 0;
    bottom: 0;
    margin-bottom: 0px !important;
}
.alert-danger {
    color: #a94442;
    background-color: #f2dede;
    border-color: #ebccd1;
}
.alert-info {
    color: #31708f;
    background-color: #d9edf7;
    border-color: #bce8f1;
}
.alert-warning-notification {
	color: #a94442;
	background-color: #f4d942;
	border-color: #f4d942;
}
.alert {
    padding: 8px;
    margin-bottom: 20px;
    border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
    border-radius: 4px;
}
</style>
<form action="index.php?pg=irecruit_usernotifications" method="post" name="frmCreateUserNotification" id="frmCreateUserNotification" onsubmit="return validateNotification()">
<table border="0" cellspacing="3" cellpadding="5" width="770" align="left">
	
	<tr>
		<td><strong>Create New Notification:</strong></td>
	</tr>
	
	<tr>
		<td>Pages:</td>
		<td>
			<select name="ddlPageName" id="ddlPageName">
				<?php 
					foreach($pages_list as $page_link=>$page_name) {
						echo '<option value="'.$page_link.'">'.$page_name.'</option>';
					}
				?>
			</select>
		</td>
	</tr>

	<tr>
		<td>Notification Message Type:</td>
		<td>
			<select name="ddlNotificationMessageType" id="ddlNotificationMessageType">
				<?php 
				foreach($notification_message_types as $msg_type_key=>$msg_type_val) {
				    ?><option value="<?php echo $msg_type_key;?>"><?php echo $msg_type_val;?></option><?php
				}
				?>
			</select>
			&nbsp;&nbsp;
			<span style="width: 100px" class="alert alert-info" id="sample_message">Sample Message</span>
		</td>
	</tr>
	
	<tr>
		<td>Notification Type:</td>
		<td>
		<select name="ddlNotificationType" id="ddlNotificationType" onchange="if(this.value == 'retainable') $('#retainable_block_info').show(); else $('#retainable_block_info').hide();">
			<option value="dismissible">Dismissible</option>
			<option value="retainable">Retainable</option>
		</select>
		
		<div id="retainable_block_info" style="display:none">
			<br>
			<label for='start_time'> Start Date: </label>
			<input type="text" name="start_time" id="start_time" value="">

			<label for='end_time'> End Date: </label>
				<input type="text" name="end_time" id="end_time" value="">
				<script type="text/javascript">
					$(function(){
					 	// -- Example Only - Set the date range --
						var d = new Date();
						var cur_month = d.getMonth();
						cur_month += 1;
						$('#start_time').val(d.getFullYear() + '-' + cur_month + '-' + d.getDate() + " 1:00 pm");
						// Example Only - Set the end date to 7 days in the future so we have an actual 
						d.setDate(d.getDate() + 7);
						$('#end_time').val(d.getFullYear() + '-' + cur_month + '-' + d.getDate() + " 13:45 ");
						// -- End Example Only Code --
		
						// Attach a change event to end_time - 
						// NOTE we are using '#ID' instead of '*[name=]' selectors in this example to ensure we have the correct field
						$('#end_time').change(function() {
						    $('#start_time').appendDtpicker({
							    maxDate: $('#end_time').val() // when the end time changes, update the maxDate on the start field
						    });
						});
				
						$('#start_time').change(function() {
						    $('#end_time').appendDtpicker({
							    minDate: $('#start_time').val() // when the start time changes, update the minDate on the end field
						    });
						});
				
						// trigger change event so datapickers get attached
						$('#end_time').trigger('change');
						$('#start_time').trigger('change');
					});
				</script>
		</div>
		</td>
	</tr>
	
	<tr>
		<td>Subject:<span style="color: red">*</span></td>
		<td><input type="text" name="txtNotificationSubject" id="txtNotificationSubject" style="width:300px"></td>
	</tr>
	
	<tr>
		<td>Message:</td>
		<td><textarea rows="5" cols="35" name="taNotificationMessage" id="taNotificationMessage"></textarea></td>
	</tr>
	
	<tr>
		<td colspan="2"><input type="submit" name="create_notification" id="create_notification" value="Create Notification"></td>
	</tr>

	
</table>

</form>

<?php 
if(count($user_notifications_list) > 0) {
	?>
	<table border="0" cellspacing="3" cellpadding="5" width="770" align="left">
		<tr>
			<td><strong>Subject</strong></td>
			<td><strong>Message</strong></td>
			<td><strong>Page</strong></td>
			<td><strong>Notification Message Type</strong></td>
			<td><strong>Notification Type</strong></td>
			<td><strong>Action</strong></td>
		</tr>
		
		<?php 
		for($i = 0; $i < count($user_notifications_list); $i++) {
			?>
			<tr>
				<td><?php echo $user_notifications_list[$i]['NotificationSubject']?></td>
				<td><?php echo $user_notifications_list[$i]['NotificationMessage']?></td>
				<td>
				    <?php
						echo $pages_list[$user_notifications_list[$i]['NotificationPage']];
					?>
				</td>
				<td><?php echo $notification_message_types[$user_notifications_list[$i]['NotificationMessageType']];?></td>
				<td>
				    <?php
						echo ucwords($user_notifications_list[$i]['NotificationType'])."<br>";
						
						if($user_notifications_list[$i]['NotificationType'] == 'retainable') {
							echo " Start Date: ".$user_notifications_list[$i]['NotificationStartDateTime'] . " <br> End Date: " . $user_notifications_list[$i]['NotificationEndDateTime'];
						}
					?>
				</td>
				<td>
					<a href="index.php?pg=irecruit_usernotifications&notification_action=delete&notfication_id=<?php echo $user_notifications_list[$i]['NotificationID'];?>">
						<img src="<?php echo IRECRUIT_HOME?>images/icons/cross.png" title="Delete" border="0">
					</a>
				</td>
			</tr>
			<?php
		}
		?>
	</table>
	<?php
}
else 
{
	echo '<table border="0" cellspacing="3" cellpadding="5" width="770" align="left">
			<tr>
				<td><br><br><strong>Notifications List</strong></td>
			</tr>
			<tr>
				<td><br>No Records Found</td>
			</tr>
		  </table>';
	
}	
?>
</div>
<script>
$("#ddlNotificationMessageType").change(
    function() {
    	$('#sample_message').attr('class', this.value);
    }
);

function validateNotification() {

	var frmObj = document.frmCreateUserNotification;
	
	if(frmObj.txtNotificationSubject.value == "") {
		   alert("Subject should not be empty");
		   return false;
	}
	
    return true;	
}
</script>