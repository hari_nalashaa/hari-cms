<?php
// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">PDF PreFill Forms Update</div>\n";

if ($_GET['OrgID'])
{
    $_POST['OrgID'] = $_GET['OrgID'];
}
?>
<style>
#tableID tr:nth-child(odd) {
    background-color: #f9f9f9;
}
</style> 

<?php
$WebFormID = $_SESSION['WebFormID'];
$OrgID = $_SESSION['OrgID'];
$query = "SELECT * FROM OrgData WHERE MultiOrgID = '' ORDER BY OrganizationName";
$ORGDATA = G::Obj('GenericQueries')->getInfoByQuery($query);

/*echo '<input type="text" id="search-box" placeholder="Enter OrgID or Name" style="width:400px"/>';
echo '<br>';
echo '<div id="suggesstion-box"></div>';
*/

echo '</form>';

echo '<form method="post" action="index.php?pg=custom_webforms_update">';
echo '<input type="hidden" name="OrgID" value="' . $_POST['OrgID'] . '">';

echo '<table border="0" cellspacing="5" cellpadding="15" width="600">';
$query = "select WebFormID, QuestionID,OrgID, Question from WebFormQuestions where WebFormID ='" . $WebFormID . "' and OrgID='" . $OrgID . "' and QuestionTypeID NOT IN (8,9,10,22,30,40,45,50,60,90,98,99,100,120) order by QuestionOrder";
 
$FEATURES = G::Obj('GenericQueries')->getInfoByQuery($query, array());
$rowcolor = "#eeeeee";
echo '<table style="width:90%;border: 1px solid #ddd;"" cellspacing="3" cellpadding="0" border: 1px solid black id="tableID" class="display">  <tr bgcolor="' . $rowcolor . '" style="width:100%;text-align:center">
       <thead>
      <tr bgcolor="' . $rowcolor . '" style="width:100%;text-align:center;background-color: rgb(238, 238, 238);height: 35px;">
     
         <th>Question</th>
         <th>Input</th>

  </tr></thead>';
foreach ($FEATURES as $AF)
{

    $query_pdffield = "select * from CustomPDFForms where FormID ='" . $AF['WebFormID'] . "' and QuestionID='" . $AF['QuestionID'] . "' ";
    $answers = G::Obj('GenericQueries')->getInfoByQuery($query_pdffield, array());
    echo '<input type="hidden" name="WebFormID" value="' . $AF['WebFormID'] . '">';
    echo '<input type="hidden" name="QuestionID[]" value="' . $AF['QuestionID'] . '">';

    echo '<tr bgcolor="' . $rowcolor . '">
              <td style="text-align:left">' . $AF['Question'] . '</td>';
    if (!empty($answers))
    {
        echo '<td style="text-align:center"><input type="text" name="pdffield[]" value="' . $answers[0]['PDFField'] . '"></td>';
    }
    else
    {
        echo '<td style="text-align:center"><input type="text" name="pdffield[]" value=""></td>';
    }

    echo '</tr>';
    $a++;
}
/*echo '<tr>
	<td colspan="100%" align="center">
       <input type="submit" name="submit" value="Update status">
       <a href="index.php?pg=custom_webforms"><input type="button" name="Back"  value="Back"></a>
</td></tr>';*/
echo '</table>';
echo '<br><div style="text-align:center;"><input type="submit" name="submit" value="Update status"><a href="index.php?pg=custom_webforms&search='.$OrgID.'">&nbsp;&nbsp;<input type="button" name="Back"  value="Back" style="margin-right:17%;"></a></div>';
// end while

?>
</form>
<?php 
if(isset($_SESSION['status']) && $_SESSION['status'] ==1 ){
         echo '<div style="margin-left: 37%;margin-top: inherit;color:green;">Succesfully Updated </div>';
    
}

 
//} // end POST OrgID

?>
<script>

$(document).ready(function() {
	$("#search-box").bind("keyup click", function() {
		$.ajax({
    		type: "POST",
    		url: "getOrganizations.php",
    		data:'keyword='+$(this).val(),
    		beforeSend: function() {
    			$("#search-box").css("background-color","#F5F5F5");
    		},
    		success: function(data){
    			$("#suggesstion-box").show();
    			$("#suggesstion-box").html(data);
    			$("#search-box").css("background","#FFF");
    		}
		});
	});




    $('#example').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );


});


function getSubscriptionInfo(obj) {
    var val =  obj.textContent;

	var org_parts = val.split(" - ");
	var OrgID = org_parts[0];
	
	document.forms['frmOrgList'].OrgID.value = OrgID;

	document.forms['frmOrgList'].submit();
}	
</script>
