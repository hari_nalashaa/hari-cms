<h3>Twilio Accounts</h3>
<?php
if(isset($_REQUEST['btnSubmit']) 
	&& $_REQUEST['btnSubmit'] == "Submit"
	&& $_REQUEST['OrgsList'] != ""
	&& $_REQUEST['account_sid'] != ""
	&& $_REQUEST['auth_token'] != ""
	&& $_REQUEST['licenses'] != ""
	&& $_REQUEST['credits'] != ""
	&& $_REQUEST['billamt'] != "") {

	$info	=	array(
					"OrgID"				=>	$_REQUEST['OrgsList'],
					"AccountSid"		=>	$_REQUEST['account_sid'],
					"AuthToken"			=>	$_REQUEST['auth_token'],
					"MessageServiceID"	=>	$_REQUEST['message_service_id'],
					"ChatServiceID"		=>	$_REQUEST['chat_service_id'],
					"Licenses"		=>	$_REQUEST['licenses'],
					"Credits"		=>	$_REQUEST['credits'],
					"BillAmt"		=>	$_REQUEST['billamt'],
					"StateID"			=>	$_REQUEST['StateNames'],
					"AreaCode"			=>	$_REQUEST['AreaCodesList'],
					"CreatedDateTime"	=>	"NOW()",
					"UpdatedDateTime"	=>	"NOW()"
				);
	$skip	=	array("CreatedDateTime");
	$ins_twilio_act_info = G::Obj('TwilioAccounts')->insUpdateTwilioAccount($info, $skip);
	
	echo '<script type="text/javascript">location.href="index.php?pg=irecruit_twilio_accounts&msg=inssuc";</script>';
}

if (isset($_GET['action']) && ($_GET['action'] == "expire") && ($_GET['orgid'] != "")) {

	G::Obj('TwilioTextingAgreement')->delTextingAgreementByOrgID($_GET['orgid']);

}

if (isset($_GET['action']) && ($_GET['action'] == "inactivate") && ($_GET['orgid'] != "")) {

	G::Obj('TwilioAccounts')->inactivateAccountByOrgID($_GET['orgid']);

}

//Get Twilio Accounts
$account_info		=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($_REQUEST['OrgsList']);

$twilio_results		=	G::Obj('TwilioAccounts')->getTwilioAccountsList();
$twilio_accounts	=	$twilio_results['results'];

//Get Area Codes List
$country_code		=	"US";
$area_codes_results	=	G::Obj('AreaCodes')->getAreaCodesByCountryCode($country_code);
$area_codes_list	=	$area_codes_results['results'];

$area_codes			=	array();
for($ac = 0; $ac < count($area_codes_list); $ac++) {
	$area_codes[$area_codes_list[$ac]['ID']]['StateName']		=	$area_codes_list[$ac]['StateName'];
	$area_codes[$area_codes_list[$ac]['ID']]['AreaCodesList']	=	$area_codes_list[$ac]['AreaCodesList'];
}

$area_codes_json	=	json_encode($area_codes);


// initial select form
echo '<form name="frmTwilioAccounts" id="frmTwilioAccounts" method="GET" action="index.php?pg=irecruit_twilio_accounts">';

echo '<input type="hidden" name="pg" value="irecruit_twilio_accounts">';

$where_info         =   array("MultiOrgID = ''");
$orgs_info          =   $OrganizationsObj->getOrgDataInfo("OrgID, OrganizationName", $where_info, 'OrganizationName', array());
$orgs_info_results  =   $orgs_info['results'];
$orgs_info_count    =   $orgs_info['count'];

echo '<table style="border-spacing: 3pt">';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Organizations List: ';
echo '</td>';

echo '<td>';
echo '<select name="OrgsList" id="OrgsList" onchange="document.forms[\'frmTwilioAccounts\'].submit();">';
echo '<option value="">Please Select</option>';
for ($o = 0; $o < $orgs_info_count; $o ++) {
    echo '<option value="' . $orgs_info_results[$o]['OrgID'] . '"';
    if ($orgs_info_results[$o]['OrgID'] == $_REQUEST['OrgsList']) {
        echo ' selected';
    }
    echo '>' . $orgs_info_results[$o]['OrgID'] . ' - ' . $orgs_info_results[$o]['OrganizationName'] . '</option>';
} // end while
echo '<select>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'State Name: ';
echo '</td>';

echo '<td>';
echo '<select name="StateNames" id="StateNames" onchange="if(this.value != \'\') getCodesList(this.value)">';
echo '<option value="">Select State</option>';

foreach($area_codes as $id => $area_code_info) {
	if($account_info['StateID'] == $id) $state_selected = ' selected="selected"';
	echo '<option value="'.$id.'" '.$state_selected.'>'.$area_code_info['StateName'].'</option>';
	unset($state_selected);
}

echo '<select>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Area Codes List: ';
echo '</td>';

echo '<td>';
echo '<select name="AreaCodesList" id="AreaCodesList" onchange="if(this.value != \'\') getAvailablePhoneNumbers(this.value)">';
echo '<option value="">Select Area Code</option>';

foreach($area_codes as $id => $area_code_info) {
	echo '<option value="'.$id.'">'.$area_code_info['StateName'].'</option>';
}

echo '<select>&nbsp;<span id="check_phone_numbers_msg" style="color:blue"></span>';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Account Sid: ';
echo '</td>';
echo '<td>';
echo '<input type="text" size="50" name="account_sid" id="account_sid" value="'.$account_info['AccountSid'].'">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Auth Token: ';
echo '</td>';
echo '<td>';
echo '<input type="text" size="50" name="auth_token" id="auth_token" value="'.$account_info['AuthToken'].'">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Message Service ID: ';
echo '</td>';
echo '<td>';
echo '<input type="text" size="50" name="message_service_id" id="message_service_id" value="'.$account_info['MessageServiceID'].'">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Chat Service ID: ';
echo '</td>';
echo '<td>';
echo '<input type="text" size="50" name="chat_service_id" id="chat_service_id" value="'.$account_info['ChatServiceID'].'">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Licenses: <i style="color:red">(old)</i>';
echo '</td>';
echo '<td>';
echo '<select name="licenses" id="licenses">';
for($ul = 0; $ul <= 10; $ul++) {
    $selected = ($account_info['Licenses'] == $ul) ? ' selected="selected"' : '';
    echo '<option value="'.$ul.'" '.$selected.'>'.$ul.'</option>';
}
echo '</select>';
echo '&nbsp;&nbsp;';
echo '(set to zero to activate new credit billing amounts. expire current agreement for reactivation.)';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Credits: ';
echo '</td>';
echo '<td>';
echo '<input type="text" size="3" name="credits" id="credits" value="'.$account_info['Credits'].'" maxlength="3">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td style="text-align:right">';
echo 'Monthly Billing Amount: ';
echo '</td>';
echo '<td>';
echo '<input type="text" size="10" name="billamt" id="billamt" value="'.$account_info['BillAmt'].'" maxlength="6">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td colspan="2" style="height:20px;text-align:center;">';
echo '<input type="submit" name="btnSubmit" id="btnSubmit" value="Submit">';
echo '</td>';
echo '</tr>';

echo '</table>';
echo '</form>';

echo '<br><br>';

echo "<table cellpadding='0' cellspacing='0' border='0' width='95%'>";
echo "<tr>";
echo "<td align='left' colspan='6' style='color:blue'>";
if(isset($_GET['msg']) && $_GET['msg'] == 'inssuc')
{
	echo 'Successfully added and modified your account.';
	echo '<br><br>';
}
echo "</td>";
echo "</tr>";
echo "</table>";

echo "<table cellpadding='5' cellspacing='1' border='1' width='95%'>";

echo "<tr style=\"background-color:teal;color:#ffffff;\">";
echo "<th align='left'>Organization Name</th>";
echo "<th align='center'>Agreement Date</th>";
echo "<th align='center'>Last Billed</th>";
echo "<th align='center'>Area Code</th>";
echo "<th align='center'>Older License Cost</th>";
echo "<th align='center'>Credits</th>";
echo "<th align='center'>Billing Amount</th>";
echo "<th align='center'>Credits In Use</th>";
echo "<th align='center'>Action</th>";
echo "</tr>";


$bgcolor_ok="#ffffff";
$bgcolor_trial="#f7baa6";
$bgcolor_demo="#ababab";
$bgcolor_paper="#b1f0ba";
$bgcolor_no="#ff905c";

$key = "<div style=\"background-color:" . $bgcolor_trial . ";width:80px;text-align:center;padding:10px;float:left;\">";
$key .= "<strong>Trial</strong>";
$key .= "</div>"; 

$key .= "<div style=\"background-color:" . $bgcolor_no . ";width:120px;text-align:center;padding:10px;float:left;\">";
$key .= "<strong>No Agreement</strong>";
$key .= "</div>"; 

$key .= "<div style=\"background-color:" . $bgcolor_paper .  ";width:100px;text-align:center;padding:10px;float:left;\">";
$key .= "<strong>Paper Bill</strong>";
$key .= "</div>"; 

$key .= "<div style=\"background-color:" . $bgcolor_demo . ";width:90px;text-align:center;padding:10px;float:left;\">";
$key .= "<strong>Demo</strong>";
$key .= "</div>"; 

$key .= "<div style=\"clear:both;\"></div>";


if(count($twilio_accounts) > 0) {

	$query  = "select OrgID, count(*) as CNT from TwilioConversations";
        $query  .= " group by OrgID";
	$params =   array();
	$RESULT =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
	$CONVERSATION=array();
	foreach ($RESULT as $C) {
		$CONVERSATION[$C['OrgID']]=$C['CNT'];
	}

	for($ta = 0; $ta < count($twilio_accounts); $ta++)
	{
		$org_name       =       $twilio_accounts[$ta]['OrgID'] . " - ";
		$org_name	.=	G::Obj('OrganizationDetails')->getOrganizationName($twilio_accounts[$ta]['OrgID'], '');
		$old_amount     =   (($twilio_accounts[$ta]['Licenses'] - 1) * 20) + 59;

		$AGREEMENT = G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($twilio_accounts[$ta]['OrgID']);


		if($AGREEMENT['IntuitCardID'] == "trial") {
		  $bgcolor=$bgcolor_trial;
		} else if ($AGREEMENT['IntuitCardID'] == "demo") {
		  $bgcolor=$bgcolor_demo;
		} else if ($AGREEMENT['IntuitCardID'] == "paperbill") {
		  $bgcolor=$bgcolor_paper;
		} else if ($AGREEMENT['IntuitCardID'] == "") {
		  $bgcolor=$bgcolor_no;
		} else {
		  $bgcolor=$bgcolor_ok;
		}


		echo "<tr style=\"background-color:".$bgcolor."\">";
		echo "<td align='left'>".$org_name."</td>";
		echo "<td align='center'>";
		if ($AGREEMENT['SubscriptionDate'] != "") {
		  echo date("m/d/Y H:i:s",strtotime($AGREEMENT['SubscriptionDate']));
		} else {
		  echo "<i>no agreement</i>";
		}
		echo "</td>";
		echo "<td align='center'>";
		if (($AGREEMENT['LastBilledDateTime'] != "") && ($AGREEMENT['LastBilledDateTime'] != "2599-12-31 00:00:00")) {
		  echo date("m/d/Y H:i:s",strtotime($AGREEMENT['LastBilledDateTime']));
		} else {
		  echo "-";
		}	
		echo "</td>";
		echo "<td align='center'>".$twilio_accounts[$ta]['AreaCode']."</td>";
		echo "<td align='center'>";
		if ($twilio_accounts[$ta]['Licenses'] > 0) {
		echo "<i style=\"color:red\">".$twilio_accounts[$ta]['Licenses']." - $".number_format($old_amount, 2, '.', ',')."</i>";
		} else {
			echo "-";
		}
		echo "</td>";
		echo "<td align='center'>".$twilio_accounts[$ta]['Credits']."</td>";
		echo "<td align='center'>$".$twilio_accounts[$ta]['BillAmt']."</td>";
		echo "<td align='center'>";
		if (($AGREEMENT['SubscriptionDate'] == "") && ($CONVERSATION[$twilio_accounts[$ta]['OrgID']] == "")) {
		echo '<a href="javascript:void(0);" onclick="inactivateAccount(\'' . $twilio_accounts[$ta]['OrgID'] . '\',\'' . str_replace("'", "\\'", $org_name ) . '\')">';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/stop.png" title="Inactivate Account" style="margin: 0px 3px -4px 0px;" border="0"> Inactivate Account';
		echo "</a>";
		} else {
		echo ($CONVERSATION[$twilio_accounts[$ta]['OrgID']] != "") ? $CONVERSATION[$twilio_accounts[$ta]['OrgID']] : 0;
		}
		echo "</td>";
		echo "<td align='center'>";
		echo "<a href='index.php?pg=irecruit_twilio_accounts&OrgsList=" . $twilio_accounts[$ta]['OrgID'] . "'>";
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" title="Edit" style="margin: 0px 3px -4px 0px;" border="0">';
		echo "</a>";
		if ($AGREEMENT['SubscriptionDate'] != "") {
		echo "&nbsp;&nbsp;";
		echo '<a href="javascript:void(0);" onclick="expireAgreement(\'' . $twilio_accounts[$ta]['OrgID'] . '\',\'' . str_replace("'", "\\'", $org_name ) . '\')">';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/date_delete.png" title="Expire Agreement" style="margin: 0px 3px -4px 0px;" border="0">';
		echo "</a>";
		}
		echo "</td>";
		echo "</tr>";
	}
}
else {
	echo "<tr>";
	echo "<td align='center' colspan='6'>No records found.</td>";
	echo "</tr>";
}

echo "</table>";

echo $key;
?>
<script type="text/javascript">
var area_codes_info	=	JSON.parse('<?php echo $area_codes_json;?>');
var state_id		=	'<?php echo $account_info['StateID']; ?>';
var area_code		=	'<?php echo $account_info['AreaCode']; ?>';

var selected_index 	=	0;	

function getCodesList(area_id) {
	var area_codes_list_json = area_codes_info[area_id].AreaCodesList;
	area_codes_list = JSON.parse(area_codes_list_json);

	$('#AreaCodesList').children('option:not(:first)').remove();

	var area_index = 1;
	$.each(area_codes_list, function(key, value) {
	     $('#AreaCodesList')
	         .append($("<option></option>")
	         .attr("value", value)
	         .text(value));

         if(area_code != "") {
            new_value = value.toString(); 
			if(area_code == new_value) {
				selected_index = area_index;
			}
		 }
		 
	     area_index++;
	});

	$('#AreaCodesList').prop("selectedIndex", selected_index);
}

function getAvailablePhoneNumbers(area_code) {
	var org_id	=	$("#OrgsList").val();
	
	if(org_id == "") {
		$('#AreaCodesList').prop("selectedIndex", 0);
		alert("Please select the Organization");	
	}
	else {
		var request = $.ajax({
			method: "POST",
	  		url: "irecruit/ajax/getAvailablePhoneNumbers.php?OrgID="+org_id+"&AreaCode="+area_code,
			type: "POST",
			beforeSend: function() {
				$("#check_phone_numbers_msg").html('Please wait.. ');
			},
			success: function(data) {
				if(data == "Available") {
					$("#check_phone_numbers_msg").html("Phone numbers are available for this area code.");
				}
				else {
					$('#AreaCodesList').prop("selectedIndex", 0);
					$("#check_phone_numbers_msg").html("Phone numbers are not available for area code "+area_code+". Please select other area code.");
				}
				
	    	}
		});
	}
}

	function expireAgreement(orgid,orgname) {
                var c = confirm("Do you want to expire the agreement for " + orgname + "?");
                if (c == true) {
			location.href = 'index.php?pg=irecruit_twilio_accounts&action=expire&orgid='+orgid;
                }
	}

	function inactivateAccount(orgid,orgname) {
                var c = confirm("Do you want to inactivate " + orgname + "?");
                if (c == true) {
			location.href = 'index.php?pg=irecruit_twilio_accounts&action=inactivate&orgid='+orgid;
                }
	}


if(state_id != "") getCodesList(state_id);
</script>
