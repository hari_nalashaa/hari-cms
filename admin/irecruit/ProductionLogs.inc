<?php
$ProductionLogOrgID = $OrgID;
if(isset($_REQUEST['ddlOrgName']) && $_REQUEST['ddlOrgName'] != "") {
    $ProductionLogOrgID = $_REQUEST['ddlOrgName'];
}

$per_page   =   "10";
$from_date  =   date('m/d/Y', strtotime("-7 day"));
$to_date    =   date('m/d/Y');

if(isset($_REQUEST['txtFromDate']) 
    && $_REQUEST['txtFromDate'] != ""
    && isset($_REQUEST['txtToDate']) 
    && $_REQUEST['txtToDate'] != "") {

    $from_date  =   $_REQUEST['txtFromDate'];
    $to_date    =   $_REQUEST['txtToDate'];
    
}

$current_page = 1;
if(isset($_REQUEST['ddlPages']) && $_REQUEST['ddlPages'] != "") {
    $current_page = $_REQUEST['ddlPages'];
}

$start      =   0;
if(isset($_REQUEST['ddlPages']) && $_REQUEST['ddlPages'] != "") {
    $start  =   ($_REQUEST['ddlPages'] * $per_page) - $per_page;
}

$limit      =   " LIMIT $start, $per_page";

$org_list = G::Obj('ProductionLogs')->getProductionOrgList();

$production_log_results = G::Obj('ProductionLogs')->getProductionLogs($ProductionLogOrgID, $from_date, $to_date, 'LastModified DESC', $limit);

$production_logs = $production_log_results['results'];
$production_log_count = $production_log_results['count'];
$total_pages = $production_log_count / $per_page;
?>
<style>
.table_production_logs {
    border-collapse: collapse;
}
.table_production_logs th, .table_production_logs td {
    border: 1px solid #f5f5f5;
    padding: 5px;
}
</style>

<form name="frmProductionFilters" id="frmProductionFilters" method="post">
<table>
    <tr>
        <td>From Date: <input type="text" name="txtFromDate" id="txtFromDate" class="form-control width-auto-inline" value="<?php echo htmlspecialchars($from_date);?>">
        &nbsp;To Date: <input type="text" name="txtToDate" id="txtToDate" class="form-control width-auto-inline" value="<?php echo htmlspecialchars($to_date);?>">
        </td>
    </tr>
    <tr>
        <td>
        Organization Name: 
        <?php 
        $org_selected = '';
        ?>
        <select name="ddlOrgName" id="ddlOrgName" class="form-control width-auto-inline">
            <?php
                foreach ($org_list AS $org) {
                    $org_selected = (isset($_REQUEST['ddlOrgName']) && $_REQUEST['ddlOrgName'] == $org['OrgID']) ? ' selected="selected"' : '';
                    if($org['MultiOrgID'] == "") {
                    	?><option value="<?php echo $org['OrgID']?>" <?php echo $org_selected;?>><?php echo $org['OrganizationName']?></option><?php
                    }
                }
            ?>   
        </select>
        <?php
        if($total_pages > 1) {
        	?>
            Page:
            <select name="ddlPages" id="ddlPages" class="form-control width-auto-inline" onchange="location.href='administration.php?action=production_logs&menu=8&ddlPages='+this.value">
            <?php
            $selected = '';
            for($p = 1; $p <= $total_pages; $p++) {
                $selected = ($p == $current_page) ? ' selected="selected"' : '';
                ?><option value="<?php echo $p;?>" <?php echo $selected;?>><?php echo $p;?></option><?php
            }
        }
       ?>
       </select>
       &nbsp;<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit">
        </td>
    </tr>
</table>
</form>

<br>
<div class="table-responsive">
<table class="table_production_logs" width="90%">
    <tr>
        <td colspan="9"><h3>Production Logs</h3></td>
    </tr>
    <tr>
        <td>Client IP</td>
        <td>Last Modified</td>
        <td>Log Type</td>
        <td>Log Info</td>
        <td>Request Info</td>
        <td>Get Info</td>
        <td>Server Info</td>
        <td>Cookie Info</td>
        <td>Session Info</td>
    </tr>
    <?php
    if(count($production_logs) > 0) {
        for($pc = 0; $pc < count($production_logs); $pc++) {
            ?>
        	<tr>
                <td valign="top"><?php echo "<pre>";echo htmlspecialchars($production_logs[$pc]['ClientIP'])."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";echo htmlspecialchars($production_logs[$pc]['LastModified'])."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";echo htmlspecialchars($production_logs[$pc]['LogType'])."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";echo htmlspecialchars($production_logs[$pc]['LogInfo'])."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";print_r(json_decode($production_logs[$pc]['RequestInfo'], true))."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";print_r(json_decode($production_logs[$pc]['GetInfo'], true))."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";print_r(json_decode($production_logs[$pc]['ServerInfo'], true))."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";print_r(json_decode($production_logs[$pc]['CookieInfo'], true))."</pre>";?></td>
                <td valign="top"><?php echo "<pre>";print_r(json_decode($production_logs[$pc]['SessionInfo'], true))."</pre>";?></td>
            </tr>
        	<?php
        }
    }
    else {
    	?>
    	<tr>
            <td colspan="9" align="center"><h4>No Logs Found</h4></td>
        </tr>
    	<?php
    }
    ?>
</table>
</div>

<script>
$(document).ready(function() {
	$('#txtFromDate').datepicker();
	$('#txtToDate').datepicker();
});
</script>
