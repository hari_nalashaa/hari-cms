<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$columns    =   "OrgID, MultiOrgID, date_format(ClientSince,'%m/%d/%Y') AS ClientSince, OrganizationName";
$ORGS_RES   =   $OrganizationsObj->getOrgDataInfo($columns, array(), 'OrgID, MultiOrgID, OrganizationName', array());
$ORGS       =   $ORGS_RES['results'];

$UserCount  =   $IrecruitUsersObj->getAllUsersCount();

$ApplicationCount   =   $ApplicationsObj->getAllJobApplicationsCount();

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("iRecruit Billing Counts")
       ->setSubject("Excel")
       ->setDescription("Counts to be used for monthly billing")
       ->setKeywords("phpExcel")
       ->setCategory("Billing");
        

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setVertical('center');


$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Organizations');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Users');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Applications');


$n  =   2;
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue(sizeof($ORGS));
$objPHPExcel->getActiveSheet()->getStyle('A'.$n)->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue( $UserCount);
$objPHPExcel->getActiveSheet()->getStyle('B'.$n)->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($ApplicationCount);
$objPHPExcel->getActiveSheet()->getStyle('C'.$n)->getAlignment()->setHorizontal('center');
$n++;
               
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('System');

// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(1);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);

$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:V1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('OrgID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Organization Name');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Client Since');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Total Applicants');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Active Users');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Non-Active Users');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Total Users');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Applicant Vault');

$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Web Forms');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Agreement Forms');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('Specification Forms');
$objPHPExcel->getActiveSheet()->getCell('L1')->setValue('State & Fed Forms');
$objPHPExcel->getActiveSheet()->getCell('M1')->setValue('Requisition Approval');
$objPHPExcel->getActiveSheet()->getCell('N1')->setValue('Email Respond');
$objPHPExcel->getActiveSheet()->getCell('O1')->setValue('User Portal');
$objPHPExcel->getActiveSheet()->getCell('P1')->setValue('Internal Requisitions');
$objPHPExcel->getActiveSheet()->getCell('Q1')->setValue('Multi-Org');
$objPHPExcel->getActiveSheet()->getCell('R1')->setValue('Extended Reports');
$objPHPExcel->getActiveSheet()->getCell('S1')->setValue('DataManager');
$objPHPExcel->getActiveSheet()->getCell('T1')->setValue('Exporter');
$objPHPExcel->getActiveSheet()->getCell('U1')->setValue('Job Board Refresh');
$objPHPExcel->getActiveSheet()->getCell('V1')->setValue('Monster');

$n  =   2;
foreach ($ORGS as $O) {
  
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($O['OrgID']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($O['OrganizationName']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($O['ClientSince']);


    $objPHPExcel->getActiveSheet()->getStyle('C'.$n)->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue( applicantCount($O['OrgID'], $O['MultiOrgID']));

    list ($USERS,$activecnt) = userCount($O['OrgID']);
    
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($activecnt);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue(sizeof($USERS)-$activecnt);
$objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue(sizeof($USERS));

    $FEATURES = orgFeatures($O['OrgID']);
    
    $objPHPExcel->getActiveSheet()->getStyle('H'.$n.':W'.$n)->getAlignment()->setHorizontal('center');
   

$objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($FEATURES['ApplicantVault']);
$objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($FEATURES['WebForms']);
$objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($FEATURES['AgreementForms']);
$objPHPExcel->getActiveSheet()->getCell('K'.$n)->setValue($FEATURES['SpecificationForms']);
$objPHPExcel->getActiveSheet()->getCell('L'.$n)->setValue($FEATURES['PreFilledForms']);
$objPHPExcel->getActiveSheet()->getCell('M'.$n)->setValue($FEATURES['RequisitionApproval']);
$objPHPExcel->getActiveSheet()->getCell('N'.$n)->setValue($FEATURES['EmailRespond']);
$objPHPExcel->getActiveSheet()->getCell('O'.$n)->setValue($FEATURES['UserPortal']);
$objPHPExcel->getActiveSheet()->getCell('P'.$n)->setValue($FEATURES['InternalRequisitions']);
$objPHPExcel->getActiveSheet()->getCell('Q'.$n)->setValue($FEATURES['MultiOrg']);
$objPHPExcel->getActiveSheet()->getCell('R'.$n)->setValue($FEATURES['ExtendedReports']);
$objPHPExcel->getActiveSheet()->getCell('S'.$n)->setValue($FEATURES['DataManagerType']);
$objPHPExcel->getActiveSheet()->getCell('T'.$n)->setValue($FEATURES['Exporter']);
$objPHPExcel->getActiveSheet()->getCell('U'.$n)->setValue($FEATURES['JobBoardRefresh']);
$objPHPExcel->getActiveSheet()->getCell('V'.$n)->setValue($FEATURES['MonsterAccount']);
    
    $n++;
    
}       

// Rename 2nd sheet
$objPHPExcel->getActiveSheet()->setTitle('Organizations');


// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(2);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(45);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setVertical('center');


$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('OrgID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Organization Name');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('UserID');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('UserName');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Role');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Active');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Last Access');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Created');


$n  =   2;
foreach ($ORGS as $O) {

    if ($O['MultiOrgID'] == "") {
        list ($USERS,$activecnt) = userCount($O['OrgID']);
        foreach ($USERS as $USER) {
         
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($O['OrgID']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($O['OrganizationName']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($USER['UserID']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($USER['LastName'] . ", " . $USER['FirstName']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue( $USER['Role']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue( $USER['Active']);


            $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getAlignment()->setHorizontal('center');
            $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue( $USER['LastAccess']);
            $objPHPExcel->getActiveSheet()->getStyle('G'.$n)->getAlignment()->setHorizontal('center');
            $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue( $USER['ActivationDate']);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$n)->getAlignment()->setHorizontal('center');
            $n++;
        } // end foreach USERS
    } // end MultiOrgID

} // end foreach ORGS       

// Rename 3rd sheet
$objPHPExcel->getActiveSheet()->setTitle('Users');


// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Add some data to the second sheet, resembling some different data types
$objPHPExcel->setActiveSheetIndex(3);

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(35);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setVertical('center');


            $objPHPExcel->getActiveSheet()->getCell('A1')->setValue('OrgID');
            $objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Organization Name');
            $objPHPExcel->getActiveSheet()->getCell('C1')->setValue('RequestID');
            $objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Title');
            $objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Post Date');
            $objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Expire Date');
            $objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Owner');
            $objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Email Address');


$query      =   "SELECT R.OrgID, OD.OrganizationName, R.RequestID, R.Title, R.PostDate,";
$query      .=  " R.ExpireDate, R.Owner, U.EmailAddress";
$query      .=  " from Requisitions R, OrgData OD, Users U";
$query      .=  " WHERE OD.OrgID = R.OrgID";
$query      .=  " AND OD.MultiOrgID = R.MultiOrgID";
$query      .=  " AND R.Active = 'Y'";
$query      .=  " AND U.UserID = R.Owner";

//Set Database
$ACTIVEREQS =   G::Obj('GenericQueries')->getInfoByQuery($query);

$n  =   2;
foreach ($ACTIVEREQS as $AR) {

 
            $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($AR['OrgID']);
            $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($AR['OrganizationName']);
            $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($AR['RequestID']);
            $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($AR['Title']);
            $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($AR['PostDate']);
            $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($AR['ExpireDate']);
            $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($AR['Owner']);
            $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($AR['EmailAddress']);


    $objPHPExcel->getActiveSheet()->getStyle('E'.$n)->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getAlignment()->setHorizontal('center');
    $n++;

} // end foreach ACTIVEREQS

// Rename 3rd sheet
$objPHPExcel->getActiveSheet()->setTitle('Active Requisitions');

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="iRecruitBillingCounts.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();


function applicantCount($OrgID, $MultiOrgID) {
    $cnt    =   G::Obj('Applications')->getApplicationsCountByOrgMultiOrgID($OrgID, $MultiOrgID);
    return $cnt;
} // end function

function userCount($OrgID) {

    $table_name =   "Users U, ApplicationPermissions AP";
    $columns    =   "IF(AP.Active = '1', 'Y', 'N') AS Active, U.LastName, U.FirstName, U.Role, DATE_FORMAT(U.ActivationDate, '%m/%d/%Y') AS ActivationDate, U.LastAccess, U.UserID";
    $where      =   array("U.UserID = AP.UserID", "U.OrgID = :orgid");
    $params     =   array(":orgid" => $OrgID);
    $USERS_RES  =   G::Obj('IrecruitApplicationFeatures')->getApplicationPermissionsInfo($table_name, $columns, $where, "AP.Active, U.Role, U.LastName, U.FirstName", array($params));
    $USERS      =   $USERS_RES['results'];
    
    $activecnt  =   0;
    foreach ($USERS as $USER) {
      if ($USER['Active'] == "Y") {
        $activecnt++;
      }
    }
    
    return array($USERS, $activecnt);

} // end function


function orgFeatures($OrgID) {
    $FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID($OrgID);
    return $FEATURES;
} // end function
?>
