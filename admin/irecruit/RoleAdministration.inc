<?php
if (isset($_POST['submit'])) {
    
    if ($_POST['Requisitions'] == 0) {
        $_POST['Requisitions_Edit']     =   0;
        $_POST['Requisitions_Costs']    =   0;
    }
    
    if ($_POST['Applicants'] == 0) {
        $_POST['Applicants_Contact']                =   0;
        $_POST['Applicants_Forward']                =   0;
        $_POST['Applicants_Edit']                   =   0;
        $_POST['Applicants_Delete']                 =   0;
        $_POST['Applicants_Assign']                 =   0;
        $_POST['Applicants_Onboard']                =   0;
        $_POST['Applicants_Update_Status']          =   0;
        $_POST['Applicants_Update_Final_Status']    =   0;
        $_POST['Applicants_Affirmative_Action']     =   0;
    }
    
    if ($_POST['Forms'] == 0) {
        $_POST['Forms_Edit'] = 0;
    }
    
    if ($_POST['Internal_Forms'] == 0) {
        $_POST['Internal_Forms_Edit'] = 0;
    }

    //Update Application Permissions By UserID
    $set_info                       =   array(
                                            "Active                 =   :active", 
                                            "Admin                  =   :admin", 
                                            "Requisitions           =   :requisitions",
                                            "Requisitions_Edit      =   :reqedit", 
                                            "Requisitions_Costs     =   :reqcosts", 
                                            "Correspondence         =   :correspondence",
                                            "Forms                  =   :forms", 
                                            "Forms_Edit             =   :formsedit", 
                                            "Internal_Forms         =   :iforms", 
                                            "Internal_Forms_Edit    =   :iformsedit",
                                            "Reports                =   :reports",
                                            "Exporter               =   :exporter",
                                            "Applicants             =   :applicants",
                                            "Applicants_Contact     =   :ac",
                                            "Applicants_Forward     =   :af", 
                                            "Applicants_Edit        =   :ae",
                                            "Applicants_Delete      =   :ad",
                                            "Applicants_Assign      =   :aa",
                                            "Applicants_Onboard     =   :ao",
                                            "Applicants_Update_Status       =   :aus",
                                            "Applicants_Update_Final_Status =   :aufs",
                                            "Applicants_Affirmative_Action  =   :aaa",
                                            "Interview_Admin        =   :ia"
                                        );
    $where_info                     =   array("UserID = :userid");
    $params_info                    =   array();
    $params_info[':active']         =   $_POST['Active'];
    $params_info[':admin']          =   $_POST['Admin'];
    $params_info[':requisitions']   =   $_POST['Requisitions'];
    $params_info[':reqedit']        =   $_POST['Requisitions_Edit'];
    $params_info[':reqcosts']       =   $_POST['Requisitions_Costs'];
    $params_info[':correspondence'] =   $_POST['Correspondence'];
    $params_info[':forms']          =   $_POST['Forms'];
    $params_info[':formsedit']      =   $_POST['Forms_Edit'];
    $params_info[':iforms']         =   $_POST['Internal_Forms'];
    $params_info[':iformsedit']     =   $_POST['Internal_Forms_Edit'];
    $params_info[':reports']        =   $_POST['Reports'];
    $params_info[':exporter']       =   $_POST['Exporter'];
    $params_info[':applicants']     =   $_POST['Applicants'];
    $params_info[':ac']             =   $_POST['Applicants_Contact'];
    $params_info[':af']             =   $_POST['Applicants_Forward'];
    $params_info[':ae']             =   $_POST['Applicants_Edit'];
    $params_info[':ad']             =   $_POST['Applicants_Delete'];
    $params_info[':aa']             =   $_POST['Applicants_Assign'];
    $params_info[':ao']             =   $_POST['Applicants_Onboard'];
    $params_info[':aus']            =   $_POST['Applicants_Update_Status'];
    $params_info[':aufs']           =   $_POST['Applicants_Update_Final_Status'];
    $params_info[':aaa']            =   $_POST['Applicants_Affirmative_Action'];
    $params_info[':ia']             =   $_POST['Interview_Admin'];
    $params_info[':userid']         =   $_POST['UserID'];

    $IrecruitApplicationFeaturesObj->updApplicationPermissionsByUserID($set_info, $where_info, array($params_info));
    
    $MESSAGE = "<div style=\"margin:-25px 0 15px 324px;color:red;\">\n";
    $MESSAGE .= "Update to <b>" . $_POST['UserID'] . "</b> is complete.";
    $MESSAGE .= "</div>\n";
} // end submit
  
// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">iRecruit Master Role Administration</div>\n";

// selection form
echo "<form method=\"POST\" action=\"index.php\">\n";
echo "<input type=\"hidden\" name=\"pg\" value=\"irecruit_roles\">\n";
echo "<div>\n";
echo "<div style=\"margin-bottom:10px;\"><select name=\"UserID\" onchange=\"submit()\">\n";
echo "<option value=\"\">Please Select</option>\n";

$where_info         =   array("UserID LIKE 'MASTER_%'", "Active = 1");
$PERMISSIONKEYS_RES =   $IrecruitApplicationFeaturesObj->getApplicationPermissionsInfo("ApplicationPermissions", "*", $where_info);
$PERMISSIONKEYS     =   $PERMISSIONKEYS_RES['results'];

foreach ($PERMISSIONKEYS as $PERMIT) {
    echo "<option value=\"" . $PERMIT['UserID'] . "\"";
    if ($PERMIT['UserID'] == $_REQUEST['UserID']) {
        echo " selected";
    }
    echo ">" . $PERMIT['UserID'];
    echo "</option>\n";
} // end foreach
echo "</select>\n";
echo "</div>\n";

echo $MESSAGE;

echo "</form>\n";

// User selected for edit
if ($_POST['UserID'] != "") {
    
    echo "<form method=\"POST\" action=\"index.php\">\n";
    echo "<input type=\"hidden\" name=\"pg\" value=\"irecruit_roles\">\n";
    echo "<input type=\"hidden\" name=\"UserID\" value=\"" . htmlspecialchars($_POST['UserID']) . "\">\n";
    
    // build permission array from the colums, skip the PK
    $AP     =   $IrecruitApplicationFeaturesObj->getApplicationPermissions();
    $api    =   0;

    foreach ($AP as $key => $value) {
        if ($api > 0) {
            $sectionList[$api] = $key;
        }
        $api ++;
    } // end foreach
    // Function Settings
    $FUNCTIONS_RES  =   G::Obj('IrecruitApplicationFeatures')->getApplicationFunctionsInfo("FunctionID, Description", array(), "FunctionID", array());
    $FUNCTIONS      =   $FUNCTIONS_RES['results'];
    
    // Get UserID settings
    $PERMISSIONS    =   $IrecruitApplicationFeaturesObj->getApplicationPermissionsByUserID("*", $_POST['UserID']);
    
    echo "<div style=\"margin:0 0 0 20px;font-size:10pt;\">Select User Permissions for: <b>" . htmlspecialchars($_POST['UserID']) . "</b></div>\n";
    
    echo "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">\n";
    $sectionIndex = 1;
    foreach ($sectionList as $s) {
        $column = $s;
        
        $columndisplay = preg_replace('/\_/i', ' ', $s);
        
        // bold main categories
        if (($column == "Active") || ($column == "Admin") || ($column == "Requisitions") || ($column == "Correspondence") || ($column == "Forms") || ($column == "Internal_Forms") || ($column == "Reports") || ($column == "Exporter") || ($column == "Applicants") || ($column == "Interview_Admin")) {
            $hl = "<b>";
            $hl2 = "</b>";
            $sp = " valign=\"bottom\" height=\"30\"";
            $limit = "onoff";
        } else {
            $hl = "";
            $hl2 = "";
            $sp = "";
            $limit = "nodelete";
        }
        
        // SET PERMISSION OPTION RESTRICTIONS
        // restrict Requisitions_List permission options to Off / View (On)
        if ($column == "Requisitions_Edit" or $column == "Requisitions_Costs") {
            $limit = "onoff";
        }
        
        if ($column == "Correspondence") {
            $limit = "onoff";
        }
        
        if ($column == "Forms_Edit") {
            $limit = "onoff";
        }
        
        if ($column == "Internal_Forms_Edit") {
            $limit = "onoff";
        }
        
        // SET PERMISSION OPTION RESTRICTIONS
        // restrict Requisitions_List permission options to Off / View (On)
        if ($column == "Applicants_Contact" or $column == "Applicants_Forward" or $column == "Applicants_Edit" or $column == "Applicants_Delete" or $column == "Applicants_Assign" or $column == "Applicants_Onboard" or $column == "Applicants_Update_Status" or $column == "Applicants_Update_Final_Status" or $column == "Applicants_Affirmative_Action") {
            $limit = "onoff";
        }

        echo "<tr><td align=\"right\"" . $sp . ">" . $hl . $columndisplay . $hl2 . ":</td><td" . $sp . ">";
        echo "<select name=\"" . $column . "\">";
        
        foreach ($FUNCTIONS as $FUNCTION) {
            
            if ($limit == "onoff") {
                $restrict = "2";
            } elseif ($limit == "nodelete") {
                $restrict = "3";
            }
            
            if ($PERMISSIONS[$column] == $FUNCTION['FunctionID']) {
                $selected = " selected";
            } else {
                $selected = "";
            }
            
            if ($FUNCTION['FunctionID'] < $restrict) {
                echo "<option value=\"" . $FUNCTION['FunctionID'] . "\"" . $selected . ">" . $FUNCTION['Description'];
                echo "</option>\n";
            }
        } // end foreach
        
        echo "</select>\n";
        echo "</td></tr>\n";
    } // end sectionList
    
    ?>
<tr>
	<td colspan="2" align="center" valign="middle" valign="bottom"
		height="80"><input type="submit" name="submit"
		value="Update User Permissions" /></td>
</tr>
</table>
</form>

<?php } // end if UserID ?>
