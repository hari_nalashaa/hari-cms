<?php
$userportal_app_step_themes     = $UserPortalInfoObj->getUserPortalApplicationStepThemes();

$up_theme_info['Name']          = '';
$up_theme_info['PendingTab']    = '';
$up_theme_info['ActiveTab']     = '';
$up_theme_info['CompletedTab']  = '';

if(isset($_REQUEST['theme_action']) && $_REQUEST['theme_action'] == 'edit')
{
    $up_theme_info = $UserPortalInfoObj->getUserPortalApplicationThemeInfo($_REQUEST['ThemeID']);
}

if(isset($_POST['rdDefaultTheme']) && $_POST['rdDefaultTheme'] != "") {
    $UserPortalInfoObj->updMainUserPortalDefaultTheme($_POST['rdDefaultTheme']);
    echo "<script>location.href='index.php?pg=irecruit_up_application_themes';</script>";
}    
else if(isset($_REQUEST['btnSubmit']) && $_REQUEST['btnSubmit'] == "Submit" && isset($_REQUEST['ThemeID']) && $_REQUEST['ThemeID'] != "" && $_REQUEST['theme_action'] == "edit") {
    //Insert New Theme
    $UserPortalInfoObj->updUserPortalApplicationStepTheme($_REQUEST['ThemeID'], $_REQUEST['txtName'], $_REQUEST['txtNavbar'], $_REQUEST['txtPendingTab'], $_REQUEST['txtActiveTab'], $_REQUEST['txtCompletedTab']);
    echo "<script>location.href='index.php?pg=irecruit_up_application_themes';</script>";
}
else if(isset($_REQUEST['ThemeID'])
    && $_REQUEST['ThemeID'] != ""
    && $_REQUEST['theme_action'] == "delete") {

    $UserPortalInfoObj->delUserPortalApplicationThemeInfo($_REQUEST['ThemeID']);
    echo "<script>location.href='index.php?pg=irecruit_up_application_themes&menu=8';</script>";
}
else if(isset($_REQUEST['btnSubmit']) && $_REQUEST['btnSubmit'] == "Submit") {
    $UserPortalInfoObj->insUserPortalApplicationStepThemes($_REQUEST['txtName'], $_REQUEST['txtNavbar'], $_REQUEST['txtPendingTab'], $_REQUEST['txtActiveTab'], $_REQUEST['txtCompletedTab']);
    echo "<script>location.href='index.php?pg=irecruit_up_application_themes';</script>";
}

$userportal_app_step_themes         =   $UserPortalInfoObj->getUserPortalApplicationStepThemes($OrgID);

$up_results                         =   $userportal_app_step_themes['results'];
$userportal_app_step_themes_count   =   $userportal_app_step_themes['count'];
?>
Note: Please add theme for userportal application steps.<br><br>
<form name="frmUserPortalApplicationSteps" id="frmUserPortalApplicationSteps" method="post">
    <table class="table table-bordered">
        <tr>
            <td>Name</td>
            <td>&nbsp;&nbsp;<input type="text" name="txtName" id="txtName" value="<?php echo $up_theme_info['Name'];?>"></td>
        </tr>
        <tr>
            <td>Navbar Color</td>
            <td>#<input type="text" name="txtNavbar" id="txtNavbar" class="color" value="<?php echo $up_theme_info['Navbar'];?>"></td>
        </tr>
        <tr>
            <td>Pending Tab Color</td>
            <td>#<input type="text" name="txtPendingTab" id="txtPendingTab" class="color" value="<?php echo $up_theme_info['PendingTab'];?>"></td>
        </tr>
        <tr>
            <td>Active Tab Color</td>
            <td>#<input type="text" name="txtActiveTab" id="txtActiveTab" class="color" value="<?php echo $up_theme_info['ActiveTab'];?>"></td>
        </tr>
        <tr>
            <td>Completed Tab Color</td>
            <td>#<input type="text" name="txtCompletedTab" id="txtCompletedTab" class="color" value="<?php echo $up_theme_info['CompletedTab'];?>"></td>
        </tr>
        <tr>
            <td><input type="submit" class="btn btn-primary" name="btnSubmit" id="btnSubmit" value="Submit"></td>
        </tr>
    </table>
</form>
<br><br>

<form name="frmDefaultTheme" id="frmDefaultTheme" method="post">
<table width="100%" border="0">
    <tr>
        <th align="left">Theme Name</th>
        <th align="left">Navbar Color</th>
        <th align="left">Pending Tab Color</th>
        <th align="left">Active Tab Color</th>
        <th align="left">Completed Tab Color</th>
        <th align="left">Default</th>
        <th align="left">Action</th>
    </tr>
    <?php 
        if($userportal_app_step_themes_count > 0) {
            
        	for($j = 0; $j < $userportal_app_step_themes_count; $j++) {
                ?>
            	<tr>
            	   <td align="left"><?php echo $up_results[$j]['Name'];?></td>
            	   <td align="left"><div style="background-color:<?php echo "#".$up_results[$j]['Navbar'];?>;color: <?php echo "#".$up_results[$j]['Navbar']?>;width:40px;height:40px"></div></td>
            	   <td align="left"><div style="background-color:<?php echo "#".$up_results[$j]['PendingTab'];?>;color: <?php echo "#".$up_results[$j]['PendingTab']?>;width:40px;height:40px"></div></td>
            	   <td align="left"><div style="background-color:<?php echo "#".$up_results[$j]['ActiveTab'];?>;color: <?php echo "#".$up_results[$j]['ActiveTab'];?>;width:40px;height:40px"></div></td>
            	   <td align="left"><div style="background-color:<?php echo "#".$up_results[$j]['CompletedTab'];?>;color: <?php echo "#".$up_results[$j]['CompletedTab'];?>;width:40px;height:40px"></div></td>
            	   <td align="left"><input type="radio" name="rdDefaultTheme" value="<?php echo $up_results[$j]['ThemeID']?>" <?php if($up_results[$j]['DefaultThemeID'] == 'Y') echo ' checked="checked"';?> onclick="document.forms['frmDefaultTheme'].submit();"></td>
            	   <td align="left">
            	       <a href="index.php?pg=irecruit_up_application_themes&menu=8&ThemeID=<?php echo $up_results[$j]['ThemeID'];?>&theme_action=edit">
            	           <img src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png" title="Edit" border="0">
            	       </a>
            	       &nbsp;
            	       <a href="index.php?pg=irecruit_up_application_themes&menu=8&ThemeID=<?php echo $up_results[$j]['ThemeID'];?>&theme_action=delete">
            	           <img src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png" title="Delete" border="0">
            	       </a>
            	   </td>
            	</tr>
            	<?php
            }

        }
        else {
        	?>
        	<tr>
        	   <td colspan="5" align="center"><br><br>No themes found</td>
        	</tr>
        	<?php
        }
    ?>
</table>
</form>