<?php
if (isset($_POST['OrgID'])) {

    $params     =   array(':orgid'=>$_POST['OrgID'], ':userid'=>$_POST['UserID']);
    $set_info   =   array("OrgID = :orgid");
    $where_info =   array("UserID = :userid");
    $IrecruitUsersObj->updUsersInfo($set_info, $where_info, array($params));
    
} // end submit
  
// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">iRecruit Master Access</div>\n";

$where_info =   array("UserID IN ('dave','lstrong','Brian','cbullock1')");
$USERS_RES  =   $IrecruitUsersObj->getUserInformation("*", $where_info, "FirstName, LastName");
$USERS      =   $USERS_RES['results'];

$ORGS_RES   =   $OrganizationsObj->getOrgDataInfo("OrgID, OrganizationName", array("MultiOrgID = ''"), 'OrganizationName');
$ORGS       =   $ORGS_RES['results'];

echo "<div style=\"line-height:240%;\">\n";

$i = 0;
foreach ($USERS as $U) {
    $i ++;
    echo "<form method=\"POST\" action=\"index.php\">\n";
    
    echo "<select name=\"OrgID\" style=\"width:300px;\" onChange=\"submit();\">\n";
    foreach ($ORGS as $ORG) {
        echo "<option value=\"" . $ORG['OrgID'] . "\"";
        if ($ORG['OrgID'] == $U['OrgID']) {
            echo " selected";
        }
        echo ">";
        echo $ORG['OrganizationName'];
        echo "</option>\n";
    } // end ORGS
    echo "</select>\n";
    
    echo "&nbsp;&nbsp;&nbsp;";
    echo $U['FirstName'] . " " . $U['LastName'];
    echo "<br>";
    
    echo "<input type=\"hidden\" name=\"UserID\" value=\"" . $U['UserID'] . "\">\n";
    echo "<input type=\"hidden\" name=\"pg\" value=\"master_access\">\n";
    echo "</form>\n";
} // end USERS

echo "</div>\n";

?>
<div style="margin: 20px 0 0 40px;">
	Log into: <a href="<?php echo IRECRUIT_HOME; ?>" target="_blank">iRecruit Application</a>
</div>
