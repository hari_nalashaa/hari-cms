<?php
if ((isset($_POST['open'])) && ($_SERVER['SERVER_NAME'] != "irecruit.pairsite.com")) {
    
    $where_info =   array("Area = 'intranet'");
    $ACCESS_RES =   $IntranetAccessObj->getIntranetAccessInfo("*", $where_info);
    $ACCESS     =   $ACCESS_RES['results'];
    
    $access = <<<END
RewriteEngine On
RewriteCond %{HTTPS} !=on
RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI} [R,L]

<FilesMatch "\.(mstr|inc)$">
   Order allow,deny
   Deny from all
</FilesMatch>

Options -Indexes

#AddType application/x-httpd-php7cgi .php
#Action application/x-httpd-php7cgi /cgi-bin/php7.cgi

<Files ~ ".(cgi)$">
allow from all
satisfy any
</Files>
END;
    
    $intranet = $access;
    
    $intranet .= "\n\n";
    
    if (sizeof($ACCESS) > 0) {
        
        $intranet .= <<<END
order deny,allow
deny from all
END;
        
        $intranet .= "\n";
    }
    
    foreach ($ACCESS as $IA) {
        
        $intranet .= "allow from " . $IA['IPADDRESS'] . "\n";
    } // end while
    
    $dir = realpath(__DIR__ . '/../..');
    
    if (strpos($_SERVER["SCRIPT_NAME"], 'david') !== false) {
        $dir = "/usr/home/cmsdev/public_html";
    }
    
    if (strpos($_SERVER["SCRIPT_NAME"], 'surya') !== false) {
        $dir = "/usr/home/cmsdev/public_html";
    }
    
    if ($_POST['open'] == "Y") {
        
        $filename = $dir . '/.htaccess';
        $fh = fopen($filename, 'w');
        if (! $fh) {
            die('Cannot open file ' . $filename);
        }
        fwrite($fh, $access);
        fclose($fh);
        
        $results = "<div style=\"margin-top:20px;padding:10px;background-color:#eee;color:green;width:300px;text-align:center;\">Server is open to outside connections</div>";
    } else {
        
        $filename = $dir . '/.htaccess';
        $fh = fopen($filename, 'w');
        if (! $fh) {
            die('Cannot open file ' . $filename);
        }
        fwrite($fh, $intranet);
        fclose($fh);
        
        $results = "<div style=\"margin-top:20px;padding:10px;background-color:#eee;color:red;width:300px;text-align:center;\">Server is limited to known connections</div>";
    }
} // end isset

echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">Remote System Access</div>\n";

echo "<div style=\"line-height:240%;\">\n";
echo "<form method=\"POST\" action=\"index.php\">\n";
echo "Open server to outside connections: \n";
echo "<input type=\"radio\" name=\"open\" value=\"N\" onchange=\"submit()\"";
if ($_POST['open'] == "N") {
    echo " checked";
}
echo "> No\n";
echo "<input type=\"radio\" name=\"open\" value=\"Y\" onchange=\"submit()\"";
if ($_POST['open'] == "Y") {
    echo " checked";
}
echo "> Yes\n";
echo "<input type=\"hidden\" name=\"pg\" value=\"remote_system_access\">\n";
echo "</form>\n";
echo "</div>\n";
echo $results;
?>
