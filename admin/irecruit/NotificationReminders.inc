<script src="javascript/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="javascript/jquery-browser-migration.js" type="text/javascript"></script>
<script src="javascript/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">

<?php
$users_list_info = $IrecruitUsersObj->getUserInformation("UserID, OrgID, FirstName, LastName",array(),"LastName, FirstName");
$ul = $users_list_info['results'];

$error_message = "";
if(isset($_REQUEST['btnAddReminderNotification']) 
	&& $_REQUEST['btnAddReminderNotification'] == 'Submit'
	&& ($_REQUEST['txtDateTime'] == ""
	|| $_REQUEST['taReminderMessage'] == ""
	|| $_REQUEST['ddlUsersList'][0] == '')) {
	
	$error_message = "Please fill the mandatory fields information";
}

$success_message = "";
if(isset($_REQUEST['txtDateTime']) 
	&& $_REQUEST['txtDateTime'] != ""
	&& isset($_REQUEST['taReminderMessage'])
	&& $_REQUEST['taReminderMessage'] != "") {

	if(isset($_REQUEST['ddlUsersList']) 
	   && $_REQUEST['ddlUsersList'] != "") {
	
		foreach ($_REQUEST['ddlUsersList'] as $user_id) {
			
			$sidatetime = $DateHelperObj->getYmdFromMdy( $_REQUEST['txtDateTime'] );
			
			//Calculate PostDate Hours
			$hrs = $_REQUEST['sihrs'];
			if($_REQUEST['siampm'] == "AM") {
				if($hrs == 12) $hrs = "00";
			}
			else if($_REQUEST['siampm'] == "PM") {
				$hrs += 12;
				if($hrs == 24) $hrs -= 12;
			}
			
			//Calculate PostDate Minutes
			$mins = $_REQUEST['simins'];
			if($_REQUEST['simins'] == 0) {
				$mins = "00";
			}
			$sidatetime .= " ".$hrs.":".$mins.":00";
			
			$lastinsertid = $RemindersObj->createReminder("", $_REQUEST['taReminderMessage'], $sidatetime, $user_id);

			if($lastinsertid > 0) $success_message = "Calendar reminder event successfully created.";
		}

	}
	
}
?>
<div style="font-size:12pt;font-weight:bold;margin-bottom:20px;">Create Calendar Reminder Event</div>
<form name="frmNotificationReminders" id="frmNotificationReminders" method="post">
<table>
	<?php 
		if(isset($_REQUEST['btnAddReminderNotification']) && $error_message != "") {
			?>
			<tr>
				<td colspan="2" style="color: red">
					<?php echo $error_message;?>
				</td>
			</tr>
			<?php
		}
		if(isset($_REQUEST['btnAddReminderNotification']) && $success_message != "") {
			?>
			<tr>
				<td colspan="2" style="color: blue">
					<?php echo $success_message;?>
				</td>
			</tr>
			<?php
		}
	?>
	<tr>	
		<td>
		Select Users:
		<span style="color: red">*</span>
		</td>
		<td>
			<select name="ddlUsersList[]" id="ddlUsersList" multiple="multiple">
				<option value="MASTER">MASTER</option>
				<?php 
				for($ui = 0; $ui < count($ul); $ui++) {
					?><option value="<?php echo $ul[$ui]['UserID'];?>">
						<?php echo $ul[$ui]['LastName'].", ".$ul[$ui]['FirstName'];?>
					  </option>
					<?php
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			Reminder Date Time:
			<span style="color: red">*</span>
		</td>	
		<td>
			<input type="text" name="txtDateTime" id="txtDateTime">
			
			<select name="sihrs" id="sihrs">
				<?php
				for($h = 1; $h <= 12; $h ++) {
					$h = ($h < 10) ? '0'.$h : $h;
					?><option value="<?php echo $h;?>"><?php echo $h;?></option><?php
				}
				?>
			</select>
			<select name="simins" id="simins">
			<?php
			for($m = 0; $m <= 45; $m += 15) {
				$m = ($m == 0) ? '00' : $m;
				?><option value="<?php echo $m;?>"><?php echo $m;?></option><?php
			}
			?>
			</select>
			<select name="siampm" id="siampm">
				<option value="AM">AM</option>
				<option value="PM">PM</option>
			</select></td>
		</td>
	</tr>
	<tr>
		<td>
		Reminder Notification Message:
		<span style="color: red">*</span>
		</td>	
		<td>
			<textarea name="taReminderMessage" id="taReminderMessage" rows="4" cols="55"></textarea>
		</td>
	</tr>
	<tr>	
		<td colspan="2">
			<input type="submit" name="btnAddReminderNotification" id="btnAddReminderNotification" value="Submit">
		</td>
	</tr>
</table>
</form>
<script>
$(document).ready(function() {
	$('#txtDateTime').datepicker();
});
</script>
