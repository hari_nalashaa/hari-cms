<?php
if (isset($_POST['submit'])) {
    
    $where_info =   array("OrgID = :OrgID");
    $params     =   array(":OrgID" => $_POST['OrgID']);
    G::Obj('GenericQueries')->delRows("ApplicationFeatures", $where_info, array($params));
    
    $info       =   array("OrgID" => $_POST['OrgID']);
    G::Obj('GenericQueries')->insRowsInfo("ApplicationFeatures", $info);
    
    foreach ($_POST as $key => $value) {
        list ($OrgIDxx, $Table) = explode(":", $key);
        
        if (($Table != "") && (strpos($Table, '_orig') === false)) {
            $update_query   =   "UPDATE ApplicationFeatures SET " . $Table . " = :value  WHERE OrgID = :orgid";
            $params         =   array(':value' => $value, ':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->updInfoByQuery($update_query, array($params));
        } // end if
        
        if (($Table == "DataManagerType") && ($_POST[$_POST['OrgID'] . ':' . $Table] == 'XML')) {
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'XML'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
        
        if (($Table == "DataManagerType") && ($_POST[$_POST['OrgID'] . ':' . $Table] == 'Exporter')) {
            
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'Exporter'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $where_info = array("OrgID = :OrgID");
            $params = array(':OrgID' => $_POST['OrgID']);
            G::Obj('GenericQueries')->delRows("SageAbraMap", $where_info, array($params));
            
            $query = "INSERT INTO SageAbraMap";
            $query .= " SELECT :orgid, iRecruitTable, FormName, iRecruitField, Section, SubSection,";
            $query .= " DB, DBTable, AbraField, SortOrder";
            $query .= " FROM SageAbraMap WHERE OrgID = 'Exporter'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

            $query = "INSERT IGNORE INTO WebForms";
            $query .= " SELECT :orgid, WebFormID, FormStatus, FormName, SortOrder";
            $query .= " FROM WebForms WHERE OrgID = 'MASTER' AND WebFormID IN ('55a67554cee7a')";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $query = "INSERT IGNORE INTO WebFormQuestions";
            $query .= " SELECT :orgid, FormID, WebFormID, QuestionID, Question, QuestionTypeID, size, maxlength,";
            $query .= " rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
            $query .= " FROM WebFormQuestions WHERE OrgID = 'MASTER' AND WebFormID IN ('55a67554cee7a')";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
        
        if (($Table == "DataManagerType") && ($_POST[$_POST['OrgID'] . ':' . $Table] == 'HRMS')) {
            
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'HRMS'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $where_info = array("OrgID = :OrgID");
            $params = array(':OrgID' => $_POST['OrgID']);
            G::Obj('GenericQueries')->delRows("SageAbraMap", $where_info, array($params));
            
            $query = "INSERT INTO SageAbraMap";
            $query .= " SELECT :orgid, iRecruitTable, FormName, iRecruitField, Section, SubSection,";
            $query .= " DB, DBTable, AbraField, SortOrder";
            $query .= " FROM SageAbraMap WHERE OrgID = 'HRMS'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

            $query = "INSERT IGNORE INTO WebForms";
            $query .= " SELECT :orgid, WebFormID, FormStatus, FormName, SortOrder";
            $query .= " FROM WebForms WHERE OrgID = 'MASTER' AND WebFormID IN ('55a67554cee7a')";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $query = "INSERT IGNORE INTO WebFormQuestions";
            $query .= " SELECT :orgid, FormID, WebFormID, QuestionID, Question, QuestionTypeID, size, maxlength,";
            $query .= " rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
            $query .= " FROM WebFormQuestions WHERE OrgID = 'MASTER' AND WebFormID IN ('55a67554cee7a')";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
        
        if ($Table == "MatrixCare") {
            
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'MatrixCare'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

            $query = "INSERT IGNORE INTO WebForms";
            $query .= " SELECT :orgid, WebFormID, FormStatus, FormName, SortOrder";
            $query .= " FROM WebForms WHERE OrgID = 'MASTER' AND WebFormID IN ('55a67554cee7a')";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $query = "INSERT IGNORE INTO WebFormQuestions";
            $query .= " SELECT :orgid, FormID, WebFormID, QuestionID, Question, QuestionTypeID, size, maxlength,";
            $query .= " rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
            $query .= " FROM WebFormQuestions WHERE OrgID = 'MASTER' AND WebFormID IN ('55a67554cee7a')";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
        
        if ($Table == "WebABA") {
            
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'WebABA'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $query = "INSERT IGNORE INTO ExporterNames";
            $query .= " SELECT :orgid, ExporterID, ExportName, ExportData, now()";
            $query .= " FROM ExporterNames WHERE OrgID = 'MASTER' AND ExporterID = 'WebABA'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $query = "INSERT IGNORE INTO Exporter";
            $query .= " SELECT :orgid, ExporterID, FormType, FormID, SectionID, QuestionID, FriendlyName, QuestionTypeID, Question, SortOrder, Multi";
            $query .= " FROM Exporter WHERE OrgID = 'MASTER' AND ExporterID = 'WebABA'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
        
        if ($Table == "Lightwork") {
            
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'Lightwork'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }

        if ($Table == "Abila") {
           
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'Abila'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

            $query = "INSERT IGNORE INTO ExporterNames";
            $query .= " SELECT :orgid, ExporterID, ExportName, ExportData, now()";
            $query .= " FROM ExporterNames WHERE OrgID = 'MASTER' AND ExporterID = 'Abila'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            
            $query = "INSERT IGNORE INTO Exporter";
            $query .= " SELECT :orgid, ExporterID, FormType, FormID, SectionID, QuestionID, FriendlyName, QuestionTypeID, Question, SortOrder, Multi";
            $query .= " FROM Exporter WHERE OrgID = 'MASTER' AND ExporterID = 'Abila'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
        
        if ($Table == "Mercer") {
            
            $query = "INSERT IGNORE INTO OnboardQuestions";
            $query .= " SELECT :orgid, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows,";
            $query .= " cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
            $query .= " FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = 'Mercer'";
            $params = array(':orgid' => $_POST['OrgID']);
            G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
        }
    } // end foreach POST
} // end submit
  
// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">iRecruit Organization Features</div>\n";

if ($_GET['OrgID']) {
    $_POST['OrgID'] = $_GET['OrgID'];
}
?>
<style>
#org-list {
	margin-left: 0px;
	padding: 0px;
	background-color: #e0e1e2;
	width: 400px;
	overflow: scroll-y;
	vertical-align: top;
	margin-top: 0px;
}

#org-list li {
	list-style: none;
	margin-left: 0px;
	padding: 5px;
	width: 400px;
}

#org-list li:hover {
	list-style: none;
	margin-left: 0px;
	padding: 5px;
	width: 390px;
	color: white;
	background-color: blue;
}

#suggesstion-box {
	/*position: fixed;*/
	z-index: 100;
	padding-top: 0px;
}
</style>
<?php
// initial select form
echo '<form method="post" name="frmOrgList" action="index.php">';
echo '<input type="hidden" name="pg" value="irecruit_features">';
echo '<input type="hidden" name="OrgID" value="">';

$query  = "SELECT * FROM OrgData WHERE MultiOrgID = '' ORDER BY OrganizationName";
$ORGDATA = G::Obj('GenericQueries')->getInfoByQuery($query);

echo '<input type="text" id="search-box" placeholder="Enter OrgID or Name" style="width:400px"/>';
echo '<br>';
echo '<div id="suggesstion-box"></div>';

$hit = "";
foreach ($ORGDATA as $OD) {
    
    if ($OD['OrgID'] == $_POST['OrgID']) {
        $hit = "yes";
    }
} // end while

echo '</form>';

// if organization is selected
if ($hit == "yes") {
    
    echo '<form method="post" action="index.php">';
    echo '<input type="hidden" name="pg" value="irecruit_features">';
    echo '<input type="hidden" name="OrgID" value="' . $_POST['OrgID'] . '">';
    
    echo '<table border="0" cellspacing="5" cellpadding="15" width="600">';
    
    $query = "SELECT AF.*, OD.OrganizationName";
    $query .= " FROM ApplicationFeatures AF, OrgData OD";
    $query .= " WHERE AF.OrgID = OD.OrgID";
    $query .= " AND OD.MultiOrgID = ''";
    $query .= " AND OD.OrgID = :orgid";
    $query .= " ORDER BY OD.OrganizationName";
    $params = array(':orgid' => $_POST['OrgID']);
    $FEATURES = G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
    
    $rowcolor = "#eeeeee";
    
    foreach ($FEATURES as $AF) {
        
        echo '<tr bgcolor="' . $rowcolor . '"><td>';
        echo '<b style="font-size:12pt;">' . $AF['OrgID'] . ' - ' . $AF['OrganizationName'] . '</b><br><br>';
        
        echo '<table border="0" cellspacing="3" cellpadding="0">';
        
        echo '<tr><td align="right">User Portal:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':UserPortal" value="Y" ';
        if ($AF['UserPortal'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right" valign="top">Internal Forms:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':WebForms" value="Y" ';
        if ($AF['WebForms'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;';
        echo 'Web Forms';
        echo '<br>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':AgreementForms" value="Y" ';
        if ($AF['AgreementForms'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;';
        echo 'Agreement Forms';
        echo '<br>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':SpecificationForms" value="Y" ';
        if ($AF['SpecificationForms'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;';
        echo 'Specification Forms';
        echo '<br>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':PreFilledForms" value="Y" ';
        if ($AF['PreFilledForms'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;';
        echo 'US State & Federal';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Job Board Refresh:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':JobBoardRefresh" value="Y" ';
        if ($AF['JobBoardRefresh'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Applicant Vault:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':ApplicantVault" value="Y" ';
        if ($AF['ApplicantVault'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Requisition Request:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':RequisitionRequest" value="Y" ';
        if ($AF['RequisitionRequest'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Requisition Approval:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':RequisitionApproval" value="Y" ';
        if ($AF['RequisitionApproval'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Internal Requisitions:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':InternalRequisitions" value="Y" ';
        if ($AF['InternalRequisitions'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Multi Organization:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':MultiOrg" value="Y" ';
        if ($AF['MultiOrg'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Received Date:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':ReceivedDate" value="Y" ';
        if ($AF['ReceivedDate'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">International Translation:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':InternationalTranslation" value="Y" ';
        if ($AF['InternationalTranslation'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Apply with LinkedIn:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':ApplyWithLinkedIn" value="Y" ';
        if ($AF['ApplyWithLinkedIn'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Monster Account:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':MonsterAccount" value="Y" ';
        if ($AF['MonsterAccount'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';

        echo '<tr><td align="right">Matrix Care Onboard:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':MatrixCare" value="Y" ';
        if ($AF['MatrixCare'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">WebABA Onboard:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':WebABA" value="Y" ';
        if ($AF['WebABA'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Lightwork Onboard:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':Lightwork" value="Y" ';
        if ($AF['Lightwork'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';

        echo '<tr><td align="right">Abila Onboard:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':Abila" value="Y" ';
        if ($AF['Abila'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Mercer Onboard:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':Mercer" value="Y" ';
        if ($AF['Mercer'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';

        echo '<tr><td align="right" valign="top">Data Manager Onboard:</td><td>';
        
        echo '<input type="radio" name="' . $AF['OrgID'] . ':DataManagerType" value="N" ';
        if ($AF['DataManagerType'] == 'N') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;None<br>';

	$DataManagerVersions	=   G::Obj('IrecruitSettings')->getValue("DataManagerVersions");
        
        echo '<input type="radio" name="' . $AF['OrgID'] . ':DataManagerType" value="Exporter"';
        if ($AF['DataManagerType'] == 'Exporter') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;Sage SQL';
	echo ' - ' . $DataManagerVersions['Exporter'] . '<br>';
        
        echo '<input type="radio" name="' . $AF['OrgID'] . ':DataManagerType" value="HRMS"';
        if ($AF['DataManagerType'] == 'HRMS') {
            echo ' checked';
        }
        echo '>';
        echo '&nbsp;Sage SQL HRMS';
	echo ' - ' . $DataManagerVersions['HRMS'];

        echo '</td></tr>';
        
        echo '<tr><td align="right">Exporter:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':Exporter" value="Y" ';
        if ($AF['Exporter'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';

        
        echo '<tr><td align="right">Email Respond:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':EmailRespond" value="Y" ';
        if ($AF['EmailRespond'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';

        
        echo '<tr><td align="right">Resume Parsing:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':ResumeParsing" value="Y" ';
        if ($AF['ResumeParsing'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Paperless:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':Paperless" value="Y" ';
        if ($AF['Paperless'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Comparative Analysis:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':ComparativeAnalysis" value="Y" ';
        if ($AF['ComparativeAnalysis'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '<tr><td align="right">Interview Forms:</td><td>';
        echo '<input type="checkbox" name="' . $AF['OrgID'] . ':InterviewForms" value="Y" ';
        if ($AF['InterviewForms'] == 'Y') {
            echo ' checked';
        }
        echo '>';
        echo '</td></tr>';
        
        echo '</table>';
        
        echo '</td></tr>';
    } // end while
    
    ?>
<tr>
	<td colspan="100%" align="center"><input type="submit" name="submit"
		value="Update Features"></td>
</tr>
</table>
</form>
<?php
} // end POST OrgID
?>
<script>
$(document).ready(function() {
	$("#search-box").bind("keyup click", function() {
		$.ajax({
    		type: "POST",
    		url: "getOrganizations.php",
    		data:'keyword='+$(this).val(),
    		beforeSend: function() {
    			$("#search-box").css("background-color","#F5F5F5");
    		},
    		success: function(data){
    			$("#suggesstion-box").show();
    			$("#suggesstion-box").html(data);
    			$("#search-box").css("background","#FFF");
    		}
		});
	});

});


function getSubscriptionInfo(obj) {
    var val =  obj.textContent;

	var org_parts = val.split(" - ");
	var OrgID = org_parts[0];
	
	document.forms['frmOrgList'].OrgID.value = OrgID;

	document.forms['frmOrgList'].submit();
}	
</script>
