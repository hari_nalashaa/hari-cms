<?php
if (isset($_REQUEST['BrandID']) && $_REQUEST['BrandID'] != "") {
    $brand_info = $BrandsObj->getBrandInfo($_REQUEST['BrandID']);
}

// Update brand information
if (isset($_REQUEST['brand_action']) && $_REQUEST['brand_action'] == 'delete' && isset($_REQUEST['BrandID']) && $_REQUEST['BrandID'] != "") {
    // File Handling
    $branddir = IRECRUIT_DIR . 'vault/brands';
    $BrandsObj->delBrandInfo($_REQUEST['BrandID']);
    @unlink($branddir . "/" . $brand_info['Logo']);
    
    echo "<script>location.href='index.php?pg=irecruit_branding&msg=succdel';</script>";
    exit();
} else if (isset($_REQUEST['btnSubmit']) && $_REQUEST['btnSubmit'] != "" && isset($_REQUEST['brand_action']) && $_REQUEST['brand_action'] == 'edit' && isset($_REQUEST['BrandID']) && $_REQUEST['BrandID'] != "") {
        
        // File Handling
        $branddir = IRECRUIT_DIR . 'vault/brands';
        
        if (! file_exists($branddir)) {
            mkdir($branddir, 0700);
            chmod($branddir, 0777);
        }
        
        // Update brand information
        $BrandsObj->updBrandInfo($_REQUEST['BrandID'], $_REQUEST['txtBrandName'], $_REQUEST['txtBrandFooterText'], $_REQUEST['txtBrandUrl']);
        
        if ($_FILES['fileBrandLogo']['name'] != "") {
            @unlink($branddir . "/" . $brand_info['Logo']);
            $brand_extension = end(explode(".", $_FILES['fileBrandLogo']['name']));
            $brand_logo_name = $branddir . "/" . $_REQUEST['BrandID'] . "-" . "logo." . $brand_extension;
            $file_name = $_REQUEST['BrandID'] . "-logo." . $brand_extension;
            @move_uploaded_file($_FILES['fileBrandLogo']['tmp_name'], $brand_logo_name);
            
            // Update brand logo
            $BrandsObj->updBrandLogo($_REQUEST['BrandID'], $file_name);
        }
        
        if (isset($_POST) && count($_POST) > 0) {
            echo "<script>location.href='index.php?pg=irecruit_branding&msg=succupd';</script>";
            exit();
        }
    } else if (isset($_REQUEST['btnSubmit']) && $_REQUEST['btnSubmit'] != "") {

            // File Handling
            $branddir = IRECRUIT_DIR . 'vault/brands';
            
            if (! file_exists($branddir)) {
                mkdir($branddir, 0700);
                chmod($branddir, 0777);
            }
            
            $ins_brand_info =   array(
                                    "BrandName"     =>  $_REQUEST['txtBrandName'],
                                    "FooterText"    =>  $_REQUEST['txtBrandFooterText'],
                                    "Url"           =>  $_REQUEST['txtBrandUrl']
                                );
            $res_brand_info =   $BrandsObj->insBrandInfo($ins_brand_info);
            $BrandID        =   $res_brand_info['insert_id'];
            
            if ($_FILES['fileBrandLogo']['name'] != "") {
                $brand_extension    =   end(explode(".", $_FILES['fileBrandLogo']['name']));
                $brand_logo_name    =   $branddir . "/" . $BrandID . "-" . "logo." . $brand_extension;
                $file_name          =   $BrandID . "-logo." . $brand_extension;
                @move_uploaded_file($_FILES['fileBrandLogo']['tmp_name'], $brand_logo_name);
                
                $BrandsObj->updBrandLogo($BrandID, $file_name);
            }
            
            echo "<script>location.href='index.php?pg=irecruit_branding&msg=succins';</script>";
            exit();
        }

$brands_info_list = $BrandsObj->getBrands();
$brands_list = $brands_info_list['results'];

$brand_logo_url = IRECRUIT_HOME . "/vault/brands/" . $brand_info['Logo'];
?>

<form name="frmCreateBrand" id="frmCreateBrand" method="post"
	enctype="multipart/form-data">
	<table>
		<tr>
			<td colspan="2"><h3>Create Brand</h3></td>
		</tr>
		<tr>
			<td>Brand Name</td>
			<td><input type="text" name="txtBrandName" id="txtBrandName"
				value="<?php echo $brand_info['BrandName'];?>"></td>
		</tr>
		<tr>
			<td>Footer Text</td>
			<td><input type="text" name="txtBrandFooterText"
				id="txtBrandFooterText"
				value="<?php echo $brand_info['FooterText'];?>"></td>
		</tr>
		<tr>
			<td>Url</td>
			<td><input type="text" name="txtBrandUrl" id="txtBrandUrl"
				value="<?php echo $brand_info['Url'];?>"></td>
		</tr>
		<tr>
			<td>Logo</td>
			<td><input type="file" name="fileBrandLogo" id="fileBrandLogo">
                <?php
                if (isset($_REQUEST['brand_action']) && $_REQUEST['brand_action'] == 'edit') {
                    ?><img src="<?php echo $brand_logo_url;?>" alt="logo"
				width="30" height="30"><?php
                }
                ?>
            </td>
		</tr>
		<tr>
			<td colspan="4"><input type="submit" name="btnSubmit" id="btnSubmit"
				value="Submit"></td>
		</tr>
	</table>

</form>

<br>
<br>
<table width="90%">
	<tr>
		<td><strong>Brand Name</strong></td>
		<td><strong>Footer Text</strong></td>
		<td><strong>Url</strong></td>
		<td><strong>Logo</strong></td>
		<td><strong>Action</strong></td>
	</tr>
<?php
if (count($brands_list) > 0) {
    for ($b = 0; $b < count($brands_list); $b ++) {
        echo '<tr>';
        
        echo '<td>' . $brands_list[$b]['BrandName'] . '</td>';
        echo '<td>' . $brands_list[$b]['FooterText'] . '</td>';
        echo '<td>' . $brands_list[$b]['Url'] . '</td>';
        echo '<td><img src="' . IRECRUIT_HOME . "/vault/brands/" . $brands_list[$b]['Logo'] . '" alt="Logo" width="30" height="30"></td>';
        
        echo '<td>';
        echo '<a href="index.php?pg=irecruit_branding&BrandID=' . $brands_list[$b]['BrandID'] . '&brand_action=edit"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" alt="Edit"></a>&nbsp;';
        echo '<a href="index.php?pg=irecruit_branding&BrandID=' . $brands_list[$b]['BrandID'] . '&brand_action=delete"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" alt="Delete"></a>';
        echo '</td>';
        
        echo '</tr>';
    }
} else {
    echo '<tr>';
    echo '<td colspan="5" align="center">There are no brands</td>';
    echo '</tr>';
}
?>
</table>