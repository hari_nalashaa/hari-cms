<style>
#org-list {
    margin-left:80px;
    padding:0px;
    background-color:#e0e1e2;
    width:400px;
    overflow: scroll-y;
    vertical-align: top;
    margin-top: 0px;
}
#org-list li {
    list-style: none;
    margin-left: 0px;
    padding: 5px;
    width: 400px;
}
#org-list li:hover {
    list-style: none;
    margin-left: 0px;
    padding: 5px;
    width:390px;
    color:white;
    background-color: blue;
}
#suggesstion-box {
    position: fixed;
    z-index: 100;
    padding-top: 0px;
}
</style>
<div id="info_message" style="color:blue"></div>
<form name="frmOrgSearch" id="frmOrgSearch" method="post">
Organizations: 
<input type="text" id="search-box" placeholder="Enter OrgID or Name" style="width:400px"/>
<br>
<div id="suggesstion-box"></div>
<input type="hidden" name="OrgID" id="OrgID" value="">
<br>
Number of subscriptions: <input type="text" name="Subscriptions" id="Subscriptions" size="5" maxlength="3" onkeypress="return isNumber(event)"><br><br>
<input type="button" name="btnSubmit" id="btnSubmit" onclick="insSubscriptionInfo()" value="Save">
</form>

<div id="subscribed_requisitions">
    
</div>

<script>
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$(document).ready(function() {
	$("#search-box").bind("keyup click", function() {
		$.ajax({
    		type: "POST",
    		url: "getOrganizations.php",
    		data:'keyword='+$(this).val(),
    		beforeSend: function() {
    			$("#search-box").css("background-color","#F5F5F5");
    		},
    		success: function(data){
    			$("#suggesstion-box").show();
    			$("#suggesstion-box").html(data);
    			$("#search-box").css("background","#FFF");
    		}
		});
	});
});

function delSubscribedRequisition(OrgID, RequestID) {

	$.ajax({
		type: "POST",
		url: "delSubscribedRequisition.php",
		data:{"OrgID":OrgID, "RequestID":RequestID},
		dataType: "json",
		beforeSend: function() {
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data) {
			var val = document.getElementById('search-box').value; 
			getSubscriptionInfo(val);
		}
	});
	
}

function insSubscriptionInfo() {

	var search_box = document.getElementById('search-box').value;
	var org_parts  = search_box.split(" - ");
	var OrgID      = org_parts[0];

	document.getElementById('OrgID').value = OrgID;
    var subscriptions = document.getElementById('Subscriptions').value;

	if(OrgID != "") {
		$.ajax({
			type: "GET",
			url: "updZipRecruiterSubscriptionSettings.php?subscriptions="+subscriptions+'&OrgID='+OrgID,
			dataType: "json",
			beforeSend: function() {
				$("#info_message").html("<br>Please wait..<br>");
			},
			success: function(data) {
				if(data > 0) {
					$("#info_message").html("<br>Successfully Updated<br>");
				}
				else {
				    $("#info_message").html("<br>Sorry unable to update<br>");
				}
			}
		});
	} 
}

function getSubscriptionInfo(val) {

	var org_parts = val.split(" - ");
	var OrgID = org_parts[0];
	document.getElementById('OrgID').value = OrgID;

    $("#search-box").val(val);
    $("#suggesstion-box").hide();
	
	$.ajax({
		type: "POST",
		url: "getSubscriptionSettingsInfo.php",
		data:'keyword='+val,
		dataType: "json",
		beforeSend: function() {
			$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
		},
		success: function(data) {
		    $("#Subscriptions").val(data['subscription_info'].TotalSubscriptions);

		    var sub_cnt = data['subscribed_requisitions_cnt'];

		    var sub_req_list = '<br><table width="870">';

		    sub_req_list += '<tr>';
	    	sub_req_list += '<th align="left">OrgID</th>';
	    	sub_req_list += '<th align="left">Title</th>';
	    	sub_req_list += '<th align="left">Action</th>';
	    	sub_req_list += '</tr>';

		    if(sub_cnt > 0) {
			    for(var sc = 0; sc < sub_cnt; sc++) {

				    var OrgID = data['subscribed_requisitions'][sc].OrgID;
				    var Title = data['subscribed_requisitions'][sc].Title;
				    var RequestID = data['subscribed_requisitions'][sc].RequestID;
				    
			    	sub_req_list += '<tr>';
			    	sub_req_list += '<td>'+OrgID+'</td>';
			    	sub_req_list += '<td>'+Title+'</td>';
			    	sub_req_list += '<td><a href="javascript:void(0);" onclick=\'delSubscribedRequisition("'+OrgID+'", "'+RequestID+'")\'>Delete</a></td>';		    	
			    	sub_req_list += '</tr>';
				}
			}
		    else {
		    	sub_req_list += '<tr>';
		    	sub_req_list += '<td colspan="3">There are no subscriptions for this organization.</td>';		    	
		    	sub_req_list += '</tr>';
			}
			
		    sub_req_list += '</table>';
		    $("#subscribed_requisitions").html(sub_req_list);
		}
	});
}
</script>