<?php 
$highlight_requsitions          =   $HighlightRequisitionsObj->getHighlightRequisitionIcons();
$highlight_requsitions_results  =   $highlight_requsitions['results'];
$highlight_requsitions_count    =   $highlight_requsitions['count'];

//Icon 
if(isset($_REQUEST['IconID']) && $_REQUEST['IconID'] != "") {
    $icon_info = $HighlightRequisitionsObj->getHighlightRequisitionIconInfo($_REQUEST['IconID']);
}

//Insert New Theme
if(isset($_REQUEST['btnSubmit']) 
    && $_REQUEST['btnSubmit'] == "Submit" 
    && isset($_REQUEST['IconID']) 
    && $_REQUEST['IconID'] != "" 
    && $_REQUEST['icon_action'] == "edit"
    && $_FILES['fileJob']['tmp_name'] != "") {

    $job_file_name  =   '';
    $IconID         =   $_REQUEST['IconID'];
    
    if(!is_dir(IRECRUIT_DIR . 'vault/highlight')) {
        mkdir(IRECRUIT_DIR . 'vault/highlight', 0777);
        chmod(IRECRUIT_DIR . 'vault/highlight', 0777);
    }
    
    if($_FILES['fileJob']['tmp_name'] != "") {
        unlink(IRECRUIT_DIR . "/vault/highlight/".$icon_info['JobIcon']);
        $job_ext = end(explode(".", $_FILES['fileJob']['name']));
        
        if($_REQUEST['ddlJobType'] == 'New') {
            $job_file_name = "NewJob-$IconID.".$job_ext;
        }   
        else if($_REQUEST['ddlJobType'] == 'Hot') {
            $job_file_name = "HotJob-$IconID.".$job_ext;
        }
        
        if(move_uploaded_file($_FILES['fileJob']['tmp_name'], IRECRUIT_DIR . 'vault/highlight/'.$job_file_name)) {
            //Update Highlight Requisition Icons
            $HighlightRequisitionsObj->updHighlightRequisitionIcon($_REQUEST['IconID'], $job_file_name);
        }
    }
  
    echo "<script>location.href='index.php?pg=irecruit_req_highlight_icons&menu=8';</script>";    
}
else if(isset($_REQUEST['IconID'])
    && $_REQUEST['IconID'] != ""
    && $_REQUEST['icon_action'] == "delete") {

    $HighlightRequisitionsObj->delHighlightRequisitionIcon($_REQUEST['IconID']);

    echo "<script>location.href='index.php?pg=irecruit_req_highlight_icons&menu=8';</script>";
}
else if(isset($_REQUEST['btnSubmit']) 
    && $_REQUEST['btnSubmit'] == "Submit"
    && $_REQUEST['icon_action'] == "add"
    && $_FILES['fileJob']['tmp_name'] != "") {
    
    $job_file_name = '';
    
    if(!is_dir(IRECRUIT_DIR . 'vault/highlight')) {
        mkdir(IRECRUIT_DIR . 'vault/highlight', 0777);
        chmod(IRECRUIT_DIR . 'vault/highlight', 0777);
    }

    if($_FILES['fileJob']['tmp_name'] != "") {

        $IconID = $HighlightRequisitionsObj->insHighlightRequisitionIcons($_REQUEST['ddlJobType'], "");
        
        if($_REQUEST['ddlJobType'] == 'New') {
            $job_ext        = end(explode(".", $_FILES['fileJob']['name']));
            $job_file_name  = "NewJob-$IconID.".$job_ext;
            move_uploaded_file($_FILES['fileJob']['tmp_name'], IRECRUIT_DIR . 'vault/highlight/'.$job_file_name);
            
        } else if($_REQUEST['ddlJobType'] == 'Hot') {
            $job_ext        = end(explode(".", $_FILES['fileJob']['name']));
            $job_file_name  = "HotJob-$IconID.".$job_ext;
            move_uploaded_file($_FILES['fileJob']['tmp_name'], IRECRUIT_DIR . 'vault/highlight/'.$job_file_name);
        }
        
    }

    if($IconID > 0) {
        $HighlightRequisitionsObj->updHighlightRequisitionIcon($IconID, $job_file_name);
    }
    
    echo "<script>location.href='index.php?pg=irecruit_req_highlight_icons&menu=8';</script>";
}
?>
<form name="frmRequisitionHighlightIcons" id="frmRequisitionHighlightIcons" method="post" enctype="multipart/form-data">
    <h4>Create and Edit Icon</h4>
    
    <?php 
    if($_REQUEST['icon_action'] == "edit"
       && isset($_REQUEST['IconID'])
       && $_REQUEST['IconID'] != "") {
       ?>
       <input type="hidden" name="ddlJobType" id="ddlJobType" value="<?php echo $icon_info['JobType'];?>">
       <?php
    }
    else {
    	?>
    	Job Type: &nbsp;
	    <select name="ddlJobType" id="ddlJobType">
            <option value="New">New</option>
            <option value="Hot">Hot</option>
        </select>
    	<?php
    }
    ?>
    <br><br>
    Jobs Icon: <input type="file" name="fileJob" id="fileJob"><br>
    <?php
    if(isset($icon_info)) {
    	?><img alt="" src="<?php echo IRECRUIT_HOME . "/vault/highlight/".$icon_info['JobIcon']?>"><?php
    }
    ?>
    <br><br>
    <?php 
        if(!isset($_REQUEST['icon_action']) || $_REQUEST['icon_action'] == "") {
        	?><input type="hidden" name="icon_action" id="icon_action" value="add"><?php
        }
    ?>
    <input type="submit" class="btn btn-primary" name="btnSubmit" id="btnSubmit" value="Submit"> 
</form>

<table width="100%" border="0">
    <tr>
        <th align="left">Job Icon</th>
        <th align="left">Job Type</th>
        <th align="left">Action</th>
    </tr>
    <?php 
        if($highlight_requsitions_count > 0) {
            
        	for($j = 0; $j < $highlight_requsitions_count; $j++) {
                ?>
            	<tr>
            	   <td align="left"><img alt="" src="<?php echo IRECRUIT_HOME . "/vault/highlight/".$highlight_requsitions_results[$j]['JobIcon']?>"></td>
            	   <td align="left"><?php echo $highlight_requsitions_results[$j]['JobType'];?></td>
            	   
            	   <td align="left">
            	       <a href="index.php?pg=irecruit_req_highlight_icons&menu=8&IconID=<?php echo $highlight_requsitions_results[$j]['IconID'];?>&icon_action=edit">
            	           <img src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png" title="Edit" border="0">
            	       </a>
            	       &nbsp;
            	       <a href="index.php?pg=irecruit_req_highlight_icons&menu=8&IconID=<?php echo $highlight_requsitions_results[$j]['IconID'];?>&icon_action=delete">
            	           <img src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png" title="Delete" border="0">
            	       </a>
            	   </td>
            	</tr>
            	<?php
            }

        }
        else {
        	?>
        	<tr>
        	   <td colspan="3" align="center"><br><br>No icons found</td>
        	</tr>
        	<?php
        }
    ?>
</table>