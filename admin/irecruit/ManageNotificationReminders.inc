<script src="javascript/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="javascript/jquery-browser-migration.js" type="text/javascript"></script>
<script src="javascript/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">

<?php
$users_list_info    =   $IrecruitUsersObj->getUserInformation("UserID, OrgID, FirstName, LastName",array(),"LastName, FirstName");
$ul                 =   $users_list_info['results'];

if (isset($_REQUEST['UserKey']) && $_REQUEST['UserKey'] != "") {
    $UserID =   $_REQUEST['UserKey'];
}
else {
    $UserID =   "MASTER";
}

$user_reminders_info    =   G::Obj('Appointments')->getRemindersByUserID($UserID);
$user_reminders         =   $user_reminders_info['results'];

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete' && isset($_REQUEST['reminder_id']) && $_REQUEST['reminder_id'] != '') {
    // Delete Reminder By AppointmentReminderID
    G::Obj('Reminders')->delAppointmentReminderInfo($_REQUEST['reminder_id']);
    echo '<script>location.href="index.php?pg=manage_notification_reminders&UserKey=' . htmlspecialchars($UserID) . '";</script>';
    exit();
}

// Edit Calendar Reminder
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['reminder_id']) && $_REQUEST['reminder_id'] != '' && isset($_REQUEST['btnAddReminderNotification']) && $_REQUEST['btnAddReminderNotification'] == "Submit") {
    
    $sidatetime = $DateHelperObj->getYmdFromMdy($_REQUEST['txtDateTime']);
    
    // Calculate PostDate Hours
    $hrs = $_REQUEST['sihrs'];
    if ($_REQUEST['siampm'] == "AM") {
        if ($hrs == 12)
            $hrs = "00";
    } else 
        if ($_REQUEST['siampm'] == "PM") {
            $hrs += 12;
            if ($hrs == 24)
                $hrs -= 12;
        }
    
    // Calculate PostDate Minutes
    $mins = $_REQUEST['simins'];
    if ($_REQUEST['simins'] == 0) {
        $mins = "00";
    }
    $sidatetime .= " " . $hrs . ":" . $mins . ":00";
    
    // Update Reminder Information
    $RemindersObj->updAppointmentReminderInfo($sidatetime, $_REQUEST['taReminderMessage'], $_REQUEST['reminder_id']);
    
    echo '<script>location.href="index.php?pg=manage_notification_reminders&UserKey=' . htmlspecialchars($UserID) . '";</script>';
    exit();
}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'edit' && isset($_REQUEST['reminder_id']) && $_REQUEST['reminder_id'] != '') {
    
    $columns = "Subject, DATE(DateTime) AS DateTime, DATE_FORMAT(DateTime, '%h') AS Hours, DATE_FORMAT(DateTime, '%i') AS Mins, DATE_FORMAT(DateTime, '%p') AS AmPm";
    $reminder_info = $RemindersObj->getAppointmentReminderDetailInfo($columns, $_REQUEST['reminder_id']);
    
    $reminder_date = $DateHelperObj->getMdyFromYmd($reminder_info['DateTime']);
    ?>
    <div style="font-size: 12pt; font-weight: bold; margin-bottom: 20px;">Edit Calendar Reminder Event</div>
    <form name="frmEditReminder" id="frmEditReminder" method="post">
    	<table width="100%">
    		<tr>
    			<td>Reminder Date Time: <span style="color: red">*</span>
    			</td>
    			<td><input type="text" name="txtDateTime" id="txtDateTime"
    				value="<?php echo $reminder_date;?>"> <select name="sihrs"
    				id="sihrs">
    					<?php
        for ($h = 1; $h <= 12; $h ++) {
            $h = ($h < 10) ? '0' . $h : $h;
            ?><option value="<?php echo $h;?>"
    						<?php if($reminder_info['Hours'] == $h) echo 'selected="selected"';?>><?php echo $h;?></option><?php
        }
        ?>
    				</select> <select name="simins" id="simins">
    				<?php
        for ($m = 0; $m <= 45; $m += 15) {
            $m = ($m == 0) ? '00' : $m;
            ?><option value="<?php echo $m;?>"
    						<?php if($reminder_info['Mins'] == $m) echo 'selected="selected"';?>><?php echo $m;?></option><?php
        }
        ?>
    				</select> <select name="siampm" id="siampm">
    					<option value="AM"
    						<?php if($reminder_info['AmPm'] == "AM") echo 'selected="selected"';?>>AM</option>
    					<option value="PM"
    						<?php if($reminder_info['AmPm'] == "PM") echo 'selected="selected"';?>>PM</option>
    			</select></td>
    			</td>
    		</tr>
    		<tr>
    			<td>Reminder Notification Message: <span style="color: red">*</span>
    			</td>
    			<td><textarea name="taReminderMessage" id="taReminderMessage"
    					rows="4" cols="55"><?php echo $reminder_info['Subject'];?></textarea>
    			</td>
    		</tr>
    		<tr>
    			<td colspan="2"><input type="submit"
    				name="btnAddReminderNotification" id="btnAddReminderNotification"
    				value="Submit"></td>
    		</tr>
    	</table>
    </form>
    <?php
}
?>
<br>
<br>
<div style="font-size: 12pt; font-weight: bold; margin-bottom: 20px;">Manage Calendar</div>
<form method="post" name="frmUserID" id="frmUserID">
	<table width="100%">
		<tr>
			<td>Select User</td>
			<td>
			<select name="UserKey" id="UserKey" onchange="this.form.submit();">
				<option value="MASTER" <?php if($UserID == "MASTER" || $UserID == "") echo 'selected="selected"';?>>Master</option>
                <?php
                for ($ui = 0; $ui < count($ul); $ui ++) {
                    ?><option value="<?php echo $ul[$ui]['UserID'];?>"
                			<?php if($UserID == $ul[$ui]['UserID']) echo 'selected="selected"';?>>
                		<?php echo $ul[$ui]['LastName'].", ".$ul[$ui]['FirstName'];?>
                	  </option>
                	<?php
                }
                ?>
            </select>
            </td>
		</tr>
	</table>
</form>

<form method="post" name="frmManageReminders" id="frmManageReminders">
	<table width="100%" cellpadding="3" cellspacing="3">
		<tr>
			<th align="left">Message</th>
			<th align="left">Date</th>
			<th align="left">Action</th>
		</tr>
        <?php
        if (count($user_reminders) > 0) {
            for ($r = 0; $r < count($user_reminders); $r ++) {
                ?>
        		<tr>
        			<td><?php echo $user_reminders[$r]['Subject']?></td>
        			<td><?php echo $user_reminders[$r]['DateTime']?></td>
        			<td><a href="index.php?pg=manage_notification_reminders&action=edit&UserKey=<?php echo htmlspecialchars($UserID);?>&reminder_id=<?php echo htmlspecialchars($user_reminders[$r]['AppointmentReminderID']);?>"><img
        					alt="Edit" src="<?php echo IRECRUIT_HOME?>images/icons/pencil.png"></a>&nbsp;
        				<a href="index.php?pg=manage_notification_reminders&action=delete&UserKey=<?php echo htmlspecialchars($UserID);?>&reminder_id=<?php echo htmlspecialchars($user_reminders[$r]['AppointmentReminderID']);?>"><img
        					alt="Delete" src="<?php echo IRECRUIT_HOME?>images/icons/cross.png"></a>
        			</td>
        		</tr>
        		<?php
            }
        } else {
            ?>
        	<tr>
    			<td colspan="3" align="center"><strong>This user doesn't have any calendar reminder events.</strong></td>
    		</tr>
        	<?php
        }
        ?>
</table>
</form>

<script>
$(document).ready(function() {
	$('#txtDateTime').datepicker();
});
</script>
