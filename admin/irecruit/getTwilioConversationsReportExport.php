<?php
require_once '../Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$OrgID	= $_REQUEST['TwilioOrgsList'];

//Config Columns List
$config_columns_list = array(
    "UserID"			=> "User ID",
    "ResourceID"		=> "Conversation ID",
    "CreatedDateTime"	=> "Conversation Date Time",
    "MessagesCount"		=> "Messages Count",
);

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Text Report")
->setSubject("Excel")
->setDescription("To get the conversations list based on selected filters")
->setKeywords("phpExcel")
->setCategory("Conversations List");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = G::Obj('GenericLibrary')->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('User ID');

$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Conversation ID');

$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Messages Count');

###  Append Conversations Data To Spread Sheet ###
$where_info		=	array();
$params_info	=	array();

if($_REQUEST['TwilioOrgsList'] != "") {
	$where_info[]			=	"OrgID = :OrgID";
	$params_info[":OrgID"]	=	$_REQUEST['TwilioOrgsList'];
}

if($_REQUEST['TwilioUsersList'] != "") {
	$where_info[]			=	"UserID = :UserID";
	$params_info[":UserID"]	=	$_REQUEST['TwilioUsersList'];
}

if($_REQUEST['FromDate'] != "" && $_REQUEST['ToDate'] != "") {
	$where_info[]				=	"DATE(CreatedDateTime) >= :FromDate";
	$where_info[]				=	"DATE(CreatedDateTime) <= :ToDate";

	$params_info[":FromDate"]	=	G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['FromDate']);
	$params_info[":ToDate"]		=	G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['ToDate']);
}

//Conversations List
$conversations_res	=	G::Obj('TwilioConversationInfo')->getAllConversations("*", $where_info, "", "", array($params_info));
$conversations_info	=	$conversations_res['results'];

for($c = 0; $c < count($conversations_info); $c++) {

	$resource_id		=	$conversations_info[$c]['ResourceID'];
	$conversations		=	G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $resource_id);
	$conversation_res	=	$conversations['Response'];

	$conversations_info[$c]['MessagesCount']	=	count($conversation_res);
}

for($ci = 0, $ci_data = 2; $ci < count($conversations_info); $ci++, $ci_data++) {
	$al = 0;
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al].$ci_data)->setValue($conversations_info[$ci]['UserID']);
	$al++;
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al].$ci_data)->setValue($conversations_info[$ci]['ResourceID']);
	$al++;
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al].$ci_data)->setValue($conversations_info[$ci]['MessagesCount']);
	
} 

// Redirect output to a client�s web browser (Excel5)

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="conversations-report.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
