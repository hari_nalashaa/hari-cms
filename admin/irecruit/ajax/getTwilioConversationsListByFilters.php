<?php
require_once '../../Configuration.inc';

$OrgID	= $_REQUEST['TwilioConversationOrgID'];


$conversations_limit = 100;

$index_start = 0;
if(isset($_REQUEST['IndexStart'])) $index_start = $_REQUEST['IndexStart'];
$order = " CreatedDateTime DESC LIMIT $index_start, $conversations_limit";

$where_info		=	array();
$params_info	=	array();

if($_REQUEST['TwilioConversationOrgID'] != "") {
	$where_info[]			=	"OrgID = :OrgID";
	$params_info[":OrgID"]	=	$_REQUEST['TwilioConversationOrgID'];
}

if($_REQUEST['TwilioConversationUserID'] != "") {
	$where_info[]			=	"UserID = :UserID";
	$params_info[":UserID"]	=	$_REQUEST['TwilioConversationUserID'];
}

if($_REQUEST['FromDate'] != "" && $_REQUEST['ToDate'] != "") {
	$where_info[]			=	"DATE(CreatedDateTime) >= :FromDate";
	$where_info[]			=	"DATE(CreatedDateTime) <= :ToDate";
	
	$params_info[":FromDate"]	=	G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['FromDate']);
	$params_info[":ToDate"]		=	G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['ToDate']);
}

//Conversations List
$conversations_res	=	G::Obj('TwilioConversationInfo')->getAllConversations("*", $where_info, "", $order, array($params_info));
$conversations_info	=	$conversations_res['results'];

//Conversations Count
$conv_cnt_result	=	G::Obj('TwilioConversationInfo')->getAllConversations("COUNT(*) AS ConvCount", $where_info, "", "", array($params_info));
$conversations_cnt	=	$conv_cnt_result['results'][0]['ConvCount'];

for($c = 0; $c < count($conversations_info); $c++) {
	
	$resource_id		=	$conversations_info[$c]['ResourceID'];
	$conversations		=	G::Obj('TwilioConversationApi')->listAllConversationMessages($OrgID, $resource_id);
	$conversation_res	=	$conversations['Response'];
	
	
	$conversations_info[$c]['MessagesCount']	=	count($conversation_res);
}

$nav_info = G::Obj('Pagination')->getPageNavigationInfo($index_start, $conversations_limit, $conversations_cnt, '', '');

//Print Conversation Info
echo json_encode(array("conversations_info"=>$conversations_info, "previous"=>$nav_info['previous'], "next"=>$nav_info['next'], "total_pages"=>$nav_info['total_pages'], "current_page"=>$nav_info['current_page'], "conversations_count"=>$conversations_cnt));
?>