<?php
require_once '../../Configuration.inc';

$OrgID				=	$_REQUEST['OrgID'];
$info				=	array("areaCode" => $_REQUEST['AreaCode']);
$phone_numbers_res	=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID, $info);
$phone_numbers		=	$phone_numbers_res['Response'];

if(count($phone_numbers) > 0) {
	echo "Available";
}
else {
	echo "NotAvailable";
}
?>