<?php
//Get Twilio Accounts List
$twilio_results		=	G::Obj('TwilioAccounts')->getTwilioAccountsList();
$twilio_accounts	=	$twilio_results['results'];

?>
Twilio Organizations List:
<select name="ddlTwilioOrgList" id="ddlTwilioOrgList" onchange="location.href='index.php?pg=irecruit_twilio_usage&OrgID='+this.value">
	<option value="">Select</option>
	<?php
	for($ta = 0; $ta < count($twilio_accounts); $ta++) {
		$TwilioAccountOrgID	=	$twilio_accounts[$ta]['OrgID'];
		
		$org_details = G::Obj('OrganizationDetails')->getOrganizationInformation($TwilioAccountOrgID, "", "OrganizationName");
		?><option value="<?php echo $TwilioAccountOrgID;?>" <?php if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] == $TwilioAccountOrgID) echo ' selected="selected"'; ?>><?php echo $org_details['OrganizationName'];?></option><?php
	}
	?>
</select>

<?php
if ($_REQUEST['OrgID'] != "") {

   echo "<div style=\"margin-top:20px;\">";
   echo G::Obj('TwilioAccounts')->getTwilioAccountUseageReport($_REQUEST['OrgID']);

   echo G::Obj('TwilioAccounts')->getTwilioAccountCostInformation($_REQUEST['OrgID']);
   echo "<div>";


} // end OrgID
?>
