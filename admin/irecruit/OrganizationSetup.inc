<?php
if (isset($_POST['submit'])) {

$ERROR  = "";

// OrgID contains only letters and numbers
$_POST['OrgID']     =   preg_replace("[^A-Za-z0-9]", "", $_POST['OrgID']);
// UserID contains no spaces
$_POST['UserID']    =   preg_replace('/\s+/', '', $_POST['UserID']);

if (!$_POST['OrgID']) {
    $ERROR .=   "OrgID is missing.<br>\n";
}

// check for duplicate OrgID 
$params     =   array(':OrgID' => $_POST['OrgID']);
$query      =   "SELECT OrgID FROM OrgData WHERE OrgID = :OrgID";
$orgck_info =   G::Obj('GenericQueries')->getRowInfoByQuery($query, array($params));
$orgck      =   $orgck_info['OrgID']; 

if ($orgck != "") {
  $ERROR .= "Organization ID exists. Please choose a different number.<br>\n"; 
}

if (!$_POST['OrganizationName']) {
  $ERROR .= "OrganizationName is missing.<br>\n";
}

if (strlen($_POST['UserID']) < 8) {
  $ERROR .= "User Name needs to be at least 8 characters and no spaces.<br>\n";
}

// check for duplicate UserID
$query          =   "SELECT UserID FROM Users WHERE UserID = :UserID";
$params         =   array(':UserID' => $_POST['UserID']);
$userck_info    =   G::Obj('GenericQueries')->getRowInfoByQuery($query, array($params));
$userck         =   $userck_info['OrgID']; 

if ($userck != "") {
  $ERROR .= "User Name exists. Please choose a different name.<br>\n"; 
}

$ERR .= $ValidateObj->validate_verification($_POST['Verification'],"","N");
$ERROR .= stripslashes(str_replace("\\n", '<br>', $ERR));


$ERR = $ValidateObj->validate_email_address($_POST['UserEmailAddress']);
$ERROR .= stripslashes(str_replace("\\n", '<br>', $ERR));

if (!$ERROR) {

$BrandID = (isset($_POST['ddlBrandID']) && $_POST['ddlBrandID'] != "") ? $_POST['ddlBrandID'] : "0";

$query  =  "INSERT INTO OrgData";
$query .= " (OrgID, OrganizationName, ContactEmail, ClientSince, BrandID)";
$query .= " VALUES (:orgid, :orgname, :contactemail, NOW(), :brandid)";
$params = array(':orgid'=>$_POST['OrgID'], ':orgname'=>$_POST['OrganizationName'], ':contactemail'=>$_POST['UserEmailAddress'], ':brandid'=>$BrandID);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query  =   "INSERT INTO ApplicationNumbers (OrgID, ApplicationID) VALUES (:OrgID, 101)";
$params =   array(':OrgID'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query  =   "INSERT INTO ApplicationFeatures(OrgID) VALUES(:OrgID)";
$params =   array(':OrgID'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query  =   "INSERT INTO Users";
$query .=   "(UserID, Verification, OrgID, EmailAddress, ActivationDate, LastAccess)";
$query .=   " VALUES (:userid, :verification, :orgid, :emailaddress, NOW(), NOW())";
$params =   array(':orgid'=>$_POST['OrgID'], ':userid'=>$_POST['UserID'], ':verification'=>password_hash($_POST['Verification'], PASSWORD_DEFAULT), ':emailaddress'=>$_POST['UserEmailAddress']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

//To Do in Password update
//password_hash( $_POST['password'] , PASSWORD_DEFAULT )
$query  =   "INSERT INTO ApplicationPermissions(UserID, Active, Admin, Requisitions, Correspondence, Forms, Reports, Applicants)";
$query .=   " VALUES(:userid, 1, 1, 1, 1, 1, 1, 1)";
$params =   array(':userid'=>$_POST['UserID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

G::Obj('ApplicationFormSections')->copyApplicationFormMasterSections($_POST['OrgID'], 'MASTER', "STANDARD", "STANDARD");

$query = "INSERT INTO FormQuestions";
$query .= " (OrgID, FormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value,";
$query .= " defaultValue, Active, SectionID, QuestionOrder, Required, Validate)";
$query .= " SELECT :orgid, FormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap,";
$query .= " value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate";
$query .= " FROM FormQuestions WHERE OrgID = 'MASTER'";
$params = array(':orgid'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query = "INSERT INTO TextBlocks";
$query .= " (OrgID, FormID, TextBlockID, Text)";
$query .= " SELECT :orgid, FormID, TextBlockID, Text";
$query .= " FROM TextBlocks WHERE OrgID = 'MASTER'";
$params = array(':orgid'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query = "INSERT INTO OrganizationLevels";
$query .= " (OrgID, OrgLevelID, OrganizationLevel)";
$query .= " VALUES (:orgid,1,'Location')";
$params = array(':orgid'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query = "INSERT INTO OrganizationLevelData";
$query .= " (OrgID, OrgLevelID, SelectionOrder, CategorySelection)";
$query .= " VALUES (:orgid,1,1,'Connecticut')";
$params = array(':orgid'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query = "INSERT INTO ApplicantProcessFlow";
$query .= " (OrgID, ProcessOrder, Description, Type)";
$query .= " VALUES (:orgid,1,'New Applicant','S')";
$params = array(':orgid'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query = "INSERT INTO ActiveMinutes(OrgID, Minutes) VALUES (:OrgID,'10')";
$params = array(':OrgID'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

$query      =   "SELECT TextBlockID, Text FROM TextBlocks WHERE OrgID = :orgid";
$params     =   array(':orgid'=>$_POST['OrgID']);
$TEXTBLOCKS =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));

foreach ($TEXTBLOCKS as $BLOCK) {

  $newText = preg_replace("/\{COMPANY\-NAME\}/i",$_POST['OrganizationName'],$BLOCK['Text']);
  $newText = preg_replace("/\{HR\-ADMIN\}/i","our Human Resources department",$newText);
  $newText = preg_replace("/\{COMPANY\-PHONE\}/i","",$newText);

  $query = "UPDATE TextBlocks SET Text = :Text WHERE OrgID = :OrgID AND TextBlockID = :TextBlockID";
  $params = array(':Text'=>$newText, ':OrgID'=>$_POST['OrgID'], ':TextBlockID'=>$BLOCK['TextBlockID']);
  G::Obj('GenericQueries')->updInfoByQuery($query, array($params));
} // end foreach


$query  =   "INSERT INTO FilePurpose(OrgID, FilePurposeID, PurposeTitle) VALUES (:OrgID, :FilePurposeID, :PurposeTitle)";
$info   =   array();
$info[] =   array(':OrgID'=>$_POST['OrgID'], ':FilePurposeID'=>"99", ':PurposeTitle'=>"Purpose");
$info[] =   array(':OrgID'=>$_POST['OrgID'], ':FilePurposeID'=>"1", ':PurposeTitle'=>"Interview Questions");
$info[] =   array(':OrgID'=>$_POST['OrgID'], ':FilePurposeID'=>"2", ':PurposeTitle'=>"Job Description");
$info[] =   array(':OrgID'=>$_POST['OrgID'], ':FilePurposeID'=>"3", ':PurposeTitle'=>"Salary Requirements");
G::Obj('GenericQueries')->insInfoByQuery($query, $info);

$query  =   "INSERT INTO NavigationDisplay(OrgID, Type) VALUES (:OrgID,'Responsive')";
$params =   array(':OrgID'=>$_POST['OrgID']);
G::Obj('GenericQueries')->insInfoByQuery($query, array($params));

//Insert Default RequisitionQuestions
$RequisitionQuestionsObj->insDefaultRequisitionQuestions($_POST['OrgID']);
//Insert default RequisitionForm
$form_info  =   array(
                    "OrgID"                 => $_POST['OrgID'],
                    "RequisitionFormID"     => "REQUISITION",
                    "RequisitionFormName"   => "Version 1",
                    "FormDefault"           => "Y",
                    "CreatedDateTime"       => "NOW()",
                );
$RequisitionFormsObj->insRequisitionForms($form_info);

echo "<div style=\"margin:20px 0 0 40px;\">\n";
echo "<span style=\"font-size:12pt;\">Success.</span><br>You will be redirected to set up Organization Features";
echo "</div>\n";

echo "<script>\n";
echo "setTimeout(function () {\n";
echo " location.replace(\"" . ADMIN_HOME . "index.php?pg=irecruit_features&OrgID=" . htmlspecialchars($_POST['OrgID']) . "\");\n";
echo "  }, 3500);\n";
echo "</script>\n";
exit();

} // end !ERROR 

} // end PROCESS

$date_info  =   G::Obj('MysqlHelper')->getDatesList("DATE_FORMAT(NOW(),'%Y%m') AS FormattedDate");
$date       =   $date_info['FormattedDate'];

if (!$_POST['OrgID']) {
    $_POST['OrgID'] = "I" . $date . "01";
}

// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">iRecruit New Organization Setup</div>\n";
?>
<form method="POST" action="index.php">
<table border="0" cellspacing="0" cellpadding="3">
<tr><td>
OrgID
</td><td>
<input type="text" name="OrgID" size="15" value="<?php echo htmlspecialchars($_POST['OrgID']); ?>" maxlength="30">
</td></tr>

<tr><td>
Organization Name
</td><td>
<input type="text" size="40" maxlength="60" name="OrganizationName" value="<?php echo htmlspecialchars($_POST['OrganizationName']); ?>">
</td></tr>

<tr><td>
UserID
</td><td>
<input type="text" name="UserID" size="20" maxlength="45" value="<?php echo htmlspecialchars($_POST['UserID']); ?>"> 
</td></tr>

<tr><td>
Verification
</td><td>
<input type="password" size="20" maxlength="45" name="Verification" value="<?php echo htmlspecialchars($_POST['Verification']); ?>">
</td></tr>

<tr><td>
User Email Address
</td><td>
<input type="text" size="40" maxlength="60" name="UserEmailAddress" value="<?php echo htmlspecialchars($_POST['UserEmailAddress']); ?>">
</td></tr>

<tr><td>
Brand
</td><td>
<?php 
$brands_info_list   = $BrandsObj->getBrands();
$brands_list        = $brands_info_list['results'];
?>
<select name="ddlBrandID" id="ddlBrandID">
    <option value="">Select</option>
    <?php 
    for($bi = 0; $bi < count($brands_list); $bi++) {
    	?><option value="<?php echo $brands_list[$bi]['BrandID'];?>"><?php echo $brands_list[$bi]['BrandName'];?></option><?php
    }
    ?>
</select>
</td></tr>

</table>

<div style="margin:10px 20px 0 20px;float:left;">
<input type="hidden" name=pg value="irecruit_orgsetup">
<input type="submit" name="submit" value="Create New Account">
</div>
<?php
if ($ERROR) {
echo "<div style=\"margin:10px 0 0 160px;\">\n";
echo "Please correct the following errors:<br>\n"; 
echo "<div style=\"margin:0 0 0 5px;color:red;\">\n";
echo $ERROR;
echo "</div>\n";
echo "</div>\n";
}
?>
</form>
