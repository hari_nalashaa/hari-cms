<?php
if (($_POST['PurchaseDate_From'] == "") || ($_POST['PurchaseDate_To'] == "")) {
    $columns    =   "DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL 30 DAY),'%m/%d/%Y') AS FromDate, DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL 1 DAY),'%m/%d/%Y') AS ToDate";
    $dates_info =   G::Obj('MysqlHelper')->getDatesList($columns);
    $_POST['PurchaseDate_From'] =   $dates_info['FromDate'];
    $_POST['PurchaseDate_To']   =   $dates_info['ToDate'];
}

// create array for Org Names
$where          =   array("MultiOrgID = ''","OrgID IN (SELECT DISTINCT(OrgID) FROM Purchases)");
$org_list       =   G::Obj('Organizations')->getOrgDataInfo("OrgID, OrganizationName", $where, 'OrganizationName');
$ORGS           =   $org_list['results'];

$ORGNAME        =   array();
foreach ($ORGS as $ORG) {
    $ORGNAME[$ORG['OrgID']] = $ORG['OrganizationName'];
} // end foreach
  
// heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">iRecruit Purchases</div>\n";

echo "<form method=\"POST\" action=\"index.php\">\n";

$LST    =   array("PurchaseDate_From", "PurchaseDate_To");
echo $DateHelperObj->dynamicCalendar($LST, '');

echo "<div style=\"margin-left:20px;margin-bottom:10px;\">\n";
echo "Company: ";
echo "<select name=\"OrgID\">\n";
echo '<option value="">ALL</option>' . "\n";

foreach ($ORGS as $OR) {
    echo "<option value=\"" . $OR['OrgID'] . "\"";
    if ($OR['OrgID'] == $_POST['OrgID']) {
        echo " selected";
    }
    echo ">" . $OR['OrganizationName'] . "</option>\n";
} // end foreach

echo "</select>\n";
echo "</div>\n";

echo "<div style=\"margin-left:20px;margin-bottom:20px;\">\n";
echo "Purchase Date From: ";
echo "<input type=\"text\" id=\"PurchaseDate_From\" name=\"PurchaseDate_From\" value=\"" . $_POST['PurchaseDate_From'] . "\" size=\"10\" onBlur=\"validate_date(this.value,'From Date');\" />";
echo "&nbsp;&nbsp;";
echo "To: ";
echo "<input type=\"text\" id=\"PurchaseDate_To\" name=\"PurchaseDate_To\" value=\"" . $_POST['PurchaseDate_To'] . "\" size=\"10\" onBlur=\"validate_date(this.value,'To Date');\" />\n";
echo "&nbsp;&nbsp;";
echo "&nbsp;&nbsp;";
echo "<input type=\"hidden\" name=\"pg\" value=\"irecruit_purchases\">\n";
echo "<input type=\"submit\" name=\"submit\" value=\"Update List\">\n";
echo "</div>\n";

echo "</form>\n";

if (isset($_POST['submit'])) {
    echo G::Obj('Purchases')->displayPurchases();
} // end submit
?>
