<?php // heading
echo "<div style=\"font-size:12pt;font-weight:bold;margin-bottom:20px;\">PDF PreFill Forms</div>\n";
if ($_GET['OrgID'])
{
    $_POST['OrgID'] = $_GET['OrgID'];
}
?> 
<style>
#tableID tr:nth-child(odd) {
    background-color: #f9f9f9;
}
</style>   
<?php $_SESSION['status']=0;
// initial select form
echo '<form method="post" name="frmOrgList" action="index.php" style="background-color: #ddd; width: 87%; text-align: end; padding: 1.5%; font-size: 13px; font-weight: 700;">';
echo '<input type="hidden" name="pg" value="custom_webforms">';
echo '<input type="hidden" name="OrgID" value="">';

$org_query = "SELECT OrgID, OrganizationName FROM OrgData";
$org_query .= " WHERE OrgID in (";
$org_query .= "SELECT distinct(WebForms.OrgID) FROM WebForms";
$org_query .= " WHERE WebForms.FormStatus = 'Active' AND WebForms.OrgID !='MASTER')";
$org_query .= " AND MultiOrgID = ''";
$org_query .= " ORDER BY OrganizationName";
$org_dropdown = G::Obj('GenericQueries')->getInfoByQuery($org_query, array());

//echo "<pre>" . print_r($org_dropdown,true) . "</pre>";

echo '<label >Select Organization:</label>
<select name="org" id="org"  onchange="getdataByorgid(this.value)" style="height: 25px;">';

if(isset($_REQUEST['search']) && $_REQUEST['search'] == "all"){
    echo '<option value=' . $slt['OrgID'] . ' name="org"   selected=' .$_REQUEST['search'] . ' >Select</option>';
    }else{
    echo '<option value="select" name="org" >Select</option>';
     }

foreach ($org_dropdown as $slt)
{ 

    if(isset($_REQUEST['search']) && $slt['OrgID'] == $_REQUEST['search']){
    echo '<option value=' . $slt['OrgID'] . ' name="org"   selected=' . $slt['OrgID'] . ' >' . $slt['OrgID'] . ' - ' . $slt['OrganizationName'] . ' </option>';
    }else{
    echo '<option value=' . $slt['OrgID'] . ' name="org" >' . $slt['OrgID'] . ' - ' . $slt['OrganizationName'] . ' </option>';
     }

}
echo '</select>';
echo '</form><br>';
$hit = "";
foreach ($ORGDATA as $OD)
{

    if ($OD['OrgID'] == $_POST['OrgID'])
    {
        $hit = "yes";
    }
} // end while
$search = ''; 
if (isset($_REQUEST['search']))
{
    $search = $_REQUEST['search'];
}

echo '<form method="post" action="index.php">';
echo '<input type="hidden" name="pg" value="custom_webforms">';
echo '<input type="hidden" name="OrgID" value="' . $_POST['OrgID'] . '">';

if (!empty($search) && $search != 'search')
{
	$query = "SELECT distinct(WebFormID), OrgID, FormName";
	$query .= " FROM WebForms WHERE FormStatus = 'Active'";
	$query .= " AND OrgID = :OrgID order by FormName";
	$params  =   array(':OrgID'    =>  $search);
	$FEATURES = G::Obj('GenericQueries')->getInfoByQuery($query, array($params)); 
}
$rowcolor = "#eeeeee";
echo ' <table style="width:90%;border: 1px solid #ddd;"  cellspacing="3" cellpadding="5" border: 2px solid black id="tableID" class="display">
     <thead>
      <tr bgcolor="' . $rowcolor . '" style="width:100%;text-align:center;background-color: rgb(238, 238, 238);height: 35px;">
     <th>Form Name</th>
    <th>Upload PDF</th>
    <th>Edit Fill Pairs</th>
    <th>Delete</th>
    </tr>
</thead><tbody>';
if(!empty($FEATURES)){
foreach ($FEATURES as $AF)
{
    echo '<tr bgcolor="' . $rowcolor . '">
    <td>' . $AF['FormName'] . '</td>';
   echo '<td style="text-align:center"><a href="index.php?pg=custom_webforms_create&WebFormID=' . $AF['WebFormID'] . '&OrgID=' . $AF['OrgID'] . '"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Upload PDF"></a></a></td>';

    $webform = IRECRUIT_DIR . 'formsInternal/PDFForms/Custom/' . $AF['OrgID'] . '-' . $AF['WebFormID'] . '.pdf';
    if (file_exists($webform))
    {
        echo '<td style="text-align:center"> <a href="index.php?pg=custom_webforms_edit&WebFormID=' . $AF['WebFormID'] . '&OrgID=' . $AF['OrgID'] . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a></td>';
        echo '<td style="text-align:center"> <a href="index.php?pg=custom_webforms_delete&WebFormID=' . $AF['WebFormID'] . '&OrgID=' . $AF['OrgID'] . '"  onclick="return confirm(\'Are you sure you want to delete the PDF fill data for the following form?\n\n' . $AF['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a></td>';

    }
    else
    {
        echo '<td style="text-align:center"></td>';
        echo '<td style="text-align:center"> </td>';
    }

    echo '</tr>';
    $a++;
}
}else{
         echo '<td  colspan="5" style="text-align:center">No data found</td>';
}
echo '</tbody></table>';
// end while

?>
</form>
<script>

function getdataByorgid(url) {
	location.href = 'index.php?pg=custom_webforms&search='+url;
}


$(document).ready(function() {

	$("#search-box").bind("keyup click", function() {
		$.ajax({
    		type: "POST",
    		url: "getOrganizations.php",
    		data:'keyword='+$(this).val(),
    		beforeSend: function() {
    			$("#search-box").css("background-color","#F5F5F5");
    		},
    		success: function(data){
    			$("#suggesstion-box").show();
    			$("#suggesstion-box").html(data);
    			$("#search-box").css("background","#FFF");
    		}
		});
	});



});


function getSubscriptionInfo(obj) {
    var val =  obj.textContent;

	var org_parts = val.split(" - ");
	var OrgID = org_parts[0];
	
	document.forms['frmOrgList'].OrgID.value = OrgID;

	document.forms['frmOrgList'].submit();
}	
</script>
