<?php
include 'Configuration.inc';

$keyword_parts = explode(" - ", $_POST['keyword']);
$OrgID = $keyword_parts[0];

$zip_recruiter_sub_info = $ZipRecruiterObj->getZipRecruiterSubscriptionSettingsInfo($keyword_parts[0]);

$columns = "OrgID, RequestID, (SELECT Title FROM Requisitions WHERE OrgID = ZipRecruiterSubscriptions.OrgID AND RequestID = ZipRecruiterSubscriptions.RequestID) as Title";
$subscribed_requisitions = $ZipRecruiterObj->getZipRecruiterSubscriptionsInfo($columns, $OrgID);

$info = array(
    "subscription_info" => $zip_recruiter_sub_info,
    "subscribed_requisitions" => $subscribed_requisitions['results'],
    "subscribed_requisitions_cnt" => $subscribed_requisitions['count']
);
echo json_encode($info);