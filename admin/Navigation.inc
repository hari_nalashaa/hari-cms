<!-- Navigation -->
<?php include 'Configuration.inc';
$AUTH = $ADMINObj->authenticate($_COOKIE['AID']);

	if ($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com") {
            if (strpos($_SERVER["SCRIPT_NAME"], 'david') !== false) {
                $rtname .= "David ";
            }
            if (strpos($_SERVER["SCRIPT_NAME"], 'hari') !== false) {
                $rtname .= "Hari";
            }
            $rtname .= "Development Administration</b>\n";
        }

        if ($_SERVER['SERVER_NAME'] == "quality.irecruit-us.com") {
            $rtname = "Quality Administration";
        }


 	if ($_SERVER['SERVER_NAME'] == "irecruit.pairsite.com") {
            $rtname = "Production Administration";
        }

 ?>
<nav class="navbar navbar-default navbar-static-top" role="navigation"
	style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?php echo  ADMIN_HOME . "index.php"  ?>">
            <?php echo  $rtname ; ?></a>
	</div>

	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle fa-user-icon">
				<?php 
				//Get User Details
				$USER   =   G::Obj('WOTCUsers')->getUserInfoByUserID("*", $AUTH['UserID']);

				echo $AUTH['UserID'];
				?>&nbsp;<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
                <li>           

                	<a href="<?php echo ADMIN_HOME?>index.php?pg=access">
                    	<i class="fa fa-user" style="float:left;margin-top:5px;margin-right:2px"></i>
                    	<div style="width: 240px;float:left"> Admin Users</div>
                    	<i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>
                	</a>
                </li>
                <li>
                    <a href="<?php echo ADMIN_HOME?>logout.php">
                        <i class="fa fa-sign-out" style="float:left;margin-top:5px;margin-right:2px"></i>
                        <div style="width: 240px;float:left"> Logout</div>
                        <i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>
                    </a>
                </li>
			</ul> <!-- /.dropdown-user --></li>
	</ul>

	<!-- /.navbar-header -->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse" >
			<ul class="nav" id="side-menu">
				<?php
				global $AUTH;
				
				$navsection =   array(
                				    "irecruit"          =>  "iRecruit",
                				    "wotcprocessing"    =>  "WOTC Processing",
                				    "wotccrm"           =>  "WOTC CRM",
                				    "wotcreporting"     =>  "WOTC Reporting",
                				    "callcenter"        =>  "Call Center",
                                                    "cms"    		=>  "Website Links"
                				);



	$navisubmenus =   array("irecruit"=>array(
 		"master_access" =>  "Master Access", 
 		"remote_system_access"  =>  "Remote System Access", 
 		"irecruit_usernotifications" => "User Notifications", 
		"irecruit_orgsetup" => "Organization Setup",
		"irecruit_features"=> "Organization Features",
                "irecruit_vault"=>"Organization Vault",
                "irecruit_roles"=>"Role Administration",
                "irecruit_themes"=>"Themes",
                "irecruit_up_application_themes"=>"User Portal Themes",
                "notification_reminders"=>"Calendar Notifications",
                "manage_notification_reminders"=>"Manage Calendar",
                "irecruit_purchases"=>"Purchases",
                "irecruit_icons"=>"Icon Usage",
                "irecruitbillingcounts"=>"Billing Counts",
                "irecruit_branding"=>"Branding",
                "irecruit_req_highlight_icons"=>"Requisition Highlight Icons",
                "irecruit_production_logs"=>"Production Logs",
                "refreshintuittoken"=>"Refresh Intuit Token",
                "irecruit_twilio_conversations"=>"iRecruit Text Report",
                "irecruit_twilio_accounts"=>"Twilio Accounts",
                "irecruit_twilio_usage"=>"Twilio Usage",
		"custom_webforms"=>"PDF PreFill Forms"
		),
                "wotcprocessing"=>array(
                "wotc_change_applicant"=>"Change Applicant",
                "state_portals"=>"State Portals",
                "wotc_hours"=>"Process Hours",
                "exportrecent"=>"Export Today",
                "exportrecent&offset=1"=>"Export Yesterday",
                "wotc_step_two"=>"WOTC Process to State",
                "wotc_administration_cms"=>"WOTC Administration",
		),
                "wotcreporting"=>array(
	        "wotcrollupreport" =>"WOTC Roll-Up Report",
                "exportdycomhires"=>"Export Dycom Hires",
                "exportshoeshowhours"=>"Export Shoe Show Hours",
                "poa_agreements"=>"Create Agreement / POA",
                )
); 

// begin permissions
if (($AUTH['wotcAccess'] != "A") && ($AUTH['iRecruitAccess'] != "A")) {
        unset($navsection);
        unset($navisubmenus);
}

if ($AUTH['wotcAccess'] != "A") {
        unset($navsection['wotcreporting']);
        unset($navsection['wotcprocessing']);
        unset($navsection['wotcdocuments']);
        unset($navsection['wotccrm']);
        unset($navsection['callcenter']);
}

if ($AUTH['iRecruitAccess'] != "A") {
        unset($navsection['irecruit']);
}


 
                $requestemenu = '';
                session_start();
               $_SESSION["activemenu"] ='';
               $_SESSION["activecolor"] = '';
               if(isset($_REQUEST['pg'])){
                 $requestemenu = $_REQUEST['pg'];

                }else if(isset($_REQUEST['export'])){
                 $requestemenu = $_REQUEST['export'];

                }else{}


               if($_SERVER['SERVER_NAME'] == "irecruit.pairsite.com"){
                   unset($navisubmenus['irecruit']['remote_system_access'] );
                }
                       
				$clcnt = 1; 
				foreach ($navsection as $section => $displaytitle) { 
                                         
				   $rtn .= '<li class="mainmenu'.$clcnt.' mainmenuall" style="font-weight: bold;">'; 

				   if($section == 'cms' || $section == 'wotccrm') {

					$rtn .= "<a href=\"" . ADMIN_HOME;
					$rtn .= "index.php?pg=" . $section;
				        $rtn .= "\">";
                                        $rtn .= "&nbsp;".$displaytitle;
				        $rtn .= '<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
				        $rtn .= "</a>";

				   } else if ($section == 'callcenter') {

					$rtn .= "<a href=\"" . WOTC_HOME;
					$rtn .= "callcenter.php";
				        $rtn .= "\" target=\"_blank\">";
                                        $rtn .= "&nbsp;".$displaytitle;
				        $rtn .= '<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
				        $rtn .= "</a>";


				     } else {

				    	$rtn .= "<a href=\"#";
				        $rtn .= "\">";
					$rtn .= "&nbsp;".$displaytitle;
				        $rtn .= '<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
				        $rtn .= "</a>";

                                        $scnt=1;
					foreach ($navisubmenus[$section] as $item => $submenu) {

					if(!empty($requestemenu) && $requestemenu == $item){
                                               session_start();
                                             $_SESSION["activemenu"] = $clcnt;
                                             $_SESSION["activecolor"] = $clcnt.$scnt;
                                        }


					 $rtn .= '<li class="submenu'.$clcnt.' submenuall submenu'.$clcnt.$scnt.' "  style="text-indent: 20px; height:32px;">';
					     if (strtolower($item) == "wotcrollupreport") { 

						     $rtn .= "<a style='text-transform:capitalize;' ";
						     $rtn .= " onclick=\"return confirm('Are you sure you want to run the Roll-Up Export.')\"";
						     $rtn .= "href=\"" . ADMIN_HOME . "index.php?pg=" .$item;

					      } else if (strtolower($item) == "refreshintuittoken") { 

				    	     $rtn .= "<a  style='text-transform:capitalize;'href=\"" . ADMIN_HOME . "intuit.php";
					     } else if(strtolower($item) =="exportconfig" || strtolower($item) =="exportrecent" || strtolower($item) =="exportdycomhires" || strtolower($item) == "exportrecent&offset=1" || strtolower($item) == "irecruitbillingcounts") {
				    	     $rtn .= "<a  style='text-transform:capitalize;'href=\"" . ADMIN_HOME . "index.php?export=" .$item;
					     } else {

				    	    $rtn .= "<a style='text-transform:capitalize;' href=\"" . ADMIN_HOME . "index.php?pg=" .$item;
					    }

				         $rtn .= " \">"; 
                                         $rtn .= "&nbsp;".$submenu;
                                     	 $rtn .= "</a>";
				         $rtn .= '</li>';

					$scnt++;
					} // end foreach subnavigation links
					                                                                                 
				    } // end menues with subnavigation				    
				    
				    $rtn .= '</li>';
				    $clcnt ++;
				} // end foreach

				echo $rtn;


				?>
			</ul> 
		</div> 
                
		<!-- /.sidebar-collapse -->
               <script>
	       $(document).ready(function(){  
              var activmenu = "<?php session_start(); echo $_SESSION["activemenu"]; ?>";  
              var activcolormenu = "<?php session_start(); echo $_SESSION["activecolor"]; ?>";  

		if(activmenu != null){   
                 $('li.ssubmenu'+activcolormenu +' a').toggleClass('active');
                }
               $('li.submenuall').hide(); 
	    
          if(activmenu != null){   
            $('li.submenu'+activmenu ).show();  
          }else{ 

            $('li.submenuall').hide(); 
         } 

for (let i = 1; i < 6; i++) {

        $('li.mainmenu'+i).click(function () { 

        if($('.submenu'+i).css('display') !== 'none'){
             $('li.mainmenu'+i+' i').removeClass('fa fa-chevron-circle-down').addClass('fa fa-chevron-circle-right');
            $('li.submenu'+i).hide();  

        }else{
             $('li.mainmenuall i').removeClass('fa fa-chevron-circle-down').addClass('fa fa-chevron-circle-right');
            $('li.submenuall').hide(); 
                 $('li.mainmenu'+i+' i').removeClass('fa fa-chevron-circle-right').addClass('fa fa-chevron-circle-down');
            $('li.submenu'+i).show();  

       }

 
        });  

}
   

     });  
		</script>

</nav>
 
