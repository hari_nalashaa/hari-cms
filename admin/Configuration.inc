<?php
ini_set ('display_errors', 0);
ini_set('memory_limit', '8192M');

require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';

require_once VENDOR_DIR . 'autoload.php';

//Include all required classes from common folder
require_once ADMIN_DIR . 'SessionConfig.inc';

if(!headers_sent()) {
	header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
}

//Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';

define("MASTERADMIN", "Y");
?>
