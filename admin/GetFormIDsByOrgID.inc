<?php

if($FormType == "ApplicationForm") {
    $FormIDs    =   G::Obj('FormQuestions')->getFormIdsByOrgID($OrgID);
    $FormIDs    =   array("All"=>"All") + $FormIDs;
}
else if($FormType == "WebForm") {
    $FormIDs    =   array();
    $web_forms  =   G::Obj('WebForms')->getWebFormsListByOrgID("WebFormID, FormName", $OrgID, "Active");
    
    for($wf = 0; $wf < count($web_forms); $wf++) {
        $FormIDs[$web_forms[$wf]['WebFormID']]  =   $web_forms[$wf]['FormName'];
    }
    
    $FormIDs    =   array("All"=>"All") + $FormIDs;
}
else if($FormType == "PreFilledForm") {
    $FormIDs    =   array();
    $pre_forms  =   G::Obj('PreFilledForms')->getPreFilledFormsListByOrgID("PreFilledFormID, FormName", "MASTER");
    
    for($wf = 0; $wf < count($pre_forms); $wf++) {
        $FormIDs[$pre_forms[$wf]['PreFilledFormID']]  =   $pre_forms[$wf]['FormName'];
    }
    
    $FormIDs    =   array("All"=>"All") + $FormIDs;
}
else if($FormType == "AgreementForm") {
    $FormIDs    =   array();
    $agr_forms  =   G::Obj('AgreementForms')->getAgreementFormsListByOrgID("AgreementFormID, FormName", $OrgID, "Active");
    
    for($wf = 0; $wf < count($agr_forms); $wf++) {
        $FormIDs[$agr_forms[$wf]['AgreementFormID']]  =   $agr_forms[$wf]['FormName'];
    }
    
    $FormIDs    =   array("All"=>"All") + $FormIDs;
}
?>
