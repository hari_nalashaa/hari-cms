<?php
require_once 'Configuration.inc';

$AUTH       =   G::Obj('ADMIN')->authenticate($_COOKIE['AID']);

$OrgID      =   $_REQUEST['OrgIDs'];
$FormType   =   $_REQUEST['FormTypes'];

require_once 'GetFormIDsByOrgID.inc';

echo json_encode(array("FormIDs"=>$FormIDs, "SelectedFormIDs"=>$form_ids));
?>