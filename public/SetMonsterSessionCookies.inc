<?php 
if(isset($_REQUEST['RandID']) && $_REQUEST['RandID'] != "") $RandID = $_REQUEST['RandID'];
else $RandID = uniqid(date('YmdHis')).sha1(crypt(time(), rand(1, 999)));

$url        =   "";

if(!isset($RequestID) || $RequestID == "") {
    $RequestID  =   "";
}

if(isset($_REQUEST['MonsterRequestURL']) && $_REQUEST['MonsterRequestURL'] != "") {
	$url = $_REQUEST['MonsterRequestURL'];
}

setcookie ( "MonsterRandID",        $RandID,    time () + 3600, "/" );
setcookie ( "MonsterRequestMethod", '1',        time () + 3600, "/" );
setcookie ( "MonsterRequestURL",    $url,       time () + 3600, "/" );
setcookie ( "MonsterRequestID",     $RequestID, time () + 3600, "/" );

$_SESSION['P'][$OrgID.$MultiOrgID]['MonsterRandID']         =   $RandID;
$_SESSION['P'][$OrgID.$MultiOrgID]['MonsterRequestMethod']  =   1;
$_SESSION['P'][$OrgID.$MultiOrgID]['MonsterRequestURL']     =   $url; 
$_SESSION['P'][$OrgID.$MultiOrgID]['MonsterRequestID']      =   $RequestID;

if(!isset($Email) || is_null($Email)) $Email = "";

if(isset($url) && $url != "") {
	//Delete existing record based on this cookie
	$MonsterObj->delSessionCookie($HoldID);
	//Insert Monster Session Cookie Information
	$MonsterObj->insMonsterSessionCookie('1', $url, $RandID, $HoldID, $RequestID, $ApplicantInfo['EmailAddress'], 'Pending');
}
?>