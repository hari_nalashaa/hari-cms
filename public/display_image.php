<?php
require_once 'public.inc';

if (isset($_REQUEST['OrgID']) && isset($_REQUEST['Type'])) {
	 	// $OrgID="B12345467";
	// $MultiOrgID="asdfkadfad";
	// $Type="Secondary"; //or Primary
		//set columns
	$columns = "OrgID, PrimaryLogoType, PrimaryLogo, SecondaryLogoType, SecondaryLogo,SocialMediaLogoType,SocialMediaLogo";
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Get Organization Logos Information
	$results = $OrganizationsObj->getOrganizationLogosInformation($columns, $where, '', array($params));

	if(is_array($results['results'])) {
		foreach ($results['results'] as $img) {
			$orgid = $img ['OrgID'];
			$type1 = $img ['PrimaryLogoType'];
			$data1 = $img ['PrimaryLogo'];
			$type2 = $img ['SecondaryLogoType'];
			$data2 = $img ['SecondaryLogo'];
			$type3 = $img ['SocialMediaLogoType'];
			$data3 = $img ['SocialMediaLogo'];

		}
	}
	
	if (($type1) && ($_REQUEST['Type'] == "Primary")) {
		header ( "Content-type: $type1" );
		echo $data1;
		exit ();
	}
	
	if (($type2) && ($_REQUEST['Type'] == "Secondary")) {
		header ( "Content-type: $type2" );
		echo $data2;
		exit ();
	}
        if (($type3) && ($_REQUEST['Type'] == "SocialMedia")) {
		header ( "Content-type: $type3" );
		echo $data3;
		exit ();
	}

} else {
	
	echo <<<END
<html>
<head><title></title></head>
<body></body>
</html>
END;
}
?>