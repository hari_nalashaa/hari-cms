<?php
//Redirect it to Job Application page
if(isset($pg) && $pg == 'application') {
    if(isset($MultiOrgID)
        && $MultiOrgID != ""
        && $RequestID != "") {
            
            $app_info_log_data  =  "\nFrom Source: " . FROM_SRC."\n";
            $app_info_log_data  .=  "\nOrgID: " . $OrgID."\n";
            $app_info_log_data  .=  "\nMultiOrgID: " . $MultiOrgID."\n";
            $app_info_log_data  .=  "\nRequestID: " . $_REQUEST['RequestID']."\n";
            $app_info_log_data  .=  "\nPG: " . $pg."\n";
            $app_info_log_data  .=   "\nServer Info: " . json_encode($_SERVER);
            $app_info_log_data  .=  "\nRequest: " . json_encode($_REQUEST);
            
            Logger::writeMessage(ROOT."logs/public_application/". $OrgID . "-" . date('Y-m-d-H') . "-index.txt", $app_info_log_data, "a+", true, "Public - Index.php - Hit");
            
            header("Location:".PUBLIC_HOME."jobApplication.php?OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID."&RequestID=".$_REQUEST['RequestID']."&HoldID=".$HoldID);
            exit();
        }
        else if(isset($OrgID) && $OrgID != "" && $RequestID != "") {
            
            $app_info_log_data  =  "\nFrom Source: " . FROM_SRC."\n";
            $app_info_log_data  .=  "\nOrgID: " . $OrgID."\n";
            $app_info_log_data  .=  "\nMultiOrgID: " . $MultiOrgID."\n";
            $app_info_log_data  .=  "\nRequestID: " . $_REQUEST['RequestID']."\n";
            $app_info_log_data  .=  "\nPG: " . $pg."\n";
            $app_info_log_data  .=   "\nServer Info: " . json_encode($_SERVER);
            $app_info_log_data  .=  "\nRequest: " . json_encode($_REQUEST);
            
            Logger::writeMessage(ROOT."logs/public_application/". $OrgID . "-" . date('Y-m-d-H') . "-index.txt", $app_info_log_data, "a+", true, "Public - Index.php - Hit");
            
            header("Location:".PUBLIC_HOME."jobApplication.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&HoldID=".$HoldID);
            exit();
        }
}

if ($feature ['InternalRequisitions'] == "Y") {
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );

 if($InternalCode == $_REQUEST['pg'] || $_REQUEST['pg'] == $InternalCode) {
    $internal_redirect =    PUBLIC_HOME."internalRequisitions.php?OrgID=".$OrgID;
    if($MultiOrgID != "") {
        $internal_redirect .=   "&MultiOrgID=".$MultiOrgID;
    }
    $internal_redirect .=   "&InternalVCode=".$InternalCode;
    if($_REQUEST['RequestID'] != "") {
        $internal_redirect .=   "&RequestID=".$_REQUEST['RequestID'];
    }
    if($HoldID != "") {
        $internal_redirect .=   "&HoldID=".$HoldID;
    }
    header("Location:".$internal_redirect);
    exit();
 }

}
?>
