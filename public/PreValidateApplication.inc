<?php
if($HoldID == '')
{
    $HsetHoldIDoldID = G::Obj('ApplicantProcess')->setHoldID($OrgID, $MultiOrgID);

    $RequisitionLink        =   "jobApplication.php?OrgID=".$OrgID;
    if($MultiOrgID != "") {
        $RequisitionLink    .=  "&MultiOrgID=".$MultiOrgID;
    }
    $RequisitionLink        .=  "&HoldID=".$HsetHoldIDoldID."&RequestID=".$RequestID;

    if($pg != "") {
        $RequisitionLink    .=  "&pg=".$pg;
    }
    
    if($InternalVCode != "") {
        $RequisitionLink    .=  "&InternalVCode=".$InternalVCode;
    }
    
    header("Location:".$RequisitionLink);
    exit();
} 

if ($HoldID) {
    $APPDATA       =   array ();
    $APPDATAREQ    =   array ();
    
    $results       =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);
    
    if(is_array($results['results'])) {
        foreach($results['results'] as $row) {
            $APPDATA [$row ['QuestionID']] = $row ['Answer'];
            $APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
        } // end foreach
    }
    
    $FormID        =   $APPDATA ['FormID'];
    
    $validate_req_info = $RequisitionsObj->getRequisitionsDetailInfo("FormID", $OrgID, $RequestID);
    if($FormID == '') {
        $FormID    =   $validate_req_info ['FormID'];
    }
    
    $source        =   $APPDATA ['source'];
} // end HoldID

$colwidth   =   "220";
$highlight  =   '#' . $COLOR ['ErrorHighlight'];

$ReqMultiOrgID  =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

if ($feature ['InternalRequisitions'] == "Y") {
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
}

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
	$typeform   =   "InternalFormID";
} else {
	$typeform   =   "FormID";
}

$req_columns    =   "InternalFormID, Title, FormID, Active, DATE(ExpireDate) as ExpireDate";
$req_details    =   G::Obj('Requisitions')->getRequisitionsDetailInfo($req_columns, $OrgID, $RequestID);
$dates_diff     =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details['ExpireDate']);
$FormID         =   $req_details[$typeform];


if($req_details['Active'] == 'N'
    || $dates_diff > 0) {
    header("Location: jobApplicationThankYou.php?RequestID=".$_REQUEST ['RequestID']."&OrgID=".$_REQUEST['OrgID'].'&msg=reqexpired');
    exit();
}

$redirect = PUBLIC_HOME . "index.php?OrgID=".$OrgID;
if (isset($MultiOrgID) && $MultiOrgID != "") {
  $redirect .= "&MultiOrgID=".$MultiOrgID;
}
$redirect .= "&navpg=listings";

if (!isset($FormID) || $FormID == "") {

	header("Location: " . $redirect);
        exit();

}

?>
