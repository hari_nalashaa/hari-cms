$(document).ready(function() {
	$("#public_application_steps li a").click(function(e) {
		e.preventDefault();
		var tab_id  =	this.id;
		var tab_key =	tab_id.substr(10);
		
		$(".public_application_process_content").hide();
		$("#section_content"+tab_key).show();

		$("#public_application_steps > li > a").removeAttr('style');
		$(this).css({"background-color":up_active_step_tab, "color":"white"});
		$("#process_message").html("");
	});
	
	$(".public_application_process_content").hide();
	$(".public_application_process > .public_application_process_content:eq(0)").show();
	$("#public_application_steps > li > a:eq(0)").css({"background-color":up_active_step_tab, "color":"white"});
});

function nextApplicationForm(next_section_id) {
	$(".public_application_process_content").hide();
	$("#section_content"+next_section_id).show();
	$("#process_message").html("");
	$("#public_application_steps > li > a").removeAttr('style');
	$("#section_id"+next_section_id).css({"background-color":up_active_step_tab, "color":"white"});	
}

/**
 * @method	clearApplicantAttachmentsTemp
 * @param	question_id
 */
function clearApplicantAttachmentsTemp(question_id) {
	
	$("#attachments_process_msg").html("<img src='images/loading-small.jpg'>Please wait.....");
	
    var OrgID		=	document.forms['frmProcessApplication'].OrgID.value;
	var MultiOrgID	=	document.forms['frmProcessApplication'].MultiOrgID.value;
    var SectionID 	=	document.forms['frmAttachmentsInfo'].SectionID.value;
    var RequestID	=	document.forms['frmAttachmentsInfo'].RequestID.value;
    var FormID		=	document.forms['frmAttachmentsInfo'].FormID.value;
    
    var input_data	=	{"OrgID":OrgID, "MultiOrgID":MultiOrgID, "SectionID":SectionID, "RequestID":RequestID, "FormID":FormID, "QuestionID":question_id};
    
    $.ajax({
		method: "POST",
  		url: "clearApplicantAttachmentsTemp.php",
  		data: input_data,
		type: "POST",
		dataType: 'json',
		success: function(data) {
			if(data.Status == "Success") {
				$("#attachments_process_msg").css({"color":"blue"});
				$("#attachments_process_msg").html("Attachment successfully removed. Please refresh this page.");
			}
			else if(data.Status == "Failed") {
				$("#attachments_process_msg").css({"color":"blue"});	
				$("#attachments_process_msg").html("Unable to remove this attachment. Please refresh this page and try again.");
			}
			
			getSectionContent(OrgID, MultiOrgID, SectionID, RequestID, FormID);
		}
    });	

}

/**
 * @method	getSectionContent
 * @param	OrgID
 * @param	MultiOrgID
 * @param	SectionID
 * @param	RequestID
 * @param	FormID
 */
function getSectionContent(OrgID, MultiOrgID, SectionID, RequestID, FormID) {
	
	$("#attachments_process_msg").html("<img src='images/loading-small.jpg'>Please wait section will refresh.....");
	var input_data	=	{"OrgID":OrgID, "MultiOrgID":MultiOrgID, "SectionID":SectionID, "RequestID":RequestID, "FormID":FormID};
	
	$.ajax({
		method: "POST",
  		url: "getAttachmentsSection.php",
  		data: input_data,
		type: "POST",
		success: function(data) {
			$("#attachments_process_msg").html("");
			$("#section_content6").html(data);
		}
    });	

}

/**
 * @method	processApplicationForm
 * @param	form_id
 * @param	section_id
 * @param	next_section_id
 */
function processApplicationForm(form_id, section_id, next_section_id) {

	$("#process_message").html('<br/><img src="images/wait.gif"/> Please wait..data is processing...<br/><br/>');
	var input_data = new FormData($('#'+form_id)[0]);
	
	$.ajax({
		method: "POST",
  		url: "processApplicationForm.php",
  		data: input_data,
		type: "POST",
		processData: false,
		contentType: false,
		dataType: 'json',
		success: function(data) {
			
			$(".form-group > label.question_name").removeAttr('style');
			
			var errors_list = '';
			var error_desc = '';
			
			if(data['success'] == 'false') {
				
				var data_errors_list = data['errors_list'];
				
			    Object.keys(data_errors_list).forEach(function(key) {
			      $('label[for="'+key+'"]').css({
			    	  "padding-bottom":"12px",
				      "background-color":"yellow"
				  });
				  //QuestionID - key
		    	  error_desc = data_errors_list[key];
				  errors_list += error_desc.replace("\\n", "<br>");
		    	});

			    errors_list += "<br><br>";
				
				if(errors_list != "") {
					//It is for testing
					$("#process_message").html(errors_list).css("color", "red");
				}
				else {
					$("#process_message").html('');
				}
			}
			else if(data['success'] == 'true') {
				$("#process_message").html('Saved Successfully');
		    	$("#process_message").css("color", "#4889f2");
		    	$(".form-group > label.question_name").removeAttr('style');
		    	$("#"+section_id).attr('class', 'public_application_step_tabs_filled');
			}
			
		    $("#process_finish_button_validation").html("<br/><img src='images/loading-small.jpg'>Please wait.....<br/>");
		    
		    $.ajax({
				method: "POST",
		  		url: "validateFilledFieldsInfo.php",
		  		data: input_data,
				type: "POST",
				processData: false,
				contentType: false,
				dataType: 'json',
				success: function(data) {

					//Get OrgID, MultiOrgID, SectionID, RequestID, FormID
				    var OrgID		=	document.forms['frmProcessApplication'].OrgID.value;
					var MultiOrgID	=	document.forms['frmProcessApplication'].MultiOrgID.value;
				    var SectionID 	=	6;
				    var RequestID	=	document.forms['frmProcessApplication'].RequestID.value;
				    var FormID		=	document.forms['frmProcessApplication'].FormID.value;
				    var first		=	$("#first").val(); 
		    		var middle		=	$("#middle").val();
			    	var last		=	$("#last").val();
			    	var applicant_name = last + " " + middle + " " + first;
			    	applicant_name	=	applicant_name.replace("  ", " ");
			    	
					if(data['success'] == 'true') {
						$("#btnProcessApplicationForm").removeAttr('class');
						$("#btnProcessApplicationForm").attr('type', 'submit');   
					}

					$("#process_finish_button_validation").html("");
					
					if(errors_list == "") {
						if(next_section_id != "") {
							$(".public_application_process_content").hide();
							$("#section_content"+next_section_id).show();
							$("#process_message").html("");
							$("#public_application_steps > li > a").removeAttr('style');
							$("#section_id"+next_section_id).css({"background-color":up_active_step_tab, "color":"white"});	
							
							$("#aa_applicant_name, #vet_applicant_name, #dis_applicant_name").html(applicant_name);
							//getSectionContent(OrgID, MultiOrgID, SectionID, RequestID, FormID);
						}
					}
				}
		    });	
			
    	}
	});
	
}

/**
 * @method	validateSignature
 * @returns {Boolean}
 */
function validateSignature(btnObj) {
	
	var frmObj		= document.frmProcessApplication;
	var signature	= frmObj.signature.value;
	var agree		= frmObj.agree.checked;
	
	if(signature == "" || agree == false) {
	
		alert("Please check the agreement and fill the signature");
		return false;
	}
	else {
		
		$("#process_message").html('<br/><img src="images/wait.gif"/> Please wait..data is processing...');
		var input_data = new FormData($('#frmProcessApplication')[0]);
		
		$("#btnProcessApplicationForm").attr("class", "btnDisabled"); 
		
		$.ajax({
			async: false,
			method: "POST",
	  		url: "processApplicationForm.php",
	  		data: input_data,
			type: "POST",
			processData: false,
			contentType: false,
			dataType: 'json',
			success: function(data) {
				$("#process_message").html('<br/><img src="images/wait.gif"/> Please wait..data is processing...');
				document.forms['frmProcessApplication'].submit();
	    	}
		});
	}
	
	return true;
}
/*
//It seems like old code
function processApplicationInfo() {
	
	$("#edit_application_progress").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...');
	
	var input_data = new FormData($('#application')[0]);
	
	$.ajax({
		method: "POST",
  		url: 'applicants/processApplicationFormInfo.php', 
		data: input_data,
		type: "POST",
		processData: false,
		contentType: false,
		success: function(data) {
			$("#edit_application_progress").html(data);
    	}
	});
}
*/