function show_hide_errors() {
	$("#errors_list").toggle();
}

function getCounties(state,wotcid,appid) {
        if (state != "") {
                document.getElementById("County").style.display = "block";
                $('#County').find('option').remove();

                var d = new Date().getTime();
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    $('#County').append(this.responseText);
                  }
                };
                xhttp.open("POST", "ajax/counties.php", true);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhttp.send("state=" + state + "&wotcid=" + wotcid + "&appid=" + appid + "&d=" + d);
        } else {
                document.getElementById("County").style.display = "none";
        }
}

function setCounty(county) {
        setTimeout(() => { document.getElementById('County').value = county; }, 500);
}

function getFormIDChildQuestionsInfo() {
	for (question_id in child_ques_info) {
		  var child_que_info =  child_ques_info[question_id];
		  for (question_id_option in child_que_info) {
			  if(que_types_list[question_id] == 2
				|| que_types_list[question_id] == 22
				|| que_types_list[question_id] == 23) {
					
                    var checked_radio = $("input[name='"+question_id+"']:checked").val();
                    
                    if(checked_radio == question_id_option)
                    {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                        	if(child_ques_list[child_ques_ids] == "show") {
                        		$("#divQue-"+child_ques_ids).show();
                        	}
                        	else if(child_ques_list[child_ques_ids] == "hidden") {
                        		$("#divQue-"+child_ques_ids).hide();
                        	}							
                        }
                    }
                    else if(typeof(checked_radio) == 'undefined') {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                            $("#divQue-"+child_ques_ids).hide();
                        }
                   }
			 }
			 else if(que_types_list[question_id] == 3) {
				  var checked_radio = $("#"+question_id).val();
				  //Exception for country question, have to handle it in simple way
				  if(question_id == 'country' && typeof(checked_radio) != 'undefined') {
					  if(checked_radio != 'US' && checked_radio != 'CA') {
						  	child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
					  }
					  else if(checked_radio == question_id_option)
	                  {
		                    child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
	                  }
				  }
				  else if(checked_radio == question_id_option)
                  {
	                    child_ques_list = child_que_info[question_id_option];
	                    for(child_ques_ids in child_ques_list) {
	                    	if(child_ques_list[child_ques_ids] == "show") {
	                    		$("#divQue-"+child_ques_ids).show();
	                    	}
	                    	else if(child_ques_list[child_ques_ids] == "hidden") {
	                    		$("#divQue-"+child_ques_ids).hide();
	                    	}							
	                    }
                  }
                  else if(typeof(checked_radio) == 'undefined') {
                      child_ques_list = child_que_info[question_id_option];
                      
                      for(child_ques_ids in child_ques_list) {
                      		$("#divQue-"+child_ques_ids).hide();
                      }
                 }
			 }
		  }
	}
}

$(document).ready(function() {
	getFormIDChildQuestionsInfo();
});
