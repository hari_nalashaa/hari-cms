function verifyReceivedCode() {
	var OrgID			=	urlParam('OrgID');
	var ReceivedCode	=	frmVerifyPhoneNumberInfo.txtPhoneVerifyCode.value;

	var verify_code_msg	=	"";
	var first 			= 	document.frmPersonalInfo.first.value;
	var last 			= 	document.frmPersonalInfo.last.value;
	var cell_phone1 	= 	document.frmPersonalInfo.cellphone1.value;
	var cell_phone2 	= 	document.frmPersonalInfo.cellphone2.value;
	var cell_phone3 	= 	document.frmPersonalInfo.cellphone3.value;
	var cell_phone		=	cell_phone1 + cell_phone2 + cell_phone3;
	
	var request = $.ajax({
		async: true,   // this will solve the problem
		method: "POST",
  		url: "verifyPhoneNumber.php?OrgID="+OrgID+"&cell_phone="+cell_phone+"&first="+first+"&last="+last+"&ReceivedCode="+ReceivedCode,
		type: "POST",
		beforeSend: function() {
			$("#approval_msg").html('Please wait.. <img src="images/wait.gif"/><br><br>');
		},
		success: function(data) {
			if(data == 'SuccessFullyApproved') {
				verify_code_msg	= "Successfully verified your number.";
				$("#approval_msg").html(verify_code_msg);
				$("#set_cellphone_permission").val("SuccessfullyVerified");
				$("#spnQueMessage-cellphonepermission").html('&nbsp;<img src="images/check-icon.png"/>&nbsp;Verified');
				setTimeout(function(){ closeForm(); }, 2000);
			}
			else {
				verify_code_msg	= "Failed to approve your number. Please try again by clicking on below link.";
				$("#resend_code_again").show();
				$("#approval_msg").html();
				$("#set_cellphone_permission").val("FailedToVerify");
				$("#spnQueMessage-cellphonepermission").html('&nbsp;<img src="images/cancel.png"/>&nbsp; Failed to verify');
			}
    	}
	});
}

function verifyPhoneNumber(first, last, cell_phone) {
	var OrgID	=	urlParam('OrgID');

	var request = $.ajax({
		async: true,   // this will solve the problem
		method: "POST",
  		url: "verifyPhoneNumber.php?OrgID="+OrgID+"&cell_phone="+cell_phone+"&first="+first+"&last="+last,
		type: "POST",
		beforeSend: function() {
			$("#spnQueMessage-cellphonepermission").html('&nbsp;Please wait.. <img src="images/wait.gif"/><br><br>');
		},
		success: function(data) {

			if(data != "Verified") {
				openForm();
				$("#spnQueMessage-cellphonepermission").html('');
			}
			else {
				$("#set_cellphone_permission").val("SuccessfullyVerified");
				$("#spnQueMessage-cellphonepermission").html('&nbsp;<img src="images/check-icon.png"/>&nbsp;Verified');
			}
    	}
	});
}

function openForm() {
	document.getElementById("myModal").style.display = "block";
}

function closeForm() {
	if($("#set_cellphone_permission").val() != "SuccessfullyVerified") {
		$('#cellphonepermission').prop("selectedIndex", 2);
	}
	document.getElementById("myModal").style.display = "none";
}
	
$(document).ready(function() {

	$('#cellphonepermission').change(function() {

		var first 		= 	document.forms['frmPersonalInfo'].first.value;
		var last 		= 	document.forms['frmPersonalInfo'].last.value;
		var cell_phone1 = 	document.forms['frmPersonalInfo'].cellphone1.value;
		var cell_phone2 = 	document.forms['frmPersonalInfo'].cellphone2.value;
		var cell_phone3 = 	document.forms['frmPersonalInfo'].cellphone3.value;
		var cell_phone	=	cell_phone1 + cell_phone2 + cell_phone3;
		
		var cell_phone_permission = this.value;
		
		if(cell_phone_permission == "Yes") {
			if(cell_phone1 != ""
				&& cell_phone2 != ""
				&& cell_phone3 != ""
				&& first != ""
				&& last != "") {
				
				verifyPhoneNumber(first, last, cell_phone);
			}
			else {
				alert("Please fill first name, last name, cell phone \nbefore selecting permission to text");
				$('#cellphonepermission').prop("selectedIndex", 0);
			}
		}
		
	});
	
});