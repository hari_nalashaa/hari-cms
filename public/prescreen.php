<?php
require_once 'public.inc';
include_once IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';
include_once PUBLIC_DIR . 'JobApplicationRequestVars.inc';

if(!isset($RequestID) || $RequestID == "") {
    $RequisitionLink    =   "index.php?OrgID=".$OrgID;
    if($MultiOrgID != "") {
        $RequisitionLink    .=   "&MultiOrgID=".$MultiOrgID;
    }
    $RequisitionLink    .=  "&navpg=listings";
    header("Location:".$RequisitionLink);
    exit();
}

$title      =   "Prescreen Questions";

//Requisition details
$req_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title", $OrgID, $RequestID);
// Bind parameters
$params     =   array (':OrgID' => $OrgID, ':RequestID' => $RequestID);
// Set condition
$where      =   array ("OrgID = :OrgID", "RequestID = :RequestID");

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
    $where [] = "Internal IN ('Y','B')";
} else {
    $where [] = "Internal IN ('N','B','')";
}

// Get Prescreen Questions Information
$rslts      =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation ( "*", $where, "SortOrder", array($params) );

$rejection_text = '';
if(isset($_POST['prescreen_process']) && $_POST['prescreen_process'] == "Process") {
    $cnt = 0;

    // loop through all questions for a match. If one doesn't it will throw a not qualify
    if (is_array ( $rslts ['results'] )) {
        foreach ( $rslts ['results'] as $PRESCREEN ) {

            if ($PRESCREEN ['Answer'] != $_POST [$PRESCREEN ['SortOrder']]) {
                $cnt ++;
            }

            //Insert Prescreen Results
            $prescreen_result_info = array (
                "OrgID"         =>  $OrgID,
                "HoldID"        =>  $HoldID,
                "RequestID"     =>  $RequestID,
                "Question"      =>  $PRESCREEN ['Question'],
                "Answer"        =>  $PRESCREEN ['Answer'],
                "SortOrder"     =>  $PRESCREEN ['SortOrder'],
                "EntryDate"     =>  "NOW()",
                "Submission"    =>  $_POST [$PRESCREEN ['SortOrder']]
            );
            
            G::Obj('PrescreenQuestions')->insUpdPrescreenResults ( $prescreen_result_info );

        } // end foreach
    }


    if ($cnt > 0) {

        //Get Prescreen Text Information
        $where      =   array("OrgID = :OrgID", "Rejection != ''");
        //Get PrescreenText Information
        $PRESCREENT =   G::Obj('PrescreenQuestions')->getPrescreenTextInfo($OrgID, $where, "OrgID LIMIT 1");
        
        // this is a fail entry
        $rejection_text .= $PRESCREENT ['Rejection'] . "\n";
        $rejection_text .= '<br><br>' . "\n";
        $rejection_text .= 'Please see our full listings for other positions you may qualify for <a href="index.php?';
        $rejection_text .= 'OrgID=' . $OrgID;
        if ($MultiOrgID) {
            $rejection_text .= '&MultiOrgID=' . $MultiOrgID;
        }
	    $rejection_text .= '&navpg=listings&pg=split';
        $rejection_text .= '">here</a>.';
        $rejection_text .= '<br><br>' . "\n";
    }
    else {
        $job_app_link        =   "jobApplication.php?OrgID=".$OrgID;
        if($MultiOrgID != "") {
            $job_app_link    .=  "&MultiOrgID=".$MultiOrgID;
        }
        
        $job_app_link        .=  "&HoldID=".$HoldID."&RequestID=".$RequestID;
        
        if($InternalVCode != "") {
            $job_app_link    .=  "&InternalVCode=".$InternalVCode;
        }

        header("Location:".$job_app_link);
        exit();
    }
}

require_once PUBLIC_DIR . 'Header.inc';

if($rejection_text == '') {
    ?>
    <form name="frmPreScreenInfo" id="frmPreScreenInfo" method="post">
    <?php
    $prescreen_app_form = include PUBLIC_DIR . "Prescreen.inc";
    echo $prescreen_app_form;
    ?>
	<input type="hidden" name="pg" id="pg" value="<?php echo $pg;?>">
    <input type="hidden" name="HoldID" id="HoldID" value="<?php echo $HoldID;?>">
    <input type="hidden" name="InternalVCode" id="InternalVCode" value="<?php echo $InternalVCode;?>">
    <input type="hidden" name="prescreen_process" id="prescreen_process" value="Process">
    <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit">
    </form>
    <?php
}
else {
    echo $rejection_text;
}

require_once PUBLIC_DIR . 'Footer.inc';
?>
