<?php
$publicportal_sections_list	=   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

$section_forms              =   array(
                                    "1"     =>  "frmPersonalInfo",
                                    "2"     =>  "frmAvailabilityForWorkInfo",
                                    "3"     =>  "frmBackgroundInfo",
                                    "4"     =>  "frmMrpInfo",
                                    "5"     =>  "frmDriversRecordInfo",
                                    "6"     =>  "frmAttachmentsInfo",
                                    "7"     =>  "frmCRTInfo",
                                    "8"     =>  "frmREFInfo",
                                    "9"     =>  "frmEDUInfo",
                                    "10"    =>  "frmSecurityInfo",
                                    "11"    =>  "frmCurrentEmploymentInfo",
                                    "12"    =>  "frmSocialMediaProfilesInfo",
                                    "13"    =>  "frmMilitaryExperienceInfo",
                                    "14"    =>  "frmApplicantProfilePictureInfo",
                                    "AA"    =>  "frmAffirmativeActionInfo",
                                    "VET"   =>  "frmVeteranInfo",
                                    "DIS"   =>  "frmDisabledInfo",
                                );
$section_forms_keys         =   array_keys($section_forms);

foreach($publicportal_sections_list as $public_portal_section_id=>$public_portal_section_info) {
    $section_title  =   preg_replace("/[^A-Za-z]/", '', $public_portal_section_info['SectionTitle']);
    
    if(!in_array($public_portal_section_id, $section_forms_keys)) {
        $section_forms[$public_portal_section_id] =   "frm".$section_title;
    }
}
?>