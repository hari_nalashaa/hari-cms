<?php
require_once 'public.inc';

$OrgID      =   isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$MultiOrgID =   isset($_REQUEST['MultiOrgID']) ? $_REQUEST['MultiOrgID'] : '';
$RequestID  =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$HoldID     =   isset($_REQUEST['HoldID']) ? $_REQUEST['HoldID'] : '';
$FormID     =   isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';

//Get Requisition information based on RequestID //Set where condition for requisition
$req_info   =   G::Obj('Requisitions')->getReqDetailInfo("Title", $OrgID, $MultiOrgID, $RequestID);

$title      =   "Thank you for applying for this postion";

if (FROM_SRC == "PUBLIC") {
  require_once PUBLIC_DIR . 'Header.inc';
  $LISTINGS_URL = strtolower(PUBLIC_HOME);
}

if(isset($_GET['msg']) && $_GET['msg'] == 'reqexpired') {
    ?>
	<p style="text-align: center;font-size: 15px;">
	<br><br><br>
	This requisition has expired and is no longer active.
    </p>
    <?php
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'duplicate') {
    ?>
	<p style="text-align: center;font-size: 15px;">
    	<br><br><br>
    	<?php
        $DUPERROR   = "An application with this information has recently been submitted.<br>";
        $DUPERROR  .= "Please check your email for confirmation before submitting again.";
        echo $DUPERROR;
        ?>
    </p>
    <?php
}
else {
	?>
	<p style="text-align: center;font-size: 15px;">
    	<br><br><br>
    	Thank you for applying for <strong><?php echo $req_info['Title'];?></strong> position. 
    	<br>You will receive confirmation mail with ApplicationID.
    	<br><br>
    	<?php
    	$Listings_Link =   $LISTINGS_URL . "index.php?OrgID=".$OrgID;
    	if($MultiOrgID != "") {
    	    $Listings_Link .=  "&MultiOrgID=".$MultiOrgID; 
    	}
    	?>
    	<a href="<?php echo $Listings_Link; ?>">Go back to the Career Center</a>
    </p>
	<?php
}

if (FROM_SRC == "PUBLIC") {
  require_once PUBLIC_DIR . 'Footer.inc';
}
?>