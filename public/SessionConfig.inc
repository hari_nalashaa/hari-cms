<?php
require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';

require_once ROOT . 'traits/TraitIrecruitConnection.inc';
require_once ROOT . 'traits/TraitUserPortalConnection.inc';
require_once ROOT . 'traits/TraitWotcConnection.inc';
require_once ROOT . 'traits/TraitAdminConnection.inc';
require_once ROOT . 'traits/TraitTwilioSettings.inc';

require_once ROOT . 'classes/ClassLogger.inc';
require_once ROOT . 'classes/ClassDatabase.inc';
require_once ROOT . 'classes/ClassSession.inc';

$SessionObj = new Session();
?>
