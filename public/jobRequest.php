<?php
require_once 'public.inc';

if(isset($_REQUEST['source']) && $_REQUEST['source'] != "") {
    $_SESSION['P'][$OrgID.$MultiOrgID.$RequestID]['source']    =   $_REQUEST['source'];
}

// custom redirect
// Same Day Delivery - Amazaon Delivery Service Partner
if ($_REQUEST['MultiOrgID'] == "5a81bfee04db9") {
    $redirect_string = USERPORTAL_HOME . "requestHold.php?" . $_SERVER['QUERY_STRING'] . "&action=register";
    header("Location: " . $redirect_string);
    exit();
} // end custom redirect

//Request Parameters
if(isset($_REQUEST['RandID']) && $_REQUEST['RandID'] != "") $RandID = $_REQUEST['RandID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];

if (isset ( $_REQUEST ['requestUrl'] )) {
	
	$data = "Date: " . date ( 'Y-m-d H:i:s' ) . "\n";
	$data .= "QueryString: " . $_SERVER ['QUERY_STRING'] . "\n";
	$data .= "Request Url: " . $_REQUEST ['requestUrl'] . "\n";
	
	//Log the monster request and response
	Logger::writeMessage ( ROOT . "logs/monster/request-url-" . date ( 'Y-m-d' ) . ".log", $data, "a+", false );
	//Log the monster information to database
	Logger::writeMessageToDb($OrgID, "MonsterJobRequest", $data);
}

require_once COMMON_DIR . 'listings/PrintRequisition.inc';

if (isset ( $_REQUEST ['requestUrl'] )) {
	$url       =   $_REQUEST ['requestUrl'];
	$SecretKey =   $MonsterObj->SecretKey;
	$url       =   $url . '&stoken=' . $SecretKey;
	
	$ApplicantJsonData = $data = $CurlObj->get ( $url ); 
	
	if($ApplicantJsonData != "Bad Request") {
		$data = str_replace ( "u000du000a", "<br>", $data );
		$data = str_replace ( '\u000d\u000a', "<br>", $data );
		
		$ApplicantInfo = $POST = json_decode ( $data, true );
		
		$message      =   $POST ['CoverLetter'];
		$fileContent  =   $POST ['FileContents'];
		$ext          =   $POST ["FileExt"];
		if(substr($ext, 0, 1) == ".") $ext = substr($ext, 1);
		
		if (isset($POST['VendorField']) && $POST['VendorField'] != "") {
			$vendorfield =   explode ( "*", $POST ["VendorField"] );
			$OrgID       =   $vendorfield [0];
			$MultiOrgID  =   $vendorfield [1];
			$RequestID   =   $POST ['JobRefID'];
		} else if (isset($POST['vendorfield']) && $POST['vendorfield'] != "") {
			$vendorfield =   explode ( "*", $POST ["vendorfield"] );
			$OrgID       =   $vendorfield [0];
			$MultiOrgID  =   $vendorfield [1];
			$RequestID   =   $POST ['JobRefID'];
		}
		
		if ($feature['ResumeParsing'] == "Y") {
		
			$dir = IRECRUIT_DIR . 'vault/' . $OrgID;
			if (! file_exists ( $dir )) {
				mkdir ( $dir, 0700 );
				chmod ( $dir, 0777 );
			}
			$apatdir = $dir . '/applicantattachments';
			if (! file_exists ( $apatdir )) {
				mkdir ( $apatdir, 0700 );
				chmod ( $apatdir, 0777 );
			}
		
			// Converting byte code data to text and writing it into file
			$resumedata = "";
			if(is_array($fileContent)) {
				foreach ( $fileContent as $byte ) {
					$resumedata .= chr ( $byte );
				}
			}
		
			$fileName = $apatdir . '/' . $HoldID . '-resume' . '.' . $ext;
			touch ( $fileName );
			$fp = fopen ( $fileName, 'w' );
			fwrite ( $fp, $resumedata );
			fclose ( $fp );
		
			chmod ( $fileName, 0666 );
		
			$FormID  =   G::Obj('RequisitionDetails')->getFormID($OrgID, $MultiOrgID, $RequestID);
			G::Obj('Sovren')->processPublicPortalResumeForm($OrgID, $FormID, $RequestID, $HoldID, $fileName);
		}
		
		// Set cookie and session
		require_once 'SetMonsterSessionCookies.inc';
	}
	
} else {
	require_once 'UnSetMonsterSessionCookies.inc';
}

//Get RequisitionInformation
$results    =   G::Obj('Requisitions')->getReqDetailInfo("RequestID", $OrgID, $MultiOrgID, $RequestID);
$RequestID  =   $results['RequestID'];

// To Remove Just For Demo For Old Jobs
// $_REQUEST['source'] = "MONST";

if ($RequestID) {

	//if ($OrgID == "I20120605") { // goodwill of NY and NJ
	if ($OrgID == "I20090301") { // Superior Beverage Group

        $link = 'index.php?';
        $link .= 'OrgID=' . $OrgID;
        
        if ($MultiOrgID != "") {
                $link .= '&MultiOrgID=' . $MultiOrgID;
        }
        
        header ( 'Location: ' . PUBLIC_HOME . $link );
        exit ();

    }
	
/*	
	if (isset ( $_REQUEST ['source'] )) {
	
		if (strtoupper ( $_REQUEST ['source'] ) == "MONST") {

			$link = 'index.php?';
			$link .= 'OrgID=' . $OrgID;
			
			if ($MultiOrgID != "") {
				$link .= '&MultiOrgID=' . $MultiOrgID;
			}

			$link .= '&RequestID=' . $_REQUEST['RequestID'];
			$link .= '&source=' . $_REQUEST['source'];

			header ( 'Location: ' . PUBLIC_HOME . $link );
			exit ();
		} // end if source MONSTER
	} // end source
*/
	
	$title = "iRecruit - Job Requisition";
	
	require_once COMMON_DIR . 'listings/SocialMedia.inc';
	include 'Header.inc';
	echo PrintReq ( $OrgID, $RequestID, '' );
	include 'Footer.inc';

} else { // no RequestID

	// assume a bad link or an expired requisition
	include 'Header.inc';

	$link = 'index.php?';
	$link .= 'OrgID=' . $OrgID;
	
	if ($MultiOrgID != "") {
		$link .= '&MultiOrgID=' . $MultiOrgID;
	}
	
	echo 'The position you are trying to access has been closed.<br><br>';
	echo 'To review our currently active positions ';
	echo '<a href="' . PUBLIC_HOME . $link . '">click here.</a>';

	include 'Footer.inc';
	exit ();
} // end else RequestID
?>
