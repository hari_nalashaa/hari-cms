<?php
require_once 'public.inc';

$OrgID			=	$_REQUEST['OrgID'];
$friendly_name	=	$_REQUEST['last'] . ' ' . $_REQUEST['first'];
$phone_number	=	$_REQUEST['cell_phone'];

$phone_number	=	str_replace(array("-", "(", ")", " "), "", $phone_number);

//Add plus one before number
if(substr($phone_number, 0, 2) != "+1") {
	$phone_number	=	"+1".$phone_number;
}

$verified_phone_num_info	=	G::Obj('TwilioVerifiedPhoneNumbers')->getTwilioVerifiedPhoneNumberInfo($phone_number);

if(isset($_REQUEST['ReceivedCode']))
{
	//Verify the token
	$check_verification_info	=	G::Obj('TwilioVerifyApi')->checkTheVerificationToken($OrgID, $_SESSION['TwilioVerifyServiceID'], $phone_number, $_REQUEST['ReceivedCode']);
	$check_verification_res		=	$check_verification_info['Response'];
	$service_id					=	$check_verification_res->sid;

	//Get the Protected properties from SMS object, by extending the object through ReflectionClass
	$reflection     			=   new ReflectionClass($check_verification_res);
	$property       			=   $reflection->getProperty("properties");
	$property->setAccessible(true);
	$properties					=   $property->getValue($check_verification_res);
	
	if($properties['status'] == "approved") {
		
		$info	=	array(
						"OrgID"			=>	$_REQUEST['OrgID'],
						"ServiceID"		=>	$_SESSION['TwilioVerifyServiceID'],
						"PhoneNumber"	=>	$phone_number,
						"Name"			=>	$friendly_name								
					);
		G::Obj('TwilioVerifiedPhoneNumbers')->insTwilioVerifiedPhoneNumbers($info);
		
		echo "SuccessFullyApproved";
	}
	else {
		echo "UnableToApprove";
	}
}
else {

	if($verified_phone_num_info['PhoneNumber'] == "") {

		//Create verification service
		$created_service_info		=	G::Obj('TwilioVerifyApi')->createVerificationService($OrgID, $friendly_name);
		$created_service_response	=	$created_service_info['Response'];
		$service_id					=	$created_service_response->sid;
	
		if($service_id != "") {
			//Send verification token
			$sent_token_info		=	G::Obj('TwilioVerifyApi')->sendVerificationTokenInfo($OrgID, $service_id, $phone_number);
			$_SESSION['TwilioVerifyServiceID']	=	$service_id;
	
			if($sent_token_info['Code'] == "Success") {
				echo "SentTokenSuccessfully";
			}
			else {
				echo "FailedToSendToken";
			}
		}
		else {
			echo "FailedCreatedService";
		}
	
	} else {
		echo "Verified";
	}
}
?>