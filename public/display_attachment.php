<?php
require_once 'public.inc';

//Request Parameters
if(isset($_REQUEST['FormID']) && $_REQUEST['FormID'] != "") $FormID = $_REQUEST['FormID'];
if(isset($_REQUEST['QuestionID']) && $_REQUEST['QuestionID'] != "") $QuestionID = $_REQUEST['QuestionID'];

if (($OrgID) && ($FormID) && ($QuestionID)) {
	
	$cnt = 0;
	
	//Set where condition
	$where = array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":QuestionID"=>$QuestionID);
	//Get value from FormQuestions
	$results = $FormQuestionsObj->getFormQuestionsInformation("value", $where, "OrgID LIMIT 1", array($params));
	$cnt = $results['count'];
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $ATTACH) {
			list ( $linkname, $ext ) = split ( "\:\:", $ATTACH ['value'] );
		}
	}
	
	if ($cnt > 0) {
		
		$dfile = IRECRUIT_DIR . 'vault/' . $OrgID . '/formattachments/' . $FormID . '-' . $QuestionID . '.' . $ext;
		
		if (file_exists ( $dfile )) {
			
			header ( 'Content-Description: File Transfer' );
			header ( 'Content-Type: application/octet-stream' );
			header ( 'Content-Disposition: attachment; filename=' . basename ( $dfile ) );
			header ( 'Content-Transfer-Encoding: binary' );
			header ( 'Expires: 0' );
			header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header ( 'Pragma: public' );
			header ( 'Content-Length: ' . filesize ( $dfile ) );
			readfile ( $dfile );
			exit ();
		} // end if exists
	} // end if cnt
} // end if OrgID, FormID, and Type
?>