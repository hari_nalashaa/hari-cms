<?php
require_once 'public.inc';

if (in_array(PUBLIC_DIR, $whitelist) && defined('PUBLIC_DIR')) {
    require_once PUBLIC_DIR . 'IndexPageRequestVars.inc';
}

if ($feature ['InternalRequisitions'] == "Y") { 
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
  
    if($_REQUEST['InternalVCode'] == $InternalCode) { 
        $Internal   =   "Y";
        $_SESSION['P'][$OrgID.$MultiOrgID.$RequestID]['source'] = $InternalCode;
    }
    else {
        $Internal   =   '';
    }
}

$_SESSION['P'][$OrgID.$MultiOrgID.$RequestID]['LINK_HTTP_REFERER'] =   $_SERVER['HTTP_REFERER'];

$req_details_info       =   array();

if(isset($RequestID) && $RequestID != "") {
    $req_details_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("FormID, Active, DATE(ExpireDate) as ExpireDate", $OrgID, $RequestID);
    $dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);
}

if (isset($_REQUEST['FormID']) && $_REQUEST['FormID'] != "") {
    $FormID = $_REQUEST['FormID'];
}

//If FormID is empty, have to pull it based on RequestID
if($FormID == '') {
    $FormID = $req_details_info['FormID'];
}

if(isset($req_details_info['Active']) && $req_details_info['Active'] == 'N'
    || (isset($dates_diff) && $dates_diff > 0)) {
    header("Location:".PUBLIC_HOME."jobApplicationThankYou.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID.'&msg=reqexpired');
    exit();
}

if ($_SERVER['HTTP_REFERER'] == "https://www.glassdoor.com/") {
	header("Location:".PUBLIC_HOME."jobRequest.php?OrgID=".$OrgID."&RequestID=".$RequestID);
	exit();
}

require_once 'SetMonsterSessionCookies.inc';

$new_job_image = '';
$hot_job_image = '';
// update new job status
$highlight_req_settings		=	G::Obj('HighlightRequisitions')->getHighlightRequisitionSettings($OrgID);

if($highlight_req_settings['HighlightNewJob'] == "Yes") {
    $new_job_days = $highlight_req_settings['NewJobDays'];
    $new_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['NewJobIcon']);
    
    if($new_job_icon != "") {
        $new_job_image = '<img src="'.IRECRUIT_HOME.'/vault/highlight/'.$new_job_icon.'">';
    }
}

if($highlight_req_settings['HighlightHotJob'] == "Yes") {
    $hot_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['HotJobIcon']);
    
    if($hot_job_icon != "") {
        $hot_job_image = '<img src="'.IRECRUIT_HOME.'/vault/highlight/'.$hot_job_icon.'">';
    }
}

//Redirect it to make it back ward compatibility
require_once PUBLIC_DIR . 'BackwardCompatibilityRedirect.inc';

//Incldue Header
if (in_array(PUBLIC_DIR, $whitelist) && defined('PUBLIC_DIR')) {
    require_once PUBLIC_DIR . 'Header.inc';
}

if ($pg == "terms") {
    if (in_array(COMMON_DIR, $whitelist) && defined('COMMON_DIR')) {
        require_once COMMON_DIR . 'terms.inc';
    }
}
else if ($pg == "policy") {
    if (in_array(COMMON_DIR, $whitelist) && defined('COMMON_DIR')) {
        require_once COMMON_DIR . 'policy.inc';
    }
}
else { 
    //Get Organization Detail Information
    $org_detail_info    =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView");
    $listings_view      =   ($org_detail_info['ListingsView'] == '') ? 'initiallist' : $org_detail_info['ListingsView'];

    //Display Listings by listings view variable
    if (in_array(COMMON_DIR, $whitelist) && in_array($listings_view, $whitelist) && defined('COMMON_DIR')) {
        require_once COMMON_DIR . 'listings/' . $listings_view . '.inc';
    } 
}

//Include Footer
if (isset($whitelist) && in_array(PUBLIC_DIR, $whitelist) && defined('PUBLIC_DIR')) {
    require_once PUBLIC_DIR . 'Footer.inc';
}
?>
