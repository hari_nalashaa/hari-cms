<?php
//Validate Prescreen Questions Information
$prescreen_res_where    =   array("OrgID = :OrgID", "RequestID = :RequestID", "HoldID = :HoldID");
$prescreen_res_params   =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":HoldID"=>$HoldID);
$prescreen_res_results  =   G::Obj('PrescreenQuestions')->getPrescreenResults("*", $prescreen_res_where, "SortOrder", array($prescreen_res_params));
$prescreen_results      =   $prescreen_res_results['results'];

$prescreen_answers      =   array();
for($pr = 0; $pr < count($prescreen_results); $pr++) {
    $prescreen_answers[$prescreen_results[$pr]['SortOrder']] = $prescreen_results[$pr]['Submission'];
}

$ReqMultiOrgID      =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

if ($feature ['InternalRequisitions'] == "Y") {
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
}

//Set parameters for prepared query
$params             =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Set condition
$where              =   array("OrgID = :OrgID", "RequestID = :RequestID");

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
    $where[] = "Internal IN ('Y','B')";
} else {
    $where[] = "Internal IN ('N','B','')";
}

//Get Prescreen Screen Question Information
$pre_screen_que_info    =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation ("*", $where, "SortOrder", array($params));
$questionsavail         =   $pre_screen_que_info ["count"];

$prescreen_errors       =   "false";
if($questionsavail > 0) {
    
    // loop through all questions for a match. If one doesn't it will throw a not qualify
    if (is_array ( $pre_screen_que_info ['results'] )) {
        foreach ( $pre_screen_que_info ['results'] as $PRESCREEN ) {
            if ($PRESCREEN ['Answer'] != $prescreen_answers[$PRESCREEN['SortOrder']]) {
                $prescreen_errors = "true";
            }
        } // end foreach
    }

    if($prescreen_errors == "true") {
        $prescreen_link =   "prescreen.php?OrgID=".$OrgID;
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            $prescreen_link .=  "&MultiOrgID=".$MultiOrgID;
        }
        $prescreen_link .=  "&RequestID=".$_REQUEST['RequestID'];
        $prescreen_link .= "&InternalVCode=".$_REQUEST['InternalVCode'];
        $prescreen_link .=  "&HoldID=".$HoldID;
        if(isset($pg) && $pg != "") {
            $prescreen_link .=  "&pg=".$pg;
        }
        
        header("Location:".$prescreen_link);
        exit();
    }
}
?>