<?php
require_once 'public.inc';

$ORGS = array (
		'I20120601',
		'I20111012',
		'I20111015',
		'I20120601',
		'I20121101',
		'I20121210',
		'I20140708',
	        'I20140922',
		'I20141112' 
);
/*
I20111012	Community Residences, Inc. 
I20111015	Slay Industries 
I20120601	Signal 88 Security 
I20121101	LeadingAge 
I20121210	Orthman Manufacturing 
I20140708	Davis Street Community Center Incorporated 
I20140922	The Primary Health Network
I20141112	Cable & Wireless Holdings Inc.
// all else has new format (list by title)
*/

echo '<link rel="stylesheet" type="text/css" href="'.PUBLIC_HOME.'css/style.css">';

if (file_exists(PUBLIC_DIR . "images/".$OrgID."/style.css")) {
	echo '<link rel="stylesheet" type="text/css" href="'.PUBLIC_HOME.'images/'.$OrgID.'/style.css">';
} 

echo '<div id="iFrame">';

if (($OrgID == "I20130812") || ($OrgID == "I20110816")) { // Shepley Bulfinch, Neill-TSP

	//Set where condition
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Get OrganizationLevelData Information
	if ((isset($_GET['sl'])) && ($OrgID == "I20110816")) { // Neill-TSP
        $where   =   array_merge($where,array("SelectionOrder = :selectionorder"));
        $params  =   array_merge($params,array(":selectionorder"=>$_GET['sl']));
	}
	$results   =   $OrganizationsObj->getOrganizationLevelDataInfo("*", $where, 'OrgLevelID, SelectionOrder', array($params));
	$i         =   0;
	
	if(is_array($results['results'])) {
	
		foreach($results['results'] as $OLD) {
			
			$where   =   array(
                            "ROL.OrgID          =   R.OrgID", 
                            "ROL.RequestID      =   R.RequestID", 
                            "ROL.OrgID          =   :OrgID", 
                            "ROL.OrgLevelID     =   :OrgLevelID",
                            "ROL.SelectionOrder =   :SelectionOrder",
                            "NOW() BETWEEN R.PostDate AND R.ExpireDate",
			    "(R.PresentOn = 'PUBLICONLY' OR R.PresentOn = 'INTERNALANDPUBLIC')",
                            "R.Active           =   'Y'"
        			         );
			$params  =   array(":OrgID"=>$OrgID, ":OrgLevelID"=>$OLD ['OrgLevelID'], ":SelectionOrder"=>$OLD ['SelectionOrder']);
			$resultsIN = $RequisitionsObj->getReqAndReqOrgLevelsInfo("*", $where, "", "R.Title", array($params));
			
			if(is_array($resultsIN['results'])) {
				foreach($resultsIN['results'] as $ROL) {

				    $STATUSLEVELS   =   array ();
				    //Get Default RequisitionFormID
				    $req_det_info   =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
				    $STATUSLEVELS   =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
				    
					if ($hold != $OLD ['CategorySelection']) {
						$i ++;
						if ($i > 1) {
							echo '<br>';
						}
						echo '<strong style="color:#555555;">' . $OLD ['CategorySelection'] . "</strong><br>\n";
					}
						
					echo '<a href="' . PUBLIC_HOME . 'jobRequest.php?OrgID=' . $OrgID . '&RequestID=' . $ROL ['RequestID'] . '" target="_blank">' . $ROL ['Title'] . '</a>';
					echo '&nbsp;&nbsp;(' . $STATUSLEVELS [$ROL ['EmpStatusID']] . ")";
					echo "<br>\n";
						
					$hold = $OLD ['CategorySelection'];
				}
			}
			
		} // end foreach
	}	
} else if (in_array ( $OrgID, $ORGS )) {
	
	//Set where condition
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Get Organization Data Information
	$results   =   $OrganizationsObj->getOrgDataInfo("*", $where, '', array($params));
	$ck        =   $results['count'];
	
	if ($ck > 0) {
		
		if ($count == "Y") {
			
			//Set where condition
			$where = array("R.OrgID = ROL.OrgID", "R.RequestID = ROL.RequestID", "R.OrgID = :OrgID", "R.Active = 'Y'");
			//Set parameters
			$params = array(":OrgID"=>$OrgID);
			
			if ($olcat) {
				$ol = $olcat;
				$where[] = "ROL.OrgLevelID = $ol";
			}
			
			$where[] = "NOW() BETWEEN R.PostDate AND R.ExpireDate";
			$where[] = "(R.PresentOn = 'PUBLICONLY' OR R.PresentOn = 'INTERNALANDPUBLIC')";
			
			//Get Requisitions and Organization Levels Information
			$results = $RequisitionsObj->getReqAndReqOrgLevelsInfo("ROL.*, count(*) cnt", $where, "OrgLevelID, SelectionOrder", "", array($params));
			$active = $results['count'];
			
			if ($active > 0) {
				if ($label != "N") {
					echo '<a href="' . PUBLIC_HOME . 'index.php?';
					echo 'OrgID=' . $OrgID;
					if ($MultiOrgID != "") {
						echo '&MultiOrgID=' . $MultiOrgID;
					} 
					echo '&olnew=';
					echo '&slnew=';
					echo '" target="_blank">';
					echo 'There are ';
					echo $active;
					echo ' open positions';
					echo '</a>';
				} else {
					echo $active;
				}
			} else {
				if ($label != "N") {
					echo 'There are currently no open positions';
				} else {
					echo '0';
				}
			}
		} else {
			
			$Public = "Y";
			$IFRAME = "Y";
			$pg="";
			require_once COMMON_DIR . 'listings/listings.inc';
		} // end count
	} // end ck
		  
	// all others
} else { // else ifs
	
	//Set where condition
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Get OrganizationData Information
	$results   =   $OrganizationsObj->getOrgDataInfo("*", $where, '', array($params));
	$ck        =   $results['count'];
	
	if ($ck > 0) {
		
		$ORGLEVELS = array ();
		//Get Organization Levels Information
		$results = $OrganizationsObj->getOrganizationLevelsInfo("*", $where, '', array($params));
		if(is_array($results['results'])) {
			foreach($results['results'] as $OL) {
				$ORGLEVELS [$OL ['OrgLevelID']] = $OL ['OrganizationLevel'];
			} // end foreach
		}
		
		
		$ORGLEVELDATA = array ();
		$results = $OrganizationsObj->getOrganizationLevelDataInfo("*", $where, '', array($params));
		if(is_array($results['results'])) {
			foreach($results['results'] as $OL) {
				$ORGLEVELDATA [$OL ['OrgLevelID']] [$OL ['SelectionOrder']] = $OL ['CategorySelection'];
			} // end foreach
		}
		
		//Set where condition
		$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "Active = 'Y'");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		$where[] = "NOW() BETWEEN PostDate AND ExpireDate";
		$where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";

		//Get Requisition Information
		$resultsIN = $RequisitionsObj->getRequisitionInformation("*", $where, "", "Title", array($params));
		
		if(is_array($resultsIN['results'])) {
			foreach ( $resultsIN['results'] as $REQ ) {
					
			    $STATUSLEVELS   =   array ();
			    //Get Default RequisitionFormID
			    $req_det_info   =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
			    $STATUSLEVELS   =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
			     
				echo '<a href="' . PUBLIC_HOME . 'jobRequest.php?OrgID=' . $OrgID . '&RequestID=' . $REQ ['RequestID'] . '" target="_blank">' . $REQ ['Title'] . '</a>';
					
				if ($REQ ['EmpStatusID']) {
					echo '&nbsp;(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
				}
				
				//Set where condition
				$where = array("OrgID = :OrgID", "RequestID = :RequestID");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":RequestID"=>$REQ ['RequestID']);
				//Get Requisition Organization Levels Information
				$resultsOL = $RequisitionsObj->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID, SelectionOrder", array($params));
				$hitOL = $resultsOL['count'];
					
				$i = 0;
				echo '<br><div class="OL">' . $ORGLEVELS [1] . ': ';
			
				if(is_array($resultsOL['results'])) {
					foreach($resultsOL['results'] as $ROL) {
						if ($ROL ['OrgLevelID'] == 1) {
							if ($i > 0) {
								echo ', ';
							}
							echo ' ' . $ORGLEVELDATA [$ROL ['OrgLevelID']] [$ROL ['SelectionOrder']];
							$i ++;
						}
					} // end foreach
				}
				echo "<br>\n";
				echo "</div>\n";
				echo "<br>\n";
					
			} // end foreach
		}
		
	} // end ck
} // end ifs

echo '</div>';
?>
