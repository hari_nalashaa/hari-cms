<?php
require_once 'public.inc';

$OrgID          =   isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$MultiOrgID     =   isset($_REQUEST['MultiOrgID']) ? $_REQUEST['MultiOrgID'] : '';
$RequestID      =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$ApplicationID  =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$HoldID         =   isset($_REQUEST['HoldID']) ? $_REQUEST['HoldID'] : '';
$FormID         =   isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';

if(isset($_REQUEST) && $_REQUEST['Process'] == "Yes") {
    if($RequestID != "" && $FormID != "" && $HoldID != "" && $OrgID != "") {
        require_once PUBLIC_DIR . 'ProcessPublicApplicationThankYou.inc';
    }
    else {
        $to = "dedgecomb@irecruit-software.com";
        $subject = "Alert - Issue with the Application";
        $txt = "OrgID: " . $OrgID . "\r\n";
        $txt .= "RequestID: " . $RequestID . "\r\n";
        $txt .= "HoldID: " . $HoldID . "\r\n";
        $txt .= "MultiOrgID: " . $MultiOrgID . "\r\n";
        $txt .= "FormID: " . $FormID . "\r\n";
        $txt .= "Request Information: " . print_r($_REQUEST, true) . "\r\n";
        $txt .= "Server Information: " . print_r($_SERVER, true) . "\r\n";
        
	$headers = "From: info@irecruit-us.com" . "\r\n";
        
        mail($to,$subject,$txt,$headers);

    	$link = "jobApplicationThankYou.php?";
    	$link .= "OrgID=" . $OrgID;
    	if ($MultiOrgID != "") {
    	  $link .= "&MultiOrgID=" . $MultiOrgID;
    	}
    	$link .= "&msg=issue_req";
        
        header("Location:" . $link);
        exit();
    }
}

//Get Requisition information based on RequestID //Set where condition for requisition
$req_info   =   G::Obj('Requisitions')->getReqDetailInfo("Title", $OrgID, $MultiOrgID, $RequestID);

$title      =   "Thank you for applying for this postion";

if (FROM_SRC == "PUBLIC") {
  require_once PUBLIC_DIR . 'Header.inc';
  $LISTINGS_URL = strtolower(PUBLIC_HOME);
}

if(isset($_GET['msg']) && $_GET['msg'] == 'reqexpired') {
    ?>
	<p style="text-align: center;font-size: 15px;">
	<br><br><br>
	This requisition has expired and is no longer active.
    </p>
    <?php
}
if(isset($_GET['msg']) && $_GET['msg'] == 'issue_req') {
    ?>
	<p style="text-align: center;font-size: 15px;">
	<br><br><br>
	Theres is an issue while processing the application. Please resubmit.
    </p>
    <?php
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'duplicate') {
    ?>
	<p style="text-align: center;font-size: 15px;">
    	<br><br><br>
    	<?php
        $DUPERROR   =   "An application with this information has recently been submitted.<br>";
        $DUPERROR   .=  "Please check your email for confirmation before submitting again.";
        echo $DUPERROR;
        ?>
    </p>
    <?php
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
    unset($_SESSION['P'][$OrgID.$MultiOrgID.$RequestID]['source']);
    require_once PUBLIC_DIR . 'ThankYou.inc';
}

if (FROM_SRC == "PUBLIC") {
  require_once PUBLIC_DIR . 'Footer.inc';
}
?>
