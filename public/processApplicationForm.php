<?php
require_once 'public.inc';

$OrgID          =   $_REQUEST['OrgID'];
$MultiOrgID     =   $_REQUEST['MultiOrgID'];
$FormID         =   $_REQUEST['FormID'];
$RequestID      =   $_REQUEST['RequestID'];
$SectionID      =   $_REQUEST['SectionID'];
$HoldID         =   $_REQUEST['HoldID'];

$processed_sections     =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "ProcessedSections");

if($processed_sections  == NULL || $processed_sections == "" || empty($processed_sections) || !isset($processed_sections)) {
    $processed_sections =   array();
}
else {
    $processed_sections =   unserialize($processed_sections);
}

//Get Request and Files Data
$SERIALIZE_REQUEST_DATA =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "REQUEST_DATA");
$SERIALIZE_FILES_DATA   =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "FILES_DATA");

//Set Request Data, Files Data
$REQUEST                =   unserialize($SERIALIZE_REQUEST_DATA);
$FILES                  =   unserialize($SERIALIZE_FILES_DATA);

if($REQUEST == ""
    || !isset($REQUEST)
    || empty($REQUEST)
    || $REQUEST == NULL) {
    $REQUEST = array();
}
    
if($FILES == ""
    || !isset($FILES)
    || empty($FILES)
    || $FILES == NULL) {
    $FILES = array();
}
    
$REQUEST        =   array_merge($REQUEST, $_REQUEST);
$FILES          =   array_merge($FILES, $_FILES);

//Insert entire request data to database to reproduce validation.
// Bind Parameters
$params =   array('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'QuestionID'=>"REQUEST_DATA", 'Answer'=>serialize($REQUEST));
// Insert applicant data temp
G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params);

// Bind Parameters
$params =   array('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'QuestionID'=>"FILES_DATA", 'Answer'=>serialize($FILES));
// Insert applicant data temp
G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params);

//Validate the information
$REQUEST_DATA   =   $REQUEST;
$FILES_DATA     =   $FILES;

if (isset($whitelist) 
    && in_array(PUBLIC_DIR, $whitelist)
    && defined('PUBLIC_DIR')
    && file_exists(PUBLIC_DIR . 'ValidateRequired.inc')) {
    require_once PUBLIC_DIR . 'ValidateRequired.inc';
}

if($_REQUEST['SectionID'] == '6') {
    if(count($errors_list) == 0) {

        if (isset($whitelist) 
            && in_array(PUBLIC_DIR, $whitelist)
            && defined('PUBLIC_DIR')
            && file_exists(PUBLIC_DIR . 'ProcessApplicationAttachmentsTemp.inc')) {
            require_once PUBLIC_DIR . 'ProcessApplicationAttachmentsTemp.inc';
        }
        
        if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
            $processed_sections[]   =   $_REQUEST['SectionID'];
            $info = array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($info);
        }
        
        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
    else {
        echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
        exit;
    }
}
else if($_REQUEST['SectionID'] == '14') {

    if ($_FILES ['applicant_profile_picture'] ['tmp_name'] != "" && count($errors_list) == 0) {
    
        // FILEHANDLING
        $dir = IRECRUIT_DIR . 'vault';

        if (! file_exists ( $dir )) {
            mkdir ( $dir, 0700 );
            chmod ( $dir, 0777 );
        }

        // FILEHANDLING
        $dir = $dir . '/' . $OrgID;
        
        if (! file_exists ( $dir )) {
            mkdir ( $dir, 0700 );
            chmod ( $dir, 0777 );
        }
    
        $app_picture_dir = $dir . '/applicant_picture';
    
        if (! file_exists ( $app_picture_dir )) {
            mkdir ( $app_picture_dir, 0700 );
            chmod ( $app_picture_dir, 0777 );
        }
    
        if ($_FILES ['applicant_profile_picture'] ['name'] != "") {
            	
            $data       =   file_get_contents ( $_FILES ['applicant_profile_picture'] ['tmp_name'] );
            $e          =   explode ( '.', $_FILES ['applicant_profile_picture'] ['name'] );
            $ecnt       =   count ( $e ) - 1;
            $ext        =   $e [$ecnt];
            $ext        =   preg_replace ( "/\s/i", '', $ext );
            $ext        =   substr ( $ext, 0, 5 );
    
            $filename   =   $app_picture_dir . '/' . $HoldID . "*" . $_REQUEST['RequestID'] . '-ApplicantPicture.' . $ext;
            $fh         =   fopen ( $filename, 'w' );
            fwrite ( $fh, $data );
            fclose ( $fh );
    
            chmod ( $filename, 0666 );
    
            $ProfileAnswer = $HoldID . "*" . $_REQUEST['RequestID'] . '-ApplicantPicture.'.$ext;
            
            if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
                $processed_sections[]   =   $_REQUEST['SectionID'];
                $info = array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp($info);
            }
            
            $info = array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "SectionID"=>$_REQUEST['SectionID'], "QuestionID"=>"ApplicantPicture", "Answer"=>$ProfileAnswer);
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($info);
            
            echo json_encode(array("errors_list"=>array(), "success"=>"true"));
            exit;
        }
    }
    else if ($_FILES ['applicant_profile_picture'] ['tmp_name'] == "" && count($errors_list) == 0) {
        
        if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
            $processed_sections[]   =   $_REQUEST['SectionID'];
            $info = array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($info);
        }
        
        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
    else {
        echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
        exit;
    }
}
else {

    if(count($errors_list) == 0) {

        /**
         * This is a temporary code until the final upgrade.
         * Upto that time, we have to keep on adding different conditions to it.
         */
        $GetFormPostAnswerObj->POST =   $_POST;    //Set Post Data

        $json_form_que_list    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID, $_REQUEST['SectionID']);
        $form_que_list         =   $json_form_que_list['json_ques'];
        
        foreach($form_que_list as $QuestionID=>$QI) {
            if($QI['QuestionTypeID'] != 99
                && $QI['QuestionTypeID'] != 8) {
                G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
                $QI['HoldID']       =   $HoldID;
                $QI['Answer']       =   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                if($QI['value'] == "{Date}") $QI['Answer'] = "NOW()";
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
            }
        }

        //Insert Other Questions To TempData
        $other_ques_list        =   array(
                                        'FormID'                    =>  array("Answer"=>$_REQUEST['FormID']),
                                        'RequestID'                 =>  array("Answer"=>$_REQUEST['RequestID']),
                                        'source'                    =>  array("Answer"=>$_SESSION['P'][$OrgID.$MultiOrgID.$RequestID]['source']),
                                        'pg'                        =>  array("Answer"=>$_POST['pg']),
                                        'subpg'                     =>  array("Answer"=>$_POST['subpg']),
                                        'ApplicationSourceInternal' =>  array("Answer"=>$_POST['ApplicationSourceInternal']),
                                        'ClientIPAddress'           =>  array("Answer"=>$_POST['ClientIPAddress']),
                                        'HTTP_USER_AGENT'           =>  array("Answer"=>$_POST['HTTP_USER_AGENT']),
                                        'REMOTE_ADDR'               =>  array("Answer"=>$_POST['REMOTE_ADDR']),
                                        'REQUEST_URI'               =>  array("Answer"=>$_POST['REQUEST_URI']),
                                        'HTTP_REFERER'              =>  array("Answer"=>$_POST['HTTP_REFERER']),
                                        'MAX_FILE_SIZE'             =>  array("Answer"=>$_POST['MAX_FILE_SIZE']),
                                        'HoldID'                    =>  array("Answer"=>$HoldID),
                                    );
        //Insert Other Questions
        foreach ($other_ques_list as $other_que_id=>$oqa) {
            $QI     =   array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "QuestionID"=>$other_que_id, "Answer"=>$oqa["Answer"]);
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
            $processed_sections[]   =   $_REQUEST['SectionID'];
            // Bind Parameters
            $QI = array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
            //Insert temporary data
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
    
        $processed_sections     =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "ProcessedSections");
        $un_processed_sections  =   unserialize($processed_sections);
        
        $public_sec_list        =   array();
        for($ui = 0; $ui < count($publicportal_sections_list); $ui++) {
            $section_id         =   $publicportal_sections_list[$ui]['SectionID'];
            $public_sec_list[]  =   $section_id;
        }

        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
    else {
        echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
        exit;
    }	
}
?>
