<?php
require_once 'public.inc';

require_once COMMON_DIR . 'color.inc';

$file_names             =   array();
$res_attachments_info   =   G::Obj('ApplicantAttachmentsTemp')->getApplicantAttachmentsTempInfo($OrgID, $_REQUEST['HoldID']);
$attachments_info       =   $res_attachments_info['results'];

$purpose                =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $_REQUEST['FormID']);

for($ka = 0; $ka < count($attachments_info); $ka++) {
    $file_names[$attachments_info[$ka]['TypeAttachment']] = IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $_REQUEST['HoldID'] . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
}

$dfile                  =   is_null($file_names[$_REQUEST['attachment_type']]) ? '' : $file_names[$_REQUEST['attachment_type']];

if (file_exists ( $dfile )) {
	header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($dfile));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($dfile));
    readfile($dfile);
    exit();
}
?>