<?php
require_once 'userportal.inc';

include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';

$FormID = $_REQUEST['FormID'];

//Get Applicant attachments information
$attachment_types       =   array();
$attachments_info       =   $UserPortalInfoObj->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[] =   $attachments_info[$aai]['TypeAttachment'];
}

require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';

$next_section_id = "";
for($uic = 0; $uic < count($userportal_sections_list); $uic++) {
    if($userportal_sections_list[$uic]['SectionID'] == '6') {
        $next_section_id = $userportal_sections_list[$uic+1]['SectionID'];
    }
}
?>
<form name="<?php echo $section_forms[$_REQUEST['SectionID']];?>" id="<?php echo $section_forms[$_REQUEST['SectionID']];?>" method="post" enctype="mutipart/form-data">
    <?php
        $section_id         =   $_REQUEST['SectionID'];
        $application_form   =   include COMMON_DIR . "application/ApplicationSectionInfo.inc";
        echo $application_form;
    ?>
    <br>
    <input type="hidden" name="SectionID" id="SectionID" value="<?php echo htmlspecialchars($_REQUEST['SectionID']);?>">
    <input type="hidden" name="RequestID" id="RequestID" value="<?php echo htmlspecialchars($_REQUEST['RequestID']);?>">
    <input type="hidden" name="FormID" id="FormID" value="<?php echo htmlspecialchars($FormID);?>">
    <input type="hidden" name="process" id="process" value="Y">
    <input type="button" name="btnInfo" id="btnInfo" value="Save/Next" onclick="processApplicationForm('<?php echo htmlspecialchars($section_forms[$_REQUEST['SectionID']]);?>', 'section_id<?php echo htmlspecialchars($_REQUEST['SectionID']);?>', '<?php echo htmlspecialchars($next_section_id);?>');">
</form>
