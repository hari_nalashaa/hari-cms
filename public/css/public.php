<?php
header("Content-type: text/css"); 
require_once '../public.inc';
?>
/* Generic Selectors */
 
body {
   margin: 0px;
   width:100%;
   font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
   font-size: 12px;
   background-color: #<?php echo $COLOR['Light']?>;
}
 
p {
<?php
if ($OrgID == "I20110901") {
echo '   font-size: 14px;';
} else {
echo '   font-size: 12px;';
}
?>
}

table {
<?php
if ($OrgID == "I20110901") {
echo '   font-size: 14px;';
} else {
echo '   font-size: 12px;';
}
?>
}
 
/**************** Pseudo classes ****************/

<?php

if ($OrgID == "I20110801") {

echo <<<END
a:link {
   color: blue;
   text-decoration: underline;
}
 
a:visited {
   color: blue;
   text-decoration: underline;
}
 
a:hover {
   color: blue;
   text-decoration: underline;
}

a:active {
   color: #555555;
   font-style: normal;
}
END;

} else {

echo <<<END
a:link {
   color: #555555;
   text-decoration: none;
}
 
a:visited {
   color: #555555;
   text-decoration: none;
}
 
a:hover {
   color: #555555;
   text-decoration: underline;
   font-weight: bold;
   font-style: normal;
}

a:active {
   color: #555555;
   font-weight: bold;
   font-style: normal;
}
END;

} // end else OrgID

?>
.title  {
   font-weight: bold;
   font-size: 14px;
   line-height: 200%;
}


/************************* ID's *************************/
#register {
   float:left;
   width:160px;
   padding:5px;
   position:relative;
   margin-right:10px;
}
#register .text {
   min-height: 100px;
}
#login {
   float:left;
   width:160px;
   padding:5px;
   position:relative;
   margin-right:10px;
}
#login .text {
   min-height: 100px;
}
#apply {
   float:left;
   width:160px;
   padding:5px;
   position:relative;
}
#apply .text {
   min-height: 100px;
}

#topDoc {
   padding: 10px 0px 0px 25px; /*top right bottom left*/
   background-color: #FFFFFF;
   border-bottom: 4px solid #<?php echo $COLOR['Heavy']?>;
}

#page-wrapper {
	padding: 0 15px;
	min-height: 568px;
	background-color: #<?php echo $COLOR['LightLight']?>;
}

@media ( min-width :768px) {
	#page-wrapper {
		background-color: #<?php echo $COLOR['LightLight']?>;
		border-bottom: 1px solid #<?php echo $COLOR['LightLight']?>;
		border-left: 1px solid #<?php echo $COLOR['LightLight']?>;
		padding: 20px;
		position: inherit;
		border-left: 1px solid #<?php echo $COLOR['LightLight']?>;
	}
}


#bottomDoc {
   clear: both;
   width: 100%;
   padding: 5px 0 5px 0; /*top right bottom left*/
   font-size: 11px;
   background-color: #<?php echo $COLOR['Light']?>;
}

#bottomDoc p {
   text-align: center;
   color: #000000;
}

#bottomDoc a {
   color: #000000;
}
#loading-div-background {
        display:none;
        position:fixed;
        top:0;
        left:0;
	background:#ffffff;
        width:100%;
        height:100%;
}

#loading-div {
         width: 100px;
         height: 100px;
         text-align:center;
         position:absolute;
         left: 50%;
         top: 50%;
         margin-left:-100px;
         margin-top: -300px;
}

<?php 
// turned off for merge 3/9/2018  The Primary Health Network
if ($OrgID == "I20140922_off") { 
?>
#topDoc {
display:none;
}
#page-wrapper div p {
display:none;
}
hr {
display:none;
}
.menu {
font-family: 'Roboto', sans-serif;
font-weight:500;
background-color: #00817d;
color: white;
padding: 10px;
}
.menuchoice select {
font-family: 'Roboto', sans-serif;
font-weight:500;
border: 1px solid #00817d;
color: #00817d;
padding: 10px;
}
.menuselect select {
font-family: 'Roboto', sans-serif;
font-weight:500;
border: 1px solid #00817d;
color: #00817d;
padding: 10px;
}
<?php } ?>

