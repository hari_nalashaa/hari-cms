<?php
// FILEHANDLING
$dir        =   IRECRUIT_DIR . 'vault';

if (! file_exists ( $dir )) {
    mkdir ( $dir, 0700 );
    chmod ( $dir, 0777 );
}

// FILEHANDLING
$dir        =   $dir . '/' . $OrgID;

if (! file_exists ( $dir )) {
    mkdir ( $dir, 0700 );
    chmod ( $dir, 0777 );
}

$apatdir    =   $dir . '/applicantattachments';

if (! file_exists ( $apatdir )) {
	mkdir ( $apatdir, 0700 );
	chmod ( $apatdir, 0777 );
}

$purpose    =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

//Set parameters
$params     =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID);
// Set Form Question Fields
$where      =   array ("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID = 8", "Active = 'Y'");
//Get FormQuestions Information
$resultsIN  =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder", array ($params) );

if (is_array ( $resultsIN ['results'] )) {
	foreach ( $resultsIN ['results'] as $FIL ) {
		
        if ($_FILES [$FIL ['QuestionID']] ['type'] != "") {

            $path_info  =   pathinfo($_FILES [$FIL ['QuestionID']] ['name']);
            $extension  =   $path_info['extension'];
			
			//Applicant Attachments Information
			$applicant_attach_info   =   array(
                            				"OrgID"            =>  $OrgID,
                                            "MultiOrgID"       =>  $MultiOrgID,
                                            "HoldID"           =>  $HoldID,
                            		        "SectionID"        =>  $_REQUEST['SectionID'],
                            		        "RequestID"        =>  $_REQUEST['RequestID'],
                            				"TypeAttachment"   =>  $FIL ['QuestionID'],
                            				"PurposeName"      =>  $purpose [$FIL ['QuestionID']],
                            			    "FileType"         =>  $extension
                            			);
			//Insert applicant attachments information
			G::Obj('ApplicantAttachmentsTemp')->insApplicantAttachmentsTemp ( $applicant_attach_info );
			
            $filename   =   $apatdir . '/' . $HoldID . '-' . $purpose [$FIL ['QuestionID']] . '.' . $extension;
            $tmp_name   =   $_FILES [$FIL ['QuestionID']] ['tmp_name'];
			move_uploaded_file($tmp_name, $filename);

			chmod ( $filename, 0666 );
		}

	} // end foreach
} 
// END FILEHANDLING
?>