<script>
var validate_dates_info	=	'<?php echo $validate_dates_info;?>';
validate_dates_info		=	JSON.parse(validate_dates_info);

var date_split_ids		=	lst.split(",");
var date_objs			=	new Array();

var i			=	0;
var date_id		=	"";
var year_range 	=	"";
for(date_id_key in date_split_ids) {
	date_id		=	date_split_ids[i];
	date_id		=	$.trim(date_id);
	year_range	=	getYearRange(validate_dates_info, date_id);
	date_picker(date_id, 'mm/dd/yy', year_range, '');
	i++;
}

var OrgID      =   '<?php echo $OrgID;?>';
var MultiOrgID =   '<?php echo $MultiOrgID;?>';
var RequestID  =   '<?php echo $MRequestID;?>';

function monster_request_on_continue() {
    location.href = 'thankyou.php?OrgID='+OrgID+'&MultiOrgID='+MultiOrgID+'&RequestID='+RequestID;
}
</script>

<?php
if ($OrgIDck != "" || $MultiOrgIDck != "") {
	require_once 'skins/FooterCustom.inc';
} else {
	require_once 'skins/FooterPublic.inc';
}

if (DEVELOPMENT != "Y") {
	echo <<<END
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-25955565-2']);
    _gaq.push(['_trackPageview']);
    
    (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
END;
}
