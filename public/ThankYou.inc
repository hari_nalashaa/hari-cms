<?php
//get answer from applicant data based on question_id email
$Email		        =	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'email');

$requisition_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID", $OrgID, $RequestID);
$FormID             =   $requisition_info['FormID'];
$JobTitle           =   $requisition_info['Title'];

//Get Wotc Ids list
$wotcid_info        =   G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $MultiOrgID);
//Get Text From TextBlocks 
$Response           =	G::Obj('FormFeatures')->getTextFromTextBlocks($OrgID, $FormID, 'Response');

echo '<style>
.modal {
  display: none;
  position: fixed;
  z-index: 9999;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.4);
}
.modal-content {
  background-color: #fefefe;
  margin: 15% auto;
  padding: 20px;
  border: 1px solid #888;
  width: 90%;
}
.user_portal_modal_close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}
.user_portal_modal_close:hover,
.user_portal_modal_close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
</style>
';

echo '<script>';
echo 'function showPopUp() {';
echo 'document.getElementById("user_portal_modal").style.display = "block";';
echo '}';

echo 'function closePopUp() {';
echo 'document.getElementById("user_portal_modal").style.display = "none";';
echo '}';
echo '</script>';

echo '<table border="0" cellspacing="0" cellpadding="10" width="100%" class="table table-striped table-bordered">';
echo '<tr><td>';

echo '<p>';
echo 'You have applied for the following position: '.$JobTitle;
echo '</p>';

echo '<p>Your Application ID is: <b>' . $ApplicationID . '</b><br><br>';
if (! preg_match ( '/addApplicant.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	echo 'Confirmation email will be sent to: <b>' . $Email . '</b></p>';
	echo '<p>' . $Response . '</p>';
}
echo '</td></tr>';
echo '</table>';

if($wotcid_info['wotcID'] != "") {
	echo '<div class="row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	
	//WOTC Form Popup Code
	echo '<a href="javascript:void(0);" onclick="showPopUp()">';
	echo 'There is a final form to submit. Please click here to submit the WOTC Form';
	echo '</a>';
	
	echo '<br><br><br><br>';
	
	echo '</div>';
	echo '</div>';
}

$IrecruitApplicationID 	=	$ApplicationID;
$IrecruitRequestID 		=	$RequestID;

if($wotcid_info['wotcID'] != "" 
	&& $is_internal_req != "Yes") {
	?>

<div id="user_portal_modal" class="modal row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="modal-content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<span class="user_portal_modal_close" onclick="closePopUp()">close &times;</span>
				</div>
			</div>		
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12" id="div_wotc_form"></div>
			</div>
		</div>
	</div>
</div>

	<script type="text/javascript">
	function loadWOTCForm(OrgID, MultiOrgID, IrecruitApplicationID, IrecruitRequestID) {
		showPopUp();
	
		var request = $.ajax({
			method: "POST",
	  		url: public_home + "wotcForm.php?OrgID="+OrgID+"&MultiOrgID="+MultiOrgID+"&IrecruitRequestID="+IrecruitRequestID+"&IrecruitApplicationID="+IrecruitApplicationID,
			type: "POST",
			beforeSend: function() {
				$("#div_wotc_form").html('Please wait.. <img src="images/loading-small.jpg"/><br><br>');
			},
			success: function(data) {
				$("#div_wotc_form").html(data);
	    	}
		});
	
	}
	
	loadWOTCForm('<?php echo $OrgID;?>', '<?php echo $MultiOrgID;?>', '<?php echo $IrecruitApplicationID;?>', '<?php echo $IrecruitRequestID;?>');
	</script>
	<?php
}
?>
