<?php
include PUBLIC_DIR . 'FilterPublicPortalSections.inc';
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';

$formtable          =   "FormQuestions";

$req_columns        =   "InternalFormID, Title, FormID, Active, DATE(ExpireDate) as ExpireDate";
$req_details        =   G::Obj('Requisitions')->getRequisitionsDetailInfo($req_columns, $OrgID, $RequestID);
$dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details['ExpireDate']);
$FormID             =   $req_details[$typeform];

$form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

foreach ( $form_sections_info as $form_section_id=>$form_section_info) {
    $TITLES [$form_section_id] = $form_section_info['SectionTitle'];
}

if ($OrgID == 'I20090304') {
	echo '<style type="text/css">
			body { 
  				font-size:12pt; 
			}
		  </style>';
}


$personal_ques_info =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, "1");
$child_ques_info    =   G::Obj('ApplicationFormQuestions')->getFormChildQuesForParentQues($OrgID, $FormID, "1");

$que_types_list     =   array();
if(is_array($personal_ques_info)) {
    foreach($personal_ques_info as $personal_que_id=>$personal_que_info) {
        $que_types_list[$personal_que_id]   =   $personal_que_info['QuestionTypeID'];
    }
}

// Query and Set Text Elements
$params		=	array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
//Get text blocks information
$where		=	array("OrgID = :OrgID", "FormID = :FormID");
$results	=	G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
	}
}

if($InternalVCode != "") {
    $go_back_link   =   "internalRequisitions.php?OrgID=".$OrgID;
}
else {
    $go_back_link   =   "index.php?OrgID=".$OrgID;
}

if($MultiOrgID != "") {
    $go_back_link    .=  "&MultiOrgID=".$MultiOrgID;
}
if($InternalVCode != "") {
    $go_back_link    .=  "&InternalVCode=".$InternalVCode;
}

echo '<div>';

if($InternalVCode != "") {
    echo 'Employee Application: ';
}
echo '<a href="'.$go_back_link.'">Back to Category List</a><br><br>';

echo '</div>';

if ($TextBlocks ["FormHeader"]) {
	echo $TextBlocks ["FormHeader"];
}

$OrganizationListing = include COMMON_DIR . 'application/OrganizationListings.inc';
if ($OrganizationListing) {
	echo $OrganizationListing;
}

//Set Parsing View Option
if ($feature['ResumeParsing'] == "Y") {
    $resumeparse    =   G::Obj('Sovren')->uploadResumeForm($OrgID, $HoldID);
    
    if (is_array($resumeparse)) {
        $APPDATA    =   $resumeparse;
    } else {
        echo $resumeparse;
    }
} // end feature

include PUBLIC_DIR . 'FilterPublicPortalSections.inc';

if($ERROR != "") {
    echo '<div class="row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12" style="color:red">';
    echo 'Please resolve the issues as shown below. <span onclick="show_hide_errors()" style="cursor:pointer;color:blue;">Click here to See/Hide the details.</span><br><br>';
    echo '</div>';
    echo '</div>';
    
    echo '<div class="row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12" style="color:red;display:none;" id="errors_list">';
    echo str_replace(array("\\n"), "<br>", $ERROR);
    echo '</div>';
    echo '</div>';
}

$processed_sections     =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "ProcessedSections");
if($processed_sections == '') $processed_sections = serialize(array());
$processed_sections     =   unserialize($processed_sections);
?>
<div>
    <h4><?php echo $req_details['Title'];?>:</h4>
    <span style="color:red">*</span>&nbsp;Please click on each section title and complete each section. Click on the Save/Next button to go to the next section. Click on the Sign Application button after all the sections are completed. All sections with required questions must be completed before the application can be submitted.
    <br><br>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['CompletedTab'];?>;color: <?php echo "#".$up_app_theme_info['CompletedTab'];?>;"></div> 
    <div style="float:left;">&nbsp;Completed Section&nbsp;</div>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['PendingTab'];?>;color: <?php echo "#".$up_app_theme_info['PendingTab'];?>;"></div>
    <div style="float:left;">&nbsp;Pending Section&nbsp;</div>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['ActiveTab'];?>;color: <?php echo "#".$up_app_theme_info['ActiveTab'];?>;"></div> 
    <div style="float:left;">&nbsp;Active Section&nbsp;</div>
    <div style="clear: both"></div>
</div>
<br><br>
<ul class="public_application_steps" id="public_application_steps">
    <?php
    foreach ($publicportal_sections_list as $section_id=>$section_info)
    {
    	$section_title = $section_info['SectionTitle'];
    	$tab_class = 'public_application_step_tabs';
    	
    	if(in_array($section_id, $processed_sections)) {
    	    $tab_class = 'public_application_step_tabs_filled';
    	}
    	
        ?>
        <li>
            <a href="javascript:void(0);" class="<?php echo $tab_class;?>" id="section_id<?php echo $section_id;?>">
                <?php
                echo $section_title;
                ?>
                </i>
            </a>
        </li>
        <?php
    }
    ?>
</ul>

<div class="public_application_process">
    <?php
    $uic        =   0;
    $keys       =   array_keys($publicportal_sections_list);
    foreach($publicportal_sections_list as $section_id=>$section_info) {
        $section_title      =   $publicportal_sections_list[$section_id]['SectionTitle'];
        $next_section_id    =   "";
        
        if(($uic+1) != count($publicportal_sections_list)) {
            $next_section_id    =   $keys[array_search($section_id, $keys) + 1];
        }
        ?>
        <div class="public_application_process_content" id="section_content<?php echo $section_id;?>">
            <form name="<?php echo $section_forms[$section_id];?>" id="<?php echo $section_forms[$section_id];?>" method="POST" enctype="mutipart/form-data">
            <?php
            if($section_id == 6) {
                $application_form = include COMMON_DIR . "application/PublicAttachments.inc";
                echo $application_form;
            }
            else if($section_id == 14) {
                $application_form = include COMMON_DIR . "application/ApplicantProfilePicture.inc";
                echo $application_form;
            }
            else {
                $application_form = include COMMON_DIR . "application/ApplicationSectionInfo.inc";
                echo $application_form;
            }
            ?>
            <input type="hidden" name="pg" value="<?php echo htmlspecialchars($pg);?>">
            <input type="hidden" name="subpg" value="<?php echo htmlspecialchars($subpg);?>">
            <input type="hidden" name="ApplicationSourceInternal" id="ApplicationSourceInternal" value="<?php echo htmlspecialchars(FROM_SRC);?>">
            <input type="hidden" name="ClientIPAddress" id="ClientIPAddress" value="<?php echo htmlspecialchars(G::Obj('ServerInformation')->getIPAddress());?>">
            <input type="hidden" name="HTTP_USER_AGENT" id="HTTP_USER_AGENT" value="<?php echo htmlspecialchars($_SERVER['HTTP_USER_AGENT']);?>">
            <input type="hidden" name="REMOTE_ADDR" id="REMOTE_ADDR" value="<?php echo htmlspecialchars($_SERVER['REMOTE_ADDR']);?>">
            <input type="hidden" name="REQUEST_URI" id="REQUEST_URI" value="<?php echo htmlspecialchars($_SERVER['REQUEST_URI']);?>">
            <input type="hidden" name="HTTP_REFERER" id="HTTP_REFERER" value="<?php echo htmlspecialchars($_SERVER['HTTP_REFERER']);?>">
            <input type="hidden" name="OrgID" value="<?php echo $OrgID;?>">
			<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID;?>">
			<input type="hidden" name="RequestID" value="<?php echo $RequestID;?>">
            <input type="hidden" name="FormID" value="<?php echo $FormID;?>">
			<input type="hidden" name="SectionID" value="<?php echo htmlspecialchars($section_id);?>">
			<input type="hidden" name="HoldID" value="<?php echo $HoldID;?>">
			<input type="hidden" name="InternalVCode" value="<?php echo $InternalVCode;?>">
            <input type="hidden" name="process" value="Y">
            <input type="button" name="btnInfo" id="btnInfo" value="Save/Next" onclick="processApplicationForm('<?php echo $section_forms[$section_id];?>', 'section_id<?php echo $section_id;?>', '<?php echo $next_section_id;?>');">
    		</form>
        </div>
		<?php
		$uic++;
    }
    ?>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12" id="process_message"></div>
</div>
					
<div id="process_finish_button_validation"></div>

<div>
    <form name="frmProcessApplication" id="frmProcessApplication" method="GET" action="signature.php">
        <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>">
        <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
        <input type="hidden" name="ProcessApplication" id="ProcessApplication" value="Yes">
        <input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
        <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $MultiOrgID;?>">
        <input type="hidden" name="HoldID" id="HoldID" value="<?php echo $HoldID;?>">
		<input type="hidden" name="InternalVCode" value="<?php echo $InternalVCode;?>">
        <?php
        $public_sec_list        =   array();
        foreach ($publicportal_sections_list as $public_section_id=>$public_section_info) {
            $section_id         =   $public_section_info['SectionID'];
            $public_sec_list[]  =   $section_id;
        }

        $sign_application       =   "true";
        foreach($public_sec_list as $public_sec_id) {
            if(is_array($processed_sections) && !in_array($public_sec_id, $processed_sections)) {
                $sign_application   =   "false";		
        	}
        }

        if(count($errors_list) == 0 && $sign_application == "true" && $HoldID != "") {
            ?><input type="submit" name="btnProcessApplicationForm" id="btnProcessApplicationForm" value="Sign Application"><?php
        }
        else {
        	?><input type="button" name="btnProcessApplicationForm" id="btnProcessApplicationForm" class="btnDisabled" value="Sign Application"><?php
        }
        ?>
    </form>
    <?php
    if (isset($TextBlocks ["FormFooter"]) && $TextBlocks ["FormFooter"] != "") {
        echo "<br>".$TextBlocks ["FormFooter"]."<br>";
    }
    ?>
    <span style="color:red">*</span>&nbsp;Please click on each section title and complete each section. Click on the Save/Next button to go to the next section. Click on the Sign Application button after all the sections are completed. All sections with required questions must be completed before the application can be submitted.
</div>

<style>
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 66%; /* Could be more or less, depending on screen size */
}
/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}
.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
</style>

<div id="myModal" class="modal">
  <div class="modal-content">
	  	<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">		
				<p style="text-align:right;">
					<a href="javascript:void(0);" title="Close Window" class="close" onclick="closeForm();">
						<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
						<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
					</a>
				</p>
				<br><br>
				<div id="divVerifyPhoneNumberInfo">
					<div id="approval_msg" style="color:blue;"></div>
					<form name="frmVerifyPhoneNumberInfo" id="frmVerifyPhoneNumberInfo" method="post">
						<input type="hidden" name="set_cellphone_permission" id="set_cellphone_permission">
						&nbsp;Enter the received code: 
						<input type="text" name="txtPhoneVerifyCode" id="txtPhoneVerifyCode">
						<input type="button" name="btnPhoneVerifyCode" id="btnPhoneVerifyCode" value="Verify" onclick="verifyReceivedCode()">
						<br>
						<a href="javascript:void(0);" onclick="verifyReceivedCode()" id="resend_code_again">Didn't Received the code. Resend again?</a>
					</form>
				</div>
				<br><br>
			</div>
		</div>
  </div>
</div>

<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info	=	JSON.parse('<?php echo json_encode($child_ques_info);?>');
</script>
<script src="<?php echo PUBLIC_HOME;?>js/child-questions.js" type="text/javascript"></script>
<script src="<?php echo PUBLIC_HOME;?>js/twilio-verify-phone-number.js" type="text/javascript"></script>
