<?php
$results    =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        $APPPROCESSDATA [$row ['QuestionID']] = $row ['Answer'];
    } // end foreach
}

$first      =   $APPPROCESSDATA['first'];
$last       =   $APPPROCESSDATA['last'];
$address    =   $APPPROCESSDATA['address'];
$city       =   $APPPROCESSDATA['city'];
$zip        =   $APPPROCESSDATA['zip'];
$email      =   $APPPROCESSDATA['email'];

// Check duplicate entry
require_once COMMON_DIR . 'process/CheckDuplicate.inc';

$link = "jobApplicationThankYou.php?";
$link .= "OrgID=".$OrgID;
if(isset($MultiOrgID) && $MultiOrgID != "") {
  $link .= "&MultiOrgID=".$MultiOrgID;
}
$link .= "&FormID=".$FormID;
$link .= "&RequestID=".$RequestID;
$link .= "&HoldID=".$HoldID;

if ($DUPERROR) {
	$urllink = $link . "&msg=duplicate";
	header("Location:" . $urllink);
	exit;
} // end DUPERROR


$req_details_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("FormID, Active, DATE(ExpireDate) as ExpireDate", $OrgID, $RequestID);
$dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);

if($req_details_info['Active'] == 'N'
	|| $dates_diff > 0) {
	$urllink = $link . "&msg=reqexpired";
	header("Location:" . $urllink);
	exit;
}

// lock for a duplicate submission
// Data to insert, here keys are table column names
$ins_app_lock_info  =   array(
                    		"EntryDate"           =>  "NOW()",
                    		"FirstName"           =>  $APPPROCESSDATA['first'],
                    		"LastName"            =>  $APPPROCESSDATA['last'],
                    		"Address"             =>  $APPPROCESSDATA['address'],
                    		"City"                =>  $APPPROCESSDATA['city'],
                    		"Zip"                 =>  $APPPROCESSDATA['zip'],
                    		"Email"               =>  $APPPROCESSDATA['email'],
                    		"SelectedPosition"    =>  $RequestID
                        );

// lock for a duplicate submission
G::Obj('Applicants')->insApplicantLock ( $ins_app_lock_info );

// Generate ApplicationID
$ApplicationID  =   G::Obj('Applications')->getApplicationID ($OrgID);

//Copy data from temporary table to ApplicantData table
G::Obj('ApplicantsData')->copyApplicantDataTempToApplicantData($OrgID, $HoldID, $ApplicationID, $RequestID);
G::Obj('ApplicantsData')->copyApplicantAttachmentsTempToAppAttachments($OrgID, $HoldID, $ApplicationID);

if ($HoldID) {
    //Set information to update
    $set_info   =   array("HoldID = :SHoldID");
    //Set condition
    $where      =   array("OrgID = :OrgID", "HoldID = :HoldID");
    //Bind the parameters
    $params     =   array(":SHoldID"=>$ApplicationID, ":OrgID"=>$OrgID, ":HoldID"=>$HoldID);
    //Update Prescreen Results
    G::Obj('PrescreenQuestions')->updPrescreenResults($set_info, $where, array($params));
}

if($feature['ResumeParsing'] == "Y") {
    //Rename the file with cookie appid to application id
    $file_path_info = G::Obj('Sovren')->copyPublicPortalParsingTempToParsing($OrgID, $HoldID, $ApplicationID, $RequestID, $FormID);
}

$LeadID         =   G::Obj('ApplicationLeads')->getLeadIDFromFormID($OrgID, $FormID);
$LeadStatus     =   ($LeadID == "") ? "" : "N";

$IsInternalApplication  =   (isset($_REQUEST['InternalVCode']) && $_REQUEST['InternalVCode'] != "") ? "Yes" : "No";
// Insert data into DB (JobApplications)
$job_app_info           =   array(
                        		"OrgID"                 =>  $OrgID,
                        		"MultiOrgID"            =>  $MultiOrgID,
                        		"ApplicationID"         =>  $ApplicationID,
                        		"RequestID"             =>  $RequestID,
                                "ApplicantSortName"     =>  $APPPROCESSDATA['last'] . ', ' . $APPPROCESSDATA['first'],
                        		"Distinction"           =>  'P',
                        		"EntryDate"             =>  "NOW()",
                        		"LastModified"          =>  "NOW()",
                        		"ProcessOrder"          =>  "1",
                        		"StatusEffectiveDate"   =>  "DATE(NOW())",
                        		"ReceivedDate"          =>  "NOW()",
                                "FormID"                =>  $FormID,
                                "Informed"              =>  $APPPROCESSDATA['informed'],
                                "RequisitionStatus"     =>  $req_details_info['Active'],
                        		"LeadGenerator"         =>  $LeadID,
                        		"LeadStatus"            =>  $LeadStatus,
                                "IsInternalApplication" =>  $IsInternalApplication
                            );
G::Obj('Applications')->insJobApplication ( $job_app_info );

// Insert Applicant Status Logs Information
G::Obj('ApplicantStatusLogs')->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, "1", "New Applicant");

// Insert into ApplicantHistory
$UpdateID   =   'Applicant';
$Comments   =   'Application submitted for: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $MultiOrgID, $RequestID );

$job_application_history = array (
		"OrgID"                 =>  $OrgID,
		"ApplicationID"         =>  $ApplicationID,
		"RequestID"             =>  $RequestID,
		"ProcessOrder"          =>  "1",
		"StatusEffectiveDate"   =>  "DATE(NOW())",
		"Date"                  =>  "NOW()",
		"UserID"                =>  $UpdateID,
		"Comments"              =>  $Comments
);
G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );

// Set OnboardStatus to ready for this Application
if ($OrgID == "I20091201") {
    //Get Applicant MultiOrgID
    $AppMultiOrgID  =   G::Obj('ApplicantDetails')->getMultiOrgID($OrgID, $ApplicationID, $RequestID);
    //Set Onboard Header Information
    $onboard_info   =   array(
                                "OrgID"         =>  $OrgID,
                                "MultiOrgID"    =>  $AppMultiOrgID,
                                "ApplicationID" =>  $ApplicationID,
                                "RequestID"     =>  $RequestID,
                                "Status"        =>  'ready',
                                "ProcessDate"   =>  "NOW()",
                                "OnboardFormID" =>  'XML',
                            );
    $on_update      =   " ON DUPLICATE KEY UPDATE Status = :UStatus, ProcessDate = NOW()";
    $update_info    =   array(":UStatus" =>  'ready');
    G::Obj('OnboardApplications')->insOnboardApplication($onboard_info, $on_update, $update_info);
}

if(DEVELOPMENT == 'N') {
	// Send Thank you email to Applicant
	$org_title_info = G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "OrganizationName");
	require_once COMMON_DIR . 'process/EmailThankYou.inc';
	sendThankYou ( $OrgID, $ApplicationID, $FormID, $org_title_info['OrganizationName'] );
}

//Set params
$params     =   array(":OrgID"=>$OrgID);
// Auto Forward
$where      =   array("OrgID = :OrgID", "Role IN ('', 'master_admin')");
//Get UserInformation
$results    =   G::Obj('IrecruitUsers')->getUserInformation("UserID", $where, "Role LIMIT 1", array($params));
//Admin
$user_id    =   $results['results'][0]['UserID'];

//Set params
$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Set condition
$where      =   array("OrgID = :OrgID", "RequestID = :RequestID");
//Get Requisition Information Forward
$results    =   G::Obj('Requisitions')->getRequisitionForwardInfo("EmailAddress", $where, "", array($params));

if ($results['count'] > 0) {

	include IRECRUIT_DIR . 'applicants/EmailApplicant.inc';

	//Have to update it with forward applicant specs information
	$ATTS               =   array();
	//Have to update it with forward applicant specs information
	$req_forward_specs  =   G::Obj('RequisitionForwardSpecs')->getRequisitionForwardSpecsDetailInfo($OrgID, $RequestID);
	$forward_specs_list =   json_decode($req_forward_specs ['ForwardSpecsList'], true);
	//Forward Specs List
	foreach($forward_specs_list as $spec_que_id) {
		$ATTS[$spec_que_id] =   'Y';
	}

	if (is_array ( $results ['results'] )) {
		$EmailList = array();
		$status_index = 0;
		foreach ( $results ['results'] as $EM ) {
			$Attachments = forwardApplicant ( $OrgID, $ApplicationID, $RequestID, $EM ['EmailAddress'], $ATTS, 'Application Auto Forwarded', $user_id, 'processforward',  $status_index);
			$EmailList[] = $EM ['EmailAddress'];
			$status_index++;
		} // end foreach
	}

	updateApplicantStatus ( $OrgID, $ApplicationID, $RequestID, 'Application', implode(", ", $EmailList), $Attachments, 'Application Auto Forwarded' );
} // end AutoForward

$urllink = $link . "&ApplicationID=".$ApplicationID."&msg=suc";
header("Location:" . $urllink);
exit();
?>
