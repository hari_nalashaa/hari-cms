<?php
require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';
require_once PUBLIC_DIR . 'SessionConfig.inc';
require_once VENDOR_DIR . 'autoload.php';

header ( 'P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"' );

define ( 'FROM_SRC', 'PUBLIC' );

ini_set ("allow_url_fopen", 1);
ini_set ("display_errors", 0);

$error_log_file_path = ROOT . "logs/php-errors/php-error-log-".date('Y-m-d').".log";

if(!is_dir(ROOT . "logs/php-errors")) {
    mkdir(ROOT . "logs/php-errors");
    chmod(ROOT . "logs/php-errors", 0777);
}

ini_set("log_errors", 1);
ini_set("error_log", $error_log_file_path);

if(isset($_REQUEST['pg']) && $_REQUEST['pg'] != "") $pg = $_REQUEST['pg'];
if(isset($_REQUEST['navpg']) && $_REQUEST['navpg'] != "") $navpg = $_REQUEST['navpg'];
if(isset($_REQUEST['navsubpg']) && $_REQUEST['navsubpg'] != "") $navsubpg = $_REQUEST['navsubpg'];
if(isset($_REQUEST['subpg']) && $_REQUEST['subpg'] != "") $subpg = $_REQUEST['subpg'];
if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];

// Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';

// Get ApplicationFeatures Information
$AF = G::Obj('IrecruitApplicationFeatures')->getApplicationFeatures ();

$afi = 0;
foreach ( $AF as $key => $value ) {
	if ($afi > 0) {
		$featureList [$afi] = $key;
	}
	$afi ++;
}

// assure no spaces in URL - Indeed check and cut and paste issue
$OrgID		=	preg_replace ( "/\s/i", '', $_REQUEST ['OrgID'] );
$MultiOrgID =	(isset($_REQUEST ['MultiOrgID']) && $_REQUEST ['MultiOrgID'] != "") ? preg_replace ( "/\s/i", '', $_REQUEST ['MultiOrgID'] ) : "";

// MultiOrg Checks
if (isset($_REQUEST ['mOrgID'])) {
	
	$org_information	=	$OrganizationDetailsObj->getOrgIDMultiOrgIDByMultiOrgID ( $_REQUEST ['mOrgID'] );
	$OrgID				=	$org_information['OrgID'];
	$MultiOrgID			=	$org_information['MultiOrgID'];
} else if (isset($_REQUEST['OrgID']) && isset($_REQUEST['MultiOrgID'])) {

	$org_information	=	$OrganizationDetailsObj->getOrganizationInformation($_REQUEST ['OrgID'], $_REQUEST ['MultiOrgID'], "OrgID, MultiOrgID");
	$OrgID				=	$org_information['OrgID'];
	$MultiOrgID			=	$org_information['MultiOrgID'];
} else if (isset($_REQUEST['OrgID']) && isset($_REQUEST['RequestID'])) {

	$MultiOrgID 		=	G::Obj('RequisitionDetails')->getMultiOrgID ( $_REQUEST ['OrgID'], $_REQUEST ['RequestID'] );
	$OrgID				=	$_REQUEST['OrgID'];
}

$brand_info				=	G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, "", "BrandID");
if(!isset($brand_info['BrandID']) || $brand_info['BrandID'] == "") $brand_info['BrandID'] = "0";
$brand_org_info			=	G::Obj('Brands')->getBrandInfo($brand_info['BrandID']);

$OrgIDck				=	"";
//Set parameters
$params_org				=	array(":OrgID"=>$OrgID);
//Get OrgData Information
$results				=	G::Obj('Organizations')->getOrgDataInfo("OrgID", array("OrgID = :OrgID"), '', array($params_org));
$OrgIDck				=	$results['results'][0]['OrgID'];

if ($OrgIDck == "") {
	header ( 'Location: http://www.irecruit-software.com');
	exit ();
}

// set functionality for add on modules
$row					=	G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID($OrgID);

foreach ( $featureList as $s ) {
	$feature [$s] = $row [$s];
}

$InternalCode = "";
if ($feature ['InternalRequisitions'] == "Y") {
    if(!isset($MultiOrgID)) $MultiOrgID = '';
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );

if($_REQUEST['InternalVCode'] == $InternalCode) {
    $Internal   =   "Y";
}

} // within feature

//White list the OrgID, MultiOrgID
$whitelist[]	=	$OrgID;
$whitelist[]	=	$MultiOrgID;

require_once COMMON_DIR . 'color.inc';
?>
