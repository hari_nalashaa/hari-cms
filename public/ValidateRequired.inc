<?php
$ERROR .= "";

//Validate the information
G::Obj('GetFormPostAnswer')->POST   =   $REQUEST_DATA;
G::Obj('GetFormPostAnswer')->FILES  =   $FILES_DATA;

//Get Modified POST Answers
$POSTANSWERS    =   G::Obj('GetFormPostAnswer')->getPostDataAnswersOfFormQuestions($OrgID, $FormID, $REQUEST_DATA['SectionID']);

//Validate the information
G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']   =   $POSTANSWERS;
G::Obj('ValidateApplicationForm')->FORMDATA['FILES']     =   $FILES_DATA;

//Validate Required Fields By SectionID
$errors_info    =   G::Obj('ValidateApplicationForm')->validateApplicationForm($OrgID, $FormID, $REQUEST_DATA['SectionID'], $HoldID);
$errors_list    =   $errors_info['ERRORS'];

if(isset($errors_list) && is_array($errors_list)) {
    foreach ($errors_list as $error_que_id=>$error_que_msg) {
        $ERROR  .=  $error_que_msg;
    }
}

return $ERROR;
?>