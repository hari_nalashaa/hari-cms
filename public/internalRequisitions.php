<?php
require_once 'public.inc';
require_once PUBLIC_DIR . 'IndexPageRequestVars.inc';

if(isset($_GET['R']) && $_GET['R'] == 'Y') {
    unset($_SESSION['P']['InternalReqSetting']);
}
if ($feature ['InternalRequisitions'] == "Y") {

    $inernal_req_settings   =   G::Obj('InternalRequisitionsSettings')->getInternalRequisitionsSettingsInfo($OrgID);
    $InternalAns            =   $inernal_req_settings['Answer'];
    $InternalCode           =   $inernal_req_settings['InternalCode'];
    
    if($_REQUEST['RUEmployee'] != '')
    {
        $_SESSION['P']['InternalReqSetting'] = $_REQUEST['RUEmployee'];
    }
    
    if($_SESSION['P']['InternalReqSetting'] != "Yes" && $_SESSION['P']['InternalReqSetting'] != "") {
        $link       =   PUBLIC_HOME."index.php?OrgID=".$OrgID;
        if($MultiOrgID != "") {
            $link   .=  "&MultiOrgID=".$MultiOrgID;
        }
        header("Location:".$link);
        exit();
    }
    
    if($_REQUEST['InternalVCode'] == $InternalCode) {
        $Internal   =   "Y";
        $_SESSION['P'][$OrgID.$MultiOrgID.$RequestID]['source'] = $InternalCode;
    }
}

$req_details_info       =   array();

if(isset($RequestID) && $RequestID != "") {
    $req_details_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("FormID, Active, DATE(ExpireDate) as ExpireDate", $OrgID, $RequestID);
    $dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);
}

$inernal_req_settings   =   G::Obj('InternalRequisitionsSettings')->getInternalRequisitionsSettingsInfo($OrgID);

//Bind parameters
	$preparams    =   array(':OrgID'=>$OrgID,  ':RequestID'=>$RequestID);
	//set condition
	$prewhere     =   array("OrgID = :OrgID", "RequestID = :RequestID");
	//Get Prescreen Results
	$prerslts      =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation ( "*", $prewhere, "SortOrder", array($preparams) );


if (isset($_REQUEST['FormID']) && $_REQUEST['FormID'] != "") {
    $FormID = $_REQUEST['FormID'];
}

//If FormID is empty, have to pull it based on RequestID
if($FormID == '') {
    $FormID = $req_details_info['FormID'];
}

if(isset($req_details_info['Active']) && $req_details_info['Active'] == 'N'
    || (isset($dates_diff) && $dates_diff > 0)) {
    header("Location:".PUBLIC_HOME."jobApplicationThankYou.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID.'&msg=reqexpired');
    exit();
}

if ($_SERVER['HTTP_REFERER'] == "https://www.glassdoor.com/") {
	header("Location:".PUBLIC_HOME."jobRequest.php?OrgID=".$OrgID."&RequestID=".$RequestID);
	exit();
}

require_once 'SetMonsterSessionCookies.inc';

$new_job_image = '';
$hot_job_image = '';
// update new job status
$highlight_req_settings		=	G::Obj('HighlightRequisitions')->getHighlightRequisitionSettings($OrgID);

if($highlight_req_settings['HighlightNewJob'] == "Yes") {
    $new_job_days = $highlight_req_settings['NewJobDays'];
    $new_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['NewJobIcon']);
    
    if($new_job_icon != "") {
        $new_job_image = '<img src="'.IRECRUIT_HOME.'/vault/highlight/'.$new_job_icon.'">';
    }
}

if($highlight_req_settings['HighlightHotJob'] == "Yes") {
    $hot_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['HotJobIcon']);
    
    if($hot_job_icon != "") {
        $hot_job_image = '<img src="'.IRECRUIT_HOME.'/vault/highlight/'.$hot_job_icon.'">';
    }
}

//Redirect it to Job Application page
if(isset($pg) && $pg == 'application') {
    if(isset($MultiOrgID) 
        && $MultiOrgID != ""
        && $RequestID != "") {

        $app_info_log_data  =  "\nFrom Source: " . FROM_SRC."\n";
        $app_info_log_data  .=  "\nOrgID: " . $OrgID."\n";
        $app_info_log_data  .=  "\nMultiOrgID: " . $MultiOrgID."\n";
        $app_info_log_data  .=  "\nRequestID: " . $_REQUEST['RequestID']."\n";
        $app_info_log_data  .=  "\nPG: " . $pg."\n";
        $app_info_log_data  .=   "\nServer Info: " . json_encode($_SERVER);
        $app_info_log_data  .=  "\nRequest: " . json_encode($_REQUEST);
        
        Logger::writeMessage(ROOT."logs/public_application/". $OrgID . "-" . date('Y-m-d-H') . "-index.txt", $app_info_log_data, "a+", true, "Public - Index.php - Hit");
       
        header("Location:".PUBLIC_HOME."jobApplication.php?OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID."&RequestID=".$_REQUEST['RequestID']."&HoldID=".$HoldID);
        exit();
    }
    else if(isset($OrgID) && $OrgID != "" && $RequestID != "") {
        
        $app_info_log_data  =  "\nFrom Source: " . FROM_SRC."\n";
        $app_info_log_data  .=  "\nOrgID: " . $OrgID."\n";
        $app_info_log_data  .=  "\nMultiOrgID: " . $MultiOrgID."\n";
        $app_info_log_data  .=  "\nRequestID: " . $_REQUEST['RequestID']."\n";
        $app_info_log_data  .=  "\nPG: " . $pg."\n";
        $app_info_log_data  .=   "\nServer Info: " . json_encode($_SERVER);
        $app_info_log_data  .=  "\nRequest: " . json_encode($_REQUEST);
        
        Logger::writeMessage(ROOT."logs/public_application/". $OrgID . "-" . date('Y-m-d-H') . "-index.txt", $app_info_log_data, "a+", true, "Public - Index.php - Hit");
        
        header("Location:".PUBLIC_HOME."jobApplication.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&HoldID=".$HoldID);
        exit();
    }
}

//Incldue Header
require_once PUBLIC_DIR . 'Header.inc';

if ($pg == "terms") {
    require_once COMMON_DIR . 'terms.inc';
}
else if ($pg == "policy") {
    require_once COMMON_DIR . 'policy.inc';
}
else {
    //Get Organization Detail Information
    $org_detail_info    =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView");
    $listings_view      =   ($org_detail_info['ListingsView'] == 'initiallist') ? 'initiallist' : $org_detail_info['ListingsView'];
    ?>
    <table class="table table-bordered">
        <tr>
        	<td colspan="2">
        		<?php
        		echo "<br><br>".$inernal_req_settings['InternalRequisitionsText']."<br><br>";
        		?>
        	</td>
        </tr>
    </table>
    <?php
    if($_SESSION['P']['InternalReqSetting'] == "") {
        ?>
        <form method="get">
			<input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
			<?php 
			if($MultiOrgID != "") {
			    ?>
				<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $MultiOrgID;?>">			    
			    <?php
			}
			?>
        	<input type="hidden" name="InternalVCode" id="InternalVCode" value="<?php echo $InternalVCode;?>">
            <table class="table table-bordered">
                <tr>
            		<td>
            			<?php echo $inernal_req_settings['Question'];?>
            		</td>
            	</tr>
            	<tr>
            		<td>
            			<input type="radio" name="RUEmployee" id="RUEmployee" value="Yes"> Yes
            			<input type="radio" name="RUEmployee" id="RUEmployee" value="No"> No
            		</td>
            	</tr>
            	<tr>
            		<td colspan="2"><input type="submit" value="Submit"></td>
            	</tr>
        	</table>
    	</form>
        <?php
    }
    else {
        if($_REQUEST['InternalVCode'] == $InternalCode) {
            //Display Listings by listings view variable
            require_once COMMON_DIR . 'listings/' . $listings_view . '.inc';
        }
        else {
            echo "<br><br><strong>No requisitions found.</strong>";
        }
    }
}

echo '<br><br><br>';
//Include Footer
require_once PUBLIC_DIR . 'Footer.inc';
?>