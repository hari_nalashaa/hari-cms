<?php
//Get CustomHeaderFooter OrgID
$results        =   G::Obj ( 'CustomHeaderFooter' )->getCustomHeaderFooter($OrgID);
$OrgIDck        =   $results['OrgID'];

$results_multi  =   G::Obj ( 'CustomHeaderFooter' )->getCustomHeaderFooterMultiOrgID($MultiOrgID);
$MultiOrgIDck   =   $results_multi['MultiOrgID'];

$columns = "MultiOrgID, Title, Description, MediaDescription, FormID, EmpStatusID, DATE_FORMAT(PostDate,'%M %d, %Y') as PostDate, 
				City, State, ZipCode, InternalFormID";
// Get Requisition Information
$req_results = G::Obj('Requisitions')->getRequisitionsDetailInfo($columns, $OrgID, $RequestID);
$description = $req_results[MediaDescription] ? $req_results[MediaDescription] : $req_results[Description];

$req_columns = array('OrgID', 'SocialMediaIcon');
 
$fileurl = '';
 if(!file_exists(PUBLIC_HOME . 'images/' . $OrgID)){ 
  $scan = array_diff(scandir(PUBLIC_DIR.'images/'.$OrgID), array('.', '..')); 
	 if(!empty($scan)){
	  foreach($scan as $singlefile){
	     $explo_file= explode(".", $singlefile);
	       if($explo_file[0] == $RequestID ){
	         $fileurl = PUBLIC_HOME . 'images/' . $OrgID.'/'.$singlefile; 
	          break;
	        }else{ 
	        $fileurl = '';
	        }	
	  }
	
	}
}
	
//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgIDck);
//Get Organization Logos Information
$logo_results = $OrganizationsObj->getOrganizationLogosInformation($req_columns, $where, '', array($params));

if ($OrgIDck != "" || $MultiOrgIDck != "") {
	require_once 'skins/HeaderCustom.inc';
} else {
	require_once 'skins/HeaderPublic.inc';
}
?>
