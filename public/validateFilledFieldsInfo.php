<?php 
require_once 'public.inc';

$OrgID          =   $_REQUEST['OrgID'];
$MultiOrgID     =   $_REQUEST['MultiOrgID'];
$RequestID      =   $_REQUEST['RequestID'];
$FormID         =   $_REQUEST['FormID'];
$HoldID         =   $_REQUEST['HoldID'];

$title          =   "Validate Filled Fields Information";

//Get Request and Files Data
$SERIALIZE_REQUEST_DATA =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "REQUEST_DATA");
$SERIALIZE_FILES_DATA   =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "FILES_DATA");

//Set Request Data, Files Data
$REQUEST_DATA   =   unserialize($SERIALIZE_REQUEST_DATA);
$FILES_DATA     =   unserialize($SERIALIZE_FILES_DATA);

$REQUEST_DATA['SectionID']  =   '';

//Get Requisition information based on RequestID //Set where condition for requisition
$req_info       =   G::Obj('Requisitions')->getReqDetailInfo("Title, FormID, MultiOrgID", $OrgID, $MultiOrgID, $RequestID);

$APPDATA        =   array ();
$APPDATAREQ     =   array ();
$results        =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        $APPDATA [$row ['QuestionID']]      =   $row ['Answer'];
        $APPDATAREQ [$row ['QuestionID']]   =   $row ['Required'];
    } // end foreach
}

$processed_sections     =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "ProcessedSections");
$un_processed_sections  =   unserialize($processed_sections);

require_once PUBLIC_DIR . 'FilterPublicPortalSections.inc';

require_once PUBLIC_DIR . 'ValidateRequired.inc';

$public_sec_list          =   array();
foreach ($publicportal_sections_list as $section_id=>$section_info) {
    $public_sec_list[]    =   $section_id;
}

$sign_application   =   "true";

foreach($public_sec_list as $public_sec_id) {
    if(is_array($un_processed_sections) && !in_array($public_sec_id, $un_processed_sections)) {
        $sign_application   =   "false";
    }
}

if(count($errors_list) > 0) {
    $sign_application   =   "false";
}

if($sign_application == "true") {
    echo json_encode(array("errors_list"=>$errors_list, "success"=>"true", "section_id_names"=>$section_id_names));
    exit;
}
else {
    echo json_encode(array("errors_list"=>$errors_list, "success"=>"false", "section_id_names"=>$section_id_names));
    exit;
}
?>