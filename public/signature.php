<?php
require_once 'public.inc';

$OrgID          =   isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$MultiOrgID     =   isset($_REQUEST['MultiOrgID']) ? $_REQUEST['MultiOrgID'] : '';
$RequestID      =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$HoldID         =   isset($_REQUEST['HoldID']) ? $_REQUEST['HoldID'] : '';
$FormID         =   isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';
$InternalVCode  =   isset($_REQUEST['InternalVCode']) ? $_REQUEST['InternalVCode'] : '';

//Get Requisition information based on RequestID //Set where condition for requisition
$req_info       =   G::Obj('Requisitions')->getReqDetailInfo("Title", $OrgID, $MultiOrgID, $RequestID);

if($OrgID == ""
    || $RequestID == ""
    || $HoldID == ""
    || $FormID == "") {

	$redirect = PUBLIC_HOME . "index.php?OrgID=".$OrgID;
	if (isset($MultiOrgID) && $MultiOrgID != "") {
	  $redirect .= "&MultiOrgID=".$MultiOrgID;
	}
	$redirect .= "&navpg=listings";

	if (!isset($FormID) || $FormID == "") {
	        header("Location: " . $redirect);
	        exit();
	}

    $to = "dedgecomb@irecruit-software.com";
    $subject = "Alert - Issue with the Application - Signature";
    $txt = "OrgID: " . $OrgID . "\r\n";
    $txt .= "RequestID: " . $RequestID . "\r\n";
    $txt .= "HoldID: " . $HoldID . "\r\n";
    $txt .= "MultiOrgID: " . $MultiOrgID . "\r\n";
    $txt .= "FormID: " . $FormID . "\r\n";
    $txt .= "Request Information: " . print_r($_REQUEST, true) . "\r\n";
    $txt .= "Server Information: " . print_r($_SERVER, true) . "\r\n";
    $txt .= "After Redirect\r\n";
    
    $headers = "From: info@irecruit-us.com" . "\r\n";
    
    mail($to,$subject,$txt,$headers);
}

if(isset($_POST['btnSubmitApplication']) 
    && $_POST['btnSubmitApplication'] == 'Submit Application'
    && $_REQUEST['agree'] != ""
    && $_REQUEST['signature'] != "") {

    //Insert Other Questions To TempData
    $other_ques_list    =   array(
                                'signature' =>  array("Answer"=>$_REQUEST['signature']),
                                'agree'     =>  array("Answer"=>$_REQUEST['agree']),
                            );
    //Insert Other Questions
    foreach ($other_ques_list as $other_que_id=>$oqa) {
        $QI     =   array("OrgID"=>$OrgID, "HoldID"=>$HoldID, "QuestionID"=>$other_que_id, "Answer"=>$oqa["Answer"]);
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
    }
    
    $thank_you_link = "jobApplicationThankYou.php?OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID."&RequestID=".$RequestID."&FormID=".$FormID."&HoldID=".$HoldID."&Process=Yes";
    if($InternalVCode != "") {
        $thank_you_link .= "&InternalVCode=".$InternalVCode;
    }
    header("Location: ".$thank_you_link);
    exit();
}
else if($_POST['btnSubmitApplication'] == 'Submit Application') {

    $ERROR  =   '';
    if ($_REQUEST['agree'] == "") {
        $ERROR  .=   " - Electronic Signature agreement indication is missing." . "<br>";
        G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp ($OrgID, $HoldID, 'agree', $REQUEST_DATA['agree'] );
    }
    
    if ($_REQUEST['signature'] == "") {
        $ERROR  .=   " - Electronic Signature is missing." . "<br>";
        G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp ($OrgID, $HoldID, 'signature', $REQUEST_DATA['signature'] );
    }
}

$title      =   "Signature";

// Query and Set Text Elements
$params		=	array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
//Get text blocks information
$where		=	array("OrgID = :OrgID", "FormID = :FormID");
$results	=	G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
    }
}

if (FROM_SRC == "PUBLIC") {
  require_once PUBLIC_DIR . 'Header.inc';
  $LISTINGS_URL = strtolower(PUBLIC_HOME);
}
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
	<form method="POST" name="frmAuthorizeApplication" id="frmAuthorizeApplication">
    <table class="table table-bordered">
    	<tr>
	    <?php
	    if ($TextBlocks ["ApplicantAuthorizations"]) {
	    	$ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
	    } else {
	    	$ApplicantAuthorizations = "";
	    }
	    
	    $requiredck = '';
	    if ($APPDATAREQ ['signature'] == "REQUIRED") {
	    	$requiredck = ' bgcolor="' . $highlight . '"';
	    	$APPDATA ['signature'] = "";
	    }
	    
	    if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	    	$Signature    =   '<td width="15%">Electronic Signature:</td>';
	    	$Signature    .=  '<td><b>' . $APPDATA ['signature'] . '</b></td>';
	    } else {
	    	$Signature    =   '<td width="15%"' . $requiredck . '><font style="color:red">*</font> Electronic Signature: <font style="color:red">X</font></td>';
	    	$Signature    .=  '<td><input type="text" name="signature" size="33" value="' . $APPDATA ['signature'] . '"></td>';
	    }
		?>
		</tr>
	</table>
	<?php
    if (($TextBlocks ["Signature"]) || ($ApplicantAuthorizations)) {
    	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
    	
    	echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
    	if($ERROR) {
    	    echo "<tr><td colspan='2' style='color:red'>".$ERROR."</td></tr>";
    	}
    	echo '<tr>';
    	echo '<td>';
    	echo '<b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '<br>';
    	echo $TextBlocks ["Signature"];
    	echo '</td>';
    	echo '</tr>';
    	echo '</table>';
    	
    	echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
    	echo '<tr><td' . $requiredck . ' colspan="2"><input type="checkbox" name="agree" value="Y"';
    	if ($APPDATA ['agree'] == "Y") {
    		echo ' checked';
    	}
    	if ($OrgID == "I20100301") { // Meritan
    		echo '> <font style="color:red">*</font> I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.</td></tr>';
    	} else {
    		echo '> <font style="color:red">*</font> I agree with the above statement.</td></tr>';
    	}
    	echo '</td>';
    	echo '<tr>';
    	echo $Signature;
    	echo '</tr>';
    	echo '</table>';
    }
    
    if ($TextBlocks ["FormFooter"]) {
		echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
		echo '<tr><td>';
		echo $TextBlocks ["FormFooter"];
		echo '</td></tr>';
		echo '</table>';
    }

    echo '<input type="hidden" name="Process" id="Process" value="Yes">';
    echo '<input type="hidden" name="OrgID" id="OrgID" value="' . $OrgID . '">';
    echo '<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="' . $MultiOrgID . '">';
    echo '<input type="hidden" name="HoldID" id="HoldID" value="' . $HoldID . '">';
    echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
    echo '<input type="hidden" name="FormID" value="' . $FormID . '">';
    echo '<input type="hidden" name="InternalVCode" value="' . $InternalVCode . '">';
    
    $go_back_link = PUBLIC_HOME . "jobApplication.php?OrgID=".$OrgID;
    if($MultiOrgID != "") {
        $go_back_link .= "&MultiOrgID=".$MultiOrgID;
    }
    $go_back_link .= "&HoldID=".$HoldID;
    $go_back_link .= "&RequestID=".$RequestID;
    if($InternalVCode != "") {
        $go_back_link .= "&InternalVCode=".$InternalVCode;
    }
    ?>
	<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">
    	<tr>
        	<td height="60" valign="middle">
            	<input type="submit" id="btnSubmitApplication" name="btnSubmitApplication" value="Submit Application">
				<input type="button" id="btnGoBack" name="btnGoBack" onclick="location.href='<?php echo $go_back_link;?>';" value="Go Back">
        	</td>
    	</tr>
	</table>

	</form>
	</div>
</div>
<?php
if (FROM_SRC == "PUBLIC") {
  require_once PUBLIC_DIR . 'Footer.inc';
}
?>
