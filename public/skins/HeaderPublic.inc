<!DOCTYPE HTML>
<html>
<head>
<title><?php echo $title;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests"> 

<meta name="title" content="<?php echo $req_results[Title]; ?>">
<meta name="description" content="<?php echo strip_tags($req_results[Description]); ?>">
<meta name="keywords" content="IRecruit,Job">


<meta property="og:title" content="<?php echo $req_results[Title]; ?>">
<meta property="og:description" content="<?php echo strip_tags($description); ?>">
<meta property="og:site_name" content="IRecruit">
<meta property="og:type" content="website">
<meta property="og:image" content="<?php echo $fileurl; ?>">
<meta property="og:url" content="<?php echo $_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]; ?>">

<meta property="twitter:title" content="<?php echo $req_results[Title]; ?>">
<meta property="twitter:description" content="<?php echo strip_tags($description); ?>">
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:image" content="<?php echo $fileurl; ?>">

<!-- Bootstrap Core CSS -->
<link href="<?php echo PUBLIC_HOME?>css/bootstrap.min.css"	rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo PUBLIC_HOME?>css/style.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="<?php echo PUBLIC_HOME?>font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo PUBLIC_HOME?>css/jquery-ui.css" rel="stylesheet" type="text/css" />

<?php
if ($OrgID == "I20140922") { 
	?>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<?php 
}

$system_styles = "\n" . '<link href="' . PUBLIC_HOME . 'css/style.css" rel="stylesheet">';

if(file_exists(PUBLIC_DIR . 'images/'.$OrgID.'/style.css')) {
	$system_styles .= "\n" . '<link href="' . PUBLIC_HOME . 'images/'.$OrgID.'/style.css" rel="stylesheet">';
}

echo $system_styles;

$user_detail_info       =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "UserPortalThemeID");
$up_app_theme_info      =   G::Obj('UserPortalInfo')->getUserPortalApplicationThemeInfo($user_detail_info['UserPortalThemeID']);
?>
<link href="<?php echo PUBLIC_HOME?>css/job-application-steps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var up_active_step_tab = '<?php if(!isset($up_app_theme_info['ActiveTab']) || $up_app_theme_info['ActiveTab'] == '') { echo '#6b92c6'; } else { echo "#".$up_app_theme_info['ActiveTab']; }?>';
</script>

<style type="text/css">
.public_application_steps li a.public_application_step_tabs {
    color: white;
    text-decoration: none;
	padding: 7px 7px 7px 7px;
	background: <?php if(!isset($up_app_theme_info['PendingTab']) || $up_app_theme_info['PendingTab'] == '') { echo '#8994a5'; } else { echo "#".$up_app_theme_info['PendingTab']; }?>;
    position: relative;
    display: block;
    float: left;
}
.public_application_steps li a.public_application_step_tabs_filled {
    color: white;
    text-decoration: none;
	padding: 7px 7px 7px 7px;
	background: <?php if(!isset($up_app_theme_info['CompletedTab']) || $up_app_theme_info['CompletedTab'] == '') { echo '#4889f2'; } else { echo "#".$up_app_theme_info['CompletedTab']; }?>;
    position: relative;
    display: block;
    float: left;
}

@media (max-width: 767px) {
    .public_application_steps li a.public_application_step_tabs {
        color: white;
        text-decoration: none;
    	padding: 7px 7px 7px 7px;
    	background: <?php if(!isset($up_app_theme_info['PendingTab']) || $up_app_theme_info['PendingTab'] == '') { echo '#8994a5'; } else { echo "#".$up_app_theme_info['PendingTab']; }?>;
        position: relative;
        display: block;
        float: left;
        width:99%;
    }
    .public_application_steps li a.public_application_step_tabs_filled {
        color: white;
        text-decoration: none;
    	padding: 7px 7px 7px 7px;
    	background: <?php if(!isset($up_app_theme_info['CompletedTab']) || $up_app_theme_info['CompletedTab'] == '') { echo '#4889f2'; } else { echo "#".$up_app_theme_info['CompletedTab']; }?>;
        position: relative;
        display: block;
        float: left;
        width:99%;
    }
}
.navbar {
	background-color: <?php if(!isset($up_app_theme_info['Navbar']) || $up_app_theme_info['Navbar'] == '') { echo '#0FA5E4'; } else { echo "#".$up_app_theme_info['Navbar']; }?>;
	box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
}
</style>
<script src="<?php echo PUBLIC_HOME;?>js/jquery-1.11.1.min.js"></script>
<script src="<?php echo PUBLIC_HOME;?>js/jquery-browser-migration.js"></script>
<script src="<?php echo PUBLIC_HOME;?>js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo PUBLIC_HOME;?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo PUBLIC_HOME;?>js/process-job-application.js"></script>
<?php
require_once COMMON_DIR . "application/ApplicationJavaScript.inc";
?>
<script>
var public_home =	'<?php echo PUBLIC_HOME;?>';
var lst			=	'<?php echo $lst;?>';
var modal		=	'<?php echo $modal;?>';
</script>
<script src="<?php echo PUBLIC_HOME;?>js/common.js" type="text/javascript"></script>
</head>
<body> 
<?php
$ImagePresent	=	0;

//Set where condition
$where			=	array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
//Set parameters
$params			=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>(string)$MultiOrgID);
$results		=	G::Obj('Organizations')->getOrganizationLogosInformation("OrgID", $where, '', array($params));

$ImagePresent	=	$results['count'];

if ($ImagePresent > 0) {
	echo '<div id="topDoc">';
	echo '<img src="' . PUBLIC_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br><br>';
	echo '</div>';
}
?>
<div id="page-wrapper">
<?php
//Get International Translation Information
$INT = $FormFeaturesObj->getInternationalTranslation($OrgID);
if ($INT ['InternationalTranslation'] == "Y") {
	?>
	<div id="google_translate_element"></div><br>
	<script>
	function googleTranslateElementInit() {
  			new google.translate.TranslateElement({
	    	pageLanguage: 'en',
	    	layout: google.translate.TranslateElement.InlineLayout.SIMPLE
    	}, 'google_translate_element');
	}
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<?php } ?>