<?php
if ($MultiOrgIDck != "") {
	// Get Header From CustomHeaderFooter Information
	$results	=	G::Obj ( 'CustomHeaderFooter' )->getCustomHeaderFooterMultiOrgID ( $MultiOrgIDck );
	$IsURL 		=	$results ['IsURL'];
	$Header 	=	$results ['Header'];
} else {
	// Get Header From CustomHeaderFooter Information
	$results	=	G::Obj ( 'CustomHeaderFooter' )->getCustomHeaderFooter ( $OrgID );
	$IsURL		=	$results ['IsURL'];
	$Header		=	$results ['Header'];
}

if ($IsURL == "Y") {
	
	$options = array (
			'http' => array (
						'method' => "GET",
						'header' => "Accept-language: en\r\n" . "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"  // i.e. An iPad
						) 
	);
	
	$Header = file_get_contents ( $Header, false, stream_context_create ( $options ) );
}


$system_styles = "\n";
$system_styles .= "<meta http-equiv=\"Content-Security-Policy\" content=\"upgrade-insecure-requests\">";
$system_styles .= "<meta property=\"og:title\" content='".$req_results[Title]."'>";
$system_styles .= "<meta property=\"og:description\" content='".strip_tags($description)."'>";
$system_styles .= "<meta property=\"og:site_name\" content='IRecruit'>";
$system_styles .= "<meta property=\"og:url\" content='".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI]."'>";
$system_styles .= "<meta property=\"og:image\" content='".$fileurl ."'>";

$system_styles .= "<meta property=\"twitter:title\" content='".$req_results[Title]."'>";
$system_styles .= "<meta property=\"twitter:description\" content='".strip_tags($description)."'>";
$system_styles .= "<meta property=\"twitter:card\" content='summary_large_image'>";
$system_styles .= "<meta property=\"twitter:image\" content='".$fileurl."'>";



$system_styles .= "\n" . '<link href="' . PUBLIC_HOME . 'css/bootstrap.min.css"	rel="stylesheet">';

$system_styles .= "\n" . '<link href="' . PUBLIC_HOME . 'css/style.css" rel="stylesheet">';

if(file_exists(PUBLIC_DIR . 'images/'.$OrgID.'/style.css')) {
	$system_styles .= "\n" . '<link href="' . PUBLIC_HOME . 'images/'.$OrgID.'/style.css" rel="stylesheet">';	
}

$system_styles .= "\n" . '<link href="' . PUBLIC_HOME . 'font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">';

echo str_replace ( "{SYSTEM_STYLES}", $system_styles, $Header );

$user_detail_info       =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "UserPortalThemeID");
$up_app_theme_info      =   $UserPortalInfoObj->getUserPortalApplicationThemeInfo($user_detail_info['UserPortalThemeID']);
?>
<link href="<?php echo PUBLIC_HOME?>css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="<?php echo PUBLIC_HOME?>css/job-application-steps.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var up_active_step_tab = '<?php if(!isset($up_app_theme_info['ActiveTab']) || $up_app_theme_info['ActiveTab'] == '') { echo '#6b92c6'; } else { echo "#".$up_app_theme_info['ActiveTab']; }?>';
</script>

<style type="text/css">
.public_application_steps li a.public_application_step_tabs {
    color: white;
    text-decoration: none;
	padding: 7px 7px 7px 7px;
	background: <?php if(!isset($up_app_theme_info['PendingTab']) || $up_app_theme_info['PendingTab'] == '') { echo '#8994a5'; } else { echo "#".$up_app_theme_info['PendingTab']; }?>;
    position: relative;
    display: block;
    float: left;
}
.public_application_steps li a.public_application_step_tabs_filled {
    color: white;
    text-decoration: none;
	padding: 7px 7px 7px 7px;
	background: <?php if(!isset($up_app_theme_info['CompletedTab']) || $up_app_theme_info['CompletedTab'] == '') { echo '#4889f2'; } else { echo "#".$up_app_theme_info['CompletedTab']; }?>;
    position: relative;
    display: block;
    float: left;
}

@media (max-width: 767px) {
    .public_application_steps li a.public_application_step_tabs {
        color: white;
        text-decoration: none;
    	padding: 7px 7px 7px 7px;
    	background: <?php if(!isset($up_app_theme_info['PendingTab']) || $up_app_theme_info['PendingTab'] == '') { echo '#8994a5'; } else { echo "#".$up_app_theme_info['PendingTab']; }?>;
        position: relative;
        display: block;
        float: left;
        width:99%;
    }
    .public_application_steps li a.public_application_step_tabs_filled {
        color: white;
        text-decoration: none;
    	padding: 7px 7px 7px 7px;
    	background: <?php if(!isset($up_app_theme_info['CompletedTab']) || $up_app_theme_info['CompletedTab'] == '') { echo '#4889f2'; } else { echo "#".$up_app_theme_info['CompletedTab']; }?>;
        position: relative;
        display: block;
        float: left;
        width:99%;
    }
}
.navbar {
	background-color: <?php if(!isset($up_app_theme_info['Navbar']) || $up_app_theme_info['Navbar'] == '') { echo '#0FA5E4'; } else { echo "#".$up_app_theme_info['Navbar']; }?>;
	box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.33);
}
</style>
<script src="<?php echo PUBLIC_HOME;?>js/jquery-1.11.1.min.js"></script>
<script src="<?php echo PUBLIC_HOME;?>js/jquery-browser-migration.js"></script>
<script src="<?php echo PUBLIC_HOME;?>js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo PUBLIC_HOME;?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo PUBLIC_HOME;?>js/process-job-application.js"></script>
<?php
require_once COMMON_DIR . "application/ApplicationJavaScript.inc";
?>
<script>
var public_home =	'<?php echo PUBLIC_HOME;?>';
var lst			=	'<?php echo $lst;?>';
var modal		=	'<?php echo $modal;?>';
</script>
<script src="<?php echo PUBLIC_HOME;?>js/common.js" type="text/javascript"></script>
<div id="page-wrapper">
