<?php
if ($MultiOrgIDck != "") {
	// Get Header From CustomHeaderFooter Information
	$results		=	G::Obj ( 'CustomHeaderFooter' )->getCustomHeaderFooterMultiOrgID ( $MultiOrgIDck );
	$IsURL			=	$results ['IsURL'];
	$Footer			=	$results ['Footer'];
	$FooterScripts	=	$results ['FooterScripts'];
} else {
	// Get CustomHeaderFooter OrgID
	$results		=	G::Obj ( 'CustomHeaderFooter' )->getCustomHeaderFooter ( $OrgID );
	$IsURL			=	$results ['IsURL'];
	$Footer			=	$results ['Footer'];
	$FooterScripts	=	$results ['FooterScripts'];
}

if ($IsURL == "Y") {

	$options = array (
			'http' => array (
					'method' => "GET",
					'header' => "Accept-language: en\r\n" . "User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.102011-10-16 20:23:10\r\n"  // i.e. An iPad
						) 
	);
	
	$Footer = file_get_contents ( $Footer, false, stream_context_create ( $options ) );
}

echo '</div>';

if (isset($whitelist) 
	&& in_array(PUBLIC_DIR, $whitelist)
	&& isset($OrgID)
	&& defined('PUBLIC_DIR')
	&& file_exists(PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc')) {
	include_once PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc';
}

echo $Footer;
