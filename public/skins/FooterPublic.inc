</div>
<div style="clear:both;"></div>
<div id="bottomDoc" style="text-align: center">
	<p>
        <?php
        $bottomnav = array (
                    "terms" => "Terms of Use",
                    "policy" => "Privacy Policy"
        );
        
        if($brand_info['BrandID'] == "0") {
             ?><a href="http://www.irecruit-software.com" target="_blank">Powered by iRecruit</a><?php
        }
        else {
             if($brand_org_info['Url'] != "") {
                ?><a href="<?php echo $brand_org_info['Url'];?>" target="_blank"><?php echo $brand_org_info['FooterText'];?></a><?php
             }
             else {
             	echo $brand_org_info['FooterText'];
             }
        }

        foreach ( $bottomnav as $section => $displaytitle ) {
        	echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
        	echo '<a href="';
		    echo PUBLIC_HOME . 'index.php?';
        	echo 'OrgID=' . $OrgID;
        	if ($MultiOrgID) {
                        echo '&MultiOrgID=' . $MultiOrgID;
        	}
        	echo '&pg=' . $section . '" target="_blank">' . $displaytitle . '</a>';
        }
	    ?>
    </p>
</div>
<?php
if (isset($whitelist) 
    && in_array(PUBLIC_DIR, $whitelist)
    && in_array($OrgID, $whitelist)
    && isset($OrgID)
    && defined('PUBLIC_DIR')
    && file_exists(PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc')) {
    include_once PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc';
}
?>
</body>
</html>