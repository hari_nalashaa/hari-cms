<?php 
require_once 'public.inc';

//Set Request Parameters
if(isset($_REQUEST['FormID']) && $_REQUEST['FormID'] != "") $FormID = $_REQUEST['FormID'];
//Get Text Based on TextInfo and FormID
$ApplicantAuthorizations = $FormFeaturesObj->getTextFromTextBlocks($OrgID, $FormID, 'ApplicantAuthorizations');

//Get International Translation Information
$INT = $FormFeaturesObj->getInternationalTranslation($OrgID);

if ($INT['InternationalTranslation'] == "Y") {
?>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script><script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<?php
}
echo $ApplicantAuthorizations;
?>