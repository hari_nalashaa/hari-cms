<?php
require_once 'public.inc';
include_once PUBLIC_DIR . 'JobApplicationRequestVars.inc';

$title  =   "Job Application";

require_once PUBLIC_DIR . 'PreValidateApplication.inc';

require_once PUBLIC_DIR . 'PrescreenCheck.inc';

require_once PUBLIC_DIR . 'Header.inc';

require_once PUBLIC_DIR . 'PublicPortalApplication.inc';

require_once PUBLIC_DIR . 'Footer.inc';
?>
