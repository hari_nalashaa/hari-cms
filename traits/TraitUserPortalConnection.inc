<?php 
trait UserPortalConnection {
	static protected $DB_CON_USERPORTAL;
	static protected $USERPORTAL_DSN;
	static protected $userportal_error = array();
	
	// Set options
	static protected $userportal_options = array(
			PDO::ATTR_PERSISTENT    => false,
			PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION	//PDO::ERRMODE_EXCEPTION
			//PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	);

	/**
	 * @method			getUserPortalConnectionObject
	 * @return			UserPortal DatabaseConnection Object
	 */
	public static function getUserPortalConnectionObject() {
	
		if(!self::$DB_CON_USERPORTAL) {
			//Create PDO connection for USERPORTAL
			try {
				self::$USERPORTAL_DSN = 'mysql:host=' . DB_SERVER_USERPORTAL . ';dbname=' . DB_SCHEMA_USERPORTAL;
				self::$DB_CON_USERPORTAL = new PDO(self::$USERPORTAL_DSN, DB_USER_USERPORTAL, DB_PASS_USERPORTAL, self::$userportal_options);
				
				self::$DB_CON_USERPORTAL->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				self::$DB_CON_USERPORTAL->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			// Catch any errors
			catch(PDOException $e) {
				self::$userportal_error['DB_CON_USERPORTAL'] = $e->getMessage();
			}
			return self::$DB_CON_USERPORTAL;
		}
		
		return self::$DB_CON_USERPORTAL;
	}

}

?>