<?php 
trait TwilioSettings {

	public $db						=	"";
	public $conn_string       		=   "IRECRUIT";
	
	/**
	 * @method			getTwilioSettings
	 * @return			Twilio Settings
	 */
	public function getTwilioSettings() {
		
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
		
		global $OrgID;

		$twilio_account_info			=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
	}

}
?>