<?php 
trait AdminConnection {
	static protected $DB_CON_ADMIN;
	static protected $ADMIN_DSN;
	static protected $error = array();

	// Set options
	static protected $admin_options = array(
			PDO::ATTR_PERSISTENT    => false,
			PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION	//PDO::ERRMODE_EXCEPTION
			//PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	);
	/**
	 * @method			getAdminConnectionObject
	 * @return			Admin DatabaseConnection Object
	 */
	public static function getAdminConnectionObject() {
	
		if(!self::$DB_CON_ADMIN) {
			//Create PDO connection for WOTC
			try {
				self::$ADMIN_DSN  = 'mysql:host=' . DB_SERVER_ADMIN . ';dbname=' . DB_SCHEMA_ADMIN;
				self::$DB_CON_ADMIN = new PDO(self::$ADMIN_DSN, DB_USER_ADMIN, DB_PASS_ADMIN, self::$admin_options);
				
				self::$DB_CON_ADMIN->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				self::$DB_CON_ADMIN->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			// Catch any errors
			catch(PDOException $e) {
				self::$error['DB_CON_ADMIN'] = $e->getMessage();
			}
			return self::$DB_CON_ADMIN;
		}
	
		return self::$DB_CON_ADMIN;
	}

}
?>