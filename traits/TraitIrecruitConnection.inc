<?php 
trait IrecruitConnection {
	static protected $DB_CON_IRECRUIT;
	static protected $IRECRUIT_DSN;
	static protected $irecruit_error = array();
	
	// Set options
	static protected $irecruit_options = array(
			PDO::ATTR_PERSISTENT    => false,
			PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION	//PDO::ERRMODE_EXCEPTION
			//PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	);

	/**
	 * @method			getIrecruitConnectionObject
	 * @return			Irecruit DatabaseConnection Object
	*/
	public static function getIrecruitConnectionObject() {
		
		if(!self::$DB_CON_IRECRUIT) {
			//Create PDO connection for IRECRUIT
			try {
				self::$IRECRUIT_DSN = 'mysql:host=' . DB_SERVER_IRECRUIT . ';dbname=' . DB_SCHEMA_IRECRUIT;

				self::$DB_CON_IRECRUIT = new PDO(self::$IRECRUIT_DSN, DB_USER_IRECRUIT, DB_PASS_IRECRUIT, self::$irecruit_options);

				self::$DB_CON_IRECRUIT->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				self::$DB_CON_IRECRUIT->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			// Catch any errors
			catch(PDOException $e) {
				self::$irecruit_error['DB_CON_IRECRUIT'] = $e->getMessage();
				require_once 'Offline.inc';
			}
			return self::$DB_CON_IRECRUIT;
		}

		return self::$DB_CON_IRECRUIT;
	}

}
?>
