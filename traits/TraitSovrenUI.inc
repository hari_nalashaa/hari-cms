<?php
trait SovrenUI {
	
    var $ai_mat_doc     =   array(
                                "UIOptions" => array(
                                    "Username"              =>  "",
                                    "ShowMatchCriteria"     =>  1,
                                    "ShowFilterCriteria"    =>  1,
                                    "ExecuteImmediately"    =>  "",
                                    "ShowBanner"            =>  1,
                                    "ShowWeights"           =>  1,
                                    "ShowDetailsButton"     =>  1,
                                    "ShowFindSimilar"       =>  1,
                                    "ShowWebSourcing"       =>  1,
                                    "ShowJobBoards"         =>  1,
                                    "ShowSavedSearches"     =>  1,
                                    "CustomValuePicklists"  =>  array(
                                        "0" => array(
                                            "Label" => "",
                                            "Options" => array
                                            (
                                                "0" => array
                                                (
                                                    "Value" => "",
                                                    "Text" => "",
                                                ),
                            
                                            ),
                                        ),
                                    ),
                                    "SkillsAutoCompleteCustomSkillsList" => [],
                                ),
                                "SaasRequest" => array
                                (
                                    //"IndexIdsToSearchInto" => $info["IndexIdsToSearchInto"],
                                    "IndexIdsToSearchInto" => ["resumes", "32620361|sovren-demo-resumes-small", "32620361|sovren-demo-resumes-large"]
                                ),
                                "ParseOptions" => array
                                (
                                    "Configuration"     =>  "",
                                    "SkillsData"        =>  [],
                                    "NormalizerData"    =>  ""
                                ),
                                "GeocodeOptions" => [],
                            );
    
}
?>