<?php 
trait WotcConnection {
	static protected $DB_CON_WOTC;
	static protected $WOTC_DSN;
	static protected $error = array();

	// Set options
	static protected $wotc_options = array(
			PDO::ATTR_PERSISTENT    => false,
			PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION	//PDO::ERRMODE_EXCEPTION
			//PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
	);
	
	/**
	 * @method			getWotcConnectionObject
	 * @return			Wotc DatabaseConnection Object
	 */
	public static function getWotcConnectionObject() {
		
		if(!self::$DB_CON_WOTC) {
			//Create PDO connection for WOTC
			try {
				self::$WOTC_DSN = 'mysql:host=' . DB_SERVER_WOTC . ';dbname=' . DB_SCHEMA_WOTC;
				self::$DB_CON_WOTC = new PDO(self::$WOTC_DSN, DB_USER_WOTC, DB_PASS_WOTC, self::$wotc_options);
				
				self::$DB_CON_WOTC->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				self::$DB_CON_WOTC->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			// Catch any errors
			catch(PDOException $e) {
				self::$error['DB_CON_WOTC'] = $e->getMessage();
			}
			return self::$DB_CON_WOTC;
		}
		
		return self::$DB_CON_WOTC;
	}

}
?>