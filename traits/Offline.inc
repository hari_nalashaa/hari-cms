<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>iRecruit Maintenance Outage</title>
</head>
<body>
<div style="margin:20px 0 0 20px;">
<a href="http://www.irecruit-software.com/" target="_blank"><img border="0" src="<?php echo USERPORTAL_HOME ?>images/iRecruit.png" width="130"></a>
</div>
<div style="margin:10px 0 0 30px;font-family: Arial, Helvetica, sans-serif;width:400px;">
is currently offline for scheduled maintenance of its server and database. We are sorry for this inconvenience and will return shortly. 
</div>
</body>
</html>
