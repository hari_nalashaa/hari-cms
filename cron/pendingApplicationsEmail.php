#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

G::Obj('GenericQueries')->conn_string    =   "USERPORTAL";
$sel_pending_apps       =   "SELECT UA.OrgID, UA.MultiOrgID, UA.UserID, UA.RequestID, UA.Status,";
$sel_pending_apps       .=  " DATE_FORMAT(UA.LastUpdatedDate,'%m/%d/%Y') AS LastUpdatedDate,";
$sel_pending_apps       .=  " (SELECT U.Email FROM Users as U WHERE U.UserID = UA.UserID) as Email,";
$sel_pending_apps       .=  " (SELECT U.FirstName FROM Users as U WHERE U.UserID = UA.UserID) as FirstName,";
$sel_pending_apps       .=  " (SELECT U.LastName FROM Users as U WHERE U.UserID = UA.UserID) as LastName";
$sel_pending_apps       .=  " FROM UserApplications as UA";
$sel_pending_apps       .=  " WHERE UA.Status = 'Pending' AND UA.Deleted = 'No' AND UA.LastUpdatedDate <= NOW()";
$sel_pending_apps       .=  " ORDER BY UA.OrgID";
$pending_apps_results   =   G::Obj('GenericQueries')->getInfoByQuery($sel_pending_apps);
$pending_apps_count     =   count($pending_apps_results);
G::Obj('GenericQueries')->conn_string    =   "IRECRUIT";

$log = "";
$sent_cnt = 0;
for ($i = 0; $i < $pending_apps_count; $i ++) {
    
    $sel_req_details = "SELECT Title, DATE_FORMAT(ExpireDate, '%m/%d/%Y') as RequisitionExpiryDate FROM Requisitions";
    $sel_req_details .= " WHERE OrgID = :OrgID";
    $sel_req_details .= " AND MultiOrgID = :MultiOrgID";
    $sel_req_details .= " AND RequestID = :RequestID";
    $sel_req_details .= " AND DATEDIFF(DATE(ExpireDate), DATE(NOW())) > 0";
    $sel_req_details .= " AND DATEDIFF(DATE(ExpireDate), DATE(NOW())) <= 5";
    $sel_req_details .= " AND DATE(ExpireDate) >= DATE(NOW())";
    $params         =   array(
                            ':OrgID'        =>  $pending_apps_results[$i]['OrgID'],
                            ':MultiOrgID'   =>  $pending_apps_results[$i]['MultiOrgID'],
                            ':RequestID'    =>  $pending_apps_results[$i]['RequestID']                            
                        );
    $req_details_results   =   G::Obj('GenericQueries')->getInfoByQuery($sel_req_details, array($params));
    $req_details = $req_details_results[0];
    
    if (isset($req_details['Title']) && $req_details['Title'] != "") {
        
        $first_name     =   $pending_apps_results[$i]['FirstName'];
        $last_name      =   $pending_apps_results[$i]['LastName'];
        $job_title      =   $req_details['Title'];
        $expire_date    =   $req_details['RequisitionExpiryDate'];
        
        $OrgID          =   $pending_apps_results[$i]['OrgID'];
        $MultiOrgID     =   $pending_apps_results[$i]['MultiOrgID'];
        $RequestID      =   $pending_apps_results[$i]['RequestID'];
        $UserID         =   $pending_apps_results[$i]['UserID'];
       
	$OE		=   array(); 
        $OE             =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, "");
        $OrgName        =   $OrganizationDetailsObj->getOrganizationName($OrgID, "");
        
        // Multiple recipients
        $to             =   $pending_apps_results[$i]['Email']; // note the comma
        $portalurl      =   "https://www.myirecruit.com/login.php/index.php?";
        $portalurl .= "OrgID=" . $OrgID;
        
        if ($MultiOrgID != "") {
            $portalurl .= "&MultiOrgID=" . $MultiOrgID;
        } 
        
        // Get email message
        $email_template =   $UserPortalEmailTemplateObj->getUserPortalEmailTemplate($OrgID);
        
        if ($email_template['EmailMessage'] == "") {
            $message = "Dear {last} {first}," . "<br><br>";
            $message .= "Please complete the application for this position {JobTitle}, on or before {ExpiryDate}.<br>";
            $message .= "<br>{PortalURL}<br>";
            $message .= '<br>Regards,<br>Hiring Manager.';
        } else {
            $message = $email_template['EmailMessage'];
        }
        
        if ($email_template['Subject'] == "") {
            $subject = 'Please complete the pending application before it expires';
        } else {
            $subject = $email_template['Subject'];
        }
        
        $subject = str_replace("{first}", $first_name, $subject);
        $subject = str_replace("{last}", $last_name, $subject);
        $subject = str_replace("{JobTitle}", $job_title, $subject);
        $subject = str_replace("{ExpiryDate}", $expire_date, $subject);
        $subject = str_replace("{PortalURL}", $portalurl, $subject);
        $subject = str_replace("{OrganizationName}", $OrgName, $subject);
        
        $message = str_replace("{first}", $first_name, $message);
        $message = str_replace("{last}", $last_name, $message);
        $message = str_replace("{JobTitle}", $job_title, $message);
        $message = str_replace("{ExpiryDate}", $expire_date, $message);
        $message = str_replace("{PortalURL}", $portalurl, $message);
        $message = str_replace("{OrganizationName}", $OrgName, $message);
       
	$PHPMailerObj = new PhpMailer(); 
        
        // Set who the message is to be sent to
        $PHPMailerObj->addAddress($to);
        // Set an alternative reply-to address
        if (($OE['Email'] != "") && ($OE['EmailVerified'] == 1)) {
            // Set who the message is to be sent from
            $PHPMailerObj->setFrom($OE['Email'], $OrgName);
            // Set an alternative reply-to address
            $PHPMailerObj->addReplyTo($OE['Email'], $OrgName);
        }
        
        // Set the subject line
        $PHPMailerObj->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        $PHPMailerObj->msgHTML($message);
        // Content Type Is HTML
        $PHPMailerObj->ContentType = 'text/html';
        
        if ($to != "") {
            // Send email
            $PHPMailerObj->send();
            $sent_cnt ++;
            $log .= $sent_cnt;
            $log .= "-" . $to . "<br>\n";
            // Update Last Notification Date Time
            $UserPortalInfoObj->updLastNotificationDateTime($OrgID, $MultiOrgID, $RequestID, $UserID);
        }
    } // end if
} // end for

$application = "pendingApplicationsEmail.php";
$status = $log . $sent_cnt . " reminders sent - script okay";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
