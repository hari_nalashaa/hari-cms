#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";

$query = "SELECT wotcID, ApplicationID, FirstName, MiddleName, LastName, EntryDate FROM ApplicantData WHERE wotcID = 'shoeshow33' and NativeAmerican = 'Y' and EntryDate > date_sub(now(),INTERVAL 3 MONTH)";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("CMS WOTC")
       ->setLastModifiedBy("System")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Not Active")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('wotcID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('First Name');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Middle Name');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Last Name');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Entry Date');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


  $n=1;
  foreach ($RESULTS AS $DATA) {
    $n++;

    $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($DATA['wotcID']);
    $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($DATA['ApplicationID']);
    $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($DATA['FirstName']);
    $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($DATA['MiddleName']);
    $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($DATA['LastName']);
    $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($DATA['EntryDate']);

  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Native American Qualified");


$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

$subject = "Native American Report" . date("m/d/Y");

$message .= "Hi Lori," . "<br><br>\n\n";

$message .= "Here is your WOTC report showing applications that indicate native american qualification.<br><br>\n\n";
$message .= "Data represents the last 3 months from: " . date("m/d/Y") . "<br><br>\n\n";

$message .= "Thank you!<br>\n";
$message .= "<strong>WOTC Processing Team</strong><br>\n";
$message .= "860-269-3044 | <a href=\"https://www.cmswotc.com\">www.cmswotc.com</a><br>\n";

// Set who the message is to be sent to
$PHPMailerObj->addAddress("lcheek@shoeshow.com", "Lori Cheek");
$PHPMailerObj->addBCC("lschneider@cmshris.com", "Lisa Schneider");
$PHPMailerObj->addBCC("dedgecomb@irecruit-software.com", "David Edgecomb");

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';

$tempfile=ROOT.'cron/temp/tmpreport.xlsx';
$objWriter= IOFactory::createWriter($objPHPExcel , 'Xlsx');
$objWriter->save($tempfile);

$data = file_get_contents($tempfile);

$PHPMailerObj->addStringAttachment($data, 'ShoeShow-NativeAmerican.xlsx', 'base64', 'text/html');

// Send email
$PHPMailerObj->send();

unlink($tempfile);

$application="wotcShoeShowNAReport.php";
$status="Done\n" . $rtn;

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>

