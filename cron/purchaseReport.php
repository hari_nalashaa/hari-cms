#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$where     =   array("PurchaseDate BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE()");
$params    =   array();
$PUR_RES   =   G::Obj('Purchases')->getPurchasesInfo("*", $where, "PurchaseDate DESC", array($params));
$RESULTS   =   $PUR_RES['results'];


// create array for Org Names
$where     =   array("MultiOrgID = ''");
$ORGS_RES  =   G::Obj('Organizations')->getOrgDataInfo("OrgID, OrganizationName", $where, '', array());
$ORGS      =   $ORGS_RES['results'];

$ORGNAME   =   array ();
foreach ( $ORGS as $ORG ) {
    $ORGNAME [$ORG ['OrgID']] = $ORG ['OrganizationName'];
} // end foreach

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("iRecruit")
       ->setLastModifiedBy("System")
       ->setTitle("Purchases Reconcile Report")
       ->setSubject("Excel")
       ->setDescription("Not Active")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('OrgID');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Organization Name');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('TransactionID');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Amount');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('CC Number');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('CC Type');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Purchase Date');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('UserID');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Full Name');


for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


  $n=1;
  foreach ($RESULTS AS $DATA) {
    $n++;

        $objPHPExcel->getActiveSheet()->getStyle('D'.$n)->getNumberFormat()->setFormatCode("#,##0.00");

       
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($DATA['OrgID']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($ORGNAME [$DATA['OrgID']]);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($DATA['TransactionID']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($DATA['AuthAmount']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($DATA['CCnumber']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($DATA['CCtype']);
$objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($DATA['PurchaseDate']);
$objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($DATA['UserID']);
$objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($DATA['FullName']);


  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Monthly Performance Totals");


$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

$subject = "Intuit Reconcile Report " . date("m/d/Y");

$message = "Here is this weeks reconcile report.<br>\nData is as of: " . date("m/d/Y") . "<br><br>\n\n";

$message .= "Thank you!<br>\n";
$message .= "<strong>iRecruit</strong><br>\n";

// Set who the message is to be sent to
$PHPMailerObj->addAddress("bkelly@chshris.com", "Brian Kelly");
$PHPMailerObj->addAddress ( "cmsaccting@cmshris.com" );
$PHPMailerObj->addBCC("dedgecomb@irecruit-software.com", "David Edgecomb");

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';

$tempfile=ROOT.'cron/temp/tmpreconcilereport.xlsx';
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$objWriter->save($tempfile);

$data = file_get_contents($tempfile);

$PHPMailerObj->addStringAttachment($data, 'PurchaseReconcileReport.xlsx', 'base64', 'text/html');

// Send email
$PHPMailerObj->send();

unlink($tempfile);

$application="purchaseReport.php";
$status="Done\n" . $rtn;

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>

