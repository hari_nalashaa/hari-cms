#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$application    =   "reverseAttachments.php";
$status		=  date("D M Y j G:i:s T") . "<br>\n";

$where          =   array();
//$where          =   array("OrgID = 'I20110901'");
$RES            =   $OrganizationsObj->getOrgDataInfo("DISTINCT(OrgID)", $where, 'OrgID');
$RESULTS        =   $RES['results'];

$i = 0;
$ii = 0;
foreach ($RESULTS as $OD) {
    $i ++;
    
    $dir = IRECRUIT_DIR . 'vault/' . $OD['OrgID'];
    
    if (! file_exists($dir)) {
        mkdir($dir, 0700);
        chmod($dir, 0777);
    }
    
    $dir .= '/applicantattachments';
    
    if (! file_exists($dir)) {
        mkdir($dir, 0700);
        chmod($dir, 0777);
    }

    
    if ($handle = opendir($dir)) {
        while (false !== ($entry = readdir($handle))) {
            if (substr($entry, 0, 2) == "IR") {
                
                $fileext = $dir . "/" . $entry;
                
                list ($appid, $type, $fullfile) = explode("-", $entry);
                list ($file, $ext) = explode(".", $fullfile);
                list ($partone, $parttwo) = explode("-", $file);

                if (filesize($fileext) > 0) {

                    if ($parttwo != "") { // rules out datestamp files

                        
                        $num        =   0;
                        $params     =   array(':OrgID'=>$OD['OrgID'], ':ApplicationID'=>$appid, ':PurposeName'=>$type . '-' . $file, ':FileType'=>$ext);
                        $query      =   "SELECT COUNT(*) AS AttachmentsCount FROM ApplicantAttachments WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND PurposeName = :PurposeName AND FileType = :FileType";
                        $att_info   =   G::Obj('GenericQueries')->getRowInfoByQuery($query, array($params));
                        $num        =   $att_info['AttachmentsCount'];   

			if (strtolower($type) == "resume") { $typeatt = "resumeupload"; }
			else if (strtolower($type) == "cover") { $typeatt = "coverletterupload"; }
			else if (strtolower($type) == "other") { $typeatt = "otherupload"; }
			else { $typeatt = $type; }
                        
                        if ($num == 0) {

				//Applicant Attachments Information
                        	$applicant_attach_info   =   array (
                                                                "OrgID"            =>  $OD['OrgID'],
                                                                "ApplicationID"    =>  $appid,
                                                                "TypeAttachment"   =>  $type,
                                                                "PurposeName"      =>  $type . '-' . $file,
                                                    "FileType"         =>  $ext
                                                 );
                        	//Insert applicant attachments information
                        	G::Obj('Attachments')->insApplicantAttachments ( $applicant_attach_info );

				$status .= "Not in DB: " . $OD['OrgID'] . '--' . $appid;
				$status .= '---' . $typeatt . '--' . $type . '-' . $file . '--' . $ext . "<br>\n";
				$status .= 'll ' . $dir . '/' . $appid . "*<br>\n";
				$ii++;


                        } // end num 0
                    } // end if ext
                }

            } // end if file IR
        } // end close dir
        closedir($handle);
    } // end if open dir
    
    if ($i % 10 == 0) {
        $CronStatusLogsObj->insUpdCronStatusLog($application, $i);
    }
} // end foreach RESULTS

$status .= "<br>\n";
$status .= "Total Not in DB: " . $ii . "<br>\n";
$status .= "<br>\n";
$status .= date("D M Y j G:i:s T");

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
