#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

// This script will create an Index table for searchs.
// It updates once a day to refresh changed data for all applicants
$query = "SET SESSION group_concat_max_len = 8192";
G::Obj('GenericQueries')->updInfoByQuery($query);

$query  =   "SELECT OrgID, ApplicationID, RequestID";
$query  .=  " FROM JobApplications";

if ($argv[1] == "entirely") {
    $query .= " WHERE LastModified > date_sub(now(), interval 1 DAY)";
} else {
    $query .= " WHERE  (ApplicationAnswers = '' or Comments = '')";
    $query .= " AND LastModified > date_sub(now(), interval 3 MONTH)";
}

$query  .=  " GROUP BY OrgID, ApplicationID, RequestID";
$query  .=  " ORDER BY OrgID, ApplicationID, RequestID";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

foreach ($RESULTS as $JA) {
    
    if ($argv[1] != "entirely") {
        updateApplicationAnswers($JA['OrgID'], $JA['ApplicationID'], $JA['RequestID']);
    }
    
    updateComments($JA['OrgID'], $JA['ApplicationID'], $JA['RequestID']);
} // end foreach

if ($argv[1] == "entirely") {
    
    $application = "updateSearchFields.php";
    $status = "script okay";
    
    // Insert Cron Status Logs
    $CronStatusLogsObj->insUpdCronStatusLog($application, $status);
}

function updateApplicationAnswers($OrgID, $ApplicationID, $RequestID)
{
    $query      =   "SELECT RequisitionID, JobID";
    $query      .=  " FROM Requisitions";
    $query      .=  " WHERE OrgID = :orgid";
    $query      .=  " AND RequestID = :requestid";
    $params     =   array(':orgid' => $OrgID, ':requestid' => $RequestID);
    $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
    $RequisitionID  =   $RESULTS[0]['RequisitionID'];
    $JobID          =   $RESULTS[0]['JobID'];
    
    // add application answers
    $set .= " (SELECT CONCAT(AD.ApplicationID,' ','" . str_replace("'", "", $RequisitionID) . " - ";
    $set .= str_replace("'", "", $JobID) . "',  GROUP_CONCAT(AD.Answer))";
    $set .= " FROM ApplicantData AD";
    $set .= " WHERE AD.OrgID = :sorgid and AD.ApplicationID = :sapplicationid";
    $set .= " GROUP BY AD.ApplicationID)";
    $set_info   =   array("ApplicationAnswers = $set");
    $where_info =   array("OrgID = :orgid", "ApplicationID = :applicationid", "RequestID = :requestid");
    $params     =   array(
                        ':sorgid'           =>  $OrgID,
                        ':sapplicationid'   =>  $ApplicationID,
                        ':orgid'            =>  $OrgID,
                        ':applicationid'    =>  $ApplicationID,
                        ':requestid'        =>  $RequestID
                    );
    G::Obj('GenericQueries')->updRowsInfo("JobApplications", $set_info, $where_info, array($params));
} // end function

function updateComments($OrgID, $ApplicationID, $RequestID)
{
    // add comments
    $query      =   "SELECT OrgID, ApplicationID, RequestID, group_concat(Comments) as 'Comments'";
    $query      .=  " FROM JobApplicationHistory";
    $query      .=  " WHERE OrgID = :orgid";
    $query      .=  " AND ApplicationID = :applicationid";
    $query      .=  " AND RequestID = :requestid";
    $query      .=  " GROUP BY ApplicationID, RequestID";
    $query      .=  " ORDER BY Date";
    $params     =   array(':orgid'=>$OrgID, ':applicationid'=>$ApplicationID, ':requestid'=>$RequestID);
    $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
    
    foreach ($RESULTS as $JAH) {
        
        $query      =   "UPDATE JobApplications";
        $query      .=  " SET Comments = :comments";
        $query      .=  " WHERE OrgID = :orgid";
        $query      .=  " AND ApplicationID = :applicationid";
        $query      .=  " AND RequestID = :requestid";
        $set_info   =   array("Comments = :comments");
        $where_info =   array("OrgID = :orgid", "ApplicationID = :applicationid", "RequestID = :requestid");
        $params     =   array(
                            ':comments'         =>  $JAH['Comments'],
                            ':orgid'            =>  $JAH['OrgID'],
                            ':applicationid'    =>  $JAH['ApplicationID'],
                            ':requestid'        =>  $JAH['RequestID']
                        );
        G::Obj('GenericQueries')->updRowsInfo("JobApplications", $set_info, $where_info, array($params));
        
    } // end foreach
} // end function
?>
