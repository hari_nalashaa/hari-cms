#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

//Organizations
$where                  =   array("OrgID = 'B12345467'");
$conversations_results  =   G::Obj('TwilioConversationInfo')->getAllConversations("*", $where, "OrgID", "OrgID", array());
$conversations_list     =   $conversations_results['results'];

$archived_conversations =   array();    //Archived Conversations List To Be Updated
$conversation_messages  =   array();
$con_res_info           =   array();

$limited_days           =   30; //Update this value based on Archived Days Count

for($cl = 0; $cl < count($conversations_list); $cl++) {
    
    $OrgID              =   $conversations_list[$cl]['OrgID'];

    $conversation_resources_info	=	G::Obj('TwilioConversationApi')->fetchAllConversationResources($OrgID);
    $conversation_resources			=	$conversation_resources_info['Response'];
    $conversation_resources_cnt     =   count($conversation_resources);
    
    $insert_info                    =   array();
    $conversation_ids               =   array();
    
    for($cr = 0; $cr < $conversation_resources_cnt; $cr++) {

        $ResourceID			=	$conversation_resources[$cr]->sid;
        
        //Get the Protected properties from SMS object, by extending the object through ReflectionClass
        $reflection         =   new ReflectionClass($conversation_resources[$cr]);
        $property           =   $reflection->getProperty("properties");
        $property->setAccessible(true);
        $conversation_resource_info     =   $property->getValue($conversation_resources[$cr]);
        
        $con_date_from_obj  =   $conversation_resource_info['dateCreated'];
        $con_date_to_obj    =   $conversation_resource_info['dateUpdated'];

        $con_date_from_info =   (array)$conversation_resource_info['dateCreated'];
        $con_date_to_info   =   (array)$conversation_resource_info['dateUpdated'];

        $con_earlier        =   new DateTime($con_date_from_obj->format("Y-m-d"));
        $con_later          =   new DateTime(date("Y-m-d"));
        
        $con_days           =   $con_later->diff($con_earlier)->format("%a");
        
        if($con_days > $limited_days) {

            $conversation_res_info  =   G::Obj('TwilioConversationInfo')->getConversationInfoByResourceID($ResourceID);
            $conversation_ids[]     =   $ResourceID;
            $conversation_users[$ResourceID]    =   $conversation_res_info['UserID'];
            
            $conversations		    =	G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $ResourceID);
            $conversation_res       =	$conversations['Response'];
            $conversation_res_cnt   =   (isset($conversation_res) && (is_object($conversation_res) || is_array($conversation_res))) ? count($conversation_res) : 0;
            
            $messages			=	array();
            for($c = 0; $c < $conversation_res_cnt; $c++) {
                
                $message_obj	=	$conversation_res[$c];
                
                //Get the Protected properties from SMS object, by extending the object through ReflectionClass
                if(is_object($message_obj)) {
                    $reflection     =   new ReflectionClass($message_obj);
                    $property       =   $reflection->getProperty("properties");
                    $property->setAccessible(true);
                    $messages[]		=   $msg_info   =   $property->getValue($message_obj);
                    
                    $message_info   =   array();
                    
                    $date_from_obj  =   $msg_info['dateCreated'];
                    $date_to_obj    =   $msg_info['dateUpdated'];
                    
                    $date_from_info =   (array)$msg_info['dateCreated'];
                    $date_to_info   =   (array)$msg_info['dateUpdated'];
                    
                    $message_info['ConversationResourceID']     =   $msg_info['conversationSid'];
                    $message_info['ConversationMessageID']      =   $msg_info['sid'];
                    $message_info['Author']                     =   $msg_info['author'];
                    $message_info['Body']                       =   utf8_decode($msg_info['body']);
                    $message_info['DateCreatedDate']            =   $date_from_info['date'];
                    $message_info['DateCreatedTimeZone_Type']   =   $date_from_info['timezone_type'];
                    $message_info['DateCreatedTimeZone']        =   $date_from_info['timezone'];
                    $message_info['DateUpdatedDate']            =   $date_to_info['date'];
                    $message_info['DateUpdatedTimeZone_Type']   =   $date_to_info['timezone_type'];
                    $message_info['DateUpdatedTimeZone']        =   $date_to_info['timezone'];
                    $message_info['CreatedDateTime']            =   "NOW()";
                    
                    //Skip fields
                    $skip_fields    =   array_keys($message_info);
                    array_pop($skip_fields);
                    
                    $earlier    =   new DateTime($date_from_obj->format("Y-m-d"));
                    $later      =   new DateTime(date("Y-m-d"));
                    
                    $days       =   $later->diff($earlier)->format("%a");
                    
                    //Assign Data To Arrays
                    $conversation_days[$ResourceID][]   =   $days;
                    $conversation_messages[$ResourceID][$msg_info['sid']]           =   $message_info;
                    $conversation_msg_skip_fields[$ResourceID][$msg_info['sid']]    =   $skip_fields;
                    $con_res_info[$ResourceID]  =   $conversation_res_info;
                }

            }
            
        }
        
    }
    
    //Archived Conversations
    foreach ($conversation_ids as $conversation_resource_id) {
        
        $archive_conv_flag  =   "true";
        
        $con_messages       =   $conversation_messages[$conversation_resource_id];
        $con_skip_fields    =   $conversation_msg_skip_fields[$conversation_resource_id];
        $con_msg_days       =   $conversation_days[$conversation_resource_id];
        $con_arch_info      =   $con_res_info[$conversation_resource_id];
        
        foreach ($con_msg_days as $msg_days) {
            if($msg_days <= $limited_days) {
                $archive_conv_flag  =   "false";
            }
        }
        
        if($archive_conv_flag == "true") {
            //Archived Conversations
            $archived_conversations[] =  $conversation_resource_id;
            
            //Insert Conversation Resource
            G::Obj('TwilioConversationInfo')->insTwilioConversationsArchive($con_arch_info);
            
            foreach ($con_messages as $con_msg_id=>$con_msg_info) {
                //Insert conversation messages
                $ins_res    =   G::Obj('TwilioConversationMessagesApi')->insertConversationMessages($con_msg_info, $con_skip_fields[$con_msg_id]);
                
                $insert_info[$conversation_resource_id][] =   $ins_res['affected_rows'];
            }
        }
    }
    
    //Get Failed Conversation List and Notify Failed Conversations
    $failed_conversations   =   array();
    foreach($insert_info as $failed_conversation_id=>$ins_res_info) {
        if(in_array(0, $ins_res_info)) {
            $failed_conversations[] = $failed_conversation_id;
            echo "Note: Some messages are failed to insert, please make sure to run it again."."<br>";
            echo $failed_conversation_id."<br>";
        }
    }
    
    //Delete successfully archived conversations
    $delete_result  =   array();
    $di = 0;
    foreach($archived_conversations as $conversation_id) {
        if(!in_array($conversation_id, $failed_conversations)) {
            //$USERID =   $conversation_users[$conversation_id]; //Presently Not Using
            
            $del_conversation_res   =   G::Obj('TwilioConversationApi')->deleteConversationResource($OrgID, $conversation_id, "CRON");
            
            $delete_result[$di]["ConversationID"]  =  $conversation_id;
            $delete_result[$di]["Code"]            =  $del_conversation_res['Code'];
            $delete_result[$di]["Message"]         =  $del_conversation_res['Message'];
            $di++;
        }
    }
    
    //Delete Empty Conversations
    foreach($conversation_ids as $conversation_id) {
        if(count($conversation_messages[$conversation_id]) == 0) {
            $del_conversation_res   =   G::Obj('TwilioConversationApi')->deleteConversationResource($OrgID, $conversation_id, "CRON");
            
            $delete_result[$di]["ConversationID"]  =  $conversation_id;
            $delete_result[$di]["Code"]            =  $del_conversation_res['Code'];
            $delete_result[$di]["Message"]         =  $del_conversation_res['Message'];
            $di++;
        }
    }
}

// Insert Cron Status Logs
G::Obj('CronStatusLogs')->insUpdCronStatusLog("insertAllConversationMessages.php", json_encode($delete_result));
?>
