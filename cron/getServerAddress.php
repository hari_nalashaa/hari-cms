#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$server = "/usr/home/irecruit/edge/iBackup";
$backup_server = "evs246.ibackup.com"; // 4/15/2021

$type = "";

if (strpos(realpath(__DIR__), '/usr/home/cmsdev') !== false) {
    $server = "/usr/home/cmsdev/edge/iBackup";
    $type = "Development";
}
if (strpos(realpath(__DIR__), '/usr/home/cmsadmin') !== false) {
    $server = "/usr/home/cmsadmin/edge/iBackup";
    $type = "Quality";
}
if (strpos(realpath(__DIR__), '/u2/release') !== false) {
    $server = "/usr/home/irecruit/edge/iBackup";
    $type = "Production";
}

$out = array();
$ok = "";

exec($server . '/idevsutil --getServerAddress brian_kelly --password-file="EVRHrD26"', $out);

foreach ($out as $v) {
    
    $match = preg_match("/$backup_server/", $v);
    
    if ($match === 1) {
        $ok = "Y";
    }
    
} // end foreach

$application = "getServerAddress.php";

$status = "";
$status .= "Server Checked: " . $type . ", ";

if ($ok != "Y") {
    $status .= "<span style=\"color:red;\">iBackup server has changed from " . $backup_server . ".<br>\n";
    $status .= "Current Server is: " . print_r($out,true) . "</span><br>\n";

    $status .= "<strong>Development</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/home/cmsdev/david/david-cms/cron/cron.txt - replace crontab";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/home/cmsdev/david/david-cms/cron/getServerAddress - update on production server";
    $status .= "<br>\n";

    $status .= "<strong>Quality</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/home/cmsadmin/edge/cron/cron.txt - replace crontab";
    $status .= "<br>\n";

    $status .= "<strong>Database</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/www/users/irecruit/edge/cron/cron.txt - replace crontab";
    $status .= "<br>\n";

    $status .= "<strong>Production</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/home/cmsdev/edge/cron/cron.txt - replace crontab<br>\n";
    $status .= "<br>\n";

    $status .= "<strong>CMS Marketing</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/home/cmsmarketing/edge/cron/cron.txt - replace crontab";
    $status .= "<br>\n";

    $status .= "<strong>WOTC</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;/usr/home/wotc/public_html/cron/cron.txt - replace crontab";
    $status .= "<br>\n";

    $status .= "<strong>Other</strong><br>\n";
    $status .= "&nbsp;&nbsp;&nbsp;AWS-FTP";
    $status .= "<br>\n";

} else {
    $status .= "<span style=\"color:green;\">Backup connection okay</span>";
}

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
