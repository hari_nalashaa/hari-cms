<?php
ini_set('display_errors', 0);
ini_set('memory_limit', '8192M');
error_reporting(E_ALL);

//Set variables defined
require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';
require_once VENDOR_DIR . 'autoload.php';

//Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';

?>
