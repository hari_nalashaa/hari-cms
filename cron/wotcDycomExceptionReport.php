#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";

$ssnmatch="";
$orgmatch="";

$nomatchcnt=0;
$nomatchlocationcnt=0;
$processedcnt=0;
$needsprocessingcnt=0;
$processednotqualifiedcnt=0;
$reconcilecnt=0;
$qualifiedandtermedcnt=0;
$total=0;

$locationcnt=0;
$statecnt=0;

$individualreports=array();

$query   =   "SELECT * FROM DycomHires";
$query  .=   " WHERE LastHiredDate > date_sub(now(), interval 1 month)";
//$query  .=   " AND TerminationDate = '0000-00-00 00:00:00'";
$query   .=   " ORDER BY FEDID, State, LastHiredDate ASC";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

$x=0;
foreach ($RESULTS AS $NEWHIRE) {
$x++;

  $query   =   "SELECT * FROM ApplicantData";
  $query  .=   " WHERE ApplicantData.wotcID in (select wotcID from OrgData where CompanyID = '62f41708245691')";
  $query  .=   " AND concat(ApplicantData.Social1,ApplicantData.Social2,ApplicantData.Social3) = :SSN";
  $query  .=   " ORDER BY ApplicantData.EntryDate DESC";
  $params  =   array(':SSN'    =>  $NEWHIRE['SSN']);
  $MATCH =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

  $query  = "SELECT OrgData.wotcID, OrgData.State, concat(OrgData.EIN1,OrgData.EIN2) AS EIN, IF(OrgData.CompanyIdentifier != '' ,OrgData.CompanyIdentifier , CRMFEIN.AKADBA) AS CompanyName";
  $query .= " FROM OrgData";
  $query .= " JOIN CRM ON CRM.CompanyID = OrgData.CompanyID";
  $query .=   " JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
  $query .= " WHERE CRM.CompanyID = '62f41708245691'"; // Dycom Industries
  $query .= " AND concat(OrgData.EIN1,OrgData.EIN2) = :FEDID";
  $query .= " AND OrgData.State = :State";
  $params  =   array(':FEDID'    =>  $NEWHIRE['FEDID'],':State'    =>  $NEWHIRE['State']);
  $ORGCK =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

  if (count($ORGCK) == 1) {
    $orgmatch="Y";
  } else if (count($ORGCK) > 1) {
    $orgmatch="M";
  } else {
    $orgmatch="N";
    $locationcnt++;
    $noorgmatch.="&nbsp;&nbsp;&nbsp;" . $locationcnt . "&nbsp;&nbspFEDID: " . $NEWHIRE['FEDID'] . ' ' . $NEWHIRE['State'] . "<br>\n";
  }

  if (count($MATCH) == 1) {
    $ssnmatch="Y";
  } else if (count($MATCH) > 1) {
    $ssnmatch="M";
  } else {
    $ssnmatch="N";

    $nomatchcnt++;

    $now = time(); // or your date as well
    $your_date = strtotime($NEWHIRE['LastHiredDate']);
    $datediff = $now - $your_date;

    if ($ckid != $NEWHIRE['FEDID']) {
      $nomatchlocationcnt++;
      $noappmatch.="<br>\n";
      $noappmatch.=" (" . $nomatchlocationcnt . ")";

      $noappmatch.=" " . $ORGCK[0]['CompanyName'];
      $noappmatch.=" - " . $NEWHIRE['FEDID'];
      $noappmatch.="-" . $NEWHIRE['State'];

  	$query  = "SELECT Name, EmailAddress";
  	$query .= " FROM DycomContacts";
  	$query .= " WHERE FEDID = :FEDID";
  	$params  =   array(':FEDID'    =>  $NEWHIRE['FEDID']);

  	$CONTACTS =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

	$i=0;
	foreach ($CONTACTS as $C) {
          $noappmatch.="<br> - Contact: " .  $C['Name'] . " " . $C['EmailAddress'];
	  $individualreports[$NEWHIRE['FEDID']]['Contact'][$i]['Name']=$C['Name'];
	  $individualreports[$NEWHIRE['FEDID']]['Contact'][$i]['EmailAddress']=$C['EmailAddress'];
	  $i++;
	}

      $noappmatch.="<br>\n";

    }

    $noappmatchreport="";
    if($NEWHIRE['TerminationDate'] == '0000-00-00 00:00:00') {
      $noappmatchreport.="&nbsp;&nbsp;" . round((28-$datediff / (60 * 60 * 24))) . " Days left to Process ";
      $noappmatchreport.="&nbsp;&nbsp;Employee Name: " . $NEWHIRE['EmployeeName'];
      $noappmatchreport.="&nbsp;&nbsp;Employee ID: " . $NEWHIRE['EmployeeID'];
      $noappmatchreport.="&nbsp;&nbsp;Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
      $noappmatchreport.="&nbsp;&nbsp;Work State: " . $NEWHIRE['State'];
      $noappmatchreport.="<br>\n";
    }

    $noappmatch.=$noappmatchreport;

    if ($noappmatchreport != "") {
      $individualreports[$NEWHIRE['FEDID']]['Employee'][]=$noappmatchreport;
    }
    $ckid = $NEWHIRE['FEDID'];

  }

  if (($ssnmatch == "M") || ($orgmatch == "M")) {

    $reconcilecnt++;
    $multi .= "&nbsp;&nbsp;&nbsp;";
    if($NEWHIRE['TerminationDate'] != '0000-00-00 00:00:00') {
      $multi .="&nbsp;&nbsp;TERMINATED";
      $multi .= "&nbsp;" . date("m/d/Y", strtotime($NEWHIRE['TerminationDate']));
    }
    $multi .= " Employee Name: " . $NEWHIRE['EmployeeName'];
    $multi .= " Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
    $multi .= " Wage: " . number_format($NEWHIRE['HourlyWage'],2,".",",");
    $multi .= " Position: " . $NEWHIRE['Position'];
    $multi .= "<br>\n";

    foreach ($ORGCK AS $O) {
      $multi .= "&nbsp;&nbsp;&nbsp;";
      $multi .= " FEDID: " . $O['EIN'];
      $multi .= " Organization Name: " . $O['CompanyName'];
      $multi .= " State: " . $O['State'];
      $multi .= " wotcID: " . $O['wotcID'];
      $multi .= "<br>\n";
    } //end foreach
   if (count($ORGCK) < 1) {
      $multi .= "&nbsp;&nbsp;&nbsp;";
      $multi .= $reconcilecnt . "&nbsp;&nbsp;";
      $multi .= " No FEDID Match: ";
      $multi .= $NEWHIRE['FEDID'] . ' ' . $NEWHIRE['State'] . "<br>\n";
   } // check for no org

    foreach ($MATCH AS $AD) {
      $multi .= "&nbsp;&nbsp;&nbsp;";
      $multi .= $reconcilecnt . "&nbsp;&nbsp;";
      $multi .= " Applicant SSN: " . $NEWHIRE['SSN'];
      $multi .= " Applicant Name: " . $NEWHIRE['EmployeeName'];
      $multi .= " ApplicationID: " . $AD['ApplicationID'];
      $multi .= " EntryDate: " . $AD['EntryDate'];
      $multi .= " wotcID: " . $AD['wotcID'];
      $multi .= " State: " . $AD['State'];
      $multi .= " EntryDate: " . $AD['EntryDate'];
      $multi .= " Name: " . $AD['FirstName'] . ' ' . $AD['MiddleName'] . '' . $AD['LastName'];
      $multi .= "<br>\n";
    } // end foreach AD

    $multi .= "<br>\n";

  } // end if match is multiple

  if (($ssnmatch == "Y") && ($orgmatch == "Y")) {

     $query  =   "SELECT Qualifies FROM ApplicantData";
     $query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
     $params =   array(':wotcid'=>$MATCH[0]['wotcID'], ':applicationid'=>$MATCH[0]['ApplicationID']);
     $Q      =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

     if($Q[0]['Qualifies'] == "Y") {

       if($NEWHIRE['TerminationDate'] != '0000-00-00 00:00:00') {

          $qualifiedandtermedcnt++;
          $qualifiedandtermed .= "&nbsp;&nbsp;" . $qualifiedandtermedcnt;
          if($NEWHIRE['TerminationDate'] != '0000-00-00 00:00:00') {
            $qualifiedandtermed .="&nbsp;&nbsp;TERMINATED";
            $qualifiedandtermed .= "&nbsp;" . date("m/d/Y", strtotime($NEWHIRE['TerminationDate']));
          }
          $qualifiedandtermed .= " Applicant SSN: " . $NEWHIRE['SSN'];
          $qualifiedandtermed .= " ApplicationID: " . $MATCH[0]['ApplicationID'];
          $qualifiedandtermed .= " wotcID: " . $MATCH[0]['wotcID'];
          $qualifiedandtermed .= " Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
          $qualifiedandtermed .= " Wage: " . number_format($NEWHIRE['HourlyWage'],2,".",",");
          $qualifiedandtermed .= " Position: " . $NEWHIRE['Position'];
          $qualifiedandtermed .= "<br>\n";

       } else {
     
     	  $query  =   "SELECT * FROM Processing";
     	  $query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
     	  $params =   array(':wotcid'=>$MATCH[0]['wotcID'], ':applicationid'=>$MATCH[0]['ApplicationID']);
     	  $P      =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

  	  if ($P[0]['wotcID'] != '') {
            $processedcnt++;
            $processed .= "&nbsp;&nbsp;" . $processedcnt;
            $processed .= " ALREADY PROCESSED -";
            $processed .= " Applicant SSN: " . $NEWHIRE['SSN'];
            $processed .= " ApplicationID: " . $MATCH[0]['ApplicationID'];
            $processed .= " wotcID: " . $MATCH[0]['wotcID'];
            $processed .= " Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
            $processed .= " Wage: " . number_format($NEWHIRE['HourlyWage'],2,".",",");
            $processed .= " Position: " . $NEWHIRE['Position'];
            $processed .= "<br>\n";
	  } else {
            $needsprocessingcnt++;

	    //Skip fields on duplicate key
	    $skip   =   array("wotcID", "ApplicationID");
	    //Information to insert
	    $info   =   array(
	                    "wotcID"            =>  $MATCH[0]['wotcID'],
	                    "ApplicationID"     =>  $MATCH[0]['ApplicationID'],
	                    "OfferDate"         =>  date("Y-m-d", strtotime($NEWHIRE['LastHiredDate'])),
	                    "HiredDate"         =>  date("Y-m-d", strtotime($NEWHIRE['LastHiredDate'])),
	                    "StartDate"         =>  date("Y-m-d", strtotime($NEWHIRE['LastHiredDate'])),
	                    "StartingWage"      =>  number_format($NEWHIRE['HourlyWage'],2,".",","),
	                    "Position"          =>  $NEWHIRE['Position'],
	                    "Received"          =>  date("Y-m-d", strtotime($NEWHIRE['LastHiredDate'])),
	                    "LastUpdated"       =>  "NOW()"
	                );
	    G::Obj('WOTCProcessing')->insUpdProcessing($info, $skip);

            $needsprocessing .= "&nbsp;&nbsp;" . $needsprocessingcnt;
            $needsprocessing .= " PROCESSED --";
            $needsprocessing .= " Applicant SSN: " . $NEWHIRE['SSN'];
            $needsprocessing .= " Employee Name: " . $NEWHIRE['EmployeeName'];
            $needsprocessing .= " ApplicationID: " . $MATCH[0]['ApplicationID'];
            $needsprocessing .= " wotcID: " . $MATCH[0]['wotcID'];
            $needsprocessing .= " Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
            $needsprocessing .= " Wage: " . number_format($NEWHIRE['HourlyWage'],2,".",",");
            $needsprocessing .= " Position: " . $NEWHIRE['Position'];
            $needsprocessing .= "<br>\n";

	  } 
       }

     } else {

       $processednotqualifiedcnt++;

       $notprocessed .= "&nbsp;&nbsp;" . $processednotqualifiedcnt;
       if($NEWHIRE['TerminationDate'] != '0000-00-00 00:00:00') {
          $notprocessed .="&nbsp;&nbsp;TERMINATED";
          $notprocessed .= "&nbsp;" . date("m/d/Y", strtotime($NEWHIRE['TerminationDate']));
       }
       $notprocessed .= " Applicant SSN: " . $NEWHIRE['SSN'];
       $notprocessed .= " ApplicationID: " . $MATCH[0]['ApplicationID'];
       $notprocessed .= " wotcID: " . $MATCH[0]['wotcID'];
       $notprocessed .= " Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
       $notprocessed .= " Wage: " . number_format($NEWHIRE['HourlyWage'],2,".",",");
       $notprocessed .= " Position: " . $NEWHIRE['Position'];
       $notprocessed .= "<br>\n";


     }
  } 

  if (($ssnmatch == "N") || ($orgmatch == "N")) {

    if ($MATCH[0]['wotcID'] != "") {
       $statecnt++;
       $state.= "&nbsp;&nbsp;" . $statecnt;
       if($NEWHIRE['TerminationDate'] != '0000-00-00 00:00:00') {
          $state .="&nbsp;&nbsp;TERMINATED";
          $state .= "&nbsp;" . date("m/d/Y", strtotime($NEWHIRE['TerminationDate']));
       }
       $state.= " Applicant SSN: " . $NEWHIRE['SSN'];
       $state.= " ApplicationID: " . $MATCH[0]['ApplicationID'];
       $state.= " wotcID: " . $MATCH[0]['wotcID'];
       $state.= " Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
       $state.= " Wage: " . number_format($NEWHIRE['HourlyWage'],2,".",",");
       $state.= " Position: " . $NEWHIRE['Position'];
       $state.="&nbsp;&nbsp;Company: " . $NEWHIRE['FEDID'];
       $state.="-" . $NEWHIRE['State'] . "--" . $ORGCK[0]['CompanyName'];
       $state.="&nbsp;&nbsp;Days to Process: " . round($datediff / (60 * 60 * 24));
       $state.="&nbsp;&nbsp;Hired Date: " . date("m/d/Y", strtotime($NEWHIRE['LastHiredDate']));
       $state.="&nbsp;&nbsp;Employee Name: " . $NEWHIRE['EmployeeName'];
       $state.="&nbsp;&nbsp;Employee ID: " . $NEWHIRE['EmployeeID'] . "<br>\n";

    }
  } 

} // end foreach NEWHIRE

$total = $nomatchcnt + $processedcnt + $needsprocessingcnt + $processednotqualifiedcnt + $reconcilecnt + $statecnt + $qualifiedandtermedcnt;

$message .= "<br>\n";
$message .= "New Hire with No Application: " . $nomatchcnt . "<br>\n";
$message .= $noappmatch . "<br>\n";

$message .= "Processed Step One (Hired date, Wage, Position): " . $processedcnt . "<br><br>\n";
$message .= $processed. "<br>\n";

$message .= "Not Processed Step One (Hired date, Wage, Position): " . $needsprocessingcnt . "<br><br>\n";
$message .= $needsprocessing. "<br>\n";

$message .= "Qualified and Termed: " . $qualifiedandtermedcnt . "<br>\n";
$message .= $qualifiedandtermed . "<br>\n";

$message .= "Not Processed / Not Qualified: " . $processednotqualifiedcnt . "<br><br>\n";
$message .= $notprocessed. "<br>\n";

$message .= "Reconcile: " . $reconcilecnt . "<br>\n";
$message .= $multi . "<br>\n";

$message .= "Location Not Matched: " . $locationcnt . "<br>\n";
$message .= $noorgmatch . "<br>\n";

$message .= "State Mismatch: " . $statecnt . "<br>\n";
$message .= $state. "<br>\n";

$message .= "Total Processed: " . $total . "<br>\n";
$message .= "Total Rows: " . $x . "<br><br>\n\n";

if ($argv[1] == "Y") {
  foreach ($individualreports as $key => $value) {

  if (count($value['Employee']) > 0) { // don't send individual report if no employees

  $emailbody = "Good Morning!<br><br>\n\n";
  $emailbody .= "The following new hires still need to be screened for WOTC:<br>";

  foreach ($value['Employee'] AS $E) {
    $emailbody .= $E;
  }

  $emailbody .= "<br>\n<a href=\"https://portal.cmswotc.com/parent.php?c=dycomindustries\">https://portal.cmswotc.com/parent.php?c=dycomindustries</a><br><br>\n\n";

  $emailbody .= "Thank you!<br>\n";
  $emailbody .= "<strong>Lisa Schneider</strong><br>\n";
  $emailbody .= "Account Manager<br>\n";
  $emailbody .= "lschneider@cmswotc.com<br>\n";
  $emailbody .= "860-269-3044 | <a href=\"https://www.cmswotc.com\">www.cmswotc.com</a><br>\n";

  $subject = "WOTC New Hire Forms Needed";

  $PHPMailerObj->clearCustomProperties();
  $PHPMailerObj->clearCustomHeaders();

  // Set who the message is to be sent to
    foreach ($value['Contact'] AS $C) {
      $PHPMailerObj->addAddress($C['EmailAddress'], $C['Name']);
    }

  $PHPMailerObj->addBCC("lschneider@cmshris.com", "Lisa Schneider");
  //$PHPMailerObj->addBCC("dedgecomb@irecruit-software.com", "David Edgecomb");

  $PHPMailerObj->setFrom("lschneider@cmswotc.com", "Lisa Schneider");
  // Set the subject line
  $PHPMailerObj->Subject = $subject;
  // convert HTML into a basic plain-text alternative body
  $PHPMailerObj->msgHTML($emailbody);
  // Content Type Is HTML
  $PHPMailerObj->ContentType = 'text/html';
  // Send email
  $PHPMailerObj->send();

  } // no individual employees

  } // end individual reports

} // end if argv[1]

$subject = "Dycom Exception Report";

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

// Set who the message is to be sent to
$PHPMailerObj->addAddress("eric.lewis@dycominc.com", "Eric Lewis");
$PHPMailerObj->addAddress("lschneider@cmshris.com", "Lisa Schneider");
$PHPMailerObj->addAddress("dedgecomb@irecruit-software.com", "David Edgecomb");

$PHPMailerObj->setFrom("WebApps@iRecruit-us.com", "iRecruit");
// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';
// Send email
$PHPMailerObj->send();

$rtn	  = "New Hire Total: " . $ssntotal . "<br><br>\n\n";

$application="wotcDycomExceptionReport.php";
$status="Done\n" . $rtn;

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>

