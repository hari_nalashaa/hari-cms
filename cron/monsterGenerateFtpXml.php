#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

//Objects for CommonMethods and MonsterInformation
$OrgsInfoResults    =   $OrganizationsObj->getOrgDataInfo("OrgID, MultiOrgID");
$OrgsInfo           =   $OrgsInfoResults['results'];

//Xmlend
$xmlend             =   "";
$flag               =   "false";
$data_exists        =   "false";

//Xmlstart
$xmlstart = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
$xmlstart .= "<jobfeed>\n";
$xmlstart .= "<provider>irecruit</provider>\n";
$xmlstart .= "<isfullfeed>true</isfullfeed>\n";
$xmlstart .= "<part>{part}</part>\n";
$xmlstart .= "<islast>{last}</islast>\n";
$xmlstart .= "<jobs>\n";

//Filename
$cfilename  = 'irecruit_'.date('Ymdhis');

//OneMB in Bytes
$OneMB = 1048576;

//Split the files data in array format
$filesdata  =   array();
$xml        =   "";
for($oi = 0; $oi < count($OrgsInfo); $oi++) {
	
	$OrgID = $OrgsInfo[$oi]['OrgID'];
	$MultiOrgID = $OrgsInfo[$oi]['MultiOrgID'];
	$MonsterConfigInfo = $MonsterObj->getMonsterInfoByOrgID($OrgID, $MultiOrgID);
	
	//$contents = str_replace("<![CDATA[", "", $contents);
	//$contents = str_replace("]]>", "", $contents);
	//$contents = str_replace("&amp;", "&", $contents);

	if($MonsterConfigInfo['row']['FreeRequisitions'] == "Yes") {
		include IRECRUIT_DIR . "monster/monsterFtpOrganization.php";
	}
}

$xmlend .= "</jobs>\n";
$xmlend .= "</jobfeed>\n";

//Delete all files in that "all" folder
$rfiles = glob(IRECRUIT_DIR . 'monster/ftpuploads/all/*'); // get all file names
foreach($rfiles as $rfile) { // iterate files
	if(is_file($rfile))
		unlink($rfile); // delete file
}

//Single File Creation
if($flag == "false") {
	
	$finalxml = $xmlstart.$xml.$xmlend;
	
	$finalxml = str_replace("{part}", "1", $finalxml);
	$finalxml = str_replace("{last}", "true", $finalxml);
	
	$cxfilename = $cfilename;
	
	if($cfilename != "") {
		$fh = fopen (IRECRUIT_DIR . "monster/ftpuploads/all/".$cfilename.".xml", 'w' );
		fwrite ( $fh, $finalxml );
		fclose ( $fh );
		chmod(IRECRUIT_DIR . "monster/ftpuploads/all/".$cfilename.".xml", 0777);
	}
}

//Multiple Files Creation
if($flag == "true") {
	
	for($j = 0, $p = 1; $j < count($filesdata); $j++, $p++) {
		
		$cxfilename = $cfilename."_part".$p;
		$finalxml 	= $xmlstart.$filesdata[$j].$xmlend;
		$finalxml 	= str_replace("{part}", $p, $finalxml);
		
		if(($j+1) == count($filesdata))
			$finalxml = str_replace("{last}", "true", $finalxml);
		else 
			$finalxml = str_replace("{last}", "false", $finalxml);
		
		$fh = fopen (IRECRUIT_DIR . "monster/ftpuploads/all/".$cxfilename.".xml", 'w' );
		fwrite ( $fh, $finalxml);
		fclose ( $fh );
		
	}
		
}

$application    =   "monsterGenerateFtpXml.php";
$status         =   "script okay";

//Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

if($data_exists == "true") {
    //Upload files to monster ftp account
    include IRECRUIT_DIR . "monster/MonsterConnectFtp.inc";
}
?>
