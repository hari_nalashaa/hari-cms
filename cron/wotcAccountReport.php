#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$Date       =   "SUBDATE(NOW(), INTERVAL 7 DAY)";
$columns    =   "DATE_FORMAT(subdate(adddate($Date, INTERVAL 6-weekday($Date) DAY), INTERVAL 7 DAY),'%Y-%m-%d') AS BeginDate,";
$columns    .=  " DATE_FORMAT(adddate($Date, INTERVAL 5-weekday($Date) DAY),'%Y-%m-%d') AS EndDate";
$dates_info =   G::Obj('MysqlHelper')->getDatesList($columns);

$begindate  =   $dates_info['BeginDate'];
$enddate    =   $dates_info['EndDate'];

$query      =   "SELECT wotcID";
$query      .=  " FROM OrgData";
$query      .=  " WHERE ClientSince BETWEEN CAST(:begindate AS DATE) AND CAST(:enddate AS DATE)";
$query      .=  " ORDER BY ClientSince";
$params     =   array(
                    ':begindate'    =>  date("Y-m-d", strtotime($begindate)),
                    ':enddate'      =>  date("Y-m-d", strtotime((new DateTime($enddate))->add(new DateInterval("P1D"))->format('m/d/Y')))
                );

$message    =   'WOTC Accounts set up for ' . $begindate . ' to ' . $enddate . "<br><br>\n\n";

G::Obj('GenericQueries')->conn_string =   "WOTC";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));

$i = 0;
foreach ($RESULTS as $ORGDATA) {
    $i ++;
    $PARENT = G::Obj('WOTCcrm')->getParentInfo($ORGDATA['wotcID']);
    $message .= $ORGDATA['wotcID'] . " - ";
    $message .= $PARENT['CompanyName'] . "<br>\n";
}

$message .= "<br>\nTotal: " . $i . "<br>\n";

$subject = "wotcAccountReport.php - Status for " . $begindate . " to " . $enddate;

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

// Set who the message is to be sent to
$PHPMailerObj->addAddress("bkelly@cmshris.com");
$PHPMailerObj->addAddress("lstrong@cmshris.com");
$PHPMailerObj->addAddress("dedgecomb@irecruit-software.com");
$PHPMailerObj->addAddress("lschneider@cmshris.com");
$PHPMailerObj->addAddress("skelly@cmshris.com");
$PHPMailerObj->addAddress("lkelly@cmshris.com");

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';
// Send email
$PHPMailerObj->send();

$application = "wotcAcountReport.php";
$status = $message;

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
