#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

//Get User Applications Information
$where_info             =   array("Status = 'Pending'", "Deleted = 'No'");
$user_apps_info         =   $UserPortalInfoObj->getUserApplicationsInfo("OrgID, UserID, RequestID, MultiOrgID", $where_info, 'LastUpdatedDate');
$pending_apps_results   =   $user_apps_info['results'];
$pending_apps_count     =   count($pending_apps_results);

$log                    =   "";
$ii                     =   0;

for($i = 0; $i < $pending_apps_count; $i++) {

    $where_info         =   array("OrgID = :OrgID", "RequestID = :RequestID", "(DATE(ExpireDate) < DATE(NOW()))", "Active = 'N'");
    $params_info        =   array(':OrgID' => $pending_apps_results[$i]['OrgID'], ':RequestID' => $pending_apps_results[$i]['RequestID']);
    $req_info           =   $RequisitionsObj->getRequisitionInformation("RequestID", $where_info, "", "", array($params_info));
    $exp_requisitions   =   $req_info['results'];
    
    if(isset($exp_requisitions[0])
        && $exp_requisitions[0]['RequestID'] != '') {

        $OrgID          =   $pending_apps_results[$i]['OrgID'];
        $RequestID      =   $pending_apps_results[$i]['RequestID'];
        $MultiOrgID     =   $pending_apps_results[$i]['MultiOrgID'];
        $UserID         =   $pending_apps_results[$i]['UserID'];
        $ii++;
        $log            .=  $OrgID . ' - ' . $UserID . ' - ' . $RequestID . "<br>\n";

        //Update user applications delete status
        $UserPortalInfoObj->updUserApplicationsDeletedStatus($OrgID, $MultiOrgID, $RequestID, $UserID, 'Yes');

    }
}

$application    =   "delExpReqApplicationsTempData.php";
$status         =   $ii . " Pending Applications Deleted - script okay<br>\n";
$status         .=  $log;

//Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
