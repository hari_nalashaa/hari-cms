#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$status = '';

// Maintain IRECRUIT DB
G::Obj('GenericQueries')->conn_string =   "IRECRUIT";

//The HoldID changed on this so it no longer has a datestamp. We need to determine how to purge.
$where_info =   array("LastModifiedDateTime < DATE_SUB(NOW(), INTERVAL 7 DAY)");
G::Obj('GenericQueries')->delRows("ApplicantDataTemp", $where_info);
$status .= "iRecruit - Trimed ApplicantDataTemp table<br>\n";

$where_info =   array("LastUpdated < DATE_SUB(NOW(), INTERVAL 7 DAY)");
G::Obj('GenericQueries')->delRows("SearchQuery", $where_info);
$status .= "iRecruit - Trimed SearchQuery table<br>\n";

$where_info =   array("LastUpdated < DATE_SUB(NOW(), INTERVAL 7 DAY)");
G::Obj('GenericQueries')->delRows("RequisitionsSearchParams", $where_info);
$status .= "iRecruit - Trimed RequisitionSearchParms table<br>\n";

$where_info =   array("LastUpdated < DATE_SUB(NOW(), INTERVAL 7 DAY)");
G::Obj('GenericQueries')->delRows("MultiOrgReporting", $where_info);
$status .= "iRecruit - Trimed MultiOrgReporting table<br>\n";

$where_info =   array("SUBSTRING(HoldID,1,2) != 'IR' AND EntryDate < DATE_SUB(NOW(), INTERVAL 5 DAY)");
G::Obj('GenericQueries')->delRows("PrescreenResults", $where_info);
$status .= "iRecruit - Trimed PrescreenResults table<br>\n";

$where_info =   array("CreatedUpdatedDate < DATE_SUB(NOW(), INTERVAL 30 DAY)");
G::Obj('GenericQueries')->delRows("ParsingTemp", $where_info);
$status .= "iRecruit - Trimed ParsingTemp table<br>\n";

$application    =   "maintenance.php";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>
