#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

/*
 * Review this connection as includes may change
 */
include COMMON_DIR . 'gateway/PostGateway.function';

// GLOBAL
$transaction["target_app"]      =   "WebCharge_v5.06";
$transaction["response_mode"]   =   "simple";
$transaction["response_fmt"]    =   "delimited";
$transaction["upg_auth"]        =   "zxcvlkjh";

$transaction["delimited_fmt_field_delimiter"]   =   "=";
$transaction["delimited_fmt_include_fields"]    =   "true";
$transaction["delimited_fmt_value_delimiter"]   =   "|";

$transaction["username"]        =   "COSTMANAGEMENTSERVICELLC113106";
$transaction["pw"]              =   "OmShanti1111";
// $transaction["username"] = "gatewaytest";
// $transaction["pw"] = "GateTest2002";
// $transaction["test_override_errors"] = "Y";

$transaction["trantype"]        =   "postauth"; // postauth, sale

$query      =   "SELECT OrgID, PurchaseNumber, TransactionID, AuthAmount, FullName,";
$query      .=  " Address1, Address2, City, State, ZipCode, Phone";
$query      .=  " FROM Purchases";
$query      .=  " WHERE Processed != 'Y'";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

foreach ($RESULTS as $PURCHASE) {

$status         .=   print_r($PURCHASE,true);
    
    $amount = 0;
    
    $query  =   "SELECT ServiceType, RequestID, BudgetTotal, Canceled";
    $query  .=  " FROM PurchaseItems";
    $query  .=  " WHERE OrgID = :orgid";
    $query  .=  " AND PurchaseNumber = :purchasenumber";
    $params =   array(":orgid"=>$PURCHASE['OrgID'], ':purchasenumber'=>$PURCHASE['PurchaseNumber']);
    $ITEMS  =   G::Obj('GenericQueries')->getInfoByQuery($query);
    
    foreach ($ITEMS as $PURCHASEITEMS) {

$status         .=   print_r($PURCHASEITEMS,true);
        
        if ($PURCHASEITEMS['Canceled'] == '') {
            $itemamount =   money_format('%i', $PURCHASEITEMS['BudgetTotal']);
            $amount     +=  $itemamount;
        } 
            
    } // end while
    
    $transaction["reference"]   =   $PURCHASE['AuthAmount']; // Original Auth amount
    $transaction["trans_id"]    =   $PURCHASE['TransactionID']; // Original Transaction ID
    $transaction["authamount"]  =   $PURCHASE['AuthAmount']; // Original Auth Amount
    
    $transaction["fulltotal"]   =   $amount; // New Auth Amount
    
    $transaction["ccname"]      =   $PURCHASE['FullName'];
    $transaction["baddress"]    =   $PURCHASE['Address1'];
    $transaction["baddress1"]   =   $PURCHASE['Address2'];
    $transaction["bcity"]       =   $PURCHASE['City'];
    $transaction["bstate"]      =   $PURCHASE['State'];
    $transaction["bzip"]        =   $PURCHASE['ZipCode'];
    $transaction["bcountry"]    =   $PURCHASE['Country'];
    $transaction["bphone"]      =   $PURCHASE['Phone'];
    
    if ($amount == 0) {
        $where      =   array("OrgID = :orgid", "PurchaseNumber = :purchasenumber");
        $set_info   =   array("Processed = 'Y'");
        $params     =   array(":orgid"=>$PURCHASE['OrgID'], ":purchasenumber"=>$PURCHASE['PurchaseNumber']);
        G::Obj('GenericQueries')->updRowsInfo("Purchases", $set_info, $where, array($params));
    } else if ($amount > 0) {
        
        // --< POST THE TRANSACTION >-----------------------
        $response = PostTransaction($transaction);
        // --< POST THE TRANSACTION >-----------------------
        
        /*
         * echo "\n"; foreach ($transaction as $key => $value) { echo $key . "-" . $value . "\n"; } echo "\n"; echo "\n"; foreach ($response as $key => $value) { echo $key . "-" . $value . "\n"; } echo "\n";
         */
        
        if ($response["approval"] != "") {
            
            $where      =   array("OrgID = :orgid", "PurchaseNumber = :purchasenumber");
            $set_info   =   array("Processed = 'Y'", "PostTransactionID = :anatransid", "PostApprovalCode = :approval", "OrderNumber = :ordernumber");
            $params     =   array(
                                ":orgid"            =>  $PURCHASE['OrgID'],
                                ":purchasenumber"   =>  $PURCHASE['PurchaseNumber'], 
                                ":ordernumber"      =>  $response["ordernumber"], 
                                ":anatransid"       =>  $response["anatransid"],
                                ":approval"         =>  $response["approval"],
                            );
            G::Obj('GenericQueries')->updRowsInfo("Purchases", $set_info, $where, array($params));
        } // end if
    } // end if amount > 0
} // foreach

$application    =   "reconcilePurchases.php";
$status         .=   "script okay";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
