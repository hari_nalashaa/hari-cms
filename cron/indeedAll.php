#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$formatted_date =   G::Obj('MysqlHelper')->getDatesList("DATE_FORMAT(NOW(), '%a, %d %b %Y %H:%i:%s EST') AS FORMATTED_DATE");
$TS             =   $formatted_date['FORMATTED_DATE'];

$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

$xml .= "<source>\n";
$xml .= "<publisher>iRecruit ATS</publisher>\n";
$xml .= "<publisherurl>https://apps.irecruit-us.com/indeed/publish.xml</publisherurl>\n";
$xml .= "<lastBuildDate>" . $TS . "</lastBuildDate>\n";
$xml .= getLinks();
$xml .= "</source>\n";

$filename = IRECRUIT_DIR . 'indeed/published-feed.xml';
$fh = fopen($filename, 'w');
fwrite($fh, $xml);
fclose($fh);
chmod($filename, 0666);

$application = "indeedAll.php";
$status = "script okay";

//Insert Cron Status Log
G::Obj('CronStatusLogs')->insUpdCronStatusLog($application, $status);

function getLinks()
{
    $HOME       =   "https://www.irecruit-us.com/";
    
    $query      =   "SELECT IF(IC.IndeedApply = 'Y', IC.IndeedApply, 'N') AS IndeedApply,
                    IF(INF.Sponsored = 'yes', INF.Sponsored, 'no') AS Sponsored, 
                    OD.OrganizationName,
                    R.OrgID, R.MultiOrgID, R.RequestID,
                    R.Title, R.Description, R.EmpStatusID,
                    DATE_FORMAT(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate,
                    R.City, R.State, R.ZipCode,
                    R.FormID, R.RequisitionFormID,
                    IF(R.IndeedLabelID = '', R.FormID, R.IndeedLabelID) AS IndeedLabelID, R.IndeedWFH,
                    INF.ContactName, INF.Email, INF.Phone, INF.BudgetTotal
                    FROM Requisitions R
                    JOIN OrgData OD ON OD.OrgID = R.OrgID AND OD.MultiOrgID = R.MultiOrgID
                    LEFT JOIN IndeedFeed INF ON INF.OrgID = R.OrgID AND INF.RequestID = R.RequestID
                    LEFT JOIN IndeedConfiguration IC ON IC.OrgID = R.OrgID AND IC.MultiOrgID = R.MultiOrgID
                    WHERE R.Active = 'Y'
                    AND OD.DemoAccount != 'Y'
                    AND NOW() BETWEEN R.PostDate AND R.ExpireDate
                    AND (R.PresentOn = 'PUBLICONLY' OR R.PresentOn = 'INTERNALANDPUBLIC')
                    AND (INF.Sponsored = 'yes' OR R.FreeJobBoardLists LIKE '%Indeed%')
                    ORDER BY R.OrgID, R.Title";
    $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);
    /*
     * removed 12/24/2020 for Piper
                    AND R.ZipCode != ''
                    AND R.City != ''
                    AND R.State != ''
     */
    
    $i = 0;
    $xml = "";
    
    foreach ($RESULTS as $REQ) {
        $i ++;
        
        // Get Default RequisitionFormID
        $STATUSLEVELS = array();
        $STATUSLEVELS = G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($REQ['OrgID'], $REQ['RequisitionFormID']);
        $EmploymentStatusLevel = $STATUSLEVELS[$REQ['EmpStatusID']];
        
        $xml .= "<job>\n";
        
        $xml .= "   <title><![CDATA[";
        $xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['Title']);
        $xml .= "]]></title>\n";
        
        $xml .= "   <date><![CDATA[";
        $xml .= $REQ['PostDate'];
        $xml .= "]]></date>\n";
        
        $xml .= "   <referencenumber><![CDATA[";
        $xml .= $REQ['RequestID'];
        $xml .= "]]></referencenumber>\n";
        
        $xml .= "   <url><![CDATA[";
        
        $xml .= $HOME . "jobRequest.php?";
        $xml .= "OrgID=" . $REQ['OrgID'];
        if ($REQ['MultiOrgID'] != "") {
            $xml .= "&MultiOrgID=" . $REQ['MultiOrgID'];
        } 
        $xml .= "&RequestID=";
        $xml .= $REQ['RequestID'];
        
        $xml .= "&source=IND";
        
        $xml .= "]]></url>\n";
        
        $xml .= "   <company><![CDATA[";
        $xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['OrganizationName']);
        $xml .= "]]></company>\n";
        $xml .= "   <sourcename><![CDATA[";
        $xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['OrganizationName']);
        $xml .= "]]></sourcename>\n";
        
        if ($REQ['City']) {
            $xml .= "   <city><![CDATA[";
            $xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['City']);
            $xml .= "]]></city>\n";
        }
        
        if ($REQ['State']) {
            $xml .= "   <state><![CDATA[";
            $xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['State']);
            $xml .= "]]></state>\n";
        }
        
        if ($REQ['ZipCode']) {
            $xml .= "   <postalcode><![CDATA[";
            $xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['ZipCode']);
            $xml .= "]]></postalcode>\n";
        }
        
        $xml .= "   <country><![CDATA[";
        if ($REQ['Country']) {
            $xml .= $REQ['Country'];
        } else {
            $xml .= "US";
        }
        $xml .= "]]></country>\n";
        
        $xml .= "   <description><![CDATA[";
        $xml .= utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $REQ['Description']));
        $xml .= "]]></description>\n";
        
        if ($EmploymentStatusLevel) {
            $xml .= "   <jobtype><![CDATA[";
            $xml .= $EmploymentStatusLevel;
            $xml .= "]]></jobtype>\n";
        }

        $xml .= "   <remotetype><![CDATA[";
        $xml .= $REQ['IndeedWFH'];
        $xml .= "]]></remotetype>\n";
        
        $xml .= "   <sponsored><![CDATA[";
        $xml .= $REQ['Sponsored'];
        $xml .= "]]></sponsored>\n";
        
        if ($REQ['Sponsored'] == 'yes') {
            $xml .= "   <contact><![CDATA[";
            $xml .= $REQ['ContactName'];
            $xml .= "]]></contact>\n";
            
            $xml .= "   <budget><![CDATA[";
            $xml .= $REQ['BudgetTotal'];
            $xml .= "]]></budget>\n";
            
            $xml .= "   <email><![CDATA[";
            $xml .= $REQ['Email'];
            $xml .= "]]></email>\n";
            
            $xml .= "   <phone><![CDATA[";
            $xml .= $REQ['Phone'];
            $xml .= "]]></phone>\n";
        }
        
        $locations_list = array();
        if ($REQ['City']) {
            $locations_list[] = preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['City']);
        }
        if ($REQ['State']) {
            $locations_list[] = preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['State']);
        }
        if ($REQ['ZipCode']) {
            $locations_list[] = preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['ZipCode']);
        }
        
        $location_info = implode(" ", $locations_list);
        
        $requisition_url = PUBLIC_HOME . "jobRequest.php?";
        $requisition_url .= "OrgID=" . $REQ['OrgID'];
        if ($REQ['MultiOrgID'] != "") {
            $requisition_url .= "&MultiOrgID=" . $REQ['MultiOrgID'];
        } 
        $requisition_url .= "&RequestID=" . $REQ['RequestID'];

        $indeed_apply_meta  =   "OrgID=" . $REQ['OrgID'];
        if($REQ['MultiOrgID'] != "") {
            $indeed_apply_meta  .=  "&MultiOrgID=" . $REQ['MultiOrgID'];
        }
        $indeed_apply_meta  .=  "&FormID=" . $REQ['IndeedLabelID'];
        
        if ($REQ['IndeedApply'] == 'Y') {            
            $xml .= "<indeed-apply-data>";
            $xml .= "<![CDATA[";
            $xml .= "indeed-apply-apiToken=" . G::Obj('Indeed')->Token;
            $xml .= "&indeed-apply-jobTitle=" . str_replace(' ', '+', preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $REQ['Title']));
            $xml .= "&indeed-apply-jobId=" . $REQ['RequestID'];
            $xml .= "&indeed-apply-jobCompanyName=" . str_replace(' ', '+', $REQ['OrganizationName']);
            $xml .= "&indeed-apply-jobLocation=" . str_replace(' ', '+', $location_info);
            $xml .= "&indeed-apply-jobUrl=" . urlencode($requisition_url);
            $xml .= "&indeed-apply-postUrl=" . urlencode(IRECRUIT_HOME . "saveIndeedApplicantInfo.php");
            $xml .= "&indeed-apply-jobMeta=" . urlencode($indeed_apply_meta);
            $xml .= "&indeed-apply-questions=" . urlencode(IRECRUIT_HOME . "indeedInterviewQuestions.php?OrgID=" . $REQ['OrgID'] . "&RequestID=" . $REQ['RequestID'] . "&FormID=" . $REQ['IndeedLabelID']);
            $xml .= "]]>";
            $xml .= "</indeed-apply-data>";
        }
        
        $xml .= "</job>\n";
    } // end foreach
    
    return $xml;
} // end function
?>
