#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$largerthan =   500; // MB size to display larger than
$status = "WOTC VaultSize - larger than " . $largerthan . " MB<br><br>\n\n";
$grandtotal =   0;
$query      =   "SELECT wotcID FROM OrgData ORDER BY wotcID";

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

foreach ($RESULTS as $OD) {
    $displayresults =   "";
    $subtotal       =   0;

    $PARENT = G::Obj('WOTCcrm')->getParentInfo($OD['wotcID']);
    
    $directory      =   WOTC_DIR . 'vault/' . $OD['wotcID'];
    
    if (file_exists($directory) && is_dir($directory) && is_readable($directory)) {
        $subtotal   =   getDirectorySize($directory);
    }
    
    $showlargerthan =   ($largerthan * 1024) * 1024;
    
    if ($subtotal > $showlargerthan) {
        $status .= $OD['wotcID'] . " - " . $PARENT['CompanyName'] . "<br>\n";
        $status .= "Total: " . DisplaySize($subtotal) . "<br><br>\n\n";
    }

    $grandtotal += $subtotal;

} // end foreach

$status .= "Total of Entire Vault: " . DisplaySize($grandtotal);

$application    =   "wotcVaultSize.php";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

function DisplaySize($size)
{
    // if the total size is bigger than 100 MB
    if ($size / 1073741824 > 1) {
        return round($size / 1073741824, 1) . ' GB';
        
        // if the total size is bigger than 1 MB
    } elseif ($size / 1048576 > 1) {
        return round($size / 1048576, 1) . ' MB';
        
        // if the total size is bigger than 1 KB
    } elseif ($size / 1024 > 1) {
        return round($size / 1024, 1) . ' KB';
        
        // else return the filesize in bytes
    } else {
        return round($size, 1) . ' bytes';
    }
} // end function

function getDirectorySize($directory)
{
    $dirSize = 0;
    
    if (! $dh = opendir($directory)) {
        return false;
    }
    
    while ($file = readdir($dh)) {
        if ($file == "." || $file == "..") {
            continue;
        }
        
        if (is_file($directory . "/" . $file)) {
            $dirSize += filesize($directory . "/" . $file);
        }
        
        if (is_dir($directory . "/" . $file)) {
            $dirSize += getDirectorySize($directory . "/" . $file);
        }
    }
    
    closedir($dh);
    
    return $dirSize;
} // end function
?>
