#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

define ('DIR', ROOT . 'wotc_files/Dycom/');

$rtn="";

$FILES=array();
if (is_dir(DIR)) {
  if ($handle = opendir(DIR)) {
    while (($file = readdir($handle)) !== false){

	if(file_exists(DIR . $file)) {
	    	//if ($file != "." && $file != ".." && preg_match("/.csv$/i", $file)) {
	    	if ($file != "." && $file != ".." && preg_match("/.csv$/i", $file)) {

			$FILES[]=$file;
		}

	} // end if file exists

    } // end while
    closedir($handle);
  } // end handle
} // end if is directory

sort($FILES);


foreach ($FILES AS $f) {

	if(file_exists(DIR . $f)) {

	    $rtn .= processFile($f);
	    $rtn .= renameFile($f);

	}
}

$application="wotcDycomNewHireProcessing.php";
$status="Done\n" . $rtn;

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

function renameFile($file) {

	rename(DIR . $file, DIR . $file . "_processed");

	$rtn="Renamed File: " . $file . "\n";

	return $rtn;

} // end function

function processFile($file) {

$FEDID=0;
$SSN=1;
$POSITION=2;
$STATE=3;
$HOURLYWAGE=4;
$LASTHIREDDATE=5;
$TERMINATIONDATE=6;
$EMPLOYEENAME=7;
$EMPLOYEEID=8;

$csv = array_map('str_getcsv', file(DIR . $file));

$i=0;
foreach ($csv as $row) {
  if ($i > 0) {

/*
echo "FEDID: " . preg_replace('/[^0-9.]+/', '',$row[$FEDID]) . "\n";
echo "SSN: " . preg_replace('/[^0-9.]+/', '',$row[$SSN]) . "\n";
echo "POSITION: " . $row[$POSITION] . "\n";
echo "STATE: " . $row[$STATE] . "\n";
echo "HOURLYWAGE: " . $row[$HOURLYWAGE] . "\n";
echo "LASTHIREDDATE: " . $row[$LASTHIREDDATE] . "\n";
echo "TERMINATIONDATE: " . $row[$TERMINATIONDATE] . "\n";
echo "EMPLOYEEID: " . $row[$EMPLOYEEID] . "\n";
echo "EMPLOYEENAME: " . $row[$EMPLOYEENAME] . "\n";
*/

       //Skip fields on duplicate key update
        $skip   =   array('FEDID','SSN');
        $info   =   array(
                        'FEDID'             =>  preg_replace('/[^0-9.]+/', '',$row[$FEDID]),
                        'SSN'               =>  preg_replace('/[^0-9.]+/', '',$row[$SSN]),
                        'EmployeeID'        =>  preg_replace('/[^0-9.]+/', '',$row[$EMPLOYEEID]),
                        'Position'          =>  $row[$POSITION],
                        'State'             =>  $row[$STATE],
                        'HourlyWage'        =>  $row[$HOURLYWAGE],
                        'LastHiredDate'     =>  $row[$LASTHIREDDATE],
                        'TerminationDate'   =>  $row[$TERMINATIONDATE],
                        'EmployeeName'      =>  $row[$EMPLOYEENAME],
                        'LastModified'      =>  "NOW()"
                    );
        G::Obj('WOTCOrganization')->insUpdDycomHiresInfo($info, $skip);


  } // end if > row 0
  $i++;
} // end foreach

$rtn = "Processed " . $i . " rows\n";

return $rtn;

} // end function

?>

