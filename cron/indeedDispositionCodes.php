#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

//Organizations
$where_info =   array("OrgID != ''", "MultiOrgID = ''");
$orgs_info  =   $OrganizationsObj->getOrgDataInfo("OrgID, OrganizationName", $where_info);
$orgs_list  =   $orgs_info['results'];
$orgs_cnt   =   $orgs_info['count'];

$file_name  =   date("Y-m-d-H-i-s").".xml";
$files_info['file_names']   =   array($file_name);

$file_data  =   '<?xml version="1.0" encoding="utf-8"?>'."\n";
$data       =   '<nodes>'."\n";

for($o = 0; $o < $orgs_cnt; $o++) {
    $OrgID              =   $orgs_list[$o]['OrgID'];
    $process_list       =   array();
    
    $hired_status_info  =   $ReportsObj->getInfo("getHiredStatusProcessOrder('$OrgID') as HiredProcessOrder");
    $hired_status       =   $hired_status_info['HiredProcessOrder'];
    
    $where_info         =   array("OrgID = :OrgID", "Searchable = 'N'");
    $params_info        =   array(":OrgID"=>$OrgID);
    $process_flow_list  =   $ApplicantsObj->getApplicantProcessFlowInfo("*", $where_info, '', array($params_info));
    $process_flow_cnt   =   $process_flow_list['count'];
    $process_flow_res   =   $process_flow_list['results'];
    
    $process_list[]     =   1;  //It is fixed for new applicant   
    for($pl = 0; $pl < $process_flow_cnt; $pl++) {
        $process_list[] =   $process_flow_res[$pl]['ProcessOrder'];
    }
    
    $app_status_logs_res    =   $IndeedObj->getIndeedApplicantStatusLogs($OrgID);
    $app_status_logs_list   =   $app_status_logs_res['results'];

    $org_app_ids_list       =   array();
    if(count($app_status_logs_list) > 0) {
        for($a = 0; $a < count($app_status_logs_list); $a++) {

            $org_app_id     =   $OrgID."-".$app_status_logs_list[$a]['ApplicationID'];
            
            if(!in_array($org_app_id, $org_app_ids_list)) {
                
                if(in_array($app_status_logs_list[$a]['Status'], $process_list)
                    && $app_status_logs_list[$a]['IsProcessed'] == "No") {
                   
                    if($app_status_logs_list[$a]['Status'] == 1) {
                    	$IndeedStatus = "New";
                    }
                    else if($hired_status   ==  $app_status_logs_list[$a]['Status']) {
                        $IndeedStatus = "Hired";
                    }
                    else {
                        $IndeedStatus = "Final";
                    }

                    $Notes  =   $app_status_logs_list[$a]['ProcessOrder']."-".$app_status_logs_list[$a]['Notes'];
                    
                    $data   .=  '<Application>'."\n";
                    $data   .=  '<apply_id>'.$app_status_logs_list[$a]['ApplyID'].'</apply_id>'."\n";
                    $data   .=  '<status>'.$IndeedStatus.'</status>'."\n";
                    $data   .=  '<disposition_timestamp>'.$app_status_logs_list[$a]['DispositionTimeStamp'].'</disposition_timestamp>'."\n";
                    $data   .=  '</Application>'."\n";
                    
                    $org_app_ids_list[] =   $org_app_id;

                    // uncomment for production
                    $ApplicantStatusLogsObj->updIndeedProcessedStatus($OrgID, $app_status_logs_list[$a]['ApplicationID'], $app_status_logs_list[$a]['Status']);
                }
                
            }            
            
        }
    }
}

$data       .=   '</nodes>'."\n";
$file_data  .=   $data;

if(!is_dir(ROOT.'irecruit/indeed_status_codes')) {
    //Directory does not exist, so lets create it.
    mkdir(ROOT.'irecruit/indeed_status_codes', 0777);
    chmod(ROOT.'irecruit/indeed_status_codes', 0777);
}

//Write status codes data to a file.
$fp = fopen(ROOT.'irecruit/indeed_status_codes/'.$file_name, 'w+');
fwrite($fp, $file_data);
fclose($fp);

$pre_signed_urls        =   $IndeedATSDispositionObj->getPreSignedUrl($files_info);
$pre_signed_urls_list   =   json_decode($pre_signed_urls, true);


foreach($pre_signed_urls_list as $file_name=>$pre_signed_url) {
    $file_path          =   ROOT.'irecruit/indeed_status_codes/'.$file_name;
    $path_info          =   parse_url($pre_signed_url);

    $url                =   $path_info['scheme']."//".$path_info['host'].$path_info['path'];
    $params_info        =   $path_info['query'];
    $params_list        =   explode("&", $params_info);
    $params             =   array();
    
    for($p = 0; $p < count($params_list); $p++) {
    	
        $param_kv      =    explode("=", $params_list[$p]);
        
        $params[$param_kv[0]]   =   $param_kv[1];
    }
    
    $info             =   $IndeedATSDispositionObj->postIndeedDispositionData($file_name, $pre_signed_url, $file_path, $params);
}

$application = "indeedDisposition.php";

if ($info['http_code'] == "200") {
$status = "transfer successful";
} else {
$status = "error";
}

$log = "Entry Point:\n";
$log .= print_r($pre_signed_urls_list,true);
$log .= "Sign Point:\n";
$log .= print_r($info,true);
$log .= "<br><br>\n" . $status;

Logger::writeMessage(ROOT."logs/indeed-disposition-codes/". $OrgID . "-" . $file_name . "-" . time() . ".txt", $log, "w", false);

//Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>
