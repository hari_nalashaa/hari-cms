#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

include COMMON_DIR . 'formsInternal/ApplicantFormStatus.inc';
include IRECRUIT_DIR . 'applicants/EmailApplicant.inc';

$rem_orgids_results =   $IconnectFormsReminderSettingsObj->getIconnFormsRemindersList();
$rem_orgids_list    =   $rem_orgids_results['results'];
$rem_orgids_count   =   $rem_orgids_results['count'];
$alertmessage       =   "";

$emails_sent        =   0;
for($o = 0; $o < $rem_orgids_count; $o++) {
    $OrgID          =   $rem_orgids_list[$o]['OrgID'];
    $RequestID      =   $rem_orgids_list[$o]['RequestID'];
    
    //Set where condition
    $where          =   array("OrgID = :OrgID", "Status < 3", "RequestID = :RequestID");
    //Set parameters
    $params         =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
    //Get InternalFormsAssigned Information Count
    $results        =   $FormsInternalObj->getInternalFormsAssignedInfo("OrgID, ApplicationID, RequestID", $where, "OrgID, ApplicationID", "", array($params));
    $apps_list      =   $results['results'];
    $apps_count     =   $results['count'];

    for($a = 0; $a < $apps_count; $a++) {
        $ApplicationID      =   $apps_list[$a]['ApplicationID'];
        $RequestID          =   $apps_list[$a]['RequestID'];
       
        $em                 =   internalFormTickler ( $OrgID, $ApplicationID, $RequestID, "" );
        internalFormManagerTickler ( $OrgID, $ApplicationID, $RequestID, "" );
        
        if(($em != "no one") && ($em != "")) {
            $alertmessage      .=   'An email has been sent to: ' . $OrgID . " - " . $ApplicationID . " - " . $RequestID . " - " . $em . "<br>\n";
            
            // Insert into ApplicantHistory
            $UpdateID           =   'AUTO';
            $Comments           =   'iConnect forms reminder email sent to: ' . $em;
            $emails_sent++;
            
            $job_application_history = array (
                "OrgID"                 =>  $OrgID,
                "ApplicationID"         =>  $ApplicationID,
                "RequestID"             =>  $RequestID,
                "ProcessOrder"          =>  "-6",
                "Date"                  =>  "NOW()",
                "UserID"                =>  $UpdateID,
                "Comments"              =>  $Comments
            );
            
            G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );
        }
    }
    
    //Update the last run date per organization
    $IconnectFormsReminderSettingsObj->updLastRunDate($OrgID, "", $RequestID);
}

$application    =   "notifyIconnectForms.php";
$status         =   $emails_sent . " messages sent - script okay<br>\n";
$status        .=   $alertmessage;

//Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>
