#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$OrgID = 'I20120420';
    
    $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    
    $xml .= "<jobs>\n";
    
    $query      =   "SELECT R.OrgID, R.MultiOrgID, R.RequestID, R.Title, R.Description,";
    $query      .=  " R.EmpStatusID, date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate, R.City,";
    $query      .=  " R.State, R.ZipCode";
    $query      .=  " FROM Requisitions R";
    $query      .=  " WHERE R.OrgID = :OrgID";
    $query      .=  " AND R.Active = 'Y'";
    $query      .=  " AND NOW() BETWEEN R.PostDate AND R.ExpireDate";
    $query      .=  " AND (R.PresentOn = 'PUBLICONLY' OR R.PresentOn = 'INTERNALANDPUBLIC')";
    $query      .=  " ORDER BY R.OrgID, R.Title";
    $params     =   array(':OrgID'=>$OrgID);
    $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));

    $query	=   "SELECT QuestionID, Question, QuestionTypeID, value";
    $query	.=  " FROM RequisitionQuestions";
    $query	.=  " WHERE OrgID = :OrgID AND RequisitionFormID";
    $query	.=  " IN (SELECT RequisitionFormID FROM RequisitionForms where OrgID = :OrgID2 and FormDefault = 'Y')";
    $query	.=  " AND QuestionID LIKE 'CUST%' AND Requisition = 'Y'";
    $params     =   array(':OrgID'=>$OrgID,':OrgID2'=>$OrgID);
    $CUSTOM	=   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));


    $i = 0;
    foreach ($RESULTS as $REQ) {
        $i ++;
        
        // Get Default RequisitionFormID
        $STATUSLEVELS = array();
        $req_det_info = $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $REQ['OrgID'], $REQ['RequestID']);
        $STATUSLEVELS = $RequisitionDetailsObj->getEmploymentStatusLevelsList($REQ['OrgID'], $req_det_info['RequisitionFormID']);
        $EmploymentStatusLevel = $STATUSLEVELS[$REQ['EmpStatusID']];
        
        // Get OrganizationName, DemoAccount
        $org_details = $OrganizationDetailsObj->getOrganizationInformation($REQ['OrgID'], $REQ['MultiOrgID'], "OrganizationName, DemoAccount");
        $OrganizationName = $org_details['OrganizationName'];
        $DemoAccount = $org_details['DemoAccount'];
        
        // Required
        if (($REQ['Title']) && ($REQ['PostDate']) && ($REQ['RequestID']) && ($REQ['Description']) && ($DemoAccount != 'Y')) {

            
            $xml .= "<job>\n";
            
            $xml .= "   <jobtitle><![CDATA[";
            $xml .= $REQ['Title'];
            $xml .= "]]></jobtitle>\n";
            
            $xml .= "   <referencenumber><![CDATA[";
            $xml .= $REQ['RequestID'];
            $xml .= "]]></referencenumber>\n";
            
            $xml .= "   <url><![CDATA[";
            
            $xml .= "https://www.irecruit-us.com/jobRequest.php?";
            $xml .= "OrgID=" . $REQ['OrgID'];
            if ($REQ['MultiOrgID'] != "") {
                $xml .= "&MultiOrgID=" . $REQ['MultiOrgID'];
            } 
            $xml .= "&RequestID=";
            $xml .= $REQ['RequestID'];
            
            $xml .= "&source=JOBING";
            
            $xml .= "]]></url>\n";
            
            if ($OrganizationName) {
                $xml .= "   <company><![CDATA[";
                $xml .= $OrganizationName;
                $xml .= "]]></company>\n";
            }
            
            if ($REQ['City']) {
                $xml .= "   <city><![CDATA[";
                $xml .= $REQ['City'];
                $xml .= "]]></city>\n";
            }
            
            if ($REQ['State']) {
                $xml .= "   <State><![CDATA[";
                $xml .= $REQ['State'];
                $xml .= "]]></State>\n";
            }
            
            if ($REQ['ZipCode']) {
                $xml .= "   <Zip><![CDATA[";
                $xml .= $REQ['ZipCode'];
                $xml .= "]]></Zip>\n";
            }
            
            $xml .= "   <Description><![CDATA[";
            $xml .= utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $REQ['Description']));
            $xml .= "]]></Description>\n";
            
            $xml .= "   <Requirements><![CDATA[";
            $xml .= "]]></Requirements>\n";
            
            if ($EmploymentStatusLevel) {
                $xml .= "   <jobtype><![CDATA[";
                $xml .= $EmploymentStatusLevel;
                $xml .= "]]></jobtype>\n";
            }
            
            if ($OrgID == 'I20120420') {
                
                $query      =   "SELECT OLD.CategorySelection FROM RequisitionOrgLevels ROL";
                $query      .=  " JOIN OrganizationLevelData OLD";
                $query      .=  " ON ROL.OrgLevelID = OLD.OrgLevelID AND ROL.SelectionOrder = OLD.SelectionOrder";
                $query      .=  " AND ROL.OrgID = OLD.OrgID";
                $query      .=  " WHERE ROL.OrgID = :orgid AND ROL.RequestID = :requestid";
                $params     =   array(':orgid'=>$REQ['OrgID'], ':requestid'=>$REQ['RequestID']);
                $ORGLEVELS  =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
                
                $ii = 0;
                foreach ($ORGLEVELS as $OL) {
                    if ($OL['CategorySelection'] != "") {
                        $ii ++;
                        $xml .= "   <location-$ii><![CDATA[";
                        $xml .= $OL['CategorySelection'];
                        $xml .= "]]></location-$ii>\n";
                    } // end if
                } // end foreach
            } // end I20120420

            foreach ($CUSTOM AS $CC) {

                $req_new_data = G::Obj('RequisitionsData')->getRequisitionsDataByQuestionID($REQ['OrgID'],$REQ['RequestID'],$CC['QuestionID']);
                $key = preg_replace('/[^A-Za-z0-9]/', '', $CC['Question']);

                $xml .= "   <" . $key . "><![CDATA[";
				if ($CC['QuestionTypeID'] == 3) {
				   	$values_list = G::Obj('QuestionTypes')->getValueList($CC['value']);
		        	$xml .= $values_list[$req_new_data['Answer']];
				} else {
		            $xml .= $req_new_data['Answer'];
				}
				
                $xml .= "]]></" . $key . ">\n";
            }
            
            $xml .= "</job>\n";
        } // end if all required fields are populated
    } // end while
    
    $xml .= "</jobs>\n";
    
    $filename = IRECRUIT_DIR . 'jobing/' . $OrgID . '.xml';
    $fh = fopen($filename, 'w');
    fwrite($fh, $xml);
    fclose($fh);
    chmod($filename, 0666);

$application = "jobing.php";
$status = "script okay";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
