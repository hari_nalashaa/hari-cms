#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$largerthan =   5500; // MB size to display larger than

$status = "iRecruit VaultSize - larger than " . $largerthan . " MB<br><br>\n\n";

$grandtotal =   0;

$RES        =   $OrganizationsObj->getOrgDataInfo("OrgID, OrganizationName", array(), 'OrgID');
$RESULTS    =   $RES['results'];

$subdir     =   array(
                    "applicantattachments",
                    "applicantvault",
                    "employmentdocuments",
                    "requisitionfiles"
                );

foreach ($RESULTS as $OD) {
    
    $dir = IRECRUIT_DIR . 'vault/' . $OD['OrgID'];
    
    if (! file_exists($dir)) {
        mkdir($dir, 0700);
        chmod($dir, 0777);
    }
    
    $directoryresult = "";
    $subtotal = 0;
    $total = 0;
    for ($i = 0; $i < count($subdir); $i ++) {
        
        $dir2 = $dir . '/' . $subdir[$i];
        
        if (! file_exists($dir2)) {
            mkdir($dir2, 0700);
            chmod($dir2, 0777);
        }
        
        $directory = IRECRUIT_DIR . 'vault/' . $OD['OrgID'] . '/' . $subdir[$i];
        
        if (file_exists($directory) && is_dir($directory) && is_readable($directory)) {
            
            $subtotal = getDirectorySize($directory);
            
            $directoryresult .= $subdir[$i] . ": ";
            $directoryresult .= DisplaySize($subtotal);
            $directoryresult .= "<br>\n";
            $total += $subtotal;
        } else { // else directory
            
            $directoryresult .= $subdir[$i] . ": ";
            $directoryresult .= DisplaySize(0);
            $directoryresult .= "<br>\n";
            $total += 0;
        } // end if directory
    } // end for each type directory
    
    $showlargerthan = ($largerthan * 1024) * 1024;
    
    if ($total > $showlargerthan) {
        
        $status .= $OD['OrgID'] . " - " . $OD['OrganizationName'] . "<br>\n";
        $status .= $directoryresult;
        $status .= "Total: " . DisplaySize($total) . "<br><br>\n\n";
    }

    $grandtotal += $total;

} // end foreach

$status .= "Total of Entire Vault: " . DisplaySize($grandtotal) . "<br><br>\n\n";

$subject = "monitorVaultSize.php - iRecruit - larger than " . $largerthan . " MB";

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

// Set who the message is to be sent to
$PHPMailerObj->addAddress("bkelly@cmshris.com");
// Set who the message is to be sent to
$PHPMailerObj->addAddress("lstrong@cmshris.com");
// Set who the message is to be sent to
$PHPMailerObj->addAddress("dedgecomb@irecruit-software.com");

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($status);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/plain';
// Send email
$PHPMailerObj->send();

$application = "monitorVaultSize.php";

//Insert Update Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

function DisplaySize($size)
{
    
    // if the total size is bigger than 100 MB
    if ($size / 1073741824 > 1) {
        return round($size / 1073741824, 1) . ' GB';
        
        // if the total size is bigger than 1 MB
    } elseif ($size / 1048576 > 1) {
        return round($size / 1048576, 1) . ' MB';
        
        // if the total size is bigger than 1 KB
    } elseif ($size / 1024 > 1) {
        return round($size / 1024, 1) . ' KB';
        
        // else return the filesize in bytes
    } else {
        return round($size, 1) . ' bytes';
    }
} // end function
function getDirectorySize($directory)
{
    $dirSize = 0;
    
    if (! $dh = opendir($directory)) {
        return false;
    }
    
    while ($file = readdir($dh)) {
        if ($file == "." || $file == "..") {
            continue;
        }
        
        if (is_file($directory . "/" . $file)) {
            $dirSize += filesize($directory . "/" . $file);
        }
        
        if (is_dir($directory . "/" . $file)) {
            $dirSize += getDirectorySize($directory . "/" . $file);
        }
    }
    
    closedir($dh);
    
    return $dirSize;
} // end function
?>
