#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$RES        =   $OrganizationsObj->getOrgDataInfo("DISTINCT(OrgID)", array(), 'ClientSince');
$RESULTS    =   $RES['results'];

$status = date("D M Y j G:i:s T") . "<br>\n";

$ii = 0;
$xx = 0;
foreach ($RESULTS as $OD) {
    
    $where_info     =   array("OrgID = :OrgID");
    $params_info    =   array(":OrgID"=>$OD['OrgID']);
    $AARESULTS_RES  =   $AttachmentsObj->getApplicantAttachments("*", $where_info, '', array($params_info));
    $AARESULTS      =   $AARESULTS_RES['results'];
    
    foreach ($AARESULTS as $AA) {
        
        $dir = IRECRUIT_DIR . 'vault/' . $OD['OrgID'];
        
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $dir .= '/applicantattachments';
        
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $file = IRECRUIT_DIR . 'vault/' . $OD['OrgID'] . '/applicantattachments/' . $AA['ApplicationID'] . "-" . $AA['PurposeName'];
        $fileext = $file . "." . $AA['FileType'];
        
        if (! file_exists($fileext)) {
            
            $status .= "<br>\nFile doesn't exist<br>\n";
            $status .= $OD['OrgID'] . '-' . $AA['ApplicationID'] . '-' . $fileext . "<br>\n";
            $status .= "cd " . IRECRUIT_DIR . 'vault/' . $OD['OrgID'] . '/applicantattachments/' . "<br>\n";
            $status .= "ll " . $AA['ApplicationID'] . '*' . "<br>\n";
            $status .= 'SELECT * FROM ApplicantAttachments where OrgID = \'' . $OD['OrgID'] . '\' and ApplicationID = \'' . $AA['ApplicationID'] . '\';' . "<br><br>\n";
            $xx ++;
        } else {
            
            if (filesize($fileext) == 0) {
                
                $status .= "<br>\nFile zero error - ";
                $status .= $OD['OrgID'] . '-' . $AA['ApplicationID'] . '-' . $fileext . "<br>\n";
                $cmd = "rm " . escapeshellarg($fileext);
                
                system($cmd);
                
                $where_info =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "TypeAttachment = :TypeAttachment", "PurposeName = :PurposeName");
                $params     =   array(":OrgID"=>$OD['OrgID'], ":ApplicationID"=>$AA['ApplicationID'], ":TypeAttachment"=>$AA['TypeAttachment'], ":PurposeName"=>$AA['PurposeName']);
                G::Obj('GenericQueries')->delRows("ApplicantAttachments", $where_info, array($params));
                
                $ii ++;
            } // end else
        } // end else file not exist
    } // end foreach AARESULTS
} // end foreach RESULTS

$status .= "<br>\n";
$status .= "Grand Total Zero: " . $ii . "<br>\n";
$status .= "Grand Total Not Exist: " . $xx . "<br>\n";
$status .= "<br>\n" . date("D M Y j G:i:s T");

$application = "monitorAttachments.php";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
