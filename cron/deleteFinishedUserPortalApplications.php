#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

G::Obj('GenericQueries')->conn_string    =   "USERPORTAL";

$query = "select UserApplications.OrgID,UserApplications.MultiOrgID,UserApplications.UserID,UserApplications.RequestID
from ApplicantProcessData
JOIN UserApplications ON ApplicantProcessData.OrgID = UserApplications.OrgID and ApplicantProcessData.MultiOrgID = UserApplications.MultiOrgID 
and ApplicantProcessData.UserID = UserApplications.UserID and ApplicantProcessData.RequestID = UserApplications.RequestID
where (UserApplications.Deleted = 'Yes' or UserApplications.Status = 'Finished') and UserApplications.LastUpdatedDate < date_sub(now(), interval 3 day)
group by UserApplications.OrgID,UserApplications.MultiOrgID,UserApplications.UserID,UserApplications.RequestID
order by UserApplications.LastUpdatedDate DESC";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

$i=0;
foreach ($RESULTS as $DEL) {

    $i++;
    $OrgID          =   $DEL['OrgID'];
    $MultiOrgID     =   $DEL['MultiOrgID'];
    $UserID         =   $DEL['UserID'];
    $RequestID      =   $DEL['RequestID'];

    $where_info     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "UserID = :UserID", "RequestID = :RequestID");
    $params         =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':UserID'=>$UserID, ':RequestID'=>$RequestID);

    G::Obj('GenericQueries')->delRows("ApplicantProcessData", $where_info, array($params));
    $log            .=  $i . '-' . $OrgID . ' - ' . $MultiOrgID . ' - ' . $UserID . ' - ' . $RequestID . "<br>\n";

}

$application    =   "deleteFinishedUserPortalApplications.php";
$status         =   " Finished Applications Deleted - script okay<br>\n";
$status		.= $log;

//Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
