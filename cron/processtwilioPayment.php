#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$application = "processTwilioPayment.php";
$status = "";

$orgs_info  =   G::Obj('TwilioTextingAgreement')->getTextingAgreementsList();

foreach($orgs_info as $OrgID=>$TwilioOrgInfo) {

    $status .= $OrgID . "\n";

    $IntuitCardID = $TwilioOrgInfo['IntuitCardID'];

    $card_info = json_decode(G::Obj('IntuitManageCards')->getCardInfoFromApi($OrgID, $IntuitCardID),true);
    $expMonth  =   $card_info['expMonth'];
    $expYear   =   $card_info['expYear'];

    $status .= "IntuitCardID: " . $IntuitCardID . "\n";

    //If card is expired
    if(strtotime("{$expYear}-{$expMonth}") < strtotime( date("Y-m")))
    {
        $info               =   $TwilioOrgInfo;

	G::Obj('TwilioTextingAgreement')->delTextingAgreementByOrgID($OrgID);

        $message            =   "Dear User,<br><br>";
        $message            .=  "The credit card for your texting subscription on iRecruit has expired, Your texting subscription has been temporarily de-activated.<br>";
        $message            .=  "Please activate it by updating your credit card on record with iRecruit and updating your texting agreement.<br><br>";
        $message            .=  "Regards<br>";
        $message            .=  "iRecruit Team<br>";
        $message            .=  "<br><br><br>";
        $message            .=  "<i style=\"font-size:6pt;\">" . $OrgID . "</i>";

        $to =   $TwilioOrgInfo['UserEmail'];
        
        //Clear properties
        G::Obj('PHPMailer')->clearCustomProperties();
        G::Obj('PHPMailer')->clearCustomHeaders();
        
        // Set who the message is to be sent to
        G::Obj('PHPMailer')->addAddress ( $to );
        // Set BCC
        G::Obj('PHPMailer')->addBCC( "dedgecomb@irecruit-software.com", "David Edgecomb" );
        // Set the subject line
        G::Obj('PHPMailer')->Subject = 'iRecruit Texting - Card Expired';
        // convert HTML into a basic plain-text alternative body
        G::Obj('PHPMailer')->msgHTML ( $message );
        // Content Type Is HTML
        G::Obj('PHPMailer')->ContentType = 'text/html';
        // send mail
        G::Obj('PHPMailer')->send ();

    	$status .= "Credit Card has Expired.\n";

        Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-card-expired.txt", $status, "a+", false);
        Logger::writeMessageToDb($OrgID, "Intuit", $status);

    }
    else {

	$amount = $TwilioOrgInfo['TotalAmount'];
        
        //Get Intuit Access Information
        $intuit_access_info     =   G::Obj('Intuit')->getUpdatedAccessTokenInfo();

        // Get year
        $year       =   G::Obj('MysqlHelper')->getDateTime ('%Y');
        
        // set where condition
        $where      =   array ("substring(PurchaseNumber, 1, 4) = :PurchaseNumber");
        // set parameters
        $params     =   array (":PurchaseNumber" => $year);
        // Get purchases information
        $results    =   G::Obj('Purchases')->getPurchasesInfo ( "*", $where, "", array ($params) );
        $invcnt     =   $results ['count'];
        
        $invcnt ++;
        $invcnt     =   "000" . $invcnt;
        $invcnt     =   substr ( $invcnt, - 4, 4 );
        
        $PurchaseNumber = $year . $invcnt;

        //Change your Access Token Here
        $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
        //Add your request ID here
        $intuit_request_id    =   uniqid(str_replace(".", "", microtime(true)));
        
        $body = [
            "amount"    =>  $amount,
            "currency"  =>  "USD",
            "context"   =>  [
                "mobile"        =>  "false",
                "isEcommerce"   =>  "true"
            ]
        ];
        
        $body["cardOnFile"]   =   $IntuitCardID;
        
        $http_header = array(
            'Accept'          =>  'application/json',
            'Request-Id'      =>  $intuit_request_id,
            'Authorization'   =>  $access_token,
            'Content-Type'    =>  'application/json;charset=UTF-8'
        );
        
        $intuit_response = G::Obj('CurlClient')->makeAPICall($IntuitObj->create_charge_url, "POST", $http_header, json_encode($body), null, false);
        
        $req_data  = "\nRequest:".json_encode($http_header)."<br>";
        $req_data .= json_encode($body)."<br>";
        $req_data .= "\nResponse:".json_encode($intuit_response);
        
        Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-payment-response.txt", $req_data, "a+", false);
        Logger::writeMessageToDb($OrgID, "Intuit", $req_data);
        
        $intuit_payment_info    =   json_decode($intuit_response, true);
        
        if($intuit_payment_info['id'] != "") {
            $intuit_info = array(
                "OrgID"             =>  $OrgID,
                "PurchaseNumber"    =>  $PurchaseNumber,
                "Created"           =>  $intuit_payment_info['created'],
                "Status"            =>  $intuit_payment_info['status'],
                "Amount"            =>  $intuit_payment_info['amount'],
                "Currency"          =>  $intuit_payment_info['currency'],
                "CardInfo"          =>  json_encode($intuit_payment_info['card']),
                "AvsStreet"         =>  $intuit_payment_info['avsStreet'],
                "AvsZip"            =>  $intuit_payment_info['avsZip'],
                "CSCMatch"          =>  $intuit_payment_info['cardSecurityCodeMatch'],
                "Id"                =>  $intuit_payment_info['id'],
                "Context"           =>  json_encode($intuit_payment_info['context']),
                "AuthCode"          =>  $intuit_payment_info['authCode'],
                "PaymentCategory"   =>  "TWILIO",
                "CreatedDateTime"   =>  "NOW()"
            );
            
            //Always it is card on file
            $intuit_info["CardOnFileID"]    =   $IntuitCardID;
            
            G::Obj('IntuitPaymentInformation')->insIntuitPaymentInformation($intuit_info);

            //Purchase items information
            $purchase_items_info = array (
                "OrgID"             =>  $OrgID,
                "PurchaseNumber"    =>  $PurchaseNumber,
                "LineNo"            =>  1,
                "ServiceType"       =>  "Twilio Subscription",
                "RequestID"         =>  "",
                "InvoiceNo"         =>  "",
                "BudgetTotal"       =>  $intuit_payment_info['amount'],
                "Comment"           =>  "Twilio Subscription"
            );
            //Insert purchase information
            G::Obj('Purchases')->insPurchaseInfo ( 'PurchaseItems', $purchase_items_info );
            
        }
        
        if ($intuit_payment_info['status'] == "CAPTURED") {
            
            //Save Twilio Texting Agreement Information
            $user_info  =   G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Phone, EmailAddress");
            
            //Update the last billed date time
            G::Obj('TwilioTextingAgreement')->updTextingAgreementLastBilledDateTime($OrgID);
            
            //Unset Payment Information Session
            unset($_SESSION['I']['payment_information']);
            
            $purchases_info                 =   array (
                                                    "OrgID"             =>  $OrgID,
                                                    "PurchaseNumber"    =>  $PurchaseNumber,
                                                    "UserID"            =>  $USERID,
                                                    "PurchaseDate"      =>  "NOW()",
                                                    "TransactionID"     =>  $intuit_payment_info['id'],
                                                    "ApprovalCode"      =>  $intuit_payment_info['status'],
                                                    "AuthAmount"        =>  $intuit_payment_info["amount"],
                                                    "Processed"         =>  "Y",
                                                    "CreatedDateTime"   =>  "NOW()",
                                                    "PaymentCategory"   =>  "TWILIO"
                                                );
            
            $purchases_info["FullName"]     =   $intuit_payment_info['card']['name'];
            $purchases_info["Address1"]     =   $intuit_payment_info['card']['address']['streetAddress'];
            $purchases_info["Address2"]     =   '';
            $purchases_info["City"]         =   $intuit_payment_info['card']['address']['city'];
            $purchases_info["State"]        =   '';
            $purchases_info["ZipCode"]      =   $intuit_payment_info['card']['address']['postalCode'];
            $purchases_info["Country"]      =   $intuit_payment_info['card']['address']['country'];
            $purchases_info["Phone"]        =   $TwilioOrgInfo['UserPhone'];
            $purchases_info["Email"]        =   $TwilioOrgInfo['UserEmail'];
            $purchases_info["CCtype"]       =   $intuit_payment_info['card']['cardType'];
            $purchases_info["CCnumber"]     =   $intuit_payment_info['card']['number'];
            $purchases_info["CCexp"]        =   $intuit_payment_info['card']['expYear'] . "-" . $intuit_payment_info['card']['expMonth'] . "-01";;
            
            //Insert purchase information
            G::Obj('Purchases')->insPurchaseInfo ( 'Purchases', $purchases_info );
            
            if (isset($TwilioOrgInfo['UserEmail']) && $TwilioOrgInfo['UserEmail'] != "") {
                
                $to         =   $TwilioOrgInfo['UserEmail'];
                $subject    =   "Your iRecruit Order # " . $PurchaseNumber;
                
                $message = '<img src="' . PUBLIC_HOME . 'images/iRecruit.png"><br><br>';
                
                $message .= "This email is to confirm your recent purchase with iRecruit." . "<br><br>";
                
                $message .= "Purchase Date: " . G::Obj('MysqlHelper')->getDateTime('%m/%d/%Y') . "<br>";
                $message .= "Purchase Number: " . $PurchaseNumber . "<br>";
                $message .= "Purchaser's Name: " . $intuit_payment_info['card']['name'] . "<br>";
                $message .= "Payment Details: $" . $intuit_payment_info ["amount"] . " was charged to card ending in " . substr($intuit_payment_info['card']['number'],-4) . "<br>";
                $message .= "Merchant Name: COST MANAGEMENT SERVICE LLC" . "<br><br>";
                
                $message .= "Information regarding your order can also be found under iRecruit Administration on the ";
                $message .= "<a href=\"" . IRECRUIT_HOME . "administration.php?action=purchases\">Purchases</a> page." . "<br><br>";
                $message .= "Thank you," . "<br>";
                $message .= "iRecruit" . "<br>";
                $message .= "<a href=\"http://www.irecruit-software.com\">www.irecruit-software.com</a>" . "<br>";
                
                $message  = nl2br($message);
                
                //Clear properties
                G::Obj('PHPMailer')->clearCustomProperties();
                G::Obj('PHPMailer')->clearCustomHeaders();
                
                // Set who the message is to be sent to
                G::Obj('PHPMailer')->addAddress ( $to );
        	// Set BCC
            	G::Obj('PHPMailer')->addBCC( "dedgecomb@irecruit-software.com", "David Edgecomb" );
                // Set the subject line
                G::Obj('PHPMailer')->Subject = $subject;
                // convert HTML into a basic plain-text alternative body
                G::Obj('PHPMailer')->msgHTML ( $message );
                // Content Type Is HTML
                G::Obj('PHPMailer')->ContentType = 'text/html';
                // send mail
                G::Obj('PHPMailer')->send ();

            } // end email

	    $status .= "Captured: " . $amount . "\n";
	    $status .= "Purchase Number: " . $PurchaseNumber . "\n";
        
            
        } else { // else approved
            
            $err_message = "";
            if(isset($intuit_payment_info['errors']) && count($intuit_payment_info['errors']) > 0) {
                for($e = 0; $e < count($intuit_payment_info['errors']); $e++) {
                    $err_message .= $intuit_payment_info['errors'][$e]['message']."\n";
                }
            }

	    $status .= "Data: " . $req_data . "\n\n";
	    $status .= $err_message . "\n\n";

            Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-card-invalid.txt", $status, "a+", false);
            Logger::writeMessageToDb($OrgID, "Intuit", $status);

	G::Obj('TwilioTextingAgreement')->delTextingAgreementByOrgID($OrgID);

        $message            =   "Dear User,<br><br>";
        $message            .=  "The credit card for your texting subscription on iRecruit is showing as invalid, Your texting subscription has been temporarily de-activated.<br>";
        $message            .=  "Please activate it by updating your credit card on record with iRecruit and updating your texting agreement.<br><br>";
        $message            .=  "Regards<br>";
        $message            .=  "iRecruit Team<br>";
        $message            .=  "<br><br><br>";
        $message            .=  "<i style=\"font-size:6pt;\">" . $OrgID . "</i>";

        $to =   $TwilioOrgInfo['UserEmail'];
        
        //Clear properties
        G::Obj('PHPMailer')->clearCustomProperties();
        G::Obj('PHPMailer')->clearCustomHeaders();
        
        // Set who the message is to be sent to
        G::Obj('PHPMailer')->addAddress ( $to );
        // Set BCC
        G::Obj('PHPMailer')->addBCC( "dedgecomb@irecruit-software.com", "David Edgecomb" );
        // Set the subject line
        G::Obj('PHPMailer')->Subject = 'iRecruit Texting - Card Invalid';
        // convert HTML into a basic plain-text alternative body
        G::Obj('PHPMailer')->msgHTML ( $message );
        // Content Type Is HTML
        G::Obj('PHPMailer')->ContentType = 'text/html';
        // send mail
        G::Obj('PHPMailer')->send ();

        } // end else approved
    }
}

$status .= "\n\nCompleted\n\n";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

?>
