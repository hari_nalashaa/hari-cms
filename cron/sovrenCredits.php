#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$result = G::Obj('Sovren')->getCredits();

// Convert xml string into an object 
$new = simplexml_load_string($result); 
  
// Convert into json 
$con = json_encode($new); 
  
// Convert into associative array 
$newArr = json_decode($con, true); 

$to	    =   "bkelly@cmshris.com";
$bcc        =   "dedgecomb@irecruit-software.com";
$subject    =   "Sovren Credit Report";

//Clear properties
G::Obj('PHPMailer')->clearCustomProperties();
G::Obj('PHPMailer')->clearCustomHeaders();

// Set who the message is to be sent to
G::Obj('PHPMailer')->addAddress ( $to );
G::Obj('PHPMailer')->addBCC( $bcc);
// Set the subject line
G::Obj('PHPMailer')->Subject = $subject;
// convert HTML into a basic plain-text alternative body
G::Obj('PHPMailer')->msgHTML ( "Sovren credits remaining: " . $newArr['Value']['CreditsRemaining'] . "<br>\nExpires: " . $newArr['Value']['ExpirationDate'] . "<br>\n");
// Content Type Is HTML
G::Obj('PHPMailer')->ContentType = 'text/plain';
//Send email
G::Obj('PHPMailer')->send ();

?>
