#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

define ('DIR', ROOT . 'wotc_files/Dycom/');

$rtn="";

$file = "hiredatawstate.csv";


if(file_exists(DIR . $file)) {

  $rtn .= processFile($file);

}


function processFile($file) {

$FEDID=0;
$STATE=1;
$JAN2018=2;
$FEB2018=3;
$MAR2018=4;
$APR2018=5;
$MAY2018=6;
$JUN2018=7;
$JUL2018=8;
$AUG2018=9;
$SEP2018=10;
$OCT2018=11;
$NOV2018=12;
$DEC2018=13;

$JAN2019=15;
$FEB2019=16;
$MAR2019=17;
$APR2019=18;
$MAY2019=19;
$JUN2019=20;
$JUL2019=21;
$AUG2019=22;
$SEP2019=23;
$OCT2019=24;
$NOV2019=25;
$DEC2019=26;

$JAN2020=28;
$FEB2020=29;
$MAR2020=30;
$APR2020=31;
$MAY2020=32;
$JUN2020=33;
$JUL2020=34;
$AUG2020=35;
$SEP2020=36;

$csv = array_map('str_getcsv', file(DIR . $file));

$i=0;
foreach ($csv as $row) {
$i++;

 if ($i > 3) {

     insertData($row[$FEDID], $row[$STATE], "2018-01-01", $row[$JAN2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-02-01", $row[$FEB2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-03-01", $row[$MAR2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-04-01", $row[$APR2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-05-01", $row[$MAY2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-06-01", $row[$JUN2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-07-01", $row[$JUL2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-08-01", $row[$AUG2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-09-01", $row[$SEP2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-10-01", $row[$OCT2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-11-01", $row[$NOV2018]);
     insertData($row[$FEDID], $row[$STATE], "2018-12-01", $row[$DEC2018]);

     insertData($row[$FEDID], $row[$STATE], "2019-01-01", $row[$JAN2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-02-01", $row[$FEB2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-03-01", $row[$MAR2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-04-01", $row[$APR2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-05-01", $row[$MAY2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-06-01", $row[$JUN2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-07-01", $row[$JUL2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-08-01", $row[$AUG2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-09-01", $row[$SEP2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-10-01", $row[$OCT2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-11-01", $row[$NOV2019]);
     insertData($row[$FEDID], $row[$STATE], "2019-12-01", $row[$DEC2019]);

     insertData($row[$FEDID], $row[$STATE], "2020-01-01", $row[$JAN2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-02-01", $row[$FEB2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-03-01", $row[$MAR2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-04-01", $row[$APR2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-05-01", $row[$MAY2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-06-01", $row[$JUN2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-07-01", $row[$JUL2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-08-01", $row[$AUG2020]);
     insertData($row[$FEDID], $row[$STATE], "2020-09-01", $row[$SEP2020]);
  }

} // end foreach


} // end function

function insertData($FEDID,$State, $ReportingDate,$HireCnt) {

  if (preg_replace('/[^0-9.]+/', '',$FEDID) != "") {
       //Skip fields on duplicate key update
        $skip   =   array('FEDID', 'ReportingDate');
        $info   =   array(
                        'FEDID'             =>  preg_replace('/[^0-9.]+/', '',$FEDID),
                        'State'             =>  $State,
                        'ReportingDate'     =>  $ReportingDate,
                        'HireCnt'           =>  $HireCnt
                    );
        G::Obj('WOTCOrganization')->insUpdDycomHireHistory($info, $skip);

        echo print_r($info,true);
  } // end if

} // end function

echo 'done';

?>

