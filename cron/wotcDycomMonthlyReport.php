#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";

$query = "SELECT DAY(now()) AS TODAY, DAY(DATE_SUB(LAST_DAY(now()), INTERVAL 7 DAY)) AS SEVENDAYS";
$DATECHECK =   G::Obj('GenericQueries')->getInfoByQuery($query);

if ($DATECHECK[0]['TODAY'] > $DATECHECK[0]['SEVENDAYS']) {

$update_query = "INSERT INTO DycomHireHistory 
SELECT FEDID, State, date_add(date_add(LAST_DAY(LastHiredDate),interval 1 DAY),interval -1 MONTH) AS ReportingDate, COUNT(*) AS HireCnt  
FROM DycomHires 
WHERE LastHiredDate > date_sub(LAST_DAY(NOW()),interval 2 MONTH)
GROUP BY FEDID, State ON DUPLICATE KEY UPDATE HireCnt = HireCnt";

//WHERE LastHiredDate BETWEEN date_add(date_sub(LAST_DAY(NOW()),interval 1 MONTH), interval 1 DAY) AND LAST_DAY(NOW())

$params         =   array();
G::Obj('GenericQueries')->updInfoByQuery($update_query, array($params));

$query = "select
concat(OrgData.EIN1,'-',OrgData.EIN2) AS FEIN, IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State) AS State, IF(OrgData.CompanyIdentifier != '' ,OrgData.CompanyIdentifier , CRMFEIN.AKADBA) AS CompanyName,
year(ApplicantData.EntryDate) as Year,
month(ApplicantData.EntryDate) as MonthSort,
monthname(ApplicantData.EntryDate) as Month,
IFNULL(((count(*) / IFNULL( (select HireCnt from DycomHireHistory where FEDID = REPLACE(FEIN,'-','') and year(ReportingDate) = year(EntryDate) and month(ReportingDate) = month(EntryDate) and State = IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State)),'.00'))),'.00') as ScreenedToHireRatio,
IFNULL((select HireCnt from DycomHireHistory where FEDID = REPLACE(FEIN,'-','') and year(ReportingDate) = year(EntryDate) and month(ReportingDate) = month(EntryDate) and State = IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State)),'0') AS Hiredcount,
count(*) as Screenedcount,
sum(if(Billing.TaxCredit is not null, Billing.TaxCredit ,0)) as RealizedTaxCredit,
sum(if(Processing.Received != '0000-00-00', 1,0)) as ReceivedCMScount,
sum(if(Processing.Received != '0000-00-00' and Processing.Filed != '0000-00-00', 1,0)) as FiledToStatecount,
sum(if(Processing.Received != '0000-00-00' and Processing.Filed != '0000-00-00' and Processing.Confirmed != '0000-00-00' and Processing.Qualified = 'Y', 1,0)) as StateQualifiedcount,
sum(if(Processing.Received != '0000-00-00' and Processing.Filed != '0000-00-00' and Processing.Confirmed = '0000-00-00' and Processing.Qualified = 'N', 1,0)) as StateDeniedcount,
sum(if(ApplicantData.Qualifies = 'Y' and Processing.Qualified = 'Y' and Billing.DateBilled != '0000-00-00', 1,0)) as Billedcount,
sum(if(ApplicantData.Qualifies = 'N' and Processing.Qualified = 'Y', 1,0)) as NoCreditcount
from ApplicantData
JOIN OrgData ON OrgData.wotcID = ApplicantData.wotcID AND OrgData.CompanyID = '62f41708245691' 
LEFT JOIN Processing ON Processing.wotcID = ApplicantData.wotcID AND Processing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN Billing ON Billing.wotcID = ApplicantData.wotcID AND Billing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2
where ApplicantData.ApplicationStatus = 'H'
group by concat(OrgData.EIN1,'-',OrgData.EIN2), IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State), year(ApplicantData.EntryDate), month(ApplicantData.EntryDate)
order by  year(ApplicantData.EntryDate) DESC, month(ApplicantData.EntryDate) DESC, CompanyName";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

$objPHPExcel = new Spreadsheet();
$objPHPExcel->getProperties()->setCreator("CMS WOTC")
       ->setLastModifiedBy("System")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Not Active")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:P1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Year');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Month');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('FEDID');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('State');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Organization Name');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Screened to Hired');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('No. Hired');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('No. Screened');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Realized Tax Credit');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('No. Received at CMS');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('No. Filed to State');
$objPHPExcel->getActiveSheet()->getCell('L1')->setValue('No. Confirmed from State');
$objPHPExcel->getActiveSheet()->getCell('M1')->setValue('No. State Qualified');
$objPHPExcel->getActiveSheet()->getCell('N1')->setValue('No. State Denied');
$objPHPExcel->getActiveSheet()->getCell('O1')->setValue('No. Completed');
$objPHPExcel->getActiveSheet()->getCell('P1')->setValue('No.  No Credit');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


  $n=1;
  foreach ($RESULTS AS $DATA) {
    $n++;

        $objPHPExcel->getActiveSheet()->getStyle('F'.$n)->getNumberFormat()->setFormatCode("0.00%");
        $objPHPExcel->getActiveSheet()->getStyle('I'.$n)->getNumberFormat()->setFormatCode("#,##0.00");

$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($DATA['Year']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue( $DATA['MonthSort'] . "-" . $DATA['Month']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($DATA['FEIN']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($DATA['State']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($DATA['CompanyName']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($DATA['ScreenedToHireRatio']);
$objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($DATA['Hiredcount']);
$objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($DATA['Screenedcount']);
$objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($DATA['RealizedTaxCredit']);
$objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($DATA['ReceivedCMScount']);
$objPHPExcel->getActiveSheet()->getCell('K'.$n)->setValue($DATA['FiledToStatecount']);
$objPHPExcel->getActiveSheet()->getCell('L'.$n)->setValue($DATA['StateQualifiedcount'] + $DATA['StateDeniedcount']);
$objPHPExcel->getActiveSheet()->getCell('M'.$n)->setValue($DATA['StateQualifiedcount']);
$objPHPExcel->getActiveSheet()->getCell('N'.$n)->setValue($DATA['StateDeniedcount']);
$objPHPExcel->getActiveSheet()->getCell('O'.$n)->setValue($DATA['Billedcount']);
$objPHPExcel->getActiveSheet()->getCell('P'.$n)->setValue($DATA['NoCreditcount']);


  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Monthly Performance Totals");

//Completed;

$query = "select 
(SELECT FiscalYear FROM DycomFY WHERE Processing.Received BETWEEN BeginDate AND EndDate) AS FiscalYear,
concat(OrgData.EIN1,'-',OrgData.EIN2) AS FEIN, IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State) AS State,
 IF(OrgData.CompanyIdentifier != '' ,OrgData.CompanyIdentifier , CRMFEIN.AKADBA) AS CompanyName, Billing.TaxCredit AS RealizedTaxCredit,
 Billing.DateBilled as VoucherDate, Processing.Received AS ConfirmedFromState,
 ApplicantData.EntryDate, ApplicantData.ApplicationID,
 concat(ApplicantData.FirstName, ' ',ApplicantData.MiddleName,' ',ApplicantData.LastName) AS Name
from ApplicantData
JOIN OrgData ON OrgData.wotcID = ApplicantData.wotcID  AND OrgData.CompanyID = '62f41708245691'
LEFT JOIN Processing ON Processing.wotcID = ApplicantData.wotcID AND Processing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN Billing ON Billing.wotcID = ApplicantData.wotcID AND Billing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2
where ApplicantData.ApplicationStatus = 'H'
and ApplicantData.Qualifies = 'Y'
and Processing.Qualified = 'Y'
and (Billing.DateBilled != '0000-00-00' AND Billing.DateBilled is not null)
order by FiscalYear, IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State), ApplicantData.ApplicationID";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Create a second sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(1);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setVertical('center');


$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Fiscal Year');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('FEDID');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('State');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Organization Name');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('Realized Tax Credit');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Application Date');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Confirmed Date');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Voucher Date');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('WOTC ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Employee Name');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


  $n=1;
  foreach ($RESULTS AS $DATA) {
    $n++;

        $objPHPExcel->getActiveSheet()->getStyle('E'.$n)->getNumberFormat()->setFormatCode("#,##0.00");


$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($DATA['FiscalYear']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($DATA['FEIN']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($DATA['State']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($DATA['CompanyName']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($DATA['RealizedTaxCredit']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($DATA['EntryDate']);
$objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($DATA['ConfirmedFromState']);
$objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($DATA['VoucherDate']);
$objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($DATA['ApplicationID']);
$objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($DATA['Name']);


  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Completed Applicants");

//Applicants Awaiting Payroll Data;

$query = "select 
Processing.HiredDate, ApplicantData.ApplicationID, concat(ApplicantData.FirstName, ' ',ApplicantData.MiddleName,' ',ApplicantData.LastName) AS Name,
concat(OrgData.EIN1,'-',OrgData.EIN2) AS FEIN, IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State) AS State,
IF(OrgData.CompanyIdentifier != '' ,OrgData.CompanyIdentifier , CRMFEIN.AKADBA) AS CompanyName 
from ApplicantData
JOIN OrgData ON OrgData.wotcID = ApplicantData.wotcID AND OrgData.CompanyID = '62f41708245691'
LEFT JOIN Processing ON Processing.wotcID = ApplicantData.wotcID AND Processing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN Billing ON Billing.wotcID = ApplicantData.wotcID AND Billing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2
where ApplicantData.ApplicationStatus = 'H'
and ApplicantData.Qualifies = 'Y'
and Processing.Qualified = 'Y'
and Billing.DateBilled is null
order by Processing.HiredDate";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Create a second sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(2);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setVertical('center');


$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Hired Date');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Name');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('FEDID');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('State');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Organization Name');


for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


  $n=1;
  foreach ($RESULTS AS $DATA) {
    $n++;

        
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($DATA['HiredDate']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($DATA['ApplicationID']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($DATA['Name']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($DATA['FEIN']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($DATA['State']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($DATA['CompanyName']);

  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Applicants Waiting Payroll Data");


//Applicants No Credit;

$query = "select 
Processing.HiredDate, ApplicantData.ApplicationID,
 concat(ApplicantData.FirstName, ' ',ApplicantData.MiddleName,' ',ApplicantData.LastName) AS Name,
concat(OrgData.EIN1,'-',OrgData.EIN2) AS FEIN, IF(ApplicantData.WorkState != '',ApplicantData.WorkState,ApplicantData.State) AS State, IF(OrgData.CompanyIdentifier != '' ,OrgData.CompanyIdentifier , CRMFEIN.AKADBA) AS CompanyName
from ApplicantData
JOIN OrgData ON OrgData.wotcID = ApplicantData.wotcID AND OrgData.CompanyID = '62f41708245691'
LEFT JOIN Processing ON Processing.wotcID = ApplicantData.wotcID AND Processing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN Billing ON Billing.wotcID = ApplicantData.wotcID AND Billing.ApplicationID = ApplicantData.ApplicationID
LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2
where ApplicantData.ApplicationStatus = 'H'
and ApplicantData.Qualifies = 'N'
and Processing.Qualified = 'Y'
order by Processing.HiredDate";

$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query);

// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Create a second sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(3);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getAlignment()->setVertical('center');


$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Hired Date');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('Name');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('FEDID');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('State');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('Organization Name');


for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


  $n=1;
  foreach ($RESULTS AS $DATA) {
    $n++;

        
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($DATA['HiredDate']);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($DATA['ApplicationID']);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($DATA['Name']);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($DATA['FEIN']);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($DATA['State']);
$objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($DATA['CompanyName']);


  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("No Credit Applicants");

$objPHPExcel->setActiveSheetIndex(0);

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

$subject = "Dycom Report " . date("m/d/Y");

$message = "Here is your monthly WOTC report.<br>\nData is as of: " . date("m/d/Y") . "<br><br>\n\n";

$message .= "Thank you!<br>\n";
$message .= "<strong>WOTC Processing Team</strong><br>\n";
$message .= "860-269-3044 | <a href=\"https://www.cmswotc.com\">www.cmswotc.com</a><br>\n";

// Set who the message is to be sent to
$PHPMailerObj->addAddress("alan.nemeth@dycominc.com", "Alan Nemeth");
$PHPMailerObj->addAddress("elizabeth.sama@dycominc.com", "Elizabeth Sama");
$PHPMailerObj->addAddress("priscilla.bautista@dycominc.com", "Priscilla Arteaga Bautista");
$PHPMailerObj->addAddress("benjamin.perlman@dycominc.com", "Benjamin Perlman");
$PHPMailerObj->addAddress("eric.lewis@dycominc.com", "Eric Lewis");
$PHPMailerObj->addBCC("lschneider@cmshris.com", "Lisa Schneider");
$PHPMailerObj->addBCC("dedgecomb@irecruit-software.com", "David Edgecomb");

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';

$tempfile=ROOT.'cron/temp/tmpreport.xlsx';
$objWriter= IOFactory::createWriter($objPHPExcel , 'Xlsx');
$objWriter->save($tempfile);


$data = file_get_contents($tempfile);

$PHPMailerObj->addStringAttachment($data, 'DycomReport.xlsx', 'base64', 'text/html');

// Send email
$PHPMailerObj->send();

unlink($tempfile);

$application="wotcDycomMonthlyReport.php";
$status="Done\n" . $rtn;

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);

} // end if TODAY > SEVENDAYS

?>

