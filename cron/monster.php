#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$OrgID = 'I20120420';

$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

$xml .= "<jobs>\n";

$table_name =   "RequisitionOrgLevels ROL, Requisitions R";
$columns    =   "ROL.OrgLevelID, ROL.SelectionOrder, R.OrgID, R.MultiOrgID, R.RequestID, R.Title, R.Description, R.EmpStatusID, date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate, R.City, R.State, R.ZipCode";
$where_info =   array(
                    "R.OrgID        =   ROL.OrgID",
                    "R.OrgID        =   :OrgID",
                    "R.RequestID    =   ROL.RequestID",
                    "R.Active       =   'Y'",
                    "NOW() BETWEEN R.PostDate AND R.ExpireDate",
                    "(R.PresentOn = 'PUBLICONLY' OR R.PresentOn = 'INTERNALANDPUBLIC')"
                );
$params     =   array(
                    ":OrgID"        =>  $OrgID
                );
$RESULTS    =   G::Obj('GenericQueries')->getAllRowsInfo($table_name, $columns, $where_info, 'R.RequestID', 'R.OrgID, R.Title', array($params));

$i = 0;
foreach ($RESULTS as $REQ) {
    $i ++;
    
    // Get Default RequisitionFormID
    $STATUSLEVELS = array();
    $req_det_info = $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $REQ['OrgID'], $REQ['RequestID']);
    $STATUSLEVELS = $RequisitionDetailsObj->getEmploymentStatusLevelsList($REQ['OrgID'], $req_det_info['RequisitionFormID']);
    $EmploymentStatusLevel = $STATUSLEVELS[$REQ['EmpStatusID']];
    
    // Get OrganizationName, DemoAccount
    $org_details = $OrganizationDetailsObj->getOrganizationInformation($REQ['OrgID'], $REQ['MultiOrgID'], "OrganizationName, DemoAccount");
    $OrganizationName = $org_details['OrganizationName'];
    $DemoAccount = $org_details['DemoAccount'];
    
    // Required
    if (($REQ['Title']) && ($REQ['PostDate']) && ($REQ['RequestID']) && ($REQ['OrgLevelID']) && ($REQ['SelectionOrder']) && ($REQ['Description']) && ($DemoAccount != 'Y')) {
        
        $xml .= "<job>\n";
        
        $xml .= "   <jobtitle><![CDATA[";
        $xml .= $REQ['Title'];
        $xml .= "]]></jobtitle>\n";
        
        $xml .= "   <referencenumber><![CDATA[";
        $xml .= $REQ['RequestID'];
        $xml .= "]]></referencenumber>\n";
        
        $xml .= "   <url><![CDATA[";
        
        $xml .= "https://www.irecruit-us.com/jobRequest.php?";
        $xml .= "OrgID=" . $REQ['OrgID'];
        if ($REQ['MultiOrgID'] != "") {
            $xml .= "&MultiOrgID=" . $REQ['MultiOrgID'];
        } 
        $xml .= "&RequestID=";
        $xml .= $REQ['RequestID'];
        
        $xml .= "&source=MONSTER";
        
        $xml .= "]]></url>\n";
        
        if ($OrganizationName) {
            $xml .= "   <company><![CDATA[";
            $xml .= $OrganizationName;
            $xml .= "]]></company>\n";
        }
        
        if ($REQ['City']) {
            $xml .= "   <city><![CDATA[";
            $xml .= $REQ['City'];
            $xml .= "]]></city>\n";
        }
        
        if ($REQ['State']) {
            $xml .= "   <State><![CDATA[";
            $xml .= $REQ['State'];
            $xml .= "]]></State>\n";
        }
        
        if ($REQ['ZipCode']) {
            $xml .= "   <Zip><![CDATA[";
            $xml .= $REQ['ZipCode'];
            $xml .= "]]></Zip>\n";
        }
        
        $xml .= "   <Description><![CDATA[";
        $xml .= utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $REQ['Description']));
        $xml .= "]]></Description>\n";
        
        $xml .= "   <Requirements><![CDATA[";
        $xml .= "]]></Requirements>\n";
        
        if ($EmploymentStatusLevel) {
            $xml .= "   <jobtype><![CDATA[";
            $xml .= $EmploymentStatusLevel;
            $xml .= "]]></jobtype>\n";
        }
        
        $xml .= "</job>\n";
    } // end if all required fields are populated
} // end foreach

$xml .= "</jobs>\n";

$filename = IRECRUIT_DIR . 'monster/' . $OrgID . '.xml';
$fh = fopen($filename, 'w');
fwrite($fh, $xml);
fclose($fh);
chmod($filename, 0666);

$application = "monster.php";
$status = "script okay";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
