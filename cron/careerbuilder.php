#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$formatted_date =   G::Obj('MysqlHelper')->getDatesList("DATE_FORMAT(NOW(), '%a, %d %b %Y %H:%i:%s EST') AS FORMATTED_DATE");
$TS             =   $formatted_date['FORMATTED_DATE'];

$OrgID = 'I20120605'; // Goodwill of NY NJ

$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

$xml .= "<source>\n";

$xml .= "<publisher>iRecruit ATS</publisher>\n";
$xml .= "<publisherurl>https://apps.irecruit-us.com/careerbuilder/" . $OrgID . ".xml</publisherurl>\n";
$xml .= "<lastBuildDate>" . $TS . "</lastBuildDate>\n";

$i = 0;

$table_name =   "RequisitionOrgLevels ROL, Requisitions R, JobBoards JB";
$columns    =   "ROL.OrgLevelID, ROL.SelectionOrder, R.OrgID, R.MultiOrgID, R.RequestID, R.Title, R.Description, R.EmpStatusID, date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate, R.City, R.State, R.ZipCode";
$where_info =   array("R.OrgID = ROL.OrgID", "R.OrgID = '$OrgID'", "R.RequestID = ROL.RequestID", "R.OrgID = JB.OrgID", "JB.Indeed = 'Y'", "R.MultiOrgID = JB.MultiOrgID", "R.ExpireDate > NOW()", "R.Active = 'Y'", "NOW() BETWEEN R.PostDate AND R.ExpireDate", "(R.PresentOn = 'PUBLICONLY' OR R.PresentOn = 'INTERNALANDPUBLIC')");
$RESULTS    =   G::Obj('GenericQueries')->getAllRowsInfo($table_name, $columns, $where_info, 'R.RequestID', 'R.OrgID, R.Title');

$i = 0;
foreach ($RESULTS as $REQ) {
    $i ++;
    
    // Get Default RequisitionFormID
    $STATUSLEVELS = array();
    $req_det_info = G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionFormID", $REQ['OrgID'], $REQ['RequestID']);
    $STATUSLEVELS = G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($REQ['OrgID'], $req_det_info['RequisitionFormID']);
    $EmploymentStatusLevel = $STATUSLEVELS[$REQ['EmpStatusID']];
    
    // Get OrganizationName, DemoAccount
    $org_details        =   G::Obj('OrganizationDetails')->getOrganizationInformation($REQ['OrgID'], $REQ['MultiOrgID'], "OrganizationName, DemoAccount");
    $OrganizationName   =   $org_details['OrganizationName'];
    $DemoAccount        =   $org_details['DemoAccount'];

    // Set columns
    $columns        =   "OLD.CategorySelection";
    $table_name     =   "RequisitionOrgLevels ROL, OrganizationLevelData OLD";
    // Set condition
    $where          =   array (
                            "ROL.OrgID          =   OLD.OrgID",
                            "ROL.OrgID          =   :OrgID",
                            "ROL.OrgLevelID     =   OLD.OrgLevelID",
                            "ROL.SelectionOrder =   OLD.SelectionOrder",
                            "ROL.RequestID      =   :RequestID"
                        );
    // Set params
    $params         =   array (
                            ":OrgID"            =>  $REQ['OrgID'],
                            ":RequestID"        =>  $REQ['RequestID'],
                        );
    // Get Org Requisition Levels Information
    $resultsIN          =   G::Obj('Organizations')->getOrgAndReqLevelsInfo ( $table_name, $columns, $where, '', array($params) );
    $CategorySelection  =   $resultsIN['results'][0]['CategorySelection'];    
    
    // Required
    if (($REQ['Title']) && ($REQ['PostDate']) && ($REQ['RequestID']) && ($REQ['OrgLevelID']) && ($REQ['SelectionOrder']) && ($REQ['Description']) && ($DemoAccount != 'Y')) {
        
        $xml .= "<job>\n";
        
        $xml .= "   <title><![CDATA[";
        $xml .= preg_replace("/[^A-Z0-9a-z &]/i", '', $REQ['Title']);
        // $xml .= $REQ['Title'];
        $xml .= "]]></title>\n";
        
        $xml .= "   <date><![CDATA[";
        $xml .= $REQ['PostDate'];
        $xml .= "]]></date>\n";
        
        $xml .= "   <referencenumber><![CDATA[";
        $xml .= $REQ['RequestID'];
        $xml .= "]]></referencenumber>\n";
        
        $xml .= "   <url><![CDATA[";
        
        $xml .= "https://www.irecruit-us.com/jobRequest.php?";
        $xml .= "OrgID=" . $REQ['OrgID'];
        if ($REQ['MultiOrgID'] != "") {
            $xml .= "&MultiOrgID=" . $REQ['MultiOrgID'];
        } 
        $xml .= "&RequestID=";
        $xml .= $REQ['RequestID'];
        
        $xml .= "&source=CB";
        
        $xml .= "]]></url>\n";
        
        if ($OrganizationName) {
            $xml .= "   <company><![CDATA[";
            $xml .= preg_replace("/[^A-Z0-9a-z ]/i", '', $OrganizationName);
            $xml .= "]]></company>\n";
        }
        
        if ($REQ['City']) {
            $xml .= "   <city><![CDATA[";
            $xml .= preg_replace("/[^A-Za-z ]/i", '', $REQ['City']);
            // $xml .= $REQ['City'];
            $xml .= "]]></city>\n";
        }
        
        if ($REQ['State']) {
            $xml .= "   <state><![CDATA[";
            $xml .= preg_replace("/[^A-Za-z]/i", '', $REQ['State']);
            // $xml .= $REQ['State'];
            $xml .= "]]></state>\n";
        }
        
        if ($REQ['ZipCode']) {
            $xml .= "   <postalcode><![CDATA[";
            $xml .= preg_replace("/[^0-9]/i", '', $REQ['ZipCode']);
            // $xml .= $REQ['ZipCode'];
            $xml .= "]]></postalcode>\n";
        }
        
        $xml .= "   <country><![CDATA[";
        if ($REQ['Country']) {
            $xml .= $REQ['Country'];
        } else {
            $xml .= "US";
        }
        $xml .= "]]></country>\n";
        
        $xml .= "   <description><![CDATA[";
        $xml .= utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $REQ['Description']));
        $xml .= "]]></description>\n";
        
        // $xml .= " <salary><![CDATA[";
        // $xml .= "]]></salary>\n";
        
        // $xml .= " <education><![CDATA[";
        // $xml .= "]]></education>\n";
        
        if ($EmploymentStatusLevel) {
            $xml .= "   <jobtype><![CDATA[";
            $xml .= $EmploymentStatusLevel;
            $xml .= "]]></jobtype>\n";
        }
        
        if ($CategorySelection) {
            $xml .= "   <categoryselection><![CDATA[";
            $xml .= $CategorySelection;
            $xml .= "]]></categoryselection>\n";
        }
        
        // $xml .= " <category><![CDATA[";
        // $xml .= "]]></category>\n";
        
        // $xml .= " <experience><![CDATA[";
        // $xml .= "]]></experience>\n";
        
        $xml .= "</job>\n";
    } // end if all required fields are populated
} // end while

$xml .= "</source>\n";

$filename = IRECRUIT_DIR . 'careerbuilder/' . $OrgID . '.xml';
$fh = fopen($filename, 'w');
fwrite($fh, $xml);
fclose($fh);
chmod($filename, 0666);

$application = "careerbuilder.php";
$status = "script okay";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
