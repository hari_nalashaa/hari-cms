#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$segments = array(
                            "sms-inbound"=>".0075",
                            "sms-outbound"=>".0075",
                            "mms-inbound"=>".01",
                            "mms-outbound"=>".02"
);

$rtn = "";

G::Obj('GenericQueries')->conn_string =   "IRECRUIT";

//Get Twilio Accounts
$twilio_accounts_info   =   G::Obj('TwilioAccounts')->getTwilioAccountsList("OrgID ASC");
foreach ($twilio_accounts_info['results'] AS $TA) {

  if ($TA['OrgID'] == 'B12345467') { /* Clean up Demo Account sooner */
    $DAYSTOARCHIVE=7;
    $RESERVEDPHONENOS=2;
  } else {
    $DAYSTOARCHIVE=30;
    $RESERVEDPHONENOS=8;
  }

  //if ($TA['OrgID'] == 'B12345467') { // For testing or limiting one organization
  if (1) {

  $query = "SELECT OrganizationName FROM OrgData WHERE OrgID = :OrgID";
  $params     =   array(':OrgID'    =>  $TA['OrgID']);
  $ORG =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));


  $rtn .= "--------------------------------<br>\n";
  $rtn .= "OrgID: " . $TA['OrgID'] . " - " . $ORG[0]['OrganizationName'] . "<br>\n<br>\n";

  $conversations	=       G::Obj('TwilioConversationApi')->fetchAllConversationResources($TA['OrgID']);

  $MESSAGES = array();

  if (count($conversations['Response']) > 0) {

    $query = "SELECT UserID, count(*) AS cnt FROM TwilioConversations WHERE OrgID = :OrgID GROUP BY UserID";
    $params     =   array(':OrgID'    =>  $TA['OrgID']);
    $RESULTS  =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

    $rtn .= "Total Active Users: " . count($RESULTS) . "<br>\n";

    foreach ($RESULTS AS $U) {
	    $rtn .= "User: " . $U['UserID'] . ", Conversations: " . $U['cnt'] . "<br>\n";
    } // end foreach

    if (count($RESULTS) > 0) {
    $rtn .= "<br>\n";
    }

    $rtn .= "Total Active Conversations less than 30 days: " . count($conversations['Response']) . "<br>\n";

    $i=0;
    foreach ($conversations['Response'] AS $C) {
	    $i++;

	    $ResourceID	=   $C->sid;

    $query = "select concat(Users.LastName, ', ', Users.FirstName) as Sender, TwilioConversations.MobileNumbers";
    $query .= " from Users join TwilioConversations ON";
    $query .= " Users.OrgID = TwilioConversations.OrgID and Users.UserID = TwilioConversations.UserID";
    $query .= " where TwilioConversations.OrgID = :OrgID";
    $query .= " and TwilioConversations.ResourceID = :ResourceID";
    $params     =   array(':OrgID'    =>  $TA['OrgID'], ':ResourceID'    =>  $ResourceID);
    $CONVERSATION =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

    $Sender = $CONVERSATION[0]['Sender'];
    $Numbers = json_decode($CONVERSATION[0]['MobileNumbers']);
    $SNumber = implode("%",str_split(substr($Numbers[0][1],-10),3));

    $query = "select JobApplications.ApplicantSortName as Receiver, JobApplications.ApplicationID";
    $query .= " from JobApplications"; 
    $query .= " join ApplicantData on ApplicantData.OrgID = JobApplications.OrgID";
    $query .= " and ApplicantData.ApplicationID = JobApplications.ApplicationID";
    $query .= " where JobApplications.OrgID = :OrgID"; 
    $query .= " and ApplicantData.QuestionID = 'cellphone'";
    $query .= " and ApplicantData.Answer like :Phone";
    $params     =   array(':OrgID'    =>  $TA['OrgID'], ':Phone'    =>  '%'.$SNumber.'%');
    $Receiver =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

	    //$rtn .= "Conversation-" . $i . ": " . $ResourceID;
	    $rtn .= "Conversation-" . $i . ": From: " . $Sender . " to " . $Receiver[0]['Receiver'] . " (".$Receiver[0]['ApplicationID'].")";

            $messages =   G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($TA['OrgID'], $ResourceID);

	    $MESSAGES = array();
	    $conversation_messages = array();
	    $conversation_msg_skip_fields = array();

	    $ii=0;
	    foreach ($messages['Response'] AS $M) {

                if(is_object($M)) {
                    $reflection     =   new ReflectionClass($M);
                    $property       =   $reflection->getProperty("properties");
                    $property->setAccessible(true);
                    $MESSAGES[]     =   $msg_info  = $property->getValue($M);

                    $message_info   =   array();

		    $date_created_info = (array)$MESSAGES[$ii]['dateCreated'];
		    $date_updated_info = (array)$MESSAGES[$ii]['dateUpdated'];

                    $message_info['ConversationResourceID']     =   $msg_info['conversationSid'];
                    $message_info['ConversationMessageID']      =   $msg_info['sid'];
                    $message_info['Author']                     =   $msg_info['author'];
                    $message_info['Body']                       =   utf8_decode($msg_info['body']);
                    $message_info['DateCreatedDate']            =   $date_created_info['date'];
                    $message_info['DateCreatedTimeZone_Type']   =   $date_created_info['timezone_type'];
                    $message_info['DateCreatedTimeZone']        =   $date_created_info['timezone'];
                    $message_info['DateUpdatedDate']            =   $date_updated_info['date'];
                    $message_info['DateUpdatedTimeZone_Type']   =   $date_updated_info['timezone_type'];
                    $message_info['DateUpdatedTimeZone']        =   $date_updated_info['timezone'];
                    $message_info['CreatedDateTime']            =   "NOW()";

                    //Assign Data To Arrays
                    $conversation_messages[$msg_info['sid']]           =   $message_info;

		    //Skip fields
                    $skip_fields    =   array_keys($message_info);
                    array_pop($skip_fields);

		    $conversation_msg_skip_fields[$msg_info['sid']]    =   $skip_fields;

		} // end is object

	    $ii++;
    	    } // end foreach Message 

	    $rtn .= ", Total Messages: " . count($MESSAGES);

	    $sort_date = array_column($MESSAGES, 'dateCreated');
	    array_multisort($sort_date, SORT_DESC, $MESSAGES);

		$date_info = (array)$MESSAGES[0]['dateCreated'];

		$earlier = new DateTime(date("Y-m-d", strtotime($date_info['date'])));
		$later = new DateTime(date("Y-m-d"));
		$days           =   $later->diff($earlier)->format("%a");

	        $rtn .= ", Last Communication: " . $days . " days";

		if ($days > $DAYSTOARCHIVE) { 

		    $rtn .= " - Archive this Conversation"; 

		} // end $DAYSTOARCHIVE 
		$rtn .= "<br>\n";

    } // end foreach Conversation
	$rtn .= "<br>\n";
  } // end if conversations > 0

  $account_phone_numbers  =     G::Obj('TwilioPhoneNumbersApi')->getAccountIncomingNumbers($TA['OrgID']);
  $phone_numbers_info     =   	$account_phone_numbers['PhoneNumbersInfo'];
  $phone_numbers_cnt	  =	count($phone_numbers_info);

  if ($phone_numbers_cnt > 0) {
    $rtn .= "Total Phone Numbers Allocated: " . $phone_numbers_cnt . ", <strong>$" . number_format($phone_numbers_cnt,2 ,'.', ',') . "</strong><br>\n";
  } else {
    $rtn .= "Account not in use.<br>\n";
  }

  $ii=0;
  $x=0;
  foreach ($phone_numbers_info as $PN) {
  $ii++;
    $query = "SELECT * FROM TwilioConversations WHERE OrgID = :OrgID and MobileNumbers like :MN";
    $params     =   array(':OrgID'    =>  $TA['OrgID'], ':MN' => '%' . $PN['phoneNumber'] . '%');
    $PHCK =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
    $in_use_cnt = count($PHCK);
    $rtn .= $ii . " - " . $PN['phoneNumber'] . " - ";
    if ($in_use_cnt > 0) { 
	$rtn .= "In Use: " . $in_use_cnt; 
    } else {
	$x++;
	if (($x <= $RESERVEDPHONENOS) && ($ii <= ($TA['Credits'] + 2))) {
	  $rtn .= "Reserved";
	} else {

	  $rtn .= " - Remove this Phone Number - " . $PN['sid'];

 	}
    }
    if ($phone_numbers_cnt > 0) {
    $rtn .= "<br>\n";
    }


  } // end foreach

  $records_info = array();

    $last_month_records_info    =       G::Obj('TwilioUsageRecordsApi')->getLastMonthUsageForAllCategories($TA['OrgID']);
    $last_month_records                 =       $last_month_records_info['Response'];

    for($c = 0; $c < count($last_month_records); $c++) {

        $last_month_obj =       $last_month_records[$c];

        //Get the Protected properties from SMS object, by extending the object through ReflectionClass
        $reflection     =   new ReflectionClass($last_month_obj);
        $property       =   $reflection->getProperty("properties");
        $property->setAccessible(true);
        $records_info[] =   $property->getValue($last_month_obj);
    }

    $total_segments = 0;
    $total_messages = 0;

    if(count($records_info) > 0) {
      for($r = 0; $r < count($records_info); $r++) {
        if(in_array($records_info[$r]['category'], array_keys($segments))) {
            $total_segments += $records_info[$r]['usage'];
            $total_messages += $records_info[$r]['count'];
        }
      }
    }

    $total_costs = 0;
    $breakdown_costs = "";
    if(count($records_info) > 0) {
        for($r = 0; $r < count($records_info); $r++) {
            if(in_array($records_info[$r]['category'], array_keys($segments))) {
                $breakdown_costs .= $records_info[$r]['category'] . ": ";
                $breakdown_costs .= $records_info[$r]['count'] . " " . $records_info[$r]['countUnit'] . ", ";
                $breakdown_costs .= $records_info[$r]['usage'] . " segments, ";
                $breakdown_costs .= "$" . number_format($records_info[$r]['usage'] * $segments[$records_info[$r]['category']], 2, '.', ','); 
                $total_costs += number_format($records_info[$r]['usage'] * $segments[$records_info[$r]['category']], 2, '.', ','); 
                $breakdown_costs .= "<br>\n"; 
            }
        }
    }
    $rtn .= "<br>\nPrevious Months Data Use: " . $total_messages.' messages, ' . $total_segments.' segments, Total Cost: <strong>$' . $total_costs . "</strong><br>\n";
    $rtn .= $breakdown_costs;

    $rtn .= "<br>\n<br>\n";

    } // end OrgID   

} // end foreach Account

$rtn .= "--------------------------------<br>\n";

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

$subject = "Twilo Use " . date("m/d/Y");

$message = "Here is your Twilio use report.<br>\nData is as of: " . date("m/d/Y") . "<br><br>\n\n";

$message .= $rtn;

// Set who the message is to be sent to
$PHPMailerObj->addAddress("dedgecomb@irecruit-software.com", "David Edgecomb");
//$PHPMailerObj->addBCC("lschneider@cmshris.com", "Lisa Schneider");

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';

// Send email
$PHPMailerObj->send();

?>
