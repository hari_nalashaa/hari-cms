#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

//Read all zip folder files
$dir        =   IRECRUIT_DIR . "zips/";
$zips_files =   scandir($dir);

$i=0;
//Get and delete all the files are older than 2 days
foreach($zips_files as $file_name) {
    if($file_name != "." && $file_name != ".." && $file_name != ".gitignore") {
        $mtime          =   date("Y-m-d H:i:s", filemtime($dir . $file_name));
        $cur_date_time  =   date("Y-m-d H:i:s");

        //Get days difference between two dates
        $date_info      =   G::Obj('DateHelper')->getDaysDifference($mtime, $cur_date_time);

        if($date_info['days'] > 2) {
	    $i++;
            @unlink($dir . $file_name);
        }
    }
}

$application = "deleteOldTempFilesZipFolder.php";
$status = "Zip directory updated. " . $i . " files deleted.";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
