#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

G::Obj('GenericQueries')->conn_string =   "IRECRUIT";

$ARCHIVEACTIVE="yes"; // yes

//Get Twilio Accounts
$twilio_accounts_info   =   G::Obj('TwilioAccounts')->getTwilioAccountsList("OrgID ASC");
foreach ($twilio_accounts_info['results'] AS $TA) {

  if ($TA['OrgID'] == 'B12345467') { /* Clean up Demo Account sooner */
    $DAYSTOARCHIVE=4;
    $RESERVEDPHONENOS=2;
  } else {
    $DAYSTOARCHIVE=30;
    $RESERVEDPHONENOS=8;
  }

  //if ($TA['OrgID'] == 'B12345467') { // For testing or limiting one organization
  if (1) {

  $query = "SELECT OrganizationName FROM OrgData WHERE OrgID = :OrgID";
  $params     =   array(':OrgID'    =>  $TA['OrgID']);
  $ORG =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));


  echo "\n";
  echo "OrgID: " . $TA['OrgID'] . " - " . $ORG[0]['OrganizationName'] . "\n";
  echo "SID: " . $TA['AccountSid'] . "\n";
  echo "Token: " . $TA['AuthToken'] . "\n";

  $conversations	=       G::Obj('TwilioConversationApi')->fetchAllConversationResources($TA['OrgID']);
  $MESSAGES = array();

  if (count($conversations['Response']) > 0) {

    $query = "SELECT UserID, count(*) AS cnt FROM TwilioConversations WHERE OrgID = :OrgID GROUP BY UserID";
    $params     =   array(':OrgID'    =>  $TA['OrgID']);
    $RESULTS  =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
    foreach ($RESULTS AS $U) {
	    echo "User: " . $U['UserID'] . ", Conversations: " . $U['cnt'] . "\n";
    } // end foreach

    echo "Total Conversations: " . count($conversations['Response']) . "\n";

    $i=0;
    foreach ($conversations['Response'] AS $C) {
	    $i++;

	    $ResourceID	=   $C->sid;

	    echo "Conversation-" . $i . ": " . $ResourceID;

            $messages =   G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($TA['OrgID'], $ResourceID);

	    $MESSAGES = array();
	    $conversation_messages = array();
	    $conversation_msg_skip_fields = array();

	    $ii=0;
	    foreach ($messages['Response'] AS $M) {

                if(is_object($M)) {
                    $reflection     =   new ReflectionClass($M);
                    $property       =   $reflection->getProperty("properties");
                    $property->setAccessible(true);
                    $MESSAGES[]     =   $msg_info  = $property->getValue($M);

                    $message_info   =   array();

		    $date_created_info = (array)$MESSAGES[$ii]['dateCreated'];
		    $date_updated_info = (array)$MESSAGES[$ii]['dateUpdated'];

                    $message_info['ConversationResourceID']     =   $msg_info['conversationSid'];
                    $message_info['ConversationMessageID']      =   $msg_info['sid'];
                    $message_info['Author']                     =   $msg_info['author'];
                    $message_info['Body']                       =   utf8_decode($msg_info['body']);
                    $message_info['DateCreatedDate']            =   $date_created_info['date'];
                    $message_info['DateCreatedTimeZone_Type']   =   $date_created_info['timezone_type'];
                    $message_info['DateCreatedTimeZone']        =   $date_created_info['timezone'];
                    $message_info['DateUpdatedDate']            =   $date_updated_info['date'];
                    $message_info['DateUpdatedTimeZone_Type']   =   $date_updated_info['timezone_type'];
                    $message_info['DateUpdatedTimeZone']        =   $date_updated_info['timezone'];
                    $message_info['CreatedDateTime']            =   "NOW()";

                    //Assign Data To Arrays
                    $conversation_messages[$msg_info['sid']]           =   $message_info;

		    //Skip fields
                    $skip_fields    =   array_keys($message_info);
                    array_pop($skip_fields);

		    $conversation_msg_skip_fields[$msg_info['sid']]    =   $skip_fields;

		} // end is object

	    $ii++;
    	    } // end foreach Message 

	    echo ", Messages: " . count($MESSAGES);

	    $sort_date = array_column($MESSAGES, 'dateCreated');
	    array_multisort($sort_date, SORT_DESC, $MESSAGES);

		$date_info = (array)$MESSAGES[0]['dateCreated'];

		$earlier = new DateTime(date("Y-m-d", strtotime($date_info['date'])));
		$later = new DateTime(date("Y-m-d"));
		$days           =   $later->diff($earlier)->format("%a");

	        echo ", Last Conversation: " . $days . " days";

		if ($days > $DAYSTOARCHIVE) { 

		    echo " - Archive this Conversation"; 


		    // get Conversation Data
		    // write Conversation Data to Archive
		    if ($ARCHIVEACTIVE == "yes") {
		      $conversation_res_info  =   G::Obj('TwilioConversationInfo')->getConversationInfoByResourceID($ResourceID);
		      G::Obj('TwilioConversationInfo')->insTwilioConversationsArchive($conversation_res_info);
		    } // end ARCHIVEACTIVE

		    // write Twilio Data to Messages
		    foreach ($conversation_messages AS $message_sid=>$messages_info) {
		        if ($ARCHIVEACTIVE == "yes") {
			  echo "\nMessage SID: " . $message_sid . "\n";
			  echo "Info: " . print_r($messages_info,true) . "\n";
                          $ins_res    =   G::Obj('TwilioConversationMessagesApi')->insertConversationMessages($messages_info, $conversation_msg_skip_fields[$message_sid]);
		          echo '  Affected Rows: ' . $ins_res['affected_rows'] . "\n";
		        } // end ARCHIVEACTIVE

		    } // end foreach 

		    // Verifies copy of data then deletes Conversation from Twilio and the Conversations table in iRecruit
		    if ($ARCHIVEACTIVE == "yes") {
			if ($ins_res['affected_rows'] > 0) {
		          /* This is a final delete */
		          $del_conversation_res   =   G::Obj('TwilioConversationApi')->deleteConversationResource($TA['OrgID'], $ResourceID, "CRON");
		          /* This is a final delete */
			}
		    } // end ARCHIVEACTIVE

	  	       if ($del_conversation_res['Code'] == 'Success') {
		         echo " - Conversation Archived"; 
		       }

		} // end $DAYSTOARCHIVE 
		echo "\n";

    } // end foreach Conversation

  } // end if conversations > 0

  $account_phone_numbers  =     G::Obj('TwilioPhoneNumbersApi')->getAccountIncomingNumbers($TA['OrgID']);
  $phone_numbers_info     =   	$account_phone_numbers['PhoneNumbersInfo'];
  $phone_numbers_cnt	  =	count($phone_numbers_info);

  echo "\nPhone Numbers: " . $phone_numbers_cnt . "\n";

  $ii=0;
  $x=0;
  foreach ($phone_numbers_info as $PN) {
  $ii++;
    $query = "SELECT * FROM TwilioConversations WHERE OrgID = :OrgID and MobileNumbers like :MN";
    $params     =   array(':OrgID'    =>  $TA['OrgID'], ':MN' => '%' . $PN['phoneNumber'] . '%');
    $PHCK =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
    $in_use_cnt = count($PHCK);
    echo $ii . " - " . $PN['phoneNumber'] . " - ";
    if ($in_use_cnt > 0) { 
	echo "In Use: " . $in_use_cnt; 
    } else {
	$x++;
	if (($x <= $RESERVEDPHONENOS) && ($ii <= ($TA['Credits'] + 2))) {
	  echo "Reserved";
	} else {

	  echo " - Remove this Phone Number - " . $PN['sid'];

	  if ($ARCHIVEACTIVE == "yes") {
	    $del_res =  G::Obj('TwilioPhoneNumbersApi')->deletePhoneNumber($TA['OrgID'], $PN['sid']);
	  } // end ARCHIVEACTIVE

	  if ($del_res['Code'] == 'Success') {
	     echo " -- Removed";
	  }

 	}
    }
    echo "\n";

    } // end OrgID   

  } // end foreach

} // end foreach Account

echo "\n\n";

?>
