#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$application = "wotcFTP.php";

ini_set('display_errors', 0);

define ('DIR',ROOT.'wotc_files/');
define ('ftp_server', 'ftp.cmswotc.com');
define ('ftp_user_name', 'wotc_admin');
define ('ftp_user_password', '7j2dKWhb');

 processFiles("Dycom");

function processFiles($procdir) {

mkdir(DIR . $procdir);
$status = "";

// set up basic ssl connection
$conn_id = ftp_ssl_connect(ftp_server);

// login with username and password
$login_result = ftp_login($conn_id, ftp_user_name, ftp_user_password);

if (!$login_result) {
    // PHP will already have raised an E_WARNING level message in this case
    $status .= "Cant't login\n\n";

    // Insert Cron Status Logs
    $CronStatusLogsObj->insUpdCronStatusLog($application, $status);

    die("can't login");
}

// change the current directory to php
ftp_chdir($conn_id, $procdir);

$FILES = ftp_nlist($conn_id,".");

foreach ($FILES as $file) {

$local_file = DIR . $procdir . '/' . $file;

  $status .= "\n";

  // download server file
  if (ftp_get($conn_id, $local_file, $file, FTP_ASCII))
  {
    $status .= "Successfully written to $local_file.\n";
  } else {
    $status .= "Error downloading $file.\n";
  }

  // try to delete file
  if (ftp_delete($conn_id, $file))
  {
    $status .= "$file deleted.\n";
  } else {
    $status .= "Could not delete $file.\n";
  }

  $status .= "\n";

} // end foreach

// close the ssl connection
ftp_close($conn_id);

} // end function

$status .= "\nDone\n\n";

// Insert Cron Status Logs
$CronStatusLogsObj->insUpdCronStatusLog($application, $status);
?>
