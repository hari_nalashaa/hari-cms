#!/usr/local/bin/php -q
<?php 
require_once realpath(__DIR__) . '/Configuration.inc';

$intuit_acc_info    =   $IntuitObj->getIntuitAccessInformation("DATEDIFF(DATE(NOW()), DATE(CreatedDateTime)) AS DaysDiff");
$days_diff          =   $intuit_acc_info['DaysDiff'];
$notification_days  =   100 - $days_diff;

if($days_diff >= 80) {
    // Subject
    $subject = 'Token is going to expire in '. $notification_days . ' days';
    
    // Message
    $message  = 'Hi Brian,<br><br>';
    $message .= 'Please update the Intuit credit card token,';
    $message .= ' otherwise it will expire in ' . $notification_days . ' days.';
    $message .= '<br><br>';
    $message .= '<a href="'.ADMIN_HOME.'intuit.php">Click here to update.</a>';
    $message .= '<br><br>';
    $message .= 'Account Hint: Cost Management Service(s), with a "s" and a comma and ends with 2178';
    $message .= '<br><br>';
    $message .= 'Thanks,<br>';
    $message .= 'David';

    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
    
    // Set who the message is to be sent to
    $PHPMailerObj->addAddress ( "bkelly@cmshris.com" );
    // Set who the message is to be sent to
    // $PHPMailerObj->addAddress ( "lstrong@cmshris.com" );
    // Set who the message is to be sent to
    $PHPMailerObj->addAddress ( "dedgecomb@irecruit-software.com" );
    
    // Set the subject line
    $PHPMailerObj->Subject = $subject;
    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );
    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/html';
    //Send email
    $PHPMailerObj->send ();
}
