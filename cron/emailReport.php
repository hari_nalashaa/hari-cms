#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__) . '/Configuration.inc';

$where_info =   array("Report = 'Y'");
$RES        =   $CronStatusLogsObj->getCronStatusLogsInfo("*", $where_info, 'LastRun');
$RESULTS    =   $RES['results'];

$message    =   "";

foreach ($RESULTS as $C) {
    $message .= $C['LastRun'] . ' - ' . $C['Application'] . "<br>\n" . $C['Status'] . "<br><br>\n\n";
}

  if ($message != "") {

    $subject = SERVER . " Cron Report";
    $to = "dedgecomb@irecruit-software.com";

    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();

    // Set who the message is to be sent to
    $PHPMailerObj->addAddress($to);
    // Set the subject line
    $PHPMailerObj->Subject = $subject;
    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML($message);
    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/plain';
    // Send email
    $PHPMailerObj->send();

  } // end message

?>
