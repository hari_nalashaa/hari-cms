<?php
header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

ini_set ('display_errors', 0);
require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';
require_once VENDOR_DIR . 'autoload.php';

//Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';

if ($_SERVER['SERVER_NAME'] == "wotcadmin.irecruit-us.com") {
$redirect = 'https://admin.cmswotc.com' . $_SERVER['REQUEST_URI'];
header('Location: ' . $redirect);
}
?>
