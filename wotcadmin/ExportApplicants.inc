<?php
$METHODS    =   G::Obj('WOTCAdmin')->getEntryMethods();

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("David Edgecomb")
       ->setLastModifiedBy("David")
       ->setTitle("WOTC Export")
       ->setSubject("Excel")
       ->setDescription("Results from query")
       ->setKeywords("phpExcel")
       ->setCategory("Output");


// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue('Company');
$objPHPExcel->getActiveSheet()->getCell('B1')->setValue('Entry Date');
$objPHPExcel->getActiveSheet()->getCell('C1')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('D1')->setValue('Name');
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue('SSN');
$objPHPExcel->getActiveSheet()->getCell('F1')->setValue('DOB');
$objPHPExcel->getActiveSheet()->getCell('G1')->setValue('Qualifies');
$objPHPExcel->getActiveSheet()->getCell('H1')->setValue('Start Date');
$objPHPExcel->getActiveSheet()->getCell('I1')->setValue('Received at CMS');
$objPHPExcel->getActiveSheet()->getCell('J1')->setValue('Filed to State');
$objPHPExcel->getActiveSheet()->getCell('K1')->setValue('Confirmed from State');
$objPHPExcel->getActiveSheet()->getCell('L1')->setValue('Approved by State');
$objPHPExcel->getActiveSheet()->getCell('M1')->setValue('Category');
$objPHPExcel->getActiveSheet()->getCell('N1')->setValue('Comments');
$objPHPExcel->getActiveSheet()->getCell('O1')->setValue('Method');
$objPHPExcel->getActiveSheet()->getCell('P1')->setValue('Estimated Value');
$objPHPExcel->getActiveSheet()->getCell('Q1')->setValue('Active Status');

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


$n  =   2;
foreach ($APPLICANTDATA as $AD) {

    $columns =  "IF(StartDate = '0000-00-00','',date_format(StartDate,'%m/%d/%Y')) StartDate,";
    $columns .= " IF(OfferDate = '0000-00-00','',date_format(OfferDate,'%m/%d/%Y')) OfferDate,";
    $columns .= " IF(HiredDate = '0000-00-00','',date_format(HiredDate,'%m/%d/%Y')) HiredDate,";
    $columns .= " IF(Received = '0000-00-00','',date_format(Received,'%m/%d/%Y')) Received,";
    $columns .= " IF(Filed = '0000-00-00','',date_format(Filed,'%m/%d/%Y')) Filed,";
    $columns .= " IF(Confirmed= '0000-00-00','',date_format(Confirmed,'%m/%d/%Y')) Confirmed,";
    $columns .= " StartingWage, Position, Qualified, Category, Comments";
    $PROC       =   G::Obj('WOTCProcessing')->getProcessingInfo($columns, $AD['wotcID'], $AD['ApplicationID']);

    $objPHPExcel->getActiveSheet()->getStyle('F'.$n.':V'.$n)->getAlignment()->setHorizontal('center');

    $AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($AD['wotcID']);
    $ADDAKA =$AKADBA['State'] . ' - (' . $AKADBA['EIN1'] . '-' . $AKADBA['EIN2'] . ') ' . $AKADBA['AKADBA'] . ', ' . $AKADBA['Address'] . ', ' . $AKADBA['City'] . ' ' . $AKADBA['State'] . ' ' . $AKADBA['ZipCode'];

    $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($ADDAKA);
    $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($AD['EntryDate']);
    $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($AD['ApplicationID']);
    
    $NAME   =   $AD['FirstName'];
    
    if ($AD['MiddleName']) $NAME .= ' ' . $AD['MiddleName'];

    $NAME   .=  ' ' . $AD['LastName'];
    
    $objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($NAME);

    if ($AUTH['UserID'] != 'dycomadmin') {
        $AD['DOB_Date'] =   "MM/DD/YYYY";
        $AD['Social1']  =   "XXX";
        $AD['Social2']  =   "XX";
        $AD['Social3']  =   "XXXX";
    }

    $objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($AD['Social1'] . '-' . $AD['Social2'] . '-' . $AD['Social3']);
    $objPHPExcel->getActiveSheet()->getCell('F'.$n)->setValue($AD['DOB_Date']);
    $objPHPExcel->getActiveSheet()->getCell('G'.$n)->setValue($AD['Qualifies']);
    $objPHPExcel->getActiveSheet()->getCell('H'.$n)->setValue($AD['StartDate']);
    $objPHPExcel->getActiveSheet()->getCell('I'.$n)->setValue($AD['Received']);
    $objPHPExcel->getActiveSheet()->getCell('J'.$n)->setValue($AD['Filed']);
    $objPHPExcel->getActiveSheet()->getCell('K'.$n)->setValue($AD['Confirmed']);
    $objPHPExcel->getActiveSheet()->getCell('L'.$n)->setValue($AD['Qualified']);
    $objPHPExcel->getActiveSheet()->getCell('M'.$n)->setValue($AD['Category']);
    $objPHPExcel->getActiveSheet()->getCell('N'.$n)->setValue($AD['Comments']);
    $objPHPExcel->getActiveSheet()->getCell('O'.$n)->setValue($AD['Initiated']);
    
    $total      =   0;
    $bill_info  =   G::Obj('WOTCBilling')->getMaxYearAndTaxCredit($AD['wotcID'], $AD['ApplicationID']);
    $year       =   $bill_info['MaxYear'];
    $total      =   $bill_info['TaxCredit'];
    $year++;
    
    $TaxCredit  =   0;
    list($TaxCredit, $active, $comment)   =   G::Obj('WOTCCalculator')->calculate_wotcCredit($AD['wotcID'], $AD['ApplicationID'], $year);

    $TaxCredit  +=  $total;

    $objPHPExcel->getActiveSheet()->getCell('P'.$n)->setValue($TaxCredit);
    $objPHPExcel->getActiveSheet()->getCell('Q'.$n)->setValue($active);


    $n++;
} // end foreach

// Rename sheet
$title  =   "Export " . date("Y-m-d",strtotime($_POST['startdate'])) . " - " . date("Y-m-d",strtotime($_POST['enddate']));
$objPHPExcel->getActiveSheet()->setTitle($title);

// Redirect output to a client's web browser (Excel5)


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="wotcExport.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
