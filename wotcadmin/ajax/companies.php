<?php
include '../Configuration.inc';

G::Obj('GenericQueries')->conn_string =   "WOTC";
$AUTH               =   G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

$ACCESS             =   "'" . join('\',\'', $AUTH['ACCESS']) . "'";

$query          =   "SELECT OrgData.wotcID, OrgData.EIN1, OrgData.EIN2, CRMFEIN.AKADBA AS CompanyName,";
$query         .=   " concat(OrgData.Address, ', ', OrgData.City,' ', OrgData.ZipCode) AS LocationAddress,";
$query         .=   " CRM.Logo,";
$query         .=   " OrgData.State AS State";
$query         .=   " FROM CRM";
$query         .=   " JOIN OrgData ON OrgData.CompanyID = CRM.CompanyID";
$query         .=   " LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = CRM.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
$query         .=   " WHERE OrgData.wotcID IN ($ACCESS)";
if ($AUTH['UserID'] != "2jsupply") {
$query 	       .=   " AND OrgData.Active = 'Y'";
}
$query         .=   " ORDER BY CRMFEIN.State, CRMFEIN.AKADBA";
$RESTRICTEDLIST =   G::Obj('GenericQueries')->getInfoByQuery($query);
$cnt                =   sizeof($RESTRICTEDLIST);

echo "Company: <select style=\"width:420px;\" name=\"wotcID\" onChange=\"loadwotcCompanies(this.value);\">\n";

echo "<option value=\"X\"";
if ($_REQUEST['wotcID'] == "X") {
    echo " selected";
}
$type = "None";
if ($cnt == 0) {
    $type = "No Company Associated With This User";
}
echo ">" . $type . "</option>\n";

if ($cnt > 1) {
    echo "<option value=\"ALL\"";
    if ($_REQUEST['wotcID'] == "ALL") {
        echo " selected";
    }
    echo ">ALL</option>\n";
} // end

$i = 0;
foreach ($RESTRICTEDLIST as $ROD) {
    $i ++;
    $cmpny = "";
    if ($i == 1) {
        $wotcID = $ROD['wotcID'];
    }
    echo '<option value="' . $ROD['wotcID'] . '"';
    if ($ROD['wotcID'] == $_REQUEST['wotcID']) {
        echo ' selected';
    }
    
    $cmpny .= $ROD['State'] . ' - (' . $ROD['EIN1'] . '-' . $ROD['EIN2'] . ') ';
    if (substr($ROD['wotcID'], 0, 8) != "prestige") {
        $cmpny .= $ROD['CompanyName'] . ', ';
	$cmpny .= $ROD['LocationAddress'];
    }
    echo '>';
    echo $cmpny;
    echo "</option>\n";
} // end foreach

echo "</select>\n";
?>
