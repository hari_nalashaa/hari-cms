<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation"
	style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="javascript:void(0);">
            WOTC ADMIN
		</a>
	</div>
				<?php 
				$USER   =   G::Obj('WOTCUsers')->getUserInfoByUserID("*", $AUTH['UserID']);
				?>

	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle fa-user-icon">
				
			<?php 
			echo ($USER['UserID'] != "") ? $USER['UserID'] : 'Admin Access'; 
			?>
			&nbsp;<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">

<?php if ($USER['UserID'] != "") { ?>
                <li>
                	<a href="<?php echo WOTCADMIN_HOME?>index.php?pg=accountinfo">
                    	<i class="fa fa-user" style="float:left;margin-top:5px;margin-right:2px"></i>
                    	<div style="width: 240px;float:left">Account Info</div>
                    	<i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>
                	</a>
                </li>
<?php } ?>
                <li>
                    <a href="<?php echo WOTCADMIN_HOME?>logout.php?OrgID=<?php echo $OrgID;?>&MultiOrgID=<?php echo $MultiOrgID?>">
                        <i class="fa fa-sign-out" style="float:left;margin-top:5px;margin-right:2px"></i>
                        <div style="width: 240px;float:left">Logout</div>
                        <i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>
                    </a>
                </li>
			</ul> <!-- /.dropdown-user --></li>
	</ul>

	<!-- /.navbar-header -->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<?php
				global $AUTH;
				
				$navsection =   array(
                				    "dashboard"         =>  "DASHBOARD",
                				    "lookup"            =>  "LOOK UP APPLICANTS",
                				    "orginfo"           =>  "ORGANIZATION INFORMATION",
                                    "paperprocessforms" =>  "Paper Processing Forms",
                				    "callcenterinfo"    =>  "CALL CENTER INFO",
                				    "wotcnews"          =>  "WOTC NEWS",
                                    "contactsupport"    =>  "CONTACT SUPPORT"
                				);
				
				if ($AUTH['Dashboard'] == "N") {
				    unset($navsection['dashboard']);
				}

				foreach ($navsection as $section => $displaytitle) {
				    $rtn .= '<li>';
				    if($section == 'lead') {
				        $rtn .= "<a href=\" http://www.cmswotc.com/lead-registration/";
				        $rtn .= "\" target=\"_blank\">";
				    } else if($section == 'wotcnews') {
				        $rtn .= "<a href=\"http://www.cmswotc.com/category/news/";
				        $rtn .= "\" target=\"_blank\">";
				    } 
				    else {
				    	$rtn .= "<a href=\"" . WOTCADMIN_HOME . "index.php?pg=" . $section;
				    	
				    	if ($wotcID) {
				    	    $rtn .= "&wotcID=" . $wotcID;
				    	}

				        $rtn .= "\">";
				    }				    
				    
				    $rtn .= "&nbsp;".$displaytitle;
				    $rtn .= '<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
				    $rtn .= "</a>";
				    $rtn .= '</li>';
				} // end foreach
				
				echo $rtn;
				?>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>
