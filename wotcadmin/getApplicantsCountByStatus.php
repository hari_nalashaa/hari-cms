<?php
include 'Configuration.inc';

$AUTH       =   G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

$wotcID     =   ($_REQUEST['wotcID'] == "All") ? "" : $_REQUEST['wotcID'];
$StartDate  =   G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['StartDate'], "/", "-");
$EndDate    =   G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['EndDate'], "/", "-");

$apps_status_count_info =   G::Obj('WOTCApplicantData')->getApplicantsCountByStatus($wotcID, $StartDate, $EndDate);

echo json_encode($apps_status_count_info);