<?php
include 'Configuration.inc';

echo G::Obj('WOTC')->wotcadminHeader(htmlspecialchars($_GET['wotcID']));
echo "<div style=\"font-family:Arial;margin:20px 0 0 100px;\">\n";

$FORM = "N";

if ($_POST['pass']) {
    
    $sessionid = $_POST['sessionid'];
    $ERROR = "";
    
    if (! preg_match("/^.*(?=.{8,}).*$/", $_POST['pass'])) {
        $ERROR .= "Passwords need to be at least 8 characters." . "<br>\n";
    }
    if (! preg_match("/^.*(?=.*[A-Z]).*$/", $_POST['pass'])) {
        $ERROR .= "Passwords must contain one upper case letter." . "<br>\n";
    }
    if (! preg_match("/^.*(?=.*\d).*$/", $_POST['pass'])) {
        $ERROR .= "Passwords must contain one digit." . "<br>\n";
    }
    
    if ($ERROR) {
        
        echo '<p style="color:red;">' . $ERROR . '</p>';
        $FORM = "Y";
    } else {
        
        //Get User Information By SessionID
        $AUTH       =   G::Obj('WOTCUsers')->getUserInfoBySessionID("*", $_POST['sessionid']);
        
        //Update Password
        $set_info   =   array("Verification = :pass", "SessionID = ''");
        $where      =   array("SessionID = :sessionid");
        $params     =   array(
                            ':pass'         =>  password_hash($_POST['pass'], PASSWORD_DEFAULT),
                            ':sessionid'    =>  $_POST['sessionid'],
                        );
        G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array($params));
        
        
        $to = $AUTH['Email'];
        
        $subject = "Account Change";
        
        $message = "\nYour password for the CMS Poral has changed.\n\n";
        $message .= "If you requested this change please ignore this email.\n";
        
        echo 'Your password has been updated<br>';
        echo 'Please login in <a href="' . WOTCADMIN_HOME . 'login.php">here</a>.';
        
        // Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
        
        // Set who the message is to be sent to
        $PHPMailerObj->addAddress($to);
        // Set the subject line
        $PHPMailerObj->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        $PHPMailerObj->msgHTML($message);
        // Content Type Is HTML
        $PHPMailerObj->ContentType = 'text/html';
        // Send email
        $PHPMailerObj->send();
    }
} else 
    if ($_GET['s']) {
        
        $FORM = "Y";
    }

if ($FORM == "Y") {

    //Get User Information By SessionID
    $AUTH   =   G::Obj('WOTCUsers')->getUserInfoBySessionID("*", $_GET['s']);
    
    if ($AUTH['UserID'] != "") {
        
        echo '<form method="POST">' . "\n";
        echo 'Please enter your new password: ' . "\n";
        echo '<input type="password" name="pass" value="" size="30" maxlength="45">' . "\n";
        echo '<input type="hidden" name="sessionid" value="' . htmlspecialchars($AUTH['SessionID']) . '">' . "\n";
        echo '<input type="submit" value="Change Password">' . "\n";
        echo '</form>' . "\n";
    } else {
        
        echo 'This link has expired. Please start the reset password process and respond to the email link.';
    }
}
echo "</div>\n";

echo "</div>\n";
echo "</body>\n";
echo "</html>\n";
?>