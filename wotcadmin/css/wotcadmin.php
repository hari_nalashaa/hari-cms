<?php 
header("Content-type: text/css"); 
?>
/* Generic Selectors */
ol {
   margin: 0;
   padding: 0px 0px 0px 20px; 
}

ol li {
   list-style-type: none;
}
ol li b {
   color: #555555;
}

a:link {
   /* color: #000000; */
   text-decoration: none;
}
 
a:visited {
   /* color: #000000; */
   text-decoration: none;
}
 
a:hover {
   /* color: #000000; */
   text-decoration: underline;
   font-weight: bold;
   font-size:110%;
}

a:active {
   /* color: #000000; */
   font-weight: bold;
}

 
/************************* ID's *************************/

#topDoc {
   height:90px;
   padding: 10px 0px 0px 25px; 
   background-color: #FFFFFF;
   width:800px;
}

#navigation {
   clear:both;
   float:left;
   margin: 10px 0 0 30px;
   width: 140px;
   line-height:225%;
}

#centerDoc {
   float:left;
   min-height:400px;
   padding: 10px; 
   background-color: #ffffff;
}

#bottomDoc {
   clear: both;
   font-size: 8pt;
   padding: 5px 0 5px 0; 
   font-size: 11px;
   background-color: #ffffff;
   text-align:center;
   width:800px;
}

.openDocuments {
        position: fixed; 
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0,0,0,0.2);
        z-index: 9999;
        opacity:0;
        -webkit-transition: opacity 400ms ease-in;
        -moz-transition: opacity 400ms ease-in;
        transition: opacity 400ms ease-in;
        pointer-events: none;
}

.openDocuments:target {
        opacity:1;
        pointer-events: auto;
}

.openDocuments > div {
        width: 650px;
        position: relative;
        margin: 5% auto;
        padding: 15px 20px 13px 20px;
        border-radius: 10px;
        background: #ffffff;
        -moz-box-shadow: 1px 1px 3px #000;
        -webkit-box-shadow: 1px 1px 3px #000;
        box-shadow: 1px 1px 3px #000;
}

.closeDocuments {
        background: #eeeeee;
        color: #ffffff;
        line-height: 25px;
        position: absolute;
        right: -12px;
        text-align: center;
        top: -10px;
        width: 24px;
        text-decoration: none;
        font-weight: bold;
        -webkit-border-radius: 12px;
        -moz-border-radius: 12px;
        border-radius: 12px;
        -moz-box-shadow: 1px 1px 3px #000;
        -webkit-box-shadow: 1px 1px 3px #000;
        box-shadow: 1px 1px 3px #000;
}

.closeDocuments:hover {
        background: #555555;
        color: red;
}