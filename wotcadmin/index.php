<?php
include 'Configuration.inc';

$AUTH           =   G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

if ($_POST['export'] == "Export to Excel") {

    $APPLICANTDATA = G::Obj('WOTCAdmin')->queryApplicants();
    include WOTCADMIN_DIR . 'ExportApplicants.inc';
}

$page_titles    =   array(
    				    "dashboard"         =>  "DASHBOARD",
    				    "lookup"            =>  "LOOK UP APPLICANTS",
    				    "paperprocessforms" =>  "Paper Processing Forms",
                        "accountinfo"       =>  "ACCOUNT INFORMATION",
	                    "orginfo"           =>  "ORGANIZATION INFORMATION",
    				    "callcenterinfo"    =>  "CALL CENTER INFO",
    				    "wotcnews"          =>  "WOTC NEWS",
                        "contactsupport"    =>  "CONTACT SUPPORT"
    				);

if (! $_REQUEST['pg'] && $_REQUEST['pg'] == "") {
    $_REQUEST['pg'] = "lookup";
    if ($AUTH['Dashboard'] == "Y") {
        $_REQUEST['pg'] = "dashboard";
    }
}

require_once 'Header.inc';
require_once 'Navigation.inc';
require_once 'PageWrapperStart.inc';

echo '<div class="row">'; // Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';

echo $page_titles[$_REQUEST['pg']];  //Page Title

echo '</h3>';
echo '</div>';
echo '</div>'; // Row End

echo '<div class="page-inner">';

if ($_REQUEST['pg'] == "lookup") {
    include 'pages/LookUp.inc';
}
else if ($_REQUEST['pg'] == "orginfo") {
    include 'pages/OrgInfo.inc';
}
else if ($_REQUEST['pg'] == "accountinfo") {
    include 'pages/AccountInfo.inc';
}
else if ($_REQUEST['pg'] == "paperprocessforms") {
    include 'pages/PaperProcessingForms.inc';
}
else if ($_REQUEST['pg'] == "dashboard") {
    include 'pages/Dashboard.inc';
}
else if ($_REQUEST['pg'] == "terms") {
    include 'pages/terms.inc';
}
else if ($_REQUEST['pg'] == "policy") {
    include 'pages/policy.inc';
}
else if ($_REQUEST['pg'] == "callcenterinfo") {
    include 'pages/CallCenterInfo.inc';
}
else if ($_REQUEST['pg'] == "contactsupport") {
    include 'pages/ContactSupport.inc';
}


echo '</div>'; // Page Wrapper End

require_once 'PageWrapperEnd.inc';

require_once 'Footer.inc';
?>
