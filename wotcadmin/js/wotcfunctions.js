function loadwotcCompanies(wotcid) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari

		xmlhttp = new XMLHttpRequest();

	} else {// code for IE6, IE5

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

	}

	xmlhttp.onreadystatechange = function() {

		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

			document.getElementById("Company").innerHTML = xmlhttp.responseText;

		}

	}

	var c = new Date().getTime();

	xmlhttp.open("POST", "ajax/companies.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("wotcID=" + wotcid + "&d=" + c);

} // end function

function loadProcessingResults(wotcID, ApplicationID) {

	var xmlhttp;
	var fillwindow = "processingresults" + wotcID + ApplicationID;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById(fillwindow).innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "ajax/cmsprocessingresults.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("wotcID=" + wotcID + "&ApplicationID=" + ApplicationID + "&d=" + d);

} // end function

function displayVault(wotcID, ApplicationID) {

	var xmlhttp;
	var fillwindow = "Vault" + wotcID + ApplicationID;
	var newfile = "newfile" + wotcID + ApplicationID;
	var vaultform = "#VaultForm" + wotcID + ApplicationID;
	// var vform = $(vaultform).serialize();

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById(fillwindow).innerHTML = xmlhttp.responseText;
		}
	}

	var formData = new FormData();
	formData.append('wotcID', wotcID);
	formData.append('ApplicationID', ApplicationID);
	// formData.append('formdata',vform);
	var fileInput = document.getElementById(newfile);
	if (fileInput != null) {
		formData.append('newfile', fileInput.files[0]);
	}

	xmlhttp.open("POST", "ajax/vault.php", true);
	xmlhttp.send(formData);

} // end function

function printElement(wotcID, ApplicationID) {

	var fillwindow = "Vault" + wotcID + ApplicationID;

	var txt = "<html><head><title></title></head><body>";
	txt += "\n<style>\nbody table{\nfont-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\nfont-size:10pt;\n}\n</style>\n";
	txt += document.getElementById(fillwindow).innerHTML;
	txt += "</bod></html>"

	var myWindow = window.open('', '', 'width=600,height=500')
	myWindow.document.write(txt)
	myWindow.print();

} // end function

function printElement(element, wotcID, ApplicationID) {

	var fillwindow = element + wotcID + ApplicationID;

	var txt = "<html><head><title></title></head><body>";
	txt += "\n<style>\nbody table{\nfont-family: \"Trebuchet MS\", Arial, Helvetica, sans-serif;\nfont-size:10pt;\n}\n</style>\n";
	txt += document.getElementById(fillwindow).innerHTML;
	txt += "</bod></html>"

	var myWindow = window.open('', '', 'width=600,height=500')
	myWindow.document.write(txt)
	myWindow.print();

} // end function

function validate_date(formdate, formmessage) {

	var err = 0;
	var errtxt = '';

	var validformat = /^\d{2}\/\d{2}\/\d{4}$/ // Basic check for format
												// validity

	if (formdate != "") {
		if (!validformat.test(formdate)) {

			errtxt += "Invalid '" + formmessage
					+ "' Format.\n\nEnter as mm/dd/yyyy\n";
			err++;

		} else {

			var monthfield = formdate.split("/")[0]
			var dayfield = formdate.split("/")[1]
			var yearfield = formdate.split("/")[2]
			var dayobj = new Date(yearfield, monthfield - 1, dayfield)

			if ((dayobj.getMonth() + 1 != monthfield)
					|| (dayobj.getDate() != dayfield)
					|| (dayobj.getFullYear() != yearfield)) {
				errtxt += "Invalid '"
						+ formmessage
						+ "' Day, Month, or Year range detected.\n\nEnter as mm/dd/yyyy\n\n";
				err++;
			}
		}
	}
	// general error
	if (err > 0) {
		alert(errtxt);
		return false;
	}
	return true;

}
