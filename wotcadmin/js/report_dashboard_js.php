<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/..') . "/Configuration.inc";

$AUTH = G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

G::Obj('GenericQueries')->conn_string   =   "WOTC";
$query  =   "SELECT DATE_FORMAT(MIN(EntryDate),'%m/%d/%Y') AS FromDate, DATE_FORMAT(MAX(EntryDate),'%m/%d/%Y') AS ToDate FROM ApplicantData";
$query  .=  " WHERE wotcID IN (SElECT wotcID FROM Access WHERE UserID = :UserID)";
$params =   array(':UserID'=>$AUTH['UserID']);
$RESULTSIN  =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
$DATES      =   $RESULTSIN[0];

$FromDate   =   $DATES['FromDate'];
$ToDate     =   $DATES['ToDate'];
?>
$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

myConfig = { auth: { token: encodeURIComponent("<?php echo WOTCReports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

visualize(function(v){

var reportDashboard = "";

DashboardReport('<?php echo $FromDate; ?>','<?php echo $ToDate; ?>');

function DashboardReport (FromDate,ToDate) {

        $('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);

	var fromStr = new Date(FromDate);
	FromDate = fromStr.getFullYear() + "-" + (fromStr.getMonth() + 1) + "-" + fromStr.getDate();

	var toStr = new Date(ToDate);
	ToDate = toStr.getFullYear() + "-" + (toStr.getMonth() + 1) + "-" + toStr.getDate();

    	reportDashboard = v.report({
	resource: "/reports/WOTC_Customer/Dashboard",
        container: "#container",
        params: { "UserID":['<?php echo $AUTH['UserID']; ?>'], "FromDate":[FromDate], "ToDate":[ToDate] },
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsDashboard = totalPages;
	        displayDashboard();
            }
        },
	success: function () {
	    displayDashboard();
        }
    });



} // end Dashboard Report


    function displayDashboard(){

	document.getElementById('ReportControlsDashboard').style.display = "block";
        document.getElementById('previousPageDashboard').style.display = "none";
        document.getElementById('nextPageDashboard').style.display = "none";
	document.getElementById('exportDashboard').style.display = "block";

        var currentPage = reportDashboard.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageDashboard').style.display = "block";
        }

        if (currentPage < tpgsDashboard) {
         document.getElementById('nextPageDashboard').style.display = "block";
        }

	reportDashboard.run();

    }

    $("#previousPageDashboard").click(function() {
        var currentPage = reportDashboard.pages() || 1;

        document.getElementById('nextPageDashboard').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageDashboard').style.display = "none";
        }
 
        reportDashboard
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });
 
    $("#nextPageDashboard").click(function() {
        var currentPage = reportDashboard.pages() || 1;

        document.getElementById('previousPageDashboard').style.display = "block";

        if(currentPage >= tpgsDashboard-1) {
         document.getElementById('nextPageDashboard').style.display = "none";
        }
 
        reportDashboard
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportDashboard").click(function () {

        reportDashboard.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formDashboard']").submit(function () {

	var formData = {};
        params = $('#formDashboard').serializeArray();

	$.each(params, function(i, val) {
	    formData[val.name]=val.value;
	});

        var fromStr = new Date(formData['FromDate']);
        FromDate = fromStr.getFullYear() + "-" + (fromStr.getMonth() + 1) + "-" + fromStr.getDate();

        var toStr = new Date(formData['ToDate']);
        ToDate = toStr.getFullYear() + "-" + (toStr.getMonth() + 1) + "-" + toStr.getDate();


        reportDashboard
            .params({ "UserID":['<?php echo htmlspecialchars($AUTH['UserID']); ?>'], "FromDate":[FromDate], "ToDate":[ToDate] })
            .run()
                .fail(function(err) { alert(err); } );
	return false;
    });

}, function(err) {
  alert("error: " + err);
});
