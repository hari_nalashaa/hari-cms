var morris_chart_obj = Morris.Bar({
    element: 'morris-bar-chart',
    data: mdg_data,
    xkey: 'y',
    ykeys: ['a', 'p', 'c'],
    labels: ['WOTC Screens', 'Pending with the State', 'Certified from the State'],
    hideHover: 'auto',
    barColors:['#3399ff', '#ffcc00', '#9AC945'],
    barSizeRatio: 0.55,
    barGap: 3,
    barOpacity: 1.0,
    barRadius: [0, 0, 0],
    xLabelMargin: 12,
    resize: true
});
