function HideContent(d) {
	document.getElementById(d).style.display = "none";
}

function ShowContent(d) {
	document.getElementById(d).style.display = "block";
}

function ReverseDisplay(d) {
	if (document.getElementById(d).style.display == "none") {
		document.getElementById(d).style.display = "block";
	} else {
		document.getElementById(d).style.display = "none";
	}
}

function getOrgInfo(wotcID) {
	var request = $.ajax({
		async: true,   // this will solve the problem
		method: "POST",
  		url: "pages/getOrgInfo.php?wotcID="+wotcID,
		type: "POST",
		beforeSend: function() {
			$("#org_info").html('<br>Please wait.. <img src="'+wotcadmin_home+'images/loading-small.jpg"/><br><br>');
		},
		success: function(data) {
			$("#org_info").html(data);
    	}
	});
}

function getApplicantsCountyByStatusPerMonth() {
	var wotcID 	=	document.getElementById('wotcIDBarChart').value;
	var Year 	=	document.getElementById('Year').value;
	
	$.ajax({
		async: true,   // this will solve the problem
		method: "POST",
  		url: "getApplicantsCountByStatusPerMonth.php?wotcID="+wotcID+"&Year="+Year,
		type: "POST",
		dataType:"JSON",
		beforeSend: function() {

		},
		success: function(response_data) {
			var All = 0;
			var Certified = 0;
			var Pending = 0;
			
			for(var r = 0; r < response_data.length; r++) {
				All += parseInt(response_data[r].a);
			}
			
			for(var r = 0; r < response_data.length; r++) {
				Pending += parseInt(response_data[r].p);	
			}
			
			for(var r = 0; r < response_data.length; r++) {
				Certified += parseInt(response_data[r].c);
			}

			$("#all_apps_count").html(All);
			$("#certified_apps_count").html(Certified);
			$("#pending_apps_count").html(Pending);
			
		    morris_chart_obj.setData(response_data);
    	}
	});
}

