<?php
include 'Configuration.inc';

if ($_POST['submit'] == "Please Login") {
    
    // Get User Details
    $AUTH = G::Obj('WOTCUsers')->getUserInfoByUserID("UserID, Verification", $_POST['user']);
    
    if (password_verify($_POST['password'], $AUTH['Verification'])) {
        if (password_needs_rehash($_POST['password'], PASSWORD_DEFAULT)) {
            $set_info   =   array("Verification = :pass");
            $where      =   array("UserID = :userid");
            $params     =   array(
                                ':pass'     =>  password_hash($_POST['password'], PASSWORD_DEFAULT),
                                ':userid'   =>  $AUTH['UserID']
                            );
            G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array($params));
        }
    } else {
        $AUTH = array();
    }
    
    // if we have a good user and password match
    if ($AUTH['UserID']) {
        
        // set a new Session ID
        // randomize a session key
        $formatted_date = $MysqlHelperObj->getDatesList("DATE_FORMAT(NOW(),'%Y%m%d%H%m%s') AS FORMATTED_DATE");
        $SESS = $formatted_date['FORMATTED_DATE'];
        
        $SESSIONID = uniqid($SESS);
        if ($_COOKIE['WID']) {
            $SESSIONID = $_COOKIE['WID'];
        }
        
        // add to database
        $set_info   =   array(
                            "LastAccess =   NOW()",
                            "SessionID  =   :sessionid"
                        );
        $where      =   array(
                            "UserID     =   :userid"
                        );
        $params     =   array(
                            ':sessionid'    =>  $SESSIONID,
                            ':userid'       =>  $AUTH['UserID']
                        );
        
        if ($_POST['keep'] == "yes") {
            $set_info[] = "Persist = 1";
        }
        
        G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array(
            $params
        ));
        
        // set session key on users browser
        if ($_POST['keep'] == "yes") {
            setcookie("WID", $SESSIONID, time() + 2592000, "/");
        } else {
            setcookie("WID", $SESSIONID, time() + 7200, "/");
        }
        
        // go to application
        header('Location: ' . WOTCADMIN_HOME);
        exit();
    } // end AUTH
    
    $ERROR = 'Login Failed';
} // end submit

if ($_POST['submit'] == "Lookup") {
    
    if ($_POST['type'] == "username") {
        $AUTH = G::Obj('WOTCUsers')->getUserInfoByUserID("Email, UserID", $_POST['lookup']);
    } else {
        $AUTH = G::Obj('WOTCUsers')->getUserInfoByEmail("Email, UserID", $_POST['lookup']);
    }
    
    if ($AUTH['Email']) {
        
        $formatted_date = $MysqlHelperObj->getDatesList("DATE_FORMAT(NOW(),'%Y%m%d%H%m%s') AS FORMATTED_DATE");
        $SESS = $formatted_date['FORMATTED_DATE'];
        $tempsessionid = uniqid($SESS);
        
        $set_info = array(
            "SessionID = :sessionid"
        );
        $where = array(
            "UserID = :userid",
            "Email = :email"
        );
        $params = array(
            ':sessionid' => $tempsessionid,
            ':userid' => $AUTH['UserID'],
            ':email' => $AUTH['Email']
        );
        G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array(
            $params
        ));
        
        $to = $AUTH['Email'];
        $subject = "WOTC Admin Reminder";

        $message = "\nYour user name is: " . $AUTH['UserID'] . "<br>\n";
        $resetlink = WOTCADMIN_HOME . "reset.php";
        $resetlink .= "?s=" . $tempsessionid;
        if ($_GET['wotcID'] != "") {
            $resetlink .= "&wotcID=" . $_GET['wotcID'];
        }
        
        $message .= "Please reset your password here: <a href=\"" . $resetlink . "\">" . $resetlink;
        
        // Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
        
        // Set who the message is to be sent to
        $PHPMailerObj->addAddress($to);
        // Set the subject line
        $PHPMailerObj->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        $PHPMailerObj->msgHTML($message);
        // Content Type Is HTML
        $PHPMailerObj->ContentType = 'text/plain';
        // Send email
        $PHPMailerObj->send();
        
        echo "<p>Recovery email has been sent to your account.</p>";
    } else {
        
        if ($_POST['type'] == "username") {
            echo "<p>Information for that user name was not found.</p>";
        } else if ($_POST['type'] == "email") {
            echo "<p>Information for that email address was not found.</p>";
        }
    }
}

require_once 'Header.inc';
?>
<style>
#page-wrapper {
    background:#efefef none repeat scroll 0 0;
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    margin: 0 0 0 0px !important;
    padding: 5px;
    position: inherit;
    border-left: 1px solid #ddd;
}
</style>
<?php
require_once 'PageWrapperStart.inc';

$Logo = G::Obj('WOTC')->getWotcAdminLogo($wotcID);
?>
<div class="page-inner">
    <div class="row" style="text-align: center">
        <div class="col-lg-5 col-md-5 col-sm-5">
            <img src="<?php echo WOTC_HOME . "images/logos/" . $Logo;?>" style="margin:0px 30px 0px 0px;max-height:100px;" border="0">
            <br><br>
            <i style="text-align:right;font-size:16pt;">WOTC Login</i>
            <br><br>
        </div>
    </div>
    <div class="row" style="text-align: center">
        <div class="col-lg-5 col-md-5 col-sm-5">
        	<form method="post" action="login.php">
        		<p>
        			Username: <input name="user" size="23" maxlength="45" type="text">
        		</p>
        		<p>
        			Password: <input name="password" size="23" maxlength="25"
        				type="password">
        		</p>
        		<p>
        			<input type="checkbox" name="keep" value="yes">&nbsp;Remember this
        			computer.
        		</p>
        		<p>
        			<input value="Please Login" name="submit" type="submit">&nbsp;&nbsp;<?php echo $ERROR?></p>
        	</form>
        	<p><a href="javascript:ReverseDisplay('emailcheck')">forgot your username or password?</a></p>
        </div>
    </div>
    <div id="emailcheck" style="display: none; margin: 0 0 0 100px;">
    	<p>
    		Please enter either the email address or user name associated to this
    		account.<br>We will email a link to update your password to the email
    		address on record.
    	</p>
    	<form method="post" action="login.php">
        <?php if (!$_POST['type']) { $_POST['type']='email'; } ?>
        <p>
    			<input type="radio" name="type" value="username"
    				<?php if ($_POST['type'] == "username") { echo ' checked'; } ?>>Username
    			<input type="radio" name="type" value="email"
    				<?php if ($_POST['type'] == "email") { echo ' checked'; } ?>>Email
    			Address <input type="text" name="lookup" size="30"> <input
    				value="Lookup" name="submit" type="submit">
    		</p>
    	</form>
    </div>
</div>
<?php 
require_once 'PageWrapperEnd.inc';
require_once 'Footer.inc';
?>
