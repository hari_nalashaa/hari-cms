<?php
include 'Configuration.inc';

$AUTH       =   G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

// remove the session key from the database
$set_info   =   array("SessionID = ''", "Persist = NULL");
$where      =   array("SessionID = :sessionid");
$params     =   array(
                    ':sessionid' => $_COOKIE['WID'],
                );
G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array($params));

// remove the session key from the users browser
setcookie("WID", "", time() - 3600, "/");

require_once 'Header.inc';
?>
<style>
#page-wrapper {
    background:#efefef none repeat scroll 0 0;
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    margin: 0 0 0 0px !important;
    padding: 5px;
    position: inherit;
    border-left: 1px solid #ddd;
}
</style>
<?php
require_once 'PageWrapperStart.inc';
?>
<div class="page-inner">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <?php
            $Logo = G::Obj('WOTC')->getWotcAdminLogo($wotcID);
            echo "<img src=\"" . WOTC_HOME . "images/logos/" . $Logo . "\" border=\"0\" style=\"margin:0px 30px 0px 0px;max-height:100px;\"><br><br>";

            echo "<i style=\"text-align:right;font-size:16pt;\">You have successfully logged out.</i><br><br>";            
            ?>        
        	<p>
        		Viewed pages during this session
        		may still be seen in the browsers cache.<br> Please be sure to close
        		your browser completely to ensure data privacy.
        	</p>
        	<p>
        		<i>Thank You</i>
        	</p>
        	<p>
        		Click <a
        			href="<?php echo WOTCADMIN_HOME; ?>login.php?wotcID=<?php echo $AUTH['ACCESS'][0]; ?>">here</a>
        		to log back in.
        	</p>
        </div>
    </div>
</div>

<?php 
require_once 'PageWrapperEnd.inc';
require_once 'Footer.inc';
?>    
