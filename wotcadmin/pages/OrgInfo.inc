<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
    <?php
    $ACCESS             =   "'" . join('\',\'', $AUTH['ACCESS']) . "'";

    G::Obj('GenericQueries')->conn_string =   "WOTC";

    $query          =   "SELECT OrgData.wotcID, CRMFEIN.AKADBA AS CompanyName, OrgData.City, OrgData.State";
    $query         .=   " FROM OrgData";
    $query         .=   " LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
    $query         .=   " WHERE OrgData.wotcID IN ($ACCESS)";
    if ($AUTH['UserID'] != "2jsupply") {
         $query         .=   " AND OrgData.Active = 'Y'";
    }
    $query         .=   " ORDER BY CRMFEIN.AKADBA";

    $RESTRICTEDLIST =   G::Obj('GenericQueries')->getInfoByQuery($query);
    
    echo "<div style=\"margin-bottom:10px;\">\n";
    echo "<b style=\"font-size:130%;\">Account Information</b><br>\n";
    echo "</div>\n";
    
    echo '<div>';
    echo "<form method=\"POST\">\n";
    echo "<select name=\"wotcID\" onChange=\"getOrgInfo(this.value);\">\n";
    
    $i = 0;
    foreach ($RESTRICTEDLIST as $ROD) {
        $i ++;
        if ($i == 1) {
            $wotcID = $ROD['wotcID'];
            $_REQUEST['wotcID'] = $ROD['wotcID'];
        }
        echo '<option value="' . $ROD['wotcID'] . '"';
        if ($ROD['wotcID'] == $_POST['wotcID']) {
            echo ' selected';
        }
	echo '>' . $ROD['wotcID'];
	if ($ROD['CompanyName'] != "") {
	  echo ' - ' . $ROD['CompanyName'] . ' - ' . $ROD['City'] . ', ' . $ROD['State'];
	}
        echo '</option>';
    } // end foreach

    echo '</select>';
    echo '</form>';
    echo '</div>';

    
    echo '<div id="org_info">';
    if (isset($wotcID) && $wotcID != "") {

	$AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($wotcID);

        echo "<div style=\"margin-top:20px;\">";
        echo "<b style=\"font-size:14pt;\">" . $AKADBA['AKADBA'] . "</b><br>";
        echo "<br>";
             
        if (($AKADBA['City'] != "") && ($AKADBA['State'] != "") && ($AKADBA['ZipCode'] != "")) {
            echo "<strong>Address:</strong> ";
            if ($AKADBA['Address']) {
                 echo $AKADBA['Address'] . ", ";
            }
            echo $AKADBA['City'] . ", " . $AKADBA['State'] . "&nbsp;&nbsp;" . $AKADBA['ZipCode'] . "<br>";
        }
    
        if (($AKADBA['EIN1'] != "") && ($AKADBA['EIN2'] != "")) {
            echo "<strong>EIN:</strong> " . $AKADBA['EIN1'] . "-" . $AKADBA['EIN2'] . "<br>";
        }

	$CP = json_decode($AKADBA['Phone'], true);
        $PhoneAreaCode = $CP[0];
        $PhonePrefix = $CP[1];
        $PhoneSuffix = $CP[2];

        if (($PhoneAreaCode != "") && ($PhonePrefix != "") && ($PhoneSuffix != "")) {
            echo "<strong>Phone:</strong> " . $PhoneAreaCode . "-" . $PhonePrefix . "-" . $PhoneSuffix . "<br>";
        }

        echo '<a href="'.WOTC_HOME.'admin.php?wotcID='.$_REQUEST['wotcID'].'" target="_blank">Add Applicant</a>';
            
        echo "</div>";
        
    } // end wotcID
    echo '</div>';

