<strong>Paper Screening</strong>:<br>
Provide to any new hire in the past 2 weeks and any going forward.<br>
After completed, attached a company cover letter - They can be scanned and emailed to our processing center at <a href="mailto:cmswotc@cmswotc.com">cmswotc@cmswotc.com</a> or securely faxed to 860-356-4382.<br><br>
Please email/fax weekly or bi-weekly to be considered timely.
<hr>
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6">
        <a href="<?php echo WOTCADMIN_HOME;?>/forms/cms_WOTC_Paper_Questionnaire.pdf" target="_blank">
            <img src="<?php echo WOTCADMIN_HOME;?>images/Picture5.png" style="border: 1px solid grey" width="350">
        </a><br><br>
        <strong>WOTC Form 8850 Paper Questionnaire</strong> - English.<br>
        New hires should be asked to complete the cover sheet 
        <br> and complete the top and sign/date the bottom of the 8850.
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <a href="<?php echo WOTCADMIN_HOME;?>/forms/cms_WOTC_Paper_Questionnaire_SPANISH.pdf" target="_blank">
            <img src="<?php echo WOTCADMIN_HOME;?>images/Picture4.png" style="border: 1px solid grey" width="350">
        </a><br><br>
        <strong>WOTC Form 8850 Paper Questionnaire</strong> - Spanish.<br>
        New hires should be asked to complete the cover sheet 
        <br> and complete the top and sign/date the bottom of the 8850.
        
    </div>

</div>
<hr>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <a href="<?php echo WOTCADMIN_HOME;?>/forms/form_5884.pdf" target="_blank">
            <img src="<?php echo WOTCADMIN_HOME;?>images/Picture3.png" style="border: 1px solid grey" width="350">
        </a><br><br>
        <strong>IRS Form 5884</strong> - Employers file IRS Form 5884 to claim 
        <br> the work opportunity credit for qualified wages paid 
        <br> to or incurred for targeted group employees during 
        <br> the tax year. Please provide to CPA or Account at time of file.
    </div>
</div>
