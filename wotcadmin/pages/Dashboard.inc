<script language="javascript" type="text/javascript">
$(function() {
    var dates = $('#startdate, #enddate').datepicker(
    {
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        showOn: 'button',
        buttonImage: wotcadmin_home+'calendar/css/smoothness/images/calendar.gif',
        yearRange: '-80:+5',
        buttonImageOnly: true,
        buttonText: 'Select Date'
    });
});

function getApplicantsCountByStatus() {

	var wotcID     =   document.frmApplicantsCount.wotcID.value;
	var startdate  =   document.frmApplicantsCount.startdate.value;
	var enddate    =   document.frmApplicantsCount.enddate.value;
	
	$.ajax({
			method: "POST",
	  		url: "getApplicantsCountByStatus.php",
			type: "POST",
			dataType:"JSON",
			data: {"wotcID":wotcID, "StartDate":startdate, "EndDate":enddate},
			success: function(data) {
			   var RealizedTaxCreditsCount =  data.RealizedTaxCredits;

			   const formatter = new Intl.NumberFormat('en-US', {
				    style: 'currency',
				    currency: 'USD',
				    minimumFractionDigits: 2
				    });
			   $("#top_total_apps_cnt").html(data.All);
			   $("#top_certified_apps_cnt").html(data.Certified);
			   $("#top_pending_apps_cnt").html(data.Pending);
			   $("#top_realized_tax_credits_cnt").html(formatter.format(RealizedTaxCreditsCount));	
	    	}
    });
}
</script>

<!-- Morris Charts CSS -->
<link href="<?php echo WOTCADMIN_HOME;?>css/morris.css" rel="stylesheet">
<style>
.dashboard-top-blocks i {
	transition: all 0.5s ease-in-out 0s;
	-moz-transition: all 0.5s ease-in-out 0s;
	-webkit-transition: all 0.5s ease-in-out 0s;
	-o-transition: all 0.5s ease-in-out 0s;
}
.dashboard-top-blocks i {
	font-size: 60px;
	opacity: 0.2;
}
.dashboard-top-blocks i:hover {
	font-size: 80px;
	transform:rotate(360deg);
	-moz-transform:rotate(360deg);
	-webkit-transform:rotate(360deg);
	-o-transform:rotate(360deg);
    font-size: 90px;
	opacity: 0.6;
}
.dashboard-top-blocks:hover {
	cursor:pointer;
}
.dashboard-top-blocks .huge {
	transition: all 0.4s ease-in-out 0s;
	-moz-transition: all 0.4s ease-in-out 0s;
	-webkit-transition: all 0.4s ease-in-out 0s;
	-o-transition: all 0.4s ease-in-out 0s;
	position: absolute;
	right: 10px;
	top: 0px;
	font-size:35px;
}
.dashboard-top-blocks:hover .huge {
	transform:rotate(360deg);
	-moz-transform:rotate(360deg);
	-webkit-transform:rotate(360deg);
	-o-transform:rotate(360deg);
	font-size:45px;
	opacity: 0.6;
}
</style>
<?php
if(isset($_REQUEST['wotcIDBarChart']) && isset($_REQUEST['Year']))
{
    $bg_wotcID     =   ($_REQUEST['wotcID'] == "All") ? "" : $_REQUEST['wotcID'];
    $bg_year       =   ($_REQUEST['Year']) ? $_REQUEST['Year'] : "";
    
    $mdg    =   G::Obj('WOTCApplicantData')->getApplicantsCountByStatusPerMonth($bg_wotcID, $bg_year);
}
else {
	$mdg    =   G::Obj('WOTCApplicantData')->getApplicantsCountByStatusPerMonth("", date('Y'));
}
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
    
    <div class="panel panel-default">
        <div class="panel-heading" style="padding-bottom:30px;">
            <i class="fa fa-bar-chart-o fa-fw"></i> WOTC Status by Date Range
            <div style="float:right;">
                <form name="frmApplicantsCount" id="frmApplicantsCount">
                 <?php
                    $apps_status_count_info =   G::Obj('WOTCApplicantData')->getApplicantsCountByStatus("", date('Y-m-d', strtotime('-3 MONTH')), date('Y-m-d'));
                    
                    $ACCESS             =   "'" . join('\',\'', $AUTH['ACCESS']) . "'";

		    G::Obj('GenericQueries')->conn_string =   "WOTC";
		    $query          =   "SELECT OrgData.wotcID, CRMFEIN.AKADBA AS CompanyName, OrgData.City, OrgData.State";
		    $query         .=   " FROM OrgData";
		    $query         .=   " LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
		    $query         .=   " WHERE OrgData.wotcID IN ($ACCESS)";
		    if ($AUTH['UserID'] != "2jsupply") {
	  		$query         .=   " AND OrgData.Active = 'Y'";
	   	    }
		    $query         .=   " ORDER BY CRMFEIN.AKADBA";
		    $RESTRICTEDLIST =   G::Obj('GenericQueries')->getInfoByQuery($query);

                    echo "<select name=\"wotcID\" id=\"wotcID\">\n";
                    echo "<option value='All'>All</option>";
                    $i = 0;
                    foreach ($RESTRICTEDLIST as $ROD) {
                        $i ++;
                        if ($i == 1) {
                            $wotcID = $ROD['wotcID'];
                        }
                        echo '<option value="' . $ROD['wotcID'] . '"';
                        if ($ROD['wotcID'] == $_POST['wotcID']) {
                            echo ' selected';
                        }
                        echo '>' . $ROD['CompanyName'] . ' - ' . $ROD['City'] . ', ' . $ROD['State'] . '</option>';
                    } // end foreach
                    
                    echo '</select>';
                ?>
                <input id="startdate" name="startdate" value="<?php echo date('m/d/Y', strtotime('-3 MONTH'));?>" class="form-control width-auto-inline" size="10">
                <input id="enddate" name="enddate" value="<?php echo date('m/d/Y');?>" class="form-control width-auto-inline" size="10">
                
                <input type="button" id="btnAppsStatusCount" name="btnAppsStatusCount" value="Filter" class="btn btn-primary" onclick="getApplicantsCountByStatus()">
                </form>
                <br>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                    
                <div class="col-lg-3 col-md-3 col-sm-6">
        			<div class="panel" style="background-color:#3399ff;color:white;text-align:center;padding:10px;">
        				<div class="dashboard-top-blocks">
        					<div class="row">
        						<div class="col-xs-6">
                                    <i class="fa fa-users" style="opacity:0.5" aria-hidden="true"></i>
        						</div>
        						<div class="col-xs-6 text-right">
        							<div class="huge" id="top_total_apps_cnt">
        								<?php echo $apps_status_count_info['All'];?>
        							</div>
        						</div>
        					</div>
        					<div class="row">
        						<div class="col-xs-12 text-right">
        							WOTC Screens
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>

        		<div class="col-lg-3 col-md-3 col-sm-6">
        			<div class="panel" style="background-color:#ffcc00;color:white;text-align:center;padding:10px;">
        				<div class="dashboard-top-blocks">
        					<div class="row">
        						<div class="col-xs-6">
                                    <i class="fa fa-calendar"></i>
        						</div>
        						<div class="col-xs-6 text-right">
        							<div class="huge" id="top_pending_apps_cnt">
        								<?php echo $apps_status_count_info['Pending'];?>
        							</div>
        						</div>
        					</div>
        					<div class="row">
        						<div class="col-xs-12 text-right">
        							Pending with the State
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>

        		<div class="col-lg-3 col-md-3 col-sm-6">
        			<div class="panel" style="background-color:#9AC945;color:white;text-align:center;padding:10px;">
        				<div class="dashboard-top-blocks">
        					<div class="row">
        						<div class="col-xs-6">
                                    <i class="fa fa-check-square-o" style="opacity:0.5" aria-hidden="true"></i>
        						</div>
        						<div class="col-xs-6 text-right">
        							<div class="huge" id="top_certified_apps_cnt">
        								<?php echo $apps_status_count_info['Certified'];?>
        							</div>
        						</div>
        					</div>
        					<div class="row">
        						<div class="col-xs-12 text-right">
        							Certified from the State
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        		
        		<div class="col-lg-3 col-md-3 col-sm-6">
        			<div class="panel" style="background-color:#203864;color:white;text-align:center;padding:10px;">
        				<div class="dashboard-top-blocks">
        					<div class="row">
        						<div class="col-xs-12 text-center" style="font-size:18px;padding-top:15px;padding-bottom:18px;" id="top_realized_tax_credits_cnt">
        								<?php echo "$".number_format($apps_status_count_info['RealizedTaxCredits'], 2, ".", ",");?>
        						</div>
        					</div>
        					<div class="row">
        						<div class="col-xs-12 text-center">
        							Realized Tax Credits
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        		
            </div>
        </div>           
    </div>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading" style="padding-bottom:30px;">
                <i class="fa fa-bar-chart-o fa-fw"></i> WOTC Year Overview
                
                <div style="float:right;">
                    <form name="frmApplicantsCountBarChart" id="frmApplicantsCountBarChart" method="get">
                     <?php
                        $apps_status_count_info =   G::Obj('WOTCApplicantData')->getApplicantsCountByStatus("", date('Y-m-d', strtotime('-3 MONTH')), date('Y-m-d'));
                        
                        $ACCESS             =   "'" . join('\',\'', $AUTH['ACCESS']) . "'";

			G::Obj('GenericQueries')->conn_string =   "WOTC";
                        $query          =   "SELECT OrgData.wotcID, CRMFEIN.AKADBA AS CompanyName, OrgData.City, OrgData.State";
                        $query         .=   " FROM OrgData";
                        $query         .=   " LEFT JOIN CRMFEIN ON CRMFEIN.CompanyID = OrgData.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
                        $query         .=   " WHERE OrgData.wotcID IN ($ACCESS)";
                        if ($AUTH['UserID'] != "2jsupply") {
                            $query         .=   " AND OrgData.Active = 'Y'";
                        }
                        $query         .=   " ORDER BY CRMFEIN.AKADBA";
                        $RESTRICTEDLIST =   G::Obj('GenericQueries')->getInfoByQuery($query);

                        
                        echo "<select name=\"wotcIDBarChart\" id=\"wotcIDBarChart\">\n";
                        echo "<option value='All'>All</option>";
                        $i = 0;
                        foreach ($RESTRICTEDLIST as $ROD) {
                            $i ++;
                            if ($i == 1) {
                                $wotcID = $ROD['wotcID'];
                            }
                            echo '<option value="' . $ROD['wotcID'] . '"';
                            if ($ROD['wotcID'] == $_POST['wotcID']) {
                                echo ' selected';
                            }
                            echo '>' . $ROD['CompanyName'] . ' - ' . $ROD['City'] . ', ' . $ROD['State'] . '</option>';
                        } // end foreach
                        
                        echo '</select>';
                    ?>
                    <select name="Year" id="Year">
                        <?php 
                            for($year = 2000; $year <= date('Y'); $year++) {
                            	echo "<option value='".$year."'";
				if($year == date('Y') ) { echo " selected"; }
				echo ">".$year."</option>";
                            }
                        ?>
                    </select>
                    <!-- onclick="getApplicantsCountyByStatusPerMonth()" -->
                    <input type="button" id="btnAppsStatusCountBarChart" name="btnAppsStatusCountBarChart" onclick="getApplicantsCountyByStatusPerMonth()" value="Filter" class="btn btn-primary">
                    </form>
                    <br>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row" style="text-align: center;">
                    <div class="col-lg-12">
                        <div style="width:160px;height:60px;display:inline-block;">
                            <span style="width:100%;padding-bottom:10px">
                            <strong id="all_apps_count">
                            <?php
                            $all_applicants = 0;
                            for($t = 1; $t <= 12; $t++) {
                            	$all_applicants += $mdg['A'][$t];
                            }
                            echo $all_applicants;
                            ?>
                            </strong></span><br><br>
                            <span style="width:100%;background-color:#3399ff;color:white;padding:5px 30px;">WOTC Screens</span>
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        <div style="width:160px;height:60px;display:inline-block;">
                            <span style="width:100%;padding-bottom:10px ">
                            <strong id="pending_apps_count">
                            <?php
                            $pending_applicants = 0;
                            for($t = 1; $t <= 12; $t++) {
                            	$pending_applicants += $mdg['P'][$t];
                            }
                            echo $pending_applicants;
                            ?>
                            </strong>
                            </span><br><br>
                            <span style="background-color:#ffcc00;color:white;padding:5px;">Pending with the State</span>
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        <div style="width:160px;height:60px;display:inline-block;">
                            <span style="width:100%;padding-bottom:10px">
                            <strong id="certified_apps_count">
                            <?php
                            $certified_applicants = 0;
                            for($t = 1; $t <= 12; $t++) {
                            	$certified_applicants += $mdg['C'][$t];
                            }
                            echo $certified_applicants;
                            ?>
                            </strong></span><br><br>
                            <span style="background-color:#9AC945;color:white;padding:5px;">Certified from the State</span>
                        </div>
                    </div>
                </div>

                <div style="clear:both"></div>

                <!--div class="row">
                    <div class="col-lg-12" style="text-align: center">
                        <span style="background-color:#3399ff;color:white;padding:5px;width:50px;height:5px;display:inline-block;margin-top:10px;"></span>&nbsp;WOTC Screens
                        &nbsp;&nbsp;<span style="background-color:#ffcc00;color:white;padding:5px;width:50px;height:5px;display:inline-block;margin-top:10px;"></span>&nbsp;Pending with the State
                        &nbsp;&nbsp;<span style="background-color:#9AC945;color:white;padding:5px;width:50px;height:5px;display:inline-block;margin-top:10px;"></span>&nbsp;Certified from the State
                    </div>
                </div-->
                
                <div class="row">
                    <div class="col-lg-12">
                        <div id="morris-bar-chart"></div>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
    </div>
</div>
<?php
$months_data_info = "{y:'January',a:".$mdg['A'][1].",p:".$mdg['P'][1].",c:".$mdg['C'][1]."},";
$months_data_info .= "{y:'February',a:".$mdg['A'][2].",p:".$mdg['P'][2].",c:".$mdg['C'][2]."},";
$months_data_info .= "{y:'March',a:".$mdg['A'][3].",p:".$mdg['P'][3].",c:".$mdg['C'][3]."},";
$months_data_info .= "{y:'April',a:".$mdg['A'][4].",p:".$mdg['P'][4].",c:".$mdg['C'][4]."},";
$months_data_info .= "{y:'May',a:".$mdg['A'][5].",p:".$mdg['P'][5].",c:".$mdg['C'][5]."},";
$months_data_info .= "{y:'June',a:".$mdg['A'][6].",p:".$mdg['P'][6].",c:".$mdg['C'][6]."},";
$months_data_info .= "{y:'July',a:".$mdg['A'][7].",p:".$mdg['P'][7].",c:".$mdg['C'][7]."},";
$months_data_info .= "{y:'August',a:".$mdg['A'][8].",p:".$mdg['P'][8].",c:".$mdg['C'][8]."},";
$months_data_info .= "{y:'September',a:".$mdg['A'][9].",p:".$mdg['P'][9].",c:".$mdg['C'][9]."},";
$months_data_info .= "{y:'October',a:".$mdg['A'][10].",p:".$mdg['P'][10].",c:".$mdg['C'][10]."},";
$months_data_info .= "{y:'November',a:".$mdg['A'][11].",p:".$mdg['P'][11].",c:".$mdg['C'][11]."},";
$months_data_info .= "{y:'December',a:".$mdg['A'][12].",p:".$mdg['P'][12].",c:".$mdg['C'][12]."}";
?>
<script type="text/javascript">
var mdg_data = [<?php echo $months_data_info;?>];
</script>
<script src="<?php echo WOTCADMIN_HOME;?>/js/raphael.min.js"></script>
<script src="<?php echo WOTCADMIN_HOME;?>/js/morris.js"></script>
<script src="<?php echo WOTCADMIN_HOME;?>/js/morris-data.js"></script>
