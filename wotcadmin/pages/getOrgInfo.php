<?php 
include '../Configuration.inc';

$AUTH   =   G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

if (isset($_REQUEST['wotcID']) && $_REQUEST['wotcID'] != "") {

    $AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($_REQUEST['wotcID']);

    echo "<div style=\"margin-top:20px;\">";
    echo "<b style=\"font-size:14pt;\">" . $AKADBA['AKADBA'] . "</b><br>";
    echo "<br>";

    if (($AKADBA['City'] != "") && ($AKADBA['State'] != "") && ($AKADBA['ZipCode'] != "")) {
        echo "<strong>Address:</strong> ";
        if ($AKADBA['Address']) {
             echo $AKADBA['Address'] . ", ";
        }
        echo $AKADBA['City'] . ", " . $AKADBA['State'] . "&nbsp;&nbsp;" . $AKADBA['ZipCode'] . "<br>";
    }

    if (($AKADBA['EIN1'] != "") && ($AKADBA['EIN2'] != "")) {
        echo "<strong>EIN:</strong> " . $AKADBA['EIN1'] . "-" . $AKADBA['EIN2'] . "<br>";
    }

    $CP = json_decode($AKADBA['Phone'], true);
    $PhoneAreaCode = $CP[0];
    $PhonePrefix = $CP[1];
    $PhoneSuffix = $CP[2];

    if (($PhoneAreaCode != "") && ($PhonePrefix != "") && ($PhoneSuffix != "")) {
        echo "<strong>Phone:</strong> " . $PhoneAreaCode . "-" . $PhonePrefix . "-" . $PhoneSuffix . "<br>";
    }

    echo '<a href="'.WOTC_HOME.'admin.php?wotcID='.$_REQUEST['wotcID'].'" target="_blank">Add Applicant</a>';

    echo "</div>";

} // end wotcID

?>
