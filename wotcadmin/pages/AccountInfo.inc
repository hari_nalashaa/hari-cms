<?php
if ($_POST['submit'] == "Change Information") {
    
    $ERROR = "";
    
    if ($_POST['pass2']) {
        $ERROR .= $ValidateObj->validate_verification($_POST['pass'], $_POST['pass2'], "Y");
    }
    
    $ERROR .= $ValidateObj->validate_email_address($_POST['email']);
    
    if ($_POST['minutes'] < 10) {
        $ERROR .= "Minutes cannot be set less than 10." . "\\n";
    }
    
    if ($ERROR) {
        $DISPLAY = "<div>";
        $DISPLAY .= "Please correct the following errors:";
        $DISPLAY .= "<div style=\"color:red;margin-left:10px;\">";
        $DISPLAY .= stripslashes(str_replace("\\n", '<br>', $ERROR));
        $DISPLAY .= "</div>";
        $DISPLAY .= "</div>";
    } else {
        //Update Minutes, Email
        $set_info   =   array("Minutes = :minutes", "Email = :email");
        $where      =   array("UserID = :userid");
        $params     =   array(
                            ':minutes'  =>  $_POST['minutes'],
                            ':email'    =>  $_POST['email'],
                            ':userid'   =>  $AUTH['UserID'],
                        );
        G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array($params));
        
        
        if ($_POST['pass2']) {

            $set_info   =   array("Verification = :pass");
            $where      =   array("UserID = :userid");
            $params     =   array(
                                ':pass'     =>  password_hash($_POST['pass2'], PASSWORD_DEFAULT),
                                ':userid'   =>  $AUTH['UserID'],
                            );
            G::Obj('WOTCUsers')->updUsersInfo($set_info, $where, array($params));
            
        }
        
        $DISPLAY = "<div style=\"font-size:120%;\">";
        $DISPLAY .= "Success!";
        $DISPLAY .= "</div>";
    }
} // end submit

if (! $_POST['submit']) {
    //Get User Details
    $USER   =   G::Obj('WOTCUsers')->getUserInfoByUserID("*", $AUTH['UserID']);
} else {
    $USER['UserID']     =   $AUTH['UserID'];
    $USER['Email']      =   $_POST['email'];
    $USER['Minutes']    =   $_POST['minutes'];
}
?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']?>">
    
    	<div style="margin-bottom: 20px; line-height: 180%;">
		<b style="font-size: 130%;">Login Information</b><br>
		Use this page to change your login information.
    	</div>
    
    	<div style="margin-bottom: 15px;">
	<table>
	<tr>
	<td align="right">User Name:</td>
	<td><b><?php echo $USER['UserID']; ?></b></td>
	</tr>

	<tr>
	<td align="right">Password:</td>
	<td><input type="password" name="pass" value="" size="20" maxlength="45"></td>
	</tr>

	<tr>
	<td align="right">Password Again:</td>
	<td><input type="password" name="pass2" value="" size="20" maxlength="45"></td>
	</tr>

	<tr>
	<td align="right">Email Address:</td>
	<td><input type="text" name="email" value="<?php echo $USER['Email']; ?>" size="40" maxlength="55"> Email will be used to recover your password.</td>
	</tr>

	<tr>
	<td align="right">Active Minutes:</td>
	<td><input type="text" name="minutes" value="<?php echo $USER['Minutes']; ?>" size="2" maxlength="3"> Number of minutes before being automatically logged out.
	</td>
	</tr>

	</table>
    
    	<div style="margin-bottom: 10px; margin-left: 20px; float: left; margin-right: 20px;">
    		<input type="hidden" name="pg" value="<?php echo $_REQUEST['pg']; ?>">
    		<input type="submit" name="submit" value="Change Information">
    	</div>
        <?php echo $DISPLAY; ?>
    </form>
    
    </div>
</div>
