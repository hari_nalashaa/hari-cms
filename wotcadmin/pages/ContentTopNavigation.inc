<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="row ui-sortable" id="topblocks">
        
        	<div class="col-lg-3 col-md-6">
        		<div class="panel panel-green" style="min-height: 120px; background-color: #5CB85C;color: white;text-align: center;padding:35px;">
                    <h4>Upload Documents</h4>
        		</div>
        	</div>
        	<div class="col-lg-3 col-md-6">
        		<div class="panel panel-red" style="min-height: 120px; background-color: #D9534F;color: white;text-align: center;padding:35px;">
        			<h4>Manage Locations</h4>
        		</div>
        	</div>
        	<div class="col-lg-3 col-md-6">
        		<div class="panel panel-yellow" style="min-height: 120px; background-color: #F0AD4E;color: white;text-align: center;padding:35px;">
        			<h4>Submit New Application</h4>
        		</div>
        	</div>
            <div class="col-lg-3 col-md-6">
        		<div class="panel panel-red" style="min-height: 120px; background-color: #D9534F;color: white;text-align: center;padding:35px;">
        			<h4>Something Else TBD</h4>
        		</div>
        	</div>
        	
        </div>
    </div>
</div>