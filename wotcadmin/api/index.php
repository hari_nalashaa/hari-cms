<?php
include '../Configuration.inc';

G::Obj('GenericQueries')->conn_string =   "WOTC";

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json");

$api_key = $_SERVER['HTTP_X_API_KEY'];

$KEY = G::Obj('WOTCcrm')->validateAPIKey($api_key);
$CompanyID = $KEY['CompanyID'];
$Assignment = $KEY['Assignment'];

if(!$api_key || !$CompanyID) {
    G::Obj('RestAPI')->send_results("Non-Authoritative Information", 203);
    return;
}

$methods_JSON = '{
            "account": {
              "GET": {
                "default": "get_account_access_info",
                "accessinfo": "get_account_access_info"
              },
              "POST": {
                "create": "create_account"
              }
	    },
	    "application": {
              "POST": {
                "process": "process_application"
              }
	    },
	    "report": {
              "GET": {
                "payroll": "get_needs_payroll",
                "status": "get_status",
                "credits": "get_credit_details",
                "applicants": "get_applicant_status"
              }
            }
	  }';

$methods = [];
$methods = json_decode($methods_JSON, true);

$http_method = $_SERVER["REQUEST_METHOD"];

//$data = file_get_contents("php://input"); // JSON string
//$data_input = sizeof($data) ? $data : null;

$path = G::Obj('RestAPI')->decodePath();
$method = isset($path[0]) && !empty($path[0]) ? $path[0] : null;
$sub_function = isset($path[1]) && !empty($path[1]) ? $path[1] : null;
$data_value = isset($path[2]) && !empty($path[2]) ? $path[2] : null;

$func_name = G::Obj('RestAPI')->get_function($methods, $method, $http_method, $sub_function);

if(!$func_name) {
    G::Obj('RestAPI')->send_results("Not Found", 404);
    return;
}

switch ($method) {
   case "account":
	if ($func_name == 'create_account') {

	       $status="";
	       $message="";

               $columns = 'CompanyID';
               $params = array(":ClientID"=>$_POST['ClientID'],":CompanyID"=>$CompanyID);
               $where[] = "ClientID = :ClientID";
	       if ($Assignment == 'Business Partners') {
           	  $where[] = "Partner = :CompanyID";
	       } else {
           	  $where[] = "CompanyID = :CompanyID";
	       }
               $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
               $DATA = $RESULTS['results'];

	       if ($DATA[0]['CompanyID'] != "") {
		  $status="error";
		  $message="The ClientID " . $_POST['ClientID'] . " is currently in use.";
	       }

	       if ($_POST['ClientID'] == "") {
		  $status="error";
		  $message="A ClientID is required.";
	       }

	       if ($status == "") {

	        $NewCompanyID = uniqid() . rand(1, 1);
		list($PhoneAreaCode,$PhonePrefix,$PhoneSuffix) = explode("-",$_POST['CompanyPhone']);
		$C = json_encode(array("Contact1"=>$_POST['ContactName'],"Email1"=>$_POST['ContactEmailAddress']));

		$info   =   array(
                 "CompanyID"    =>  $NewCompanyID,
                 "Active"       =>  'Y',
                 "Assignment"   =>  'BP Clients',
                 "CompanyName"  =>  $_POST['CompanyName'],
                 "Address"      =>  $_POST['Address'],
                 "City"         =>  $_POST['City'],
                 "State"        =>  $_POST['State'],
                 "ZipCode"      =>  $_POST['ZipCode'],
                 "CompanyPhone" =>  json_encode(array($PhoneAreaCode,$PhonePrefix,$PhoneSuffix)),
                 "Contacts"     =>  $C,
                 "ClientID"     =>  $_POST['ClientID']
                );

	        if ($Assignment == 'Business Partners') {
		  $info['Partner']=$CompanyID;
	        }

		$skip   =   array("CompanyID");

		G::Obj('WOTCcrm')->insUpdCRM($info, $skip);

	        if ($_FILES['POAFile']['name']) {

		    $dir = ADMIN_DIR . 'vault/' . $NewCompanyID;

        	    if (! file_exists($dir)) {
	                mkdir($dir, 0700);
	                chmod($dir, 0777);
	            }

	            $name = $_FILES ['POAFile'] ['name'];
	            $name = preg_replace ( '/\s/', '_', $name );
	            $name = preg_replace ( '/\&/', '', $name );
		    $name = "POA-" . date("YmdHis") . "-" . $name;

	            $data = file_get_contents ( $_FILES ['POAFile'] ['tmp_name'] );

	            $filename = $dir . "/" . $name;

	            $fh = fopen ( $filename, 'w' );
	            fwrite ( $fh, $data );
	            fclose ( $fh );

	            chmod ( $filename, 0666 );

		    $link = ADMIN_HOME . 'vault/' . $NewCompanyID . "/" . $name;
		    $hyperlink = '<a href="' . $link . '" target="_blank">' . $name . '</a>';

	        } // end FILES

		$Notes = "Company Added.\n";
		$Details = " - ClientID: " . $_POST['ClientID'] . ".\n";
		$Details .= " - Needs wotcID setup for EIN: " . $_POST['EIN'] . ".\n";
		if ($hyperlink != "") {
		  $Details .= " - A POA File is located here: " . $hyperlink . ".\n";
		}
		$Notes .= $Details;

	        $infohistory   =   array(
                 "HistoryID"    =>  uniqid() . rand(1, 1),
                 "CompanyID"    =>  $NewCompanyID,
                 "Date"         =>  'NOW()',
                 "UserID"       =>  'API',
		 "Notes"        =>  $Notes
		);

	        $skiphistory   =   array("HistoryID");

		G::Obj('WOTCcrm')->insUpdCRMHistory($infohistory, $skiphistory);

		$subject="New BP Client Added to CRM";
		$message="The following company has been set up in the CRM under BP Clients.<br><br>";
		$message.=$_POST['CompanyName'] . "<br>";
		$message.=preg_replace('/\n/', '<br>',$Details);
		
		G::Obj('PHPMailer')->clearCustomProperties();
        	G::Obj('PHPMailer')->clearCustomHeaders();
        	G::Obj('PHPMailer')->setFrom ('dedgecomb@irecruit-software.com', 'David Edgecomb');
        	G::Obj('PHPMailer')->addAddress ('skelly@cmswotc.com','Sean Kelly');
        	G::Obj('PHPMailer')->addBCC('dedgecomb@irecruit-software.com', 'David Edgecomb');
        	G::Obj('PHPMailer')->Subject = $subject;
        	G::Obj('PHPMailer')->msgHTML ( $message );
        	G::Obj('PHPMailer')->ContentType = 'text/html';

	        if ($_FILES['POAFile']['name']) {
	          G::Obj('PHPMailer')->addStringAttachment($data, $name, 'base64', 'text/html');
	        } // end POA-Attachment

		G::Obj('PHPMailer')->send ();
		
		$status="success";
		$message="New account created for " . $_POST['ClientID'];

	       }

	    $res = array("status"=>$status,"message"=>$message);


	} else if ($func_name == 'get_account_access_info') {
	
	    $APIACCESS = G::Obj('WOTCcrm')->getAPIAccessInfo($CompanyID,$Assignment,$data_value);

	         $i=0;
	    	 foreach ($APIACCESS AS $A) {
		    $res[$i]['ClientID']=$A['ClientID'];
		    $res[$i]['wotcID']=$A['wotcID'];
		    $res[$i]['EIN']=$A['FEIN'];
		    $res[$i]['Employer']=$A['AKADBA'];
		    $res[$i]['Address']=$A['Address'];
		    $res[$i]['City']=$A['City'];
		    $res[$i]['State']=$A['State'];
		    $res[$i]['ZipCode']=$A['ZipCode'] . " ";
		  $i++;
	    	 } // end foreach STATUS
	} 

   case "application":
	if ($func_name == 'process_application') {

	    $status="";
	    $type="";
	    $message=array();

	    $WOTCID = G::Obj('WOTCcrm')->validatewotcID_API($_POST['wotcID']);

	    if ($WOTCID == "") {
	      $status="error";
	      $type="wotcID";
	      $message = "Invalid wotcID.";
	    } else {

	    	$duplicate = G::Obj('WOTCcrm')->checkDuplicate_API($_POST['wotcID'],$_POST);
	    	if ($duplicate != "") {
	    	  $status="error";
	    	  $type="Duplicate";
	    	  $message = $duplicate;
	    	} else {

	    		if ($_POST['FirstName'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['FirstName'] = 'First Name data is missing.';
	    		}

	    		if ($_POST['LastName'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['LastName'] = 'Last Name data is missing.';
	    		}

	    		if ($_POST['Address'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Address'] = 'Address data is missing.';
	    		}

	    		if ($_POST['City'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['City'] = 'City data is missing.';
	    		}

	    		if ($_POST['State'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['State'] = 'State data is missing.';
	    		}

	    		if ($_POST['ZipCode'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['ZipCode'] = 'Zip Code data is missing.';
	    		}

	    		if ($_POST['Social1'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Social1'] = 'Social Security Number data is missing.';
	    		}

	    		if ($_POST['Social2'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Social2'] = 'Social Security Number data is missing.';
	    		}

	    		if ($_POST['Social3'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Social3'] = 'Social Security Number data is missing.';
	    		}

	    		if ($_POST['DOB_Date'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['DOB_Date'] = 'Date of Birth data is missing.';
	    		}

	    		if ($_POST['Signature'] == "") {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Signature'] = 'Signature acknowledgement data is missing.';
	    		}

			$NOTIN=array("Y","N");

	    		if (!in_array($_POST['Worked'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Worked'] = 'Worked before data is missing.';
	    		}

	    		if (!in_array($_POST['TANF'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['TANF'] = 'Temporary Assistance for Needy Families data is missing.';
	    		}

	    		if (!in_array($_POST['SNAP'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['SNAM'] = 'Supplemental Nutrition Assistance Program data is missing.';
	    		}

	    		if (!in_array($_POST['SSI'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['SSI'] = 'Supplemental Security Income benefits data is missing.';
	    		}

	    		if (!in_array($_POST['Felony'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Felony'] = 'Felony conviction data is missing.';
	    		}

	    		if (!in_array($_POST['Veteran'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['Veteran'] = 'Veteran data is missing.';
	    		}

	    		if (!in_array($_POST['SWA2'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['SWA2'] = 'Being feferred by an agency data is missing.';
	    		}

	    		if (!in_array($_POST['SWA3'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['SWA3'] = 'Being referred by Social Security Ticket to Work Program data is missing.';
	    		}

	    		if (!in_array($_POST['SWA4'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['SWA4'] = 'Being referred by the Department of Veteran Affairs data is missing.';
	    		}

	    		if (!in_array($_POST['LTU'],$NOTIN)) {
	    		  $status="error";
	    		  $type="Data";
	    		  $message['LTU'] = 'Received unemployment compensation data is missing.';
	    		}


			$message=json_encode($message);

		}

	    }

	    if ($status != "error") {

	       $_POST['Initiated'] = "Partner";
	       $_POST['ApplicationStatus'] = "H";
	       $_POST['captcha'] = $_POST['Signature'];

	       require_once WOTC_DIR . 'forms/Process.inc';

	       $APPDATA            =       G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $WOTCID, $WotcFormID, $ApplicationID);

	       if ((($APPDATA['EnterpriseZone'] == 'Y') && ($APPDATA['DOB'] == 'Y')) || ($APPDATA['Processed'] == 'R')) {

                $set_info   =   array("Qualifies = 'Y'", "Processed = 'P'", "Processed_Date = now()");
                $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
                $params =   array(':wotcID'=>$WOTCID, ':ApplicationID'=>$ApplicationID);
                G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));

               } else { // else not qualified

                $set_info   =   array("Qualifies = 'N'", "Processed_Date = now()");
                $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
                $params =   array(':wotcID'=>$WOTCID, ':ApplicationID'=>$ApplicationID);
                G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));

	       } // end qualified

	      $status="success";
	      $type="Application";
	      $message=$ApplicationID . ': ApplicationID has been created.';

	    } // end process 


	    $res = array("status"=>$status,"type"=>$type,"message"=>$message);

	} 

   case "report":
	if ($func_name == 'get_needs_payroll') {

	   if ($data_value != "") {

               $columns = 'CompanyID';
               $params = array(":ClientID"=>$data_value,":CompanyID"=>$CompanyID);
               $where[] = "ClientID = :ClientID";
	       if ($Assignment == 'Business Partners') {
           	  $where[] = "Partner = :CompanyID";
	       } else {
           	  $where[] = "CompanyID = :CompanyID";
	       }
               $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
               $DATA = $RESULTS['results'];

	       $PAYROLLDATA = array();
	       if ($DATA[0]['CompanyID'] != "") {
	         $PAYROLLDATA = G::Obj('WOTCcrm')->getAwaitingPayrollData($DATA[0]['CompanyID']);
	       }

	        $i=0;
	    	foreach ($PAYROLLDATA AS $PD) {
		    $res[$i]['ClientID']=$data_value;
		    $res[$i]['ApplicationID']=$D['ApplicationID'];
		    $res[$i]['EIN']=$PD['EIN'];
		    $res[$i]['Employer']=$D['AKADBA'];
		    $res[$i]['POAState']=$D['POAState'];
		    $res[$i]['HiredDate']=$D['HiredDate'];
		    $res[$i]['Confirmed']=$D['Confirmed'];
		    $res[$i]['ApplicantName']=$D['ApplicantName'];
		    $res[$i]['SSN']=$D['SSN'];
		  $i++;
	    	} // end foreach PAYROLLDATA

	   } // end data_value

	} else if ($func_name == 'get_status') {

	   if ($data_value != "") {

               $columns = 'CompanyID';
               $params = array(":ClientID"=>$data_value,":CompanyID"=>$CompanyID);
               $where[] = "ClientID = :ClientID";
	       if ($Assignment == 'Business Partners') {
           	  $where[] = "Partner = :CompanyID";
	       } else {
           	  $where[] = "CompanyID = :CompanyID";
	       }
               $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
               $DATA = $RESULTS['results'];

	       $STATUSDATA = array();
	       if ($DATA[0]['CompanyID'] != "") {
	         $STATUSDATA = G::Obj('WOTCcrm')->getRealTimeReportData($DATA[0]['CompanyID']);

	         $i=0;
	    	 foreach ($STATUSDATA['results'] AS $SD) {
		    $res[$i]['Year']=$SD['Year'];
		    $res[$i]['EIN']=$SD['FEIN'];
		    $res[$i]['Employer']=$SD['AKADBA'];
		    $res[$i]['WOTCScreens']=$SD['WOTCScreens'];
		    $res[$i]['Qualified']=$SD['Qualified'];
		    $res[$i]['PendingWithState']=$SD['PendingwithState'];
		    $res[$i]['CertifiedFromState']=$SD['CertifiedfromState'];
		    $res[$i]['AwaitingHoursWages']=$SD['AwaitingHoursWages'];
		    $res[$i]['RealizedCredits']=number_format($SD['RealizedCredits'],2,'.',',');
		  $i++;
	    	 } // end foreach STATUSDATA
	       }

	   } // end data_value


	} else if ($func_name == 'get_credit_details') {

	   if ($data_value != "") {

               $columns = 'CompanyID';
               $params = array(":ClientID"=>$data_value,":CompanyID"=>$CompanyID);
               $where[] = "ClientID = :ClientID";
	       if ($Assignment == 'Business Partners') {
           	  $where[] = "Partner = :CompanyID";
	       } else {
           	  $where[] = "CompanyID = :CompanyID";
	       }
               $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
               $DATA = $RESULTS['results'];

	       $BILLINGDATA = array();
	       if ($DATA[0]['CompanyID'] != "") {
	         $BILLINGDATA = G::Obj('WOTCcrm')->getBillingData($DATA[0]['CompanyID']);
	       }

	        $i=0;
	  	foreach ($BILLINGDATA AS $BD) {
		    $res[$i]['ClientID']=$data_value;
		    $res[$i]['ApplicationID']=$BD['ApplicationID'];
		    $res[$i]['EIN']=$BD['EIN'];
		    $res[$i]['Employer']=$BD['AKADBA'];
		    $res[$i]['POAState']=$BD['POAState'];
		    $res[$i]['ApplicantName']=$BD['ApplicantName'];
		    $res[$i]['SSN']=$BD['SSN'];
		    $res[$i]['Termed']=$BD['Termed'];
		    $res[$i]['Reason']=$BD['Reason'];
		    $res[$i]['TaxCredit']=number_format($BD['TaxCredit'],2,'.',',');
		    $res[$i]['DateRealized']=$BD['DateBilled'];
		    $res[$i]['DateIssued']=$BD['DatePaid'];
		    $i++;
	    	} // end foreach

	   } // end data_value

	} else if ($func_name == 'get_applicant_status') {

	   if ($data_value != "") {

               $columns = 'CompanyID';
               $params = array(":ClientID"=>$data_value,":CompanyID"=>$CompanyID);
               $where[] = "ClientID = :ClientID";
	       if ($Assignment == 'Business Partners') {
           	  $where[] = "Partner = :CompanyID";
	       } else {
           	  $where[] = "CompanyID = :CompanyID";
	       }
               $RESULTS = G::Obj('WOTCcrm')->getCRM($columns, $where, '', '', array($params));
               $DATA = $RESULTS['results'];

	       $STATUS = array();
	       if ($DATA[0]['CompanyID'] != "") {
	         $STATUS = G::Obj('WOTCcrm')->getApplicantStatus($DATA[0]['CompanyID']);

		 $utc_offset =  date('Z') / 3600;
		 if ($utc_offset == '-4') { $TZ=" EDT"; } else { $TZ=" EST"; }

	         $i=0;
	    	 foreach ($STATUS AS $S) {
		    $res[$i]['wotcID']=$S['wotcID'];
		    $res[$i]['EIN']=$S['EIN'];
		    $res[$i]['Employer']=$S['AKADBA'];
		    $res[$i]['ApplicationID']=$S['ApplicationID'];
		    $res[$i]['ApplicationDateTime']=$S['EntryDate'] . $TZ;
		    $res[$i]['ApplicantName']=$S['ApplicantName'];
		    $res[$i]['SSN']=$S['SSN'];
		    $res[$i]['PotentiallyQualified']=$S['PotentiallyQualified'];
		    $res[$i]['ReceivedCMS']=$S['ReceivedCMS'];
		    $res[$i]['FiledWithState']=$S['FiledWithState'];
		    $res[$i]['ConfirmedFromState']=$S['ConfirmedFromState'];
		    $res[$i]['StateQualified']=$S['StateQualified'];
		    $res[$i]['DateRealized']=$S['DateRealized'];
		    $res[$i]['DateIssued']=$S['DateIssued'];
		    $res[$i]['Reason']=$S['Reason'];
		    $res[$i]['TaxCredit']=$S['TaxCredit'];
		    $res[$i]['Attributes']=$S['Attributes'];
		  $i++;
	    	 } // end foreach STATUS
	       }

	   } // end data_value
	}

       	G::Obj('RestAPI')->send_results($res, 200);

   break;
       	default:
       	G::Obj('RestAPI')->send_results("Not Found", 404);

}

?>
