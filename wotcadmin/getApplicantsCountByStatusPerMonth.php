<?php
include 'Configuration.inc';

$AUTH       =   G::Obj('WOTCAuthenticate')->authenticate($_COOKIE['WID']);

$wotcID     =   ($_REQUEST['wotcID'] == "All") ? "" : $_REQUEST['wotcID'];
$Year       =   ($_REQUEST['Year']) ? $_REQUEST['Year'] : "";

$mdg        =   G::Obj('WOTCApplicantData')->getApplicantsCountByStatusPerMonth($wotcID, $Year);

$months_data_info = [];
$months_data_info[] = [y=>"January",a=>$mdg['A'][1],p=>$mdg['P'][1],c=>$mdg['C'][1]];
$months_data_info[] = [y=>"February",a=>$mdg['A'][2],p=>$mdg['P'][2],c=>$mdg['C'][2]];
$months_data_info[] = [y=>"March",a=>$mdg['A'][3],p=>$mdg['P'][3],c=>$mdg['C'][3]];
$months_data_info[] = [y=>"April",a=>$mdg['A'][4],p=>$mdg['P'][4],c=>$mdg['C'][4]];
$months_data_info[] = [y=>"May",a=>$mdg['A'][5],p=>$mdg['P'][5],c=>$mdg['C'][5]];
$months_data_info[] = [y=>"June",a=>$mdg['A'][6],p=>$mdg['P'][6],c=>$mdg['C'][6]];
$months_data_info[] = [y=>"July",a=>$mdg['A'][7],p=>$mdg['P'][7],c=>$mdg['C'][7]];
$months_data_info[] = [y=>"August",a=>$mdg['A'][8],p=>$mdg['P'][8],c=>$mdg['C'][8]];
$months_data_info[] = [y=>"September",a=>$mdg['A'][9],p=>$mdg['P'][9],c=>$mdg['C'][9]];
$months_data_info[] = [y=>"October",a=>$mdg['A'][10],p=>$mdg['P'][10],c=>$mdg['C'][10]];
$months_data_info[] = [y=>"November",a=>$mdg['A'][11],p=>$mdg['P'][11],c=>$mdg['C'][11]];
$months_data_info[] = [y=>"December",a=>$mdg['A'][12],p=>$mdg['P'][12],c=>$mdg['C'][12]];

echo json_encode($months_data_info);

