<?php 
/**
 * @class		Purchases
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Purchases {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}	
	
	/**
	 * @method		getPurchasesInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getPurchasesInfo($columns, $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM Purchases";

		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$row_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_info, $info );
	
		return $row_info;
	}
	
	
	/**
	 * @method		getPurchaseItems
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getPurchaseItems($columns, $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM PurchaseItems";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$row_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_info, $info );
	
		return $row_info;
	}
	
	/**
	 * @method		getBasketInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getBasketInfo($columns, $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM Basket";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$row_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_info, $info );
	
		return $row_info;
	}
	
	/**
	 * @method		getPurchasesAndPurchaseItems
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getPurchasesAndPurchaseItems($table_name, $columns, $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM $table_name";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;

		$row_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_info, $info );
	
		return $row_info;
	}
	
	
	/**
	 * @method	delBasketAndPurchaseInfo
	 * @return	array
	 */
	function delBasketAndPurchaseInfo($table_name, $where_info = array(), $info = array()) {
		
		$del_info = "DELETE FROM $table_name";
		if(count($where_info) > 0) {
			$del_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		$res_info = $this->db->getConnection("IRECRUIT")->delete ( $del_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method	insPurchaseInfo
	 * @param	$info
	 */
	function insPurchaseInfo($table_name, $info) {

		$ins_info = $this->db->buildInsertStatement($table_name, $info);

		if($table_name == 'Purchases') $ins_purchase_info = $ins_info["stmt"] . " ON DUPLICATE KEY UPDATE PurchaseDate = NOW()";
		else $ins_purchase_info = $ins_info["stmt"];
		
		$res_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_purchase_info, $ins_info["info"] );
	
		return $res_info;
	}
		
	/**
	 * @method	updRequisitionsInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updPurchaseInfo($table_name, $set_info = array(), $where_info = array(), $info) {
		
		$upd_req_info = $this->db->buildUpdateStatement($table_name, $set_info, $where_info);
		$res_req_info = $this->db->getConnection("IRECRUIT")->update($upd_req_info, $info);
	
		return $res_req_info;
	}
	
	/**
	 * @method     displayPurchases
	 * @return     string
	 */
	public function displayPurchases() {
	    $rtn       =   "";
	    $columns   =   "*, date_format(CCexp,'%m/%Y') AS EXP";
	    $where     =   array("PurchaseDate BETWEEN :PurchaseDateFrom AND :PurchaseDateTo");
	    $params    =   array(
	                       ":PurchaseDateFrom" =>  date ( "Y-m-d", strtotime ( $_POST ['PurchaseDate_From'] ) ),
	                       ":PurchaseDateTo"   =>  date ( "Y-m-d", strtotime ( $_POST ['PurchaseDate_To'] ) )
	                   );
	    if ($_POST ['OrgID'] != "") {
	        $where[]   =   "OrgID = :OrgID";
	        $params[":OrgID"]  =   $_POST['OrgID'];
	    }
	    $PUR_RES   =   $this->getPurchasesInfo($columns, $where, "PurchaseDate DESC", array($params));
	    $PUR       =   $PUR_RES['results'];
	    
	    if (sizeof ( $PUR ) == "0") {
	        $rtn .= "<div style=\"margin:30px 0 0 40px;\">";
	        $rtn .= "There are currently no purchases available.\n";
	        $rtn .= "</div>\n";
	    } else {
	        	
	        // create array for Org Names
	        $where     =   array("MultiOrgID = ''");
	        $ORGS_RES  =   G::Obj('Organizations')->getOrgDataInfo("OrgID, OrganizationName", $where, '', array());
	        $ORGS      =   $ORGS_RES['results'];
	        
	        $ORGNAME   =   array ();
	        foreach ( $ORGS as $ORG ) {
	            $ORGNAME [$ORG ['OrgID']] = $ORG ['OrganizationName'];
	        } // end foreach
	
	        $x=0;
	        foreach ( $PUR as $PURCHASES ) {
	            $x++;
	            	
	            if ($x > 1) {
	                $rtn .= '<br>';
	            }
	            $rtn .= '<br>';
	            $rtn .= "\n\n" . '<div class="table-responsive">';
	            $rtn .= '<table border="0" cellspacing="0" cellpading="3" width="770" class="table table-bordered">' . "\n";
	            $rtn .= '<tr><td>';
	
	            $rtn .= "\n\n" . '<table border="0" cellspacing="0" cellpading="3">' . "\n";
	
	            if (MASTERADMIN == "Y") {
	                $rtn .= '<tr>';
	                $rtn .= '<td align="right" valign="bottom">Customer:</td><td valign="bottom">';
	                $rtn .= $PURCHASES ['OrgID'] . "&nbsp;-&nbsp;" . $ORGNAME [$PURCHASES ['OrgID']];
	                $rtn .= '</td>';
	                $rtn .= '</tr>' . "\n";
	            } // end if
	
	            $rtn .= '<tr>';
	            $rtn .= '<td align="right" valign="bottom">Purchase Number:</td><td valign="bottom">';
	            $rtn .= "<b>" . $PURCHASES ['PurchaseNumber'] . "</b>";
	            $rtn .= '</td>';
	            $rtn .= '</tr>' . "\n";
	
	            if (isset ( $PURCHASES ['OrderNumber'] ) && $PURCHASES ['OrderNumber'] != "") {
	                $rtn .= '<tr>';
	                $rtn .= '<td align="right" valign="bottom">Order Number:</td><td valign="bottom">';
	                $rtn .= "<b>" . $PURCHASES ['OrderNumber'] . "</b>";
	                $rtn .= '</td>';
	                $rtn .= '</tr>' . "\n";
	            }
	
	            if (isset ( $PURCHASES ['TransactionID'] )) {
	                $rtn .= '<tr>';
	                $rtn .= '<td align="right" valign="bottom">TransactionID:</td><td valign="bottom">';
	                $rtn .= "<b>" . $PURCHASES ['TransactionID'] . "</b>";
	                $rtn .= "&nbsp;&nbsp;" . $PURCHASES ['ApprovalCode'] . "</b>";
	                $rtn .= '</td>';
	                $rtn .= '</tr>' . "\n";
	            }
	
	            if (isset ( $PURCHASES ['PostTransactionID'] ) && $PURCHASES ['PostTransactionID'] != "") {
	                $rtn .= '<tr>';
	                $rtn .= '<td align="right" valign="bottom">Post TransactionID:</td><td valign="bottom">';
	                $rtn .= "<b>" . $PURCHASES ['PostTransactionID'] . "</b>";
	                $rtn .= "&nbsp;&nbsp;" . $PURCHASES ['PostApprovalCode'] . "</b>";
	                $rtn .= '</td>';
	                $rtn .= '</tr>' . "\n";
	            }
	
	            $rtn .= '<tr>';
	            $rtn .= '<td align="right">Auth Amount: $</td><td>';
	            $rtn .= number_format ( $PURCHASES ['AuthAmount'], 2, '.', '' );
	            $rtn .= '</td>';
	            $rtn .= '</tr>' . "\n";
	
	            $rtn .= '<tr>';
	            $rtn .= '<td align="right">Purchase Date:</td><td>';
	            $rtn .= $PURCHASES ['PurchaseDate'];
	            $rtn .= '</td>';
	            $rtn .= '</tr>' . "\n";
	
	            $rtn .= '<tr>';
	            $rtn .= '<td align="right" valign="top">Purchaser\'s Name:</td><td valign="top">';
	            $rtn .= $PURCHASES ['FullName'] . "<br>";
	            $rtn .= $PURCHASES ['Address1'] . "<br>";
	            if ($PURCHASES ['Address2'] != "") {
	                $rtn .= $PURCHASES ['Address2'] . "<br>";
	            }
	            $rtn .= $PURCHASES ['City'] . ", ";
	            $rtn .= $PURCHASES ['State'] . "&nbsp;&nbsp;";
	            $rtn .= $PURCHASES ['ZipCode'] . "<br>";
	            $rtn .= $PURCHASES ['Country'] . "<br><br>";
	            $rtn .= '</td>';
	            $rtn .= '</tr>' . "\n";
	
	            $rtn .= '</table>' . "\n";
	
	            $rtn .= '</td><td valign="top">';
	
	            $rtn .= "\n\n" . '<table border="0" cellspacing="0" cellpading="3">' . "\n";
	            if ($PURCHASES ['Email'] != "") {
	                $rtn .= "<tr><td align=\"right\">Email:</td><td>" . $PURCHASES ['Email'] . "</td></tr>\n";
	            }
	            if ($PURCHASES ['Phone'] != "") {
	                $rtn .= "<tr><td align=\"right\">Phone:</td><td>" . $PURCHASES ['Phone'] . "</td></tr>\n";
	            }
	            $rtn .= "<tr><td align=\"right\">Partial Card #:</td><td>" . $PURCHASES ['CCnumber'] . "</td></tr>\n";
	            $rtn .= "<tr><td align=\"right\">Expiration:</td><td>" . $PURCHASES ['EXP'] . "</td></tr>\n";
	            $rtn .= "<tr><td align=\"right\">Placed by:</td><td>" . $PURCHASES ['FullName'] . " (" . $PURCHASES ['UserID'] . ")" . "</td></tr>\n";
	            $rtn .= '</table>' . "\n";
	
	            $rtn .= '</td></tr>' . "\n";
	            $rtn .= '</table>';
	            $rtn .= "</div>\n";
	
	            $params    =   array(':OrgID'=>$PURCHASES['OrgID'], ':PurchaseNumber'=>$PURCHASES['PurchaseNumber']);
	            $where     =   array("OrgID = :OrgID", "PurchaseNumber = :PurchaseNumber");
	            $ITEMS_RES =   $this->getPurchaseItems("*", $where, "LineNo", array($params));
	            $ITEMS     =   $ITEMS_RES['results'];
	
	            foreach ($ITEMS AS $I) {
	
	                $rtn .= "<div style=\"background:#eeeeee;border-radius:3pt;width:400px;padding:10px;margin-left:20px;margin-bottom:2px;\">\n";
	                $rtn .= "Line No: " . $I['LineNo'] . ")&nbsp;&nbsp;";
	                $rtn .= $I['ServiceType'];
	                if ($I['InvoiceNo'] != "") {
	                    $rtn .= "&nbsp;&nbsp;No: " . $I['InvoiceNo'];
	                }
	                $rtn .= "&nbsp;&nbsp;Amount: $" . number_format($I['BudgetTotal'],2,'.','');;
	                $rtn .= "</div>\n";
	            }
	
	        } // end foreach
	    } // end else PUR
	
	    return $rtn;
	} // end function
	
}
?>
