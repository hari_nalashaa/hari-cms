<?php
/**
 * @class		Logger
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 * @tutorial	It is a class to write the warning, error, information messages to a file
 */

class Logger {
	
	private static $file;
	private static $error = false;

	/**
	 * @method     setFilePath
	 * @param      string $file
	 */
	public static function setFilePath($file) {
		if(file_exists($file) && ((filesize($file) / 1024) > 1024)) {
			$dirname = dirname($file);
			$filename = basename($file);
			$newname = $dirname.'/'.date('d.m.Y').'_'.$filename;
			if(!rename($file, $newname)) {
				self::$error = 'Can\'t rename the old log file';
			}
			foreach (glob($dirname.'/*.log') as $logfile) {
				if(filemtime($logfile) < (time() - (30 * 24 * 3600))) {
					unlink($logfile);
				}
			}
			file_put_contents($file, '');
		}
		else if(!file_exists($file)) {

			$path_parts = pathinfo($file);

			if(!is_dir($path_parts['dirname'])) {
				//Directory does not exist, so lets create it.
				mkdir($path_parts['dirname'], 0777);
				chmod($path_parts['dirname'], 0777);
			}
			
			@touch($file);
			@chmod($file, 0777);
		}
		self::$file = $file;
	}
	
	/**
	 * @method     writeMessage
	 * @param      string $file_path
	 * @param      string $message
	 * @param      string $mode
	 * @param      boolean $send_mail
	 * @param      string $mail_subject
	 * @param      string $e
	 */
	public static function writeMessage($file_path, $message, $mode = 'a+', $send_mail = true, $mail_subject = '', $e = '') {
        
		Logger::setFilePath($file_path);
		
		if(trim($message) != "") {
			
			$start_message_time_stamp  = date('m-d-Y h:i:s');
			$message = $start_message_time_stamp." ".$message.PHP_EOL;
			//$message = ""; //To make the file empty

			$fp = @fopen($file_path, $mode);
			if (@fwrite($fp, $message) === FALSE) {
				self::$error = 'Can\'t write to log';
			}
			@fclose($fp);
			
			//Send mail
			if($send_mail === true)
			{
			        $to  =   'David Edgecomb <dedgecomb@irecruit-software.com>';
			    
				if($mail_subject == '') {
					$mail_subject = 'Error information in query';
				}
				
				if($e != '' && isset($e->errorInfo[1]) && $e->errorInfo[1] == 2006) {
				    $mail_subject = 'MySql server has gone away';
		        }
		        else if($e != '' && isset($e->errorInfo[1]) && $e->errorInfo[1] == 1205) {
				    $mail_subject = 'Lock wait timeout exceeded. Try restarting transaction';
		        }
				
				$mail_subject .= ' - ' . $_SERVER['SERVER_NAME'];
					
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					
				// Additional headers
				$headers .= 'From: iRecruit <info@irecruit-us.com>' . "\r\n";
				
				//Stop Sending Mails - When the file is triggered from cron folder
				if (!preg_match ( '/cron/', __FILE__ )) {
				    // Mail it
				    //@mail($to, $mail_subject, $message, $headers);
				}
			}
			
		}
		
	}

	/**
	 * @method     writeMessageToDb
	 * @param      string $OrgID
	 * @param      string $LogType
	 * @param      string $LogInfo
	 */
	public static function writeMessageToDb($OrgID, $LogType, $LogInfo) {
	    
	    $dbhost     = "cmsdev.pairserver.com";
	    $dbname     = "cmsdev_irecruitapp";
	    $dbusername = "cmsdev";
	    $dbpassword = "mpqkjhEY";
	    
	    $db_conn_link = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbusername, $dbpassword);
	    
	    $ins_stmt   =   "INSERT INTO ProductionLogs(OrgID, ClientIP, ServerEnvironment, LogType, LogInfo, RequestInfo, GetInfo, ServerInfo, CookieInfo, SessionInfo, LastModified)";
	    $ins_stmt  .=   " VALUES(:OrgID, :ClientIP, :ServerEnvironment, :LogType, :LogInfo, :RequestInfo, :GetInfo, :ServerInfo, :CookieInfo, :SessionInfo, NOW())";
	    
	    
	    $ClientIP       =   G::Obj('ServerInformation')->getIPAddress();
	    
	    $RequestInfo    =   json_encode($_REQUEST);
	    $GetInfo        =   json_encode($_GET);
	    $ServerInfo     =   json_encode($_SERVER);
	    $CookieInfo     =   json_encode($_COOKIE);
	    $SessionInfo    =   json_encode($_SESSION);

	    
	    try
	    {
	        $statement  =   $db_conn_link->prepare($ins_stmt);
	        $statement->bindParam(':OrgID',                $OrgID,                     PDO::PARAM_STR);
	        $statement->bindParam(':ClientIP',             $ClientIP,                  PDO::PARAM_STR);
	        $statement->bindParam(':ServerEnvironment',    $_SERVER['SERVER_NAME'],    PDO::PARAM_STR);
	        $statement->bindParam(':LogType',              $LogType,                   PDO::PARAM_STR);
	        $statement->bindParam(':LogInfo',              $LogInfo,                   PDO::PARAM_STR);
	        $statement->bindParam(':RequestInfo',          $RequestInfo,               PDO::PARAM_STR);
	        $statement->bindParam(':GetInfo',              $GetInfo,                   PDO::PARAM_STR);
	        $statement->bindParam(':ServerInfo',           $ServerInfo,                PDO::PARAM_STR);
	        $statement->bindParam(':CookieInfo',           $CookieInfo,                PDO::PARAM_STR);
	        $statement->bindParam(':SessionInfo',          $SessionInfo,               PDO::PARAM_STR);
	         
	        $statement->execute();
	    }
	    catch(PDOException $e)
	    {
	            $to    =   'David Edgecomb <dedgecomb@irecruit-software.com>';
			
			if($mail_subject == '') {
				$mail_subject = 'Error while writing the log to DB';
			}
			
			$mail_subject .= ' - ' . $_SERVER['SERVER_NAME'];
				
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
			// Additional headers
			$headers .= 'From: iRecruit <info@irecruit-us.com>' . "\r\n";
				
			//Stop Sending Mails - When the file is triggered from cron folder
			if (!preg_match ( '/cron/', __FILE__ )) {
			    // Mail it
			    //@mail($to, $mail_subject, $message, $headers);
			}
	    }
	}
	
	/**
	 * @method     isError
	 * @return     boolean
	 */
	public static function isError() {
		if(self::$error != false) {
			return true;
		}
		return false;
	}
    
	/**
	 * @method     getError
	 * @return     boolean|string
	 */
	public static function getError() {
		return self::$error;
	}
}
