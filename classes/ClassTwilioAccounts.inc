<?php
/**
 * @class		TwilioAccounts
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioAccounts {

    public $db;

	var $conn_string       		=   "IRECRUIT";

	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		// Create a new instance of a SOAP 1.2 client
		$this->db	=	Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	} // end function
	
	/**
	 * @method		insUpdateTwilioAccount
	 * @return		array
	 */
	function insUpdateTwilioAccount($info, $skip = array()) {
    
        $ins_twilio_account	= $this->db->buildOnDuplicateKeyUpdateStatement("TwilioAccounts", $info, $skip);
        $res_twilio_account	= $this->db->getConnection ( $this->conn_string )->insert ( $ins_twilio_account["stmt"], $ins_twilio_account["info"] );
        
        return $res_twilio_account;
	}

	/**
	 * @method		inactivateAccountByOrgID
	 * @return		array
	 */
	function inactivateAccountByOrgID($OrgID) {
	
		$params				=	array(":OrgID"=>$OrgID);
		$upd_msg_service_id	=	"UPDATE TwilioAccounts SET Active = 'N' WHERE OrgID = :OrgID";
		$res_msg_service_id	=	$this->db->getConnection ( $this->conn_string )->update ( $upd_msg_service_id, array($params) );
	
		return $res_msg_service_id;
	}

	/**
	 * @method		updTwilioMessageServiceID
	 * @return		array
	 */
	function updTwilioMessageServiceID($OrgID, $message_service_id) {
	
		$params				=	array(":OrgID"=>$OrgID, ":MessageServiceID"=>$message_service_id);
		$upd_msg_service_id	=	"UPDATE TwilioAccounts SET MessageServiceID = :MessageServiceID WHERE OrgID = :OrgID";
		$res_msg_service_id	=	$this->db->getConnection ( $this->conn_string )->update ( $upd_msg_service_id, array($params) );
	
		return $res_msg_service_id;
	}
	
	/**
	 * @method		getTwilioAccountsList
	 * @return		array
	 */
	function getTwilioAccountsList($order_by = '') {

		$sel_info	=	"SELECT * FROM TwilioAccounts WHERE MessageServiceID != 'default' AND Active = 'Y'";
		if($order_by == "") {
		    $sel_info	.=  " ORDER BY getOrgnizationName(OrgID, '') ASC";
		}
		else if($order_by != "") {
		    $sel_info	.=  " ORDER BY " . $order_by;
		}
		
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info );

		return $res_info;
	}
	
	/**
	 * @method		getTwilioAccountInfo
	 * @return		array
	 */
	function getTwilioAccountInfo($OrgID) {
		
		$params		=	array(":OrgID"=>$OrgID);
		$sel_info	=	"SELECT * FROM TwilioAccounts WHERE OrgID = :OrgID AND Active = 'Y'";
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
		
		return $res_info;
	}

	/**
	 * @method		getTwilioAccountUseageReport
	 * @return		html
	 */
	function getTwilioAccountUseageReport($OrgID = '') {

		$rtn="";
		$CONV = array();
  		$MESSAGES = array();
		$expire_days="30";
		$total_conversations=0;

		$accountinfo 	      =       $this->getTwilioAccountInfo($OrgID);
  		$conversations        =       G::Obj('TwilioConversationApi')->fetchAllConversationResources($OrgID);

    		if (count($conversations['Response']) > 0) {

    		  $i=0;

    		  foreach ($conversations['Response'] AS $C) {
            	  $i++;

            	  $ResourceID =   $C->sid;

    		  $query = "select concat(Users.LastName, ', ', Users.FirstName) as Sender, TwilioConversations.MobileNumbers";
    		  $query .= " from Users join TwilioConversations ON";
    		  $query .= " Users.OrgID = TwilioConversations.OrgID and Users.UserID = TwilioConversations.UserID";
    		  $query .= " where TwilioConversations.OrgID = :OrgID";
    		  $query .= " and TwilioConversations.ResourceID = :ResourceID";
    		  $params     =   array(':OrgID'    =>  $OrgID, ':ResourceID'    =>  $ResourceID);
    		  $CONVERSATION =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

    		  $Sender = $CONVERSATION[0]['Sender'];
    		  $Numbers = json_decode($CONVERSATION[0]['MobileNumbers']);
    		  $SNumber = implode("%",str_split(substr($Numbers[0][1],-10),3));

    		  $query = "select JobApplications.ApplicantSortName as Receiver, JobApplications.ApplicationID";
    		  $query .= " from JobApplications";
    		  $query .= " join ApplicantData on ApplicantData.OrgID = JobApplications.OrgID";
    		  $query .= " and ApplicantData.ApplicationID = JobApplications.ApplicationID";
    		  $query .= " where JobApplications.OrgID = :OrgID";
    		  $query .= " and ApplicantData.QuestionID = 'cellphone'";
    		  $query .= " and ApplicantData.Answer like :Phone";
    		  $params     =   array(':OrgID'    =>  $OrgID, ':Phone'    =>  '%'.$SNumber.'%');
    		  $Receiver =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

		  $CONV[$i]['Sender'] = $Sender;
		  $CONV[$i]['Receiver'] = $Receiver[0]['Receiver'];
		  $CONV[$i]['ApplicationID'] = $Receiver[0]['ApplicationID'];

    		  $messages =   G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $ResourceID);

            	  $MESSAGES = array();
            	  $conversation_messages = array();
            	  $conversation_msg_skip_fields = array();

            	  $ii=0;
            	  foreach ($messages['Response'] AS $M) {

                  if(is_object($M)) {
                    $reflection     =   new ReflectionClass($M);
                    $property       =   $reflection->getProperty("properties");
                    $property->setAccessible(true);
                    $MESSAGES[]     =   $msg_info  = $property->getValue($M);

                    $message_info   =   array();

                    $date_created_info = (array)$MESSAGES[$ii]['dateCreated'];
                    $date_updated_info = (array)$MESSAGES[$ii]['dateUpdated'];

                    $message_info['ConversationResourceID']     =   $msg_info['conversationSid'];
                    $message_info['ConversationMessageID']      =   $msg_info['sid'];
                    $message_info['Author']                     =   $msg_info['author'];
                    $message_info['Body']                       =   utf8_decode($msg_info['body']);
                    $message_info['DateCreatedDate']            =   $date_created_info['date'];
                    $message_info['DateCreatedTimeZone_Type']   =   $date_created_info['timezone_type'];
                    $message_info['DateCreatedTimeZone']        =   $date_created_info['timezone'];
                    $message_info['DateUpdatedDate']            =   $date_updated_info['date'];
                    $message_info['DateUpdatedTimeZone_Type']   =   $date_updated_info['timezone_type'];
                    $message_info['DateUpdatedTimeZone']        =   $date_updated_info['timezone'];
                    $message_info['CreatedDateTime']            =   "NOW()";

                    //Assign Data To Arrays
                    $conversation_messages[$msg_info['sid']]           =   $message_info;

                    //Skip fields
                    $skip_fields    =   array_keys($message_info);
                    array_pop($skip_fields);

                    $conversation_msg_skip_fields[$msg_info['sid']]    =   $skip_fields;

                  } // end is object

            	  $ii++;
            	  } // end foreach Message

		  $CONV[$i]['TotalMessages'] = count($MESSAGES);

            	  $sort_date = array_column($MESSAGES, 'dateCreated');
            	  array_multisort($sort_date, SORT_DESC, $MESSAGES);

                    $date_info = (array)$MESSAGES[0]['dateCreated'];

                    $earlier = new DateTime(date("Y-m-d", strtotime($date_info['date'])));
                    $later = new DateTime(date("Y-m-d"));
                    $days           =   $later->diff($earlier)->format("%a");
		    $expires = $expire_days - $days;
		    $CONV[$i]['LastCommunication'] = $days;
		    $CONV[$i]['Expires'] = $expires;

    		  } // end foreach Conversation

		  $total_conversations=$i;

  		}  // end if conversations > 0

		$rtn .= '<div style="font-size:14pt;font-weight:bold;margin-bottom:5px;border-bottom:1px solid #000;">';
		$rtn .= "Active texting less than " . $expire_days . " days:";
		$rtn .= "&nbsp;&nbsp;<span style=\"font-size:12pt;font-weight:normal;font-style:italic;\"><strong>";
		if ($accountinfo['Licenses'] > 0 ) {
		  $rtn .= "Licenses: " . $accountinfo['Licenses'];
                  $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;Conversations: " . $total_conversations;
		} else {
		  $rtn .= "Credits: " . $accountinfo['Credits'];
		  $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;Used: " . $total_conversations;
		}
	        $rtn .= "</strong></span>";
		$rtn .= "</span>";
		$rtn .= "</div>";

		if ($total_conversations > 0) {

		$rtn .= '<table style="border-spacing:5px 10px 5px 10px;">';

		$rtn .= "<tr style=\"border:1px solid #000;background-color:teal;\">";
	        $rtn .= "<th style=\"color:#fff;font-weight:bold;width:200px;padding:5px\">From</th>";
		$rtn .= "<th style=\"color:#fff;font-weight:bold;width:200px;padding:5px;\">To</th>";
		$rtn .= "<th style=\"color:#fff;font-weight:bold;width:95px;padding:5px;\">ApplicationID</th>";
		$rtn .= "<th style=\"color:#fff;font-weight:bold;width:80px;padding:5px;\">Messages</th>";
		$rtn .= "<th style=\"color:#fff;font-weight:bold;width:80px;padding:5px;\">Last Communication</th>";
		$rtn .= "<th style=\"color:#fff;font-weight:bold;width:80px;padding:5px;\">Credit</th>";
		$rtn .= "<th style=\"color:#fff;font-weight:bold;width:80px;padding:5px;\">Expires</th>";
		$rtn .= "</tr>";

            	$sort = array_column($CONV, 'Expires');
            	array_multisort($sort, SORT_ASC, $CONV);

		$i=0;
		foreach ($CONV AS $C) {
		$i++;

		if ($background == "#ffffff") { $background = "#eeeeee"; } else { $background = "#ffffff"; }
		if ($C['LastCommunication'] > 1) { $lc = "s"; } else { $lc = ""; }
		if ($C['Expires'] > 1) { $e = "s"; } else { $e = ""; }

		$rtn .= "<tr>";
	        $rtn .= "<td style=\"font-weight:normal;background-color:". $background . ";padding:3px 0;\">" . $C['Sender'] . "</td>";
		$rtn .= "<td style=\"font-weight:normal;background-color:". $background . ";padding:3px 0;\">" . $C['Receiver'] . "</td>";
		$rtn .= "<td style=\"font-weight:normal;background-color:". $background . ";padding:3px 0;\">" . $C['ApplicationID'] . "</td>";
		$rtn .= "<td style=\"font-weight:normal;text-align:center;background-color:". $background . ";padding:3px 0;padding:3px 0;\">" . $C['TotalMessages'] . "</td>";
		$rtn .= "<td style=\"font-weight:normal;text-align:center;background-color:". $background . ";padding:3px 0;\">" . $C['LastCommunication'] . " day" . $lc . "</td>";
		$rtn .= "<td style=\"font-weight:normal;text-align:center;background-color:". $background . ";\">1</td>";
		$rtn .= "<td style=\"font-weight:normal;text-align:center;background-color:". $background . ";padding:3px 0;\">" . $C['Expires'] . " day" . $e . "</td>";
		$rtn .= "</tr>";

		} // end foreach $CONV

		$rtn .= "</table>";

		} else { // total conversations

		  $rtn .= "<div style=\"font-size:10pt;padding:5px;\">There currently are no active conversations.</div>";
		}


		return $rtn;
	}

	/**
	 * @method		getTwilioAccountUseageReport
	 * @return		html
	 */
	function getTwilioAccountCostInformation($OrgID = '') {

		$segments = array(
                            "sms-inbound"=>".0075",
                            "sms-outbound"=>".0075",
                            "mms-inbound"=>".01",
                            "mms-outbound"=>".02"
		);

		$rtn="<div style=\"width:650px;font-size:12pt;padding:10px;background-color:LightSeaGreen;border:1px solid #000;border-radius:15px;margin-top:20px;\">";

		$account_phone_numbers  =     G::Obj('TwilioPhoneNumbersApi')->getAccountIncomingNumbers($OrgID);
  		$phone_numbers_info     =     $account_phone_numbers['PhoneNumbersInfo'];
  		$phone_numbers_cnt      =     count($phone_numbers_info);


  		if ($phone_numbers_cnt > 0) {
			$rtn .= "Phone Number Cost: " . $phone_numbers_cnt . " numbers, <strong>$" . number_format($phone_numbers_cnt,2 ,'.', ',') . "</strong>";
			$rtn .= "<br>";
  		} else {
  		  $rtn .= "Account not in use.<br>";
  		}



		$records_info = array();

    		$last_month_records_info    =       G::Obj('TwilioUsageRecordsApi')->getLastMonthUsageForAllCategories($OrgID);
    		$last_month_records                 =       $last_month_records_info['Response'];


    		for($c = 0; $c < count($last_month_records); $c++) {

    		    $last_month_obj =       $last_month_records[$c];

    		    //Get the Protected properties from SMS object, by extending the object through ReflectionClass
    		    $reflection     =   new ReflectionClass($last_month_obj);
    		    $property       =   $reflection->getProperty("properties");
    		    $property->setAccessible(true);
    		    $records_info[] =   $property->getValue($last_month_obj);
    		}


    		$total_segments = 0;
    		$total_messages = 0;

    		if(count($records_info) > 0) {
    		  for($r = 0; $r < count($records_info); $r++) {
    		    if(in_array($records_info[$r]['category'], array_keys($segments))) {
    		        $total_segments += $records_info[$r]['usage'];
    		        $total_messages += $records_info[$r]['count'];
    		    }
    		  }
    		}

    		$total_costs = 0;
    		$breakdown_costs = "";
    		if(count($records_info) > 0) {
    		    for($r = 0; $r < count($records_info); $r++) {
    		        if(in_array($records_info[$r]['category'], array_keys($segments))) {
    		            $breakdown_costs .= $records_info[$r]['category'] . ": ";
    		            $breakdown_costs .= $records_info[$r]['count'] . " " . $records_info[$r]['countUnit'] . ", ";
   		            $breakdown_costs .= $records_info[$r]['usage'] . " segments, ";
    		            $breakdown_costs .= "$" . number_format($records_info[$r]['usage'] * $segments[$records_info[$r]['category']], 2, '.', ',');
    		            $total_costs += number_format($records_info[$r]['usage'] * $segments[$records_info[$r]['category']], 2, '.', ',');
    		            $breakdown_costs .= "<br>";
    		        }
    		    }
    		}
    		$rtn .= "Previous Months Segment Cost: " . $total_messages.' messages, ' . $total_segments.' segments, <strong>$' . $total_costs . "</strong><br>";
		$rtn .= "Total Cost: <strong>$" . number_format($total_costs + $phone_numbers_cnt,2 ,'.', ',') . "</strong>";
    		//$rtn .= $breakdown_costs . "<br>";;
		$accountinfo 	      =       $this->getTwilioAccountInfo($OrgID);

		$rtn .= "<br><br>";
		$billing =   number_format((($accountinfo['Licenses'] - 1) * 20) + 59, 2, '.', ',');
		$rtn .= "<strong>Current</strong><br>Licenses: " . $accountinfo['Licenses'] . "&nbsp;&nbsp;&nbsp;Bill: $" . $billing . "</strong>";
		if ($accountinfo['Credits'] > 0 ) {
		  $rtn .= "<br><br>";
		  $rtn .= "<strong>New</strong><br>Credits: " . $accountinfo['Credits'] . "&nbsp;&nbsp;&nbsp;Bill: $" . $accountinfo['BillAmt'] . "</strong>";
		}



		$rtn .= "</div>";


		return $rtn;
	}
	
}
