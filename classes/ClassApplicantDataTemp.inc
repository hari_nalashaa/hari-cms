<?php
/**
 * @class		ApplicantDataTemp
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantDataTemp {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method 		getApplicantDataTempInfo
	 * @param 		string $OrgID
	 * @param 		string $HoldID
	 * @return 		array
	 */
	function getApplicantDataTempInfo($OrgID, $HoldID) {
	
	    // Set parameters for prepared query
	    $params = array (":OrgID" => $OrgID, ":HoldID" => $HoldID);
	    $sel_applicant_data = "SELECT * FROM ApplicantDataTemp WHERE OrgID = :OrgID AND HoldID = :HoldID";
	    $res_applicant_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, array ($params) );
	
	    return $res_applicant_data;
	}
	
	/**
	 * @method		getApplicantDataTempQueInfo
	 * @param		string $OrgID, $MultiOrgID, $RequestID
	 * @return		string
	 */
	function getApplicantDataTempQueInfo($OrgID, $HoldID, $QuestionID) {
	    
	    //Set parameters for prepared query
	    $params     = array(":OrgID"=>$OrgID, ":HoldID"=>$HoldID, ":QuestionID"=>$QuestionID);
	    $sel_info   = "SELECT Answer FROM ApplicantDataTemp WHERE OrgID = :OrgID AND HoldID = :HoldID AND QuestionID = :QuestionID";
	    $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
	    
	    return $res_info['Answer'];
	} // end function
	
	/**
	 * @method		insApplicantDataTemp
	 * @param		array $records_list
	 * @tutorial	Here $records_list contains set of rows data that have to insert
	 *           	All the values will be passed as in documentation
	 */
	function insApplicantDataTemp($info) {

	    if ($info['HoldID'] != '') {

	    if($info['Required'] == 'Y') $info['Required'] = 'REQUIRED';
	    
	    $params            =   array(
                                    "OrgID"                 =>  $info['OrgID'],
                                    "HoldID"                =>  $info['HoldID'],
                                    "QuestionID"            =>  $info['QuestionID'],
                                    "SectionID"             =>  $info['SectionID'],
                                    "Answer"                =>  $info['Answer'],
                                    "Required"              =>  $info['Required'],
                                    "LastModifiedDateTime"  =>  "NOW()",
                                );
        $skip               =   array("OrgID", "HoldID", "QuestionID");
        $ins_app_data_temp	=   $this->db->buildOnDuplicateKeyUpdateStatement("ApplicantDataTemp", $params, $skip);
	    $res_app_data_temp	=   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data_temp["stmt"], $ins_app_data_temp["info"] );
        
        return $res_app_data_temp;
	} // end if HoldID

	}
	
	/**
	 * @method		insUpdApplicantDataTemp
	 */
	function insUpdApplicantDataTemp($insert_info, $on_update = '', $update_info = array()) {
	
	    // Set parameters for prepared query
	    $ins_form_data = $this->db->buildInsertStatement ( 'ApplicantDataTemp', $insert_info );
	
	    $insert_statement = $ins_form_data ["stmt"];
	    if ($on_update != '') {
	        $insert_statement .= $on_update;
	        if (is_array ( $update_info )) {
	            foreach ( $update_info as $upd_key => $upd_value ) {
	                $ins_form_data ["info"][0][$upd_key] = $upd_value;
	            }
	        }
	    }
	
	    $res_form_data = $this->db->getConnection ( $this->conn_string )->insert ( $insert_statement, $ins_form_data ["info"] );
	    return $res_form_data;
	}
	
	/**
	 * @method		updApplicantDataTemp
	 * @param		string $OrgID
	 * @param		string $HoldID
	 */
	function updApplicantDataTemp($OrgID, $HoldID, $QuestionID, $Answer) {

	    if ($HoldID != '') {
	
	    // Set parameters for prepared query
	    $params            =   array(
                                    ":OrgID"        => $OrgID,
                                    ":HoldID"       => $HoldID,
                                    ":QuestionID"   => $QuestionID,
                                    ":Answer"       => $Answer
                               );
	    $upd_app_data_temp =   "UPDATE ApplicantDataTemp SET Answer = :Answer, LastModifiedDateTime = NOW() WHERE OrgID = :OrgID AND HoldID = :HoldID AND QuestionID = :QuestionID";
	    $res_app_data_temp =   $this->db->getConnection ( $this->conn_string )->update ( $upd_app_data_temp, array ($params) );
	
	    return $res_app_data_temp;

	    } // end HoldID
	}
	
	/**
	 * @method		delApplicantDataTemp
	 * @param		string $OrgID
	 * @param		string $HoldID
	 */
	function delApplicantDataTemp($OrgID, $HoldID) {
	
	    // Set parameters for prepared query
	    $params    =   array (":OrgID" => $OrgID, ":HoldID" => $HoldID);
	    $del_app_data_temp = "DELETE FROM ApplicantDataTemp WHERE OrgID = :OrgID AND HoldID = :HoldID";
	    $res_app_data_temp = $this->db->getConnection ( $this->conn_string )->delete ( $del_app_data_temp, array ($params) );
	}
	
	/**
	 * @method		validateDefInputUpdApplicantDataTemp
	 * @param		$field
	 * @param		$value
	 */
	function validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $field, $value) {
	
	    // Set parameters for prepared query
	    $params = array(":OrgID" => $OrgID, ":HoldID" => $HoldID, ":QuestionID" => $field, ":Answer" => $value, ":UAnswer" => $value);
	    $ins_applicant_data_temp = "INSERT INTO ApplicantDataTemp(OrgID, HoldID, QuestionID, Answer, Required, LastModifiedDateTime)";
		$ins_applicant_data_temp .= " VALUES (:OrgID, :HoldID, :QuestionID, :Answer, 'REQUIRED', NOW())";
		$ins_applicant_data_temp .= " ON DUPLICATE KEY UPDATE Answer = :UAnswer, Required = 'REQUIRED'";
	    $this->db->getConnection ( $this->conn_string )->insert ( $ins_applicant_data_temp, array ($params) );
	
	} // end function
}
