<?php
/**
 * @class		Validate
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Validate {
	
	/**
	 * @method		validate_email_address
	 * @param		string $email
	 * @return		string
	 */
	function validate_email_address($email) {
		$ERROR = "";

		if (! preg_match ( "/^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/", $email )) {
		    $ERROR = "Email addess is not formatted correctly." . "\\n";
		}

		return $ERROR;
	} // end function
	
	/**
	 * @method		validate_verification
	 * @param		string $email
	 * @return		string
	 */
	function validate_verification($verification, $verification2, $compare) {
		$ERROR = "";
		
		if (! preg_match ( "/^.*(?=.{8,}).*$/", $verification )) {
			$ERROR .= "Passwords need to be at least 8 characters." . "\\n";
		}
		if (! preg_match ( "/^.*(?=.*[A-Z]).*$/", $verification )) {
			$ERROR .= "Passwords must contain one upper case letter." . "\\n";
		}
		if (! preg_match ( "/^.*(?=.*\d).*$/", $verification )) {
			$ERROR .= "Passwords must contain one digit." . "\\n";
		}
		
		if ($compare == "Y") {
			if ($verification != $verification2) {
				$ERROR .= "Passwords entered Don\'t match" . "\\n";
			}
		}
		
		return $ERROR;
	}
	
	/**
	 * @method		validate_userid
	 * @param		string $email
	 * @return		string
	 */
	function validate_userid($userid) {
		$ERROR = "";
		
		if (! preg_match ( "/^.*(?=.{6,}).*$/", $userid )) {
			$ERROR .= "User names need to be at least 6 characters." . "\\n";
		}
		
		if (preg_match ( "/\s/", $userid )) {
			$ERROR .= "User names cannot contain spaces." . "\\n";
		}
		
		return $ERROR;
	}
	
	/**
	 * @method		validate_name
	 * @param		string $name
	 * @return		string
	 */
	function validate_name($name, $name_type = "Name") {
	    $ERROR = "";
	
	    if (! preg_match ( "/^.*(?=.{1,}).*$/", $name )) {
	        $ERROR .= "$name_type Name is missing information." . "\\n";
	    }
	
	    return $ERROR;
	}
	
	/**
	 * @method escape
	 * @param  $input
	 * @return string
	 */
	public function escape($input) {
		return addcslashes($input, "\x00, \n, \r, \x5C, \x27, \x22, \x1A, \x2D");
	}
}
