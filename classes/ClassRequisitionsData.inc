<?php
/**
 * @class		RequisitionsData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection, by default connection is based on 
 * 				constructor identifer that we are using
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionsData {

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	/**
	 * @method		insRequisitionsData
	 * @param		array
	 */
	function insRequisitionsData($info) {
	
		$ins_requisitions_data = "INSERT INTO RequisitionsData (OrgID, RequestID, QuestionID, QuestionOrder, Question, QuestionTypeID, Answer, AnswerStatus) VALUES(:OrgID, :RequestID, :QuestionID, :QuestionOrder, :Question, :QuestionTypeID, :Answer, :AnswerStatus)";
		$res_requisitions_data = $this->db->getConnection ( $this->conn_string )->insert ( $ins_requisitions_data, $info );
	
		return $res_requisitions_data;
	}
	
	/**
	 * @method		insUpdRequisitionsData
	 * @param		array
	 */
	function insUpdRequisitionsData($info, $skip_fields = array()) {
	
		$ins_upd_req_data	=	$this->db->buildOnDuplicateKeyUpdateStatement("RequisitionsData", $info, $skip_fields);
		$res_upd_req_data	=	$this->db->getConnection ( $this->conn_string )->insert (  $ins_upd_req_data["stmt"], $ins_upd_req_data["info"] );
	
		return $res_upd_req_data;
	}
	
	/**
	 * @method		delRequisitionsData
	 * @param		array
	 */
	function delRequisitionsData($OrgID, $RequestID) {
	
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		$del_requisitions_data = "DELETE FROM RequisitionsData WHERE OrgID = :OrgID AND RequestID = :RequestID";
		$res_requisitions_data = $this->db->getConnection ( $this->conn_string )->delete ( $del_requisitions_data, array($params) );
	
		return $res_requisitions_data;
	}
	
	/**
	 * @method	 	getRequisitionsData
	 * @param 		$OrgID, $RequestID
	 * @return 		associative array
	 */
	function getRequisitionsData($OrgID, $RequestID) {
	
		// Set parameters for prepared query
		$params = array (":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	
		$sel_requisitions_data    =   "SELECT QuestionID, Answer FROM RequisitionsData WHERE OrgID = :OrgID AND RequestID = :RequestID";
		$res_requisitions_data    =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_requisitions_data, array ($params) );
	
		$requisitions_info_count  =   $res_requisitions_data ['count'];
		$requisitions_info        =   $res_requisitions_data ['results'];
	
		//Please don't rename $APPDATA, just to avoid further confusion to sync with DisplayQuestions,
		//same name has been used here also
		$APPDATA = array();
		for($i = 0; $i < $requisitions_info_count; $i ++) {
			$APPDATA [$requisitions_info [$i] ['QuestionID']] = $requisitions_info [$i] ['Answer'];
		}
	
		return $APPDATA;
	} // end function

	/**
	 * @method		getRequisitionsDataByQuestionID
	 * @param		string $OrgID, $RequestID, $QuestionID
	 * @return		array
	 */
	function getRequisitionsDataByQuestionID($OrgID, $RequestID, $QuestionID) {
	
		//Set parameters for prepared query
		$params			=	array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$QuestionID);
		$sel_request	=	"SELECT * FROM RequisitionsData WHERE OrgID = :OrgID AND RequestID = :RequestID AND QuestionID = :QuestionID";
		$res_request 	=	$this->db->getConnection( $this->conn_string )->fetchAssoc($sel_request, array($params));
	
		return $res_request;
	} // end function
		
}
?>
