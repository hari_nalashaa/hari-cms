<?php
/**
 * @class		AgreementFormData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class AgreementFormData {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	
	/**
	 * @method		getAgreementFormData
	 */
	function getAgreementFormData($OrgID, $AgreementFormID, $RequestID, $ApplicationID) {
	
	    $params_info    =  array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	    $sel_form_data  =  "SELECT * FROM AgreementFormData WHERE OrgID = :OrgID AND AgreementFormID = :AgreementFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID";
	    $res_form_data  =  $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
	
	    return $res_form_data;
	}
	
    
	/**
	 * @method		getAgreementFormDataApplicationIDs
	 * @tutorial    It is a temporary function, we will have to remove it after we have done the upgrade
	 */
	function getAgreementFormDataApplicationIDs($OrgID) {
	
	    $params_info   =   array(":OrgID"=>$OrgID);
	    $sel_form_data =   "SELECT ApplicationID, RequestID FROM AgreementFormData WHERE OrgID = :OrgID GROUP BY ApplicationID, RequestID";
	    $res_form_data =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
	    $form_data_res =   $res_form_data['results'];
	    $form_data_cnt =   $res_form_data['count'];
	     
	    $app_ids       =   array();
	    for($fd = 0; $fd < $form_data_cnt; $fd++) {
	        $app_ids[$fd]['ApplicationID'] =   $form_data_res[$fd]['ApplicationID'];
	        $app_ids[$fd]['RequestID']     =   $form_data_res[$fd]['RequestID'];
	    }
	     
	    return $app_ids;
	}
	
	
    /**
	 * @method		insUpdAgreementFormData
	 */
	function insUpdAgreementFormData($QI) {

		$params_info    = array(":OrgID"=>$QI['OrgID'], ":ApplicationID"=>$QI['ApplicationID'], ":RequestID"=>$QI['RequestID'], ":AgreementFormID"=>$QI['AgreementFormID'], ":QuestionID"=>$QI['QuestionID'], ":Question"=>$QI['Question'], ":QuestionOrder"=>$QI['QuestionOrder'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":value"=>$QI['value'], ":AnswerStatus"=>1, ":IAnswer"=>$QI['Answer'], ":UQuestionTypeID"=>$QI['QuestionTypeID'], ":UQuestion"=>$QI['Question'], ":UAnswer"=>$QI['Answer'], ":Uvalue"=>$QI['value']);
		$ins_form_data  = "INSERT INTO AgreementFormData(OrgID, ApplicationID, RequestID, AgreementFormID, QuestionID, Question, QuestionOrder, QuestionTypeID, value, AnswerStatus, Answer)";
		$ins_form_data .= " VALUES(:OrgID, :ApplicationID, :RequestID, :AgreementFormID, :QuestionID, :Question, :QuestionOrder, :QuestionTypeID, :value, :AnswerStatus, :IAnswer)";
		$ins_form_data .= " ON DUPLICATE KEY UPDATE Answer = :UAnswer, AnswerStatus = 1, QuestionTypeID = :UQuestionTypeID, value = :Uvalue, Question = :UQuestion";
		$res_form_data  = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_data, array($params_info) );
		
		return $res_form_data;
	}
	

	/**
	 * @method     delApplicantAgreementFormData
	 * @param      string $OrgID
	 * @param      string $AgreementFormID
	 * @param      string $RequestID
	 * @param      string $ApplicationID
	 * @return     array
	 */
	function delApplicantAgreementFormData($OrgID, $AgreementFormID, $RequestID, $ApplicationID) {
	     
	    if($OrgID != "" 
	        && $AgreementFormID != ""
            && $RequestID != ""
            && $ApplicationID != "") {
	        
	        // Set parameters for prepared query
	        $params_info        =   array (":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	        $del_agr_form_data  =   "DELETE FROM AgreementFormData WHERE OrgID = :OrgID AND AgreementFormID = :AgreementFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID";
	        $res_agr_form_data  =   $this->db->getConnection ( $this->conn_string )->delete ( $del_agr_form_data, array ($params_info) );

	        return $res_agr_form_data;
	    }

	    return "";
	}
}
