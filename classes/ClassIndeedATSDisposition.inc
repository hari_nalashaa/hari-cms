<?php
/**
 * @class		IndeedATSDisposition
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class IndeedATSDisposition {
	
    var $conn_string        =   "IRECRUIT";
    var $APIKey             =   "76700e1a7f034fe1b27b5955bf4a4019";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	
	/**
	 * @method     getPreSignedUrl
	 * @return     string
	 */
	function getPreSignedUrl($data) {
	    $data_string = json_encode($data);

	    $ch = curl_init("https://indeed-atsdi.com/api/get_upload_url");
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                        	    'Content-Type: application/json',
                                        	    'token:'.$this->APIKey,
                                        	    'Accept: application/json',
                                        	    'Content-Length: ' . strlen($data_string))
                                    	    );
	    $result = curl_exec($ch);
	    
	    if(curl_errno($ch))
	    {
	        $result = 'Curl error: ' . curl_error($ch);
	    }

	    curl_close($ch);
	    
	    return $result;
	}
	
	
	/**
	 * @method     postIndeedDispositionData
	 * @param      string  $url
	 * @param      mixed   $file_data
	 */
	function postIndeedDispositionData($file_name, $pre_signed_url, $file_path, $params) {

        $fh         =   fopen($file_path, 'rb');
	    
        // for testing
        // CURLOPT_VERBOSE           =>  1,
        $options    =   array(
                    		CURLOPT_POST              =>  1,
                    		CURLOPT_RETURNTRANSFER    =>  1,
                    		CURLOPT_URL               =>  $pre_signed_url,
                    		CURLOPT_INFILE            =>  $fh,
                    		CURLOPT_INFILESIZE        =>  filesize($file_path),
                    		CURLOPT_PUT               =>  1
                    	);
        $ch         =   curl_init();
    	curl_setopt_array($ch, $options);
    
    
        $result     =   curl_exec($ch);
        $info       =   curl_getinfo($ch);
        
        if(curl_errno($ch))
        {
            $info = 'Curl error: ' . curl_error($ch);
        }

        fclose($fh);
        curl_close($ch);
        
        return $info;

	}
}
