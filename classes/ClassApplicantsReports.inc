<?php
/**
 * @class		ApplicantsReports
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantsReports {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getApplicantsCountByMonthAndYear
     * @param		string $OrgID
     * @return		string
     */
    function getApplicantsCountByMonthAndYear($OrgID) {
        
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID);
        $sel_info   =   "SELECT YEAR(EntryDate) as Year, MONTHNAME(EntryDate) as Month, COUNT(*) AS ApplicantsCount"; 
        $sel_info   .=  " FROM JobApplications"; 
        $sel_info   .=  " WHERE OrgID = :OrgID GROUP BY YEAR(EntryDate), MONTH(EntryDate)";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_info, array($params));
        
        $app_results    =   $res_info['results'];
        
        $applicants_count_list  =   array();
        for($r = 0; $r < $res_info['count']; $r++) {
            $applicants_count_list[$app_results[$r]['Year']][$app_results[$r]['Month']]    =   $app_results[$r]['ApplicantsCount'];
        }
        
        return $applicants_count_list;
    } // end function
    
}
