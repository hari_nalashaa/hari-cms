<?php
/**
 * @class       GenerateQuestionTypeHtml
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class GenerateQuestionTypeHtml {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      showQuestionType2
     */
    public function showQuestionType2($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType2.inc';
    }
    
    /**
     * @method      showQuestionType3
     */
    public function showQuestionType3($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType3.inc';
    }
    
    /**
     * @method      showQuestionType4
     */
    public function showQuestionType4($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType4.inc';
    }
    
    /**
     * @method      showQuestionType5
     */
    public function showQuestionType5($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType5.inc';
    }
    
    /**
     * @method      showQuestionType6
     */
    public function showQuestionType6($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType6.inc';
    }
    
    /**
     * @method      showQuestionType7
     */
    public function showQuestionType7($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType7.inc';
    }
    
    /**
     * @method      showQuestionType8
     */
    public function showQuestionType8($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType8.inc';
    }
    
    /**
     * @method      showQuestionType9
     */
    public function showQuestionType9($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType9.inc';
    }
    
    /**
     * @method      showQuestionType10
     */
    public function showQuestionType10($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType10.inc';
    }
    
    /**
     * @method      showQuestionType11
     */
    public function showQuestionType11($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType11.inc';
    }
    
    /**
     * @method      showQuestionType12
     */
    public function showQuestionType12($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType12.inc';
    }
    
    /**
     * @method      showQuestionType13
     */
    public function showQuestionType13($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType13.inc';
    }
    
    /**
     * @method      showQuestionType14
     */
    public function showQuestionType14($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType14.inc';
    }
    
    /**
     * @method      showQuestionType15
     */
    public function showQuestionType15($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType15.inc';
    }
    
    /**
     * @method      showQuestionType16
     */
    public function showQuestionType16($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType16.inc';
    }
    
    /**
     * @method      showQuestionType17
     */
    public function showQuestionType17($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType17.inc';
    }
    
    /**
     * @method      showQuestionType18
     */
    public function showQuestionType18($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType18.inc';
    }
    
    /**
     * @method      showQuestionType21
     */
    public function showQuestionType21($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType21.inc';
    }
    
    /**
     * @method      showQuestionType23
     */
    public function showQuestionType23($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType23.inc';
    }
    
    /**
     * @method      showQuestionType25
     */
    public function showQuestionType25($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType25.inc';
    }
    
    /**
     * @method      showQuestionType30
     */
    public function showQuestionType30($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType30.inc';
    }
    
    /**
     * @method      showQuestionType50
     */
    public function showQuestionType50($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType50.inc';
    }
    
    /**
     * @method      showQuestionType60
     */
    public function showQuestionType60($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType60.inc';
    }
    
    /**
     * @method      showQuestionType90
     */
    public function showQuestionType90($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType90.inc';
    }
    
    /**
     * @method      showQuestionType98
     */
    public function showQuestionType98($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType98.inc';
    }
    
    /**
     * @method      showQuestionType99
     */
    public function showQuestionType99($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType99.inc';
    }
    
    /**
     * @method      showQuestionType100
     */
    public function showQuestionType100($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType100.inc';
    }
    
    /**
     * @method      showQuestionType120
     */
    public function showQuestionType120($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType120.inc';
    }
    
    /**
     * @method      showQuestionType1818
     */
    public function showQuestionType1818($qi) {
        require_once COMMON_DIR . 'quetypesHtml/GenerateQuestionType1818.inc';
    }
    
}
?>
