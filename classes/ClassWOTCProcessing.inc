<?php
/**
 * @class		WOTCProcessing
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCProcessing {
	
	var $conn_string       =   "WOTC";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	/**
	 * @method		insUpdProcessing
	 * @param		$info
	 * @return		array
	 */
	function insUpdProcessing($info, $skip = array()) {
	
	    $ins_info  =   $this->db->buildOnDuplicateKeyUpdateStatement("Processing", $info, $skip);
	    $res_info  =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
	
	    return $res_info;
	}
	
	/**
     * @method      getProcessingInfo
     * @param       $WotcID, $ApplicationID
     */
    function getProcessingInfo($columns, $wotcID, $ApplicationID) {
    
        $columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
        $params_info    =   array(":wotcID"=>$wotcID, ":ApplicationID"=>$ApplicationID);
        $sel_info       =   "SELECT $columns FROM Processing WHERE wotcID = :wotcID AND ApplicationID = :ApplicationID";
        $res_info       =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params_info) );
        
        return $res_info;
    }

}	
