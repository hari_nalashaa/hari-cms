<?php 
/**
 * @class       Email
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Email {
	
	/**
	 * @tutorial	Constructor to load the default database
	 *          	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	
	/**
	 * @method		getEmailTemplateInfo
	 * @param 		$columns, $where_info, $order_by, $info
	 * @return 		array
	 * @tutorial	This method will retrieve the EmailTemplate information based on OrgID.
	 */
	function getEmailTemplateInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_email_template = "SELECT $columns FROM EmailTemplate";

		if(count($where_info) > 0) {
			$sel_email_template .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_email_template .= " ORDER BY " . $order_by;
		
		$res_email_template = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_email_template, $info );
		
		return $res_email_template;
	}
	
	/**
	 * @method     insEmailTemplateMasterInfo
	 * @param      $info
	 */
	function insEmailTemplateMasterInfo($OrgID) {
		
		$ins_tpl_info = "INSERT INTO EmailTemplate(OrgID, Cc, Bcc, IsCc, IsBcc, GoogleCalendar, OutlookCalendar, IcalCalendar, LotusCalendar, Body, LastModified) SELECT '$OrgID', Cc, Bcc, IsCc, IsBcc, GoogleCalendar, OutlookCalendar, IcalCalendar, LotusCalendar, Body, LastModified FROM EmailTemplate WHERE OrgId = 'MASTER'";
		$res_tpl_info = $this->db->getConnection("IRECRUIT")->insert ( $ins_tpl_info );
	
		return $res_tpl_info;
	}
	
	/**
	 * @method     updEmailTemplateInfo
	 * @param      $set_info, $where_info, $info
	 */
	function updEmailTemplateInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_email_tpl_info = $this->db->buildUpdateStatement('EmailTemplate', $set_info, $where_info);
		$res_email_tpl_info = $this->db->getConnection("IRECRUIT")->update($upd_email_tpl_info, $info);
	
		return $res_email_tpl_info;
	}
	

	/**
	 * @method		getEmailResponsesInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getEmailResponsesInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_email_responses_info = "SELECT $columns FROM EmailResponses";
		$where = "";
		if(count($where_info) > 0)
		{
			$where	= " WHERE " . implode(" AND ", $where_info);
		}
	
		$sel_email_responses_info .= $where;
		if($order_by != "") $sel_email_responses_info .= " ORDER BY " . $order_by;
	
		$res_email_responses_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_email_responses_info, $info );
	
		return $res_email_responses_info;
	}
	
	/**
	 * @method     insEmailResponsesInfo
	 * @param      $info
	 */
	function insEmailResponsesInfo($info) {
		
		$ins_email_responses_info = $this->db->buildInsertStatement('EmailResponses', $info);
		$res_email_responses_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_email_responses_info["stmt"], $ins_email_responses_info["info"] );
	
		return $res_email_responses_info;
	}
	
	/**
	 * @method	    delEmailResponsesInfo
	 * @return	    array
	 */
	function delEmailResponsesInfo($where_info = array(), $info = array(), $limit = "") {
		
		$del_email_responses_info = "DELETE FROM EmailResponses";

		if(count($where_info) > 0) {
			$del_email_responses_info .= " WHERE " . implode(" AND ", $where_info);
		}
		if($limit != "") {
			$del_email_responses_info .= " LIMIT $limit";
		}
	
		$res_email_responses_info = $this->db->getConnection("IRECRUIT")->delete ( $del_email_responses_info, $info );
	
		return $res_email_responses_info;
	}
	
	/**
	 * @method     getOrganizationFromEmail
	 */
	function getOrganizationFromEmail($OrgID, $MultiOrgID) {
	     
		$from			=	G::Obj('IrecruitSettings')->getValue("From");
	    
		$params			=   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		$sel_email_info	=   "SELECT Email, EmailVerified FROM OrganizationEmail WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
		$res_email_info	=   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_email_info, array($params) );
		    
		if($res_email_info['Email'] == "" || $res_email_info['EmailVerified'] == 0) {
			$res_email_info['Email'] = $from["Email"];
		}
	    
	    return $res_email_info['Email'];
	}

}
?>
