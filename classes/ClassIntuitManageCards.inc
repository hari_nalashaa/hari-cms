<?php
/**
 * @class		IntuitManageCards
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IntuitManageCards {    

    var $conn_string        =   "IRECRUIT";
    var $customers_url      =   '';
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
        
        if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com"
            || $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com") {
            //Development
            $this->customers_url     =   "https://sandbox.api.intuit.com/quickbooks/v4/customers/";
        }
        else {
            //Production
            $this->customers_url     =   "https://api.intuit.com/quickbooks/v4/customers/";
        }
        
    }
	
    /**
    * @method     getCardInfoFromApi
    */
    function getCardInfoFromApi($OrgID, $CardInfoID) {

      global $IntuitObj, $CurlClientObj;
	    
	    //Get Intuit Access Information
	    $intuit_access_info   =   $IntuitObj->getUpdatedAccessTokenInfo();
	    //Change your Access Token Here
	    $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
	    //Add your request ID here
	    $intuit_request_id    =   uniqid();
	    
	    $http_header = array(
	        'Accept'          =>  'application/json',
	        'Authorization'   =>  $access_token,
	        'Content-Type'    =>  'application/json;charset=UTF-8'
	    );
	    
        $card_on_file_url       =   $this->customers_url.$OrgID.'/cards/'.$CardInfoID;
        $intuit_card_response   =   $CurlClientObj->makeAPICall($card_on_file_url, "GET", $http_header, array(), null, false);
	     
      return $intuit_card_response;
        
    }
	
    /**
    * @method     getAllCardsInfoFromApi
    */
    function getAllCardsInfoFromApi($OrgID) {

      global $IntuitObj, $CurlClientObj;
	     
	    //Get Intuit Access Information
	    $intuit_access_info   =   $IntuitObj->getUpdatedAccessTokenInfo();
	    //Change your Access Token Here
	    $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
	    //Add your request ID here
	    $intuit_request_id    =   uniqid();
	     
	    $http_header = array(
	        'Accept'          =>  'application/json',
	        'Authorization'   =>  $access_token,
	        'Content-Type'    =>  'application/json;charset=UTF-8'
	    );
	     
	    $card_on_file_url       =   $this->customers_url.$OrgID.'/cards';
	    $intuit_card_response   =   $CurlClientObj->makeAPICall($card_on_file_url, "GET", $http_header, array(), null, false);
	
      return $intuit_card_response;
	
    }
	
}
?>
