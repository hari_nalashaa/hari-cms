<?php 
class Curl {

	/**
	 * @method get
	 * @param $url
	 * @return $result
	 */
	function get($url) {
		// create curl resource
		$ch = curl_init();
			
		// set url
		curl_setopt($ch, CURLOPT_URL, $url);
			
		//return the transfer as a string
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
		// $output contains the output string
		$result = curl_exec($ch);
			
		// close curl resource to free up system resources
		curl_close($ch);
	
			
		if($result === false)
		{
			//We will not display any errors
			echo curl_error($ch);
		}
		
		return $result;
	}
	
	
	/**
	 * @method     getIrecruitData
	 * @param      $data, $url
	 * @return     $result
	 */
	function postJSON($url, $data, $content_type) {
		$data_string = json_encode($data);
	
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
					);
		$result = curl_exec($ch);
		curl_close($ch);
	
		if($result === false)
		{
			echo 'Curl error: ' . curl_error($ch);
		}
		
		return $result;
	}
}
?>