<?php
/**
 * @class		ValidateApplicationForm
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ValidateApplicationForm {

    var $conn_string        =   "IRECRUIT";
    var $FORMDATA           =   array();
    var $ERRORS             =   array();
    var $alphabet           =   array();        //Alphabets
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
        
        //Alphabets
        $this->alphabet     =   G::Obj('GenericLibrary')->getAlphabetsKeyFrom1();
    }

    /**
     * @method      validateApplicationForm
     * @param       array $qi
     */
    function validateApplicationForm($OrgID, $FormID, $SectionID = '', $HoldID = '') {
        
        if($SectionID != "") {
            $section_info       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $FormID, $SectionID);
            $form_sections[$SectionID]  =   $section_info;
        }
        else {
            //Get All Active Sections
            $form_sections      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
        }
        
        foreach ($form_sections as $section_id=>$section_info) {
            
            $ques_info          =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, $section_id);
            $parent_ques        =	G::Obj('ApplicationFormQuestions')->getFormParentQuesForChildQues($OrgID, $FormID, $section_id);
            $child_ques_info    =   json_decode($ques_info['country']['ChildQuestionsInfo'], true);

            if(is_array($ques_info)) {
                foreach ($ques_info as $QuestionID=>$QI)
                {

                    $QuestionTypeID =   $QI['QuestionTypeID'];
                    $Question       =   trim($QI['Question'], ":");
                    $ParentQueID	=	$parent_ques[$QuestionID];
                    $ChildQuesInfo	=	json_decode($ques_info[$ParentQueID]['ChildQuestionsInfo'], true);

                    $QI['SectionTitle'] =   $section_info['SectionTitle'];
                    $QI['HoldID']       =   $HoldID;
                    
                    //Validate parent and child questions
                    if($parent_ques[$QuestionID] != "") {
                        $parent_que_val_info	=	$ChildQuesInfo[$this->FORMDATA['REQUEST'][$ParentQueID]];
                        
                        if($parent_que_val_info[$QuestionID] == "show") {
                            call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QI, $HoldID);
                        }
                    }
                    else {
                        if(($QI['Required'] == 'Y'
                            && $QuestionTypeID != '17')
                            || $QuestionID == 'cellphonepermission') {
                            call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QI, $HoldID);
                        }
                        else if($QuestionTypeID == '17') {
                            call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QI, $HoldID);
                        }
                    }
                    
                }
            }
        }

        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType2
     * @input_type  radio
     */
    function validateQuestionType2($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']] ==  "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question']." is missing." . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType3
     * @input_type  pulldown
     */
    function validateQuestionType3($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']]  ==  ""
            && $QI['Required'] == 'Y') {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =   " - " . $QI['SectionTitle'] . " - " . strip_tags($QI['Question'])." is missing." . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        $twilio_account_info    =	G::Obj('TwilioAccounts')->getTwilioAccountInfo($QI['OrgID']);
        $twilio_agreement_info  =   G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($QI['OrgID']);
        
        if($this->FORMDATA['REQUEST']['cellphonepermission'] == "Yes"
            && $twilio_agreement_info['Active'] == 'Y'
            && $twilio_account_info['OrgID'] != ""
            && $QI['QuestionID'] == 'cellphonepermission')
        {
            $applicant_cell_number_sms	=	"false";

            $cell_number                =   json_decode($this->FORMDATA['REQUEST']['cellphone'], true);
                    
            $applicant_cell_number		=	$cell_number[0].$cell_number[1].$cell_number[2];
            
            $applicant_cell_number		=	str_replace(array("-", "(", ")", " "), "", $applicant_cell_number);

            if($applicant_cell_number != "") {

                //Add plus one before number
                if(substr($applicant_cell_number, 0, 2) != "+1") {
                    $applicant_cell_number	=	"+1".$applicant_cell_number;
                }
                
                //Get LoopUp Phone number information
                $lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($QI['OrgID'], $applicant_cell_number);
                
                try {
                    //Get the Protected properties from SMS object, by extending the object through ReflectionClass
                    $reflection     =   new ReflectionClass($lookup_phone_num_info['Response']);
                    $property       =   $reflection->getProperty("properties");
                    $property->setAccessible(true);
                    $properties     =   $property->getValue($lookup_phone_num_info['Response']);
                }
                catch (Exception $e) {
                    //Presently do nothing
                }

                if($properties['carrier']['type'] == 'mobile'
                    || $properties['carrier']['type'] == 'voip') {
                    $applicant_cell_number_sms	=	"true";
                }
                    
                if($applicant_cell_number_sms == "false") {
                    $cellphone_error_msg	=	' You have opted to receive text messages and this cell phone number is not capable. Please select No to permission question or update the phone number.';
                    
                    $this->ERRORS['ERRORS']['cellphone']        =   " - " . $QI['SectionTitle'] . " - " . $cellphone_error_msg . "\\n";
                    $this->ERRORS['QuestionInfo']['cellphone']  =   $QI;
                    
                    $this->ERRORS['ERRORS']['cellphonepermission']          =   "\\n";
                    $this->ERRORS['QuestionInfo']['cellphonepermission']    =   $QI;
                    
                    G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
                }
            }
            else {
                $cellphone_error_msg	=	' You have opted to receive text messages and this cell phone number is not capable. Please select No to permission question or update the phone number.';
                
                $this->ERRORS['ERRORS']['cellphone']        =   " - " . $QI['SectionTitle'] . " - " . $cellphone_error_msg . "\\n";
                $this->ERRORS['QuestionInfo']['cellphone']  =   $QI;
                
                $this->ERRORS['ERRORS']['cellphonepermission']          =   "\\n";
                $this->ERRORS['QuestionInfo']['cellphonepermission']    =   $QI;
                
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
            }
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType5
     * @input_type  textarea
     */
    function validateQuestionType5($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']]  ==  "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =   " - " . $QI['SectionTitle'] . " - " . $QI['Question']." is missing." . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType6
     * @input_type  text
     */
    function validateQuestionType6($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']]  ==  "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =   " - " . $QI['SectionTitle'] . " - " . $QI['Question']." is missing." . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType8
     * @input_type  file
     */
    function validateQuestionType8($QI, $HoldID) {

        if($this->FORMDATA['FILES'][$QI['QuestionID']]['name'] == ""
            && $this->FORMDATA['REQUEST'][$QI['QuestionID']] == "") {
            
            if($this->FORMDATA['FILES'][$QI['QuestionID']]['name'] != "") {
                $QI['Answer'] =   $this->FORMDATA['FILES'][$QI['QuestionID']]['name'];
            }
            else if($this->FORMDATA['REQUEST'][$QI['QuestionID']] != "") {
                $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            }
            
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =   " - " . $QI['SectionTitle'] . " - " . $QI['Question']." is missing." . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType9
     */
    function validateQuestionType9($QI, $HoldID) {

        $hitone         =   0;
        $values_list    =   json_decode($this->FORMDATA['REQUEST'][$QI['QuestionID']], true);

        if(is_array($values_list)) {
            foreach($values_list as $val9_key=>$val9_val) {
                if (($QI['QuestionTypeID'] == 9)
                    && ($val9_val[$QI['QuestionID']."-".$val9_key] != "")
                    && ($val9_val[$QI['QuestionID']."-".$val9_key."-yr"] != "")
                    && ($val9_val[$QI['QuestionID']."-".$val9_key."-comments"] != "")) {
                        $hitone ++;
                    }
            }
        }
        
        if ($hitone == 0) {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
    }
    
    /**
     * @method      validateQuestionType13
     * @input_type  phone
     */
    function validateQuestionType13($QI, $HoldID) {
        $phone_info   =   json_decode($this->FORMDATA['REQUEST'][$QI['QuestionID']], true);
        
        if ($phone_info[0] == ""
            || $phone_info[1] == ""
            || $phone_info[2] == "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }        
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType14
     * @input_type  phone-with-extension
     */
    function validateQuestionType14($QI, $HoldID) {
        $phone_ext_info   =   json_decode($this->FORMDATA['REQUEST'][$QI['QuestionID']], true);
        
        if($phone_ext_info[0]  ==  ""
            || $phone_ext_info[1]  ==  ""
            || $phone_ext_info[2]  ==  ""
            || $phone_ext_info[3]  ==  "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
            
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType15
     * @input_type  social-security
     */
    function validateQuestionType15($QI, $HoldID) {
        $social_security_info   =   json_decode($this->FORMDATA['REQUEST'][$QI['QuestionID']], true);
        
        if($social_security_info[0]  ==  ""
            || $social_security_info[1]  ==  ""
            || $social_security_info[2]  ==  "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
            
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType17
     * @input_type  date
     */
    function validateQuestionType17($QI, $HoldID) {
        
        if($QI['Required'] == 'Y'
            && $this->FORMDATA['REQUEST'][$QI['QuestionID']] == "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =   " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . '\\n';
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        else if($QI['Validate'] != "") {
            $validate_que_info = json_decode($QI['Validate'], true);
            
            if($validate_que_info['years_min'] != "") {
                
                if($this->FORMDATA['REQUEST'][$QI['QuestionID']] != "") {
                    
                    //date information
                    $date_ans			=	G::Obj('DateHelper')->getYmdFromMdy($this->FORMDATA['REQUEST'][$QI['QuestionID']]);
                    $d1 				=	new DateTime(date('Y-m-d'));
                    $d2 				=	new DateTime($date_ans);
                    
                    $ans_date_diff		=	$d2->diff($d1);
                    
                    if($ans_date_diff->y < $validate_que_info['years_min']) {
                        $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
                        $this->ERRORS['ERRORS'][$QI['QuestionID']] =   " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' date needs to be more than '.$validate_que_info['years_min'].' years' . '\\n';
                        $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
                        G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
                    }
                    
                }
            }
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType18
     * @input_type  checkbox
     */
    function validateQuestionType18($QI, $HoldID) {
        $chk_values =   json_decode($this->FORMDATA['REQUEST'][$QI['QuestionID']], true);
        $valcnt     =   count($chk_values);
        
        $hitone     =   0;
        
        if(is_array($chk_values)) {
            foreach ($chk_values as $chk_values_key=>$chk_values_val) {
                if ($chk_values_val != "") {
                    $hitone ++;
                }
            }
        }
        
        if ($hitone == 0) {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType22
     * @input_type  radio-list
     */
    function validateQuestionType22($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']]  ==  "") {
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType23
     * @input_type  radio-long-question
     */
    function validateQuestionType23($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']]  ==  "") {
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType25
     * @input_type  date-time
     */
    function validateQuestionType25($QI, $HoldID) {
        if($this->FORMDATA['REQUEST'][$QI['QuestionID']]  ==  "") {
            $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
        }
        
        return $this->ERRORS;
    }

    /**
     * @method      validateQuestionType40
     * @input_type  date-time
     */
    function validateQuestionType40($QI, $HoldID) {
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType45
     * @input_type  date-time
     */
    function validateQuestionType45($QI, $HoldID) {
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType99
     * @input_type  date-time
     */
    function validateQuestionType99($QI, $HoldID) {
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType100
     * @input_type  radio-with-group-skills
     */
    function validateQuestionType100($QI, $HoldID) {
        $que_100_ans = unserialize($this->FORMDATA['REQUEST'][$QI['QuestionID']]);

        if($QI['QuestionTypeID'] == 100) {
            if(!isset($que_100_ans) 
                || count($que_100_ans) == 0) {
                $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
                $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
                $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
            }
            else if(isset($que_100_ans)) {
                if(is_array($que_100_ans)) {
                    foreach($que_100_ans as $cus_que100_label=>$cus_que100_value) {
                        if(is_array($cus_que100_value)) {
                            foreach($cus_que100_value as $cus_que100_lbl_info=>$cus_que100_lbl_val) {
                                if($cus_que100_lbl_val == "") {
                                    $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
                                    $this->ERRORS['ERRORS'][$QI['QuestionID']] =  " - " . $QI['SectionTitle'] . " - Please select required option in " . $FQCK ['Question'] . "\\n";
                                    $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
                                    G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType120
     * @input_type  pulldown-shifts
     */
    function validateQuestionType120($QI, $HoldID) {
        if(!isset($this->FORMDATA['REQUEST'][$QI['QuestionID']]) 
            || $this->FORMDATA['REQUEST'][$QI['QuestionID']] == "") {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        else if(isset($this->FORMDATA['REQUEST'][$QI['QuestionID']])) {
            
            $que_120_ans = unserialize($this->FORMDATA['REQUEST'][$QI['QuestionID']]);

            if(is_array($que_120_ans)) {
                $from_available_flag = false;
                if(is_array($que_120_ans['from_time'])) {
                    foreach($que_120_ans['from_time'] as $cus_que120_from_val) {
                        if($cus_que120_from_val == "") {
                            $from_available_flag = true;
                        }
                    }
                }
            }
            
            if(is_array($que_120_ans)) {
                $to_available_flag = false;
                if(is_array($que_120_ans['to_time'])) {
                    foreach($que_120_ans['to_time'] as $cus_que120_to_val) {
                        if($cus_que120_to_val == "") {
                            $to_available_flag = true;
                        }
                    }
                }
            }
            
            if($from_available_flag == true
                || $to_available_flag == true)
            {
                $que_120_msg = "";
                if($from_available_flag == true
                    && $to_available_flag == true)
                {
                    $que_120_msg = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ':' . ' from time and to time fields are missing.' . "\\n";
                }
                else if($from_available_flag == true) {
                    $que_120_msg = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ':' . ' from time fields are missing.' . "\\n";
                }
                else if($to_available_flag == true) {
                    $que_120_msg = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ':' . ' to time fields are missing.' . "\\n";
                }
                
                $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
                $this->ERRORS['ERRORS'][$QI['QuestionID']] = $que_120_msg;
                $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
            }
            
        }
        
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType1818
     * @input_type  checkboxes-list
     */
    function validateQuestionType1818($QI, $HoldID) {
        $chk_values =   json_decode($this->FORMDATA['REQUEST'][$QI['QuestionID']], true);
        $valcnt     =   count($chk_values);
        
        $hitone     =   0;
        
        if(is_array($chk_values)) {
            foreach ($chk_values as $chk_values_key=>$chk_values_val) {
                if ($chk_values_val != "") {
                    $hitone ++;
                }
            }
        }
        
        if($hitone == 0) {
            $QI['Answer'] =   $this->FORMDATA['REQUEST'][$QI['QuestionID']];
            $this->ERRORS['ERRORS'][$QI['QuestionID']] = " - " . $QI['SectionTitle'] . " - " . $QI['Question'] . ' is missing.' . "\\n";
            $this->ERRORS['QuestionInfo'][$QI['QuestionID']]  =   $QI;
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp($QI);
        }
        
        return $this->ERRORS;
    }
    
}
