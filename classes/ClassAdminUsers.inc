<?php
/**
 * @class		AdminUsers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class AdminUsers {
	
    public $db;
    
	var $conn_string       =   "ADMIN";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
    
    /**
	 * @method		getUserInfoByUserID
	 * @return		array
	 * @param 		$USERID, $columns        	
	 */
	function getUserInfoByUserID($UserID, $columns = "*") {
		
		$columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
		
		// Set parameters for prepared query
		$params         =   array (":UserID"=>$UserID);
		$sel_user_info  =   "SELECT $columns FROM Users WHERE UserID = :UserID";
		$res_user_info  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_user_info, array ($params) );
		
		return $res_user_info;
    }

	/**
	 * @method		getUserInfoBySessionID
	 * @return		array
	 * @param 		$SessionID, $columns        	
	 */
	function getUserInfoBySessionID($SessionID, $columns = "*") {
		
		$columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
		
		// Set parameters for prepared query
		$params         =   array (":SessionID"=>$SessionID);
		$sel_user_info  =   "SELECT $columns FROM Users WHERE SessionID = :SessionID";
		$res_user_info  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_user_info, array ($params) );
		
		return $res_user_info;
    }

    /**
	 * @method		getUserInfoByEmail
	 * @return		array
	 * @param 		$Email, $columns        	
	 */
	function getUserInfoByEmail($Email, $columns = "*") {
		
		$columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
		
		// Set parameters for prepared query
		$params         =   array (":Email"=>$Email);
		$sel_user_info  =   "SELECT $columns FROM Users WHERE Email = :Email";
		$res_user_info  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_user_info, array ($params) );
		
		return $res_user_info;
    }


    /**
	 * @method		updUsersInfo
	 */
	function updUsersInfo($set_info = array(), $where_info = array(), $info = array()) {
		
		$upd_user_info = $this->db->buildUpdateStatement('Users', $set_info, $where_info);
		$res_user_info = $this->db->getConnection( $this->conn_string )->update($upd_user_info, $info);
	
		return $res_user_info;
	}

}
