<?php
/**
 * @class		IntranetAccess
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IntranetAccess {

	var $conn_string       =   "ADMIN";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
    }
    
    /**
	 * @method		getIntranetAccessInfo
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial	This method will fetch the form sections informtaion
	 */
	function getIntranetAccessInfo($columns = "", $where_info = array(), $order_by = "", $params = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM IntranetAccess";

		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$res_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
	
		return $res_info;
	}

	/**
	 * @method		getIntranetAccessInfoByIpAddress
	 */
	function getIntranetAccessInfoByIpAddress($columns = "*", $IPADDRESS) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$params		=	array(":IPADDRESS"	=>	$IPADDRESS);
		$sel_info	=	"SELECT $columns FROM IntranetAccess WHERE IPADDRESS = :IPADDRESS";
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params));
	
		return $res_info;
	}

	/**
	 * @method		insUpdIntranetAccess
	 */
	function insUpdIntranetAccess($info) {
		
		$params		=	array(
							":UserID"		=>	$info['UserID'],
							":IPADDRESS"		=>	$info['IPADDRESS'], 
							":UIPADDRESS"		=>	$info['IPADDRESS']
						);
		$ins_info	=	"INSERT INTO IntranetAccess (UserID, IPADDRESS, LastUpdated) VALUES (:UserID, :IPADDRESS, NOW()) ON DUPLICATE KEY UPDATE IPADDRESS = :UIPADDRESS, LastUpdated = NOW()";
		$res_info	=	$this->db->getConnection( $this->conn_string )->insert ( $ins_info, array($params) );
	
		return $res_info;
	}
}
?>
