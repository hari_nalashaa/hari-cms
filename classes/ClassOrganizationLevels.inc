<?php 
/**
 * @class		OrganizationLevels
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class OrganizationLevels {
    
    /**
     * @tutorial	Set the default database connection as irecruit
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    
    /**
     * @method      getOrgLevels
     * @tutorial    To get all OrgLevels based on $OrgID and $MultiOrgID
     */
    public function getOrgLevels($OrgID, $MultiOrgID = '') {

        $params_info        =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
        $sel_org_levels     =   "SELECT * FROM OrganizationLevels WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
        $res_org_levels     =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_org_levels, array($params_info) );
        
        return $res_org_levels['results'];
    }
    
    
    /**
     * @method      getOrgLevelInfo
     * @tutorial    To get OrgLevelInfo based on $OrgID, $MultiOrgID, $OrgLevelID
     */
    public function getOrgLevelInfo($OrgID, $MultiOrgID, $OrgLevelID) {

        $params_info        =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrgLevelID"=>$OrgLevelID);
        $sel_org_level      =   "SELECT * FROM OrganizationLevels WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND OrgLevelID = :OrgLevelID";
        $res_org_level      =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_org_level, array($params_info) );
        
        return $res_org_level;
    }
    
    /**
     * @method      getOrgLevelCategories
     * @tutorial    To get all OrgLevels based on $OrgID and $MultiOrgID
     */
    public function getOrgLevelCategories($OrgID, $MultiOrgID, $OrgLevelID) {
    
        $params_info            =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrgLevelID"=>$OrgLevelID);
        $sel_org_level_cats     =   "SELECT * FROM OrganizationLevelData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND OrgLevelID = :OrgLevelID";
        $res_org_level_cats     =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_org_level_cats, array($params_info) );
        $org_level_cats         =   $res_org_level_cats['results'];
        
        $org_level_cats_list    =   array();
        for($o = 0; $o < count($org_level_cats); $o++) {
            $selection_order    =   $org_level_cats[$o]['SelectionOrder'];
            $org_level_cats_list[$selection_order]  =   $org_level_cats[$o]['CategorySelection'];
        }
        
        return $org_level_cats_list;
    }

    /**
     * @method      getOrgLevelDataByOrgLevelIDCount
     * @tutorial    To get all OrgLevels based on $OrgID and $MultiOrgID
     */
    public function getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, $OrgLevelID, $ConditionType = '') {

        $params_info            =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
        $sel_org_levels_data    =   "SELECT * FROM OrganizationLevelData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";

        if($ConditionType == 'less_equal') {
            $sel_org_levels_data    .=   " AND OrgLevelID <= :OrgLevelID";
            $params_info[":OrgLevelID"] =   $OrgLevelID;
        }
        else if($ConditionType == 'equal') {
            $sel_org_levels_data    .=   " AND OrgLevelID = :OrgLevelID";
            $params_info[":OrgLevelID"] =   $OrgLevelID;
        }
        
        $res_org_levels_data    =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_org_levels_data, array($params_info) );
        $org_levels_data        =   $res_org_levels_data['results'];
        
        return $org_levels_data;
    }
}
?>
