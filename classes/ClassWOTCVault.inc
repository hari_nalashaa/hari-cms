<?php
/**
 * @class		WOTCVault
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCVault
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method      vault
     * @return      string
     */
    public function vault()
    {
        global $AUTH;
        
        $rtn    =   "";
        
        $VAL    =   array();
        $DATA   =   explode('&', $_POST['formdata']);
        foreach ($DATA as $n => $v) {
            list ($key, $value) = explode('=', $v);
            $VAL[$key] = $value;
        }
        if (sizeof($VAL) > 1) {
            $_POST = array_merge($_POST, $VAL);
        }
        
        $dir    =   WOTC_DIR . "vault/" . $_POST['wotcID'];
        $dir    =   $dir;

        if (! file_exists($dir)) {
            @mkdir($dir, 0700);
            @chmod($dir, 0777);
        }
        
        $appvaultdir    =   $dir . "/applicantvault";
        $appvaultdir    =   $appvaultdir;

        if (! file_exists($appvaultdir)) {
            @mkdir($appvaultdir, 0700);
            @chmod($appvaultdir, 0777);
        }

        if ($_FILES['newfile']['type'] != "") {
            $e          =   explode('.', $_FILES['newfile']['name']);
            $ecnt       =   count($e) - 1;
            $FileType   =   $e[$ecnt];
            $FileType   =   preg_replace("/\s/i", '', $FileType);
            $FileName   =   $e[$ecnt - 1];
            $FileName   =   preg_replace("/\s/i", '_', $FileName);
            $data       =   file_get_contents($_FILES['newfile']['tmp_name']);
            
            $query      =   "INSERT INTO ApplicantVault";
            $query      .=   " (wotcID, ApplicationID, FileName, FileType, Date, UpdateID, UserID)";
            $query      .=   " VALUES (:wotcid,:applicationid,:filename,:filetype,now(),NULL,:userid)";
            $params     =   array(
                                ':wotcid'           =>  $_POST['wotcID'],
                                ':applicationid'    =>  $_POST['ApplicationID'],
                                ':filename'         =>  $FileName,
                                ':filetype'         =>  $FileType,
                                ':userid'           =>  !empty($AUTH['UserID']) ? $AUTH['UserID'] : 'CMS'
                            );
            $this->db->getConnection ( $this->conn_string )->insert($query, array($params));
            
            $query      =   "SELECT LAST_INSERT_ID() FROM ApplicantVault LIMIT 1";
            list ($li)  =   $this->db->getConnection ( $this->conn_string )->fetchRow($query);
            
            $filename   =   $appvaultdir . "/" . $_POST['ApplicationID'] . "-" . $li . "-" . $FileName . "." . $FileType;
            $filename   =   $filename;
            $fh         =   fopen($filename, 'w');
            fwrite($fh, $data);
            fclose($fh);
            
            chmod($filename, 0666);
        } // end if FILES
        
        if (($_POST['type'] == "VC") && ($_POST['action'] != "")) {
            
            $query      =   "UPDATE Processing";
            $query      .=  " SET VaultComplete = :action,";
            $query      .=  " LastUpdated = NOW()";
            $query      .=  " WHERE wotcID = :wotcid and ApplicationID = :applicationid";
            $params     =   array(
                                ':action'           =>  $_POST['action'],
                                ':wotcid'           =>  $_POST['wotcID'],
                                ':applicationid'    =>  $_POST['ApplicationID'],
                            );
            $this->db->getConnection ( $this->conn_string )->update($query, array($params));
        } // end type VC
        
        if (($_POST['type'] == "Delete") && ($_POST['action'] != "")) {
            
            $query      =   "SELECT * FROM ApplicantVault";
            $query      .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid AND UpdateID = :action";
            $params     =   array(
                                ':wotcid'           =>  $_POST['wotcID'],
                                ':applicationid'    =>  $_POST['ApplicationID'],
                                ':action'           =>  $_POST['action']
                            );
            $AV         =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
            
            $query      =   "SELECT DATE_FORMAT(NOW(),'%Y%m%d%H%m%s')";
            list ($hld) =   $this->db->getConnection ( $this->conn_string )->fetchRow($query);
            
            $filenameA = $AV['ApplicationID'] . "-" . $AV['UpdateID'] . "-" . $AV['FileName'] . "." . $AV['FileType'];
            $filenameB = $AV['ApplicationID'] . "-" . $AV['UpdateID'] . "-" . $AV['FileName'] . "-" . $hld . "D." . $AV['FileType'];
            
            $command = "mv " . $appvaultdir . "/" . $filenameA . " " . $appvaultdir . "/" . $filenameB;
            rename($filenameA , $filenameB);
            
            $query      =   "DELETE FROM ApplicantVault";
            $query      .=  " WHERE wotcID = :wotcid and ApplicationID = :applicationid AND UpdateID = :updateid";
            $params     =   array(
                                ':wotcid'           =>  $AV['wotcID'],
                                ':applicationid'    =>  $AV['ApplicationID'],
                                ':updateid'         =>  $AV['UpdateID']
                            );
            $this->db->getConnection ( $this->conn_string )->delete($query, array($params));
        } // end type Delete
        
        $query                  =   "SELECT VaultComplete FROM Processing";
        $query                  .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params                 =   array(
                                        ':wotcid'           =>  $_POST['wotcID'],
                                        ':applicationid'    =>  $_POST['ApplicationID'],
                                    );
        list ($VaultComplete)   =   $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
        
        $query  =   "SELECT * FROM ApplicantData";
        $query  .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params =   array(
                        ':wotcid'           =>  $_POST['wotcID'],
                        ':applicationid'    =>  $_POST['ApplicationID'],
                    );
        $AD     =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $rtn .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\n";
        $rtn .= "<tr><td align=\"right\">Application ID:</td>";
        $rtn .= "<td>" . $AD['ApplicationID'] . " (XXX-XX-" . $AD['Social3'] . ")</td></tr>\n";
        $rtn .= "<tr><td align=\"right\" valign=\"top\">Applicant:</td>";
        $rtn .= "<td valign=\"top\">\n";
        $rtn .= "<b>" . $AD['FirstName'] . " " . $AD['MiddleName'] . " " . $AD['LastName'] . "</b><br>";
        $rtn .= $AD['Address'] . "<br>";
        $rtn .= $AD['City'] . ", " . $AD['State'] . " " . $AD['ZipCode'];
        $rtn .= "</td></tr>\n";
        $rtn .= "</table>\n";
        
        $query      =   "SELECT wotcID, ApplicationID, FileName, FileType,";
        $query      .=  " DATE_FORMAT(Date,'%Y-%m-%d') AS Date, UpdateID, UserID";
        $query      .=  " FROM ApplicantVault";
        $query      .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid order by FileName";
        $params     =   array(
                            ':wotcid'           =>  $_POST['wotcID'],
                            ':applicationid'    =>  $_POST['ApplicationID'],
                        );
        $FILES_RES  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($query, array($params));
        $FILES_RES  =   $FILES['results'];
        $cnt        =   $FILES_RES['count'];
        
        if ($cnt > 0) {
            
            $rtn .= "<table border=\"0\" cellspacing=\"3\" cellpadding=\"5\" width=\"645\" align=\"center\">\n";
            
            // header row
            $rtn .= "<tr height=\"30\">";
            
            $rtn .= "<td width=\"230\">";
            $rtn .= "<b>File Name</b>";
            $rtn .= "</td>";
            
            $rtn .= "<td>";
            $rtn .= "<b>Date</b>";
            $rtn .= "</td>";
            
            if ($VaultComplete != "Y") {
                $rtn .= "<td width=\"110\">";
            } else {
                $rtn .= "<td width=\"215\">";
            }
            $rtn .= "<b>Uploaded By</b>";
            $rtn .= "</td>";
            
            $rtn .= "<td align=\"center\" width=\"20\">";
            $rtn .= "<b>Download</b>";
            $rtn .= "</td>";
            
            if (MASTERADMIN == "Y") { // CMS Admin only
                $rtn .= "<td align=\"center\" width=\"20\">\n";
                $rtn .= "<b>Delete</b>";
                $rtn .= "</td>";
            } // end MASTERADMIN
            
            $rtn .= "<tr>\n";
            
            // loop through data
            foreach ($FILES as $FILE) {
                if ($rowcolor == "#eeeeee") {
                    $rowcolor = "#ffffff";
                } else {
                    $rowcolor = "#eeeeee";
                }
                
                $rtn .= "<tr bgcolor=\"" . $rowcolor . "\">";
                
                $rtn .= "<td>";
                $rtn .= $FILE['FileName'] . "." . $FILE['FileType'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $FILE['Date'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $FILE['UserID'];
                $rtn .= "</td>";
                
                if (MASTERADMIN == "Y") { // CMS Admin only
                    $DOWNLOAD = ADMIN_HOME . "wotc/";
                } else {
                    $DOWNLOAD = WOTCADMIN_HOME;
                }
                
                $rtn .= "<td align=\"center\">";
                $rtn .= "<a href=\"" . $DOWNLOAD . "downloadVaultFile.php?wotcID=" . $FILE['wotcID'] . "&ApplicationID=" . $FILE['ApplicationID'] . "&UpdateID=" . $FILE['UpdateID'] . "\" target=\"_blank\">";
                $rtn .= "<img src=\"" . WOTCADMIN_HOME . "images/page_white_magnify.png\" border=\"0\" title=\"View\" style=\"margin:0px 3px -4px 0px;\">";
                $rtn .= "</a>";
                $rtn .= "</td>";
                
                if (MASTERADMIN == "Y") { // CMS Admin only
                    $rtn .= "<td align=\"center\">";
                    $rtn .= "<a href=\"#\" onclick=\"processVault('" . $FILE['wotcID'] . "','" . $FILE['ApplicationID'] . "','Delete','" . $FILE['UpdateID'] . "','Are you sure you want to delete the file? " . $FILE['FileName'] . "." . $FILE['FileType'] . "');return false;\">";
                    $rtn .= "<img src=\"" . ADMIN_HOME . "images/cross.png\" border=\"0\" title=\"Delete\" style=\"margin:0px 3px -4px 0px;\">";
                    $rtn .= "</a>";
                    $rtn .= "</td>";
                }
                
                $rtn .= "</tr>\n";
            } // end foreach
            
            $rtn .= "</table>\n";
        } // end cnt
          
        // add file section
        if ($VaultComplete != "Y") {
            
            $rtn .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"645\" align=\"center\">\n";
            $rtn .= "<tr><td>";
            $rtn .= "<hr size=\"1\">";
            $rtn .= "New File: " . '<input type="file" name="newfile' . $_POST['wotcID'] . $_POST['ApplicationID'] . '" id="newfile' . $_POST['wotcID'] . $_POST['ApplicationID'] . '">';
            $rtn .= "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"10485760\">";
            $rtn .= "<button id=\"file_upload\" onclick=\"displayVault('" . $_POST['wotcID'] . "','" . $_POST['ApplicationID'] . "');return false;\">Add New File</button>";
            $rtn .= "<hr size=\"1\">";
            $rtn .= "</td></tr>\n";
            $rtn .= "</table>\n";
        } // end VaultComplete
        
        if ((MASTERADMIN == "Y") && ($VaultComplete != "")) { // CMS Admin only
            $rtn .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"645\" align=\"center\">\n";
            $rtn .= "<tr><td align=\"right\">";
            if ($VaultComplete == "Y") {
                $rtn .= "<hr size=\"1\">";
                $rtn .= '<a href="#" onclick="processVault(\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'VC\',\'N\',\'Are you sure you want re-activate the vault?\');return false;">Activate Vault</a>';
            } else {
                $rtn .= '<a href="#" onclick="processVault(\'' . $_POST['wotcID'] . '\',\'' . $_POST['ApplicationID'] . '\',\'VC\',\'Y\',\'Are you sure you want mark the vault complete?\');return false;">Mark Vault Complete</a>';
            } // end VaultComplete
              // $rtn .= "<hr size=\"1\">";
            $rtn .= "</td></tr>\n";
            $rtn .= "</table>\n";
        } // end MASTERADMIN
        
        return $rtn;
    } // end function

    /**
     * @method  downloadVaultFile
     * @param   string $wotcID
     * @param   string $ApplicationID
     * @param   string $UpdateID
     * @return  string
     */
    public function downloadVaultFile($wotcID, $ApplicationID, $UpdateID)
    {
        $query  =   "SELECT * FROM ApplicantVault";
        $query  .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid AND UpdateID = :updateid";
        $params =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID, ':updateid'=>$UpdateID);
        $AV     =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $filename = WOTC_DIR . "vault/" . $wotcID . "/applicantvault/";
        $filename .= $ApplicationID . "-" . $AV['UpdateID'] . "-" . $AV['FileName'] . "." . $AV['FileType'];
        
        if (file_exists($filename)) {
            
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            exit();
        } else {
            
            $rtn = "<div style=\"margin:60px;font-family:Arial,Helvetica;\">\n";
            $rtn .= "File not found";
            $rtn .= "</div>\n";
            
            return $rtn;
        }
    } // end function
} // end Class
