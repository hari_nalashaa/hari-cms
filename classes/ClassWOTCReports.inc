<?php
/**
 * @class		WOTCReports
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCReports
{
    private static $ip = "reports.irecruit-us.com";

    private static $key = "E123WOT456CMS789"; // 16 Character Key
    private static $iv = "CMS1IRECRUITWOTC"; // It should be exactly 16 bytes
    private static $cipher = "aes-128-cbc";
    private static $cipher_key_len = 16; //128 bits
    
    public static $encrypttext = "";

    public static $decrypttext = "";

    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    public function __construct() {
        
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );

        $query  =   "SELECT CONCAT(DATE_FORMAT(DATE_ADD(NOW(),INTERVAL 1 DAY),'%Y%m%d%h%i%s'),'-0400') as ExpDate";
        $DT     =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query);
        
        if (!defined('JASPERSOFT_SERVER')) {
            define('JASPERSOFT_SERVER', 'https://' . self::$ip . '/jasperserver-pro/client/visualize.js');
        }
        
        $token  =   "u=wotc|r=ROLE_WOTC_CUSTOMER|o=organization_1|exp=" . $DT['ExpDate'];
        
        self::$encrypttext = self::encrypt(self::$key, self::$iv, $token);
        
    } // end function
    
    /**
     * @method      sortDashboardForm
     * @return      string
     */
    public function sortDashboardForm()
    {
        $rtn =  "";
       
        $LST =  array("FromDate", "ToDate");

        $rtn .= G::Obj('DateHelper')->dynamicCalendar($LST, '');
        
        $rtn .= '<div style="margin-bottom:10px;padding-bottom:10px;border-bottom:1px solid #000;display:none;" id="ReportControlsDashboard">' . "\n";
        $rtn .= '<form id="formDashboard" method="POST">' . "\n";
        
        $rtn .= '<div style="float:left;margin-bottom:10px;">' . "\n";
        $rtn .= "<strong style=\"font-size:14pt;\">Dashboard</strong>\n";
        $rtn .= '</div>' . "\n";
        
        $rtn .= '<div style="clear:both;"></div>' . "\n";
        
        $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;\n";
        $rtn .= "From Date: \n";
        $rtn .= "<input type=\"text\" id=\"FromDate\" name=\"FromDate\" value=\"\" size=\"10\">\n";
        $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;\n";
        $rtn .= "To Date: \n";
        $rtn .= "<input type=\"text\" id=\"ToDate\" name=\"ToDate\" value=\"\" size=\"10\">\n";
        
        $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;\n";
        $rtn .= "<input type=\"submit\" name=\"submit\" value=\"Filter Report\">\n";
        
        $rtn .= "</form>\n";
        
        $rtn .= '</div>' . "\n";
        
        $rtn .= '<div style="float:left;">' . "\n";
        $rtn .= '<button type="button" style="margin-right:10px;display:none;" id="previousPageDashboard"><--Previous Page</button>' . "\n";
        $rtn .= '</div>' . "\n";
        
        $rtn .= '<div style="float:left;">' . "\n";
        $rtn .= '<button type="button" style="margin-right:10px;display:none;" id="nextPageDashboard">Next Page--></button>' . "\n";
        $rtn .= '</div>' . "\n";
        
        $rtn .= '<div style="float:right;">' . "\n";
        $rtn .= '<button type="button" style="display:none;" id="exportDashboard">Print / Export PDF</button>' . "\n";
        $rtn .= '</div>' . "\n";
        
        $rtn .= '<div style="clear:both;"></div>' . "\n";

        return $rtn;

    } // end function
    
    /**
     * @method      callJaspersoft
     * @return      string
     */
    public function callJaspersoft()
    {
        $rtn = '<script src="' . JASPERSOFT_SERVER . '"></script>' . "\n";
        $rtn .= '<div id="container"></div>' . "\n";
        
        return $rtn;
    } // end function
    
    /**
     * Encrypt data using AES Cipher (CBC) with 128 bit key
     * 
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $iv - initialization vector
     * @param type $data - data to encrypt
     * @return encrypted data in base64 encoding with iv attached at end after a :
     */
    public static function encrypt($key, $iv, $data) {
        
        if (strlen($key) < self::$cipher_key_len) {
            $key = str_pad("$key", self::$cipher_key_len, "0"); //0 pad to len 16
        } else if (strlen($key) > self::$cipher_key_len) {
            $key = substr($str, 0, self::$cipher_key_len); //truncate to 16 bytes
        }

        $encodedEncryptedData   =   base64_encode(openssl_encrypt($data, self::$cipher, $key, OPENSSL_RAW_DATA, $iv));
        $encodedIV              =   base64_encode($iv);
        $encryptedPayload       =   $encodedEncryptedData.":".$encodedIV;

        return $encryptedPayload;

    }

    /**
     * Decrypt data using AES Cipher (CBC) with 128 bit key
     * 
     * @param type $key - key to use should be 16 bytes long (128 bits)
     * @param type $data - data to be decrypted in base64 encoding with iv attached at the end after a :
     * @return decrypted data
     */
    public static function decrypt($key, $data) {
        if (strlen($key) < self::$cipher_key_len) {
            $key = str_pad("$key", self::$cipher_key_len, "0"); //0 pad to len 16
        } else if (strlen($key) > self::$cipher_key_len) {
            $key = substr($str, 0, self::$cipher_key_len); //truncate to 16 bytes
        }

        $parts          =   explode(':', $data); //Separate Encrypted data from iv.
        $decryptedData  =   openssl_decrypt(base64_decode($parts[0]), self::$cipher, $key, OPENSSL_RAW_DATA, base64_decode($parts[1]));

        return $decryptedData;
    }
    
    /**
     * @method      pkcs5_pad
     * @param       string $text
     * @param       number $blocksize
     * @return      string
     */
    private static function pkcs5_pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

} // end Class

?>
