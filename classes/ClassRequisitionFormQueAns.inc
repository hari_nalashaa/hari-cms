<?php
/**
 * @class		RequisitionFormQueAns
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 * @tutorial    Here $AppQueAnswers is a reference variable.
 *              $AppQueAnswers array keys follows below sequence
 */

class RequisitionFormQueAns {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method     getRequisitionFormQueAnswersInfo
	 * @param      string $OrgID
	 * @param      string $APPDATA
	 * @return     array
	 */
	function getRequisitionFormQueAnswersInfo($OrgID, $RequisitionFormID, $RequestID) {
		
	    $APPDATA	=	G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);
	    
	    // Bind the columns
	    $columns   	=   "QuestionID, Question, QuestionTypeID, value";
	    // Get WebForm Questions Information
	    $results   	=   $this->getRequisitionFormQuestionsInfo($columns, $OrgID, $RequisitionFormID, "QuestionOrder");

	    $ques_list	=	G::Obj('RequisitionQuestions')->getAdditionalRequisitionFields($OrgID, $RequisitionFormID);
	    
	    if($results['count'] > 0) {
	        
	        foreach ($results['results'] as $FQ) {

	        	if(in_array($FQ ['QuestionID'], $ques_list))
	        	{
	        		// format answers
	        		if ($FQ ['QuestionTypeID'] == 1) {
	        			$date_answer = '';
	        			if($APPDATA [$FQ ['QuestionID'] . 'from'] != "" && $APPDATA [$FQ ['QuestionID'] . 'to'] != "") {
	        				$date_answer = "from " . $APPDATA [$FQ ['QuestionID'] . 'from'] . " to " . $APPDATA [$FQ ['QuestionID'] . 'to'];
	        			}
	        		
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $date_answer;
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   1;
	        		} // end from, to dates question
	        		else if ($FQ ['QuestionTypeID'] == 3) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   3;
	        		} // end pulldown question
	        		else if ($FQ ['QuestionTypeID'] == 5) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   5;
	        		} // end textarea question
	        		else if ($FQ ['QuestionTypeID'] == 6) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   6;
	        		} // end text box question
	        		else if ($FQ ['QuestionTypeID'] == 9) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getAnswer9($OrgID, $FQ ['QuestionID'], $APPDATA [$FQ ['QuestionID']], $FQ['value']);
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        		} // end instructions
	        		else if ($FQ ['QuestionTypeID'] == 10) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   10;
	        		} // end date question
	        		else if ($FQ ['QuestionTypeID'] == 13) {
	        		
	        			if(G::Obj('GenericLibrary')->isJSON($APPDATA [$FQ['QuestionID']]) === true) {
	        				$Answer13  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer13[0], $Answer13[1], $Answer13[2], '' );
	        			}
	        			else if(is_string($APPDATA [$FQ['QuestionID']]) && $APPDATA [$FQ['QuestionID']] != "") {
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $APPDATA [$FQ['QuestionID']];
	        			}
	        			else {
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA [$FQ['QuestionID'] . '1'], $APPDATA [$FQ['QuestionID'] . '2'], $APPDATA [$FQ['QuestionID'] . '3'], '' );
	        			}
	        			 
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   13;
	        		} // end phone without extension
	        		else if ($FQ ['QuestionTypeID'] == 14) {
	        			 
	        			if(G::Obj('GenericLibrary')->isJSON($APPDATA [$FQ['QuestionID']]) === true) {
	        				$Answer14  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer14[0], $Answer14[1], $Answer14[2], $Answer14[3] );
	        			}
	        			else if(is_string($APPDATA [$FQ['QuestionID']]) && $APPDATA [$FQ['QuestionID']] != "") {
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $APPDATA [$FQ['QuestionID']];
	        			}
	        			else {
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA [$FQ['QuestionID'] . '1'], $APPDATA [$FQ['QuestionID'] . '2'], $APPDATA [$FQ['QuestionID'] . '3'], $APPDATA [$FQ['QuestionID'] . 'ext'] );
	        			}
	        		
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   14;
	        		} // end phone with extension
	        		else if ($FQ ['QuestionTypeID'] == 15) {
	        			if ($_GET ['emailonly'] == 'Y') {
	        				$AppQueAnswers[$FQ ['QuestionID']]['Question']             =   $FQ['Question'];
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']               =   'XXX-XX-XXXX';
	        				$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']       =   15;
	        			} else {
	        				$Answer15  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
	        				$AppQueAnswers[$FQ ['QuestionID']]['Question']             =   $FQ['Question'];
	        				$AppQueAnswers[$FQ ['QuestionID']]['Answer']               =   $Answer15[0] . '-' . $Answer15[1] . '-' . $Answer15[2];
	        				$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']       =   15;
	        			}
	        		}
	        		else if ($FQ ['QuestionTypeID'] == 17) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   17;
	        		} // end date
	        		else if (($FQ ['QuestionTypeID'] == 18)) {
	        			$Answer18  = json_decode($APPDATA[$FQ ['QuestionID']], true);
	        			foreach ($Answer18 as $Answer18Key=>$Answer18Value) {
	        				$APPDATA[$Answer18Key] =   $Answer18Value;
	        			}
	        			$APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer18);
	        			 
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        		} // end instructions
	        		else if ($FQ ['QuestionTypeID'] == 24) {
	        			$date_answer = '';
	        			if($APPDATA [$FQ ['QuestionID'] . 'from'] != "" && $APPDATA [$FQ ['QuestionID'] . 'to'] != "") {
	        				$date_answer = "from " . $APPDATA [$FQ ['QuestionID'] . 'from'] . " to " . $APPDATA [$FQ ['QuestionID'] . 'to'];
	        			}
	        		
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $date_answer;
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   24;
	        		}
	        		else if ($FQ ['QuestionTypeID'] == 60) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   60;
	        		}
	        		else if ($FQ['QuestionTypeID'] == 30) { // Signature
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   'Electronically by symbol ' . $APPDATA['captcha'] . ' on ' . $APPDATA[$FQ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   30;
	        		}
	        		else if ($FQ ['QuestionTypeID'] == 99) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   99;
	        		} // end pulldown question
	        		else if ($FQ ['QuestionTypeID'] == 100) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   @unserialize($APPDATA [$FQ ['QuestionID']]);
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   100;
	        		} // end skills rating
	        		else if($FQ ['QuestionTypeID'] == 120) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   @unserialize($APPDATA [$FQ ['QuestionID']]);
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   120;
	        		} // end daily shifts
	        		else if ($FQ ['QuestionTypeID'] == 1818) {
	        			$Answer1818  = json_decode($APPDATA[$FQ ['QuestionID']], true);
	        			foreach ($Answer1818 as $Answer1818Key=>$Answer1818Value) {
	        				$APPDATA[$Answer1818Key] =   $Answer1818Value;
	        			}
	        			$APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer1818);
	        			 
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        		} // end instructions
	        		else if ($FQ ['value']) {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        		}
	        		else {
	        			$AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	        			$AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	        			$AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        		}
	        		 
	        	}	        	
	        }
	         
	    }
	    
	    return $AppQueAnswers;
	}
	
	/**
	 * @method     getValueList
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getValueList($value) {
	
	    $values_list    =   array();
	    $que_values     =   explode ( '::', $value );
	
	    foreach ( $que_values as $v => $q ) {
	        list ( $vv, $qq )   =   explode ( ":", $q);
	        $values_list[$vv]   =   $qq;
	    }
	
	    return $values_list;
	}
	
	/**
	 * @method     getAnswer9
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer9($OrgID, $QuestionID, $Answer, $QueValue) {
	     
        $skills_info        =   json_decode($Answer, true);
        $ques_values        =   $this->getValueList($QueValue);
	
        $rtn                =   '';
	    foreach($skills_info as $skills_key=>$skills_value) {
	        $values  =  array_values($skills_value);
	        $rtn    .=  '&#8226;&nbsp;<strong>' . $ques_values[$values[0]] . ", " . $values[1] . " yrs. (" . $values[2].")</strong><br>";
	    }
	
	    return $rtn;
	}
	
	/**
	 * @method     getAnswer18
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer18($OrgID, $QuestionID, $Answer, $QueValue) {
	
        $info   =   json_decode($Answer, true);
        $values =   $this->getValueList($QueValue);
        
        $rtn    =   '';
	    foreach($info as $key=>$value) {
	        $rtn    .=  '&#8226;&nbsp;' . $values[$value];
	    }
	
	    return $rtn;
	}
	
	/**
	 * @method     getAnswer1818
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer1818($OrgID, $QuestionID, $Answer, $QueValue) {
	
	    $info      =   json_decode($Answer, true);
	    $values    =   $this->getValueList($QueValue);
	
	    $rtn       =   '';
	    foreach($info as $key=>$value) {
	        $rtn .= '&#8226;&nbsp;' .  $values[$value] . "\n";
	    }
	    	
	    return $rtn;
	}
	
	/**
	 * @method     getDisplayValue
	 * @param      string $answer
	 * @param      string $displaychoices
	 * @return     string
	 */
	function getDisplayValue($answer, $displaychoices) {
	    $rtn = '';
	
	    if ($answer != '') {
	        $Values = explode ( '::', $displaychoices );
	        foreach ( $Values as $v => $q ) {
	            list ( $vv, $qq ) = explode ( ":", $q, 2 );
	            if ($vv == $answer) {
	                $rtn .= $qq;
	            }
	        }
	    }
	    
	    return $rtn;
	    
	} // end of function

	
	/**
	 * @method     getMultiAnswer
	 * @param      array $APPDATA
	 * @param      string $id
	 * @param      string $displaychoices
	 * @param      string $questiontype
	 * @return     string
	 */
	function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
	    $rtn = '';
	    $val = '';
	    $lex = '';
	
	    if (($id == 'daysavailable') || ($id == 'typeavailable')) {
	
	        $le = ', ';
	        $li = '';
	        $lecnt = - 2;
	    } else {
	
	        $le = '<br>';
	        $li = '&#8226;&nbsp;';
	        $lecnt = - 4;
	    }
	
	    if($questiontype == 18 || $questiontype == 1818) {
	        $displaychoices_vals = explode ( '::', $displaychoices );
	        $cnt   =   count($displaychoices_vals);
	    }
	    else {
	        $cnt   =   $APPDATA [$id . 'cnt'];
	    }
	
	    for($i = 1; $i <= $cnt; $i ++) {
	
	        $val = $this->getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
	        if ($val) {
	            $rtn .= $li . $val;
	            $lex = 'on';
	            $val = '';
	        }
	
	        if ($questiontype == 9) {
	            	
	            $val = $APPDATA [$id . '-' . $i . '-yr'];
	            if ($val) {
	                $rtn .= ',&nbsp;' . $val . ' yrs. ';
	                $lex = 'on';
	                $val = '';
	            }
	            	
	            $val = $APPDATA [$id . '-' . $i . '-comments'];
	            if ($val) {
	                $rtn .= '&nbsp;(' . $val . ') ';
	                $lex = 'on';
	                $val = '';
	            }
	        } // end questiontype
	
	        if ($lex == 'on') {
	            $rtn .= $le;
	            $lex = '';
	        }
	    }
	
	    $rtn = substr ( $rtn, 0, $lecnt );
	
	    return $rtn;
	} // end of function

	/**
	 * @method     getRequisitionFormQuestionsInfo
	 * @param      string $columns
	 * @param      string $OrgID
	 * @param      string $RequisitionFormID
	 * @return     array
	 */
	public function getRequisitionFormQuestionsInfo($columns = "*", $OrgID, $RequisitionFormID, $order_by = "") {
		
		$columns  		=	$this->db->arrayToDatabaseQueryString ( $columns );
		$params   		=   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
		
		$sel_que_info	.=	"SELECT $columns FROM RequisitionQuestions";
		$sel_que_info 	.=	" WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID";
		$sel_que_info 	.=	" AND QuestionOrder > 0";
		
		if($order_by != "") $sel_que_info .= " ORDER BY " . $order_by;

		$res_que_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_que_info, array($params) );
	
		return $res_que_info;
	    	     
	}
	
}
