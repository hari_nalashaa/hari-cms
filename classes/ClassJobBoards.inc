<?php
/**
 * @class		JobBoards
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class JobBoards {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method	getJobBoardsInfo
	 * @param	string $OrgID, $MultiOrgID
	 * @return	numeric array
	 */
	function getJobBoardsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_job_boards_info = "SELECT $columns FROM JobBoards";
	
		if(count($where_info) > 0) {
			$sel_job_boards_info .= " WHERE " . implode(" AND ", $where_info);
		}
		if($order_by != "") $sel_job_boards_info .= " ORDER BY " . $order_by;
	
		$res_job_boards_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_job_boards_info, $info );
	
		return $res_job_boards_info;
	}
	
	/**
	 * @method		insJobBoardsInfo
	 * @param		array $info
	 * @return		array
	 */
	function insJobBoardsInfo($info, $on_update = '', $update_info = array()) {
		
		$ins_job_boards_info = $this->db->buildInsertStatement('JobBoards', $info);
	
		$insert_statement = $ins_job_boards_info ["stmt"];
		if ($on_update != '') {
			$insert_statement .= $on_update;
			if (is_array ( $update_info )) {
				foreach ( $update_info as $upd_key => $upd_value ) {
					$ins_job_boards_info ["info"][0][$upd_key] = $upd_value;
				}
			}
		}
	
		$res_job_boards_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $ins_job_boards_info["info"] );
	
		return $res_job_boards_info;
	}
	
	/**
	 * @method	delUserPortalInfo
	 * @param	$where_info, $info
	 */
	function delJobBoardsInfo($where_info = array(), $info = array()) {
		
		$del_job_boards_info = "DELETE FROM JobBoards";
		if (count ( $where_info ) > 0) {
			$del_job_boards_info .= " WHERE " . implode ( " AND ", $where_info );
		}
		$res_job_boards_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_job_boards_info, $info );
	
		return $res_job_boards_info;
	}
	
	
	/**
	 * @method	updJobBoardsInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updJobBoardsInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_job_boards_info = $this->db->buildUpdateStatement('JobBoards', $set_info, $where_info);
		$res_job_boards_info = $this->db->getConnection("IRECRUIT")->update($upd_job_boards_info, $info);
	
		return $res_job_boards_info;
	}
	
	
	/**
	 * @method		insJobGroupCodesInfo
	 * @param		array $info
	 * @return		array
	 */
	function insJobGroupCodesInfo($info) {
		
		$ins_job_group_codes = $this->db->buildInsertStatement('JobGroupCodes', $info);
		$res_job_group_codes = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_job_group_codes["stmt"], $ins_job_group_codes["info"] );
	
		return $res_job_group_codes;
	}
	
	
	/**
	 *
	 * @method	delJobGroupCodesInfo
	 * @param	$OrgID
	 * @return	string
	 */
	function delJobGroupCodesInfo($OrgID) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID);
	
		$del_job_group_codes = "DELETE FROM JobGroupCodes WHERE OrgID = :OrgID";
		$res_job_group_codes = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_job_group_codes, array($params) );
	
		return $res_job_group_codes;
	}
}
?>
