<?php
/**
 * @class		FormQuestionsDetails
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormQuestionsDetails {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      getFormQuestionDetailsInfo
     * @param       string $columns
     * @param       string $OrgID
     * @param       string $QuestionID
     * @return      array
     */
    public function getFormQuestionDetailsInfo($columns = "*", $OrgID, $FormID, $QuestionID) {
        
        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        
        $params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":QuestionID"=>$QuestionID);
        $sel_info   =   "SELECT $columns FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND QuestionID = :QuestionID";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
        
        return $res_info;
    }

    /**
     * @method      getFormQuestionInfoBySecIDQuesID
     * @param       string $columns
     * @param       string $OrgID
     * @param       string $QuestionID
     * @return      array
     */
    public function getFormQuestionInfoBySecIDQuesID($columns = "*", $OrgID, $FormID, $SectionID, $QuestionID) {
        
        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        
        $params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$SectionID, ":QuestionID"=>$QuestionID);
        $sel_info   =   "SELECT $columns FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND SectionID = :SectionID AND QuestionID = :QuestionID";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
        
        return $res_info;
    }
    
    /**
     * @method      getActiveFormQuestionsCountByFormID
     * @param       string $columns
     * @param       string $OrgID
     * @param       string $FormID
     * @return      array
     */
    public function getActiveFormQuestionsCountByFormID($OrgID, $FormID, $SectionID) {
    
    	$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$SectionID);
    	$sel_info   =   "SELECT COUNT(QuestionID) As FormQuestionsCount FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND SectionID = :SectionID AND Active = 'Y'";
    	$res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    	$count		=	$res_info['FormQuestionsCount'];
    	 
    	return $count;
    }
}
?>
