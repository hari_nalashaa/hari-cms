<?php
/**
 * @class		WOTCNotFound
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCNotFound
{
    public $db;

    var $conn_string       =   "WOTC";

    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method      insNotFound
     */
    function insNotFound($state, $notes) {

        $query  =   "INSERT INTO NotFound";
        $query .=   " (State, Notes, EnteredDate)";
        $query .=   " VALUES (:state, :notes, now())";
        $params =   array(
                        ':state'    =>  $state,
                        ':notes'    =>  $notes
                    );
        $result =   $this->db->getConnection( $this->conn_string )->insert($query, array($params));
        
        return $result;
    }
}    
