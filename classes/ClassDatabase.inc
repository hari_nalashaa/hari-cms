<?php
/**
 * @class 			Database
 * 
 * @tutorial 		Class will contain all common database operations and
 * 			 		this class is inheriting class as a base class
 * 			 		for DatabaseConnection's.
 * 
 *                  Database Operations:: database operations example calls are given at 
 *                  the top of the Model Classes like (ClassApplicants.inc) in Comments block.
 *                  
 *                  Please refer any model class methods to bind the parameters.
 *                  We are using PDO library to do the database operations. 
 *
 */

class Database {
	
    use IrecruitConnection, UserPortalConnection, WotcConnection, AdminConnection;
    
	//static variable to set the instance of this class
	private static $_instance;
	
	//default connection variable to set the database object
	private $_connection;

	//errors information
	private $error_info;
	
	//prepared statement
	private $stmt;
	
	//query_statement
	private $query_statement;
	
	//flag to print database errors
	public $print_errors = false;
	
	//flag to log error messages
	public $log_errors = true;
	
	//set debug parameters
	public $debug_param_flag = true;
	
	//set debug trace information
	public $trace_info_debug;
	
	//Set function name
	public $function_name;
	
	//Set class name
	public $class_name;
	
	//Set parameters array
	public $bind_params = array();
	
	/**
	 * @method		getInstance
	 * @return		Object of this class
	 */
	public static function getInstance()
	{
		if (!self::$_instance) { 
			// If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	/**
	 * @method		prepare
	 * @param		string $statement
	 */
	private function prepare($statement) {
		global $OrgID;
		
		//Set error information to empty
		$this->bind_params        =   array();
		$this->error_info         =   NULL;
		$this->query_statement    =   NULL;
		
		//Assign the raw statement to query_statement variable
		$this->query_statement    =   $statement;
		
		//All the prepared startements are executed based on this stmt
		//variable so making it null for new query to avoid the unwanted results
		$this->stmt	= NULL;

		try {
			//Prepare the statement and assign the prepared result to $this->stmt
			$this->stmt = $this->_connection->prepare($statement);
		}
		// Catch any errors
		catch(PDOException $e) {
			$this->error_info = $e->getMessage();

			$error_message   =   "Function: " . $this->function_name . ", Classname: ".$this->class_name . ", FileName: " . $_SERVER['REQUEST_URI'];
			//$error_message   .=  ", Server Info: "  . "<pre>" . print_r($_SERVER, true) . "</pre>";//Enable when needed
			$error_message   .=  "<pre>" . print_r($this->trace_info_debug, true) . "</pre>";
			$error_message   .=  "<br>" . $this->query_statement . "<br>" . $this->error_info . "<br>";
				
			if($this->print_errors == true) {
				echo $error_message;
			}
			
			if($this->log_errors == true && $this->error_info != "") {
				//Log error messages
				//Logger::writeMessage(ROOT."logs/query-errors/".date('Y-m-d')."_db_errors.log", $error_message, 'a+', true, '', $e);
			}
		}
	}
	
	/**
	 * @method		bind
	 * @param 		$param, $value, $type
	 */
	private function bind($param, $value, $type = null) {

		if(is_null($value) || $value == NULL) $value = '';
		
		if (is_null($type)) {
			switch (true) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}
		
		if($this->stmt !== NULL) {
			$this->bind_params[] = array("param"=>$param, "value"=>$value, "type"=>gettype($value));
			try {
				//Bind the parameter based on data type
				$this->stmt->bindValue($param, $value, $type);
			}
			// Catch any errors
			catch(PDOException $e) {
				$this->error_info = $e->getMessage();
				$error_message  =   "Function: " . $this->function_name . ", Classname: ".$this->class_name . ", FileName: " . $_SERVER['REQUEST_URI'];
				//$error_message .=   ", Server Info: "  . "<pre>" . print_r($_SERVER, true) . "</pre>"; //Enable when it is needed
				$error_message .=   "<pre>" . print_r($this->trace_info_debug, true) . "</pre>";
				$error_message .=   "<br>" . $this->query_statement . "<br>" . $this->error_info . "<br>";
				
				if($this->print_errors == true) {
					echo $error_message;
				}
				
				if($this->log_errors == true && $this->error_info != "") {
					//Log error messages
					//Logger::writeMessage(ROOT."logs/query-errors/".date('Y-m-d')."_db_errors.log", $error_message, 'a+', true, '', $e);
				}
			}
			
		}
		
	}
	
	/**
	 * @method		resultSetByQueryType
	 * @return		array
	 * @param		string $query_type, $fetch_type
	 */
	private function resultSetByQueryType($query_type, $fetch_type) {
		
		$result	= array();
		
		if($query_type == "INSERT") {
			$last_insert_id = $this->_connection->lastInsertId();
				
			if($this->stmt !== NULL) {
				$row_count = $this->stmt->rowCount();
			}
			else {
				$row_count = 0;
			}
		
			$result = array("insert_id" => $last_insert_id, "affected_rows" => $row_count, "error_info"=>$this->error_info);
		}
		if($query_type == "UPDATE" || $query_type == "DELETE") {
			if($this->stmt !== NULL) $row_count = $this->stmt->rowCount();
			$result = array("affected_rows"=>$row_count, "error_info"=>$this->error_info);
		}
		if($query_type == "SELECT") {
			
			if($fetch_type	==	'fetchRow') {
				if($this->stmt !== NULL) $result = $this->stmt->fetch(PDO::FETCH_NUM);
			}
			if($fetch_type	==	'fetchAssoc') {
				if($this->stmt !== NULL) $result = $this->stmt->fetch(PDO::FETCH_ASSOC);
			}
			if($fetch_type	==	'fetchAllAssoc') {
				if($this->stmt !== NULL) {
					$rows_list	= $this->stmt->fetchAll(PDO::FETCH_ASSOC);
					$count	= $this->stmt->rowCount();
					$result	= array("results" => $rows_list, "count" => $count);
				}
			}
		}
		
		return $result;
	}
	
	/**
	 * @method		executePrepareStatement
	 * @param		$query_type, $records_list, $fetch_type
	 */
	private function executePrepareStatement($query_type, $records_list = array(), $fetch_type)  {
		$records_list_count = count($records_list);
		
		//If array of parameters exist, have to bind it to query
		if($records_list_count > 0) {
			
			for($r = 0; $r < $records_list_count; $r++) {
					
				$fields_list = $records_list[$r];
				//Get all fields_list keys
				if(is_array($fields_list)) {
					$keys_list = array_keys($fields_list);
				}
					
				//Check if the parameters exist to set in query
				if(count($keys_list) > 0) {
					$bind_param_index = "True";
						
					//If key is string
					if(substr($keys_list[0], 0, 1) == ":") {
						$bind_param_index = "False";
					}

					$i = 1;
					if(is_array($fields_list)) {
					    foreach($fields_list as $field_key=>$field_info) {
					        $param_index = $i;
					        if($bind_param_index == "False") $param_index = $field_key;
					    
					        if(is_array($field_info)) {
					            if(count($field_info) == 2) {
					                $this->bind($param_index, $field_info[0], $field_info[1]);
					            }
					            else {
					                $this->bind($param_index, $field_info[0]);
					            }
					        }
					        else {
					            $this->bind($param_index, $field_info);
					        }
					    
					        $i++;
					    }
					}						
				}
				
				//Execute the prepared statement with binded parameters
				$this->execute();
				
				//Function to return result set array based on Query Type
				$result = $this->resultSetByQueryType($query_type, $fetch_type);
				
				//Return if the array has only one record
				if($records_list_count == 1) {
					return $result;
				}
				else {
					$results[] = $result;
				}
			}
			
			//If we are fetching, inserting, updating or deleting multiple records 
			//we are returning the result of each statement
			return $results;
		}
		else {
			//Execute the prepared statement with binded parameters
			$this->execute();
			
			//Function to return result set array based on Query Type
			$result = $this->resultSetByQueryType($query_type, $fetch_type);
			
			return $result;
		}
		
	}
	
	/**
	 * @method		getConnection
	 * @tutorial	It will check the DatabaseString based on that,
	 * 				it will return the DatabaseConnectionObject
	 * @param 		string $DatabaseConnectionString
	 * @return 		Database Object
	 */
	public function getConnection($database_connection_string) {
		
		//It will return the IRECRUIT Object
		if($database_connection_string == "USERPORTAL") {
			$this->_connection = $this->getUserPortalConnectionObject();
		}
	
		//It will return the USERPORTAL Object
		if($database_connection_string == "IRECRUIT") {
			$this->_connection = $this->getIrecruitConnectionObject();
		}

		//It will return the WOTC Object
		if($database_connection_string == "WOTC") {
		    $this->_connection = $this->getWotcConnectionObject();
		}

		//It will return the ADMIN Object
		if($database_connection_string == "ADMIN") {
		    $this->_connection = $this->getAdminConnectionObject();
		}
		
		return Database::getInstance();
	}
	
	/**
	 * @method		setDebugParameters
	 */
	public function setDebugParameters($func_name = "", $class_name = "") {
		//Set function name
		$this->function_name = $func_name;
		//Set class name
		$this->class_name = $class_name;
	}
	
	/**
	 * @method		setDebugTraceInformation
	 */
	public function setDebugTraceInformation($debug_trace_information) {
		//Set function name
		$this->trace_info_debug = $debug_trace_information;
	}
	
	/**
	 * @tutorial	It will fetch the values based on query, 
	 * 				and it will return numerical array
	 * @method		fetchRow
	 * @param		query $statement
	 * @param		object $db_connection_string
	 * @return		numeric array
	 * 
	 */
	public function fetchRow($statement, $records_list = array()) {
		//Prepare the statement
		$this->prepare($statement);
		
		//Execute query and fetch row
		return $this->executePrepareStatement('SELECT', $records_list, 'fetchRow');
	}
	
	/**
	 * @tutorial	It will fetch the values based on query, 
	 * 				and it will return associate array
	 * @method		fetchAssoc
	 * @param		query $statement
	 * @param		object $records_list
	 * @return		associative array
	 */
	public function fetchAssoc($statement, $records_list = array()) {
		//Prepare the statement
		$this->prepare($statement);
		
		//Execute query and fetch row
		return $this->executePrepareStatement('SELECT', $records_list, 'fetchAssoc');
	}
	
	/**
	 * @method		fetchAllAssoc
	 * @param		query $statement
	 * @param		object $records_list
	 * @return		associative array
	 */
	public function fetchAllAssoc($statement, $records_list = array()) {
		//Prepare the statement
		$this->prepare($statement);

		//Execute query and fetch row
		return $this->executePrepareStatement('SELECT', $records_list, 'fetchAllAssoc');
	}
	
	/**
	 * @method		insert
	 * @param		query $statement
	 * @param		object $records_list
	 * @return		associative array
	 */
	public function insert($statement, $records_list = array()) {
		//Prepare the statement
		$this->prepare($statement);
		
		//Execute query and fetch row based on column names
		return $this->executePrepareStatement('INSERT', $records_list, '');
	}
	
	
	/**
	 * @method		update
	 * @param		query $statement
	 * @param		object $records_list
	 * @return		associative array
	 */
	public function update($statement, $records_list = array()) {
		//Prepare the statement
		$this->prepare($statement);
		
		//Execute query and fetch row based on column names
		return $this->executePrepareStatement('UPDATE', $records_list, '');
	}
	
	
	/**
	 * @method		delete
	 * @param		query $statement
	 * @param		object $records_list
	 * @return		associative array
	 */
	public function delete($statement, $records_list = array()) {
		//Prepare the statement
		$this->prepare($statement);
		
		//Execute query and fetch row based on column names
		return $this->executePrepareStatement('DELETE', $records_list, '');
	}
	
	/**
	 * @method		execute
	 */
	private function execute() {
		global $OrgID;
		
		try {
			if($this->stmt !== NULL && $this->error_info == "") {
				//Log error messages
				//Logger::writeMessage(ROOT."logs/queries-list/". $OrgID . "_" . date('Y-m-d-H') . "_queries.log", $this->query_statement, "a+", false);
				$this->stmt->execute();
			}
			else {
				
				if($this->log_errors == true && $this->error_info != "") {
					$error_message = "Function: " . $this->function_name . ", Classname: ".$this->class_name . ", FileName: " . $_SERVER['REQUEST_URI'];
					//$error_message .= ", Server Info: "  . "<pre>" . print_r($_SERVER, true) . "</pre>";
					$error_message .= "<pre>" . print_r($this->trace_info_debug, true) . "</pre>";
					$error_message .= "<br>" . $this->query_statement . "<br>" . $this->error_info . "<br>";
					$error_message .= "<pre>".print_r($this->bind_params, true).print_r($_REQUEST, true);

					//Log error messages
					//Logger::writeMessage(ROOT."logs/query-errors/".date('Y-m-d')."_db_errors.log", $error_message, 'a+', true, '', '');
				}
			}
		}
		catch(PDOException $e) {
			$this->error_info = $e->getMessage();
			$error_message = "Function: " . $this->function_name . ", Classname: ".$this->class_name . ", FileName: " . $_SERVER['REQUEST_URI'];
			//$error_message .= ", Server Info: "  . "<pre>" . print_r($_SERVER, true) . "</pre>";   //Enable when needs it.
			$error_message .= "<pre>" . print_r($this->trace_info_debug, true) . "</pre>";
			$error_message .= "<br>" . $this->query_statement . "<br>" . $this->error_info . "<br>";
			$error_message .= "<pre>".print_r($this->bind_params, true).print_r($_REQUEST, true);
			
			
			if($this->print_errors == true) {
				echo $error_message;
			}
			
			if($this->log_errors == true && $this->error_info != "") {
				//Log error messages
				//Logger::writeMessage(ROOT."logs/query-errors/".date('Y-m-d')."_db_errors.log", $error_message, 'a+', true, '', $e);
			}
		}
	}
	
	/**
	 * @method		rowCount
	 * @return		number
	 */
	public function rowCount(){
		return $this->stmt->rowCount();
	}
	
	/**
	 * @method		lastInsertId
	 * @return		number
	 */
	public function lastInsertId(){
		return $this->_connection->lastInsertId();
	}
	
	/**
	 * @method		query
	 * @param		query $statement
	 */
	public function query($statement) {
		//Prepare the statement
		$this->prepare($statement);
		
		//Execute query
		return $this->execute();
	}
	
	/**
	 * @method		beginTransaction
	 * @return		Transaction start object
	 */
	public function beginTransaction() {
		return $this->_connection->beginTransaction();
	}
	
	/**
	 * @method		endTransaction
	 * @return		Transaction end object
	 */
	public function endTransaction() {
		return $this->_connection->commit();
	}
	
	/**
	 * @method		cancelTransaction
	 * @return		Transaction cancelled object
	 */
	public function cancelTransaction() {
		return $this->_connection->rollBack();
	}
	
	/**
	 * @method		debugDumpParams
	 * @return		Sql statement with parameters
	 */
	public function debugDumpParams() {
		return $this->stmt->debugDumpParams();
	}
	
	/**
	 * @method		interPolateQuery
	 * @return		query statement
	 * @tutorial	Cutom function to print the query
	 * 			    i.e ready to execute
	 */
	public function interPolateQuery($params) {
		
		$query	=  $this->query_statement;
		
		$keys = array();
		$values = $params;
		$values_limit = [];
	
		$words_repeated = array_count_values(str_word_count($query, 1, ':_'));
	
		# build a regular expression for each parameter
		foreach ($params as $key => $value) {
			if (is_string($key)) {
				$keys[] = '/:'.$key.'/';
				$values_limit[$key] = (isset($words_repeated[':'.$key]) ? intval($words_repeated[':'.$key]) : 1);
			} else {
				$keys[] = '/[?]/';
				$values_limit = [];
			}
	
			if (is_string($value))
				$values[$key] = "'" . $value . "'";
	
			if (is_array($value))
				$values[$key] = "'" . implode("','", $value) . "'";
	
			if (is_null($value))
				$values[$key] = 'NULL';
		}
	
		if (is_array($values)) {
			foreach ($values as $key => $val) {
				if (isset($values_limit[$key])) {
					$query = preg_replace(['/:'.$key.'/'], [$val], $query, $values_limit[$key], $count);
				} else {
					$query = preg_replace(['/:'.$key.'/'], [$val], $query, 1, $count);
				}
			}
			unset($key, $val);
		} else {
			$query = preg_replace($keys, $values, $query, 1, $count);
		}
		unset($keys, $values, $values_limit, $words_repeated);
	
		return $query;
	}
	
	/**
	 * @method		arrayToDatabaseQueryString
	 * @param		$column_names - array of columns or string
	 * @return		string with column names
	 */
	public function arrayToDatabaseQueryString($column_names) {
	
		$columns		=	'';
	
		if(is_array($column_names)) {
			$columns	=	implode(", ", $column_names);
		}
		else if(is_string($column_names) && $column_names != "") {
			$columns	=	$column_names;
		}
		else {
			$columns	=	'*';
		}
	
		return $columns;
	}
	
	/**
	 * @method		buildInsertStatement
	 * @param		string $table_name
	 * @param		string $form_data
	 * @param		string $inbuilt_func
	 * @return		string
	 */
	function buildInsertStatement($table_name, $form_data, $inbuilt_func = array()) {
		
		$db_inbuilt_func  =   array("NOW()", "DATE(NOW())", "CURDATE()", "NULL", "DATE_ADD(NOW(), INTERVAL 10 SECOND)", "DATE_ADD(NOW(), INTERVAL 6 MONTH)", 'DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00")', 'DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 6 MONTH), "%Y-%m-%d 23:00:00")');
		$db_inbuilt_func  =   array_merge($db_inbuilt_func, $inbuilt_func);

		$form_keys        =   array();
		
		foreach ( $form_data as $fkey => $fvalue ) {
			if(is_string($fvalue)) $fvalue = trim($fvalue);
			if($fvalue != "0" && in_array($fvalue, $db_inbuilt_func)) {
				$form_keys[] = $fvalue;
			}
			else {
				$form_keys[] = ":".$fkey;
				$insert_info[":".$fkey] = $fvalue;
			}
		}
		
		// retrieve the keys of the array (column titles)
		$fields           =   array_keys ( $form_data );
		
		// build the query
		$insert_stmt      =   "INSERT INTO " . $table_name . " (`" . implode ( '`, `', $fields ) . "`) VALUES(" . implode ( ", ", $form_keys ) . ")";

		//inbuilt database functions to replace
		foreach ($db_inbuilt_func as $value) {
			$db_inbuilt_func_rep[] = str_replace("'", "", $value);
		}
		
		$insert_stmt      =   str_replace($db_inbuilt_func, $db_inbuilt_func_rep, $insert_stmt);
		
		$records_list[]   =   $insert_info;
		
		// run and return the query result resource
		return array("stmt"=>$insert_stmt, "info"=>$records_list);
	}
	
	/**
	 * @method		buildUpdateStatement
	 * @param		array $set_info
	 * @param		array $where_info
	 */
	function buildUpdateStatement($table_name, $set_info = array(), $where_info = array()) {
	
		// build the query
		$update_stmt = "UPDATE " . $table_name . " SET ";
		
		$set = '';
		$set = implode(", ", $set_info);
		
		$update_stmt .= $set;
		
		if(count($where_info) > 0) {
			$update_stmt .= " WHERE " . implode(" AND ", $where_info);
		}

		return $update_stmt;
	}
	
	/**
	 * @method		buildOnDuplicateKeyUpdateStatement
	 * @param		array $set_info
	 * @param		array $where_info
	 */
	function buildOnDuplicateKeyUpdateStatement($table_name, $info = array(), $skip = array(), $inbuilt_func = array()) {
	
        $db_inbuilt_func    =   array("NOW()", "DATE(NOW())", "CURDATE()", "NULL", "DATE_ADD(NOW(), INTERVAL 10 SECOND)", "DATE_ADD(NOW(), INTERVAL 6 MONTH)", 'DATE_FORMAT(NOW(), "%Y-%m-%d 00:00:00")', 'DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 6 MONTH), "%Y-%m-%d 23:00:00")');
        $db_inbuilt_func    =   array_merge($db_inbuilt_func, $inbuilt_func);
        
        $form_keys          =   array();
         
        $ins_info           =   $this->buildInsertStatement($table_name, $info);
        $ins_stmt           =   $ins_info["stmt"];
        
        $ins_stmt           .=  " ON DUPLICATE KEY UPDATE ";
        
        $update_info        =   array();
        foreach ( $info as $upd_key => $upd_value ) {
            if(!in_array($upd_key, $skip)) {
                if(in_array($upd_value, $db_inbuilt_func)) {
                	$ins_stmt   .=  " `$upd_key` = $upd_value, ";
                }
                else {
                    $ins_stmt   .=  " `$upd_key` = :U$upd_key, ";
                    $ins_info["info"][0][":U".$upd_key] = $upd_value;
                }
            }
        }
        
        $ins_stmt   =   trim($ins_stmt, ", ");
        
        // run and return the query result resource
        return array("stmt"=>$ins_stmt, "info"=>$ins_info["info"]);
	    
	}

	
	/**
	 * @method escape
	 * @param  $input
	 * @return string
	 */
	public static function escape($input) {
		return addcslashes($input, "\x00, \n, \r, \x5C, \x27, \x22, \x1A, \x2D");
	}
}
