<?php
/**
 * @class		TwilioConversationParticipantApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioConversationParticipantApi {
	
    public $db;
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           	=	Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method		fetchConversationParticipantInfo
     */
    public function fetchConversationParticipantInfo($OrgID, $conversation_id, $participant_id) {
    	
    	$account_info  =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client        =   new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
    		//Get Participant Info
    		$participant = $client->conversations->v1->conversations($conversation_id)
											    		->participants($participant_id)
											    		->fetch();
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully fetched the participant information", "Response"=>$participant);
    		 
    	} catch (TwilioException $e) {
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to fetch the conversation", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    
    }    
} // end Class
