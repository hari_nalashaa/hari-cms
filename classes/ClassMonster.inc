<?php
/**
 * @class		Monster
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Monster {
	
	//Production Keys
	var $ApiKey		= 	'';
	var $SecretKey	=	'';	

	/*
	//Development Keys
	var $ApiKey		= 	'EAAQREqkci5Ia3gYVNUIGO5lGG.nyL0jKDtzUSuz.0aalIg-';
	var $SecretKey	=	'B8CE90AA8FE83C9401419E2CED32CE8A321461B761458489CC5BEF0693ED2A93';
	*/
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
		
		if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com" 
			|| $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com") {
		     //Development Keys
		    $this->ApiKey		= 	'EAAQREqkci5Ia3gYVNUIGO5lGG.nyL0jKDtzUSuz.0aalIg-';
		    $this->SecretKey	=	'B8CE90AA8FE83C9401419E2CED32CE8A321461B761458489CC5BEF0693ED2A93';
		}
		else {
		    //Production Keys
		    $this->ApiKey		= 	'EAAQUo0VumxM1lDB2.bBybfIpA--';
		    $this->SecretKey	=	'B3994613111B586EBA233FBCFF0DD318582AA1B05AC08A5C1B7C7848F02169EF';
		}   
	}
	
	
	/**
	 * @method		getMonsterInfoByOrgID
	 * @param		$OrgID, $MultiOrgID
	 */
	public function getMonsterInfoByOrgID($OrgID, $MultiOrgID) {
		
		$params = array(":OrgID"=>$OrgID);
		
		$sel_mon_config = "SELECT * FROM MonsterConfiguration WHERE OrgID = :OrgID";
		$res_mon_config = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_mon_config, array($params));
		$numrowconfig = $res_mon_config['count'];
		$rowconfig = $res_mon_config['results'][0];

		//Overwrite Existing Data
		$rowconfig['ApiKey'] = $this->ApiKey;
		$rowconfig['SecretKey'] = $this->SecretKey;
		
		if($rowconfig['FlagApplyWithMonster'] ==  1) $flagawm = 'yes';
		else $flagawm = 'no';
		
		return array("numrows"=>$numrowconfig, "row"=>$rowconfig, "flagawm"=>$flagawm);
	}
	
	
	/**
	 * @method		getMonsterPostData
	 * @return		array
	 */
	public function getMonsterPostData($EmailAddress, $RequestID) {
		
		//Set parameters
		$params = array(":EmailAddress"=>$EmailAddress, ":JobRefID"=>$RequestID);
		$sel_applicant_info = "SELECT ID, CreatedTimeStamp
								FROM MonsterPostData
								WHERE EmailAddress = :EmailAddress 
								AND JobRefID = :JobRefID 
								ORDER BY `CreatedTimeStamp`
								DESC LIMIT 1";
		$row_applicant_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_applicant_info, array($params));
		
		return $row_applicant_info;
	}
	
	/**
	 * @method		insertMonsterInfoForOrg
	 * @param 		string $USERID
	 * @param 		string $OrgID
	 * @param 		string $MultiOrgID
	 * @param 		string $RecruiterReferenceUserName
	 * @param 		string $Password
	 * @param 		string $FreeRequisitions
	 * @param 		string $Duration
	 * @return	 	array
	 */
	public function insertMonsterInfoForOrg($USERID, $OrgID, $MultiOrgID, $RecruiterReferenceUserName, $Password, $FreeRequisitions, $Duration) {
		
		$params   =   array(
                          ":UserID"=>$USERID, 
                          ":RecruiterReferenceUserName"=>$RecruiterReferenceUserName,
                          ":Password"=>$Password, 
                          ":OrgID"=>$OrgID, 
                          ":MultiOrgID"=>$MultiOrgID, 
                          ":FreeRequisitions"=>$FreeRequisitions, 
                          ":Duration"=>$Duration
                        );
		
		$updateconfig = "INSERT INTO MonsterConfiguration
						 SET CreatedDate = NOW(),
						 UserID = :UserID,
						 RecruiterReferenceUserName = :RecruiterReferenceUserName,
						 Password = :Password,
					     OrgID = :OrgID,
					   	 MultiOrgID = :MultiOrgID,
					   	 FreeRequisitions = :FreeRequisitions,
					   	 FlagApplyWithMonster = '1',
					   	 PostType = 'POST',
					   	 Duration = :Duration";
		$resultconfig = $this->db->getConnection ( "IRECRUIT" )->insert($updateconfig, array($params));
		
		return $resultconfig;
	}

	/**
	 * @method		updateMonsterInfoForOrg
	 * @param 		string $OrgID
	 * @param 		string $MultiOrgID
	 * @param 		string $RecruiterReferenceUserName
	 * @param 		string $Password
	 * @param 		string $FreeRequisitions
	 * @param 		string $Awm
	 * @param 		string $Duration
	 * @param 		string $PostType
	 * @return 		array
	 */
	public function updateMonsterInfoForOrg($OrgID, $MultiOrgID, $RecruiterReferenceUserName, $Password, $FreeRequisitions, $Awm, $Duration, $PostType) {
		global $USERID;
		
		$params = array(
		                  ":RecruiterReferenceUserName"   =>  $RecruiterReferenceUserName,
						  ":Password"                     =>  $Password, 
		                  ":FreeRequisitions"             =>  $FreeRequisitions, 
						  ":FlagApplyWithMonster"         =>  $Awm, 
		                  ":Duration"                     =>  $Duration,
						  ":PostType"                     =>  $PostType, 
		                  ":UserID"                       =>  $USERID, 
						  ":OrgID"                        =>  $OrgID, 
		                  ":MultiOrgID"                   =>  $MultiOrgID
		              );
		
        $updateconfig   =   "UPDATE MonsterConfiguration
        			   	    SET 
                            ModifiedDate                =   NOW(),
                            RecruiterReferenceUserName  =   :RecruiterReferenceUserName,
                            Password                    =   :Password,
                            FreeRequisitions            =   :FreeRequisitions,
                            FlagApplyWithMonster        =   :FlagApplyWithMonster,
                            Duration                    =   :Duration,
                            PostType                    =   :PostType,
                            UserID                      =   :UserID		
                            WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
		$resultconfig = $this->db->getConnection ( "IRECRUIT" )->update($updateconfig, array($params));
		
		return $resultconfig;
	}
	
	/**
	 * @method		monsterJobCategories
	 * @return		array
	 */
	public function monsterJobCategories($key_cat = "no") {
		
		$seljobcat = "SELECT JobCategoryId, JobCategoryAlias FROM MonsterJobCategory GROUP BY JobCategoryId ORDER BY JobCategoryAlias ASC";
		$resjobcat = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($seljobcat);
		
		if($key_cat == "no") {
			if(is_array($resjobcat['results'])) {
				foreach($resjobcat['results'] as $rowjobcat) {
					$jobcategories[] = $rowjobcat;
				}
			}
		}
		if($key_cat == "yes") {
			if(is_array($resjobcat['results'])) {
				foreach($resjobcat['results'] as $rowjobcat) {
					$jobcategories[$rowjobcat['JobCategoryId']] = $rowjobcat['JobCategoryAlias'];
				}
			}
		}
		return $jobcategories;
	}
	
	/**
	 * @method		getMonsterJobCategoriesOccupations
	 * @return		array
	 */
	public function getMonsterJobCategoriesOccupations() {
	
		$seljoboccupations = "SELECT OccupationId, OccupationAlias FROM MonsterJobCategory ORDER BY OccupationAlias ASC";
		$resjoboccupations = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($seljoboccupations, array($params));
	
		if(is_array($resjoboccupations['results'])) {
			foreach($resjoboccupations['results'] as $rowjoboccupations) {
				$joboccupations[$rowjoboccupations['OccupationId']] = $rowjoboccupations['OccupationAlias'];
			}
		}
	
		return $joboccupations;
	}
	
	/**
	 * @method		jobOccupationsByCategory
	 * @param		$cat_id
	 * @return		array
	 */
	public function jobOccupationsByCategory($cat_id) {
		
		$params = array(":JobCategoryId"=>$cat_id);
		$seljoboccupations = "SELECT * FROM MonsterJobCategory WHERE JobCategoryId = :JobCategoryId ORDER BY OccupationAlias ASC";
		$resjoboccupations = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($seljoboccupations, array($params));
		
		if(is_array($resjoboccupations['results'])) {
			foreach($resjoboccupations['results'] as $rowjoboccupations) {
				$joboccupations[] = $rowjoboccupations;
			}	
		}
		
		return $joboccupations;
	}

	/**
	 * @method		monsterOrgRequisitions
	 * @param		$OrgID, $MultiOrgID
	 * @return		array
	 */
	public function monsterOrgRequisitions($OrgID, $MultiOrgID) {
		
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		
		$query = "SELECT ROL.OrgLevelID, ROL.SelectionOrder, R.OrgID, R.MultiOrgID, 
				 R.RequestID, R.Title, R.Description, R.EmpStatusID, 
				 date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate, 
				 R.City, R.State, R.ZipCode, R.Country, R.RequisitionFormID
				 FROM RequisitionOrgLevels ROL, Requisitions R
				 WHERE R.OrgID = ROL.OrgID
				 AND R.OrgID = :OrgID
				 AND R.MultiOrgID = :MultiOrgID
				 AND R.RequestID = ROL.RequestID
				 AND R.Active = 'Y'
				 AND R.PostDate < now()
				 AND R.ExpireDate > now() 
		         AND R.FreeJobBoardLists LIKE '%Monster%'
				 GROUP by R.RequestID
				 ORDER by R.OrgID, R.Title";
		$results = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($query, array($params));
		return $results;
	}
	
	/**
	 * @method		insertMonsterSessionCookie
	 * @param 		string $IsRequestMethod
	 * @param 		string $MonsterRequestURL
	 * @param 		string $RandID
	 * @param 		string $RequestID
	 * @param		string $Email
	 * @param 		string $Status
	 */
	public function insMonsterSessionCookie($IsRequestMethod, $MonsterRequestURL, $RandID, $CookieApp, $RequestID, $Email, $Status) {
		
		//Set parameters
		$params = array(":IsRequestMethod"=>$IsRequestMethod, ":MonsterRequestURL"=>$MonsterRequestURL,
						":RandID"=>$RandID, ":CookieApp"=>$CookieApp, ":RequestID"=>$RequestID, ":Email"=>$Email, ":Status"=>$Status);
		//Insert query
		$ins_sesscookie = "INSERT INTO MonsterSessionCookie(IsRequestMethod, MonsterRequestURL, CookieApp, RandID, RequestID, Email, Status, CreatedDateTime) 
						   VALUES(:IsRequestMethod, :MonsterRequestURL, :CookieApp, :RandID, :RequestID, :Email, :Status, NOW())";
		$res_sesscookie = $this->db->getConnection ( "IRECRUIT" )->insert($ins_sesscookie, array($params));
	}
	
	/**
	 * @method		getMonsterSessionCookieInfoByRandID
	 * @param 		string $RandID
	 * @param 		string $RequestID
	 * @return		array
	 */
	public function getMonsterSessionCookieInfoByRandID($RandID, $RequestID) {
		
		$params = array(":RandID"=>$RandID, ":RequestID"=>$RequestID);
		
		$sel_sesscookie = "SELECT * FROM MonsterSessionCookie WHERE RandID = :RandID AND RequestID = :RequestID";
		$row_sesscookie = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_sesscookie, array($params));
		
		return $row_sesscookie;		
	}
	
	/**
	 * @method		getMonsterSessionCookieByCookieApp
	 * @param 		string $RandID
	 * @param 		string $RequestID
	 * @return		array
	 */
	public function getMonsterSessionCookieByCookieApp($CookieApp, $RequestID) {
	
		$params = array(":CookieApp"=>$CookieApp, ":RequestID"=>$RequestID);
	
		$sel_sesscookie = "SELECT * FROM MonsterSessionCookie WHERE CookieApp = :CookieApp AND RequestID = :RequestID";
		$row_sesscookie = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_sesscookie, array($params));
	
		return $row_sesscookie;
	}
	
	/**
	 * @method		updateRequisitionPostedDate
	 * @param 		string $OrgID
	 * @param 		string $MultiOrgID
	 * @param 		string $RequestID
	 * @return 		array
	 */
	public function updateRequisitionPostedDate($OrgID, $MultiOrgID, $RequestID) {
		
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
		
		$updreq = "UPDATE Requisitions SET MonsterJobPostedDate = NOW()
				   WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID 
				   AND RequestID = :RequestID";
		$resreq = $this->db->getConnection ( "IRECRUIT" )->update($updreq, array($params));
		
		return $resreq;
	}
	
	/**
	 * @method		getMonsterJobIndustries
	 * @return 		array
	 */
	public function getMonsterJobIndustries() {
		
		$sel_industries = "SELECT * FROM MonsterJobIndustries WHERE IndustryId > 0 ORDER BY IndustryName ASC";
		$res_industries = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_industries);
		
		if(is_array($res_industries['results'])) {
			foreach ($res_industries['results'] as $row_industries) {
				$rows[$row_industries['IndustryId']] = $row_industries['IndustryName'];
			}	
		}
				
		return $rows;
	}
	
	/**
	 * @method		getJobCategoryNameByCategoryId
	 * @param		$JobCategoryId
	 * @return 		array
	 */
	public function getJobCategoryNameByCategoryId($JobCategoryId) {
		
		$params = array(":JobCategoryId"=>$JobCategoryId);
		$sel_monster_job_cat = "SELECT * FROM MonsterJobCategory WHERE JobCategoryId = :JobCategoryId LIMIT 1";
		$res_monster_job_cat = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_monster_job_cat, array($params));
		
		return $res_monster_job_cat;
	}
	
	/**
	 * @method		getOccupationByOccupationId
	 * @param		$OccupationId
	 * @return 		array
	 */
	public function getOccupationByOccupationId($OccupationId) {
		
		$params = array(":OccupationId"=>$OccupationId);
		$sel_occupation_info = "SELECT * FROM MonsterJobCategory WHERE OccupationId = :OccupationId";
		$row_occupation_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_occupation_info, array($params));
	
		return $row_occupation_info;
	}
	
	/**
	 * @method		getJobIndustryNameById
	 * @param		$IndustryId
	 * @return 		array
	 */
	public function getJobIndustryNameById($IndustryId) {
		
		$params = array(":IndustryId"=>$IndustryId);
		$sel_job_industries_info = "SELECT * FROM MonsterJobIndustries WHERE IndustryId = :IndustryId";
		$row_job_industries_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_job_industries_info, array($params));
		
		return $row_job_industries_info;
	}
	
	/**
	 * @method		insMonsterPostData
	 * @param		$info
	 * @return 		array
	 */
	public function insMonsterPostData($info) {
		
		// Insert Applicant Information
		$insert_post_data = $this->db->buildInsertStatement('MonsterPostData', $info);
		$result_post_data = $this->db->getConnection ( "IRECRUIT" )->insert($insert_post_data['stmt'], $insert_post_data['info']);
		
		return $result_post_data;
	}
	
	/**
	 * @method		getPostedData
	 * @param		$RequestID
	 * @return 		array
	 */
	public function getPostedData($RequestID) {
		
		$params = array(":RequestID"=>$RequestID);
		$selpostedjob = "SELECT P.*, M.* 
						 FROM PostRequisitions P,  
						 MonsterPostData M 
						 WHERE P.RequestID = M.JobRefID 
						 AND P.RequestID = :RequestID";
		$respostedjob = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($selpostedjob, array($params));
		
		return $respostedjob;
	}
	
	/**
	 * @method		saveOperations
	 * @param		$EmailAddress, $date1, $date2, $RequestID
	 * @return 		array
	 */
	public function saveOperations($EmailAddress, $date1, $date2, $RequestID) {

		$track_info_desc = $RequestID . " - " . $_SERVER['REMOTE_ADDR'] . " " . $EmailAddress." -Date1: ".$date1." -Date2: ".$date2 ."-". $RequestID;
		$ins_save_operations_info = "INSERT INTO SaveOperationsInfo(TableName, Description) VALUES('MonsterPostData', '".$track_info_desc."')";
		
		$respostedjob = $this->db->getConnection ( "IRECRUIT" )->insert($ins_save_operations_info);
	}
	
	/**
	 * @method		delSessionCookie
	 * @param		$CookieApp
	 * @return 		array
	 */
	public function delSessionCookie($CookieApp) {
		$del_sess_cookie = "DELETE FROM MonsterSessionCookie where CookieApp = '".$CookieApp."'";
		$res_sess_cookie = $this->db->getConnection ( "IRECRUIT" )->delete($del_sess_cookie);
		
		return $res_sess_cookie;
	}

	/**
	 * @method     getMonsterRequisitionInformation
	 * @param      string $columns
	 * @param      string $OrgID
	 * @param      string $MultiOrgID
	 * @param      string $RequestID
	 * @return     string
	 */
	function getMonsterRequisitionInformation($columns = "*", $OrgID, $MultiOrgID, $RequestID) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	    $sel_post_requisition_info = "SELECT $columns FROM PostRequisitions WHERE OrgID = :OrgID AND RequestID = :RequestID";
	    $res_post_requisition_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_post_requisition_info, array($params) );
	
	    return $res_post_requisition_info;
	}
}
?>
