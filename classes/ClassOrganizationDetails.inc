<?php
/**
 * @class		OrganizationDetails
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class OrganizationDetails {
	
	/**
	 * @tutorial	Set the default database connection as irecruit
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}

	/**
	 * @method		getOrgIDMultiOrgIDByMultiOrgID
	 * @param		string $MultiOrgID
	 * @return		array
	 */
	function getOrgIDMultiOrgIDByMultiOrgID($MultiOrgID) {
	
	    //Set parameters for prepared query
	    $params = array(":MultiOrgID"=>$MultiOrgID);
	    $sel_org_query = "SELECT OrgID, MultiOrgID FROM OrgData WHERE MultiOrgID = :MultiOrgID";
	    $row_org_info  = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_org_query, array($params) );
	
	    return $row_org_info;
	}
	
	/**
	 * @method		getOrganizationNameByRequestID
	 * @param		$OrgID, $RequestID
	 * @return		string
	 */
	function getOrganizationNameByRequestID($OrgID, $RequestID) {
	
	    //Set parameters for prepared query
	    $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	
	    $sel_org_info = "SELECT OD.OrganizationName
						 FROM OrgData OD, Requisitions R
						 WHERE OD.OrgID = R.OrgID
						 AND OD.MultiOrgID = R.MultiOrgID
						 AND OD.OrgID = :OrgID
						 AND R.RequestID = :RequestID";
	    list ( $OrganizationName ) = $this->db->getConnection("IRECRUIT")->fetchRow($sel_org_info, array($params));
	
	    return $OrganizationName;
	} // end function
	
	/**
	 * @method		getOrganizationInformation
	 * @tutorial	Function designed to fetch the required columns
	 * @param		$OrgID, $MultiOrgID, $columns(optional)
	 * @return		associative array
	 */
	function getOrganizationInformation($OrgID, $MultiOrgID, $columns = "*") {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    //Set parameters for prepared query
	    $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	
	    $sel_org_info = "SELECT $columns FROM OrgData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	    $row_org_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_org_info, array($params) );
	
	    return $row_org_info;
	}
	
	/**
	 * @method     getOrganizationName
	 */
	function getOrganizationName($OrgID, $MultiOrgID) {
	     
	    //Set parameters for prepared query
	    $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	    $sel_org_info = "SELECT OrganizationName FROM OrgData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	    $row_org_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_org_info, array($params) );
	    
	    return $row_org_info['OrganizationName'];
	}
	
	/**
	 * @method     getEmailAndEmailVerified
	 */
	function getEmailAndEmailVerified($OrgID, $MultiOrgID) {
	     
	    //Set parameters
	    $params_orgemail   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	    //Set columns
	    $columns           =   "Email, CAST(EmailVerified AS unsigned int) EmailVerified, OrgName, DeadLetterEmail, CAST(DeadLetterEmailVerified AS unsigned int) DeadLetterEmailVerified";
	    $sel_org_email     =   "SELECT $columns FROM OrganizationEmail WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	    $res_org_email     =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_org_email, array($params_orgemail) );
	    
	    return $res_org_email; 
	}
}
