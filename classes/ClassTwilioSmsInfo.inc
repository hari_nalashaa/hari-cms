<?php
/**
 * @class		TwilioSmsInfo
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioSmsInfo {

    public $db;
	var $conn_string		=	"IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method		saveSentSmsInfo
     * @param		$sent_sms_info
     * @return		array
     */
    function saveSentSmsInfo($sms_info) {
		
		$info			=	array(
								"MessageServiceSid"	=>	$sms_info['messagingServiceSid'],
								"SmsMessageSid"		=>	$sms_info['sid'],
								"AccountSid"		=>	$sms_info['accountSid'],
								"DateCreated"		=>	json_encode($sms_info['dateCreated']),
								"DateUpdated"		=>	json_encode($sms_info['dateUpdated']),
								"DateSent"			=>	$sms_info['dateSent'],
								"ToNumber"			=>	$sms_info['to'],
								"FromNumber"		=>	$sms_info['from'],
								"SmsMessage"		=>	$sms_info['body'],
								"Status"			=>	$sms_info['status'],
								"NumSegments"		=>	$sms_info['numSegments'],
								"NumMedia"			=>	$sms_info['numMedia'],
								"Direction"			=>	$sms_info['direction'],
								"ApiVersion"		=>	$sms_info['apiVersion'],
								"Price"				=>	$sms_info['price'],
								"PriceUnit"			=>	$sms_info['priceUnit'],
								"ErrorCode"			=>	$sms_info['errorCode'],
								"ErrorMessage"		=>	$sms_info['errorMessage'],
								"MessageUriPath"	=>	$sms_info['uri'],
								"SubresourceUris"	=>	json_encode($sms_info['subresourceUris']),
								"CreatedDateTime"	=>	"NOW()",
							);
		$insert_info 	=	$this->db->buildInsertStatement('TwilioSentSmsInfo', $info);
		$result_info 	=	$this->db->getConnection ( $this->conn_string )->insert ( $insert_info["stmt"], $insert_info["info"] );
	
		return $result_info;
    }
    
    /**
     * @method		saveReceivedSmsInfo
     */
    public function saveReceivedSmsInfo($sms_info) {
    	
		$info			=	array(
								"MessageServiceSid"	=>	$sms_info['MessageSid'],
								"SmsMessageSid"		=>	$sms_info['SmsMessageSid'],
								"AccountSid"		=>	$sms_info['AccountSid'],
								"ToCountry"			=>	$sms_info['ToCountry'],
								"ToState"			=>	$sms_info['ToState'],
								"NumMedia"			=>	$sms_info['NumMedia'],
								"ToCity"			=>	$sms_info['ToCity'],
								"FromZip"			=>	$sms_info['FromZip'],
								"FromState"			=>	$sms_info['FromState'],
								"SmsStatus"			=>	$sms_info['SmsStatus'],
								"FromCity"			=>	$sms_info['FromCity'],
								"SmsMessage"		=>	$sms_info['Body'],
								"FromCountry"		=>	$sms_info['FromCountry'],
								"ToNumber"			=>	$sms_info['To'],
								"ToZip"				=>	$sms_info['ToZip'],
								"NumSegments"		=>	$sms_info['NumSegments'],
								"FromNumber"		=>	$sms_info['From'],
								"ApiVersion"		=>	$sms_info['ApiVersion'],
								"CreatedDateTime"	=>	"NOW()",
							);
		$insert_info 	=	$this->db->buildInsertStatement('TwilioReceivedSmsInfo', $info);
		$result_info 	=	$this->db->getConnection ( $this->conn_string )->insert ( $insert_info["stmt"], $insert_info["info"] );
    	
		return $result_info;
    }
    	
} // end Class
