<?php
/**
 * @class		TwilioConversationUsersOrg
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioConversationUsersOrgs {

    public $db;

	var $conn_string       		=   "IRECRUIT";

	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		// Create a new instance of a SOAP 1.2 client
		$this->db	=	Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	} // end function

	/**
	 * @method		getAllTwilioConversationOrgsInfo
	 */
	public function getAllTwilioConversationOrgsInfo() {
	
		$sel_orgs	=	"SELECT OrgID, getOrgnizationName(OrgID, '') AS OrgName FROM TwilioConversations GROUP BY OrgID";
		$res_orgs	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_orgs );
		$orgs_list	=	$res_orgs['results'];
		 
		return $orgs_list;
	}
	
	/**
	 * @method		getAllTwilioConversationUsersByOrgID
	 */
	public function getAllTwilioConversationUsersByOrgID($OrgID) {
		
		$params		=	array(":OrgID"=>$OrgID);
		$sel_users	=	"SELECT UserID FROM TwilioConversations WHERE OrgID = :OrgID GROUP BY UserID";
		$res_users	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_users, array($params) );
		$users_list	=	$res_users['results'];

		for($u = 0; $u < count($users_list); $u++) {
			$user_info	=	G::Obj('IrecruitUsers')->getUserInfoByUserID($users_list[$u]['UserID'], "FirstName, LastName");
			
			$users_list[$u]['FirstName']	=	$user_info['FirstName'];
			$users_list[$u]['LastName']		=	$user_info['LastName'];
		}
		
		return $users_list;
	}

}
