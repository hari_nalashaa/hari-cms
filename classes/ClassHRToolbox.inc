<?php
class HRToolbox 
{

    public $db;
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
    * @method          insUpdHRToolbox
    * @param           $info, $skip
    * @return          array
    */
    function insUpdHRToolbox($info, $skip = array()) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("HRToolbox", $info, $skip);
        $results    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
        return $results;
        
    } // end function

    /**
     * @method          getHRToolboxFiles
     */
    function getHRToolboxFiles($OrgID) {

        $query = "SELECT * FROM HRToolbox WHERE OrgID = :OrgID ORDER BY SortOrder";
        $params = array(":OrgID"=>$OrgID);

        $results = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($query, array($params));

        return $results['results'];
    }

    /**
    * @method              delHRToolboxID
    * @param               $OrgID, $HRToolboxID
    */
    public function delHRToolboxID($OrgID, $HRToolboxID) {

       $params = array(":OrgID"=>$OrgID,":HRToolboxID"=>$HRToolboxID);
       $del_info = "DELETE FROM HRToolbox WHERE HRToolboxID = :HRToolboxID AND OrgID = :OrgID";
       $res = $this->db->getConnection ( $this->conn_string )->delete ( $del_info, array($params) );

       return $res;
   }


    /**
     * @method          displayHRToolboxFiles
     */
    function displayHRToolboxFiles($OrgID,$Admin='') {

	$HRToolboxHOME = IRECRUIT_HOME . "vault/".$OrgID."/HRToolbox/";
	$HRToolboxDIR = IRECRUIT_DIR . "vault/".$OrgID."/HRToolbox/";
        $ret="";

	$TOOLS = $this->getHRToolboxFiles($OrgID);

	$rtn .= '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">';

	$rtn .= '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';
	$rtn .= '<tr><td>';

	if (count($TOOLS) < 1) {
          $rtn .= 'There are currently no files available in the toolbox.';
	}

	$rtn .= '<div id="documentorder">'; // sort container

	foreach ($TOOLS AS $T) {

	   $document = $HRToolboxHOME . $T['FileName'];
	   $mimetype = mime_content_type($HRToolboxDIR . $T['FileName']);

	   $width="500px";
	   $height="375px";
	   if ($Admin == "Y") {
	      $width="250px";
	      $height="188px";
	   }

	   $rtn .= '<div document-id="' . $T['HRToolboxID'] . '" style="float:left;margin-bottom:20px;line-height:280%;width:100%;padding:5px 20px 20px 20px;border:1px solid #cccccc;background-color:#ffffff;">'; // outside container

	   if ($Admin == "Y") {
             $rtn .= '<div style="float:right;line-height:280%;">';
             $rtn .= '&nbsp;&nbsp;&nbsp;<a href="administration.php?action=hrtoolbox&menu=8&delete=' . $T['HRToolboxID'] . '" onclick="return confirm(\'Are you sure you want to delete this item?\n\nDescription: ' . $T['Description'] . '\n\n\')">';
             $rtn .= '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" title="Delete" border="0">';
             $rtn .= '</a>';
             $rtn .= '</div>';
	   }

	   $rtn .= '<a href="' . $document . '" target="_blank"><span style="font-size:14pt;">' . $T['Description'] . '</span></a>';
	   $rtn .= '<br>';

	   $rtn .= '<div style="overflow-y:auto;overflow-x:auto;resize:both;width:'.$width.';height:'.$height.';">'; // resizable container

	   if ($mimetype == 'application/pdf') {

	     $rtn .= '<embed src="' . $document . '" width="100%" height="100%" type="' . $mimetype . '">';

	   } else {
	   
	     $rtn .= '<iframe src=\'https://view.officeapps.live.com/op/embed.aspx?src=' . $document . '\' width=\'100%\' height=\'100%\' frameborder=\'0\'> </iframe>';

	   }
           $rtn .= '</div>'; // resizable container

           $rtn .= '</div>'; // outside container

	   $rtn .= '<div style="clear:both;"></div>';

	} // end foreach

        $rtn .= '</div>'; // sort container

	$rtn .= '</td></tr>';
	$rtn .= '</table>';

	$rtn.='<script>' . "\n";
        $rtn.='$( function() {' . "\n";
        $rtn.='$( "#documentorder" ).sortable({' . "\n";
        $rtn.='update: function( event, ui ) {' . "\n";
        $rtn.='$.ajax({' . "\n";
        $rtn.='url: "' . IRECRUIT_HOME . 'js/hrsort.php",' . "\n";
        $rtn.='type: "POST",' . "\n";
        $rtn.='data : "order="+ui.item.index()+"&id="+ui.item.attr(\'document-id\'),' . "\n";
        $rtn.='success: function(result){' . "\n";
        //$rtn.='alert(result);' . "\n";
        $rtn.='location.reload();' . "\n";
        $rtn.='}' . "\n";
        $rtn.='});' . "\n";
        $rtn.='}'. "\n";
        $rtn.='});' . "\n";
        $rtn.='$( "#documentorder" ).disableSelection();' . "\n";
        $rtn.='} );' . "\n";
        $rtn.='</script>' . "\n";

        return $rtn;
    }

    /**
     * @method         sortHRToolboxID 
     */
    function sortHRToolboxID($OrgID, $id, $order) {

	$skip   =   array("HRToolboxID");

        $info   =   array(
                 "HRToolboxID"       =>  $id,
                 "SortOrder"         =>  $order,
                 "LastModified"      =>  "NOW()"
                 );

	if (($OrgID != "") && ($id != "")) {
          G::Obj('HRToolbox')->insUpdHRToolbox($info, $skip);
	}

	$query = "select @i := 0";
	$this->db->getConnection ( $this->conn_string )->update($query);

	$query = "update HRToolbox set SortOrder = (select @i := @i + 1) where OrgID = :OrgID order by SortOrder";
	$params=array(":OrgID"=>$OrgID);
	$this->db->getConnection ( $this->conn_string )->update($query,array($params));
    }

} // end Class
