<?php
/**
 * @class		TwilioConversationApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioUsageRecordsApi {
	use TwilioSettings;
	
	public $db                  =	"";
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	var $twilio_number			=	"+16572208043";	//+16572208043
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

	/**
	 * @method		getLastMonthUsageForAllCategories()
	 */
    public function getLastMonthUsageForAllCategories($OrgID) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	   
    	try {
    		$records_list	=	array();
			
			// Loop over the list of transcriptions and echo a property for each one
			foreach ($client->usage->records->lastMonth->read() as $record) {
			    $records_list[]	= $record;
			}
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-last-month-records-api.txt", serialize($records_list), "w+", false);
    		
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully fetched the last month records", "Response"=>$records_list);
    	
    	}
		catch (RestException | TwilioException | Exception $e) {    	

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-last-month-records-api-error.txt", serialize($client->usage), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to fetch the last month records", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	
    }

    
} // end Class
