<?php
/**
 * @class		WOTCAgreements
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCAgreements
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getAgrreeementDataInfo
     * @param		string $columns
     * @param		string $where_info
     * @param 		string $order_by
     * @param 		string $info
     * @return 		array
     */
    function getAgreementDataInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_org = "SELECT $columns FROM Agreement";
        
        if(count($where_info) > 0) {
            $sel_org .= " WHERE " . implode(" AND ", $where_info);
        }
        
        if($group_by != "") $sel_org .= " GROUP BY " . $group_by;
        if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
        
        $res_org = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_org, $info );
        
        return $res_org;
    }
    
    /**
     * @method      getAgreementsInfo
     * @param       string $cookie
     * @return      array
     */
    public function getAgreementsInfo($columns = "*", $agreementID) {
        
        $params     =   array(":AgreementID"=>$agreementID);
        $query      =   "SELECT $columns FROM Agreement WHERE AgreementID = :AgreementID AND Active='Y'";
        $agreement_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $agreement_info;
    }
    
    /**
     * @method      getAllAgreementsData
     * @param       string $cookie
     * @return      array
     */
    public function getAllAgreementsData($columns = "*")
    {
        $query      =   "SELECT $columns FROM Agreement WHERE Active='Y' ORDER BY AgreementID DESC";
        $agreement_info   =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($query);
        
        return $agreement_info;
    }
    
    /**
     * @method		insUpdAgreementDataInfo
     * @param		$info
     * @return		array
     */
    function insUpdAgreementDataInfo($info, $skip = array()) {
        
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("Agreement", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
        
        return $res_org;
    }
    
    /**
     * @method      updAgreementDataInfo
     * @param       $set_info, $where_info, $info
     */
    function updAgreementDataInfo($set_info = array(), $where_info = array(), $info) {
        
        $upd_org_data_info = $this->db->buildUpdateStatement("Agreement", $set_info, $where_info);
        $res_org_data_info = $this->db->getConnection( $this->conn_string )->update($upd_org_data_info, $info);
        
        return $res_org_data_info;
    }
    
    /**
     * @method		delAgreementInfo
     * @param		$where_info = array(), $info = array()
     */
    function delAgreementInfo($where_info = array(), $info = array()) {
        
        $del_rows   =   "DELETE FROM Agreement";
        
        if (count ( $where_info ) > 0) {
            $del_rows   .=  " WHERE " . implode ( " AND ", $where_info );
        }
        
        $res_rows = $this->db->getConnection ( $this->conn_string )->delete ( $del_rows, $info );
        
        return $res_rows;
    }
    
}
