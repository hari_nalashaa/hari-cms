<?php
/**
 * @class		ApiUsers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApiUsers
{
    public $db;

    var $conn_string       =   "ADMIN";

    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getApiUserInfoByLoginToken
     */
    public function getApiUserInfoByLoginToken($LoginToken) {
    	
    	$query     =   "SELECT * FROM ApiUsers WHERE LoginToken = :LoginToken";
    	$params    =   array(':LoginToken'=>$LoginToken);
    	$user_info =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));

        return $user_info;
    }

}    
?>
