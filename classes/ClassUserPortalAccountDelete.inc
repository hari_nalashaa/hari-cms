<?php
/**
 * @class		UserPortalAccountDelete
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class UserPortalAccountDelete
{
    var $conn_string       =   "USERPORTAL";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "USERPORTAL" );
    }

    /**
     * @method      insUsersDeleteHistory
     */
    public function insUsersDeleteHistory($UserID, $FirstName, $LastName, $Email, $LastAccess) {
        
        $params_info                =   array(":UserID"=>$UserID, ":FirstName"=>$FirstName, ":LastName"=>$LastName, ":Email"=>$Email, ":LastAccess"=>$LastAccess, ":DeletedDate"=>"NOW()");
        $ins_users_delete_history   =   "INSERT INTO UsersDeleteHistory(UserID, FirstName, LastName, Email, LastAccess, DeletedDate) VALUES(:UserID, :FirstName, :LastName, :Email, :LastAccess, :DeletedDate)";
        $res_users_delete_history   =   $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_users_delete_history, array($params_info) );
        
        return $res_users_delete_history;
    }
    
    /**
     * @method      
     */
    public function delUsersInfo($UserID) {
    	
        $params = array(":UserID"=>$UserID);
        $del_user = "DELETE FROM Users WHERE UserID = :UserID";
        $res_user = $this->db->getConnection("USERPORTAL")->delete ( $del_user, array($params) );

        if($res_user['affected_rows'] > 0) {
            
            $params = array(":UserID"=>$UserID);
            $del_profile_data = "DELETE FROM ProfileData WHERE UserID = :UserID";
            $res_profile_data = $this->db->getConnection("USERPORTAL")->delete ( $del_profile_data, array($params) );
            
            $params = array(":UserID"=>$UserID);
            $del_app_process_data = "DELETE FROM ApplicantProcessData WHERE UserID = :UserID";
            $res_app_process_data = $this->db->getConnection("USERPORTAL")->delete ( $del_app_process_data, array($params) );

            $params = array(":UserID"=>$UserID);
            $del_app_attachments = "DELETE FROM ApplicantAttachments WHERE UserID = :UserID";
            $res_app_attachments = $this->db->getConnection("USERPORTAL")->delete ( $del_app_attachments, array($params) );

            G::Obj('Files')->delDir(USERPORTAL_DIR.'vault/'.$UserID);
        }        
    }
}
?>
