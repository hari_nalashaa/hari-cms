<?php
/**
 * @class		CronStatusLogs
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class CronStatusLogs {

    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getCronStatusLogsInfo
     * @param		string $columns
     * @param		string $where_info
     * @param 		string $order_by
     * @param 		string $info
     * @return 		array
     */
    function getCronStatusLogsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_info = "SELECT $columns FROM CronStatus";
    
        if(count($where_info) > 0) {
            $sel_info .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
    
        $res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_info, $info );
    
        return $res_org;
    }
    
    
    /**
     * @method      insUpdCronStatusLog
     * @param       string $OrgID, $ApplicationID, $RequestID
     * @return      string
     */
    function insUpdCronStatusLog($application, $status) {
    
        $params     =   array(':application'=>$application, ':status'=>$status, ':ustatus'=>$status);
        
        $query      =   "INSERT INTO CronStatus";
        $query     .=   " (Application, LastRun, Status) VALUES (:application, NOW(), :status)";
        $query     .=   " ON DUPLICATE KEY UPDATE LastRun = NOW(), Status = :ustatus";
        //Insert CronStatus
        $this->db->getConnection("IRECRUIT")->insert($query, array($params));
    }
}
?>
