<?php
/**
 * @class		GetFormPostAnswer
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class GetFormPostAnswer {
	
	var $conn_string       =   "IRECRUIT";
	var $POST              =   array();
	var $FILES             =   array();
	var $QueInfo           =   array();

	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	/**
	 * @method      getPostDataAnswersOfFormQuestions
	 */
	function getPostDataAnswersOfFormQuestions($OrgID, $FormID, $SectionID = '') {

	    $form_questions_list   =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, $SectionID);
	    $answers_info          =   array();
        
	    //Build data that will store in ApplicantData table
	    if(is_array($form_questions_list)) {
	        foreach($form_questions_list as $QuestionID=>$QI) {
                $Answer   =   call_user_func( array( $this, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $answers_info[$QuestionID]  =   $Answer;
    	    }
	    }
	    
	    return $answers_info;
	}
	
	/**
	 * @method     getQuestionAnswer2
	 * @param      
	 */
	function getQuestionAnswer2($QuestionID) {
	    //Radio Question
	    //It returns only one value
        return $this->POST[$QuestionID];
	}

	/**
	 * @method     getQuestionAnswer3
	 * @param      $QuestionID
	 */
	function getQuestionAnswer3($QuestionID) {
	    //PullDown Question
	    //It returns only one value
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer5
	 * @param      $QuestionID
	 */
	function getQuestionAnswer5($QuestionID) {
	    //Text Area
	    //It returns only one value
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer6
	 * @param      
	 */
	function getQuestionAnswer6($QuestionID) {
	    //Textbox
	    //It returns only one value
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer7
	 * @param      $QuestionID
	 */
	function getQuestionAnswer7($QuestionID) {
	   

	}

	/**
	 * @method     getQuestionAnswer8
	 * @param      $QuestionID
	 */
	function getQuestionAnswer8($QuestionID) {
	    //It returns only one value, in case if it exists
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer9
	 * @param      $QuestionID
	 */
	function getQuestionAnswer9($QuestionID) {

	    $skills_info    =   array();
	    $skills_cnt     =   $this->POST[$QuestionID.'cnt'];
        $val_list       =   $this->getDisplayValue($this->QueInfo['value']);
        
	    for($sc = 1; $sc <= $skills_cnt; $sc++) {
	        if(isset($this->POST["$QuestionID-$sc"]) && $this->POST["$QuestionID-$sc"] != "") {
	            $skills_info[$sc]["$QuestionID-$sc"]             =   $this->POST["$QuestionID-$sc"];
	            $skills_info[$sc]["$QuestionID-$sc-yr"]          =   $this->POST["$QuestionID-$sc-yr"];
	            $skills_info[$sc]["$QuestionID-$sc-comments"]    =   $this->POST["$QuestionID-$sc-comments"];
	        }
	    }
	     
	    return json_encode($skills_info);
	}
	
	/**
	 * @method     getQuestionAnswer10
	 * @param      $QuestionID
	 */
	function getQuestionAnswer10($QuestionID) {

	}
	
	/**
	 * @method     getQuestionAnswer11
	 * @param      $QuestionID
	 */
	function getQuestionAnswer11($QuestionID) {
	     

	}
	
	/**
	 * @method     getQuestionAnswer13
	 * @param      $QuestionID
	 */
	function getQuestionAnswer13($QuestionID) {

	    $Ans1           =   empty($this->POST[$QuestionID.'1']) ? "" : $this->POST[$QuestionID.'1'];
	    $Ans2           =   empty($this->POST[$QuestionID.'2']) ? "" : $this->POST[$QuestionID.'2'];
	    $Ans3           =   empty($this->POST[$QuestionID.'3']) ? "" : $this->POST[$QuestionID.'3'];
	    $Answer         =   json_encode([$Ans1, $Ans2, $Ans3]);

	    return $Answer;
	}
	
	/**
	 * @method     getQuestionAnswer14
	 * @param      $QuestionID
	 */
	function getQuestionAnswer14($QuestionID) {

	    $Ans1           =   empty($this->POST[$QuestionID.'1']) ? "" : $this->POST[$QuestionID.'1'];
	    $Ans2           =   empty($this->POST[$QuestionID.'2']) ? "" : $this->POST[$QuestionID.'2'];
	    $Ans3           =   empty($this->POST[$QuestionID.'3']) ? "" : $this->POST[$QuestionID.'3'];
	    $Ans4           =   empty($this->POST[$QuestionID.'ext']) ? "" : $this->POST[$QuestionID.'ext'];
	    $Answer         =   json_encode([$Ans1, $Ans2, $Ans3, $Ans4]);
	    
	    return $Answer;
	}
	
	/**
	 * @method     getQuestionAnswer15
	 * @param      $QuestionID
	 */
	function getQuestionAnswer15($QuestionID) {

	    $Ans1           =   empty($this->POST[$QuestionID.'1']) ? "" : $this->POST[$QuestionID.'1'];
	    $Ans2           =   empty($this->POST[$QuestionID.'2']) ? "" : $this->POST[$QuestionID.'2'];
	    $Ans3           =   empty($this->POST[$QuestionID.'3']) ? "" : $this->POST[$QuestionID.'3'];
	    $Answer         =   json_encode([$Ans1, $Ans2, $Ans3]);
	    
	    return $Answer;
	}
	
	/**
	 * @method     getQuestionAnswer16
	 * @param      $QuestionID
	 */
	function getQuestionAnswer16($QuestionID) {

	}
	
	/**
	 * @method     getQuestionAnswer17
	 * @param      $QuestionID
	 */
	function getQuestionAnswer17($QuestionID) {
	    //Date
	    //It returns only one value
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer18
	 * @param      $QuestionID
	 */
	function getQuestionAnswer18($QuestionID) {
        $cnt            =   $this->POST[$QuestionID.'cnt'];
        
        $AnswersList    =   array();
        for($c18 = 1; $c18 <= $cnt; $c18++) {
            if(isset($this->POST[$QuestionID.'-'.$c18]) && !is_null($this->POST[$QuestionID.'-'.$c18])) {
                $AnswersList[$QuestionID.'-'.$c18]  =   $this->POST[$QuestionID.'-'.$c18];
            }
        }
        
        $Answer         =   json_encode($AnswersList);
         
        return $Answer;
	}
	
	/**
	 * @method     getQuestionAnswer22
	 * @param      $QuestionID
	 */
	function getQuestionAnswer22($QuestionID) {
	    //Radio Question
	    //It returns only one value
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer23
	 * @param      $QuestionID
	 */
	function getQuestionAnswer23($QuestionID) {
	    //Radio Question
	    //It returns only one value
	    return $this->POST[$QuestionID];
	}
	
	/**
	 * @method     getQuestionAnswer24
	 * @param      $QuestionID
	 */
	function getQuestionAnswer24($QuestionID) {

	    
	}
	
	/**
	 * @method     getQuestionAnswer30
	 * @param      $QuestionID
	 */
	function getQuestionAnswer30($QuestionID) {


	}
	
	/**
	 * @method     getQuestionAnswer60
	 * @param      $QuestionID
	 * @tutorial   Specific to Agreement 
	 */
	function getQuestionAnswer60($QuestionID) {

	}

	/**
	 * @method     getQuestionAnswer90
	 * @param      $QuestionID
	 */
	function getQuestionAnswer90($QuestionID) {

	}

	/**
	 * @method     getQuestionAnswer98
	 * @param      $QuestionID
	 */
	function getQuestionAnswer98($QuestionID) {

	}

	/**
	 * @method     getQuestionAnswer99
	 * @param      $QuestionID
	 */
	function getQuestionAnswer99($QuestionID) {
	    
	}
	
	/**
	 * @method     getQuestionAnswer100
	 * @param      $QuestionID
	 */
	function getQuestionAnswer100($QuestionID) {
	    $que_100_ans   =   $this->POST ['LabelSelect'] [$QuestionID];
	    
	    if($que_100_ans == null) $que_100_ans = array();
	    
	    return serialize ( $que_100_ans );
	}
	
	/**
	 * @method     getQuestionAnswer120
	 * @param      $QuestionID
	 */
	function getQuestionAnswer120($QuestionID) {
	    
	    $qa ['from_time']  =   $this->POST ['shifts_schedule_time'] [$QuestionID] [from_time];
	    $qa ['to_time']    =   $this->POST ['shifts_schedule_time'] [$QuestionID] [to_time];
	    $qa ['days']       =   $this->POST ['shifts_schedule_time'] [$QuestionID] [days];
	    
	    $Answer            =   serialize ( $qa );
	    
	    return $Answer;
	}
	
	/**
	 * @method     getQuestionAnswer1818
	 * @param      $QuestionID
	 */
	function getQuestionAnswer1818($QuestionID) {

	    $cnt            =   $this->POST[$QuestionID.'cnt'];
	    
	    $AnswersList    =   array();
	    for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
	        if(!is_null($this->POST[$QuestionID.'-'.$c1818])) {
	            $AnswersList[$QuestionID.'-'.$c1818]  =   $this->POST[$QuestionID.'-'.$c1818];
	        }
	    }
	    
	    $Answer         =   json_encode($AnswersList);
	     
	    return $Answer;
	}
	
	/**
	 * @method     getDisplayValue
	 */
	function getDisplayValue($value) {
		$list  =   explode("::", $value);
		
		$values_list  =   array();
		
		foreach($list as $val_info) {
            $val    =   explode(":", $val_info);
			$values_list[$val[0]]   =   isset($val[1]) ? $val[1] : "";
		}
		
		return $values_list;
	}
}
