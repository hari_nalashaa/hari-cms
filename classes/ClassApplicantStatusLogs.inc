<?php
/**
 * @class		ApplicantStatusLogs
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantStatusLogs {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	

	/**
	 * @method     insApplicantStatusLog
	 */
	function insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, $ProcessOrder, $DispositionCode) {
	    global $ApplicationsObj, $IndeedObj, $ApplicantDetailsObj;

	    //Get Job Details Information
	    $job_app_info          =   $ApplicationsObj->getJobApplicationsDetailInfo("ProcessOrder, DispositionCode", $OrgID, $ApplicationID, $RequestID);
	    //Indeed Application Information
	    $indeed_app_info       =   $IndeedObj->getIndeedApplicantInfo($OrgID, $RequestID, $ApplicationID);
	    //Disposition Code Information
	    $DispositionCodeDesc   =   $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $DispositionCode );
	    
	    if(isset($indeed_app_info['ID']) 
	       && $indeed_app_info['ID'] != "") {

	            $app_status_logs       =   array(
                                                ":OrgID"                =>  $OrgID, 
                                                ":ApplicationID"        =>  $ApplicationID, 
                                                ":ApplyID"              =>  $indeed_app_info['ID'], 
                                                ":Status"               =>  $ProcessOrder, 
                                                ":Notes"                =>  $DispositionCodeDesc,
                                                ":UStatus"              =>  $ProcessOrder,
                                                ":UNotes"               =>  $DispositionCodeDesc,
	                                           );	            
	            $ins_app_status_logs   =   "INSERT INTO ApplicantStatusLogs(OrgID, ApplicationID, ApplyID, Status, CreatedDateTime, UpdatedDateTime, Notes)";
	            $ins_app_status_logs  .=   " VALUES(:OrgID, :ApplicationID, :ApplyID, :Status, NOW(), NOW(), :Notes)";
	            $ins_app_status_logs  .=   " ON DUPLICATE KEY UPDATE Status = :UStatus, UpdatedDateTime = NOW(), Notes = :UNotes";
	            $res_app_status_logs   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_status_logs, array($app_status_logs) );
	            
 	    }
	}
	
	
	/**
	 * @method     updIndeedProcessedStatus
	 */
	function updIndeedProcessedStatus($OrgID, $ApplicationID, $Status) {

	    $params_info           =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":Status"=>$Status);
	    $ins_upd_process_date  =   "UPDATE ApplicantStatusLogs SET IsProcessed = 'Yes', ProcessedDateTime = NOW() WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND Status = :Status";
	    $res_upd_process_date  =   $this->db->getConnection ( $this->conn_string )->update( $ins_upd_process_date, array($params_info) );

	    return $res_upd_process_date;
	}
}	
