<?php
/**
 * @class		TwilioConversationMessagesApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioConversationMessagesApi {
	use TwilioSettings;
	
    public $db                  =	"";
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	var $twilio_number			=	"+16572208043";	//+16572208043
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method		createConversationMessage
     */
    public function createConversationMessage($OrgID, $conversation_id, $author, $conversation_msg) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
    	
    		$message	=	$client->conversations->v1->conversations($conversation_id)
							    	->messages
							    	->create(array(
							    			"author"		=>	$author,
							    			"body"			=>	$conversation_msg
							    	)
    						);
									    	
	    	//Log the file information
	    	Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-conversation-msg-api.txt", serialize($message), "w+", false);
									    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Created message successfully", "Response"=>$message);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
	    	//Log the file information
	    	Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-conversation-msg-api-error.txt", serialize($message), "w+", false);
    		    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to update the participant", "Response"=>serialize($e));
    	}
    	 
    }

    /**
     * @method		listAllConversationMessages
     */
    public function listAllConversationMessages($OrgID, $conversation_id) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
    		$messages	=	$client->conversations->v1->conversations($conversation_id)->messages->read(array(), 20);

    		//Log the file information
    		//Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-list-conversation-msgs-api.txt", serialize($messages), "w+", false);
    		
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Fetched conversations list", "Response"=>$messages);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
		    Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-list-conversations-api-error.txt", serialize($e), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to update the participant", "Response"=>serialize($e));
    	}
    
    }
    
    /**
     * @method      insertConversationMessages
     */
    public function insertConversationMessages($message_info, $skip_fields = array()) {
        
        $ins_message_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("TwilioConversationMessagesArchive", $message_info, $skip_fields);
        $res_message_info   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_message_info["stmt"], $ins_message_info["info"] );
        
        return $res_message_info;
    }
    
} // end Class
