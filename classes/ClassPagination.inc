<?php
/**
 * @class		Pagination
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 * @tutorial    This class will generate the pagination links based on the parameters
 */

class Pagination {
	
	function getPageNavigationInfo($start, $limit, $total, $file_path, $other_params) {
		$total_pages	=	ceil ( $total / $limit );
		$current_page	=	floor ( $start / $limit ) + 1;
		
		$previous		=	"";
		$next			=	"";
		
		if ($current_page > 1) {
			$previous = "<a href=\"javascript:void(0);\" class=\"pagination_link\" onclick=\"getRecordsByPage('".(($current_page - 2)*$limit)."');\" id=\"previous_left_nav\"><i class='fa fa-arrow-left' aria-hidden='true'></i>&nbsp;Previous</a>";
		}
		
		if ($current_page < $total_pages) {
			$next = "<a href=\"javascript:void(0);\" class=\"pagination_link\" onclick=\"getRecordsByPage('".$current_page * $limit."');\" id=\"next_left_nav\">Next&nbsp;<i class='fa fa-arrow-right' aria-hidden='true'></i></a>";
		}
		
		return array("previous"=>$previous, "next"=>$next, "total_pages"=>$total_pages, "current_page"=>$current_page);
	}
}
?>