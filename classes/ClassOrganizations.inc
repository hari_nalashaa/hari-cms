<?php
/**
 * @class		Organizations
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Organizations {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getOrgDataInfo
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getOrgDataInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM OrgData";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	/**
	 * @method		getOrgTitle
	 * @param		string $OrgID
	 * @param		string $MultiOrgID
	 * @return 		array
	 */
	function getOrgTitle($OrgID, $MultiOrgID) {
	
	    $params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	    $sel_org   =   "SELECT OrganizationName FROM OrgData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	    $res_org   =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_org, array($params) );
	
	    return $res_org['OrganizationName'];
	}
	
	/**
	 * @method		insOrganizationInfo
	 * @param		array $info
	 * @return		array
	 */
	function insOrganizationInfo($table_name, $info, $on_update = '', $update_info = array()) {
		
		$ins_org_info = $this->db->buildInsertStatement($table_name, $info);
		
		$insert_statement = $ins_org_info ["stmt"];
		if ($on_update != '') {
			$insert_statement .= $on_update;
			if (is_array ( $update_info )) {
				foreach ( $update_info as $upd_key => $upd_value ) {
					$ins_org_info ["info"][0][$upd_key] = $upd_value;
				}
			}
		}
		
		$res_org_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $ins_org_info["info"] );
	
		return $res_org_info;
	}
	
	/**
	 * @method		delOrganizationInfo
	 * @param		$where_info, $info
	 */
	function delOrganizationInfo($table_name, $where_info = array(), $info = array()) {
		
		$del_org_info = "DELETE FROM $table_name";
		if (count ( $where_info ) > 0) {
			$del_org_info .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		$res_org_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_org_info, $info );
	
		return $res_org_info;
	}
	
	/**
	 * @method		updOrganizationInfo
	 * @param		$set_info, $where_info, $info
	 */
	function updOrganizationInfo($table_name, $set_info = array(), $where_info = array(), $info) {
		
		$upd_req_info = $this->db->buildUpdateStatement($table_name, $set_info, $where_info);
		$res_req_info = $this->db->getConnection("IRECRUIT")->update($upd_req_info, $info);
	
		return $res_req_info;
	}
	
	/**
	 * @method		getOrganizationColorsInfo
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getOrganizationColorsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM OrganizationColors";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	
	/**
	 * @method		getOrganizationTaxNumbers
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getOrganizationTaxNumbers($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM OrganizationTaxNumbers";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	// #################################################################################
	// #### Table LocationHold Operations
	// #################################################################################
	
	/**
	 * @method		getLocationHoldIdByCookie
	 * @return		numeric array
	 */
	function getLocationHoldIdByCookie() {
		
		//Set parameters for prepared query
		$params   =   array(":HoldID" => $_COOKIE ['LOC']);
		$sel_location_hold_info = "SELECT * FROM LocationHold WHERE HoldID = :HoldID";
		$location_hold_info 	= $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_location_hold_info, array($params) );
		
		return $location_hold_info;
	}
	
	/**
	 * @method		deleteLocationHoldIdByCookieLoc
	 * @return		numeric array
	 */
	function deleteLocationHoldIdByCookieLoc() {
		
		//Set parameters for prepared query
		$params[":HoldID"] = array($_COOKIE ['LOC']);
		
		$del_loc_hold = "DELETE FROM LocationHold WHERE HoldID  = :HoldID";
		$res_loc_hold = $this->db->getConnection("IRECRUIT")->delete ( $del_loc_hold, array($params) );
		
		return $res_loc_hold;
	}
	
	/**
	 * @method		delLocationHoldIdByTimeInterval
	 * @return		associative array
	 */
	function delLocationHoldIdByTimeInterval() {
	
		//Delete Location Hold Information
		$del_loc_hold = "DELETE FROM LocationHold WHERE HoldID < (SELECT date_format(date_sub(now(), interval 1 hour),'%Y%m%d%H%m%s'))";
		$res_loc_hold = $this->db->getConnection("IRECRUIT")->delete ( $del_loc_hold, array($params) );
	
		return $res_loc_hold;
	}
	
	/**
	 * @method		insLocationHoldIdInfo
	 * @param		array $info
	 * @return		array
	 */
	function insLocationHoldIdInfo($info) {

		$ins_loc_hold_id_info = $this->db->buildInsertStatement('LocationHold', $info);

		$ins_loc_hold = $ins_loc_hold_id_info["stmt"] . " ON DUPLICATE KEY UPDATE OrgID = :UOrgID, MultiOrgID = :UMultiOrgID, ApplicationID = :UApplicationID, RequestID = :URequestID, Task = :UTask, source = :Usource";

		$ins_params = $ins_loc_hold_id_info["info"];
		$ins_params[0][':UOrgID'] = $ins_params[0][':OrgID'];
		$ins_params[0][':UMultiOrgID'] = $ins_params[0][':MultiOrgID'];
		$ins_params[0][':UApplicationID'] = $ins_params[0][':ApplicationID'];
		$ins_params[0][':URequestID'] = $ins_params[0][':RequestID'];
		$ins_params[0][':UTask'] = $ins_params[0][':Task'];
		$ins_params[0][':Usource'] = $ins_params[0][':source'];
		
		$res_loc_hold_id_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_loc_hold, $ins_params );
	
		return $res_loc_hold_id_info;
	}

	/**
	 * @method		linkToLocationHoldTask
	 * @param		array $loc_info
	 * @return		URL link
	 */
	function linkToLocationHoldTask($loc_info) {

	 $link="";

	 if (is_array($loc_info)) {

	   $locdata = "?OrgID=" . $loc_info['OrgID'];

	   if ($loc_info['MultiOrgID'] != "") {
		   $locdata .= "&MultiOrgID=" . $loc_info['MultiOrgID'];
	   }
	   if (($loc_info['Task'] != "Application") && ($loc_info['ApplicationID'] != "")) {
		   $locdata .= "&ApplicationID=" . $loc_info['ApplicationID'];
	   }
	   if ($loc_info['RequestID'] != "") {
		   $locdata .= "&RequestID=" . $loc_info['RequestID'];
	   }

	   $link .= USERPORTAL_HOME;

	   if ($loc_info['Task'] == "Application") {

	     $link .= "jobApplication.php";
	     $link .= $locdata;

	   } else if ($loc_info['Task'] == "EditApplication") {

	     $link .= "editApplicationForm.php";
	     $link .= $locdata;

	   } else if ($loc_info['Task'] == "AssignedForms") {

	     $link .= "assignedInternalForms.php";
	     $link .= $locdata;
	     $link .= "&navpg=profiles&navsubpg=view";

	   } else {

	     $link .= "index.php";

	   }

	 } // end if array

	 return  $link;

	} // end function


	/**
	 * @method		createLocationHoldLink
	 * @param		string OrgID
	 * @param		string MultiOrgID
	 * @param		string ApplicationID 
	 * @param		string RequestID
	 * @param		string Task
	 * @return		string URL Link
	 */
	function createRequestHoldLink($OrgID,$MultiOrgID,$ApplicationID,$RequestID,$Task) {

           $TASKS=array('Application','EditApplication','AssignedForms');

	   // Application, used by public links to hold reference to requisition while logging in or registering for userportal 
	   // EditApplication, used by Lead processing template to have user directed to edit application page after login
	   // AssignedForms, used by UserPortal templage for iConnect reminders to the users direct to the forms for an application after login.

	   $link = USERPORTAL_HOME . "requestHold.php?OrgID=" . $OrgID;

	   if ($MultiOrgID != "") { $link .= "&MultiOrgID=" . $MultiOrgID; }
	   if ($ApplicationID != "") {$link .= "&ApplicationID=" . $ApplicationID; }
	   if ($RequestID != "") {$link .= "&RequestID=" . $RequestID; }
	   if (in_array($Task,$TASKS)) { $link .= "&Task=" . $Task; } 

	   return  $link;

	} // end function
	
	
	###################################################################################
	
	/**
	 * @method		displayEEO
	 * @param 		$EEOCode
	 * @return 		string
	 */
	function displayEEO($EEOCode) {
		
		//Set parameters for prepared query
		$params[":Code"] = array($EEOCode);
		
		$sel_description = "SELECT Description FROM EEOClassifications WHERE Code = :Code";
		list ( $Description ) = $this->db->getConnection("IRECRUIT")->fetchRow($sel_description, array($params));
	
		return $Description;
	} // end function
	
	/**
	 * @method		getEEOJobDescription
	 * @param 		$EEOCode
	 * @return 		string
	 */
	function getEEOJobDescription($EEOCode) {
		
		//Set parameters for prepared query
		$params	=	array(":Code"=>$EEOCode);
		$sel_eeo_classifications =  "SELECT * FROM EEOClassifications WHERE Code = :Code";
		$res_eeo_classifications =  $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_eeo_classifications, array($params));
	
		return $res_eeo_classifications['Description'];
	} // end function
	
	
	/**
	 * @method		getEEOClassificationsInfo
	 * @param 		string $OrgID
	 * @param 		string $MultiOrgID
	 * @return 		string
	 */
	function getEEOClassificationsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		//Set parameters for prepared query
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_eeo_classifications = "SELECT $columns FROM EEOClassifications";
		if(count($where_info) > 0) {
			$sel_eeo_classifications .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_eeo_classifications .= " ORDER BY " . $order_by;
	
		$res_eeo_classifications = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_eeo_classifications, $info );
	
		return $res_eeo_classifications;
	}
	
	
	/**
	 * @method		displayLocation
	 * @param 		$OrgID, $MultiOrgID, $OrgLevelID, $SelectionOrder
	 * @return 		string
	 */
	function displayLocation($OrgID, $MultiOrgID, $OrgLevelID, $SelectionOrder) {
		
		//Set parameters for prepared query
		$params[":OrgID"] = array($OrgID);
		$params[":MultiOrgID"] = array((string)$MultiOrgID);
		$params[":OrgLevelID"] = array($OrgLevelID);
				
		$sel_org_level = "SELECT OrganizationLevel FROM OrganizationLevels 
						  WHERE OrgID = :OrgID  
						  AND MultiOrgID = :MultiOrgID 
						  AND OrgLevelID = :OrgLevelID";
		list ( $OrganizationLevel ) = $this->db->getConnection("IRECRUIT")->fetchRow($sel_org_level, array($params));
	
		$params[":SelectionOrder"] = array($SelectionOrder);

		$sel_cat_selection = "SELECT CategorySelection FROM OrganizationLevelData 
							  WHERE OrgID = :OrgID  
							  AND MultiOrgID = :MultiOrgID  
							  AND OrgLevelID = :OrgLevelID  
							  AND SelectionOrder = :SelectionOrder";
		list ( $CategorySelection ) = $this->db->getConnection("IRECRUIT")->fetchRow($sel_cat_selection, array($params));
	
		return $OrganizationLevel . ": " . $CategorySelection;
	} // end function
	
	
	/**
	 * @method		getWotcOrgDataInfo
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getWotcOrgDataInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM OrgData";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "WOTC" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	/**
	 * @method		getOrganizationEmailInfo
	 * @param 		string $OrgID
	 * @param 		string $MultiOrgID
	 * @return 		string
	 */
	function getOrganizationEmailInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		//Set parameters for prepared query
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_org_email = "SELECT $columns FROM OrganizationEmail";
	
		if(count($where_info) > 0) {
			$sel_org_email .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org_email .= " ORDER BY " . $order_by;
	
		$res_org_email = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org_email, $info );
	
		return $res_org_email;
	}
	
	/**
	 * @method		getExportType
	 */
	function getExportType($OrgID) {
		
		//Set parameters for prepared query
		$params[":OrgID"] = array($OrgID);
		
		$sel_export_type = "SELECT ExportType FROM DataManager WHERE OrgID = :OrgID";
		$res_export_type = $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_export_type, array($params));
		
		return $res_export_type['ExportType'];
	}
	
	/**
	 * @method		getOrganizationLogosInformation
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getOrganizationLogosInformation($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org_logos = "SELECT $columns FROM OrganizationLogos";
	
		if(count($where_info) > 0) {
			$sel_org_logos .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org_logos .= " ORDER BY " . $order_by;
	
		$res_org_logos = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org_logos, $info );
	
		return $res_org_logos;
	}
	
	/**
	 * @method		getActiveLevels
	 * @return 		array
	 */
	function getActiveLevels($OrgID, $MultiOrgID, $Internal) {

		$query = "SELECT RequisitionOrgLevels.OrgLevelID, RequisitionOrgLevels.SelectionOrder";
		$query .= ", count(*) AS cnt";
		$query .= " FROM RequisitionOrgLevels";
		$query .= " LEFT JOIN Requisitions";
		$query .= " ON RequisitionOrgLevels.OrgID = Requisitions.OrgID";
		$query .= " AND RequisitionOrgLevels.RequestID = Requisitions.RequestID";
		$query .= " WHERE RequisitionOrgLevels.OrgID = '$OrgID'";
		$query .= " AND RequisitionOrgLevels.RequestID IN";
		$query .= " (";
		$query .= "SELECT RequestID FROM Requisitions";
		$query .= " WHERE OrgID = '$OrgID'";
		$query .= " AND MultiOrgID = '$MultiOrgID'";
		$query .= " AND Active = 'Y'";

    	if ($Internal == "Y") {
    	    $query .= " AND (PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    	} else {
    	    $query .= " AND (PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    	}
        	
		$query .= ")";

		$query .= " GROUP BY RequisitionOrgLevels.OrgLevelID, RequisitionOrgLevels.SelectionOrder;";

		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $query, array() );

	    return $res_org;

	}
	
	/**
	 * @method		getOrganizationLevelsInfo
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getOrganizationLevelsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM OrganizationLevels";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	
	/**
	 * @method		getOrganizationLevelDataInfo
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getOrganizationLevelDataInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM OrganizationLevelData";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	/**
	 * @method		getEmploymentStatusLevelsInfo
	 * @param		string $columns
	 * @param		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		array
	 */
	function getEmploymentStatusLevelsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM EmploymentStatusLevels";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	/**
	 * @method		insEmploymentStatusLevelsInfo
	 * @param		array $info
	 * @return		array
	 */
	function insEmploymentStatusLevelsInfo($info) {
		
		$ins_emp_status_levels_info = $this->db->buildInsertStatement('EmploymentStatusLevels', $info);
		$ins_emp_status_levels = $ins_emp_status_levels_info["stmt"] . " ON DUPLICATE KEY UPDATE EmploymentStatusLevel = :UEmploymentStatusLevel";
		$ins_params = $ins_emp_status_levels_info["info"];
		$ins_params[0][':UEmploymentStatusLevel'] = $ins_params[0][':EmploymentStatusLevel'];
		
		$res_emp_status_levels_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_emp_status_levels,  $ins_params);
	
		return $res_emp_status_levels_info;
	}
	
	/**
	 * @method		delEmploymentStatusLevelsInfo
	 * @param		$where_info, $info
	 */
	function delEmploymentStatusLevelsInfo($where_info = array(), $info = array()) {
		
		$del_emp_status_levels_info = "DELETE FROM EmploymentStatusLevels";
	
		if (count ( $where_info ) > 0) {
			$del_emp_status_levels_info .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		$res_emp_status_levels_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_emp_status_levels_info, $info );
	
		return $res_emp_status_levels_info;
	}
	
	/**
	 * @method		getOrgAndReqLevelsInfo
	 */
	function getOrgAndReqLevelsInfo($table_name, $columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_org = "SELECT $columns FROM $table_name";
	
		if(count($where_info) > 0) {
			$sel_org .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
	
		$res_org = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_org, $info );
	
		return $res_org;
	}
	
	/**
	 * @method	getSearchQuery
	 */
	function getSearchQuery($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM SearchQuery";
	
		if(count($where_info) > 0) $sel_info .= " WHERE " . implode(" AND ", $where_info);
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
		
		$res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method	updSearchQuery
	 * @param 	string 	$set
	 * @param 	string 	$where_info
	 * @param 	string 	$info
	 * @return 	array
	 */
	function updSearchQuery($set = '', $where_info = array(), $info = array()) {
	
		$upd_search_query  = "UPDATE SearchQuery SET ";
		$upd_search_query .= $set;
	
		if(count($where_info) > 0) {
			$upd_search_query .= " WHERE " . implode(" AND ", $where_info);
		}
	
		$res_search_query = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_search_query, $info );
	
		return $res_search_query;
	}
	
	/**
	 * @method		insSearchQuery
	 * @param		array $info
	 * @return		array
	 */
	
	function insSearchQuery($insert_info, $on_update = '', $update_info = array()) {
		
		//Set parameters for prepared query
		$ins_search_query = $this->db->buildInsertStatement('SearchQuery', $insert_info);
	
		$insert_statement = $ins_search_query["stmt"];
		if($on_update !=  '') {
			$insert_statement = $ins_search_query["stmt"].$on_update;
			foreach($update_info as $upd_key=>$upd_value) {
				$ins_search_query["info"][0][$upd_key] = $upd_value;
			}
		}
		
		$res_form_data = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $ins_search_query["info"] );
		return $res_form_data;
	}
	
	/**
	 * @method		delSearchQuery
	 * @param		array $info
	 * @return		array
	 */
	function delSearchQuery($OrgID, $UserID) {
		
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
		//Set parameters for prepared query
		$del_search_query = "DELETE FROM SearchQuery WHERE OrgID = :OrgID AND UserID = :UserID";
		$res_search_query = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_search_query, array($params) );
		return $res_search_query;
	}
}
