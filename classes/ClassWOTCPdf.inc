<?php
/**
 * @class		WOTCPdf
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCPdf
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    function createPDFs($wotcID, $ApplicationID) {

        list ($PROCESSOR, $ORG, $APPDATA, $PRS, $Signature, $Date, $One, $Two, $Three, $Four, $Five, $Six, $Seven, $workedA, $workedAN, $ck12A, $ck12AN, $ck13A, $ck13AN, $ck13B, $ck13BN, $ck13C, $ck13CN, $ck13D, $ck13DN, $ck13E, $ck13EN, $ck14A, $ck14AN, $ck14B, $ck14BN, $ck15A, $ck15AN, $ck15B, $ck15BN, $ck15C, $ck15CN, $ck16A, $ck16AN, $ck16B, $ck16BN, $ck16C, $ck16CN, $ck16D, $ck16DN, $ck17A, $ck17AN, $ck18A, $ck18AN, $ck19A, $ck19AN, $ck20A, $ck20AN, $ck21A, $ck21AN, $ck22A, $ck22AN, $ck23A, $ck23AN, $items) = G::Obj('WOTCPdf')->getData($wotcID, $ApplicationID);
        
	$PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
	$employmentconfirmation=$PARENT['EmploymentConfirmation'];
        
        $dir    =   WOTC_DIR . 'vault/' . $wotcID;
        
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $appvaultdir    =   $dir . '/applicantvault';
        
        if (! file_exists($appvaultdir)) {
            mkdir($appvaultdir, 0700);
            chmod($appvaultdir, 0777);
        }
        
        // 8850
        $XFDF8850BEGIN = <<<END
<?xml version="1.0" encoding="UTF-8"?>
<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">
<f href="8850.pdf"/>
<fields>
END;
        $XFDF .= '<field name="Name"><value>';
        $XFDF .= $APPDATA['FirstName'] . " ";
        if ($APPDATA['MiddleName'] != "") {
            $XFDF .= $APPDATA['MiddleName'] . " ";
        }
        $XFDF .= $APPDATA['LastName'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="SSN"><value>';
        $XFDF .= $APPDATA['Social1'] . "-";
        $XFDF .= $APPDATA['Social2'] . "-";
        $XFDF .= $APPDATA['Social3'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Address"><value>';
        $XFDF .= $APPDATA['Address'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="CityTownZip"><value>';
        $XFDF .= $APPDATA['City'] . ", ";
        $XFDF .= $APPDATA['State'] . " ";
        $XFDF .= $APPDATA['ZipCode'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="County"><value>';
        $XFDF .= $APPDATA['County'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Telephone"><value>';
        $XFDF .= $APPDATA['PhoneAreaCode'] . "-";
        $XFDF .= $APPDATA['PhonePrefix'] . "-";
        $XFDF .= $APPDATA['PhoneSuffix'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="DOB"><value>';
        $XFDF .= $APPDATA['DOB_Date'];
        $XFDF .= '</value></field>' . "\n";
        $XFDF .= <<<END
<field name="One"><value>$One</value></field>
<field name="Two"><value>$Two</value></field>
<field name="Three"><value>$Three</value></field>
<field name="Four"><value>$Four</value></field>
<field name="Five"><value>$Five</value></field>
<field name="Six"><value>$Six</value></field>
<field name="Seven"><value>$Seven</value></field>
END;
        
        $XFDF .= '<field name="Signature"><value>';
        $XFDF .= $Signature;
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Date"><value>';
        $XFDF .= $Date;
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EmployersName"><value>';
        $XFDF .= $ORG['OrganizationName'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EmpTelephone"><value>';
        $XFDF .= $ORG['PhoneAreaCode'] . "-";
        $XFDF .= $ORG['PhonePrefix'] . "-";
        $XFDF .= $ORG['PhoneSuffix'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EIN"><value>';
        $XFDF .= $ORG['EIN1'] . "-";
        $XFDF .= $ORG['EIN2'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EmpAddress"><value>';
        $XFDF .= $ORG['Address'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EmpCityStateZip"><value>';
        $XFDF .= $ORG['City'] . ", ";
        $XFDF .= $ORG['State'] . " ";
        $XFDF .= $ORG['ZipCode'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="DiffContact"><value>';
        $XFDF .= $PROCESSOR['OrganizationName'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="DiffTelephone"><value>';
        $XFDF .= $PROCESSOR['PhoneAreaCode'] . "-";
        $XFDF .= $PROCESSOR['PhonePrefix'] . "-";
        $XFDF .= $PROCESSOR['PhoneSuffix'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="DiffAddress"><value>';
        $XFDF .= $PROCESSOR['Address'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="DiffCityStateZip"><value>';
        $XFDF .= $PROCESSOR['City'] . ", ";
        $XFDF .= $PROCESSOR['State'] . " ";
        $XFDF .= $PROCESSOR['ZipCode'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="GaveInformation"><value>';
        if ($employmentconfirmation == 'Y') {
            $XFDF .= date("m/d/Y", strtotime($PRS['OfferDate']));
        } else {
            $XFDF .= $Date;
        }
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="OfferedJob"><value>';
        $XFDF .= date("m/d/Y", strtotime($PRS['OfferDate']));
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Hired"><value>';
        $XFDF .= date("m/d/Y", strtotime($PRS['HiredDate']));
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Started"><value>';
        $XFDF .= date("m/d/Y", strtotime($PRS['StartDate']));
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Title"><value>';
        $XFDF .= 'POA';
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="SignedDate"><value>';
        $XFDF .= date("m/d/Y");
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF8850END = <<<END
<?xml version="1.0" encoding="UTF-8"?>
</fields>
<ids original="5F9F7DF4D29F24254185F353C6E01EC5" modified="9577CFDBB54F4880B7091569A84B6B2F"/>
</xfdf>
END;
        
        if ($APPDATA['State'] == "TX") {
            $pdfform = WOTC_DIR . 'forms/8850-2.pdf';
        } else {
            $pdfform = WOTC_DIR . 'forms/8850.pdf';
        }
        
        $filename   =   WOTC_DIR . 'vault/' . $wotcID . '/' . $ApplicationID . '_8850.xfdf';
        $outputpdf  =   WOTC_DIR . 'vault/' . $wotcID . '/' . $ApplicationID . '_8850.pdf';
        $filename   =   $filename;
        
        $fh         =   fopen($filename, 'w');
        if (! $fh) {
            die('Cannot open file ' . $filename);
        }
        fwrite($fh, $XFDF8850BEGIN . $XFDF . $XFDF8850END);
        fclose($fh);
        chmod($filename, 0666);
        
        $command = '/usr/local/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        
        // DEV, Quality are /usr/bin - no local
        if ($_SERVER['HTTP_HOST'] == "dev.irecruit-us.com") {
            $command = '/usr/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        }
        if ($_SERVER['HTTP_HOST'] == "quality.irecruit-us.com") {
            $command = '/usr/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        }
        
        system($command);
        unlink($filename);
        chmod($outputpdf, 0666);
        
        // 9061
        $XFDF9061BEGIN = <<<END
<?xml version="1.0" encoding="UTF-8"?>
<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">
<f href="9061.pdf"/>
<fields>
END;
        
        $XFDF .= '<field name="AgencyDateReceived"><value>';
        // $XFDF .= date("m/d/Y", strtotime($PRS['Received']));
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EmployerName"><value>';
        $XFDF .= $ORG['OrganizationName'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="9061Address"><value>';
        $XFDF .= $ORG['Address'] . "\n";
        $XFDF .= $ORG['City'] . ", ";
        $XFDF .= $ORG['State'] . " ";
        $XFDF .= $ORG['ZipCode'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Telephone"><value>';
        $XFDF .= $ORG['PhoneAreaCode'] . "-";
        $XFDF .= $ORG['PhonePrefix'] . "-";
        $XFDF .= $ORG['PhoneSuffix'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="9061EIN"><value>';
        $XFDF .= $ORG['EIN1'] . "-";
        $XFDF .= $ORG['EIN2'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="LastFirstMI"><value>';
        $XFDF .= $APPDATA['LastName'] . ", ";
        $XFDF .= $APPDATA['FirstName'];
        if ($APPDATA['MiddleName'] != "") {
            $XFDF .= " " . $APPDATA['MiddleName'];
        }
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="9061SSN"><value>';
        $XFDF .= $APPDATA['Social1'] . "-";
        $XFDF .= $APPDATA['Social2'] . "-";
        $XFDF .= $APPDATA['Social3'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="workeddate"><value>';
        $XFDF .= $APPDATA['Worked_Date'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="EmploymentStartDate"><value>';
        $XFDF .= date("m/d/Y", strtotime($PRS['StartDate']));
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="StartingWage"><value>';
        $XFDF .= $PRS['StartingWage'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="Position"><value>';
        $XFDF .= $PRS['Position'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="9061DOB"><value>';
        $XFDF .= $APPDATA['DOB_Date'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="recipient13"><value>';
        $XFDF .= $APPDATA['VET_Recipient'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="citystate13"><value>';
        $XFDF .= $APPDATA['VET_City'] . ", ";
        $XFDF .= $APPDATA['VET_State'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="recipient14"><value>';
        $XFDF .= $APPDATA['SNAP_Recipient'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="citystate14"><value>';
        $XFDF .= $APPDATA['SNAP_City'] . ", ";
        $XFDF .= $APPDATA['SNAP_State'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="recipient16"><value>';
        $XFDF .= $APPDATA['TANF_Recipient'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="citystate16"><value>';
        $XFDF .= $APPDATA['TANF_City'] . ", ";
        $XFDF .= $APPDATA['TANF_State'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="felonyconvictiondate"><value>';
        $XFDF .= $APPDATA['FelonyConviction_Date'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="felonyreleasedate"><value>';
        $XFDF .= $APPDATA['FelonyRelease_Date'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="federalconviction"><value>';
        if ($APPDATA['Felony2'] == "F") {
            $XFDF .= "Yes";
        } else {
            $XFDF .= "Off";
        }
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="stateconviction"><value>';
        if ($APPDATA['Felony2'] == "S") {
            $XFDF .= "Yes";
        } else {
            $XFDF .= "Off";
        }
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="RCC"><value>';
        $XFDF .= $APPDATA['RCEZName'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="unemploymentstate"><value>';
        $XFDF .= $APPDATA['LTU_State'];
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= <<<END
<field name="whoEmployer"><value>Off</value></field>
<field name="whoParticipatingAgency"><value>Off</value></field>
<field name="whoParentGuardian"><value>Off</value></field>
<field name="whoConsultant"><value>Yes</value></field>
<field name="whoApplicant"><value>Off</value></field>
<field name="whoSWA"><value>Off</value></field>
<field name="workedA"><value>$workedA</value></field>
<field name="workedAN"><value>$workedAN</value></field>
<field name="12A"><value>$ck12A</value></field>
<field name="12AN"><value>$ck12AN</value></field>
<field name="13A"><value>$ck13A</value></field>
<field name="13AN"><value>$ck13AN</value></field>
<field name="13B"><value>$ck13B</value></field>
<field name="13BN"><value>$ck13BN</value></field>
<field name="13C"><value>$ck13C</value></field>
<field name="13CN"><value>$ck13CN</value></field>
<field name="13D"><value>$ck13D</value></field>
<field name="13DN"><value>$ck13DN</value></field>
<field name="13E"><value>$ck13E</value></field>
<field name="13EN"><value>$ck13EN</value></field>
<field name="14A"><value>$ck14A</value></field>
<field name="14AN"><value>$ck14AN</value></field>
<field name="14B"><value>$ck14B</value></field>
<field name="14BN"><value>$ck14BN</value></field>
<field name="15A"><value>$ck15A</value></field>
<field name="15AN"><value>$ck15AN</value></field>
<field name="15B"><value>$ck15B</value></field>
<field name="15BN"><value>$ck15BN</value></field>
<field name="15C"><value>$ck15C</value></field>
<field name="15CN"><value>$ck15CN</value></field>
<field name="16A"><value>$ck16A</value></field>
<field name="16AN"><value>$ck16AN</value></field>
<field name="16B"><value>$ck16B</value></field>
<field name="16BN"><value>$ck16BN</value></field>
<field name="16C"><value>$ck16C</value></field>
<field name="16CN"><value>$ck16CN</value></field>
<field name="16D"><value>$ck16D</value></field>
<field name="16DN"><value>$ck16DN</value></field>
<field name="17A"><value>$ck17A</value></field>
<field name="17AN"><value>$ck17AN</value></field>
<field name="18A"><value>$ck18A</value></field>
<field name="18AN"><value>$ck18AN</value></field>
<field name="19A"><value>$ck19A</value></field>
<field name="19AN"><value>$ck19AN</value></field>
<field name="20A"><value>$ck20A</value></field>
<field name="20AN"><value>$ck20AN</value></field>
<field name="21A"><value>$ck21A</value></field>
<field name="21AN"><value>$ck21AN</value></field>
<field name="22A"><value>$ck22A</value></field>
<field name="22AN"><value>$ck22AN</value></field>
<field name="23A"><value>$ck23A</value></field>
<field name="23AN"><value>$ck23AN</value></field>
END;
        
        $XFDF .= '<field name="9061Signature"><value>';
        if ($APPDATA['State'] == "TX") {
            $XFDF .= 'electronically signed by Lisa Schneider';
        } else {
            $XFDF .= 'electronically signed by Brian Kelly';
        }
        // $XFDF .= $Signature;
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF .= '<field name="9061Date"><value>';
        $XFDF .= $Date;
        $XFDF .= '</value></field>' . "\n";
        
        $XFDF9061END = <<<END
</fields>
<ids original="13644DD5B520BE362935EEB1C3BA3C3C" modified="602B9EF45966074B8A40596B0C82DB32"/>
</xfdf>
END;
        
        $pdfform    =   WOTC_DIR . 'forms/9061.pdf';
        $filename   =   WOTC_DIR . 'vault/' . $wotcID . '/' . $ApplicationID . '_9061.xfdf';
        $outputpdf  =   WOTC_DIR . 'vault/' . $wotcID . '/' . $ApplicationID . '_9061.pdf';
        
        $filename   =   $filename;
        $outputpdf  =   $outputpdf;

        $fh         =   fopen($filename, 'w');
        if (! $fh) {
            die('Cannot open file' . $filename);
        }
        fwrite($fh, $XFDF9061BEGIN . $XFDF . $XFDF9061END);
        fclose($fh);
        chmod($filename, 0666);
        
        $command    =   '/usr/local/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        
        // DEV, Quality are /usr/bin - no local
        if ($_SERVER['HTTP_HOST'] == "dev.irecruit-us.com") {
            $command = '/usr/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        }
        if ($_SERVER['HTTP_HOST'] == "quality.irecruit-us.com") {
            $command = '/usr/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        }
        
        system($command);
        unlink($filename);
        chmod($outputpdf, 0666);
        
        // write combined file
        // 88509061
        $XFDF88509061BEGIN = <<<END
<?xml version="1.0" encoding="UTF-8"?>
<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">
<f href="88509061.pdf"/>
<fields>
END;
        
        $XFDF88509061END = <<<END
</fields>
<ids original="13644DD5B520BE362935EEB1C3BA3C3C" modified="602B9EF45966074B8A40596B0C82DB32"/>
</xfdf>
END;
        if ($APPDATA['State'] == "TX") {
            $pdfform = WOTC_DIR . 'forms/88509061-2.pdf';
        } else {
            $pdfform = WOTC_DIR . 'forms/88509061.pdf';
        }
        
        $filename   =   WOTC_DIR . 'vault/' . $wotcID . '/' . $ApplicationID . '_88509061.xfdf';
        $outputpdf  =   WOTC_DIR . 'vault/' . $wotcID . '/' . $ApplicationID . '_88509061.pdf';
        $filename   =   $filename;

        $fh         =   fopen($filename, 'w');
        if (! $fh) {
            die('Cannot open file' . $filename);
        }
        fwrite($fh, $XFDF88509061BEGIN . $XFDF . $XFDF88509061END);
        fclose($fh);
        chmod($filename, 0666);
        
        $command = '/usr/local/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        
        // DEV, Quality are /usr/bin - no local
        if ($_SERVER['HTTP_HOST'] == "dev.irecruit-us.com") {
            $command = '/usr/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        }
        if ($_SERVER['HTTP_HOST'] == "quality.irecruit-us.com") {
            $command = '/usr/bin/pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($filename) . ' output ' . escapeshellarg($outputpdf);
        }

        system($command);
        unlink($filename);
        chmod($outputpdf, 0666);
        
        $query = "UPDATE ApplicantData";
        $query .= " SET Processed = 'Y',";
        $query .= " Processed_Date = now(),";
        $query .= " DMStatus = 'Y',";
        $query .= " DMProcessDate = now()";
        $query .= " WHERE wotcID = :wotcid and ApplicationID = :applicationid";
        $params = array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID);
        $this->db->getConnection ( $this->conn_string )->update($query, array($params));
    } // end function

    function getData($wotcID, $ApplicationID)
    {

	$PROCESSOR=array();
	$PROCESSOR['OrganizationName']="Cost Management Services, LLC";
	$PROCESSOR['Address']="321 Main Street";
	$PROCESSOR['City']="Farmington";
	$PROCESSOR['State']="CT";
	$PROCESSOR['ZipCode']="06032";

        $PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
	$AKADBA = G::Obj('WOTCcrm')->getAKADBAInfo($wotcID);

	$ORG=array();
	$ORG['OrganizationName']=$AKADBA['AKADBA'];
	$ORG['Address']=$AKADBA['Address'];
	$ORG['City']=$AKADBA['City'];
	$ORG['State']=$AKADBA['State'];
	$ORG['ZipCode']=$AKADBA['ZipCode'];
	$CP = json_decode($AKADBA['Phone'], true);
	$ORG['PhoneAreaCode'] = $CP[0];
        $ORG['PhonePrefix'] = $CP[1];
        $ORG['PhoneSuffix'] = $CP[2];
	$ORG['EIN1']=$AKADBA['EIN1'];
	$ORG['EIN2']=$AKADBA['EIN2'];
        
        $query      =   "SELECT * FROM ApplicantData";
        $query      .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params     =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID);
        $APPDATA    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $query      =   "SELECT if(StartDate = '0000-00-00','',StartDate) StartDate,";
        $query      .=  " IF(Received = '0000-00-00','',Received) Received,";
        $query      .=  " IF(Filed = '0000-00-00','',Filed) Filed,";
        $query      .=  " IF(Confirmed = '0000-00-00','',Confirmed) Confirmed,";
        $query      .=  " IF(OfferDate = '0000-00-00','',OfferDate) OfferDate,";
        $query      .=  " IF(HiredDate = '0000-00-00','',HiredDate) HiredDate,";
        $query      .=  " StartingWage, Position, Qualified, Category, Comments";
        $query      .=  " FROM Processing";
        $query      .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params     =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID);
        $PRS        =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        if ($APPDATA['Signature'] != "") {
            $Signature = 'electronically signed by ' . $APPDATA['FirstName'] . ' ' . $APPDATA['LastName'];
            $Date = date("m/d/Y", strtotime($APPDATA['EntryDate']));
        }
        
        // 8850 toggles
        $One    =   "Off";
        $Two    =   "Off";
        $Three  =   "Off";
        $Four   =   "Off";
        $Five   =   "Off";
        $Six    =   "Off";
        $Seven  =   "Off";
        
        if ($APPDATA['TANF'] == "Y") {
            $Two = "Yes";
        }
        if (($APPDATA['Veteran'] == "Y") && ($APPDATA['Veteran6'] == "Y")) {
            $Two = "Yes";
        }
        if (($APPDATA['SWA2'] == "Y") || ($APPDATA['SWA3'] == "Y") || ($APPDATA['SWA4'] == "Y")) {
            $Two = "Yes";
        }
        if (($APPDATA['DOB'] == "Y") && ($APPDATA['SNAP'] == "Y")) {
            $Two = "Yes";
        }
        if ($APPDATA['Felony'] == "Y") {
            $Two = "Yes";
        }
        if ($APPDATA['SSI'] == "Y") {
            $Two = "Yes";
        }
        if (($APPDATA['Veteran'] == "Y") && (($APPDATA['Veteran2'] == "Y") || ($APPDATA['Veteran3'] == "Y"))) {
            $Two = "Yes";
        }
        
        if (($APPDATA['Veteran'] == "Y") && ($APPDATA['Veteran2'] == "Y")) {
            $Three = "Yes";
        }
        
        if (($APPDATA['Veteran'] == "Y") && (($APPDATA['Veteran4'] == "Y") && ($APPDATA['Veteran5'] == "Y"))) {
            $Four = "Yes";
        }
        
        if (($APPDATA['Veteran'] == "Y") && ($APPDATA['Veteran5'] == "Y") && ($APPDATA['Veteran2'] == "Y")) {
            $Five = "Yes";
        }
        
        if ($APPDATA['TANF'] == "Y") {
            $Six = "Yes";
        }
        if ($APPDATA['LTU'] == "Y") {
            $Seven = "Yes";
        }
        // end 8850
        
        // 9061 toggles
        $workedA    =   "Off";
        $workedAN   =   "Off";
        
        $ck12A      =   "Off";
        $ck12AN     =   "Off";
        
        $ck13A      =   "Off";
        $ck13AN     =   "Off";
        $ck13B      =   "Off";
        $ck13BN     =   "Off";
        $ck13C      =   "Off";
        $ck13CN     =   "Off";
        $ck13D      =   "Off";
        $ck13DN     =   "Off";
        $ck13E      =   "Off";
        $ck13EN     =   "Off";
        
        $ck14A      =   "Off";
        $ck14AN     =   "Off";
        $ck14B      =   "Off";
        $ck14BN     =   "Off";
        
        $ck15A      =   "Off";
        $ck15AN     =   "Off";
        $ck15B      =   "Off";
        $ck15BN     =   "Off";
        $ck15C      =   "Off";
        $ck15CN     =   "Off";
        
        $ck16A      =   "Off";
        $ck16AN     =   "Off";
        $ck16B      =   "Off";
        $ck16BN     =   "Off";
        $ck16C      =   "Off";
        $ck16CN     =   "Off";
        $ck16D      =   "Off";
        $ck16DN     =   "Off";
        
        $ck17A      =   "Off";
        $ck17AN     =   "Off";
        
        $ck18A      =   "Off";
        $ck18AN     =   "Off";
        
        $ck19A      =   "Off";
        $ck19AN     =   "Off";
        
        $ck20A      =   "Off";
        $ck20AN     =   "Off";
        
        $ck21A      =   "Off";
        $ck21AN     =   "Off";
        
        $ck22A      =   "Off";
        $ck22AN     =   "Off";
        
        $ck23A      =   "Off";
        $ck23AN     =   "Off";
        
        // worked
        if ($APPDATA['Worked'] == 'Y') {
            $workedA = "Yes";
        } else {
            $workedAN = "Yes";
        }
        
        // question 12
        $question12 = 0;
        if ($APPDATA['DOB'] == 'Y') {
            $ck12A = "Yes";
            $question12 ++;
        } else {
            $ck12AN = "Yes";
        }
        
        // question 13
        $question13 = 0;
        if ($APPDATA['Veteran'] == "Y") {
            $ck13A = "Yes";
            $question13 ++;
            
            /* new */
            if ($APPDATA['Veteran6'] == "Y") {
                $ck13B = "Yes";
                $question13 ++;
            } else {
                $ck13BN = "Yes";
            }
            if ($APPDATA['Veteran5'] == "Y") {
                $ck13C = "Yes";
                $question13 ++;
            } else {
                $ck13CN = "Yes";
            }
            if ($APPDATA['Veteran2'] == "Y") {
                $ck13E = "Yes";
                $question13 ++;
            } else {
                $ck13EN = "Yes";
            }
            if ($APPDATA['Veteran4'] == "Y") {
                $ck13D = "Yes";
                $question13 ++;
            } else {
                $ck13DN = "Yes";
            }
            /* new */
        } else {
            $ck13AN = "Yes";
        }
        
        // question 14
        $question14 = 0;
        if ($APPDATA['SNAP'] == "Y") {
            $ck14A = "Yes";
            $question14 ++;
        } else {
            $ck14AN = "Yes";
        }
        
        if ($APPDATA['SNAP2'] == "Y") {
            $ck14B = "Yes";
            $question14 ++;
        } else {
            $ck14BN = "Yes";
        }
        
        // question 15
        $question15 = 0;
        if ($APPDATA['SWA2'] == "Y") {
            $ck15A = "Yes";
            $question15 ++;
        } else {
            $ck15AN = "Yes";
        }
        
        if ($APPDATA['SWA3'] == "Y") {
            $ck15B = "Yes";
            $question15 ++;
        } else {
            $ck15BN = "Yes";
        }
        
        if ($APPDATA['SWA4'] == "Y") {
            $ck15C = "Yes";
            $question15 ++;
        } else {
            $ck15CN = "Yes";
        }
        
        // question 16
        $question16 = 0;
        if ($APPDATA['TANF'] == "Y") {
            $ck16A = "Yes";
            $ck16BN = "Yes";
            $ck16CN = "Yes";
            $ck16DN = "Yes";
            $question16 ++;
        } else {
            $ck16AN = "Yes";
            $ck16B = "Off";
            $ck16C = "Off";
            $ck16D = "Off";
        }
        
        // question 17
        $question17 = 0;
        if ($APPDATA['Felony'] == "Y") {
            $ck17A = "Yes";
            $question17 ++;
        } else {
            $ck17AN = "Yes";
        }
        
        // question 18
        $question18 = 0;
        // if ($APPDATA['EnterpriseZone'] == "Y") {
        if (($APPDATA['EnterpriseZone'] == "Y") && ($APPDATA['DOB'] == 'Y')) {
            // 6/23/2016 Lisa indicates change to only within DOB
            $ck18A = "Yes";
            $question18 ++;
        } else {
            $ck18AN = "Yes";
        }
        
        // question 19
        $question19 = 0;
        if (($APPDATA['EnterpriseZone'] == "Y") && ($APPDATA['DOB'] == 'Y')) {
            // 4/26/2016 Lisa indicates always no
            // $ck19A="Yes";
            // $question19++;
        } else {
            $ck19AN = "Yes";
        }
        
        // question 20
        $question20 = 0;
        if ($APPDATA['SSI'] == "Y") {
            $ck20A = "Yes";
            $question20 ++;
        } else {
            $ck20AN = "Yes";
        }
        
        // question 21
        $question21 = 0;
        if (($APPDATA['Veteran'] == "Y") && ($APPDATA['Veteran2'] == "Y")) {
            $ck21A = "Yes";
            $question21 ++;
        } else {
            $ck21AN = "Yes";
        }
        
        // question 22
        $question22 = 0;
        if (($APPDATA['Veteran'] == "Y") && ($APPDATA['Veteran3'] == "Y")) {
            $ck22A = "Yes";
            $question22 ++;
        } else {
            $ck22AN = "Yes";
        }
        
        // question 23
        $question23 = 0;
        if ($APPDATA['LTU'] == "Y") {
            $ck23A = "Yes";
            $question23 ++;
        } else {
            $ck23AN = "Yes";
        }
        
        $items = "";
        
        if ($question12 > 0) {
            $items .= '<li> One item of proof of age and residence.<br>Examples: Drives License, School I.D. Card, Work Permit, Federal/State/Local Gov\'t I.D. Copy of Hospital Record of Birth, Birth Certificate' . "\n";
        }
        
        if ($question13 > 0) {
            $items .= '<li> DD-214 or Discharge Papers' . "\n";
            $items .= '<li> Reserve Unit Contacts' . "\n";
            $items .= '<li> FL 21-802 (Issued ONLY by DVA. Certifies a Veteran with a service connected disability)' . "\n";
            $items .= '<li> UI claims records (for unemployed status)' . "\n";
        }
        
        if (($question14 > 0) || ($question16 > 0)) {
            $items .= '<li> TANF/SNAP (Food Stamp) Benefit History' . "\n";
            $items .= '<li> Signed Statement from Authorized Individual with Specific Description of the Months Benefits Were Received' . "\n";
            $items .= '<li> Case Number Identifier' . "\n";
        }
        
        if ($question15 > 0) {
            $items .= '<li> Vocational Rehabilitation - Agency Contact' . "\n";
            if (($question14 == 0) && ($question16 == 0)) {
                $items .= '<li> Signed Statement from Authorized Individual with Specific Description of the Months Benefits Were Received' . "\n";
            }
            $items .= '<li> Veterans Administration' . "\n";
        }
        
        if ($question17 > 0) {
            $items .= '<li> Parole Officer\'s Name or Statement' . "\n";
            $items .= '<li> Correction Institution Records' . "\n";
            $items .= '<li> Court Records Extracts' . "\n";
        }
        
        if ($question19 > 0) {
            $items .= '<li> SSI Record Authorization' . "\n";
            $items .= '<li> SSI Contact' . "\n";
            $items .= '<li> Evidence of SSI Benefits' . "\n";
        }
        
        if ($question20 > 0) {
            $items .= '<li> Unemployment Insurance (UI) Claims Records' . "\n";
            $items .= '<li> UI Wage Records' . "\n";
        }
        
        if ($question21 > 0) {
            if ($question20 <= 0) {
                $items .= '<li> Unemployment Insurance (UI) Claims Records' . "\n";
                $items .= '<li> UI Wage Records' . "\n";
            }
        }
        
        return array(
            $PROCESSOR,
            $ORG,
            $APPDATA,
            $PRS,
            $Signature,
            $Date,
            $One,
            $Two,
            $Three,
            $Four,
            $Five,
            $Six,
            $Seven,
            $workedA,
            $workedAN,
            $ck12A,
            $ck12AN,
            $ck13A,
            $ck13AN,
            $ck13B,
            $ck13BN,
            $ck13C,
            $ck13CN,
            $ck13D,
            $ck13DN,
            $ck13E,
            $ck13EN,
            $ck14A,
            $ck14AN,
            $ck14B,
            $ck14BN,
            $ck15A,
            $ck15AN,
            $ck15B,
            $ck15BN,
            $ck15C,
            $ck15CN,
            $ck16A,
            $ck16AN,
            $ck16B,
            $ck16BN,
            $ck16C,
            $ck16CN,
            $ck16D,
            $ck16DN,
            $ck17A,
            $ck17AN,
            $ck18A,
            $ck18AN,
            $ck19A,
            $ck19AN,
            $ck20A,
            $ck20AN,
            $ck21A,
            $ck21AN,
            $ck22A,
            $ck22AN,
            $ck23A,
            $ck23AN,
            $items
        );
    } // end function
    
    public function downloadPDF($wotcID, $ApplicationID, $Type)
    {
        $filename = WOTC_DIR . "vault/" . $wotcID . "/" . $ApplicationID . "_" . $Type . ".pdf";
        
        if (file_exists($filename)) {
            
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($filename));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            exit();
        } else {
            
            $rtn = "<div style=\"margin:60px;font-family:Arial,Helvetica;\">\n";
            $rtn .= "File not found";
            $rtn .= "</div>\n";
            
            return $rtn;
        }
    } // end function
} // end Class
