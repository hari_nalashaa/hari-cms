<?php
/**
 * @class		CustomWebForm
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class CustomWebForm {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method 		getApplicantVaultInfo
	 * @param
	 */
	function getApplicantVaultInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	    $sel_applicant_vault = "SELECT $columns FROM ApplicantVault";
	
	    if (count ( $where_info ) > 0) {
	        $sel_applicant_vault .= " WHERE " . implode ( " AND ", $where_info );
	    }
	
	    if ($order_by != "")
	        $sel_applicant_vault .= " ORDER BY " . $order_by;
	
	    $res_applicant_vault = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_vault, $info );
	
	    return $res_applicant_vault;
	}
	
	/**
	 * @method     insCustomWebForm
	 * @param      $info
	 */
	function insCustomWebForm($info) {
	
	    $ins_app_vault = $this->db->buildInsertStatement ('CustomWebForm', $info );
	    $res_app_vault = $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_vault ['stmt'], $ins_app_vault ['info'] );
	
	    return $res_app_vault;
	}
	
	/**
	 * @method		updCustomWebForm
	 * @param		$info
	 */
	function updCustomWebForm($info) {
	
	    $upd_applicant_vault = "UPDATE CustomWebForm SET FileName = :FileName, FileType = :FileType, Date = NOW(), UserID = :UserID
								WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND UpdateID = :UpdateID";
	    $res_applicant_vault = $this->db->getConnection ( $this->conn_string )->update ( $upd_applicant_vault, $info );
	
	    return $res_applicant_vault;
	}
	
	/**
	 * @method 		delApplicantVault
	 * @param		$where_info = array(), $info = array()
	 */
	function delApplicantVault($where_info = array(), $info = array()) {
	
	    $del_applicant_vault = "DELETE FROM ApplicantVault";
	
	    $del_applicant_vault .= " WHERE " . implode ( " AND ", $where_info );
	
	    $res_applicant_vault = $this->db->getConnection ( $this->conn_string )->delete ( $del_applicant_vault, $info );
	
	    return $res_applicant_vault;
	}
	
	/**
	 * @method		getUploadedDocumentsCount
	 * @param 		$OrgID, $ApplicationID, $RequestID
	 * @return		integer
	 */
	function getUploadedDocumentsCount($OrgID, $ApplicationID, $RequestID) {
	
	    // Set parameters for prepared query
	    $params        =   array (":OrgID" => $OrgID, ":ApplicationID" => $ApplicationID, ":RequestID" => $RequestID);
	    $sel_doc_count =   "SELECT FileName, FileType, Date, UserID from ApplicantVault";
	    $sel_doc_count .=  " WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND CONCAT('', UserID * 1) = UserID";
	    $res_doc_count =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_doc_count, array ($params) );
	
	    return $res_doc_count;
	}
}
