<?php
/**
 * @class		UserPortalInfo
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class UserPortalInfo {

	var $conn_string       =   "USERPORTAL";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
    /**
	 * @method		getUserApplicationsInfo
	 * @return		array
	 * @tutorial	This method will fetch the iconnect email template information.
	 */
	public function getUserApplicationsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns  =   $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info =   "SELECT $columns FROM UserApplications";
		
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
		$res_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
		
		return $res_info;
	}
	
	
	/**
	 * @method	getUserPortalInfo
	 * @param	string $OrgID, $MultiOrgID
	 * @return	numeric array
	 */
	function getUserPortalInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_user_portal_info = "SELECT $columns FROM UserPortalInfo";
		if(count($where_info) > 0) {
			$sel_user_portal_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_user_portal_info .= " ORDER BY " . $order_by;
		$res_user_portal_info = $this->db->getConnection( "IRECRUIT" )->fetchAllAssoc ( $sel_user_portal_info, $info );
		
		return $res_user_portal_info;
	}
	
	/**
	 * @method getUserPortalInfoByOrgIDMultiOrgID
	 * @param  string $OrgID, $MultiOrgID
	 * @return numeric array
	 */
	function getUserPortalInfoByOrgIDMultiOrgID($OrgID, $MultiOrgID, $SortOrder = 0) {
		
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		$sel_user_portal_info = "SELECT * FROM UserPortalInfo WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	
		if($SortOrder == 0) $sel_user_portal_info .= " AND SortOrder = 0";
		else $sel_user_portal_info .= " AND SortOrder > 0";
	
		$sel_user_portal_info .= " ORDER BY SortOrder";
	
		$res_user_portal_info = $this->db->getConnection( "IRECRUIT" )->fetchAllAssoc ( $sel_user_portal_info, array($params) );
	
		return $res_user_portal_info;
	}
	
	/**
	 * @method		insUserPortalInfo
	 * @param		array $info
	 * @return		array
	 */
	function insUserPortalInfo($info, $on_update = '', $update_info = array()) {
		
		$ins_userportal_info = $this->db->buildInsertStatement('UserPortalInfo', $info);
	
		$insert_statement = $ins_userportal_info ["stmt"];
		if ($on_update != '') {
			$insert_statement .= $on_update;
			if (is_array ( $update_info )) {
				foreach ( $update_info as $upd_key => $upd_value ) {
					$ins_userportal_info ["info"][0][$upd_key] = $upd_value;
				}
			}
		}
	
		$res_userportal_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $ins_userportal_info["info"] );
	
		return $res_userportal_info;
	}
	
	/**
	 * @method	delUserPortalInfo
	 * @param	$where_info, $info
	 */
	function delUserPortalInfo($where_info = array(), $info = array()) {
		
		$del_userportal_info = "DELETE FROM UserPortalInfo";
		if (count ( $where_info ) > 0) {
			$del_userportal_info .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		$res_userportal_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_userportal_info, $info );
	
		return $res_userportal_info;
	}
	
	/**
	 * @method	updUserPortalInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updUserPortalInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_userportal_info = $this->db->buildUpdateStatement('UserPortalInfo', $set_info, $where_info);
		$res_userportal_info = $this->db->getConnection( "IRECRUIT" )->update($upd_userportal_info, $info);
	
		return $res_userportal_info;
	}

	/**
	 * @method     getApplicationInformation
	 * @param      string $OrgID, $UserID, $RequestID
	 * @return     numeric array
	 */
	function getApplicationInformation($OrgID, $MultiOrgID, $UserID, $RequestID) {
	
	    $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID);
	    $sel_app_info  =   "SELECT * FROM ApplicantProcessData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID";
	    $res_app_info  =   $this->db->getConnection( "USERPORTAL" )->fetchAllAssoc ( $sel_app_info, array($params) );
	
	    return $res_app_info;
	}
	
	
	/**
	 * @method     getApplicationQuestionInformation
	 * @param      string $OrgID, $UserID, $RequestID, $QuestionID
	 * @return     numeric array
	 */
	function getApplicationQuestionInformation($OrgID, $MultiOrgID, $UserID, $RequestID, $QuestionID) {

	    $params        =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID, ":QuestionID"=>$QuestionID);
	    $sel_app_info  =   "SELECT Answer FROM ApplicantProcessData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID AND QuestionID = :QuestionID";
	    $res_app_info  =   $this->db->getConnection( "USERPORTAL" )->fetchAssoc ( $sel_app_info, array($params) );
	
	    return $res_app_info["Answer"];
	}
	
	/**
	 * @method		insApplicationFormInfo
	 * @param		array   $info
	 * @tutorial	Here $info contains set of rows data that have to insert
	 *           	All the values will be passed as in documentation
	 */
	function insApplicationFormInfo($QI) {

	    if(!isset($QI['AnswerStatus'])) $QI['AnswerStatus'] = 0;
	    if($QI['value'] == '{Date}') $QI['Answer'] = "NOW()";
	    
	    $params_form_info          =   array(":OrgID"=>$QI['OrgID'], ":MultiOrgID"=>$QI['MultiOrgID'], ":UserID"=>$QI['UserID'], ":SectionID"=>$QI['SectionID'], ":RequestID"=>$QI['RequestID'], ":QuestionID"=>$QI['QuestionID'], ":IAnswer"=>$QI['Answer'], ":UAnswer"=>$QI['Answer']);
	    $ins_application_form_info =   "INSERT INTO ApplicantProcessData(OrgID, MultiOrgID, UserID, SectionID, RequestID, QuestionID, Answer) VALUES(:OrgID, :MultiOrgID, :UserID, :SectionID, :RequestID, :QuestionID, :IAnswer) ON DUPLICATE KEY UPDATE Answer = :UAnswer";
	    $res_application_form_info =   $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_application_form_info, array($params_form_info) );
	}
	
	/**
	 * @method		insApplicantData
	 * @param		array $OrgID, $UserID, $RequestID, $ApplicationID
	 * @tutorial	Here $info contains set of rows data that have to insert
	 *           	All the values will be passed as in documentation
	 */
	function insApplicantData($OrgID, $MultiOrgID, $UserID, $RequestID, $ApplicationID) {
	
	    $params            =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID);
	    $sel_app_info      =   "SELECT * FROM ApplicantProcessData WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID";
	    $res_app_info      =   $this->db->getConnection( "USERPORTAL" )->fetchAllAssoc ( $sel_app_info, array($params) );
	    $app_info_results  =   $res_app_info['results'];
	    $app_info_res_cnt  =   $res_app_info['count'];
	    for($i = 0; $i < $app_info_res_cnt; $i++) {
	    
	        if($app_info_results[$i]['QuestionID'] == "ApplicantPicture") {
	            $path_parts            =   pathinfo($app_info_results[$i]['Answer']);
	            $orig_file_name        =   $path_parts['filename'];
	            $file_extension        =   $path_parts['extension'];
                $file_name             =   $ApplicationID."-ApplicantPicture".".".$file_extension;
                //Rename old file to new name
                @rename(IRECRUIT_DIR . "vault/".$OrgID."/applicant_picture/".$app_info_results[$i]['Answer'], IRECRUIT_DIR . "vault/".$OrgID."/applicant_picture/".$file_name);
	            $app_params            =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>$app_info_results[$i]['QuestionID'], ":Answer"=>$file_name, ":UAnswer"=>$file_name);
	            $ins_applicant_data    =   "INSERT INTO ApplicantData(OrgID, ApplicationID, QuestionID, Answer) VALUES (:OrgID, :ApplicationID, :QuestionID, :Answer)";
	            $ins_applicant_data   .=   " ON DUPLICATE KEY UPDATE Answer = :UAnswer";
	            $res_applicant_data    =   $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_applicant_data, array($app_params) );
	        }
	        else {
	            //Skip fields
	            $skip_fields           =   array("OrgID", "ApplicationID", "QuestionID");
	            $info                  =   array(
                                                "OrgID"             =>  $OrgID,
                                                "ApplicationID"     =>  $ApplicationID, 
                            	                "QuestionID"        =>  $app_info_results[$i]['QuestionID'],
                                                "Answer"            =>  $app_info_results[$i]['Answer'],
	                                       );
	            $ins_applicant_data	   =   $this->db->buildOnDuplicateKeyUpdateStatement("ApplicantData", $info, $skip_fields);
	            $res_applicant_data    =   $this->db->getConnection ( "IRECRUIT" )->insert (  $ins_applicant_data["stmt"], $ins_applicant_data["info"] );
	        }
	    }
	    
	    $app_params            =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>'PortalUserID', ":Answer"=>$UserID, ":UAnswer"=>$UserID);
	    $ins_applicant_data    =   "INSERT INTO ApplicantData(OrgID, ApplicationID, QuestionID, Answer) VALUES (:OrgID, :ApplicationID, :QuestionID, :Answer)";
	    $ins_applicant_data    .=  " ON DUPLICATE KEY UPDATE Answer = :UAnswer";
	    $res_applicant_data    =   $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_applicant_data, array($app_params) );
	}
	
	/**
	 * @method		getApplicantAttachments
	 * @param		$OrgID, $UserID, $RequestID, $ApplicationID
	 * @tutorial
	 */
	public function getApplicantAttachments($OrgID, $MultiOrgID, $UserID, $RequestID) {
	
	    $params_attachments_info   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID);
	    $select_attachments_info   =   "SELECT * FROM ApplicantAttachments WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID";
	    $result_attachments_info   =   $this->db->getConnection( "USERPORTAL" )->fetchAllAssoc ( $select_attachments_info, array($params_attachments_info) );
	    $result_attachments_res    =   $result_attachments_info['results'];
	    $result_attachments_cnt    =   $result_attachments_info['count'];
	
	    return $result_attachments_res;
	}
	
	/**
	 * @method		insApplicantAttachments
	 * @param		$OrgID, $UserID, $RequestID, $ApplicationID
	 * @tutorial	
	 */
	public function insApplicantAttachments($OrgID, $MultiOrgID, $UserID, $RequestID, $ApplicationID) {
	
	    $params_attachments_info   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID);
	    $select_attachments_info   =   "SELECT * FROM ApplicantAttachments WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID";
	    $result_attachments_info   =   $this->db->getConnection( "USERPORTAL" )->fetchAllAssoc ( $select_attachments_info, array($params_attachments_info) );
        $result_attachments_res    =   $result_attachments_info['results'];
        $result_attachments_cnt    =   $result_attachments_info['count'];
        
        for($i = 0; $i < $result_attachments_cnt; $i++) {
 
            $params_info                        =   array();
            $params_info[':OrgID']              =   $result_attachments_res[$i]['OrgID'];
            $params_info[':TypeAttachment']     =   $result_attachments_res[$i]['TypeAttachment'];
            $params_info[':PurposeName']        =   $result_attachments_res[$i]['PurposeName'];
            $params_info[':FileType']           =   $result_attachments_res[$i]['FileType'];
            $params_info[':ApplicationID']      =   $ApplicationID;
            $params_info[':UApplicationID']     =   $ApplicationID;
            $params_info[':UPurposeName']       =   $result_attachments_res[$i]['PurposeName'];
            
            $old_file_name                      =   IRECRUIT_DIR . "vault/".$OrgID."/applicantattachments/".$UserID."*".$RequestID."-".$result_attachments_res[$i]['PurposeName'].".".$result_attachments_res[$i]['FileType'];
            $new_file_name                      =   IRECRUIT_DIR . "vault/".$OrgID."/applicantattachments/".$ApplicationID."-".$result_attachments_res[$i]['PurposeName'].".".$result_attachments_res[$i]['FileType'];

            //Rename old file to new name
            rename($old_file_name, $new_file_name);
            
            $ins_attachments                    =   "INSERT INTO ApplicantAttachments(OrgID, ApplicationID, TypeAttachment, PurposeName, FileType)";
            $ins_attachments                   .=   " VALUES(:OrgID, :ApplicationID, :TypeAttachment, :PurposeName, :FileType) ON DUPLICATE KEY UPDATE ApplicationID = :UApplicationID, PurposeName = :UPurposeName";
            $res_attachments                    =   $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_attachments, array($params_info) );
            
        }
        
        return $res_attachments;
	}
	
	/**
	 * @method		insApplicantAttachmentsTemp
	 * @param		$attachment_info
	 * @tutorial	buildInsertStatement will generate the insert
	 * 				query based on the key value pair array
	 */
	public function insApplicantAttachmentsTemp($attachment_info) {
	
	    $ins_app_atchmnt_info  =   $this->db->buildInsertStatement('ApplicantAttachments', $attachment_info);
	    $ins_attachment_stmt   =   $ins_app_atchmnt_info["stmt"] . " ON DUPLICATE KEY UPDATE PurposeName = :UPurposeName";
	    $ins_params            =   $ins_app_atchmnt_info["info"];
	    
	    $ins_params[0][':UPurposeName']    =   $ins_params[0][':PurposeName'];
	    
	    if($ins_params[0][':FileType'] != "") {
	        $ins_attachment_stmt          .=   ", FileType = :UFileType";
	        $ins_params[0][':UFileType']   =   $ins_params[0][':FileType'];
	    }
	    
	    $res_app_atchmnt_info = $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_attachment_stmt, $ins_params );
	
	    return $res_app_atchmnt_info;
	}
	
	/**
	 * @method		getPendingUserApplicationsInfo
	 * @param		$OrgID, $UserID, $RequestID
	 * @tutorial
	 */
	public function getPendingUserApplicationsInfo($OrgID, $MultiOrgID, $UserID) {
	
	    $params_user_apps_info     =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID);
	    $select_user_apps_info     =   "SELECT OrgID, MultiOrgID, UserID, RequestID, Status, DATE_FORMAT(LastUpdatedDate,'%m/%d/%Y') AS LastUpdatedDate";
	    $select_user_apps_info    .=   " FROM UserApplications";
	    $select_user_apps_info    .=   " WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND Status = 'Pending' AND Deleted = 'No'";
	    $result_user_apps_info     =   $this->db->getConnection( "USERPORTAL" )->fetchAllAssoc ( $select_user_apps_info, array($params_user_apps_info) );
	
	    return $result_user_apps_info;
	}
	
	/**
	 * @method		getUserApplications
	 * @param		$OrgID, $UserID, $RequestID
	 * @tutorial
	 */
	public function getUserApplications($OrgID, $MultiOrgID, $UserID, $RequestID) {
	
	    $params_user_apps_info     =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID);
	    $select_user_apps_info     =   "SELECT * FROM UserApplications WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID";
	    $result_user_apps_info     =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $select_user_apps_info, array($params_user_apps_info) );
	
	    return $result_user_apps_info;
	}

	/**
	 * @method		getAllUserApplicationsList
	 * @param		$OrgID, $UserID, $RequestID
	 * @tutorial
	 */
	public function getAllUserApplicationsList() {
	
	    $select_user_apps_info     =   "SELECT * FROM UserApplications WHERE Status = 'Finished' AND ApplicationID = ''";
	    $result_user_apps_info     =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $select_user_apps_info);
	
	    return $result_user_apps_info;
	}
	
	
	/**
	 * @method		insUserApplications
	 * @param		array $info
	 * @tutorial	
	 */
	function insUserApplications($OrgID, $MultiOrgID, $UserID, $RequestID, $ApplicationID, $RequisitionTitle, $Status) {

	    $params_info   =   array(":IOrgID"=>$OrgID, ":IMultiOrgID"=>$MultiOrgID, ":IUserID"=>$UserID, ":IRequestID"=>$RequestID, ":IRequisitionTitle"=>$RequisitionTitle, ":IStatus"=>$Status, ":UStatus"=>$Status);
	    $ins_user_app  =   "INSERT INTO UserApplications(OrgID, MultiOrgID, UserID, RequestID, RequisitionTitle, Status, LastUpdatedDate) VALUES(:IOrgID, :IMultiOrgID, :IUserID, :IRequestID, :IRequisitionTitle, :IStatus, NOW()) ON DUPLICATE KEY UPDATE Status = :UStatus, LastUpdatedDate = NOW()";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_user_app, array($params_info));

	    if($ApplicationID != "") {
	        $params_info   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	        $upd_user_app  =   "UPDATE UserApplications SET ApplicationID = :ApplicationID WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND UserID = :UserID AND RequestID = :RequestID";
	        $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->update ( $upd_user_app, array($params_info));
	    }
	    
	    return $res_user_app;
	}
	
	/**
	 * @method		updUserApplicationsDeletedStatus
	 * @param		array $info
	 * @tutorial
	 */
	function updUserApplicationsDeletedStatus($OrgID, $MultiOrgID, $RequestID, $UserID, $DeleteStatus) {
	
	    $params_info   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID, ":UserID"=>$UserID, ":Deleted"=>$DeleteStatus);
	    $upd_user_app  =   "UPDATE UserApplications SET Deleted = :Deleted WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID AND UserID = :UserID";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->insert ( $upd_user_app, array($params_info));
	     
	    return $res_user_app;
	}

	/**
	 * @method		updLastNotificationDateTime
	 * @param		array $info
	 * @tutorial
	 */
	function updLastNotificationDateTime($OrgID, $MultiOrgID, $RequestID, $UserID) {
	
	    $params_info   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID, ":UserID"=>$UserID);
	    $upd_user_app  =   "UPDATE UserApplications SET LastNotificationDateTime = NOW() WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID AND UserID = :UserID";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->insert ( $upd_user_app, array($params_info));
	
	    return $res_user_app;
	}
	
	########### User Portal Stepping Process ############
	/**
	 * @method		insUserPortalApplicationStepThemes
	 * @param		array $info
	 * @tutorial
	 */
	function insUserPortalApplicationStepThemes($Name, $NavbarColor, $PendingTabColor, $ActiveTabColor, $CompletedTabColor) {
	
	    $ThemeID       =   uniqid(time()).time();
	    $params_info   =   array(":ThemeID"=>$ThemeID, ":Name"=>$Name, ":Navbar"=>$NavbarColor, ":PendingTab"=>$PendingTabColor, ":ActiveTab"=>$ActiveTabColor, ":CompletedTab"=>$CompletedTabColor);
	    $ins_user_app  =   "INSERT INTO ApplicationStepsColorSettings(ThemeID, Name, Navbar, PendingTab, ActiveTab, CompletedTab) VALUES(:ThemeID, :Name, :Navbar, :PendingTab, :ActiveTab, :CompletedTab)";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_user_app, array($params_info));
	    
	    return $res_user_app;
	}

	/**
	 * @method		updUserPortalApplicationStepTheme
	 * @param		array $info
	 * @tutorial
	 */
	function updUserPortalApplicationStepTheme($ThemeID, $Name, $NavbarColor, $PendingTabColor, $ActiveTabColor, $CompletedTabColor) {
	
	    $params_info   =   array(":ThemeID"=>$ThemeID, ":Name"=>$Name, ":Navbar"=>$NavbarColor, ":PendingTab"=>$PendingTabColor, ":ActiveTab"=>$ActiveTabColor, ":CompletedTab"=>$CompletedTabColor);
	    $upd_user_app  =   "UPDATE ApplicationStepsColorSettings SET Name = :Name, Navbar = :Navbar, PendingTab = :PendingTab, ActiveTab = :ActiveTab, CompletedTab = :CompletedTab";
	    $upd_user_app .=   " WHERE ThemeID = :ThemeID";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->insert ( $upd_user_app, array($params_info));
	
	    return $res_user_app;
	}
	
	/**
	 * @method		updOrgUserPortalMainDefaultTheme
	 * @param		array $info
	 * @tutorial
	 */
	function updOrgUserPortalMainDefaultTheme($OrgID) {

	    $sel_info  =   "SELECT ThemeID FROM ApplicationStepsColorSettings WHERE DefaultThemeID = 'Y'";
	    $res_info  =   $this->db->getConnection ( "USERPORTAL" )->fetchAssoc ( $sel_info );

	    if(isset($res_info['ThemeID']) && $res_info['ThemeID'] != "") {
	        $params_info   =   array(":UserPortalThemeID"=>$res_info['ThemeID'], ":OrgID"=>$OrgID);
	        $upd_user_app  =   "UPDATE OrgData SET UserPortalThemeID = :UserPortalThemeID WHERE OrgID = :OrgID AND UserPortalThemeID = ''";
	        $res_user_app  =   $this->db->getConnection ( "IRECRUIT" )->update ( $upd_user_app, array($params_info));
	    }
	
	    return $res_user_app;
	}
	
	/**
	 * @method		updOrgUserPortalApplicationDefaultTheme
	 * @param		array $info
	 * @tutorial
	 */
	function updOrgUserPortalApplicationDefaultTheme($OrgID, $ThemeID) {
	
	    $params_info   =   array(":UserPortalThemeID"=>$ThemeID, ":OrgID"=>$OrgID);
	    $upd_user_app  =   "UPDATE OrgData SET UserPortalThemeID = :UserPortalThemeID WHERE OrgID = :OrgID";
	    $res_user_app  =   $this->db->getConnection ( "IRECRUIT" )->update ( $upd_user_app, array($params_info));
	
	    return $res_user_app;
	}
	
	/**
	 * @method		updMainUserPortalDefaultTheme
	 * @param		array $info
	 * @tutorial
	 */
	function updMainUserPortalDefaultTheme($ThemeID) {

	    $upd_user_app  =   "UPDATE ApplicationStepsColorSettings SET DefaultThemeID = 'N'";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->update ( $upd_user_app, array());
	    
	    $params_info   =   array(":ThemeID"=>$ThemeID);
	    $upd_user_app  =   "UPDATE ApplicationStepsColorSettings SET DefaultThemeID = 'Y' WHERE ThemeID = :ThemeID";
	    $res_user_app  =   $this->db->getConnection ( "USERPORTAL" )->update ( $upd_user_app, array($params_info));
	
	    return $res_user_app;
	}
	
	/**
	 * @method		insUserPortalApplicationStepThemes
	 * @param		array $info
	 * @tutorial
	 */
	function getUserPortalApplicationStepThemes() {
	
	    $params        =   array(":OrgID"=>$OrgID);
	    $sel_user_app  =   "SELECT * FROM ApplicationStepsColorSettings";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->fetchAllAssoc ( $sel_user_app, array($params) );
	
	    return $res_user_app;
	}
	
	/**
	 * @method		getUserPortalApplicationThemeInfo
	 * @param		array $info
	 * @tutorial
	 */
	function getUserPortalApplicationThemeInfo($ThemeID) {
	
	    $params        =   array(":ThemeID"=>$ThemeID);
	    $sel_user_app  =   "SELECT * FROM ApplicationStepsColorSettings WHERE ThemeID = :ThemeID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->fetchAssoc ( $sel_user_app, array($params) );
	
	    return $res_user_app;
	}
	
	/**
	 * @method		delUserPortalApplicationThemeInfo
	 * @param		array $info
	 * @tutorial
	 */
	function delUserPortalApplicationThemeInfo($ThemeID) {
	
	    $params        =   array(":ThemeID"=>$ThemeID);
	    $sel_user_app  =   "DELETE FROM ApplicationStepsColorSettings WHERE ThemeID = :ThemeID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->fetchAssoc ( $sel_user_app, array($params) );
	
	    return $res_user_app;
	}
	
	/**
	 * @method		getApplicantAttachmentInfo
	 * @param		$OrgID, $UserID, $RequestID, $ApplicationID
	 * @tutorial   
	 */
	public function getApplicantAttachmentInfo($OrgID, $MultiOrgID, $RequestID, $TypeAttachment, $UpUserID) {
	
	    $params_user   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID, ":TypeAttachment"=>$TypeAttachment, ":UserID"=>$UpUserID);
	    $sel_user_app  =   "SELECT * FROM ApplicantAttachments WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND TypeAttachment = :TypeAttachment AND UserID = :UserID AND RequestID = :RequestID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->fetchAssoc ( $sel_user_app, array($params_user) );
	
	    return $res_user_app;
	}
	
	/**
	 * @method     delApplicantAttachmentsTemp
	 * @param      $OrgID, $MultiOrgID, $RequestID, $QuestionID, $USERID
	 */
	function delApplicantAttachmentsTemp($OrgID, $MultiOrgID, $RequestID, $TypeAttachment, $USERID) {
	     
	    $params_user   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID, ":TypeAttachment"=>$TypeAttachment, ":UserID"=>$USERID);
	    $del_user_app  =   "DELETE FROM ApplicantAttachments WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID AND TypeAttachment = :TypeAttachment AND UserID = :UserID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->delete ( $del_user_app, array($params_user) );
	    
	    return $res_user_app;
	}

	/**
	 * @method		insUpdApplicantData
	 * @param		array $records_list
	 * @tutorial	Here $records_list contains set of rows data that have to insert
	 *           	all the values will be passed as in documentation
	 */
	function insUpdApplicantData($QI) {
	
	    //Skip fields
	    $skip_fields           =   array("OrgID", "ApplicationID", "QuestionID");
	    $info                  =   array(
                                        "OrgID"             =>  $QI['OrgID'],
                                        "ApplicationID"     =>  $QI['ApplicationID'],
                                        "QuestionID"        =>  $QI['QuestionID'],
                            	        "Answer"            =>  $QI['Answer']
                            	    );
	    $ins_applicant_data	   =   $this->db->buildOnDuplicateKeyUpdateStatement("ApplicantData", $info, $skip_fields);
	    $res_applicant_data    =   $this->db->getConnection ( "IRECRUIT" )->insert (  $ins_applicant_data["stmt"], $ins_applicant_data["info"] );
	}
	
	##############################################################
	### Delete Function After Launch This Stepping Module
	##############################################################
	/**
	 * @method		delApplicantTempData
	 * @param		array $info
	 * @tutorial
	 */
	function delApplicantTempData($UserID) {
	
	    $params        =   array(":UserID"=>$UserID);
	    $del_user_app  =   "DELETE FROM UserApplications WHERE UserID = :UserID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->delete ( $del_user_app, array($params) );
	
	    $del_user_app  =   "DELETE FROM ApplicantProcessData WHERE UserID = :UserID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->delete ( $del_user_app, array($params) );
	    
	    $del_user_app  =   "DELETE FROM ApplicantAttachments WHERE UserID = :UserID";
	    $res_user_app  =   $this->db->getConnection( "USERPORTAL" )->delete ( $del_user_app, array($params) );
	}
}
