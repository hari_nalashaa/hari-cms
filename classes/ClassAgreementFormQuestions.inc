<?php 
/**
 * @class		AgreementFormQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class AgreementFormQuestions {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    
    /**
     * @method		getQuestionDetails
     * @param		$OrgID, $FormID, $QuestionID
     * @return		array
     */
    function getQuestionDetails($columns = "*", $OrgID, $AgreementFormID, $QuestionID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        //Set parameters for prepared query
        $params_answer_info  = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionID"=>$QuestionID);
        $sel_que_answer_info = "SELECT $columns FROM AgreementFormQuestions WHERE OrgID = :OrgID AND AgreementFormID = :AgreementFormID AND QuestionID = :QuestionID";
        $row_que_answer_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_que_answer_info, array($params_answer_info));
    
        return $row_que_answer_info;
    }
    
    /**
     * @method		getAgreementFormQuestionsListByQueTypeID
     * @param		$OrgID, $AgreementFormID, $QuestionTypeID
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getAgreementFormQuestionsListByQueTypeID($OrgID, $AgreementFormID, $QuestionTypeID) {
    
    	$columns 		=	$this->db->arrayToDatabaseQueryString ( $columns );
    
    	$params_info	=	array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionTypeID"=>$QuestionTypeID);
    	$sel_que_info	=	"SELECT * FROM AgreementFormQuestions WHERE OrgID = :OrgID AND AgreementFormID = :AgreementFormID AND QuestionTypeID = :QuestionTypeID";
    	$res_que_info   =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_que_info, array($params_info) );
    
    	return $res_que_info;
    }
    
    /**
     * @method  getAgreementFormQuestionsList
     * @param   string $OrgID
     * @param   string $WebFormID
     * @param   string $AgreementFormID
     */
    function getAgreementFormQuestionsList($OrgID, $AgreementFormID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params_info        =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":Active"=>"Y");
        $sel_agr_form_ques  =   "SELECT $columns FROM AgreementFormQuestions WHERE OrgID = :OrgID AND AgreementFormID = :AgreementFormID AND Active = :Active ORDER BY QuestionOrder ASC";
        $res_agr_form_ques  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_agr_form_ques, array($params_info) );
        $res_agr_form_cnt   =   $res_agr_form_ques['count'];
        $res_agr_form_res   =   $res_agr_form_ques['results'];

        $agr_form_ques_list =   array();
        for($w = 0; $w < $res_agr_form_cnt; $w++) {
            $agr_form_ques_list[$res_agr_form_res[$w]['QuestionID']]   =   $res_agr_form_res[$w];
        }

        return $agr_form_ques_list;
    } 

}
?>
