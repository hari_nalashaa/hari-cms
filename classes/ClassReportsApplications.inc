<?php
/**
 * @class		ReportsApplications
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ReportsApplications {
	
	var $conn_string       =   "IRECRUIT";
		
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getUserApplications
	 * @return		array
	 */
	function getUserApplications($where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$sel_job_apps = "SELECT `OrgID`, `MultiOrgID`, `UserID`, `RequestID`, `RequisitionTitle`, `Status`, `LastUpdatedDate`, `ApplicationID`, `Deleted`, (SELECT `FirstName` FROM Users WHERE UserID = UA.UserID) AS FirstName, 
                        (SELECT `LastName` FROM Users WHERE UserID = UA.UserID) AS LastName,
                        (SELECT `Email` FROM Users WHERE UserID = UA.UserID) AS UserEmail
                        FROM UserApplications UA";

		if(count($where_info) > 0) {
			$sel_job_apps .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($group_by != "") $sel_job_apps .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_job_apps .= " ORDER BY " . $order_by;
		
		$res_job_apps = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_job_apps, $info );
		
		return $res_job_apps;
	}	

	/**
	 * @method		getUserApplicationsCount
	 * @return		array
	 */
	function getUserApplicationsCount($where_info = array(), $order_by = "", $info = array()) {
	
	    $sel_job_apps = "SELECT COUNT(*) as UserApplicationsCount FROM UserApplications UA";
	
	    if(count($where_info) > 0) {
	        $sel_job_apps .= " WHERE " . implode(" AND ", $where_info);
	    }
	
	    if($order_by != "") $sel_job_apps .= " ORDER BY " . $order_by;
	
	    $res_job_apps = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_job_apps, $info );
	
	    return $res_job_apps['UserApplicationsCount'];
	}
	
	
}
