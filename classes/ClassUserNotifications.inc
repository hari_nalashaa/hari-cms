<?php 
/**
 * @class		UserNotifications
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class UserNotifications {

	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}

	/**
	 * @method		getUserNotifications
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getUserNotifications($columns = "", $where_info = array(), $order_by = "", $info = array()) {

		$columns = $this->db->arrayToDatabaseQueryString ( $columns );

		$sel_user_notifications_info = "SELECT $columns FROM UserNotifications";
		if(count($where_info) > 0) {
			$sel_user_notifications_info .= " WHERE " . implode(" AND ", $where_info);
		}

		if($order_by != "") $sel_user_notifications_info .= " ORDER BY " . $order_by;

		$row_user_notifications_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_user_notifications_info, $info );

		return $row_user_notifications_info;
	}

	/**
	 * @method		getUserNotificationStatus
	 * @param		$UserID, $NotificationID
	 * @return		associative array
	 */
	function getUserNotificationStatus($UserID, $NotificationID) {
	
		$params	=	array(":UserID"=>$UserID, ":NotificationID"=>$NotificationID);
		$sel_user_notification_status = "SELECT * FROM UserNotificationStatus WHERE UserID = :UserID AND NotificationID = :NotificationID";
		$row_user_notification_status = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_user_notification_status, array($params) );
	
		return $row_user_notification_status;
	}
	
	/**
	 * @method		insUserNotification
	 * @param 		string $insert_info
	 * @return 		array
	 */
	function insUserNotification($insert_info) {
	
		//Set parameters for prepared query
		$ins_notification 	= $this->db->buildInsertStatement('UserNotifications', $insert_info);
		$res_notification	= $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_notification['stmt'], $ins_notification["info"] );
		
		return $res_notification;
	}
	
	/**
	 * @method		insUserNotificationStatus
	 * @param 		string $insert_info
	 * @return 		array
	 */
	function insUserNotificationStatus($insert_info) {
	
		//Set parameters for prepared query
		$ins_notification 	= "INSERT INTO UserNotificationStatus(`UserID`, `NotificationID`, `NotificationType`, `NotificationStatus`, `LastModified`) VALUES(:UserID, :INotificationID, :INotificationType, :INotificationStatus, NOW()) ON DUPLICATE KEY UPDATE LastModified = NOW(), NotificationType = :UNotificationType, NotificationStatus = :UNotificationStatus";
		$res_notification	= $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_notification, array($insert_info) );
	
		return $res_notification;
	}
	
	/**
	 * @method		delUserNotifications
	 * @param 		string $delete_info
	 * @return 		array
	 */
	function delUserNotifications($NotificationID) {
	
		$params = array(":NotificationID"=>$NotificationID);
		$del_notification = "DELETE FROM UserNotifications WHERE NotificationID = :NotificationID";
		$res_notification = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_notification, array($params) );
	
		$del_notification_status = "DELETE FROM UserNotificationStatus WHERE NotificationID = :NotificationID";
		$res_notification_status = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_notification_status, array($params) );
		
		return $res_notification_status;
	}
	
}	
?>
