<?php 
/**
 * @class		GenericQueries
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class GenericQueries {
	
    public $db;

    var $conn_string       =   "IRECRUIT";

    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      getAllRowsInfo
     */
    function getAllRowsInfo($table_name, $columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM $table_name";
	
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($group_by != "") $sel_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info['results'];
    }
    
    /**
     * @method      getInfoByQuery
     */
    function getInfoByQuery($query, $info = array()) {
    
        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $query, $info );
    
        return $res_info['results'];
    }
    
    /**
     * @method      getRowInfo
     */
    function getRowInfo($table_name, $columns, $where_info = array(), $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_info = "SELECT $columns FROM $table_name";
    
        if(count($where_info) > 0) {
            $sel_info .= " WHERE " . implode(" AND ", $where_info);
        }
    
        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, $info );
    
        return $res_info;
    }
	
	/**
	 * @method		getRowInfoByQuery
	 */
	function getRowInfoByQuery($query, $info = array()) {
	
	    $row_info  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, $info);
	
	    return $row_info;
	}
	
    /**
     * @method      insInfoByQuery
     * @param       $insert_query
     */
    function insInfoByQuery($insert_query, $info = array()) {
    
        $res_info   =   $this->db->getConnection( $this->conn_string )->insert($insert_query, $info);
    
        return $res_info;
    }

    /**
     * @method      insRowsInfo
     * @param       $info
     */
    function insRowsInfo($table_name, $info) {
    
        $ins_app_data   =   $this->db->buildInsertStatement($table_name, $info);
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data["stmt"], $ins_app_data["info"] );
    
        return $res_app_data;
    }
    
    /**
     * @method      updInfoByQuery
     * @param       $update_query
     */
    function updInfoByQuery($update_query, $info = array()) {
    
        $res_info   =   $this->db->getConnection( $this->conn_string )->update($update_query, $info);
    
        return $res_info;
    }
    
    /**
     * @method      updRowsInfo
     * @param       $set_info, $where_info, $info
     */
    function updRowsInfo($table_name, $set_info = array(), $where_info = array(), $info) {
    
        $upd_applications_info = $this->db->buildUpdateStatement($table_name, $set_info, $where_info);
        $res_applications_info = $this->db->getConnection( $this->conn_string )->update($upd_applications_info, $info);
    
        return $res_applications_info;
    }
    
    /**
     * @method		delRows
     * @param		$where_info = array(), $info = array()
     */
    function delRows($table_name, $where_info = array(), $info = array()) {
    
        $del_rows   =   "DELETE FROM $table_name";
    
        if (count ( $where_info ) > 0) {
            $del_rows   .=  " WHERE " . implode ( " AND ", $where_info );
        }
    
        $res_rows = $this->db->getConnection ( $this->conn_string )->delete ( $del_rows, $info );
    
        return $res_rows;
    }
}	
