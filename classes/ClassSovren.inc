<?php
/**
 * @class		Sovren
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Sovren {

    use SovrenParameters;
    
    public $db;

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
    public function __construct() {

	$this->headers[0]   =   "accept: application/xml";
        $this->headers[1]   =   "content-type: application/json; charset=utf-8";
        $this->headers[2]   =   "sovren-accountid: 28091668";
        $this->headers[3]   =   "sovren-servicekey: 8mMxZQZwwdLKE7dzWouXYseLC2/lrea3JpU8Ky+R";

        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method      populateFields
     * @param       string $OrgID
     * @param       string $HoldID
     * @return      array
     */
    public function populateFields($OrgID, $HoldID) {
        // take the data extracted and prepopulate the application form
    	
    	$APPDATA = array();
    
        $query  =   "SELECT * FROM ParsingTemp WHERE OrgID = :OrgID AND HoldID = :HoldID";
        $params =   array(':OrgID' => $OrgID, ':HoldID' => $HoldID);
        $DATA   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
    		
        if ($DATA['XML'] != "") {
            $APPDATA = $this->parseXML($DATA['XML']);
        }

    	return $APPDATA;
    } // end function

    /**
     * @method      getParsedResumeInfo
     * @param       string $OrgID
     * @param       string $UserID
     * @param       string $RequestID
     * @return      array
     */
    public function getParsedResumeInfo($OrgID, $ApplicationID) {
        // take the data extracted and prepopulate the application form
        $APPDATA    =   array();

        $params     =   array(':OrgID'=>$OrgID, ':ApplicationID'=>$ApplicationID);
        $query      =   "SELECT * FROM Parsing WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
        $DATA       =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $DATA;
        
    } // end function
    
    /**
     * @method      getUserPortalParsedResumeInfo
     * @param       string $OrgID
     * @param       string $UserID
     * @param       string $RequestID
     * @return      array
     */
    public function getUserPortalParsedResumeInfo($OrgID, $UserID, $RequestID) {
        // take the data extracted and prepopulate the application form
        $APPDATA = array();
    
        $query  =   "SELECT * FROM UserPortalResumeParsing WHERE OrgID = :OrgID AND UserID = :UserID AND RequestID = :RequestID";
        $params =   array(':OrgID'=>$OrgID, ':UserID'=>$UserID, ':RequestID'=>$RequestID);
        $DATA   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));

        if ($DATA['XML'] != "") {
            $APPDATA = $this->parseXML($DATA['XML']);
        }
         
        return $APPDATA;
    
    } // end function
    
    /**
     * @method      insPublicPortalParsingInfo
     * @param       string $OrgID
     * @param       string $UserID
     * @param       string $RequestID
     * @param       string $ApplicationID
     */
    public function insPublicPortalParsingInfo($OrgID, $HoldID, $RequestID, $ApplicationID) {
        
        $query   =  "INSERT INTO Parsing";
        $query  .=  " (OrgID, ApplicationID, XML, HTML, RTF, `TEXT`, FILEEXT, CreatedUpdatedDate)";
        $query  .=  " SELECT OrgID, '$ApplicationID', XML, HTML, RTF, `TEXT`, FILEEXT, NOW() FROM ParsingTemp";
        $query  .=  " WHERE OrgID = :OrgID AND HoldID = :HoldID AND RequestID = :RequestID ORDER BY CreatedUpdatedDate DESC LIMIT 1";
        $params =   array(":OrgID"=>$OrgID, ":HoldID"=>$HoldID, ":RequestID"=>$RequestID);
        $this->db->getConnection( $this->conn_string )->insert($query, array($params));
        
    }
    
    /**
     * @method      insUserPortalParsingInfo
     * @param       string $OrgID
     * @param       string $UserID
     * @param       string $RequestID
     * @param       string $ApplicationID
     */
    public function insUserPortalParsingInfo($OrgID, $UserID, $RequestID, $ApplicationID) {
         
        $query   =  "INSERT INTO Parsing";
        $query  .=  " (OrgID, ApplicationID, XML, HTML, RTF, `TEXT`, FILEEXT, CreatedUpdatedDate)";
        $query  .=  " SELECT OrgID, '$ApplicationID', XML, HTML, RTF, `TEXT`, FILEEXT, NOW() FROM UserPortalResumeParsing";
        $query  .=  " WHERE OrgID = :OrgID AND UserID = :UserID AND RequestID = :RequestID ORDER BY CreatedUpdatedDate DESC LIMIT 1";
        $params =   array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID);
        $this->db->getConnection( $this->conn_string )->insert($query, array($params));
        
    }
    
    /**
     * @method      processPublicPortalResumeForm
     * @param       string $OrgID
     * @param       string $HoldID
     * @param       string $FILE
     */
	public function processPublicPortalResumeForm($OrgID, $FormID, $RequestID, $HoldID, $FILE) {

	    //Get Purpose names list
	    $purpose    =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);
	     
        // parse the resume at Sovren and then put results in DB
	    if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com"
	        || $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com"
            ) {
	    	list ($xml, $text, $rtf, $html, $credits, $fileext) = $this->parseResume($FILE);
	    }
	    else {
	        list ($xml, $text, $rtf, $html, $credits, $fileext) = $this->parseResume($FILE);
	    }
	    
        $apatdir    =	IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
        $filename   =	$apatdir . '/' . $HoldID . '-' . $purpose ['resumeupload'] . '.' . $fileext;
        
        if($xml) {
            
            //Applicant Attachments Information
            $applicant_attach_info   =   array(
                                            "OrgID"            =>  $OrgID,
                                            "MultiOrgID"       =>  $MultiOrgID,
                                            "HoldID"           =>  $HoldID,
                                            "SectionID"        =>  6,
                                            "RequestID"        =>  $RequestID,
                                            "TypeAttachment"   =>  'resumeupload',
                                            "PurposeName"      =>  $purpose ['resumeupload'],
                                            "FileType"         =>  $fileext
                                        );
            //Insert applicant attachments information
            G::Obj('ApplicantAttachmentsTemp')->insApplicantAttachmentsTemp ( $applicant_attach_info );

            $query          =   "INSERT INTO ParsingTemp (OrgID, HoldID, RequestID, XML, HTML, RTF, `TEXT`, FILEEXT, CreatedUpdatedDate) VALUES (:OrgID, :HoldID, :RequestID, :XML, :HTML, :RTF, :TEXT, :FILEEXT, NOW())";
            $query          .=  " ON DUPLICATE KEY UPDATE XML = :UXML, HTML = :UHTML, RTF = :URTF, `TEXT` = :UTEXT, FILEEXT = :UFILEEXT, CreatedUpdatedDate = NOW()";
            $params         =   array(
                                    ":OrgID"        =>  $OrgID,
                                    ":HoldID"       =>  $HoldID,
                                    ":RequestID"    =>  $RequestID,
                                    ":XML"          =>  $xml,
                                    ":HTML"         =>  $html,
                                    ":RTF"          =>  $rtf,
                                    ":TEXT"         =>  $text,
                                    ":FILEEXT"      =>  $fileext,
                                    ":UXML"         =>  $xml,
                                    ":UHTML"        =>  $html,
                                    ":URTF"         =>  $rtf,
                                    ":UTEXT"        =>  $text,
                                    ":UFILEEXT"     =>  $fileext
                                );
            $res_org        =   G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
            $file_to_log    =   ROOT."logs/resume-parsing/". $OrgID . "-" . $HoldID . "-" . "resume.xml";
            $path_parts     =   pathinfo($file_to_log);

            //Directory does not exist, so lets create it.
            if(!is_dir($path_parts['dirname'])) {
            	mkdir($path_parts['dirname'], 0777);
            	chmod($path_parts['dirname'], 0777);
            }
                    
            $fp = fopen($file_to_log, 'w');
            fwrite($fp, $xml);
            fclose($fp);
            chmod($file_to_log, 0666);
                    
            if(!file_exists($filename)) {
            	$data  =   file_get_contents ( $FILE );
            	$fp    =   fopen($filename, 'w');
            	fwrite($fp, $data);
            	fclose($fp);
            	chmod($filename, 0666);
            }
                    
            if (($credits > 975) && ($credits < 1000)) {
            
                $message    =   "Credit count is: " . $credits;
                $to         =   "dedgecomb@irecruit-software.com";
                $subject    =   "Sovren Credits are LOW";
            
                //Clear properties
                G::Obj('PHPMailer')->clearCustomProperties();
                G::Obj('PHPMailer')->clearCustomHeaders();
                
                // Set who the message is to be sent to
                G::Obj('PHPMailer')->addAddress ( $to );
                // Set the subject line
                G::Obj('PHPMailer')->Subject = $subject;
                // convert HTML into a basic plain-text alternative body
                G::Obj('PHPMailer')->msgHTML ( $message );
                // Content Type Is HTML
                G::Obj('PHPMailer')->ContentType = 'text/plain';
                //Send email
                G::Obj('PHPMailer')->send ();
            }

        }
	} // end function

	/**
	 * @method     uploadResumeForm
	 * @param      string $OrgID
	 * @param      string $HoldID
	 * @param      string $FILE
	 */
	public function uploadResumeForm($OrgID, $HoldID) {

	    global $RequestID;
		
        $query  =   "SELECT HoldID FROM ParsingTemp WHERE OrgID = :OrgID AND HoldID = :HoldID";
        $params =   array(":OrgID"=>$OrgID, ":HoldID"=>$HoldID);
        $DATA   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
        $parsed =   $DATA['HoldID'];
        
        $rtn    =   "";
        
        if($parsed == "") {
        
            $rtn .= "\n<script type=\"text/javascript\">\n";
            
            $rtn .= "function uploadParseResume() {\n";
            
            $rtn .= "var resume_file = $('#resumefile').val();";
            $rtn .= "if(resume_file == '') {\n";
			$rtn .= "alert('Please select file from browse button to upload.')\n";
			$rtn .= "return false;";
            $rtn .= "}\n";
            $rtn .= "$('#resume_parsing_process_msg').html('Please wait...');";
            $rtn .= "var formData = new FormData($('#resumeparse')[0]);\n";
            
            $rtn .= "var request = new XMLHttpRequest();\n";
            $rtn .= "var response = \"\";\n";
            
            $rtn .= "request.onreadystatechange = function () { \n";
            $rtn .= " if(this.readyState ==4 && this.status ==200) {\n";
            $rtn .= "  if(this.responseText == \"RELOAD\") {\n";
            $rtn .= "$('#resume_parsing_process_msg').html('');";
            $rtn .= "   location.reload();\n";
            $rtn .= "  }\n";
            $rtn .= " }\n";
            $rtn .= "}\n";
            
            $rtn .= "request.open(\"POST\", \"parse.php\");\n";
            $rtn .= "request.send(formData);\n";
            
            $rtn .= "return false;\n";
            $rtn .= "}\n";
            $rtn .= "</script>\n";
            
            $rtn .= "<form id=\"resumeparse\" onsubmit=\"return uploadParseResume();\">\n";
            $rtn .= "<div class=\"row\">";
            $rtn .= "<div class=\"col-lg-3 col-md-3 col-sm-3\">";
            
            $rtn .= "To prefill this form please upload your resume.<br>\n";
            $rtn .= "<input type=\"file\" name=\"resumefile\" id=\"resumefile\"><br>\n";
            $rtn .= "<input type=\"hidden\" name=\"OrgID\" value=\"" . $OrgID . "\">\n";
            $rtn .= "<input type=\"hidden\" name=\"RequestID\" value=\"" . $RequestID . "\">\n";
            $rtn .= "<input type=\"hidden\" name=\"HoldID\" value=\"" . $HoldID . "\">\n";
            
            $rtn .= "</div>";
            
            $rtn .= "<div class=\"col-lg-9 col-md-9 col-sm-9\">";
            $rtn .= "<input type=\"submit\" value=\"Upload Resume\">\n";
            $rtn .= "<span id=\"resume_parsing_process_msg\" style=\"color:blue;\"></span>\n";
            $rtn .= "</div>";
            $rtn .= "</div>";
            $rtn .= "</form><br>";
            
        } else { //else parsed
        
            $rtn = $this->populateFields($OrgID, $HoldID);
        
        } // end parsed
        
        return $rtn;

	} // end function
	
	/**
	 * @method     parseResume
	 * @param      string $File
	 * @return     array
	 */
	public function parseResume($file) {
		
		// Call the Parse method
		$result   =   G::Obj('SovrenParseResume')->parseResume($file);
		$result   =   simplexml_load_string($result);
		
		$xml      =   "";
		$rtf      =   "";
		$html     =   "";
		$credits  =   "";
		$fileext  =   "";

		$code     =   $result->Info->Code; 
		
        if ($code == "Success") {
        
          $xml      =   $result->Value->ParsedDocument;   //Based on request this request will come
          $text     =   $result->Value->Text;
          $rtf      =   $result->Value->Rtf;
          $html     =   $result->Value->Html;
          $credits  =   $result->Value->CreditsRemaining; 
          $fileext  =   $result->Value->FileExtension;
        
        } // end if success
		   
		return array($xml, $text, $rtf, $html, $credits, $fileext);
	} // end function
	
	/**
	 * @method     processUserPortalResume
	 * @param      string $OrgID
	 * @param      string $UserID
	 * @param      string $RequestID
	 * @param      string $FILE
	 */
	public function processUserPortalResume($OrgID, $UserID, $RequestID, $ApplicationID = '', $FILE) {
	    
        $path_parts     =   pathinfo($FILE);
        $file_name      =   $path_parts['filename'];
	    
	    // parse the resume at Sovren and then put results in DB
	    if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com"
            || $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com"
            ) {
	        list ($xml, $text, $rtf, $html, $credits, $fileext) = $this->parseResume($FILE);
	    }
	    else {
	        list ($xml, $text, $rtf, $html, $credits, $fileext) = $this->parseResume($FILE);
	    }
	
	    if($xml) {

            $query      =   "INSERT INTO UserPortalResumeParsing";
            $query      .=  " (`OrgID`, `UserID`, `RequestID`, `XML`, `HTML`, `RTF`, `TEXT`, `FILEEXT`, `CreatedUpdatedDate`)";
            $query      .=  " VALUES (:OrgID, :UserID, :RequestID, :XML, :HTML, :RTF, :TEXT, :FILEEXT, NOW())";
            $query      .=  " ON DUPLICATE KEY UPDATE";
            $query      .=  " XML = :UXML, HTML = :UHTML, RTF = :URTF, `TEXT` = :UTEXT, FILEEXT = :UFILEEXT, CreatedUpdatedDate = NOW()";
            $params     =   array(
                                ":OrgID"                =>  $OrgID,
                                ":UserID"               =>  $UserID,
                                ":RequestID"            =>  $RequestID,
                                ":XML"                  =>  $xml,
                                ":HTML"                 =>  $html,
                                ":RTF"                  =>  $rtf,
                                ":TEXT"                 =>  $text,
                                ":FILEEXT"              =>  $fileext,
                                ":UXML"                 =>  $xml,
                                ":UHTML"                =>  $html,
                                ":URTF"                 =>  $rtf,
                                ":UTEXT"                =>  $text,
                                ":UFILEEXT"             =>  $fileext
                            );
            $res        =   $this->db->getConnection( $this->conn_string )->insert($query, array($params));
             
            if($ApplicationID != "") {
                $query  =   "INSERT INTO Parsing";
                $query .=  " (`OrgID`, `ApplicationID`, `XML`, `HTML`, `RTF`, `TEXT`, `FILEEXT`, `CreatedUpdatedDate`)";
                $query .=  " VALUES (:OrgID, :ApplicationID, :XML, :HTML, :RTF, :TEXT, :FILEEXT, NOW())";
                $query .=  " ON DUPLICATE KEY UPDATE";
                $query .=  " XML = :UXML, HTML = :UHTML, RTF = :URTF, `TEXT` = :UTEXT, FILEEXT = :UFILEEXT, CreatedUpdatedDate = NOW()";
                $params     =  array(
                                    ":OrgID"			=>  $OrgID,
                                    ":ApplicationID"    =>  $ApplicationID,
                                    ":XML"              =>  $xml,
                                    ":HTML"             =>  $html,
                                    ":RTF"              =>  $rtf,
                                    ":TEXT"             =>  $text,
                                    ":FILEEXT"          =>  $fileext,
                                    ":UXML"             =>  $xml,
                                    ":UHTML"            =>  $html,
                                    ":URTF"             =>  $rtf,
                                    ":UTEXT"            =>  $text,
                                    ":UFILEEXT"         =>  $fileext,
                                );
                $this->db->getConnection( $this->conn_string )->insert($query, array($params));
            }
	             
	        $file_to_log    =   ROOT."logs/resume-parsing/". $file_name . "resume.xml";
	        $path_parts     =   pathinfo($file_to_log);
	
	        if(!is_dir($path_parts['dirname'])) {
	            //Directory does not exist, so lets create it.
	            mkdir($path_parts['dirname'], 0777);
	            chmod($path_parts['dirname'], 0777);
	        }
	
	        $fp = fopen($file_to_log, 'w');
	        fwrite($fp, $xml);
	        fclose($fp);
	        chmod($file_to_log, 0666);
	
	        if (($credits > 975) && ($credits < 1000)) {
	
	            $message    =   "Credit count is: " . $credits;
	            $to         =   "dedgecomb@irecruit-software.com";
	            $subject    =   "Sovren Credits are LOW";
	
	            G::Obj('PHPMailer')->clearCustomProperties();
	            
	            // Set who the message is to be sent to
	            G::Obj('PHPMailer')->addAddress ( $to );
	            // Set the subject line
	            G::Obj('PHPMailer')->Subject = $subject;
	            // convert HTML into a basic plain-text alternative body
	            G::Obj('PHPMailer')->msgHTML ( $message );
	            // Content Type Is HTML
	            G::Obj('PHPMailer')->ContentType = 'text/plain';
	            //Send email
	            G::Obj('PHPMailer')->send ();
	            
	        }
	
	    }
	
	} // end function

	/**
	 * @method     copyPublicPortalParsingTempToParsing
	 * @param      string $OrgID
	 * @param      string $ApplicationID
	 * @return     array
	 */
	public function copyPublicPortalParsingTempToParsing($OrgID, $HoldID, $ApplicationID, $RequestID, $FormID) {
	    
	    //Get Purpose names list
        $purpose        =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);
		
        $query          =   "SELECT FILEEXT FROM ParsingTemp WHERE OrgID = :OrgID and HoldID = :HoldID";
        $params         =   array(':OrgID'=>$OrgID, ':HoldID'=>$HoldID);
        $DATA           =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
        $fileext        =   $DATA['FILEEXT'];
		
		$query   		=  	"INSERT INTO Parsing";
        $query  		.=  " (OrgID, ApplicationID, XML, HTML, `TEXT`, RTF, FILEEXT, CreatedUpdatedDate)";
        $query  		.=  " SELECT OrgID, '$ApplicationID', XML, HTML, `TEXT`, RTF, FILEEXT, NOW() FROM ParsingTemp";
        $query  		.=  " WHERE OrgID = :OrgID AND HoldID = :HoldID AND RequestID = :RequestID ORDER BY CreatedUpdatedDate DESC LIMIT 1";
        $params 		=	array(":OrgID"=>$OrgID, ":HoldID"=>$HoldID, ":RequestID"=>$RequestID);
        $this->db->getConnection( $this->conn_string )->insert($query, array($params));

        $apatdir		=	IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
        $old_filename	=	$apatdir . '/' . $HoldID . '-' . $purpose['resumeupload'] . '.' . $fileext;
        $new_filename	=	$apatdir . '/' . $ApplicationID . '-' . $purpose['resumeupload'] . '.' . $fileext;
        
        $path_parts     =   pathinfo($new_filename);
        
        @copy($old_filename, $new_filename);
        @rename($old_filename, $new_filename);
		//@unlink($old_filename);		
		
		return $path_parts;
	}
	
	/**
	 * @method     parseXML
	 * @param      string $xmlstr
	 * @return     array
	 */
	public function parseXML($xmlstr) {
	    
        $ALPHA      =   G::Obj('GenericLibrary')->getAlphabets();
        $RESUMEDATA =   array();

        $xml        =   simplexml_load_string($xmlstr) or die("Error: Cannot create object");
		
		if(is_array($xml->StructuredXMLResume->ContactInfo->PersonName) || is_object($xml->StructuredXMLResume->ContactInfo->PersonName)) {
			foreach ($xml->StructuredXMLResume->ContactInfo->PersonName as $CI) {
				//$RESUMEDATA['FormattedName'] = (string) $CI->FormattedName;
				$RESUMEDATA['first']    = (string) $CI->GivenName;
				$RESUMEDATA['middle']   = (string) $CI->MiddleName;
				$RESUMEDATA['last']     = (string) $CI->FamilyName;
				//$RESUMEDATA['Affix'] = (string) $CI->Affix;
			}
		}
		
		if(is_array($xml->StructuredXMLResume->ContactInfo->ContactMethod) || is_object($xml->StructuredXMLResume->ContactInfo->ContactMethod)) {
			foreach ($xml->StructuredXMLResume->ContactInfo->ContactMethod as $CM) {
			
				if(is_array($CM->PostalAddress->DeliveryAddress) || is_object($CM->PostalAddress->DeliveryAddress)) {
					foreach ($CM->PostalAddress->DeliveryAddress as $DA) {
						$x=0;
						if ($DA->AddressLine != "") {
							$RESUMEDATA['address'] = (string) $DA->AddressLine;
							if ($x == 1) {
								$RESUMEDATA['address2'] = (string) $DA->AddressLine;
							}
							$x++;
						}
					}
				}
					
				if ($CM->PostalAddress->Municipality != "") {
					$RESUMEDATA['city'] = (string) $CM->PostalAddress->Municipality;
				}
				if ($CM->PostalAddress->Region != "") {
					$RESUMEDATA['state'] = (string) $CM->PostalAddress->Region;
				}
				if ($CM->PostalAddress->PostalCode != "") {
					$RESUMEDATA['zip'] = (string) $CM->PostalAddress->PostalCode;
				}
				if ($CM->PostalAddress->CountryCode != "") {
					//$RESUMEDATA['Country'] = (string) $CM->PostalAddress->CountryCode;
				}
				if ($CM->Telephone->FormattedNumber != "") {
					$homephone = preg_replace('/\D/', '', (string) $CM->Telephone->FormattedNumber);
					$homephone = substr($homephone,-10);
					$RESUMEDATA['homephone1'] = substr($homephone,0,3);
					$RESUMEDATA['homephone2'] = substr($homephone,3,3);
					$RESUMEDATA['homephone3'] = substr($homephone,-4);
				}
				if ($CM->Mobile->FormattedNumber != "") {
					$cellphone = preg_replace('/\D/', '', (string) $CM->Mobile->FormattedNumber);
					$cellphone = substr($cellphone,-10);
					$RESUMEDATA['cellphone1'] = substr($cellphone,0,3);
					$RESUMEDATA['cellphone2'] = substr($cellphone,3,3);
					$RESUMEDATA['cellphone3'] = substr($cellphone,-4);
				}
				if ($CM->Fax->FormattedNumber != "") {
					//$RESUMEDATA['Fax'] = (string) $CM->Fax->FormattedNumber;
				}
				if ($CM->Pager->FormattedNumber != "") {
					//$RESUMEDATA['Pager'] = (string) $CM->Pager->FormattedNumber;
				}
				if ($CM->TTYTDD->FormattedNumber != "") {
					//$RESUMEDATA['TTYTDD'] = (string) $CM->TTYTDD->FormattedNumber;
				}
				if ($CM->InternetEmailAddress != "") {
					$RESUMEDATA['email'] = (string) $CM->InternetEmailAddress;
				}
				if ($CM->InternetWebAddress != "") {
					//$RESUMEDATA['WebSite'] = (string) $CM->InternetWebAddress;
				}
			
			}
		}
		
	
		/*	
		$ACHIEVEMENTS=array();
		$i=0;
		foreach ($xml->StructuredXMLResume->Achievements->Achievement as $A) {
			$ACHIEVEMENTS[$i]['Description'] = (string) $A->Description;
			$ACHIEVEMENTS[$i]['Title'] = (string) $A->Title;
			$i++;
		}
		if ($i > 0) { $RESUMEDATA['Achievements']=$ACHIEVEMENTS;}
		*/
		
		$EDUCATION    =   array();
		$edu_address  =   array();
		$i            =   0;
		
		
		if(is_array($xml->StructuredXMLResume->EducationHistory->SchoolOrInstitution) || is_object($xml->StructuredXMLResume->EducationHistory->SchoolOrInstitution)) {
			foreach ($xml->StructuredXMLResume->EducationHistory->SchoolOrInstitution as $S) {
				$RESUMEDATA[$ALPHA[$i].'attended'] = $EDUCATION[$i]['SchoolName'] = (string) $S->School->SchoolName;
				
				if(@(string)$S->PostalAddress->Municipality != "") $edu_address[] = $EDUCATION[$i]['Town'] = (string) $S->PostalAddress->Municipality;
				if(@(string)$S->PostalAddress->Region != "") $edu_address[] = $EDUCATION[$i]['State'] = (string) $S->PostalAddress->Region;
				if(@(string)$S->PostalAddress->CountryCode != "") $edu_address[] = $EDUCATION[$i]['Country'] = (string) $S->PostalAddress->CountryCode;
				$EDUCATION[$i]['DegreeType'] = (string) $S->Degree->attributes()->degreeType;
				$EDUCATION[$i]['SchoolType'] = (string) $S->attributes()->schoolType;
				$EDUCATION[$i]['Degree'] = (string) $S->Degree->DegreeName;
				$EDUCATION[$i]['Major'] = (string) $S->Degree->DegreeMajor->Name;
				$EDUCATION[$i]['Minor'] = (string) $S->Degree->DegreeMinor->Name;
				
				$RESUMEDATA[$ALPHA[$i].'eduaddress'] = implode(",", $edu_address);
				$i++;
			}
		}
		if ($i > 0) { 
			$RESUMEDATA['Education']=$EDUCATION;
			 
		}
		
		
		$LICENSESCERTIFICATIONS   =   array();
		$i    =   0;
		if(is_array($xml->StructuredXMLResume->LicensesAndCertifications->LicenseOrCertification) || is_object($xml->StructuredXMLResume->LicensesAndCertifications->LicenseOrCertification)) {
			foreach ($xml->StructuredXMLResume->LicensesAndCertifications->LicenseOrCertification as $LC) {
				$LICENSESCERTIFICATIONS[$i]['Name'] = @(string) $LC->Name;
				$LICENSESCERTIFICATIONS[$i]['Description'] = @(string) $LC->Description;
				
				if (@$LC[$i]->EffectiveDate->FirstIssueDate->AnyDate != "") {
					if ((@$LC[$i]->EffectiveDate->FirstIssueDate->AnyDate != "notKnown") && (@$LC[$i]->EffectiveDate->FirstIssueDate->AnyDate != "notApplicable")) {
						$LICENSESCERTIFICATIONS[$i]['FirstIssued'] = @(string) $LC[$i]->EffectiveDate->FirstIssueDate->AnyDate;
					}
				}
				if (@$LC[$i]->EffectiveDate->ValidFrom->AnyDate != "") {
					//if (($LC[$i]->EffectiveDate->ValidFrom->AnyDate != "notKnown") && ($LC[$i]->EffectiveDate->ValidFrom->AnyDate != "notApplicable")) {
					$LICENSESCERTIFICATIONS[$i]['ValidFrom'] = @(string) $LC[$i]->EffectiveDate->ValidFrom->AnyDate;
					//}
				}
				if (@$LC[$i]->EffectiveDate->ValidTo->AnyDate != "") {
					//if (($LC[$i]->EffectiveDate->ValidTo->AnyDate != "notKnown") && ($LC[$i]->EffectiveDate->ValidTo->AnyDate != "notApplicable")) {
					$LICENSESCERTIFICATIONS[$i]['ValidTo'] = @(string) $LC[$i]->EffectiveDate->ValidTo->AnyDate;
					//}
				}
				$i++;
			}
		}
		if ($i > 0) { $RESUMEDATA['LicensesCertifications']   =   $LICENSESCERTIFICATIONS; }
		
		$WORKHISTORY  =   array();
		$i    =   0;
		$ji   =   0;
		if(is_array($xml->StructuredXMLResume->EmploymentHistory->EmployerOrg) || is_object($xml->StructuredXMLResume->EmploymentHistory->EmployerOrg)) {
			foreach ($xml->StructuredXMLResume->EmploymentHistory->EmployerOrg as $E) {
				
				if($ji == 0) {
					
					$RESUMEDATA['cur_companyname'] = @(string) $E[0]->EmployerOrgName;
					$RESUMEDATA['cur_jobtitle'] = @(string) $E[0]->PositionHistory->Title;
					
					if (@$E[0]->PositionHistory->StartDate->AnyDate != "") {
						if ((@$E[0]->PositionHistory->StartDate->AnyDate != "notKnown") && (@$E[0]->PositionHistory->StartDate->AnyDate != "notApplicable")) {
							$fromdate = date_create(@(string) $E[0]->PositionHistory->StartDate->AnyDate);
							$RESUMEDATA['cur_employmentdate'] = date_format($fromdate,'m/d/Y');
						}
					}
					
					$RESUMEDATA['cur_describeposition'] = @(string) $E[0]->PositionHistory->Description;
				}
				else {
					$RESUMEDATA[$ALPHA[$i] . 'mrpcompanyname'] = @(string) $E[$ji]->EmployerOrgName;
					$RESUMEDATA[$ALPHA[$i] . 'mrpposition'] = @(string) $E[$ji]->PositionHistory->Title;
						
					if (@$E[$ji]->PositionHistory->StartDate->AnyDate != "") {
						if ((@$E[$ji]->PositionHistory->StartDate->AnyDate != "notKnown") && (@$E[$ji]->PositionHistory->StartDate->AnyDate != "notApplicable")) {
							$fromdate = date_create(@(string) $E[$ji]->PositionHistory->StartDate->AnyDate);
							$RESUMEDATA[$ALPHA[$i] . 'mrpdatesofemploymentfrom'] = date_format($fromdate,'m/d/Y');
						}
					}
						
					if (@$E[$ji]->PositionHistory->EndDate->AnyDate != "") {
						if ((@$E[$ji]->PositionHistory->EndDate->AnyDate != "notKnown") && (@$E[$ji]->PositionHistory->EndDate->AnyDate != "notApplicable")) {
							$todate = date_create(@(string) $E[$ji]->PositionHistory->EndDate->AnyDate);
							$RESUMEDATA[$ALPHA[$i] . 'mrpdatesofemploymentto'] = date_format($todate,'m/d/Y');
						}
					}
						
					$RESUMEDATA[$ALPHA[$i] . 'mrpdescribeposition'] = @(string) $E[$ji]->PositionHistory->Description;
					$i++;
				}
				$ji++;
			}
		}
		
		$COMP     =   array();
		$i        =   0;
		if(is_array($xml->StructuredXMLResume->Qualifications->Competency) || is_object($xml->StructuredXMLResume->Qualifications->Competency)) {
			foreach ($xml->StructuredXMLResume->Qualifications->Competency as $C) {
				$key = @$C[$i]->attributes()->name;
				$COMP[(string)$key] = @$C[$i]->CompetencyEvidence->attributes()->typeDescription;
				$i++;
			}
		}
		
		$SKILLS   =   array();
		$i        =   0;
		if(is_array($COMP)) {
			foreach ($COMP as $k => $v) {
				$SKILLS[$i]['Competency']   =   $k;
				$i++;
			}
		}
		
		if ($i > 0) {
		    $RESUMEDATA['Skills'] =   $SKILLS;
		}
		
		return $RESUMEDATA;
	
	} // end function

	/**
	 * @method     getHtmlResume
	 * @param      string $OrgID
	 * @param      string $ApplicationID
	 * @return     string
	 */
	public function getHtmlResume($OrgID, $ApplicationID) {
        $query    =   "SELECT HTML FROM Parsing WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
		$params   =   array(':OrgID'=>$OrgID, ':ApplicationID'=>$ApplicationID);
		$DATA     =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
		
		return $DATA['HTML'];
	}

        /**
         * @method     getCredits
         * @return     string
         */
        public function getCredits() {

        $url    =   "https://rest.resumeparsing.com/v9/account";
        $ch     =   curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        $result =   curl_exec($ch);
        curl_close($ch);

        if($result === false)
        {
            $result = 'Curl error: ' . curl_error($ch);
        }

        return $result;

        }
} // end Class
