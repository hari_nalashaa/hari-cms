<?php
/**
 * @class		IntuitPaymentInformation
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class IntuitPaymentInformation {
    
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		insIntuitPaymentInformation
     * @return		array
     */
    function insIntuitPaymentInformation($intuit_info) {
        
        $ins_payment_info = $this->db->buildInsertStatement('IntuitPaymentInformation', $intuit_info);
        $res_payment_info = $this->db->getConnection ( $this->conn_string )->insert ( $ins_payment_info["stmt"], $ins_payment_info["info"] );
        
        return $res_payment_info;
    } // end function

    /**
     * @method		getIntuitPaymentInformation
     * @return		array
     */
    function getLastIntuitPaymentInformationByCategory($OrgID, $PaymentCategory) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":PaymentCategory"=>$PaymentCategory);
        $sel_payment_info   =   "SELECT * FROM IntuitPaymentInformation WHERE OrgID = :OrgID AND PaymentCategory = :PaymentCategory ORDER BY CreatedDateTime DESC";
        $res_payment_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc( $sel_payment_info, array($params_info) );
        
        return $res_payment_info;
    } // end function
    
}
?>
