<?php 
/**
 * @class		PreFilledFormQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class PreFilledFormQuestions {
	
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getQuestionDetails
     * @param		$OrgID, $PreFilledFormID, $QuestionID
     * @return		array
     */
    function getQuestionDetails($columns = "*", $OrgID, $PreFilledFormID, $QuestionID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        //Set parameters for prepared query
        $params_answer_info  = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QuestionID);
        $sel_que_answer_info = "SELECT $columns FROM PreFilledFormQuestions WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID AND QuestionID = :QuestionID";
        $row_que_answer_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_que_answer_info, array($params_answer_info));
    
        return $row_que_answer_info;
    }    
    
    /**
     * @method  getPreFilledFormQuestionsList
     * @param   string $OrgID
     * @param   string $WebFormID
     * @param   string $PreFilledFormID
     */
    function getPreFilledFormQuestionsList($OrgID, $PreFilledFormID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params_info        =   array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID);
        $sel_pre_form_ques  =   "SELECT $columns FROM PreFilledFormQuestions WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID ORDER BY QuestionOrder ASC";
        $res_pre_form_ques  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_pre_form_ques, array($params_info) );
        $res_pre_form_cnt   =   $res_pre_form_ques['count'];
        $res_pre_form_res   =   $res_pre_form_ques['results'];

        $pre_form_ques_list =   array();
        for($w = 0; $w < $res_pre_form_cnt; $w++) {
            $pre_form_ques_list[$res_pre_form_res[$w]['QuestionID']]   =   $res_pre_form_res[$w];
        }

        return $pre_form_ques_list;
    }
    
}
?>
