<?php
/**
 * @class		TwilioVerifiedPhoneNumbers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioVerifiedPhoneNumbers {
	
    public $db;
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		insTwilioVerifiedPhoneNumbers
     * @param		$info
     */
    public function insTwilioVerifiedPhoneNumbers($info) {
    
    	$ins_verified_phone_number = $this->db->buildInsertStatement ( 'TwilioVerifiedPhoneNumbers', $info );
    	$res_verified_phone_number = $this->db->getConnection ( $this->conn_string )->insert ( $ins_verified_phone_number["stmt"], $ins_verified_phone_number["info"] );
    
    	return $res_verified_phone_number;
    }
    
    /**
     * @method		getTwilioVerifiedPhoneNumberInfo
     * @param		$info
     */
    public function getTwilioVerifiedPhoneNumberInfo($phone_number) {
    
    	$params		=	array(":PhoneNumber"=>$phone_number);
    	$sel_info	=	"SELECT * FROM TwilioVerifiedPhoneNumbers WHERE PhoneNumber = :PhoneNumber";
    	$res_info 	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    	
    	return $res_info;
    }
}
?>
