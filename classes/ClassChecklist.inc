<?php
/**
 * @class		CheckList
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Checklist {

    /**
     * @tutorial	Set the default database connection as irecruit
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }

    /**
     * @method		getCheckList
     * @param		$ChecklistID, $columns(optional)
     * @return		associative array
     */
    function getCheckList($ChecklistID, $OrgID, $Active = 'Y') {
    
        //Set parameters for prepared query
        $params             =   array(":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID,":Active"=>$Active);    
        $sel_checklist_info =   "SELECT * FROM ChecklistSetup WHERE ChecklistID = :ChecklistID AND OrgID = :OrgID AND Active=:Active";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );

        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListByOrgID
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function getCheckListByOrgID($OrgID, $Active = 'Y') {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID,":Active"=>$Active);
        $sel_checklist_info =   "SELECT * FROM ChecklistSetup WHERE OrgID = :OrgID AND Active=:Active";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListProcess
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function getCheckListProcess($OrgID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID);
        $sel_checklist_info =   "SELECT * FROM ChecklistConfiguration WHERE OrgID = :OrgID";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
	
    /**
     * @method		getCheckListCategoryByOrgID
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function getCheckListCategoryByOrgID($OrgID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID);
        $sel_checklist_info =   "SELECT * FROM ChecklistSetup WHERE OrgID = :OrgID AND Active='Y'";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		insChecklist
     * @param		$ChecklistID, $StatusCategory, $Checklist, $Version
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insChecklist($OrgID, $StatusCategory, $Checklist, $ChecklistName) {
    
        $params_info        =   array(":OrgID"=>$OrgID, ":StatusCategory"=>$StatusCategory, ":Checklist"=>$Checklist, ":ChecklistName"=>$ChecklistName);
        $ins_statement      =   "INSERT INTO ChecklistSetup(OrgID, StatusCategory, Checklist, ChecklistName, LastUpdated) VALUES(:OrgID, :StatusCategory, :Checklist, :ChecklistName, NOW())";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE LastUpdated = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
    
        return $res_statement;
    }
    
    function insChecklistName($OrgID, $ChecklistName) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":ChecklistName"=>$ChecklistName);
        $ins_statement      =   "INSERT INTO ChecklistSetup(OrgID, ChecklistName, LastUpdated) VALUES(:OrgID, :ChecklistName, NOW())";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE LastUpdated = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    function insChecklistProcessOrder($OrgID, $ProcessOrder) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":ProcessOrder"=>$ProcessOrder);
        $ins_statement      =   "INSERT INTO ChecklistConfiguration(OrgID, ProcessOrder) VALUES(:OrgID, :ProcessOrder)";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    function insChecklistDisplayOnUserPortal($OrgID, $DisplayUserPortal) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":DisplayOnUserPortal"=>$DisplayUserPortal);
        $ins_statement      =   "INSERT INTO ChecklistConfiguration(OrgID, DisplayOnUserPortal) VALUES(:OrgID, :DisplayOnUserPortal)";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    /**
     * @method      updAgreementDataInfo
     * @param       $set_info, $where_info, $info
     */
    function updChecklistProcessOrder($set_info = array(), $where_info = array(), $info) {
        
        $upd_checklist_data_info = $this->db->buildUpdateStatement("ChecklistConfiguration", $set_info, $where_info);
        $res_checklist_data_info = $this->db->getConnection( $this->conn_string )->update($upd_checklist_data_info, $info);
        
        return $res_checklist_data_info;
    }
    
    /**
     * @method      updAgreementDataInfo
     * @param       $set_info, $where_info, $info
     */
    function updChecklist($set_info = array(), $where_info = array(), $info) {
        
        $upd_checklist_data_info = $this->db->buildUpdateStatement("ChecklistSetup", $set_info, $where_info);
        $res_checklist_data_info = $this->db->getConnection( $this->conn_string )->update($upd_checklist_data_info, $info);
        
        return $res_checklist_data_info;
    }
    
    
    /**
	 * @method		copyChecklist
	 */
    public function copyChecklist($OrgID, $ChecklistID, $ChecklistName) {
	    
	    $params = array(":OrgID"=>$OrgID, ":ChecklistID"=>$ChecklistID);
	    $now = date('Y-m-d H:i:s');
	    
        //Insert Application Form Sections Master Sections OrgID
        $ins_checklist_ques 	=	"INSERT INTO ChecklistSetup (OrgID, StatusCategory, Checklist, ChecklistName, Locked, Active, LastUpdated)
						  	SELECT  OrgID, StatusCategory, Checklist, '".$ChecklistName."', 'N',  'P', '".$now."' 
						  	FROM ChecklistSetup WHERE OrgID = :OrgID AND ChecklistID = :ChecklistID";
        $res_form_ques	=	$this->db->getConnection ( "IRECRUIT" )->insert($ins_checklist_ques, array($params));	    
	}
}
?>
