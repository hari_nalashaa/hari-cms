<?php 
/**
 * @class		RequisitionsReports
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionsReports {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      getRequisitionsApprovalFieldsInfo
     * @param       $OrgID, $RequisitionFormID
     */
    function getRequisitionsApprovalFieldsInfo($OrgID, $from_date, $to_date) {

        // set where conditon
        $where      =   array ("OrgID = :OrgID", "Active != 'R'", "DATE(DateEntered) >= :FromDate", "DATE(DateEntered) <= :ToDate");
        // set parameters
        $params     =   array (":OrgID"=>$OrgID, ":FromDate"=>G::Obj('DateHelper')->getYmdFromMdy($from_date), ":ToDate"=>G::Obj('DateHelper')->getYmdFromMdy($to_date));

        $columns    =   "*, date_format(DateEntered,'%m/%d/%Y') DateEntered, date_format(LastModified,'%m/%d/%Y') LastModified,";
        $columns    .=  "IF((NOW() < ExpireDate), DATEDIFF(NOW(), PostDate), DATEDIFF(ExpireDate, PostDate)) DaysOpen";

        $resultsR   =   G::Obj('Requisitions')->getRequisitionInformation ( $columns, $where, "", "DateEntered DESC", array ($params) );
        
        $r          =   0;
        $max_approvers_list =   array();
        $req_approval_list  =   array();
        
        if (is_array ( $resultsR ['results'] )) {
            foreach ( $resultsR ['results'] as $RR ) {
                
                $multiorgid_req =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RR ['RequestID']);

                $req_approval_list[$r]['OrganizationTitle']     =   G::Obj('OrganizationDetails')->getOrganizationNameByRequestID ( $OrgID, $RR ['RequestID'] );
                $req_approval_list[$r]['DateEntered']           =   $RR ['DateEntered'];
                $req_approval_list[$r]['RequesterName']         =   $RR ['RequesterName'];
                $req_approval_list[$r]['Title']                 =   $RR ['Title'];
                $req_approval_list[$r]['RequisitionID']         =   $RR ['RequisitionID'];
                $req_approval_list[$r]['JobID']                 =   $RR ['JobID'];
                $req_approval_list[$r]['RequesterReason']       =   $RR ['RequesterReason'];
                $req_approval_list[$r]['PostDate']              =   $RR ['PostDate'];
                $req_approval_list[$r]['ExpireDate']            =   $RR ['ExpireDate'];
                $req_approval_list[$r]['ApprovalStatus']        =   ($RR ['Approved'] == "Y") ? "Active" : "Pending";
                $req_approval_list[$r]['ApprovalLevelRequired'] =   $RR ['ApprovalLevelRequired'];
                $req_approval_list[$r]['DaysOpen']              =   $RR ['DaysOpen'];
                $req_approval_list[$r]['ApplicantsCount']       =   $RR ['ApplicantsCount'];
                $req_approval_list[$r]['LastModified']          =   $RR ['LastModified'];
                
                $max_approvers  =   0;
                if (($RR ['RequisitionID'] != '') || ($RR ['JobID'] != '')) {
                    //set where condition
                    $where      =   array("OrgID = :OrgID", "RequestID = :RequestID");
                    //set parameters
                    $params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RR ['RequestID']);
                    //get Request approvers information
                    $results    =   G::Obj('Request')->getRequestApproversInfo("AuthorityLevel, EmailAddress, IF (DateApproval != '0000-00-00 00:00:00', DateApproval, 'Awaiting Approval') AS DateApproval", $where, "AuthorityLevel", array($params));

                    $req_approval_list[$r]['RequestApprovers']  =   $results['results'];
                    $max_approvers          =   count($results['results']);
                    $max_approvers_list[]   =   $max_approvers;
                }
                
                $req_approval_list[$r]['MaxApprovers'] = ($req_approval_list[$r]['MaxApprovers'] > $max_approvers) ? $req_approval_list[$r]['MaxApprovers'] : $max_approvers;
                
                $r++;
            }
        }
        
        $map_config_columns_list   =   array(
                                            "DateEntered"           =>  "Date Requested",
                                            "RequesterName"         =>  "Requested by",
                                            "Title"                 =>  "Job Title",
                                            "RequisitionID"         =>  "Requisition ID",
                                            "JobID"                 =>  "Job ID",
                                            "RequesterReason"       =>  "Reason for Request",
                                            "PostDate"              =>  "External Post Date",
                                            "ExpireDate"            =>  "External Expire Date",
                                            "ApprovalLevelRequired" =>  "Approver(s) Assigned",
                                            "MaxApprovers"          =>  max($max_approvers_list),
                                            "Status"                =>  "Approval Status",
                                            "DaysOpen"              =>  "Days Open",
                                            "ApplicantsCount"       =>  "Applicants Count",
                                            );
        
        return array("req_approval_list"=>$req_approval_list, "map_columns"=>$map_config_columns_list);
    }
}
?>
