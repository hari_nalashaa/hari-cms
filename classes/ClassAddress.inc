<?php
/**
 * @class		Address
 * @todo		Use  $this->conn_string , "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection( $this->conn_string )->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection( $this->conn_string )->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Address {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getCountries
	 * @return		Array
	 */
	public function getCountries() {
		
		$sel_countries = "SELECT Description, Abbr FROM AddressCountry ORDER BY Description";
		$res_countries = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_countries);
		
		return $res_countries;
	}
	
	/**
	 * @method		getCountryByCode
	 * @return		Array
	 */
	public function getCountryByCode($country_code) {
		
		//Set parameters for prepared query
        $params         =   array(":Abbr"=>$country_code);
        $sel_countries  =   "SELECT Description, Abbr FROM AddressCountry WHERE Abbr = :Abbr";
        $res_countries  =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_countries, array($params));
		
		return $res_countries;
	}
	
	/**
	 * @method		getInternationalizationInfo
	 * @return		array
	 */
	public function getInternationalizationInfo($OrgID, $Allow = '') {
		
		//Set parameters for prepared query
		$params   =   array(":OrgID"=>$OrgID);
		$sel_internationalization  = "SELECT Allow, DefaultCountry FROM Internationalization WHERE OrgID = :OrgID";
		if($Allow != "")  $sel_internationalization .= " AND Allow = '".$Allow."'";
		$res_internationalization  = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_internationalization, array($params));
		
		return $res_internationalization;
	}
	
	/**
	 * @method	insInternationalizationInfo
	 * @param	$info
	 */
	function insInternationalizationInfo($info) {
		
		$ins_internationalization_info = $this->db->buildInsertStatement('Internationalization', $info);
		$res_internationalization_info = $this->db->getConnection( $this->conn_string )->insert ( $ins_internationalization_info["stmt"], $ins_internationalization_info["info"] );
	
		return $res_internationalization_info;
	}
	
	/**
	 * @method	delInternationalizationInfo
	 * @return	array
	 */
	function delInternationalizationInfo($where_info = array(), $info = array()) {
		
		$del_internationalization_info = "DELETE FROM Internationalization";
	
		if(count($where_info) > 0) {
			$del_internationalization_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		$res_internationalization_info = $this->db->getConnection( $this->conn_string )->delete ( $del_internationalization_info, $info );
	
		return $res_internationalization_info;
	}
	
	/**
	 * @method		getInternationalizationInfo
	 * @return		array
	 */
	public function getAddressProvinceList() {
		
		$sel_address_province =   "SELECT Description, Abbr FROM AddressProvince ORDER BY Description";
		$res_address_province =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_address_province);
		
		$province_list        =   array();
		foreach ($res_address_province['results'] as $row) {
		    $province_list[$row['Abbr']]  =   $row['Description'];
		}
		
		return $province_list;
	}

	
	/**
	 * @method		getAddressStateList
	 * @return		array
	 */
	public function getAddressStateList() {
		
		$sel_address_state = "SELECT Description, Abbr FROM AddressState ORDER BY Description";
		$res_address_state = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_address_state);
		
		return $res_address_state;
	}
	
	/**
	 * @method		formatPhone
	 * @param 		string $OrgID
	 * @param 		string $Country
	 * @param 		string $Phone1
	 * @param 		string $Phone2
	 * @param 		string $Phone3
	 * @param 		string $ext
	 * @return 		string
	 */
	function formatPhone($OrgID, $Country, $Phone1, $Phone2, $Phone3, $ext) {
	    $Phone1    =   is_null($Phone1) ? "" : $Phone1;
	    $Phone2    =   is_null($Phone2) ? "" : $Phone2;
	    $Phone3    =   is_null($Phone3) ? "" : $Phone3;
	    $ext       =   is_null($ext) ? "" : $ext;
	    
	    $phone = '';
	
		if ($Country == "") {
			$Country = "US";
		}
	
		if (($OrgID) && ($Phone1) && ($Phone2) && ($Phone3)) {
	
			if (($Country == "CA") || ($Country == "US")) {
	
				$phone = "($Phone1) $Phone2-$Phone3";
				if ($ext) {
					$phone .= " ext. " . $ext;
				}
			} else {
	
				$phone = "+$Phone1 $Phone2 $Phone3";
				if ($ext) {
					$phone .= " ext. " . $ext;
				}
			}
		} // end if items
	
		return $phone;
	} // end function
	
	/**
	 * @method		formatAddress
	 * @param		string $OrgID
	 * @param		string $Country
	 * @param		string $City
	 * @param		string $State
	 * @param		string $Province
	 * @param		string $Zip
	 * @param		string $County
	 * @return		string
	 */
	function formatAddress($OrgID, $Country, $City, $State, $Province, $Zip, $County) {
		$address	=	'';
	
		if ($Country == "") {
			$Country = "US";
		}
	
		if (($OrgID) && ($City)) {
	
			if ($Country == "CA") {
	
				$address = "$City, $Province&nbsp;&nbsp;$Zip";
				
				if ($County != "") {
					$address .= "<br>";
					$address .= $County . " County";
				}
			}
			else if ($Country == "US") {
	
				$address = "$City, $State&nbsp;&nbsp;$Zip";
				
				if ($County != "") {
					$address .= "<br>";
					$address .= $County . " County";
				}
			} else {
	
				$address = "$City&nbsp;&nbsp;$Zip";
			}
		} // end items

		return $address;
		
	} // end function

	/**
	 * @method         getFormattedLeadPhoneNumber
	 * @tutorial       By default we will consider the phone number as US number,
                       So ignore the country code for now. Will be taken care internally based on address.
                       Convert all above phone formats to json string as mentioned below.
                       Example phone numbers list 
                       ("+1 6166485419", 
                       "+234 8082299922", 
                       "+44 248-3959 33125", 
                       "+31 207-784-5441", 
                       "4437540917")
                       ["xxx", "xxx", "xxxx"] or ["xxx", "xxx", "xxxx", "xxxx"]
	 */
	function getFormattedLeadPhoneNumber($phone_number) {
		
        $phone_number       =   preg_replace("/[^0-9 +]/", "", $phone_number);
	    
        $phone_parts        =   explode(" ", $phone_number);
        $phone_parts_cnt    =   count($phone_parts);
	    
	    if(isset($phone_parts[0])) {
	        
	        $is_plus       =   substr($phone_parts[0], 0, 1);
	        $concat_phone  =   "";
	        
	        if($is_plus == "+") {
	            
	            for($i = 1; $i < $phone_parts_cnt; $i++) {
	                $concat_phone .= $phone_parts[$i];
	            }
	            	
	            $formatted_phone = ["", "", ""];
	            if(strlen($concat_phone) <= 10) {
	                 
	                $cphone1 = substr($concat_phone, 0, 3);
	                $cphone2 = substr($concat_phone, 3, 3);
	                $cphone3 = substr($concat_phone, 6, 4);
	                 
	                $formatted_phone = ["$cphone1", "$cphone2", "$cphone3"];
	                 
	                return array("Phone"=>json_encode($formatted_phone), "QuestionTypeID"=>13);	    
	            }
	            else if(strlen($concat_phone) > 10) {
	    
	                $cphone1 = substr($concat_phone, 0, 3);
	                $cphone2 = substr($concat_phone, 3, 3);
	                $cphone3 = substr($concat_phone, 6, 4);
	                $cphone4 = substr($concat_phone, 10, 4);
	                $cphone4 = str_pad($cphone4, 4, "0", STR_PAD_LEFT);
	                 
	                $formatted_phone = ["$cphone1", "$cphone2", "$cphone3", "$cphone4"];
	                 
	                return array("Phone"=>json_encode($formatted_phone), "QuestionTypeID"=>14);
	            }
	    
	        }
	        else {
	    
	            $concat_phone  =   "";
	    
	            for($i = 0; $i < $phone_parts_cnt; $i++) {
	                $concat_phone .= $phone_parts[$i];
	            }
	            
	            if(strlen($concat_phone) <= 10) {
	                $formatted_phone = ["", "", ""];
	                $cphone1 = substr($concat_phone, 0, 3);
	                $cphone2 = substr($concat_phone, 3, 3);
	                $cphone3 = substr($concat_phone, 6, 4);
	    
	                $formatted_phone = ["$cphone1", "$cphone2", "$cphone3"];
	    
	                return array("Phone"=>json_encode($formatted_phone), "QuestionTypeID"=>13);
	            }
	            else if(strlen($concat_phone) > 10) {
	                $formatted_phone = ["", "", ""];
	                $cphone1 = substr($concat_phone, 0, 3);
	                $cphone2 = substr($concat_phone, 3, 3);
	                $cphone3 = substr($concat_phone, 6, 4);
	                $cphone4 = substr($concat_phone, 10, 4);
                    $cphone4 = str_pad($cphone4, 4, "0", STR_PAD_LEFT);	                 
                    
	                $formatted_phone = ["$cphone1", "$cphone2", "$cphone3", "$cphone4"];

	                return array("Phone"=>json_encode($formatted_phone), "QuestionTypeID"=>14);
	            }
	    
	        }
	    }
	    
	}
}
