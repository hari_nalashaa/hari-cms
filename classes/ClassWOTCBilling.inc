<?php
/**
 * @class		WOTCBilling
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCBilling
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method      getBillingInfoByPrimaryKey
     * @param       string $wotcID
     * @param       string $ApplicationID
     * @return      string
     */
    function getBillingInfoByPrimaryKey($columns = "*", $wotcID, $ApplicationID, $Year)
    {
        $query          =   "SELECT $columns FROM Billing WHERE wotcID = :wotcid AND ApplicationID = :ApplicationID AND Year = :Year";
        $params         =   array(':wotcid' => $wotcID, ':ApplicationID' => $ApplicationID, ":Year"=>$Year);
        $billing        =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
    
        return $billing;
    }
    
    
    /**
     * @method      getBillingInfoByWotcIDANDApplicationID
     * @param       string $wotcID
     * @param       string $ApplicationID
     * @return      string
     */
    function getBillingInfoByWotcIDANDApplicationID($wotcID, $ApplicationID)
    {
        $query          =   "SELECT Year, if(Termed= '0000-00-00','',date_format(Termed,'%m/%d/%Y')) Termed,";
        $query          .=  " Reason, TaxCredit, if(DateBilled= '0000-00-00','',date_format(DateBilled,'%m/%d/%Y')) DateBilled,";
        $query          .=  " if(DatePaid= '0000-00-00','N/A',date_format(DatePaid,'%m/%d/%Y')) DatePaid, Comments, InvoiceNumber";
        $query          .=  " FROM Billing WHERE wotcID = :wotcid and ApplicationID = :applicationid";
        $query          .=  " ORDER BY Year";
        $params         =   array(':wotcid' => $wotcID, ':applicationid' => $ApplicationID);
        $BILLING_RES    =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($query, array($params));
        $BILLING        =   $BILLING_RES['results'];
        
        return $BILLING;
    }
    
    /**
     * @method      getMaxYearByWotcIDANDApplicationID
     * @param       string $wotcID
     * @param       string $ApplicationID
     */
    function getMaxYearByWotcIDANDApplicationID($wotcID, $ApplicationID) {
        
        $query          =   "SELECT MAX(Year) as MaxYear FROM Billing WHERE wotcID = :wotcid and ApplicationID = :applicationid";
        $params         =   array(':wotcid' => $wotcID, ':applicationid' => $ApplicationID);
        $max_year_info  =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $max_year_info['MaxYear'];
    }
    
    /**
     * @method      getMaxYearAndTaxCredit
     * @param       string $wotcID
     * @param       string $ApplicationID
     */
    function getMaxYearAndTaxCredit($wotcID, $ApplicationID) {
    
        $query  =   "SELECT MAX(Year) AS MaxYear, SUM(TaxCredit) AS TaxCredit FROM Billing WHERE wotcID = :wotcid and ApplicationID = :applicationid";
        $params =   array(':wotcid' => $wotcID, ':applicationid' => $ApplicationID);
        $info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
    
        return $info;
    }

    /**
     * @method      checkBilling
     * @param       string $wotcID
     * @param       string $ApplicationID
     * @return      integer
     */
    function checkBilling($wotcID, $ApplicationID)
    {
        $query      =   "SELECT COUNT(*) AS Hit FROM Billing WHERE wotcID = :wotcid and ApplicationID = :applicationid";
        $params     =   array(':wotcid' => $wotcID, ':applicationid' => $ApplicationID);
        $hit_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $hit_info['Hit'];
    } // end function
    
    /**
     * @method      insBillingInfo
     * @param       $info
     */
    function insBillingInfo($info) {
    
        $ins_app_data   =   $this->db->buildInsertStatement("Billing", $info);
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data["stmt"], $ins_app_data["info"] );
    
        return $res_app_data;
    }
    
    /**
     * @method      updBillingInfo
     * @param       $set_info, $where_info, $info
     */
    function updBillingInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_info   =   $this->db->buildUpdateStatement("Billing", $set_info, $where_info);
        $res_info   =   $this->db->getConnection( $this->conn_string )->update($upd_info, $info);
    
        return $res_info;
    }

    /**
     * @method          delBillingInfo
     * @param           $where_info = array(), $info = array()
     */
    function delBillingInfo($where_info = array(), $info = array()) {

        if (count ( $where_info ) > 0) {
            $del_rows   =   "DELETE FROM Billing";
            $del_rows   .=  " WHERE " . implode ( " AND ", $where_info );
        }

        $res_rows = $this->db->getConnection ( $this->conn_string )->delete ( $del_rows, $info );

        return $res_rows;
    }
    
    
    
}    
?>
