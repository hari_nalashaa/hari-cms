<?php
/**
 * @class		WOTC
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTC
{
    public $db;
    
	var $conn_string       =   "WOTC";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method     wotcHeader
	 * @param      string $wotcID
	 * @return     string
	 */
    public function wotcHeader($wotcID) {

	$PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
  	$PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($wotcID);
	$PartnerName=$PARTNER['CompanyName'];
	$PartnerLogo=$PARTNER['Logo'];
	$ParentCompanyName=$PARENT['CompanyName'];
	$ParentLogo=$PARENT['Logo'];
	$WotcFormID=$PARENT['WotcFormID'];

        
        if ($ParentLogo != "") {
            $PartnerName = $ParentCompanyName;
            $PartnerLogo = $ParentLogo;
        }
        
        if ($PartnerLogo) {
            $Logo = $PartnerLogo;
        } else {
            $Logo = "cms_logo_wotc.png";
        }
        
        if ($PartnerName) {
            $title = $PartnerName . ' WOTC';
        } else {
            $title = 'CMS WOTC';
        }
        
        $rtn = "";
        $rtn .= "<!DOCTYPE html>\n";
        $rtn .= "<html lang='en'>\n";
        $rtn .= "<head>\n";
        $rtn .= "<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n";
        $rtn .= "<meta http-equiv='X-UA-Compatible' content='IE=edge'>\n";
        $rtn .= "<meta charset=\"UTF-8\">\n";
        $rtn .= "<title>" . $title . "</title>\n";
        $rtn .= "<script type=\"text/javascript\" src=\"" . WOTC_HOME . "js/wotc.js\"></script>\n";
        $rtn .= "<!-- Favicons-->\n";
        $rtn .= "<link rel=\"stylesheet\" href=\"" . WOTC_HOME . "css/bootstrap.min.css\">\n";
        $rtn .= "<link rel=\"shortcut icon\" href=\"" . WOTC_HOME . "images/fav.png?v=3\">\n";
        $rtn .= "<link rel=\"icon\" href=\"" . WOTC_HOME . "images/fav.png?v=3\">\n";
        $rtn .= "<link rel=\"apple-touch-icon\" href=\"" . WOTC_HOME . "images/fav.png?v=3\">\n";
        $rtn .= "<link rel=\"apple-touch-icon-precomposed\" href=\"icon\">\n";

        $rtn .= "<link rel=\"stylesheet\" href=\"".WOTC_HOME."css/wotc.css?v=2.0\">\n";
        $rtn .= "<script type=\"text/javascript\" src=\"".WOTC_HOME."js/jquery.js\"></script>\n";

        $rtn .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . WOTC_HOME . "css/jquery-ui.css\" />\n";
        $rtn .= "<script type=\"text/javascript\" src=\"".WOTC_HOME."js/jquery-ui.min.js\"></script>\n";
        
        $rtn .= "<link href='".WOTC_HOME."css/bootstrap.min.css' rel='stylesheet'>";
        $rtn .= "<link href='".WOTC_HOME."css/sb-admin-2.css' rel='stylesheet'>";
        
        $rtn .= "<style>";
        $rtn .= ".row {";
        $rtn .= "margin-right: 0px !important;";
        $rtn .= "margin-left: 0px !important;";
        $rtn .= "}";
        $rtn .= "</style>";
        $rtn .= "</head>\n";
        $rtn .= "<body>\n";
        
    	$rtn .= "<div class=\"row\">\n";
    	
    	$rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
	$rtn .= "<a href=\"".WOTC_HOME."?wotcID=" . $wotcID . "\">";
    	$rtn .= "<img src=\"".WOTC_HOME."images/logos/".$Logo."\" class=\"img-responsive\" border=\"0\">";
    	$rtn .= "</a>\n";
    	$rtn .= "</div>\n";

    	$rtn .= "</div>\n";    //End Row


    	$rtn .= "<div class=\"row\">\n";
    	$rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
    	$rtn .= "<br>";                

	 if (($_REQUEST['pg'] == "terms") || ($_REQUEST["pg"] == "policy")) {
            $rtn .= "<div id=\"google_translate_element\"></div>\n";
            $rtn .= "<script type=\"text/javascript\">\n";
            $rtn .= "function googleTranslateElementInit() {\n";
            $rtn .= "  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'en,es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');}\n";
            $rtn .= "</script>";
            $rtn .= "<script type=\"text/javascript\" src=\"//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit\"></script>\n";
	 } else if ((substr($_SERVER['PHP_SELF'],-8) != "form.php") && (substr($_SERVER['PHP_SELF'],-8) != "process.php") && (substr($_SERVER['PHP_SELF'],-14) != "callcenter.php") && ($_REQUEST['Initiated'] != 'callcenter')) {

    	     $rtn .= "<div style=\"margin:5px 0;\">";                
	     if ($_COOKIE['LN'] == "sp") {
    	       $rtn .= "<a href=\"".WOTC_HOME."admin.php?wotcID=" . $_REQUEST['wotcID'] . "&ln=en\">Haga clic aqu&#xED; para traducir al ing&#xE9;s</a>";                
	     } else {
    	       $rtn .= "<a href=\"".WOTC_HOME."admin.php?wotcID=" . $_REQUEST['wotcID'] . "&ln=sp\">Haga clic aqu&#xED; para traducir al esp&#xF1;ol</a>";                
	     }
    	     $rtn .= "</div>";                

	  } // end else terms & policy pages

        $rtn .= "<br>";
        $rtn .= "</div>\n";
        $rtn .= "</div>\n"; //End Row
        
        $rtn .= "<div id=\"content\">\n";
        
        return $rtn;
    } // end function
    
    /**
     * @method      wotcFooter
     * @param       string $wotcID
     * @return      string
     */
    public function wotcFooter($wotcID)
    {
        $rtn = "";
        
        $rtn .= "</div>\n";
        $rtn .= "<div id=\"copyright\" class=\"row\">\n";
        $rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
        $rtn .= "&copy; " . date("Y") . " Cost Management Services, LLC<br>";
        $rtn .= "Powered by <a href=\"https://www.irecruit-software.com\" target=\"_blank\">iRecruit Technology</a>.\n";
        
        $bottomnav = array(
            "terms" => "Terms of Use",
            "policy" => "Privacy Policy"
        );
        
        foreach ($bottomnav as $section => $displaytitle) {
            $rtn .= "&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;\n";
            $rtn .= "<a href=\"" . WOTC_HOME . "admin.php?pg=" . $section;
            if ($wotcID) {
                $rtn .= "&wotcID=" . $wotcID;
            }
            $rtn .= "\">" . $displaytitle . "</a>\n";
        } // end foreach
        
        $rtn .= "</div>\n";
        $rtn .= "</div>\n";
        $rtn .= "<script src=\"" . WOTC_HOME . "js/bootstrap.min.js\"></script>\n";
        $rtn .= "</body>\n";
        $rtn .= "</html>\n";
        
        return $rtn;
    } // end function

    /**
     * @method      getWotcAdminLogo
     */
    function getWotcAdminLogo($wotcID) {

	$PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
        $PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($wotcID);
	$BrandingCompanyName=$PARTNER['CompanyName'];
	$BrandingLogo=$PARTNER['Logo'];
	$ParentCompanyName=$PARENT['CompanyName'];
	$ParentLogo=$PARENT['Logo'];
        
        if ($ParentLogo != "") {
            $BrandingCompanyName = $ParentCompanyName;
            $BrandingLogo = $ParentLogo;
        }
        
        if ($BrandingLogo) {
            $Logo = $BrandingLogo;
        } else {
            $Logo = "cms_logo_wotc.png";
        }
        
        return $Logo;
    }
    
    /**
     * @method      wotcadminHeader
     * @param       string $wotcID
     * @return      string
     */
    public function wotcadminHeader($wotcID) {

	$PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
	$PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($wotcID);
	$BrandingCompanyName=$PARTNER['CompanyName'];
	$BrandingLogo=$PARTNER['Logo'];
	$ParentCompanyName=$PARENT['CompanyName'];
	$ParentLogo=$PARENT['Logo'];

        if ($ParentLogo != "") {
            $BrandingCompanyName = $ParentCompanyName;
            $BrandingLogo = $ParentLogo;
        }
        
        if ($BrandingLogo) {
            $Logo = $BrandingLogo;
        } else {
            $Logo = "cms_logo_wotc.png";
        }
        
        if ($BrandingCompanyName) {
            $title = $BrandingCompanyName . ' WOTC Administration';
        } else {
            $title = 'CMS WOTC Administration';
        }
        
        $rtn = "";
        $rtn .= "<!DOCTYPE html>\n";
        $rtn .= "<html lang='en'>\n";
        $rtn .= "<head>\n";
        $rtn .= "<meta name='viewport' content='width=device-width, initial-scale=1.0'>\n";
        $rtn .= "<meta http-equiv='X-UA-Compatible' content='IE=edge'>\n";
        $rtn .= "<meta charset=\"UTF-8\">\n";
        $rtn .= "<title>" . $title . "</title>\n";
        
        if (! strstr($_SERVER["SCRIPT_NAME"], "login") && ! strstr($_SERVER["SCRIPT_NAME"], "logout") && ! strstr($_SERVER["SCRIPT_NAME"], "reset")) {
            
            $rtn .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . WOTCADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
        }

        $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . WOTCADMIN_HOME . "calendar/js/jquery-1.4.2.min.js\"></script>\n";
        $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . WOTCADMIN_HOME . "calendar/js/jquery-ui-1.8.4.custom.min.js\"></script>\n";
        $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . WOTCADMIN_HOME . "javascript/wotcfunctions.js\"></script>\n";
        $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . WOTCADMIN_HOME . "javascript/wotcadmin.js\"></script>\n";
        
        $rtn .= "<!-- Favicons-->\n";
        $rtn .= "<link rel=\"shortcut icon\" href=\"" . WOTCADMIN_HOME . "images/fav.png?v=2\">\n";
        $rtn .= "<link rel=\"icon\" href=\"" . WOTCADMIN_HOME . "images/fav.png?v=2\">\n";
        $rtn .= "<link rel=\"apple-touch-icon\" href=\"" . WOTCADMIN_HOME . "images/fav.png?v=2\">\n";
        $rtn .= "<link rel=\"apple-touch-icon-precomposed\" href=\"icon\">\n";
        
        $rtn .= "<link rel=\"stylesheet\" href=\"" . WOTCADMIN_HOME . "css/wotcadmin.php?v=1.0\">\n";
        $rtn .= "</head>\n";
        $rtn .= "<body>\n";
        
        $rtn .= "<div id=\"topDoc\">\n";
        $rtn .= "<img src=\"" . WOTC_HOME . "images/logos/" . $Logo . "\" style=\"margin:0px 30px 0px 0px;float:left;max-height:100px;\" border=\"0\">";
        $rtn .= "<br><br>\n";
        $rtn .= "<i style=\"text-align:right;font-size:16pt;\">WOTC Processing Center</i>\n";
        $rtn .= "</div>\n";
        
        if (! strstr($_SERVER["SCRIPT_NAME"], "login") && ! strstr($_SERVER["SCRIPT_NAME"], "logout") && ! strstr($_SERVER["SCRIPT_NAME"], "reset")) {
            
            $navsection =   array(
                                "dashboard" =>  "Dashboard",
                                "lookup"    =>  "Lookup Applicants",
                                "account"   =>  "Account Information",
                                "logininfo" =>  "Login Information"
                            );
            
            global $AUTH;
            if ($AUTH['Dashboard'] == "N") {
                unset($navsection['dashboard']);
            }
            
            $rtn .= "<div id=\"navigation\">\n";
            
            foreach ($navsection as $section => $displaytitle) {
                $rtn .= "<a href=\"" . WOTCADMIN_HOME . "index.php?pg=" . $section;
                if ($wotcID) {
                    $rtn .= "&wotcID=" . $wotcID;
                }
                $rtn .= "\">" . $displaytitle . "</a><br>\n";
            } // end foreach
            
            $rtn .= "<div style=\"margin:15px 0 0 20px;\"><a href=\"" . WOTCADMIN_HOME . "logout.php\"><img src=\"images/door_out.png\" border=\"0\" title=\"Log Out\" style=\"margin:0px 3px -4px 0px;\"><b style=\"FONT-SIZE:8pt;COLOR:#000000\">Log Out</b></a></div>\n";
            
            $rtn .= "</div>\n";
        }
        
        $rtn .= "<div id=\"centerDoc\">\n";
        
        return $rtn;
    } // end function
    
    /**
     * @method      wotcadminFooter    
     * @param       string $wotcID
     * @return      string
     */
    public function wotcadminFooter($wotcID)
    {
        $rtn = "";
        $rtn .= "</div>\n";
        $rtn .= "<div id=\"bottomDoc\">\n";
        $rtn .= "&copy; " . date("Y") . " Cost Management Services, LLC<br>";
        $rtn .= "Powered by <a href=\"https://www.irecruit-software.com\" target=\"_blank\">iRecruit Technology</a>.\n";
        
        $bottomnav = array(
            "terms" => "Terms of Use",
            "policy" => "Privacy Policy"
        );
        
        foreach ($bottomnav as $section => $displaytitle) {
            $rtn .= "&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;\n";
            $rtn .= "<a href=\"" . WOTCADMIN_HOME . "index.php?pg=" . $section;
            if ($wotcID) {
                $rtn .= "&wotcID=" . $wotcID;
            }
            $rtn .= "\">" . $displaytitle . "</a>\n";
        } // end foreach
        
        $rtn .= "</div>\n";
        $rtn .= "<script src=\"".WOTCADMIN_HOME."js/bootstrap.min.js\"></script>\n";
        $rtn .= "</body>\n";
        $rtn .= "</html>\n";
        
        return $rtn;
    } // end function
    
} // end Class
