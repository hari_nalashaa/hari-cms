<?php 
/**
 * @class		Attachments
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Attachments {

	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	
	/**
	 * @method		insApplicantAttachments
	 * @param		$attachment_info
	 * @tutorial	buildInsertStatement will generate the insert
	 * 				query based on the key value pair array
	 */
	public function insApplicantAttachments($attachment_info) {
		
		$ins_app_atchmnt_info             =   $this->db->buildInsertStatement('ApplicantAttachments', $attachment_info);
		$ins_attachment_stmt              =   $ins_app_atchmnt_info["stmt"] . " ON DUPLICATE KEY UPDATE PurposeName = :UPurposeName, FileType = :UFileType";
		$ins_params                       =   $ins_app_atchmnt_info["info"];
		$ins_params[0][':UPurposeName']   =   $ins_params[0][':PurposeName'];
		$ins_params[0][':UFileType']      =   $ins_params[0][':FileType'];
		
		$res_app_atchmnt_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_attachment_stmt, $ins_params );
		
		return $res_app_atchmnt_info;
	}
	
	/**
	 * @method		getApplicantAttachments
	 * @return		associative array
	 * @tutorial	This method will fetch the correspondence packages information.
	 */
	public function getApplicantAttachments($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_applicant_attachments = "SELECT $columns FROM ApplicantAttachments";
	
		if(count($where_info) > 0) $sel_applicant_attachments .= " WHERE " . implode(" AND ", $where_info);
	
		if($order_by != "") $sel_applicant_attachments .= " ORDER BY " . $order_by;
	
		$res_applicant_attachments = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_applicant_attachments, $info );
	
		return $res_applicant_attachments;
	}

	
	/**
	 * @method		getApplicantAttachmentInfo
	 * @return		associative array
	 * @tutorial	This method will fetch the correspondence packages information.
	 */
	public function getApplicantAttachmentInfo($OrgID, $ApplicationID, $TypeAttachment) {
	   
	    $params_info       =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":TypeAttachment"=>$TypeAttachment);
	    $sel_app_att_info  =   "SELECT * FROM ApplicantAttachments WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND TypeAttachment = :TypeAttachment";
	    $res_app_att_info  =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_app_att_info, array($params_info) );
	
	    return $res_app_att_info;
	}
	
	/**
	 * @method     getPurposeNamesList
	 * @params     $OrgID, $FormID
	 */
	public function getPurposeNamesList($OrgID, $FormID) {

        $purpose_names          =   array (
                                        'resumeupload'      =>  'resume',
                                        'coverletterupload' =>  'coverletter',
                                        'otherupload'       =>  'other'
                                    );
	    //Set parameters
        $params_info            =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
        $sel_form_ques_info     =   "SELECT QuestionID, value FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND QuestionTypeID = 8";
        $res_form_ques_info     =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_form_ques_info, array($params_info) );
        $res_form_ques_res      =   $res_form_ques_info['results'];
        $res_form_ques_cnt      =   $res_form_ques_info['count'];
	    
        $purpose                =   array();
        for($pi = 0; $pi < $res_form_ques_cnt; $pi++) {

            if(in_array($res_form_ques_res[$pi]['QuestionID'], array_keys($purpose_names))
                && $res_form_ques_res[$pi]['value'] == "") {
                $res_form_ques_res[$pi]['value']    =   $purpose_names[$res_form_ques_res[$pi]['QuestionID']];
            }
            
            if($res_form_ques_res[$pi]['value'] == "") $res_form_ques_res[$pi]['value'] = "File";

            $res_form_ques_res[$pi]['value']                    =   preg_replace("/[^A-Za-z0-9]/", '', $res_form_ques_res[$pi]['value']).'-'.$res_form_ques_res[$pi]['QuestionID'];
            $purpose[$res_form_ques_res[$pi]['QuestionID']]     =   $res_form_ques_res[$pi]['value'];
        }
        
        return $purpose;
	}
	
	/**
	 * @method		delApplicantAttachments
	 */
	public function delApplicantAttachments($OrgID, $ApplicationID, $Type) {
		
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":TypeAttachment"=>$Type);
		$del_attachments = "DELETE FROM ApplicantAttachments WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND TypeAttachment = :TypeAttachment";
		$res_attachments = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $del_attachments, array($params) );
		
		return $res_attachments;
	}
	
}
?>
