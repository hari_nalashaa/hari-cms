<?php
/**
 * @class		PreFilledForms
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class PreFilledForms {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      getPreFilledFormsListByOrgID
     */
    function getPreFilledFormsListByOrgID($columns = "*", $OrgID) {
        
        $columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
        
        $params_info    =   array(":OrgID"=>$OrgID);
        $sel_forms      =   "SELECT $columns FROM PreFilledForms WHERE OrgID = :OrgID ORDER BY SortOrder ASC";
        $res_forms      =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_forms, array($params_info) );
        $res_forms_cnt  =   $res_forms['count'];
        $res_forms_res  =   $res_forms['results'];
        
        $pre_forms      =   array();
        for($p = 0; $p < $res_forms_cnt; $p++) {
            $pre_forms[]    =   $res_forms_res[$p];
        }
        
        return $pre_forms;
    }
    
}
?>
