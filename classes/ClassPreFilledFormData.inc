<?php
/**
 * @class		PreFilledFormData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class PreFilledFormData {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	
	/**
	 * @method		getPreFilledFormData
	 */
	function getPreFilledFormData($OrgID, $PreFilledFormID, $RequestID, $ApplicationID) {
	
	    $params_info    = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	    $sel_form_data  = "SELECT * FROM PreFilledFormData WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID";
	    $res_form_data  = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
	
	    return $res_form_data;
	}
	
	
	/**
	 * @method		getPreFilledFormDataApplicationIDs
	 * @tutorial    It is a temporary function, we will have to remove it after we have done the upgrade
	 */
	function getPreFilledFormDataApplicationIDs($OrgID) {
	
	    $params_info   =   array(":OrgID"=>$OrgID);
	    $sel_form_data =   "SELECT ApplicationID, RequestID FROM PreFilledFormData WHERE OrgID = :OrgID GROUP BY ApplicationID, RequestID";
	    $res_form_data =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
	    $form_data_res =   $res_form_data['results'];
	    $form_data_cnt =   $res_form_data['count'];
	     
	    $app_ids       =   array();
	    for($fd = 0; $fd < $form_data_cnt; $fd++) {
	        $app_ids[$fd]['ApplicationID'] =   $form_data_res[$fd]['ApplicationID'];
	        $app_ids[$fd]['RequestID']     =   $form_data_res[$fd]['RequestID'];
	    }
	     
	    return $app_ids;
	}
	
	
    /**
	 * @method		insUpdPreFilledFormData
	 */
	function insUpdPreFilledFormData($QI) {

		$params_info    = array(":OrgID"=>$QI['OrgID'], ":ApplicationID"=>$QI['ApplicationID'], ":RequestID"=>$QI['RequestID'], ":PreFilledFormID"=>$QI['PreFilledFormID'], ":QuestionID"=>$QI['QuestionID'], ":Question"=>$QI['Question'], ":QuestionOrder"=>$QI['QuestionOrder'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":value"=>$QI['value'], ":AnswerStatus"=>1, ":IAnswer"=>$QI['Answer'], ":UQuestionTypeID"=>$QI['QuestionTypeID'], ":UQuestion"=>$QI['Question'], ":UAnswer"=>$QI['Answer'], ":Uvalue"=>$QI['value']);
		$ins_form_data  = "INSERT INTO PreFilledFormData(OrgID, ApplicationID, RequestID, PreFilledFormID, QuestionID, Question, QuestionOrder, QuestionTypeID, value, AnswerStatus, Answer)";
		$ins_form_data .= " VALUES(:OrgID, :ApplicationID, :RequestID, :PreFilledFormID, :QuestionID, :Question, :QuestionOrder, :QuestionTypeID, :value, :AnswerStatus, :IAnswer)";
		$ins_form_data .= " ON DUPLICATE KEY UPDATE Answer = :UAnswer, AnswerStatus = 1, QuestionTypeID = :UQuestionTypeID, value = :Uvalue, Question = :UQuestion";
		$res_form_data  = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_data, array($params_info) );
		
		return $res_form_data;
	}
	

	/**
	 * @method     delApplicantPreFilledFormData
	 * @param      string $OrgID
	 * @param      string $PreFilledFormID
	 * @param      string $RequestID
	 * @param      string $ApplicationID
	 * @return     array
	 */
	function delApplicantPreFilledFormData($OrgID, $PreFilledFormID, $RequestID, $ApplicationID) {
	     
	    if($OrgID != "" 
	        && $PreFilledFormID != ""
            && $RequestID != ""
            && $ApplicationID != "") {
	        
	        // Set parameters for prepared query
	        $params_info        =   array (":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	        $del_pre_form_data  =   "DELETE FROM PreFilledFormData WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID";
	        $res_pre_form_data  =   $this->db->getConnection ( $this->conn_string )->delete ( $del_pre_form_data, array ($params_info) );

	        return $res_pre_form_data;
	    }

	    return "";
	}

	/**
	 * @method		determineCurrentI9
	 */
	function determineCurrentI9 ($OrgID, $RequestID, $ApplicationID) {

	    $currentform = 'FE-I9-2020';
	    $currentformm = 'FE-I9m-2020';
	
	    $params_info    = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$currentform, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	    $sel_form_data  = "SELECT PreFilledFormID FROM PreFilledFormData WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID limit 1";
	    $res_form_data  = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_form_data, array($params_info) );

	    if ($res_form_data['PreFilledFormID'] == $currentform) {
	      $res = array("I9"=>$currentform,"I9m"=>$currentformm);  
	    } else {
	      $res = array("I9"=>'FE-I9',"I9m"=>'FE-I9m');
	    }
	
	    return $res;
	}

	/**
	 * @method		determineCurrentW4
	 */
	function determineCurrentW4 ($OrgID, $RequestID, $ApplicationID) {

	    $currentform = 'FE-2021';
	    $olderform = 'FE-2020';

	    $params_info    = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$currentform, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	    $sel_form_data  = "SELECT PreFilledFormID FROM PreFilledFormData WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID limit 1";
	    $res_form_data  = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_form_data, array($params_info) );

	    if ($res_form_data['PreFilledFormID'] == $currentform) {
	      $res = array("W4"=>$currentform);  
	    } else if ($res_form_data['PreFilledFormID'] == $olderform) {
	      $res = array("W4"=>$olderform);  
	    } else {
	      $res = array("W4"=>'FE-1');
	    }
	
	    return $res;
	}
	
}
