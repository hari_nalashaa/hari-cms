<?php
/**
 * @class		RequisitionAttachments
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionAttachments{
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method getRequisitionAttachmentsInfo
	 * @param
	 */
	function getRequisitionAttachmentsInfo($columns, $where_info = array(), $order_by = '', $info = array()) {  
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	    $sel_applicant_vault = "SELECT $columns FROM RequisitionAttachments";
	   
	    if (count ( $where_info ) > 0) {
		    $sel_applicant_vault .= " WHERE " . implode ( " AND ", $where_info );
	    }
	
	    if ($order_by != "")
	        $sel_applicant_vault .= " ORDER BY " . $order_by;
	     $res_applicant_vault = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_vault, $info );
	     return $res_applicant_vault;
	}
	
	/**
	 * @method     insRequisitionAttachments
	 * @param      $info
	 */
	function insRequisitionAttachments($info) {
	
	    $ins_app_vault = $this->db->buildInsertStatement ('RequisitionAttachments', $info );
	    $res_app_vault = $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_vault ['stmt'], $ins_app_vault ['info'] );
	
	    return $res_app_vault;
	}
	
	/**
	 * @method      updRequisitionAttachments
	 * @param       $set_info, $where_info, $info
	 */
	function updRequisitionAttachments($set_info = array(), $where_info = array(), $info) {
	    
	    $upd_checklist_data_info = $this->db->buildUpdateStatement("RequisitionAttachments", $set_info, $where_info);
	    $res_checklist_data_info = $this->db->getConnection( $this->conn_string )->update($upd_checklist_data_info, $info);
	    
	    return $res_checklist_data_info;
	}
	
	
	/**
	 * @method 		delRequisitionAttachements
	 * @param		$where_info = array(), $info = array()
	 */ 
	function delRequisitionAttachments($where_info = array(), $info = array()) {
	
	    $del_applicant_vault = "DELETE FROM RequisitionAttachments";
	
	    $del_applicant_vault .= " WHERE " . implode ( " AND ", $where_info );
	
	    $res_applicant_vault = $this->db->getConnection ( $this->conn_string )->delete ( $del_applicant_vault, $info );
	
	    return $res_applicant_vault;
	}
	

	/**
	 * @method 		softdelRequisitionAttachments
	 * @param		$where_info = array(), $info = array()
	 */ 
	function softdelRequisitionAttachments($where_info = array(), $info = array()) {
	     
            $upd_applicant_vault = "UPDATE RequisitionAttachments SET Deleted = 1, Date = NOW(),WHERE OrgID = :OrgID AND RequestID = :RequestID AND Actual_File = :Actual_File";
            $res_applicant_vault = $this->db->getConnection ($this->conn_string )->update ($upd_applicant_vault, $info );
	    	
	    return $res_applicant_vault;
	}

	/**
	 * @method		getAttachmentFileName
	 * @param 		$OrgID, $RequestID, $FileID
	 * @return		integer
	 */
	function getAttachmentFileName($OrgID, $RequestID, $FileID) {

	    // Set parameters for prepared query
	    $params        =   array (":OrgID" => $OrgID, ":RequestID" => $RequestID, ":FileID" => $FileID);
	    $sel_filename  =   "SELECT FileName from RequisitionAttachments";
	    $sel_filename .=  " WHERE OrgID = :OrgID AND RequestID = :RequestID AND FileID = :FileID";
	    $result =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_filename, array ($params) );
	
	    return $result['FileName'];
	}
}
