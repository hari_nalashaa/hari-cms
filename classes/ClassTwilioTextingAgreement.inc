<?php
/**
 * @class		TwilioTextingAgreement
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioTextingAgreement {
    
    public $db;
    
    var $conn_string       		=   "IRECRUIT";
    
    /**
     * @tutorial	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db	=	Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function
    
    /**
     * @method      getTextingAgreementByOrgID
     * @return      array
     */
    function getTextingAgreementByOrgID($OrgID) {
        
        $params		=	array(":OrgID"=>$OrgID);
        $sel_info	=	"SELECT * FROM TwilioTextAgreement WHERE OrgID = :OrgID";
        $res_info	=	$this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
        
        return $res_info;
    }
    
    /**
     * @method      getTextingAgreementByOrgID
     * @return      array
     */
    function getTextingAgreementsList() {
        
        $sel_info	=	"SELECT * FROM TwilioTextAgreement WHERE Active = 'Y' AND (DATEDIFF(DATE(NOW()), DATE(LastBilledDateTime)) > 30)";
        $res_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
        
        $text_agreement_orgs_info   =   array();
        foreach ($res_info['results'] as $res_info) {
            $text_agreement_orgs_info[$res_info['OrgID']]   =   $res_info;
        }
        
        return $text_agreement_orgs_info;
    }
    
    /**
     * @method  	insTextingAgreementByOrgID
     */
    public function insTextingAgreementByOrgID($info) {
        $ins_text_agreement =   $this->db->buildInsertStatement('TwilioTextAgreement', $info);
        $res_text_agreement =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_text_agreement["stmt"], $ins_text_agreement["info"] );
        
        return $res_text_agreement;
    }
    
    /**
     * @method  	updTextingAgreementByOrgID
     */
    public function updTextingAgreementByOrgID($info) {
        // Set parameters
        $params 	=	array (
                            ":OrgID"                        =>  $info['OrgID'],
                            ":UserID"                       =>  $info['UserID'],
                            ":TotalAmount"                  =>  $info['TotalAmount'],
                            ":AgreeToIrecruitPaidText"      =>  $info['AgreeToIrecruitPaidText'],
                            ":AgreeToTermsOfService"        =>  $info['AgreeToTermsOfService'],
                            ":AgreeToAcceptableUsePolicy"   =>  $info['AgreeToAcceptableUsePolicy'],
                            ":LastModifiedDateTime"         =>  "NOW()",
                        );
        // Update session values
        $upd_info  	=	'UPDATE TwilioTextAgreement';
        $upd_info  	.=	' SET UserID = :UserID, ';
        $upd_info  	.=	' TotalAmount = :TotalAmount,';
        $upd_info  	.=	' AgreeToIrecruitPaidText = :AgreeToIrecruitPaidText,';
        $upd_info  	.=	' AgreeToTermsOfService = :AgreeToTermsOfService,';
        $upd_info  	.=	' AgreeToAcceptableUsePolicy = :AgreeToAcceptableUsePolicy,';
        $upd_info  	.=	' LastModifiedDateTime = :LastModifiedDateTime,';
        $upd_info  	.=	' WHERE OrgID = :OrgID';
        $res_info 	=	$this->db->getConnection ( $this->conn_string )->update ( $upd_info, array ($params) );
        
        return $res_info;
    }
    
    /**
     * @method  	updTextingAgreementLastBilledDateTime
     */
    public function updTextingAgreementLastBilledDateTime($OrgID) {
        // Set parameters
        $params 	=	array (
                            ":OrgID"    =>  $OrgID,
                        );
        // Update session values
        $upd_info  	=	'UPDATE TwilioTextAgreement';
        $upd_info  	.=	' SET LastBilledDateTime = NOW(), ';
        $upd_info  	.=	' LastModifiedDateTime = NOW()';
        $upd_info  	.=	' WHERE OrgID = :OrgID';
        $res_info 	=	$this->db->getConnection ( $this->conn_string )->update ( $upd_info, array ($params) );
        
        return $res_info;
    }
    
    /**
     * @method  	delTextingAgreementByOrgID
     */
    public function delTextingAgreementByOrgID($OrgID) {
        
        $params		=	array(":OrgID"=>$OrgID);
        $del_info  	=	'DELETE FROM TwilioTextAgreement WHERE OrgID = :OrgID';
        $res_info 	=	$this->db->getConnection ( $this->conn_string )->delete ( $del_info, array ($params) );
        
        return $res_info;
    }
    
}
