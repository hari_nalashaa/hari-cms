<?php 
/**
 * @class		WeightedSearch
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WeightedSearch {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	// Select Active Forms
	public function FormQuestions($OrgID) {
		
		$queryforms = "SELECT DISTINCT(FormID) FROM FormQuestions
						WHERE OrgID = '".$OrgID."'
						AND FormID IN (SELECT DISTINCT(FormID) FROM Requisitions
						WHERE OrgID = '".$OrgID."')";
		$resultforms  = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($queryforms);
		
		return $resultforms;
	}
	
	//Select FormQuestions By Section
	public function FormQuestionsBySection($FormID, $SectionID, $OrgID) {
		
		$selformquestions = "SELECT * FROM FormQuestions
							 WHERE FormID = '".$FormID."'
						     AND SectionID = '" . $SectionID . "'
							 AND OrgID = '".$OrgID."'
							 AND Active = 'Y' ORDER BY QuestionOrder ASC";
		$resformquestions = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($selformquestions);
		
		return $resformquestions;
	}
	
	//Get Saved Search
	public function WeightedSearchInfo($UWkey) {
		
		$selweightedsearch = "SELECT * FROM WeightSaveSearch WHERE WeightedSearchRandId = '".$UWkey."'";
		$resweightedsearch  = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($selweightedsearch);
		
		return $resweightedsearch['results'];
	}
	
	//Weighted Search List
	public function WeightedSearchList($USERID) {
		
		$selweightedsearch  = "SELECT * FROM WeightSaveSearch WHERE UserID = '".$USERID."' GROUP BY WeightedSearchRandId";
		$resweightedsearch  = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($selweightedsearch);

		return $resweightedsearch['results'];
	}
	

	/**
	* @method		getWeightedSearchNameValues
	* @param		$columns, $where_info, $order_by, $info
	* @return		associative array
	* @tutorial	    This method will fetch the requisition informtaion
	*          	    based on OrgID, MultiOrgID, RequestID.
	*/
	public function getWeightedSearchNameValues($OrgID, $UserID) {

	       $params = array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
           $sel_weighted_search  = "SELECT WeightedSearchRandId, SaveSearchName FROM WeightSaveSearch WHERE OrgID = :OrgID AND UserID = :UserID GROUP BY WeightedSearchRandId";
           $res_weighted_search  = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_weighted_search, array($params));
           $weighted_search_info = $res_weighted_search['results'];

	       $weighted_search_name_values = array();
           
           for($i = 0; $i < count($weighted_search_info); $i++) {
                $weighted_search_name_values[$weighted_search_info[$i]['WeightedSearchRandId']] = $weighted_search_info[$i]['SaveSearchName'];
           }

           return $weighted_search_name_values;
    }
	
	
	/**
	 * @method		getWeightSaveSearchRowInfo
	 * @param		$columns, $WeightedSearchRandId
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	public function getWeightSaveSearchRowInfo($columns = "", $WeightedSearchRandId) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$params = array(":WeightedSearchRandId"=>$WeightedSearchRandId);
		$sel_info = "SELECT $columns FROM WeightSaveSearch WHERE WeightedSearchRandId = :WeightedSearchRandId";
		$res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_info, array($params) );
	
		return $res_info;
	}
	
	/**
	 * @method		getWeightSaveSearchInfo
	 * @param		$columns, $where_info, $order_by, $info
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	public function getWeightSaveSearchInfo($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $sel_info = "SELECT $columns FROM WeightSaveSearch";
	
	    if (count ( $where_info ) > 0) {
	        $sel_info .= " WHERE " . implode ( " AND ", $where_info );
	    }
	
	    if($group_by != "") $sel_info .= " GROUP BY " . $group_by;
	    if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
	    $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_info, $info );
	
	    return $res_info;
	}
	
	/**
	 * @method	insWeightSaveSearch
	 * @param	$info
	 */
	public function insWeightSaveSearch($info) {
		
		$insert_info = $this->db->buildInsertStatement('WeightSaveSearch', $info);
		$result_info = $this->db->getConnection ( "IRECRUIT" )->insert($insert_info['stmt'], $insert_info['info']);
		return $result_info;
	}
	
	
	/**
	 * @method		insUpdWeightSaveSearch
	 * @param		$weig_search_ins_info, $on_update = '', $update_info = array()
	 * @return		array
	 */
	function insUpdWeightSaveSearch($weig_search_ins_info, $on_update = '', $update_info = array()) {
	
	    $weighted_search_info  =   $this->db->buildInsertStatement('WeightSaveSearch', $weig_search_ins_info);
	    $insert_statement      =   $weighted_search_info["stmt"];
	    
	    if ($on_update != '') {
	        $insert_statement .= $on_update;
	        if (is_array ( $update_info )) {
	            foreach ( $update_info as $upd_key => $upd_value ) {
	                $weighted_search_info["info"][0][$upd_key] = $upd_value;
	            }
	        }
	    }

	    $res_weighted_search_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $weighted_search_info["info"] );
	
	    return $res_weighted_search_info;
	}	
	
	
	/**
	 * @method	delWeightSaveSearch
	 * @param	$info
	 */
	public function delWeightSaveSearch($uwkey) {
		
		$params = array(":WeightedSearchRandId"=>$uwkey);
		$delsavedsearch = "DELETE FROM `WeightSaveSearch` WHERE WeightedSearchRandId = :WeightedSearchRandId";
		$ressavedsearch = $this->db->getConnection ( "IRECRUIT" )->delete($delsavedsearch, array($params));
		return $ressavedsearch;
	}
	
	
	/**
	 * @method	delWeightSaveSearchQuestion
	 * @param	$info
	 */
	public function delWeightSaveSearchQuestion($uwkey, $question_id) {
	
	    $params = array(":WeightedSearchRandId"=>$uwkey, ":QuestionID"=>$question_id);
	    $delsavedsearch = "DELETE FROM `WeightSaveSearch` WHERE WeightedSearchRandId = :WeightedSearchRandId AND QuestionID = :QuestionID";
	    $ressavedsearch = $this->db->getConnection ( "IRECRUIT" )->delete($delsavedsearch, array($params));
	    return $ressavedsearch;
	}
	
	/**
	 * @method	getWeightedScoreOfApplicant
	 * @param	$info
	 */
	public function getWeightedScoreOfApplicant($OrgID, $ApplicationID, $WeightedSearchCategoryID) {

	    global $ApplicantsObj, $WeightedSearchObj;
	    
	    $WSFList = $WeightedSearchObj->WeightedSearchInfo($WeightedSearchCategoryID);
	    $APPDATA = $ApplicantsObj->getAppData($OrgID, $ApplicationID);
	    
	    /**
	     * @tutorial   Calculate Total Weight Of That Application
	     */
	    $total = 0;
	    $answer = 0;
	    
	    $WEIGHT = array();
	    for($we = 0; $we < count ( $WSFList ); $we ++) {
	    
	        $input = $WSFList [$we] ['QuestionAnswerKey'];
	        $rank = $WSFList [$we] ['QuestionAnswerWeight'];
	    
	        if ($rank != "") {
	            $val = explode ( ':', $input );
	    
	            //Set where condition
	            $where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "(QuestionID LIKE :QuestionID OR SUBSTRING(QuestionID FROM 2) = :SUBQuestionID)", "Answer != :Answer");
	            //Set parameters
	            $params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>$val[1]."%", ":SUBQuestionID"=>$val [1], ":Answer"=>$val[1]);
	            //Get ApplicantData Information
	            $resultsQ = $ApplicantsObj->getApplicantDataInfo("*", $where, '', 'ApplicationID', array($params));
	            
	            if($resultsQ['count'] > 0) {
	                if($WSFList[$we]['QuestionTypeID'] != "100") {
	                    if($val[2] == 'QUS') {
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["QUS"] = $rank;
	                    }
	                    else {
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ALL"][] = $rank;
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ANSMULWEIGHT"][$val[3]] = $rank;
	                    }
	                }
	                else if($WSFList[$we]['QuestionTypeID'] == "100") {
	                    if($val[2] == 'QUS') {
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["QUS"] = $rank;
	                    }
	                }
	            }
	            else {
	                $WEIGHT[$ApplicationID][$val [0]][$val[1]]["QUS"] = 0;
	                $WEIGHT[$ApplicationID][$val[0]][$val[1]]["ANS"] = 0;
	            }
	            
	            
	            if(is_array($resultsQ['results'])) {
	                foreach($resultsQ['results'] as $AD) {
	                    if($WSFList[$we]['QuestionTypeID'] == "100") {
	                    	$cust_que_answer_weight = unserialize($WSFList[$we]['CustomQuestionAnswerWeight']);
    				    	$cust_que_information = unserialize($WSFList[$we]['CustomQuestion']);
    				    	$cust_que_answer_info = unserialize($AD["Answer"]);
    				    	
    				    	$flip_cust_que_label_info = array_flip($cust_que_information['LabelValRow']);
    				    	$flip_cust_que_rval_info = array_flip($cust_que_information['RVal']);
    				    	
    				    	$cust_que_col_info = $cust_que_information['RVal'];
    				    	$cust_que_row_info = $cust_que_information['LabelValRow'];
    				    	$answer_weight_info = array();
    				    	$answer_weight = 0;
    				    	foreach ($cust_que_answer_info as $cust_que_label_name=>$cust_que_sel_info) {
    				    	    $cust_que_sel_label_info = array_keys($cust_que_sel_info);
    				    	    $cust_que_sel_col_value = $cust_que_sel_info[$cust_que_sel_label_info[0]];
    				    	    $cust_que_sel_col_key = $flip_cust_que_rval_info[$cust_que_sel_col_value];
    				    	    $label_row_id = $flip_cust_que_label_info[$cust_que_label_name];
    				    	    $answer_weight += $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
    				    	    $answer_weight_info[$label_row_id."-".$cust_que_sel_col_key] = $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
    				    	}
	                        
	                        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["ANS"] = $answer_weight;
	                    }
	                    else if($WSFList[$we]['QuestionTypeID'] == "1818" || $WSFList[$we]['QuestionTypeID'] == "18") {
	                        $answer = 0;
	                    
	                        $ans_cnt = $APPDATA[$val[1].'cnt'];
	                    
	                        if($ans_cnt > 0) {
	                            for($ac = 1; $ac <= $ans_cnt; $ac++) {
	                    
	                                $mul_ans = $APPDATA[$val[1].'-'.$ac];
	                    
	                                if($mul_ans != "") {
	                                    $answer += $WEIGHT[$ApplicationID][$val[0]][$val[1]]["ANSMULWEIGHT"][$mul_ans];
	                                }
	                    
	                            }
	                        }
	                    
	                        $WEIGHT[$ApplicationID][$val[0]][$val[1]]["ANS"]   = $answer;
	                    }
	                    else if ($AD["Answer"] == "$val[3]") {
	                        $answer = $rank;
	                        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["ANS"] = $answer;
	                    }
	                } // end foreach
	            }
	        } // end rank
	    }
	    
	    
	    $total = 0;
	    
	    if(is_array($WEIGHT) 
	        && isset($WEIGHT[$ApplicationID])
	        && is_array($WEIGHT[$ApplicationID]) 
	        && isset($WeightedSearchCategoryID) 
	        && $WeightedSearchCategoryID != "") {
	        foreach($WEIGHT[$ApplicationID] as $WS=>$WQ) {
	            foreach($WQ as $QuestionID=>$WQINFO) {
	                $total += $WQINFO['QUS'];
	                $total += $WQINFO['ANS'];
	            }
	        }
	    }
	    
	    return $total;
	}
	
	
	/**
	 * @method	getScoreOfApplicantForWebForm
	 * @param	$info
	 */
	public function getScoreOfApplicantForWebForm($OrgID, $ApplicationID, $WebFormID, $WeightedSearchCategoryID) {
	
	    global $ApplicantsObj, $WeightedSearchObj, $FormDataObj;
	     
	    $WSFList = $WeightedSearchObj->WeightedSearchInfo($WeightedSearchCategoryID);
	    $WebFormID = $WSFList[0]['SectionID']; //Here WebFormID = SectionID only (for only FormID = WebForms)
	    
	    //Set where condition
	    $where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID");
	    //Set parameters
	    $params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":WebFormID"=>$WebFormID);
	    //Get ApplicantData Information
	    $resultsQ = $FormDataObj->getWebFormData("*", $where, '', 'ApplicationID', array($params));
	    $resultsQ_results = $resultsQ['results'];
	    
	    for($jw = 0; $jw < count($resultsQ_results); $jw++) {
	        $WEBFORMDATA[$resultsQ_results[$jw]['QuestionID']] = $resultsQ_results[$jw]['Answer'];
	    }
	     
	    /**
	     * @tutorial   Calculate Total Weight Of That Application
	     */
	    $total = 0;
	    $answer = 0;
	    $WEIGHT = array();
	    for($we = 0; $we < count ( $WSFList ); $we ++) {
	         
	        $input = $WSFList [$we] ['QuestionAnswerKey'];
	        $rank = $WSFList [$we] ['QuestionAnswerWeight'];
	         
	        if ($rank != "") {
	            $val = explode ( ':', $input );

	            //Set where condition
	            $where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID", "(QuestionID LIKE :QuestionID OR SUBSTRING(QuestionID FROM 2) = :SUBQuestionID)", "Answer != :Answer", "Answer != ''");
	            //Set parameters
	            $params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":WebFormID"=>$WebFormID, ":QuestionID"=>$val[1]."%", ":SUBQuestionID"=>$val [1], ":Answer"=>$val[1]);
	            //Get ApplicantData Information
	            $resultsQ = $FormDataObj->getWebFormData("*", $where, '', 'ApplicationID', array($params));
	            
	            if($resultsQ['count'] > 0) {
	                if($WSFList[$we]['QuestionTypeID'] != "100") {
	                    if($val[2] == 'QUS') {
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["QUS"] = $rank;
	                    }
	                    else {
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ALL"][] = $rank;
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ANSMULWEIGHT"][$val[3]] = $rank;
	                    }
	                }
	                else if($WSFList[$we]['QuestionTypeID'] == "100") {
	                    if($val[2] == 'QUS') {
	                        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["QUS"] = $rank;
	                    }
	                }
	            }
	            else {
	                $WEIGHT[$ApplicationID][$val [0]][$val[1]]["QUS"] = 0;
	                $WEIGHT[$ApplicationID][$val[0]][$val[1]]["ANS"] = 0;
	            }
	            
	            if(is_array($resultsQ['results'])) {
	                foreach($resultsQ['results'] as $AD) {
	                    if($WSFList[$we]['QuestionTypeID'] == "100") {
	                    $cust_que_answer_weight = unserialize($WSFList[$we]['CustomQuestionAnswerWeight']);
    				    	$cust_que_information = unserialize($WSFList[$we]['CustomQuestion']);
    				    	$cust_que_answer_info = unserialize($AD["Answer"]);
    				    	
    				    	$flip_cust_que_label_info = array_flip($cust_que_information['LabelValRow']);
    				    	$flip_cust_que_rval_info = array_flip($cust_que_information['RVal']);
    				    	
    				    	$cust_que_col_info = $cust_que_information['RVal'];
    				    	$cust_que_row_info = $cust_que_information['LabelValRow'];
    				    	$answer_weight_info = array();
    				    	$answer_weight = 0;
    				    	foreach ($cust_que_answer_info as $cust_que_label_name=>$cust_que_sel_info) {
    				    	    $cust_que_sel_label_info = array_keys($cust_que_sel_info);
    				    	    $cust_que_sel_col_value = $cust_que_sel_info[$cust_que_sel_label_info[0]];
    				    	    $cust_que_sel_col_key = $flip_cust_que_rval_info[$cust_que_sel_col_value];
    				    	    $label_row_id = $flip_cust_que_label_info[$cust_que_label_name];
    				    	    $answer_weight += $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
    				    	    $answer_weight_info[$label_row_id."-".$cust_que_sel_col_key] = $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
    				    	}
	                         
	                        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["ANS"] = $answer_weight;
	                    }
	                    else if($WSFList[$we]['QuestionTypeID'] == "1818" || $WSFList[$we]['QuestionTypeID'] == "18") {
	                        $answer = 0;
	                         
	                        $ans_cnt = $WEBFORMDATA[$val[1].'cnt'];
	                         
	                        if($ans_cnt > 0) {
	                            for($ac = 1; $ac <= $ans_cnt; $ac++) {
	                                 
	                                $mul_ans = $WEBFORMDATA[$val[1].'-'.$ac];
	                                 
	                                if($mul_ans != "") {
	                                    $answer += $WEIGHT[$ApplicationID][$val[0]][$val[1]]["ANSMULWEIGHT"][$mul_ans];
	                                }
	                                 
	                            }
	                        }
	                         
	                        $WEIGHT[$ApplicationID][$val[0]][$val[1]]["ANS"]   = $answer;
	                    }
	                    else if ($AD["Answer"] == "$val[3]") {
	                        $answer = $rank;
	                        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["ANS"] = $answer;
	                    }
	                } // end foreach
	            }
	        } // end rank
	    }

	    $total = 0;
	    if(is_array($WEIGHT) && is_array($WEIGHT[$ApplicationID]) && $WeightedSearchCategoryID != "") {
	        foreach($WEIGHT[$ApplicationID] as $WS=>$WQ) {
	            foreach($WQ as $QuestionID=>$WQINFO) {
	                $total += $WQINFO['QUS'];
	                $total += $WQINFO['ANS'];
	            }
	        }
	    }
	    
	    return $total;
	}
}
?>
