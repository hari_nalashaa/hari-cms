<?php
/**
 * @class		TwilioLookUpApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioLookUpApi {
	use TwilioSettings;
	
	public $db                  =	"";
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           	=   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
        
        //Get twilio settings
        $this->getTwilioSettings();
    } // end function

    /**
     * @method		getLookUpPhoneNumberInfo
     */
    public function getLookUpPhoneNumberInfo($OrgID, $phone_number) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
	    	$lookup_phone_num_info	=	$client->lookups->v1->phoneNumbers($phone_number)->fetch(["type" => ["carrier"]]);
	    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-lookup-phone-number-api.txt", serialize($lookup_phone_num_info), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Get the lookup phone number info successfully", "Response"=>$lookup_phone_num_info);
    	}
 		catch (RestException | TwilioException | Exception $e) {

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-lookup-phone-number-api-error.txt", serialize($e->getMessage()), "w+", false);
    		
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed get the lookup phone number info", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }

}
