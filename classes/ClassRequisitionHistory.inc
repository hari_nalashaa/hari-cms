<?php
/**
 * @class		RequisitionHistory
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection, by default connection is based on 
 * 				constructor identifer that we are using
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionHistory {

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

    /**
     * @method      getRequisitionHistoryInfo
     */
	function getRequisitionHistoryInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM RequisitionHistory";
	
		if(count($where_info) > 0) $sel_info .= " WHERE " . implode(" AND ", $where_info);
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method      getRequisitionHistoryDetailInfo
	 */
	function getRequisitionHistoryDetailInfo($OrgID, $RequestID, $RequisitionHistoryID) {
	
	    $params_info   =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":RequisitionHistoryID"=>$RequisitionHistoryID);
	    $sel_info      =   "SELECT * FROM RequisitionHistory WHERE OrgID = :OrgID AND RequestID = :RequestID AND RequisitionHistoryID = :RequisitionHistoryID";
	    $res_info      =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params_info) );
	
	    return $res_info;
	}

	/**
	 * @method		insRequisitionHistory
	 * @param		array
	 */
	function insRequisitionHistory($insert_info) {
	
	    $ins_req_history   =   $this->db->buildInsertStatement('RequisitionHistory', $insert_info); 
	    $res_req_history   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_req_history["stmt"], $ins_req_history["info"] );
	
	    return $res_req_history;
	}
	
	/**
	 * @method     delRequisitionHistoryByPrimaryKey
	 * @param      string $OrgID
	 * @param      string $RequestID
	 * @param      string $RequisitionHistoryID
	 * @return     array
	 */
	function delRequisitionHistoryByPrimaryKey($OrgID, $RequestID, $RequisitionHistoryID) {
	
	    $params_history    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":RequisitionHistoryID"=>$RequisitionHistoryID);
	    $del_req_history   =   "DELETE RequisitionHistory WHERE OrgID = :OrgID AND RequestID = :RequestID AND RequisitionHistoryID = :RequisitionHistoryID";
	    $res_req_history   =   $this->db->getConnection ( $this->conn_string )->delete( $del_req_history, array($params_history) );
	
	    return $res_req_history;
	}
	
	/**
	 * @method     delRequisitionHistoryByRequestID
	 * @param      string $OrgID
	 * @param      string $RequestID
	 */
	function delRequisitionHistoryByRequestID($OrgID, $RequestID) {
	
	    $params_history    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	    $del_req_history   =   "DELETE RequisitionHistory WHERE OrgID = :OrgID AND RequestID = :RequestID";
	    $res_req_history   =   $this->db->getConnection ( $this->conn_string )->delete( $del_req_history, array($params_history) );
	
	    return $res_req_history;
	}
	
}
?>
