<?php
class DateHelper {

	/**
	 * @method getYmdFromMdy
	 * @param $mdy
	 * @return string
	 * @tutorial This method will convert ymd format to mdy.
	 */
	function getYmdFromMdy($mdy, $input_seperator = "/", $output_seperator = "-") {
		if ($mdy == "")
			return null;
		
		$date = explode ( $input_seperator, $mdy );

		return $date [2] . $output_seperator . $date [0] . $output_seperator . $date [1];
	}
	
	/**
	 * @method getMdyFromYmd
	 * @param  $ymd
	 * @return string
	 * @tutorial This method will convert mdy format to ymd.
	 */
	function getMdyFromYmd($ymd, $input_seperator = "-", $output_seperator = "/") {
		if ($ymd == "")
			return null; // ymd , mdy
		$date = explode ( $input_seperator, $ymd );
		return $date [1] . $output_seperator . $date [2] . $output_seperator . $date [0];
	}
	
	/**
	 * @method getMdyFromDmy
	 * @param  $dmy
	 * @return string
	 * @tutorial This method will convert dmy format to mdy.
	 */
	function getMdyFromDmy($dmy, $input_seperator = "/", $output_seperator = "/") {
	    if ($dmy == "")
	        return null; // dmy , mdy
	    $date = explode ( $input_seperator, $dmy );
	    return $date [1] . $output_seperator . $date [0] . $output_seperator . $date [2];
	}
	
	/**
	 * @method getMonthsList
	 * @return array
	 */
	function getMonthsList() {
		$months = array("1"=>"January", "2"=>"February", "3"=>"March", "4"=>"April", 
						"5"=>"May", "6"=>"June", "7"=>"July", "8"=>"August", 
						"9"=>"September", "10"=>"October", "11"=>"November", "12"=>"December");
		return $months;
	}
	
	/**
	 * @method     getDaysDifference
	 */
	function getDaysDifference($date1, $date2) {
	    // Declare and define two dates
	    $date1 = strtotime($date1);
	    $date2 = strtotime($date2);
	    
        // Formulate the Difference between two dates 
        $diff = abs($date2 - $date1);    
          
        // To get the year divide the resultant date into 
        // total seconds in a year (365*60*60*24) 
        $years = floor($diff / (365*60*60*24));  
          
        // To get the month, subtract it with years and 
        // divide the resultant date into 
        // total seconds in a month (30*60*60*24) 
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
          
          
        // To get the day, subtract it with years and  
        // months and divide the resultant date into 
        // total seconds in a days (60*60*24) 
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
          
          
        // To get the hour, subtract it with years,  
        // months & seconds and divide the resultant 
        // date into total seconds in a hours (60*60) 
        $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
          
          
        // To get the minutes, subtract it with years, 
        // months, seconds and hours and divide the  
        // resultant date into total seconds i.e. 60 
        $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);  
          
          
        // To get the minutes, subtract it with years, 
        // months, seconds, hours and minutes  
        $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));  
          
        return array("years"=>$years, "months"=>$months, "days"=>$days, "hours"=>$hours, "minutes"=>$minutes, "seconds"=>$seconds);  
	}
	
    /**
     * @method      dynamicCalendar
     * @param       array $LST
     * @param       string $modal
     * @return      HTML
     */
    public function dynamicCalendar($LST, $modal)
    {
        $rtn = "";
        $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\">\n";
        
        $lst = "";
        foreach ($LST as $cal) {
            $lst .= "#" . $cal . ", ";
        }
        $lst = substr($lst, 0, - 2);
        
        if ($lst) {
            
            if ($modal == 'modal') {
                $rtn .= "function again_cal() {\n";
                $rtn .= "var dates = $('" . $lst . "').datepicker(\n";
                $rtn .= "  {\n";
                $rtn .= "    changeMonth: true,\n";
                $rtn .= "    changeYear: true,\n";
                $rtn .= "    showButtonPanel: true,\n";
                $rtn .= "    showOn: 'button',\n";
                $rtn .= "    buttonImage: '" . WOTCADMIN_HOME . "calendar/css/smoothness/images/calendar.gif',\n";
                $rtn .= "    yearRange: '-80:+5',\n";
                $rtn .= "    buttonImageOnly: true,\n";
                $rtn .= "    buttonText: 'Select Date'\n";
                $rtn .= "  });\n";
                $rtn .= "}\n";
            } else {
                $rtn .= "$(function() {\n";
                $rtn .= "var dates = $('" . $lst . "').datepicker(\n";
                $rtn .= "  {\n";
                $rtn .= "    changeMonth: true,\n";
                $rtn .= "    changeYear: true,\n";
                $rtn .= "    showButtonPanel: true,\n";
                $rtn .= "    showOn: 'button',\n";
                $rtn .= "    buttonImage: '" . WOTCADMIN_HOME . "calendar/css/smoothness/images/calendar.gif',\n";
                $rtn .= "    yearRange: '-80:+5',\n";
                $rtn .= "    buttonImageOnly: true,\n";
                $rtn .= "    buttonText: 'Select Date'\n";
                $rtn .= "  });\n";
                $rtn .= "});\n";
            }
        } // end lst
        
        $rtn .= "</script>\n";
        return $rtn;
    } // end function
	
}
?>