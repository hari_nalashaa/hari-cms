<?php
/**
 * @class		Applicants
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Applicants {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getApplicantDataInfo
	 */
	function getApplicantDataInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_applicant_data = "SELECT $columns FROM ApplicantData";
		
		if (count ( $where_info ) > 0) {
			$sel_applicant_data .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		if($group_by != "") $sel_applicant_data .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_applicant_data .= " ORDER BY " . $order_by;
		
		$res_applicant_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, $info );
		
		return $res_applicant_data;
	}
	
	/**
	 * @method		getApplicantSummaryQuestionsInfo
	 */
	function getApplicantSummaryQuestionsInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_applicant_data = "SELECT $columns FROM ApplicantSummaryQuestions";
	
		if (count ( $where_info ) > 0) {
			$sel_applicant_data .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		if($group_by != "") $sel_applicant_data .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_applicant_data .= " ORDER BY " . $order_by;
	
		$res_applicant_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, $info );
	
		return $res_applicant_data;
	}
	
	
	/**
	 * @method		insApplicantSummaryQuestionsInfo
	 * @param		array $records_list
	 * @tutorial	Here $records_list contains set of rows data that have to insert
	 *           	all the values will be passed as in documentation
	 */
	function insApplicantSummaryQuestionsInfo($info) {

		$ins_app_summ_que =   $this->db->buildInsertStatement('ApplicantSummaryQuestions', $info);
		$ins_statement    =   $ins_app_summ_que["stmt"] . " ON DUPLICATE KEY UPDATE Question = :UQuestion";
		$params           =   $ins_app_summ_que["info"];
		$params[0][':UQuestion'] = $params[0][':Question'];
		$res_app_summ_que = $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, $params );
		
		return $res_app_summ_que;
	}

	/**
	 * @method 		delApplicantSummaryQuestionsInfo
	 * @param 		string $OrgID
	 * @param 		string $QuestionID
	 */
	function delApplicantSummaryQuestionsInfo($OrgID, $QuestionID) {
	
		// Set parameters for prepared query
		$params       =   array (":OrgID" => $OrgID, ":QuestionID" => $QuestionID);
		$del_app_data =   "DELETE FROM ApplicantSummaryQuestions WHERE OrgID = :OrgID AND QuestionID = :QuestionID";
		$res_app_data =   $this->db->getConnection ( $this->conn_string )->delete ( $del_app_data, array ($params) );
	
		return $res_app_data;
	}
	
	/**
	 * @method		getComparativeAnalysisQuestionsInfo
	 */
	function getComparativeAnalysisQuestionsInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_applicant_data = "SELECT $columns FROM ComparativeAnalysisQuestions";
	
		if (count ( $where_info ) > 0) {
			$sel_applicant_data .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		if($group_by != "") $sel_applicant_data .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_applicant_data .= " ORDER BY " . $order_by;
	
		$res_applicant_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, $info );
	
		return $res_applicant_data;
	}
	
	
	/**
	 * @method		insComparativeAnalysisQuestionsInfo
	 * @param		array $records_list
	 * @tutorial	Here $records_list contains set of rows data that have to insert
	 *           	all the values will be passed as in documentation
	 */
	function insComparativeAnalysisQuestionsInfo($info) {

		$ins_app_summ_que =   $this->db->buildInsertStatement('ComparativeAnalysisQuestions', $info);
		$ins_statement    =   $ins_app_summ_que["stmt"] . " ON DUPLICATE KEY UPDATE Question = :UQuestion";
		$params           =   $ins_app_summ_que["info"];
		$params[0][':UQuestion'] = $params[0][':Question'];
		$res_app_summ_que = $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, $params );
		
		return $res_app_summ_que;
	}

	/**
	 * @method 		delComparativeAnalysisQuestionsInfo
	 * @param 		string $OrgID
	 * @param 		string $QuestionID
	 */
	function delComparativeAnalysisQuestionsInfo($OrgID, $QuestionID) {
	
		// Set parameters for prepared query
		$params       =   array (":OrgID" => $OrgID, ":QuestionID" => $QuestionID);
		$del_app_data =   "DELETE FROM ComparativeAnalysisQuestions WHERE OrgID = :OrgID AND QuestionID = :QuestionID";
		$res_app_data =   $this->db->getConnection ( $this->conn_string )->delete ( $del_app_data, array ($params) );
	
		return $res_app_data;
	}
	
	/**
	 * @method 		delApplicantData
	 * @param 		string $OrgID
	 * @param 		string $HoldID
	 */
	function delApplicantData($OrgID, $ApplicationID, $QuestionID) {
		
		// Set parameters for prepared query
		$params       =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>$QuestionID);
		$del_app_data =   "DELETE FROM ApplicantData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND QuestionID = :QuestionID";
		$res_app_data =   $this->db->getConnection ( $this->conn_string )->delete ( $del_app_data, array (
				$params
		) );
	
		return $res_app_data;
	}
	
	/**
	 * @method		insApplicantData
	 * @param		array $records_list
	 * @tutorial	Here $records_list contains set of rows data that have to insert
	 *           	all the values will be passed as in documentation
	 */
	function insApplicantData($QI) {
		
		$ins_applicant_data   =   "INSERT INTO ApplicantData(OrgID, ApplicationID, QuestionID, Answer)";
		$ins_applicant_data   .=  " VALUES (:OrgID, :ApplicationID, :QuestionID, :Answer)";
		$res_applicant_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_applicant_data, $QI );
		
		return $res_applicant_data;
	}
	
	/**
	 * @method		insUpdApplicantData
	 */
	function insUpdApplicantData($QI) {

		$params_info      =   array(":OrgID"=>$QI['OrgID'], ":ApplicationID"=>$QI['ApplicationID'], ":QuestionID"=>$QI['QuestionID'], ":IAnswer"=>$QI['Answer'], ":UAnswer"=>$QI['Answer']);
		$ins_form_data    =   "INSERT INTO ApplicantData(OrgID, ApplicationID, QuestionID, Answer)";
		$ins_form_data    .=  " VALUES(:OrgID, :ApplicationID, :QuestionID, :IAnswer)";
		$ins_form_data    .=  " ON DUPLICATE KEY UPDATE Answer = :UAnswer";
		$res_form_data    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_data, array($params_info) );
		
		return $res_form_data;
	}
	
	/**
	 * @method		getApplicants
	 * @param		$POST
	 * @return		array @type	json
	 */
	function getApplicants($POST = '') {
		
		global $USERID, $USERROLE, $OrgID;
		
		// Set parameters for prepared query
		$params = array (":OrgID"=>$OrgID);
		
		/**
		 * @input json data
		 */
		$seljobapplicants = "SELECT J.ApplicationID, J.OrgID, J.RequestID, J.EntryDate 
							 FROM JobApplications AS J
					 		 WHERE J.OrgID = :OrgID";


		if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params[":ROrgID"] = $OrgID;
			$params[":UserID"] = $USERID;
			$seljobapplicants .= " AND J.RequestID in (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ROrgID AND UserID = :UserID)";
		} // end hiring manager
		
		$seljobapplicants .= " ORDER BY J.EntryDate DESC LIMIT 5";
		
		$resjobapplicants = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $seljobapplicants, array (
				$params 
		) );
		
		$i = 0;
		foreach ( $resjobapplicants ['results'] as $row ) {
			
			// Set parameters for prepared query
			$params = array (":OrgID"=>$OrgID, ":ApplicationID"=>$row['ApplicationID']);
			
			$selapplicantdata = "SELECT A.* 
								FROM ApplicantData A 
								WHERE A.OrgID = :OrgID AND A.ApplicationID = :ApplicationID 
								AND (QuestionID = 'first' or QuestionID = 'last' or QuestionID = 'email')";
			$resapplicantdata = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $selapplicantdata, array (
					$params 
			) );
			
			foreach ( $resapplicantdata ['results'] as $rowappdata ) {
				if ($rowappdata ['QuestionID'] == 'first')
					$row ['FirstName'] = $rowappdata ['Answer'];
				if ($rowappdata ['QuestionID'] == 'last')
					$row ['LastName'] = $rowappdata ['Answer'];
				if ($rowappdata ['QuestionID'] == 'email')
					$row ['Email'] = $rowappdata ['Answer'];
				$i ++;
			}
			
			$rowapplicants [] = $row;
		}
		
		// If no records return empty string
		if ($i == 0)
			return '';
		else
			return json_encode ( $rowapplicants );
	}
	
	/**
	 * @method	 	getAppData
	 * @param 		$OrgID, $ApplicationID        	
	 * @return 		Associative array
	 */
	function getAppData($OrgID, $ApplicationID) {

		if($ApplicationID == "" || $OrgID == "") return array();
		
		// Set parameters for prepared query
		$params               =   array (":OrgID" => $OrgID, ":ApplicationID" => $ApplicationID);
		$sel_applicant_data   =   "SELECT QuestionID, Answer FROM ApplicantData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
		$res_applicant_data   =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, array ($params) );
		$applicant_info_count =   $res_applicant_data ['count'];
		$applicant_info       =   $res_applicant_data ['results'];
		
		for($i = 0; $i < $applicant_info_count; $i ++) {
		    //Set the value as empty if it is null.
		    if(is_null($applicant_info [$i] ['Answer'])) $applicant_info [$i] ['Answer'] = "";
			$AppData [$applicant_info [$i] ['QuestionID']] = $applicant_info [$i] ['Answer'];
		}
		
		return $AppData;
	} // end function

	/**
	 * @method		displayStatus
	 * @param		$OrgID, $ProcessOrder        	
	 * @return		Associative array
	 */
	function displayStatus($OrgID, $ProcessOrder) {
		
		// Set parameters for prepared query
		$params = array (":OrgID" => $OrgID, ":ProcessOrder" => $ProcessOrder);
		
		$sel_description = "SELECT Description FROM ApplicantProcessFlow WHERE OrgID = :OrgID AND ProcessOrder = :ProcessOrder";
		list ( $Description ) = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_description, array ($params) );
		
		return $Description;
	} // end function
	
	/**
	 * @method		insApplicantDispositionCodesInfo
	 * @param		array $info
	 * @return		array
	 */
	function insApplicantsDispositionCodesInfo($info) {
		
		$ins_app_disposition_codes = $this->db->buildInsertStatement('ApplicantDispositionCodes', $info);
		$res_app_disposition_codes = $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_disposition_codes["stmt"], $ins_app_disposition_codes["info"] );
	
		return $res_app_disposition_codes;
	}
	
	/**
	 * @method		delApplicantDispositionCodes
	 * @param		$OrgID
	 * @return		string
	 */
	function delApplicantsDispositionCodesInfo($OrgID) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID);
	
		$del_app_disposition_codes = "DELETE FROM ApplicantDispositionCodes WHERE OrgID = :OrgID";
		$res_app_disposition_codes = $this->db->getConnection ( $this->conn_string )->delete ( $del_app_disposition_codes, array($params) );
	
		return $res_app_disposition_codes;
	}
	
	/**
	 * @method		getApplicantInfo
	 * @param		$OrgID, $ApplicationID, $RequestID
	 * @return		string
	 */
	function getApplicantInfo($OrgID, $ApplicationID, $RequestID) {
		
		// Set parameters for prepared query
		$params = array (":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
		$sel_application_info = "SELECT QuestionID, Answer FROM ApplicantData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND QuestionID IN ('first','last','city','state', 'province','zip','country')";
		$res_application_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_application_info, array (
				$params 
		) );
		
		$application_info_count = $res_application_info ['count'];
		$AD = $res_application_info ['results'];
		
		for($i = 0; $i < $application_info_count; $i ++) {
			if ($AD [$i] ['QuestionID'] == 'first') {
				$firstname = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'last') {
				$lastname = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'city') {
				$city = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'state') {
				$state = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'province') {
			    $province = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'county') {
				$county = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'zip') {
				$zip = $AD [$i] ['Answer'];
			}
			if ($AD [$i] ['QuestionID'] == 'country') {
				$country = $AD [$i] ['Answer'];
			}
		}
		
        //Swap state variable with province, if country is Canada
		$multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
		$locaddr  =   G::Obj('Address')->formatAddress ( $OrgID, $country, $city, $state, $province, $zip, $county );
		$jobtitle =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
		
		$name     =   $firstname . ' ' . $lastname . ', ' . $locaddr . '&nbsp;-&nbsp;Applied for: ' . $jobtitle;
		
		return $name;
	} // end function
	
	/**
	 * @method		getApplicantDispositionCodes
	 * @param		$OrgID
	 */
	function getApplicantDispositionCodes($OrgID, $Active="") {
		
		// Set parameters for prepared query
		$params = array (":OrgID" => $OrgID);
		if ($Active != "") {
		  $params[":Active"] = $Active;
		}
		
		$sel_applicant_disp_codes = "SELECT `Code`, `Description`, `Active` FROM ApplicantDispositionCodes WHERE OrgID = :OrgID";
		if ($Active != "") {
		  $sel_applicant_disp_codes .= " AND Active = :Active";
		}
	        $sel_applicant_disp_codes .= " ORDER BY Description";
		$res_applicant_disp_codes = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_disp_codes, array (
				$params 
		) );
		
		return $res_applicant_disp_codes;
	}
	
	/**
	 * @method		getApplicantProcessFlowInfo
	 * @param 		mixed	$columns
	 * @param 		array	$where_info
	 * @param 		string	$order_by
	 * @param 		array	$info
	 * @return 		array
	 */
	function getApplicantProcessFlowInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_applicant_process_flow = "SELECT $columns FROM ApplicantProcessFlow";
		
		if (count ( $where_info ) > 0) {
			$sel_applicant_process_flow .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		if ($order_by != "")
			$sel_applicant_process_flow .= " ORDER BY " . $order_by;
		
		$res_applicant_process_flow = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_process_flow, $info );
		
		return $res_applicant_process_flow;
	}
	
	/**
	 * @method		delApplicantProcessFlow
	 * @param		$where_info = array(), $info = array()
	 */
	function delApplicantProcessFlow($where_info = array(), $info = array()) {
		
		$del_applicant_process = "DELETE FROM ApplicantProcessFlow";
	
		if (count ( $where_info ) > 0) {
			$del_applicant_process .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		$res_applicant_process = $this->db->getConnection ( $this->conn_string )->delete ( $del_applicant_process, $info );
	
		return $res_applicant_process;
	}
	
	/**
	 * @method 		insApplicantProcessFlow
	 */
	function insApplicantProcessFlow($insert_info, $on_update = '', $update_info = array()) {
		
		// Set parameters for prepared query
		$ins_form_data = $this->db->buildInsertStatement ( 'ApplicantProcessFlow', $insert_info );
	
		$insert_statement = $ins_form_data ["stmt"];
		if ($on_update != '') {
			$insert_statement .= $on_update;
			if (is_array ( $update_info )) {
				foreach ( $update_info as $upd_key => $upd_value ) {
					$ins_form_data ["info"][0][$upd_key] = $upd_value;
				}
			}
		}
	
		$res_form_data = $this->db->getConnection ( $this->conn_string )->insert ( $insert_statement, $ins_form_data ["info"] );
		return $res_form_data;
	}
	
	
	#########################################################################################
	############## ApplicantLock
	#########################################################################################
	
	/**
	 * @method		insApplicantLock
	 * @param		$info
	 */
	function insApplicantLock($info) {
		
		$insert_app_lock = $this->db->buildInsertStatement ( 'ApplicantLock', $info );
		$result_app_lock = $this->db->getConnection ( $this->conn_string )->insert ( $insert_app_lock ['stmt'], $insert_app_lock ['info'] );
	}
	
	/**
	 * @method		getApplicantLockInfo
	 * @param		$columns, $where_info = array(), $order_by = '', $info = array()
	 */
	function getApplicantLockInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_applicant_lock = "SELECT $columns FROM ApplicantLock";
	
		if (count ( $where_info ) > 0) {
			$sel_applicant_lock .= " WHERE " . implode ( " AND ", $where_info );
		}
	
		if ($order_by != "") $sel_applicant_lock .= " ORDER BY " . $order_by;
	
		$res_applicant_lock = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_lock, $info );
	
		return $res_applicant_lock;
	}
	
	/**
	 * @method		delApplicantLock
	 * @param		$where_info = array(), $info = array()
	 */
	function delApplicantLock($where_info = array(), $info = array()) {
		
		$del_applicant_lock = "DELETE FROM ApplicantLock";
		
		if (count ( $where_info ) > 0) {
			$del_applicant_lock .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		$res_applicant_lock = $this->db->getConnection ( $this->conn_string )->delete ( $del_applicant_lock, $info );
		
		return $res_applicant_lock;
	}
	
	/**
	 * @method     getUserPortalApplicationID
	 * @param      string $OrgID
	 * @param      string $UserID
	 * @param      string $RequestID
	 */
	function getUserPortalApplicationID($OrgID, $UserID, $RequestID) {
	    
	    $where         =   array("OrgID = :OrgID", "RequestID = :RequestID");
	    $params        =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	    $job_app_res   =   G::Obj('Applications')->getJobApplicationsInfo("ApplicationID", $where, '', '', array($params));
	    $job_app_list  =   $job_app_res['results'];
	    $job_app_cnt   =   $job_app_res['count'];
	    
	    for($ja = 0; $ja < $job_app_cnt; $ja++) {
	        $ApplicationID     =   "";
	        $PortalUserID      =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $job_app_list[$ja]['ApplicationID'], 'PortalUserID');
	        
	        if($PortalUserID == $UserID) {
	            $ApplicationID =   $job_app_list[$ja]['ApplicationID'];
	            $ja            =   $job_app_cnt;   //It will break the loop
	        } else {
	            $email         =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $job_app_list[$ja]['ApplicationID'], 'email');
	            $user_info     =   G::Obj('UserPortalUsers')->getUserDetailInfoByEmail("UserID", $email);
	            if($user_info['UserID'] == $UserID)
	            {
	                $ApplicationID =   $job_app_list[$ja]['ApplicationID'];
	                $ja            =   $job_app_cnt;   //It will break the loop
	            }
	        }
	    }
	    
	    return $ApplicationID;
	}
}
