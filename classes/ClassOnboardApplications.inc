<?php
/**
 * @class		OnboardApplications
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class OnboardApplications {
	
	var $conn_string       =   "IRECRUIT";
		
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	
	/**
	 * @method		getOnboardApplications
	 * @return		array
	 */
	function getOnboardApplications($columns = "*", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_onboard_applications = "SELECT $columns FROM OnboardApplications";

		if(count($where_info) > 0) {
			$sel_onboard_applications .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($group_by != "") $sel_onboard_applications .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_onboard_applications .= " ORDER BY " . $order_by;
		
		$res_onboard_applications = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_onboard_applications, $info );
		
		return $res_onboard_applications;
	}	
	

	/**
	 * @method		getOnboardApplicationDetails
	 * @return		array
	 */
	function getOnboardApplicationDetails($OrgID, $ApplicationID, $RequestID, $OnboardFormID) {
	
	    $params_info           =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":OnboardFormID"=>$OnboardFormID);
	    $sel_onboard_app_info  =   "SELECT * FROM OnboardApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND OnboardFormID = :OnboardFormID";
	    $res_onboard_app_info  =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_onboard_app_info, array($params_info) );
	
	    return $res_onboard_app_info;
	}
	
	
	/**
	 * @method		insOnboardApplication
	 * @param		$onboard_application_info
	 * @return		array
	 */
	function insOnboardApplication($onboard_application_info, $on_update = '', $update_info = array()) {
	
	    $ins_onboard_applications  =   $this->db->buildInsertStatement('OnboardApplications', $onboard_application_info);
	    $insert_statement          =   $ins_onboard_applications["stmt"];

	    if ($on_update != '') {
	        $insert_statement .= $on_update;
	        if (is_array ( $update_info )) {
	            foreach ( $update_info as $upd_key => $upd_value ) {
	                $ins_onboard_applications["info"][0][$upd_key] = $upd_value;
	            }
	        }
	    }
	    
	    $res_onboard_applications  =   $this->db->getConnection ( $this->conn_string )->insert ( $insert_statement, $ins_onboard_applications["info"] );
	
	    return $res_onboard_applications;
	}
	
	
	/**
	 * @method	updOnboardApplicationsInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updOnboardApplicationsInfo($set_info = array(), $where_info = array(), $info) {
	
	    $upd_applications_info = $this->db->buildUpdateStatement("OnboardApplications", $set_info, $where_info);
	    $res_applications_info = $this->db->getConnection( $this->conn_string )->update($upd_applications_info, $info);
	
	    return $res_applications_info;
	}
	
}
