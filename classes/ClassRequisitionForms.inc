<?php
/**
 * @class		RequisitionForms
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionForms {

    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    /**
     * @method		insRequisitionForms
     * @param		$fq_info
     */
    public function insRequisitionForms($form_info) {
        
        $params_info    =   array(":OrgID"=>$form_info['OrgID']);
        $sel_forms_cnt  =   "SELECT COUNT(*) AS FormsCount FROM RequisitionForms WHERE OrgID = :OrgID GROUP BY OrgID";
        $res_forms_cnt  =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_forms_cnt, array($params_info) );
        $forms_count    =   $res_forms_cnt['FormsCount'] + 1;
        
        $form_info['RequisitionFormName'] = "Version " . $forms_count;
        $ins_form_ques = $this->db->buildInsertStatement("RequisitionForms", $form_info);
        $res_form_ques = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_form_ques["stmt"], $ins_form_ques["info"] );
    
        return $res_form_ques;
    }

    /**
     * @method      getRequisitionForms
     */
    function getRequisitionForms($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_forms = "SELECT $columns FROM RequisitionForms";
    
        if(count($where_info) > 0) {
            $sel_forms .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($group_by != "") $sel_forms .= " GROUP BY " . $group_by;
        if($order_by != "") $sel_forms .= " ORDER BY " . $order_by;
    
        $res_forms = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_forms, $info );
    
        return $res_forms;
    }
    
    /**
     * @method		getDefaultRequisitionFormID
     * @param		$OrgID
     */
    public function getDefaultRequisitionFormID($OrgID) {
    
        $params_info    =   array(":OrgID"=>$OrgID);
        $sel_form_ques  =   "SELECT RequisitionFormID FROM RequisitionForms WHERE OrgID = :OrgID AND FormDefault = 'Y'";
        $res_form_ques  =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_form_ques, array($params_info) );
    
        return $res_form_ques['RequisitionFormID'];
    }
    
    /**
     * @method		getDefaultRequisitionFormInfo
     * @param		$OrgID
     */
    public function getDefaultRequisitionFormInfo($OrgID) {
    
        $params_info    =   array(":OrgID"=>$OrgID);
        $sel_form_ques  =   "SELECT * FROM RequisitionForms WHERE OrgID = :OrgID AND FormDefault = 'Y'";
        $res_form_ques  =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_form_ques, array($params_info) );
    
        return $res_form_ques;
    }
    
    /**
     * @method		setFormDefault
     * @param		$OrgID
     * @param		$RequisitionFormID
     */
    public function setFormDefault($OrgID, $RequisitionFormID) {
    
        $params_info    =   array(":OrgID"=>$OrgID);
        $upd_form_def   =   "UPDATE RequisitionForms SET FormDefault = '' WHERE OrgID = :OrgID";
        $res_form_def   =   $this->db->getConnection ( "IRECRUIT" )->update ( $upd_form_def, array($params_info) );
        
        $params_info    =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $upd_form_def   =   "UPDATE RequisitionForms SET FormDefault = 'Y' WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID";
        $res_form_def   =   $this->db->getConnection ( "IRECRUIT" )->update ( $upd_form_def, array($params_info) );
    
        return $res_form_def;
    }
    
}
?>
