<?php
/**
 * @class		OnboardFormData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class OnboardFormData {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    
    /**
     * @method		getOnboardDataApplicationIDs
     * @tutorial    It is a temporary function, we will have to remove it after we have done the upgrade
     */
    function getOnboardDataApplicationIDs($OrgID) {
    
        $params_info   =   array(":OrgID"=>$OrgID);
        $sel_form_data =   "SELECT ApplicationID, RequestID FROM OnboardData WHERE OrgID = :OrgID GROUP BY ApplicationID, RequestID";
        $res_form_data =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
        $form_data_res =   $res_form_data['results'];
        $form_data_cnt =   $res_form_data['count'];
         
        $app_ids       =   array();
        for($fd = 0; $fd < $form_data_cnt; $fd++) {
            $app_ids[$fd]['ApplicationID'] =   $form_data_res[$fd]['ApplicationID'];
            $app_ids[$fd]['RequestID']     =   $form_data_res[$fd]['RequestID'];
        }
         
        return $app_ids;
    }
    
    
    /**
	 * @method		insUpdOnboardFormData
	 */
	function insUpdOnboardFormData($QI) {

		$params_info    = array(":OrgID"=>$QI['OrgID'], ":ApplicationID"=>$QI['ApplicationID'], ":RequestID"=>$QI['RequestID'], ":OnboardFormID"=>$QI['OnboardFormID'], ":QuestionID"=>$QI['QuestionID'], ":QuestionOrder"=>$QI['QuestionOrder'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Question"=>$QI['Question'], ":Answer"=>$QI['Answer'], ":value"=>$QI['value'], ":AnswerStatus"=>1, ":UQuestion"=>$QI['Question'], ":UQuestionTypeID"=>$QI['QuestionTypeID'], ":UAnswer"=>$QI['Answer'], ":UAnswerStatus"=>$QI['AnswerStatus'], ":Uvalue"=>$QI['value']);
		$ins_form_data  = "INSERT INTO OnboardData(OrgID, ApplicationID, RequestID, OnboardFormID, QuestionID, QuestionOrder, QuestionTypeID, Question, Answer, value, AnswerStatus)";
		$ins_form_data .= " VALUES(:OrgID, :ApplicationID, :RequestID, :OnboardFormID, :QuestionID, :QuestionOrder, :QuestionTypeID, :Question, :Answer, :value, :AnswerStatus)";
		$ins_form_data .= " ON DUPLICATE KEY UPDATE Question = :UQuestion, QuestionTypeID = :UQuestionTypeID, value = :Uvalue, Answer = :UAnswer, AnswerStatus = :UAnswerStatus";
		$res_form_data  = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_data, array($params_info) );
		
		return $res_form_data;
	}
	
	
	/**
	 * @method     getOnboardFormData
	 * @param      string $OrgID
	 * @param      string $OnboardFormID
	 * @param      string $ApplicationID
     * @param      string $RequestID
	 * @return     multitype:array
	 */
	function getOnboardFormData($OrgID, $ApplicationID, $RequestID, $OnboardFormID) {
	
	    $columns           =   $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $params_info       =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":OnboardFormID"=>$OnboardFormID);
	    $sel_onboard_ques  =   "SELECT * FROM OnboardData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND OnboardFormID = :OnboardFormID ORDER BY QuestionOrder ASC";
	    $res_onboard_ques  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_onboard_ques, array($params_info) );
	    $res_onboard_cnt   =   $res_onboard_ques['count'];
	    $res_onboard_res   =   $res_onboard_ques['results'];
	
	    $onboard_data      =   array();
	    for($w = 0; $w < $res_onboard_cnt; $w++) {
	        $onboard_data[$res_onboard_res[$w]['QuestionID']]   =   $res_onboard_res[$w]['Answer'];
	    }
	
	    return $onboard_data;
	}
	
	
	/**
	 * @method		getApplicantOnboardAnswer
	 * @param		string $OrgID, $OnboardFormID, $ApplicationID, $QuestionID
	 * @return		string
	 */
	function getApplicantOnboardAnswer($OrgID, $OnboardFormID, $ApplicationID, $QuestionID) {
	
	    //Set parameters for prepared query
	    $params     =  array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>$QuestionID);
	    //Get Title
	    $sel_info   =  "SELECT Answer FROM OnboardData WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND ApplicationID = :ApplicationID AND QuestionID = :QuestionID";
	    $res_info   =  $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
	
	    $column	    =  is_null($res_info['Answer']) ? '' : $res_info['Answer'];
	
	    return $column;
	} // end function

	/**
	 * @method		checkDuplicateOnboardAnswer
	 * @param		string $OrgID, $OnboardFormID, $QuestionID, $Answer
	 * @return		string
	 */
	function checkDuplicateOnboardAnswer($OrgID, $OnboardFormID, $QuestionID, $Answer) {
	
	    //Set parameters for prepared query
	    $params     =  array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID, ":QuestionID"=>$QuestionID, ":Answer"=>$Answer);
	    //Get Title
	    $sel_info   =  "SELECT count(*) AS CNT FROM OnboardData WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND QuestionID = :QuestionID AND Answer = :Answer";
	    $res_info   =  $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
	
	    return $res_info['CNT'];

	} // end function
	
	
	/**
	 * @method     getOnboardApplicantsList
	 */
	function getOnboardApplicantsList($OrgID, $USERROLE, $USERID, $FinalDate, $BeginDate, $OnboardFormID) {
	    
	    $rolerestrict = '';

	    if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	        //Set parameters for prepared query
	        $params = array(":OrgID"=>$OrgID, ":ReqManOrgID"=>$OrgID, ":ReqManUserID"=>$USERID);
	         
	        $sel_requestids  = "SELECT RequestID FROM Requisitions WHERE OrgID = :OrgID";
	        $sel_requestids .= " AND RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManOrgID AND UserID = :ReqManUserID)";
	        $res_requestids  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_requestids, array($params));
	         
	        foreach ($res_requestids['results'] as $REQS) {
	            $restrictrequest .= $REQS['RequestID'] . "', '";
	        }
	         
	        if ($restrictrequest != "") {
	            $restrictrequest = substr ( $restrictrequest, 0, - 4 );
	            $rolerestrict = " AND OA.RequestID IN ('$restrictrequest')";
	        } else {
	            $rolerestrict = " AND OA.RequestID IN ('X')";
	        }
	    }
	    
	    //Set parameters for prepared query
	    $params     =  array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID);
	    //Get Title
	    $sel_info   =  "SELECT * FROM OnboardApplications OA"; 
        $sel_info  .=  " WHERE OA.OrgID = :OrgID";
        $sel_info  .=  " AND OA.OnboardFormID = :OnboardFormID";
        $sel_info  .=  " AND OA.ProcessDate < '$FinalDate'";
        $sel_info  .=  " AND OA.ProcessDate > '$BeginDate'";
        $sel_info  .=   $rolerestrict;
        $sel_info  .=  " GROUP BY OA.OrgID, OA.ApplicationID, OA.OnboardFormID";
        $sel_info  .=  " ORDER BY OA.ProcessDate DESC";
        
	    $res_info   =  $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_info, array($params));
	    
	    return $res_info;
	}
}
?>
