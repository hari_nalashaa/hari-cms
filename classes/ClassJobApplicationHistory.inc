<?php
/**
 * @class		JobApplicationHistory
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class JobApplicationHistory {
	
	var $conn_string       =   "IRECRUIT";
		
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		insJobApplicationHistory
	 * @param 		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @return		array
	 */
	function insJobApplicationHistory($job_app_history) {
	
	    $ins_job_app_history = $this->db->buildInsertStatement('JobApplicationHistory', $job_app_history);
	    $res_job_app_history = $this->db->getConnection ( $this->conn_string )->insert ( $ins_job_app_history["stmt"], $ins_job_app_history["info"] );
	
	    return $res_job_app_history;
	}
	
	/**
	 * @method		getJobApplicationHistoryInfo
	 */
	function getJobApplicationHistoryInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	    $sel_applications = "SELECT $columns FROM JobApplicationHistory";
	
	    if(count($where_info) > 0) $sel_applications .= " WHERE " . implode(" AND ", $where_info);
	
	    if($group_by != "") $sel_applications .= " GROUP BY " . $group_by;
	    if($order_by != "") $sel_applications .= " ORDER BY " . $order_by;
	
	    $res_applications = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applications, $info );
	
	    return $res_applications;
	}
		
	/**
	 * @method	insJobApplicationHistoryArchive
	 * @return	array
	 */
	function insJobApplicationHistoryArchive($info = array()) {
	
	    $ins_info = "INSERT INTO JobApplicationHistoryArchive
					SELECT * from JobApplicationHistory
					WHERE OrgID = :OrgID
					AND ApplicationID = :ApplicationID
					AND RequestID = :RequestID
					AND UpdateID = :UpdateID";
	    $res_info = $this->db->getConnection( $this->conn_string )->insert ( $ins_info, $info );
	
	    return $res_info;
	}
	
}
