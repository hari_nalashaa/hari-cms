<?php
/**
 * @class       OnboardSettings
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class OnboardSettings {
		
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	/**
	 * @method		getOnboardSettingsInfoByOrgID
	 */
	function getOnboardSettingsInfoByOrgID($OrgID) {
	
		$params		=	array(":OrgID"=>$OrgID);
		$sel_info	=	"SELECT * FROM OnboardSettings WHERE OrgID = :OrgID";
		$res_info	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
	
		return $res_info;
	}
	
	/**
     * @method		insUpdOnboardSettings
     * @param		$info
     * @return		array
     */
    function insUpdOnboardSettings($info, $skip = array()) {
    
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("OnboardSettings", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
    
        return $res_org;
    }
}
?>
