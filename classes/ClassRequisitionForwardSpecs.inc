<?php
/**
 * @class		RequisitionForwardSpecs
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionForwardSpecs {

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	
	/**
	 * @method	getRequisitionForwardSpecs
	 * @return	array
	 */
	function getRequisitionForwardSpecsInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns              =   $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_requisition_info =   "SELECT $columns FROM RequisitionForwardSpecs";

		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	
	/**
	 * @method	getRequisitionForwardSpecsDetailInfo
	 * @return	array
	 */
	function getRequisitionForwardSpecsDetailInfo($OrgID, $RequestID) {
	
	    $params_info   =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	    $sel_info      =   "SELECT * FROM RequisitionForwardSpecs WHERE OrgID = :OrgID AND RequestID = :RequestID";
	    $row_info      =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params_info) );
	
	    return $row_info;
	}
	
	/**
	 * @method	delRequisitionForwardSpecsInfo
	 * @return	array
	 */
	function delRequisitionForwardSpecsInfo($where_info = array(), $info = array(), $limit = "") {
		
		$del_req_info = "DELETE FROM RequisitionForwardSpecs";
		
		if(count($where_info) > 0) {
			$del_req_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($limit != "") {
			$del_req_info .= " LIMIT $limit";
		}
		
		$res_req_info = $this->db->getConnection( $this->conn_string )->delete ( $del_req_info, $info );
	
		return $res_req_info;
	}

	
	/**
	 * @method	insRequisitionForwardSpecsInfo
	 * @param	$info
	 */
	function insRequisitionForwardSpecsInfo($req_info) {
	
	    $ins_requisitions  =    $this->db->buildInsertStatement("RequisitionForwardSpecs", $req_info);
	    $res_requisitions  =    $this->db->getConnection ( $this->conn_string )->insert ( $ins_requisitions["stmt"], $ins_requisitions["info"] );
	
	    return $res_requisitions;
	}	
	
	
	/**
	 * @method	updRequisitionForwardSpecsInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updRequisitionForwardSpecsInfo($set_info = array(), $where_info = array(), $info) {
	
	    $upd_req_info = $this->db->buildUpdateStatement("RequisitionForwardSpecs", $set_info, $where_info);
	    $res_req_info = $this->db->getConnection( $this->conn_string )->update($upd_req_info, $info);
	
	    return $res_req_info;
	}
}
?>
