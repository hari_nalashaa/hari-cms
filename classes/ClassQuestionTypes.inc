<?php
/**
 * @class       QuestionTypes
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class QuestionTypes {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method     getValueList
     * @param      string  $OrgID
     * @param      string  $QuestionID
     * @param      string  $Answer
     */
    function getValueList($value) {
    
        $values_list    =   array();
        $que_values     =   explode ( '::', $value );
    
        foreach ( $que_values as $v => $q ) {
            list ( $vv, $qq )   =   explode ( ":", $q);
            $values_list[$vv]   =   $qq;
        }
    
        return $values_list;
    }
    
    /**
     * @method		getQuestionTypes
     * @param		$que_type_id
     */
    public function getQuestionTypeInfo($que_type_id) {
        
        $params = array(':QuestionTypeID'=>$que_type_id);
        
        $sel_default_values = "SELECT * FROM QuestionTypes WHERE QuestionTypeID = :QuestionTypeID";
        $res_default_values = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_default_values, array($params));
        return $res_default_values;
    }
    
    /**
     * @method		getQuestionTypesInfo
     * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
     */
    function getQuestionTypesInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $sel_que_types_info = "SELECT $columns FROM QuestionTypes";
        
        if(count($where_info) > 0) $sel_que_types_info .= " WHERE " . implode(" AND ", $where_info);
        if($order_by != "") $sel_que_types_info .= " ORDER BY " . $order_by;
        
        $res_que_types_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_que_types_info, $info );
        
        return $res_que_types_info;
    }
    
}
?>
