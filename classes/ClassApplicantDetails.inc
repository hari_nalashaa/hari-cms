<?php
/**
 * @class		ApplicantDetails
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantDetails {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getApplicantSortName
     * @param		string $OrgID, $ApplicationID, $RequestID
     * @return		string
     */
    function getApplicantSortName($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT ApplicantSortName FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['ApplicantSortName'] != '') ? $res_info['ApplicantSortName'] : '';
    
        return $column;
    } // end function
    
    /**
     * @method		getFormID
     * @param		string $OrgID, $ApplicationID, $RequestID
     * @return		string
     */
    function getFormID($OrgID, $ApplicationID, $RequestID) {
        
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get FormID
        $sel_info   =   "SELECT FormID FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));

        if(!isset($res_info['FormID'])) $res_info['FormID'] = "STANDARD";
        
        // validate the FormID
        $FormID     =   $res_info['FormID'];
        //Bind parameters
        $params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
        //Set condition
        $where      =   array("OrgID = :OrgID", "FormID = :FormID");
        //Get Distinct FormID's count
        $results    =   G::Obj('FormQuestions')->getFormQuestionsInformation(array("DISTINCT(FormID) AS DFormID"), $where, "", array($params));
        $hit        =   $results['count'];
        
        if ($hit < 1) {
            $FormID = 'STANDARD';
        }

        return $FormID;
    } // end function
    
    /**
     * @method 		getApplicantName
     * @param 		$OrgID, $ApplicationID
     * @return 		string
     */
    function getApplicantName($OrgID, $ApplicationID) {
    
        // Set parameters for prepared query
        $params                 =   array (":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
        $sel_application_info   =   "SELECT QuestionID, Answer FROM ApplicantData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND QuestionID IN ('first','middle','last')";
        $res_application_info   =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_application_info, array ($params) );
    
        $application_info       =   $res_application_info ['results'];
    
        foreach ( $application_info as $AD ) {
            if ($AD ['QuestionID'] == "first") {
                $first = $AD ['Answer'];
            }
            if ($AD ['QuestionID'] == "middle") {
                $middle = $AD ['Answer'];
            }
            if ($AD ['QuestionID'] == "last") {
                $last = $AD ['Answer'];
            }
        }
    
        $name = '';
        if ($first != "") {
            $name .= $first;
        }
        if ($middle != "") {
            $name .= ' ' . $middle;
        }
        if ($last != "") {
            $name .= ' ' . $last;
        }
    
        return $name;
    } // end function
    
    
    /**
     * @method		getProcessOrder
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getProcessOrder($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     =	array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   =	"SELECT ProcessOrder FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   =	$this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['ProcessOrder'] != '') ? $res_info['ProcessOrder'] : '';
    
        return $column;
    } // end function
    
    
    /**
     * @method		getApplicantConsiderationStatus1
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getApplicantConsiderationStatus1($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT ApplicantConsiderationStatus1 FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['ApplicantConsiderationStatus1'] != '') ? $res_info['ApplicantConsiderationStatus1'] : '';
    
        return $column;
    } // end function
    
    
    /**
     * @method		getApplicantConsiderationStatus2
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getApplicantConsiderationStatus2($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT ApplicantConsiderationStatus2 FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['ApplicantConsiderationStatus2'] != '') ? $res_info['ApplicantConsiderationStatus2'] : '';
    
        return $column;
    } // end function
    
    /**
     * @method		getApplicantConsiderationStatus3
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getApplicantConsiderationStatus3($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT ApplicantConsiderationStatus3 FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['ApplicantConsiderationStatus3'] != '') ? $res_info['ApplicantConsiderationStatus3'] : '';
    
        return $column;
    } // end function

    /**
     * @method		getRating
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getRating($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT Rating FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['Rating'] != '') ? $res_info['Rating'] : '';
    
        return $column;
    } // end function
    
    /**
     * @method		getInformed
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getInformed($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT Informed FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['Informed'] != '') ? $res_info['Informed'] : '';
    
        return $column;
    } // end function
    
    
    /**
     * @method		getLeadGeneratorInfo
     * @param		string $OrgID, $ApplicationID, $RequestID
     * @return		string
     */
    function getLeadGeneratorInfo($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params         =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info       =   "SELECT LeadGenerator FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info       =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
        $lead_gen_info  =   $res_info['LeadGenerator'];
    
        return $lead_gen_info;
    } // end function
    
    /**
     * @method		getAnswer
     * @param		string $OrgID, $ApplicationID, $QuestionID
     * @return		string
     */
    function getAnswer($OrgID, $ApplicationID, $QuestionID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>$QuestionID);
        //Get Title
        $sel_info   = "SELECT Answer FROM ApplicantData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND QuestionID = :QuestionID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
	$column     = ($res_info['Answer'] != '') ? $res_info['Answer'] : '';
    
        return $column;
    } // end function
    
    /**
     * @method		getProcessOrderDescription
     * @param		$OrgID, $ProcessOrder
     * @return		string
     */
    function getProcessOrderDescription($OrgID, $ProcessOrder) {
    
        // Set parameters for prepared query
        $params = array (
            ":OrgID"        => $OrgID,
            ":ProcessOrder" => $ProcessOrder
        );
    
        if ($ProcessOrder == 0) {
            $ProcessFlowDescription = 'Comment';
        } else if ($ProcessOrder == - 1) {
            $ProcessFlowDescription = 'Email Forward';
        } else if ($ProcessOrder == - 2) {
            $ProcessFlowDescription = 'Onboard Applicant';
        } else if ($ProcessOrder == - 3) {
            $ProcessFlowDescription = 'Data Update';
        } else if ($ProcessOrder == - 4) {
            $ProcessFlowDescription = 'Correspondence';
        } else if ($ProcessOrder == - 5) {
            $ProcessFlowDescription = 'Download';
        } else if ($ProcessOrder == - 6) {
            $ProcessFlowDescription = 'Reminder';
        } else {
            	
            $sel_description = "SELECT Description FROM ApplicantProcessFlow WHERE OrgID = :OrgID AND ProcessOrder = :ProcessOrder";
            $res_description = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_description, array (
                $params
            ) );
            $ProcessFlowDescription = $res_description [0];
        }
    
        return $ProcessFlowDescription;
    } // end function
    
    /**
     * @method		getDispositionCodeDescription
     * @param 		$OrgID, $DispositionCode
     * @return 		string
     */
    function getDispositionCodeDescription($OrgID, $DispositionCode) {
    
        // Set parameters for prepared query
        $params = array (
            ":OrgID"    => $OrgID,
            ":Code"     => $DispositionCode
        );
    
        $sel_description = "SELECT Description FROM ApplicantDispositionCodes WHERE OrgID = :OrgID AND Code = :Code";
        $res_description = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_description, array (
            $params
        ) );
        $DispositionCodeDescription = $res_description [0];
    
        return $DispositionCodeDescription;
    } // end function
    
    
    /**
     * @method		getMultiOrgID
     * @param		string $OrgID, $ApplicationID, $RequestID
     * @return		string
     */
    function getMultiOrgID($OrgID, $ApplicationID, $RequestID) {

        global $ApplicationsObj;
    
        //Get JobApplications Information
        $where 		= array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
        $params 	= array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        $results 	= $ApplicationsObj->getJobApplicationsInfo('MultiOrgID', $where, '', '', array($params));
        $MultiOrgID = $results['results'][0]['MultiOrgID'];

	$MultiOrgID = ($MultiOrgID != '') ? $MultiOrgID : '';
        
        return $MultiOrgID;
    } // end function
    
    
    /**
     * @method      getLeadStatus
     * @param       string $OrgID, $ApplicationID, $RequestID
     * @return		string
     */
    function getLeadStatus($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params         =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info       =   "SELECT LeadStatus FROM JobApplications WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_info       =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
        $lead_status    =   $res_info['LeadStatus'];
    
        return $lead_status;
    }
    
    
    /**
     * @method      updApplicantAnswer
     * @param       string $OrgID, $ApplicationID, $RequestID
     * @return      string
     */
    function updApplicantAnswer($OrgID, $ApplicationID, $QuestionID, $Answer) {
        
        //Insert Information
        $insert_info =  array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "QuestionID"=>$QuestionID, "Answer"=>$Answer);
        //On Update
        $on_update   =  " ON DUPLICATE KEY UPDATE Answer = :UAnswer";
        //Update Information
        $update_info =  array(":UAnswer"=>$Answer);
        //Insert ApplicantData
        G::Obj('Applicants')->insUpdApplicantData($insert_info, $on_update, $update_info);
        
    }
    
}
?>
