<?php
/**
 * @class		AgreementForms
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class AgreementForms {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      getAgreementFormsListByOrgID
     */
    function getAgreementFormsListByOrgID($columns = "*", $OrgID, $FormStatus) {
        
        $columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
        
        $params_info    =   array(":OrgID"=>$OrgID, ":FormStatus"=>$FormStatus);
        $sel_forms      =   "SELECT $columns FROM AgreementForms WHERE OrgID = :OrgID AND FormStatus = :FormStatus ORDER BY SortOrder ASC";
        $res_forms      =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_forms, array($params_info) );
        $res_forms_cnt  =   $res_forms['count'];
        $res_forms_res  =   $res_forms['results'];
        
        $agr_forms      =   array();
        for($a = 0; $a < $res_forms_cnt; $a++) {
            $agr_forms[]    =   $res_forms_res[$a];
        }
        
        return $agr_forms;
    }
    
}
?>
