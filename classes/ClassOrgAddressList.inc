<?php
/**
 * @class		OrgAddressList
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class OrgAddressList {

    /**
     * @tutorial	Set the default database connection as irecruit
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }

    /**
     * @method		getAddressList
     * @param		$OrgID, $MultiOrgID, $columns(optional)
     * @return		associative array
     */
    function getAddressList($OrgID) {
    
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID);    
        $sel_address_info   =   "SELECT * FROM OrgAddressList WHERE OrgID = :OrgID";
        $res_address_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_address_info, array($params) );

        return $res_address_info;
    }
	
    /**
     * @method		insApplicantSummaryQuestionsInfo
     * @param		$OrgID, $AddressList
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insAddressList($OrgID, $AddressList) {
    
        $params_info        =   array(":OrgID"=>$OrgID, ":AddressList"=>$AddressList, ":UAddressList"=>$AddressList);
        $ins_statement      =   "INSERT INTO OrgAddressList(OrgID, AddressList, LastModifiedDateTime) VALUES(:OrgID, :AddressList, NOW())";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE AddressList = :UAddressList, LastModifiedDateTime = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
    
        return $res_statement;
    }
    
    /**
     * @method      insDefaultAddress
     */
    function insDefaultAddress($OrgID) {
        $address_info   =   $this->getAddressList($OrgID);
        
        if($address_info['OrgID'] == "") {
            
            $OrgData    =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, "", "*");
            
            $MAddress   =   array (
                                $OrgData ['Address1'],
                                $OrgData ['Address2'],
                                $OrgData ['City'],
                                $OrgData ['State'],
                                $OrgData ['ZipCode']
                            );
            $CAddress   =   array (
                                $OrgData ['ContactAddress'],
                                $OrgData ['ContactAddress2'],
                                $OrgData ['City'],
                                $OrgData ['State'],
                                $OrgData ['ZipCode']
                            );
            
            // It will unset the null values, it will make a clean array
            $MAddress   =   array_filter ( $MAddress );
            $CAddress   =   array_filter ( $CAddress );
            
            $MyOfficeAddress    =   implode ( ",", $MAddress );
            $CorporateAddress   =   implode ( ",", $CAddress );

            $address_details    =   array();
            $address_details[]  =   ["Address"=>$MyOfficeAddress, "Default"=>"Y"];
            if($CorporateAddresss != null)
            $address_details[]  =   ["Address"=>$CorporateAddresss, "Default"=>"N"];
            $AddressList        =   json_encode($address_details);
            
            $params_info        =   array(":OrgID"=>$OrgID, ":AddressList"=>$AddressList, ":UAddressList"=>$AddressList);
            $ins_statement      =   "INSERT INTO OrgAddressList(OrgID, AddressList, LastModifiedDateTime) VALUES(:OrgID, :AddressList, NOW())";
            $ins_statement      .=  " ON DUPLICATE KEY UPDATE AddressList = :UAddressList, LastModifiedDateTime = NOW()";
            $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
            
        }
    }
}
?>
