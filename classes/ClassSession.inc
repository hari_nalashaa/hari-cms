<?php
/**
 * @class		Session
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Session {
	
    var $conn_string        =   "IRECRUIT";
    
	/**
	 *
	 * @tutorial Constructor to load the default database,
	 *           instantiate the Database class 
	 *           and set session handler
	 *          
	 */
	public function __construct() {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	    
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
		
		// Set handler to overide SESSION
		session_set_save_handler ( array (
				$this,
				"_open" 
		), array (
				$this,
				"_close" 
		), array (
				$this,
				"_read" 
		), array (
				$this,
				"_write" 
		), array (
				$this,
				"_destroy" 
		), array (
				$this,
				"_gc" 
		) );
		
		//It will autoload all the classes
		spl_autoload_register(array($this, 'iRecruitAutoloader'));
				
		// Start the session
		session_start ();
	}

	/**
	 * @method     iRecruitAutoloader
	 * @param      string $class_name
	 */
	function iRecruitAutoloader($class_name)
	{
	    $path = ROOT . 'classes/Class';
	
	    if(file_exists($path.$class_name.'.inc') 
	       && $class_name != "Session") {
	        
	        require_once $path.$class_name.'.inc';
	        
	    }
	}
	
	/**
	 * Open
	 */
	public function _open() {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	    	     
		// If successful
		if ($this->db->getConnection ( $this->conn_string )) {
			// Return True
			return true;
		}
		// Return False
		return false;
	}
	
	/**
	 * Close
	 */
	public function _close() {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	    	     
		// If successful
		if ($this->db->getConnection ( $this->conn_string )) {
			// Return True
			return true;
		}
		// Return False
		return false;
	}
	
	/**
	 * Read
	 */
	public function _read($id) {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	     
		/**
		 * Generates a backtrace to make the debugging process easier, enable it only if it is required because it is epensive operation
		 */
		$params = array (":ID" => $id);
		$sel_session_info = "SELECT Data FROM Sessions WHERE ID = :ID";
		$res_session_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_session_info, array (
				$params 
		) );
		
		$session_data = ($res_session_info ['Data']) ? $res_session_info ['Data'] : '';
		return $session_data;
	}
	
	/**
	 * Write
	 */
	public function _write($id, $data) {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	     
		// Create time stamp
		$access       =   time ();
		
		// Set parameters
		$params       =   array (':ID' => $id, ':Access' => $access, ':Data' => $data, ':UAccess' => $access, ':UData' => $data);
		// Update session values
		$upd_session  =   'INSERT INTO Sessions VALUES (:ID, :Access, :Data) ON DUPLICATE KEY UPDATE Access = :UAccess, Data = :UData';
		// Set query
		$res_session  =   $this->db->getConnection ( $this->conn_string )->update ( $upd_session, array ($params) );
		
		// If successful
		if ($res_session ['error_info'] == '') {
			// Return True
			return true;
		}
		
		// Return False
		return false;
	}
	
	/**
	 * Destroy
	 */
	public function _destroy($id) {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	     
		$params = array (':ID' => $id);
		$del_session = 'DELETE FROM Sessions WHERE ID = :ID';
		$res_session = $this->db->getConnection ( $this->conn_string )->delete ( $del_session, array (
				$params 
		) );
		
		// If successful
		if ($res_session ['error_info'] == '' || $res_session ['error_info'] === NULL) {
			// Return True
			return true;
		}
		
		// Return False
		return false;
	}
	
	/**
	 * Garbage Collection
	 */
	public function _gc($max) {
	    
	    if(FROM_SRC == 'USERPORTAL') $this->conn_string = 'USERPORTAL';
	    else $this->conn_string = 'IRECRUIT';
	     
		// Calculate what is to be deemed old
		$old = time () - $max;
		
		// Set parameters
		$params = array (':Old' => $old);
		// Set query
		$del_session = 'DELETE FROM Sessions WHERE Access < :Old';
		$res_session = $this->db->getConnection ( $this->conn_string )->delete ( $del_session, array (
				$params 
		) );
		
		// If successful
		if ($res_session ['error_info'] == '' || $res_session ['error_info'] === NULL) {
			// Return True
			return true;
		}
		
		// Return False
		return false;
	}
}
?>
