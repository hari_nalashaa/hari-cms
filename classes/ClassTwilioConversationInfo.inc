<?php
/**
 * @class		TwilioConversationInfo
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioConversationInfo {

    public $db;

	var $conn_string       		=   "IRECRUIT";

	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		// Create a new instance of a SOAP 1.2 client
		$this->db	=	Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	} // end function

	/**
	 * @method		getUserConversationResources
	 */
	public function getUserConversationResources($OrgID, $UserID, $applicant_number, $user_number) {
	
		$params				=	array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":MobileNumbers1"=>"%".$applicant_number."%", ":MobileNumbers2"=>"%".$user_number."%");
		$sel_conversations	=	"SELECT * FROM TwilioConversations WHERE OrgID = :OrgID AND UserID = :UserID AND (MobileNumbers LIKE :MobileNumbers1 AND MobileNumbers LIKE :MobileNumbers2)";
		$res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
		$conversations_info	=	$res_conversations['results'];
		 
		return $conversations_info;
	}
	
	/**
	 * @method		getProxyNumbersOfPersonalNumber
	 */
	public function getProxyNumbersOfPersonalNumber($PersonalNumber) {

		//Get records for that mobile number
		$params				=	array(":MobileNumbers"=>"%".$PersonalNumber."%");
		$sel_conversations	=	"SELECT * FROM TwilioConversations WHERE MobileNumbers LIKE :MobileNumbers";
		$res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
		$conversations_info	=	$res_conversations['results'];

		$used_proxys		=	array();
		for($k = 0; $k < count($conversations_info); $k++) {
			$mobile_numbers		=	json_decode($conversations_info[$k]['MobileNumbers'], true);

			if($mobile_numbers[0][1] == $PersonalNumber) {
				$used_proxys[]	=	$mobile_numbers[0][0];	
			}
			else if($mobile_numbers[1][1] == $PersonalNumber) {
				$used_proxys[]	=	$mobile_numbers[1][0];
			}
		}
		
		return $used_proxys;
	}
	
	
    /**
     * @method		getAvailableProxyNumberPair
     */
	public function getAvailableProxyNumberPair($OrgID, $twilio_numbers, $applicant_number, $user_number) {
    	
    	$account_info 		=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	//Get conversation info realted to applicant
    	$params				=	array(":MobileNumbers"=>"%".$applicant_number."%");
    	$sel_conversations	=	"SELECT * FROM TwilioConversations WHERE (MobileNumbers LIKE :MobileNumbers)";
    	$res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
    	$applicants_info	=	$res_conversations['results'];
		
    	//Get Used Applicant Numbers Proxy List
    	$applicants_proxy	=	array();
    	for($u = 0; $u < count($applicants_info); $u++) {
    		if($applicants_info[$u]['MobileNumbers'] != "") {
    			$applicants_numbers_info	=	json_decode($applicants_info[$u]['MobileNumbers'], true);
    			$applicants_proxy[]			=	$applicants_numbers_info[0][0];
    		}
    	}
    	
    	//Get Used Applicant Proxys
    	$used_proxys		=	$this->getProxyNumbersOfPersonalNumber($applicant_number);
    	$applicants_proxy	=	array_merge($applicants_proxy, $used_proxys);
    	$applicants_proxy	=	array_unique($applicants_proxy);
    	
    	//Get the available applicant proxy nums
    	$remaining_applicant_proxy_nums	=	array_diff($twilio_numbers, $applicants_proxy);
    	$remaining_applicant_proxy_nums	=	array_values($remaining_applicant_proxy_nums);
    	$avilable_applicant_number		=	$remaining_applicant_proxy_nums[0];
    	
    	//Get conversation info realted to user 
		$params				=	array(":MobileNumbers"=>"%".$user_number."%");
    	$sel_conversations	=	"SELECT * FROM TwilioConversations WHERE (MobileNumbers LIKE :MobileNumbers)";
    	$res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
    	$user_info			=	$res_conversations['results'];
		
    	//Get Used User Numbers Proxy List
    	$user_proxy_nums	=	array();
    	for($i = 0; $i < count($user_info); $i++) {
    		if($user_info[$i]['MobileNumbers'] != "") {
    			$user_numbers_info	=	json_decode($user_info[$i]['MobileNumbers'], true);
    			$user_proxy_nums[]	=	$user_numbers_info[1][0];
    		}
    	}

    	//Get Used Applicant Proxys
    	$used_proxys				=	$this->getProxyNumbersOfPersonalNumber($user_number);
    	$user_proxy_nums			=	array_merge($user_proxy_nums, $used_proxys);
    	$user_proxy_nums			=	array_unique($user_proxy_nums);

    	//Get the available applicant proxy nums
    	$remaining_user_proxy_nums	=	array_diff($twilio_numbers, $user_proxy_nums);
    	$remaining_user_proxy_nums	=	array_values($remaining_user_proxy_nums);
    	$avilable_user_number		=	$remaining_user_proxy_nums[0];
    	
    	//Update available user number, if it is same
    	if($avilable_applicant_number == $avilable_user_number) {
			$avilable_user_number	=	$remaining_user_proxy_nums[1];
    	}

    	//Purchase Applicant Phone Number
    	if($avilable_applicant_number == "")
    	{
    		//Available Phone Numbers
    		$phone_numbers_response		=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
    		$available_phone_numbers	=	$phone_numbers_response['Response'];
    		
    		//Purchase number
    		if(is_array($available_phone_numbers) && count($available_phone_numbers) > 0) {
    			 
				$phone_numbers_result	=	G::Obj('TwilioPhoneNumbersApi')->getPhoneNumbersCountByMsgServiceID($OrgID, $account_info['MessageServiceID']);
				$phone_numbers_count	=	$phone_numbers_result['PhoneNumbersCount'];
				
				if($phone_numbers_count >= 400) {
				    $message_service_info	=	G::Obj('TwilioMessagingServiceApi')->createMessageService($OrgID, G::Obj('GenericLibrary')->getGuIDBySalt($OrgID));
				}
    			
    			$available_phone_number	=	$available_phone_numbers[0];
    			$new_num_response		=	G::Obj('TwilioPhoneNumbersApi')->createIncomingPhoneNumber($OrgID, $available_phone_number);
    			$number_response		=	$new_num_response['Response'];

    			if($number_response->sid != "") {
    				$phone_number_res_info		=	G::Obj('TwilioMessagingServiceApi')->createPhoneNumberResource($OrgID, $account_info['MessageServiceID'], $number_response->sid);
    				$avilable_applicant_number	=	$available_phone_number;
    			}

    			// send email
    			mail("dedgecomb@irecruit-software.com",	 $_SERVER['SERVER_NAME'] . "Line: " . __LINE__, print_r($_POST,true) . '<br>ClassTwilioConversationInfo.inc<br>Phone Number Assigned: ' . $OrgID . '--' . $avilable_applicant_number);
    		}
    		
    	}
    	
    	//Purchase User Phone Number
		if($avilable_user_number == "")
		{
    		//Available Phone Numbers
		    $phone_numbers_response		=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
    		$available_phone_numbers	=	$phone_numbers_response['Response'];
    		
    		//Purchase number
    		if(is_array($available_phone_numbers) && count($available_phone_numbers) > 0) {
    			
				$phone_numbers_result	=	G::Obj('TwilioPhoneNumbersApi')->getPhoneNumbersCountByMsgServiceID($OrgID, $account_info['MessageServiceID']);
				$phone_numbers_count	=	$phone_numbers_result['PhoneNumbersCount'];
				
				if($phone_numbers_count >= 400) {
				    $message_service_info	=	G::Obj('TwilioMessagingServiceApi')->createMessageService($OrgID, G::Obj('GenericLibrary')->getGuIDBySalt($OrgID));
				}
    			
    			$available_phone_number	=	$available_phone_numbers[0];
    			$new_num_response		=	G::Obj('TwilioPhoneNumbersApi')->createIncomingPhoneNumber($OrgID, $available_phone_number);
    			$number_response		=	$new_num_response['Response'];
    			
    			if($number_response->sid != "") {
    				$phone_number_res_info	=	G::Obj('TwilioMessagingServiceApi')->createPhoneNumberResource($OrgID, $account_info['MessageServiceID'], $number_response->sid);
    				$avilable_user_number	=	$available_phone_number;
    			}

    			// send email
    			mail("dedgecomb@irecruit-software.com",	 $_SERVER['SERVER_NAME'] . "Line: " . __LINE__, print_r($_POST,true) . '<br>ClassTwilioConversationInfo.inc<br>Phone Number Purchased ' . $OrgID . '--' . print_r($new_num_response,true));
    			
    		}
    	}
    	
    	return array("ApplicantProxy"=>$avilable_applicant_number, "UserProxy"=>$avilable_user_number);
    }

    /**
     * @method		getAllConversations
     */
    public function getAllConversations($columns = "", $where_info = array(), $group_by = "",$order_by = "", $info = array()) {
    	
    	$columns = $this->db->arrayToDatabaseQueryString ( $columns );
    	
    	$sel_conversations	=	"SELECT $columns FROM TwilioConversations";
    	
    	if(count($where_info) > 0) {
    		$sel_conversations .= " WHERE " . implode(" AND ", $where_info);
    	}

    	if($group_by != "") $sel_conversations .= " GROUP BY " . $group_by;
    	if($order_by != "") $sel_conversations .= " ORDER BY " . $order_by;
    	
    	$res_conversations	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_conversations, $info );
    	
    	return $res_conversations;
    }
    
    
    /**
     * @method		getAllUserConversations
     */
    public function getAllUserConversations($OrgID, $UserID) {
    
    	$params				=	array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
    	$sel_conversations	=	"SELECT * FROM TwilioConversations WHERE OrgID = :OrgID AND UserID = :UserID";
    	$res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
    	$conversations_info	=	$res_conversations['results'];
    	 
    	return $conversations_info;
    }
    
    /**
     * @method		getAllConversationResources
     */
    public function getAllConversationResources($personal_number) {
    
    	$params				=	array(":MobileNumbers"=>"%".$personal_number."%");
    	$sel_conversations	=	"SELECT * FROM TwilioConversations WHERE MobileNumbers LIKE :MobileNumbers";
    	$res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
    	$conversations_info	=	$res_conversations['results'];
    	 
    	return $conversations_info;
    }

    /**
     * @method		insConversationResourceInfo
     */
    public function insConversationResourceInfo($OrgID, $UserID, $ResourceID, $numbers_info) {
    	
    	$numbers_data	=	[];
    	$numbers_data[]	=	[$numbers_info[NID1],$numbers_info[NID2]];
		$info			=	array(
								"ResourceID"		=>	$ResourceID,
								"OrgID"				=>	$OrgID,
								"UserID"			=>	$UserID,
								"MobileNumbers"		=>	json_encode($numbers_data),
								"CreatedDateTime"	=>	"NOW()",
							);
		$insert_info 	=	$this->db->buildInsertStatement('TwilioConversations', $info);
		$result_info 	=	$this->db->getConnection ( $this->conn_string )->insert ( $insert_info["stmt"], $insert_info["info"] );
    	
		return $result_info;
    }

    /**
     * @method		insTwilioConversationsArchive
     */
    public function insTwilioConversationsArchive($conversation_res_info) {
        
        $info			=	array(
                                "ResourceID"		=>	$conversation_res_info['ResourceID'],
                                "OrgID"				=>	$conversation_res_info['OrgID'],
                                "UserID"			=>	$conversation_res_info['UserID'],
                                "MobileNumbers"		=>	$conversation_res_info['MobileNumbers'],
                                "CreatedDateTime"	=>	$conversation_res_info['CreatedDateTime'],
                            );
        $skip_fields    =   array("ResourceID", "OrgID", "UserID", "MobileNumbers");
        $insert_info 	=	$this->db->buildOnDuplicateKeyUpdateStatement('TwilioConversationsArchive', $info, $skip_fields);
        $result_info 	=	$this->db->getConnection ( $this->conn_string )->insert ( $insert_info["stmt"], $insert_info["info"] );
        
        return $result_info;
    }
    
    /**
     * @method		updConversationResourceInfo
     */
    public function updConversationResourceInfo($OrgID, $UserID, $ResourceID, $numbers_info) {
    	 
    	$conversation_info	=	G::Obj('TwilioConversationInfo')->getConversationResourceInfo($OrgID, $UserID, $ResourceID);
    	$mobile_numbers		=	json_decode($conversation_info['MobileNumbers'], true);
    	$mobile_numbers[]	=	[$numbers_info[NID1],$numbers_info[NID2]];
    	
    	$params				=	array(
    								":OrgID"			=>	$OrgID,
					    			":UserID"			=>	$UserID,
    								":ResourceID"		=>	$ResourceID,
					    			":MobileNumbers"	=>	json_encode($mobile_numbers),
					    		);
    	$update_info 		=	"UPDATE TwilioConversations SET MobileNumbers = :MobileNumbers, CreatedDateTime = NOW() WHERE OrgID = :OrgID AND UserID = :UserID AND ResourceID = :ResourceID";
    	$result_info 		=	$this->db->getConnection ( $this->conn_string )->update ( $update_info, array($params) );
    	 
    	return $result_info;
    }
    
    /**
     * @method		delConversationResourceInfo
     */
    public function delConversationResourceInfo($OrgID, $ResourceID) {
    	 
    	$params			=	array(":OrgID"=>$OrgID, ":ResourceID"=>$ResourceID);
    	$delete_info	=	"DELETE FROM TwilioConversations WHERE OrgID = :OrgID AND ResourceID = :ResourceID";
    	$result_info 	=	$this->db->getConnection ( $this->conn_string )->delete ( $delete_info, array($params) );
    	 
    	return $result_info;
    }
    
    /**
     * @method		getConversationResourceInfo
     */
    public function getConversationResourceInfo($OrgID, $UserID, $ResourceID) {
    	
    	$params_info		=	array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":ResourceID"=>$ResourceID);
    	$sel_conversation 	=	"SELECT * FROM TwilioConversations WHERE OrgID = :OrgID AND UserID = :UserID AND ResourceID = :ResourceID";
    	$res_conversation 	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_conversation, array($params_info) );
    	 
    	return $res_conversation;
    }
    
    /**
     * @method		getConversationInfoByResourceID
     */
    public function getConversationInfoByResourceID($ResourceID) {
    	 
    	$params_info		=	array(":ResourceID"=>$ResourceID);
    	$sel_conversation 	=	"SELECT * FROM TwilioConversations WHERE ResourceID = :ResourceID";
    	$res_conversation 	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_conversation, array($params_info) );
    
    	return $res_conversation;
    }
    
    /**
     * @method		getOrgIDByResourceID
     */
    public function getOrgIDByResourceID($ResourceID) {
    	
    	$params_info		=	array(":ResourceID"=>$ResourceID);
    	$sel_conversation 	=	"SELECT OrgID FROM TwilioConversations WHERE ResourceID = :ResourceID";
    	$res_conversation 	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_conversation, array($params_info) );
    	
    	if(isset($res_conversation['OrgID']) && $res_conversation['OrgID'] != "") {
    	    return $res_conversation['OrgID'];
    	}
    	else {
    	    return "";
    	}
    }
}
