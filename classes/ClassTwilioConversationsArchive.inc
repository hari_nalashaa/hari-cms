<?php
/**
 * @class		TwilioConversationsArchive
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioConversationsArchive {

    public $db;

	var $conn_string       		=   "IRECRUIT";

	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		// Create a new instance of a SOAP 1.2 client
		$this->db	=	Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	} // end function

	/**
	 * @method		getUserConversationsArchiveResources
	 */
	public function getUserConversationsArchiveResources($OrgID, $UserID, $applicant_number, $user_number) {
	    
	    //, ":MobileNumbers2"=>"%".$user_number."%"  AND MobileNumbers LIKE :MobileNumbers2
	    $params				=	array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":MobileNumbers1"=>"%".$applicant_number."%");
	    $sel_conversations	=	"SELECT * FROM TwilioConversationsArchive WHERE OrgID = :OrgID AND UserID = :UserID AND (MobileNumbers LIKE :MobileNumbers1) order by CreatedDateTime DESC";
	    $res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
	    $conversations_info	=	$res_conversations['results'];
	    
	    return $conversations_info;
	}
	
	/**
	 * @method		getTwilioConversationsArchiveByResourceID
	 */
	public function getTwilioConversationsArchiveByResourceID($ResourceID) {
	    
	    $params				=	array(":ResourceID"=>$ResourceID);
	    $sel_conversations	=	"SELECT * FROM TwilioConversationsArchive WHERE ResourceID = :ResourceID";
	    $res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_conversations, array($params) );
	    
	    return $res_conversations;
	}
	
	/**
	 * @method		getTwilioConversationsArchive
	 */
	public function getTwilioConversationsArchive($OrgID, $UserID, $applicant_number, $user_number) {
	
		//":MobileNumbers2"=>"%".$user_number."%"  AND MobileNumbers LIKE :MobileNumbers2
        $conversations_info     =   array();
        
        if($applicant_number != "") {
            $params				=	array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":MobileNumbers1"=>"%".$applicant_number."%");
            $sel_conversations	=	"SELECT * FROM TwilioConversationsArchive WHERE OrgID = :OrgID AND UserID = :UserID AND (MobileNumbers LIKE :MobileNumbers1)";
            $res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
            $conversations_info	=	$res_conversations['results'];
        }
		 
		return $conversations_info;
	}
	
}
?>
