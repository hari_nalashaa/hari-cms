<?php
/**
 * @class		WotcFormQueAns
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 * @tutorial    Here $AppQueAnswers is a reference variable.
 *              $AppQueAnswers array keys follows below sequence
 */

class WOTCFormQueAns {
	
	var $conn_string       =   "IRECRUIT";

	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method     getWotcFormQueAnswersInfo
	 * @param      string $OrgID
	 * @param      string $AgreementFormID
	 * @param      string $APPDATA
	 * @param      string $SectionID
	 * @return     string
	 */
	function getWotcFormQueAnswersInfo($wotcID, $WotcFormID, $ApplicationID) {
		
	    global $permit, $FormsInternalObj, $AddressObj, $FormDataObj, $FormQuestionsObj, $GenericLibraryObj;
	    
	    //Get Applicant Data 
	    $WOTCAPPDATA	=	G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $wotcID, $WotcFormID, $ApplicationID);

	    //Get Wotc Form Questions
	    $where_info		=	array("WotcID = :WotcID", "WotcFormID = :WotcFormID");
	    $params_info	=	array(":WotcID"=>"MASTER", ":WotcFormID"=>"STANDARD");
	    $results   		=   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsInfo("*", $where_info, "", "QuestionOrder", array($params_info));
	    $wotc_frm_que	=	$results['results'];
	    
	    $AppQueAnswers	=	array();
	    
	    if(count($wotc_frm_que) > 0) {
	        
	        foreach ($wotc_frm_que as $FQ) {

	            // format answers
	            if ($FQ ['QuestionTypeID'] == 1) {
	                $date_answer = '';
	                if($WOTCAPPDATA [$FQ ['QuestionID'] . 'from'] != "" 
	                	&& $WOTCAPPDATA [$FQ ['QuestionID'] . 'to'] != "") {
	                    $date_answer = "from " . $WOTCAPPDATA [$FQ ['QuestionID'] . 'from'] . " to " . $WOTCAPPDATA [$FQ ['QuestionID'] . 'to'];
	                }

	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $date_answer;
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   1;
                    $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
                    
	            } // end from, to dates question
	            else if ($FQ ['QuestionTypeID'] == 3) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getDisplayValue ( $WOTCAPPDATA [$FQ ['QuestionID']], $FQ ['value'] );
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   3;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end pulldown question
	            else if ($FQ ['QuestionTypeID'] == 5) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   5;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end textarea question
	            else if ($FQ ['QuestionTypeID'] == 6) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   6;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end text box question
	            else if ($FQ ['QuestionTypeID'] == 10) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   10;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end date question
	            else if ($FQ ['QuestionTypeID'] == 13) {

	                if($GenericLibraryObj->isJSON($WOTCAPPDATA [$FQ['QuestionID']]) === true) {
	                    $Answer13  =   ($WOTCAPPDATA [$FQ['QuestionID']] != "") ? json_decode($WOTCAPPDATA [$FQ['QuestionID']], true) : "";
	                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $AddressObj->formatPhone ( $OrgID, $WOTCAPPDATA ['country'], $Answer13[0], $Answer13[1], $Answer13[2], '' );
	                }
	                else if(is_string($WOTCAPPDATA [$FQ['QuestionID']]) && $WOTCAPPDATA [$FQ['QuestionID']] != "") {
	                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $WOTCAPPDATA [$FQ['QuestionID']];
	                }
	                else {
	                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $AddressObj->formatPhone ( $OrgID, $WOTCAPPDATA ['country'], $WOTCAPPDATA [$FQ['QuestionID'] . '1'], $WOTCAPPDATA [$FQ['QuestionID'] . '2'], $WOTCAPPDATA [$FQ['QuestionID'] . '3'], '' );
	                }
	                
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   13;
                    $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
                    
	            } // end phone without extension
	            else if ($FQ ['QuestionTypeID'] == 14) {
	                
	                if($GenericLibraryObj->isJSON($WOTCAPPDATA [$FQ['QuestionID']]) === true) {
	                    $Answer14  =   ($WOTCAPPDATA [$FQ['QuestionID']] != "") ? json_decode($WOTCAPPDATA [$FQ['QuestionID']], true) : "";
	                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $AddressObj->formatPhone ( $OrgID, $WOTCAPPDATA ['country'], $Answer14[0], $Answer14[1], $Answer14[2], $Answer14[3] );
	                }
	                else if(is_string($WOTCAPPDATA [$FQ['QuestionID']]) && $WOTCAPPDATA [$FQ['QuestionID']] != "") {
	                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $WOTCAPPDATA [$FQ['QuestionID']];
	                }
	                else {
	                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']  =   $AddressObj->formatPhone ( $OrgID, $WOTCAPPDATA ['country'], $WOTCAPPDATA [$FQ['QuestionID'] . '1'], $WOTCAPPDATA [$FQ['QuestionID'] . '2'], $WOTCAPPDATA [$FQ['QuestionID'] . '3'], $WOTCAPPDATA [$FQ['QuestionID'] . 'ext'] );
	                }
	                 
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   14;
                    $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end phone with extension
	            else if ($FQ ['QuestionTypeID'] == 15) {
                    $Answer15  =   ($WOTCAPPDATA [$FQ['QuestionID']] != "") ? json_decode($WOTCAPPDATA [$FQ['QuestionID']], true) : "";
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Question']             		=   $FQ['Question'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']               		=   $WOTCAPPDATA ['Social1'] . '-' . $WOTCAPPDATA ['Social2'] . '-' . $WOTCAPPDATA ['Social3'];
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']       		=   15;
                    $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            }
	            else if ($FQ ['QuestionTypeID'] == 17) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   17;
                    $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end date
	            else if (($FQ ['QuestionTypeID'] == 18)) {
	                $Answer18  = json_decode($WOTCAPPDATA[$FQ ['QuestionID']], true);
	                foreach ($Answer18 as $Answer18Key=>$Answer18Value) {
	                    $WOTCAPPDATA[$Answer18Key] =   $Answer18Value;
	                }
	                $WOTCAPPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer18);
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getMultiAnswer ( $WOTCAPPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end instructions
	            else if ($FQ ['QuestionTypeID'] == 24) {
	                $date_answer = '';
	                if($WOTCAPPDATA [$FQ ['QuestionID'] . 'from'] != "" && $WOTCAPPDATA [$FQ ['QuestionID'] . 'to'] != "") {
	                    $date_answer = "from " . $WOTCAPPDATA [$FQ ['QuestionID'] . 'from'] . " to " . $WOTCAPPDATA [$FQ ['QuestionID'] . 'to'];
	                }
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                    $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $date_answer;
                    $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   24;
                    $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            }
	            else if ($FQ ['QuestionTypeID'] == 60) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   60;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            }
	            else if ($FQ['QuestionTypeID'] == 30) { // Signature
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   'Electronically by symbol ' . $WOTCAPPDATA['captcha'] . ' on ' . $WOTCAPPDATA[$FQ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   30;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            }
	            else if ($FQ ['QuestionTypeID'] == 99) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   99;
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end pulldown question
	            else if ($FQ ['QuestionTypeID'] == 1818) {
	                $Answer1818  = json_decode($WOTCAPPDATA[$FQ ['QuestionID']], true);
	                foreach ($Answer1818 as $Answer1818Key=>$Answer1818Value) {
	                    $WOTCAPPDATA[$Answer1818Key] =   $Answer1818Value;
	                }
	                $WOTCAPPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer1818);
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getMultiAnswer ( $WOTCAPPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            } // end instructions
	            else if ($FQ ['value']) {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $this->getDisplayValue ( $WOTCAPPDATA [$FQ ['QuestionID']], $FQ ['value'] );
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            }
	            else {
	            	$AppQueAnswers[$FQ ['QuestionID']]['QuestionID']				=   $FQ['QuestionID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	                $AppQueAnswers[$FQ ['QuestionID']]['Answer']                    =   $WOTCAPPDATA [$FQ ['QuestionID']];
	                $AppQueAnswers[$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	                $AppQueAnswers[$FQ ['QuestionID']]['ChildQuestionsInfo']		=   $FQ['ChildQuestionsInfo'];
	            }
	        }
	         
	    }
	    
	    return $AppQueAnswers;
	}
	
	/**
	 * @method     getValueList
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getValueList($value) {
	
	    $values_list    =   array();
	    $que_values     =   explode ( '::', $value );
	
	    foreach ( $que_values as $v => $q ) {
	        list ( $vv, $qq )   =   explode ( ":", $q);
	        $values_list[$vv]   =   $qq;
	    }
	
	    return $values_list;
	}
	
	/**
	 * @method     getAnswer18
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer18($OrgID, $QuestionID, $Answer, $QueValue) {
	
        $info   =   json_decode($Answer, true);
        $values =   $this->getValueList($QueValue);
        
        $rtn    =   '';
	    foreach($info as $key=>$value) {
	        $rtn    .=  '&#8226;&nbsp;' . $values[$value];
	    }
	
	    return $rtn;
	}
	
	/**
	 * @method     getAnswer1818
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer1818($OrgID, $QuestionID, $Answer, $QueValue) {
	
	    $info      =   json_decode($Answer, true);
	    $values    =   $this->getValueList($QueValue);
	
	    $rtn       =   '';
	    foreach($info as $key=>$value) {
	        $rtn .= '&#8226;&nbsp;' .  $values[$value] . "\n";
	    }
	    	
	    return $rtn;
	}
	
	/**
	 * @method     getDisplayValue
	 * @param      string $answer
	 * @param      string $displaychoices
	 * @return     string
	 */
	function getDisplayValue($answer, $displaychoices) {
	    $rtn = '';
	
	    if ($answer != '') {
	        $Values = explode ( '::', $displaychoices );
	        foreach ( $Values as $v => $q ) {
	            list ( $vv, $qq ) = explode ( ":", $q, 2 );
	            if ($vv == $answer) {
	                $rtn .= $qq;
	            }
	        }
	    }
	    
	    return $rtn;
	    
	} // end of function

	
	/**
	 * @method     getMultiAnswer
	 * @param      array $APPDATA
	 * @param      string $id
	 * @param      string $displaychoices
	 * @param      string $questiontype
	 * @return     string
	 */
	function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
	    $rtn = '';
	    $val = '';
	    $lex = '';
	
	    if (($id == 'daysavailable') || ($id == 'typeavailable')) {
	
	        $le = ', ';
	        $li = '';
	        $lecnt = - 2;
	    } else {
	
	        $le = '<br>';
	        $li = '&#8226;&nbsp;';
	        $lecnt = - 4;
	    }
	
	    if($questiontype == 18 || $questiontype == 1818) {
	        $displaychoices_vals = explode ( '::', $displaychoices );
	        $cnt   =   count($displaychoices_vals);
	    }
	    else {
	        $cnt   =   $APPDATA [$id . 'cnt'];
	    }
	
	    for($i = 1; $i <= $cnt; $i ++) {
	
	        $val = $this->getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
	        if ($val) {
	            $rtn .= $li . $val;
	            $lex = 'on';
	            $val = '';
	        }
	
	        if ($questiontype == 9) {
	            	
	            $val = $APPDATA [$id . '-' . $i . '-yr'];
	            if ($val) {
	                $rtn .= ',&nbsp;' . $val . ' yrs. ';
	                $lex = 'on';
	                $val = '';
	            }
	            	
	            $val = $APPDATA [$id . '-' . $i . '-comments'];
	            if ($val) {
	                $rtn .= '&nbsp;(' . $val . ') ';
	                $lex = 'on';
	                $val = '';
	            }
	        } // end questiontype
	
	        if ($lex == 'on') {
	            $rtn .= $le;
	            $lex = '';
	        }
	    }
	
	    $rtn = substr ( $rtn, 0, $lecnt );
	
	    return $rtn;
	} // end of function

}
