<?php
/**
 * @class		ZipRecruiter
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ZipRecruiter
{
    public $db;
    
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct()
    {
        $this->db = Database::getInstance();
        $this->db->getConnection( $this->conn_string );
    }

    /**
     * @method      getZipRecruiterOrgRequisitions
     * @param       $OrgID, $MultiOrgID            
     * @return      array
     */
    public function getZipRecruiterOrgRequisitions($OrgID, $MultiOrgID) {
        
        $params =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
        $query  =   "SELECT R.OrgID, R.MultiOrgID, R.ZipRecruiterLabelID, 
                    R.RequestID, R.Title, R.Description, R.EmpStatusID, R.FreeJobBoardLists, 
                    date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate,
                    R.Address1, R.Address2, R.City, R.State, R.ZipCode, R.Country, R.Salary_Range_From,
                    R.Salary_Range_To, R.RequisitionFormID
                    FROM Requisitions R
                    WHERE R.OrgID = :OrgID
                    AND R.MultiOrgID = :MultiOrgID 
                    AND getDemoAccountStatus(R.OrgID, R.MultiOrgID) != 'Y'
                    AND R.Active = 'Y'
                    AND R.ZipRecruiterLabelID != ''
                    AND R.PostDate < NOW()
                    AND R.ExpireDate > NOW()
                    AND getRequisitionFinalStageStatus(R.OrgID, R.RequisitionStage) != 'Y'
                    GROUP by R.RequestID
                    ORDER by R.OrgID, R.Title limit 150";
        $results =  $this->db->getConnection( $this->conn_string )->fetchAllAssoc($query, array($params));
        return $results;
    }

    /**
     * @method      getZipRecruiterPremiumOrgRequisitions
     * @param       $OrgID, $MultiOrgID
     * @return      array
     */
    public function getZipRecruiterPremiumOrgRequisitions($OrgID, $MultiOrgID) {
    
        $params =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
        $query  =   "SELECT R.OrgID, R.MultiOrgID, R.ZipRecruiterLabelID,
                    R.RequestID, R.Title, R.Description, R.EmpStatusID,
                    date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate,
                    R.Address1, R.Address2, R.City, R.State, R.ZipCode, R.Country, R.Salary_Range_From,
                    R.Salary_Range_To, R.ZipRecruiterTrafficBoost, R.RequisitionFormID
                    FROM PurchaseItems PI
                    JOIN Purchases P ON P.OrgID = PI.OrgID AND P.PurchaseNumber = PI.PurchaseNumber
                    JOIN Requisitions R ON P.OrgID = R.OrgID AND PI.RequestID = R.RequestID
                    WHERE P.OrgID = :OrgID
                    AND P.PurchaseDate > date_sub(now(), interval 30 day)
                    AND PI.ServiceType = 'ZipRecruiter'
                    AND R.Active = 'Y'
                    AND R.PostDate < NOW()
                    AND R.ExpireDate > NOW()
                    AND R.MultiOrgID = :MultiOrgID
                    AND getRequisitionFinalStageStatus(R.OrgID, R.RequisitionStage) != 'Y'
                    ORDER BY R.Title";
        $results =  $this->db->getConnection( $this->conn_string )->fetchAllAssoc($query, array($params));
        
        return $results;
    }
    
    /**
     * @method      getZipRecruiterSubscribedRequisitions
     * @param       $OrgID, $MultiOrgID
     * @return      array
     */
    public function getZipRecruiterSubscribedRequisitions($OrgID, $MultiOrgID) {
    
        $params =   array(":OrgID" => $OrgID, ":MultiOrgID"=>$MultiOrgID);
        $query  =   "SELECT ROL.OrgLevelID, ROL.SelectionOrder, R.OrgID, R.MultiOrgID, R.ZipRecruiterLabelID, 
                    R.RequestID, R.Title, R.Description, R.EmpStatusID,
                    date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate,
                    R.Address1, R.Address2, R.City, R.State, R.ZipCode, R.Country, R.Salary_Range_From, 
                    R.Salary_Range_To, R.ZipRecruiterTrafficBoost, R.RequisitionFormID
                    FROM RequisitionOrgLevels ROL, Requisitions R
                    WHERE R.OrgID = ROL.OrgID
                    AND R.OrgID = :OrgID
                    AND R.RequestID IN (SELECT Z.RequestID FROM ZipRecruiterSubscriptions Z WHERE Z.OrgID = R.OrgID)
                    AND R.MultiOrgID = :MultiOrgID
                    AND R.RequestID = ROL.RequestID
                    AND R.Active = 'Y' 
                    AND R.ZipRecruiterLabelID != '' 
                    AND R.PostDate < NOW()
                    AND R.ExpireDate > NOW()
                    AND getRequisitionFinalStageStatus(R.OrgID, R.RequisitionStage) != 'Y'
                    GROUP by R.RequestID
                    ORDER by R.OrgID, R.Title";
        $results =  $this->db->getConnection( $this->conn_string )->fetchAllAssoc($query, array($params));
    
        return $results;
    }
    
    
    /**
     * @method      insZipRecruiterApplicantData
     * @param       $info
     * @return      array
     */
    public function insZipRecruiterApplicantData($info) {
            
            // Insert Applicant Information
        $insert_post_data = $this->db->buildInsertStatement('ZipRecruiterApplicants', $info);
        $result_post_data = $this->db->getConnection( $this->conn_string )->insert($insert_post_data['stmt'], $insert_post_data['info']);
        
        return $result_post_data;
    }
    
    
    /**
     * @method      getZipRecruiterApplicants
     * @param       $OrgID
     * @return      array
     */
    public function getZipRecruiterApplicants($OrgID, $columns = "") {
    
        if($columns == "") {
            $columns = "`OrgID`, `MultiOrgID`, `FormID`, `JobID`, `ApplicationID`, `Name`, `FirstName`, `LastName`, `Email`, `Phone`, `Response`";
        }
        
        // Select Applicants Information
        $params_info            =   array(":OrgID"=>$OrgID);
        $sel_zip_recruiter_apps =   "SELECT $columns FROM ZipRecruiterApplicants WHERE OrgID = :OrgID";
        $res_zip_recruiter_apps =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_zip_recruiter_apps, array($params_info));
    
        return $res_zip_recruiter_apps;
    }

    
    /**
     * @method      getAllZipRecruiterApplicants
     * @return      array
     */
    public function getAllZipRecruiterApplicants($columns = "") {
    
        if($columns == "") {
            $columns = "`OrgID`, `MultiOrgID`, `FormID`, `JobID`, `ApplicationID`, `Name`, `FirstName`, `LastName`, `Email`, `Phone`, `Response`";
        }
    
        // Select Applicants Information
        $sel_zip_recruiter_apps =   "SELECT $columns FROM ZipRecruiterApplicants";
        $res_zip_recruiter_apps =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_zip_recruiter_apps, array());
    
        return $res_zip_recruiter_apps;
    }
    
    

    /**
     * @method      getJobCategories
     */
    public function getZipRecruiterJobCategories()
    {
        $zip_recruiter_categories = array(
            "Accounting/Finance",
            "Admin/Secretarial",
            "Advertising",
            "Architect/Design",
            "Art/Media/Writers",
            "Automotive",
            "Banking",
            "Biotech/Pharmaceutical",
            "Computer/Software",
            "Construction/Skilled Trade",
            "Customer Service",
            "Domestic Help/Care",
            "Education",
            "Engineering",
            "Environmental Science",
            "Events",
            "Everything Else",
            "Facilities/Maintenance",
            "General Labor/Warehouse",
            "Gov/Military",
            "HR & Recruiting",
            "Healthcare",
            "Hospitality/Restaurant",
            "Information Technology",
            "Insurance",
            "Internet",
            "Law Enforcement/Security",
            "Legal",
            "Management & Exec",
            "Manufacturing/Operations",
            "Marketing/PR",
            "Nonprofit & Fund",
            "Oil/Energy/Power",
            "Quality Assurance",
            "Real Estate",
            "Research & Dev",
            "Retail",
            "Sales & Biz Dev",
            "Salon/Beauty/Fitness",
            "Social Services",
            "Supply Chain/Logistics",
            "Telecommunications",
            "Travel",
            "Trucking/Transport",
            "TV/Film/Musicians",
            "Vet & Animal Care",
            "Work from Home"
        );
        
        return $zip_recruiter_categories;
    }
    
    
    /**
     * @method		generateInterviewQuestionsJSONString
     */
    function generateInterviewQuestionsJSONString($OrgID, $FormID) {
        
	    $AppQueAnswers     =   G::Obj('ApplicationFormQuestions')->getApplicationFormQuestionsInfo($OrgID, $FormID);
                    
	    $IgnoreQuestions   =   array('first', 'last', 'email');
	    
	    foreach ($AppQueAnswers as $SectionID=>$QA)
	    {
            $SubSecCount = count($AppQueAnswers[$SectionID]);
             
            for($subseci = 0; $subseci < $SubSecCount; $subseci++) {
                 
                $que_section = 0;
                foreach ($QA[$subseci] as $QuestionID=>$QuestionAnswerInfo) {

    			    $QuestionAnswerInfo['Question']  =   utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $QuestionAnswerInfo['Question']));
    			    $QuestionAnswerInfo['value']     =   utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $QuestionAnswerInfo['value']));

    			    $QuestionAnswerInfo['Question']  =   str_replace('"', '\"', $QuestionAnswerInfo['Question']);
    			    $QuestionAnswerInfo['value']     =   str_replace('"', '\"', $QuestionAnswerInfo['value']);
                    $que_section++;
    
                    if(!in_array($QuestionID, $IgnoreQuestions)) {
                    	
                        if($QuestionAnswerInfo['QuestionTypeID'] == 3) {
                            $val_info    =    $QuestionAnswerInfo['value'];
                            $val_res     =    explode("::", $val_info);

                            $options_status = '0';
                            $options_info   = '';
                            for($k = 0; $k < count($val_res); $k++) {
                                $value =  explode(":", $val_res[$k]);
                            
                                if($value[0] != "") {
                                    $options_info .= '{ "value":"'.$value[0].'", "label": "'.$value[1].'"},';
                                    $options_status = '1';
                                }
                            }
                            
                            $options_info = trim($options_info, ",");
                            
                            if(count($val_res) > 0 && $options_status == '1') {
                                //Pull Down
                                $xmlstart   .=   '{';
                                $xmlstart   .=   '"id": "'.$QuestionID.'",';
                                $xmlstart   .=   '"type": "select",';
                                $xmlstart   .=   '"options": [';
                                
                                $xmlstart   .=  $options_info;
                                
                                $xmlstart   .=   '],';
                                
                                if($QuestionAnswerInfo['Required'] == "Y") {
                                    $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                    $xmlstart   .=   '"required": true';
                                }
                                else {
                                    $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                                }
                                
                                $xmlstart   .=   '},';
                            }
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 5) {
                            //Textarea
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "text",';
                        
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                            }
                        
                            $xmlstart   .=   '},';
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 6) {
                            //Text
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "text",';
                        
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                            }
                        
                            $xmlstart   .=   '},';
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 13) {
                            //Phone with out extension
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "text",';
                        
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                            }
                        
                            $xmlstart   .=   '},';
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 14) {
                            //Phone with extension
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "text",';
                        
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                            }
                        
                            $xmlstart   .=   '},';
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 17) {            
                            //Date
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "date",';
                            $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"required": true,';
                            }
                            $xmlstart   .=   '"format": "dd/MM/yyyy"';
                            $xmlstart   .=   '},';
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 18) {

                            $val_info    =    $QuestionAnswerInfo['value'];
                            $val_res     =    explode("::", $val_info);

                            $options_status = '0';
                            $options_info   = '';
                            for($k = 0; $k < count($val_res); $k++) {
                                $value =  explode(":", $val_res[$k]);
                            
                                if($value[0] != "") {
                                    $options_info .= '{ "value":"'.$value[0].'", "label": "'.$value[1].'"},';
                                    $options_status = '1';
                                }
                            }
                            
                            $options_info = trim($options_info, ",");
                            
                            if(count($val_res) > 0 && $options_status == '1') {
                                //Checkboxes
                                $xmlstart   .=   '{';
                                $xmlstart   .=   '"id": "'.$QuestionID.'",';
                                $xmlstart   .=   '"type": "multiselect",';
                                $xmlstart   .=   '"options": [';
                                
                                $xmlstart   .=  $options_info;
                                
                                $xmlstart   .=   '],';
                                
                                if($QuestionAnswerInfo['Required'] == "Y") {
                                    $xmlstart   .=   '"questions": "'.$QuestionAnswerInfo['Question'].'",';
                                    $xmlstart   .=   '"required": true';
                                }
                                else {
                                    $xmlstart   .=   '"questions": "'.$QuestionAnswerInfo['Question'].'"';
                                }
                                
                                $xmlstart   .=   '},';
                            }
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 2) {
                            $val_info    =    $QuestionAnswerInfo['value'];
                            $val_res     =    explode("::", $val_info);

                            $options_status = '0';
                            $options_info   = '';
                            for($k = 0; $k < count($val_res); $k++) {
                                $value =  explode(":", $val_res[$k]);
                            
                                if($value[0] != "") {
                                    $options_info  .=   '{ "value":"'.$value[0].'", "label": "'.$value[1].'"},';
                                    $options_status =   '1';
                                }
                            }
                            
                            $options_info = trim($options_info, ",");
                            
                            if(count($val_res) > 0 && $options_status == '1') {
                                //Select Box
                                $xmlstart   .=   '{';
                                $xmlstart   .=   '"id": "'.$QuestionID.'",';
                                $xmlstart   .=   '"type": "select",';
                                $xmlstart   .=   '"options": [';                                
                                
                                $xmlstart   .=  $options_info;
                                
                                $xmlstart   .=   '],';
                                
                                if($QuestionAnswerInfo['Required'] == "Y") {
                                    $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                    $xmlstart   .=   '"required": true';
                                }
                                else {
                                    $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                                }
                                
                                $xmlstart   .=   '},';
                            }                            
                        }
                        else if($QuestionAnswerInfo['QuestionTypeID'] == 8) {
                            if($QuestionID != 'resumeupload') {
                                //Attachments
                                $xmlstart   .=   '{';
                                $xmlstart   .=   '"id": "'.$QuestionID.'",';
                                $xmlstart   .=   '"type": "upload",';
                            
                                if($QuestionAnswerInfo['Required'] == "Y") {
                                    $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'",';
                                    $xmlstart   .=   '"required": true';
                                }
                                else {
                                    $xmlstart   .=   '"question": "'.$QuestionAnswerInfo['Question'].'"';
                                }
                            
                                $xmlstart   .=   '},';
                            }
                        }
                    }
                }
            }
	    }
	     
        return trim($xmlstart, ",");
    }
    
    
    /**
     * @method      getZipRecruiterFeed
     */
    function getZipRecruiterFeed($OrgID, $RequestID) {
    
        $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $sel_zip_recruiter_info = "SELECT * FROM ZipRecruiterFeed WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_zip_recruiter_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_zip_recruiter_info, array($params) );
    
        return $res_zip_recruiter_info;
    }
    
    /**
     * @method      isPaidRequisition
     */
    function isPaidRequisition($OrgID, $RequestID) {
    
        $is_paid = "No";
        
        $res_zip_feed = $this->getZipRecruiterFeed($OrgID, $RequestID);
        
        if(isset($res_zip_feed['RequestID']) && $res_zip_feed['RequestID'] != "") {
            $is_paid = "Yes";
            return $is_paid;
        }
        else if($res_zip_feed['RequestID'] == "") {
            $res_zip_subs = $this->getZipRecruiterSubscriptionInfo($OrgID, $RequestID);
            if(isset($res_zip_subs['RequestID']) && $res_zip_subs['RequestID'] != "") {
                $is_paid = "Yes";
                return $is_paid;
            }
        }
        
        return $is_paid;
    }
    
    /**
     * @method      getZipRecruiterFeedCount
     */
    function getZipRecruiterFeedCount($OrgID) {
    
        $params = array(":OrgID"=>$OrgID);
        $sel_zip_recruiter_info = "SELECT COUNT(*) AS ZipFeedCount FROM ZipRecruiterFeed WHERE OrgID = :OrgID";
        $res_zip_recruiter_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_zip_recruiter_info, array($params) );
    
        return $res_zip_recruiter_info;
    }
    
    /**
     * @method		insZipRecruiterSubscriptionSettings
     * @param		array $records_list
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insZipRecruiterSubscriptionSettings($OrgID, $TotalSubscriptions) {
    
        $params = array("OrgID"=>$OrgID, "TotalSubscriptions"=>$TotalSubscriptions, ":UTotalSubscriptions"=>$TotalSubscriptions);
        $ins_zip_recruiter_sub  = "INSERT INTO ZipRecruiterSubscriptionSettings(OrgID, TotalSubscriptions, LastModified) VALUES(:OrgID, :TotalSubscriptions, NOW())";
        $ins_zip_recruiter_sub .= " ON DUPLICATE KEY UPDATE TotalSubscriptions = :UTotalSubscriptions, LastModified = NOW()";
        $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_zip_recruiter_sub, array($params));
    
        return $res_zip_recruiter_sub;
    }

    /**
     * @method		getZipRecruiterSubscriptionSettingsInfo
     * @param		array $record_list
     * @tutorial	
     */
    function getZipRecruiterSubscriptionSettingsInfo($OrgID) {
    
        $params = array("OrgID"=>$OrgID);
        $sel_zip_recruiter_sub = "SELECT * FROM ZipRecruiterSubscriptionSettings WHERE OrgID = :OrgID";
        $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_zip_recruiter_sub, array($params) );
    
        return $res_zip_recruiter_sub;
    }

    /**
     * @method		getZipRecruiterSubscriptionSettingsList
     * @param		array $records_list
     * @tutorial
     */
    function getZipRecruiterSubscriptionSettingsList() {
    
        $sel_zip_recruiter_sub = "SELECT * FROM ZipRecruiterSubscriptionSettings";
        $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_zip_recruiter_sub, array($params) );
    
        return $res_zip_recruiter_sub;
    }

    /**
     * @method		updZipRecruiterSubscriptionSettingCount
     * @param		array $records_list
     * @tutorial
     */
    function updZipRecruiterSubscriptionSettingCount($OrgID, $inc_count, $inc_type) {
    
        if($inc_type != "" && $inc_type == "+") {
            $params = array(":TotalSubscriptions"=>$inc_count, ":OrgID"=>$OrgID);
            $upd_zip_recruiter_sub = "UPDATE ZipRecruiterSubscriptionSettings SET TotalSubscriptions = TotalSubscriptions + :TotalSubscriptions WHERE OrgID = :OrgID";
            $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_zip_recruiter_sub, array($params) );
        }
        else if($inc_type != "" && $inc_type == "-") {
            $params = array(":TotalSubscriptions"=>$inc_count, ":OrgID"=>$OrgID);
            $upd_zip_recruiter_sub = "UPDATE ZipRecruiterSubscriptionSettings SET TotalSubscriptions = TotalSubscriptions - :TotalSubscriptions WHERE OrgID = :OrgID";
            $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_zip_recruiter_sub, array($params) );
        }
    
        return $res_zip_recruiter_sub;
    }
    
    /**
     * @method      insZipRecruiterSubscriptions
     */
    function insZipRecruiterSubscriptions($OrgID, $RequestID) {
    
        $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $ins_zip_recruiter_info = "INSERT INTO ZipRecruiterSubscriptions(OrgID, RequestID, LastModified) VALUES(:OrgID, :RequestID, NOW())";
        $res_zip_recruiter_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $ins_zip_recruiter_info, array($params) );
    
        $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $upd_req_info = "UPDATE Requisitions SET ZipRecruiterSubscription = 'Yes' WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_req_info = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_req_info, array($params) );
        
        return $res_zip_recruiter_info;
    }
    
    /**
     * @method      delZipRecruiterSubscriptions
     */
    function delZipRecruiterSubscriptions($OrgID, $RequestID) {
    
        $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $del_zip_recruiter_info = "DELETE FROM ZipRecruiterSubscriptions WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_zip_recruiter_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_zip_recruiter_info, array($params) );

        $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $upd_req_info = "UPDATE Requisitions SET ZipRecruiterSubscription = '' WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_req_info = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_req_info, array($params) );
        
        return $res_zip_recruiter_info;
    }
    
    /**
     * @method		getZipRecruiterSubscriptionsInfo
     * @param		array $record_list
     * @tutorial
     */
    function getZipRecruiterSubscriptionsInfo($columns = "*", $OrgID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $params = array("OrgID"=>$OrgID);
        $sel_zip_recruiter_sub = "SELECT $columns FROM ZipRecruiterSubscriptions WHERE OrgID = :OrgID";
        $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_zip_recruiter_sub, array($params) );
    
        return $res_zip_recruiter_sub;
    }
    
    /**
     * @method		getZipRecruiterSubscriptionInfo
     * @param		$OrgID, 
     * @param		$RequestID
     */
    function getZipRecruiterSubscriptionInfo($OrgID, $RequestID) {
    
        $params = array("OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $sel_zip_recruiter_sub = "SELECT * FROM ZipRecruiterSubscriptions WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_zip_recruiter_sub, array($params) );
    
        return $res_zip_recruiter_sub;
    }
    
    /**
     * @method		getZipRecruiterSubscriptionsCount
     * @param		$OrgID
     */
    function getZipRecruiterSubscriptionsCount($OrgID) {
    
        $params = array("OrgID"=>$OrgID);
        $sel_zip_recruiter_sub = "SELECT COUNT(*) AS SubscriptionCount FROM ZipRecruiterSubscriptions WHERE OrgID = :OrgID";
        $res_zip_recruiter_sub = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_zip_recruiter_sub, array($params) );
    
        return $res_zip_recruiter_sub['SubscriptionCount'];
    }
    
    /**
     * @method      insZipRecruiterSubscriptionHistory
     * @param       string $OrgID
     * @param       string $RequestID
     * @param       string $Status
     * @return      array
     */
    function insZipRecruiterSubscriptionHistory($OrgID, $RequestID, $Status) {
    
        $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":Status"=>$Status);
        $ins_zip_recruiter_info = "INSERT INTO ZipRecruiterSubscriptionHistory(OrgID, RequestID, Status, LastModified) VALUES(:OrgID, :RequestID, :Status, NOW())";
        $res_zip_recruiter_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $ins_zip_recruiter_info, array($params) );
    
        return $res_zip_recruiter_info;
    }
    
}
?>
