<?php
/**
 * @class		IrecruitUserPreferences
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IrecruitUserPreferences {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getUserPreferences
	 * @param		array $info
	 * @return		array
	 */
	function getUserPreferences($USERID, $columns = "*") {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		// Set parameters for prepared query
		$params = array (":UserID"=>$USERID);
		$user_preferences = "SELECT $columns FROM UserPreferences WHERE UserID = :UserID";
		$preferences_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $user_preferences, array($params) );
		
		return $preferences_info;
	}	

	/**
	 * @method		getUserPreferencesByUserID
	 * @param		$USERID
	 * @return		array
	 */
	function getUserPreferencesByUserID($USERID) {
		
		// Set parameters for prepared query
		$params = array(":UserID"=>$USERID);
		
		$user_preferences = "SELECT * FROM UserPreferences WHERE UserID = :UserID";
		$preferences_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $user_preferences, array ($params) );
		
		if (! isset ( $preferences_info ["UserID"] )) {
			
			$default_settings = $this->getDefaultPreferences ();
			
			if ($default_settings ['AppointmentDuration'] == 0) {
				$duration = 60;
			} else {
				$duration = $default_settings ['AppointmentDuration'];
			}
			
			$sel_def_pref_info = "SELECT * FROM UserPreferences WHERE UserID = 'MASTER'";
			$row_def_pref_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_def_pref_info );
			
			if ($USERID != "") {
				
				// Set parameters for prepared query
				$up_params = array (
                                ":UserID"                       =>  $USERID,
                                ":ThemeID"                      =>  $default_settings ['ThemeID'],
                                ":ReminderType"                 =>  $row_def_pref_info ['ReminderType'],
                                ":ReminderTypeStatus"           =>  $row_def_pref_info ['ReminderTypeStatus'],
                                ":DefaultCalendarView"          =>  $row_def_pref_info ['DefaultCalendarView'],
                                ":DefaultTimeZone"              =>  $row_def_pref_info ['DefaultTimeZone'],
                                ":DefaultSearchDateRange"       =>  $row_def_pref_info ['DefaultSearchDateRange'],
                                ":AppointmentDuration"          =>  $row_def_pref_info ['AppointmentDuration'],
                                ":MonsterNotificationEmail"     =>  $row_def_pref_info ['MonsterNotificationEmail'],
                                ":ApplicantsSearchResultsLimit" =>  $row_def_pref_info ['ApplicantsSearchResultsLimit'],
                                ":ReportsSearchResultsLimit"    =>  $row_def_pref_info ['ReportsSearchResultsLimit'],
                                ":ReportsSearchResultsLimit"    =>  $row_def_pref_info ['ReportsSearchResultsLimit'],
                                ":DashboardWidgetsDateRange"    =>  $row_def_pref_info ['DashboardWidgetsDateRange']
				);
				
				$ins_theme_preferences = "INSERT INTO UserPreferences SET";
				$ins_theme_preferences .= " UserID = :UserID, ThemeID = :ThemeID,";
				$ins_theme_preferences .= " ReminderType = :ReminderType, ReminderTypeStatus = :ReminderTypeStatus,";
				$ins_theme_preferences .= " DefaultCalendarView = :DefaultCalendarView, DefaultTimeZone = :DefaultTimeZone,";
				$ins_theme_preferences .= " DefaultSearchDateRange = :DefaultSearchDateRange, AppointmentDuration = :AppointmentDuration,";
				$ins_theme_preferences .= " MonsterNotificationEmail = :MonsterNotificationEmail, ApplicantsSearchResultsLimit = :ApplicantsSearchResultsLimit,";
				$ins_theme_preferences .= " DashboardWidgetsDateRange = :DashboardWidgetsDateRange,";
				$ins_theme_preferences .= " ReportsSearchResultsLimit = :ReportsSearchResultsLimit";
				
				$res_theme_preferences = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_theme_preferences, array (
						$up_params 
				) );
			}
			
			// Get inserted preferences again
			$preferences_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $user_preferences, array($params) );
		}
		
		return $preferences_info;
	}

	/**
	 * @method 		getUserTheme
	 * @return 		array
	 */
	function getUserTheme() {
		global $USERID;
		
		// Set parameters for prepared select query
		$params = array(":UserID"=>$USERID);
		
		G::Obj('IrecruitUserPreferences')->getUserPreferencesByUserID ( $USERID );
		
		$sel_theme = "SELECT U.*, T.* FROM UserPreferences U, Themes T 
					  WHERE U.UserID = :UserID AND U.ThemeID = T.ThemeID";
		$row_theme = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_theme, array (
				$params 
		) );
		
		if (! isset ( $row_theme ['UserID'] )) {
			$default = $this->getDefaultPreferences ();
			
			// Set parameters for prepared update query
			$up_params = array(":ThemeID"=>$default['ThemeID'], ":UserID"=>$USERID);
			
			$upd_theme_info = "UPDATE UserPreferences SET ThemeID = :ThemeID WHERE UserID = :UserID";
			$res_theme_info = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_theme_info, array ($up_params) );
			
			$row_theme = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_theme, array($params) );
		}
		return $row_theme;
	}

	/**
	 * @method		updateUserPreferences
	 * @return		array
	 * @tutorial	This method will update the UserPreferences
	 * @param		It is a array of post values
	 */
	function updUserPreferences($USERID, $RQ) {
		
		$params = array(":ThemeID"=>$RQ ['ThemesList'],
						":ReminderType"=>$RQ ['ReminderType'],
						":ReminderTypeStatus"=>$RQ ['ReminderStatus'],
						":AppointmentDuration"=>$RQ ['AppointmentDuration'],
						":DefaultCalendarView"=>$RQ ['DefaultCalendarView'],
						":DefaultTimeZone"=>$RQ ['DefaultTimeZone'],
						":DefaultSearchDateRange"=>$RQ ['SearchDateRange'],
						":MonsterNotificationEmail"=>$RQ['MonsterNotificationEmail'],
						":ApplicantsSearchResultsLimit"=>$RQ['ApplicantsSearchResultsLimit'],
		                ":ReportsSearchResultsLimit"=>$RQ['ReportsSearchResultsLimit'],
		                ":DashboardWidgetsDateRange"=>$RQ['DashboardWidgetsDateRange'],
						":UserID"=>$USERID);
		
		$upd_user_pref = "UPDATE UserPreferences
						SET ThemeID	= :ThemeID,
						ReminderType = :ReminderType,
						ReminderTypeStatus = :ReminderTypeStatus,
						AppointmentDuration = :AppointmentDuration,
						DefaultCalendarView = :DefaultCalendarView,
						DefaultTimeZone = :DefaultTimeZone,
						DefaultSearchDateRange = :DefaultSearchDateRange,
						MonsterNotificationEmail = :MonsterNotificationEmail,
						ApplicantsSearchResultsLimit = :ApplicantsSearchResultsLimit,
		                ReportsSearchResultsLimit = :ReportsSearchResultsLimit,
		                DashboardWidgetsDateRange = :DashboardWidgetsDateRange
						WHERE  UserID = :UserID";
		$res_preferences = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_user_pref, array($params) );
		return $res_preferences;
	}

	/**
	 * @method	setAdminDefaultTheme
	 */
	function setAdminDefaultTheme($ThemeID) {
		
        $params     =	array(":ThemeID"=>$ThemeID);
        $upd_def    =	"UPDATE UserPreferences SET ThemeID = :ThemeID WHERE UserID = 'MASTER'";
        $result		=	$this->db->getConnection ( "IRECRUIT" )->update ( $upd_def, array($params) );

		return $result;
	}

	/**
	 * @method getDefaultPreferences
	 * @return array
	 */
	function getDefaultPreferences() {
		
		$sel_default	=	"SELECT UP.* FROM UserPreferences UP, Themes T WHERE UP.UserID = 'MASTER' AND T.ThemeID = UP.ThemeID";
		$row_default	=	$this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_default );
		
		if (! isset ( $row_default ['UserID'] )) {
			$update_default =	"UPDATE UserPreferences SET ThemeID = (SELECT ThemeID FROM Themes ORDER BY ThemeID ASC LIMIT 1) WHERE UserID = 'MASTER'";
			$result_default =	$this->db->getConnection ( "IRECRUIT" )->update ( $update_default );
			$row_default	=	$this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_default ); //Pull the data again after update
		}
		
		return $row_default;
	}

	/**
	 * @method 		getThemes
	 * @return 		array
	 * @tutorial 	This method will retrieve the Themes List
	 * 			 	We are using the connection identifier explicitly
	 * 			 	for this Themes table because it is accessing in admin section,
	 * 			 	there the default connection identifier is wotc schema
	 */
	function getThemes() {
		
		$sel_themes = "SELECT * FROM Themes";
		$row_themes = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_themes );
		
		return $row_themes['results'];
	}

	/**
	 * @method 		getThemeInfoByThemeID
	 * @return 		array
	 * @tutorial 	This method will retrieve the Themes List
	 * 			 	We are using the connection identifier explicitly
	 * 			 	for this Themes table because it is accessing in admin section,
	 * 			 	there the default connection identifier is wotc schema
	 */
	function getThemeInfoByThemeID($ThemeID) {
		
		$params		=	array(":ThemeID"=>$ThemeID);
		$sel_theme	=	"SELECT * FROM Themes WHERE ThemeID = :ThemeID";
		$row_theme	=	$this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_theme, array($params) );
		
		return $row_theme;
	}

    /**
     * @method      insThemesInfo
     * @param       $info
     */
    function insThemesInfo($info) {
    
        $ins_app_data   =   $this->db->buildInsertStatement("Themes", $info);
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data["stmt"], $ins_app_data["info"] );
    
        return $res_app_data;
	}
	
	/**
     * @method      updThemesInfo
     * @param       $set_info, $where_info, $info
     */
    function updThemesInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_applications_info = $this->db->buildUpdateStatement("Themes", $set_info, $where_info);
        $res_applications_info = $this->db->getConnection( $this->conn_string )->update($upd_applications_info, $info);
    
        return $res_applications_info;
	}
	
	/**
	 * @method		delThemeByThemeID
	 * @param		$where_info, $info
	 */
	function delThemeByThemeID($ThemeID) {

		$params		=	array(":ThemeID"=>$ThemeID);
		$del_info	=	"DELETE FROM Themes WHERE ThemeID = :ThemeID";
		$res_info	=	$this->db->getConnection ( $this->conn_string )->delete($del_info, array($params));
		
		return $res_info;
	}

}
