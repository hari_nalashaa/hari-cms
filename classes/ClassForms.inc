<?php
/**
 * @class		Forms
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Forms {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		updFormTableQuestionOrder
     * @param		$set_que_order, $formtable, $section, $con_que_order, $form
     */
    public function updFormTableQuestionOrder($set_que_order, $formtable, $section, $con_que_order, $form) {
        global $OrgID;
        
        $params = array(':SetQuestionOrder'=>$set_que_order, ':ConQuestionOrder'=>$con_que_order, ':OrgID'=>$OrgID, ':FormID'=>$form);
        
        $upd_form_table_que_order  = "UPDATE $formtable SET QuestionOrder = :SetQuestionOrder ";
        $upd_form_table_que_order .= "WHERE OrgID = :OrgID AND FormID = :FormID";
        
        if ($formtable == "FormQuestions") {
            $params[':SectionID'] = array($section);
            $upd_form_table_que_order .= " AND SectionID = :SectionID";
        }
        $upd_form_table_que_order .= " AND QuestionOrder = :ConQuestionOrder";
        
        //Update Question Order of Form Tables
        $res_que_order = $this->db->getConnection( $this->conn_string )->update($upd_form_table_que_order, array($params));
        
        return $res_que_order;
    }
    
    /**
     * @method		updFormTableInfo
     * @param		$formtable, $set, $info
     */
    public function updFormTableInfo($formtable, $set, $info) {
        
        $upd_form_table_info  = "UPDATE " . $formtable . " SET ";
        $upd_form_table_info .= $set;
        $upd_form_table_info .= " WHERE OrgID = :OrgID AND FormID = :FormID AND QuestionID = :QuestionID";
        
        //Update Question Order of Form Tables
        $res_form_table_info = $this->db->getConnection( $this->conn_string )->update($upd_form_table_info, $info);
        
        return $res_form_table_info;
    }
    
    /**
     * @method		updFormsInfo
     * @param		$set_info = array(), $where_info = array(), $info
     */
    public function updFormsInfo($table_name, $set_info = array(), $where_info = array(), $info) {
        
        $upd_que_info = $this->db->buildUpdateStatement($table_name, $set_info, $where_info);
        $res_que_info = $this->db->getConnection( $this->conn_string )->update($upd_que_info, $info);
        
        return $res_que_info;
    }
    
    /**
     * @method		delFormsInfo
     * @param		$where_info = array(), $info
     */
    public function delFormsInfo($table_name, $where_info = array(), $info) {
        
        //Set parameters for prepared query
        $del_forms_info = "DELETE FROM $table_name";
        
        if(count($where_info) > 0) $del_forms_info .= " WHERE " . implode(" AND ", $where_info);
        $res_forms_info = $this->db->getConnection( $this->conn_string )->delete($del_forms_info, $info);
        
        return $res_forms_info;
        
    }
    
    /**
     * @method		insFormsInfo
     * @param		$table_name, $info
     */
    public function insFormsInfo($table_name, $info) {
        
        $ins_forms_info = $this->db->buildInsertStatement($table_name, $info);
        $res_forms_info = $this->db->getConnection ( $this->conn_string )->insert ( $ins_forms_info["stmt"], $ins_forms_info["info"] );
        
        return $res_forms_info;
    }
    
    /**
     * @method		getFormTableInfo
     * @param		$formtable, $OrgID, $form, $section
     */
    public function getFormTableInfo($formtable, $OrgID, $form, $section) {
        
        //Bind parameters
        $params = array(':OrgID'=>$OrgID);
        
        $sel_frm_table_info  = "SELECT * FROM " . $formtable . " WHERE OrgID = :OrgID";
        //List FormQuestions by section
        if ($formtable == "OnboardQuestions") {
            $sel_frm_table_info .= " AND OnboardFormID = :OnboardFormID";
            $params[':OnboardFormID'] = $form;
        }
        else {
            $sel_frm_table_info .= " AND FormID = :FormID";
            $params[':FormID'] = $form;
        }
        
        $sel_frm_table_info .= " AND QuestionTypeID != '10'";
        
        //List FormQuestions by section
        if ($formtable == "FormQuestions") {
            $params[':SectionID'] = $section;
            $sel_frm_table_info .= " AND SectionID = :SectionID";
        }
        
        $sel_frm_table_info .= " ORDER BY QuestionOrder";
        
        //List FormQuestions by section
        $res_form_table_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_frm_table_info, array($params));
        
        return $res_form_table_info;
    }

}
