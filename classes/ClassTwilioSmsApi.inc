<?php
/**
 * @class		TwilioSmsApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioSmsApi {
	
    public $db;
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	var $twilio_number			=	"+13134665957";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

	/**
	 * @method		sendSms
	 */
    public function sendSms($OrgID, $to_number, $sms_message) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	   
    	try {
    		$message_info	=	$client->messages->create(
					    				// Where to send a text message (your cell phone?)
					    				$to_number, //'+18606145063',
					    				array(
											'body'					=>	$sms_message,
				    						//"messagingServiceSid"	=>	$account_info['MessageServiceID'],
					    					"from"					=>	$this->twilio_number
					    				)
					    		);
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-sent-info.txt", serialize($message_info), "w+", false);
    		
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully sent this message", "Response"=>$message_info);
    	
    	}
		catch (RestException | TwilioException | Exception $e) {

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-sent-info-error.txt", serialize($message_info), "w+", false);
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to send this message", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	
    }

    /**
     * @method		sendMMS
     */
    public function sendMMS($OrgID, $to_number, $message, $image_url) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
    		$message_info	=	$client->messages
                  					->create($to_number, // to
			                           [
			                               "body" => $message,
			                               "from" => "+18509205621",
			                               "mediaUrl" => [$image_url]
			                           ]
                  				);
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-sent-info.txt", serialize($message_info), "w+", false);
    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully sent the mms message", "Response"=>$message_info);
    		 
    	}
    	catch (RestException | TwilioException | Exception $e) {
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-sent-info-error.txt", serialize($message_info), "w+", false);
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to sent the mms message", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    /**
     * @method		getAllMessages
     * @return		array
     */
    public function getAllMessages($OrgID) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
    		$messages	=	$client->messages->read(array(), 20);

    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully retrieved the messages.", "Response"=>$messages);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to retrieved the messages.", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    /**
     * @method		fetchMediaInfo
     */
    public function fetchMediaInfo($OrgID, $message_id, $media_id) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client		=	new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
			$media	=	$client->messages($message_id)
				                ->media($media_id)
				                ->fetch();
    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully retrieved the media information.", "Response"=>$media);
    		 
    	}
    	catch (RestException | TwilioException | Exception $e) {
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to retrieve the media information.", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	
    }
} // end Class
