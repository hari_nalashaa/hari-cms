<?php
/**
 * @class		IconnectFormsReminderSettings
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class IconnectFormsReminderSettings {

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

    /**
     * @method  insUpdIconnFormsReminderSettings
     * @param   array $params
     * @return  $res_data
     */	
    public function insUpdIconnFormsReminderSettings($info) {
        
        $ins_data   =   "INSERT INTO IconnectFormsReminderSettings(OrgID, MultiOrgID, RequestID, ReminderNotification, NumberOfDays)";
        $ins_data  .=   " VALUES(:OrgID, :MultiOrgID, :RequestID, :ReminderNotification, :NumberOfDays)";
        $ins_data  .=   " ON DUPLICATE KEY UPDATE ReminderNotification = :UReminderNotification, NumberOfDays = :UNumberOfDays";
        $res_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_data, array($info) );
        
        return $res_data;
    }
    
    /**
     * @method  updLastRunDate
     * @param   array $params
     * @return  $res_data
     */
    public function updLastRunDate($OrgID, $MultiOrgID, $RequestID) {
    
        $params     =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        $upd_date   =   "UPDATE IconnectFormsReminderSettings SET LastRunDateTime = DATE_SUB(NOW(), INTERVAL 1 HOUR) WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_date   =   $this->db->getConnection ( $this->conn_string )->insert ( $upd_date, array($params) );
    
        return $res_date;
    }
    
    /**
     * @method  getIconnectFormsReminderSettings
     * @param   array $params
     * @return  $res_data
     */
    public function getIconnectFormsReminderSettings($OrgID, $MultiOrgID, $RequestID) {

        $params     =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        $sel_info   =   "SELECT * FROM IconnectFormsReminderSettings WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    
        return $res_info;
    }
    
    /**
     * @method  getIconnFormsRemindersList
     * @return  $res_info
     */
    public function getIconnFormsRemindersList() {

        $sel_info = "SELECT IconnectFormsReminderSettings.OrgID, IconnectFormsReminderSettings.RequestID FROM IconnectFormsReminderSettings";
        $sel_info .= " JOIN Requisitions ON IconnectFormsReminderSettings.OrgID = Requisitions.OrgID AND IconnectFormsReminderSettings.RequestID = Requisitions.RequestID";
        $sel_info .= " WHERE (DATEDIFF(NOW(), IconnectFormsReminderSettings.LastRunDateTime) >= IconnectFormsReminderSettings.NumberOfDays OR IconnectFormsReminderSettings.LastRunDateTime = '0000-00-00 00:00:00') AND IconnectFormsReminderSettings.ReminderNotification = 'On'";
        $sel_info .= " AND Requisitions.Active = 'Y' AND Requisitions.PostDate <= NOW() AND Requisitions.ExpireDate >= NOW();";
    
        $res_info   =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info );
    
        return $res_info;
    }
    
    /**
     * @method  getNextRunDate
     * @return  $res_data
     */
    public function getNextRunDate($OrgID, $MultiOrgID, $RequestID) {
    
        $ifrs = $this->getIconnectFormsReminderSettings($OrgID, "", $RequestID);
        
        $number_of_days     = $ifrs['NumberOfDays'];
        $last_run_date_time = $ifrs['LastRunDateTime'];
        
        if($number_of_days != "" && $last_run_date_time != "") {
            $sel_info   =   "SELECT DATE_ADD('$last_run_date_time', INTERVAL $number_of_days DAY) AS NextRunDate";
            $res_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info );
        }
    
        return $res_info['NextRunDate'];
    }
}
?>
