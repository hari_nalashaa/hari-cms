<?php 
/**
 * @class		MysqlHelper
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class MysqlHelper {
	
	public $db;
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
	    $this->db = Database::getInstance ();
	    $this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method 		getDateTime
	 * @tutorial	Get the date and time from databse based on
	 * 			 	given format
	 */
	public function getDateTime($date_format = '%m/%d/%Y') {
		
		$sel_date_time = "SELECT DATE_FORMAT(NOW(), '$date_format')";
		$res_date_time = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date_time);
		
		return $res_date_time[0];
	}
	
	/**
	 * @method		getDateTimeFromString
	 * @param		$string, $date_time_format
	 */
	public function getDateTimeFromString($date_string, $date_time_format) {
		
		$sel_date_time = "SELECT STR_TO_DATE('$date_string', '$date_time_format')";
		$res_date_time = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date_time);
		
		return $res_date_time[0];
	}
	
	/**
	 * @method		getTimeFormat
	 * @param		$time_string
	 */
	public function getTimeFormat($time_string, $time_format = '%S') {
		
		$sel_date_time = "SELECT time_format($time_string, '$time_format')";
		$res_date_time = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date_time);
		
		return $res_date_time[0];
	}
	
	/**
	 * @method		getNow
	 * @param		$string, $date_time_format
	 */
	public function getNow() {
		
		$sel_date_time = "SELECT NOW()";
		$res_date_time = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date_time);
	
		return $res_date_time[0];
	}
	
	/**
	 * @method		getTime
	 * @param		$string, $date_time_format
	 */
	public function getTime() {
		
		$sel_date_time = "SELECT TIME(NOW())";
		$res_date_time = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date_time);
	
		return $res_date_time[0];
	}
	
	/**
	 * @tutorial	Get the date difference from present date and given date
	 */
	public function getDateDiffWithNow($date) {
		
		$sel_date_time = "SELECT DATEDIFF( NOW(), '$date' )";
		$res_date_time = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date_time);
	
		return $res_date_time[0];
	}
	
	/**
	 * @method 		getCurDate
	 * @tutorial	Get the current date from database
	 */
	public function getCurDate() {
		
		$sel_date = "SELECT CURDATE()";
		$res_date = $this->db->getConnection ( $this->conn_string )->fetchRow($sel_date);

		return $res_date[0];
	}
	
	/**
	 * @method		getDatesList
	 */
	public function getDatesList($columns, $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_dates_list = "SELECT $columns";
		$res_dates_list = $this->db->getConnection ( $this->conn_string )->fetchAssoc($sel_dates_list, $info);
		
		return $res_dates_list;
	}
}
?>
