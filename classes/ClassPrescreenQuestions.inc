<?php
/**
 * @class		PrescreenQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class PrescreenQuestions {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method	getPrescreenQuestionsInformation
     * @return	associative array
     * @param	string $OrgID
     */
    function getPrescreenQuestionsInformation($columns = "", $where_info = array(), $order_by = "", $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $sel_prescreen_que_info = "SELECT $columns FROM PrescreenQuestions";
        if(count($where_info) > 0) {
            $sel_prescreen_que_info .= " WHERE " . implode(" AND ", $where_info);
        }
        
        if($order_by != "") $sel_prescreen_que_info .= " ORDER BY " . $order_by;
        
        $res_prescreen_que_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_prescreen_que_info, $info );
        
        return $res_prescreen_que_info;
    }
    
    /**
     * @method		getPrescreenResults
     * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
     */
    function getPrescreenResults($columns = "", $where_info = array(), $order_by = "", $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $sel_prescreen_results = "SELECT $columns FROM PrescreenResults";
        if(count($where_info) > 0) {
            $sel_prescreen_results .= " WHERE " . implode(" AND ", $where_info);
        }
        
        if($order_by != "") $sel_prescreen_results .= " ORDER BY " . $order_by;
        $res_prescreen_results = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_prescreen_results, $info );
        
        return $res_prescreen_results;
    }
    
    /**
     * @method		insUpdPrescreenResults
     * @param		$info
     */
    function insUpdPrescreenResults($params) {
        
        $insert_prescreen  =   $this->db->buildInsertStatement('PrescreenResults', $params);
        $ins_statement     =   $insert_prescreen["stmt"] . " ON DUPLICATE KEY UPDATE Submission = :USubmission, EntryDate = NOW()";
        $params            =   $insert_prescreen["info"];
        $params[0][':USubmission'] = $params[0][':Submission'];
        
        $result_prescreen  =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, $params );
        
        return $result_prescreen;
    }
    
    /**
     * @method		insPrescreenQuestions
     * @param		$info
     */
    function insPrescreenQuestions($info) {
        
        $insert_prescreen = $this->db->buildInsertStatement('PrescreenQuestions', $info);
        $result_prescreen = $this->db->getConnection ( $this->conn_string )->insert($insert_prescreen['stmt'], $insert_prescreen['info']);
        return $result_prescreen;
    }
    
    /**
     * @method		updPrescreenResults
     * @param		$info
     */
    function updPrescreenResults($set_info = array(), $where_info = array(), $info) {

            $upd_pre_screen_results = $this->db->buildUpdateStatement('PrescreenResults', $set_info, $where_info);
            $res_pre_screen_results = $this->db->getConnection("IRECRUIT")->update($upd_pre_screen_results, $info);

            return $res_pre_screen_results;
    }
    
    /**
     * @method		getPrescreenTextInfo
     * @param		$OrgID
     * @return		array
     */
    function getPrescreenTextInfo($OrgID, $where_info = array(), $order_by = "") {
        
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID);
        
        $sel_prescreen_text = "SELECT * FROM PrescreenText";
        
        if(count($where_info) > 0) $sel_prescreen_text .= " WHERE " . implode(" AND ", $where_info);
        if($order_by != "") $sel_prescreen_text .= " ORDER BY " . $order_by;
        
        $row_prescreen_text = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_prescreen_text, array($params));
        
        return $row_prescreen_text;
    }
    
    /**
     * @method		delPrescreenTextInfo
     * @param		$OrgID
     * @return		array
     */
    function delPrescreenTextInfo($OrgID) {
        
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID);
        // escape input values from sql injection
        $del_prescreen_text = "DELETE FROM PrescreenText WHERE OrgID = :OrgID";
        $res_prescreen_text = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $del_prescreen_text, array($params));
        
        return $res_prescreen_text;
    }
    
    /**
     * @method		insPrescreenTextInfo
     * @param		$info
     * @return		array
     */
    function insPrescreenTextInfo($info) {
        
        $ins_prescreen_text = $this->db->buildInsertStatement('PrescreenText', $info);
        $res_prescreen_text = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_prescreen_text["stmt"], $ins_prescreen_text["info"] );
        
        return $res_prescreen_text;
    }
}
