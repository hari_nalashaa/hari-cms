<?php
/**
 * @class		WOTCApp
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCApp
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method      getApplicantData
     * @param       string $WOTCID
     * @param       string $social1
     * @param       string $social2
     * @param       string $social3
     * @return      array
     */
    public function getApplicantData($WOTCID, $social1, $social2, $social3)
    {
        $sel_app    =   "SELECT * FROM ApplicantData WHERE wotcID = :wotcID AND Social1 = :Social1 AND Social2 = :Social2 AND Social3 = :Social3";
        $params     =   array(':wotcID'=>$WOTCID, ':Social1'=>$social1, ':Social2'=>$social2, ':Social3'=>$social3);
        $app_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($sel_app, array($params));
        
        return $app_info;
    }

    /**
     * @method      validatewotcID
     * @return      string
     */
    public function validatewotcID()
    {
        if ($_REQUEST['wotcID']) {
            $id = $_REQUEST['wotcID'];
        }
        else if ($_COOKIE['VID']) 
        {
            $id = $_COOKIE['VID'];
        }
        
        $query          =   "SELECT wotcID, Active, Callcenter";
        $query 	        .=  " FROM OrgData WHERE wotcID = :wotcid AND Active in ('N', 'Y')";
        $params         =   array(':wotcid'=>$id);
        list ($wotcID, $Active, $Callcenter)  =   $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
       
	if (($wotcID) && ($Active == "Y")) {
            setcookie("VID", $wotcID, time() + 7200, "/");
            return $wotcID;
	} else {
            setcookie("VID", "", time() - 3600, "/");
            $rtn = "";
            $rtn .= G::Obj('WOTC')->wotcHeader('');
            if (($Active == "N") && ($Callcenter == "Y")) {
              $rtn .= "<span style=\"color:red;font-style:italic;\">";
              $rtn .= "This company is currently not taking applications.";
              $rtn .= "</span>";
            } else {
		$rtn .= "<div style=\"line-height:200%;padding:10px;\">Dear Employee,<br>Please tell your Hiring Manager, HR representative or Employer to <span style=\"color:red;\">Contact CMS</span> directly to resolve this issue regarding WOTC screening.</div>\n";
	    }
            $rtn .= "</div>\n</body>\n</html>\n";
            echo $rtn;
            exit();
	}
    } // end function

    /**
     * @method		isWotcIDExists
     */
    function isWotcIDExists($id = '') {
    	if($id == '') {
    		if ($_REQUEST['wotcID']) {
    			$id = $_REQUEST['wotcID'];
    		}
    		else if ($_COOKIE['VID'])
    		{
    			$id = $_COOKIE['VID'];
    		}
    	}

    	$query          =   "SELECT wotcID FROM OrgData WHERE wotcID = :wotcid AND Active = 'Y'";
    	$params         =   array(':wotcid'=>$id);
    	list ($wotcID)  =   $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
    	
    	if (! $wotcID) {
    		setcookie("VID", "", time() - 3600, "/");
    		return "";
    	}
		else {
			setcookie("VID", $wotcID, time() + 7200, "/");
			return $wotcID;
		}    		 
    }
    
    /**
     * @method      letterPage
     * @param       string $wotcID
     * @return      string
     */
    public function letterPage($wotcID)
    {

	if ($_COOKIE['LN'] == "sp") {
	  $language="SPANISH";
	} else {
	  $language="MASTER";
	}

        $rtn = "";
        
        $query  =   "SELECT iRecruitOrgID, iRecruitMultiOrgID";
        $query .=   " FROM OrgData WHERE wotcID = :wotcid";
        $params =   array(':wotcid' => $wotcID);

        list ($iRecruitOrgID, $iRecruitMultiOrgID) = $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));

	$PARENT = G::Obj('WOTCcrm')->getParentInfo($wotcID);
        $OrganizationName=$PARENT['CompanyName'];

        $PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($wotcID);
        $PartnerName=$PARTNER['CompanyName'];

	$RESTRICT = G::Obj('WOTCcrm')->getWOTCIDs('62f41708245691'); // Dycom Industries
        
        $OrganizationLetter   =   G::Obj('WOTCDisplayText')->getOrganizationLetter($language);
        
        $rtn .= "<div class=\"row\">\n";
        $rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
        $rtn .= str_replace("{OrganizationName}", $OrganizationName, $OrganizationLetter);
        $rtn .= "<br><br>";
        $rtn .= "</div>\n";
        $rtn .= "</div>\n";

        $IRF = G::Obj('WOTCDisplayText')->getiRecruitForm($language);

        $rtn .= "<form method=\"POST\" action=\"form.php\">\n";
        
        if ($iRecruitOrgID) {


            $rtn .= "<div style=\"background-color:#eeeeee;margin:20px 15px 20px 15px;padding:10px 3px 10px 3px;\">\n";
            
            $rtn .= "<div class='row'>\n";
            $rtn .= "<div class='col-lg-12 col-md-12 col-sm-12'>\n";

	    $rtn .= $IRF['opt'] . "<br>\n";

            $rtn .= "</div>\n";
            $rtn .= "</div>\n";
            
            $rtn .= "<div class='row' style='margin-bottom:5px; margin-top:15px;'>\n";
            $rtn .= "<div class='col-lg-3 col-md-3 col-sm-3'>\n";
	    $rtn .= $IRF['appid'];
            $rtn .= "</div>\n";
            $rtn .= "<div class='col-lg-9 col-md-9 col-sm-9'>\n";
            $rtn .= "<input type=\"text\" name=\"IrecruitApplicationID\" value=\"" . $_REQUEST['IrecruitApplicationID'] . "\" size=\"20\" maxlength=\"20\">\n";
            $rtn .= "</div>\n";
            $rtn .= "</div>\n";
            
            $rtn .= "<div class='row' style='margin-bottom:5px;'>\n";
            $rtn .= "<div class='col-lg-3 col-md-3 col-sm-3'>\n";
	    $rtn .= $IRF['last'];
            $rtn .= "</div>\n";
            $rtn .= "<div class='col-lg-9 col-md-9 col-sm-9'>\n";
            $rtn .= "<input type=\"text\" name=\"lname\" value=\"" . $_REQUEST['lname'] . "\" size=\"20\" maxlength=\"20\">\n";
            $rtn .= "<input type=\"hidden\" name=\"iRecruitOrgID\" value=\"" . $iRecruitOrgID . "\">\n";
            $rtn .= "<input type=\"hidden\" name=\"iRecruitMultiOrgID\" value=\"" . $iRecruitMultiOrgID . "\">\n";

            $rtn .= "</div>\n";
            $rtn .= "</div>\n";
            
            $rtn .= "</div>\n";

        } // end iRecruitOrgID
        
        $rtn .= "<div class=\"row\">\n";
        $rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
        $rtn .= "<input type=\"hidden\" name=\"Initiated\" value=\"" . $_REQUEST['Initiated'] . "\">\n";
        $rtn .= "<input type=\"submit\" class=\"btn btn-primary\" value=\"" . $IRF['lettersubmit'] . "\">\n";
        $rtn .= "</div>\n";
        $rtn .= "</div>\n";
        
        $rtn .= "</form>\n";

        
        $company = "Cost Management Services, LLC";
        
        if ($PartnerName) {
            $company = $PartnerName . " & CMS";
            if ($PartnerName == "Paycor") {
                $company = $PartnerName;
            }
            if ($PartnerName == "iNetHR") {
                // $company = "CMS";
            }
            if ($PartnerName == "NEEPAA") {
                $company = $PartnerName;
            }
        }
        
        $rtn .= "<div class=\"row\">\n";
        $rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
        $rtn .= "<br>";

	$RetainInfo =   G::Obj('WOTCDisplayText')->getRetainInfo($language);

	$rtn .= str_replace("{Company}", $company, $RetainInfo);
        
        $rtn .= "</div>\n";
        $rtn .= "</div>\n";
        
        if (! in_array($wotcID, $RESTRICT)) {

	    $EEO =   G::Obj('WOTCDisplayText')->getEEO($language);

            $rtn .= "<div class=\"row\" style=\"margin-bottom:20px;\">\n";
            $rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
            
	    $rtn .= str_replace("{OrganizationName}", $OrganizationName, $EEO);

            $rtn .= "</div>\n";
            $rtn .= "</div>\n";

        } // end RESTRICT
        
        return $rtn;
    } // end function
    
    /**
     * @method      checkDuplicate
     */
    public function checkDuplicate()
    {
        $WOTCID = $this->validatewotcID();
        
        // clean up db from older entries
        $query  =   "DELETE FROM ApplicantLock WHERE EntryDate < DATE_ADD(NOW(), INTERVAL -5 MINUTE)";
        $this->db->getConnection ( $this->conn_string )->delete($query);
        
        // check new input to old value
        $query  =   "SELECT CONCAT(FirstName, LastName, Address, City, State, ZipCode)";
        $query .=   " FROM ApplicantLock";
        $query .=   " WHERE FirstName = :FirstName";
        $query .=   " AND LastName = :LastName";
        $query .=   " AND Address = :Address";
        $query .=   " AND City = :City";
        $query .=   " AND State = :State";
        $query .=   " AND ZipCode = :ZipCode";
        $params =   array(
                        ':FirstName'    =>  $_POST['FirstName'],
                        ':LastName'     =>  $_POST['LastName'],
                        ':Address'      =>  $_POST['Address'],
                        ':City'         =>  $_POST['City'],
                        ':State'        =>  $_POST['State'],
                        ':ZipCode'      =>  $_POST['ZipCode']
                    );
        list ($appentered) = $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
        
        $app_info = G::Obj('WOTCApp')->getApplicantData($WOTCID, $_POST['Social1'], $_POST['Social2'], $_POST['Social3']);
        
        // if available error otherwise add to lock
        if ($app_info['wotcID'] != "") {
            
            $rtn = "";
            $rtn .= G::Obj('WOTC')->wotcHeader($WOTCID);
            
            $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;";
            $rtn .= "You previously submitted an application to the WOTC program on " . date('F j, Y', strtotime($app_info['EntryDate'])) . "<br><br>";

	    $PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
            $displaycode=$PARENT['DisplayCode'];
            
            $codereceipt = "N";
            
            if ($app_info['Initiated'] == "callcenter") {
                $codereceipt = "Y";
            }
            
            if ($_REQUEST['Initiated'] == "callcenter") {
                $codereceipt = "Y";
            }
            if ($displaycode == "Y") {
                $codereceipt = "Y";
            }
            
            if ($codereceipt == "Y") {
                
                $query  =   "SELECT Qualifies FROM ApplicantData";
                $query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
                $params =   array(':wotcid'=>$WOTCID, ':applicationid'=>$app_info['ApplicationID']);
                list ($CCQ) =   $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
                
                $CCCODE = array('Y' => '1','N' => '2');
                $CCID = strtoupper(substr($WOTCID,0,2)) . str_pad(preg_replace("/\D/","",$WOTCID),2,'00', STR_PAD_LEFT) . substr($app_info['ApplicationID'], - 4) . $CCCODE[$CCQ];
                $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;";
                $rtn .= "Please give this ID to your supervisor: ";
                $rtn .= "<strong style=\"font-size:14pt;color:red;\">" . $CCID . "</strong>";
                $rtn .= "\n";
            } // end codereceipt
            
            $rtn .= "</div>\n</body>\n</html>\n";
            echo $rtn;
            exit();
        } else 
            if ($appentered != "") {
                
                $rtn = "";
                $rtn .= G::Obj('WOTC')->wotcHeader($WOTCID);
                $rtn .= "An application with this information has recently been submitted.<br>\n";
                $rtn .= "</div>\n</body>\n</html>\n";
                echo $rtn;
                exit();
            } else {
                
                $query  =   "INSERT INTO ApplicantLock";
                $query .=   " (EntryDate, FirstName, LastName, Address, City, State, ZipCode)";
                $query .=   " VALUES (NOW(), :FirstName, :LastName, :Address, :City, :State, :ZipCode)";
                $params =   array(
                                ':FirstName'    =>  $_POST['First'],
                                ':LastName'     =>  $_POST['Last'],
                                ':Address'      =>  $_POST['Address'],
                                ':City'         =>  $_POST['City'],
                                ':State'        =>  $_POST['State'],
                                ':ZipCode'      =>  $_POST['ZipCode']
                            );
                $this->db->getConnection ( $this->conn_string )->insert($query, array($params));
            }
    } // end function
    
    /**
     * @method      processThankYou
     * @param       string $WOTCID
     * @param       string $ApplicationID
     * @return      string
     */
    public function processThankYou($WOTCID, $ApplicationID)
    {

	if ($_COOKIE['LN'] == "sp") {
          $language="SPANISH";
        } else {
          $language="MASTER";
	}

	$RES = G::Obj('WOTCDisplayText')->getReceipt($language);

        $rtn        =   "<div style=\"margin-left:50px;\">\n";
        
        $query      =   "SELECT * FROM ApplicantData WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params     =   array(':wotcid'=>$WOTCID, ':applicationid'=>$ApplicationID);
        $APPDATA    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));

        $query      =   "SELECT iRecruitOrgID, iRecruitMultiOrgID";
        $query      .=  " FROM OrgData WHERE wotcID = :wotcid";
        $params     =   array(':wotcid'=>$WOTCID);
        list ($iRecruitOrgID, $iRecruitMultiOrgID) = $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
        
	$PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
        $displaycode=$PARENT['DisplayCode'];
        $employmentconfirmation=$PARENT['EmploymentConfirmation'];
	$OrganizationName=$PARENT['CompanyName'];

        $PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($wotcID);
        $PartnerName=$PARTNER['CompanyName'];
        
        $codereceipt    =   "N";
        $confirmation   =   "N";
        
        if ($displaycode == "Y") {
            $codereceipt = "Y";
        }
       
        if ($_POST['Initiated'] == "callcenter") {
            $codereceipt = "Y";
            if (($_POST['Initiated'] == 'callcenter') && ($employmentconfirmation == "Y")) {
                $confirmation = "Y";
            }
        }
       
        if ((($APPDATA['EnterpriseZone'] == 'Y') && ($APPDATA['DOB'] == 'Y')) || ($APPDATA['Processed'] == 'R')) {
            
            $query  =   "UPDATE ApplicantData SET Qualifies = 'Y', Processed = 'P', Processed_Date = now()";
            $query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
            $params =   array(':wotcid'=>$WOTCID, ':applicationid'=>$ApplicationID);
            $this->db->getConnection ( $this->conn_string )->update($query, array($params));
            
            $rtn .= $RES['ty1'] ."<br><br>\n";
            if ($confirmation != "Y") {
                $rtn .= $RES['ty2'] . "<br><br>\n";
            }
        } else { // else not qualified
            
            $query = "UPDATE ApplicantData SET Qualifies = 'N', Processed_Date = now()";
            $query .= " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
            $params =   array(':wotcid'=>$WOTCID, ':applicationid'=>$ApplicationID);
            $this->db->getConnection ( $this->conn_string )->update($query, array($params));
            
            $rtn .= $RES['ty3'] . "<br><br>\n";
            
            if ($confirmation == "Y") {
                $rtn .= $RES['ty4'] . "<br><br>\n";
		    } else {
	        	$rtn .= $RES['ty5'] . "<br><br>\n";
		    }
            
            $confirmation = "N";
        } // end qualified
        
        if ($confirmation == "Y") {
            
            $rtn .= $RES['ty6'] . "<br><br>\n";
            
            $LST = array();
            array_push($LST, "offerdatep");
            array_push($LST, "hireddatep");
            array_push($LST, "startdatep");
            
            $rtn .= G::Obj('DateHelper')->dynamicCalendar($LST, '');
            
            $rtn .= <<<END
<table>
<form method="POST" action="empconf.php" onsubmit="return validate_hireform(this)">
<tr><td align="right">
Offer Date:
</td><td>
<input id="offerdatep" name="offerdatep" type="text" value="" size="10" onBlur="validate_date(this.value,'Offer Date');" />
</td></tr>

<tr><td align="right">
Hired Date:
</td><td>
<input id="hireddatep"  name="hireddatep" type="text" value="" size="10" onBlur="validate_date(this.value,'Hired Date');" />
</td></tr>

<tr><td align="right">
Start Date:
</td><td>
<input id="startdatep" name="startdatep" type="text" value="" size="10" onBlur="validate_date(this.value,'Start Date');" />
</td></tr>

<tr><td align="right">
Starting Wage:
</td><td>
<input name="startingwagep" type="text" value="" size="10" />
</td></tr>

<tr><td align="right">
Position:
</td><td>
<input name="positionp" size="25" maxlength="255" type="text" value="" size="10" />
</td></tr>

<tr><td colspan="2">
<br><input value="$ApplicationID" name="ApplicationID" type="hidden">
<input value="$WOTCID" name="wotcID" type="hidden">
<input type="submit" class="btn btn-primary" value="Add New Hire Information">
</td></tr>
</form>
</table>
<br>
END;
        }
        
        if ($codereceipt == "Y") {
            
            $query  =   "SELECT Qualifies FROM ApplicantData";
            $query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
            $params =   array(':wotcid'=>$WOTCID, ':applicationid'=>$ApplicationID);
            list ($CCQ) =   $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
            
            $CCCODE =   array('Y'=>'1','N'=>'2');
            $CCID   =   strtoupper(substr($WOTCID,0,2)) . str_pad(preg_replace("/\D/","",$WOTCID),2,'00', STR_PAD_LEFT) . substr($ApplicationID, - 4) . $CCCODE[$CCQ];
            
            if($employmentconfirmation == "Y") {
                $rtn .= $RES['ty7'] . " ";
            } else { 
                $rtn .= $RES['ty8'] . " ";
            }
            $rtn .= "<strong style=\"font-size:14pt;color:red;\">" . $CCID . "</strong>";
            $rtn .= "<br><br>\n";
        } // end codereceipt
        
        if ($PartnerName == "NEEPAA") {
            
            $rtn .= '<form method="get" action="https://app.neepaa.com">';
            $rtn .= '<button type="submit">Back to NEEPAA</button>';
            $rtn .= '</form>';
        }
        
        $rtn .= "<br><br><br><br><br>";
        $rtn .= "<br><br><br><br><br>";
        $rtn .= "<span style=\"font-size:6pt;\">" . $APPDATA['ApplicationID'] . "</span>\n";
        
        $rtn .= "</div>\n";
        
        return $rtn;
    } // end function
    
    /**
     * @method      getAppData
     * @param       string  $OrgID
     * @param       string  $ApplicationID
     * @param       string  $lname
     * @return      array
     */
    function getAppData($OrgID, $ApplicationID, $lname) {
    
        $query      =   "SELECT COUNT(*) AS count FROM ApplicantData";
        $query      .=  " WHERE OrgID = :orgid AND ApplicationID = :applicationid AND QuestionID = 'last' AND Answer = :lname";
        $params     =   array(
                            ':orgid'            =>  $OrgID,
                            ':applicationid'    =>  $ApplicationID,
                            ':lname'            =>  $lname
                        );
        list($cnt)  =   $this->db->getConnection ( "IRECRUIT" )->fetchRow($query, array($params));
    
        if ($cnt == 1) {
            $query      =   "SELECT QuestionID, Answer FROM ApplicantData";
            $query      .=  " WHERE OrgID = :orgid AND ApplicationID = :applicationid";
            $params     =   array(
                                ':orgid'            =>  $OrgID,
                                ':applicationid'    =>  $ApplicationID,
                            );
            $DATA_RES   =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($query, array($params));
            $DATA       =   $DATA_RES['results'];
            
            foreach ($DATA as $AD) {
                $AppData[$AD['QuestionID']] = $AD['Answer'];
            }
        } // end hit
    
        return $AppData;
    
    } // end function
    
    /**
     * @method      getApplicationInfo
     * @param       string $WOTCID
     * @param       string $ApplicationID
     * @return      array
     */
    function getApplicationInfo($WOTCID, $ApplicationID) {
        $query      =   "SELECT * FROM ApplicantData WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params     =   array(':wotcid'=>$WOTCID, ':applicationid'=>$ApplicationID);
        $app_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $app_info;
    }
    
} // end Class
