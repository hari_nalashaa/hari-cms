<?php
/**
 * @class		WOTCFormQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCFormQuestions {
	
	var $conn_string       =   "WOTC";
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	/**
	 * @method		getActiveWotcFormIDs
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial		This method will fetch the distinct active forms in english 
	 */
	function getActiveWotcFormIds() {
	  $forms = "";

	    $sel_frm_que_info   =   "select distinct(WotcFormID) from WotcFormQuestions where substr(WotcFormID, -2) != 'SP'";
            $res_frm_que_info   =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, array() );
            $forms	=   $res_frm_que_info['results'];

          return $forms;
	}

	/**
	 * @method		getWotcFormQuestionsByFormID
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getWotcFormQuestionsByWotcIDFormID($WotcID, $WotcFormID) {
	
        $params_info        =   array(":WotcID"=>$WotcID, ":WotcFormID"=>$WotcFormID);       
        $sel_frm_que_info   =   "SELECT * FROM WotcFormQuestions WHERE WotcID = :WotcID AND WotcFormID = :WotcFormID ORDER BY QuestionOrder ASC";
        $res_frm_que_info   =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, array($params_info) );
        $form_que_list      =   $res_frm_que_info['results'];
        $form_questions     =   array();
        
        for($i = 0; $i < count($form_que_list); $i++) {
            $form_questions[$form_que_list[$i]['QuestionID']]   =   $form_que_list[$i];
        }
        
        return $form_questions;
	}
	
	/**
	 * @method		getWotcFormQuestionsListByQueTypeID
	 * @param		$OrgID, $WotcFormID, $QuestionTypeID
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getWotcFormQuestionsListByQueTypeID($WotcID, $WotcFormID, $QuestionTypeID) {
	
		$columns 		=	$this->db->arrayToDatabaseQueryString ( $columns );
	
		$params_info	=	array(":WotcID"=>$WotcID, ":WotcFormID"=>$WotcFormID, ":QuestionTypeID"=>$QuestionTypeID);
		$sel_que_info	=	"SELECT * FROM WotcFormQuestions WHERE WotcID = :WotcID AND WotcFormID = :WotcFormID AND QuestionTypeID = :QuestionTypeID";
		$res_que_info   =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_que_info, array($params_info) );

		return $res_que_info;
	}
	
	/**
	 * @method		getWotcFormQuestionsInfo
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getWotcFormQuestionsInfo($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_frm_que_info = "SELECT $columns FROM WotcFormQuestions";
		
		if(count($where_info) > 0) {
			$sel_frm_que_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($group_by != "") $sel_frm_que_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_frm_que_info .= " ORDER BY " . $order_by;

		$res_frm_que_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, $info );
	
		return $res_frm_que_info;
	}
	
	/**
	 * @method		getWotcFormChildQuestionsInfo
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getWotcFormParentQuesForChildQues($WotcID, $WotcFormID) {
	
		$form_questions		=	$this->getWotcFormQuestionsByWotcIDFormID($WotcID, $WotcFormID);
		$child_que_parents	=	array();

		foreach($form_questions as $question_id=>$question_info) {
			if($question_info['Active'] == 'Y' 
				&& $question_info['ChildQuestionsInfo'] != "")
			{
				$child_ques_info	=	json_decode($question_info['ChildQuestionsInfo'], true);

				foreach($child_ques_info as $parent_que_option=>$child_ques_list) {
					
					foreach ($child_ques_list as $child_que_id=>$child_que_disp_status) {
						$child_que_parents[$child_que_id]	=	$question_id;
					}
				}
			}
		}
		
		return $child_que_parents;
	}
	
	/**
	 * @method		copyWotcFormQuestions
	 * @param		$OrgID, $new_form, $old_form
	 */
	public function copyWotcFormQuestions($OldWotcID, $NewWotcID, $new_form_name, $old_form_name) {
		
		//Set parameters for prepared query
		$params           =   array(":OldWotcID"=>$OldWotcID, ":WotcFormID"=>$old_form_name);
		$ins_form_ques    =   "INSERT INTO WotcFormQuestions (WotcID, WotcFormID, QuestionID , ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate)
                              SELECT '" . $NewWotcID . "', '" . $new_form_name . "', QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate
                              FROM WotcFormQuestions WHERE WotcID = :OldWotcID AND WotcFormID = :WotcFormID ON DUPLICATE KEY UPDATE wrap = ''";
		$res_form_ques    =   $this->db->getConnection ( $this->conn_string )->insert($ins_form_ques, array($params));
	
		return $res_form_ques;
	}
	
	/**
	 * @method		insWotcFormQuestionsInfo
	 * @param		$info
	 */
	public function insWotcFormQuestionsInfo($info) {
	
	    $ins_form_ques = $this->db->buildInsertStatement("WotcFormQuestions", $info);
	    $res_form_ques = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_ques["stmt"], $ins_form_ques["info"] );
	
	    return $res_form_ques;
	}
	
	/**
	 * @method		updWotcFormQuestionsInfo
	 * @param		$set_info = array(), $where_info = array(), $info
	 */
	public function updWotcFormQuestionsInfo($set_info = array(), $where_info = array(), $info) {
	
	    $upd_que_info  =   $this->db->buildUpdateStatement("WotcFormQuestions", $set_info, $where_info);
	    $res_que_info  =   $this->db->getConnection( $this->conn_string )->update($upd_que_info, $info);
	
	    return $res_que_info;
	}
	
	/**
	 * @method		delWotcFormQuestionsInfo
	 * @param		$where_info = array(), $info
	 */
	public function delWotcFormQuestionsInfo($where_info = array(), $info) {
		
		//Set parameters for prepared query
		$del_que_info = "DELETE FROM WotcFormQuestions";
		
		if(count($where_info) > 0) $del_que_info .= " WHERE " . implode(" AND ", $where_info);
		$res_que_info = $this->db->getConnection( $this->conn_string )->delete($del_que_info, $info);
		
		return $res_que_info;		
	}
	
	/**
	 * @method		updWotcFormQuestionsOrder
	 * @param		$WotcID, $WotcFormID, $QuestionID, $que_order
	 */
	public function updWotcFormQuestionsOrder($WotcID, $WotcFormID, $QuestionID, $que_order) {

		//Update Question Order of Form Tables
		$params_info      =   array(':WotcID'=>$WotcID, ':WotcFormID'=>$WotcFormID, ':QuestionID'=>$QuestionID, ':SetQuestionOrder'=>$que_order);
		$upd_que_order    =   "UPDATE WotcFormQuestions SET QuestionOrder = :SetQuestionOrder WHERE OrgID = :OrgID AND WotcFormID = :WotcFormID AND QuestionOrder = :SetQuestionOrder";
		$res_que_order    =   $this->db->getConnection( $this->conn_string )->update($upd_que_order, array($params_info));
	
		return $res_que_order;
	}
	
}
