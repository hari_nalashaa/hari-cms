<?php
/**
 * @class		WOTCOrganization
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCOrganization
{
    public $db;

    var $conn_string       =   "WOTC";

    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getCCID
     */
    public function getCCID($WOTCID, $ApplicationID) {

	$CCID = "";

	$PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
        $DisplayCode=$PARENT['DisplayCode'];
    	
    	$query  =   "SELECT Qualifies FROM ApplicantData";
    	$query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
    	$params =   array(':wotcid'=>$WOTCID, ':applicationid'=>$ApplicationID);
    	list ($CCQ) =   $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));

	if ($CCQ == "") { $CCQ = "N"; }

    	$CCCODE = array('Y' => '1','N' => '2');
	if ($DisplayCode == "Y") {	
    	    $CCID = strtoupper(substr($WOTCID,0,2)) . str_pad(preg_replace("/\D/","",$WOTCID),2,'00', STR_PAD_LEFT) . substr($ApplicationID, - 4) . $CCCODE[$CCQ];
	}
    	
    	return $CCID;
    	 
    }
    
    /**
     * @method		getOrgDataInfo
     * @param		string $columns
     * @param		string $where_info
     * @param 		string $order_by
     * @param 		string $info
     * @return 		array
     */
    function getOrgDataInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_org = "SELECT $columns FROM OrgData";
    
        if(count($where_info) > 0) {
            $sel_org .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($group_by != "") $sel_org .= " GROUP BY " . $group_by;
        if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
    
        $res_org = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_org, $info );
    
        return $res_org;
    }
    
    /**
     * @method      getWotcIdInfoByOrgIDMultiOrgID
     * @param       string $cookie
     * @return      array
     */
    public function getWotcIdInfoByOrgIDMultiOrgID($columns = "*", $OrgID, $MultiOrgID) {
    
    	$params		=   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":Active"=>'Y');
    	$query		=   "SELECT $columns FROM OrgData WHERE iRecruitOrgID = :OrgID AND iRecruitMultiOrgID = :MultiOrgID AND Active = :Active";
        if (SERVER == 'CANADA') {
            $query      .=   " AND iRecruitOrgID != 'B12345467'";
        }
    	$org_info	=   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));

    	return $org_info;
    }
    
    /**
     * @method      getOrganizationInfo
     * @param       string $cookie
     * @return      array
     */
    public function getOrganizationInfo($columns = "*", $wotcID) {
        
        $params     =   array(":wotcID"=>$wotcID);
        $query      =   "SELECT $columns FROM OrgData WHERE wotcID = :wotcID";
        $org_info   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $org_info;
    }
    
    /**
     * @method      updOrgDataInfo
     * @param       $set_info, $where_info, $info
     */
    function updOrgDataInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_org_data_info = $this->db->buildUpdateStatement("OrgData", $set_info, $where_info);
        $res_org_data_info = $this->db->getConnection( $this->conn_string )->update($upd_org_data_info, $info);
    
        return $res_org_data_info;
    }
    
    /**
     * @method		insUpdOrgDataInfo
     * @param		$info
     * @return		array
     */
    function insUpdOrgDataInfo($info, $skip = array()) {
    
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("OrgData", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
    
        return $res_org;
    }

    /**
     * @method		insUpdDycomHiresInfo
     * @param		$info
     * @return		array
     */
    function insUpdDycomHiresInfo($info, $skip = array()) {
    
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("DycomHires", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
    
        return $res_org;
    }

    /**
     * @method		insUpdDycomHireHistory
     * @param		$info
     * @return		array
     */
    function insUpdDycomHireHistory($info, $skip = array()) {
    
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("DycomHireHistory", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
    
        return $res_org;
    }

    /**
     * @method		insUpdDycomContacts
     * @param		$info
     * @return		array
     */
    function insUpdDycomContacts($info, $skip = array()) {
    
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("DycomContacts", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
    
        return $res_org;
    }
}    
?>
