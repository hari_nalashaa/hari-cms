<?php
/**
 * @class		Requisitions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection, by default connection is based on 
 * 				constructor identifer that we are using
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Requisitions {

	var $conn_string       =   "IRECRUIT";
	
	//Skip Duplicate Data Questions - Split Fields Data
	var $skip_req_data_fields	=	array(
										"AdvertisingOption-Budget", 
										"AdvertisingOption-Desc", 
										"AdvertisingOption-Period",
										"AdvertisingOption-specialinstructions",
										"ExpireHour",
										"ExpireMeridian",
										"ExpireMinute",
										"PostHour",
										"PostMeridian",
										"PostMinute",
										"PostMinute",
										"action",
										"process_requisition"
									);
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	/**
	 * @method     getRequisitionsByRequisitionManagers
	 * @param      string $columns
	 * @param      string $Keyword
	 * @param      string $ManagerUserID
	 * @param      string $OwnerUserID
	 * @param      string $FromDate
	 * @param      string $ToDate
	 * @param      string $RequisitionStage
	 * @param      string $RequisitionStageDate
	 * @param      string $IsRequisitionApproved
	 * @param      string $Active
	 * @param      string $order_by
	 * @return     array
	 */
	public function getRequisitionsByRequisitionManagers($columns, $FilterOrganization, $Keyword = '', $ManagerUserID, $OwnerUserID = '', $FromDate = '', $ToDate = '', $RequisitionStage = '', $RequisitionStageDate = '', $IsRequisitionApproved, $Active, $order_by = '') {
	     
	    global $OrgID;
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	    $params  = array(":ManagerUserID"=>$ManagerUserID);
	    
	    $sel_info = "SELECT $columns FROM Requisitions, RequisitionManagers RM
            	     WHERE RM.OrgID = Requisitions.OrgID
            	     AND RM.RequestID = Requisitions.RequestID
            	     AND RM.UserID = :ManagerUserID
	                 AND Requisitions.Approved = 'Y'";

	    if($FilterOrganization != "") {
	        list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
	        
	        // Set condition
	        $sel_info  .=  " AND Requisitions.OrgID = :OrgID AND Requisitions.MultiOrgID = :MultiOrgID";
	        // Set parameters
	        $params    =   array (":OrgID"=>$FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
	    }
	    else {
	        // Set condition
	        $sel_info  .= " AND Requisitions.OrgID = :OrgID";
	        // Set parameters
	        $params =   array (":OrgID" => $OrgID);
	    }
	    
	    
	    if($OwnerUserID != "") {
	        $sel_info  .= " AND Requisitions.Owner = :OwnerUserID";
	        $params[":OwnerUserID"] = $OwnerUserID;
	    }
	    
	    if(isset($FromDate) && $FromDate != "" && isset($ToDate) && $ToDate != "") {
	        $sel_info  .= " AND Requisitions.PostDate >= :FromDate AND Requisitions.PostDate <= :ToDate";
	        $params[":FromDate"] = G::Obj('DateHelper')->getYmdFromMdy($FromDate);
	        $params[":ToDate"] = G::Obj('DateHelper')->getYmdFromMdy($ToDate);
	    }
	    
	    if(isset($Keyword) && $Keyword != "") {
	        $sel_info  .= " AND (Requisitions.Title LIKE :Keyword1 OR Requisitions.RequisitionID LIKE :Keyword2 OR Requisitions.JobID LIKE :Keyword3)";
	        $params[":Keyword1"]    =   "%".$Keyword."%";
	        $params[":Keyword2"]    =   "%".$Keyword."%";
	        $params[":Keyword3"]    =   "%".$Keyword."%";
	    }
	    
	    if(isset($RequisitionStage) && $RequisitionStage != "") {
	       $sel_info  .= " AND (Requisitions.RequisitionStage = :RequisitionStage)";
           $params[":RequisitionStage"]   =  $_REQUEST['RequisitionStage'];
	    }
	    
	    if(isset($RequisitionStageDate) && $RequisitionStageDate != "") {
           $sel_info  .= " AND (Requisitions.RequisitionStageDate = :RequisitionStageDate)";
           $params[":RequisitionStageDate"]   =  G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['RequisitionStageDate']);
	    }
	    
	    if ($feature ['RequisitionApproval'] != "Y") {
	        if($Active == "A") {
	            $sel_info  .= " AND Active != :Active";
	            $params[":Active"]  =   'R';
	        }
	        else {
	            $sel_info  .= " AND Active = :Active";
	            $params[":Active"]  =   $Active;
	        }
	    }
	    else if ($feature ['RequisitionApproval'] == "Y") {
	        if($Active != "A") {
	            $sel_info  .= " AND Active = :Active";
	            $params[":Active"]  =   $Active;
	        }
	    }
	    
	    if(isset($IsRequisitionApproved) && $IsRequisitionApproved == "N") {
	        $sel_info  .= " AND Approved = 'N'";
	    }
	    
	    if($Active == "Y") $where[] = " AND Approved = 'Y'";
	    
	    if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	    
	    $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
	    
	    return $res_info;
	}
	
	/**
	 * @method		getRequisitions
	 * @param		$POST
	 * @return		array @type	json
	 */
	public function getRequisitions($POST = '') {
		
		global $OrganizationsObj, $ApplicationsObj, $USERID, $USERROLE, $OrgID, $organization_info;
		
		/**
		 * @input json data
		 */
		if ($POST == '') $POST = file_get_contents ( 'php://input' );
		$JSON = json_decode ( $POST, true );
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":Active"=>$JSON ['Active']);
		
		if (! isset ( $JSON ['Active'] )) $JSON ['Active'] == 'Y';
		
		$limit = $JSON ['LIMIT'];
		
		$selrequisitions = "SELECT RequestID, RequisitionID, JobID, DATE_FORMAT(PostDate,'%m/%d/%Y') PostDate, 
						  DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate, Title, 
						  DATEDIFF(NOW(), PostDate) Open, DATEDIFF(ExpireDate,PostDate) Closed, 
						  date_format(DATE_ADD(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current, DATEDIFF(ExpireDate, NOW()) ExpireDays
						  FROM Requisitions 
						  WHERE OrgID = :OrgID 
						  AND Active = :Active 
						  AND Approved = 'Y'";
		
		if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params[":ReqManOrgID"] = $OrgID;
			$params[":UserID"] = $USERID;
			$selrequisitions .= " AND RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManOrgID and UserID = :UserID)";
		} // end hiring manager
		
		if(isset($JSON ['ORDER_BY']) && $JSON ['ORDER_BY'] != "")
		{
			if($JSON['SORT'] == "D") $sort_order = "DESC";
			else if($JSON['SORT'] == "A") $sort_order = "ASC";
			else $sort_order = "ASC";
			
			$order_by = $JSON ['ORDER_BY'];
			$selrequisitions .= " ORDER BY $order_by $sort_order";
		} else {
			$selrequisitions .= " ORDER BY DATEDIFF(ExpireDate, NOW()) ASC";
		}
		
		$selrequisitions .= $limit;
		
		$resrequisitions = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $selrequisitions, array($params) );

		if(is_array($resrequisitions ['results'])) {
			foreach ( $resrequisitions ['results'] as $row ) {
				$applicantscount = $ApplicationsObj->getApplicantsCount ( $row ['RequestID'] );
				$row ['OrganizationName'] = $organization_info ['OrganizationName'];
				$row ['ApplicantsCount']  = $applicantscount;
				$rows [] = $row;
			}
		}
		
		return json_encode ( $rows );
	}
	
	/**
	 * @method		getRequisitionsSearchParams
	 * @param 		mixed $columns
	 * @param 		array $where_info
	 * @param 		string $order_by
	 * @param 		array $info
	 * @return 		array
	 */
	function getRequisitionsSearchParams($columns, $where_info = array(), $order_by = '', $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM RequisitionsSearchParams";
	
		if(count($where_info) > 0) $sel_info .= " WHERE " . implode(" AND ", $where_info);
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$res_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method		insRequisitionsSearchParams
	 * @param 		string $insert_info
	 * @param 		string $on_update
	 * @param 		array $update_info
	 * @return 		array
	 */
	function insRequisitionsSearchParams($insert_info, $on_update = '', $update_info = array()) {
	
		//Set parameters for prepared query
		$ins_search_query = $this->db->buildInsertStatement('RequisitionsSearchParams', $insert_info);
	
		$insert_statement = $ins_search_query["stmt"];
		if($on_update !=  '') {
			$insert_statement = $ins_search_query["stmt"].$on_update;
			foreach($update_info as $upd_key=>$upd_value) {
				$ins_search_query["info"][0][$upd_key] = $upd_value;
			}
		}
	
		$res_form_data = $this->db->getConnection ( $this->conn_string )->insert ( $insert_statement, $ins_search_query["info"] );
		return $res_form_data;
	}
	
	/**
	 * @method		getRequisitionInformation
	 * @param		$OrgID, $MultiOrgID, $RequestID
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	function getRequisitionInformation($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM Requisitions";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($group_by != "") $sel_requisition_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;

		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method		getRequisitionAdvertisingOptions
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	function getRequisitionAdvertisingOptions($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM RequisitionAdvertisingOptions";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method		getRequisitionManagersInfo
	 * @param		$OrgID, $MultiOrgID, $RequestID
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	function getRequisitionManagersInfo($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM RequisitionManagers";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($group_by != "") $sel_requisition_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method		getPostRequisitionsInformation
	 * @param 		string $columns
	 * @param 		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		string
	 */
	function getPostRequisitionsInformation($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_post_requisition_info = "SELECT $columns FROM PostRequisitions";
		if(count($where_info) > 0) {
			$sel_post_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_post_requisition_info .= " ORDER BY " . $order_by;
	
		$res_post_requisition_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_post_requisition_info, $info );
	
		return $res_post_requisition_info;
	}
	
	/**
	 * @method		getRequisitionNotesInfo
	 * @param 		string $columns
	 * @param 		string $where_info
	 * @param 		string $order_by
	 * @param 		string $info
	 * @return 		string
	 */
	function getRequisitionNotesInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM RequisitionNotes";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
		$res_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method		getRequisitionsByApplications
	 * @param		$POST
	 * @return		array @type	json
	 */
	public function getRequisitionsByApplications() {
		
		global $USERID, $USERROLE, $OrgID;
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID);
		
		$selrequisitions = "SELECT J.RequestID, R.Title, R.JobID, R.RequisitionID 
							FROM JobApplications J 
							LEFT JOIN Requisitions R 
							ON R.RequestID = J.RequestID 
							WHERE J.RequestID = R.RequestID";
		$selrequisitions .= " AND R.OrgID = :OrgID";
		
		$selrequisitions .= " AND DATE(J.EntryDate) > last_day(date_sub(now(), INTERVAL 12 MONTH))";
		
		if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params[":ReqManOrgID"] = array($OrgID);
			$params[":UserID"] = array($USERID);
				
			$selrequisitions .= " AND R.RequestID in (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManOrgID AND UserID = :UserID)";
		} // end hiring manager
		
		$selrequisitions .= " GROUP BY J.RequestID";
		$selrequisitions .= " ORDER BY R.PostDate";
		
		$resrequisitions = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $selrequisitions, array($params));
		
		foreach ( $resrequisitions ['results'] as $row ) {
			$rows [] = $row;
		}
		
		return $rows;
	}
	
	/**
	 * @method		insertInternalPrescreenFormsByRequestID
	 * @param		string $OrgID, $RequestID, $OriginalRequestID
	 * @return		array
	 */
	function insertInternalPrescreenFormsByRequestID($OrgID, $RequestID, $OriginalRequestID) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":OriginalRequestID"=>$OriginalRequestID);
		
		$ins_internal_forms = "INSERT INTO InternalForms SELECT `OrgID`, '$RequestID', `UniqueID`,
								`FormStatus`, `FormName`, `FormType`, `PresentedTo`,
								`AssignStatus`, `CompleteByInterval`, `CompleteByFactor`
								FROM InternalForms
								WHERE OrgID = :OrgID 
								AND RequestID = :OriginalRequestID";
		$res_app_process_temp = $this->db->getConnection( $this->conn_string )->insert ( $ins_internal_forms, array($params) );
		
		$ins_prescreen_que = "INSERT INTO PrescreenQuestions SELECT `OrgID`, '$RequestID', `Question`, `Answer`, `SortOrder`, `Internal`  
							  FROM `PrescreenQuestions` 
							  WHERE  OrgID = :OrgID 
							  AND RequestID = :OriginalRequestID";
		$res_prescreen_que = $this->db->getConnection( $this->conn_string )->insert ( $ins_prescreen_que, array($params) );
		
		return array (
				$res_app_process_temp,
				$res_prescreen_que 
		);
	}
	
	/**
	 * @method		getRequisitionManagersNames
	 * @param		string $OrgID, $RequestID
	 * @return		string
	 */
	function getRequisitionManagersNames($OrgID, $RequestID) {
		
		global $IrecruitUsersObj;
		$rtn          =   "";
		$managernames =   "";
		
		//Set parameters for prepared query
		$params       =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		$sel_user_id  =   "SELECT UserID FROM RequisitionManagers WHERE OrgID = :OrgID AND RequestID = :RequestID";
		$res_user_id  =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_user_id, array($params));

		$requisition_managers_count = $res_user_id['count'];
		$requisition_managers_info  = $res_user_id['results'];
		
		for($i = 0; $i < $requisition_managers_count; $i++) {
			$Managers[$i] = $requisition_managers_info[$i]['UserID'];
		}
		
		if(is_array($Managers)) {
			foreach ( $Managers as $c => $userid ) {
				$managernames .= $IrecruitUsersObj->getUsersName( $OrgID, $userid ) . ", ";
			}	
		}
	
		$rtn = substr ( $managernames, 0, strlen ( $managernames ) - 2 );
		if ($rtn == "") {
			$rtn = 'Not Assigned';
		}
		
		return $rtn;
	} // end function
	
	/**
	 * @method		restrictRequestID
	 * @param		$OrgID, $USERID
	 */
	function restrictRequestID($OrgID, $USERID) {
		
		$rolerestrict = '';

		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":ReqManOrgID"=>$OrgID, ":ReqManUserID"=>$USERID);
		
		$sel_requestids  = "SELECT RequestID FROM Requisitions WHERE OrgID = :OrgID";
		$sel_requestids .= " AND RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManOrgID AND UserID = :ReqManUserID)";
		$res_requestids  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_requestids, array($params));
	
		foreach ($res_requestids['results'] as $REQS) {
			$restrictrequest .= $REQS['RequestID'] . "', '";
		}
	
		if ($restrictrequest != "") {
			$restrictrequest = substr ( $restrictrequest, 0, - 4 );
			$rolerestrict = "RequestID IN ('$restrictrequest')";
		} else {
			$rolerestrict = "RequestID IN ('X')";
		}

		return $rolerestrict;
	} // end function
	
	/**
	 * @method	getRequisitionForwardInfo
	 * @return	array
	 */
	function getRequisitionForwardInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_requisition_info = "SELECT $columns FROM RequisitionForward";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
		
		$row_requisition_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
		
		return $row_requisition_info;
	}
	

	/**
	 * @method	getRequisitionOrgLevels
	 * @return	array
	 */
	function getRequisitionOrgLevelsInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM RequisitionOrgLevels";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method	getRequisitionsByUser
	 * @return	array
	 */
	function getRequisitionsByUser($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM Requisitions R, Users U";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method	getRequisitionsByUser
	 * @return	array
	 */
	function getRequisitionsAndUsers($columns = "", $where_info = array(), $order_by = "", $info = array()) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $sel_requisition_info = "SELECT $columns FROM Requisitions R LEFT JOIN Users U ON R.OrgID = U.OrgID AND R.Owner = U.UserID";
	    if(count($where_info) > 0) {
	        $sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
	    }
	
	    if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
	    $row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
	    return $row_requisition_info;
	}
	
	/**
	 * @method	getReqAndReqOrgLevelsInfo
	 * @return	array
	 */
	function getReqAndReqOrgLevelsInfo($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM Requisitions R, RequisitionOrgLevels ROL";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($group_by != "") $sel_requisition_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
		
		//echo $sel_requisition_info."<br>";
		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method	getRequisitionFilesInfo
	 * @return	array
	 */
	function getRequisitionFilesInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM RequisitionFiles";
		if(count($where_info) > 0)
		{
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$row_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
	
		return $row_info;
	}
	
	/**
	 * @method	delRequisitionsInfo
	 * @return	array
	 */
	function delRequisitionsInfo($table_name, $where_info = array(), $info = array(), $limit = "") {
		
		$del_req_info = "DELETE FROM $table_name";
		
		if(count($where_info) > 0) {
			$del_req_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($limit != "") {
			$del_req_info .= " LIMIT $limit";
		}
		
		$res_req_info = $this->db->getConnection( $this->conn_string )->delete ( $del_req_info, $info );
	
		return $res_req_info;
	}
	
	/**
	 * @method	updRequisitionsInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updRequisitionsInfo($table_name, $set_info = array(), $where_info = array(), $info) {
		
		$upd_req_info = $this->db->buildUpdateStatement($table_name, $set_info, $where_info);
		$res_req_info = $this->db->getConnection( $this->conn_string )->update($upd_req_info, $info);
		return $res_req_info;
	}
	
	/**
	 * @method	updRequisitionsInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updRequisitionsFilesInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_req_info = $this->db->buildUpdateStatement('RequisitionFiles', $set_info, $where_info);
		$res_req_info = $this->db->getConnection( $this->conn_string )->update($upd_req_info, $info);
	
		return $res_req_info;
	}
	
	/**
	 * @method	insRequisitionsInfo
	 * @param	$info
	 */
	function insRequisitionsInfo($table_name, $req_info) {
		
		$ins_requisitions = $this->db->buildInsertStatement($table_name, $req_info);
		$res_requisitions = $this->db->getConnection ( $this->conn_string )->insert ( $ins_requisitions["stmt"], $ins_requisitions["info"] );
		
		return $res_requisitions;
	}
	
	/**
	 * @method	insRequisitionsArchive
	 * @return
	 */
	function insRequisitionsArchive($info) {
		
		$ins_req_archieve = "INSERT INTO RequisitionsArchive
							 SELECT * FROM Requisitions
							 WHERE OrgID = :OrgID AND RequestID = :RequestID";
		$res_req_archieve = $this->db->getConnection( $this->conn_string )->insert ( $ins_req_archieve, $info );
	
		return $res_req_archieve;
	}
	
	/**
	 * @method		getRequisitionsDetailInfo
	 * @param		$OrgID, $RequestID
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, RequestID.
	 */
	function getRequisitionsDetailInfo($columns, $OrgID, $RequestID) {
		
		$columns          =   $this->db->arrayToDatabaseQueryString ( $columns );
		
		//Set parameters
		$params           =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		//Get Requisitions Information
		$sel_requisitions =   "SELECT $columns FROM Requisitions WHERE OrgID = :OrgID AND RequestID = :RequestID";
		$res_requisitions =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_requisitions, array($params) );
		
		return $res_requisitions;
	}
	
	/**
	 * @method		getReqDetailInfo
	 * @param		$OrgID, $MultiOrgID, $RequestID
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	function getReqDetailInfo($columns, $OrgID, $MultiOrgID, $RequestID) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
		//Get Requisitions Information
		$sel_requisitions = "SELECT $columns FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
		$res_requisitions = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_requisitions, array($params) );
	
		return $res_requisitions;
	}

	/**
	 * @method		getRequisitionFilesAndFilePurpose
	 * @param	 
	 */
	function getRequisitionFilesAndFilePurpose($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_requisition_info = "SELECT $columns FROM RequisitionFiles RF, FilePurpose FP";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($group_by != "") $sel_requisition_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
		
		$row_requisition_info  = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_requisition_info, $info );
		
		return $row_requisition_info;
	}
	
	/**
	 * @method		delInfoByTable
	 * @param		$table_name, $where_info = array(), $info
	 */
	function delInfoByTable($table_name, $where_info = array(), $info) {
	
		$del_info = "DELETE FROM $table_name";
	
		if(count($where_info) > 0) $del_info .= " WHERE " . implode(" AND ", $where_info);
	
		$res_info = $this->db->getConnection ( $this->conn_string )->delete($del_info, $info);
		return $res_info;
	}
	
	/**
	 * @method		getInfo
	 * @param		$query
	 */
	function getInfo($query, $info) {
	
		$results = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($query, $info);
		return $results;
	}
	
	/**
	 * @method		getRequisitionStageInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getRequisitionStageInfo($table_name, $columns = "", $where_info = array(), $order_by = "", $info = array()) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $sel_info = "SELECT $columns FROM $table_name";
	    if(count($where_info) > 0) {
	        $sel_info .= " WHERE " . implode(" AND ", $where_info);
	    }
	
	    if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
	    $row_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
	
	    return $row_info;
	}
	
	/**
	 * @method	delRequisitionStage
	 * @return	array
	 */
	function delRequisitionStage($table_name, $where_info = array(), $info = array()) {
	
	    $del_info = "DELETE FROM $table_name";
	    if(count($where_info) > 0) {
	        $del_info .= " WHERE " . implode(" AND ", $where_info);
	    }
	
	    $res_info = $this->db->getConnection( $this->conn_string )->delete ( $del_info, $info );
	
	    return $res_info;
	}
	
	/**
	 * @method	insRequisitionStage
	 * @param	$info
	 */
	function insRequisitionStage($table_name, $info) {
	
	    $ins_info = $this->db->buildInsertStatement($table_name, $info);
	    $res_info = $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
	
	    return $res_info;
	}
	
	/**
	 * @method	insDefaultRequisitionStageInfo
	 * @param	$info
	 */
	function insDefaultRequisitionStageInfo($OrgID) {
	
	    $ins_info = "INSERT INTO RequisitionStage(OrgID, Code, Description, IsFinal) SELECT '$OrgID', Code, Description, IsFinal FROM RequisitionStage WHERE OrgID = 'MASTER'";
	    $res_info = $this->db->getConnection ( $this->conn_string )->insert ( $ins_info );
	
	    return $res_info;
	}
	
	/**
	 * @method	insDefaultRequisitionStageReasonsInfo
	 * @param	$info
	 */
	function insDefaultRequisitionStageReasonsInfo($OrgID) {
	
	    $ins_info = "INSERT INTO RequisitionStageReason(OrgID, Code, Description) SELECT '$OrgID', Code, Description FROM RequisitionStageReason WHERE OrgID = 'MASTER'";
	    $res_info = $this->db->getConnection ( $this->conn_string )->insert ( $ins_info );
	
	    return $res_info;
	}

    /**
     * @method  getEEOClassifications
     */
    function getEEOClassifications() {

        $sel_eeo_classifications = "SELECT `Code`, `Description` FROM EEOClassifications ORDER BY Description";
        $res_eeo_classifications = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_eeo_classifications, array (
                        array()
        ) );

        return $res_eeo_classifications;
    }

}
?>
