<?php 
/**
 * @class		IconnectEmailTemplate
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class IconnectEmailTemplate {
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getIconnectEmailTemplate
	 * @return		associative array
	 * @tutorial	This method will fetch the iconnect email template information.
	 */
	public function getIconnectEmailTemplate($OrgID) {
	
		$params_info = array(":OrgID"=>$OrgID);
		$sel_iconnect_email_template = "SELECT * FROM IconnectEmailTemplates WHERE OrgID = :OrgID";
		$res_iconnect_email_template = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_iconnect_email_template, array($params_info) );
	
		return $res_iconnect_email_template;
	}
	
	/**
	 * @method		insIconnectEmailTemplate
	 * @return		associative array.
	 */
	public function insIconnectEmailTemplate($OrgID, $Subject, $EmailTemplate) {

		$params = array(
						':OrgID'			=>	$OrgID, 
						':ISubject'			=>	$Subject,
						':USubject'			=>	$Subject,
						':IEmailMessage'	=>	$EmailTemplate, 
						':UEmailMessage'	=>	$EmailTemplate
						);
		
		$ins_iconnect_email_template = "INSERT INTO IconnectEmailTemplates(OrgID, Subject, EmailMessage) VALUES(:OrgID, :ISubject, :IEmailMessage) ON DUPLICATE KEY UPDATE Subject = :USubject, EmailMessage = :UEmailMessage";
		$res_iconnect_email_template = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_iconnect_email_template,  array($params));
	
		return $res_iconnect_email_template;
	}
	
}
?>
