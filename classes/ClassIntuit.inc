<?php
/**
 * @class		Intuit
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class Intuit {
	
	var $conn_string       =   "IRECRUIT";
	var $redirect_url      =   ""; //updated in construct

	//Production
	var $create_charge_url =   "";
	var $get_charge_url    =   ""; //Have to append charge_id dynamically
	var $client_id         =   "";
	var $client_secret     =   "";
	var $scope_payment     =   "";
	var $ref_acc_token_url =   ""; 
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
		$this->redirect_url = ADMIN_HOME . "intuitRedirect.php";
		
		if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com"
		    || $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com"
            ) {
		    //Development
		    $this->create_charge_url =   "https://sandbox.api.intuit.com/quickbooks/v4/payments/charges";
		    $this->get_charge_url    =   "https://sandbox.api.intuit.com/quickbooks/v4/payments/charges/"; //Have to append charge_id dynamically

		    $this->client_id         =   "Q00hZk1CBxTO2RHEZYa6U0Q3zFZ5LWt1XzDxlAgaUTJHSmpmhr";
		    $this->client_secret     =   "xEbdubUUrFsZucA31D2qxO4ZzgajByjfOjWfrMFU";
		    $this->scope_payment     =   "com.intuit.quickbooks.payment";
		    $this->ref_acc_token_url =   "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer";
		}
		else {
		    //Production
		    $this->create_charge_url =   "https://api.intuit.com/quickbooks/v4/payments/charges";
		    $this->get_charge_url    =   "https://api.intuit.com/quickbooks/v4/payments/charges/"; //Have to append charge_id dynamically
		    $this->client_id         =   "Q0crQErOLJi7XdJz6FDy04gzbISKVlv9wqj1G0ciwmtWE90TR7";
		    $this->client_secret     =   "rjL7qR2viXuTAOewgpNkCkXyGYjiGKnl8h2Vawro";
		    $this->scope_payment     =   "com.intuit.quickbooks.payment";
		    $this->ref_acc_token_url =   "https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer";
		}
		
	}
	
	/**
	 * @method		insIntuitAccessInformation
	 * @return		array
	 */
	function insIntuitAccessInformation($intuit_info) {
	
        $ins_intuit_acc_info = $this->db->buildInsertStatement('IntuitAccessInformation', $intuit_info);
		$res_intuit_acc_info = $this->db->getConnection ( $this->conn_string )->insert ( $ins_intuit_acc_info["stmt"], $ins_intuit_acc_info["info"] );
		
		return $res_intuit_acc_info;
	
	} // end function
	
	/**
	 * @method     getIntuitAccessInformation
	 */
	function getIntuitAccessInformation($columns = "*") {
	    
        $columns                =   $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_intuit_acc_info    =   "SELECT $columns FROM IntuitAccessInformation ORDER BY LastModifiedDateTime DESC LIMIT 1";
        $res_intuit_acc_info    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_intuit_acc_info );
        
        return $res_intuit_acc_info;
	}
	
	
	/**
	 * @method     getUpdatedAccessTokenInfo
	 */
	function getUpdatedAccessTokenInfo() {

	    global $OrgID;
	    
	    $now                   =   G::Obj('MysqlHelper')->getNow();
	    $intuit_access_info    =   G::Obj('Intuit')->getIntuitAccessInformation("*");

	    $intuit_access_info['TokenError'] = "";
	    
	    $time_diff_sec = strtotime($now) - strtotime($intuit_access_info['LastModifiedDateTime']);

        $body = [
            "grant_type"    =>  "refresh_token",
            "refresh_token" =>  $intuit_access_info['RefreshTokenValue'],
        ];
        
        $http_header = array(
            'Content-Type'      =>  'application/x-www-form-urlencoded',
            'Accept'            =>  'application/json',
            'Authorization'     =>  "Basic " . base64_encode($this->client_id . ":" . $this->client_secret)
        );

        if($time_diff_sec > 3000) {
            $intuit_access_token_response = G::Obj('CurlClient')->makeAPICall($this->ref_acc_token_url, "POST", $http_header, "grant_type=refresh_token&refresh_token=".$intuit_access_info['RefreshTokenValue'], null, false);
            $intuit_access_token_info = json_decode($intuit_access_token_response, true);
            
            $req_data  = "\n". "Access Token Url: ".$this->ref_acc_token_url."\n";
            $req_data .= "Client ID: ".$this->client_id."\n";
            $req_data .= "Client Secret: ".$this->client_secret."\n";
            $req_data .= print_r($http_header, true)."\n";
            $req_data .= "Refresh Token: ".$intuit_access_info['RefreshTokenValue']."\n";
            $req_data .= print_r($intuit_access_token_info, true);
            
            Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-token-response.txt", $req_data, "a+", false);
            Logger::writeMessageToDb($OrgID, "Intuit", $req_data);
            
            $token_error = "";
            if($intuit_access_token_info['access_token'] != "") {
                $upd_acc_token_info = "UPDATE IntuitAccessInformation
                                        SET AccessTokenValue = :AccessTokenValue,
                                        RefreshTokenValue = :RefreshTokenValue,
                                        AccessTokenValidationPeriod = :AccessTokenValidationPeriod,
                                        RefreshTokenValidationPeriod = :RefreshTokenValidationPeriod,
                                        LastModifiedDateTime = NOW()
                                        WHERE GuID = :GuID";
                $acc_token_params = array(
                                        ":AccessTokenValue"             =>  $intuit_access_token_info['access_token'],
                                        ":RefreshTokenValue"            =>  $intuit_access_token_info['refresh_token'],
                                        ":AccessTokenValidationPeriod"  =>  $intuit_access_token_info['expires_in'],
                                        ":RefreshTokenValidationPeriod" =>  $intuit_access_token_info['x_refresh_token_expires_in'],
                                        ":GuID"                         =>  $intuit_access_info['GuID']
                                        );
            
                $res_acc_token_info = $this->db->getConnection ( $this->conn_string )->update ( $upd_acc_token_info, array ($acc_token_params) );
            }
            else {
                $token_error = "yes";
            }
            
            $intuit_access_info =   G::Obj('Intuit')->getIntuitAccessInformation("*");
            
            if($token_error == "yes") {
                $intuit_access_info['TokenError'] = "Yes";
            }
            else {
                $intuit_access_info['TokenError'] = "";
            }
        }
        
        
        return $intuit_access_info;
	}
	
	/**
	 * @method     getRefreshTokenExpireDaysDiff
	 */
	function getRefreshTokenExpireDaysDiff() {
	    
        $sel_refresh_token_info = "SELECT DATEDIFF(DATE(NOW()), DATE(CreatedDateTime)) AS DaysDiff FROM IntuitAccessInformation ORDER BY LastModifiedDateTime DESC LIMIT 1";
        $res_refresh_token_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_refresh_token_info );
        $days_diff              = $res_refresh_token_info['DaysDiff'];

        return $days_diff;
	}
	
}
