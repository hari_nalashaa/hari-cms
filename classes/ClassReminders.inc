<?php
/**
 * @class		Reminders
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Reminders {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getRequestHistory
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getAppointmentReminderInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM AppointmentReminder";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$res_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method 		createReminder
	 * @param 		$SID, $Subject, $DateTime
	 * @tutorial 	$lastinsertid(integer)
	 */
	function createReminder($SID, $Subject, $DateTime, $USERID) {
		
		$params = array(":SID"=>$SID, ":UserID"=>$USERID, ":Subject"=>$Subject, ":DateTime"=>$DateTime);
		$ins_reminder = "INSERT INTO AppointmentReminder(SID, UserID, Subject, DateTime) VALUES(:SID, :UserID, :Subject, :DateTime)";
		$res_reminder = $this->db->getConnection ( "IRECRUIT" )->insert($ins_reminder, array($params));
		$last_insertid = $res_reminder['insert_id'];
		return $last_insertid;
	}
	
	/**
	 * @method		getRequisitions
	 * @param		$POST
	 * @return		array
	 * @type		json
	 */
	public function getReminders($POST='') {
		
		/**
		 * @input json data
		 */
		if($POST == '') $POST = file_get_contents( 'php://input' );
		$JSON = json_decode($POST, true);
		
		global $USERID, $USERROLE;
		$sort     =   'ASC';
		
		//Set parameters for prepared query
		$params   =   array(":RMonth"=>$JSON['RMonth'], ":RYear"=>$JSON['RYear'], ":UserID"=>$USERID);
		
		$selreminders = "SELECT SID, DAY(DateTime) as RDay, Subject, AppointmentReminderID,
						 DATE_FORMAT(DateTime,'%h:%i %p') AS RTime, DATE_FORMAT(DateTime,'%m/%d/%Y') AS RDate, UserID
						 FROM AppointmentReminder 
						 WHERE MONTH(DateTime) = :RMonth 
						 and YEAR(DateTime) = :RYear 
						 and UserID = :UserID";
		
		if(isset($JSON['ReminderType']) && $JSON['ReminderType'] == "DAY") {
			//Set Params
			$params[":SDay"]	= array($JSON['SDay']);
			$selreminders .= " AND DAY(DateTime) = :SDay";
		}
		
		//Set Params
		$params[":SDate"]	= array($JSON['SDate']);
		$selreminders .= " AND DATE(DateTime) >= :SDate";
		
		if(isset($JSON['Sort']) && $JSON['Sort'] != "") {
			$selreminders .= $JSON['Sort'];
		}
		
		//Set Parameters
		$resreminders = $this->db->fetchAllAssoc($selreminders, array($params));

		$rows = array();
		foreach ($resreminders['results'] as $row) {
			$rows[$row['RDay']][] = $row;
		}
		return json_encode($rows);
	}
	
	/**
	 * @method		countReminders
	 * @param		$POST(optional)
	 * @return		array
	 * @type		json
	 */
	public function countReminders($POST='') {
		
		if($POST == '') $POST = file_get_contents( 'php://input' );
		$JSON = json_decode($POST, true);
	
		global $USERID, $USERROLE;
	
		//Set parameters for prepared query
		$params[":SMonth"]	= array($JSON['SMonth']);
		$params[":SYear"]	= array($JSON['SYear']);
		$params[":UserID"]	= array($USERID);
		
		$selreminders = "SELECT COUNT(*) as ReminderCount FROM AppointmentReminder WHERE UserID = :UserID";
	
		if($JSON['ReminderType'] == "DAY") {
			//Set Params
			$params[":SDay"]	= array($JSON['SDay']);
			$selreminders .= " AND DAY(DateTime) = :SDay";
		}
	
		$selreminders .= " AND MONTH(DateTime) = :SMonth";
		$selreminders .= " AND YEAR(DateTime) = :SYear";
		$selreminders .= " ORDER BY DateTime ASC";
		
		//Set Parameters
		$row = $this->db->fetchAssoc ( $selreminders, array($params));
	
		return $row['ReminderCount'];
	}
	
	
	/**
	 * @method		getAppointmentReminderDetailInfo
	 * @param		$AppointmentReminderID
	 * @return		array @type json
	 */
	public function getAppointmentReminderDetailInfo($columns = "*", $AppointmentReminderID) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$params = array(":AppointmentReminderID"=>$AppointmentReminderID);
	
		$selreminder = "SELECT $columns FROM AppointmentReminder
						WHERE AppointmentReminderID = :AppointmentReminderID";
		$resreminder = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($selreminder, array($params));
	
		return $resreminder;
	}
	
	/**
	 * @method		delAppointmentReminderInfo
	 * @param		$OrgID, $Active, $IsAdmin
	 * @return		array @type json
	 */
	public function delAppointmentReminderInfo($AppointmentReminderID) {
		
		$params = array(":AppointmentReminderID"=>$AppointmentReminderID);
	
		$del_app_rem = "DELETE FROM AppointmentReminder WHERE AppointmentReminderID = :AppointmentReminderID";
		$res_app_rem = $this->db->getConnection ( "IRECRUIT" )->delete($del_app_rem, array($params));
	
		return $res_app_rem;
	}
	
	/**
	 * @method		updAppointmentReminderInfo
	 * @param		$sidatetime, $sisubject, $arid
	 * @return		array @type json
	 */
	public function updAppointmentReminderInfo($sidatetime, $sisubject, $arid) {
		
		//Set parameters
		$params = array(":DateTime"=>$sidatetime, ":Subject"=>$sisubject, ":AppointmentReminderID"=>$arid);
	
		$upd_appointment = "UPDATE AppointmentReminder
						   SET
						   DateTime = :DateTime,
						   Subject = :Subject
						   WHERE AppointmentReminderID = :AppointmentReminderID";
		$res_appointment = $this->db->getConnection ( "IRECRUIT" )->update($upd_appointment, array($params));
	
		return $res_appointment;
	}
}
