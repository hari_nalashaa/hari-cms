<?php
/**
 * @class		AWSLoader
 */

use Aws\Ec2\Ec2Client;

class AWSInstances {
	
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        
    }
    
    /**
     * @method    getInstancesList
     *              
     */
    function getInstancesList() {

	if (SERVER == "DEVELOPMENT") {
          putenv("HOME=/usr/home/cmsdev");
        } else if (SERVER == "QUALITY") {
          putenv("HOME=/usr/home/cmsadmin");
        } else {
          putenv("HOME=/usr/home/irecruit");
        }

	$rtn = "";
        
        $region     =   "us-east-1";
        $profile    =   "default";
        
        
        $ec2Client      =   new Ec2Client([
            'region'    =>  $region,
            'version'   =>  '2016-11-15',
            'profile'   =>  $profile
        ]);
        
        $result         =   $ec2Client->describeInstances();
        
        $rtn .= "<br><br>\n";
        $reservations   =   $result['Reservations'];
        
        $instances_info =   array();
        
        $rtn .= "<table border='1' width='100%'>\n";
        
        $rtn .= "<tr>";
        $rtn .= "<th>Instance Name</th>";
        $rtn .= "<th>State</th>";
        $rtn .= "<th>Instance ID</th>";
        $rtn .= "<th>Image ID</th>";
        $rtn .= "<th>Private Dns Name</th>";
        $rtn .= "<th>Instance Type</th>";
        $rtn .= "<th>Security Group</th>";
        $rtn .= "<th>Action</th>";
        $rtn .= "</tr>\n";
        
        foreach ($reservations as $reservation) {

            $instances = $reservation['Instances'];
            foreach ($instances as $instance) {

	     if ($instance['InstanceId'] != "i-fec0e3d1") {
                
                $instanceName = '';
                foreach ($instance['Tags'] as $tag) {
                    if ($tag['Key'] == 'Name') {
                        $instanceName = $tag['Value'];
                    }
                }
                
                $instances_info[$instance['InstanceId']]   =   $instance['State']['Name'];
                
                $rtn .= "<tr>";
                
                $rtn .= "<td>";
                $rtn .= $instanceName;
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $instance['State']['Name'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $instance['InstanceId'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $instance['ImageId'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $instance['PrivateDnsName'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $instance['InstanceType'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                $rtn .= $instance['SecurityGroups'][0]['GroupName'];
                $rtn .= "</td>";
                
                $rtn .= "<td>";
                if($instance['InstanceId'] == 'i-01eb045345138f352'
                    && $instances_info['i-01eb045345138f352'] == "running") {
                        $rtn .= "<a href='?instance=" . $instance['InstanceId'] . "&action=STOP'>STOP</a>";
                } else if($instance['InstanceId'] == 'i-01eb045345138f352'
                        && $instances_info['i-01eb045345138f352'] == "stopped") {
                            $rtn .= "<a href='?instance=" . $instance['InstanceId'] . "&action=START'>START</a>";
                }
                $rtn .= "</td>";
                $rtn .= "</tr>\n";
	     
	    } // end instance filter
		    //
          } // end foreach

        } // end foreach
        
        $rtn .= "</table>\n";

	if (isset($_GET['action']) && $_GET['action'] == 'START') {
	   if (isset($_GET['instance']) && $_GET['instance'] != '') {
		   if ($instances_info['i-01eb045345138f352'] == "stopped") {
			$result = $ec2Client->startInstances(array(
			    'InstanceIds' => array($_GET['instance']),
			));
	           $rtn .= "<br>\n" . $_GET['instance'] . " has been sent start command.";
		   }
	   }
	} else if (isset($_GET['action']) && $_GET['action'] == 'STOP') {
	   if (isset($_GET['instance']) && $_GET['instance'] != '') {
		   if ($instances_info['i-01eb045345138f352'] == "running") {
			$result = $ec2Client->stopInstances(array(
			    'InstanceIds' => array($_GET['instance']),
			));
	           $rtn .= "<br>\n" . $_GET['instance'] . " has been sent stop command.";
		   }
	   }
	}
	
	return $rtn;
    }
}
