<?php
/**
 * @class		LeadGeneratorEmailTemplate
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class LeadGeneratorEmailTemplate {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	
	/**
	 * @method		insUpdProcessLeadTemplate
	 * @param 		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @return		array
	 */
	function insUpdProcessLeadTemplate($OrgID, $RegUserSub, $RegUserMsg, $NonRegUserSub, $NonRegUserMsg) {
	
	    $params_info           =   array(
                                        ":OrgID"                      =>  $OrgID,
                                        ":RegisteredUserSubject"      =>  $RegUserSub, 
                                        ":RegisteredUserMessage"      =>  $RegUserMsg, 
                                        ":NonRegisteredUserSubject"   =>  $NonRegUserSub, 
                                        ":NonRegisteredUserMessage"   =>  $NonRegUserMsg,
                                        ":URegisteredUserSubject"     =>  $RegUserSub,
                                        ":URegisteredUserMessage"     =>  $RegUserMsg,
                                        ":UNonRegisteredUserSubject"  =>  $NonRegUserSub,
                                        ":UNonRegisteredUserMessage"  =>  $NonRegUserMsg
	                               );
	    $ins_pro_lead_template  =   "INSERT INTO ProcessLeadEmailTemplate(OrgID, RegisteredUserSubject, RegisteredUserMessage, NonRegisteredUserSubject, NonRegisteredUserMessage, LastModifiedDateTime)";
	    $ins_pro_lead_template .=   " VALUES(:OrgID, :RegisteredUserSubject, :RegisteredUserMessage, :NonRegisteredUserSubject, :NonRegisteredUserMessage, NOW())";
        $ins_pro_lead_template .=   " ON DUPLICATE KEY UPDATE RegisteredUserSubject = :URegisteredUserSubject, RegisteredUserMessage = :URegisteredUserMessage, NonRegisteredUserSubject = :UNonRegisteredUserSubject, NonRegisteredUserMessage = :UNonRegisteredUserMessage, LastModifiedDateTime = NOW()";
	    $res_pro_lead_template  =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_pro_lead_template, array($params_info) );
	
	    return $res_pro_lead_template;
	}
	
	
	/**
	 * @method     getProcessLeadTemplateInfo
	 * @params     $OrgID
	 */
	function getProcessLeadTemplateInfo($OrgID) {
	    
	    $params_info           =   array(":OrgID"=>$OrgID);
	    $sel_pro_lead_template =   "SELECT * FROM ProcessLeadEmailTemplate WHERE OrgID = :OrgID";
	    $res_pro_lead_template =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_pro_lead_template, array($params_info) );
	    
	    return $res_pro_lead_template;
	    
	}	
}
