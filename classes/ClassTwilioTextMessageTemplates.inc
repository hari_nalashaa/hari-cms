<?php
/**
 * @class		TwilioTextMessageTemplates
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioTextMessageTemplates {

    public $db;

	var $conn_string       		=   "IRECRUIT";

	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		// Create a new instance of a SOAP 1.2 client
		$this->db	=	Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	} // end function
	
	/**
	 * @method		insDefaultTwilioTextMessagingTemplates
	 * @return		array
	 */
	function insDefaultTwilioTextMessagingTemplates($OrgID) {
	
		$params		=	array(":OrgID"=>$OrgID);
		$sel_info	=	"SELECT * FROM TwilioTextMessagingTemplates WHERE OrgID = :OrgID";
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
	
		if($res_info['count'] == 0)	{
			$params		=	array(":OrgID"=>"MASTER");
			$sel_info	=	"SELECT * FROM TwilioTextMessagingTemplates WHERE OrgID = :OrgID";
			$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
			$templates	=	$res_info['results'];
			
			foreach($templates as $template_key=>$template_value) {

				//Get global unique id
				$GuID		=	G::Obj('GenericLibrary')->getGlobalUniqueID($OrgID);
					
				// Set parameters
				$params 	=	array (
									":TemplateGuID"	=>	$GuID,
									":OrgID"      	=>  $OrgID,
									":PassedValue"	=>  $template_value['PassedValue'],
									":DisplayText"	=>  $template_value['DisplayText'],
									":SortOrder"	=>	$template_value['SortOrder']
								);
				// Update session values
				$ins_info  	=	'INSERT INTO TwilioTextMessagingTemplates(TemplateGuID, OrgID, PassedValue, DisplayText, SortOrder, CreatedUpdatedDateTime) VALUES(:TemplateGuID, :OrgID, :PassedValue, :DisplayText, :SortOrder, NOW())';
				// Settings value
				$res_info 	=	$this->db->getConnection ( $this->conn_string )->insert ( $ins_info, array ($params) );

			}
				
		}
	}
	
	/**
	 * @method		getTwilioTextMessagingTemplatesByOrgID
	 * @return		array
	 */
	function getTwilioTextMessagingTemplatesByOrgID($OrgID) {
		
		$params		=	array(":OrgID"=>$OrgID);
		$sel_info	=	"SELECT * FROM TwilioTextMessagingTemplates WHERE OrgID = :OrgID ORDER BY SortOrder ASC";
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
		
		return $res_info;
	}
	
	/**
	 * @method		getTwilioTextMessagingTemplatesByTemplateGuID
	 * @return		array
	 */ 
	function getTwilioTextMessagingTemplatesByTemplateGuID($TemplateGuID) {
	
		$params		=	array(":TemplateGuID"=>$TemplateGuID);
		$sel_info	=	"SELECT * FROM TwilioTextMessagingTemplates WHERE TemplateGuID = :TemplateGuID";
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
	
		return $res_info;
	}	

	/**
	 * @method		getMaxSortOrderByOrgID
	 * @return		array
	 */
	function getMaxSortOrderByOrgID($OrgID) {
	
		$params		=	array(":OrgID"=>$OrgID);
		$sel_info	=	"SELECT MAX(SortOrder) AS SortOrder FROM TwilioTextMessagingTemplates where OrgID = :OrgID";
		$res_info	=	$this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
	
		return $res_info['SortOrder'];
	}	
	
	/**
	 * @method  	insTextMessagingTemplate
	 */
	public function insTextMessagingTemplate($OrgID, $PassedValue, $DisplayText) {
		
		//Get Max Sort Order
		$SortOrder	=	$this->getMaxSortOrderByOrgID($OrgID);
		
		//Get global unique id
		$GuID		=	G::Obj('GenericLibrary')->getGlobalUniqueID($OrgID);
		
		// Set parameters
		$params 	=	array (
							":TemplateGuID"	=>	$GuID,							
							":OrgID"      	=>  $OrgID,
							":PassedValue"	=>  $PassedValue,
							":DisplayText"	=>  $DisplayText,
							":SortOrder"	=>	($SortOrder + 1),
						);
		// Update session values
		$ins_info  	=	'INSERT INTO TwilioTextMessagingTemplates(TemplateGuID, OrgID, PassedValue, DisplayText, SortOrder, CreatedUpdatedDateTime) VALUES(:TemplateGuID, :OrgID, :PassedValue, :DisplayText, :SortOrder, NOW())';
		// Settings value
		$res_info 	=	$this->db->getConnection ( $this->conn_string )->insert ( $ins_info, array ($params) );
	
		return $res_info;
	}
	
	/**
	 * @method  	updTextMessagingTemplate
	 */
	public function updTextMessagingTemplate($GuID, $OrgID, $PassedValue, $DisplayText) {

		//Get global unique id
		$GuID		=	G::Obj('GenericLibrary')->getGlobalUniqueID($OrgID);
	
		// Set parameters
		$params 	=	array (
							":TemplateGuID"	=>	$GuID,
							":OrgID"      	=>  $OrgID,
							":PassedValue"	=>  $PassedValue,
							":DisplayText"	=>  $DisplayText
						);
		// Update session values
		$upd_info  	=	'UPDATE TwilioTextMessagingTemplates SET PassedValue = :PassedValue, DisplayText = :DisplayText WHERE OrgID = :OrgID AND TemplateGuID = :TemplateGuID';
		// Settings value
		$res_info 	=	$this->db->getConnection ( $this->conn_string )->update ( $upd_info, array ($params) );
	
		return $res_info;
	}
	
	/**
	 * @method  	delTextMessagingTemplatesByOrgID
	 */
	public function delTextMessagingTemplatesByOrgID($OrgID) {
	
		$params		=	array(":OrgID"=>$OrgID);
		// Update session values
		$del_info  	=	'DELETE FROM TwilioTextMessagingTemplates WHERE OrgID = :OrgID';
		// Settings value
		$res_info 	=	$this->db->getConnection ( $this->conn_string )->delete ( $del_info, array ($params) );
	
		return $res_info;
	}
	
	/**
	 * @method	updateTextMessageTemplatesSortOrder
	 * @param	$FormID, $SortOrder
	 * @return	integer
	 */
	function updateTextMessageTemplatesSortOrder($OrgID) {
	
		//Begin the transaction
		$this->db->getConnection( $this->conn_string )->beginTransaction();
	
		for($fi = 0; $fi < count($_POST['forms_info']); $fi++) {
	
			$TemplateGuID		=	$_POST['forms_info'][$fi]['GuID'];
			$SortOrder			=	$_POST['forms_info'][$fi]['SortOrder'];
	
			//Set parameters for prepared query
			$params				=	array(":OrgID"=>$OrgID, ":TemplateGuID"=>$TemplateGuID, ":SortOrder"=>$SortOrder);
			$update_sort_order	=	"UPDATE TwilioTextMessagingTemplates SET SortOrder = :SortOrder";
			$update_sort_order 	.=	" WHERE OrgID = :OrgID AND TemplateGuID = :TemplateGuID";
			$result_sort_order	=	$this->db->getConnection( $this->conn_string )->update ( $update_sort_order, array($params) );
		}
	
		//End the transaction
		$this->db->getConnection( $this->conn_string )->endTransaction();	
	}
	
}
