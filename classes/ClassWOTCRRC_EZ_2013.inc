<?php
/**
 * @class		WOTCRRC_EZ_2013
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCRRC_EZ_2013
{
    public $db;

    var $conn_string       =   "WOTC";

    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getRRC_EZ_2013Info
     * @param		string $columns
     * @param		string $where_info
     * @param 		string $order_by
     * @param 		string $info
     * @return 		array
     */
    function getRRC_EZ_2013Info($columns, $where_info = array(), $order_by = '', $info = array()) {
    
        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_rrc    =   "SELECT $columns FROM RRC_EZ_2013";
    
        if(count($where_info) > 0) {
            $sel_rrc .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($order_by != "") $sel_rrc .= " ORDER BY " . $order_by;
    
        $res_rrc    =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_rrc, $info );
    
        return $res_rrc;
    }
    
}
