<?php
/**
 * @class		MatrixCare
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class MatrixCare {
	
    var $client_secret      =   "Basic aXJlY3J1aXRpcmlzY2xpZW50Omdvb2RsdWNr";
    var $ident_url          =   "https://auth.soneto.net/1.0";
    var $tenant             =   "SonetoAPIDemo";
    var $api_router_url     =   "https://api.soneto.net/v1/api/";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    
    /**
     * @method		insertMatrixCareLoginInformation
     * @param		string $MatrixUserName
     * @param		string $MatrixPassword
     * @return		array
     * @tutorial	create new record for that organization if not exist
     */
    function insertMatrixCareLoginInformation($OrgID, $CompanyName, $MatrixUserName, $MatrixPassword, $Tenant, $UpperCase) {
        global $USERID, $USERROLE;
    
        $CompanyID              =   uniqid(str_replace(".", "", microtime(true)));
        $default_company_info   =   $this->getDefaultCompanyInfo($OrgID);
        
        //Set parameters for prepared query
        $matrix_care_info       =   array(
                                        "CompanyName"  =>  $CompanyName,
                                        "CompanyID"    =>  $CompanyID,
                                        "UserName"     =>  $MatrixUserName, 
                                        "Password"     =>  $MatrixPassword, 
                                        "Tenant"       =>  $Tenant, 
                                        "UserID"       =>  $USERID, 
                                        "OrgID"        =>  $OrgID,
                                        "UpperCase"    =>  $UpperCase
                                    );

        if($default_company_info['CompanyID'] == "") {
            $matrix_care_info['CompanyIsDefault'] = 'Y';
        }
        
        $ins_login_info         =   $this->db->buildInsertStatement('MatrixCare', $matrix_care_info);
        $res_login_info         =   $this->db->getConnection ( "IRECRUIT" )->insert($ins_login_info['stmt'], $ins_login_info["info"]);
        	
        $last_insert_id         =   $res_login_info['insert_id'];
    
        return $CompanyID;
    }
    
    
    /**
     * @method		updateMatrixCareLoginInformation
     * @param		string $MatrixUserName
     * @param		string $MatrixPassword
     * @return		array
     * @tutorial	update record for that organization
     */
    function updateMatrixCareLoginInformation($OrgID, $CompanyName, $CompanyID, $MatrixUserName, $MatrixPassword, $Tenant, $UpperCase) {
        global $USERID, $USERROLE;
    
        $default_company_info   =   $this->getDefaultCompanyInfo($OrgID);
    
        //Set parameters for prepared query
        $params                 =   array(
                                        ":CompanyName"  =>  $CompanyName,
                                        ":CompanyID"    =>  $CompanyID,
                                        ":UserName"     =>  $MatrixUserName,
                                        ":Password"     =>  $MatrixPassword,
                                        ":Tenant"       =>  $Tenant,
                                        ":OrgID"        =>  $OrgID,
                                        ":UpperCase"    =>  $UpperCase
                                        );
        //Set Prameters
        $updlogininfo           =   "UPDATE MatrixCare SET  CompanyName = :CompanyName, UserName = :UserName, Password = :Password, Tenant = :Tenant, UpperCase = :UpperCase";
    
        if($default_company_info['CompanyID'] == "") {
            $updlogininfo .=  ", CompanyIsDefault   =   'Y'";
        }
    
        $updlogininfo          .=   " WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
        $reslogininfo           =   $this->db->getConnection ( "IRECRUIT" )->update($updlogininfo, array($params));
    
        $affected_rows          =   $reslogininfo['affected_rows'];
    
        return $affected_rows;
    }
    
    
    /**
     * @method		updateCompanyIsDefault
     * @tutorial
     */
    function updateCompanyIsDefault($OrgID, $CompanyID) {
    
        if(isset($_REQUEST['ddlMatrixCareIsDefault']) && $_REQUEST['ddlMatrixCareIsDefault'] == 'Y') {
            //Set parameters for prepared query
            $params                 =   array(":OrgID"=>$OrgID);
            //Set Prameters
            $upd_cloud_question     =   "UPDATE MatrixCare SET CompanyIsDefault = 'N' WHERE OrgID = :OrgID";
            $res_cloud_question     =   $this->db->getConnection ( "IRECRUIT" )->update($upd_cloud_question, array($params));
            
            //Set parameters for prepared query
            $params                 =   array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID);
            //Set Prameters
            $upd_cloud_question     =   "UPDATE MatrixCare SET CompanyIsDefault = 'Y' WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
            $res_cloud_question     =   $this->db->getConnection ( "IRECRUIT" )->update($upd_cloud_question, array($params));
        }

        //Get the default company information
        $default_comp_info          =   $this->getDefaultCompanyInfo($OrgID);
    
        if($default_comp_info['OrgID'] == "") {
            //Set parameters for prepared query
            $params                 =   array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID);
            //Set Prameters
            $upd_cloud_question     =   "UPDATE MatrixCare SET CompanyIsDefault = 'Y' WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
            $res_cloud_question     =   $this->db->getConnection ( "IRECRUIT" )->update($upd_cloud_question, array($params));
        }
        
        return $res_cloud_question['affected_rows'];
    }
    
    
    /**
     * @method      getMatrixCareLoginSessionID
     * @param       string $OrgID
     * @param       string $CompanyID
     * @return      array
     */
    function getMatrixCareLoginSessionID($OrgID, $CompanyID) {
        global $MatrixCareObj;
        
        list($MatrixCareInfo, $MatrixCount) = $MatrixCareObj->getMatrixCareLoginInformation($OrgID, $CompanyID);
        
        $matrixcare_username    =   trim($MatrixCareInfo["UserName"]);
        $matrixcare_password    =   trim($MatrixCareInfo["Password"]);
        $matrixcare_tenant      =   trim($MatrixCareInfo["Tenant"]);

        if (($matrixcare_username != "") && ($matrixcare_password != "") && ($matrixcare_tenant != "")) { 
              $config_matrixcare_url  =   $MatrixCareObj->ident_url . "/core/connect/token";
              $input_data             =   array("grant_type"=>"password", "username"=>$matrixcare_username, "password"=>$matrixcare_password, "scope"=>"openid roles soneto_data", "acr_values"=>"tenant:".$matrixcare_tenant." bypass_tenant_field:true");
           
              $matrixcare_info        =   $MatrixCareObj->getMatrixCareAccessTokenInfo($config_matrixcare_url, $input_data);
              $matrixcare_info        =   json_decode($matrixcare_info, true);
              $matrixcare_info['tenant']    =   $matrixcare_tenant;
        }
        
        return $matrixcare_info;
    }
    
    
    /**
     * @method      getMatrixCareContactInfo
     * 
     */
    function getMatrixCareContactInfo($OrgID, $RequestID, $ApplicationID) {
        
        $params_web_form_id =   array(":OrgID"=>$OrgID, ":orgid"=>$OrgID);
        $sel_web_form_id    =   "SELECT WebFormID FROM WebForms WHERE OrgID = :orgid AND WebFormID IN (
                                SELECT DISTINCT(WebFormID) FROM WebFormQuestions WHERE OrgID = :OrgID AND FormID = 'emergencycontact') and FormStatus = 'Active' LIMIT 1";
        $res_web_form_id    =   G::Obj('GenericQueries')->getRowInfoByQuery($sel_web_form_id, array($params_web_form_id));
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID, ":WebFormID"=>$res_web_form_id['WebFormID']);
        //Select contact information        
        $sel_info           =   "SELECT * FROM WebFormData WHERE OrgID = :OrgID AND RequestID = :RequestID AND ApplicationID = :ApplicationID AND WebFormID = :WebFormID ORDER BY QuestionOrder ASC";
        $res_info           =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_info, array($params));
         
        return $res_info;
    }
    
    
    /**
     * @method		getDefaultCompanyInfo
     * @tutorial	get matrix care details based on organization
     */
    function getDefaultCompanyInfo($OrgID) {
    
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID);
        //Set Prameters
        $sel_info   =   "SELECT * FROM MatrixCare WHERE OrgID = :OrgID AND CompanyIsDefault = 'Y'";
        $res_info   =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_info, array($params));
    
        return $res_info;
    }
    
    /**
     * @method		getMatrixCareApiCompanies
     * @tutorial	get matrix care details based on organization
     */
    function getMatrixCareCompanies($OrgID) {
    
        //Set parameters for prepared query
        $params   = array(":OrgID"=>$OrgID);
        //Set Prameters
        $sel_info = "SELECT * FROM MatrixCare WHERE OrgID = :OrgID";
        $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_info, array($params));
    
        return $res_info;
    }
    
    
    /**
     * @method		getMatrixCareCompaniesCount
     * @tutorial	get matrix care details based on organization
     */
    function getMatrixCareCompaniesCount($OrgID) {
    
        //Set parameters for prepared query
        $params   = array(":OrgID"=>$OrgID);
        //Set Prameters
        $sel_info = "SELECT COUNT(*) AS MatrixCareCompaniesCount FROM MatrixCare WHERE OrgID = :OrgID";
        $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_info, array($params));
    
        return $res_info['MatrixCareCompaniesCount'];
    }
    

    
    /**
     * @method		delMatrixCareApiLoginInformation
     * @tutorial	delete matrix care details based on organization, company
     */
    function delMatrixCareApiLoginInformation($OrgID, $CompanyID) {
    
        $mc_companies_list      =   $this->getMatrixCareCompanies($OrgID);
        $FirstCompanyID         =   $mc_companies_list['results'][0]['CompanyID'];
         
        //Set parameters for prepared query
        $params                 =   array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID);
        //Set Prameters
        $del_logininfo          =   "DELETE FROM MatrixCare WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
        $res_logininfo          =   $this->db->getConnection ( "IRECRUIT" )->delete($del_logininfo, array($params));
    
        $default_company_info   =   $this->getDefaultCompanyInfo($OrgID);
         
        if($default_company_info['CompanyID'] == '') {
            //Params
            $default_params     =   array(":OrgID"=>$OrgID, ":CompanyID"=>$FirstCompanyID);
            //Set Prameters
            $upd_cloud_question =   "UPDATE MatrixCare SET CompanyIsDefault  =  'Y' WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
            $res_cloud_question =   $this->db->getConnection ( "IRECRUIT" )->update($upd_cloud_question, array($default_params));
        }
         
        return $res_logininfo;
    }
    
    
    /**
     * @method		getMatrixCareLoginInformation
     * @tutorial	get matrix care details based on organization
     */
    function getMatrixCareLoginInformation($OrgID, $CompanyID) {
    
        //Set parameters for prepared query
        $params        = array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID);
    
        //Set Prameters
        $sel_logininfo = "SELECT * FROM MatrixCare WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
        $res_logininfo = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_logininfo, array($params));
    
        $count		   = $res_logininfo['count'];
        $rowlogininfo  = @$res_logininfo['results'][0];
    
        return array($rowlogininfo, $count);
    }
    
    /**
     * @method      getMatrixCareAccessTokenInfo
     * @param       string $url
     * @param       array $input_data
     * @return      json_string
     */
    function getMatrixCareAccessTokenInfo($url, $input_data) {
        $data_string = http_build_query($input_data);
        
        //Set headers
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        $headers[] = 'Authorization: '. $this->client_secret;
        $headers[] = 'Content-Length: ' . strlen($data_string);
        
		$ch = curl_init($url);
		
		//Set request type, By default it is POST
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_type);
    	curl_setopt($ch, CURLOPT_POST, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		$result = curl_exec($ch);

		$info = curl_getinfo($ch);
		
		curl_close($ch);
	
		if($result === false)
		{
			echo 'Curl error: ' . curl_error($ch);
		}

		return $result;
    }
    
    
    /**
     * @method      getMatrixCareGiverInfo
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function getMatrixCareGiverInfo($url, $input_data, $headers_list) {
        $data_string = http_build_query($input_data);
    
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: '.$headers_list['token_type'].' '.$headers_list['access_token'];
        
        $ch = curl_init($url);
    
        //Set request type, By default it is POST
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_type);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $result = curl_exec($ch);
    
        $info = curl_getinfo($ch);
    
        curl_close($ch);
    
        if($result === false) {
            echo 'Curl error: ' . curl_error($ch);
        }

        return $result;
    }
    
    
    /**
     * @method      getOfficeIdsByMatrxCareApi
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function getOfficeIdsByMatrxCareApi($url, $headers_list) {
    
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: '.$headers_list['token_type'].' '.$headers_list['access_token'];
        
        $ch = curl_init($url);
    
        //Set request type, By default it is POST
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $result = curl_exec($ch);
    
        $info = curl_getinfo($ch);
        
        curl_close($ch);

        if($result === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
    
        return $result;
    }
    
    
    /**
     * @method      setMatrixCareGiverInfo
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function setMatrixCareGiverInfo($url, $input_data, $headers_list = array()) {
        
        $input_json_data = json_encode($input_data);
        
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: '.$headers_list['token_type'].' '.$headers_list['access_token'];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        
        $ch = curl_init($url);
    
        //Set request type, By default it is POST
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_type);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json_data);
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        $result = curl_exec($ch);
    
        $info = curl_getinfo($ch);
    
        curl_close($ch);
    
        if($result === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }

        return $result;
    
    }

    
    /**
     * @method      setMatrixCareContactInfo
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function setMatrixCareContactInfo($url, $input_data, $headers_list) {
    
        $input_json_data = json_encode($input_data);
    
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: '.$headers_list['token_type'].' '.$headers_list['access_token'];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
    
        $ch = curl_init($url);
    
        //Set request type, By default it is POST
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json_data);
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        
        $result = curl_exec($ch);
        $info   = curl_getinfo($ch);
        
        curl_close($ch);
    
        if($result === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
    
        return $result;
    }

    
    /**
     * @method      editMatrixCareContactInfo
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function editMatrixCareContactInfo($url, $input_data, $headers_list) {
    
        $input_json_data = json_encode($input_data);
    
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: '.$headers_list['token_type'].' '.$headers_list['access_token'];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
    
        $ch = curl_init($url);
    
        //Set request type, By default it is POST
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json_data);
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    
        $result = curl_exec($ch);
        $info   = curl_getinfo($ch);
    
        curl_close($ch);
    
        if($result === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
    
        return $result;
    }
    
    
    /**
     * @method      setCaregiverPhones
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function setCaregiverPhones($url, $input_data, $headers_list) {
    
        $input_json_data = json_encode($input_data);
    
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: '.$headers_list['token_type'].' '.$headers_list['access_token'];
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
    
        $ch = curl_init($url);
    
        //Set request type, By default it is PUT
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $input_json_data);
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    
        $result = curl_exec($ch);
        $info   = curl_getinfo($ch);
    
        curl_close($ch);
    
        if($result === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
    
        return $result;
    }
    
    
    /**
     * @method      delMatrixCareContactInfo
     * @param       string $url
     * @param       array $input_data
     * @param       array $headers_list
     * @return      json_string
     */
    function delMatrixCareContactInfo($url) {
    
        //Set headers
        $headers = array();
        $headers[] = 'Authorization: Bearer';
    
        $ch = curl_init($url);
    
        //Set request type, By default it is POST
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    
        $result = curl_exec($ch);
        $info   = curl_getinfo($ch);
        
        curl_close($ch);
    
        if($result === false) {
            echo 'Curl error: ' . curl_error($ch);
        }
    
        return $result;
    }

    
    /**
     * @method      getMatrixCareCountries
     */
    function getMatrixCareCountries() {

        //Set Prameters
        $sel_countries  =   "SELECT * FROM MatrixCareCountries";
        $res_countries  =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_countries);
        
        $count          =   $res_countries['count'];
        $info           =   @$res_countries['results'][0];
        
        $countries      =   array();
        for($i = 0; $i < $count; $i++) {
            $countries[$info[$i]['ID']]  =   $info[$i]['Name'];  
        }
        
        return $countries;
    }
    
    
    /**
     * @method      getMatrixCareMaritalStatus
     */
    function getMatrixCareMaritalStatus() {
        
        //Set Prameters
        $sel_maritalstatus  =   "SELECT * FROM MatrixCareMaritalStatus";
        $res_maritalstatus  =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_maritalstatus);
        
        $marital_status     =   array();
        $count              =   $res_maritalstatus['count'];
        $info               =   @$res_maritalstatus['results'][0];
        
        $marital_status      =   array();
        for($i = 0; $i < $count; $i++) {
            $marital_status[$info[$i]['MaritalStatusID']]  =   $info[$i]['MaritalStatus'];  
        }
        
        return $marital_status;
    }
    
    
    /**
     * @method      getMatrixCareRaceInformation
     */
    function getMatrixCareRaceInformation() {
        
        //Set Prameters
        $sel_raceinfo   =   "SELECT * FROM MatrixCareRaceInformation";
        $res_raceinfo   =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_raceinfo);
        
        $count          =   $res_raceinfo['count'];
        $info           =   @$res_raceinfo['results'][0];
        
        $race_info      =   array();
        for($i = 0; $i < $count; $i++) {
            $race_info[$info[$i]['RaceID']]  =   $info[$i]['RaceName'];  
        }
        
        return $race_info;
    }
    
    /**
     * @method      getMatrixCareOfficeIds
     */
    function getMatrixCareOfficeIds($OrgID, $CompanyID) {

        //Set Prameters
        $params_info    =   array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID);
        $sel_info       =   "SELECT * FROM MatrixCareOfficeIds WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
        $res_info       =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_info, array($params_info));
    
        return $res_info;
    }
    
    /**
     * @method      insMatrixCareOfficeIds
     */
    function insMatrixCareOfficeIds($OrgID, $CompanyID, $branches_list) {
    
        $branches_info  =   json_encode($branches_list);
        //Set Prameters
        $params_info    =   array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID, ":IBranchesInfo"=>$branches_info, ":UBranchesInfo"=>$branches_info);
        $sel_info       =   "INSERT INTO MatrixCareOfficeIds(OrgID, CompanyID, BranchesInfo) VALUES(:OrgID, :CompanyID, :IBranchesInfo) ON DUPLICATE KEY UPDATE BranchesInfo = :UBranchesInfo";
        $res_info       =   $this->db->getConnection ( "IRECRUIT" )->insert($sel_info, array($params_info));
    
        return $res_info;
    }

    /**
     * @method      updMatrixCareOfficeIds
     */
    function updMatrixCareOfficeIds($OrgID, $CompanyID, $OfficeID) {
    
        //Set Prameters
        $params_info    =   array(":OrgID"=>$OrgID, ":CompanyID"=>$CompanyID, ":OfficeID"=>$OfficeID);
        $sel_info       =   "UPDATE MatrixCareOfficeIds SET DefaultValue = :OfficeID WHERE OrgID = :OrgID AND CompanyID = :CompanyID";
        $res_info       =   $this->db->getConnection ( "IRECRUIT" )->insert($sel_info, array($params_info));
    
        return $res_info;
    }
    
}
?>
