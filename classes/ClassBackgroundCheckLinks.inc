<?php
/**
 * @class		BackgroundCheckLinks
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class BackgroundCheckLinks {
	
	var $displayNavigation = 'off';
	var $navigationLink = '#';
	var $navigationTarget = '_self';
	var $navigationTitle = 'Background Check Links';
	
	/**
	 *
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
		
		global $OrgID;
		
		//Set parameters for prepared query
		$params   =   array(":OrgID"=>$OrgID);
		
		$query    =   "SELECT * FROM BackgroundCheckLinks WHERE OrgID = :OrgID ORDER BY Company";
		$BCL      =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $query, array($params) );
		
		if (is_array($BCL) && count ( $BCL ) >= 1) {
			$this->navigationLink = $BCL ['URL'];
			$this->navigationTarget = '_blank';
			$this->displayNavigation = 'on';
		} else if (isset($cnt) && $cnt > 1) {
			$this->navigationLink = IRECRUIT_HOME . 'applicants.php?action=backgroundcheck';
			$this->displayNavigation = 'on';
		}
	}
	
	/**
	 * @method getAdminPage
	 */
	public function getAdminPage() {
		global $OrgID;
		
		$rtn = '';
		
		if (isset ( $_GET ['delete'] )) {
			
			//Set parameters for prepared query
			$params = array(":OrgID"=>$OrgID, ":Company"=>$_GET ['delete']);
			
			$del_bck_chk_links = "DELETE FROM BackgroundCheckLinks WHERE OrgID = :OrgID AND Company = :Company";
			$this->db->getConnection ( "IRECRUIT" )->delete ( $del_bck_chk_links, array($params) );
		}
		
		if ($_POST ['process'] == "Y") {
			
			for($i = 1; $i <= $_POST ['cnt']; $i ++) {
				
				$company = 'Company-' . $i;
				$url = 'URL-' . $i;
				$instructions = 'Instructions-' . $i;
				
				if ($_POST [$company] != "") {
					//Set parameters for prepared query
					$params = array(":OrgID"=>$OrgID,
									":ICompany"=>$_POST [$company],
									":IURL"=>$_POST [$url],
									":IInstructions"=>$_POST [$instructions],
									":UCompany"=>$_POST [$company],
									":UURL"=>$_POST [$url],
									":UInstructions"=>$_POST [$instructions]);
					
					$query = "INSERT INTO BackgroundCheckLinks
					          (OrgID, Company, URL, Instructions)
					          VALUES (:OrgID, :ICompany, :IURL, :IInstructions) ON DUPLICATE KEY
							  UPDATE Company = :UCompany, URL = :UURL, Instructions = :UInstructions";
					
					$this->db->getConnection ( "IRECRUIT" )->insert ( $query, array($params) );
				}
			} // end for
		} // end if process
		
		$rtn .= '<div style="margin-left:10px;">';
		$rtn .= '<form method="POST" action="administration.php">';
		$rtn .= 'Enter your background check companies here with any special instructions.';
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID);
		
		$query = "SELECT * FROM BackgroundCheckLinks WHERE OrgID = :OrgID ORDER BY Company";
		$result = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $query, array($params) );
		$i = 0;
		
		foreach ( $result ['results'] as $BCL ) {
			$i ++;
			$rtn .= '<div style="margin-left:10px;margin-top:15px;">';
			$rtn .= '<b>Company:</b>&nbsp;';
			$rtn .= '<input type="text" name="Company-' . $i . '" value="' . $BCL ['Company'] . '" size="30" maxlength="255">';
			$rtn .= '&nbsp;&nbsp;&nbsp;&nbsp;';
			$rtn .= '<b>URL:</b>&nbsp;';
			$rtn .= '<input type="text" name="URL-' . $i . '" value="' . $BCL ['URL'] . '" size="30" maxlength="255">';
			
			$rtn .= '<a href="administration.php?action=backgroundchecklinks&delete=' . urlencode ( $BCL ['Company'] ) . '" onclick="return confirm(\'Are you sure you want to delete this company?\n\n' . $BCL ['Company'] . '\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0 0 -4px 15px;"></a>';
			
			$rtn .= '<br>';
			$rtn .= '<b>Instructions:</b><br>';
			$rtn .= '<textarea name="Instructions-' . $i . '" cols="80" rows="6">';
			$rtn .= $BCL ['Instructions'];
			$rtn .= '</textarea>';
			$rtn .= '</div>';
		} // end while
		
		$i ++;
		$rtn .= '<div style="margin-left:10px;margin-top:15px;">';
		$rtn .= '<b>Company:</b>&nbsp;';
		$rtn .= '<input type="text" name="Company-' . $i . '" size="30" maxlength="255">';
		$rtn .= '&nbsp;&nbsp;&nbsp;&nbsp;';
		$rtn .= '<b>URL:</b>&nbsp;';
		$rtn .= '<input type="text" name="URL-' . $i . '" size="30" maxlength="255">';
		$rtn .= '<br>';
		$rtn .= '<b>Instructions:</b><br>';
		$rtn .= '<textarea name="Instructions-' . $i . '" cols="80" rows="6"></textarea>';
		$rtn .= '</div>';
		
		$rtn .= '</div>';
		$rtn .= '<div style="margin:20px auto;width:100px;">';
		$rtn .= '<input type="hidden" name="action" value="backgroundchecklinks">';
		$rtn .= '<input type="hidden" name="cnt" value="' . $i . '">';
		$rtn .= '<input type="hidden" name="process" value="Y">';
		$rtn .= '<input type="submit" value="Update Links" class="btn btn-primary">';
		$rtn .= '</form>';
		$rtn .= '</div>';
		return $rtn;
	} // end function
	
	/**
	 *
	 * @method getPage
	 */
	public function getPage() {
		global $OrgID;
		
		$rtn = '<div style="margin-left:20px;">';
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID);
		
		$query = "SELECT * FROM BackgroundCheckLinks WHERE OrgID = :OrgID ORDER BY Company";
		$result = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $query, array($params));
		
		foreach ( $result ['results'] as $BCL ) {
			
			$rtn .= '<div style="padding-left:10px;margin-top:10px;padding-bottom:4px;border-bottom:1px solid #dddddd;">';
			$rtn .= '<a href="' . $BCL ['URL'] . '" target="_blank" style="font-size:14pt;">' . $BCL ['Company'] . '</a>';
			
			if ($BCL ['Instructions']) {
				$rtn .= '<div style="margin: 0 0 4px 6px">';
				$rtn .= $BCL ['Instructions'];
				$rtn .= '</div>';
			}
			
			$rtn .= '</div>';
		}
		
		$rtn .= '</div>';
		
		return $rtn;
	} // end function
}
?>
