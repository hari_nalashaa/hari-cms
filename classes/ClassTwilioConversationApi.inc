<?php
/**
 * @class		TwilioConversationApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioConversationApi {
	use TwilioSettings;
	
    public $db                  =	"";
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	var $twilio_number			=	"+16572208043";	//+16572208043
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

	/**
	 * @method		createConversationResource
	 */
    public function createConversationResource($OrgID) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	   
    	try {
			$conversation_info	=	$client->conversations->v1->conversations
				                                          ->create(array(
				                                                       	"friendlyName" 	=>	"Conversation" . time() . rand()
				                                                   )
				                                          );
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-conversation-api.txt", serialize($conversation_info), "w+", false);
    		
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created the conversation", "Response"=>$conversation_info);
    	
    	}
		catch (RestException | TwilioException | Exception $e) {    	

    		//Log the file information
		    Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-conversation-api-error.txt", serialize($e), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create the conversation", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	
    }

    /**
     * @method		fetchConversationResource
     */
    public function fetchConversationResource($OrgID, $conversation_id) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
    		//Get Conversations
			$conversation	=	$client->conversations->v1->conversations($conversation_id)->fetch();    		 

    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully fetched the conversation", "Response"=>$conversation);
    	}
    	catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-fetch-conversation-api-error.txt", serialize($conversation_info), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create the conversation", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    /**
     * @method		fetchConversationParticipants
     */
    public function fetchConversationParticipants($OrgID, $conversation_id) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
			$participants = $client->conversations->v1->conversations($conversation_id)
                                          ->participants
                                          ->read(20);
    		
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully fetched the conversation participants", "Response"=>$participants);
    	}
    	catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-fetch-conversation-participants-api-error.txt", serialize($conversation_info), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to fetch the conversation participants", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    
    }
    
    /**
     * @method		fetchAllConversationResources
     */
    public function fetchAllConversationResources($OrgID) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

    	if(isset($account_info['AccountSid']) 
    	    && isset($account_info['AccountSid']) 
    	    && $account_info['AccountSid'] != ""
    	    && $account_info['AuthToken'] != "") {

    	    $client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	    
    	    try {
    	        //Fetch all conversations
    	        $conversations	=	$client->conversations->v1->conversations->read(); //read(array(), 20);
    	        
    	        // Display a confirmation message on the screen
    	        return array("Code"=>"Success", "Message"=>"Successfully fetched all conversations", "Response"=>$conversations);
    	    }
    	    catch (RestException | TwilioException | Exception $e) {
    	        
    	        //Log the file information
    	        Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-fetch-all-conversations-api-error.txt", serialize($conversation_info), "w+", false);
    	        
    	        //Return error information
    	        return array("Code"=>"Failed", "Message"=>"Failed to fetch all the conversations", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	    }
    	    
    	}
    
    }
    
    /**
     * @method		deleteConversationResource
     */
    public function deleteConversationResource($OrgID, $conversation_id, $del_type = "CRON") {
            
        $del_flag = "false";
        if($del_type == "CRON") {
            $conversation_info  =   G::Obj('TwilioConversationsArchive')->getTwilioConversationsArchiveByResourceID($conversation_id);
            
            if($conversation_info['ResourceID'] != "") {
                $del_flag = "true";
            }
        }
        else if($del_type == "MANUAL") {
            $del_flag = "true";
        }
        
        if($del_flag == "true") {

            $account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
            
            $client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
            
            try {
                //Delete a conversation based on conversation id
                $deleted_conversation	=	$client->conversations->v1->conversations($conversation_id)->delete();
                
                if($deleted_conversation == 1) {
                    G::Obj('TwilioConversationInfo')->delConversationResourceInfo($OrgID, $conversation_id);
                }
                
                // Display a confirmation message on the screen
                return array("Code"=>"Success", "Message"=>"Deleted conversation successfully", "Response"=>$deleted_conversation);
            }
            catch (RestException | TwilioException | Exception $e) {
                
                //Return error information
                return array("Code"=>"Failed", "Message"=>"Failed to delete the conversation", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
            }
            
        }
        else {
            //Return error information
            return array("Code"=>"Failed", "Message"=>"Failed to delete the conversation. It is failed to bypass the delete flag.");
        }
    }
    
    /**
     * @method		addConversationSMSParticipant
     */
    public function addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $user_info) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);

    	try {
    		$participant	=	$client->conversations->v1->conversations($resource_id)
								    	->participants
								    	->create(array(
								    			"messagingBindingAddress"		=>	$numbers_info['NID2'],	//Personal Number
								    			"messagingBindingProxyAddress" 	=>	$numbers_info['NID1'],
								    			"attributes"					=>	json_encode(array($user_info))
								    	)
	    							);
    		
	    	//Log the file information
	    	Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . uniqid() . "-add-sms-participant-api.txt", serialize($participant), "w+", false);
	    		
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully added the participant", "Response"=>$participant);
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
		    Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-add-sms-participant-api-error.txt", serialize($e), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create the conversation", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }

    /**
     * @method      updateConversationSMSParticipant
     */
    public function updateConversationSMSParticipant($OrgID, $resource_id, $participant_id) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
    		$participant	=	$client->conversations->v1->conversations($resource_id)
                                         ->participants($participant_id)
                                         ->update(array(
                                                      	"dateUpdated" 	=>	new \DateTime('2019-05-15T13:37:35Z'),
                                         				"attributes"	=>	json_encode(array("payload"=>"first meter")),
                                                  )
                                         );
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-update-sms-participant-api.txt", serialize($participant), "w+", false);
    		 
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully updated the participant", "Response"=>$participant);
    	} 
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-update-sms-participant-api-error.txt", serialize($e), "w+", false);
    			
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to update the participant", "Response"=>serialize($e));
		}
    }
    
    /**
     * @method		createConversationMessage
     */
    public function createConversationMessage($OrgID, $conversation_id, $author, $conversation_msg) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
    		$message	=	$client->conversations->v1->conversations($conversation_id)
							    	->messages
							    	->create(array(
							    			"author"		=>	$author,
							    			"body"			=>	$conversation_msg
							    	)
    						);
									    	
	    	//Log the file information
	    	Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-conversation-msg-api.txt", serialize($message), "w+", false);
									    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Created message successfully", "Response"=>$message);
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
	    	//Log the file information
	    	Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-conversation-msg-api-error.txt", serialize($message), "w+", false);
    		    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to update the participant", "Response"=>serialize($e));
    	}
    	 
    }
    
    /**
     * @method		createProxyService
     */
    public function createProxyService($OrgID) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    		$proxy_unique_id	=	"+1".rand(1111111111,9999999999);
    		$service			=	$client->proxy->v1->services->create($proxy_unique_id);
    		
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new-proxy-service.txt", serialize($service), "w+", false);
    		
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created proxy service", "ProxyID"=>$proxy_unique_id, "Response"=>serialize($service));
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new-proxy-service-error.txt", serialize($e), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create proxy service", "Response"=>serialize($e));
    	}
    
    }
    
    /**
     * @method		createProxySession
     */
    public function createProxySession($OrgID, $proxy_sid, $proxy_unique_id) {
    
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    		$proxy_session_id	=	"SESS".$proxy_unique_id;
			$session			=	$client->proxy->v1->services($proxy_sid)->sessions->create(array("uniqueName" => $proxy_session_id));
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new-proxy-session.txt", serialize($session), "w+", false);
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created proxy session", "ProxySessionID"=>$proxy_session_id, "Response"=>serialize($session));
    	}
		catch (RestException | TwilioException | Exception $e) {
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new-proxy-session-error.txt", serialize($e), "w+", false);
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create proxy session", "Response"=>serialize($e));
    	}
    
    }
    
    /**
     * @method		addTwilioPhoneNumberToService
     * @return		array
     */
    public function addTwilioPhoneNumberToService($OrgID, $proxy_sid, $phone_num_sid) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
			$info	=	$client->proxy->v1->services($proxy_sid)
		                                  ->phoneNumbers
		                                  ->create(array(
		                                               "sid" => $phone_num_sid
		                                           )
		                                  );
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-add-twilio-phone-to-service.txt", serialize($info), "w+", false);
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created proxy session", "Response"=>serialize($info));
    	}
		catch (RestException | TwilioException | Exception $e) {
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-add-twilio-phone-to-service-error.txt", serialize($e), "w+", false);
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create proxy session", "Response"=>serialize($e));
    	}
    	
    }

    /**
     * @method		createProxyParticipant
     */
    public function createProxyParticipant($OrgID, $proxy_sid, $proxy_session_sid, $participant_phone_number) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
			$participant = $client->proxy->v1->services($proxy_sid)
			                                 ->sessions($proxy_session_sid)
			                                 ->participants
			                                 ->create($participant_phone_number, // identifier
			                                          array("friendlyName" => "Twilio Number From")
			                                 	);
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new-proxy-participant.txt", serialize($participant), "w+", false);
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created proxy participant", "Response"=>serialize($participant));
    	}
		catch (RestException | TwilioException | Exception $e) {
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new-proxy-participant-error.txt", serialize($e), "w+", false);
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create proxy participant", "Response"=>serialize($e));
    	}
    
    }
    
    /**
     * @method		sentMessageToParticipant
     */
    public function sentMessageToParticipant($OrgID, $proxy_sid, $proxy_session_sid, $proxy_participant_sid, $message) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
			$message_interaction = $client->proxy->v1->services($proxy_sid)
			                                         ->sessions($proxy_session_sid)
			                                         ->participants($proxy_participant_sid)
			                                         ->messageInteractions
			                                         ->create(array(
			                                                      "body" => $message
			                                                  )
			                                         );
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-msg-proxy-participant.txt", serialize($message_interaction), "w+", false);
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created proxy participant", "Response"=>serialize($message_interaction));
    	}
		catch (RestException | TwilioException | Exception $e) {
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-msg-proxy-participant-error.txt", serialize($e), "w+", false);
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create proxy participant", "Response"=>serialize($e));
    	}
    	 
    }
    
} // end Class
