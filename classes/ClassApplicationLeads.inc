<?php
/**
 * @class		ApplicationLeads
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class ApplicationLeads {
	
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    
    /**
     * @method		insApplicationLead
     * @param 		string $OrgID
     * @param		string $LeadID
     * @param		string $LeadName
     * @param		string $FormID
     * @return		array
     */
    function insUpdApplicationLead($OrgID, $LeadName, $FormID) {

        $LeadID         =   strtolower(str_replace(".", "", microtime(true).uniqid($OrgID, true)));
        $params_info    =   array(":OrgID"=>$OrgID, ":LeadName"=>$LeadName, ":ULeadName"=>$LeadName, ":LeadID"=>$LeadID, ":FormID"=>$FormID);
        $ins_job_app    =   'INSERT INTO ApplicationLeads(OrgID, FormID, LeadID, LeadName) VALUES(:OrgID, :FormID, :LeadID, :LeadName) ON DUPLICATE KEY UPDATE LeadName = :ULeadName';
        $res_job_app    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_job_app, array($params_info) );
    
        return $res_job_app;
    }
    
    
    /**
     * @method		delApplicationLead
     * @param 		string $OrgID
     * @param		string $LeadID
     * @param		string $FormID
     * @return		array
     */
    function delApplicationLead($OrgID, $FormID) {
    
        $params_job_leads   =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
        $del_job_app_leads  =   "DELETE FROM ApplicationLeads WHERE OrgID = :OrgID AND FormID = :FormID";
        $res_job_app_leads  =   $this->db->getConnection ( $this->conn_string )->delete ( $del_job_app_leads, array($params_job_leads) );
    
        return $res_job_app_leads;
    }
    
    
    /**
     * @method		getApplicationLeadsList
     * @param 		string $OrgID
     * @return		array
     */
    function getApplicationLeadsList($OrgID) {
    
        $params_job_leads   =   array(":OrgID"=>$OrgID);
        $sel_job_app_leads  =   "SELECT * FROM ApplicationLeads WHERE OrgID = :OrgID";
        $res_job_app_leads  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_job_app_leads, array($params_job_leads) );
        $job_app_leads      =   $res_job_app_leads['results'];

        return $job_app_leads;
    }
    
    
    /**
     * @method		getLeadIDFromFormID
     * @param 		string $OrgID
     * @return		array
     */
    function getLeadIDFromFormID($OrgID, $FormID) {
    
        $params_job_leads   =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
        $sel_job_app_leads  =   "SELECT * FROM ApplicationLeads WHERE OrgID = :OrgID AND FormID = :FormID";
        $res_job_app_leads  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_job_app_leads, array($params_job_leads) );
    
        $LeadID             =   is_null($res_job_app_leads['LeadID']) ? "" : $res_job_app_leads['LeadID'];

        return $LeadID;
    }
    
    
    /**
     * @method		getFormIDFromLeadID
     * @param 		string $OrgID
     * @return		array
     */
    function getFormIDFromLeadID($OrgID, $LeadID) {
    
        $params_job_leads   =   array(":OrgID"=>$OrgID, ":LeadID"=>$LeadID);
        $sel_job_app_leads  =   "SELECT * FROM ApplicationLeads WHERE OrgID = :OrgID AND LeadID = :LeadID";
        $res_job_app_leads  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_job_app_leads, array($params_job_leads) );
    
        $FormID             =   is_null($res_job_app_leads['FormID']) ? "" : $res_job_app_leads['FormID'];
        
        return $FormID;
    }
    
    
    /**
     * @method		getLeadName
     * @param 		string $OrgID
     * @return		array
     */
    function getLeadName($OrgID, $LeadID) {
    
        $params_job_lead    =   array(":OrgID"=>$OrgID, ":LeadID"=>$LeadID);
        $sel_job_app_lead   =   "SELECT LeadName FROM ApplicationLeads WHERE OrgID = :OrgID AND LeadID = :LeadID";
        $res_job_app_lead   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_job_app_lead, array($params_job_lead) );
    
        $LeadName           =   is_null($res_job_app_lead['LeadName']) ? "" : $res_job_app_lead['LeadName'];
        return $LeadName;
    }
    
}
?>
