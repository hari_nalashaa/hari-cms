<?php
/**
 * @class		TwilioConversationMediaApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioConversationMediaApi {
    public $db;
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	var $twilio_number			=	"+16572208043";	//+16572208043
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

	/**
	 * @method		getConversationMediaResource
	 */
    public function getConversationMediaResource($OrgID, $media_sid) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$url	=	"https://mcs.us1.twilio.com/v1/Services/".$account_info["ChatServiceID"]."/Media/".$media_sid;
    	// Get cURL resource
        $curl   =   curl_init();
    	// Set some options - we are passing in a useragent too here
    	curl_setopt_array($curl, [
	    	CURLOPT_RETURNTRANSFER => 1,
	    	CURLOPT_URL => $url,
	    	CURLOPT_HTTPHEADER => array(
		    	"Content-Type: application/x-www-form-urlencoded",
		    	"Authorization: Basic ".base64_encode($account_info['AccountSid'].":".$account_info['AuthToken'])
	    	)
    	]);
    	// Send the request & save response to $resp
    	$resp = curl_exec($curl);
    	// Close request to clear up some resources
    	curl_close($curl);
    	
    	return $resp;
    }
    
    /**
     * @method		insConversationMediaResource
     */
    public function insConversationMediaResource($info, $skip) {
        
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("TwilioMediaInfo", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
        
        return $res_org;
    }
}
