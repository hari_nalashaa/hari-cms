<?php
/**
 * @class		QuestionPreFilledInfo
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class QuestionPreFilledInfo {

    /**
     * @tutorial	Set the default database connection as irecruit
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    /**
     * @method      getDays
     */
    public function getDays() {

        $days_list  =   array(
                            "SUN"   =>  "Sunday", 
                            "MON"   =>  "Monday", 
                            "TUE"   =>  "Tuesday", 
                            "WED"   =>  "Wednesday", 
                            "THU"   =>  "Thursday", 
                            "FRI"   =>  "Friday", 
                            "SAT"   =>  "Saturday"
                        );
        
        return $days_list;
    }
    
    /**
     * @method      getMonths
     */
    public function getMonths() {

        $months_list    =   array(
                                "JAN"   =>  "January",
                                "FEB"   =>  "February",
                                "MAR"   =>  "March",
                                "APR"   =>  "April",
                                "MAY"   =>  "May",
                                "JUN"   =>  "June",
                                "JUL"   =>  "July",
                                "AUG"   =>  "August",
                                "SEP"   =>  "September",
                                "OCT"   =>  "October",
                                "NOV"   =>  "November",
                                "DEC"   =>  "December"
                            );
        
        return $months_list;
    }
    
    /**
     * @method      getSocialMedia
     */
    public function getSocialMedia() {
        
        $social_media   =   array(
                                "Facebook"  => "Facebook",
                                "Twitter"   => "Twitter",
                                "Linkedin"  => "Linkedin",
                                "Google+"   => "Google+",
                                "YouTube"   => "YouTube",
                                "Pintrest"  => "Pintrest",
                                "Instagram" => "Instagram",
                                "Digg"	    => "Digg"
                            );
        
        return $social_media;
        
    }
}    
