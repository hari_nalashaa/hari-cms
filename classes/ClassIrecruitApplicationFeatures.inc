<?php
/**
 * @class		IrecruitApplicationFeatures
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IrecruitApplicationFeatures {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getApplicationFunctionsInfo
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 */
	function getApplicationFunctionsInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_app_func_info = "SELECT $columns FROM ApplicationFunctions";

		if (count ( $where_info ) > 0) $sel_app_func_info .= " WHERE " . implode ( " AND ", $where_info );
	
		if ($order_by != "") $sel_app_func_info .= " ORDER BY " . $order_by;
	
		$res_app_func_info = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_app_func_info, $info );
	
		return $res_app_func_info;
	}
	
	/**
	 * @method 		getApplicationPermissions
	 * @return 		Associative Array
	 */
	function getApplicationPermissions($order_by = '') {
		
		if($order_by == '') $order_by = 'Active';
		
		$sel_application_permissions = "SELECT * FROM ApplicationPermissions";
		$sel_application_permissions .= " ORDER BY ".$order_by;
		$sel_application_permissions .= " LIMIT 0,1";
		$res_application_permissions = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_application_permissions );
		
		return $res_application_permissions;
	}
	
	
	/**
	 * @method		insApplicationPermissions
	 * @param		array $info
	 * @return		array
	 */
	function insApplicationPermissions($info) {
		
		$ins_options = $this->db->buildInsertStatement('ApplicationPermissions', $info);
		$ins_app_permissions = $ins_options['stmt'] . " ON DUPLICATE KEY UPDATE Active = '1'";
		$res_app_permissions = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_app_permissions, $ins_options["info"] );
	
		return $res_app_permissions;
	}
	
	/**
	 * @method		updApplicationPermissionsByUserID
	 */
	function updApplicationPermissionsByUserID($set_info, $where_info, $info) {
		
		$upd_app_perm_info = $this->db->buildUpdateStatement('ApplicationPermissions', $set_info, $where_info);
		$res_app_perm_info = $this->db->getConnection("IRECRUIT")->update($upd_app_perm_info, $info);
		
		return $res_app_perm_info;
	}
	
	/**
	 * @method		updApplicationPermissions
	 */
	function updApplicationPermissions($UserID, $Role) {
		
		$params = array(":AP1UserID"=>$UserID, ":AP2UserID"=>$Role);
		
		$upd_app_permissions = "UPDATE ApplicationPermissions AS AP1
								JOIN ApplicationPermissions AS AP2 ON AP1.UserID = :AP1UserID AND AP2.UserID = :AP2UserID
								SET AP1.Active = AP2.Active,
								AP1.Admin = AP2.Admin,
								AP1.Requisitions = AP2.Requisitions,
								AP1.Requisitions_Edit = AP2.Requisitions_Edit,
							 	AP1.Requisitions_Costs = AP2.Requisitions_Costs,
								AP1.Forms = AP2.Forms,
								AP1.Forms_Edit = AP2.Forms_Edit,
								AP1.Internal_Forms = AP2.Internal_Forms,
								AP1.Internal_Forms_Edit = AP2.Internal_Forms_Edit,
								AP1.Correspondence = AP2.Correspondence,
								AP1.Reports = AP2.Reports,
								AP1.Exporter = AP2.Exporter,
								AP1.Applicants = AP2.Applicants,
								AP1.Applicants_Contact = AP2.Applicants_Contact,
								AP1.Applicants_Forward = AP2.Applicants_Forward,
								AP1.Applicants_Edit = AP2.Applicants_Edit,
								AP1.Applicants_Delete = AP2.Applicants_Delete,
								AP1.Applicants_Assign = AP2.Applicants_Assign,
								AP1.Applicants_Onboard = AP2.Applicants_Onboard,
								AP1.Applicants_Update_Status = AP2.Applicants_Update_Status,
								AP1.Applicants_Update_Final_Status = AP2.Applicants_Update_Final_Status,
								AP1.Applicants_Affirmative_Action = AP2.Applicants_Affirmative_Action,
								AP1.Interview_Admin = AP2.Interview_Admin";
		$res_app_permissions = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_app_permissions, array($params) );
		
		return $res_app_permissions;
	}
	
	/**
	 * @method getApplicationFeatures
	 * @return Associative Array
	 */
	function getApplicationFeatures() {
		
		$sel_application_features = "SELECT * FROM ApplicationFeatures LIMIT 0,1";
		$res_application_features = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_application_features );
		
		return $res_application_features;
	}
	
	/**
	 * @method getApplicationFeaturesByOrgID
	 * @return Associative Array
	 */
	function getApplicationFeaturesByOrgID($OrgID) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID);
		
		$sel_application_features = "SELECT * FROM ApplicationFeatures WHERE OrgID = :OrgID";
		$res_application_features = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_application_features, array($params) );
		
		return $res_application_features;
	}
	
	/**
	 * @method getApplicationPermissionsByUserID
	 * @return Associative Array
	 */
	function getApplicationPermissionsByUserID($columns, $UserID, $fetch_type = 'fetchAssoc') {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		//Set parameters for prepared query
		$params = array(":UserID"=>$UserID);
		
		$sel_application_permissions = "SELECT $columns FROM ApplicationPermissions WHERE UserID = :UserID";
		
		if($fetch_type == 'fetchAssoc')
			$res_application_permissions = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_application_permissions, array($params) );
		else
			$res_application_permissions = $this->db->getConnection ( "IRECRUIT" )->fetchRow ( $sel_application_permissions, array($params) );
		
		return $res_application_permissions;
	}
	
	/**
	 * @method getApplicationPermissionsInfo
	 * @return Associative Array
	 */
	function getApplicationPermissionsInfo($table_name, $columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_app_permissions_info = "SELECT $columns FROM $table_name";

		if(count($where_info) > 0) $sel_app_permissions_info .= " WHERE " . implode(" AND ", $where_info);
		if($order_by != "") $sel_app_permissions_info .= " ORDER BY " . $order_by;
		
		$res_user_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_app_permissions_info, $info );
	
		return $res_user_info;
	}
	
	/**
	 * @method	delApplicationPermissions
	 * @param	$where_info = array(), $info = array()
	 */
	function delApplicationPermissions($where_info = array(), $info = array()) {
		
		$del_app_permissions = "DELETE FROM ApplicationPermissions";
		if (count ( $where_info ) > 0) $del_app_permissions .= " WHERE " . implode ( " AND ", $where_info );

		$res_app_permissions = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_app_permissions, $info );
	
		return $res_app_permissions;
	}
	
	/**
	 * @method	getIrecruitUserAuthenticationInfo
	 * @return	Array
	 */
	function getIrecruitUserAuthenticationInfo() {
		global $IrecruitUsersObj, $IRECRUIT_USER_AUTH;
		
		if (isset ( $_GET ['k'] ) != "") {

			$AccessCodeInfo  =   $IrecruitUsersObj->getUserInfoByAccessCode ( $_GET ['k'], "UserID");
			$USERID          =   $AccessCodeInfo["UserID"];

		} else if (isset ( $_GET ['User'] ) != "") {
			$USERID = $_GET ['User'];
		} else if ($IRECRUIT_USER_AUTH == TRUE 
		          && (! preg_match ( '/dm.php$/', $_SERVER ["SCRIPT_NAME"] )) 
		          && (! preg_match ( '/mark.php$/', $_SERVER ["SCRIPT_NAME"] )) 
		          && (! preg_match ( '/populate.php$/', $_SERVER ["SCRIPT_NAME"] )) 
		          && (! preg_match ( '/webapp.php$/', $_SERVER ["SCRIPT_NAME"] ))) {
			include IRECRUIT_DIR . 'Authenticate.inc';
		}
		
		$irecruit_auth_info ['USERID'] = $USERID;
		
		$user_info_fields =   array ('OrgID', 'AccessCode');
		$user_info        =   $IrecruitUsersObj->getUserInfoByUserID ( $USERID, $user_info_fields );
		
		$irecruit_auth_info ['OrgID']         =   $OrgID      =   $user_info ['OrgID'];
		$irecruit_auth_info ['AccessCode']    =   $AccessCode =   $user_info ['AccessCode'];
		
		return $irecruit_auth_info;
	}
	
	/**
	 * @method		getIrecruitApplicationConfigSettings
	 * @param
	 * @return 		array
	 */
	function getIrecruitApplicationConfigSettings() {
		
		global $IrecruitUsersObj, $USERID, $OrgID, $AccessCode, $PAGE_TYPE, $OrganizationsObj;
		
		// build permission array from the colums, skip the PK
		$AP = $this->getApplicationPermissions ();
		
		$api = 0;
		foreach ( $AP as $key => $value ) {
			if ($api > 0) {
				$permissionList [$api] = $key;
			}
			$api ++;
		}
		
		$irecruit_settings ['permissionList'] = $permissionList;
		
		// build application features array
		$AF = $this->getApplicationFeatures ();
		
		$afi = 0;
		foreach ( $AF as $key => $value ) {
			if ($afi > 0) {
				$featureList [$afi] = $key;
			}
			$afi ++;
		}
		
		if ($USERID) {
		
			// get results by name not by array index
			$row = $IrecruitUsersObj->getUserInfoByUserID ( $USERID, 'Role' );
			
			// if they have a predefined role then use role's permissions
			if ($row ['Role'] != "") {
				$permId = $row ['Role'];
				$USERROLE = $row ['Role'];
				$irecruit_settings['USERROLE'] = $USERROLE;
			} else {
				$permId = $USERID;
				$USERROLE = '';
				$irecruit_settings['USERROLE'] = $USERROLE;
			}
		
			// set permissions //Set where condition
			$where = array("UserID = :UserID", "Active = 1");
			//Set parameters
			$params = array(":UserID"=>$permId);
			//Get Application Permissions based on userid
			$results_app_permissions = $this->getApplicationPermissionsInfo('ApplicationPermissions', "*", $where, "", array($params));
			$row = $results_app_permissions['results'][0];
			
			foreach ( $permissionList as $s ) {
				$permit [$s] = $row [$s];
			}
		} // end USERID
		
		
		//
		$permit ['RequisitionApproval'] = 1;
		
		if (($OrgID == "I20120605") && ($USERROLE == "master_hiring_manager")) {
			if ($_SERVER ['HTTP_HOST'] == 'dev.irecruit-us.com') {
			} else {
				$permit ['RequisitionApproval'] = 0;
			}
		}
		if (($OrgID == "I20120901") && ($USERROLE == "master_hiring_manager")) {
			$permit ['RequisitionApproval'] = 0;
		}
		
		$irecruit_settings['PublicLink'] = PUBLIC_HOME . 'index.php?OrgID=' . $OrgID;
		
		$irecruit_settings['PortalLink'] = USERPORTAL_HOME . 'index.php?OrgID=' . $OrgID;
		
		// set functionality for add on modules
		$row = $this->getApplicationFeaturesByOrgID ( $OrgID );
		
		foreach ( $featureList as $s ) {
			$feature [$s] = $row [$s];
		}
		$irecruit_settings ['feature'] = $feature;
		
		if (($OrgID == "I20140912") && (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager')) { // Community Research Foundation
			$feature ['PreFilledForms'] = "N";
		}
		
		if ($feature ['RequisitionApproval'] == "Y") {
			$feature ['JobBoardRefresh'] = "N";
		}
		
		// turn on feature for particular user
		if ($USERID == "dave") { }
		
        $irecruit_settings ['permit']       =   $permit;
        $irecruit_settings ['featureList']  =   $featureList;
        $InternalCode                       =   "internal";
		
		if ($feature ['InternalRequisitions'] == "Y") {
            //Get InternalCode
            $InternalCode   =   G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID($OrgID);
		}
		
		$irecruit_settings ['InternalCode'] = $InternalCode;
		
		if($PAGE_TYPE == "PopUp" || $PAGE_TYPE == "Email" || $PAGE_TYPE == "Default") {
			//Set stylesheets with postion either header or footer
			$irecruit_settings["default_styles"]['header'][]	=	"css/bootstrap.min.css";
			$irecruit_settings["default_styles"]["header"][] 	= 	"css/jquery-ui.css";
			$irecruit_settings["default_styles"]["header"][] 	= 	"css/sb-admin-2.css";
			
			//Set javascripts with postion either header or footer
			$irecruit_settings["default_scripts"]['footer'][]	=	"js/bootstrap.min.js";
		}
		else {
            
		    //Load style sheets based on the selected template
	        //Load styles for responsive theme
	        $irecruit_settings["default_styles"]["header"][] = 'css/bootstrap.min.css';
	        $irecruit_settings["default_styles"]["header"][] = 'css/plugins/metisMenu/metisMenu.min.css';
	        $irecruit_settings["default_styles"]["header"][] = 'css/plugins/dataTables.bootstrap.css';
	        //$irecruit_settings["default_styles"]["header"][] = 'css/plugins/dataTables.responsive.css';
	        $irecruit_settings["default_styles"]["header"][] = 'css/plugins/timeline.css';
	        $irecruit_settings["default_styles"]["header"][] = 'css/plugins/morris.css';
	        // this one
	        $irecruit_settings["default_styles"]["header"][] = 'css/sb-admin-2.css';
	        // this one
	        
	        $irecruit_settings["default_styles"]["header"][] = 'css/print.css';
	        $irecruit_settings["default_styles"]["header_attributes"]["css/print.css"] = array("media"=>"print");
	        
	        $irecruit_settings["default_styles"]["header"][] = 'css/custom-styles.css';
	        $irecruit_settings["default_styles"]["header"][] = 'css/customstyles.css';
	        $irecruit_settings["default_styles"]["header"][] = 'css/jquery-ui.css';
	        $irecruit_settings["default_styles"]["header"][] = 'font-awesome-4.1.0/css/font-awesome.min.css';
	        
	        $irecruit_settings["default_scripts"]["footer"][] = "js/bootstrap.min.js";
	        $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/metisMenu/metisMenu.min.js";
	        $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/flot/excanvas.min.js";
	        
	        
	        if (preg_match('/index.php$/',$_SERVER["SCRIPT_NAME"])) {
	            $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/flot/jquery.flot.js";
	            $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/flot/jquery.flot.time.js";
	            $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/flot/jquery.flot.pie.js";
	            $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/flot/jquery.flot.resize.js";
	            $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/flot/jquery.flot.tooltip.min.js";
	        }
	        
	        $irecruit_settings["default_scripts"]["footer"][] = "js/common-responsive.min.js";
	        $irecruit_settings["default_scripts"]["footer"][] = "js/appointmentscheduling.min.js";
	        
	        if (preg_match('/reports\/requisitions.php$/',$_SERVER["SCRIPT_NAME"])
	            || preg_match('/reports\/requisitionsByManagerOrOwner.php$/',$_SERVER["SCRIPT_NAME"])
	            || preg_match('/reports\/applicantHistory.php$/',$_SERVER["SCRIPT_NAME"])
	            || preg_match('/reports\/getApplicantsStatusByRequisition.php$/',$_SERVER["SCRIPT_NAME"])
	            || preg_match('/reports\/getApplicantsByReferralSource.php$/',$_SERVER["SCRIPT_NAME"])
	            ) {
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/jquery.dataTables.1.10.15.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/dataTables.bootstrap.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/dataTables.1.3.1.buttons.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/buttons.colVis.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/buttons.flash.1.3.1.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/jszip.3.1.3.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/pdfmake.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/vfs_fonts.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/buttons.html5.1.3.1.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/buttons.print.1.3.1.min.js";
            }
            else {
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/jquery.dataTables.1.10.15.min.js";
                $irecruit_settings["default_scripts"]["footer"][] = "js/plugins/dataTables/dataTables.bootstrap.js";
            }
            
            $irecruit_settings["default_scripts"]["footer"][] = "js/sb-admin-2.min.js";                

		}
		
		//Get userpreferences information
		$preferences = $irecruit_settings['preferences'] = G::Obj('IrecruitUserPreferences')->getUserPreferencesByUserID($USERID);
		
		$irecruit_settings['piechartids'] = array (
												"1" => "applications-by-distinction",
												"2" => "applications-searchable",
												"3" => "applications-by-processorder",
												"4" => "applications-by-dispositioncode"
											);

		$irecruit_settings["BOOTSTRAP_SKIN"] =	true;
		
		//Userinformation
		$irecruit_settings['user_info'] = G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "*");
		
		//Get usertheme info for calendar
		$irecruit_settings['user_preferences'] = G::Obj('IrecruitUserPreferences')->getUserTheme();
		
		if($preferences ['DefaultCalendarView'] != "Table" && $preferences ['DefaultCalendarView'] != "List") {
			$preferences = G::Obj('IrecruitUserPreferences')->getUserPreferencesByUserID('MASTER');
		}
			
		if ($preferences ['DefaultCalendarView'] == "Table")
			$irecruit_settings['calenderview'] = 'calendartableview';
		if ($preferences ['DefaultCalendarView'] == "List")
			$irecruit_settings['calenderview'] = 'calendarlistview';
			
		return $irecruit_settings;
	}
	
	/**
	 * @method	getIrecruitDefaultSettings
	 * @param	
	 */
	function getIrecruitDefaultSettings($OrgID) {
		// build permission array from the colums, skip the PK
		$AP = $this->getApplicationPermissions ();
		
		$api = 0;
		foreach ( $AP as $key => $value ) {
			if ($api > 0) {
				$permissionList [$api] = $key;
			}
			$api ++;
		}
		
		$irecruit_settings ['permissionList'] = $permissionList;
		
		// build application features array
		$AF = $this->getApplicationFeatures ();
		
		$afi = 0;
		foreach ( $AF as $key => $value ) {
			if ($afi > 0) {
				$featureList [$afi] = $key;
			}
			$afi ++;
		}
		
		// set functionality for add on modules
		$row  =   $this->getApplicationFeaturesByOrgID ( $OrgID );
		
		foreach ( $featureList as $s ) {
			$feature [$s] = $row [$s];
		}
		$irecruit_settings ['feature'] = $feature;
		
		//Set stylesheets with postion either header or footer
		$irecruit_settings["default_styles"]['header'][]	=	"css/bootstrap.min.css";
		
		//Set javascripts with postion either header or footer
		$irecruit_settings["default_scripts"]['footer'][]	=	"js/bootstrap.min.js";
		
		return $irecruit_settings;
	}
	
	
	function insDocumentsZipDownloadOptions($info) {
		
        $ins_options    =   $this->db->buildInsertStatement('DocumentsZipDownloadOptions', $info);
        $ins_stmt       =   $ins_options['stmt'] . " ON DUPLICATE KEY UPDATE DocumentsList = :UDocumentsList";
		//Set update parameter
		$ins_options['info'][0][':UDocumentsList'] = $ins_options['info'][0][':DocumentsList'];
		
		$res_options = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_stmt, $ins_options["info"] );
		
		return $res_options;
	}
	
	function getDocumentsZipDownloadOptions($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_options = "SELECT $columns FROM DocumentsZipDownloadOptions";
		
		if (count ( $where_info ) > 0) $sel_options .= " WHERE " . implode ( " AND ", $where_info );
		if ($order_by != "") $sel_options .= " ORDER BY " . $order_by;
		
		$res_options = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_options, $info );
		
		return $res_options;
	}
}
