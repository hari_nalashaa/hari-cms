<?php
/**
 * @class		ValidateFormsCaptcha
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ValidateFormsCaptcha {
	
    /**
	 * @tutorial	Set the default database connection as irecruit
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
    
    /**
     * @method  validateUserPortalAgreementFormCaptcha
     */
    function validateUserPortalAgreementFormCaptcha($insert_info, $on_update, $update_info) {
        
        if (isset ( $_POST ["captcha"] )) {
            if ($_POST ["captcha"] == "") {
                $ERROR = " - Please enter the 6 symbols that represents your signature." . "\\n";
        
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            } else if ($_SESSION["U"]["captcha"] != $_POST ["captcha"]) {
                $ERROR = " - The 6 symbols entered could not be verified." . "\\n";
        
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            }
        } // end captcha session
        
        return $ERROR;
    }
    
    /**
     * @method  validateUserPortalPreFilledFormCaptcha
     */
    function validateUserPortalPreFilledFormCaptcha($insert_info, $on_update, $update_info) {
        
        if (isset ( $_POST ["captcha"] )) {
            if ($_POST ["captcha"] == "") {

                $ERROR = " - Please enter the 6 symbols that represents your signature." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID']  =   'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
                
            } else if ($_SESSION["U"]["captcha"] != $_POST ["captcha"]) {
                
                $ERROR = " - The 6 symbols entered could not be verified." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID']  =   'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
                
            }
        } // end captcha session       
        
        return $ERROR;
    }
    
    /**
     * @method validateUserPortalWebFormCaptcha
     */
    function validateUserPortalWebFormCaptcha($insert_info, $on_update, $update_info) {
        
        if (isset ( $_POST ["captcha"] )) {
            if ($_POST ["captcha"] == "") {
                $ERROR = " - Please enter the 6 symbols that represents your signature." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            } else if ($_SESSION["U"]["captcha"] != $_POST ["captcha"]) {
                $ERROR = " - The 6 symbols entered could not be verified." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            }
        } // end captcha session
        
        return $ERROR;
    }    
    
    /**
     * @method  validateIrecruitAgreementFormCaptcha
     */
    function validateIrecruitAgreementFormCaptcha($insert_info, $on_update, $update_info) {
    
        if (isset ( $_POST ["captcha"] )) {
            if ($_POST ["captcha"] == "") {
                $ERROR = " - Please enter the 6 symbols that represents your signature." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            } else if ($_SESSION["I"]["captcha"] != $_POST ["captcha"]) {
                $ERROR = " - The 6 symbols entered could not be verified." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            }
        } // end captcha session
    
        return $ERROR;    
    }
    
    /**
     * @method  validateIrecruitPreFilledFormCaptcha
     */
    function validateIrecruitPreFilledFormCaptcha($insert_info, $on_update, $update_info) {
    
        if (isset ( $_POST ["captcha"] )) {
            if ($_POST ["captcha"] == "") {
                $ERROR = " - Please enter the 6 symbols that represents your signature." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            } else if ($_SESSION["I"]["captcha"] != $_POST ["captcha"]) {
                $ERROR = " - The 6 symbols entered could not be verified." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            }
        } // end captcha session
    
        return $ERROR;
    }
    
    /**
     * @method validateIrecruitWebFormCaptcha
     */
    function validateIrecruitWebFormCaptcha($insert_info, $on_update, $update_info) {
    
        if (isset ( $_POST ["captcha"] )) {
            if ($_POST ["captcha"] == "") {
                $ERROR = " - Please enter the 6 symbols that represents your signature." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            } else if ($_SESSION["I"]["captcha"] != $_POST ["captcha"]) {
                $ERROR = " - The 6 symbols entered could not be verified." . "\\n";
                //Set QuestionID to insert
                $insert_info['QuestionID'] = 'captcha';
                //Insert Update Applicant Data Temp
                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
            }
        } // end captcha session
    
        return $ERROR;
    }    
}
?>