<?php
/**
 * @class		CheckList
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IconnectPackagesHeader{

    /**
     * @tutorial	Set the default database connection as irecruit
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }

    /**
     * @method		getCheckList
     * @param		$ChecklistID, $columns(optional)
     * @return		associative array
     */
    function geticonnectPackagesHeader($OrgID,$IconnectPackagesHeaderID) {
    
        //Set parameters for prepared query
        $params             =   array(":IconnectPackagesHeaderID"=>$IconnectPackagesHeaderID, ":OrgID"=>$OrgID,":FormStatus"=>'Active');    
        $sel_checklist_info =   "SELECT * FROM IconnectPackagesHeader  WHERE IconnectPackagesHeaderID = :IconnectPackagesHeaderID AND OrgID = :OrgID AND FormStatus=:FormStatus";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
       
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListByOrgID
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function geticonnectPackagesHeaderByOrgID($OrgID, $Active) { 
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID,":FormStatus"=>$Active);
        $sel_checklist_info =   "SELECT * FROM IconnectPackagesHeader WHERE OrgID = :OrgID AND FormStatus=:FormStatus";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_checklist_info, array($params) );
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListProcess
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function geticonnectPackagesHeaderProcess($OrgID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID);
        $sel_checklist_info =   "SELECT * FROM ChecklistConfiguration WHERE OrgID = :OrgID";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
	
    /**
     * @method		getCheckListCategoryByOrgID
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function getCheckListCategoryByOrgID($OrgID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID);
        $sel_checklist_info =   "SELECT * FROM ChecklistSetup WHERE OrgID = :OrgID AND Active='Y'";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }


/**
     * @method		getCheckListCategoryByOrgID
     * @param		$OrgID, $columns(optional)
     * @return		associative array
     */
    function getIconnectCategoryByPackageName($OrgID,$PackageName) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID,":PackageName"=>$PackageName);
        $sel_checklist_info =   "SELECT * FROM IconnectPackagesHeader WHERE OrgID = :OrgID AND PackageName= :PackageName AND FormStatus='Active'";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }

    
    /**
     * @method		insChecklist
     * @param		$ChecklistID, $StatusCategory, $Checklist, $Version
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insiconnectPackagesHeader($OrgID,$packageName) { 
    
        $params_info        =   array(":OrgID"=>$OrgID, ":PackageName"=>$packageName);
        $ins_statement      =   "INSERT INTO IconnectPackagesHeader(OrgID,PackageName, LastUpdated) VALUES(:OrgID,:PackageName, NOW())";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE LastUpdated = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
    
        return $res_statement;
    }
    
    function insiconnectPackagesHeaderName($OrgID, $PackageName) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":PackageName"=>$PackageName);
        $ins_statement      =   "INSERT INTO IconnectPackagesHeader(OrgID, PackageName, LastUpdated) VALUES(:OrgID, :PackageName, NOW())";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE LastUpdated = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    function insiconnectPackagesHeaderProcessOrder($OrgID, $ProcessOrder) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":ProcessOrder"=>$ProcessOrder);
        $ins_statement      =   "INSERT INTO ChecklistConfiguration(OrgID, ProcessOrder) VALUES(:OrgID, :ProcessOrder)";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    function insChecklistDisplayOnUserPortal($OrgID, $DisplayUserPortal) {
        
        $params_info        =   array(":OrgID"=>$OrgID, ":DisplayOnUserPortal"=>$DisplayUserPortal);
        $ins_statement      =   "INSERT INTO ChecklistConfiguration(OrgID, DisplayOnUserPortal) VALUES(:OrgID, :DisplayOnUserPortal)";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    /**
     * @method      updAgreementDataInfo
     * @param       $set_info, $where_info, $info
     */
    function updChecklistProcessOrder($set_info = array(), $where_info = array(), $info) {
        
        $upd_checklist_data_info = $this->db->buildUpdateStatement("ChecklistConfiguration", $set_info, $where_info);
        $res_checklist_data_info = $this->db->getConnection( $this->conn_string )->update($upd_checklist_data_info, $info);
        
        return $res_checklist_data_info;
    }
    
        
          /**
	 * @method	updInternalFormsAssigned
	 * @return	array
	 * @param	$set_info, $where_info, $info
	 */
	function updiconnectPackagesHeader($set_info = array(), $where_info = array(), $info) {  
		 
		$upd_internal_forms_assigned = $this->db->buildUpdateStatement('IconnectPackagesHeader', $set_info, $where_info);
		$res_internal_forms_assigned = $this->db->getConnection("IRECRUIT")->update($upd_internal_forms_assigned, $info );
		
		return $res_internal_forms_assigned;
	}

	
	
          /**
	 * @method     delFormData
	 * @param      $set_info, $where_info, $info
	 * @table	
	 */
	function delIconnectFormData($table_name, $where_info = array(), $info) {
		
		$del_pre_form_data = "DELETE FROM $table_name";
		if(count($where_info) > 0) $del_pre_form_data .= " WHERE " . implode(" AND ", $where_info);

		$res_pre_form_data = $this->db->getConnection("IRECRUIT")->delete($del_pre_form_data, $info);
	
		return $res_pre_form_data;
	}

    
    /**
	 * @method		copyChecklist
	 */
    public function copyiconnectPackagesHeader($OrgID, $ChecklistID, $ChecklistName) {
	    
	    $params = array(":OrgID"=>$OrgID, ":ChecklistID"=>$ChecklistID);
	    $now = date('Y-m-d H:i:s');
	    
        //Insert Application Form Sections Master Sections OrgID
        $ins_checklist_ques 	=	"INSERT INTO ChecklistSetup (OrgID, StatusCategory, Checklist, ChecklistName, Locked, Active, LastUpdated)
						  	SELECT  OrgID, StatusCategory, Checklist, '".$ChecklistName."', 'N',  'P', '".$now."' 
						  	FROM ChecklistSetup WHERE OrgID = :OrgID AND ChecklistID = :ChecklistID";
        $res_form_ques	=	$this->db->getConnection ( "IRECRUIT" )->insert($ins_checklist_ques, array($params));	    
	}
}
?>
