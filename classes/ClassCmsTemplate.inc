<?php
/**
 * @tutorial	Class will control the template structure.
 * 				Here we are loading all the default styles of selected template
 * 
 * @methods		__construct, __get, __set,
 * 				
 * @class		CmsTemplate
 */
class CmsTemplate {
    
	private $vars = array ();
	public $default_view_dir;
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		global $class_files;
		
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
		$this->default_view_dir = IRECRUIT_DIR;

	    // Classes not requied to create an object
		$classes  =   array (
            				"CmsTemplate",
            				"Logger",
            				"Database",
		                    "Session"
                		);
		
		//It will autoload all the classes
		spl_autoload_register(array($this, 'iRecruitAutoloader'));
		
		//Create dynamic objects with the class name
		//eg: $ClassNameObj->getInfo();
		$classes_dir        =   ROOT . 'classes/';
		$scanned_directory  =   array_diff(scandir($classes_dir), array('..', '.'));
		
		foreach($scanned_directory as $class_file_name) {
		    if(!is_dir($class_file_name) && substr($class_file_name, 0, 1) != ".") {
		
		        $first_trim =   substr($class_file_name, 5);
		        $class_name =   substr($first_trim, 0, -4);
		
		        if(!in_array($class_name, $classes)) {
		            $this->{$class_name."Obj"} = new $class_name;
		        }
		    }
		}
		
	}
	
	/**
	 * @method     iRecruitAutoloader
	 * @param      $class_name
	 */
	function iRecruitAutoloader($class_name)
	{
	    $path = ROOT . 'classes/Class';
	
	    if(file_exists($path.$class_name.'.inc') && $class_name != "Session") {
	        require_once $path.$class_name.'.inc';
	    }
	}
	
	/**
	 * @method     _get
	 * @param      string $name
	 * @return     multitype:
	 */
	public function __get($name) {
	    $var_name  = isset($this->vars [$name]) ? $this->vars [$name] : "";
		return $var_name;
	}
	
	/**
	 * @method     __set
	 * @param      string $name
	 * @param      string $value
	 * @throws     Exception
	 */
	public function __set($name, $value) {
		if ($name == 'view_template_file') {
			throw new Exception ( "Cannot bind variable named 'view_template_file'" );
		}
		$this->vars [$name] = $value;
	}
	
	/**
	 * @method 		render
	 * @param 		string $view_template_file        	
	 * @throws 		Exception
	 * @return 		TemplateContent
	 */
	public function render($view_template_file) {
		if (array_key_exists ( 'view_template_file', $this->vars )) {
			throw new Exception ( "Cannot bind variable called 'view_template_file'" );
		}
		extract ( $this->vars );
		ob_start ();
		include ($view_template_file . ".inc");
		return ob_get_clean ();
	}
	
	/**
	 * @method     displayIrecruitTemplate
	 * @param      string $view_template_file
	 * @throws     Exception
	 */
	public function displayIrecruitTemplate($view_template_file) {
		// Irecruit application settings
	    global $whitelist, $irecruit_app_settings, $irecruit_auth_info, $organization_info, $PAGE_TYPE, $IRECRUIT_USER_AUTH;
	    
		//Extract All Variables
		extract ( $this->vars );

		// Load settings for authorized users
		if ($IRECRUIT_USER_AUTH == TRUE) {
			// Extract irecruit app settings
			if (isset ( $irecruit_app_settings ))
				extract ( $irecruit_app_settings );
		}
		
		// Load settings for unauthorized users
		if ($IRECRUIT_USER_AUTH == FALSE) {
			// Extract irecruit app settings
			if (isset ( $irecruit_app_settings ))
				extract ( $irecruit_app_settings );
		}
		
		// Authentication Information
		if (isset ( $irecruit_auth_info ))
			extract ( $irecruit_auth_info );
			
			// Organization information
		if (isset ( $organization_info ) && $organization_info !== FALSE)
			extract ( $organization_info );
		
		ob_start ();
		
		/*
		 * Html Upto Body Tag, It is common for all templates All the css, js files will be loaded from pages
		 */
		include IRECRUIT_DIR . "skins/Header.inc";
		
		// Template based wrapper, based on user login
		if (! isset ( $PAGE_TYPE ) && $PAGE_TYPE != "PopUp") {
		    include IRECRUIT_DIR . "skins/MainWrapperStartResponsive.inc";
		}
		else if ($PAGE_TYPE == "Email") {
		    include IRECRUIT_DIR . "skins/MainWrapperStartEmail.inc";
		}
		else {
		    include IRECRUIT_DIR . "skins/MainWrapperStartDefault.inc";
		}
		
		if (array_key_exists ( 'view_template_file', $this->vars )) {
			throw new Exception ( "Cannot bind variable called 'view_template_file'" );
		}
		
		$template_file = $this->default_view_dir . $view_template_file;
		
		if ($PAGE_TYPE == "Email") {
			if ($CHECK == 0) {
				echo '<table border="0" height="200" width="100%">';
				echo '<tr><td align="center">';
				echo '<p>Please access this page only from an email link.</p>';
				echo '</td></tr>';
				echo '</table>';
			} else {
			    if(in_array($template_file, $whitelist)) {
                    //Don't do until the veracode scan 
			        include $template_file . ".inc";
		        }
			}
		} else {
		    if(in_array($template_file, $whitelist)) {
		        //Don't do until the veracode scan
		        include $template_file . ".inc";
		    }
		}
		
		// Main wrapper end
		if (! isset ( $PAGE_TYPE ) && $PAGE_TYPE != "PopUp")
			include IRECRUIT_DIR . "skins/MainWrapperEndResponsive.inc";
		else if ($PAGE_TYPE == "Email")
			include IRECRUIT_DIR . "skins/MainWrapperEndEmail.inc";
		else
			include IRECRUIT_DIR . "skins/MainWrapperEndDefault.inc";

		/*
		 * Html End From Body Tag, 
		 * It is common for all templates All the js files will be loaded from pages
		 */
		include IRECRUIT_DIR . "skins/Footer.inc";

		return ob_get_clean ();
	}
}
?>
