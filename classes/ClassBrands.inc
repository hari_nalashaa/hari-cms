<?php
/**
 * @class		Brands
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Brands {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getBrands
	 * @return		Array
	 */
	public function getBrands() {
		
		$sel_brands = "SELECT * FROM Brands";
		$res_brands = $this->db->getConnection("IRECRUIT")->fetchAllAssoc($sel_brands);
		
		return $res_brands;
	}
	
	/**
	 * @method		getBrandInfo
	 * @return		Array
	 */
	public function getBrandInfo($BrandID) {
	
	    $params_info   =   array(":BrandID"=>$BrandID);
	    $sel_brands    =   "SELECT * FROM Brands WHERE BrandID = :BrandID";
	    $res_brands    =   $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_brands, array($params_info));
	
	    return $res_brands;
	}
	
	/**
	 * @method		insBrandInfo
	 * @param		array $info
	 * @tutorial	
	 */
	function insBrandInfo($info) {
	
	    $ins_brands_info = $this->db->buildInsertStatement('Brands', $info);
	    $res_brands_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_brands_info["stmt"], $ins_brands_info["info"] );
	    
	    return $res_brands_info;
	}
	
	/**
	 * @method 		delBrandInfo
	 * @param 		string $OrgID
	 * @param 		string $QuestionID
	 */
	function delBrandInfo($BrandID) {
	
	    // Set parameters for prepared query
	    $params = array (":BrandID" => $BrandID);
	
	    $del_brand_info = "DELETE FROM Brands WHERE BrandID = :BrandID";
	    $res_brand_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_brand_info, array ($params) );
	
	    return $res_brand_info;
	}
	
	/**
	 * @method		updBrandLogo
	 * @param		string $BrandID
	 * @param		string $Logo
	 */
	function updBrandLogo($BrandID, $Logo) {
	
	    // Set parameters for prepared query
	    $params = array (":BrandID"     =>  $BrandID, ":Logo" => $Logo);
	
	    $upd_brand_info  = "UPDATE Brands SET Logo = :Logo WHERE BrandID = :BrandID";
	    $res_brand_info  = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_brand_info, array ($params) );
	
	    return $res_brand_info;
	}
	
	/**
	 * @method		updBrandInfo
	 * @param		string $BrandID
	 * @param		string $BrandName
	 * @param		string $FooterText
	 * @param		string $Url
	 * @param		string $Logo
	 */
	function updBrandInfo($BrandID, $BrandName, $FooterText, $Url) {
	
	    // Set parameters for prepared query
	    $params = array (
                	        ":BrandID"     =>  $BrandID,
                	        ":BrandName"   =>  $BrandName,
                	        ":FooterText"  =>  $FooterText,
                	        ":Url"         =>  $Url
                	    );
	
	    $upd_brand_info  = "UPDATE Brands SET BrandName = :BrandName, FooterText = :FooterText, Url = :Url WHERE BrandID = :BrandID";
	    $res_brand_info  = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_brand_info, array ($params) );
	
	    return $res_brand_info;
	}
	
}
