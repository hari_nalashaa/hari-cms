<?php 
/**
 * @class		ApplicantAttachmentsTemp
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantAttachmentsTemp {
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		insApplicantAttachmentsTemp
	 * @param		$attachment_info
	 * @tutorial	buildInsertStatement will generate the insert
	 * 				query based on the key value pair array
	 */
	public function insApplicantAttachmentsTemp($attachment_info) {
	    
	    $ins_app_atchmnt_info  =   $this->db->buildInsertStatement('ApplicantAttachmentsTemp', $attachment_info);
	    $ins_attachment_stmt   =   $ins_app_atchmnt_info["stmt"] . " ON DUPLICATE KEY UPDATE PurposeName = :UPurposeName";
	    $ins_params            =   $ins_app_atchmnt_info["info"];
	    
	    $ins_params[0][':UPurposeName']    =   $ins_params[0][':PurposeName'];
	    
	    if($ins_params[0][':FileType'] != "") {
	        $ins_attachment_stmt          .=   ", FileType = :UFileType";
	        $ins_params[0][':UFileType']   =   $ins_params[0][':FileType'];
	    }
	    
	    $res_app_atchmnt_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_attachment_stmt, $ins_params );
	    
	    return $res_app_atchmnt_info;
	}
	
	
	/**
	 * @method 		getApplicantAttachmentsTempInfo
	 * @param 		string $OrgID
	 * @param 		string $HoldID
	 * @return 		array
	 */
	function getApplicantAttachmentsTempInfo($OrgID, $HoldID) {
	    
	    // Set parameters for prepared query
	    $params = array (":OrgID" => $OrgID, ":HoldID" => $HoldID);
	    $sel_applicant_data = "SELECT * FROM ApplicantAttachmentsTemp WHERE OrgID = :OrgID AND HoldID = :HoldID";
	    $res_applicant_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, array ($params) );
	    
	    return $res_applicant_data;
	}
	
}
?>
