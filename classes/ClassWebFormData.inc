<?php
/**
 * @class		WebFormData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WebFormData {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 *
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}

	
	/**
	 * @method		getWebFormData
	 */
	function getWebFormData($OrgID, $WebFormID, $RequestID, $ApplicationID) {
	
	    $params_info    = array(":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	    $sel_form_data  = "SELECT * FROM WebFormData WHERE OrgID = :OrgID AND WebFormID = :WebFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID";
	    $res_form_data  = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
	
	    return $res_form_data;
	}

	
	/**
	 * @method		getWebFormDataApplicationIDs
	 * @tutorial    It is a temporary function, we will have to remove it after we have done the upgrade
	 */
	function getWebFormDataApplicationIDs($OrgID) {
	
	    $params_info   =   array(":OrgID"=>$OrgID);
	    $sel_form_data =   "SELECT ApplicationID, RequestID FROM WebFormData WHERE OrgID = :OrgID GROUP BY ApplicationID, RequestID";
	    $res_form_data =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form_data, array($params_info) );
	    $form_data_res =   $res_form_data['results'];
	    $form_data_cnt =   $res_form_data['count'];
	    
	    $app_ids       =   array();
	    for($fd = 0; $fd < $form_data_cnt; $fd++) {
	        $app_ids[$fd]['ApplicationID'] =   $form_data_res[$fd]['ApplicationID'];
	        $app_ids[$fd]['RequestID']     =   $form_data_res[$fd]['RequestID'];
	    }
	    
	    return $app_ids;
	}
	
	
	/**
	 * @method		insUpdWebFormData
	 */
	function insUpdWebFormData($QI) {
	
	    $params_info    = array(":OrgID"=>$QI['OrgID'], ":ApplicationID"=>$QI['ApplicationID'], ":RequestID"=>$QI['RequestID'], ":WebFormID"=>$QI['WebFormID'], ":QuestionID"=>$QI['QuestionID'], ":Question"=>$QI['Question'], ":QuestionOrder"=>$QI['QuestionOrder'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":value"=>$QI['value'], ":AnswerStatus"=>1, ":IAnswer"=>$QI['Answer'], ":UQuestionTypeID"=>$QI['QuestionTypeID'], ":UQuestion"=>$QI['Question'], ":UAnswer"=>$QI['Answer'], ":Uvalue"=>$QI['value']);
  		$ins_form_data  = "INSERT INTO WebFormData(OrgID, ApplicationID, RequestID, WebFormID, QuestionID, Question, QuestionOrder, QuestionTypeID, value, AnswerStatus, Answer)";
 		$ins_form_data .= " VALUES(:OrgID, :ApplicationID, :RequestID, :WebFormID, :QuestionID, :Question, :QuestionOrder, :QuestionTypeID, :value, :AnswerStatus, :IAnswer)";
 		$ins_form_data .= " ON DUPLICATE KEY UPDATE Answer = :UAnswer, AnswerStatus = 1, QuestionTypeID = :UQuestionTypeID, value = :Uvalue, Question = :UQuestion";
 		
 		$res_form_data  = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_data, array($params_info) );
 		
 		return $res_form_data;
	}
	
	
	/**
	 * @method     delApplicantWebFormData
	 * @param      string $OrgID
	 * @param      string $WebFormID
	 * @param      string $RequestID
	 * @param      string $ApplicationID
	 * @return     array
	 */
	function delApplicantWebFormData($OrgID, $WebFormID, $RequestID, $ApplicationID) {
	     
	    if($OrgID != "" 
	        && $WebFormID != ""
            && $RequestID != ""
            && $ApplicationID != "") {
	        
	        // Set parameters for prepared query
	        $params_info        =   array (":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	        $del_web_form_data  =   "DELETE FROM WebFormData WHERE OrgID = :OrgID AND WebFormID = :WebFormID AND RequestID = :RequestID AND ApplicationID = :ApplicationID";
	        $res_web_form_data  =   $this->db->getConnection ( $this->conn_string )->delete ( $del_web_form_data, array ($params_info) );

	        return $res_web_form_data;
	    }

	    return "";
	}
}
