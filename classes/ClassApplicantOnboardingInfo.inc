<?php
/**
 * @class		ApplicantOnboardingInfo
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 * 
 * $url =   "https://api.prodatawfs.com/jsonpd/services/JsonService?wsdl";
 * $url =   "https://mypayspace.mypayrollhr.com/json/services/JsonService?wsdl";
 */

class ApplicantOnboardingInfo {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getI9FormData
	 * @tutorial
	 * @table		PreFilledFormData
	 */
	function getI9FormData($OrgID, $ApplicationID, $RequestID) {

		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);
		
		//Set parameters for prepared query
		$params       =   array(
    		                  ":PreFilledFormID"  =>  $CURRENT['I9'],
    		                  ":ApplicationID"    =>  $ApplicationID,
    		                  ":RequestID"        =>  $RequestID,
    		                  ":OrgID"            =>  $OrgID
    		              );
		//Set Prameters
		$sel_i9_data  =   "SELECT * FROM 
                          PreFilledFormData 
                          WHERE PreFilledFormID = :PreFilledFormID
                          AND ApplicationID = :ApplicationID 
                          AND RequestID = :RequestID 
                          AND OrgID = :OrgID";
		$row_i9_data  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_i9_data, array($params));
		
		return $row_i9_data;
	}
	
	/**
	 * @method		getW4FormData
	 * @param		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @return		associative array
	 */
	function getW4FormData($OrgID, $ApplicationID, $RequestID) {

		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentW4 ($OrgID, $RequestID, $ApplicationID);
		
		//Set parameters for prepared query
		$params       =   array(
		                      ":PreFilledFormID"  =>  $CURRENT['W4'],
		                      ":ApplicationID"    =>  $ApplicationID,
		                      ":RequestID"        =>  $RequestID,
		                      ":OrgID"            =>  $OrgID
		                  );		
		//Set Prameters
		$sel_w4_data = "SELECT * FROM 
						PreFilledFormData 
						WHERE PreFilledFormID = :PreFilledFormID
						AND ApplicationID = :ApplicationID 
						AND RequestID = :RequestID 
						AND OrgID = :OrgID";
		$res_w4_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_w4_data, array($params));
	
		return $res_w4_data;
	}

	/**
	 * @method		getI9MFormData
	 * @param		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @return		associative array
	 */
	function getI9MFormData($OrgID, $ApplicationID, $RequestID) {

		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);
		
		//Set parameters for prepared query
		$params       =   array(
    		                  ":PreFilledFormID"  =>  $CURRENT['I9m'],
    		                  ":ApplicationID"    =>  $ApplicationID,
    		                  ":RequestID"        =>  $RequestID,
    		                  ":OrgID"            =>  $OrgID
    		                  );
		//Set Prameters
		$sel_i9m_data =   "SELECT * FROM
						  PreFilledFormData
						  WHERE PreFilledFormID = :PreFilledFormID
						  AND ApplicationID = :ApplicationID 
						  AND RequestID = :RequestID 
						  AND OrgID = :OrgID";
		$row_i9m_data =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_i9m_data, array($params));
		
		return $row_i9m_data;
	}
	
	/**
	 * @method		getEmergencyContactsFormData
	 * @param		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @return		associative array
	 */
	function getEmergencyContactsFormData($OrgID, $ApplicationID, $RequestID) {

        //Set parameters for prepared query
        $params_web_form_id =   array(":OrgID"=>$OrgID);
        //Select contact information
        $sel_web_form_id    =   "SELECT WebFormID FROM WebForms WHERE WebFormID IN (
                                SELECT DISTINCT(WebFormID) FROM WebFormQuestions WHERE OrgID = :OrgID AND FormID = 'emergencycontact') and FormStatus = 'Active' LIMIT 1";
        $res_web_form_id    =   G::Obj('GenericQueries')->getRowInfoByQuery($sel_web_form_id, array($params_web_form_id));
        
        $params             =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID, ":WebFormID"=>$res_web_form_id['WebFormID']);
        $sel_info           =   "SELECT * FROM WebFormData WHERE OrgID = :OrgID AND RequestID = :RequestID AND ApplicationID = :ApplicationID AND WebFormID = :WebFormID ORDER BY QuestionOrder ASC";
        $res_info           =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_info, array($params));
         
        return $res_info;
    }
	
	/**
	 * @method		getDirectDepositFormData
	 * @param		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @return		associative array
	 */
	function getDirectDepositFormData($OrgID, $ApplicationID, $RequestID) {
		
		//Set parameters for prepared query
		$params   =   array(
		                  ":ApplicationID"    =>  $ApplicationID,
		                  ":RequestID"        =>  $RequestID,
		                  ":OrgID"            =>  $OrgID
		              );
		//Set Prameters
		$sel_direct_deposit_data = "SELECT * FROM
									WebFormData
									WHERE WebFormID = '55a65bbf7fa38'
									AND ApplicationID = :ApplicationID 
									AND RequestID = :RequestID 
									AND OrgID = :OrgID";
		$res_direct_deposit_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_direct_deposit_data, array($params));
	
		return $res_direct_deposit_data;
	}	
	
	
	/**
	 * @method		getInternalFormsAssigned
	 * @param		string $OrgID
	 * @param		string $ApplicationID
	 * @param		string $RequestID
	 * @param		string $UniqueID
	 * @return		associative array
	 */
	function getInternalFormsAssigned($OrgID, $ApplicationID, $RequestID, $FormType, $UniqueID) {
		
		//Set parameters for prepared query
		$params       = array(
    		                  ":ApplicationID"    =>  $ApplicationID,
    		                  ":RequestID"        =>  $RequestID,
    		                  ":OrgID"            =>  $OrgID,
    		                  ":UniqueID"         =>  $UniqueID,
    		                  ":FormType"         =>  $FormType
        		              );
		
		//Set Prameters
		$sel_int_forms_assigned = "SELECT * FROM `InternalFormsAssigned` 
								   WHERE ApplicationID = :ApplicationID  
								   AND RequestID = :RequestID  
								   AND OrgID = :OrgID 
								   AND UniqueID = :UniqueID 
								   AND FormType = :FormType";
		
		$res_int_forms_assigned = $this->db->getConnection ( $this->conn_string )->fetchAssoc($sel_int_forms_assigned, array($params));

		return $res_int_forms_assigned;
	}	
	
	
	/**
	 * @method		getApplicationInfo
	 * @param		$OrgID
	 * @param		$ApplicationID
	 * @return		associative array
	 */
	function getApplicationInfo($OrgID, $ApplicationID) {
		
		//Set parameters for prepared query
		$params = array(":ApplicationID" => $ApplicationID, ":OrgID" => $OrgID);
		$sel_application_info = "SELECT * FROM `ApplicantData` WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
		$res_application_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_application_info, array($params));
		
		return $res_application_info;
	}
}
?>
