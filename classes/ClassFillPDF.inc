<?php
/**
 * @class       FillPDF
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FillPDF
{

    public function __construct()
    {
        $this->db = Database::getInstance();
        $this->db->getConnection("IRECRUIT");
    }

    /**
     *
     * @method isCustomPDFForm
     * @param string $FormID            
     * @return string
     */
    function isCustomPDFForm($OrgID,$FormID)
    {
            
        // Set parameters for prepared query
        $params     =   array(":formid"=>$FormID,":OrgID"=>$OrgID);
        // escape input values from sql injection
        $sel_form   =   "SELECT distinct(FormID) as FormID FROM CustomPDFForms WHERE OrgID = :OrgID AND FormID = :formid";
        $row_ans    =   $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_form, array($params));
        
        return $row_ans['FormID'];
    }

    /**
     *
     * @method createCusomPDF
     * @param string $WebFormID, $OrgID, ApplicationID, $RequestID
     * @return file
     */
    function createCustomPDF($WebFormID, $OrgID, $ApplicationID, $RequestID)
    {
        global $FormsInternalObj, $FormDataObj, $ApplicantsObj;
        
        $where  =   array(
                        "OrgID      =   :OrgID",
                        "WebFormID  =   :WebFormID"
                    );
        // Bind the parameters
        $params =   array(
                        ":OrgID"        =>  $OrgID,
                        ":WebFormID"    =>  $WebFormID
                    );
        // Get WebFormsInformation
        $web_forms_info = $FormsInternalObj->getWebFormsInfo("FormName", $where, "", array($params));
        
        list ($FileName) = array_values($web_forms_info['results'][0]);
        
        // Create Directories
        $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $dir .= "/applicantvault";
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $dir .= "/internalforms";
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        // PDF Forms
        $pdfform = IRECRUIT_DIR . 'formsInternal/PDFForms/Custom/' . $OrgID . '-' . $WebFormID . '.pdf';
        
        // Temp file for merged data
        $txfdffile = $dir . '/' . $ApplicationID . '_' . $WebFormID . '.xfdf';
        
        // Applicants Output file
        $outputpdf = $dir . '/' . $ApplicationID . '-' . $RequestID . '-' . $WebFormID . '.pdf';
        
        // get Custom WebForm Data
        
        // Set columns
        $columns    =   "QuestionID, QuestionTypeID, Answer, value";
        $where      =   array(
                            "OrgID          =   :OrgID",
                            "ApplicationID  =   :ApplicationID",
                            "RequestID      =   :RequestID",
                            "WebFormID      =   :WebFormID"
                        );
        $params     =   array(
                            ":OrgID"            =>  $OrgID,
                            ":ApplicationID"    =>  $ApplicationID,
                            ":RequestID"        =>  $RequestID,
                            ":WebFormID"        =>  $WebFormID
                        );
        // Get Web Form Data
        $WEBDATA    =   $FormDataObj->getWebFormData($columns, $where, "", "", array($params));
        
        // Set parameters for prepared query
        $params2[":formid"] = array($WebFormID);
        
        $CUSTOMDATA =   array();
        $REPLACE    =   array();
        
        foreach ($WEBDATA['results'] as $FD) {
            $CUSTOMDATA[$FD['QuestionID']]['Answer']            =   $FD['Answer'];
            $CUSTOMDATA[$FD['QuestionID']]['QuestionTypeID']    =   $FD['QuestionTypeID'];
            $CUSTOMDATA[$FD['QuestionID']]['value']		=   $FD['value'];
        }
        
        // escape input values from sql injection
        $sel_form   =   "SELECT QuestionID, PDFField FROM CustomPDFForms WHERE FormID = :formid";
        $sel_form .=    " and (QuestionID not like '%-fullname' and QuestionID not like '%-signaturedate')";
        $CUSTOMFORM =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc($sel_form, array($params2));
        
        $XFDF = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $XFDF .= "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n";
        $XFDF .= "<f href=\"" . $WebFormID . ".pdf\"/>\n";
        $XFDF .= "<fields>\n";
        foreach ($CUSTOMFORM['results'] as $CF) {
            
            if ($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 30) {
                
                $sel_fullname = "SELECT group_concat(Answer ORDER BY Question ASC SEPARATOR ' ') AS fullname";
                $sel_fullname .= " FROM WebFormData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
                $sel_fullname .= " AND RequestID = :RequestID";
		$sel_fullname .= " AND WebFormID = :WebFormID";
                $sel_fullname .= " AND QuestionID in (";
                $sel_fullname .= "SELECT QuestionID FROM CustomPDFForms WHERE FormID = :WebFormID2";
                $sel_fullname .= " AND ReplaceFields in ('firstname','lastname'))";

                $params3    =   array(
                            ":OrgID"            =>  $OrgID,
                            ":ApplicationID"    =>  $ApplicationID,
                            ":RequestID"        =>  $RequestID,
                            ":WebFormID"        =>  $WebFormID,
                            ":WebFormID2"       =>  $WebFormID
                        );
                
                $FULLNAME = $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_fullname, array($params3));
                
                $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                $XFDF .= "<value>Electronically signed by " . $FULLNAME['fullname'] . "</value>";
                $XFDF .= "</field>\n";
                $XFDF .= "<field name=\"Date\">";
                $XFDF .= "<value>" . $CUSTOMDATA[$CF['QuestionID']]['Answer'] . "</value>";
                $XFDF .= "</field>\n";
                $XFDF .= "<field name=\"Print Name\">";
                $XFDF .= "<value>" . $FULLNAME['fullname'] . "</value>";
                $XFDF .= "</field>\n";
            } else if ($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 18) {

                $Answer18 = json_decode($CUSTOMDATA[$CF['QuestionID']]['Answer'],true);

		   foreach ($Answer18 as $Answer18Key=>$Answer18Value) {

                     $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                     $XFDF .= "<value>" . $Answer18Value . "</value>";
                     $XFDF .= "</field>\n";

                   }
            } else if ($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 1818) {

                $Answer1818 = json_decode($CUSTOMDATA[$CF['QuestionID']]['Answer'],true);

		   foreach ($Answer1818 as $Answer1818Key=>$Answer1818Value) {

                     $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                     $XFDF .= "<value>" . $Answer1818Value . "</value>";
                     $XFDF .= "</field>\n";

                   }
	    } else if (($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 2) 
		|| ($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 3)) {

                $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                $XFDF .= "<value>" . G::Obj('WebFormQueAns')->getDisplayValue($CUSTOMDATA[$CF['QuestionID']]['Answer'],$CUSTOMDATA[$CF['QuestionID']]['value']) . "</value>";
                $XFDF .= "</field>\n";
		

	    } else if (($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 13) 
		|| ($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 14)) {


		$ans = $CUSTOMDATA[$CF['QuestionID']]['Answer'];

               	$Answer13   =   ($ans != "") ? json_decode($ans, true) : "";
			
               	if(!isset($Answer13[3])) { $Answer13[3] = ''; }

		$PHONE = G::Obj('Address')->formatPhone ( $OrgID, 'US', $Answer13[0], $Answer13[1], $Answer13[2], $Answer13[3] );
                $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                $XFDF .= "<value>" . $PHONE . "</value>";
                $XFDF .= "</field>\n";


            } else if ($CUSTOMDATA[$CF['QuestionID']]['QuestionTypeID'] == 15) {
                    
                $Answer15 = json_decode($CUSTOMDATA[$CF['QuestionID']]['Answer'],true);
                $SSN =   implode("-", $Answer15);
                $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                $XFDF .= "<value>" . $SSN . "</value>";
                $XFDF .= "</field>\n";

            } else {
                
                $XFDF .= "<field name=\"" . $CF['PDFField'] . "\">";
                $XFDF .= "<value>" . $CUSTOMDATA[$CF['QuestionID']]['Answer'] . "</value>";
                $XFDF .= "</field>\n";
            }
        }
        $XFDF .= "</fields>\n";
        $XFDF .= "</xfdf>\n";
        
        // Write temp data file
        $fh = fopen($txfdffile, 'w');
        if (! $fh) {
            die('Cannot open file ' . $txfdffile);
        }
        fwrite($fh, $XFDF);
        fclose($fh);
        
        // Merge temp file with PDF and write to applicants file
        $command = 'pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($txfdffile) . ' output ' . escapeshellarg($outputpdf);
        
        system($command);
        // remove temp file
        system('rm ' . escapeshellarg($txfdffile));
        @chmod($outputpdf, 0666);
        
        return $outputpdf;
    } // end function
    
    /**
     *
     * @method createCRFPDF
     * @param string $pdfFileName,
     *            $WebFormID, $OrgID, ApplicationID, $RequestID
     * @return file
     */
    function createCRFPDF($pdfFileName, $WebFormID, $OrgID, $ApplicationID, $RequestID)
    {
        global $FormDataObj, $ApplicantsObj;
        
        // PDF Forms
        $pdfform    =   IRECRUIT_DIR . 'formsInternal/PDFForms/Custom/' . preg_replace('/\s/', '_', $pdfFileName) . '.pdf';
        // SFDF file for input
        $xfdffile   =   IRECRUIT_DIR . 'formsInternal/PDFForms/Custom/' . preg_replace('/\s/', '_', $pdfFileName) . '_data.xfdf';
        
        // Create Directories
        $dir        =   IRECRUIT_DIR . 'vault/' . $OrgID;
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $dir .= "/applicantvault";
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        $dir .= "/internalforms";
        if (! file_exists($dir)) {
            mkdir($dir, 0700);
            chmod($dir, 0777);
        }
        
        // Temp file for merged data
        $txfdffile  =    $dir . '/' . $ApplicationID . '_' . preg_replace('/\s/', '_', $pdfFileName) . '.xfdf';
        
        // Applicants Output file
        $outputpdf  =    $dir . '/' . preg_replace('/\s/', '_', $pdfFileName) . '-' . $ApplicationID . '-' . $RequestID . '.pdf';
        
        // Gather Data // Applicant
        $APPDATA    =   $ApplicantsObj->getAppData($OrgID, $ApplicationID);

	    $CURRENT    =   G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);
        
        // set where condition to get prefilled form data
        $where_info =   array(
                            "OrgID               = :OrgID",
                            "PreFilledFormID     = :PreFilledFormID",
                            "ApplicationID       = :ApplicationID",
                            "RequestID           = :RequestID"
                        );
        
        // i9Data
        $pre_form_data_info[] = array(
                            ":OrgID"            =>  $OrgID,
                            ":PreFilledFormID"  =>  $CURRENT['I9'],
                            ":ApplicationID"    =>  $ApplicationID,
                            ":RequestID"        =>  $RequestID
                        );
        
        // Get PreFilled Form Data Information
        $pre_form_data_results = $FormDataObj->getPreFilledFormData("*", $where_info, "", "", $pre_form_data_info);
        
        // i9Data
        if (is_array($pre_form_data_results['results'][0])) {
            foreach ($pre_form_data_results['results'] as $PFD) {
                $I9[$PFD['QuestionID']] = $PFD['Answer'] != "" ? $PFD['Answer'] : "N/A";
            } // end foreach
        }
        
        // Data
        $FirstName  =   $APPDATA['first'];
        $LastName   =   $APPDATA['last'];
        $MiddleName =   $APPDATA['middle'];
        
        $MiddleInitial  =   strtoupper($APPDATA['middle']);
        if (($MiddleInitial != "N/A") && ($MiddleInitial != "")) {
            $MiddleInitial = strtoupper(substr($APPDATA['middle'], 0, 1));
        }
        
        $Address1           =   $APPDATA['address'];
        $City               =   $APPDATA['city'];
        $State              =   $APPDATA['state'];
        $ZipCode            =   $APPDATA['zip'];
        $EmailAddress       =   "";
        $TelephoneNumber    =   "";
        $SSN                =   "";
        $DOB                =   "";
        
        if ($I9['FirstName'] != "") {
            
            $FirstName      =   $I9['FirstName'];
            $LastName       =   $I9['LastName'];
            $MiddleName     =   $I9['MiddleName'];
            
            $MiddleInitial  =   strtoupper($I9['MiddleName']);
            if (($MiddleInitial != "N/A") && ($MiddleInitial != "")) {
                $MiddleInitial = strtoupper(substr($I9['MiddleName'], 0, 1));
            }
            
            $Address1           =   $I9['Address1'];
            $Address2           =   $I9['Address2'];
            $City               =   $I9['City'];
            $State              =   $I9['State'];
            $ZipCode            =   $I9['ZipCode'];
            $EmailAddress       =   $I9['EmailAddress'];
            $TelephoneNumber    =   $I9['TelephoneNumber'];

            $Answer15 = json_decode($I9['SSN'],true);
            $SSN =   implode("-", $Answer15);

            $DOB                =   $I9['DOB'];
        }
        
        // get Custom WebForm Data
        
        // Set columns
        $columns    =   array("QuestionID", "Answer");
        $where      =   array(
                            "OrgID              =   :OrgID",
                            "ApplicationID      =   :ApplicationID",
                            "RequestID          =   :RequestID",
                            "WebFormID          =   :WebFormID"
                        );
        $params     =   array(
                            ":OrgID"            =>  $OrgID,
                            ":ApplicationID"    =>  $ApplicationID,
                            ":RequestID"        =>  $RequestID,
                            ":WebFormID"        =>  $WebFormID
                        );
        
        // Get Web Form Data
        $results    =   $FormDataObj->getWebFormData($columns, $where, "", "", array($params));
        
        if (is_array($results['results'])) {
            foreach ($results['results'] as $WFA) {
                $APPDATA[$WFA['QuestionID']] = $WFA['Answer'];
            }
        }
        
        $CAY        =   "";
        $CAN        =   "";
        $ANY        =   "";
        $ANN        =   "";
        
        if ($APPDATA['CUST54230e03be1ac'] == "Yes") {
            $CAY    =   "Yes";
        }
        if ($APPDATA['CUST54230e03be1ac'] == "No") {
            $CAN    =   "Yes";
        }
        if ($APPDATA['CUST54230e08b0c6c'] == "Yes") {
            $ANY    =   "Yes";
        }
        if ($APPDATA['CUST54230e08b0c6c'] == "No") {
            $ANN    =   "Yes";
        }
        
        $OrgName    =   $APPDATA['CUST54230e0be4d95'];
        $OrgNumber  =   $APPDATA['CUST54230e0d5c72d'];
        $Name       =   $APPDATA['CUST54230e0f266b1'];
        $Address1   =   $APPDATA['CUST54230e10c03a6'];
        $City       =   $APPDATA['CUST54230e1243b78'];
        $Zip        =   $APPDATA['CUST54230e13a8226'];

        $Answer15 = json_decode($APPDATA['CUST54230e1528e0a'],true);
        $SSN =   implode("-", $Answer15);

        $DOB        =   $APPDATA['CUST54230e16aa933'];
        
        $DMVLicenseNumber = $APPDATA['CUST54230e19292eb'];
        
        $Offense    =   $APPDATA['CUST54230e22acb6c'];
        $Occur      =   $APPDATA['CUST54230e276fad9'];
        $Happened   =   $APPDATA['CUST54230e29b1989'] . ' ' . $APPDATA['CUST54230e2ba7d02'];
        $CityStateOffense = $APPDATA['CUST54230e2521db2'];
        
        if ($APPDATA['captcha'] != "") {
            $Signature  =   'electronically signed  by ' . $Name;
            $Date       =   $APPDATA['CUST54230f5496bba'];
        } else {
            $Signature  =   "N/A";
            $Date       =   "N/A";
        }
        
        $OrgName    =   $OrgName != "" ? $OrgName : "N/A";
        $OrgNumber  =   $OrgNumber != "" ? $OrgNumber : "N/A";
        $Name       =   $Name != " " ? $Name : "N/A";
        $Address1   =   $Address1 != "" ? $Address1 : "N/A";
        $City       =   $City != "" ? $City : "N/A";
        $Zip        =   $Zip != "" ? $Zip : "N/A";
        $SSN        =   $SSN != "--" ? $SSN : "N/A";
        $DOB        =   $DOB != "" ? $DOB : "N/A";
        
        $DMVLicenseNumber   =   $DMVLicenseNumber != "" ? $DMVLicenseNumber : "N/A";
        $Date               =   $Date != "" ? $Date : "N/A";
        
        $Offense            =   $Offense != "" ? $Offense : "N/A";
        $CityStateOffense   =   $CityStateOffense != "" ? $CityStateOffense : "N/A";
        $Occur              =   $Occur != "" ? $Occur : "N/A";
        $Happened           =   $Happened != "" ? $Happened : "N/A";
        
        if (file_exists($xfdffile)) {
            $XFDF   =   "";
            $fh     =   fopen($xfdffile, 'r');
            while (! feof($fh)) {
                
                $line = fgets($fh);
                
                $line = preg_replace('/\[CAY\]/', $CAY, $line);
                $line = preg_replace('/\[CAN\]/', $CAN, $line);
                $line = preg_replace('/\[ANY\]/', $ANY, $line);
                $line = preg_replace('/\[ANN\]/', $ANN, $line);
                $line = preg_replace('/\[OrgName\]/', $OrgName, $line);
                $line = preg_replace('/\[OrgNumber\]/', $OrgNumber, $line);
                $line = preg_replace('/\[Name\]/', $Name, $line);
                $line = preg_replace('/\[Address\]/', $Address1, $line);
                $line = preg_replace('/\[City\]/', $City, $line);
                $line = preg_replace('/\[Zip\]/', $Zip, $line);
                $line = preg_replace('/\[SSN\]/', $SSN, $line);
                $line = preg_replace('/\[DOB\]/', $DOB, $line);
                $line = preg_replace('/\[DMVLicenseNumber\]/', $DMVLicenseNumber, $line);
                $line = preg_replace('/\[Signature\]/', $Signature, $line);
                $line = preg_replace('/\[Date\]/', $Date, $line);
                
                $line = preg_replace('/\[Offense\]/', $Offense, $line);
                $line = preg_replace('/\[CityStateOffense\]/', $CityStateOffense, $line);
                $line = preg_replace('/\[Occur\]/', $Occur, $line);
                $line = preg_replace('/\[Happened\]/', $Happened, $line);
                
                $line = preg_replace('/\[Signature2\]/', $Signature, $line);
                $line = preg_replace('/\[Date2\]/', $Date, $line);
                
                $XFDF .= $line;
            }
        }
        fclose($fh);
        
        // Write temp data file
        $fh = fopen($txfdffile, 'w');
        if (! $fh) {
            die('Cannot open file ' . $txfdffile);
        }
        fwrite($fh, $XFDF);
        fclose($fh);
        
        // Merge temp file with PDF and write to applicants file
        $command = 'pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($txfdffile) . ' output ' . escapeshellarg($outputpdf);
        system($command);
        // remove temp file
        system('rm ' . escapeshellarg($txfdffile));
        @chmod($outputpdf, 0666);
        
        return $outputpdf;
    } // end function

} // end Class
