<?php
/**
 * @class		Appointments
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Appointments {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}	
	
	/**
	 * @method	getScheduleInterviewInfo
	 */
	function getScheduleInterviewInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM ScheduleInterview";
	
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($group_by != "") $sel_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
	
		$res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method	getApplicantInterviewDate
	 */
	function getApplicantInterviewDate($columns, $OrgID, $ApplicationID) {
	
	    $columns   =   $this->db->arrayToDatabaseQueryString ( $columns );
	    $params    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
	    $sel_info  =   "SELECT $columns FROM ScheduleInterview WHERE SIOrgID = :OrgID AND SIApplicationID = :ApplicationID";
	    $res_info  =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_info, array($params) );
	
	    return $res_info;
	}
	
	/**
	 * @method	getAppointments
	 * @param	$OrgID, $Active, $IsAdmin
	 * @return 	array @type json
	 */
	public function getAppointments($POST = '') {
		global $USERID, $OrgID, $USERROLE;
		
		/**
		 * @input json data
		 */
		if ($POST == '')
			$POST = file_get_contents ( 'php://input' );
		$JSON = json_decode ( $POST, true );
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$JSON ['SMonth'], ":SYear"=>$JSON ['SYear'], ":SIOrgID"=>$OrgID, ":SDate"=>$JSON ['SDate']);
		
		$sort = 'ASC';
		$scheduleinterviews = "SELECT SID, SIRecipient, DAY(SIDateTime) as SIDay, SISubject, 
							   DATE_FORMAT(SIDateTime,'%h:%i %p') AS SITime, 
							   DATE_FORMAT(SIDateTime,'%m/%d/%Y') AS SIDate, 
							   SIApplicationID, SIRequestID, SIFlag, SIEmail
							   FROM ScheduleInterview WHERE MONTH(SIDateTime) = :SMonth  
							   AND YEAR(SIDateTime) = :SYear";
		$scheduleinterviews .= " AND SIOrgID = :SIOrgID";
		
		$scheduleinterviews .= " AND DATE(SIDateTime) >= :SDate";
		
		if (isset($JSON ['AppointmentType']) && $JSON ['AppointmentType'] == "DAY") {
			$params [":SDay"] = $JSON ['SDay'];
			$scheduleinterviews .= " AND DAY(SIDateTime) = :SDay";
		}
		
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params [":ReqManagerUserID"] = $USERID;
			$params [":ReqOwnerUserID"] = $USERID;
			$params [":ReqManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :ReqOwnerUserID";
			$scheduleinterviews .= " OR ";
			$scheduleinterviews .= " SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManagerOrgID AND UserID = :ReqManagerUserID)";
			$scheduleinterviews .= ")";
		} // end hiring manager
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) != 'master_hiring_manager') {
			$params [":SIRequisitionOwner"] = $USERID;
			$params [":UserID"] = $USERID;
			$params [":SIReqManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIRequisitionOwner OR SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqManagerOrgID AND UserID = :UserID))";
		}
		
		$scheduleinterviews .= " ORDER BY SIDateTime $sort LIMIT 10";
		
		// Set Parameters
		$resultinterviews = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $scheduleinterviews, array($params) );
		
		$dayschedules = array();
		foreach ( $resultinterviews ['results'] as $row ) {
			$dayschedules [$row ['SIDay']] [] = $row;
		}
		
		return json_encode ( $dayschedules );
	}
	
	/**
	 *
	 * @method	countAppointments
	 * @param	$POST(optional)
	 * @return	array @type json
	 */
	public function countAppointments($POST = '') {
		
		if ($POST == '')
			$POST = file_get_contents ( 'php://input' );
		$JSON = json_decode ( $POST, true );
		
		global $USERID, $USERROLE;
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$JSON ['SMonth'], ":SYear"=>$JSON ['SYear'], ":UserID"=>$USERID);
		
		$selappointments = "SELECT COUNT(*) as AppointmentCount 
							FROM ScheduleInterview 
							WHERE SIUserID = :UserID";
		
		if ($JSON ['AppointmentType'] == "DAY") {
			$params [":SDay"] = $JSON ['SDay'];
			$selappointments .= " AND DAY(SIDateTime) = :SDay";
		}
		
		$selappointments .= " AND MONTH(SIDateTime) = :SMonth";
		$selappointments .= " AND YEAR(SIDateTime) = :SYear";
		$selappointments .= " ORDER BY SIDateTime ASC";
		
		$row = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $selappointments, array($params) );
		
		return $row ['AppointmentCount'];
	}
	
	/**
	 * @method		getRemindersByUserID
	 * 
	 */
	function getRemindersByUserID($UserID) {
		
		$params = array(":UserID"=>$UserID, ":DateTime"=>date('Y-m-d H:i:s'));
		$sel_reminders = "SELECT * FROM AppointmentReminder WHERE UserID = :UserID AND DateTime >= :DateTime";
		$res_reminders = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_reminders, array($params) );
		
		return $res_reminders;
	}
	
	/**
	 * @method		calendarWeekView
	 * @param 	      	
	 * @return 		string(calendar in table format)
	 * @tutorial 	This method will create a calendar based on given month, year, day.
	 */
	function calendarWeekView($SYear, $SMonth, $SDay, $Navigation = '') {
		global $OrgID, $USERID, $USERROLE, $feature, $permit, $IrecruitUsersObj;
		
		$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $USERID, "*" );
		
		$date1 = date ( "$SYear-$SMonth-$SDay" );
		
		// Based on the $Navigation, we will calculate the date range
		if ($Navigation == "" || $Navigation == "next") {
			$date2 = date ( "Y-m-d", strtotime ( '+6 days', strtotime ( $date1 ) ) );
			$max_date = $date2;
			$min_date = $date1;
		}
		if ($Navigation == "previous") {
			$date2 = date ( "Y-m-d", strtotime ( '-6 days', strtotime ( $date1 ) ) );
			$max_date = $date1;
			$min_date = $date2;
		}
		
		$div_icon1_class = "cal_tableview_div_icon1";
		$div_icon2_class = "cal_tableview_div_icon2";
		$div_subject_class = "cal_tableview_div_subject";
		$div_time_class = "cal_tableview_div_time";
		$div_app_rem_block_class = "cal_weekview_appointment_rem_block";
		$div_img_time_block = 'reminder_img_time_block';
		
		$imgsrcreminder = '<img src="' . IRECRUIT_HOME . 'images/blue-bell-1-16.png">';
		$imgsrcappointment = '<img src="' . IRECRUIT_HOME . 'images/EventReminderSmall.png">';
		
		$calendar_class = "calendar_top_navigation";
		
		// Set parameters for prepared query
		$params = array (":MinDate"=>$min_date, ":MaxDate"=>$max_date, ":UserID"=>$USERID);
		$selreminders = "SELECT SID, DAY(DateTime) as RDay, MONTH(DateTime) as RMonth, Subject, AppointmentReminderID,
							date_format(DateTime,'%h:%i %p') AS RTime, DateTime as ReminderDateTime
							FROM AppointmentReminder
							WHERE (DATE(DateTime) >= :MinDate AND DATE(DateTime) <= :MaxDate) AND (UserID = :UserID OR UserID = 'MASTER')";
		$resreminders = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $selreminders, array($params) );
		
		$params = array (":MinDate"=>$min_date, ":MaxDate"=>$max_date, ":SIOrgID"=>$OrgID);
		$scheduleinterviews = "SELECT SID, SIRecipient, DAY(SIDateTime) as SIDay, MONTH(SIDateTime) as SIMonth,
								date_format(SIDateTime,'%h:%i %p') AS SITime, SIApplicationID, SIRequestID,
								SIFlag, SISubject, SIDateTime as AppointmentDateTime
								FROM ScheduleInterview WHERE (DATE(SIDateTime) >= :MinDate AND DATE(SIDateTime) <= :MaxDate)";
		$scheduleinterviews .= " AND SIOrgID = :SIOrgID";
		
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params [":SIReqManagersUserID"] = $USERID;
			$params [":SIReqOwnerUserID"] = $USERID;
			$params [":SIReqManagersOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqOwnerUserID";
			$scheduleinterviews .= " OR ";
			$scheduleinterviews .= " SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqManagersOrgID AND UserID = :SIReqManagersUserID)";
			$scheduleinterviews .= ")";
		} // end hiring manager
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) != 'master_hiring_manager') {
			$params [":SIReqHManagersUserID"] = $USERID;
			$params [":SIReqHOwnerUserID"] = $USERID;
			$params [":SIReqHManagersOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqHOwnerUserID OR SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqHManagersOrgID AND UserID = :SIReqHManagersUserID))";
		}
		
		// Set Parameters
		$scheduleinterviews .= " ORDER BY SIDateTime ASC";
		$resultinterviews = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $scheduleinterviews, array($params));
		
		for($day_index = 0; $day_index <= 6; $day_index ++) {
			$date_day = date ( "d", strtotime ( '+' . $day_index . ' days', strtotime ( $min_date ) ) );
			$days [( int ) $date_day] = 1;
		}
		
		if (is_array ( $resultinterviews ['results'] )) {
			foreach ( $resultinterviews ['results'] as $row ) {
				$dayschedules [$row ['SIDay']] [] = $row;
			}
		}
		
		if (is_array ( $resreminders ['results'] )) {
			foreach ( $resreminders ['results'] as $rowrem ) {
				$dayschedules1 [$rowrem ['RDay']] [] = $rowrem;
			}
		}
		
		$calendar = "";
		$calendar .= "<div class='table-responsive'>";
		$calendar .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table table-bordered table-hover calendar'>";
		$calendar .= "<tr>";
		for($d = 0; $d < 7; $d ++) {
			$calendar .= "<th align='center' width='10%' nowrap='nowrap'>" . date ( 'M d Y', strtotime ( "+" . $d . " days", strtotime ( $min_date ) ) ) . "</th>";
		}
		$calendar .= "</tr>";
		
		$calendar .= "<tr>";
		
		$appointments_reminders = 'No';
		$td_count = 0;
		
		// $i is the current day
		foreach ( $days as $i => $value ) {
			$td_count ++;
			$calendar .= "<td align='left'>";
			for($di = 0; $di < count ( $dayschedules [$i] ); $di ++) {
				$appointments_reminders = 'Yes';
				
				if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( 0, $dayschedules1 [$i] )) {
					$reset_reminders_array = false;
					for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
						if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( $ri, $dayschedules1 [$i] ) && strtotime ( $dayschedules1 [$i] [$ri] ['ReminderDateTime'] ) < strtotime ( $dayschedules [$i] [$di] ['AppointmentDateTime'] )) {
							include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
							
							$reset_reminders_array = true;
							unset ( $dayschedules1 [$i] [$ri] );
						}
					}
					if ($reset_reminders_array == true) {
						$dayschedules1 [$i] = array_values ( $dayschedules1 [$i] );
					}
				}
				
				include IRECRUIT_DIR . 'appointments/AppointmentsInfo.inc';
			}
			for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
				$appointments_reminders = 'Yes';
				include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
			}
			$calendar .= "</td>";
		}
		
		if ($td_count > 0) {
			while ( $td_count < 7 ) {
				$calendar .= "<td>&nbsp;</td>";
				$td_count ++;
			}
		}
		
		if ($appointments_reminders == 'No') {
			$calendar .= "<tr>";
			$calendar .= "<td align='center' colspan='7'>";
			$calendar .= "You don't have any reminders or appointments in this week";
			$calendar .= "</td>";
			$calendar .= "</tr>";
		}
		
		$calendar .= "</tr>";
		
		$calendar .= "</table>";
		$calendar .= "</div>";
		
		// Print the entire calendar
		return $calendar;
	}
	
	/**
	 * @method 		calendarDayView
	 * @param		$SYear, $SMonth, $SDay
	 * @return 		string(calendar in table format)
	 * @tutorial	This method will create a calendar based on given month, year, day.
	 */
	function calendarDayView($SYear, $SMonth, $SDay) {
		global $OrgID, $USERID, $USERROLE, $feature, $permit, $IrecruitUsersObj;
		
		$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $USERID, "*" );
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$SMonth, ":SYear"=>$SYear, ":SDay"=>$SDay, ":UserID"=>$USERID);
		//Select reminders
		$selreminders = "SELECT SID, DAY(DateTime) AS RDay, Subject, AppointmentReminderID,
						 date_format(DateTime,'%h:%i %p') AS RTime, DateTime as ReminderDateTime
						 FROM AppointmentReminder
						 WHERE MONTH(DateTime) = :SMonth 
						 AND YEAR(DateTime) = :SYear 
						 AND DAY(DateTime) = :SDay
						 AND (UserID = :UserID OR UserID = 'MASTER')";
		$resreminders = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $selreminders, array($params) );
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$SMonth, ":SYear"=>$SYear, ":SDay"=>$SDay, ":SIOrgID"=>$OrgID);
		// Get ScheduleInterview Information
		$scheduleinterviews = "SELECT SID, SIRecipient, SISubject, DAY(SIDateTime) as SIDay, SIDateTime as AppointmentDateTime,
							   date_format(SIDateTime,'%h:%i %p') AS SITime, SIApplicationID, SIRequestID, SIFlag
							   FROM ScheduleInterview
							   WHERE MONTH(SIDateTime) = :SMonth 
							   AND YEAR(SIDateTime) = :SYear 
							   AND DAY(SIDateTime) = :SDay";
		$scheduleinterviews .= " AND SIOrgID = :SIOrgID";
		
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params [":SIReqHOwnerUserID"] = $USERID;
			$params [":SIReqHManagerUserID"] = $USERID;
			$params [":SIReqHManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqHOwnerUserID";
			$scheduleinterviews .= " OR ";
			$scheduleinterviews .= " SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqHManagerOrgID AND UserID = :SIReqHManagerUserID)";
			$scheduleinterviews .= ")";
		} // end hiring manager
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) != 'master_hiring_manager') {
			$params [":SIReqOwnerUserID"] = $USERID;
			$params [":SIReqManagerUserID"] = $USERID;
			$params [":SIReqManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqOwnerUserID";
			$scheduleinterviews .= " OR SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqManagerOrgID AND UserID = :SIReqManagerUserID))";
		}
		
		$scheduleinterviews .= " ORDER BY SIDateTime ASC";
		
		// Set Parameters
		$resultinterviews = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $scheduleinterviews, array($params));
		
		if (is_array ( $resultinterviews ['results'] )) {
			foreach ( $resultinterviews ['results'] as $row ) {
				$dayschedules [$row ['SIDay']] [] = $row;
			}
		}
		
		if (is_array ( $resreminders ['results'] )) {
			foreach ( $resreminders ['results'] as $rowrem ) {
				$dayschedules1 [$rowrem ['RDay']] [] = $rowrem;
			}
		}
		
		$days = array (
				"1" => "Sunday",
				"Monday",
				"Tuesday",
				"Wednesday",
				"Thursday",
				"Friday",
				"Saturday" 
		);
		
		$div_icon1_class = "cal_tableview_div_icon1";
		$div_icon2_class = "cal_tableview_div_icon2";
		$div_subject_class = "cal_dayview_div_subject";
		$div_time_class = "cal_dayview_div_time";
		$div_app_rem_block_class = "cal_dayview_appointment_rem_block";
		$div_img_time_block = 'cal_dayview_app_rem_img_time_wrapper';
		
		$imgsrcreminder = '<img src="' . IRECRUIT_HOME . 'images/blue-bell-1-16.png">';
		$imgsrcappointment = '<img src="' . IRECRUIT_HOME . 'images/EventReminderSmall.png">';
		
		$calendar_class = "calendar_top_navigation";
		
		$time = strtotime ( date ( "$SYear-$SMonth-$SDay" ) );
		$firstDay = mktime ( 0, 0, 0, $SMonth, 1, $SYear );
		$daysInMonth = cal_days_in_month ( 0, $SMonth, $SYear );
		$dayOfWeek = date ( 'w', $firstDay );
		
		$calendar = "";
		$calendar .= "<div class='table-responsive' style='border:1px solid #ded9d9;overflow:hidden'>";
		$calendar .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table table-bordered table-hover calendar' style='margin-bottom:0px'>";
		
		$calendar .= "<tr>";
		$calendar .= "<th align='center'><strong>" . date ( 'F - j l Y', strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) . "</strong></th>";
		$calendar .= "</tr>";
		
		$appointments_reminders = 'No';
		$i = ( int ) $SDay;
		if (is_array ( $dayschedules [$i] ) ) {
		for($di = 0; $di < count ( $dayschedules [$i] ); $di ++) {
			$appointments_reminders = 'Yes';
			
			if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( 0, $dayschedules1 [$i] )) {
				$reset_reminders_array = false;
				for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
					if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( $ri, $dayschedules1 [$i] ) && strtotime ( $dayschedules1 [$i] [$ri] ['ReminderDateTime'] ) < strtotime ( $dayschedules [$i] [$di] ['AppointmentDateTime'] )) {
						include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
						
						$reset_reminders_array = true;
						unset ( $dayschedules1 [$i] [$ri] );
					}
				}
				if ($reset_reminders_array == true) {
					$dayschedules1 [$i] = array_values ( $dayschedules1 [$i] );
				}
			}
			
			include IRECRUIT_DIR . 'appointments/AppointmentsInfo.inc';
		}
		}
		for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
			$appointments_reminders = 'Yes';
			include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
		}
		
		if ($appointments_reminders == 'No') {
			$calendar .= "<tr>";
			$calendar .= "<td align='center'>";
			$calendar .= "You don't have any reminders or appointments for this day";
			$calendar .= "</td>";
			$calendar .= "</tr>";
		}
		
		$calendar .= "</table>";
		$calendar .= "</div>";
		
		// Print the entire calendar
		return $calendar;
	}
	
	/**
	 * @method		calendarTableView
	 * @param 		$SYear, $SMonth, $SDay, $HeaderNavigation
	 * @return 		string(calendar in table format)
	 * @tutorial 	This method will create a calendar based on given month, year, day.
	 */
	function calendarTableView($SYear, $SMonth, $SDay, $HeaderNavigation = true) {
		global $OrgID, $USERID, $USERROLE, $feature, $permit, $IrecruitUsersObj;
		
		$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $USERID, "*" );
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$SMonth, ":SYear"=>$SYear, ":UserID"=>$USERID);
		$selreminders = "SELECT SID, DAY(DateTime) AS RDay, Subject, AppointmentReminderID,
						 date_format(DateTime,'%h:%i %p') AS RTime, DateTime AS ReminderDateTime
						 FROM AppointmentReminder
						 WHERE MONTH(DateTime) = :SMonth 
						 AND YEAR(DateTime) = :SYear AND (UserID = :UserID OR UserID = 'MASTER')";
		$resreminders = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $selreminders, array($params));
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$SMonth, ":SYear"=>$SYear, ":SIOrgID"=>$OrgID);
		$scheduleinterviews = "SELECT SID, SIRecipient, DAY(SIDateTime) AS SIDay, SIDateTime AS AppointmentDateTime,
							  date_format(SIDateTime,'%h:%i %p') AS SITime, SIApplicationID, SIRequestID, SIFlag
							  FROM ScheduleInterview WHERE MONTH(SIDateTime) = :SMonth 
							  AND YEAR(SIDateTime) = :SYear";
		$scheduleinterviews .= " and SIOrgID = :SIOrgID";
		
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params [":SIReqHOwnerUserID"] = $USERID;
			$params [":SIReqHManagerUserID"] = $USERID;
			$params [":SIReqHManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqHOwnerUserID";
			$scheduleinterviews .= " OR ";
			$scheduleinterviews .= " SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqHManagerOrgID AND UserID = :SIReqHManagerUserID)";
			$scheduleinterviews .= ")";
		} // end hiring manager
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) != 'master_hiring_manager') {
			$params [":SIReqOwnerUserID"] = $USERID;
			$params [":SIReqManagerUserID"] = $USERID;
			$params [":SIReqManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqOwnerUserID OR SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqManagerOrgID AND UserID = :SIReqManagerUserID))";
		}
		
		$scheduleinterviews .= " ORDER BY SIDateTime ASC";
		
		// Set Parameters
		$resultinterviews = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $scheduleinterviews, array($params));
		
		if (is_array ( $resultinterviews ['results'] )) {
			foreach ( $resultinterviews ['results'] as $row ) {
				$dayschedules [$row ['SIDay']] [] = $row;
			}
		}
		
		if (is_array ( $resreminders ['results'] )) {
			foreach ( $resreminders ['results'] as $rowrem ) {
				$dayschedules1 [$rowrem ['RDay']] [] = $rowrem;
			}
		}
		
		$days = array (
				"1" => "Sunday",
				"Monday",
				"Tuesday",
				"Wednesday",
				"Thursday",
				"Friday",
				"Saturday" 
		);
		
		$time = strtotime ( date ( "$SYear-$SMonth-$SDay" ) );
		$firstDay = mktime ( 0, 0, 0, $SMonth, 1, $SYear );
		$daysInMonth = cal_days_in_month ( 0, $SMonth, $SYear );
		$dayOfWeek = date ( 'w', $firstDay );
		
		// Calculate Previous and Next month
		$previousMonth = date ( 'Y-m-d', strtotime ( "-1 months", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		$nextMonth = date ( 'Y-m-d', strtotime ( "+1 months", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		
		// Calculate Previous and Next year
		$previousYear = date ( 'Y-m-d', strtotime ( "-1 year", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		$nextYear = date ( 'Y-m-d', strtotime ( "+1 year", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		
		// $HeaderNavigation is false this calendar is using in dashboard widget
		// $calendar_class = ($HeaderNavigation == false) ? "dashboard_cal_top_navigation" : "calendar_top_navigation";
		$calendar_class = "calendar_top_navigation";
		
		$div_icon1_class = "cal_tableview_div_icon1";
		$div_icon2_class = "cal_tableview_div_icon2";
		$div_subject_class = "cal_tableview_div_subject";
		$div_time_class = "cal_tableview_div_time";
		$div_app_rem_block_class = "cal_monthview_appointment_rem_block";
		$div_img_time_block = 'reminder_img_time_block';
		
		$calendar = "";
		
		// $HeaderNavigation is false we will not load this header, and this calendar is called in dashboard
		if ($HeaderNavigation === true) {
			$calendar .= "<div class='table-responsive'>";
			$calendar .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table table-bordered table-hover $calendar_class'>";
			$calendar .= '<tr>';
			$calendar .= '<th align="center" colspan="7" style="text-align:center">';
			$calendar .= '&nbsp;&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("Table", "' . $previousYear . '")\'><<</a>'; // Previous Month
			$calendar .= '&nbsp;&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("Table", "' . $previousMonth . '")\'><</a>'; // Previous Year
			$calendar .= '&nbsp;&nbsp;' . date ( 'F', $time ) . ' ' . $SYear;
			$calendar .= '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("Table", "' . $nextMonth . '")\'>></a>'; // Next Year
			$calendar .= '&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("Table", "' . $nextYear . '")\'>>></a></th>'; // Next Month
			$calendar .= '</tr>';
			$calendar .= '</table>';
			$calendar .= '</div>';
		}
		
		$calendar .= "<div class='table-responsive'>";
		$calendar .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table table-bordered table-hover calendar'>";
		
		// It will display the days like Sunday, Monday, Tuesday...
		$calendar .= "<tr>";
		for($d = 1; $d <= 7; $d ++)
			$calendar .= "<th align='center' width='14%'>" . $days [$d] . "</th>";
		$calendar .= "</tr>";
		
		// Calendar body data starts here
		$calendar .= "<tr>";
		
		// Flag to check the tr is closed or not
		$trclosed = false;
		$td_count = 0;
		// Create empty cells until before first day starts
		for($k = 1; $k <= $dayOfWeek; $k ++) {
			$td_count ++;
			$calendar .= "<td>&nbsp;</td>";
		}
		
		for($i = 1; $i <= $daysInMonth; $i ++) {
			// Flag for tr opened or closed
			$trclosed = false;
			
			$eventdayclass = '';
			if (isset ( $dayschedules [$i] ))
				$eventdayclass = ' eventday';
			if (isset ( $dayschedules1 [$i] ))
				$eventdayclass = ' eventday';
			if (date ( 'w', mktime ( 0, 0, 0, $SMonth, $i, $SYear ) ) == 0)
				$eventdayclass = ' weekend';
			
			$imgsrcreminder = '<img src="' . IRECRUIT_HOME . 'images/blue-bell-1-16.png">';
			$imgsrcappointment = '<img src="' . IRECRUIT_HOME . 'images/EventReminderSmall.png">';
			
			$td_count ++;
			if ($td_count >= 7)
				$td_count = 0;
				
				// print days
			$calendar .= "<td valign='top' class='calendar-day$eventdayclass'>";
			
			$calendar .= "<div>";
			
			$calendar .= "<div style='height:25px;width:100%;text-align:right'>";
			$calendar .= $i;
			$calendar .= "</div>";
			
			if (isset ( $dayschedules [$i] )) {
				for($di = 0; $di < count ( $dayschedules [$i] ); $di ++) {
					
					if (isset ( $dayschedules1 [$i] )
					   && is_array ( $dayschedules1 [$i] ) 
					   && is_array ( $dayschedules1 [$i] ) 
					   && array_key_exists ( 0, $dayschedules1 [$i] )) {
						$reset_reminders_array = false;
						for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
							
							if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( $ri, $dayschedules1 [$i] ) && strtotime ( $dayschedules1 [$i] [$ri] ['ReminderDateTime'] ) < strtotime ( $dayschedules [$i] [$di] ['AppointmentDateTime'] )) {
								include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
								
								$reset_reminders_array = true;
								unset ( $dayschedules1 [$i] [$ri] );
							}
						}
						if ($reset_reminders_array == true) {
							$dayschedules1 [$i] = array_values ( $dayschedules1 [$i] );
						}
					}
					
					include IRECRUIT_DIR . 'appointments/AppointmentsInfo.inc';
				}
			}
			
			if (isset ( $dayschedules1 [$i] )) {
				for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
					include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
				}
			}
			
			$calendar .= "</div>";
			$calendar .= "</td>";
			
			// Fill the remaining td as empty
			if ($i == $daysInMonth && $td_count > 0) {
				while ( $td_count < 7 ) {
					$calendar .= "<td>&nbsp;</td>";
					$td_count ++;
				}
			}
			
			// Open and close tr properly
			if (date ( 'w', mktime ( 0, 0, 0, $SMonth, $i, $SYear ) ) == 6) {
				$trclosed = true;
				$calendar .= "</tr>";
			}
			if ((date ( 'w', mktime ( 0, 0, 0, $SMonth, $i, $SYear ) ) == 6) && $daysInMonth != $i) {
				$calendar .= "<tr>";
			}
		}
		
		if ($trclosed == false)
			$calendar .= "</tr>";
		
		$calendar .= "</table>";
		$calendar .= "</div>";
		
		// Print the entire calendar
		return $calendar;
	}
	
	/**
	 * @method		defaultTimeZone
	 * @param		$usertimezone
	 * @return		array
	 */
	function defaultTimeZone($usertimezone) {
		$timezones = array (
				"EST" => "America/New_York",
				"CST" => "America/Chicago",
				"MST" => "America/Denver",
				"PST" => "America/Los_Angeles",
				"AKST" => "America/Anchorage",
				"HST" => "America/Adak" 
		);
		$timezones_flip = array_flip ( $timezones );
		return array (
				$timezones,
				$timezones_flip,
				$timezones_flip [$usertimezone] 
		);
	}
	
	/**
	 * @method		calendarListView
	 * @param		$SYear, $SMonth, $SDay
	 * @return		string(calendar in list view)
	 * @tutorial	This method will create a calendar based on given month, year, day.
	 */
	function calendarListView($SYear, $SMonth, $SDay) {
		global $OrgID, $USERID, $USERROLE, $feature, $permit, $IrecruitUsersObj;
		
		$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $USERID, "*" );
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$SMonth, ":SYear"=>$SYear, ":UserID"=>$USERID);
		
		$selreminders = "SELECT SID, DAY(DateTime) as RDay, Subject, AppointmentReminderID,
						  date_format(DateTime,'%h:%i %p') AS RTime, DateTime as ReminderDateTime
						  FROM AppointmentReminder
						  WHERE MONTH(DateTime) = :SMonth 
						  and YEAR(DateTime) = :SYear AND (UserID = :UserID OR UserID = 'MASTER')";
		$resreminders = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $selreminders, array($params) );
		
		// Set parameters for prepared query
		$params = array (":SMonth"=>$SMonth, ":SYear"=>$SYear, ":SIOrgID"=>$OrgID);
		
		$scheduleinterviews = "SELECT SID, SIRecipient, DAY(SIDateTime) as SIDay, SIDateTime as AppointmentDateTime,
							  date_format(SIDateTime,'%h:%i %p') AS SITime, SIApplicationID, SIRequestID
							  FROM ScheduleInterview WHERE MONTH(SIDateTime) = :SMonth 
							  AND YEAR(SIDateTime) = :SYear";
		$scheduleinterviews .= " AND SIOrgID = :SIOrgID";
		
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$params [":SIReqHOwnerUserID"] = $USERID;
			$params [":SIReqHManagerUserID"] = $USERID;
			$params [":SIReqHManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqHOwnerUserID";
			$scheduleinterviews .= " OR ";
			$scheduleinterviews .= " SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqHManagerOrgID AND UserID = :SIReqHManagerUserID)";
			$scheduleinterviews .= ")";
		} // end hiring manager
		if ($USERROLE != 'master_admin' && substr ( $USERROLE, 0, 21 ) != 'master_hiring_manager') {
			$params [":SIReqOwnerUserID"] = $USERID;
			$params [":SIReqManagerUserID"] = $USERID;
			$params [":SIReqManagerOrgID"] = $OrgID;
			
			$scheduleinterviews .= " AND (SIRequisitionOwner = :SIReqOwnerUserID OR SIRequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :SIReqManagerOrgID AND UserID = :SIReqManagerUserID))";
		}
		
		$scheduleinterviews .= " ORDER BY SIDateTime ASC";
		
		$resultinterviews = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $scheduleinterviews, array($params) );
		
		if (is_array ( $resultinterviews ['results'] )) {
			foreach ( $resultinterviews ['results'] as $row ) {
				$dayschedules [$row ['SIDay']] [] = $row;
			}
		}
		
		if (is_array ( $resreminders ['results'] )) {
			foreach ( $resreminders ['results'] as $rowrem ) {
				$dayschedules1 [$rowrem ['RDay']] [] = $rowrem;
			}
		}
		
		$time = strtotime ( date ( "$SYear-$SMonth-$SDay" ) );
		$MonthName = date ( 'F', $time );
		$daysInMonth = cal_days_in_month ( 0, $SMonth, $SYear );
		
		// Calculate Previous and Next month
		$previousMonth = date ( 'Y-m-d', strtotime ( "-1 months", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		$nextMonth = date ( 'Y-m-d', strtotime ( "+1 months", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		
		// Calculate Previous and Next year
		$previousYear = date ( 'Y-m-d', strtotime ( "-1 year", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		$nextYear = date ( 'Y-m-d', strtotime ( "+1 year", strtotime ( date ( "$SYear-$SMonth-$SDay" ) ) ) );
		
		$imgsrcreminder = '<img src="' . IRECRUIT_HOME . 'images/blue-bell-1-16.png">';
		$imgsrcappointment = '<img src="' . IRECRUIT_HOME . 'images/EventReminderSmall.png">';
		
		$div_icon1_class = "cal_listview_div_icon1";
		$div_icon2_class = "cal_listview_div_icon2";
		
		$calendar = "";
		$calendar .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class='table table-bordered table-hover calendar_listview calendar_top_navigation_list'>";
		$calendar .= '<tr>';
		$calendar .= '<th colspan="5" style="text-align:center">';
		$calendar .= '&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("List", "' . $previousYear . '")\'><<</a>&nbsp;&nbsp;&nbsp;&nbsp;'; // Previous Month
		$calendar .= '&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("List", "' . $previousMonth . '")\'><</a>'; // Previous Year
		$calendar .= '&nbsp;&nbsp;&nbsp;' . date ( 'F', $time ) . ' ' . $SYear . '&nbsp;&nbsp;';
		$calendar .= '&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("List", "' . $nextMonth . '")\'>></a>&nbsp;&nbsp;&nbsp;&nbsp;'; // Next Month
		$calendar .= '&nbsp;<a href="javascript:void(0)" onclick=\'loadAppointmentsCalendar("List", "' . $nextYear . '")\'>>></a>'; // Next year
		$calendar .= '</th>';
		$calendar .= '</tr>';
		$calendar .= '<tr>';
		
		for($i = 1; $i <= $daysInMonth; $i ++) {
			// print days
			$eventdayclass = '';
			if (isset ( $dayschedules [$i] ))
				$eventdayclass = ' eventday';
			if (isset ( $dayschedules1 [$i] ))
				$eventdayclass = ' eventday';
			
			if ($i == 1) {
				$td1borderclass = 'td1borderday';
				$td2borderclass = 'td2borderday';
				$td3borderclass = 'td3borderday';
			} else {
				$td1borderclass = 'td1borderdays';
				$td2borderclass = 'td2borderdays';
				$td3borderclass = 'td3borderdays';
			}
			
			$calendar .= "<tr>";
			$calendar .= "<td width=\"15%\" valign='top' align='right' class='$eventdayclass $td1borderclass'>" . date ( 'l', strtotime ( date ( "$SYear-$SMonth-$i" ) ) ) . "</td>";
			$calendar .= "<td width=\"10%\" align='right' valign='top' class='$eventdayclass $td2borderclass'>$i</td>";
			$calendar .= "<td valign='top' align='left' class='calendar-day $eventdayclass $td3borderclass'>";
			
			$flag = false;
			
			if (isset ( $dayschedules [$i] )) {
				for($di = 0; $di < count ( $dayschedules [$i] ); $di ++) {
					
					if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( 0, $dayschedules1 [$i] )) {
						$reset_reminders_array = false;
						for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
							
							if (is_array ( $dayschedules1 [$i] ) && array_key_exists ( $ri, $dayschedules1 [$i] ) && strtotime ( $dayschedules1 [$i] [$ri] ['ReminderDateTime'] ) < strtotime ( $dayschedules [$i] [$di] ['AppointmentDateTime'] )) {
								include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
								
								$reset_reminders_array = true;
								unset ( $dayschedules1 [$i] [$ri] );
							}
						}
						if ($reset_reminders_array == true) {
							$dayschedules1 [$i] = array_values ( $dayschedules1 [$i] );
						}
					}
					
					include IRECRUIT_DIR . 'appointments/AppointmentsInfo.inc';
				}
				$flag = true;
			}
			if (isset ( $dayschedules1 [$i] )) {
				for($ri = 0; $ri < count ( $dayschedules1 [$i] ); $ri ++) {
					include IRECRUIT_DIR . 'appointments/RemindersInfo.inc';
				}
				$flag = true;
			}
			$calendar .= "</td>";
			
			$calendar .= "</tr>";
		}
		$calendar .= "</table>";
		
		// Print the entire calendar
		return $calendar;
	}
	
	/**
	 * @method	insScheduleInterviewInfo
	 * @param	$job_application_info
	 * @return	array
	 */
	function insScheduleInterviewInfo($schedule_interview_info) {
		
		$ins_info = $this->db->buildInsertStatement ( 'ScheduleInterview', $schedule_interview_info );
		$res_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_info ["stmt"], $ins_info ["info"] );
		
		return $res_info;
	}
	
	/**
	 *
	 * @method getScheduledAppointment
	 * @return array
	 * @tutorial This method will schedule appointment
	 */
	function getScheduledAppointment($SID, $columns = "*") {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$params = array (":SID" => $SID);
		$sel_schedule_interview = "SELECT $columns FROM ScheduleInterview WHERE SID = :SID";
		$res_schedule_interview = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_schedule_interview, array (
				$params 
		) );
		
		return $res_schedule_interview;
	}
	
	/**
	 *
	 * @method userAppointmentInformation
	 * @param $appointmentid, $duration        	
	 * @return array
	 */
	function userAppointmentInformation($appointmentid, $duration) {
		
		$params = array(":SID"=>$appointmentid);
		$sel_app_info = "SELECT date_format(SIDateTime,'%Y%m%dT%H%i00') as begindate, DATE(SIDateTime) as SIDate,
						  SIApplicationID, SIOrgID, date_format(DATE_ADD(SIDateTime, INTERVAL $duration MINUTE),'%Y%m%dT%H%i00') as enddate,
						  date_format(SIDateTime,'%H:%i %p') as SITime, SISubject, SILocation, SIDescription, SITimeZone
						  FROM ScheduleInterview
						  WHERE SID = :SID";
		$res_app_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_app_info, array($params) );
		
		return $res_app_info;
	}
	
	/**
	 * @method	getScheduleIntwUserPreferences
	 * @param	$appid
	 * @return	array
	 */
	function getScheduleIntwUserPreferences($appid) {
		
		$params = array(":SID"=>$appid);
		$selappuser = "SELECT UP.* FROM ScheduleInterview S, UserPreferences UP WHERE S.SID = :SID AND UP.UserID = S.SIUserID";
		$rowappuser = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $selappuser, array($params) );
		
		if ($rowappuser ['UserID'] == "" || ! isset ( $rowappuser ['UserID'] ) || empty ( $rowappuser ['UserID'] )) {
			$seldefup = "SELECT * FROM UserPreferences WHERE UserID = 'MASTER'";
			$rowappuser = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $seldefup );
		}
		
		return $rowappuser;
	}
	
	/**
	 * @method	updScheduleInterview
	 * @param	$sidatetime, $sisubject, $silocation, $sidescription, $sid
	 * @return	array
	 */
	function updScheduleInterview($sidatetime, $sisubject, $silocation, $sidescription, $sid) {
		
        $params =   array (
                        ":SIDateTime"       =>  $sidatetime,
                        ":SISubject"        =>  $sisubject,
                        ":SILocation"       =>  $silocation,
                        ":SIDescription"    =>  $sidescription,
                        ":SID"              =>  $sid 
                    );
		$updappointment = "UPDATE ScheduleInterview SET 
							SIDateTime = :SIDateTime,
						    SISubject = :SISubject,
						    SILocation = :SILocation,
						    SIDescription = :SIDescription 
						    WHERE SID = :SID";
		$resappointment = $this->db->getConnection ( "IRECRUIT" )->update ( $updappointment, array (
				$params
		) );
		
		return $resappointment;
	}
	
	
	/**
	 * @method	delScheduleInterview
	 * @param	$sid
	 * @return	array
	 */
	function delScheduleInterview($sid) {
		
		$params = array (":SID" => $sid);
		$delappointment = "DELETE FROM ScheduleInterview WHERE SID = :SID";
		$resappointment = $this->db->getConnection ( "IRECRUIT" )->delete ( $delappointment, array (
				$params
		) );
	
		return $resappointment;
	}
}
