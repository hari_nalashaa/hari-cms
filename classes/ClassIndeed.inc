<?php
/**
 * @class		Indeed
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Indeed {

    var $Token             =   '59422105f9a7972142ec888c5d7031469ae2a62c23d7d830d4ee87163ef4f39b';
    var $Secret            =   'Hm46iGRvTitfqz6TLMmb1fwdP4g7h3EI0jjRv3k8sB9M3UBMVAa8lQRq6Xu7tzqP';
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	
	/**
	 * @method     insUpdIndeedConfig
	 * @param      string $OrgID
	 * @param      string $MultiOrgID
	 * @param      array $indeed_info
	 * @return     array
	 */
	function insUpdIndeedConfig($OrgID, $MultiOrgID, $indeed_info) {
	    
	    //Set parameters
	    $params_info   =   array(
                                ":IOrgID"       =>  $OrgID, 
                                ":IMultiOrgID"  =>  $MultiOrgID, 
                                ":IIndeedApply" =>  $indeed_info['ddlIndeedConfig'],
                                ":IEmail"       =>  $indeed_info['Email'],  
                                ":IPhone"       =>  $indeed_info['Phone'],
                                ":IContactName" =>  $indeed_info['ContactName'],
                                ":UIndeedApply" =>  $indeed_info['ddlIndeedConfig'],
                                ":UEmail"       =>  $indeed_info['Email'],
                                ":UPhone"       =>  $indeed_info['Phone'],
                                ":UContactName" =>  $indeed_info['ContactName'],
	                           );	    
	    // Insert Applicant Information
	    $insert_indeed_info  = "INSERT INTO IndeedConfiguration(OrgID, IndeedApply, MultiOrgID, Email, Phone, ContactName) VALUES(:IOrgID, :IIndeedApply, :IMultiOrgID, :IEmail, :IPhone, :IContactName)";
	    $insert_indeed_info .= " ON DUPLICATE KEY UPDATE IndeedApply = :UIndeedApply, Email = :UEmail, Phone = :UPhone, ContactName = :UContactName";
	    $result_indeed_info  = $this->db->getConnection ( "IRECRUIT" )->insert($insert_indeed_info, array($params_info));
	    
	    return $result_indeed_info;	     
	}
	
	
	/**
	 * @method     getIndeedConfigInfo
	 * @param      string $OrgID
	 * @param      string $MultiOrgID
	 * @param      array $indeed_info
	 * @return     array
	 */
	function getIndeedConfigInfo($OrgID, $MultiOrgID) {

	    $params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	    $sel_info  =   "SELECT * FROM IndeedConfiguration WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	    $res_info  =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_info, array($params) );
	    
	    return $res_info;
	}
	
	
	/**
	 * @method		getIndeedInformation
	 * @return		associative array
	 * @tutorial	This method will fetch the requisition informtaion
	 *          	based on OrgID, MultiOrgID, RequestID.
	 */
	function getIndeedInformation($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_info = "SELECT $columns FROM IndeedFeed";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}

		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
		$res_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_info, $info );
	
		return $res_info;
	}
	
	/**
	 * @method		generateInterviewQuestionsJSONString
	 */
	function generateInterviewQuestionsJSONString($OrgID, $FormID) {
	
	    $AppQueAnswers     =   G::Obj('ApplicationFormQuestions')->getApplicationFormQuestionsInfo($OrgID, $FormID);
        $xmlstart  .=   '[';
	    
        $page_num   =   1;
        foreach ($AppQueAnswers as $SectionID=>$QA)
	    {
	        $xmlstart   .=   '{';
	        $xmlstart   .=   '"id":"page'.$page_num.'_open",';
	        $xmlstart   .=   '"type":"pagebreak"';
	        $xmlstart   .=   '},';
	             
            if($SectionID != "1") {
                if($TITLES[$SectionID] != "") {
                    //Set Information
                    $xmlstart   .=   '{';
                    $xmlstart   .=   '"id": "'.str_replace(".", "", uniqid('', true)).'",';
                    $xmlstart   .=   '"type": "information",';
                    $xmlstart   .=   '"text": "'.strip_tags(str_replace(array("\n", "\r", "\t"), "", $TITLES[$SectionID])).'"';
                    $xmlstart   .=   '},';
                }
            }
    
            $SubSecCount = count($AppQueAnswers[$SectionID]);
    
            for($subseci = 0; $subseci < $SubSecCount; $subseci++) {
    
                $que_section = 0;
                foreach ($QA[$subseci] as $QuestionID=>$QuestionAnswerInfo) {

		            $QuestionAnswerInfo['Question'] = str_replace("'","\'",$QuestionAnswerInfo['Question']);
    
                    if($que_section == 0) {

                        if($subseci == 0) {
                            if($QuestionAnswerInfo['SubSecInstruction'] != "") {
                                //Set Information
                                $xmlstart   .=   '{';
                                $xmlstart   .=   '"id": "'.str_replace(".", "", uniqid('', true)).'",';
                                $xmlstart   .=   '"type": "information",';
                                $xmlstart   .=   '"text": "'.strip_tags(str_replace(array("\n", "\r", "\t"), "", $QuestionAnswerInfo['SubSecInstruction'])).'"';
                                $xmlstart   .=   '},';
                            }
                        }
                         
                        if($QuestionAnswerInfo['SubSecTitle'] != "") {
                                //Set Information
                                $xmlstart   .=   '{';
                                $xmlstart   .=   '"id": "'.str_replace(".", "", uniqid('', true)).'",';
                                $xmlstart   .=   '"type": "information",';
                                $xmlstart   .=   '"text": "'.strip_tags(str_replace(array("\n", "\r", "\t"), "", $QuestionAnswerInfo['SubSecTitle'])).'"';
                                $xmlstart   .=   '},';
                        }

                    }
                     
                    $que_section++;
                         
                    if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
                        if(trim($QuestionAnswerInfo['Question']) != "") {
                            //Text
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "information",';
                            $xmlstart   .=   '"text": "'.strip_tags(str_replace(array("\n", "\r", "\t"), "", $QuestionAnswerInfo['Question'])).'"';
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 5) {
                        if(trim($QuestionAnswerInfo['Question']) != "") {
                            //Phone Question With Extension
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "textarea",';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 13) {
                        if(trim($QuestionAnswerInfo['Question']) != "") {
                            //Phone Question With Extension
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "text",';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 14) {
                        if(trim($QuestionAnswerInfo['Question']) != "") {
                            //Phone Question With Extension
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "text",';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 3) {
                        
                        $val_info    =    $QuestionAnswerInfo['value'];
                        $val_res     =    explode("::", $val_info);

                        $options_info   = '';
                        $options_status = '0';
                        for($k = 0; $k < count($val_res); $k++) {
                            $option_pair   =  explode(":", $val_res[$k]);
                            if($option_pair[0] != "") {
                                $options_info .= '{ "value":"'.$option_pair[0].'", "label": "'.$option_pair[1].'"},';
                                $options_status = '1';
                            }
                        }
                         
                        $options_info = trim($options_info, ",");
                        
                        if(count($val_res) > 0 && $options_status == '1') {
                            //Pull Down
                            $xmlstart   .=   "{";
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "select",';
                            $xmlstart   .=   '"options": [';
                             
                            $xmlstart   .= $options_info;
                             
                            $xmlstart   .=   '],';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags(str_replace(array("<BR>", "<br>"), "\n", $QuestionAnswerInfo['Question'])).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags(str_replace(array("<BR>", "<br>"), "\n", $QuestionAnswerInfo['Question'])).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 6) {
                        //Text
                        $xmlstart   .=   '{';
                        $xmlstart   .=   '"id": "'.$QuestionID.'",';
                        $xmlstart   .=   '"type": "text",';
                         
                        if($QuestionAnswerInfo['Required'] == "Y") {
                            $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                            $xmlstart   .=   '"required": true';
                        }
                        else {
                            $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                        }
                         
                        $xmlstart   .=   '},';
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 17) {
                        //Date
                        $xmlstart   .=   '{';
                        $xmlstart   .=   '"id": "'.$QuestionID.'",';
                        $xmlstart   .=   '"type": "date",';
                         
                        if($QuestionAnswerInfo['Required'] == "Y") {
                            $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                            $xmlstart   .=   '"required": true';
                        }
                        else {
                            $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                        }
                         
                        $xmlstart   .=   '},';
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 18) {
                        
                        $val_info    =    $QuestionAnswerInfo['value'];
                        $val_res     =    explode("::", $val_info);

                        $options_info   = '';
                        $options_status = '0';
                        for($k = 0; $k < count($val_res); $k++) {
                            $option_pair   =  explode(":", $val_res[$k]);
                            if($option_pair[0] != "") {
                                $options_info .= '{ "value":"'.$option_pair[0].'", "label": "'.$option_pair[1].'"},';
                                $options_status = '1';
                            }
                        }
                         
                        $options_info = trim($options_info, ",");
                        
                        if(count($val_res) > 0 && $options_status == '1') {
                            //Checkboxes
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "multiselect",';
                            $xmlstart   .=   '"options": [';
                             
                            $xmlstart   .= $options_info;
                             
                            $xmlstart   .=   '],';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 2) {
                        
                        $val_info    =    $QuestionAnswerInfo['value'];
                        $val_res     =    explode("::", $val_info);
                        
                        $options_info = '';
                        $options_status = '0';
                        for($k = 0; $k < count($val_res); $k++) {
                            $option_pair   =  explode(":", $val_res[$k]);
                            if($option_pair[0] != "") {
                                $options_info  .=   '{ "value":"'.$option_pair[0].'", "label": "'.$option_pair[1].'"},';
                                $options_status = '1';
                            }
                        }
                         
                        $options_info = trim($options_info, ",");
                        
                        if(count($val_res) > 0 && $options_status == '1') {
                            //Select Box
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "select",';
                            $xmlstart   .=   '"options": [';
                             
                            $xmlstart   .= $options_info;
                             
                            $xmlstart   .=   '],';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 8) {
                        if($QuestionAnswerInfo['QuestionID'] != 'resumeupload') {
                            //Attachments
                            $xmlstart   .=   '{';
                            $xmlstart   .=   '"id": "'.$QuestionID.'",';
                            $xmlstart   .=   '"type": "file",';
                             
                            if($QuestionAnswerInfo['Required'] == "Y") {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'",';
                                $xmlstart   .=   '"required": true';
                            }
                            else {
                                $xmlstart   .=   '"question": "'.strip_tags($QuestionAnswerInfo['Question']).'"';
                            }
                             
                            $xmlstart   .=   '},';
                        }
                    }
    
                }
            }
	        
	        
	        $xmlstart   .=   '{';
	        $xmlstart   .=   '"id":"page'.$page_num.'_close",';
	        $xmlstart   .=   '"type":"pagebreak"';
	        $xmlstart   .=   '},';
	        
	        $page_num++;
	    }
	     
	    $xmlstart = trim($xmlstart, ",");
	    
        $xmlstart   .=   ']';
	     
	    return $xmlstart;
	}
	
	
	/**
	 * @method		getPurchasesInfo
	 * @return		associative array
	 */
	function getPurchasesInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_info = "SELECT $columns FROM Purchases";
		if(count($where_info) > 0) {
			$sel_info .= " WHERE " . implode(" AND ", $where_info);
		}
		if($order_by != "") $sel_info .= " ORDER BY " . $order_by;
		
		$res_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc( $sel_info, $info );
		
		return $res_info;
	}
	
	/**
	 * @method     getIndeedPurchaseInformation
	 * @param      string $columns
	 * @param      string $OrgID
	 * @param      string $MultiOrgID
	 * @param      string $RequestID
	 * @return     string
	 */
	function getIndeedPurchaseInformation($columns = "*", $OrgID, $PurchaseNumber) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $params = array(":OrgID"=>$OrgID, ":PurchaseNumber"=>$PurchaseNumber);
	    $sel_info = "SELECT $columns FROM Purchases WHERE OrgID = :OrgID AND PurchaseNumber = :PurchaseNumber";
	    $res_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_info, array($params) );
	    
	    return $res_info;
	}

	/**
	 * @method     getIndeedPurchaseItemsInfo
	 * @param      string $columns
	 * @param      string $OrgID
	 * @param      string $RequestID
	 * @param      string $PurchaseNumber
	 * @return     string
	 */
	function getIndeedPurchaseItemsInfo($columns = "*", $OrgID, $RequestID, $PurchaseNumber) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":PurchaseNumber"=>$PurchaseNumber);
	    $sel_info = "SELECT $columns FROM PurchaseItems WHERE OrgID = :OrgID AND RequestID = :RequestID AND PurchaseNumber = :PurchaseNumber";
	    $res_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_info, array($params) );
	    
	    return $res_info;
	}
	
	/**
	 * @method     getIndeedFeedInformation
	 * @param      string $columns
	 * @param      string $OrgID
	 * @param      string $MultiOrgID
	 * @param      string $RequestID
	 * @return     string
	 */
	function getIndeedFeedInformation($columns = "*", $OrgID, $MultiOrgID, $RequestID) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
	    $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	    $sel_requisition_info = "SELECT $columns FROM IndeedFeed WHERE OrgID = :OrgID AND RequestID = :RequestID";
	    $res_requisition_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_requisition_info, array($params) );
	
	    return $res_requisition_info;
	}
	
	/**
	 * @method		insIndeedPostData
	 * @param		$info
	 * @return 		array
	 */
	public function insIndeedPostData($info) {
	
	    // Insert Applicant Information
	    $insert_post_data = $this->db->buildInsertStatement('IndeedApplicants', $info);
	    $result_post_data = $this->db->getConnection ( "IRECRUIT" )->insert($insert_post_data['stmt'], $insert_post_data['info']);
	
	    return $result_post_data;
	}

	
	/**
	 * @method		getIndeedApplicantInfo
	 * @param		$OrgID, $RequestID, $ApplicationID
	 * @return 		array
	 */
	public function getIndeedApplicantInfo($OrgID, $RequestID, $ApplicationID) {
	
	    // Insert Applicant Information
	    $params_info           =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
	    $sel_applicant_info    =   "SELECT * FROM IndeedApplicants WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
	    $res_applicant_info    =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_applicant_info, array($params_info));
	
	    return $res_applicant_info;
	}
	
	
	/**
	 * @method		getIndeedApplicantsInfo
	 * @param		$info
	 * @return 		array
	 */
	public function getIndeedApplicantsInfo($OrgID, $RequestID, $ApplicationID) {
	
	    // Insert Applicant Information
	    $params_info           =   array(":OrgID"=>$OrgID);
	    $sel_applicant_info    =   "SELECT * FROM IndeedApplicants WHERE OrgID = :OrgID";
	    $res_applicant_info    =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_applicant_info, array($params_info));
	
	    return $res_applicant_info['results'];
	}
	
	
	/**
	 * @method     updIndeedFeedInfo
	 * @param      $set_info, $where_info, $info
	 */
	function updIndeedFeedInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_indeed_info = $this->db->buildUpdateStatement('IndeedFeed', $set_info, $where_info);
		$res_indeed_info = $this->db->getConnection("IRECRUIT")->update($upd_indeed_info, $info);
	
		return $res_indeed_info;
	}
	
	
	/**
	 * @method     getIndeedApplicantStatusLogs
	 * @param      $OrgID
	 */
	function getIndeedApplicantStatusLogs($OrgID) {
	    
	    $params_info_logs      =   array(":OrgID"=>$OrgID);
	    $sel_app_status_logs   =   "SELECT OrgID, ApplicationID, ApplyID, Status, getProcessOrderDescription(OrgID, Status) AS ProcessOrder, substr(concat(DATE_FORMAT(UpdatedDateTime, '%Y-%m-%dT%H:%i:%s'),TIMEDIFF(NOW(), UTC_TIMESTAMP)),-28,25) as DispositionTimeStamp, DATE_FORMAT(DATE(UpdatedDateTime), '%m/%d/%Y') AS LogDate, TIME(UpdatedDateTime) AS LogTime, IsProcessed, Notes FROM ApplicantStatusLogs WHERE OrgID = :OrgID ORDER BY ApplicationID DESC, UpdatedDateTime DESC";
	    $res_app_status_logs   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc($sel_app_status_logs, array($params_info_logs));
	    
	    return $res_app_status_logs;
	}
	
	
	/**
	 * @method	insIndeedFeedInfo
	 * @param	$set_info, $where_info, $info
	 */
	function insIndeedFeedInfo($info) {
	
	    // Insert Applicant Information
	    $params_info       =   array(
	                               ":IOrgID"                   =>  $info['OrgID'], 
	                               ":IMultiOrgID"              =>  $info['MultiOrgID'], 
	                               ":IRequestID"               =>  $info['RequestID'],
	                               ":IPurchaseNumber"          =>  '', 
	                               ":IBudgetTotal"             =>  $info['BudgetTotal'], 
	                               ":IContactName"             =>  $info['ContactName'], 
	                               ":IEmail"                   =>  $info['Email'], 
	                               ":IPhone"                   =>  $info['Phone'], 
	                               ":UBudgetTotal"             =>  $info['BudgetTotal'],
	                               ":ISponsored"               =>  'yes'
                        	    );
	    
	    $insert_feed_info  =   "INSERT INTO IndeedFeed(OrgID, MultiOrgID, RequestID, PurchaseNumber, BudgetTotal, ContactName, Email, Phone, Sponsored, LastModifiedDateTime)";
	    $insert_feed_info .=   " VALUES(:IOrgID, :IMultiOrgID, :IRequestID, :IPurchaseNumber, :IBudgetTotal, :IContactName, :IEmail, :IPhone, :ISponsored, NOW())";
	    $insert_feed_info .=   " ON DUPLICATE KEY UPDATE Sponsored = 'yes', BudgetTotal = :UBudgetTotal, LastModifiedDateTime = NOW()";
	    $result_feed_info  =   $this->db->getConnection ( "IRECRUIT" )->insert($insert_feed_info, array($params_info));
	
	    return $result_feed_info;
	}
}
?>
