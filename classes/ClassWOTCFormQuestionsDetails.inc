<?php
/**
 * @class		WOTCFormQuestionsDetails
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCFormQuestionsDetails {
	
    var $conn_string        =   "WOTC";
    
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection( $this->conn_string );
    }
    
    /**
     * @method      getWOTCFormQuestionsDetailsInfo
     * @param       string $columns
     * @param       string $OrgID
     * @param       string $QuestionID
     * @return      array
     */
    public function getWOTCFormQuestionsDetailsInfo($columns = "*", $WotcID, $WotcFormID, $QuestionID) {
        
        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        
        $params     =   array(":WotcID"=>$WotcID, ":WotcFormID"=>$WotcFormID, ":QuestionID"=>$QuestionID);
        $sel_info   =   "SELECT $columns FROM WotcFormQuestions WHERE WotcID = :WotcID AND WotcFormID = :WotcFormID AND QuestionID = :QuestionID";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
        
        return $res_info;
    }
}
?>
