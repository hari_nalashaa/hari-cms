<?php
/**
 * @class		SovrenParseResume
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class SovrenParseResume {

    use SovrenParameters;
    
    public $db;
    public $parse_client;

    var $conn_string        =   "IRECRUIT";
    
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
    public function __construct() {

        $this->headers[0]   =   "accept: application/xml";  
        $this->headers[1]   =   "content-type: application/json; charset=utf-8";
        
        // parse the resume at Sovren and then put results in DB
        if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com"
            || $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com"
                ) {
            $this->headers[2]   =   "sovren-accountid: 28091668";   //12283437
            $this->headers[3]   =   "sovren-servicekey: 8mMxZQZwwdLKE7dzWouXYseLC2/lrea3JpU8Ky+R";  ///zarWrnLBdOX5i9z6jz8by+uy12UMlHSyRIMKX2p
        }
        else {
            $this->headers[2]   =   "sovren-accountid: 28091668";
            $this->headers[3]   =   "sovren-servicekey: 8mMxZQZwwdLKE7dzWouXYseLC2/lrea3JpU8Ky+R";
        }
        
        $this->db   =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function
    
    /**
     * @method      getParsedResume
     */
    function getParsedResume($columns, $OrgID, $ApplicationID) {
        
        $columns            =   $this->db->arrayToDatabaseQueryString ( $columns );
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
        $sel_parsed_resume  =   "SELECT $columns FROM Parsing WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
        $res_parsed_resume  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_parsed_resume, array($params) );
        
        return $res_parsed_resume;
    }
    
    /**
     * @method      parseResume
     */
    public function parseResume($file) {
        
        $data           =   file_get_contents($file);

        $doc_info       =   array(
                                "DocumentAsBase64String"    =>  base64_encode($data),
                                "OutputHtml"                =>  true,
                                "OutputRtf"                 =>  true,
                                "OutputCandidateImage"      =>  true,
                                "OutputPdf"                 =>  true,
                                "Configuration"             =>  "",
                                "RevisionDate"              =>  "",
                                "SkillsData"                =>  [],
                                "NormalizerData"            =>  "",
                                "GeocodeOptions"            =>  array(
                                                                    "IncludeGeocoding"  =>  false,
                                                                    "Provider"          =>  "",
                                                                    "ProviderKey"       =>  "",
                                                                    "PostalAddress"     =>  array(
                                                                        "CountryCode"   =>  "",
                                                                        "PostalCode"    =>  "",
                                                                        "Region"        =>  "",
                                                                        "Municipality"  =>  "",
                                                                        "AddressLine"   =>  ""
                                                                    ),
                                                                    "GeoCoordinates"    =>  array(
                                                                        "Latitude"      =>  0,
                                                                        "Longitude"     =>  0
                                                                    ),
                                                                ),
                                /*
                                "IndexingOptions"           =>  array(
                                                                    "IndexId"       =>  strtolower($OrgID.'-resumes'),
                                                                    "DocumentId"    =>  strtolower($RequestID."-".$FormID."-".$ApplicationID."-".$OrgID),
                                                                    "CustomIds"     =>  [$OrgID, $RequestID, $FormID, $ApplicationID],
                                                                )
                                                                */
                            );
        
        $data_string    =   json_encode($doc_info);
    
        $url    =   "https://rest.resumeparsing.com/v9/parser/resume";
        $ch     =   curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        $result =   curl_exec($ch);
        curl_close($ch);
    
        if($result === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
    
        return $result;
    }    
}
