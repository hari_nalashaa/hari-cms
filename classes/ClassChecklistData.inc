<?php
/**
 * @class		CheckList
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ChecklistData {

    /**
     * @tutorial	Set the default database connection as irecruit
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }

    /**
     * @method		getCheckListheader
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @return		associative array
     */
    function getCheckListHeader($OrgID, $ApplicationID, $RequestID) {
    
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);    
        $sel_checklist_info =   "SELECT * FROM ChecklistDataHeader WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );

        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListheader
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @return		associative array
     */
    function getCheckListHeaderByChecklistID($OrgID, $ApplicationID, $RequestID, $ChecklistID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":ChecklistID"=>$ChecklistID);
        $sel_checklist_info =   "SELECT * FROM ChecklistDataHeader WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND ChecklistID = :ChecklistID";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListheader
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @return		associative array
     */
    function getCheckListDataByChecklistID($OrgID, $ApplicationID, $RequestID, $ChecklistID, $Checklist) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":ChecklistID"=>$ChecklistID, ":Checklist"=>$Checklist);
        $sel_checklist_info =   "SELECT * FROM ChecklistData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND ChecklistID = :ChecklistID AND Checklist = :Checklist";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListDataHistory
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @return		associative array
     */
    function getCheckListHistory($OrgID, $ApplicationID, $RequestID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        $sel_checklist_info =   "SELECT * FROM ChecklistDataHistory WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID ORDER BY UpdateID desc";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListDataHistory
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @return		associative array
     */
    function getCheckListDataHistory($OrgID, $ApplicationID, $RequestID, $ChecklistID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":ChecklistID"=>$ChecklistID);
        $sel_checklist_info =   "SELECT * FROM ChecklistDataHistory WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND ChecklistID = :ChecklistID";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		getCheckListDataHistory
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @return		associative array
     */
    function getCheckListDataHistoryByUpdateID($OrgID, $ApplicationID, $RequestID, $UpdateID) {
        
        //Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":UpdateID"=>$UpdateID);
        $sel_checklist_info =   "SELECT * FROM ChecklistDataHistory WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND RequestID = :RequestID AND UpdateID = :UpdateID";
        $res_checklist_info   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_checklist_info, array($params) );
        
        return $res_checklist_info;
    }
    
    /**
     * @method		insChecklistHeaderData
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insChecklistHeaderData($OrgID, $ApplicationID, $RequestID, $ChecklistID, $StatusMatch) {
    
        $params_info        =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":ChecklistID"=>$ChecklistID, ":StatusMatch"=>$StatusMatch);
        $ins_statement      =   "INSERT INTO ChecklistDataHeader(OrgID, ApplicationID, RequestID, ChecklistID, LastUpdated, StatusMatch) VALUES(:OrgID, :ApplicationID, :RequestID, :ChecklistID, NOW(), :StatusMatch)";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE LastUpdated = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
    
        return $res_statement;
    }
    
    /**
     * @method		insChecklistHeaderData
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insChecklistData($ChecklistID, $OrgID, $ApplicationID, $RequestID, $Checklist, $StatusCategory, $Comments,$CreatedBy) {
        
        $params_info        =   array(":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":Checklist"=>$Checklist, ":StatusCategory"=>$StatusCategory, ":Comments"=>$Comments,":CreatedBy"=>$CreatedBy);
        $ins_statement      =   "INSERT INTO ChecklistData(ChecklistID, OrgID, ApplicationID, RequestID, Checklist, StatusCategory, Comments, CreatedBy, LastUpdated) VALUES(:ChecklistID,:OrgID,:ApplicationID,:RequestID, :Checklist, :StatusCategory, :Comments, :CreatedBy, NOW())";
        $ins_statement      .=  " ON DUPLICATE KEY UPDATE LastUpdated = NOW()";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
    /**
     * @method		insChecklistHeaderData
     * @param		$OrgID, $ApplicationID, $RequestID, $ChecklistID
     * @tutorial	Here $records_list contains set of rows data that have to insert
     *           	all the values will be passed as in documentation
     */
    function insChecklistDataHistory($ChecklistID, $OrgID, $ApplicationID, $RequestID, $Comments, $CreatedBy, $Checklist) {
        
        $params_info        =   array(":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":Comments"=>$Comments,":UserID"=>$CreatedBy, ":Checklist"=>$Checklist);
        $ins_statement      =   "INSERT INTO ChecklistDataHistory(ChecklistID, OrgID, ApplicationID, RequestID, Comments, UserID, Checklist) VALUES(:ChecklistID,:OrgID,:ApplicationID,:RequestID, :Comments, :UserID, :Checklist)";
        $res_statement      =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_statement, array($params_info) );
        
        return $res_statement;
    }
    
   
    
    /**
     * @method      updChecklistData
     * @param       $set_info, $where_info, $info
     */
    function updChecklistData($set_info = array(), $where_info = array(), $info) {
        
        $upd_checklist_data_info = $this->db->buildUpdateStatement("ChecklistData", $set_info, $where_info);
        $res_checklist_data_info = $this->db->getConnection( $this->conn_string )->update($upd_checklist_data_info, $info);
        
        return $res_checklist_data_info;
    }
    
    /**
     * @method      updChecklistDataHeader
     * @param       $set_info, $where_info, $info
     */
    function updChecklistDataHeader($set_info = array(), $where_info = array(), $info) {
        
        $upd_checklist_data_info = $this->db->buildUpdateStatement("ChecklistDataHeader", $set_info, $where_info);
        $res_checklist_data_info = $this->db->getConnection( $this->conn_string )->update($upd_checklist_data_info, $info);
        
        return $res_checklist_data_info;
    }

    /**
     * @method      getChecklistView
     * @param       $OrgID, $ApplicationID, $RequestID, $ChecklistID
     */
    function getChecklistView($OrgID, $ApplicationID, $RequestID, $ChecklistID) {

    	$checklist_info =   G::Obj('Checklist')->getCheckList($ChecklistID,$OrgID);
    	$checklist_items     =   json_decode($checklist_info['Checklist'], true);
    	$checklist_Status_Lists     =   json_decode($checklist_info['StatusCategory'], true);
    	$STATUSLIST=array();
    	foreach ($checklist_Status_Lists AS $SL) {
            $STATUSLIST[$SL['passed_value']]=$SL['field_name'];
    	}

	$res .= '<h3 style="margin-top:0px;a">Onboard Status</h3>';

	$res .= '<table style="padding:15px;width:100%;border:1px solid #eeeeee;">';
	$res .= '<thead>';
	$res .= '<th style="height:30px;background-color:#eeeeee;text-align:middle;padding-left:10px;">';
	$res .= $checklist_info['ChecklistName'];
	$res .= '</th>';
	$res .= '<th style="height:30px;background-color:#eeeeee;text-align:middle;padding-left:10px;">';
	$res .= 'Date Updated';
	$res .= '</th>';
	$res .= '<th style="height:30px;background-color:#eeeeee;text-align:middle;padding-left:10px;">';
	$res .= 'Updated By';
	$res .= '</th>';
	$res .= '<th style="height:30px;background-color:#eeeeee;text-align:middle;padding-left:10px;">';
	$res .= 'Status';
	$res .= '</th>';
	$res .= '<th style="height:30px;background-color:#eeeeee;text-align:middle;padding-left:10px;">';
	$res .= 'Comment';
	$res .= '</th>';
	$res .= '</thead>';
	$res .- '<tbody>';


    	foreach($checklist_items AS $checklist_item){

            $get_checklist_data =   G::Obj('ChecklistData')->getCheckListDataByChecklistID($OrgID, $ApplicationID, $RequestID, $ChecklistID,$checklist_item['passed_value']);

            $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $get_checklist_data[CreatedBy]);

            $res .= '<tr style="height:20px;text-align:middle;">';
            $res .= '<td style="padding-left:10px;border:1px solid #eeeeee;">'.$checklist_item[ChecklistItem].'</td>';

            if($get_checklist_data[LastUpdated]){
                $res .= '<td style="padding-left:10px;border:1px solid #eeeeee;">'.date("m/d/Y",strtotime($get_checklist_data[LastUpdated])).'</td>';
            }else{
                $res .= '<td style="padding-left:10px;border:1px solid #eeeeee;">&nbsp;&nbsp;-</td>';
            }

            $res .= '<td style="padding-left:10px;border:1px solid #eeeeee;">'.$userName.'</td>';
            $res .= '<td style="padding-left:10px;border:1px solid #eeeeee;">';
            $res .= $STATUSLIST[$get_checklist_data['StatusCategory']];
            $res .= '</td>';
            $res .='<td style="padding-left:10px;border:1px solid #eeeeee;">'.$get_checklist_data[Comments].'</td>';
            $res .='</tr>';

    	}

    	$res .= '</tbody></table>';

	return $res;
    }


    /**
     * @method      getChecklistForm
     * @param       $OrgID, $ApplicationID, $RequestID, $ChecklistID
     */
    function getChecklistForm($OrgID, $ApplicationID, $RequestID, $ChecklistID) {


	$get_checklist_header =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);
	$ChecklistID = $get_checklist_header[ChecklistID];
	$checklist =   G::Obj('Checklist')->getCheckList($ChecklistID,$OrgID);
	$checklist_Steps =   json_decode($checklist['Checklist'], true);
	$checklist_Status_Lists     =   json_decode($checklist['StatusCategory'], true);

	$res = '<div class="checklist_sec" style="margin:30px 0;">';
	$res .= '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover"><thead>';
	$res .= '<th>' . $checklist['ChecklistName'] . '</th><th>Date Updated</th><th>Updated By</th><th>Status</th>';
	$res .= '<th>Comment</th><th>Action</th>';
	$res .= '</thead><tbody>';


	foreach($checklist_Steps AS $checklist_item){

            $get_checklist_data =   G::Obj('ChecklistData')->getCheckListDataByChecklistID($OrgID, $ApplicationID, $RequestID, $ChecklistID,$checklist_item['passed_value']);

            $res .= '<tr><td>'.$checklist_item[ChecklistItem].'</td>';
            if($get_checklist_data[LastUpdated]){
                $res .= '<td>'.date("m/d/Y",strtotime($get_checklist_data[LastUpdated])).'</td>';
            }else{
                $res .= '<td>-</td>';
            }

            $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $get_checklist_data[CreatedBy]);
            $res .= '<td>'.$userName.'</td>';

            $res .= '<td><select name="statusCategory" id="statusCategory">';

	                        foreach($checklist_Status_Lists AS $checklist_Status){
                        if($get_checklist_data['StatusCategory']){
                            if($checklist_Status[passed_value] == $get_checklist_data['StatusCategory']){
                                $res .='<option value="'.$checklist_Status[passed_value].'" Selected=Selected>'.$checklist_Status[field_name].'</option>';
                            }else{
                                $res .='<option value="'.$checklist_Status[passed_value].'">'.$checklist_Status[field_name].'</option>';
                            }
                        }else{
                            if($checklist_Status['default']){
                                $res .='<option value="'.$checklist_Status[passed_value].'" Selected=Selected>'.$checklist_Status[field_name].'</option>';
                            }else{
                                $res .='<option value="'.$checklist_Status[passed_value].'">'.$checklist_Status[field_name].'</option>';
                            }
                        }
                    }
            $res .='</select></td>
            <td><textarea row="6" col="80"  style="height: 40px;width: 300px;">'.$get_checklist_data[Comments].'</textarea></td>';
            $res .='<td><div id="update_checklist_data_'.$checklist_item[passed_value].'" class="btn btn-primary update_checklist_data" style="cursor:pointer;">Update</div></td></tr>';


} // end foreach

        $res .= '</tbody></table>';
        $res .= '<input type="hidden" name="ChecklistID" id="ChecklistID" value="'.$ChecklistID.'" />';
        $res .= '<input type="hidden" name="ApplicationID" id="ApplicationID" value="'.$get_checklist_header[ApplicationID].'" />';
        $res .= '<input type="hidden" name="RequestID" id="RequestID" value="'.$get_checklist_header[RequestID].'" />';
        $res .= '<input type="hidden" name="OrgID" id="OrgID" value="'.$get_checklist_header[OrgID].'" />';

	$res .= '</div>';

	return $res;
    }

}
?>
