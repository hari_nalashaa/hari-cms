<?php
/**
 * @class		SovrenParseTemp
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class SovrenParseTemp {

    use SovrenParameters;
    
    public $db;

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
    public function __construct() {
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method      getParsedTempResumeInfo
     * @param       string $OrgID
     * @param       string $UserID
     * @param       string $RequestID
     * @return      array
     */
    public function getParsedTempResumeInfo($OrgID, $HoldID) {
        // take the data extracted and prepopulate the application form
        $APPDATA    =   array();
        
        $params     =   array(':OrgID'=>$OrgID, ':HoldID'=>$HoldID);
        $query      =   "SELECT * FROM ParsingTemp WHERE OrgID = :OrgID AND HoldID = :HoldID";
        $DATA       =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
        
        return $DATA;
        
    } // end function
    
    /**
     * @method      processTempResumeForm
     * @param       string $OrgID
     * @param       string $APPID
     * @param       string $FILE
     */
    public function processTempResumeForm($OrgID, $FormID, $RequestID, $APPID, $FILE) {
        
        //Get Purpose names list
        $purpose    =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);
        
        // parse the resume at Sovren and then put results in DB
        if($_SERVER['SERVER_NAME'] == "dev.irecruit-us.com"
            || $_SERVER['SERVER_NAME'] == "quality.irecruit-us.com"
            ) {
                list ($xml, $text, $rtf, $html, $credits, $fileext) = G::Obj('Sovren')->parseResume($FILE);
            }
            else {
                list ($xml, $text, $rtf, $html, $credits, $fileext) = G::Obj('Sovren')->parseResume($FILE);
            }
            
            $apatdir    =	IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
            $filename   =	$apatdir . '/' . $APPID . '-' . $purpose ['resumeupload'] . '.' . $fileext;
            
            $query      =   "SELECT ApplicationID FROM Parsing WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
            $params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$APPID);
            $result     =   $this->db->getConnection( $this->conn_string )->fetchAssoc($query, array($params));
            
            if($xml) {
                
                //Applicant Attachments Information
                $applicant_attach_info   =   array(
                    "OrgID"            =>  $OrgID,
                    "MultiOrgID"       =>  $MultiOrgID,
                    "HoldID"           =>  $APPID,
                    "SectionID"        =>  6,
                    "RequestID"        =>  $RequestID,
                    "TypeAttachment"   =>  'resumeupload',
                    "PurposeName"      =>  $purpose ['resumeupload'],
                    "FileType"         =>  $fileext
                );
                //Insert applicant attachments information
                G::Obj('ApplicantDataTemp')->insApplicantAttachmentsTemp ( $applicant_attach_info );
                
                $query      =   "INSERT INTO Parsing (OrgID, ApplicationID, XML, HTML, RTF, `TEXT`, FILEEXT, CreatedUpdatedDate) VALUES (:OrgID, :ApplicationID, :XML, :HTML, :RTF, :TEXT, :FILEEXT, NOW())";
                $query      .=  " ON DUPLICATE KEY UPDATE XML = :UXML, HTML = :UHTML, RTF = :URTF, `TEXT` = :UTEXT, FILEEXT = :UFILEEXT, CreatedUpdatedDate = NOW()";
                $params     =   array(
                    ":OrgID"            =>  $OrgID,
                    ":ApplicationID"    =>  $APPID,
                    ":XML"              =>  $xml,
                    ":HTML"             =>  $html,
                    ":RTF"              =>  $rtf,
                    ":TEXT"             =>  $text,
                    ":FILEEXT"          =>  $fileext,
                    ":UXML"             =>  $xml,
                    ":UHTML"            =>  $html,
                    ":URTF"             =>  $rtf,
                    ":UTEXT"            =>  $text,
                    ":UFILEEXT"         =>  $fileext
                );
                $res_org    =   G::Obj('GenericQueries')->insInfoByQuery($query, array($params));
                
                $file_to_log    =   ROOT."logs/resume-parsing/". $OrgID . "-" . $APPID . "-" . "resume.xml";
                $path_parts     =   pathinfo($file_to_log);
                
                if(!is_dir($path_parts['dirname'])) {
                    //Directory does not exist, so lets create it.
                    mkdir($path_parts['dirname'], 0777);
                    chmod($path_parts['dirname'], 0777);
                }
                
                $fp = fopen($file_to_log, 'w');
                fwrite($fp, $xml);
                fclose($fp);
                chmod($file_to_log, 0666);
                
                if(!file_exists($filename)) {
                    $data  =   file_get_contents ( $FILE );
                    $fp    =   fopen($filename, 'w');
                    fwrite($fp, $data);
                    fclose($fp);
                    chmod($filename, 0666);
                }
                
                if (($credits > 975) && ($credits < 1000)) {
                    
                    $message    =   "Credit count is: " . $credits;
                    $to         =   "dedgecomb@irecruit-software.com";
                    $subject    =   "Sovren Credits are LOW";
                    
                    //Clear properties
                    G::Obj('PHPMailer')->clearCustomProperties();
                    G::Obj('PHPMailer')->clearCustomHeaders();
                    
                    // Set who the message is to be sent to
                    G::Obj('PHPMailer')->addAddress ( $to );
                    // Set the subject line
                    G::Obj('PHPMailer')->Subject = $subject;
                    // convert HTML into a basic plain-text alternative body
                    G::Obj('PHPMailer')->msgHTML ( $message );
                    // Content Type Is HTML
                    G::Obj('PHPMailer')->ContentType = 'text/plain';
                    //Send email
                    G::Obj('PHPMailer')->send ();
                }
                
            }
    } // end function
    
} // end Class
