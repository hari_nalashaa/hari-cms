<?php
/**
 * @class		FormSections
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormSections {
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getAllTextBlockSections
	 * @date		January 20 2016
	 */
	public function getAllTextBlockSections() {
		
		$sel_txtblock_sec = "SELECT TextBlockID, Section FROM TextBlockSections ORDER BY SortOrder";
		$res_txtblock_sec = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_txtblock_sec);
	
		return $res_txtblock_sec;
	}
	
	/**
	 * @method		getTextBlockSectionsInfo
	 * @date		January 20 2016
	 */
	public function getTextBlockSectionInfoByID($params) {
		
		$sel_txtblock_sec = "SELECT * FROM TextBlockSections WHERE TextBlockID = :TextBlockID";
		$res_txtblock_sec = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc($sel_txtblock_sec, $params);
	
		return $res_txtblock_sec;
	}
	
	/**
	 * @method		getSectionsInfoByCategory
	 * 
	 */
	function getSectionsInfoByCategory($section_table, $columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_frm_sec_info = "SELECT $columns FROM $section_table";
		$where = "";
		if(count($where_info) > 0)
		{
			$where	= " WHERE " . implode(" AND ", $where_info);
		}
	
		$sel_frm_sec_info .= $where;
		if($order_by != "") $sel_frm_sec_info .= " ORDER BY " . $order_by;
	
		$res_frm_sec_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_frm_sec_info, $info );
	
		return $res_frm_sec_info;
	}
	
}
