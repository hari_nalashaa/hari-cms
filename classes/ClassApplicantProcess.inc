<?php
/**
 * @class		ApplicantProcess
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantProcess {

	var $conn_string        =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method     setHoldID
	 */
	public function setHoldID($OrgID, $MultiOrgID) {
		$CurrentDateTime	=   G::Obj('MysqlHelper')->getDateTime('%Y%m%d%H%m%s');
		$HoldID				=   uniqid ( $CurrentDateTime );

		return $HoldID;
	} // end function


	/**
	 * @method     unsetHoldID
	 */
	public function unsetHoldID() {
		//To-Do
	} // end function
	
} // end class
?>
