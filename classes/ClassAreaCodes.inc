<?php 
/**
 * @class		AreaCodes
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class AreaCodes {
	
	var $conn_string       	=   "IRECRUIT";

	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getAreaCodesByCountryCode
	 */
	function getAreaCodesByCountryCode($country_code, $order_by = '') {
		
		$params_info       	=   	array(":CountryCode"=>$country_code);
		$sel_area_codes		=   	"SELECT * FROM AreaCodes WHERE CountryCode = :CountryCode";
		if($order_by != "") $sel_area_codes  .=	" ORDER BY ID";
		$res_app_att_info  	=   	$this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_area_codes, array($params_info) );
		
		return $res_app_att_info;
	}
}
?>
