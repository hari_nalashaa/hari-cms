<?php
/**
 * @class		GenericLibrary
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class GenericLibrary {

    public $db;
    
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
	/**
	 * @method		getGlobalUniqueID
	 * @return		value
	 */
	public function getGlobalUniqueID($OrgID) {

		$guid = $this->getGuIDV4().uniqid($OrgID.microtime(), true);
		
		return str_replace(array(" ", ".", "-", "/"), "", $guid);
	}
	
	/**
	 * @method		getGuIDV4
	 * @return		string
	 */
	public function getGuIDV4() {
		$data = openssl_random_pseudo_bytes(16);
		$data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
		$data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
		return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
	}
	
	/**
	 * @method		getGuIDBySalt
	 */
	public function getGuIDBySalt($OrgID, $Salt1 = '', $Salt2 = '', $Salt3 = '', $Salt4 = '') {

		$guid = uniqid($OrgID.microtime(), true);
		
		if($Salt1 != "") $guid .= $Salt1;
		if($Salt2 != "") $guid .= $Salt2;
		if($Salt3 != "") $guid .= $Salt3;
		if($Salt4 != "") $guid .= $Salt4;
		
		$guid = strtolower($guid);
		
		return str_replace(array(" ", ".", "-", "/"), "", $guid);
	}
	
	/**
	 * @method     getAlphabets
	 * @return     array
	 */
	public function getAlphabets() {
		return range("A", "Z");
	}

	/**
	 * @method     getAlphabetsKeyFrom1
	 */
	public function getAlphabetsKeyFrom1() {
	    $a = range('A', 'Z');
	    array_unshift($a,"");
	    unset($a[0]);
	    return $a;
	}
	
	/**
	 * @method     getMonths
	 * @return     array
	 */
	public function getMonths() {
	    $months = array (
                		"1"   =>  "January",
                		"2"   =>  "February",
                		"3"   =>  "March",
                		"4"   =>  "April",
                		"5"   =>  "May",
                		"6"   =>  "June",
                		"7"   =>  "July",
                		"8"   =>  "August",
                		"9"   =>  "September",
                		"10"  =>  "October",
                		"11"  =>  "November",
                		"12"  =>  "December" 
                        );
	    
	    return $months;
	}
	
	/**
	 * @method     isJSON
	 * @param      $string
	 * @return     boolean
	 */
	function isJSON($string) {
	    return is_string($string) && is_array(@json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	/**
	 * @method		generateRandomSessionKey
	 * @tutorial	generate the random session key
	 * @return		string(random)
	 */
	function generateRandomSessionKey() {
		
		$sel_date = "SELECT DATE_FORMAT(NOW(),'%Y%m%d%H%m%s')";
		list ( $SESS ) = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_date );
		return uniqid ( $SESS );
	}
	
}
