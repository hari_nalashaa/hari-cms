<?php
/**
 * @class		TwilioWebHookConversation
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */


use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioWebHookConversation {

    public $db;
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	var $twilio_number			=	"+16572208043";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
        
        
    } // end function


    /**
     * @method		getWebHookConversationInfo
     */
    public function getWebHookConversationInfo() {

    	global $OrgID;
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {

			$webhook	=	$client->conversations->v1->webhooks()->fetch();
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-twilio-web-hook-info.txt", serialize($webhook), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully got the webhook information", "Response"=>serialize($webhook));
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-twilio-web-hook-info-error.txt", $e->getCode() . ' : ' . $e->getMessage(), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to get the webhook information", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    
    /**
     * @method		enableAllWebHooks
     */
    public function enableAllWebHooks() {

    	global $OrgID;
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    
    		$webhook = $client->conversations->v1->webhooks()
                                     ->update(array(
                                                  "postWebhookUrl" => "https://dev.irecruit-us.com/surya/irecruit/twilio/conversations/preWebHookConversation.php",
                                                  "preWebhookUrl" => "https://dev.irecruit-us.com/surya/irecruit/twilio/conversations/preWebHookConversation.php",
                                                  "method" => "POST"
                                              )
                                     );
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-enable-twilio-web-hook.txt", serialize($webhook), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully enabled webhook", "Response"=>serialize($webhook));
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-twilio-enable-web-hook-info-error.txt", $e->getCode() . ' : ' . $e->getMessage(), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to get the webhook information", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    
    }
} // end Class
