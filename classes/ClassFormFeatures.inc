<?php
/**
 * @class		FormFeatures
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormFeatures {
	
	/**
	 *
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getInternationalTranslation
	 * @param		string $OrgID        	
	 * @return		associative array
	 */
	function getInternationalTranslation($OrgID) {
		
		//Set parameters for prepared query
		$params   =   array(":OrgID"=>$OrgID);
		// escape input values from sql injection
		$sel_international_translation = "SELECT InternationalTranslation FROM ApplicationFeatures WHERE OrgID = :OrgID";
		$row_international_translation = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_international_translation, array($params));
		
		return $row_international_translation;
	}
	
	/**
	 * @method		insDefaultTextBlocks
	 * @param		$OrgID, $new_name, $form
	 * @date		January 20 2016		
	 */
	function insDefaultTextBlocks($OrgID, $new_name, $form) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":FormID"=>$form);
		
		$sel_frm_cert_sec = "SELECT Text FROM TextBlocks WHERE OrgID = :OrgID AND FormID = :FormID";
		$row_frm_cert_sec = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_frm_cert_sec, array($params));
		
        $ins_def_text_blocks    =   "INSERT INTO TextBlocks (OrgID, FormID, TextBlockID, Text) 
                                    SELECT '" . $OrgID . "', '" . $new_name . "', TextBlockID, Text FROM TextBlocks 
                                    WHERE OrgID = :OrgID AND FormID = :FormID ON DUPLICATE KEY UPDATE Text = :UText";
        $params[':UText']       =   $row_frm_cert_sec['Text'];
        $res_def_text_blocks    =   $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_def_text_blocks, array($params));
		
		return $res_def_text_blocks;
	}
	
	/**
	 * @method		insDefaultTextBlocks
	 * @param		$OrgID, $new_name, $form
	 * @date		January 20 2016
	 */
	function insTextBlocksUpdateOnDuplicate($info) {
		
		$ins_text_blocks = "INSERT INTO TextBlocks(OrgID, FormID, TextBlockID, Text) 
							VALUES (:OrgID, :FormID, :TextBlockID, :IText) ON 
							DUPLICATE KEY UPDATE Text = :UText";
		$res_text_blocks = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_text_blocks, $info );
	
		return $res_text_blocks;
	}
	
	/**
	 * @method		delFormCreationInfo
	 */
	function delFormCreationInfo($table_name, $OrgID, $FormID) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
		
		$del_frm_cre_info = "DELETE FROM $table_name WHERE OrgID = :OrgID AND FormID = :FormID";
		$res_frm_cre_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_frm_cre_info, array($params));
		
		return  $res_frm_cre_info;
	}
	
	/**
	 * @method		insMasterTextBlocks
	 * @param		$OrgID, $new_name, $form
	 * @date		January 20 2016
	 */
	function insMasterTextBlocks($OrgID) {
		
		$del_text_blocks = "DELETE FROM TextBlocks WHERE OrgID = '".$OrgID."'";
		$res_text_blocks = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_text_blocks );
		
		$ins_text_blocks = "INSERT INTO TextBlocks (OrgID, FormID, TextBlockID, Text) 
							SELECT '" . $OrgID . "', FormID, TextBlockID, Text 
							FROM TextBlocks WHERE OrgID = 'MASTER'";
		$res_text_blocks = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_text_blocks );
	
		return $res_def_text_blocks;
	}
	
	/**
	 * @method		getTextFromTextBlocks
	 * @param		$OrgID, $FormID, $TextBlockID
	 * @return		associative array
	 */
	function getTextFromTextBlocks($OrgID, $FormID, $TextBlockID) {
		
		//Set parameters for prepared query
		$params 	=    array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":TextBlockID"=>$TextBlockID);
        $sel_text   =   "SELECT Text FROM TextBlocks WHERE OrgID = :OrgID AND FormID = :FormID AND TextBlockID = :TextBlockID";
        $row_text   =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_text, array($params));
	
		return $row_text['Text'];
	}
	
	/**
	 * @method		getTextBlocksInfo
	 * @param		$OrgID, $FormID
	 * @date		January 20 2016
	 */
	function getTextBlocksInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_text_blocks_info = "SELECT $columns FROM TextBlocks";

		if(count($where_info) > 0) $sel_text_blocks_info .= " WHERE " . implode(" AND ", $where_info);
		if($order_by != "") $sel_text_blocks_info .= " ORDER BY " . $order_by;
		
		$res_frm_sec_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_text_blocks_info, $info );
		
		return $res_frm_sec_info;
	}
	
	/**
	 * @method		getTextBlocksInfoByFormID
	 * @param		$OrgID, $FormID
	 */
	function getTextBlocksInfoByFormID($OrgID, $FormID) {
	    
	    $params_info       =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
	    $sel_text_blocks   =   "SELECT * FROM TextBlocks WHERE OrgID = :OrgID AND FormID = :FormID";
	    $res_text_blocks   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_text_blocks, array($params_info) );
	    
	    $text_blocks_list  =   array();
	    foreach($res_text_blocks['results'] as $text_block_info) {
	        $text_blocks_list[$text_block_info['TextBlockID']]  =   $text_block_info;
	    }
	    
	    return $text_blocks_list;
	}
	
	/**
	 * @method		updTextBlocks
	 * @param		$OrgID, $FormID, $TextBlockID, $Text
	 * @date		January 20 2016
	 */
	function updTextBlocks($OrgID, $FormID, $TextBlockID, $Text) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":TextBlockID"=>$TextBlockID, ":Text"=>$Text);
	
		$upd_text_blocks = "UPDATE TextBlocks SET Text = :Text WHERE OrgID = :OrgID AND FormID = :FormID AND TextBlockID = :TextBlockID";
		$res_text_blocks = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_text_blocks, array($params));
	
		return  $res_text_blocks;
	}
	
	/**
	 * @method		getAutoForwardList
	 * @param		$OrgID
	 * @return		array
	 */
	function getAutoForwardList($OrgID, $order_by = "") {
		
		$params = array(':OrgID'=>$OrgID);
		$sel_auto_forward_list = "SELECT * FROM AutoForwardList WHERE OrgID = :OrgID";
		if($order_by != "") $sel_auto_forward_list .= " ORDER BY ".$order_by;
		$res_auto_forward_list = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_auto_forward_list, array($params));
		
		return $res_auto_forward_list;
	}
	
	/**
	 * @method		insAutoForwardListInfo
	 * @param		array $info
	 * @return		array
	 */
	function insAutoForwardListInfo($info) {
		
		$ins_auto_forward_list_info = $this->db->buildInsertStatement('AutoForwardList', $info);
		$ins_auto_forward_stmt = $ins_auto_forward_list_info["stmt"] . " ON DUPLICATE KEY UPDATE FirstName = :UFirstName";
		$ins_params = $ins_auto_forward_list_info["info"];
		$ins_params[0][':UFirstName'] = $ins_params[0][':FirstName'];
		
		$res_auto_forward_list_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_auto_forward_stmt, $ins_params );
	
		return $res_auto_forward_list_info;
	}
	
	/**
	 * @method		delAutoForwardListInfo
	 * @param		$where_info, $info
	 */
	function delAutoForwardListInfo($where_info = array(), $info = array()) {
		
		$del_auto_forward_list_info = "DELETE FROM AutoForwardList";
		if (count ( $where_info ) > 0) $del_auto_forward_list_info .= " WHERE " . implode ( " AND ", $where_info );
		$res_auto_forward_list_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_auto_forward_list_info, $info );
	
		return $res_auto_forward_list_info;
	}
	
	/**
	 * @method     getChangeStatusOptionsByFormType
	 */
	function getChangeStatusOptionsByFormType($OrgID, $FormType, $FormID) {
	    
	    global $feature, $FormsInternalObj;

	    $form_options = array();
        if($FormType ==  "PreFilledForm" && $feature ['PreFilledForms'] == "Y") {
            //set where condition
            $where = array("OrgID = :OrgID", "PreFilledFormID = :PreFilledFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$FormID);
            //Get PrefilledForms Information
            $results = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, Form", array($params));
            $PFF = $results['results'][0];
            
            $form_options[] = "InternalStaff";
            $Restrict = "";
            
            if ($PFF ['PreFilledFormID'] == "FE-I9m") {
                $Restrict = "Primary";
            }
            if ($PFF ['PreFilledFormID'] == "FE-I9m-2020") {
                $Restrict = "Primary";
            }
            
            $display_option = "true";
            if($Restrict == "Primary") {
                $display_option = "false";
            }
            
            if($display_option == "true") {
                $form_options[] = "Applicant";
            }
        }
        else if($FormType ==  "AgreementForm" && $feature ['AgreementForms'] == "Y") {
            //set where condition
            $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$FormID);
            // Agreement Forms
            $results = $FormsInternalObj->getAgreementFormsInfo("*", $where, "FormName", array($params));
            $AFN = $results['results'][0];
            
            $form_options[] = "InternalStaff";
            $Restrict = "";
            
            $Restrict = $AFN ['FormPart'];
            	
            $display_option = "true";
            if($Restrict == "Primary") {
                $display_option = "false";
            }

            if($display_option == "true") {
                $form_options[] = "Applicant";
            }
        }
        else if($FormType ==  "WebForm" && $feature ['WebForms'] == "Y") {
            //set where condition
            $where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":WebFormID"=>$FormID);
            //Get WebForms Information
            $results = $FormsInternalObj->getWebFormsInfo("*", $where, "FormName", array($params));
            $WFQNT = $results['results'][0];
            
            $form_options[] = "InternalStaff";
            $Restrict = "";
             
            $display_option = "true";
            if($Restrict == "Primary") {
                $display_option = "false";
            }
            if($display_option == "true") {
                $form_options[] = "Applicant";
            }
        }
        else if($FormType ==  "SpecificationForm" && $feature ['SpecificationForms'] == "Y") {
            //set where condition
            $where = array("OrgID = :OrgID", "SpecificationFormID = :SpecificationFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":SpecificationFormID"=>$FormID);
            //get specification forms
            $results = $FormsInternalObj->getSpecificationForms("SpecificationFormID, DisplayTitle, FormType", $where, "FormType, DisplayTitle", array($params));
            
            $form_options[] = "InternalStaff";
            
            $Restrict = "";
            	
            $display_option = "true";
            if($Restrict == "Primary") {
                $display_option = "false";
            }
            	
            if($display_option == "true") {
                $form_options[] = "Applicant";
            }
        }
        
        return $form_options;
	}
}
