<?php
/**
 * @class		UserPortalUsers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class UserPortalUsers {
	
	/**
	 * @tutorial	Constructor to load the default database
	 *          	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "USERPORTAL" );
	}
	
	/**
	 * @method		getUserPortalUserInfo
	 * @return		numeric array
	 */
	function getUserPortalUserInfo() {
	    
	    if(!isset($_COOKIE ['PID'])) {
	        return "";
	    }
	    else {
	        $columns   =   "UserID, OrgID, MultiOrgID, cast(Persist as unsigned int) as CastPersist, ProfileAvatarPicture, DefaultProfile, EmailVerified, Email, AdminStatus";
	        $sel_userportal_userinfo = "SELECT $columns FROM Users WHERE SessionID = '" . $_COOKIE ['PID'] . "'";
	        $row_userportal_userinfo = $this->db->getConnection ( "USERPORTAL" )->fetchAssoc($sel_userportal_userinfo);
	        
	        return $row_userportal_userinfo;
	    }
	}
	

	/**
	 * @method		updPassword
	 * @param 		sessionid
	 * @return 		associate array
	 */
	function updPassword($SessionID, $Password) {
	
	    // Set parameters for prepared query
        $params             =   array (":SessionID"=>$SessionID, ":Verification"=>$Password);
        $upd_forgot_pass    =   "UPDATE Users SET Verification = :Verification WHERE SessionID = :SessionID";
        $res_forgot_pass    =   $this->db->getConnection ( "USERPORTAL" )->update ( $upd_forgot_pass, array ( $params ) );
	
	    return $res_forgot_pass;
	}
	
	
	/**
	 * @method		updateMultiOrgIdByOrgIdAndUserId
	 * @category	UserPortal Users
	 * @param 		$LocMultiOrgID, $LocOrgID, $USERID
	 * @return		integer
	 */
	function updateMultiOrgIdByOrgIdAndUserId($LocMultiOrgID, $LocOrgID, $USERID) {
	
		$upd_users  = "UPDATE Users SET MultiOrgID = '".$LocMultiOrgID."'";
		$upd_users .= " WHERE OrgID = '".$LocOrgID."' AND UserID = '".$USERID."'";
	
		$res_users  = $this->db->getConnection ( "USERPORTAL" )->update($upd_users);
	
		return $res_users['affected_rows'];
	}
	
	/**
	 * @method		updateUserProfileAvatarPhoto
	 * @category	UserPortal Users
	 * @param		$AvatarProfilePhoto, $USERID
	 * @return		integer
	 */
	function updateUserProfileAvatarPhoto($AvatarProfilePhoto, $USERID) {
	
		$upd_user_profile_avatar  = "UPDATE Users SET ProfileAvatarPicture = '".$AvatarProfilePhoto."'";
		$upd_user_profile_avatar .= " WHERE UserID = '".$USERID."'";
	
		$res_user_profile_avatar  = $this->db->getConnection ( "USERPORTAL" )->update($upd_user_profile_avatar);
	
		return $res_user_profile_avatar['affected_rows'];
	}
	
	/**
	 * @method		updateLastAccess
	 */
	function updateUserPortalUserLastAccess() {
		global $USERID;
	
		$update_last_access_key = "UPDATE Users SET LastAccess = NOW() WHERE UserID = '" . $USERID . "'";
		$result_last_access_key = $this->db->getConnection ( "USERPORTAL" )->update($update_last_access_key);
	
		return $result_last_access_key['affected_rows'];
	}
	
	/**
	 * @method		updateUsersSessionID
	 * @category	UserPortal Users
	 * @return		integer
	 */
	function updateUsersSessionID() {
		/*	
		$update_users_sessionid = "UPDATE Users U SET U.SessionID = ''";
		$update_users_sessionid .= " WHERE U.SessionID != ''";
		$update_users_sessionid .= " AND U.LastAccess <= DATE_SUB(NOW(), INTERVAL U.Minutes MINUTE)";
		$update_users_sessionid .= " AND (U.Persist IS NULL OR U.Persist = 0)";
		$result_users_sessionid = $this->db->getConnection ( "USERPORTAL" )->update($update_users_sessionid);
	
		return $result_users_sessionid['affected_rows'];
	        */
	}
	
	/**
	 * @method		getUsersInformation
	 * @param 		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial 	This method will fetch the list of users details based on condition
	 */
	function getUsersInformation($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_user_info = "SELECT $columns FROM Users";
	
		if (count ( $where_info ) > 0) $sel_user_info .= " WHERE " . implode ( " AND ", $where_info );
	
		if($group_by != "") $sel_user_info .= " GROUP BY " . $group_by;
		if ($order_by != "") $sel_user_info .= " ORDER BY " . $order_by;
		
		$res_user_info = $this->db->getConnection ( "USERPORTAL" )->fetchAllAssoc ( $sel_user_info, $info );
	
		return $res_user_info;
	}
	
	/**
	 * @method		getProfileDataInfo
	 * @param 		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial 	This method will fetch the list of users details based on condition
	 */
	function getProfileDataInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_profiles = "SELECT $columns FROM ProfileData";
	
		if (count ( $where_info ) > 0) $sel_profiles .= " WHERE " . implode ( " AND ", $where_info );
	
		if ($order_by != "") $sel_profiles .= " ORDER BY " . $order_by;
		$res_profiles = $this->db->getConnection ( "USERPORTAL" )->fetchAllAssoc ( $sel_profiles, $info );
	
		return $res_profiles;
	}
	
	
	/**
	 * @method		insUsers
	 * @param		array $info
	 * @return		associative array
	 */
	function insUsers($info) {
	
		$ins_users = $this->db->buildInsertStatement('Users', $info);
		$res_users = $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_users["stmt"], $ins_users["info"] );
	
		return $res_users;
	}
	
	/**
	 * @method		insUsers
	 * @param		array $info
	 * @return		associative array
	 */
	function updUsersSessionID($session_id = '', $USERID) {
	
		//Set parameters
		$params  = array(":SessionID"=>$session_id, ":UserID"=>$USERID);
		$upd_users_session = "UPDATE Users SET SessionID = :SessionID, Persist = NULL where UserID = :UserID";
		$res_users_session = $this->db->getConnection ( "USERPORTAL" )->update ( $upd_users_session, array($params) );
	
		return $res_users_session;
	}
	
	/**
	 * @method		updUsersInfo
	 * @param		$set_info = array(), $where_info = array(), $info
	 */
	public function updUsersInfo($set_info = array(), $where_info = array(), $info) {
	
		$upd_users_info = $this->db->buildUpdateStatement('Users', $set_info, $where_info);
		$res_users_info = $this->db->getConnection ( "USERPORTAL" )->update($upd_users_info, $info);
	
		return $res_users_info;
	}
	
	
	/**
	 * @method		insProfileData
	 * @param		array $info
	 * @return		associative array
	 */
	function insProfileData($info) {

		$params       =   array(":UserID"=>$info['UserID'], ":ProfileID"=>$info['ProfileID'], ":QuestionID"=>$info['QuestionID'], ":Answer"=>$info['Answer'], ":UAnswer"=>$info['Answer']);
		$ins_profiles =   "INSERT INTO ProfileData(UserID, ProfileID, QuestionID, Answer) VALUES(:UserID, :ProfileID, :QuestionID, :Answer) ON DUPLICATE KEY UPDATE Answer = :UAnswer";
		$res_profiles =   $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_profiles, array($params));
	
		return $res_profiles;
	}
	
	/**
	 * @method		insProfilesDataByUserInfo
	 * @param		array $info
	 * @return		associative array
	 */
	function insProfilesDataByUserInfo($profileid, $USERID, $original) {
	
		$ins_profiles = "INSERT INTO ProfileData (UserID, ProfileID, QuestionID, Answer) 
						SELECT UserID, '" . $profileid . "', QuestionID, Answer 
						FROM ProfileData 
						WHERE UserID = '" . $USERID . "' and ProfileID = '" . $original . "'";
		$res_profiles = $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_profiles );
	
		return $res_profiles;
	}
	
	/**
	 * @method		updProfileData
	 * @param		array $info
	 * @return		associative array
	 */
	function updProfileData($profileid, $USERID, $original) {
	
		$upd_profiles = "UPDATE ProfileData SET ProfileID = '".$profileid."' WHERE UserID = '".$USERID."' AND ProfileID = '".$original."'";
		$res_profiles = $this->db->getConnection ( "USERPORTAL" )->update ( $upd_profiles );
	
		return $res_profiles;
	}

	/**
	 * @method		getUserDetailInfoByUser
	 * @param 		$columns, $user_name
	 * @return		associative array
	 * @tutorial 	This method will fetch the user detail based on username
	 */
	function getUserDetailInfoByEmail($columns = "*", $email) {
	
		//set parameters
		$params = array(":Email"=>$email);
		//set columns
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		//select userinformation
		$sel_user_info = "SELECT $columns FROM Users WHERE Email = :Email";
		$res_user_info = $this->db->getConnection ( "USERPORTAL" )->fetchAssoc ( $sel_user_info, array($params) );
	
		return $res_user_info;
	}
	
	/**
	 * @method		getUserDetailInfoBySessionID
	 * @param 		$columns, $user_name
	 * @return		associative array
	 * @tutorial 	This method will fetch the user detail based on username
	 */
	function getUserDetailInfoBySessionID($columns = "*", $session_id) {
	
		//set parameters
		$params = array(":SessionID"=>$session_id);
		//set columns
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		//select userinformation
		$sel_user_info = "SELECT $columns FROM Users WHERE SessionID = :SessionID";
		$res_user_info = $this->db->getConnection ( "USERPORTAL" )->fetchAssoc ( $sel_user_info, array($params) );
	
		return $res_user_info;
	}
	
	/**
	 * @method		getUserDetailInfoByUserID
	 * @param 		$columns, $user_name
	 * @return		associative array
	 * @tutorial 	This method will fetch the user detail based on username
	 */
	function getUserDetailInfoByUserID($columns = "*", $user_id) {

		//set columns
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		//set parameters
		$params = array(":UserID"=>$user_id);
		//select userinformation
		$sel_user_info = "SELECT $columns FROM Users WHERE UserID = :UserID";
		$res_user_info = $this->db->getConnection ( "USERPORTAL" )->fetchAssoc ( $sel_user_info, array($params) );
	
		return $res_user_info;
	}
	
	/**
	 * @method		getUserDetailInfoByUserAndPassword
	 * @param 		$columns, $user_name
	 * @return		associative array
	 * @tutorial 	This method will fetch the user detail based on username
	 */
	function getUserDetailInfoByUserAndPassword($columns = "*", $email, $password) {
	
		//set columns
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		//set parameters
		$params = array(":Email"=>$email);
		//select userinformation
		$sel_user_info = "SELECT $columns FROM Users WHERE Email = :Email";
		$res_user_info = $this->db->getConnection ( "USERPORTAL" )->fetchAssoc ( $sel_user_info, array($params) );

		if (password_verify($password, $res_user_info['Verification'])) {
		    	
		    if (password_needs_rehash($password, PASSWORD_DEFAULT)) {
		         
		        // Set parameters for prepared query
		        $params = array (":Verification"=>password_hash( $password, PASSWORD_DEFAULT ), ":Email"=>$email);
		         
		        $upd_user_info = "UPDATE Users SET Verification = :Verification";
		        $upd_user_info .= " WHERE Email = :Email";
		         
		        $this->db->getConnection ( "USERPORTAL" )->update ($upd_user_info, array($params));
		         
		    }
		}
		else {
		    $res_user_info = array();
		}
		
		
		return $res_user_info;
	}
	
	/**
	 * @method		getProfileDataByUserIDProfileID
	 * @param		integer $USERID        	
	 * @param		string $ProfileID        	
	 * @return		associative array
	 */
	function getProfileDataByUserIDProfileID($USERID, $ProfileID) {
		
		//Set parameters for prepared query
		$params = array(":UserID"=>$USERID, ":ProfileID"=>$ProfileID);
		
		$sel_profile_data = "SELECT QuestionID, Answer FROM ProfileData 
							 WHERE UserID = :UserID AND ProfileID = :ProfileID";
		$row_profile_data = $this->db->getConnection ( "USERPORTAL" )->getConnection("USERPORTAL")->fetchAllAssoc ( $sel_profile_data, array($params));

		return $row_profile_data ['results'];
	}
	
	/**
	 * @method		insertUserProfileWithApplicaitonInfo
	 * @param		$USERID, $OrgID, $ApplicationID
	 */
	function insertUserProfileWithApplicaitonInfo($USERID, $OrgID, $ApplicationID) {
		
		global $ApplicantsObj;
		
		//Set parameters for prepared query
		$params = array(":UserID"=>$USERID);
		
		$sel_profile_id = "SELECT ProfileID FROM ProfileData WHERE UserID = :UserID";
		$res_profile_id = $this->db->getConnection("USERPORTAL")->fetchAssoc ( $sel_profile_id, array($params) );
		$profile_id = $res_profile_id ['ProfileID'];
		
		$del_user_profile = "DELETE FROM ProfileData WHERE UserID = :UserID";
		$res_user_profile = $this->db->getConnection("USERPORTAL")->delete ( $del_user_profile, array($params) );
		
		//Set where condition
		$app_where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
		//Set parameters
		$app_params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
		//Get applicant data information
		$application_data = $ApplicantsObj->getApplicantDataInfo("*", $app_where, '', '', array($app_params));
		
		$application_info = $application_data ['results'];
		$application_info_count = $application_data ['count'];
		
		if ($profile_id == "") {
			$profile_id = 'STANDARD';
		}
		
		//Update Default Profile Information
		$upd_profile_params   =   array(":DefaultProfile"=>$ApplicationID, ":UserID"=>$USERID);
		$upd_def_profile      =   "UPDATE Users SET DefaultProfile = :DefaultProfile WHERE UserID = :UserID";
		$res_def_profile      =   $this->db->getConnection("USERPORTAL")->update($upd_def_profile, array($upd_profile_params));
		
		for($i = 0; $i < $application_info_count; $i ++) {
			$params[":QuestionID"]   =   $application_info [$i] ['QuestionID'];
			$params[":Answer"]       =   $application_info [$i] ['Answer'];
			$params[":ProfileID"]    =   $profile_id;
			$params[":UAnswer"]      =   $application_info [$i] ['Answer'];
			$ins_user_profile = "INSERT INTO ProfileData(UserID, ProfileID, QuestionID, Answer) VALUES(:UserID, :ProfileID, :QuestionID, :Answer) ON DUPLICATE KEY UPDATE Answer = :UAnswer";
			$res_user_profile = $this->db->getConnection("USERPORTAL")->insert ( $ins_user_profile, array($params) );

		}
	}

	/**
	 * @method		clearUserProfileData
	 * @param		$USERID, $OrgID, $ApplicationID
	 */
	function clearUserProfileData($USERID) {
	
	    global $ApplicantsObj;
	
	    //Set parameters for prepared query
	    $params = array(":UserID"=>$USERID);
	
	    $del_user_profile = "DELETE FROM ProfileData WHERE UserID = :UserID";
	    $res_user_profile = $this->db->getConnection("USERPORTAL")->delete ( $del_user_profile, array($params) );
	
	    //Update Default Profile Information
	    $upd_profile_params   =   array(":DefaultProfile"=>'', ":UserID"=>$USERID);
	    $upd_def_profile      =   "UPDATE Users SET DefaultProfile = :DefaultProfile WHERE UserID = :UserID";
	    $res_def_profile      =   $this->db->getConnection("USERPORTAL")->update($upd_def_profile, array($upd_profile_params));
	}
	
	
	/**
	 * @method		delProfileData
	 */
	function delProfileData($USERID, $ProfileID = "") {
		
		$params = array(":UserID"=>$USERID);
		$del_user_profile = "DELETE FROM ProfileData WHERE UserID = :UserID";
		if($ProfileID != "") {
			$params[':ProfileID'] = $ProfileID;
			$del_user_profile .= " AND ProfileID = :ProfileID";
		}
		$res_user_profile = $this->db->getConnection("USERPORTAL")->delete ( $del_user_profile, array($params) );
		
		return $res_user_profile;
	}
	
	
	/**
	 * @method		delProfiles
	 */
	function delProfiles($USERID, $ProfileID) {
	
		$params = array(":UserID"=>$USERID, ":ProfileID"=>$ProfileID);
		$del_user_profile = "DELETE FROM Profiles WHERE UserID = :UserID AND ProfileID = :ProfileID";
		$res_user_profile = $this->db->getConnection("USERPORTAL")->delete ( $del_user_profile, array($params) );
	
		return $res_user_profile;
	}
	
	/**
	 * @method		getQuestionIDAnswer
	 * @param		$USERID
	 * @return		array(data_list, count)
	 * @category	UserPortal Users
	 */
	function getQuestionIDAnswer($USERID) {
		
		//Set parameters for prepared query
		$params = array(":UserID"=>$USERID);
		
		$sel_quesid_answer  = "SELECT QuestionID, Answer FROM ProfileData WHERE UserID = :UserID";
		$sel_quesid_answer .= " AND QuestionID IN ('first','last') ORDER BY ProfileID, QuestionID";
	
		$que_answer_info = $this->db->getConnection("USERPORTAL")->fetchAllAssoc($sel_quesid_answer, array($params));
	
		return $que_answer_info;
	}
	
	/**
	 * @method		getUsersInfoByOrgIDMultiOrgID
	 * @return		associative array
	 */
	function getUsersInfoByOrgIDMultiOrgID($OrgID, $MultiOrgID) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>(string)$MultiOrgID);
		
		$sel_users_info  = "SELECT * FROM Users WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
		$res_users_info  = $this->db->getConnection("USERPORTAL")->fetchAllAssoc($sel_users_info, array($params));
		return $res_users_info;
	}
	
	/**
	 * @method		insUpdatedUsersListInfo
	 * @param		$info
	 */
	public function insUpdatedUsersListInfo($info) {
	
	    $ins_users_info = $this->db->buildInsertStatement('UpdatedUsersList', $info);
	    $res_users_info = $this->db->getConnection ( "USERPORTAL" )->insert ( $ins_users_info["stmt"], $ins_users_info["info"] );
	    
	    return $res_users_info;
	}
	
	/**
	 * @method		getUpdatedUsersListInfo
	 * @param		$info
	 */
	public function getUpdatedUsersListInfo() {
	
	    $sel_users_info  = "SELECT * FROM UpdatedUsersList";
	    $res_users_info  = $this->db->getConnection("USERPORTAL")->fetchAllAssoc($sel_users_info);
	    return $res_users_info;
	    
	}
	
	
	/**
	 * @method		getInfo
	 * @param		$info
	 */
	public function getInfo($query, $info = array()) {
	    $res_info = $this->db->getConnection ( "USERPORTAL" )->fetchAllAssoc ( $query, $info );
	    
	    return $res_info;
	}
	
	
	/**
	 * @method		delRecords
	 */
	function delRecords($del_query, $del_params) {
	
	    $res_records = $this->db->getConnection("USERPORTAL")->delete ( $del_query, array($del_params) );
	
	    return $res_records;
	}
	
	
	/**
	 * @method		updInformation
	 * @param		array $info
	 * @return		associative array
	 */
	function updInformation($upd_query, $upd_params, $con) {
	
	    $res_profiles = $this->db->getConnection ( $con )->update ( $upd_query, array($upd_params) );
	
	    return $res_profiles;
	}
	
	
	/**
	 * @method     getUpdatePortalUserIDs
	 * 
	 */
	function getUpdatePortalUserIDs() {
	    global $ApplicantDetailsObj, $UserPortalUsersObj;
	    
	    $sel_app_data  =   "SELECT * FROM ApplicantData WHERE QuestionID = 'PortalUserID' AND Answer REGEXP '[a-zA-Z]'";
	    $res_app_data  =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_app_data, $info );
	    $app_list      =   $res_app_data['results'];
	    $app_cnt       =   $res_app_data['count'];
	     
	    for($ja = 0; $ja < $app_cnt; $ja++) {
	        $OrgID             =   $app_list[$ja]['OrgID'];
	        $ApplicationID     =   $app_list[$ja]['ApplicationID'];
	        
	        $email             =   $ApplicantDetailsObj->getAnswer($OrgID, $ApplicationID, 'email');
	        $user_info         =   $UserPortalUsersObj->getUserDetailInfoByEmail("UserID", $email);
	        $UserID            =   $user_info['UserID'];
	        
	        if($UserID != "") {
	        	
	            $upd_params    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":Answer"=>$UserID);
	            $upd_query     =   "UPDATE ApplicantData SET Answer = :Answer WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID AND QuestionID = 'PortalUserID'";
	            $res_query     =   $this->db->getConnection ( "IRECRUIT" )->update ( $upd_query, array($upd_params) );
	            
	        }
	    }
	     
	     
	}
}
