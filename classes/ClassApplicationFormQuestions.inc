<?php
/**
 * @class		ApplicationFormQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 */

class ApplicationFormQuestions {
    
    var $conn_string        =   "IRECRUIT";
    var $alphabet           =   array();        //Alphabets
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
        
        //Alphabets
        $this->alphabet     =   G::Obj('GenericLibrary')->getAlphabetsKeyFrom1();
    }

    /**
     * @tutorial    getFormQuestionsList
     */
    function getFormQuestionsList($OrgID, $FormID, $SectionID = '', $APPDATA = array()) {
        
        // Bind parameters
        $params     =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID);
        // Set condition
        $where      =   array ("OrgID = :OrgID", "FormID = :FormID");

        if($SectionID != "") {
            $where[]        =   "SectionID = :SectionID";
            $params[":SectionID"]   =   $SectionID;
        }
        
        $where[]    =   "Active = 'Y'";
        
        // Get Form Questions Information
        $que_results    =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder ASC", array ($params) );
        $que_list       =   $que_results['results'];
        
        //Json Questions
        $que_ids        =   array();
        $ques           =   array();
        
        for($jq = 0; $jq < count($que_list); $jq++) {
            $ques[$que_list[$jq]['QuestionID']]    =   $que_list[$jq];
            $que_ids[] = $que_list[$jq]['QuestionID'];
        }

        return array("json_que_ids"=>$que_ids, "json_ques"=>$ques);
    }
    
    /**
     * @method      string  getApplicationFormQuestionsInfo
     * @param       string  $OrgID
     * @param       string  $ApplicationID
     * @param       string  $APPDATA
     */
    function getApplicationFormQuestionsInfo($OrgID, $FormID) {
        
        $form_sections      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
        
        foreach($form_sections as $SectionID=>$SectionInfo) {
            $SectionType    =   "single";
            $SecQueAnswers  =   $this->getSingleQuestionSectionInfo($OrgID, $FormID, $SectionID, $SectionType, $AppQues);
            $AppQues        =   $SecQueAnswers;
        }
    
        return $AppQues;
    }
    
    /**
     * @method     getSingleQuestionSectionInfo
     * @param      string $OrgID
     * @param      string $FormID
     * @param      string $APPDATA
     * @param      string $SectionID
     * @return     string
     */
    function getSingleQuestionSectionInfo($OrgID, $FormID, $SectionID, $SectionType, &$AppQues) {
        
        global $permit, $feature, $FormQuestionsDetailsObj, $FormSettingsObj;
         
        //Set the parameters
        $params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
        //Set columns
        $columns    =   array("Question", "QuestionID", "QuestionTypeID", "value", "SectionID", "Required");
        //Set condition
        $where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
        //Get Questions information
        $results    =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder ASC", array($params));
   
        foreach ($results['results'] as $FQ) {
            $AppQues[$SectionID][0][$FQ ['QuestionID']]['Question']               =   strip_tags($FQ['Question']); //preg_replace('/[^a-z0-9 ]/i','',$FQ['Question']);
            $AppQues[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']         =   $FQ['QuestionTypeID'];
            $AppQues[$SectionID][0][$FQ ['QuestionID']]['Required']               =   $FQ['Required'];
            $AppQues[$SectionID][0][$FQ ['QuestionID']]['value']                  =   $FQ['value'];
        } // end foreach
    
        return $AppQues;
    }

    /**
     * @method		getFormParentQuesForChildQues
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getFormParentQuesForChildQues($OrgID, $FormID, $SectionID) {
        
        $form_questions		=	$this->getFormQuestionsListBySectionID($OrgID, $FormID, $SectionID);
        $child_que_parents	=	array();
        
        if(is_array($form_questions)) {
            foreach($form_questions as $question_id=>$question_info) {
                if($question_info['Active'] == 'Y'
                    && $question_info['ChildQuestionsInfo'] != "")
                {
                    $child_ques_info	=	json_decode($question_info['ChildQuestionsInfo'], true);
                    
                    foreach($child_ques_info as $parent_que_option=>$child_ques_list) {
                        
                        foreach ($child_ques_list as $child_que_id=>$child_que_disp_status) {
                            $child_que_parents[$child_que_id]	=	$question_id;
                        }
                    }
                }
            }
        }
        
        return $child_que_parents;
    }

    /**
     * @method		getFormParentQuesForChildQues
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getFormChildQuesForParentQues($OrgID, $FormID, $SectionID) {
        
        $form_questions		=	$this->getFormQuestionsListBySectionID($OrgID, $FormID, $SectionID);
        $child_que_list	    =	array();
        
        if(is_array($form_questions)) {
            foreach($form_questions as $question_id=>$question_info) {
                if($question_info['Active'] == 'Y'
                    && $question_info['ChildQuestionsInfo'] != "")
                {
                    $child_que_list[$question_id]  =   json_decode($question_info['ChildQuestionsInfo'], true);
                }
            }
        }
        
        return $child_que_list;
    }
    
    /**
     * @method      getMultiAnswerInfo
     * @param       $APPDATA
     * @param       $id
     */
    function getMultiAnswerInfo($APPDATA, $id) {
    	
        $cnt = $APPDATA [$id . 'cnt'];

        $multi_answer = array();
        for($i = 1; $i <= $cnt; $i ++) {
            if(isset($APPDATA[$id.'-'.$i]) && $APPDATA[$id.'-'.$i] != "") {
                $multi_answer[$i] = $APPDATA[$id.'-'.$i];
            }
        }
        
        return $multi_answer;
    }
    
    /**
     * @method      getFormQuestionsListBySectionID
     * @tutorial    Get FormQuestions By SectionID
     */
    function getFormQuestionsListBySectionID($OrgID, $FormID, $SectionID = '') {
        
        // Bind parameters
        $params         =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID, ':Active'=>'Y');
        // Set condition
        $where          =   array ("OrgID = :OrgID", "FormID = :FormID", "Active = :Active");
        
        if($SectionID != "") {
            $params[':SectionID']   =   $SectionID;
            $where[]                =   "SectionID = :SectionID";
        }
        
        // Get Form Questions Information
        $que_results    =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder ASC", array ($params) );
        $que_list       =   $que_results['results'];
        
        $ques           =   array();
        for($jq = 0; $jq < count($que_list); $jq++) {
            $ques[$que_list[$jq]['QuestionID']]  =   $que_list[$jq];
        }
        
        return $ques;
    }
    
    /**
     * @method      getFormQuestionsCountBySectionID
     * @tutorial    Get FormQuestions By SectionID
     */
    function getFormQuestionsCountBySectionID($OrgID, $FormID, $SectionID = '', $SkipQueTypes = array()) {
        
        // Bind parameters
        $params             =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
        // Get Form Questions Information
        $sel_frm_que_info   =   "SELECT COUNT(*) AS QuestionsCount FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND SectionID = :SectionID AND Active = 'Y'";
        $skip_ques_types    =   "'" . implode ( "', '", $SkipQueTypes ) . "'";
        if(count($SkipQueTypes) > 0) {
            $sel_frm_que_info   .=   " AND QuestionTypeID NOT IN($skip_ques_types)";
        }
        $res_frm_que_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_frm_que_info, array($params));
        
        return $res_frm_que_info['QuestionsCount'];
    }
}
?>
