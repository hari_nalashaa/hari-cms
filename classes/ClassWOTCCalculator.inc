<?php
/**
 * @class		WOTCCalculator
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCCalculator
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      calculate_wotcCredit
     * @param       string $wotcID
     * @param       string $ApplicationID
     * @param       string $year
     * @return      array
     */
    function calculate_wotcCredit($wotcID, $ApplicationID, $year)
    {
        $value      =   0;
        $comment    =   '';
        $commentcnt =   0;
        $active     =   "N";
        
        $wotcID             =   !empty($wotcID) ? $wotcID : '';
        $ApplicationID      =   !empty($ApplicationID) ? $ApplicationID : '';
        $year               =   !empty($year) ? $year : '';
        $PRS['Category']    =   !empty($PRS['Category']) ? $PRS['Category'] : '';
            
        $query  =   "SELECT DATEDIFF(now(),StartDate) Anniversary,";
        $query  .=  " if(StartDate = '0000-00-00','',date_format(StartDate,'%m/%d/%Y')) StartDate,";
        $query  .=  " if(Received = '0000-00-00','',date_format(Received,'%m/%d/%Y')) Received,";
        $query  .=  " if(Filed = '0000-00-00','',date_format(Filed,'%m/%d/%Y')) Filed,";
        $query  .=  " if(Confirmed= '0000-00-00','',date_format(Confirmed,'%m/%d/%Y')) Confirmed,";
        $query  .=  " Qualified, Category, Comments";
        $query  .=  " FROM Processing";
        $query  .=  " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID);
        $PRS    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $query  =   "SELECT * FROM ApplicantData WHERE wotcID = :wotcid and ApplicationID = :applicationid";
        $params =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID);
        $AD     =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $query  =   "SELECT Year, if(Termed= '0000-00-00','',date_format(Termed,'%m/%d/%Y')) Termed,";
        $query  .=  " Reason, TaxCredit,";
        $query  .=  " if(DateBilled= '0000-00-00','',date_format(DateBilled,'%m/%d/%Y')) DateBilled,";
        $query  .=  " if(DatePaid= '0000-00-00','',date_format(DatePaid,'%m/%d/%Y')) DatePaid,";
        $query  .=  " Comments";
        $query  .=  " FROM Billing WHERE wotcID = :wotcid";
        $query  .=  " AND ApplicationID = :applicationid AND Year = :year";
        $params =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID, ':year'=>$year);
        $BILL   = $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $PRS['Category']    =   !empty($PRS['Category']) ? $PRS['Category'] : '';
        $query  =   "SELECT CapYearOne, CapYearTwo";
        $query  .=  " FrOM QualificationCodes WHERE QualificationCode = :qc";
        $params =   array(':qc'=>$PRS['Category']);
        $QC     =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        if ($AD['Qualifies'] == "Y") {
            if (($PRS['Confirmed'] != "") && ($PRS['Qualified'] == "Y")) {
                $active = "Y";
            }
        }
        
        // if already billed turn off adding hours for all but long term or 2nd year
        $query          =   "SELECT * FROM Billing WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
        $params         =   array(':wotcid' => $wotcID, ':applicationid'=>$ApplicationID);
        $BILLING_RES    =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($query, array($params));
        $BILLING        =   $BILLING_RES['results'];
        $billcnt        =   $BILLING_RES['count'];
        
        if (($billcnt > 0) && ($QC['CapYearTwo'] == 0)) {
            $active = "B";
        }
        
        // Add comment for Billed values
        if ($BILL['Year'] == $year) {
            if ($commentcnt > 0) {
                $comment .= "&nbsp;&nbsp;";
            }
            $comment .= "(Billed)";
            $commentcnt ++;
        }
        
        if ($year > 2) {
            $active = "B";
        }
        
        // calculate wotc Estimated Credit
        $query  =   "SELECT SUM(Hours) Hours, SUM(Wages) Wages,";
        $query .=   " MAX(if(ASOF = '0000-00-00','',ASOF)) ASOF,";
        $query .=   " GROUP_CONCAT(Comments) Comments";
        $query .=   " FROM Hours WHERE wotcID = :wotcid";
        $query .=   " AND ApplicationID = :applicationid AND Year = :year";
        $query .=   " GROUP BY wotcID, ApplicationID";
        $params =   array(':wotcid' => $wotcID, ':applicationid'=>$ApplicationID, ':year'=>$year);
        $TTHRS  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        $factor = .4;
        // calculate benefit
        if (($PRS['Category'] == "LTTANF") && ($year == 1)) {
            if ($TTHRS['Hours'] <= 400) {
                if ($TTHRS['Wages'] > 10000) {
                    $TTHRS['Wages'] = 10000;
                }
                $factor = .25;
            }
        }
        if (($PRS['Category'] == "LTTANF") && ($year == 2)) {
            $factor = .5;
        }
        
        // mark if second year benefit available
        if ($PRS['Category'] == "LTTANF") {
            if ($commentcnt > 0) {
                $comment .= "&nbsp;&nbsp;";
            }
            $comment .= "Second year benefit";
            $commentcnt ++;
        }
        
        if ($TTHRS['Hours'] >= 120) {
            $value = $TTHRS['Wages'] * .25;
        }
        if ($TTHRS['Hours'] >= 400) {
            $value = $TTHRS['Wages'] * $factor;
        }
        if ((($TTHRS['Hours'] >= 120) && ($TTHRS['Hours'] < 400)) && ($TTHRS['Wages'] >= 6000)) {
            if ($PRS['Category'] == "VU6M") {
                if ($TTHRS['Wages'] >= 14000) {
                    $TTHRS['Wages'] = 14000;
                }
                $value = $TTHRS['Wages'] * .25;
            } else {
                $value = $TTHRS['Wages'] * $factor;
            }
        }
        
        // Cap Wages for benefit
        if ($year == 1) {
            if ($value > $QC['CapYearOne']) {
                $value = $QC['CapYearOne'];
                if (($TTHRS['Wages'] > 6000) && ($TTHRS['Hours'] < 400)) {
                    $value = 1500;
                }
                if ($commentcnt > 0) {
                    $comment .= "&nbsp;&nbsp;";
                }
                $comment .= "Maxed Limit";
                $commentcnt ++;
            }
        } else 
            if ($year == 2) {
                if ($value > $QC['CapYearTwo']) {
                    $value = $QC['CapYearTwo'];
                    if ($commentcnt > 0) {
                        $comment .= "&nbsp;&nbsp;";
                    }
                    $comment .= "Maxed Limit";
                    $commentcnt ++;
                }
            }
        
        // if date is past one year mark then indicate as comment
        if ($PRS['Anniversary'] > 365) {
            if ($commentcnt > 0) {
                $comment .= "&nbsp;&nbsp;";
            }
            $comment .= "Past one year anniversary";
            $commentcnt ++;
        }
        if ($PRS['Anniversary'] > 365 * 2) {
            if ($commentcnt > 0) {
                $comment .= "&nbsp;&nbsp;";
            }
            $comment .= "Past second year anniversary";
            $commentcnt ++;
        }
        $daysleft = 365 - $PRS['Anniversary'];
        if ($daysleft < 0) {
            $daysleft = 0;
        }
        if ($daysleft >= 0) {
            if ($commentcnt > 0) {
                $comment .= "&nbsp;&nbsp;";
            }
            $comment .= $daysleft . " days left in first year";
            $commentcnt ++;
        }
        
        return array(
            round($value, 2, PHP_ROUND_HALF_DOWN),
            $active,
            $comment
        );
    } // end function
} // end class

?>
