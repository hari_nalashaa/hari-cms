<?php
/**
 * @class		TwilioMessagingServiceApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioMessagingServiceApi {
	
    public $db;
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           	=   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method		createPhoneNumberResource
     */
    public function createPhoneNumberResource($OrgID, $message_service_id, $phone_number_sid) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	    
    	try {
    		
			$phone_number_resource_info	=	$client->messaging->v1->services($message_service_id)
		                                      ->phoneNumbers
		                                      ->create($phone_number_sid);

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-phone-number-resource-api.txt", serialize($phone_number_resource_info), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created phone number resource", "Response"=>$phone_number_resource_info);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-phone-number-resource-api-error.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create phone number resource", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    /**
     * @method		createMessageService
     */
    public function createMessageService($OrgID, $friendly_name) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
    	
    		$service	=	$client->messaging->v1->services->create($friendly_name);
    	
    		//Update the message service id
    		G::Obj('TwilioAccounts')->updTwilioMessageServiceID($OrgID, $service->sid);
    		
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-message-service-api.txt", serialize($service), "w+", false);
    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created phone number resource", "Response"=>$service);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-message-service-api-error.txt", serialize($e->getMessage()), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create phone number resource", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }
    
    /**
     * @method		fetchMessageService
     */
    public function fetchMessageService($OrgID, $msg_service_id) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
    		 
			$service = $client->messaging->v1->services($msg_service_id)->fetch();
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-fetch-message-service-api.txt", serialize($service), "w+", false);
    		 
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully fetched message service", "Response"=>$service);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-fetch-message-service-api-error.txt", serialize($e->getMessage()), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to fetch message service", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }
    
    /**
     * @method		readService
     */
    public function readService($OrgID) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    		 
    		$services	=	$client->messaging->v1->services->read(20);
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-read-service-api.txt", serialize($services), "w+", false);
    		 
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Read all resources", "Response"=>$services);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-read-service-api-error.txt", serialize($e->getMessage()), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to read all resources", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }

    /**
     * @method		deleteService
     */
    public function deleteService($OrgID, $service_id) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);

    	try {
    		 
    		$service_info	=	$client->messaging->v1->services($service_id)->delete();
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-delete-service-api.txt", serialize($service_info), "w+", false);
    		 
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully deleted the resource", "Response"=>$service_info);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-delete-service-api-error.txt", serialize($e->getMessage()), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to delete the resource", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }
    
    /**
     * @method		getMessageInfo
     */
    public function getMessageInfo($OrgID, $message_id) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {

    		$message_info = $client->messages($message_id)->fetch();

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-message-info-api.txt", serialize($message_info), "w+", false);
    		 
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Retrieved the message information successfully", "Response"=>$message_info);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-message-info-api-error.txt", serialize($e->getMessage()), "w+", false);
    		 
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to delete the resource", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }
}
