<?php
/**
 * @class		RequisitionQuestionsHistory
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionQuestionsHistory {

    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    /**
     * @method		insRequisitionQuestionsHistory
     * @param		$que_info
     */
    public function insRequisitionQuestionsHistory($que_info) {
    
        $ins_form_ques = $this->db->buildInsertStatement("RequisitionQuestionsHistory", $que_info);
        $res_form_ques = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_form_ques["stmt"], $ins_form_ques["info"] );
    
        return $res_form_ques;
    }
    
}
?>
