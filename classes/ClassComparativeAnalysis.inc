<?php
/**
 * @class		ComparativeAnalysis
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ComparativeAnalysis {
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    /**
     * @method      getComparativeLabels
     * @param       $OrgID, $UserID
     */
    function getComparativeLabels($OrgID, $UserID) {
        
        $params     =   array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
        $sel_comparative_search_labels = "SELECT * FROM ComparativeSearchLabels WHERE OrgID = :OrgID AND UserID = :UserID";
        $res_comparative_search_labels = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_comparative_search_labels, array($params) );
        
        return $res_comparative_search_labels;
    }
    
    /**
     * @method		insComparativeLabels
     * @param		$OrgID, $UserID, $LabelID, $LabelName
     * @return		
     */
    function insComparativeLabels($OrgID, $UserID, $LabelID, $LabelName) {

        $params = array(":IOrgID"=>$OrgID, ":IUserID"=>$UserID, ":ILabelID"=>$LabelID, ":ILabelName"=>$LabelName, ":ULabelName"=>$LabelName);
        $ins_comparative_search_labels   =   "INSERT INTO ComparativeSearchLabels(OrgID, UserID, LabelID, LabelName) VALUES(:IOrgID, :IUserID, :ILabelID, :ILabelName)";
        $ins_comparative_search_labels  .=   " ON DUPLICATE KEY UPDATE LabelName = :ULabelName";
        $res_comparative_search_labels   =   $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_comparative_search_labels, array($params) );
    
        return $res_comparative_search_labels;
    }

    /**
     * @method		getApplicantComparativeLabels
     * @param		$OrgID, $UserID, $RequestID, $ApplicationID
     * @return		
     */
    function getApplicantComparativeLabels($OrgID, $UserID, $RequestID, $ApplicationID) {

        $query = "SELECT group_concat(ComparativeSearchLabels.LabelName SEPARATOR ', ') FROM ComparativeLabelApplicants";
	$query .= " JOIN ComparativeSearchLabels ON ComparativeSearchLabels.LabelID = ComparativeLabelApplicants.LabelID";
	$query .= " WHERE ComparativeLabelApplicants.UserID = :UserID ";
	$query .= " AND ComparativeLabelApplicants.OrgID = :OrgID";
	$query .= " AND ComparativeLabelApplicants.RequestID = :RequestID";
	$query .= " AND ComparativeLabelApplicants.ApplicationID = :ApplicationID";

        $params = array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);

	 $RESULTS = $this->db->getConnection ( "IRECRUIT" )->fetchRow ( $query, array($params) );

	if ($RESULTS['0'] != "") {
		$rtn = $RESULTS['0'];
	} else { 
		$rtn = "";
	}
    
	return $rtn;
    }
    
    
    /**
     * @method		delComparativeLabels
     * @param		$OrgID, $UserID, $LabelID
     * @return		array
     */
    function delComparativeLabels($OrgID, $UserID, $LabelID) {

        $params     =   array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":LabelID"=>$LabelID);
        $del_comparative_search_labels = "DELETE FROM ComparativeSearchLabels WHERE OrgID = :OrgID AND UserID = :UserID AND LabelID = :LabelID";
        $res_comparative_search_labels = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_comparative_search_labels, array($params) );
        
        //Delete applicants
        $this->delComparativeLabelApplicants($LabelID, $UserID, '');
        
        return $res_comparative_search_labels;
    }

    
    /**
     * @method      getComparativeLabelInfo
     * @param       $OrgID, $UserID, $LabelID
     */
    function getComparativeLabelInfo($OrgID, $UserID, $LabelID) {
    
        $params     =   array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":LabelID"=>$LabelID);
        $sel_comparative_search_label_info = "SELECT * FROM ComparativeSearchLabels WHERE OrgID = :OrgID AND UserID = :UserID AND LabelID = :LabelID";
        $res_comparative_search_label_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_comparative_search_label_info, array($params) );
    
        return $res_comparative_search_label_info;
    }
    
    #################    Comparative Label Applicants    ######################
    
    /**
     * @method      getComparativeLabelApplicants
     * @param       $LabelID, $UserID
     */
    function getComparativeLabelApplicants($LabelID, $UserID) {
    
        $params     =   array(":LabelID"=>$LabelID, ":UserID"=>$UserID);
        $sel_comparative_label_applicants = "SELECT * FROM ComparativeLabelApplicants WHERE LabelID = :LabelID AND UserID = :UserID";
        $res_comparative_label_applicants = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_comparative_label_applicants, array($params) );
    
        return $res_comparative_label_applicants;
    }
    
    /**
     * @method		insComparativeLabelApplicants
     * @param		$LabelID, $UserID, $RequestID, $ApplicationID
     * @return
     */
    function insComparativeLabelApplicants($OrgID, $LabelID, $UserID, $ApplicationID, $RequestID) {
    
        $params     =   array(":OrgID"=>$OrgID, ":LabelID"=>$LabelID, ":UserID"=>$UserID);
        $sel_comparative_label_applicants = "SELECT COUNT(ApplicationID) AS ApplicationsCount FROM ComparativeLabelApplicants WHERE OrgID = :OrgID AND LabelID = :LabelID AND UserID = :UserID";
        $res_comparative_label_applicants = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_comparative_label_applicants, array($params) );
        
        if($res_comparative_label_applicants['ApplicationsCount'] <= 100) {
            $params = array(":IOrgID"=>$OrgID, ":ILabelID"=>$LabelID, ":IUserID"=>$UserID, ":IRequestID"=>$RequestID, ":IApplicationID"=>$ApplicationID);
            $ins_comparative_search_labels   =   "INSERT INTO ComparativeLabelApplicants(OrgID, LabelID, UserID, RequestID, ApplicationID, LastModified) VALUES(:IOrgID, :ILabelID, :IUserID, :IRequestID, :IApplicationID, NOW())";
            $ins_comparative_search_labels  .=   " ON DUPLICATE KEY UPDATE LastModified = NOW()";
            $res_comparative_search_labels   =   $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_comparative_search_labels, array($params) );
            
            return $res_comparative_search_labels;
        }
        else {
        	return "Maximum Limit Reached";
        }
    }
    
    
    /**
     * @method		delComparativeLabelApplicants
     * @param		$LabelID, $UserID, $ApplicationID
     * @return		array
     */
    function delComparativeLabelApplicants($LabelID, $UserID, $ApplicationID = '') {
    
        $params     =   array(":LabelID"=>$LabelID, ":UserID"=>$UserID);
        $del_comparative_search_labels = "DELETE FROM ComparativeLabelApplicants WHERE UserID = :UserID AND LabelID = :LabelID";
        
        if($ApplicationID != "") {
            $params[":ApplicationID"] =  $ApplicationID;
            $del_comparative_search_labels .= " AND ApplicationID = :ApplicationID";
        }
        
        $res_comparative_search_labels = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_comparative_search_labels, array($params) );
    
        return $res_comparative_search_labels;
    }
    
    #################    Comparative Analysis Configuration    ######################
    
    /**
     * @method      getComparativeAnalysisConfig
     * @param       $UserID
     */
    function getComparativeAnalysisConfig($OrgID, $UserID) { 
    
        $params     =   array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
        $sel_comparative_label_applicants = "SELECT * FROM ComparativeAnalysisConfig WHERE UserID = :UserID AND OrgID = :OrgID";
        $res_comparative_label_applicants = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_comparative_label_applicants, array($params) );
    
        return $res_comparative_label_applicants;
    }
    
    /**
     * @method		insComparativeAnalysisConfig
     * @param		$OrgID, $UserID, $columns_list
     * @return
     */
    function insUpdComparativeAnalysisConfig($OrgID, $UserID, $columns_list) {

                $params     =   array(
                              ":IOrgID"			=>	$OrgID, 
                              ":IUserID"		=>	$UserID, 
                              ":IColumnsList"	=>	json_encode($columns_list),
                              ":UColumnsList"	=>	json_encode($columns_list)
                             );  
        $ins_comparative_analysis_config = "INSERT INTO ComparativeAnalysisConfig(OrgID, UserID, ColumnsList, LastModified) VALUES(:IOrgID, :IUserID, :IColumnsList, NOW()) ON DUPLICATE KEY UPDATE ColumnsList = :UColumnsList, LastModified = NOW()";
        $res_comparative_analysis_config = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_comparative_analysis_config, array($params) );

        return $res_comparative_analysis_config;
    }
    
    /**
     * @method  getJobApplicationsInfoByLabelID
     * @param   string $OrgID
     * @param   string $UserID
     * @param   string $LabelID
     */
    function getJobApplicationsInfoByLabelID($OrgID, $UserID, $LabelID) {
        
        $params = array(":OrgID"=>$OrgID, ":UserID"=>$UserID, ":LabelID"=>$LabelID);
        $sel_job_apps_by_label_id = "SELECT J.OrgID, J.RequestID, J.ApplicationID, J.ApplicantSortName, J.ProcessOrder, J.ProcessOrder, J.DispositionCode, J.EntryDate, J.ApplicantConsiderationStatus1, J.ApplicantConsiderationStatus2, J.Rating, C.UserID  
                                     FROM JobApplications J,  ComparativeLabelApplicants C 
                                     WHERE J.OrgID = C.OrgID 
                                     AND J.RequestID = C.RequestID
                                     AND J.ApplicationID = C.ApplicationID
                                     AND C.LabelID = :LabelID
                                     AND C.UserID = :UserID
                                     AND J.OrgID = :OrgID";
        $res_job_apps_by_label_id = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_job_apps_by_label_id, array($params) );
        
        return $res_job_apps_by_label_id;
    }
    
    
    /**
     * @method		getApplicantDistance
     * @param		
     */
    function getApplicantDistance($OrgID, $MultiOrgID, $RequestID, $ApplicationID) {
    
        $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
        $sel_applicant_distance  = "SELECT LPAD(FORMAT((3958 * 3.1415926 * sqrt(((SELECT latitude FROM zipcodes WHERE zipcode = (select zipcode from Requisitions where OrgID = JA.OrgID and RequestID = JA.RequestID)) - (select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) ) * ((select latitude from zipcodes where zipcode = (select zipcode from Requisitions where OrgID = JA.OrgID and RequestID = JA.RequestID)) - (select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) ) + cos((select latitude from zipcodes where zipcode = (select zipcode from Requisitions where OrgID = JA.OrgID and RequestID = JA.RequestID)) / 57.29578) * cos((select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) / 57.29578) * ((select longitude from zipcodes where zipcode = (select zipcode from Requisitions where OrgID = JA.OrgID and RequestID = JA.RequestID)) - (select longitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip'))) * ((select longitude from zipcodes where zipcode = (select zipcode from Requisitions where OrgID = JA.OrgID and RequestID = JA.RequestID)) - (select longitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip'))) ) / 180),2),6,'0') as Distance
                                    FROM JobApplications JA
                                    WHERE JA.OrgID = :OrgID
                                    AND JA.MultiOrgID = :MultiOrgID
                                    AND JA.RequestID = :RequestID
                                    AND JA.ApplicationID = :ApplicationID";
        $res_applicant_distance  = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_applicant_distance, array($params));
        return $res_applicant_distance['Distance'];
    }
    
    /**
     * @method  getApplicantQuestionsSummary
     * @param   string $QuestionID
     * @param   string $QuestionTypeID
     * @param   string $Question
     * @param   string $SectionID
     */
    function getApplicantQuestionsSummary($OrgID, $ApplicationID, $QuestionID, $QuestionTypeID, $Question, $SectionID) {
        global $FormQuestionsObj, $ApplicantsDataObj, $AttachmentsObj, $ApplicantsObj;
        
        // Get applicant data
        $APPDATA = $ApplicantsObj->getAppData ($OrgID, $ApplicationID );
        
        if($QuestionTypeID == 8) {
        
            // Set where condition
            $formque_where = array (
                "OrgID          = :OrgID",
                "FormID         = :FormID",
                "SectionID      = 6",
                "QuestionTypeID = 8",
                "Active         = 'Y'"
            );
            // Set parameters
            $formque_params = array (
                ":OrgID"        =>  $OrgID,
                ":FormID"       =>  "STANDARD"
            );
            $resultsA = $FormQuestionsObj->getFormQuestionsInformation ( "QuestionID, value, Required", $formque_where, "QuestionOrder", array (
                $formque_params
            ) );
        
        
            $attachments = array();
            if (is_array ( $resultsA ['results'] )) {
                foreach ( $resultsA ['results'] as $AA ) {
        
                    // Set attachment parameters
                    $attach_params = array (
                        ":OrgID" => $OrgID,
                        ":ApplicationID" => $_REQUEST['ApplicationID'],
                        ":TypeAttachment" => $QuestionID
                    );
                    // Set attachment where condition
                    $attach_where = array (
                        "OrgID = :OrgID",
                        "ApplicationID = :ApplicationID",
                        "TypeAttachment = :TypeAttachment"
                    );
                    $resultsB = $AttachmentsObj->getApplicantAttachments ( "*", $attach_where, '', array (
                        $attach_params
                    ) );
        
                    $hit = $resultsB ['count'];
        
                    if ($hit > 0) {
                        $attachments[$AA ['QuestionID']]['AttachmentType'] = $AA ['value'];
                        $attachments[$AA ['QuestionID']]['Link'] = 'display_attachment.php?OrgID=' . $OrgID . '&ApplicationID=' . $_REQUEST['ApplicationID'] . '&Type=' . $QuestionID;
                    } else {
                        $attachments[$AA ['QuestionID']]['AttachmentType'] = $AA ['value'];
                        $attachments[$AA ['QuestionID']]['Link'] = "Not Submitted";
                    }
        
                } // end foreach
            }
        
            if($attachments[$QuestionID]['Link'] != "Not Submitted") {
                return "<a href='".$attachments[$QuestionID]['Link']."'>Download</a>";
            }
            else {
                return "Not Submitted";
            }
        }
        else {
            return $ApplicantsDataObj->printSection($OrgID, "STANDARD", $Question, $QuestionID, $QuestionTypeID, $SectionID, $APPDATA, "false");
        }
    }
}
?>
