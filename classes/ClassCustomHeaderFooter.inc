<?php
/**
 * @class		CustomHeaderFooter
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class CustomHeaderFooter {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method	getCustomHeaderFooterAllOrgsInfo
	 */
	function getCustomHeaderFooterAllOrgsInfo($OrgID) {
	
		$params		=	array(":OrgID"=>$OrgID);
		$sel_info 	=	"SELECT * FROM CustomHeaderFooter WHERE OrgID = :OrgID";
		$res_info 	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params));
	
		return $res_info['results'];
	}

	/**
	 * @method	getCustomHeaderFooterByOrgIDMultiOrgID
	 */
	function getCustomHeaderFooterByOrgIDMultiOrgID($OrgID, $MultiOrgID) {
	
		$params		=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		$sel_info 	=	"SELECT * FROM CustomHeaderFooter WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
		$res_info 	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params));
	
		return $res_info;
	}
	
	/**
	 * @method	getCustomHeaderFooter
	 */
	function getCustomHeaderFooter($OrgID) {
	
		$params = array(":OrgID"=>$OrgID);
		$sel_info = "SELECT * FROM CustomHeaderFooter WHERE OrgID = :OrgID";
		$res_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params));
	
		return $res_info;
	}
	
	/**
	 * @method	getCustomHeaderFooterMultiOrgID
	 */
	function getCustomHeaderFooterMultiOrgID($MultiOrgID) {
	
		$params = array(":MultiOrgID"=>$MultiOrgID);
		$sel_info = "SELECT * FROM CustomHeaderFooter WHERE MultiOrgID = :MultiOrgID";
		$res_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params));
	
		return $res_info;
	}
	
	/**
	 * @method		updCustomHeaderFooterInfo
	 * @param		$set_info, $where_info, $info
	 */
	function updCustomHeaderFooterInfo($set_info = array(), $where_info = array(), $info) {
	
		$upd_info = $this->db->buildUpdateStatement("CustomHeaderFooter", $set_info, $where_info);
		$res_info = $this->db->getConnection("IRECRUIT")->update($upd_info, $info);
	
		return $res_info;
	}
}
?>
