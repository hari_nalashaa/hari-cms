<?php
/**
 * @class		TwilioMediaInfo
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioMediaInfo {
    
    public $db;
    
    var $conn_string       		=   "IRECRUIT";
    
    /**
     * @tutorial	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db	=	Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function
    
    /**
     * @method		getMediaInfoByConversationIDMessageID
     */
    public function getMediaInfoByConversationIDMessageID($ResourceID, $MessageSID) {
        
        $params				=	array(":ConversationResourceID"=>$ResourceID, ":MessageSID"=>$MessageSID);
        $sel_conversations	=	"SELECT * FROM TwilioMediaInfo WHERE ConversationResourceID = :ConversationResourceID AND MessageSID = :MessageSID";
        $res_conversations	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_conversations, array($params) );
        $conversations_info	=	$res_conversations['results'];
        
        return $conversations_info;
    }
    
}
