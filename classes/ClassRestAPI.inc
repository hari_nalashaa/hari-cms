<?php 
/**
 * @class		RestAPI
 */

class RestAPI {

	/**
	 * @method	decodePath
	 */

	public function decodePath() {

    	  if(!empty($_GET['path'])) {
    	    $path_parts = $_GET['path'];
    	  } else {
    	    $path_parts = false;
    	  }

    	  if(empty($path_parts)) {
    	    return false;
    	  }

    	  if(strrpos($path_parts, '/') === false) {
    	    $path_parts .= '/';
    	  }

    	  $path_parts = explode('/', $path_parts);

    	  if(empty($path_parts[count($path_parts) - 1])) {
    	    array_pop($path_parts);
    	  }

  	  return $path_parts;
  	}


	/**
	 * @method	send_results
	 */

	 public function send_results ($data, $status = 200) {

    	  if(isset($data) && !empty($data)) {
    	    $this->show_request($data);
    	  } else {
    	    $this->show_request("Nothing to show", 400);
    	  }

  	}

	/**
	 * @method	show_request
	 */

  	private function show_request ($display_data, $status = null, $numeric_check = true) {

    	  if($display_data) {
      	    $this->header_status($status ? $status : 200);

      	  if($numeric_check) {
      	    echo json_encode($display_data, JSON_NUMERIC_CHECK);
      	  } else {
      	    echo json_encode($display_data);
      	  }

    	  } else {
	    $this->header_status($status ? $status : 200); 
    	  }

  	}

	/**
	 * @method	header_status
	 */

	 private function header_status($statusCode) {

    	  static $status_codes = null;

    	  if ($status_codes === null) {
      	    $status_codes = array (
            100 => 'Continue',
            101 => 'Switching Protocols',
            102 => 'Processing',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            207 => 'Multi-Status',
            299 => 'Fake 4xx error from middleware API',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            422 => 'Unprocessable Entity',
            423 => 'Locked',
            424 => 'Failed Dependency',
            426 => 'Upgrade Required',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            506 => 'Variant Also Negotiates',
            507 => 'Insufficient Storage',
            509 => 'Bandwidth Limit Exceeded',
            510 => 'Not Extended'
            );
    	  }

    	  if ($status_codes[$statusCode] !== null) {
    	      $status_string = $statusCode . ' ' . $status_codes[$statusCode];
    	      header($_SERVER['SERVER_PROTOCOL'] . ' ' . $status_string, true, $statusCode);
    	  }

	 }

  	 public function get_function ($methods, $method, $http_method, $end_point) {

    	  if(!$end_point) {
    	    $end_point = "default";
    	  }

    	  $method_item =      $this->find_in_array_by_key($methods, $method);
    	  $http_method_item = $this->find_in_array_by_key($method_item, $http_method);
    	  $function =         $this->find_in_array_by_key($http_method_item, $end_point);

  	    return $function;
  	 }

  	 private function find_in_array_by_key ($arr, $key) {
    	  $results = array_filter($arr, function ($item) use ($key) {
    	    return $item === $key;
    	  }, ARRAY_FILTER_USE_KEY);

    	  if(isset($results) && !empty($results) && count($results) === 1) {
      	    return $results[$key];
    	  } else {
    	    return [];
    	  }

  	 }
}
?>
