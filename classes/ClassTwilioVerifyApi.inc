<?php
/**
 * @class		TwilioVerifyApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioVerifyApi {
	
    public $db;
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           	=   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    } // end function

    /**
     * @method		createVerificationService
     */
    public function createVerificationService($OrgID, $friendly_name) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	 
    	try {
    		
    		$service_info	=	$client->verify->v2->services->create($friendly_name);
    		
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-verification-service-api.txt", serialize($service_info), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Created verification service successfully", "Response"=>$service_info);
    		 
    	}
    	catch (RestException | TwilioException | Exception $e) {

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-create-verification-service-api-error.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create verification service", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    		
    	}
    	 
    }

    /**
     * @method		sendVerificationTokenInfo
     */
    public function sendVerificationTokenInfo($OrgID, $service_id, $phone_number) {

    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    
			$verification	=	$client->verify->v2->services($service_id)
                                   ->verifications
                                   ->create($phone_number, "sms");
    		
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-send-verification-token-api.txt", serialize($verification), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Sent verification token successfully", "Response"=>$verification);
    		 
    	}
    	catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-send-verification-token-api-error.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to sent verification token", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }

    /**
     * @method		sendEmailVerificationTokenInfo
     */
    public function sendEmailVerificationTokenInfo($OrgID, $service_id, $email) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    
			$verification	= 	$client->verify->v2->services($service_id)
									->verifications
									->create($email, "email");
			    		    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-send-email-verification-token-api.txt", serialize($verification), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Sent verification token successfully", "Response"=>$verification);
    		 
    	}
    	catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-send-email-verification-token-api-error.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to sent verification token", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    
    }
    
    /**
     * @method		checkTheVerificationToken
     */
    public function checkTheVerificationToken($OrgID, $service_id, $phone_number, $code) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
		$verification_check     =       $client->verify->v2->services($_SESSION['TwilioVerifyServiceID'])
                                         ->verificationChecks
                                         ->create([
                                                      "to" => $phone_number,
                                                      "code" => $_REQUEST['ReceivedCode']
                                                  ]
					  );

    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-check-the-verification-token-api.txt", serialize($verification_check), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Checked the verification token successfully", "Response"=>$verification_check);
    		 
    	}
    	catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-check-the-verification-token-api-error.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed get the lookup phone number info", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	
    }
    
}
