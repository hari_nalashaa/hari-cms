<?php
/**
 * @class		EntryMethods
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class EntryMethods {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		insEntryMethodsInfo
	 * @param		array $info
	 * @return		array
	 */
	function insEntryMethodsInfo($info, $on_update = '', $update_info = array()) {
	    
	    $ins_org_info = $this->db->buildInsertStatement('EntryMethods', $info);
	    
	    $insert_statement = $ins_org_info ["stmt"];
	    if ($on_update != '') {
	        $insert_statement .= $on_update;
	        if (is_array ( $update_info )) {
	            foreach ( $update_info as $upd_key => $upd_value ) {
	                $ins_org_info ["info"][0][$upd_key] = $upd_value;
	            }
	        }
	    }
	    
	    $res_org_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $ins_org_info["info"] );
	    
	    return $res_org_info;
	}
	
	/**
	 * @method		getEntryMethodInfoFromOrgIDMultiOrgID
	 * @param		$OrgID, $MultiOrgID
	 */
	function getEntryMethodInfoFromOrgIDMultiOrgID($OrgID, $MultiOrgID) {
	    
	    //Set parameters for prepared query
	    $params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	    $sel_info  =   "SELECT * FROM EntryMethods WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
	    $res_info  =   $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_info, array($params));
	    
	    return $res_info;
	}
	
	/**
	 * @method		getEntryMethodsInfo
	 * @param		string $OrgID
	 * @return		associative array
	 */
	function getEntryMethodsInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
	    
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	    
	    $sel_entry_methods_info = "SELECT $columns FROM EntryMethods";
	    if(count($where_info) > 0) {
	        $sel_entry_methods_info .= " WHERE " . implode(" AND ", $where_info);
	    }
	    
	    if($order_by != "") $sel_entry_methods_info .= " ORDER BY " . $order_by;
	    
	    $res_entry_methods_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_entry_methods_info, $info );
	    
	    return $res_entry_methods_info;
	}
	
}
