<?php
/**
 * @class		WOTCCounties
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCCounties
{
    public $db;
    
	var $conn_string       =   "WOTC";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method     getCounties 
	 * @param      string $state
	 * @param      string $wotcID
	 * @param      string $ApplicationID
	 * @return     string
	 */
    public function getCounties($state,$wotcID,$ApplicationID)
    {

	$rtn = "";

        $query = "SELECT County FROM ApplicantData WHERE wotcID = :wotcID AND ApplicationID = :ApplicationID";
        $params =   array(':wotcID'=>$wotcID,':ApplicationID'=>$ApplicationID);
        list ($County) = $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));

        $query = "SELECT * FROM Counties WHERE State = :State";
        $query .= " ORDER BY County";
        $params =   array(':State'=>$state);
        $COUNTIES = $this->db->fetchAllAssoc($query, array($params));

        $rtn .= '<option value="">Select</option>';

        foreach ($COUNTIES['results'] AS $C) {

          $rtn .= '<option value="' . $C['County'] . '"';
          if ($C['County'] == $County) {
            $rtn .= ' selected';
          }
          $rtn .= '>' . $C['County'] . '</option>';

        } // end foreach

	return $rtn;

    } // end function
    
} // end Class
