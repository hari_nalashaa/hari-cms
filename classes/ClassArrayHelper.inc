<?php
/**
 * @class		ArrayHelper
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ArrayHelper {

    public $db;
    
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * Remove an element from an array.
     */
    function removeElement($element, $array){
        $index = array_search($element, $array);
        if($index !== false){
            unset($array[$index]);
        }
        
        $array = array_values($array);
        
        return $array;
    }
    
    /**
     * Remove elements from an array.
     */
    function removeElements($elements, $array){
        
        foreach ($elements as $key=>$value) {
            unset($array[$key]);
        }
        
        $array = array_values($array);
        
        return $array;
    }
}
