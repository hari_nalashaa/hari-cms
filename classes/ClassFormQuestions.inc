<?php
/**
 * @class		FormQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormQuestions {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getQuestionInformation
     * @param		$OrgID, $QuestionID, $FormTable
     * @return		array
     */
    function getQuestionInformation($info, $form_table, $refine = '', $active = '') {
        
        //Set parameters for prepared query
        $sel_que_answer_info = "SELECT * FROM $form_table WHERE QuestionID = :QuestionID AND OrgID = :OrgID";
        if($refine != "") $sel_que_answer_info .= $refine;
        if($active != "") $sel_que_answer_info .= $active;
        
        $row_que_answer_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_que_answer_info, $info );
        
        return $row_que_answer_info;
    }
    
    /**
     * @method		getQuestionDetails
     * @param		$OrgID, $QuestionID, $FormTable
     * @return		array
     */
    function getQuestionDetails($columns = "*", $form_table, $OrgID, $FormID, $QuestionID) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        //Set parameters for prepared query
        $params_answer_info  = array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":QuestionID"=>$QuestionID);
        $sel_que_answer_info = "SELECT $columns FROM $form_table WHERE OrgID = :OrgID AND FormID = :FormID AND QuestionID = :QuestionID";
        $row_que_answer_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_que_answer_info, array($params_answer_info));
        
        return $row_que_answer_info;
    }
    
    /**
     * @method		getFormQuestionsInformation
     * @param		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getFormQuestionsInformation($columns = "", $where_info = array(), $order_by = "", $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $sel_frm_que_info = "SELECT $columns FROM FormQuestions";
        if(count($where_info) > 0) {
            $sel_frm_que_info .= " WHERE " . implode(" AND ", $where_info);
        }

        if($order_by != "") $sel_frm_que_info .= " ORDER BY " . $order_by;
        
        $res_frm_que_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, $info );
        
        return $res_frm_que_info;
    }
    
    /**
     * @method		getFormQuestionsAndTemplatesInfo
     * @param		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getFormQuestionsAndTemplatesInfo($table_name, $columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $sel_frm_que_info = "SELECT $columns FROM $table_name";
        if(count($where_info) > 0) {
            $sel_frm_que_info .= " WHERE " . implode(" AND ", $where_info);
        }
        
        if($group_by != "") $sel_frm_que_info .= " GROUP BY " . $group_by;
        if($order_by != "") $sel_frm_que_info .= " ORDER BY " . $order_by;

        $res_frm_que_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, $info );
        
        return $res_frm_que_info;
    }
    
    /**
     * @method		insDefaultFormQuestions
     * @param		$OrgID, $new_name, $form
     * @date		January 20 2016
     */
    public function insDefaultFormQuestions($OrgID, $new_name, $form) {
        
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID, ":FormID"=>$form);
        
        $sel_frm_cert_sec = "SELECT Question FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID";
        $row_frm_cert_sec = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_frm_cert_sec, array($params));
        
        $ins_form_ques = "INSERT INTO FormQuestions (OrgID, FormID, QuestionID , ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate)
						  SELECT '" . $OrgID . "', '" . $new_name . "', QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate
						  FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID ON DUPLICATE KEY UPDATE Question = '".$row_frm_cert_sec['Question']."'";
        $res_form_ques = $this->db->getConnection ( $this->conn_string )->insert($ins_form_ques, array($params));
        
        return $res_form_ques;
    }
    
    /**
     * @method		insMasterFormQuestions
     * @param		$OrgID, $new_name, $form
     * @date		January 20 2016
     */
    public function insMasterFormQuestions($OrgID) {
        
        $del_form_ques = "DELETE FROM FormQuestions WHERE OrgID = '".$OrgID."'";
        $res_form_ques = $this->db->getConnection ( $this->conn_string )->delete($del_form_ques);
        
        $ins_form_ques = "INSERT INTO FormQuestions (OrgID, FormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate)
						  SELECT '" . $OrgID . "', FormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, SectionID, QuestionOrder, Required, Validate
						  FROM FormQuestions WHERE OrgID = 'MASTER'";
        $res_form_ques = $this->db->getConnection ( $this->conn_string )->insert($ins_form_ques);
        
        return $res_form_ques;
    }
    
    /**
     * @method		insFormQuestions
     * @param		$fq_info
     */
    public function insFormQuestions($table_name, $fq_info) {
        
        $ins_form_ques = $this->db->buildInsertStatement($table_name, $fq_info);
        $res_form_ques = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_ques["stmt"], $ins_form_ques["info"] );
        
        return $res_form_ques;
    }
    
    /**
     * @method		delQuestionsInfo
     * @param		$table_name, $where_info = array(), $info
     */
    public function delQuestionsInfo($table_name, $where_info = array(), $info) {
        
        //Set parameters for prepared query
        $del_que_info = "DELETE FROM $table_name";
        
        if(count($where_info) > 0) $del_que_info .= " WHERE " . implode(" AND ", $where_info);
        $res_que_info = $this->db->getConnection( $this->conn_string )->delete($del_que_info, $info);
        
        return $res_que_info;
    }

    /**
     * @method		getFormQuestionsListByQueTypeID
     * @param		$OrgID, $OnboardFormID, $QuestionTypeID
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getFormQuestionsListByQueTypeID($OrgID, $FormID, $SectionID = '', $QuestionTypeID) {
        
        $columns 		=	$this->db->arrayToDatabaseQueryString ( $columns );
        
        $params_info	=	array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":QuestionTypeID"=>$QuestionTypeID);
        $sel_que_info	=	"SELECT * FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND QuestionTypeID = :QuestionTypeID";
        
        if($SectionID != "") {
            $sel_que_info	.=	" AND SectionID = :SectionID";
            $params_info[':SectionID'] = $SectionID;
        }
        
        $res_que_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_que_info, array($params_info) );
        
        return $res_que_info;
    }
    
    /**
     * @method		getFormIdsByOrgID
     * @param		$OrgID
     */
    public function getFormIdsByOrgID($OrgID) {
        
        //Set parameters for prepared query
        $params_info	=	array(":OrgID"=>$OrgID);
        $sel_form_ids	=	"SELECT FormID FROM FormQuestions WHERE OrgID = :OrgID GROUP BY FormID";
        $res_form_ids 	=	$this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_form_ids, array($params_info));
        $form_ids		=	$res_form_ids['results'];
        
        $form_ids_list	=	array();
        for($f = 0; $f < count($form_ids); $f++) {
            $form_ids_list[$form_ids[$f]['FormID']]	=	$form_ids[$f]['FormID'];
        }
        
        return $form_ids_list;
    }
    
    /**
     * @method		updQuestionsInfo
     * @param		$set_info = array(), $where_info = array(), $info
     */
    public function updQuestionsInfo($table_name, $set_info = array(), $where_info = array(), $info) { 
        
        $upd_que_info = $this->db->buildUpdateStatement($table_name, $set_info, $where_info); 
        $res_que_info = $this->db->getConnection( $this->conn_string )->update($upd_que_info, $info);
        
        return $res_que_info;
    }
    
    /**
     * @method		insFormQuestionsMaster
     * @param
     */
    function insFormQuestionsMaster($table_name, $columns, $where_info = array(), $info) {
        
        $ins_form_que_master = "INSERT INTO $table_name";
        
        $ins_form_que_master .= " (SELECT $columns FROM $table_name";
        
        if(count($where_info) > 0) $ins_form_que_master .= " WHERE " . implode(" AND ", $where_info);
        
        $ins_form_que_master .= ")";
        $res_form_que_master = $this->db->getConnection( $this->conn_string )->insert($ins_form_que_master, $info);
        
        return $res_form_que_master;
    }
    
    /**
     * @method		getQuestionsInformation
     * @param		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getQuestionsInformation($table_name, $columns = "", $where_info = array(), $order_by = "", $info = array()) {
        
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        
        $sel_frm_que_info = "SELECT $columns FROM $table_name";
        if(count($where_info) > 0) {
            $sel_frm_que_info .= " WHERE " . implode(" AND ", $where_info);
        }
        
        if($order_by != "") $sel_frm_que_info .= " ORDER BY " . $order_by;
        
        $res_frm_que_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, $info );
        
        return $res_frm_que_info;
    }
}
