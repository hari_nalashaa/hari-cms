<?php
/**
 * @class       FormData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormData {

	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		getWebFormData
	 */
	function getWebFormData($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_web_form_data = "SELECT $columns FROM WebFormData";

		if(count($where_info) > 0) $sel_web_form_data .= " WHERE " . implode(" AND ", $where_info);
		if($group_by != "") $sel_web_form_data .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_web_form_data .= " ORDER BY " . $order_by;
		
		$res_web_form_data = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_web_form_data, $info );
	
		return $res_web_form_data;
	}
	
	/**
	 * @method		getPreFilledFormData
	 */
	function getPreFilledFormData($columns = "*", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_pre_filled_form_data = "SELECT $columns FROM PreFilledFormData";

		if(count($where_info) > 0) $sel_pre_filled_form_data .= " WHERE " . implode(" AND ", $where_info);
		if($group_by != "") $sel_pre_filled_form_data .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_pre_filled_form_data .= " ORDER BY " . $order_by;
	
		$res_pre_filled_form_data = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_pre_filled_form_data, $info );
	
		return $res_pre_filled_form_data;
	}
	
	/**
	 * @method		getAgreementFormData
	 * @return
	 */
	function getAgreementFormData($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_agreement_form_data = "SELECT $columns FROM AgreementFormData";
		
		if(count($where_info) > 0) $sel_agreement_form_data .= " WHERE " . implode(" AND ", $where_info);
		if($group_by != "") $sel_agreement_form_data .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_agreement_form_data .= " ORDER BY " . $order_by;
		
		$res_agreement_form_data = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_agreement_form_data, $info );
	
		return $res_agreement_form_data;
	}
	
	/**
	 * @method		getWebFormDataQuestionInfo
	 * @param		
	 */
	public function getWebFormDataQuestionInfo($columns, $OrgID, $ApplicationID, $RequestID, $WebFormID, $QuestionID) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_que_info  = "SELECT $columns FROM WebFormData";
		$sel_que_info .= " WHERE OrgID = :OrgID";
		$sel_que_info .= " AND ApplicationID = :ApplicationID";
		$sel_que_info .= " AND RequestID = :RequestID";
		$sel_que_info .= " AND WebFormID = :WebFormID";
		$sel_que_info .= " AND QuestionID = :QuestionID";
		
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":WebFormID"=>$WebFormID, ":QuestionID"=>$QuestionID);
		$res_que_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_que_info, array($params) );
	
		return $res_que_info;
	}

	/**
	 * @method		getPreFilledFormDataQuestionInfo
	 * @param
	 */
	public function getPreFilledFormDataQuestionInfo($columns, $OrgID, $ApplicationID, $RequestID, $PreFilledFormID, $QuestionID) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_que_info  = "SELECT $columns FROM PreFilledFormData";
		$sel_que_info .= " WHERE OrgID = :OrgID";
		$sel_que_info .= " AND ApplicationID = :ApplicationID";
		$sel_que_info .= " AND RequestID = :RequestID";
		$sel_que_info .= " AND PreFilledFormID = :PreFilledFormID";
		$sel_que_info .= " AND QuestionID = :QuestionID";
	
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QuestionID);
		$res_que_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_que_info, array($params) );
	
		return $res_que_info;
	}
	
	/**
	 * @method		getAgreementFormDataQuestionInfo
	 * @param
	 */
	public function getAgreementFormDataQuestionInfo($columns, $OrgID, $ApplicationID, $RequestID, $AgreementFormID, $QuestionID) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_que_info  = "SELECT $columns FROM AgreementFormData";
		$sel_que_info .= " WHERE OrgID = :OrgID";
		$sel_que_info .= " AND ApplicationID = :ApplicationID";
		$sel_que_info .= " AND RequestID = :RequestID";
		$sel_que_info .= " AND AgreementFormID = :AgreementFormID";
		$sel_que_info .= " AND QuestionID = :QuestionID";
	
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":AgreementFormID"=>$AgreementFormID, ":QuestionID"=>$QuestionID);
		$res_que_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_que_info, array($params) );
	
		return $res_que_info;
	}
}	
?>
