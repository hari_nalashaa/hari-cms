<?php
class ADMIN
{
    public $db;

    var $conn_string       =   "ADMIN";


    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      adminHeader
     * @return      string
     */
    public function adminHeader()
    {
        $rtn = "";
        $rtn .= "<!DOCTYPE html>\n";
        $rtn .= "<html>\n";
        $rtn .= "<head>\n";
        $rtn .= "<meta charset=\"UTF-8\">\n";
        $rtn .= "<title>CMS Administration</title>\n";
        
        $rtn .= "<!-- Favicons-->\n";
        $rtn .= "<link rel=\"shortcut icon\" href=\"" . ADMIN_HOME . "images/fav.png?v=3\">\n";
        $rtn .= "<link rel=\"icon\" href=\"" . ADMIN_HOME . "images/fav.png?v=3\">\n";
        $rtn .= "<link rel=\"apple-touch-icon\" href=\"" . ADMIN_HOME . "images/fav.png?v=3\">\n";
        $rtn .= "<link rel=\"apple-touch-icon-precomposed\" href=\"icon\">\n";
        
        $load_jquery = "false";

        // WOTC Specific
        if (strstr($_REQUEST["pg"], "wotc")) {

            $rtn .= "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "wotc/css/masteradminwotc.css?v=1.0\">\n";

            // processing form
            $rtn .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-1.12.4.js\"></script>\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-ui.min.js\"></script>\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "wotc/javascript/masteradminwotc.js\"></script>\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "wotc/javascript/wotcfunctions.js\"></script>\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "tiny_mce/tinymce.min.js\"></script>\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/irec_Textareas.js\"></script>\n";

            $load_jquery = "true";
        }
        // WOTC Specific
        
        // iRecruit Specific
        if (strstr($_REQUEST["pg"], "irecruit")) {
            $rtn .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "css/jquery.simple-dtpicker.css\">\n";
            $rtn .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
            $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-1.7.2.min.js\"></script>\n";
            $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "calendar/js/jquery-ui-1.8.4.custom.min.js\"></script>\n";
            $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "jscolor/jscolor.js\"></script>\n";
            $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery.simple-dtpicker.js\"></script>\n";
            
            $load_jquery = "true";
        }
        
        if($load_jquery == "false") {
            // processing form
            $rtn .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"" . ADMIN_HOME . "calendar/css/smoothness/jquery-ui-1.8.4.custom.css\" />\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-1.12.4.js\"></script>\n";
            $rtn .= "<script type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/jquery-ui.min.js\"></script>\n";
        }
        
        // iRecruit Specific
        $rtn .= "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "css/admin.css?v=2.0\">\n";
        $rtn .= "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "css/modal.css?v=2.0\">\n";
        $rtn .= "<link rel=\"stylesheet\" href=\"" . ADMIN_HOME . "css/custom-styles.css?v=2.0\">\n";
        $rtn .= "<script language=\"JavaScript\" type=\"text/javascript\" src=\"" . ADMIN_HOME . "javascript/admin.js\"></script>\n";
        $rtn .= "</head>\n";
        $rtn .= "<body>\n";
        
        if (strstr($_SERVER["SCRIPT_NAME"], "index")) {
            $rtn .= $this->adminNavigation();
        }
        
        $rtn .= "<div id=\"content\">\n";
        
        return $rtn;
    } // end function
    
    
    /**
     * @method      adminFooter
     * @return      string
     */
    public function adminFooter()
    {
        global $IntuitObj;
        
        $rtn = "";
        
        $days_diff = $IntuitObj->getRefreshTokenExpireDaysDiff();

        $notification_days = 100 - $days_diff;
        
        $rtn .= "<script type=\"text/javascript\">\n";
        $rtn .= "$(document).ready(function(){\n";
        $rtn .= "$('#ui-datepicker-div').hide();\n";
        $rtn .= "});";
        $rtn .= "</script>";
        $rtn .= "</div>\n";
        
        $rtn .= "</div>\n";
        
        return $rtn;
    } // end function

    /**
     * @method      getUserInformation
     * @param       string $userid
     */
    public function getUserInformation($userid)
    {
        $sel_info =   "SELECT UserID, ActivationDate, LastAccess, Email, iRecruitAccess, wotcAccess FROM Users WHERE UserID = :userid";
        $params =   array(':userid'=>$userid);
        $USERS_RES  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($sel_info, array($params));
        $USERINFO      =   $USERS_RES['results'][0];
        
        return $USERS_RES;

    } // end function
    
    /**
     * @method      listUsers
     * @return      string
     */
    public function listUsers()
    {
        $rtn = "";
        
        $sel_info   =   "SELECT UserID, DATE_FORMAT(ActivationDate,'%m/%d/%Y') AS ActivationDate, DATE_FORMAT(LastAccess,'%m/%d/%Y %h:%i %p') AS LastAccess, Email, iRecruitAccess, wotcAccess FROM Users ORDER BY UserID";
        $USERS_RES  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($sel_info);
        $USERS      =   $USERS_RES['results'];
        
        $rtn .= "<div style=\"font-size:12pt;font-weight:bold;:\">";
        $rtn .= "Admin Users";
        $rtn .= "</div>\n";
        $rtn .= "<div style=\"line-height:180%;margin:20px 0 20px 10px;\">\n";
        
        foreach ($USERS as $U) {
            
            $rtn .= "&nbsp;&nbsp;&nbsp;";
            $rtn .= "<span style=\"font-size:10pt;font-weight:bold;\">";
            $rtn .= $U['UserID'];
            $rtn .= "</span>";
            $rtn .= "&nbsp;&nbsp;&nbsp;";
            $rtn .= "User Since: " . $U['ActivationDate'];
            $rtn .= "&nbsp;&nbsp;&nbsp;";
            $rtn .= "Last Access: " . $U['LastAccess'];
            $rtn .= "&nbsp;&nbsp;&nbsp;";
            $rtn .= "Email: " . $U['Email'] . "<br>\n";
            $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            $rtn .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            $rtn .= "iRecruitAccess: " . $U['iRecruitAccess'] . "&nbsp;&nbsp;";
            $rtn .= "wotcAccess: " . $U['wotcAccess'] . "\n";
            $rtn .= "<br><br>\n";
        } // end foreach
        
        $rtn .= "</div>\n";
        
        return $rtn;
    } // end function
    
    
    /**
     * @method      authenticate
     * @param       string $cookie
     * @return      
     */
    public function authenticate($cookie)
    {
        $AUTH = array();

        // general lockout for time
        $query  =   "UPDATE Users SET SessionID = '' WHERE LastAccess <= DATE_SUB(NOW(), INTERVAL 90 MINUTE) AND (Persist IS NULL OR Persist = 0)";
        $res    =   $this->db->getConnection ( $this->conn_string )->update ( $query );
        
        // bounce to login page if no COOKIE
        if (! $cookie) {
            header("Location: " . ADMIN_HOME . "login.php");
            exit;
        }

        // bounce if COOKIE not identifyable
        $params     =   array(':SessionID' => $cookie);
        $sel_info   =   "SELECT UserID, cast(Persist as UNSIGNED int) as Persist, iRecruitAccess, wotcAccess FROM Users WHERE SessionID = :SessionID";
        $AUTH       =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($sel_info, array($params));

        if ($AUTH['UserID'] == "") {
            header("Location: " . ADMIN_HOME . "login.php");
            exit;
        }
        
        // general lockout for time
        $query  =   "UPDATE Users SET LastAccess = NOW() WHERE UserID = :UserID";
        $params =   array(":UserID"=>$AUTH['UserID']);
        $res    =   $this->db->getConnection ( $this->conn_string )->update ( $query, array($params) );
        
        if ($AUTH['Persist'] == 1) {
            setcookie("AID", $cookie, time() + 2592000, "/");
        }        
        
        return $AUTH;
    } // end function
    
} // end Class

