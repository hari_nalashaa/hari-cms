<?php
/**
 * @class		ValidateWotcForm
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCValidateForm {
	
    var $conn_string        =   "IRECRUIT";
    var $FORMDATA           =   array();
    var $ERRORS             =   array();
    var $WotcID             =   "";
    var $WotcFormID         =   "";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    
    /**
     * @method      validateWotcForm
     * @param       array $qi
     */
    function validateWotcForm($WotcID, $WotcFormID) {

        $this->WotcID       =   $WotcID;
        $this->WotcFormID   =   $WotcFormID;
        
        $form_que_res   =   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsByWotcIDFormID($WotcID, $WotcFormID);
        $parent_ques	=	G::Obj('WOTCFormQuestions')->getWotcFormParentQuesForChildQues($WotcID, $WotcFormID);

        foreach($form_que_res  as $QuestionID=>$QI)
        {            
        	//Validate only Active questions
        	if($QI['Active'] == "Y") {
        		$QuestionTypeID =   $QI['QuestionTypeID'];
        		$Question       =   trim($QI['Question'], ":");
        		$ParentQueID	=	$parent_ques[$QuestionID];
        		$ChildQuesInfo	=	json_decode($form_que_res[$ParentQueID]['ChildQuestionsInfo'], true);
        		
        		//Validate required fields
        		if($QI['Required'] == 'Y') {
        			if($parent_ques[$QuestionID] != "") {
        				$parent_que_val_info	=	$ChildQuesInfo[$this->FORMDATA['REQUEST'][$ParentQueID]];
        		
        				if($parent_que_val_info[$QuestionID] == "show") {
        					call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QuestionID, $Question);
        				}
        			}
        			else {
						if($QuestionID == "verify") {
							//Special condition
        					if($_POST['Initiated'] == "callcenter") {
        						call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QuestionID, $Question);
        					}
        				}
        				else {
        					if($_POST['Initiated'] == "callcenter") {
        						
        						if(trim($QuestionID) != "captcha") {
        							call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QuestionID, $Question);
        						}
        					}
        					else {
        						call_user_func( array( $this, 'validateQuestionType'.$QuestionTypeID ), $QuestionID, $Question);
        					}
        				}
        			}
        		}
        	}
        }
        
        return $this->ERRORS;
    }
    

    /**
     * @method      validateQuestionType2
     * @input_type  radio
     */
    function validateQuestionType2($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType3
     * @input_type  pulldown
     */
    function validateQuestionType3($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType5
     * @input_type  textarea
     */
    function validateQuestionType5($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    

    /**
     * @method      validateQuestionType6
     * @input_type  text
     */
    function validateQuestionType6($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }

    
    /**
     * @method      validateQuestionType8
     * @input_type  file
     */
    function validateQuestionType8($QuestionID, $Question) {
        if($this->FORMDATA['FILES'][$QuestionID]['name']  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType13
     * @input_type  phone
     */
    function validateQuestionType13($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID."1"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."2"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."3"]  ==  ""
            ) {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType14
     * @input_type  phone-with-extension
     */
    function validateQuestionType14($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID."1"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."2"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."3"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."ext"]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType15
     * @input_type  social-security
     */
    function validateQuestionType15($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID."1"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."2"]  ==  ""
            || $this->FORMDATA['REQUEST'][$QuestionID."3"]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType17
     * @input_type  date
     */
    function validateQuestionType17($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        //Validate Question Type 17
        $results	=   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsListByQueTypeID($this->WotcID, $this->WotcFormID, '17');
        
        $validate_dates_list	=	array();
        $questions_info			=	array();
        if(is_array($results['results'])) {
            foreach($results['results'] as $FQCK) {
                $validate_dates_list[$FQCK['QuestionID']] = json_decode($FQCK['Validate'], true);
                $questions_info[$FQCK['QuestionID']]['Question'] = $FQCK['Question'];
            }
        }
        
        foreach ($validate_dates_list as $question_id=>$validate_que_info) {
            if($validate_que_info['years_min'] != "") {
                
                if($_POST [$question_id] != "") {
                    
                    //date information
                    $date_ans			=	G::Obj('DateHelper')->getYmdFromMdy($_POST [$question_id]);
                    $d1 				=	new DateTime(date('Y-m-d'));
                    $d2 				=	new DateTime($date_ans);
                    
                    $ans_date_diff		=	$d2->diff($d1);

                    if($ans_date_diff->y < $validate_que_info['years_min']) {
                        $this->ERRORS[$question_id] =  $questions_info[$question_id]['Question'] . ' date needs to be more than '.$validate_que_info['years_min'].' years';
                    }
                    
                }
            }
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType18
     * @input_type  checkbox
     */
    function validateQuestionType18($QuestionID, $Question) {
        $count  =   $this->FORMDATA['REQUEST'][$QuestionID.'cnt'];
        if($this->FORMDATA['REQUEST'][$QuestionID.'cnt']  >  0) {
            $error_status  =   "0";
            for($j = 1; $j <= $count; $j++) {
                if($this->FORMDATA['REQUEST'][$QuestionID.'-'.$j] != "") {
                	$error_status  =   "1";
                }
            }
            
            if($error_status == "0") {
                $this->ERRORS[$QuestionID] =   $Question;
            }
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType22
     * @input_type  radio-list
     */
    function validateQuestionType22($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    

    /**
     * @method      validateQuestionType23
     * @input_type  radio-long-question
     */
    function validateQuestionType23($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType25
     * @input_type  date-time
     */
    function validateQuestionType25($QuestionID, $Question) {
        if($this->FORMDATA['REQUEST'][$QuestionID]  ==  "") {
            $this->ERRORS[$QuestionID] =   $Question;
        }
        
        return $this->ERRORS;
    }

    /**
     * @method      validateQuestionType40
     */
    function validateQuestionType40() {
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType45
     */
    function validateQuestionType45() {
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType99
     */
    function validateQuestionType99() {
        return $this->ERRORS;
    }
    
    /**
     * @method      validateQuestionType100
     * @input_type  radio-with-group-skills
     */
    function validateQuestionType100($QuestionID, $Question) {
        if(is_array($this->FORMDATA['REQUEST']['LabelSelect'][$QuestionID])) {
            foreach($this->FORMDATA['REQUEST']['LabelSelect'][$QuestionID] as $cus_que100_label=>$cus_que100_value) {
                if(is_array($cus_que100_value)) {
                    foreach($cus_que100_value as $cus_que100_lbl_info=>$cus_que100_lbl_val) {
                        if($cus_que100_lbl_val == "") {
                            $this->ERRORS[$QuestionID] =   $Question;
                        }
                    }
                }
            }
        }
        
        
        return $this->ERRORS;
    }
    
    
    /**
     * @method      validateQuestionType120
     * @input_type  pulldown-shifts
     */
    function validateQuestionType120($QuestionID, $Question) {
		if(is_array($this->FORMDATA['REQUEST']['shifts_schedule_time'][$QuestionID]['from_time'])) {
			$from_available_flag = false;
			foreach($this->FORMDATA['REQUEST']['shifts_schedule_time'][$QuestionID]['from_time'] as $cus_que120_from_val) {
				if($cus_que120_from_val == "") {
					$from_available_flag = true;
				}
			}
			if($from_available_flag == true) 
			{
				$this->ERRORS[$QuestionID] =    $Question;
			}	
		}
		if(is_array($this->FORMDATA['REQUEST']['shifts_schedule_time'][$QuestionID]['to_time'])) {
			$to_available_flag = false;
			foreach($this->FORMDATA['REQUEST']['shifts_schedule_time'][$QuestionID]['to_time'] as $cus_que120_to_val) {
				if($cus_que120_to_val == "") {
					$to_available_flag = true;
				}							
			}
			if($to_available_flag == true) {
				$this->ERRORS[$QuestionID] =    $Question;
			} 
		}
    }
    
    
    /**
     * @method      validateQuestionType1818
     * @input_type  checkboxes-list
     */
    function validateQuestionType1818($QuestionID, $Question) {
        $count  =   $this->FORMDATA['REQUEST'][$QuestionID.'cnt'];
        if($this->FORMDATA['REQUEST'][$QuestionID.'cnt']  >  0) {
            $error_status  =   "0";
            for($j = 1; $j <= $count; $j++) {
                if($this->FORMDATA['REQUEST'][$QuestionID.'-'.$j] != "") {
                	$error_status  =   "1";
                }
            }
            
            if($error_status == "0") {
                $this->ERRORS[$QuestionID] =   $Question;
            }
        }
    }
}
?>
