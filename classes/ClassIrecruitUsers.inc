<?php
/**
 * @class		IrecruitUsers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IrecruitUsers {
	
	public $db;
	
	var $conn_string		=   "IRECRUIT";
	
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
    /**
     * @method      getAllUsersCount
     * @return      integer
     */
	function getAllUsersCount() {
	    
	    $columns       =   $this->db->arrayToDatabaseQueryString ( $columns );
	    $sel_count     =   "SELECT COUNT(*) AS UsersCount FROM Users";
	    $res_count     =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_count );
	    
	    return $res_count['UsersCount'];
	}
	
    /**
     * @method      getUserInformation
     * @param 		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial 	This method will fetch the form questions informtaion
     */
	function getUserInformation($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		
		$sel_user_info = "SELECT $columns FROM Users";
		
		if (count ( $where_info ) > 0) {
			$sel_user_info .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		if ($order_by != "") $sel_user_info .= " ORDER BY " . $order_by;
		$res_user_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_user_info, $info );
		
		return $res_user_info;
	}
	
	/**
	 * @method		insUsers
	 * @param		array $info
	 * @return		array
	 */
	function insUsers($info) {

               // Users Information
                $params = array (
                                ":UserID"            =>  $info['UserID'],
                                ":Verification"      =>  $info['Verification'],
                                ":OrgID"             =>  $info['OrgID'],
                                ":Role"              =>  $info['Role'],
                                ":AccessCode"        =>  $info['AccessCode'],
                                ":FirstName"         =>  $info['FirstName'],
                                ":LastName"          =>  $info['LastName'],
                                ":Address1"          =>  $info['Address1'],
                                ":Address2"          =>  $info['Address2'],
                                ":City"              =>  $info['City'],
                                ":State"             =>  $info['State'],
                                ":ZipCode"           =>  $info['ZipCode'],
                                ":Phone"             =>  $info['Phone'],
                                ":Cell"              =>  $info['Cell'],
                                ":Title"             =>  $info['Title'],
                                ":EmailAddress"      =>  $info['EmailAddress'],
                                ":ApplicationView"   =>  $info['ApplicationView'],
                                ":CostCenter"        =>  $info['CostCenter'], 
                                ":UVerification"     =>  $info['Verification'],
                                ":URole"             =>  $info['Role'],
                                ":UAccessCode"       =>  $info['AccessCode'],
                                ":UFirstName"        =>  $info['FirstName'],
                                ":ULastName"         =>  $info['LastName'],
                                ":UAddress1"         =>  $info['Address1'],
                                ":UAddress2"         =>  $info['Address2'],
                                ":UCity"             =>  $info['City'],
                                ":UState"            =>  $info['State'],
                                ":UZipCode"          =>  $info['ZipCode'],
                                ":UPhone"            =>  $info['Phone'],
                                ":UCell"             =>  $info['Cell'],
                                ":UTitle"            =>  $info['Title'],
                                ":UEmailAddress"     =>  $info['EmailAddress']
                );

                $ins_users_info = "INSERT INTO Users (UserID, Verification, OrgID, Role, AccessCode, FirstName, LastName, Address1, Address2, City, State, ZipCode, Phone, Cell, Title, EmailAddress, ActivationDate, LastAccess, ApplicationView, CostCenter) VALUES(:UserID, :Verification, :OrgID, :Role, :AccessCode, :FirstName, :LastName, :Address1, :Address2, :City, :State, :ZipCode, :Phone, :Cell, :Title, :EmailAddress, now(), now(), :ApplicationView, :CostCenter) ON DUPLICATE KEY UPDATE Verification = :UVerification, Role = :URole, AccessCode = :UAccessCode, FirstName = :UFirstName, LastName = :ULastName, Address1 = :UAddress1, Address2 = :UAddress2, City = :UCity, State = :UState, ZipCode = :UZipCode, Phone = :UPhone, Cell = :UCell, Title = :UTitle, EmailAddress = :UEmailAddress";

                $res_users = $this->db->getConnection( $this->conn_string )->insert($ins_users_info, array($params));
	
		return $res_users;
	}
	
	/**
	 * @method		delIrecruitUsers
	 * @param 		$where_info, array(), $info = array()
	 */
	function delIrecruitUsers($where_info = array(), $info = array()) {
		
		$del_irecruit_users = "DELETE FROM Users";
		if (count ( $where_info ) > 0) {
			$del_irecruit_users .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		$res_irecruit_users = $this->db->getConnection ( $this->conn_string )->delete ( $del_irecruit_users, $info );
		
		return $res_irecruit_users;
	}
	
	/**
	 * @method		validateUserLogin
	 * @tutorial	Validate the user is authenticated or not
	 * @param		string $user        	
	 * @param		string $password        	
	 * @return 		numeric array
	 */
	function validateUserLogin($user, $password) {
		
		// Set parameters for prepared query
		$params = array (":UserID"=>$user);
		
		$sel_user_info = "SELECT U.UserID, U.Persist, U.OrgID, U.AccessCode, U.Verification,U.EmailAddress,U.TwofEnable,U.FirstName
						FROM Users U, ApplicationPermissions AP 
						WHERE U.UserID = AP.UserID 
						AND AP.Active = 1 
						AND U.UserID = :UserID";
		$res_user_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_user_info, array($params));
		
		if (password_verify($password, $res_user_info['Verification'])) {
			
		    if (password_needs_rehash($password, PASSWORD_DEFAULT)) {
		    	
		    	// Set parameters for prepared query
		    	$params = array (":Verification"=>password_hash( $password, PASSWORD_DEFAULT ), ":UserID"=>$user);
		    	
		    	$upd_user_info = "UPDATE Users SET Verification = :Verification";
		    	$upd_user_info .= " WHERE UserID = :UserID";
		    	
		    	$this->db->getConnection( $this->conn_string )->update ($upd_user_info, array($params));
		    	
		    }
		}
		else {
			$res_user_info = array();
		}
		
		return $res_user_info;
	}
	
	/**
	 * @method		updateUserAccessSession
	 * @tutorial 	update user last access and session id
	 * @return 		associate array
	 */
	function updateUserAccessSession($SESSIONID, $USERID, $keeper = '') {
		
		// Set parameters for prepared query
		$params = array (":SessionID"=>$SESSIONID, ":UserID"=>$USERID);
		
		$upd_user_sess_access = "UPDATE Users SET LastAccess = NOW(), SessionID = :SessionID";
		$upd_user_sess_access .= $keeper;
		$upd_user_sess_access .= " WHERE UserID = :UserID";
		
		$res_user_info = $this->db->getConnection ( $this->conn_string )->update ( $upd_user_sess_access, array (
				$params 
		) );
		
		return $res_user_info;
	}
	
	
	/**
	 * @method		updUserSession
	 * @tutorial 	update user session id related to forgot password
	 * @return 		associate array
	 */
	function updUserSession($SESSIONID, $USERID) {
	
		if($USERID != "") {
			// Set parameters for prepared query
			$params = array (":ForgotPasswordAccessKey"=>$SESSIONID, ":UserID"=>$USERID);
			
			$upd_forgot_sess = "UPDATE Users SET ForgotPasswordAccessKey = :ForgotPasswordAccessKey";
			$upd_forgot_sess .= " WHERE UserID = :UserID";
			
			$res_user_info = $this->db->getConnection ( $this->conn_string )->update ( $upd_forgot_sess, array (
					$params
			) );
			
			return $res_user_info;
		}
		
		return '';
	}
	
	/**
	 * @method		updPassword
	 * @param 		sessionid
	 * @return 		associate array
	 */
	function updPassword($SESSIONID, $Password) {

		// Set parameters for prepared query
		$params = array (":ForgotPasswordAccessKey"=>$SESSIONID, ":Verification"=>$Password);
	
		$upd_forgot_pass = "UPDATE Users SET Verification = :Verification";
		$upd_forgot_pass .= " WHERE ForgotPasswordAccessKey = :ForgotPasswordAccessKey";
	
		$res_info = $this->db->getConnection ( $this->conn_string )->update ( $upd_forgot_pass, array (
				$params
		) );
	
		return $res_info;
	}
	
	/**
	 * @method 		updateUserSessionLockOutTime
	 * @return 		associate array
	 */
	function updateUserSessionLockOutTime() {
		
		$upd_user_lockout_time = "UPDATE Users U, ActiveMinutes AM SET U.SessionID = ''
								WHERE U.OrgID = AM.OrgID
								AND U.SessionID != ''
								AND U.LastAccess <= DATE_SUB(NOW(), INTERVAL AM.Minutes MINUTE)
								AND (U.Persist IS NULL OR U.Persist = 0)";
	
		//$res_user_lockout_time = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_user_lockout_time );
		
		//return $res_user_lockout_time;
	}
	
	/**
	 * @method 		getUserInfoByCookie
	 * @return 		numeric array
	 */
	function getUserInfoByCookie($fields_list) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $fields_list );
		
		// Bind the parameters to query
		$params [":SessionID"] = $_COOKIE ['ID'];
		
		$sel_user_info = "SELECT $columns FROM Users WHERE SessionID = :SessionID";
		$res_user_info = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_user_info, array($params) );
		
		return $res_user_info;
	}
	
	/**
	 * @method		updateUserAccess
	 * @param		$USERID
	 * @return		associate array
	 */
	function updateUserAccess($USERID) {
		
		// Set parameters for prepared query
		$params = array (":UserID"=>$USERID );
		
		$upd_user_access = "UPDATE Users SET LastAccess = NOW() WHERE UserID = :UserID";
		$res_user_access = $this->db->getConnection ( $this->conn_string )->update ( $upd_user_access, array (
				$params 
		) );
		
		return $res_user_access;
	}
	
	/**
	 * @method		getUserInfoByAccessCode
	 * @param 		$k(GET parameter)        	
	 * @return 		$USERID
	 */
	function getUserInfoByAccessCode($k, $fields_list = "*") {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $fields_list );
		
		// Set parameters for prepared query
		$params = array(":AccessCode"=>$k);
		
		$sel_user_info = "SELECT $columns FROM Users WHERE AccessCode = :AccessCode LIMIT 0, 1";
		$res_user_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_user_info, array ( $params ) );
		
		return $res_user_info;
	}
	
	/**
	 * @method		getUserInfoByUserID
	 * @return		numerical array
	 * @param 		$USERID, $fields_list        	
	 */
	function getUserInfoByUserID($UserID, $fields_list = "*") {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $fields_list );
		
		// Set parameters for prepared query
		$params = array (":UserID"=>$UserID);
		
		$sel_user_info = "SELECT $columns FROM Users WHERE UserID = :UserID";
		$res_user_info = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_user_info, array ($params) );
		
		return $res_user_info;
	}
	
	/**
	 * @method		unsetUserSessionByUserID
	 * @param		$USERID
	 */
	function unsetUserSessionByUserID($USERID) {
		
		// Set parameters for prepared update query
		$params [":UserID"] = $USERID;
		
		$upd_user_session =   "UPDATE Users SET SessionID = '', Persist = NULL WHERE UserID = :UserID";
		$res_user_session =   $this->db->getConnection ( $this->conn_string )->update ( $upd_user_session, array ($params) );
	}
	
	/**
	 * @method		updateUserInformation
	 * @param 		array $POST        	
	 * @return 		int
	 */
	function updateUserInformation($POST) {
		global $USERID;
		
		if ($POST == '')
			$POST = file_get_contents ( 'php://input' );
		$JSON = json_decode ( $POST, true );
		
		if($JSON ['UsersVerification'] != "") {
			$JSON ['UsersVerification'] = password_hash( $JSON ['UsersVerification'], PASSWORD_DEFAULT );
		}
				
		// Set parameters for prepared update query
		$params		=	array (
							":Role"				=>	$JSON ['Role'],
							":ApplicationView"	=>	$JSON ['UsersAppView'],
							":FirstName"		=>	$JSON ['UsersFirstName'],
							":LastName"			=>	$JSON ['UsersLastName'],
							":Title"			=>	$JSON ['UsersTitle'],
							":EmailAddress"		=>	$JSON ['UsersEmailAddress'],
							":Address1"			=>	$JSON ['UsersAddress1'],
							":Address2"			=>	$JSON ['UsersAddress2'],
							":City"				=>	$JSON ['UsersCity'],
							":State"			=>	$JSON ['UsersState'],
							":ZipCode"			=>	$JSON ['UsersZipCode'],
							":Phone"			=>	$JSON ['UsersPhone'],
							":Cell"				=>	$JSON ['UsersCell'],
							":UserID"			=>	$USERID
                        );
		
		if($JSON ['UsersVerification'] != "") {
			$params[":Verification"] = $JSON ['UsersVerification'];
		}
		
		$upd_user_info = "UPDATE Users SET";
		if($JSON ['UsersVerification'] != "") {
			$params[":Verification"] = $JSON ['UsersVerification'];
			$upd_user_info .= " Verification = :Verification,";
		}
		
		$upd_user_info .= " Role				=	:Role,
							ApplicationView 	=	:ApplicationView,
							FirstName			=	:FirstName,
							LastName			=	:LastName,
							Title				=	:Title,
							EmailAddress		=	:EmailAddress,
							Address1			=	:Address1,
							Address2			=	:Address2,
							City				=	:City,
							State				=	:State,
							ZipCode				=	:ZipCode,
							Phone				=	:Phone,
							Cell				=	:Cell,
							LastAccess			=	NOW()
							WHERE UserID		=	:UserID";
		
		$res_user_info = $this->db->getConnection ( $this->conn_string )->update ( $upd_user_info, array($params) );
		
		return $res_user_info ['affected_rows'];
	}
	
	/**
	 * @method		updUsersInfo
	 */
	function updUsersInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_req_info = $this->db->buildUpdateStatement('Users', $set_info, $where_info);
		$res_req_info = $this->db->getConnection ( $this->conn_string )->update($upd_req_info, $info);
	
		return $res_req_info;
	}

/**
	 * @method		updateUserOTP
	 * @param		$USERID
	 * @return		associate array
	 */
	function updateUserOtp($USERID,$otp) {
		
		// Set parameters for prepared query
		$params = array (":UserID"=>$USERID,":TwofAuthToken"=>$otp);
		$upd_user_access = "UPDATE Users SET LastAccess = NOW(),TwofAuthToken=:TwofAuthToken WHERE UserID = :UserID";
		$res_user_access = $this->db->getConnection ( $this->conn_string )->update ( $upd_user_access, array ($params) );
		
		return $res_user_access;
	}
	
	/**
	 * @method		getUsersName
	 * @param		$OrgID, $UserID        	
	 */
	function getUsersName($OrgID, $UserID) {
		
		// Set parameters for prepared select query
		$params	=	array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
		
		$sel_full_name = "SELECT CONCAT(FirstName, ' ', LastName) FROM Users WHERE OrgID = :OrgID AND UserID = :UserID";
		$row_full_name = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_full_name, array ($params) );
		
		return $row_full_name [0];
	} // end function
	
	/**
	 * @method		getWelcomeMessage
	 * @return		string
	 */
	function getWelcomeMessage() {
		
		$sel_wel_message = "SELECT Message FROM WelcomeMessage";
		$res_wel_message = $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_wel_message );
		$welcome_message = $res_wel_message ['Message'];
		return $welcome_message;
	}
	
	/**
	 * @method		getApplicationview
	 */
	function getApplicationview($OrgID, $UserID) {
		
		// Set parameters for prepared select query
		$params = array(":OrgID"=>$OrgID, ":UserID"=>$UserID);
		
		$sel_application_view = "SELECT Applicationview FROM Users WHERE OrgID = :OrgID AND UserID = :UserID";
		$res_application_view = $this->db->getConnection ( $this->conn_string )->fetchRow ( $sel_application_view, array (
				$params 
		) );
		
		return $res_application_view;
	}
	
	/**
	 * #############################################################################
	 * ####	UserEmailLists
	 * #############################################################################
	 */
	
	/**
	 * @method		getUserEmailLists
	 * @return		associative array
	 * @tutorial	This method will fetch the user email lists information.
	 */
	public function getUserEmailLists($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_user_email_lists = "SELECT $columns FROM UserEmailLists";
		
		if (count ( $where_info ) > 0) {
			$sel_user_email_lists .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		if ($order_by != "") $sel_user_email_lists .= " ORDER BY " . $order_by;
		$res_user_email_lists = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_user_email_lists, $info );
		
		return $res_user_email_lists;
	}
	
	/**
	 * @method		insUserEmailLists
	 * @param		$info
	 */
	public function insUserEmailLists($info) {
		
		$ins_user_email_lists = $this->db->buildInsertStatement ( 'UserEmailLists', $info );
		$res_user_email_lists = $this->db->getConnection ( $this->conn_string )->insert ( $ins_user_email_lists ["stmt"], $ins_user_email_lists ["info"] );
		
		return $res_user_email_lists;
	}
	
	/**
	 * @method		delUserEmailLists
	 * @param		$OrgID, $item        	
	 */
	public function delUserEmailLists($OrgID, $UserID, $emailaddress) {
		
		// Set parameters for prepared select query
		$params = array (":OrgID"=>$OrgID, ":UserID"=>$UserID, ":EmailAddress"=>$emailaddress);
		
		// Delete UserEmail Lists
		$del_user_email_lists = "DELETE FROM UserEmailLists WHERE OrgID = :OrgID AND UserID = :UserID AND EmailAddress = :EmailAddress";
		$res_user_email_lists = $this->db->getConnection ( $this->conn_string )->delete ( $del_user_email_lists, array ($params) );
		
		return $res_user_email_lists;
	}
	
	/**
	 * @method		updUserEmailLists
	 * @param		$info, $UpdateID        	
	 */
	public function updUserEmailLists($user_email_info) {
		
		// Set parameters for prepared select query
		$params [":EmailAddress"]		=	$user_email_info ['EmailAddress'];
		$params [":FirstName"]			=	$user_email_info ['FirstName'];
		$params [":LastName"]			=	$user_email_info ['LastName'];
		$params [":OrgID"]				=	$user_email_info ['OrgID'];
		$params [":UserID"]				=	$user_email_info ['UserID'];
		$params [":OrgEmailAddress"]	=	$user_email_info ['OrgEmailAddress'];
		
		$upd_user_email_lists = "UPDATE UserEmailLists SET EmailAddress = :EmailAddress, 
								 FirstName = :FirstName, 
								 LastName = :LastName 
								 WHERE OrgID = :OrgID 
								 AND UserID = :UserID 
								 AND EmailAddress = :OrgEmailAddress";
		$res_user_email_lists = $this->db->getConnection ( $this->conn_string )->update ( $upd_user_email_lists, array (
				$params 
		) );
		
		return $res_user_email_lists;
	}
	
	/**
	 * @method		getUserSavedSearches
	 */
	public function getUserSavedSearches($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_info = "SELECT $columns FROM UserSavedSearches";

		if (count ( $where_info ) > 0) {
			$sel_info .= " WHERE " . implode ( " AND ", $where_info );
		}
		if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;
		
		$res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, $info );
		
		return $res_info;
	}
	
	/**
	 * @method		insUserSavedSearches
	 * @param		array $info
	 * @return		array
	 */
	function insUserSavedSearches($info) {
		
		$ins_info = $this->db->buildInsertStatement('UserSavedSearches', $info);
		$insert_user_search = $ins_info["stmt"] . " ON DUPLICATE KEY 
											UPDATE Active = '".$info['Active']."', 
											SearchWords = '".$info['SearchWords']."',
											RequestIDs = '".$info['RequestIDs']."',
											ProcessOrder = '".$info['ProcessOrder']."',
											Code = '".$info['Code']."', 
											ApplicationDate_From = '".$info['ApplicationDate_From']."',
											ApplicationDate_To = '".$info['ApplicationDate_To']."', 
											DataManager = '".$info['DataManager']."',
											LimitReceivedDate = '".$info['LimitReceivedDate']."',
											MultiOrgID = '".$info['MultiOrgID']."'";
		$res_info = $this->db->getConnection ( $this->conn_string )->insert ( $insert_user_search, $ins_info["info"] );
	
		return $res_info;
	}
	
	/**
	 * @method		delUserSavedSearches
	 * @param		$OrgID, $UserID, $SearchName
	 */
	public function delUserSavedSearches($OrgID, $UserID, $SearchName) {
		
		// Set parameters for prepared select query
		$params 	=	array (":OrgID"=>$OrgID, ":UserID"=>$UserID, ":SearchName"=>$SearchName);
	
		// Delete UserEmail Lists
		$del_info	=	"DELETE FROM UserSavedSearches WHERE OrgID = :OrgID AND UserID = :UserID AND SearchName = :SearchName";
		$res_info	=	$this->db->getConnection ( $this->conn_string )->delete ( $del_info, array ($params) );
	
		return $res_info;
	}
}
