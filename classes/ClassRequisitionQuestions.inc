<?php
/**
 * @class		RequisitionQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionQuestions {
	
	var $RequisitionFormID = "";
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getRequisitionQuestions
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getRequisitionQuestions($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_frm_que_info = "SELECT $columns FROM RequisitionQuestions";
		if(count($where_info) > 0) {
			$sel_frm_que_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_frm_que_info .= " ORDER BY " . $order_by;

		$res_frm_que_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_frm_que_info, $info );
	
		return $res_frm_que_info;
	}
	
	/**
	 * @method		getRequisitionQuestionsByQueID
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getRequisitionQuestionsByQueID($columns, $OrgID, $RequisitionFormID) {
	
	    $columns = $this->db->arrayToDatabaseQueryString ( $columns );
	    
        $params_info        =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $sel_frm_que_info   =   "SELECT $columns FROM RequisitionQuestions WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID";	
        $res_frm_que_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_frm_que_info, array($params_info) );
        $res_frm_ques       =   $res_frm_que_info['results'];
        
        $req_questions      =   array();
        
        for($r = 0; $r < count($res_frm_ques); $r++) {
            $req_questions[$res_frm_ques[$r]['QuestionID']]  =   $res_frm_ques[$r];
        }
        
        return $req_questions;
	}
	
	/**
	 * @method		getRequisitionQuestionInfo
	 * @param		$columns
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getRequisitionQuestionInfo($columns = "", $OrgID, $QuestionID, $RequisitionFormID) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$params_info = array(":OrgID"=>$OrgID, ":QuestionID"=>$QuestionID, ":RequisitionFormID"=>$RequisitionFormID);
		$sel_frm_que_info = "SELECT $columns FROM RequisitionQuestions WHERE OrgID = :OrgID AND QuestionID = :QuestionID AND RequisitionFormID = :RequisitionFormID";
		$res_frm_que_info = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_frm_que_info, array($params_info) );
	
		return $res_frm_que_info;
	}

	/**
	 * @method		getRequisitionQuestionsByQuestionTypeID
	 * @param		$columns
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getRequisitionQuestionsByQuestionTypeID($columns = "", $OrgID, $RequisitionFormID, $QuestionTypeID) {
	
		$columns 		=	$this->db->arrayToDatabaseQueryString ( $columns );
	
		$params_info	=	array(":OrgID"=>$OrgID, ":QuestionTypeID"=>$QuestionTypeID, ":Requisition"=>'Y', ":RequisitionFormID"=>$RequisitionFormID);
		$sel_que_info 	= 	"SELECT $columns FROM RequisitionQuestions WHERE OrgID = :OrgID AND QuestionTypeID = :QuestionTypeID AND Requisition = :Requisition AND RequisitionFormID = :RequisitionFormID";
		$res_que_info 	= 	$this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_que_info, array($params_info) );
	
		return $res_que_info;
	}

	/**
	 * @method		getRequestQuestionsByQuestionTypeID
	 * @param		$columns
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getRequestQuestionsByQuestionTypeID($columns = "", $OrgID, $RequisitionFormID, $QuestionTypeID) {
	
		$columns 		=	$this->db->arrayToDatabaseQueryString ( $columns );
	
		$params_info	=	array(":OrgID"=>$OrgID, ":QuestionTypeID"=>$QuestionTypeID, ":Request"=>'Y', ":RequisitionFormID"=>$RequisitionFormID);
		$sel_que_info 	= 	"SELECT $columns FROM RequisitionQuestions WHERE OrgID = :OrgID AND QuestionTypeID = :QuestionTypeID AND Request = :Request AND RequisitionFormID = :RequisitionFormID";
		$res_que_info 	= 	$this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_que_info, array($params_info) );
	
		return $res_que_info;
	}
	
	/**
	 * @method		updRequisitionQuestionsOrder
	 * @param		$set_que_order, $con_que_order
	 */
	public function updRequisitionQuestionsOrder($OrgID, $RequisitionFormID, $QuestionID, $QuestionOrder) {

		//Update Question Order of Form Tables
		$params_que_order     =   array(':OrgID'=>$OrgID, ':RequisitionFormID'=>$RequisitionFormID, ":QuestionID"=>$QuestionID, ':QuestionOrder'=>$QuestionOrder);
		$upd_req_que_order    =   "UPDATE RequisitionQuestions SET QuestionOrder = :QuestionOrder";
		$upd_req_que_order   .=   " WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID AND QuestionID = :QuestionID";
		$res_req_que_order    =   $this->db->getConnection("IRECRUIT")->update($upd_req_que_order, array($params_que_order));
	
		return $res_req_que_order;
	}

	/**
	 * @method		copyRequisitionQuestions
	 * @param		$OrgID
	 */
	public function copyRequisitionQuestions($SourceOrgID, $DestinationOrgID, $RequisitionFormID, $IRequisitionFormID) {
	
	    //Set parameters for prepared query
	    $params = array(":SourceOrgID"=>$SourceOrgID, ":RequisitionFormID"=>$RequisitionFormID);
	
	    $ins_req_ques = "INSERT INTO RequisitionQuestions (`OrgID`, `RequisitionFormID`, `QuestionID`, `Question`, `QuestionTypeID`, `size`, `maxlength`, `rows`, `cols`, `wrap`, `value`, `defaultValue`, `QuestionOrder`, `Request`, `Requisition`, `Required`, `Validate`, `Edit`, `LastModifiedDateTime`)
						 SELECT '" . $DestinationOrgID . "', '" . $IRequisitionFormID . "', `QuestionID`, `Question`, `QuestionTypeID`, `size`, `maxlength`, `rows`, `cols`, `wrap`, `value`, `defaultValue`, `QuestionOrder`, `Request`, `Requisition`, `Required`, `Validate`, `Edit`, `LastModifiedDateTime`
						 FROM RequisitionQuestions WHERE OrgID = :SourceOrgID AND RequisitionFormID = :RequisitionFormID";
	    $res_req_ques = $this->db->getConnection ( "IRECRUIT" )->insert($ins_req_ques, array($params));
	
	    return $res_req_ques;
	}
	
	/**
	 * @method		insDefaultRequisitionQuestions
	 * @param		$OrgID
	 */
	public function insDefaultRequisitionQuestions($OrgID) {
	
	    //Set parameters for prepared query
	    $params = array(":OrgID"=>$OrgID);
	
	    $sel_que_id	  = "SELECT `QuestionID` FROM RequisitionQuestions WHERE OrgID = :OrgID";
	    $res_que_id	  = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_que_id, array($params));

	    if(!isset($res_que_id['QuestionID']) || $res_que_id['QuestionID'] == "") {
	        //Insert Requisition Questions
	        $res_req_ques = $this->copyRequisitionQuestions('MASTER', $OrgID, 'REQUISITION', 'REQUISITION');
	    }
	
	    return $res_req_ques;
	}
	
	/**
	 * @method		insRequisitionQuestions
	 * @param		$fq_info
	 */
	public function insRequisitionQuestions($fq_info) {
		
		$ins_form_ques = $this->db->buildInsertStatement("RequisitionQuestions", $fq_info);
		$res_form_ques = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_form_ques["stmt"], $ins_form_ques["info"] );
	
		return $res_form_ques;
	}
	
	/**
	 * @method		delRequisitionQuestions
	 * @param		$table_name, $where_info = array(), $info
	 */
	public function delRequisitionQuestions($where_info = array(), $info) {
		
		//Set parameters for prepared query
		$del_que_info = "DELETE FROM RequisitionQuestions WHERE ";
		
		if(count($where_info) > 0) $del_que_info .= implode(" AND ", $where_info);
		$res_que_info = $this->db->getConnection("IRECRUIT")->delete($del_que_info, $info);
		
		return $res_que_info;		
	}
	
	/**
	 * @method		updRequisitionQuestions
	 * @param		$set_info = array(), $where_info = array(), $info
	 */
	public function updRequisitionQuestions($set_info = array(), $where_info = array(), $info) {
		
		$upd_que_info = $this->db->buildUpdateStatement("RequisitionQuestions", $set_info, $where_info);
		$res_que_info = $this->db->getConnection("IRECRUIT")->update($upd_que_info, $info);
	
		return $res_que_info;
	}
	
	/**
	 * @param 		string $ans
	 * @param 		string $displaychoices
	 * @return 		string 
	 */
	function getDisplayValue($ans, $displaychoices) {
		$rtn = '';
	
		if ($ans != '') {
			$Values = explode ( '::', $displaychoices );
			foreach ( $Values as $v => $q ) {
				list ( $vv, $qq ) = explode ( ":", $q, 2 );
				if ($vv == $ans) {
					$rtn .= $qq;
				}
			}
		}
		return $rtn;
	} // end of function
	
	
	/**
	 * @param 		string 	$APPDATA
	 * @param 		string 	$id
	 * @param 		string	$displaychoices
	 * @param 		string	$questiontype
	 * @return		string
	 */
	function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
		global $RequisitionQuestionsObj;
		$rtn = '';
		$val = '';
		$lex = '';
	
		if (($id == 'daysavailable') || ($id == 'typeavailable')) {
	
			$le = ', ';
			$li = '';
			$lecnt = - 2;
		} else {
	
			$le = ', ';
			$li = '';
			$lecnt = - 4;
		}
	
		$cnt = $APPDATA [$id . 'cnt'];
		
		for($i = 1; $i <= $cnt; $i ++) {
	
			$val = $RequisitionQuestionsObj->getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
			if ($val) {
				$rtn .= $li . $val;
				$lex = 'on';
				$val = '';
			}
	
			if ($questiontype == 9) {
					
				$val = $APPDATA [$id . '-' . $i . '-yr'];
				if ($val) {
					$rtn .= ', ' . $val . ' yrs. ';
					$lex = 'on';
					$val = '';
				}
					
				$val = $APPDATA [$id . '-' . $i . '-comments'];
				if ($val) {
					$rtn .= ' (' . $val . ') ';
					$lex = 'on';
					$val = '';
				}
			} // end questiontype
	
			if ($lex == 'on') {
				$rtn .= $le;
				$lex = '';
			}
		}
	
		$rtn = substr ( $rtn, 0, $lecnt );
		
		return $rtn;
	} // end of function
	
	
	/**
	 * @param 	string 	$OrgID
	 * @param 	string 	$Question
	 * @param 	string 	$QuestionID
	 * @param 	string 	$QuestionTypeID
	 * @param 	array 	$APPDATA
	 * @return 	string
	 */
	function printSection($OrgID, $RequestID, $Question, $value, $QuestionID, $QuestionTypeID, $APPDATA) {
		global $permit, $feature, $RequisitionQuestionsObj, $AddressObj, $RequisitionsObj;

		$rtn = '';
		
		if($QuestionID == "MultiCostCenter") {
			$APPDATA [$QuestionID] = ($APPDATA [$QuestionID] == "Y") ? "Yes" : "No";
		}
		else if($QuestionID == "WorkDays") {
			$APPDATA [$QuestionID] = (is_array($APPDATA [$QuestionID])) ? implode(", ", array_keys($APPDATA [$QuestionID])) : '';
		}
		else if($QuestionID == "AdvertisingOptions") {
			$AdvertisingOptions = $APPDATA [$QuestionID];
			// long display for verifying request process
			$request = '<table border="0" cellspacing="3" cellpadding="5" width="100%">';
			
			if (count ( $AdvertisingOptions ) > 2) {
				
				if ($AdvertisingOptions ['AdvertisingOption-Other'] == "Y") {
					$adother = 'Other - ' . $AdvertisingOptions ['AdvertisingOption-Desc'] . ', ';
					$adother .= $AdvertisingOptions ['AdvertisingOption-Budget'] . ', ';
					$adother .= $AdvertisingOptions ['AdvertisingOption-Period'];
				}
				
				unset ( $AdvertisingOptions ['AdvertisingOption-Other'] );
				unset ( $AdvertisingOptions ['AdvertisingOption-Desc'] );
				unset ( $AdvertisingOptions ['AdvertisingOption-Budget'] );
				unset ( $AdvertisingOptions ['AdvertisingOption-Period'] );
				
				$request .= '<tr><td>';
				
				foreach ( $AdvertisingOptions as $key => $value ) {
					list ( $Site, $Price, $Period ) = explode ( "::", $value );
					if ($key != "AdvertisingOption-specialinstructions") {
						$request .= $Site . ' - ' . $Price . ', ' . $Period . '<br>';
					}
				}
				
				$request .= $adother . "<br>";
				
				if ($AdvertisingOptions ['AdvertisingOption-specialinstructions']) {
					$request .= "Special Instructions:<br>" . $AdvertisingOptions ['AdvertisingOption-specialinstructions'];
				}
				
				$request .= '</td></tr>';
			} // end if AdvertisingOptions
			
			$request .= '</table>';
			
			$APPDATA [$QuestionID]	=	$request;
		}
		else if (($QuestionTypeID == 18) || ($QuestionTypeID == 1818) || ($QuestionTypeID == 9)) {
			$APPDATA [$QuestionID] = $RequisitionQuestionsObj->getMultiAnswer ( $APPDATA, $QuestionID, $value, $QuestionTypeID );
		}
		else if (($QuestionTypeID == 1) || ($QuestionTypeID == 24)) {
			$APPDATA [$QuestionID] = "from " . $APPDATA [$QuestionID . from] . " to " . $APPDATA [$QuestionID . to];
		}
		else if ($QuestionTypeID == 4) {
		    if($QuestionID == 'RequisitionManagers') {
		        $APPDATA [$QuestionID]    =   $RequisitionsObj->getRequisitionManagersNames($OrgID, $RequestID);
		    }
		}
		
		
		if ($QuestionTypeID == 100) {
				
			if ($APPDATA [$QuestionID] != "") {
	
				$canswer = @unserialize ( $APPDATA [$QuestionID] );
				$radio_cust_que = '';
				if (is_array ( $canswer )) {
					$radio_cust_que .= '<div class="form-group"><label>' . $Question . '</label>';
						
					foreach ( $canswer as $cakey => $caval ) {
						$radio_cust_que .= $cakey . ':';
	
						foreach ( $caval as $cavk => $cavv ) {
							$radio_cust_que .= $cavv . ', ';
						}
					}
				}
	
				$rtn .= "<strong>".trim ( $radio_cust_que, ", " )."</strong>";
				$rtn .= '</div>';
			}
		} else if ($QuestionTypeID == 120) {
			if ($APPDATA [$QuestionID] != "") {
				$canswer = unserialize ( $APPDATA [$QuestionID] );
	
				$available_time = array ();
				if (is_array ( $canswer )) {
					$rtn .= '<div class="form-group"><label>' . $Question . '</label>';
						
					$days_count = count ( $canswer ['days'] );
						
					for($ci = 0; $ci < $days_count; $ci ++) {
						$available_time [] = $canswer ['days'] [$ci] . ": " . $canswer ['from_time'] [$ci] . " - " . $canswer ['to_time'] [$ci];
					}
						
					$rtn .= "<strong>".implode ( ", ", $available_time )."</strong>";
					
					$rtn .= '</div>';
				}
			}
		} else if ($QuestionTypeID == 99) {
			$rtn .= '<div class="form-group"><label>' . $Question . '</label></div>';
		} else {
			$rtn .= '<div class="form-group"><label>' . $Question . ' </label> ' . $APPDATA [$QuestionID] . '</div>';
		}
	
		return $rtn;
	} // end of function
	
	/**
	 * @method	getAdditionalRequisitionFields
	 */
	public function getAdditionalRequisitionFields($OrgID, $RequisitionFormID) {
		
		require IRECRUIT_DIR . 'requisitions/DefaultRequisitionFields.inc';
		
		$requisition_questions	=	$this->getRequisitionQuestionsByQueID("*", $OrgID, $RequisitionFormID);
		
		$req_ques_list			=	array_keys($requisition_questions);

		$additional_ques_list	=	array_diff($req_ques_list, $def_requisition_fields);
		
		$additional_ques_list	=	array_values($additional_ques_list);
		
		return $additional_ques_list;
	}
}
