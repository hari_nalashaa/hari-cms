<?php 
/**
 * @class		WOTCUsers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCUsers
{
    var $conn_string       =   "WOTC";

    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method		getUsersInfo
     * @param		string $columns
     * @param		string $where_info
     * @param 		string $order_by
     * @param 		string $info
     * @return 		array
     */
    function getUsersInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_org = "SELECT $columns FROM Users";
    
        if(count($where_info) > 0) {
            $sel_org .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($order_by != "") $sel_org .= " ORDER BY " . $order_by;
        
        $res_org = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_org, $info );
    
        return $res_org;
    }
    
    /**
     * @method      getUserInfoByUserID
     */
    public function getUserInfoByUserID($columns = "*", $USERID) {

        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_info   =   "SELECT $columns FROM Users WHERE UserID = :UserID";
        $params     =   array(':UserID' => $USERID);
        $row_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));

        return $row_info;
    }

    /**
     * @method      getUserInfoByEmail
     */
    public function getUserInfoByEmail($columns = "*", $Email) {
    
        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_info   =   "SELECT $columns FROM Users WHERE Email = :Email";
        $params     =   array(':Email' => $Email);
        $row_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
        return $row_info;
    }

    /**
     * @method      getUserInfoBySessionID
     */
    public function getUserInfoBySessionID($columns = "*", $SessionID) {
    
        $columns    =   $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_info   =   "SELECT $columns FROM Users WHERE SessionID = :SessionID";
        $params     =   array(':SessionID' => $SessionID);
        $row_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
        return $row_info;
    }

    /**
     * @method          insUpdUsersInfo
     * @param           $info
     * @return          array
     */
    function insUpdUsersInfo($info, $skip = array()) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("Users", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );

        return $res_org;
    }

    /**
     * @method      updUsersInfo
     * @param       $set_info, $where_info, $info
     */
    function updUsersInfo($set_info = array(), $where_info = array(), $info) {

        $upd_users_info    =   $this->db->buildUpdateStatement("Users", $set_info, $where_info);
        $res_users_info    =   $this->db->getConnection( $this->conn_string )->update($upd_users_info, $info);

        return $res_users_info;
    }

    /**
     * @method		delUsersInfo
     * @param		$where_info = array(), $info = array()
     */
    function delUsersInfo($where_info = array(), $info = array()) {
    
        $del_rows   =   "DELETE FROM Users";
    
        if (count ( $where_info ) > 0) {
            $del_rows   .=  " WHERE " . implode ( " AND ", $where_info );
        }
    
        $res_rows = $this->db->getConnection ( $this->conn_string )->delete ( $del_rows, $info );
    
        return $res_rows;
    }

}
?>
