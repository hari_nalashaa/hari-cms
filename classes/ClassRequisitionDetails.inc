<?php 
/**
 * @class		RequisitionDetails
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class RequisitionDetails {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    
    /**
     * @method		getJobTitle
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getJobTitle($OrgID, $MultiOrgID, $RequestID) {
    
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        //Get Title
        $sel_info   = "SELECT Title FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
        $column	    = is_null($res_info['Title']) ? '' : $res_info['Title'];
    
        return $column;
    } // end function

    /**
     * @method		getFormID
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getFormID($OrgID, $MultiOrgID, $RequestID) {
    
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        //Get RequisitionID
        $sel_info   =   "SELECT FormID FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
        $column     =   is_null($res_info['FormID']) ? 'STANDARD' : $res_info['FormID'];
    
        return $column;
    } // end function
    
    /**
     * @method		getRequisitionFormID
     * @param		string $OrgID, $RequestID
     * @return		string
     */
    function getRequisitionFormID($OrgID, $RequestID) {
    
    	//Set parameters for prepared query
    	$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
    	//Get RequisitionID
    	$sel_info   =   "SELECT RequisitionFormID FROM Requisitions WHERE OrgID = :OrgID AND RequestID = :RequestID";
    	$res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
    
    	return $res_info['RequisitionFormID'];
    } // end function    
    
    /**
     * @method		getRequisitionID
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getRequisitionID($OrgID, $MultiOrgID, $RequestID) {
    
        //Set parameters for prepared query
        $params		=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        //Get RequisitionID
        $sel_info   =	"SELECT RequisitionID FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info   =	$this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
        $column     =	!isset($res_info['RequisitionID']) ? '' : $res_info['RequisitionID'];
    
        return $column;
    } // end function
    
    
    /**
     * @method		getEEOCode
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getEEOCode($OrgID, $MultiOrgID, $RequestID) {
    
        $column     = '';
        //Set parameters for prepared query
        $params     = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        //Get EEOCode
        $sel_info   = "SELECT EEOCode FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
        $column     = is_null($res_info['EEOCode']) ? '' : $res_info['EEOCode'];
    
        return $column;
    } // end function
    
    
    /**
     * @method		getJobID
     * @param		string $OrgID, $MultiOrgID, $RequestID
     * @return		string
     */
    function getJobID($OrgID, $MultiOrgID, $RequestID) {
    
        $column = '';
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        //Get JobID
        $sel_info   = "SELECT JobID FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info   = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_info, array($params));
        $column     = !isset($res_info['JobID']) ? '' : $res_info['JobID'];
    
        return $column;
    } // end function
    
    
    /**
     * @method		getMultiOrgID
     * @param		string $OrgID, $RequestID
     * @return		string
     */
    function getMultiOrgID($OrgID, $RequestID) {
    
        $column     =   '';
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        //Select MultiOrgID From Requisitions based on OrgID and RequestID
        $sel_info   =   "SELECT MultiOrgID FROM Requisitions WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_info   =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
        $column     =   !isset($res_info['MultiOrgID']) ? '' : $res_info['MultiOrgID'];
        
        return $column;
    }
    
    
    /**
     * @method		getReqJobIDs
     * @param		$OrgID, $MultiOrgID, $RequestID
     * @return		string 
     */
    function getReqJobIDs($OrgID, $MultiOrgID, $RequestID) {
    
        $reqjobid = '';
    
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);
        //Get RequisitionID, JobID
        $sel_info = "SELECT RequisitionID, JobID FROM Requisitions WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID AND RequestID = :RequestID";
        $res_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );
    
        foreach ( $res_info ['results'] as $RT ) {
            $reqjobid = $RT ['RequisitionID'] . '/' . $RT ['JobID'];
        }
    
        return $reqjobid;
    } // end function
    
    
    /**
     * @method		getRequisitionOwnersName
     * @param		string $OrgID, $RequestID
     * @return		string
     */
    function getRequisitionOwnersName($OrgID, $RequestID) {
    
        global $IrecruitUsersObj;
    
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
        $sel_owner  =   "SELECT Owner FROM Requisitions WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $res_owner  =   $this->db->getConnection("IRECRUIT")->fetchRow ( $sel_owner, array($params) );
        $name       =   $IrecruitUsersObj->getUsersName ( $OrgID, $res_owner [0] );
        
        if ($name == "") {
            $name = 'Not Assigned';
        }
        
        return $name;
    } // end function
    
    
    /**
     * @method		getInternalRequisitionsCount
     */
    function getInternalRequisitionsCount($OrgID, $MultiOrgID) {
    
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
    
        $sel_internal_requisitions = "SELECT COUNT(*) AS InternalReqCount FROM Requisitions
									  WHERE OrgID = :OrgID
									  AND MultiOrgID = :MultiOrgID
									  AND Active = 'Y'
									  AND PresentOn IN ('INTERNALONLY','INTERNALANDPUBLIC')
                                      AND NOW() BETWEEN PostDate AND ExpireDate";
        $res_internal_requisitions = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_internal_requisitions, array($params));
    
        return $res_internal_requisitions['InternalReqCount'];
    }
    
    
    /**
     * @method		getPublicRequisitionsCount
     */
    function getPublicRequisitionsCount($OrgID, $MultiOrgID) {
    
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>(string)$MultiOrgID);
    
        $sel_public_requisitions = "SELECT COUNT(*) AS PublicReqCount FROM Requisitions
									WHERE OrgID = :OrgID
									AND MultiOrgID = :MultiOrgID
									AND Active = 'Y'
                                    AND PresentOn IN ('PUBLICONLY','INTERNALANDPUBLIC')
                                    AND NOW() BETWEEN PostDate AND ExpireDate";
        $res_public_requisitions = $this->db->getConnection( $this->conn_string )->fetchAssoc($sel_public_requisitions, array($params));
    
        return $res_public_requisitions['PublicReqCount'];
    }
    
    /**
     * @method 		getHiringManagerRequestID
     * @return 		array @type	json
     */
    function getHiringManagerRequestID() {
    
        global $USERID, $USERROLE, $OrgID;

        if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
            $params[":UserID"] = array($USERID);
            $params[":ReqManOrgID"] = array($OrgID);
    
            $selrequisitions = "SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManOrgID AND UserID = :UserID";
        } // end hiring manager
    
        $rows = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $selrequisitions, array($params) );
		$results = $rows['results'];
		foreach($results as $result){
			$RequestID[] = $result['RequestID'];
		}
        return $RequestID;
    }


    /**
     * @method 		countRequistions
     * @param		$POST
     * @return 		array @type	json
     */
    function countRequistions($POST = '') {
    
        global $USERID, $USERROLE, $OrgID;
    
        /**
         * @input json data
         */
        if ($POST == '') {
            $POST = file_get_contents ( 'php://input' );
        }
        $JSON = json_decode ( $POST, true );

        if (! isset ( $JSON ['Active'] ))
            $JSON ['Active'] == 'Y';
    
        //Set parameters for prepared query
        $params = array(":OrgID"=>$OrgID, ":Active"=>$JSON ['Active']);
    
    
        $selrequisitions = "SELECT COUNT(*) as ReqCount
							FROM Requisitions R
							WHERE R.OrgID = :OrgID
						    AND R.Active = :Active
						  	AND R.Approved = 'Y'";
	if ($JSON ['Active'] == "Y") {
		$selrequisitions .= " AND NOW() BETWEEN R.PostDate AND R.ExpireDate";
	}
    
        if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
            $params[":UserID"] = array($USERID);
            $params[":ReqManOrgID"] = array($OrgID);
    
            $selrequisitions .= " AND R.RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ReqManOrgID AND UserID = :UserID)";
        } // end hiring manager
    
        $row = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $selrequisitions, array($params) );
    
        return $row ['ReqCount'];
    }

    
    /**
     * @method      getRequisitionJobBoardsStatus
     */
    function getRequisitionJobBoardsStatus($OrgID, $RequestID) {
        global $IndeedObj, $ZipRecruiterObj, $MonsterObj, $RequisitionsObj;
        
        //Get the details of job boards
        $res_zip_feed       =   $ZipRecruiterObj->getZipRecruiterFeed($OrgID, $RequestID);
        $res_zip_subs       =   $ZipRecruiterObj->getZipRecruiterSubscriptionInfo($OrgID, $RequestID);
        $res_ind_feed       =   $IndeedObj->getIndeedFeedInformation("OrgID, RequestID", $OrgID, "", $RequestID);
        $res_mon_info       =   $MonsterObj->getMonsterRequisitionInformation("*", $OrgID, "", $RequestID);
        $req_detail_info    =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $RequestID);
        
        $ziprecruiter_paid  =   "No";
        $ziprecruiter_subs  =   "No";
        $indeed_paid        =   "No";
        $monster_paid       =   "No";
        $indeed_free        =   "No";
        $monster_free       =   "No";
        $ziprecruiter_free  =   "No";
        
        if(isset($res_zip_feed['RequestID']) && $res_zip_feed['RequestID'] != "") {
            $ziprecruiter_paid  = "Yes";
        }
        if(isset($res_zip_subs['RequestID']) && $res_zip_subs['RequestID'] != "") {
            $ziprecruiter_subs  = "Yes";
        }
        if(isset($res_ind_feed['RequestID']) && $res_ind_feed['RequestID'] != "") {
            $indeed_paid        = "Yes";
        }
        if(isset($res_mon_info['RequestID']) && $res_mon_info['RequestID'] != "") {
            $monster_paid       = "Yes";
        }
        
        if($req_detail_info['FreeJobBoardLists'] != "") {
            $free_jobboard_lists = json_decode($req_detail_info['FreeJobBoardLists'], true);
            
            if(in_array("Indeed", $free_jobboard_lists)) {
                $indeed_free    = "Yes";
            }
            if(in_array("Monster", $free_jobboard_lists)) {
                $monster_free   = "Yes";
            }
            if(in_array("ZipRecruiter", $free_jobboard_lists)) {
                $ziprecruiter_free = "Yes";
            }
        }
        
        $job_boards_info    =   array(
                                    "PaidZipRecruiter"      =>  $ziprecruiter_paid,
                                    "SubscribeZipRecruiter" =>  $ziprecruiter_subs,
                                    "FreeZipRecruiter"      =>  $ziprecruiter_free,
                                    "PaidIndeed"            =>  $indeed_paid,
                                    "FreeIndeed"            =>  $indeed_free,
                                    "PaidMonster"           =>  $monster_paid,
                                    "FreeMonster"           =>  $monster_free,
                                    );
        
        return json_encode($job_boards_info);
    }
    
    
    /**
     * @method      getEmploymentStatusLevelsList
     * @param       $OrgID, $RequisitionFormID
     */
    function getEmploymentStatusLevelsList($OrgID, $RequisitionFormID) {
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $sel_levels =   "SELECT value FROM RequisitionQuestions WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID AND QuestionID = 'EmpStatusID'";
        $res_levels =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_levels, array($params) );
        
        $levels_res =   explode("::", $res_levels['value']);
        
        $levels     =   array();
        
        for($l = 0; $l < count($levels_res); $l++) {
            list($level_id, $level_name)    =   explode(":", $levels_res[$l]);
            $levels[$level_id]  =   $level_name;
        }
        
        return $levels;
    }
    
    
    /**
     * @method      getJobGroupCodesList
     * @param       $OrgID, $RequisitionFormID
     */
    function getJobGroupCodesList($OrgID, $RequisitionFormID) {
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $sel_codes  =   "SELECT value FROM RequisitionQuestions WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID AND QuestionID = 'JobGroupCode'";
        $res_codes  =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_codes, array($params) );
    
        $codes_res  =   explode("::", $res_codes['value']);
    
        $codes      =   array();
    
        for($l = 0; $l < count($codes_res); $l++) {
            list($code_id, $code_name)    =   explode(":", $codes_res[$l]);
            $codes[$code_id]  =   $code_name;
        }
    
        return $codes;
    }
    
    
    /**
     * @method      getImportanceLevelsList
     * @param       $OrgID, $RequisitionFormID
     */
    function getImportanceLevelsList($OrgID, $RequisitionFormID) {
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $sel_levels =   "SELECT value FROM RequisitionQuestions WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID AND QuestionID = 'ImportanceID'";
        $res_levels =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_levels, array($params) );
        
        $levels_res =   explode("::", $res_levels['value']);
        
        $levels     =   array();
        
        for($l = 0; $l < count($levels_res); $l++) {
            list($level_id, $level_name)    =   explode(":", $levels_res[$l]);
            $levels[$level_id]  =   $level_name;
        }
        
        return $levels;
    }
    
    
    /**
     * @method      getSalaryGradeInfo
     * @param       $OrgID, $RequisitionFormID
     */
    function getSalaryGradeInfo($OrgID, $RequisitionFormID) {
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $sel_sal    =   "SELECT value FROM RequisitionQuestions WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID AND QuestionID = 'SalaryGradeID'";
        $res_sal    =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_sal, array($params) );
    
        $sal_res    =   explode("::", $res_sal['value']);
    
        $sal_grades =   array();
    
        for($l = 0; $l < count($sal_res); $l++) {
            list($sal_grade_id, $sal_grade_desc)    =   explode(":", $sal_res[$l]);
            $sal_grades[$sal_grade_id]  =   $sal_grade_desc;
        }
    
        return $sal_grades;
    }
    
    
    /**
     * @method      getRequestReasonsInfo
     * @param       $OrgID, $RequisitionFormID
     */
    function getRequestReasonsInfo($OrgID, $RequisitionFormID) {
        //Set parameters for prepared query
        $params     =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
        $sel_req    =   "SELECT value FROM RequisitionQuestions WHERE OrgID = :OrgID AND RequisitionFormID = :RequisitionFormID AND QuestionID = 'RequesterReason'";
        $res_req    =   $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_req, array($params) );
    
        $req_res    =   explode("::", $res_req['value']);
    
        $req_reasons    =   array();
    
        for($l = 0; $l < count($req_res); $l++) {
            list($req_reason_id, $req_reason_desc)    =   explode(":", $req_res[$l]);
            $req_reasons[$req_reason_id]  =   $req_reason_desc;
        }
    
        return $req_reasons;
    }
}
?>
