<?php
/**
 * @class		TwilioTextingAgreementHistory
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class TwilioTextingAgreementHistory {

    public $db;

	var $conn_string       		=   "IRECRUIT";

	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		// Create a new instance of a SOAP 1.2 client
		$this->db	=	Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	} // end function
	
	/**
	 * @method		insUpdTwilioTextingAgreementHistory
	 * @return		array
	 */
	function insUpdTwilioTextingAgreementHistory($info, $skip = array()) {
    
        $ins_twilio_account_sub	= $this->db->buildOnDuplicateKeyUpdateStatement("TwilioTextingAgreementHistory", $info, $skip);
        $res_twilio_account_sub	= $this->db->getConnection ( $this->conn_string )->insert ( $ins_twilio_account_sub["stmt"], $ins_twilio_account_sub["info"] );
        
        return $res_twilio_account_sub;
	}
	
	/**
	 * @method		getTwilioTextingAgreementHistoryInfo
	 * @return		array
	 */
	function getTwilioTextingAgreementHistoryInfo($OrgID) {
		
        $params     =   array(":OrgID"=>$OrgID);
        $sel_info   =	"SELECT * FROM TwilioTextingAgreementHistory WHERE OrgID = :OrgID";
        $res_info   =	$this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
		
		return $res_info;
	}
}
