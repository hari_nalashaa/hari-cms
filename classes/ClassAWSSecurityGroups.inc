<?php
/**
 * @class		AWSSecurityGroups	
 */

use Aws\Ec2\Ec2Client;

class AWSSecurityGroups {
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		date_default_timezone_set('America/New_York');
	}
	
	/**
	 * @method     getUserIPAddress
	 */
	function updateAWS ($server, $region, $policy_grp_name, $port_from, $port_to, $IPADDRESS, $UserID) {

	if ($_SERVER['HTTP_HOST'] == "dev.irecruit-us.com") {
	  putenv("HOME=/usr/home/cmsdev");
	} else if ($_SERVER['HTTP_HOST'] == "quality.irecruit-us.com") {
	  putenv("HOME=/usr/home/cmsadmin");
	} else {
	  putenv("HOME=/usr/home/irecruit");
	}

	$rtn = "";

        $ec2Client = new Ec2Client([
                'region' => $region,
                'version' => '2016-11-15',
                'profile' => 'default' 
        ]);

        //Describe the Policy
        $result = $ec2Client->describeSecurityGroups(array(
                'GroupNames' => [$policy_grp_name],
        ));

	$ipRange = array();

        //Get all of the IP's attached to the policy
        foreach ($result['SecurityGroups']['0']['IpPermissions'] as $policy) {
                foreach ($policy['IpRanges'] as $listedIP) {
			if ($policy['FromPort'] == $port_from) {
                           $ipRange[] = explode("/", $listedIP['CidrIp'])['0'];
		        }
                }

        }

        //Sanatize the IP array
	$ipListClean = array_unique($ipRange);


        $rtn .= "\n";
        $rtn .= "Server: " . $server . "\n";
        $rtn .= "Region: " . $region . "\n";
        $rtn .= "Policy Group Name: " . $policy_grp_name . "\n";
        $rtn .= "Port From: " . $port_from . "\n";
        $rtn .= "Port To: " . $port_to . "\n";
	$rtn .= "UserID: " . $UserID . "\n";
	$rtn .= "IPADDRESS: " . $IPADDRESS . "\n";

	if (($IPADDRESS != "" && $UserID != "") && (!in_array($IPADDRESS, $ipListClean))) {

		if (($region != "") && ($policy_grp_name != "") 
			&& ($port_from != "") && ($port_to != "")) {

        	$rtn .= "Configured\n";

		$result = $ec2Client->authorizeSecurityGroupIngress(array(
                        'GroupName' => $policy_grp_name,
                        'IpPermissions' => [
                                [
					'FromPort' => $port_from,
                                        'ToPort' => $port_to,
                                        'IpProtocol' => 'TCP',
                                        'IpRanges' => [
                                                        [
                                                                'CidrIp' => $IPADDRESS . '/32',
                                                                'Description' => $UserID . ' ' . date("m-d-Y H:i:s"),
                                                        ],
                                        ],
                                ],
                        ],
                ));

		} else { // else if data

        	$rtn .= "Not Configured, no data.\n";

		}  // end else data


	unset($ipRange);

	} else { // end clean IP

        $rtn .= "Already Configured\n";

	} // end clean IP

        $rtn .= "\n";

	return $rtn;

	} // end function

} // end class
