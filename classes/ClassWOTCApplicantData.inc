<?php
/**
 * @class		WOTCApplicantData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCApplicantData
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }


       /**
    * @method              getRealTimeReportData
    * @param               $ACCESS
    */
   public function getRealTimeReportData($ACCESS,$StartDate,$EndDate) {

       $ACCESS         =   "'" . join('\',\'', $ACCESS) . "'";

       $sel_app_data   =   "SELECT";
       $sel_app_data   .=  " year(EntryDate) AS Year,";
       $sel_app_data   .=  " month(EntryDate) AS Month,";
       $sel_app_data   .=  " count(*) AS WOTCScreens";
       $sel_app_data   .=  " FROM ApplicantData";
       $sel_app_data   .=  " WHERE wotcID IN ($ACCESS)";
       $sel_app_data   .=  " AND ApplicationStatus = 'H'";
       if (($StartDate != "") && ($EndDate != "")) {
       $sel_app_data   .=  " AND DATE(EntryDate) BETWEEN :StartDate AND :EndDate";
       $params    =   array(":StartDate" => $StartDate, ":EndDate" => $EndDate);
       }

       $sel_app_data   .=  " GROUP BY Year, Month";
       $sel_app_data   .=  " ORDER BY Year desc, Month";

       $res = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_app_data, array($params) );

       $InitiatedCnt=0;
       for ($i = 0; $i < $res['count']; $i++) {

           $sel_app_data   =   "SELECT";
           $sel_app_data   .=  "  year(EntryDate) as Year, count(*) as Qualified";
           $sel_app_data   .=  " FROM ApplicantData";
           $sel_app_data   .=  " WHERE wotcID IN ($ACCESS)";
           $sel_app_data   .=  " AND ApplicationStatus = 'H'";
           $sel_app_data   .=  " AND Qualifies = 'Y'";
           $sel_app_data   .=  " AND year(EntryDate) = '".$res['results'][$i]['Year']."'";
           $sel_app_data   .=  " AND month(EntryDate) = '".$res['results'][$i]['Month']."'";
           $sel_app_data   .=  " ORDER BY Year desc, Month";

           $qual = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_app_data );

           $Qualified=0;
           foreach ($qual['results'] AS $Q) {
               if (($res['results'][$i]['Year'] == $Q['Year']) && ($res['results'][$i]['Month'] == $Q['Month'])) {
                   $Qualified=$Q['Qualified'];
               }
           }

           $res['results'][$i]['Qualified']=$Qualified;


           $sel_app_data   =   "SELECT Processing.Qualified, count(*) AS StateStatus";
           $sel_app_data   .=  " FROM Processing";
           $sel_app_data   .=  " JOIN ApplicantData ON ApplicantData.wotcID = Processing.wotcID AND ApplicantData.ApplicationID = Processing.ApplicationID";
           $sel_app_data   .=  " WHERE Processing.wotcID IN ($ACCESS)";
           $sel_app_data   .=  " AND Processing.Qualified in ('Y','U')";
           $sel_app_data   .=  " AND Processing.Filed != '0000-00-00'";
           $sel_app_data   .=  " AND ApplicantData.Qualifies = 'Y'";
           $sel_app_data   .=  " AND ApplicantData.ApplicationStatus = 'H'";
           $sel_app_data   .=  " AND year(ApplicantData.EntryDate) = '".$res['results'][$i]['Year']."'";
           $sel_app_data   .=  " AND month(ApplicantData.EntryDate) = '".$res['results'][$i]['Month']."'";
           $sel_app_data   .=  " GROUP BY Processing.Qualified";
           $sel_app_data   .=  " ORDER BY Processing.Qualified";

           $applicants = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_app_data );

           $PendingwithState=0;
           $CertifiedfromState=0;
           foreach ($applicants['results'] AS $A) {
               if ($A['Qualified'] == 'Y') {
                   $CertifiedfromState=$A['StateStatus'];
               } else if ($A['Qualified'] == 'U') {
                   $PendingwithState=$A['StateStatus'];
               }
           }

           $res['results'][$i]['PendingwithState']=$PendingwithState;
           $res['results'][$i]['CertifiedfromState']=$CertifiedfromState;


           $sel_app_data   =   "SELECT count(*) AS CNT, FORMAT(SUM(Billing.TaxCredit),2) AS RealizedTaxCredit";
           $sel_app_data   .=  " FROM Billing";
           $sel_app_data   .=  " JOIN ApplicantData ON ApplicantData.wotcID = Billing.wotcID AND ApplicantData.ApplicationID = Billing.ApplicationID";
           $sel_app_data   .=  " WHERE Billing.wotcID IN ($ACCESS)";
           $sel_app_data   .=  " AND ApplicantData.Qualifies = 'Y'";
           $sel_app_data   .=  " AND ApplicantData.ApplicationStatus = 'H'";
           $sel_app_data   .=  " AND year(ApplicantData.EntryDate) = '".$res['results'][$i]['Year']."'";
           $sel_app_data   .=  " AND month(ApplicantData.EntryDate) = '".$res['results'][$i]['Month']."'";

           $credit = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_app_data );

           $RealizedTaxCredit ="0.00";

           if ($credit['results'][0]['RealizedTaxCredit'] > 0) { $RealizedTaxCredit = $credit['results'][0]['RealizedTaxCredit']; }

           $cnt = $credit['results'][0]['CNT'];

           $res['results'][$i]['AwaitingHoursWages']=($CertifiedfromState - $cnt);
           $res['results'][$i]['RealizedCredits']=$RealizedTaxCredit;


       }

       return $res;
   }

    /**
     * @method      getApplicantsCountByStatus
     */
    function getApplicantsCountByStatus($wotcID, $StartDate, $EndDate) {
    	global $AUTH;    	

	if ($wotcID != "") {
	  $DATA = $this->getRealTimeReportData(array($wotcID),$StartDate, $EndDate);
	} else {
	  $DATA = $this->getRealTimeReportData($AUTH['ACCESS'],$StartDate, $EndDate);
	}

	$all_apps_cnt   =   0;
	$pend_apps_cnt  =   0;
	$cert_apps_cnt  =   0;
	$rea_tax_credits   =  0;

	foreach ($DATA['results'] AS $R) {

		$all_apps_cnt   +=   $R['WOTCScreens'];
		$cert_apps_cnt  +=   $R['CertifiedfromState'];
		$pend_apps_cnt  +=   $R['PendingwithState'];
		$rea_tax_credits   +=   preg_replace('/[^0-9]/', '',$R['RealizedCredits']);

	} // end foreach

        return array("All"=>$all_apps_cnt, "Certified"=>$cert_apps_cnt, "Pending"=>$pend_apps_cnt, "RealizedTaxCredits"=>$rea_tax_credits/100);

    }

    /**
     * @method		getWotcApplicantsByKeyword
     * @param		$keyword
     * @return		array
     */
    function getWotcApplicantsByKeyword($keyword, $qualifies, $wotc_srch_from_date, $wotc_srch_to_date, $sort_field, $sort_order) {
    
    	global $OrgID, $USERROLE, $USERID;

    	if($sort_field != ""
    		&& $sort_order != "") {
    	
    		if($sort_field == "entry_date") {
    			$sort_field = "CreatedDateTime";
    		}
    		else if($sort_field == "app_id") {
    			$sort_field = "ApplicationID";
    		}
    		else if($sort_field == "app_name") 	{
    			$sort_field = "getApplicantSortName(OrgID, IrecruitApplicationID)";
    		}
    		else if($sort_field == "req_title") 	{
    			$sort_field = "getRequisitionTitle(OrgID, IrecruitRequestID)";
    		}
    		else {
    			$sort_field = "CreatedDateTime";
    		}
    	
    		if($sort_order != "asc" && $sort_order != "desc") $sort_order = "asc";
   	
    	}
    	 
    	//Set from, to dates
    	$from_date 				= 	G::Obj('DateHelper')->getYmdFromMdy($wotc_srch_from_date);
    	$to_date 				= 	G::Obj('DateHelper')->getYmdFromMdy($wotc_srch_to_date);
    	 
		//where clauses    	
    	$where_info 			=	array("OrgID = :OrgID", "DATE(CreatedDateTime) <= :FinalDate", "DATE(CreatedDateTime) >= :BeginDate");
    	//Set parameters for prepared query
    	$params					=	array(":OrgID"=>$OrgID, ":FinalDate"=>$to_date, ":BeginDate"=>$from_date);
    	
    	if($qualifies != "") {
    		$where_info[]			=	'Qualifies = :Qualifies';
    		$params[':Qualifies']	=	$qualifies;	
    	}
		
		if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			$RequestID 			= G::Obj('RequisitionDetails')->getHiringManagerRequestID();
			$where_info[]       =   "IrecruitRequestID IN ('" . implode("','",$RequestID) . "')";
		}
		$columns				=	"OrgID, WotcFormID, wotcID, getApplicantSortName(OrgID, IrecruitApplicationID) as ApplicantSortName, getRequisitionTitle(OrgID, IrecruitRequestID) as RequisitionTitle, Qualifies, IrecruitApplicationID, IrecruitRequestID, ApplicationID, DATE_FORMAT(CreatedDateTime,'%m/%d/%Y %h:%i:%s') AS CreatedDateTime";
		$wotc_application_res	=	G::Obj('WOTCIrecruitApplications')->getWotcApplicationsInfo($columns, $where_info, "$sort_field $sort_order LIMIT 10", array($params));

    	return $wotc_application_res;
    }
    
    
    /**
     * @method      getApplicantsCountByStatusPerMonth
     */
    function getApplicantsCountByStatusPerMonth($wotcID, $Year) {
        global $AUTH;

	if ($wotcID != "") {
	  $DATA = $this->getRealTimeReportData(array($wotcID),'','');
	} else {
	  $DATA = $this->getRealTimeReportData($AUTH['ACCESS'],'','');
	}

	$all_rec_info=array();
	$cer_rec_info=array();
	$pen_rec_info=array();
	$rea_tax_credits_info=array();

	foreach ($DATA['results'] AS $R) {

		if ($R['Year'] == $Year) {
			$all_rec_info[$R['Month']] =   $R['WOTCScreens'];
			$cer_rec_info[$R['Month']] =   $R['CertifiedfromState'];
			$pen_rec_info[$R['Month']] =   $R['PendingwithState'];
			$rea_tax_credits_info[$R['Month']] =   (preg_replace('/[^0-9]/', '',$R['RealizedCredits'])/100);

		} // else if


	} // end foreach
	//Set all unassigned keys to zero
        for($m = 1; $m <= 12; $m++) {
            if(!isset($all_rec_info[$m])) {
                $all_rec_info[$m] = 0;
            }
            if(!isset($cer_rec_info[$m])) {
                $cer_rec_info[$m] = 0;
            }
            if(!isset($pen_rec_info[$m])) {
                $pen_rec_info[$m] = 0;
            }
            if(!isset($rea_tax_credits_info[$m])) {
                $rea_tax_credits_info[$m] = 0;
            }
        }

        return array("A"=>$all_rec_info, "C"=>$cer_rec_info, "P"=>$pen_rec_info, "D"=>$rea_tax_credits_info);

    }
    
    /**
     * @method      getApplicantDataInfo
     * @param       $WotcID, $ApplicationID
     */
    function getApplicantDataInfo($columns, $wotcID, $WotcFormID, $ApplicationID) {
    
        $columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
        $params_info    =   array(":wotcID"=>$wotcID, ":WotcFormID"=>$WotcFormID, ":ApplicationID"=>$ApplicationID);
        $sel_app_data   =   "SELECT $columns FROM ApplicantData WHERE wotcID = :wotcID AND ApplicationID = :ApplicationID AND WotcFormID = :WotcFormID";
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_app_data, array($params_info) );
        
        return $res_app_data;
    }

    /**
     * @method      insApplicantDataInfo
     * @param       $set_info, $where_info, $info
     */
    function insApplicantDataInfo($info) {
    
        $ins_app_data   =   $this->db->buildInsertStatement("ApplicantData", $info);
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data["stmt"], $ins_app_data["info"] );
        
        return $res_app_data;
    }
    
    /**
     * @method      updApplicantDataInfo
     * @param       $set_info, $where_info, $info
     */
    function updApplicantDataInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_applications_info = $this->db->buildUpdateStatement("ApplicantData", $set_info, $where_info);
        $res_applications_info = $this->db->getConnection( $this->conn_string )->update($upd_applications_info, $info);
    
        return $res_applications_info;
    }

    /**
     * @method          getApplicantData
     * @param           mixed   $columns
     * @param           array   $where_info
     * @param           string  $order_by
     * @param           string  $group_by
     * @param           array   $info
     * @return          array
     */
    function getApplicantData($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {

        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_data = "SELECT $columns FROM ApplicantData";

        if(count($where_info) > 0) {
            $sel_data .= " WHERE " . implode(" AND ", $where_info);
        }

        if($group_by != "") $sel_data .= " GROUP BY " . $group_by;
        if($order_by != "") $sel_data .= " ORDER BY " . $order_by;

        $res_data = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_data, $info );

        return $res_data;
    }
    
}
?>
