<?php 
/**
 * @class		Correspondence
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Correspondence {
	
	/**
	 * @tutorial	Constructor to load the default database
	 *          	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	
	/**
	 * @method		delCorrespondencePackages
	 * @param		$OrgID, $item
	 * @tutorial	
	 */
	public function delCorrespondencePackages($OrgID, $item) {
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":UpdateID"=>$item);
		
		$del_corres_pkg = "DELETE FROM CorrespondencePackages WHERE OrgID = :OrgID AND UpdateID = :UpdateID";
		$res_corres_pkg = $this->db->getConnection ( "IRECRUIT" )->delete($del_corres_pkg, array($params));
		
		return $res_corres_pkg['affected_rows'];
	}
	
	/**
	 * @method		getCorrespondencePackages
	 * @return		associative array
	 * @tutorial	This method will fetch the correspondence packages information.
	 */
	public function getCorrespondencePackages($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_correspondence_packages = "SELECT $columns FROM CorrespondencePackages";
		
		if(count($where_info) > 0) {
			$sel_correspondence_packages .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_correspondence_packages .= " ORDER BY " . $order_by;
		
		$res_correspondence_packages = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_correspondence_packages, $info );
	
		return $res_correspondence_packages;
	}
	
	/**
	 * @method		insCorrespondencePackages
	 * @return		associative array
	 * @tutorial	This method will fetch the correspondence packages information.
	 */
	public function insCorrespondencePackages($corres_pkg_info) {
		
		$ins_corres_pkgs = $this->db->buildInsertStatement('CorrespondencePackages', $corres_pkg_info);
		$res_corres_pkgs = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_corres_pkgs["stmt"],  $ins_corres_pkgs["info"]);
		
		return $res_corres_pkgs;
	}
	
	
	/**
	 * @method		updCorrespondencePackages
	 * @return		
	 */
	public function updCorrespondencePackages($cpkg_info, $UpdateID) {
		
		//Set parameters for prepared query
        $params             =   array(
                                    ":UserID"           =>  $cpkg_info['UserID'],
                                    ":PackageName"      =>  $cpkg_info['PackageName'],
                                    ":Letters"          =>  $cpkg_info['Letters'],
                                    ":Attachments"      =>  $cpkg_info['Attachments'],
                                    ":ProcessOrder"     =>  $cpkg_info['ProcessOrder'],
                                    ":DispositionCode"  =>  $cpkg_info['DispositionCode'],
                                    ":FromEmail"        =>  $cpkg_info['FromEmail'],
                                    ":UpdateID"         =>  $UpdateID
                                );

        $upd_corres_pkgs    =   "UPDATE CorrespondencePackages 
                                SET 
                                UserID              =   :UserID, 
                                PackageName         =   :PackageName, 
                                Letters             =   :Letters, 
                                Attachments         =   :Attachments, 
                                ProcessOrder        =   :ProcessOrder,  
                                DispositionCode     =   :DispositionCode,
                                FromEmail           =   :FromEmail
                                WHERE 
                                UpdateID            =   :UpdateID";
        $res_corres_pkgs    =   $this->db->getConnection ( "IRECRUIT" )->update ( $upd_corres_pkgs, array($params));
		
		return $res_corres_pkgs;
		
	}
	
	
	/**
	 * @method		getCorrespondenceLetters
	 * @return		associative array
	 * @tutorial	This method will fetch the correspondence packages information.
	 */
	public function getCorrespondenceLetters($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_correspondence_letters = "SELECT $columns FROM CorrespondenceLetters";
	
		if(count($where_info) > 0) {
			$sel_correspondence_letters .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_correspondence_letters .= " ORDER BY " . $order_by;
	
		$res_correspondence_letters = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_correspondence_letters, $info );
		
		return $res_correspondence_letters;
	}
	
	/**
	 * @method		delCorrespondenceLetters
	 * @param		$OrgID, $item
	 */
	public function delCorrespondenceLetters($OrgID, $item) {
		
		//Set parameters for prepared query
		$params				=	array(":OrgID"=>$OrgID, ":UpdateID"=>$item);
		$del_corres_letters	=	"DELETE FROM CorrespondenceLetters WHERE OrgID = :OrgID AND UpdateID = :UpdateID";
		$res_corres_letters	=	$this->db->getConnection ( "IRECRUIT" )->delete($del_corres_letters, array($params));
		
		return $res_corres_letters;		
	}
	
	/**
	 * @method		insCorrespondenceLetters
	 * 
	 */
	public function insCorrespondenceLetters($cl_info) {
		
		$ins_corres_letters = $this->db->buildInsertStatement('CorrespondenceLetters', $cl_info);
		$res_corres_letters = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_corres_letters["stmt"], $ins_corres_letters["info"] );
		
		return $res_corres_letters;
	}
	
	/**
	 * @method		updCorrespondenceLetters
	 * @param		$cl_info, $UpdateID
	 */
	public function updCorrespondenceLetters($cl_info, $UpdateID) {
		
		//Set parameters for prepared query
		$params	=	array(
						":UserID"			=>	$cl_info['UserID'],
						":Classification"	=>	$cl_info['Classification'],
						":Subject"			=>	$cl_info['Subject'],
						":Body"				=>	$cl_info['Body'],
						":UpdateID"			=>	$UpdateID	
					);
		
		$upd_corres_letters	=	"UPDATE CorrespondenceLetters SET UserID = :UserID, 
								 Classification = :Classification, Subject = :Subject, 
								 Body = :Body WHERE UpdateID = :UpdateID";
		$res_corres_letters = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_corres_letters, array($params));
		
		return $res_corres_letters;
	}
	
	/**
	 * @method		getCorrespondenceAttachments
	 * @return		associative array
	 * @tutorial	This method will fetch the correspondence packages information.
	 */
	public function getCorrespondenceAttachments($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_correspondence_attachments = "SELECT $columns FROM CorrespondenceAttachments";
	
		$where = "";
		if(count($where_info) > 0)
		{
			$where	= " WHERE " . implode(" AND ", $where_info);
		}
	
		$sel_correspondence_attachments .= $where;
		if($order_by != "") $sel_correspondence_attachments .= " ORDER BY " . $order_by;
	
		$res_correspondence_attachments = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_correspondence_attachments, $info );
	
		return $res_correspondence_attachments;
	}
	
	
	/**
	 * @method		insCorrespondenceAttachments
	 * @param		$ca_info
	 */
	public function insCorrespondenceAttachments($ca_info) {
		
		$ins_corres_attachs = $this->db->buildInsertStatement('CorrespondenceAttachments', $ca_info);
		$res_corres_attachs = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_corres_attachs["stmt"], $ins_corres_attachs["info"] );
	
		return $res_corres_attachs;
	}
	
	/**
	 * @method		updCorrespondenceAttachmentFileName
	 * @param		$OrgID, $item
	 */
	public function updCorrespondenceAttachmentFileName($OrgID, $UpdateID, $FileName) {
	    
	    //Set parameters for prepared query
	    $params        =   array(":OrgID"=>$OrgID, ":UpdateID"=>$UpdateID, ":Filename"=>$FileName);
	    $corres_attach =   "UPDATE CorrespondenceAttachments SET Filename = :Filename WHERE OrgID = :OrgID AND UpdateID = :UpdateID";
	    $res_attach    =   $this->db->getConnection("IRECRUIT")->update($corres_attach, array($params));
	    
	    return $res_attach;
	}
	
	/**
	 * @method		delCorrespondenceAttachments
	 * @param		$OrgID, $item
	 */
	public function delCorrespondenceAttachments($OrgID, $item) {
		
		//Set parameters for prepared query
		$params				=	array(":OrgID"=>$OrgID, ":UpdateID"=>$item);
		$del_corres_attach	=	"DELETE FROM CorrespondenceAttachments WHERE OrgID = :OrgID AND UpdateID = :UpdateID";
		$res_corres_attach	=	$this->db->getConnection("IRECRUIT")->delete($del_corres_attach, array($params));
		
		return $res_corres_attach;
	}

	/**
	 * @method		updCorrespondenceAttachments
	 * @param		$cl_info, $UpdateID
	 */
	public function updCorrespondenceAttachments($ca_info, $UpdateID) {
		
		//Set parameters for prepared query
		$params					=	array(
										":UserID"	=>	$ca_info['UserID'],
										":Filename"	=>	$ca_info['Filename'],
										":UpdateID"	=>	$UpdateID
									);
		$upd_corres_attachs		=	"UPDATE CorrespondenceAttachments SET 
							   		UserID = :UserID, Filename = :Filename  
							   		WHERE UpdateID = :UpdateID";
		$res_corres_letters		=	$this->db->getConnection ( "IRECRUIT" )->update ( $upd_corres_attachs, array($params));
	
		return $res_corres_letters;
	}
	
	function checkIfSelected($string, $id) {
		$selected = '';
	
		$e = explode ( ':', $string );
		$ecnt = count ( $e ) - 1;
	
		for($i = 0; $i < $ecnt; $i ++) {
	
			if ($id == $e [$i]) {
				$selected = ' selected';
			}
		}
	
		return $selected;
	} // end function
}
?>
