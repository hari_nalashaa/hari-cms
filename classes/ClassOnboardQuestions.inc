<?php 
/**
 * @class		OnboardQuestions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */
class OnboardQuestions {
    
    var $conn_string        =   "IRECRUIT";
    //Alphabets
    var $alphabet           =   array ('XX', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    
    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method		getOnboardPersonalQuestionsInfo
     * @param		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getOnboardPersonalQuestionsInfo($OrgID, $OnboardFormID) {
    
        $params_onboard_info    =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID);
        $sel_onboard_que_info   =   "SELECT * FROM OnboardQuestions WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND QuestionID IN ('first', 'middle', 'last', 'email', 'country', 'address', 'address2', 'state', 'province', 'county', 'city', 'zip')";
        $res_onboard_que_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_onboard_que_info, array($params_onboard_info) );
    
        $onboard_ques_info      =   array();
        
        if(is_array($res_onboard_que_info['results'])) {
        	foreach($res_onboard_que_info['results'] as $onboard_que_info) {
        	    $onboard_ques_info[$onboard_que_info['QuestionID']]   =   $onboard_que_info;
        	} 
        }
        
        return $onboard_ques_info;
    }
    
    /**
     * @method		getOnboardFormChildQuesForParentQues
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getOnboardFormChildQuesForParentQues($OrgID, $OnboardFormID) {
        
        $form_questions		=	$this->getOnboardQuestionsList($OrgID, $OnboardFormID);
        $child_que_list	    =	array();
        
        if(is_array($form_questions)) {
            foreach($form_questions as $question_id=>$question_info) {
                if($question_info['Active'] == 'Y'
                    && $question_info['ChildQuestionsInfo'] != "")
                {
                    $child_que_list[$question_id]  =   json_decode($question_info['ChildQuestionsInfo'], true);
                }
            }
        }
        
        return $child_que_list;
    }
    
    
    /**
     * @method		getOnboardQuestionsInformation
     * @param		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getOnboardQuestionsInformation($columns = "*", $where_info = array(), $order_by = "", $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $sel_onboard_que_info = "SELECT $columns FROM OnboardQuestions";

        if(count($where_info) > 0) {
            $sel_onboard_que_info .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($order_by != "") $sel_onboard_que_info .= " ORDER BY " . $order_by;
    
        $res_onboard_que_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_onboard_que_info, $info );
    
        return $res_onboard_que_info;
    }

    /**
     * @method		getOnboardQuestionsListByQueTypeID
     * @param		$OrgID, $OnboardFormID, $QuestionTypeID
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getOnboardQuestionsListByQueTypeID($OrgID, $OnboardFormID, $QuestionTypeID) {
    
    	$params_info	=	array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID, ":QuestionTypeID"=>$QuestionTypeID);
    	$sel_que_info	=	"SELECT * FROM OnboardQuestions WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND QuestionTypeID = :QuestionTypeID";
    	$res_que_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_que_info, array($params_info) );
    	
    	return $res_que_info;
    }
    
    /**
     * @method		getOnboardFormIDs
     * @param		$columns, $where_info, $order_by
     * @return		associative array
     * @tutorial	This method will fetch the form questions informtaion
     */
    function getOnboardFormIDs($OrgID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params_onboard_info    =   array(":OrgID"=>$OrgID);
        $sel_onboard_que_info   =   "SELECT OnboardFormID FROM OnboardQuestions WHERE OrgID = :OrgID GROUP BY OnboardFormID";
        $res_onboard_que_info   =   $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_onboard_que_info, array($params_onboard_info) );
    
        $FormIDs    =   array();
        if(is_array($res_onboard_que_info['results'])) {
            foreach ($res_onboard_que_info['results'] as $FormID) {
                $FormIDs[$FormID['OnboardFormID']]   =   $FormID['OnboardFormID'];
            }
        }

        return $FormIDs;
    }
    
    /**
     * @method      getOnboardQuestionsList
     * @param       string $OrgID
     * @param       string $OnboardFormID
     * @return      array
     */
    function getOnboardQuestionsList($OrgID, $OnboardFormID) {
	
	    $params_info       =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID);
	    $sel_onboard_ques  =   "SELECT * FROM OnboardQuestions WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID ORDER BY QuestionOrder ASC";
	    $res_onboard_ques  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_onboard_ques, array($params_info) );
	    $res_onboard_cnt   =   $res_onboard_ques['count'];
	    $res_onboard_res   =   $res_onboard_ques['results'];
	
	    $onboard_ques_list =   array();
	    for($w = 0; $w < $res_onboard_cnt; $w++) {
	        $onboard_ques_list[$res_onboard_res[$w]['QuestionID']]   =   $res_onboard_res[$w];
	    }
	
	    return $onboard_ques_list;
    }
    
    /**
     * @method      getOnboardQuestionInfo
     */
    function getOnboardQuestionInfo($OrgID, $OnboardFormID, $QuestionID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params_info       =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID, ":QuestionID"=>$QuestionID);
        $sel_onboard_ques  =   "SELECT * FROM OnboardQuestions WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND QuestionID = :QuestionID ORDER BY QuestionOrder ASC";
        $res_onboard_ques  =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_onboard_ques, array($params_info) );
    
        return $res_onboard_ques;
    }
    
    /**
     * @method		updateOnboardQuestionsInfo
     * @param		$set_info = array(), 
     * @param		$where_info = array(), 
     * @param		$info
     */
    public function updateOnboardQuestionsInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_que_info = $this->db->buildUpdateStatement("OnboardQuestions", $set_info, $where_info);
        $res_que_info = $this->db->getConnection("IRECRUIT")->update($upd_que_info, $info);
    
        return $res_que_info;
    }
    
    /**
     * @method	updateOnboardQuestionsSortOrder
     * @param	$OnboardFormID, $QuestionID, $SortOrder
     * @return	integer
     */
    function updateOnboardQuestionsSortOrder($OrgID) {
    
        //Begin the transaction
        $this->db->getConnection("IRECRUIT")->beginTransaction();
    
        for($fi = 0; $fi < count($_POST['forms_info']); $fi++) {
    
            $OnboardFormID      =   $_POST['forms_info'][$fi]['FormID'];
            $QuestionID         =   $_POST['forms_info'][$fi]['QuestionID'];
            $SortOrder          =   $_POST['forms_info'][$fi]['SortOrder'];
    
            //Set parameters for prepared query
            $params             =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID, ":QuestionID"=>$QuestionID, ":QuestionOrder"=>$SortOrder);
            $update_sort_order  =   "UPDATE OnboardQuestions SET QuestionOrder = :QuestionOrder";
            $update_sort_order .=   " WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND QuestionID = :QuestionID";
            $result_sort_order  =   $this->db->getConnection("IRECRUIT")->update ( $update_sort_order, array($params) );
        }
    
        //End the transaction
        $this->db->getConnection("IRECRUIT")->endTransaction();
    }
    
    /**
     * @method	getOnboardAndRequisitionCommonQuestionsList
     */
    public function getOnboardAndRequisitionCommonQuestionsList($OrgID, $OnboardFormID, $RequisitionFormID) {

    	$params_info		=	array(
    								":ROrgID"				=>	$OrgID,
    								":OOrgID"				=>	$OrgID,
    								":RequisitionFormID"	=>	$RequisitionFormID,
    								":OnboardFormID"		=>	$OnboardFormID
    							);
		$onboard_ques_list	=	"SELECT R.*";
		$onboard_ques_list	.=	" FROM RequisitionQuestions R, OnboardQuestions O";
		$onboard_ques_list	.=	" WHERE R.OrgID = :ROrgID";
		$onboard_ques_list	.=	" AND O.OrgID = :OOrgID";
		$onboard_ques_list	.=	" AND R.RequisitionFormID = :RequisitionFormID";
		$onboard_ques_list	.=	" AND OnboardFormID = :OnboardFormID";
		$onboard_ques_list	.=	" AND R.QuestionID = O.QuestionID";
		$question_ids_list	=	$this->db->getConnection ( $this->conn_string )->fetchAllAssoc( $onboard_ques_list, array($params_info));

		$que_ids    =   array();
		if(is_array($question_ids_list['results'])) {
			foreach ($question_ids_list['results'] as $QueInfo) {
				$que_ids[$QueInfo['QuestionID']]	=	$QueInfo;
			}
		}
		
		return $que_ids;
    }
    
    /**
     * @method		copyOnboardMasterQuestions
     */
    public function copyOnboardMasterQuestions($OrgID, $OnboardFormID) {

    	//Set where condition
    	$where      =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID");
    	//Set parameters
    	$params     =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$OnboardFormID);
    	//Get Questions Information
    	$results    =   G::Obj('FormQuestions')->getQuestionsInformation('OnboardQuestions', "*", $where, "", array($params));

    	if($results['count'] == 0) {
			//Insert onboard master questions with OrgID 
    		$ins_form_ques 	=	"INSERT INTO OnboardQuestions (OrgID, OnboardFormID, QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock)
							  	SELECT '" . $OrgID . "', '".$OnboardFormID."', QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock 
							  	FROM OnboardQuestions WHERE OrgID = 'MASTER' AND OnboardFormID = '$OnboardFormID'";
    		$res_form_ques	=	$this->db->getConnection ( "IRECRUIT" )->insert($ins_form_ques);
    	} // end if OnboardQuestions
    	
    }
}
?>
