<?php
/**
 * @class		Labels
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 * @tutorial    It needs to implement labels interface having the parameter table_name for all functions
 */

class Labels {

    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    
    /**
     * @method      getLabel
     * @param       string $table_name
     * @param       string $OrgID
     * @param       string $label_id
     * @return      array
     */
    function getLabel($OrgID, $FormID) {
        
        $params = array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
        $sel_label_info = "SELECT FormID FROM FormQuestions WHERE OrgID = :OrgID AND FormID = :FormID";
        $res_label_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_label_info, array($params) );
        
        return $res_label_info;
    }
    

    /**
     * @method      getAllLabels
     * @param       string $table_name
     * @param       string $OrgID
     * @return      array
     */
    function getAllLabels($OrgID) {
        
        $params             =   array(":OrgID"=>$OrgID);
        $sel_label_info     =   "SELECT FormID FROM FormQuestions WHERE OrgID = :OrgID GROUP BY FormID";
        $res_label_info     =   $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_label_info, array($params) );
        $labels_list        =   $res_label_info['results'];
        $labels_count       =   $res_label_info['count'];

        $labels = array();
        for($i = 0; $i < $labels_count; $i++)
        {
            $labels[] = $labels_list[$i]['FormID'];
        }
        
        return $labels;
    }
}
?>
