<?php
/**
 * @class		Interview
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Interview {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial 	Constructor to load the default database
     *           	and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
    * @method              getInterviewForms
    * @param               $columns, $where_info, $order_by
    * @return              associative array
    * @tutorial    This method will fetch data from InterviewForms
    */
    function getInterviewForms($columns = "", $where_info = array(), $order_by = "", $params = array()) {

        $columns = $this->db->arrayToDatabaseQueryString ( $columns );

        $sel_info = "SELECT $columns FROM InterviewForms";

        if (count ( $where_info ) > 0) $sel_info .= " WHERE " . implode ( " AND ", $where_info );

        if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;

        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );

        return $res_info;

    }

    /**
    * @method              getInterviewFormQuestions
    * @param               $columns, $where_info, $order_by
    * @return              associative array
    * @tutorial    This method will fetch data from InterviewFormQuestions
    */
    function getInterviewFormQuestions($columns = "", $where_info = array(), $order_by = "", $params = array()) {

        $columns = $this->db->arrayToDatabaseQueryString ( $columns );

        $sel_info = "SELECT $columns FROM InterviewFormQuestions";

        if (count ( $where_info ) > 0) $sel_info .= " WHERE " . implode ( " AND ", $where_info );

        if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;

        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );

        return $res_info;

    }

    /**
    * @method              getInterviewFormData
    * @param               $columns, $where_info, $order_by
    * @return              associative array
    * @tutorial    This method will fetch data from InterviewFormData
    */
    function getInterviewFormData($columns = "", $where_info = array(), $order_by = "", $params = array()) {

        $columns = $this->db->arrayToDatabaseQueryString ( $columns );

        $sel_info = "SELECT $columns FROM InterviewFormData";

        if (count ( $where_info ) > 0) $sel_info .= " WHERE " . implode ( " AND ", $where_info );

        if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;

        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );

        return $res_info;

    }

    /**
    * @method              getInterviewApplicants
    * @param               $columns, $where_info, $order_by
    * @return              associative array
    * @tutorial    This method will fetch data from InterviewFormData
    */
    function getInterviewApplicants($columns = "", $where_info = array(), $order_by = "", $params = array()) {

        $columns = $this->db->arrayToDatabaseQueryString ( $columns );

        $sel_info = "SELECT $columns FROM InterviewApplicants";

        if (count ( $where_info ) > 0) $sel_info .= " WHERE " . implode ( " AND ", $where_info );

        if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;

        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );

        return $res_info;

    }

    /**
    * @method              getInterviewFormHistory
    * @param               $columns, $where_info, $order_by
    * @return              associative array
    * @tutorial    This method will fetch data from InterviewFormQuestions
    */
    function getInterviewFormHistory($columns = "", $where_info = array(), $order_by = "", $params = array()) {

        $columns = $this->db->arrayToDatabaseQueryString ( $columns );

        $sel_info = "SELECT $columns FROM InterviewFormHistory";

        if (count ( $where_info ) > 0) $sel_info .= " WHERE " . implode ( " AND ", $where_info );

        if ($order_by != "") $sel_info .= " ORDER BY " . $order_by;

        $res_info = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params) );

        return $res_info;

    }
		
    /**
     * @method      presentResults
     */
    function presentResults($OrgID,$RequestID,$USERID,$Role,$PERMIT) {

	  $display="Y";

	  $MANAGERS =   $this->getManagerUserIDs($OrgID, $RequestID);
	  $Owner    =   $this->getOwnerID($OrgID, $RequestID);

	  if (in_array($USERID,$MANAGERS,true)) {
	    $display="N";
	  }
	  if ($USERID == $Owner) {
	    $display="Y";
	  }

        return $display;

    }


    /**
     * @method      getActiveInterviewForm
     */
    function getActiveInterviewForm($OrgID,$RequestID,$ApplicationID,$USERID,$Role,$PERMIT) {

	  $MANAGERS =   $this->getManagerUserIDs($OrgID, $RequestID);
	  $Owner    =   $this->getOwnerID($OrgID, $RequestID);
	  $InterviewFormID = "";

	  $PRESENT=array();
	  if (in_array($USERID,$MANAGERS,true)) {
		  $PRESENT[]="Manager";
	  }
	  if ($USERID == $Owner) {
		  $PRESENT[]="Owner";
	  }
	  if ($PERMIT['Interview_Admin'] == 1) {
		  $PRESENT[]="Admin";
	  }

	  $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
	  $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	  $IFS =   $this->getInterviewForms("*", $where_info, "SortOrder ASC", $params);

	  // loop through all forms and determine next form
	  $IF=array();
	  foreach ($IFS['results'] AS $F) {

	     // determine if the form has data and if so don't display
	     $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "QuestionID = 'process_status'", "Answer = 'final'");
	     $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":InterviewFormID"=>$F['InterviewFormID'],":ApplicationID"=>$ApplicationID);

	     // determine if user filled form if hiring manager form
	     if ($F['Target'] == "Manager") {
		     $where_info[]='UserID = :UserID';
		     $params[':UserID']=$USERID;
	     }

	     $IFD =   $this->getInterviewFormData("*", $where_info, "", $params);

	     if ($IFD['count'] == 0) { 
		     $IF=$F;
		     break;
	     } // end count

	  } // end foreach
		     
             // determine if user is tartet of the form
	     if (in_array($IF['Target'],$PRESENT,true)) {

	          // determine when to present form
		  if($IF['WhenAvailable'] == "Immediate") {

		      // set only one time in the loop to traverse through forms	
		      // hold form if awaiting Interview
		      if ($InterviewFormID == "") {
	                $InterviewFormID = $IF['InterviewFormID'];
		      }

		  } else if ($IF['WhenAvailable'] == "Interview") {

		      // set form to be filled only for applicants selected
		      $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
		      $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		      $IA =   $this->getInterviewApplicants("*", $where_info, "", $params);
		      $INTERVIEW = json_decode($IA['results'][0]['InterviewAppIDs'],true);

		      if (in_array($ApplicationID,$INTERVIEW)) {
			 if ($InterviewFormID == "") {
	                   $InterviewFormID = $IF['InterviewFormID'];
		         }
		      }

		  } // end when availalbe

	      } // end if Target


	return $InterviewFormID;

    }

     /**
     * @method     getManagerUserIDs
     */
    function getManagerUserIDs($OrgID,$RequestID) {

        $sel_info = "SELECT UserID FROM RequisitionManagers";
        $sel_info .= " WHERE OrgID = :OrgID AND RequestID = :RequestID";

        $params_info[':OrgID'] = $OrgID;
        $params_info[':RequestID'] = $RequestID;

        $res_users =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_info, array($params_info) );
	$MANAGERS=array();
	foreach ($res_users['results'] AS $U) {
	   $MANAGERS[]=$U['UserID'];;

	}
        return $MANAGERS;
    }

    /**
     * @method     getOwnerID
     */
    function getOwnerID($OrgID,$RequestID) {

        $sel_info = "SELECT Owner FROM Requisitions";
        $sel_info .= " WHERE OrgID = :OrgID AND RequestID = :RequestID";
        $params_info[':OrgID'] = $OrgID;
        $params_info[':RequestID'] = $RequestID;

        $res_owner =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_info, array($params_info) );

        return $res_owner['Owner'];
    }

    /**
     * @method      getAvailableRequisitions
     */
    function getAvailableRequisitions($OrgID, $RequestID = "") {

	$params_info[':OrgID'] = $OrgID;
	$params_info[':IOrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;

	$sel_forms	=   "SELECT Requisitions.RequestID, Requisitions.Title";
	$sel_forms	.=  " FROM Requisitions";
	$sel_forms	.=  " WHERE Requisitions.OrgID = :OrgID"; 
	$sel_forms	.=  " AND Requisitions.Active = 'Y'"; 
	$sel_forms	.=  " AND Requisitions.RequestID != :RequestID"; 
	$sel_forms	.=  " AND Requisitions.RequestID in ("; 
	$sel_forms	.=  " SELECT RequestID from InterviewForms";
	$sel_forms	.=  " WHERE InterviewForms.Target != '' AND InterviewForms.WhenAvailable != ''";
       	$sel_forms	.=  " AND InterviewFormID IN ("; 
	$sel_forms	.=  " SELECT distinct(InterviewFormID) FROM InterviewFormQuestions WHERE OrgID = :IOrgID AND Active = 'Y'))"; 
        $sel_forms  	.=  " ORDER BY Requisitions.Title ASC";
        $res_forms      =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_forms, array($params_info) );
        return $res_forms;

    }

    /**
     * @method     getCalculableInterviewQuestions 
     */
    function getCalculableInterviewQuestions($OrgID, $RequestID = "", $InterviewFormID="", $QuestionID="") {

	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':InterviewFormID'] = $InterviewFormID;
	$params_info[':QuestionID'] = $QuestionID;

	$IQT = $this->getInterviewQuestionTypes();
	$QT=array();
	foreach($IQT AS $k=>$I) {
		if ($I['Calcuable'] == "Y") {
			$QT[]=$k;
		}
	}

	$sel_forms	=   "SELECT *";
	$sel_forms	.=  " FROM InterviewFormQuestions";
	$sel_forms	.=  " WHERE OrgID = :OrgID"; 
	$sel_forms	.=  " AND RequestID = :RequestID"; 
	$sel_forms	.=  " AND InterviewFormID = :InterviewFormID"; 
	$sel_forms	.=  " AND QuestionID != :QuestionID"; 
	$sel_forms	.=  " AND QuestionTypeID in ('"; 
	$sel_forms	.=   implode($QT,"','");	
	$sel_forms	.=  "')"; 
        $sel_forms  	.=  " ORDER BY QuestionOrder ASC";

        $res_forms      =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_forms, array($params_info) );
        return $res_forms;

    }

    /**
     * @method          insUpdInterviewForms
     * @param           $info, $skip
     * @return          array
     */
    function insUpdInterviewForms($info, $skip = array()) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("InterviewForms", $info, $skip);
        $res_form    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );

        return $res_form;
    }

    /**
     * @method          insUpdInterviewFormQuestions
     * @param           $info, $skip
     * @return          array
     */
    function insUpdInterviewFormQuestions($info, $skip = array()) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("InterviewFormQuestions", $info, $skip);
        $res_form    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );

        return $res_form;
    }

    /**
     * @method          insUpdInterviewData
     * @param           $info, $skip
     * @return          array
     */
    function insUpdInterviewData($info, $skip = array()) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("InterviewFormData", $info, $skip);
        $res_form    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );

        return $res_form;
    }

     /**
     * @method          insInterviewFormHistory
     * @param           $interview_from_history
     * @return          array
     */
    function insInterviewFormHistory($interview_form_history) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("InterviewFormHistory", $interview_form_history);
        $res_form    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );

        return $res_form;
    }

    /**
     * @method          insUpdInterviewApplicants
     * @param           $info, $skip
     * @return          array
     */
    function insUpdInterviewApplicants($info, $skip = array()) {

        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("InterviewApplicants", $info, $skip);
        $res_form    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );

        return $res_form;
    }

    /**
     * @method          getInterviewFormInfo 
     * @param           $OrgID, $RequestID, $InterviewFormID
     * @return          array
     */
    function getInterviewFormInfo($columns = "*", $OrgID, $RequestID = "", $InterviewFormID = "") {

        $columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
        
	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':InterviewFormID'] = $InterviewFormID;

	$sel_form      =   "SELECT $columns FROM InterviewForms";
        $sel_form	.=   " WHERE OrgID = :OrgID";
        $sel_form  	.=   " AND RequestID = :RequestID";
        $sel_form  	.=   " AND InterviewFormID = :InterviewFormID";

        $res_form      =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_form, array($params_info) );
        return $res_form;
    }

    /**
     * @method          copyInterviewForm
     * @param           $OrgID, $RequestID, $InterviewFormID
     * @return          array
     */
    function copyInterviewForm($OrgID, $RequestID = "", $InterviewFormID = "") {

	$New_InterviewFormID = strtotime(date('Y-m-d H:i:s')).uniqid();

	$copy_info = "INSERT INTO InterviewForms";
	$copy_info .= "(OrgID, RequestID, InterviewFormID, FormName, Target, WhenAvailable, SortOrder)";
	$copy_info .= "SELECT";
        $copy_info .= "	OrgID, RequestID, '" . $New_InterviewFormID . "', concat(FormName,' copy'), Target, WhenAvailable, SortOrder";
        $copy_info .= " FROM InterviewForms";
	$copy_info .= " WHERE OrgID = :OrgID";
	$copy_info .= " AND RequestID = :RequestID";
	$copy_info .= " AND InterviewFormID = :InterviewFormID";

	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':InterviewFormID'] = $InterviewFormID;

        $this->db->getConnection ( $this->conn_string )->insert ( $copy_info, array($params_info));

	$copy_info = "INSERT INTO InterviewFormQuestions";
	$copy_info .= "(OrgID, RequestID, InterviewFormID, QuestionID, Question, QuestionTypeID, value, Active, QuestionOrder, Required)";
	$copy_info .= "SELECT";
        $copy_info .= "	OrgID, RequestID, '" . $New_InterviewFormID . "', QuestionID, Question, QuestionTypeID, value, Active, QuestionOrder, Required";
        $copy_info .= " FROM InterviewFormQuestions";
	$copy_info .= " WHERE OrgID = :OrgID";
	$copy_info .= " AND RequestID = :RequestID";
	$copy_info .= " AND InterviewFormID = :InterviewFormID";

        $this->db->getConnection ( $this->conn_string )->insert ( $copy_info, array($params_info));

    }

    /**
     * @method          deleteInterviewForm
     * @param           $OrgID, $RequestID, $InterviewFormID
     * @return          array
     */
    function deleteInterviewForm($OrgID, $RequestID = "", $InterviewFormID = "") {

	$delete_info = "DELETE FROM InterviewFormQuestions";
	$delete_info .= " WHERE OrgID = :OrgID";
	$delete_info .= " AND RequestID = :RequestID";
	$delete_info .= " AND InterviewFormID = :InterviewFormID";

	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':InterviewFormID'] = $InterviewFormID;

        $this->db->getConnection ( $this->conn_string )->delete ( $delete_info, array($params_info));

	$delete_info = "DELETE FROM InterviewForms";
	$delete_info .= " WHERE OrgID = :OrgID";
	$delete_info .= " AND RequestID = :RequestID";
	$delete_info .= " AND InterviewFormID = :InterviewFormID";

        $this->db->getConnection ( $this->conn_string )->delete ( $delete_info, array($params_info));

	$delete_info = "DELETE FROM InterviewFormData";
	$delete_info .= " WHERE OrgID = :OrgID";
	$delete_info .= " AND RequestID = :RequestID";
	$delete_info .= " AND InterviewFormID = :InterviewFormID";

        $this->db->getConnection ( $this->conn_string )->delete ( $delete_info, array($params_info));

    }

    /**
     * @method          deleteInterviewQuestion
     * @param           $OrgID, $RequestID, $InterviewFormID, $QuestionID
     * @return          array
     */
    function deleteInterviewFormQuestion($OrgID, $RequestID = "", $InterviewFormID = "", $QuestionID = "") {

	$delete_info = "DELETE FROM InterviewFormQuestions";
	$delete_info .= " WHERE OrgID = :OrgID";
	$delete_info .= " AND RequestID = :RequestID";
	$delete_info .= " AND InterviewFormID = :InterviewFormID";
	$delete_info .= " AND QuestionID = :QuestionID";

	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':InterviewFormID'] = $InterviewFormID;
	$params_info[':QuestionID'] = $QuestionID;

        $this->db->getConnection ( $this->conn_string )->delete ( $delete_info, array($params_info));

    }

    /**
     * @method          deleteInterviewQuestion
     * @param           $OrgID, $RequestID, $InterviewFormID, $QuestionID
     * @return          array
     */
    function deleteInterviewFormData($OrgID, $RequestID = "", $InterviewFormID = "", $UserID = "", $ApplicationID) {

	$delete_info = "DELETE FROM InterviewFormData";
	$delete_info .= " WHERE OrgID = :OrgID";
	$delete_info .= " AND RequestID = :RequestID";
	$delete_info .= " AND InterviewFormID = :InterviewFormID";
	$delete_info .= " AND UserID = :UserID";
	$delete_info .= " AND ApplicationID = :ApplicationID";

	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':InterviewFormID'] = $InterviewFormID;
	$params_info[':UserID'] = $UserID;
	$params_info[':ApplicationID'] = $ApplicationID;

        $this->db->getConnection ( $this->conn_string )->delete ( $delete_info, array($params_info));

    }

    /**
     * @method          copyInterviewFormsSet
     * @param           $OrgID, $RequestID
     * @return          array
     */
    function copyInterviewFormsSet($OrgID, $RequestID, $CopyRequestID = "") {

	$copy_info = "INSERT INTO InterviewForms";
	$copy_info .= "(OrgID, RequestID, InterviewFormID, FormName, Target, WhenAvailable, SortOrder)";
	$copy_info .= "SELECT";
        $copy_info .= "	OrgID, :RequestID, InterviewFormID, FormName, Target, WhenAvailable, SortOrder";
        $copy_info .= " FROM InterviewForms";
	$copy_info .= " WHERE OrgID = :OrgID";
	$copy_info .= " AND RequestID = :CopyRequestID";

	$params_info[':OrgID'] = $OrgID;
	$params_info[':RequestID'] = $RequestID;
	$params_info[':CopyRequestID'] = $CopyRequestID;

        $this->db->getConnection ( $this->conn_string )->insert ( $copy_info, array($params_info));

	$copy_info = "INSERT INTO InterviewFormQuestions";
	$copy_info .= "(OrgID, RequestID, InterviewFormID, QuestionID, Question, QuestionTypeID, value, Active, QuestionOrder, Required)";
	$copy_info .= "SELECT";
        $copy_info .= "	OrgID, :RequestID, InterviewFormID, QuestionID, Question, QuestionTypeID, value, Active, QuestionOrder, Required";
        $copy_info .= " FROM InterviewFormQuestions";
	$copy_info .= " WHERE OrgID = :OrgID";
	$copy_info .= " AND RequestID = :CopyRequestID";

        $this->db->getConnection ( $this->conn_string )->insert ( $copy_info, array($params_info));


    }

    /**
     * @method          displayInterviewForm
     * @param           array
     * @return          html 
     */
    function displayInterviewForm($OrgID,$RequestID,$InterviewFormID,$ApplicationID,$USERID,$Edit='N') {

	$where_question_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","ApplicationID = :ApplicationID","QuestionID = :QuestionID", "UserID = :UserID");
	$params_question  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$InterviewFormID,":ApplicationID"=>$_REQUEST['ApplicationID']);

	$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active='Y'","QuestionTypeID not in ('TOTAL','PREFERRED')");
   	$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":InterviewFormID"=>$InterviewFormID);
   	$IFQ =   $this->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

	$rtn .= '<div style="margin-top:20px;margin-left:40px;line-height:180%">';
        $rtn .= '<form name="InterviewForm">';
        $rtn .= '<input type="hidden" name="RequestID" value="'.$RequestID.'">';
	$rtn .= '<input type="hidden" name="ApplicationID" value="'.$ApplicationID.'">';
        $rtn .= '<input type="hidden" name="InterviewFormID" value="'.$InterviewFormID.'">';

	$REQUIRED=array();
	foreach ($IFQ['results'] AS $FQ) {

	   $params_question[':QuestionID']=$FQ['QuestionID'];
	   $params_question[':UserID']=$USERID;
           $IFD =   $this->getInterviewFormData("*", $where_question_info, "", $params_question);
           $QUESTIONDATA=$IFD['results'][0];

           if ($FQ['Required'] == "Y") { $REQUIRED[][$FQ['QuestionID']]=preg_replace('/[^A-Za-z0-9-. ]/','',$FQ['Question']); }
           $rtn .= $this->displayQuestion($FQ,$QUESTIONDATA);

	} // end foreach

        $rtn .= '<div style="margin:30px 0 40px 80px;/">';
        $rtn .= '<input type="hidden" id="Required" value=\''.json_encode($REQUIRED).'\'>';

	if ($Edit == 'Y') {
           $rtn .= '<input type="button" value="Edit" class="btn btn-primary" onclick="processEditInterviewForm(\'edit\',\''.$USERID.'\')">';
	} else {
           $rtn .= '<input type="button" value="Finalize" class="btn btn-primary" onclick="processInterviewForm(\'final\')">';
	   $rtn .= '&nbsp;&nbsp;&nbsp;&nbsp;';
	   $rtn .= '&nbsp;&nbsp;&nbsp;&nbsp;';
           $rtn .= '<input type="button" value="Save" class="btn btn-primary" onclick="processInterviewForm(\'temp\')">';
	}

        $rtn .= '</div>' . "\n";

        $rtn .= '</form>';
        $rtn .= '</div>' . "\n";


	return $rtn;

    }


    /**
     * @method          displayQuestion
     * @param           array
     * @return          html 
     */
    function displayQuestion($FQ,$QUESTIONDATA) {

	$rtn="";

	if ($FQ['QuestionTypeID'] == "INSTRUCTION") {

		$rtn .= '<div style="margin-bottom:10px;">';
		$rtn .= $FQ['Question'];
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "DIVIDER") {

		$rtn .= '<div>';
		$rtn .= '<hr size="1" width="90%" align="left">';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "RADIO") {

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		if ($FQ['Required'] == "Y") {
			$rtn .= '&nbsp;<b style="color:DarkRed;">*</b>';
		}
		$rtn .= '<div style="margin: 5px 0 10px 20px;">';
		$VALUES = json_decode($FQ['value'], true);
		foreach ($VALUES AS $V) {
			$rtn .= '<input type="radio" id="'.$FQ['QuestionID'].'" name="'.$FQ['QuestionID'].'" value="'.$V['Value'].'"';
			if ($QUESTIONDATA['Answer'] == $V['Value']) {
			   $rtn .= ' checked';
			} else if ($V['Default'] == "Y") {
			   $rtn .= ' checked';
			}
			$rtn .= '>';
			$rtn .= '&nbsp;';
		        $rtn .=	$V['Display'];
			$rtn .= '&nbsp;&nbsp;&nbsp;';
		}
		$rtn .= '</div>';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "PULLDOWN") {

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		if ($FQ['Required'] == "Y") {
			$rtn .= '&nbsp;<b style="color:DarkRed;">*</b>';
		}
		$rtn .= '<div style="margin: 5px 0 10px 20px;">';
		$VALUES = json_decode($FQ['value'], true);
		$rtn .= '<select id="'.$FQ['QuestionID'].'" name="'.$FQ['QuestionID'].'"';
		$rtn .= '>';
		foreach ($VALUES AS $V) {
			$rtn .= '<option value="'.$V['Value'].'"';
			if ($QUESTIONDATA['Answer'] == $V['Value']) {
			   $rtn .= ' selected';
			} else if ($V['Default'] == "Y") {
			   $rtn .= ' selected';
			}
			$rtn .= '>'.$V['Display'].'</option>';
		}
	        $rtn .= '</select>';
		$rtn .= '</div>';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "TEXTAREA") {

		$rtn .= '<div>';
		$rtn .= $FQ['Question'];
		if ($FQ['Required'] == "Y") {
			$rtn .= '&nbsp;<b style="color:DarkRed;">*</b>';
		}
	       	$rtn .= '<br>';
		$rtn .= '<textarea cols="80" rows="5" id="'.$FQ['QuestionID'].'" name="'.$FQ['QuestionID'].'"';
		$rtn .= '>';
		$rtn .= $QUESTIONDATA['Answer'];
		$rtn .= '</textarea>';
		$rtn .= '</div>';

	}

	return $rtn;

    }

    /**
     * @method          displayFormResults
     * @param           array
     * @return          html 
     */
    function displayFormResults($FQ,$QUESTIONDATA) {

	$rtn = "";

	if ($FQ['QuestionTypeID'] == "INSTRUCTION") {

		$rtn .= '<div style="margin-bottom:10px;">';
		$rtn .= $FQ['Question'];
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "DIVIDER") {

		$rtn .= '<div>';
		$rtn .= '<hr size="1" width="90%" align="left">';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "RADIO") {

		$VALS = json_decode($FQ['value'], true);
		$Preferred="";
		foreach ($VALS AS $V) {
			$VALUES[$V['Value']]=$V['Display'];
			if ($V['Preferred'] == "Y") {
				$Preferred = $V['Value'];
			}
		}

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $VALUES[$QUESTIONDATA['Answer']];
		if ($Preferred != "") {
			$rtn .= '<font style="color:DarkKhaki;">';
			$rtn .= '&nbsp;&nbsp;Preferred Answer: ' . $VALUES[$Preferred];
			$rtn .= '</font>';
		}
		$rtn .= '</div>';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "PULLDOWN") {

		$VALS = json_decode($FQ['value'], true);
		$Preferred="";
		foreach ($VALS AS $V) {
			$VALUES[$V['Value']]=$V['Display'];
			if ($V['Preferred'] == "Y") {
				$Preferred = $V['Value'];
			}
		}

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $VALUES[$QUESTIONDATA['Answer']];
		if ($Preferred != "") {
			$rtn .= '<font style="color:DarkKhaki;">';
			$rtn .= '&nbsp;&nbsp;Preferred Answer: ' . $VALUES[$Preferred];
			$rtn .= '</font>';
		}
		$rtn .= '</div>';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "TEXTAREA") {

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $QUESTIONDATA['Answer'];
		$rtn .= '</div>';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "TOTAL") {

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $QUESTIONDATA['Answer'];
		$rtn .= '</div>';
		$rtn .= '</div>';

	} else if ($FQ['QuestionTypeID'] == "PREFERRED") {

		$PREFERRED = $this->getPreferred();

		$rtn .= '<div style="margin-left:20px;">';
		$rtn .= $FQ['Question'];
		if ($QUESTIONDATA['Answer'] == "P") {
			$color="DarkGreen";
		} else {
			$color="DarkRed";
		}
		$rtn .= '<div style="margin-left:20px;color:'.$color.';">';
		$rtn .= $PREFERRED[$QUESTIONDATA['Answer']];
		$rtn .= '</div>';
		$rtn .= '</div>';

	}

	return $rtn;
    }

     /**
     * @method    displayFormCell 
     */
     function displayFormCell($OrgID, $F, $A, $UserID) {

	// determine if the form has data and if so don't display
        $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "QuestionID = 'process_status'", "Answer = 'final'");
        $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID'],":ApplicationID"=>$A['ApplicationID']);

        // determine if user filled form if hiring manager form
        $where_info[]='UserID = :UserID';
        $params[':UserID']=$UserID;

        $FINAL =   $this->getInterviewFormData("Answer", $where_info, "", $params);

	$rtn = "";


        $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "UserID = :UserID", "ApplicationID = :ApplicationID");
        $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID'],":UserID" => $UserID,":ApplicationID"=>$A['ApplicationID']);
        $IFD =   $this->getInterviewFormData("Answer", $where_info, "", $params);

        if (($FINAL['count'] > 0) && ($IFD['count'] > 0)) {
          $rtn .= '<a href="#" onclick="getInterviewFormResults(\''.$F['RequestID'].'\',\''.$F['InterviewFormID'].'\',\''.$UserID.'\',\''.$A['ApplicationID'].'\');">';
          $rtn .= '<img src="' . IRECRUIT_HOME . 'images/icons/application_form.png" border="0" title="View Form" style="margin:0px 8px 0px 0px;"></a>';
             if ($F['Target'] == "Admin") {
               $rtn .= '<br><font style="font-size:8pt;">' . G::Obj('IrecruitUsers')->getUsersName($OrgID,$UserID) . '</font>';
             }
        } else {
                $rtn .= '-';
        }

	return $rtn;

     }

    /**
    * @method     displayAnswerCell
    */
    function displayAnswerCell($OrgID, $F, $Q, $A, $UserID) {

        $PREFERRED = $this->getPreferred();

	// determine if the form has data and if so don't display
        $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "QuestionID = 'process_status'", "Answer = 'final'");
        $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID'],":ApplicationID"=>$A['ApplicationID']);

        // determine if user filled form if hiring manager form
        $where_info[]='UserID = :UserID';
        $params[':UserID']=$UserID;

        $FINAL =   $this->getInterviewFormData("Answer", $where_info, "", $params);

        $rtn = "";

        $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "UserID = :UserID", "ApplicationID = :ApplicationID", "QuestionID = :QuestionID");
        $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID'],"UserID" => $UserID,":ApplicationID"=>$A['ApplicationID'], "QuestionID" => $Q['QuestionID']);
        $IFD =   $this->getInterviewFormData("Answer", $where_info, "", $params);
        $QD = $IFD['results'][0];

        $Total=0;

        if (($FINAL['count'] > 0) && ($IFD['count'] > 0)) {

            if ($Q['QuestionTypeID'] == "PREFERRED") {
                if ($QD['Answer'] == "P") {
                  $color="DarkGreen";
                } else {
                  $color="DarkRed";
                }
             } else {
                     $color="#000000";
            }
            $rtn .= '<font style="color:'.$color.';">';
             if ($Q['QuestionTypeID'] == "PREFERRED") {
               $rtn .= $PREFERRED[$QD['Answer']];
	       $Answer = $PREFERRED[$QD['Answer']];
             } else {
               $rtn .= $QD['Answer'];
	       $Answer = $QD['Answer'];
               $Total += $QD['Answer'];
             }
             $rtn .= '</font>';
	   
           } else {
                $rtn .= "-";
           }

        return array($rtn,$Answer,$Total);

     } // end function

    /**
     * @method          getInterviewQuestionTypes
     * @param           none
     * @return          array
     */
    function getInterviewQuestionTypes() {

	$QUESTIONTYPEIDS ['RADIO'] = array("Description"=>"Radio","Values"=>"Y","Required"=>"Y","Calcuable"=>"Y");
	$QUESTIONTYPEIDS ['PULLDOWN'] = array("Description"=>"Pulldown","Values"=>"Y","Required"=>"Y","Calcuable"=>"Y");
	$QUESTIONTYPEIDS ['TOTAL'] = array("Description"=>"Reporting Total","Values"=>"Y","Required"=>"","Calcuable"=>"");
	$QUESTIONTYPEIDS ['PREFERRED'] = array("Description"=>"Reporting Pass/Fail Indicator","Values"=>"","Required"=>"","Calcuable"=>"");
	$QUESTIONTYPEIDS ['TEXTAREA'] = array("Description"=>"Text","Values"=>"","Required"=>"Y","Calcuable"=>"");
	$QUESTIONTYPEIDS ['INSTRUCTION'] = array("Description"=>"Instruction","Values"=>"","Required"=>"","Calcuable"=>"");
	$QUESTIONTYPEIDS ['DIVIDER'] = array("Description"=>"Divider","Values"=>"","Required"=>"","Calcuable"=>"");

	return $QUESTIONTYPEIDS;
    }

    /**
     * @method          getInterviewTarget
     * @param           none
     * @return          array
     */
    function getInterviewTarget() {

        $TARGET = array("Admin"=>"Interview Admin","Owner"=>"Requistion Owner","Manager"=>"Requisition Hiring Manager");

	return $TARGET;
    }

    /**
     * @method          getInterviewWhenAvailable
     * @param           none
     * @return          array
     */
    function getInterviewWhenAvailable() {

        $WHENAVAIL = array("Immediate"=>"Immediate","Interview"=>"Interview");

	return $WHENAVAIL;
    }

    /**
     * @method          getPreferred
     * @param           none
     * @return          array
     */
    function getPreferred() {

        $WHENAVAIL = array("P"=>"Pass","F"=>"Fail");

	return $WHENAVAIL;
    }




}

?>
