<?php
/**
 * @class		ApplicationFormQueAns
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query)
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query)
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query)
 * 				$this->db->getConnection("IRECRUIT")->insert($query)
 * 				$this->db->getConnection("USERPORTAL")->update($query)
 * 				$this->db->getConnection("WOTC")->delete($query)
 * @tutorial    Here $AppQueAnswers is a reference variable.
 *              $AppQueAnswers array keys follows below sequence
 *              $AppQueAnswers[$SectionID][$SectionType][$QuestionsListIndex][$QuestionID] = array("Question"=>"xxx", "Answer"=>"xxx", "QuestionTypeID"=>"xxx")
 */

class ApplicationFormQueAns {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
    /**
     * @method      string  getApplicationViewInfo
     * @param       string  $OrgID
     * @param       string  $ApplicationID
     * @param       string  $APPDATA
     */
	function getApplicationViewInfo($OrgID, $ApplicationID, $RequestID, $APPDATA) {

	    $AppQueAnswers =   array();
	    $FormID        =   G::Obj('ApplicantDetails')->getFormID($OrgID, $ApplicationID, $RequestID);
	    $Sections      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
	    
	    foreach($Sections as $SectionID=>$SectionInfo) {
			$SectionType    =   "single";
			$SecQueAnswers 	=	$this->getSingleQuestionSectionInfo($OrgID, $FormID, $SectionID, $SectionType, $APPDATA, $AppQueAnswers);
			$AppQueAnswers 	=	$SecQueAnswers;
	    }

        return $AppQueAnswers;
	}
	
	/**
	 * @method     getSingleQuestionSectionInfo
	 * @param      string $OrgID
	 * @param      string $FormID
	 * @param      string $APPDATA
	 * @param      string $SectionID
	 * @return     string
	 */
	function getSingleQuestionSectionInfo($OrgID, $FormID, $SectionID, $SectionType, $APPDATA, &$AppQueAnswers) {
		
	    global $permit, $feature, $AddressObj, $GenericLibraryObj;
	    
        //Set the parameters
        $params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
        //Set columns
        $columns    =   array("Question", "QuestionID", "QuestionTypeID", "value", "SectionID");
        //Set condition
        $where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
        //Get Questions information
        $results    =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));
        
        $format_address = G::Obj('Address')->formatAddress ( $OrgID, $APPDATA ["country"], $APPDATA ["city"], $APPDATA ["state"], $APPDATA['province'], $APPDATA ["zip"], $APPDATA ["county"] );
        
        //Push formatted address to question answers array
        $AppQueAnswers[1][0]["name"]      =   array(
                                                        "Question"          =>  "County:",
                                                        "QuestionTypeID"    =>  "6",
                                                        "Answer"            =>  $APPDATA ["first"] . ' ' . $APPDATA ["middle"] . ' ' . $APPDATA ["last"]
                                                    );
        $AppQueAnswers[1][0]["faddress"]  =   array(
                                                        "Question"          =>  "Formatted Address:",
                                                        "QuestionTypeID"    =>  "6",
                                                        "Answer"            =>  $format_address
                                                    );
        
	    foreach ($results['results'] as $FQ) {
	        
	        // format answers
	        if ($FQ ['QuestionTypeID'] == 1) {
	            $date_answer = '';
	            if($APPDATA [$FQ ['QuestionID'] . 'from'] != "") {
	                $date_answer = "from " . $APPDATA [$FQ ['QuestionID'] . 'from'];
	            }
    		    if ($APPDATA [$FQ ['QuestionID'] . 'to'] != "") {
                    $date_answer .= " to " . $APPDATA [$FQ ['QuestionID'] . 'to'];
    		    }
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                 =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                   =   $date_answer;
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']           =   1;
	        } // end from, to dates question
	        else if ($FQ ['QuestionTypeID'] == 3) {
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   3;
	        } // end pulldown question
	        else if ($FQ ['QuestionTypeID'] == 5) {
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   5;
	        } // end textarea question
	        else if ($FQ ['QuestionTypeID'] == 6) {
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   6;
	        } // end text box question
	        else if ($FQ ['QuestionTypeID'] == 9) {
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getAnswer9($OrgID, $FQ ['QuestionID'], $APPDATA [$FQ ['QuestionID']], $FQ ['value']);
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        } // end instructions
	        else if ($FQ ['QuestionTypeID'] == 10) {
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   10;
	        } // end date question
	        else if ($FQ ['QuestionTypeID'] == 13) {
	             
	            if(G::Obj('GenericLibrary')->isJSON($APPDATA [$FQ['QuestionID']]) === true) {
	                $Answer13  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']              =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer13[0], $Answer13[1], $Answer13[2], '' );
	            }
	            else if(is_string($APPDATA [$FQ['QuestionID']]) && $APPDATA [$FQ['QuestionID']] != "") {
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']              =   $APPDATA [$FQ['QuestionID']];
	            }
	            else {
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']              =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA [$FQ['QuestionID'] . '1'], $APPDATA [$FQ['QuestionID'] . '2'], $APPDATA [$FQ['QuestionID'] . '3'], '' );
	            }
	             
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']          =   13;
	        } // end phone without extension
	        else if ($FQ ['QuestionTypeID'] == 14) {
	            
	            if(G::Obj('GenericLibrary')->isJSON($APPDATA [$FQ['QuestionID']]) === true) {
	                $Answer14  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']              =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer14[0], $Answer14[1], $Answer14[2], $Answer14[3] );
	            }
	            else if(is_string($APPDATA [$FQ['QuestionID']]) && $APPDATA [$FQ['QuestionID']] != "") {
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']              =   $APPDATA [$FQ['QuestionID']];
	            }
	            else {
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']              =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA [$FQ['QuestionID'] . '1'], $APPDATA [$FQ['QuestionID'] . '2'], $APPDATA [$FQ['QuestionID'] . '3'], $APPDATA [$FQ['QuestionID'] . 'ext'] );
	            }
	            
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                 =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']           =   14;
	        } // end phone with extension
	        else if ($FQ ['QuestionTypeID'] == 15) {
	            if ($_GET ['emailonly'] == 'Y') {
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']             =   $FQ['Question'];
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']               =   'XXX-XX-XXXX';
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']       =   15;
	            } else {
	                $Answer15  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']             =   $FQ['Question'];
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']               =   $Answer15[0] . '-' . $Answer15[1] . '-' . $Answer15[2];
	                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']       =   15;
	            }
	        }
	        else if ($FQ ['QuestionTypeID'] == 17) {
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                 =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                   =   $APPDATA [$FQ ['QuestionID']];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']           =   17;
	        } // end date
	        else if (($FQ ['QuestionTypeID'] == 18)) {
	            
	            $Answer18  =   json_decode($APPDATA[$FQ ['QuestionID']], true);
                
	            foreach ($Answer18 as $Answer18Key=>$Answer18Value) {
	                $APPDATA[$Answer18Key] =   $Answer18Value;
	            }

	            $APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer18);
                	                                  
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	            //$AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getAnswer18($OrgID, $FQ ['QuestionID'], $APPDATA[$FQ ['QuestionID']], $FQ ['value']);
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
	        } // end instructions
	        else if ($FQ ['QuestionTypeID'] == 24) {
	            $date_answer = '';
	            
	            if($APPDATA [$FQ ['QuestionID'] . 'from'] != "") {
	                $date_answer = "from " . $APPDATA [$FQ ['QuestionID'] . 'from'];
	            }

    		    if ($APPDATA [$FQ ['QuestionID'] . 'to'] != "") {
        			$date_answer .= " to " . $APPDATA [$FQ ['QuestionID'] . 'to'];
    		    }

	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                 =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                   =   $date_answer;
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']           =   24;
	        }
	        else if ($FQ ['QuestionTypeID'] == 99) {
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
	            $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   99;
	        } // end pulldown question
	        else if ($FQ ['QuestionTypeID'] == 100) {
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   @unserialize($APPDATA [$FQ ['QuestionID']]);
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   100;
            } // end skills rating
            else if($FQ ['QuestionTypeID'] == 120) {
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   @unserialize($APPDATA [$FQ ['QuestionID']]);
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   120;
            } // end daily shifts
            else if ($FQ ['QuestionTypeID'] == 1818) {
                $Answer1818  = json_decode($APPDATA[$FQ ['QuestionID']], true);
                
                foreach ($Answer1818 as $Answer1818Key=>$Answer1818Value) {
                    $APPDATA[$Answer1818Key] =   $Answer1818Value;
                }
                
                $APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer1818);
                
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                //$AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getAnswer1818($OrgID, $FQ ['QuestionID'], $APPDATA[$FQ ['QuestionID']], $FQ ['value']);
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
            } // end instructions
            else if ($FQ ['value']) {
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $this->getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
            }
            else {
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Question']                  =   $FQ['Question'];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['Answer']                    =   $APPDATA [$FQ ['QuestionID']];
                $AppQueAnswers[$SectionID][0][$FQ ['QuestionID']]['QuestionTypeID']            =   $FQ ['QuestionTypeID'];
            }
	    }
	    
	    return $AppQueAnswers;
	}

	/**
	 * @method     getValueList
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getValueList($value) {
	     
        $values_list    =   array();
        $que_values     =   explode ( '::', $value );
        
        foreach ( $que_values as $v => $q ) {
            list ( $vv, $qq )   =   explode ( ":", $q);
            $values_list[$vv]   =   $qq;
        }
        
        return $values_list;
	}
	
	/**
	 * @method     getAnswer9
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer9($OrgID, $QuestionID, $Answer, $QueValue) {
	    
        $skills_info        =   json_decode($Answer, true);
        $ques_values        =   $this->getValueList($QueValue);
        
        $rtn                =   '';
        foreach($skills_info as $skills_key=>$skills_value) {
            $values  =  array_values($skills_value);
            $rtn    .=  '&#8226;&nbsp;<strong>' . $ques_values[$values[0]] . ", " . $values[1] . " yrs. (" . $values[2].")</strong><br>";
        }
        
        return $rtn;
	}
	
	/**
	 * @method     getAnswer18
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer18($OrgID, $QuestionID, $Answer, $QueValue) {
	
        $info   =   json_decode($Answer, true);
        $values =   $this->getValueList($QueValue);
        
        $rtn    =   '';
	    foreach($info as $key=>$value) {
	        $rtn    .=  '&#8226;&nbsp;' . $values[$value];
	    }
	
	    return $rtn;
	}
	
	/**
	 * @method     getAnswer1818
	 * @param      string  $OrgID
	 * @param      string  $QuestionID
	 * @param      string  $Answer
	 */
	function getAnswer1818($OrgID, $QuestionID, $Answer, $QueValue) {
	
	    $info      =   json_decode($Answer, true);
	    $values    =   $this->getValueList($QueValue);
	
	    $rtn       =   '';
	    foreach($info as $key=>$value) {
	        $rtn .= '&#8226;&nbsp;' .  $values[$value] . "\n";
	    }
	    	
	    return $rtn;
	}
	
	/**
	 * @method     getDisplayValue
	 * @param      string $answer
	 * @param      string $displaychoices
	 * @return     string
	 */
	function getDisplayValue($answer, $displaychoices) {
	    $rtn = '';
	
	    if ($answer != '') {
	        $Values = explode ( '::', $displaychoices );
	        foreach ( $Values as $v => $q ) {
	            list ( $vv, $qq ) = explode ( ":", $q, 2 );
	            if ($vv == $answer) {
	                $rtn .= $qq;
	            }
	        }
	    }
	    
	    return $rtn;
	    
	} // end of function
	
	/**
	 * @method     getMultiAnswer
	 * @param      array $APPDATA
	 * @param      string $id
	 * @param      string $displaychoices
	 * @param      string $questiontype
	 * @return     string
	 */
	function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
	    $rtn = '';
	    $val = '';
	    $lex = '';
	
	    if (($id == 'daysavailable') || ($id == 'typeavailable')) {
	
	        $le = ', ';
	        $li = '';
	        $lecnt = - 2;
	    } else {
	
	        $le = '';
	        $li = '&#8226;&nbsp;';
	        $lecnt = - 4;
	    }
	
	    if($questiontype == 18 || $questiontype == 1818) {
	        $displaychoices_vals = explode ( '::', $displaychoices );
	        $cnt   =   count($displaychoices_vals);
	    }
	    else {
	        $cnt   =   $APPDATA [$id . 'cnt'];
	    }
	
	    
	    for($i = 1; $i <= $cnt; $i ++) {
	
	        $val = $this->getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
	        
	        if ($val) {
	            $rtn .= $li . $val;
	            $lex = 'on';
	            $val = '';
	        }
	
	        if ($questiontype == 9) {
	            	
	            $val = $APPDATA [$id . '-' . $i . '-yr'];
	            if ($val) {
	                $rtn .= ',&nbsp;' . $val . ' yrs. ';
	                $lex = 'on';
	                $val = '';
	            }
	            	
	            $val = $APPDATA [$id . '-' . $i . '-comments'];
	            if ($val) {
	                $rtn .= '&nbsp;(' . $val . ') ';
	                $lex = 'on';
	                $val = '';
	            }
	        } // end questiontype
	
	        if ($lex == 'on') {
	            $rtn .= $le;
	            $lex = '';
	        }
	    }
	
	    $rtn = substr ( $rtn, 0, $lecnt );
	    
	    
	    return $rtn;
	} // end of function	
}
