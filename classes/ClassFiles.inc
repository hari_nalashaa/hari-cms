<?php 
class Files {
	
    /**
     * @method  delDir($src)
     * @param   string  $src
     */
    function delDir($src) {
        $dir = opendir($src);
        
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $src . '/' . $file;
                if ( is_dir($full) ) {
                    $this->delDir($full);
                }
                else {
                    unlink($full);
                }
            }
        }
        
        closedir($dir);
        rmdir($src);
    }
    
    /**
     * @method  getFilePathsInDir
     */
    function getFilePathsByDir($dir) {
        $files_list =   [];
        $dir_info   =   array_diff(scandir($dir), array('.', '..'));
        
        foreach ($dir_info as $sub_dir_info) {
            if(is_dir($sub_dir_info)) {
                
                $sub_dir_list   =   array_diff(scandir($sub_dir_info), array('.', '..'));
                foreach ($sub_dir_list as $files) {
                    $files_list[]   =   $files;
                }
            }
            else {
                $files_list[]   =   $sub_dir_info;
            }
        }
        
        return $files_list;
    }
}
?>