<?php
/**
 * @class		CustomLinks
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class CustomLinks {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method getCustomLinks
	 */
	function getCustomLinks() {
		global $OrgID, $USERID;
		
		//Set parameters for prepared query
		$params   =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
		$sel_custom_links = "SELECT * FROM CustomLinks WHERE OrgID = :OrgID AND UserID = :UserID ORDER BY SortOrder ASC";
		$res_custom_links = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_custom_links, array($params) );
		
		return $res_custom_links;
	}
	
	/**
	 * @method insertCustomLink
	 * @param array $vals        	
	 */
	function insertCustomLink($vals) {
		global $OrgID, $USERID;
		
		//Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
		$sel_max_sort_order	=	"SELECT MAX(SortOrder) AS SortOrder FROM CustomLinks 
							   	WHERE OrgID = :OrgID AND UserID = :UserID";
		$res_max_sort_order	=	$this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_max_sort_order, array($params) );
		$SortOrder			=	$res_max_sort_order ['SortOrder'] + 1;
		
		//Set parameters for prepared query
        $params             =   array(
                                    ":OrgID"        =>  $OrgID,
                                    ":UserID"       =>  $USERID,
                                    ":LinkTitle"    =>  $vals['LinkTitle'],
                                    ":SortOrder"    =>  $SortOrder,
                                    ":LinkTarget"   =>  $vals ['LinkTarget']
                                );		
		$ins_custom_links	=	"INSERT INTO CustomLinks(OrgID, UserID, LinkTitle, SortOrder, LinkTarget) 
							 	VALUES(:OrgID, :UserID, :LinkTitle, :SortOrder, :LinkTarget)";
		$res_custom_links	=	$this->db->getConnection ( $this->conn_string )->insert ( $ins_custom_links, array($params) );
		
		return $res_custom_links;
	}
	
	/**
	 * @method deleteCustomLink
	 * @param int $link_id        	
	 */
	function deleteCustomLink($link_id) {
		global $OrgID, $USERID;
		
		//Set parameters for prepared query
        $params             =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":LinkID"=>$link_id);
        $del_custom_link    =   "DELETE FROM CustomLinks WHERE OrgID = :OrgID AND UserID = :UserID AND LinkID = :LinkID";
        $res_custom_link    =   $this->db->getConnection ( $this->conn_string )->delete ( $del_custom_link, array($params) );
		
		return $res_custom_link;
	}
	
	/**
	 * @method updateCustomLinksSortOrder
	 * @param array $form_data        	
	 */
	function updateCustomLinksSortOrder($form_data) {
		global $OrgID, $USERID;
		
		$upd_sort_order  = "UPDATE CustomLinks SET SortOrder = :SortOrder";
		$upd_sort_order .= " WHERE UserID = :UserID";
		$upd_sort_order .= " AND OrgID = :OrgID";
		$upd_sort_order .= " AND LinkID = :LinkID";

		//Set parameters for prepared query
		$params   =   array(":UserID"=>$USERID, ":OrgID"=>$OrgID);
		
		foreach ( $form_data as $custom_links_info ) {
			$params[":SortOrder"]    =   $custom_links_info ['SortOrder'];
			$params[":LinkID"]       =   $custom_links_info ['LinkID'];
			$res_sort_order          =   $this->db->getConnection ( $this->conn_string )->update ( $upd_sort_order, array($params) );
		}
	}
}
?>
