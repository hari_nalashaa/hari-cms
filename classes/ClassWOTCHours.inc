<?php 
/**
 * @class		WOTCHours
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCHours
{
    var $conn_string       =   "WOTC";

    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method		getHoursInfo
     * @param		mixed   $columns
     * @param		array   $where_info
     * @param 		string  $order_by
     * @param       string  $group_by
     * @param 		array   $info
     * @return 		array
     */
    function getHoursInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
        $sel_hrs = "SELECT $columns FROM Hours";
    
        if(count($where_info) > 0) {
            $sel_hrs .= " WHERE " . implode(" AND ", $where_info);
        }
    
        if($group_by != "") $sel_hrs .= " GROUP BY " . $group_by;
        if($order_by != "") $sel_hrs .= " ORDER BY " . $order_by;
    
        $res_hrs = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_hrs, $info );
    
        return $res_hrs;
    }

    /**
     * @method      hoursTotal
     * @param       string $wotcID
     * @param       string $ApplicationID
     * @param       string $year
     * @return      string
     */
    function hoursTotal($wotcID, $ApplicationID, $year)
    {
        $query      =   "SELECT SUM(Hours) Hours, SUM(Wages) Wages, MAX(IF(ASOF = '0000-00-00','',ASOF)) ASOF,";
        $query      .=  " GROUP_CONCAT(Comments) Comments";
        $query      .=  " FROM Hours WHERE wotcID = :wotcid AND ApplicationID = :applicationid AND Year = :year";
        $query      .=  " GROUP BY wotcID, ApplicationID";
        $params     =   array(':wotcid'=>$wotcID, ':applicationid'=>$ApplicationID, ':year'=>$year);
        $HOURS_RES  =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $query, array($params) );
        $HOURS      =   $HOURS_RES['results'];
    
        return $HOURS;
    } // end function
    
    /**
     * @method      insHoursInfo
     * @param       $info
     */
    function insHoursInfo($info) {
    
        $ins_app_data   =   $this->db->buildInsertStatement("Hours", $info);
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data["stmt"], $ins_app_data["info"] );
    
        return $res_app_data;
    }
    
    /**
     * @method      updHoursInfo
     * @param       $set_info, $where_info, $info
     */
    function updHoursInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_applications_info = $this->db->buildUpdateStatement("Hours", $set_info, $where_info);
        $res_applications_info = $this->db->getConnection( $this->conn_string )->update($upd_applications_info, $info);
    
        return $res_applications_info;
    }
    
    /**
     * @method		delHoursInfo
     * @param		$where_info = array(), $info = array()
     */
    function delHoursInfo($where_info = array(), $info = array()) {
    
        if (count ( $where_info ) > 0) {
            $del_rows   =   "DELETE FROM Hours";
            $del_rows   .=  " WHERE " . implode ( " AND ", $where_info );
        }
    
        $res_rows = $this->db->getConnection ( $this->conn_string )->delete ( $del_rows, $info );
    
        return $res_rows;
    }
    
}
?>
