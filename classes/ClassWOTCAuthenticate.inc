<?php
/**
 * @class		WOTCAuthenticate
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCAuthenticate
{
    public $db;
    
    var $conn_string       =   "WOTC";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method      authenticate
     * @param       string $cookie
     * @return      array
     */
    public function authenticate($cookie)
    {
        $AUTH = array();

	list($adminuser,$companyid) = explode(':',$cookie);
	if ($companyid != "") {

	    $AUTH['UserID']=$adminuser;

            $query =  "SELECT OrgData.wotcID";
            $query .= " FROM OrgData";
            $query .= " WHERE OrgData.CompanyID = :CompanyID";
            $params     =   array(':CompanyID'=>$companyid);
            $IDS_RES    =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($query, array($params));
            $IDS        =   $IDS_RES['results'];

            foreach ($IDS as $ID) {
                $ACCESSWOTCID[] = $ID['wotcID'];
            }

            $AUTH['ACCESS'] = $ACCESSWOTCID;

            if (sizeof($IDS) == 0) {
                $AUTH['ACCESS'] = array(
                    'NONE'
                );
            }

	} else { // else adminuser
        
        // general lockout for time
        $query  =   "UPDATE Users SET SessionID = '' WHERE LastAccess <= DATE_SUB(NOW(), INTERVAL 90 MINUTE) AND (Persist IS NULL OR Persist = 0)";
        $this->db->getConnection ( $this->conn_string )->update($query);
        
        $go = WOTCADMIN_HOME . "login.php";
        if ($_GET['wotcID']) {
            $go .= "?wotcID=" . $_GET['wotcID'];
        }

        // check signon from iRecruit
        if ($_GET['acc'] != "") {
	
            $params =   array();
	    $query  = "SELECT Users.UserID";
	    $query .= " FROM Users";
	    $query .= " JOIN OrgData ON OrgData.CompanyID = Users.CompanyID";
	    $query .= " WHERE OrgData.wotcID = :wotcID";
	    $query .= " AND OrgData.Active = 'Y'";
	    $query .= " LIMIT 1";
            $params[':wotcID']  =   !empty($_GET['wotcID']) ? $_GET['wotcID']: '';
            list ($user) = $this->db->getConnection ( $this->conn_string )->fetchRow($query, array($params));
            
            if ($user) {
                // set a new Session ID
                // randomize a session key
                $query          =   "SELECT DATE_FORMAT(NOW(),'%Y%m%d%H%m%s')";
                list ($SESS)    =   $this->db->getConnection ( $this->conn_string )->fetchRow($query);
                $cookie         =   uniqid($SESS);
                
                $query          =   "UPDATE Users SET LastAccess = NOW(), SessionID = :sessionid WHERE UserID = :userid";
                $params         =   array(':sessionid'=>$cookie, ':userid'=>$user);
                $this->db->getConnection ( $this->conn_string )->update($query, array($params));
                
                setcookie("WID", $cookie, time() + 7200, "/");
                
                exit(header('Location: ' . WOTCADMIN_HOME . "index.php"));
            } // end user
        } // end acc
          
        // bounce to login page if no COOKIE
        if (! $cookie) {
            exit(header('Location: ' . $go));
        }
        
        // bounce if COOKIE not identifyable
        $query      =   "SELECT UserID, CAST(Persist AS UNSIGNED INT) AS Persist FROM Users WHERE SessionID = :sessionid";
        $params     =   array(':sessionid'=>$cookie);
        $AUTH       =   $this->db->getConnection ( $this->conn_string )->fetchAssoc($query, array($params));
        
        
        if ($AUTH['UserID'] == "") {
            exit(header('Location: ' . $go));
        }

	    $query =  "SELECT OrgData.wotcID";
	    $query .= " FROM OrgData";
	    $query .= " JOIN Users ON OrgData.CompanyID = Users.CompanyID";
	    $query .= " WHERE Users.UserID = :UserID";
            $params     =   array(':UserID'=>$AUTH['UserID']);
            $IDS_RES    =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc($query, array($params));
            $IDS        =   $IDS_RES['results'];
        
        foreach ($IDS as $ID) {
            $ACCESSWOTCID[] = $ID['wotcID'];
        }
        
        $AUTH['ACCESS'] = $ACCESSWOTCID;
        
        if (sizeof($IDS) == 0) {
            $AUTH['ACCESS'] = array(
                'NONE'
            );
        }
        
        $query  =   "UPDATE Users SET LastAccess = NOW() WHERE UserID = :userid";
        $params =   array(':userid'=>$AUTH['UserID']);
        $this->db->getConnection ( $this->conn_string )->update($query, array($params));
        
        if ($AUTH['Persist'] == 1) {
            setcookie("WID", $cookie, time() + 2592000, "/");
        }

	} // end else adminuser
        
        return $AUTH;
    } // end function
} // end Class
