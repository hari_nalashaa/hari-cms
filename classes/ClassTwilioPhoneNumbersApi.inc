<?php
/**
 * @class		TwilioPhoneNumbersApi
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

class TwilioPhoneNumbersApi {
	use TwilioSettings;
	
	public $db                  =	"";
    public $parse_client;

	var $conn_string       		=   "IRECRUIT";
	
	/**
	 * @tutorial	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
    public function __construct() {
        // Create a new instance of a SOAP 1.2 client
        $this->db           =   Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
        
        //Get twilio settings
        $this->getTwilioSettings();
    } // end function

    /**
     * @method		getAccountIncomingNumbers
     */
    public function getAccountIncomingNumbers($OrgID) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
		
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
        $phone_numbers_info     =   array();
    	
    	try {
			$incoming_phone_numbers	=	$client->incomingPhoneNumbers->read(array(), 100);
			
			$properties			=	array();
			$sms_capabile_nums	=	array();
			
			for($c = 0; $c < count($incoming_phone_numbers); $c++) {
			
				$properties_obj	=	$incoming_phone_numbers[$c];
			
				//Get the Protected properties from SMS object, by extending the object through ReflectionClass
				$reflection     =   new ReflectionClass($properties_obj);
				$property       =   $reflection->getProperty("properties");
				$property->setAccessible(true);
				$properties		=   $property->getValue($properties_obj);
			
				if($properties['capabilities']['sms']	==	1) {
					$sms_capabile_nums[]   =   $properties['phoneNumber'];
					$phone_numbers_info[]  =   $properties;
				}
			}
				
    		//Log the file information
    		//Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-account-phone-numbers-api.txt", serialize($incoming_phone_numbers), "w+", false);
    
    		// Display a confirmation message on the screen
			return array("Code"=>"Success", "Message"=>"Get Account Incoming Numbers", "Response"=>$sms_capabile_nums, "PhoneNumbersInfo"=>$phone_numbers_info);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		//Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-account-phone-numbers-api-errors.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to get account incoming numbers", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }

	/**
	 * @method		getAvailablePhoneNumbers
	 */
    public function getAvailablePhoneNumbers($OrgID, $info = array()) {

    	$account_info 	=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    	
    	if(isset($info['areaCode']) && $info['areaCode'] != "") {
    		//Get Area Information
    		$area_info		=	$info;
    	}
    	else {
    		//Get Area Information
    		$area_info		=	array("areaCode"=>$account_info["AreaCode"]);
    	}
    	
    	//If both area codes are empty, will have to select global phone numbers
    	if($account_info["AreaCode"] == "" && $info['areaCode'] == "") {
    		$area_info = array();
    	}
    	 
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);

    	try {
    		//array("areaCode" => 205)
    		//read(array("areaCode" => 510), 20)
    		//Get Response Of Phone Numbers
    		$phone_numbers_api	=	$client->availablePhoneNumbers("US")->local->read($area_info, 20);
    		
    		$phone_numbers_list	=	array();
    		for($p = 0; $p < count($phone_numbers_api); $p++) {
    			$phone_numbers_list[]	=	$phone_numbers_api[$p]->phoneNumber;
    		}

    		//In case didn't find the phone numbers with area code
    		if(count($phone_numbers_list) == 0) {
	    		$phone_numbers_api	=	$client->availablePhoneNumbers("US")->local->read(array(), 20);

	    		$phone_numbers_list	=	array();
	    		for($p = 0; $p < count($phone_numbers_api); $p++) {
	    			$phone_numbers_list[]	=	$phone_numbers_api[$p]->phoneNumber;
	    		}
    		}
    		
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-phone-numbers-api.txt", serialize($phone_numbers_api), "w+", false);
    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Get Available Phone Numbers", "Response"=>$phone_numbers_list);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-phone-numbers-api-errors.txt", serialize($e->getMessage()), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to get available Phone Numbers", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    }

    /**
     * @method		createIncomingPhoneNumber
     * @param		$available_phone_num
     */
    public function createIncomingPhoneNumber($OrgID, $available_phone_num) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
		
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
	    	$incoming_phone_number = $client->incomingPhoneNumbers->create(array("phoneNumber" => $available_phone_num));
	    	
	    	$message = "Hi David, <br>";
	    	$message .= "Customer ".$OrgName.": ".$OrgID." - have purchased a new phone number, It needs the MMS capable settings. Please updated it.<br>";
	    	$message .= "Phone Number: " . $available_phone_num . "<br>";
	    	$message .= "Regards,<br>";
	    	$message .= "iRecruit Team";
	    	
	    	// Set who the message is to be sent from
	    	G::Obj('PHPMailer')->setFrom ( "info@irecruit-us.com", "iRecruit" );
	    	// Set an alternative reply-to address
	    	G::Obj('PHPMailer')->addReplyTo ( "info@irecruit-us.com", "iRecruit" );
	    	
	    	// Set who the message is to be sent to
	    	G::Obj('PHPMailer')->addAddress ( 'David Edgecomb <dedgecomb@irecruit-software.com>' );
	    	// Set the subject line
	    	G::Obj('PHPMailer')->Subject = "Twilio New Phone Number Notification";
	    	// convert HTML into a basic plain-text alternative body
	    	G::Obj('PHPMailer')->msgHTML ( $message );
	    	// Content Type Is HTML
	    	G::Obj('PHPMailer')->ContentType = 'text/html';
	    	
	    	G::Obj('PHPMailer')->send ();
	    	
	    	//Log the file information
	    	Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-new_phone_number-api.txt", $message, "w+", true, "Twilio New Phone Number Notification");
	    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-incoming_phone_number-api.txt", serialize($incoming_phone_number), "w+", false);
    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully created the phone number", "Response"=>$incoming_phone_number);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-incoming_phone_number-api-errors.txt", serialize($e->getMessage()), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to create phone number", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    /**
     * @method      deletePhoneNumber
     */
    public function deletePhoneNumber($OrgID, $sid) {
        
        $account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
        
        $client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
        
        try {
            $del_phone_number_info  =   $client->incomingPhoneNumbers($sid)->delete();
            
            //Log the file information
            Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-delete-phone-number-api.txt", serialize($del_phone_number_info), "w+", false);
            
            // Display a confirmation message on the screen
            return array("Code"=>"Success", "Message"=>"Deleted phone number successfully", "Response"=>$phone_numbers);
            
        }
        catch (RestException | TwilioException | Exception $e) {
            
            //Log the file information
            Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-delete-phone-number-api-errors.txt", serialize($e->getMessage()), "w+", false);
            
            //Return error information
            return array("Code"=>"Failed", "Message"=>"Failed to deleted the phone number successfully", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
        }
        
    }
    
    /**
     * @method		getAvailableCountries
     */
    public function getAvailableCountries($OrgID) {
    	 
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
		
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    
    	try {
    		$phone_numbers = $client->availablePhoneNumbers->read(array(), 20);
    			
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-phone-numbers-api.txt", serialize($conversation_info), "w+", false);
    
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Get Available Countries", "Response"=>$phone_numbers);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-get-phone-numbers-api-errors.txt", serialize($e->getMessage()), "w+", false);
    
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to get available countries", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	 
    }
    
    /**
     * @method		getPhoneNumbersCountByMsgServiceID
     */
    public function getPhoneNumbersCountByMsgServiceID($OrgID, $msg_service_id) {
    	
    	$account_info = G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
		
    	$client = new Client($account_info['AccountSid'], $account_info['AuthToken']);
    	
    	try {
    		$phone_numbers	=	$client->messaging->v1->services($msg_service_id)
                                      ->phoneNumbers
                                      ->read(400);
			
    		$phone_numbers_count = 0;
			foreach ($phone_numbers as $record) {
			    $phone_numbers_count++;
			}
    		 
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-phone-numbers-count-by-service-id-api.txt", serialize($phone_numbers), "w+", false);
    	
    		// Display a confirmation message on the screen
    		return array("Code"=>"Success", "Message"=>"Successfully fetched the phone numbers", "Response"=>$phone_numbers, "PhoneNumbersCount"=>$phone_numbers_count);
    		 
    	}
		catch (RestException | TwilioException | Exception $e) {
    	
    		//Log the file information
    		Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-phone-numbers-count-by-service-id-errors.txt", serialize($e->getMessage()), "w+", false);
    	
    		//Return error information
    		return array("Code"=>"Failed", "Message"=>"Failed to fetched the phone numbers", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
    	}
    	
    }
    
    /**
     * @method		getWotcIDBy
     */
    public function getPhoneNumberInfo($OrgID, $call_sid) {
        
        $account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
        
        $client         =   new Client($account_info['AccountSid'], $account_info['AuthToken']);
        
        try {
            $phone_number_info  =   $client->calls($call_sid)->fetch();
            
            $message    =   "<pre>" . print_r($phone_number_info, true);
            
            // Set the subject line
            G::Obj('PHPMailer')->Subject = "Linked Information";
            // convert HTML into a basic plain-text alternative body
            G::Obj('PHPMailer')->msgHTML ( $message );
            
            // Multiple recipients
            $to = 'dedgecomb@irecruit-software.com'; // note the comma
            
            // Subject
            $subject = 'WOTC Inside Test';
            
            // To send HTML mail, the Content-type header must be set
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            
            // Additional headers
            $headers[] = 'From: WebApps@iRecruit-us.com';
            
            mail($to, $subject, $message, implode("\r\n", $headers));
            
            //Log the file information
            Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-phone-numbers-count-by-service-id-api.txt", serialize($phone_numbers), "w+", false);
            
            // Display a confirmation message on the screen
            return array("Code"=>"Success", "Message"=>"Successfully fetched the phone numbers", "Response"=>$phone_number_info);
            
        }
        catch (RestException | TwilioException | Exception $e) {
            
            //Log the file information
            Logger::writeMessage(ROOT."logs/twilio/" . date('Y-m-d-H-i-s') . "-phone-numbers-count-by-service-id-errors.txt", serialize($e->getMessage()), "w+", false);
            
            //Return error information
            return array("Code"=>"Failed", "Message"=>"Failed to fetched the phone numbers", "Response"=>$e->getCode() . ' : ' . $e->getMessage());
        }
        
    }
    
} // end Class
