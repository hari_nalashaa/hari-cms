<?php
/**
 * @class		HighlightRequisitions
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection, by default connection is based on 
 * 				constructor identifer that we are using
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class HighlightRequisitions extends Requisitions {

	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}

	/**
	 * @method		getHighlightRequisitionIcons
	 * @param		$OrgID
	 * @return		associative array
	 */
	function getHighlightRequisitionIcons($JobType = "") {
	
	    // Set parameters for prepared query
	    $params  = array();
	    $sel_info = "SELECT * FROM HighlightRequisitionIcons";
	    if($JobType == "New") {
	        $sel_info .= " WHERE JobType = :JobType";
	        $params = array(":JobType"=>$JobType);
	    }
	    else if($JobType == "Hot") {
	        $sel_info .= " WHERE JobType = :JobType";
	        $params = array(":JobType"=>$JobType);
	    }
	    
	    $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAllAssoc ( $sel_info, array ($params) );
	
	    return $res_info;
	}
	
	/**
	 * @method		getHighlightRequisitionIconInfo
	 * @param		$IconID
	 * @return		associative array
	 */
	function getHighlightRequisitionIconInfo($IconID) {
	
	    // Set parameters for prepared query
	    $params = array (":IconID"  =>  $IconID);
	    $sel_info = "SELECT * FROM HighlightRequisitionIcons WHERE IconID = :IconID";
	    $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_info, array ($params) );
	
	    return $res_info;
	}
	
	/**
	 * @method     insHighlightRequisitionIcons
	 *
	 */
	public function insHighlightRequisitionIcons($JobType, $JobIcon) {
	     
	    $params = array(":JobType"=>$JobType, ":JobIcon"=>$JobIcon);
	    $ins_highlight_req  = "INSERT INTO HighlightRequisitionIcons (`JobType`, `JobIcon`) VALUES(:JobType, :JobIcon)";
	    $res_highlight_req  = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_highlight_req, array($params) );
	     
	    return $res_highlight_req['insert_id'];
	}
	
	/**
	 * @method     delHighlightRequisitionIcon
	 *
	 */
	public function delHighlightRequisitionIcon($IconID) {
	
	    $icon_info = $this->getHighlightRequisitionIconInfo($_REQUEST['IconID']);
	    @unlink(IRECRUIT_DIR . '/vault/highlight/'. $icon_info['JobIcon']);
	    
	    $params = array(":IconID"=>$IconID);
	    $del_highlight_req_icon = "DELETE FROM HighlightRequisitionIcons WHERE `IconID` = :IconID";
	    $res_highlight_req_icon = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_highlight_req_icon, array($params) );
	    
	    return $res_highlight_req_icon;
	}
	
	/**
	 * @method     updHighlightRequisitionIcon
	 *
	 */
	public function updHighlightRequisitionIcon($IconID, $JobIcon) {
	
	    if($IconID != "" || $JobIcon != "") {

	        $params = array(":IconID"=>$IconID);
	        $ins_highlight_req = "UPDATE HighlightRequisitionIcons SET ";
	         
	        $set_info = array();
	        if($JobIcon != "") {
	            $set_info[] =  "`JobIcon` = :JobIcon";
	            $params[":JobIcon"] = $JobIcon;
	        }
	         
	        $ins_highlight_req  .= implode(", ", $set_info);
	        $ins_highlight_req  .= " WHERE IconID = :IconID";
	        
	        $res_highlight_req  = $this->db->getConnection ( "IRECRUIT" )->update ( $ins_highlight_req, array($params) );
	        
	        return $res_highlight_req['affected_rows'];
	    }
	}
	
	
	/**
	 * @method     insUpdHighlightRequisitionSettings
	 * 
	 */
	public function insUpdHighlightRequisitionSettings($OrgID, $HighlightHotJob, $HighlightNewJob, $NewJobDays, $NewJobIcon, $HotJobIcon) {
		
	    $params = array(
	                       ":IOrgID"           =>  $OrgID, 
	                       ":IHighlightNewJob" =>  $HighlightNewJob, 
	                       ":IHighlightHotJob" =>  $HighlightHotJob, 
	                       ":INewJobDays"      =>  $NewJobDays,
	                       ":INewJobIcon"      =>  $NewJobIcon,
	                       ":IHotJobIcon"      =>  $HotJobIcon,
	                       ":UHighlightNewJob" =>  $HighlightNewJob, 
	                       ":UHighlightHotJob" =>  $HighlightHotJob,
	                       ":UNewJobIcon"      =>  $NewJobIcon,
	                       ":UHotJobIcon"      =>  $HotJobIcon,
	                       ":UNewJobDays"      =>  $NewJobDays
	                   );
	    
	    $ins_highlight_req  = "INSERT INTO HighlightRequisitions (`OrgID`, `HighlightHotJob`, `HighlightNewJob`, `NewJobDays`, `NewJobIcon`, `HotJobIcon`, `LastUpdatedDate`)";
	    $ins_highlight_req .= " VALUES(:IOrgID, :IHighlightHotJob, :IHighlightNewJob, :INewJobDays, :INewJobIcon, :IHotJobIcon, NOW())";
	    $ins_highlight_req .= " ON DUPLICATE KEY UPDATE `HighlightNewJob` = :UHighlightNewJob, `HighlightHotJob` = :UHighlightHotJob, `NewJobDays` = :UNewJobDays,";
	    $ins_highlight_req .= " NewJobIcon = :UNewJobIcon, HotJobIcon  = :UHotJobIcon, `LastUpdatedDate` = NOW()";
	    
	    if($NewJobIcon != "") {
	        $ins_highlight_req .= ", NewJobIcon = :NewJobIcon";
	        $params[":NewJobIcon"] = $NewJobIcon;
	    }
	    if($HotJobIcon != "") {
	        $ins_highlight_req .= ", HotJobIcon = :HotJobIcon";
	        $params[":HotJobIcon"] = $HotJobIcon;
	    }
	    
	    $res_highlight_req  = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_highlight_req, array($params) );
	    
	    return $res_highlight_req;
	    
	}
	
	/**
	 * @method		getHighlightRequisitionSettings
	 * @param		$OrgID
	 * @return		associative array
	 */
	function getHighlightRequisitionSettings($OrgID) {
	
	    // Set parameters for prepared query
	    $params = array (":OrgID"  =>  $OrgID);
	    $sel_info = "SELECT * FROM HighlightRequisitions WHERE OrgID = :OrgID";
	    $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_info, array ($params) );
	
	    return $res_info;
	}
	
	/**
	 * @method     updHighlightHotRequisition
	 * @param      $OrgID, $RequestID, $Highlight
	 */
	public function updHighlightHotRequisition($OrgID, $RequestID, $Highlight) {
	     
	    $params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":Highlight"=>$Highlight);
	    $upd_highlight_req  = "UPDATE Requisitions SET `Highlight` = :Highlight WHERE `OrgID` = :OrgID AND `RequestID` = :RequestID";
	    $res_highlight_req  = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_highlight_req, array($params) );
	     
	    return $res_highlight_req;
	     
	}
	
	/**
	 * @method     updHighlightNewRequisitionStatus
	 * @param      $OrgID, $Days
	 */
	public function updHighlightNewRequisitionStatus($OrgID, $Days) {
	
	    $res_highlight_req = array();
	    if($Days != "" && $Days > 0) {
	        $params = array(":OrgID"=>$OrgID, ":Highlight"=>"New", ":Days"=>$Days);
	        $upd_highlight_req  = "UPDATE Requisitions SET `Highlight` = :Highlight WHERE `OrgID` = :OrgID AND Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= :Days";
	        $res_highlight_req  = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_highlight_req, array($params) );
	    }
	
	    return $res_highlight_req;
	}
	
	/**
	 * @method     updClearNewRequisitionStatus
	 * @param      $OrgID, $Days
	 */
	public function updClearNewRequisitionStatus($OrgID, $Days) {
	
	    $res_highlight_req = array();
	    
	    if($Days != "" && $Days > 0) {
	        $params = array(":OrgID"=>$OrgID, ":Highlight"=>"", ":Days1"=>$Days, ":Days2"=>$Days);
	        $upd_highlight_req  = "UPDATE Requisitions SET `Highlight` = :Highlight WHERE `OrgID` = :OrgID AND Highlight = 'New' AND (DATEDIFF(NOW(), DATE(PostDate)) > :Days1 OR DATEDIFF(DATE(PostDate), NOW()) > :Days2)";
	        $res_highlight_req  = $this->db->getConnection ( "IRECRUIT" )->update ( $upd_highlight_req, array($params) );
	    }
	
	    return $res_highlight_req;
	}
	
	/**
	 * @method     getJobImage
	 */
	public function getJobImage($IconID) {
	    
	    // Set parameters for prepared query
	    $params = array (":IconID"=>$IconID);
	    $sel_info = "SELECT JobIcon FROM HighlightRequisitionIcons WHERE IconID = :IconID";
	    $res_info = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_info, array ($params) );
	    
	    return $res_info['JobIcon'];
	}
	
}
?>
