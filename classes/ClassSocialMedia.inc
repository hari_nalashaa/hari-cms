<?php 
/**
 * @class		SocialMedia
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class SocialMedia {

	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method	getSocialMediaDetailInfo
	 * @param	string $OrgID, $MultiOrgID
	 * @return	numeric array
	 */
	function getSocialMediaDetailInfo($OrgID, $MultiOrgID) {
		
		$columns			=	$this->db->arrayToDatabaseQueryString ( $columns );
		$params_info		=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		$sel_social_media 	=	"SELECT $columns FROM SocialMedia WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
		$res_social_media 	=	$this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_social_media, array($params_info) );
	
		return $res_social_media;
	}

	/**
	 * @method	getSocialMediaInfo
	 * @param	string $OrgID, $MultiOrgID
	 * @return	numeric array
	 */
	function getSocialMediaInfo($columns, $where_info = array(), $order_by = '', $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
		$sel_social_media_info = "SELECT $columns FROM SocialMedia";
		if(count($where_info) > 0) {
			$sel_social_media_info .= " WHERE " . implode(" AND ", $where_info);
		}
		
		if($order_by != "") $sel_social_media_info .= " ORDER BY " . $order_by;
		$res_social_media_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_social_media_info, $info );
	
		return $res_social_media_info;
	}
	
	/**
	 * @method		insSocialMediaInfo
	 * @param		array $info
	 * @return		array
	 */
	function insSocialMediaInfo($info, $on_update = '', $update_info = array()) {
		
		$ins_social_media_info = $this->db->buildInsertStatement('SocialMedia', $info);
	
		$insert_statement = $ins_social_media_info ["stmt"];
		if ($on_update != '') {
			$insert_statement .= $on_update;
			if (is_array ( $update_info )) {
				foreach ( $update_info as $upd_key => $upd_value ) {
					$ins_social_media_info ["info"][0][$upd_key] = $upd_value;
				}
			}
		}
	
		$res_social_media_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $insert_statement, $ins_social_media_info["info"] );
	
		return $res_social_media_info;
	}
	
	/**
	 * @method	delUserPortalInfo
	 * @param	$where_info, $info
	 */
	function delSocialMediaInfo($where_info = array(), $info = array()) {
		
		$del_social_media_info = "DELETE FROM SocialMedia";
		if (count ( $where_info ) > 0) {
			$del_social_media_info .= " WHERE " . implode ( " AND ", $where_info );
		}
		
		$res_social_media_info = $this->db->getConnection ( "IRECRUIT" )->delete ( $del_social_media_info, $info );
	
		return $res_social_media_info;
	}
	
	
	/**
	 * @method	updSocialMediaInfo
	 * @param	$set_info, $where_info, $info
	 */
	function updSocialMediaInfo($set_info = array(), $where_info = array(), $info) {
		
		$upd_social_media_info = $this->db->buildUpdateStatement('SocialMedia', $set_info, $where_info);
		$res_social_media_info = $this->db->getConnection("IRECRUIT")->update($upd_social_media_info, $info);
	
		return $res_social_media_info;
	}
}
?>
