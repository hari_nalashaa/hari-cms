<?php
/**
 * @class		HiddenForm
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class HiddenForm {
	
	var $conn_string       =   "IRECRUIT";
	var $APPDATA           =   array();
	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	
	/**
	 * @method     getQuestionView1
	 * @param      array $qi
	 */
	function getQuestionView1($qi) {
	    
	    $name          =   trim ( $qi['name'] );
	    $lbl_class     =   $qi ['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    $namef = $name . "from";
	    $namet = $name . "to";
	    
	    $value_from = $this->APPDATA [$namef];
	    $value_to = $this->APPDATA [$namet];
	    
	    $rtn  = '<input type="hidden" id="' . $namef . '" name="' . $namef . '" value="' . $value_from . '"/>';
	    $rtn .= '<input type="hidden" id="' . $namet . '" name="' . $namet . '" value="' . $value_to . '"/>';
	    
	    return $rtn;
	}

	
	/**
	 * @method     getQuestionView2
	 * @param      array $qi
	 */
	function getQuestionView2($qi) {
	
        $name           =   trim ( $qi['name'] );
        $defaultValue   =   $qi ['defaultValue'];
        $values         =   explode ( "::", $qi ['value']);
        $lbl_class      =   $qi ['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
        
		$i = 0;
    	foreach ( $values as $v => $q ) {
    		
    		$i ++;
    		list ( $vv, $qq ) = explode ( ":", $q, 2 );
    		$vv = trim ( $vv );
    		
    		$this->APPDATA [$name] = trim ( $this->APPDATA [$name] );
    		
    		if ($this->APPDATA [$name]) {
    			$defaultValue = "";
    		}
    		
    		$label_name = str_replace(" ", "", $qq);
    		$rtn .= '<input type="hidden" name="' . $name . '" id="'.$label_name.'" value="' . $vv . '">';
    		
    		if (($qi['name'] == "veteran") && ($i >= 3)) {
    			$i = 0;
    		}
    	}
    	
	    return $rtn;
	}

	
	/**
	 * @method     getQuestionView3
	 * @param      $qi
	 */
	function getQuestionView3($qi) {
        
        $name           =   trim ( $qi['name'] );
        $defaultValue   =   $qi ['defaultValue'];
        $values         =   explode ( "::", $qi ['value']);
        $maxlength      =   $qi ['maxlength'];
        $size           =   $qi ['size'];
        $lbl_class      =   $qi ['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
        
        if($_REQUEST [$name] != "") {
        	$selected_value    =   $_REQUEST [$name];
        }
        else if($defaultValue != "") {
            $selected_value    =   $_REQUEST [$name];
        }
        
    	$rtn = "<input type='hidden' name='$name' id='$name' ";
    	
    	foreach ( $values as $v => $q ) {
    		
    		list ( $vv, $qq ) = explode ( ":", $q, 2 );
    		
    		$vv = trim ( $vv );
    		if ($this->APPDATA [$name]) {
    			$defaultValue = "xx";
    		}
    		
    		$this->APPDATA [$name] = trim ( $this->APPDATA [$name] );
    		
    		$sel = '';
    		if (isset ( $_REQUEST [$name] ) && $_REQUEST [$name] == $vv) {
    			$rtn .= " value='".$_REQUEST [$name]."'";
    		} else if (! isset ( $_REQUEST [$name] ) && (($vv == $defaultValue))) {
    			$rtn .= " value='".$defaultValue."'";
    		}
    		else if($vv == $this->APPDATA [$name]) {
    		    $rtn .= " value='".$this->APPDATA [$name]."'";
    		}
    		
    	}
    	
    	$rtn .= ">";
    	
    	return $rtn;	    
	}

	
	/**
	 * @method     getQuestionView5
	 * @param      $qi
	 */
	function getQuestionView5($qi) {
	
	    $name          =   trim ( $qi['name'] );
	    $lbl_class     =   $qi ['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
    	$this->APPDATA [$name] = trim ( $this->APPDATA [$name] );
    
    	if($qi ['size'] <> 0) {
            $cnt = $qi ['size'];
    	} else {	
            $cnt = 250;
    	}
    	
    	$skillslist = ($skillnames != "") ? $skillnames : $this->APPDATA [$name];
    	
    	if(isset($_REQUEST[$name])) $skillslist = $_REQUEST[$name];
    	
        $rtn    =   '<input type="hidden" name="'.$name.'" id="'.$name.'" value="'.$skillslist.'">';
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView6
	 * @param      $qi
	 */
	function getQuestionView6($qi) {
	
        $name               =   trim ( $qi['name'] );
        $defaultValue       =   $qi ['defaultValue'];
        $size               =   $qi ['size'];
        $maxlength          =   $qi ['maxlength'];
        $lbl_class          =   $qi ['lbl_class'];
        $requiredck         =   $qi ['requiredck'];
        
        $this->APPDATA [$name]    =   trim ( $this->APPDATA [$name] );
    	
    	if (($this->APPDATA [$name] == "") && (($name == "Sage_PayPeriodHRS") || ($name == "Sage_Rate"))) {
    		$this->APPDATA [$name] = $defaultValue;
    	}
    	
    	if (isset ( $_REQUEST [$name] )) {
    	    $this->APPDATA [$name] = $_REQUEST [$name];
    	}

        $ques_size_limit    =   array('soc_linkedin', 'soc_facebook', 'soc_twitter', 'soc_google', 'soc_instagram');   
    	
        $rtn    =   '<input type="hidden" name="' . $name . '" id="' . $name .'" value="' . $this->APPDATA [$name] .'">';
    	    	
    	return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView7
	 * @param      $qi
	 */
	function getQuestionView7($qi) {
	   
	    global $FormFeaturesObj;
	    
        $name       =   trim ( $qi['name'] );
        $lbl_class  =   $qi ['lbl_class'];
        $requiredck =   $qi ['requiredck'];
        
        //Get Text From Text Blocks
        $value      =   $FormFeaturesObj->getTextFromTextBlocks($qi['OrgID'], $qi['FormID'], $qi['QuestionID']);
    	
        $rtn        =   '<input type="hidden" name="'.$name.'" id="'.$name.'" value="'.$qi['value'].'">';
        
    	return $rtn;
	}


	/**
	 * @method     getQuestionView8
	 * @param      $qi
	 */
	function getQuestionView8($qi) {
	    
	    $name          =   trim ( $qi['name'] );
	    $lbl_class     =   $qi ['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
    	$rtn           =   '<input type="hidden" name="' . $name . '" id="' . $name . '">';
    	 
	    return $rtn;
	}

	
	/**
	 * @method     getQuestionView9
	 * @param      $qi
	 */
	function getQuestionView9($qi) {

	    $name           =   trim ( $qi['name'] );
	    $defaultValue   =   $qi ['defaultValue'];
	    $values         =   explode ( "::", $qi ['value']);
	    
	    $Question       =   $qi ['Question'];
	    $QuestionTypeID =   $qi ['QuestionTypeID'];
	    $size           =   $qi ['size'];
	    $maxlength      =   $qi ['maxlength'];
	    $rows           =   $qi ['rows'];
	    $cols           =   $qi ['cols'];
	    $wrap           =   $qi ['wrap'];
	    $value          =   $qi ['value'];
	    
	    $required       =   $qi ['Required'];
	    $validate       =   $qi ['Validate'];
        $lbl_class      =   $qi ['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
        	    
        $rtn            =   '';
       	$ii             =   0;
    	
    	foreach ( $values as $v => $q ) {
    		
    		$ii ++;

    		$name1    =   $name . "-" . $ii;
    		$name1yr  =   $name . "-" . $ii . "-yr";
    		$name1cmt =   $name . "-" . $ii . "-comments";
    		
    		$this->APPDATA [$name1]       =   trim ( $this->APPDATA [$name1] );
    		$this->APPDATA [$name1yr]     =   trim ( $this->APPDATA [$name1yr] );
    		$this->APPDATA [$name1cmt]    =   trim ( $this->APPDATA [$name1cmt] );
    		
    		$this->APPDATA [$name1]       =   trim ( $this->APPDATA [$name1] );
    		
    		list ( $vv, $qq ) = explode ( ":", $q, 2 );
    		
    		if ($this->APPDATA [$name1]) {
    			$defaultValue = "";
    		}
    		if (($vv == $defaultValue) || ($this->APPDATA [$name1])) {
    			$ck = ' checked';
    		} else {
    			$ck = '';
    		}
    		
    		if($ck != "") {
    		    $rtn .= '<input type="hidden" name="' . $name1 . '" value="' . $vv . '">';
    		    $rtn .= '<input type="hidden" name="' . $name1yr . '" value="' . $this->APPDATA [$name1yr] . '">';
    		    $rtn .= '<input type="hidden" name="' . $name1cmt . '" value="' . $this->APPDATA [$name1cmt] . '">';
    		}
    	}

    	// this is new
    	$rtn .= '<input type="hidden" name="' . $name . '" value="' . $name . '">';
    	$rtn .= '<input type="hidden" name="' . $name . 'cnt" value="' . $ii . '">';
    	
    	return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView10
	 * @param      $qi
	 */
	function getQuestionView10($qi) {
	    global $MysqlHelperObj;
	    
	    $name          =   trim ( $qi['name'] );
	    $value         =   $qi['value'];
	    $size          =   $qi ['size'];
	    $lbl_class     =   $qi['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    if ($value == "{Date}") {
	        $value = $MysqlHelperObj->getNow();
	    } // end {Date}
	    
	    $rtn = '<input type="hidden" name="'.$name.'" size="'.$size.'" value="'.$value.'">';
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView11
	 * @param      $qi
	 */
	function getQuestionView11($qi) {
	     
	    $name          =   trim ( $qi['name'] );
	    $size          =   $qi ['size'];
	    $maxlength     =   $qi ['maxlength'];
	    $lbl_class     =   $qi['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    $rtn           =   '<input type="hidden" name="'.$name.'" size="'.$size.'">';
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView13
	 * @param      $qi
	 */
	function getQuestionView13($qi) {

	    global $MApplicantInfo, $GenericLibraryObj;
	    
	    $name          =   trim ( $qi['name'] );
	    $lbl_class     =   $qi['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    $i             =   1;
	    $ii            =   2;
	    $iii           =   3;
	    
	    $name1         =   $name . $i;
	    $name2         =   $name . $ii;
	    $name3         =   $name . $iii;
	    
	    $MPhone = str_replace (array ("-", " "), "", $MApplicantInfo ['PhoneNumber']);
	    
	    if ($MApplicantInfo ['PhoneNumber'] != "") {
	        
	        $MPhone = substr ( $MPhone, - 10 );
	    
	        if ($MPhone != "") {
	            	
	            $homephone1 = substr ( $MPhone, 0, 3 );
	            $homephone2 = substr ( $MPhone, 3, 3 );
	            $homephone3 = substr ( $MPhone, 6, 4 );
	            	
	            $hphone1 = $homephone1;
	            $hphone2 = $homephone2;
	            $hphone3 = $homephone3;
	        }
	        
	    } else {

	        if($GenericLibraryObj->isJSON($this->APPDATA [$name]) === true) {
                $Answer13   =   json_decode($this->APPDATA [$name], true);
                $hphone1    =   $Answer13[0];
                $hphone2    =   $Answer13[1];
                $hphone3    =   $Answer13[2];
	        }
	        else {
                $hphone1    =   $this->APPDATA [$name1];
                $hphone2    =   $this->APPDATA [$name2];
                $hphone3    =   $this->APPDATA [$name3];
	        }
	         
	    }
	    
	    $rtn  = '';
	    
	    if (($International ['Allow'] == "Y") && (($this->APPDATA ["country"] != "US") && ($this->APPDATA ["country"] != "CA"))) {
	    
	        $rtn .= "<input type='hidden' name='$name1' value='$hphone1'>";
	        $rtn .= "<input type='hidden' name='$name2' value='$hphone2'>";
	        $rtn .= "<input type='hidden' name='$name3' value='$hphone3'>";
	        
	    } else {
	    
	        $rtn .= "<input type='hidden' name='$name1' value='$hphone1'>";
	        $rtn .= "<input type='hidden' name='$name2' value='$hphone2'>";
	        $rtn .= "<input type='hidden' name='$name3' value='$hphone3'>";
	    }
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView14
	 * @param      $qi
	 */
	function getQuestionView14($qi) {

	    global $International, $GenericLibraryObj;
	    
	    $name          =   trim ( $qi['name'] );
	    $lbl_class     =   $qi['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    $i     =   1;
	    $ii    =   2;
	    $iii   =   3;
	    $iiii  =   "ext";
	    
	    $name1 =   $name . $i;
	    $name2 =   $name . $ii;
	    $name3 =   $name . $iii;
	    $name4 =   $name . $iiii;
	    
	    if($GenericLibraryObj->isJSON($this->APPDATA [$name]) === true) {
            $Answer14  =   json_decode($this->APPDATA [$name], true);
            $hphone1   =   $Answer14[0];
            $hphone2   =   $Answer14[1];
            $hphone3   =   $Answer14[2];
            $hphone4   =   $Answer14[3];
	    }
	    else {
            $hphone1 = $this->APPDATA [$name1];
            $hphone2 = $this->APPDATA [$name2];
            $hphone3 = $this->APPDATA [$name3];
            $hphone4 = $this->APPDATA [$name.'ext'];
	    }
	    
	    $rtn  = '';
	    if (($International ['Allow'] == "Y") && (($this->APPDATA ["country"] != "US") && ($this->APPDATA ["country"] != "CA"))) {
	    
	        $rtn .= "<input type='hidden' name='$name1' value='$hphone1'>";
	        $rtn .= "<input type='hidden' name='$name2' value='$hphone2'>";
	        $rtn .= "<input type='hidden' name='$name3' value='$hphone3'>";
	        $rtn .= "<input type='hidden' name='$name4' value='$hphone4'>";
	        
	    } else {
	    
	        $rtn .= "<input type='hidden' name='$name1' value='$hphone1'>";
	        $rtn .= "<input type='hidden' name='$name2' value='$hphone2'>";
	        $rtn .= "<input type='hidden' name='$name3' value='$hphone3'>";
	        $rtn .= "<input type='hidden' name='$name4' value='$hphone4'>";
	    }
	     
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView15
	 * @param      $qi
	 */
	function getQuestionView15($qi) {

	    $name          =   trim ( $qi['QuestionID'] );
	    $lbl_class     =   $qi['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    $i             =   1;
	    $ii            =   2;
	    $iii           =   3;
	    $name1         =   $name . $i;
	    $name2         =   $name . $ii;
	    $name3         =   $name . $iii;

	    // this is new
	    $rtn  = '';
	    $rtn .= '<input type="hidden" name="'.$name.'" value="'.$name.'">';
	    $rtn .= '<input type="hidden" name="'.$name1.'" value="'.$this->APPDATA[$name1].'">';
	    $rtn .= '<input type="hidden" name="'.$name2.'" value="'.$this->APPDATA[$name2].'">';
	    $rtn .= '<input type="hidden" name="'.$name3.'" value="'.$this->APPDATA[$name3].'">';
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView17
	 * @param      $qi
	 */
	function getQuestionView17($qi) {
	    global $MysqlHelperObj;
	    
        // Get current date and time from database
        $currentdate    =   $MysqlHelperObj->getDateTime ();
         
        $name           =   trim ( $qi['name'] );
        $defaultValue   =   $qi ['defaultValue'];
        $Date           =   $this->APPDATA [$name];
        $lbl_class      =   $qi['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
        
	    //if (($Date == "") && ($name == "Sage_HireDate") && ($defaultValue == "Y")) {
	        //$Date = $currentdate;
	    //} // end questionid
	    
	    $rtn  = '';
	    $rtn .= '<input type="hidden" id="' . $name . '" name="' . $name . '" value="' . $Date . '">';
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView18
	 * @param      $qi
	 */
	function getQuestionView18($qi) {

	    $name           =   trim ( $qi['name'] );
	    $defaultValue   =   $qi ['defaultValue'];
	    $values         =   explode ( "::", $qi ['value']);
	    $lbl_class      =   $qi['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
	    
	    if (count ( $values ) > 7) {
	        $collength = 3;
	    } else {
	        $collength = 7;
	    }
	    
	    if ($qi['OrgID'] == "I20090201") {
	        $collength = 2;
	    }
	    
	    if ($SID == 10) {
	        $collength = 1;
	    }
	    
	    $ii    =   0;
	    $ri    =   0;
	    
	    $rtn   =   '';
	    foreach ( $values as $v => $q ) {
	        $ii ++;
	        $ri ++;
	    
	        $name1 = $name . "-" . $ii;
	    
	        list ( $vv, $qq ) = explode ( ":", $q, 2 );
	        $vv = trim ( $vv );
	        if ($this->APPDATA [$name1]) {
	            $defaultValue = "";
	        }
	        
	        $this->APPDATA [$name1] = trim ( $this->APPDATA [$name1] );
	        if (($vv == $defaultValue) || ($this->APPDATA [$name1])) {
	            $ck = ' checked';
	        } else {
	            $ck = '';
	        }
	    
	        if($ck != "") {
	            $rtn .= '<input type="hidden" name="' . $name1 . '" value="' . $vv . '">';
	        }
	    
	        if ($ri == $collength) {
	            $ri = 0;
	        }
	    }
	    
	    if ($ri == 0) {
	        $ri = $collength;
	    }
	    
	    while ( $ri < $collength ) {
	        $ri ++;
	    } // end while
	     
	    // this is new
	    $rtn .= '<input type="hidden" name="' . $name . '" value="' . $name . '">';
	    $rtn .= '<input type="hidden" name="' . $name . 'cnt" value="' . $ii . '">';
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView22
	 * @param      $qi
	 */
	function getQuestionView22($qi) {

	    $name           =   trim ( $qi['name'] );
	    $defaultValue   =   $qi ['defaultValue'];
	    $values         =   explode ( "::", $qi ['value']);
	    $lbl_class      =   $qi['lbl_class'];
	    $requiredck     =   $qi ['requiredck'];
	    
	    $i = 0;
	    
        $rtn            =   '';
	    foreach ( $values as $v => $q ) {
	    
	        $i ++;
	        list ( $vv, $qq ) = explode ( ":", $q, 2 );
	        $vv = trim ( $vv );
	    
	        $this->APPDATA [$name] = trim ( $this->APPDATA [$name] );
	    
	        if ($this->APPDATA [$name]) {
	            $defaultValue = "";
	        }
	    
	        if (($vv == $defaultValue) || ($vv == $this->APPDATA [$name])) {
	            $chk = ' checked="checked"';
	        } else {
	            $chk = '';
	        }
	    
	        $label_name = str_replace(" ", "", $qq);
	         
	        if($chk != "") {
	            $rtn .= '<input type="hidden" name="' . $name . '" id="'.$label_name.'" value="' . $vv . '">';
	        }
	    }
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView23
	 * @param      $qi
	 */
	function getQuestionView23($qi) {
	    
        $name           =   trim ( $qi['name'] );
        $defaultValue   =   $qi ['defaultValue'];
        $values         =   explode ( "::", $qi ['value']);
        $lbl_class      =   $qi['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
        
        $rtn            =   '';
        $i              =   0;
        
	    foreach ( $values as $v => $q ) {
	    
	        $i ++;
	        list ( $vv, $qq ) = explode ( ":", $q, 2 );
	        $vv = trim ( $vv );
	    
	        $this->APPDATA [$name] = trim ( $this->APPDATA [$name] );
	    
	        if ($this->APPDATA [$name]) {
	            $defaultValue = "";
	        }
	    
	        if (($vv == $defaultValue) || ($vv == $this->APPDATA [$name])) {
	            $chk = ' checked="checked"';
	        } else {
	            $chk = '';
	        }
	    
	        $label_name = str_replace(" ", "", $qq);
	        if($chk != "") {
	            $rtn .= '<input type="hidden" name="' . $name . '" id="'.$label_name.'" value="' . $vv . '">';
	        }
	    }
	
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView24
	 * @param      $qi
	 */
	function getQuestionView24($qi) {

	    $name          =   trim ( $qi['name'] );
	    $maxlength     =   $qi ['maxlength'];
	    $lbl_class     =   $qi['lbl_class'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    $multifrom     =   $name . 'from';
	    $Date          =   $this->APPDATA [$multifrom];
	    
	    $namef         =   $name . "from";
	    $rtn           =   '<input type="hidden" name="' . $namef . '" id="' . $namef . '" value="' . $Date . '" placeholder="MM/DD/YYYY" />';
	    $Date          =   "";
	    
	    $multito       =   $name . 'to';

	    if($this->APPDATA [$multito] != "") {
	        $Date      =   $this->APPDATA [$multito];
	    }
	    else {
	        $Date      =   date('m/d/Y');
	    }
	    
	    $namet = $name . "to";
	    $rtn .= ' <input type="hidden" name="' . $namet . '" id="' . $namet . '" value="' . $Date . '" />';
	    $Date = "";
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView30
	 * @param      $qi
	 */
	function getQuestionView30($qi) {

	    require_once COMMON_DIR . 'formsInternal/Signature.inc';

	    $rtn   =   Signature ( $qi['QuestionID'] );
	    	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView60
	 * @param      $qi
	 * @tutorial   Specific to Agreement 
	 */
	function getQuestionView60($qi) {
	    $name  =   trim ( $qi['name'] );
	    global $AgreementFormID, $ApplicationID, $RequestID;
	    
	    require_once COMMON_DIR . 'formsInternal/MergeContent.inc';
	    $value =   MergeContent ( $AgreementFormID, $qi['QuestionID'], $ApplicationID, $RequestID );
	    $value =   htmlspecialchars ( $value, ENT_QUOTES );
	    
	    $rtn   =    '<input type="hidden" name="' . $name . '" value="' . $value . '">';
	    
	    return $rtn;
	}

	/**
	 * @method     getQuestionView90
	 * @param      $qi
	 */
	function getQuestionView90($qi) {
	    $name          =   trim ( $qi['name'] );
	    $value         =   $qi ['value'];
	    $requiredck    =   $qi ['requiredck'];
	    
	    list ( $linkname, $ext ) = explode ( "::", $value );
	    
	    if ($value != "") {
	        $rtn .= '<a href="' . PUBLIC_HOME . 'display_attachment.php?OrgID=' . $qi['OrgID'] . '&FormID=' . $qi['FormID'] . '&QuestionID=' . $name . '" target="_blank">' . $linkname . '</a>';
	    }
	    
	    return $rtn;
	}
	

	/**
	 * @method     getQuestionView99
	 * @param      $qi
	 */
	function getQuestionView99($qi) {
        $name   =   trim ( $qi['name'] );

        $rtn    =   '';
        if(trim($qi['Question']) != "") {
            // this is new
            $rtn .=    '<input type="hidden" name="instructions" value="instructions">';
        }
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView100
	 * @param      $qi
	 */
	function getQuestionView100($qi) {
           
	    global $FormQuestionsObj, $RequisitionsObj, $FormsInternalObj;
	    
        $name           =   trim ( $qi['name'] );
        $defaultValue   =   $qi ['defaultValue'];
        $values         =   explode ( "::", $qi ['value']);
        $lbl_class      =   $qi['lbl_class'];
        $requiredck     =   $qi ['requiredck'];
        
	    include COMMON_DIR . 'application/CustomQuestionsAndData.inc';

        $params         =   array(":QuestionID"=>$qi['QuestionID'], ":OrgID"=>$qi['OrgID']);
        $gskills        =   $FormQuestionsObj->getQuestionInformation ( array($params), $qi['FormTable'] );
	    
	    if($FormTable == "RequisitionQuestions") {
	        $shifts_web_form_data = G::Obj('RequisitionsData')->getRequisitionsDataByQuestionID($qi['OrgID'], $qi['RequestID'], $qi['QuestionID']);
	        $shifts_data = unserialize ( $shifts_web_form_data ['Answer'] );
	    }
	    else {
	        if (isset ( $_REQUEST ['RequestID'] )) $RequestID = $_REQUEST ['RequestID'];
	        $web_form_data_skills = $FormsInternalObj->getFormData ( $qi['OrgID'], $qi['ApplicationID'], $qi['RequestID'], $CFormID, $qi['QuestionID'], $FormDataTable );
	        $data_skills = unserialize ( $web_form_data_skills ['Answer'] );
	    }
	    
	    
	    $group_of_skills = unserialize ( $gskills ['value'] );
	    
	    $num_rows = $gskills ['rows'];
	    $num_cols = $gskills ['cols'];
	    
	    $QuestionPrefix = "";
	    if ($EductionSectionFlag == "1")
	        $QuestionPrefix = $SectionID;
	    else if ($WorkExperienceSectionFlag == "1")
	        $QuestionPrefix = $SectionID . "mrp";
	    else if ($CRTSectionFlag == "1")
	        $QuestionPrefix = $SectionID . "crt";
	    else if ($REFSectionFlag == "1")
	        $QuestionPrefix = $SectionID . "ref";
	    
	    $rtn  = '';
	    
	    if (isset ( $group_of_skills ) && is_array ( $group_of_skills )) {
	    
	        for($row_index = 0; $row_index <= $num_rows; $row_index ++) {
	            	
	            for($col_index = 0; $col_index <= $num_cols; $col_index ++) {
	                if ($row_index == 0) {
	                    if ($col_index == 0) {
	                        $rtn .= '';
	                    } else {
	                        $rtn .= '';
	                    }
	                } else {
	                    if ($col_index == 0) {
	                        $rtn .= '';
	                    } else {
	                        $question_name = "LabelSelect[" . $QuestionPrefix . $qi['QuestionID'] . "][" . $label_name . "][LabelSelect$row_index]";
	                        $question_value = $group_of_skills ["RVal"] ["RVal" . $col_index];
	                        $request_question_info = @unserialize ( $this->APPDATA [$QuestionPrefix . $qi['QuestionID']] );
	    
	                        $rtn .= '<input type="hidden" name="' . $question_name . '" ';
	    
	                        if (($data_skills [$label_name] ['LabelSelect' . $row_index] == $question_value) || (@$request_question_info [$label_name] ['LabelSelect' . $row_index] == $question_value))
	                            $rtn .= ' value="' . $question_value . '"';
	    
	                        $rtn .= '>';
	                    }
	                }
	            }
	        }

	    }
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView120
	 * @param      $qi
	 */
	function getQuestionView120($qi) {
	    global $FormQuestionsObj, $RequisitionsObj, $FormsInternalObj, $UserPortalUsersObj, $UpUserID;
	    
	    $name           =   trim ( $qi['name'] );
	    $defaultValue   =   $qi ['defaultValue'];
	    $values         =   explode ( "::", $qi ['value']);
	    $lbl_class      =   $qi['lbl_class'];
	    $requiredck     =   $qi ['requiredck'];
	    
	    include COMMON_DIR . 'application/CustomQuestionsAndData.inc';

	    $params = array(":QuestionID"=>$qi['QuestionID'], ":OrgID"=>$qi['OrgID']);
	    $ques_info = $FormQuestionsObj->getQuestionInformation ( array($params), $qi['FormTable'] );
	     
	    $from_to_time ["12:00am"] = "12:00am";
	    for($i = 1; $i <= 12; $i ++) {
	        $ap = "am";
	        if ($i == 12)
	            $ap = "pm";
	        $from_to_time ["$i:00$ap"] = "$i:00$ap";
	    }
	    for($j = 1; $j <= 11; $j ++) {
	        $from_to_time ["$j:00pm"] = "$j:00pm";
	    }
	    
	    $days = array (
	        "Monday",
	        "Tuesday",
	        "Wednesday",
	        "Thursday",
	        "Friday",
	        "Saturday",
	        "Sunday"
	    );
	    
	    if($FormTable == "RequisitionQuestions") {
	        $shifts_web_form_data	=	G::Obj('RequisitionsData')->getRequisitionsDataByQuestionID($qi['OrgID'], $qi['RequestID'], $qi['QuestionID']);
	        $shifts_data = unserialize ( $shifts_web_form_data ['Answer'] );
	    }
	    else {
	        if (isset ( $_REQUEST ['RequestID'] )) $RequestID = $_REQUEST ['RequestID'];
	        $shifts_web_form_data = $FormsInternalObj->getFormData ( $qi['OrgID'], $qi['RequestID'], $qi['RequestID'], $CFormID, $qi['QuestionID'], $FormDataTable );
	        $shifts_data = unserialize ( $shifts_web_form_data ['Answer'] );
	    }
	    
	    
	    if(isset($navpg) && isset($navsubpg)) {
	        //Set where condition
	        $where = array("UserID = :UserID", "QuestionID = :QuestionID");
	        //Set parameters
	        $params = array(":UserID"=>$UpUserID, ":QuestionID"=>$qi['QuestionID']);
	        //Get ProfileData Information
	        $results = $UserPortalUsersObj->getProfileDataInfo("QuestionID, Answer", $where, "", array($params));
	    
	        if(is_array($results['results'])) $shifts_data = unserialize ( $results['results'][0]['Answer'] );
	    }
	    
	    $shifts = unserialize ( $ques_info ['value'] );
	    
	    $QuestionPrefix = "";
	    if ($EductionSectionFlag == "1")
	        $QuestionPrefix = $SectionID;
	    else if ($WorkExperienceSectionFlag == "1")
	        $QuestionPrefix = $SectionID . "mrp";
	    else if ($CRTSectionFlag == "1")
	        $QuestionPrefix = $SectionID . "crt";
	    else if ($REFSectionFlag == "1")
	        $QuestionPrefix = $SectionID . "ref";
	    
	    $available_shifts_data  = '';
	    for($data_index = 0; $data_index < count ( $shifts ['day_names'] ); $data_index ++) {
	    
	        $day_name              =   $shifts ['day_names'] [$data_index];
	        $from_question_name    =   "shifts_schedule_time[" . $QuestionPrefix . $qi['QuestionID'] . "][from_time][]";
	        $to_question_name      =   "shifts_schedule_time[" . $QuestionPrefix . $qi['QuestionID'] . "][to_time][]";
	        $days_list_name        =   "shifts_schedule_time[" . $QuestionPrefix . $qi['QuestionID'] . "][days][]";
	        $request_question_info =   @unserialize ( $this->APPDATA [$QuestionPrefix . $qi['QuestionID']] );
	    
	        $available_shifts_data .= '<input type="hidden" name="' . $days_list_name . '" value="' . $shifts ['day_names'] [$data_index] . '">';
	        $available_shifts_data .= '<input type="hidden" name="' . $from_question_name . '"';
	    
	        foreach ( $from_to_time as $ft_time ) {
	            if ($shifts_data [from_time] [$data_index] == $ft_time || $request_question_info [from_time] [$data_index] == $ft_time)
	            {
	                $selected_value    =   $ft_time;
	            }
	        }
	        
	        $available_shifts_data .= ' value="' . $selected_value . '">';
	         
	        $available_shifts_data .= '<input type="hidden" name="' . $to_question_name . '"';
	    
	        foreach ( $from_to_time as $to_time ) {
	            if ($shifts_data [to_time] [$data_index] == $to_time || $request_question_info [to_time] [$data_index] == $to_time)
	            {
	                $selected_value    =   $to_time;
	            }
	        }
	    
	        $available_shifts_data .= ' value="' . $selected_value . '">';
	        
	    }

	    
	    $rtn = $available_shifts_data;
	    
	    return $rtn;
	}
	
	
	/**
	 * @method     getQuestionView1818
	 * @param      $qi
	 */
	function getQuestionView1818($qi) {

	    $name           =   trim ( $qi['name'] );
	    $defaultValue   =   $qi ['defaultValue'];
	    $values         =   explode ( "::", $qi ['value']);
	    $lbl_class      =   $qi['lbl_class'];
	    $requiredck     =   $qi ['requiredck'];
	    
	    $collength = 1;
	    
	    if ($qi ['OrgID'] == "I20090201") {
	        $collength = 2;
	    }
	    
	    if ($SID == 10) {
	        $collength = 1;
	    }
	    
	    $ii = 0;
	    $ri = 0;

	    $rtn = '';
	    foreach ( $values as $v => $q ) {
	        $ii ++;
	        $ri ++;
	    
	        $name1 = $name . "-" . $ii;
	    
	        list ( $vv, $qq ) = explode ( ":", $q, 2 );
	        $vv = trim ( $vv );
	        if ($this->APPDATA [$name1]) {
	            $defaultValue = "";
	        }
	        
	        $this->APPDATA [$name1] = trim ( $this->APPDATA [$name1] );
	        if (($vv == $defaultValue) || ($this->APPDATA [$name1])) {
	            $ck = ' checked';
	        } else {
	            $ck = '';
	        }
	    
	        if($ck != "") {
	            $rtn .= '<input type="hidden" name="' . $name1 . '" value="' . $vv . '">';
	        }
	         
	        if ($ri == $collength) {
	            $ri = 0;
	        }
	    }
	    
	    if ($ri == 0) {
	        $ri = $collength;
	    }
	    
	    while ( $ri < $collength ) {
	        $ri ++;
	    } // end while
	     
	    // this is new
	    $rtn .= '<input type="hidden" name="' . $name . '" value="' . $name . '">';
	    
	    $rtn .= '<input type="hidden" name="' . $name . 'cnt" value="' . $ii . '">';
	    
	    return $rtn;	  
	}
	
}
