<?php
/**
 * @class       FormDetails
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormDetails {
	
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method	getPreFilledFormDetails
     */
    function getPreFilledDetails($columns, $OrgID, $PreFilledFormID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID);
        $sel_info = "SELECT $columns FROM PreFilledForms WHERE OrgID = :OrgID AND PreFilledFormID = :PreFilledFormID";
        $res_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    
        return $res_info;
    }
    
    /**
     * @method	getAgreementFormDetails
     */
    function getAgreementFormDetails($columns, $OrgID, $AgreementFormID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID);
        $sel_info = "SELECT $columns FROM AgreementForms WHERE OrgID = :OrgID AND AgreementFormID = :AgreementFormID";
        $res_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    
        return $res_info;
    }
    
    /**
     * @method	getWebFormDetails
     */
    function getWebFormDetails($columns, $OrgID, $WebFormID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params = array(":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID);
        $sel_info = "SELECT $columns FROM WebForms WHERE OrgID = :OrgID AND WebFormID = :WebFormID";
        $res_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    
        return $res_info;
    }
    
    /**
     * @method	getSpecificationFormDetails
     */
    function getSpecificationFormDetails($columns, $OrgID, $SpecificationFormID) {
    
        $columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
        $params = array(":OrgID"=>$OrgID, ":SpecificationFormID"=>$SpecificationFormID);
        $sel_info = "SELECT $columns FROM SpecificationForms WHERE OrgID = :OrgID AND SpecificationFormID = :SpecificationFormID";
        $res_info = $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_info, array($params) );
    
        return $res_info;
    }
}
?>
