<?php
/**
 * @class		ApplicationFormSections
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicationFormSections {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method		insApplicationFormSections
	 * @param		$fq_info
	 */
	public function insApplicationFormSections($info) {
	
		$ins_form_sec = $this->db->buildInsertStatement("ApplicationFormSections", $info);
		$res_form_sec = $this->db->getConnection ( $this->conn_string )->insert ( $ins_form_sec["stmt"], $ins_form_sec["info"] );
	
		return $res_form_sec;
	}

	/**
	 * @method		updApplicationFormSections
	 * @param		$set_info, $where_info, $info
	 */
	function updApplicationFormSections($set_info = array(), $where_info = array(), $info) {
	
		$upd_info =	$this->db->buildUpdateStatement('ApplicationFormSections', $set_info, $where_info);
		$res_info = $this->db->getConnection ( $this->conn_string )->update($upd_info, $info);
	
		return $res_info;
	}
	
	/**
	 * @method		insUpdApplicationFormSections
	 */
	function insUpdApplicationFormSections($info) {
	    
	    $params            =   array(
                        	        "OrgID"            =>  $info['OrgID'],
                        	        "FormID"           =>  $info['FormID'],
                        	        "SectionID"        =>  $info['SectionID'],
                        	        "SectionTitle"     =>  $info['SectionTitle'],
                        	        "Active"           =>  $info['Active'],
                        	        "ViewPrintStatus"  =>  $info['ViewPrintStatus'],
                        	        "IsDeleted"        =>  $info['IsDeleted'],
                        	        "SortOrder"        =>  $info['SortOrder'],
                        	    );
	    $skip               =   array("OrgID", "FormID", "SectionID");
	    $ins_app_form_sec	=   $this->db->buildOnDuplicateKeyUpdateStatement("ApplicationFormSections", $params, $skip);
	    $res_app_form_sec	=   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_form_sec["stmt"], $ins_app_form_sec["info"] );
	    
	    return $res_app_form_sec;
	}
	
	/**
	 * @method		getApplicationFormSectionsInfo
	 * @param		$columns, $where_info, $order_by
	 * @return		associative array
	 * @tutorial	This method will fetch the form questions informtaion
	 */
	function getApplicationFormSectionsInfo($columns = "", $where_info = array(), $group_by = "", $order_by = "", $info = array()) {
	
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_frm_que_info = "SELECT $columns FROM ApplicationFormSections";
		
		if(count($where_info) > 0) {
			$sel_frm_que_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($group_by != "") $sel_frm_que_info .= " GROUP BY " . $group_by;
		if($order_by != "") $sel_frm_que_info .= " ORDER BY " . $order_by;
	
		$res_frm_que_info = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, $info );
	
		return $res_frm_que_info;
	}
	
	/**
	 * @method     getApplicationFormSectionsByFormID
	 */
	function getApplicationFormSectionsByFormID($columns = "", $OrgID, $FormID, $Active = "", $QuesCountCheck = "Yes") {
	    
	    $columns           =   $this->db->arrayToDatabaseQueryString ( $columns );

	    $params_info       =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":IsDeleted"=>"No");
	    $sel_frm_que_info  =   "SELECT $columns FROM ApplicationFormSections WHERE OrgID = :OrgID AND FormID = :FormID AND IsDeleted = :IsDeleted";
	    
	    if($Active != "") {
	        $sel_frm_que_info  .=  " AND Active = :Active";
	        $params_info[":Active"]    =   $Active;
	    }
	    	    
	    $sel_frm_que_info  .=  " ORDER BY SortOrder ASC";
	    $res_frm_que_info  =   $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_frm_que_info, array($params_info) );
	    $forms_results     =   $res_frm_que_info['results'];	    
	    
	    $forms_list        =   array();
	    for($f = 0; $f < count($forms_results); $f++) {	        
	        $QuestionsCount    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsCountBySectionID($OrgID, $FormID, $forms_results[$f]['SectionID'], array('99'));
	        if($QuesCountCheck == "Yes") {
	            if($QuestionsCount > 0) {
	                $forms_list[$forms_results[$f]['SectionID']]    =   $forms_results[$f];
	            }
	        }
	        else {
	            $forms_list[$forms_results[$f]['SectionID']]    =   $forms_results[$f];
	        }
	    }
	    
	    return $forms_list;
	}
	
	/**
	 * @method     getApplicationFormSectionInfo
	 */
	function getApplicationFormSectionInfo($columns = "", $OrgID, $FormID, $SectionID) {
	    
	    $columns           =   $this->db->arrayToDatabaseQueryString ( $columns );
	    
	    $params_info       =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$SectionID);
	    $sel_frm_que_info  =   "SELECT $columns FROM ApplicationFormSections WHERE OrgID = :OrgID AND FormID = :FormID AND SectionID = :SectionID";
	    $res_frm_que_info  =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_frm_que_info, array($params_info) );
	    
	    return $res_frm_que_info;
	}
	
	/**
	 * @method     getMaxSortOrderByFormID
	 */
	function getMaxSortOrderByFormID($OrgID, $FormID) {
	    
	    $params_info       =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
	    $sel_frm_que_info  =   "SELECT MAX(SortOrder) AS NewSortOrder FROM ApplicationFormSections WHERE OrgID = :OrgID AND FormID = :FormID";
	    $res_frm_que_info  =   $this->db->getConnection( $this->conn_string )->fetchAssoc ( $sel_frm_que_info, array($params_info) );
	    
	    return $res_frm_que_info['NewSortOrder'];
	}

	/**
	 * @method     getAllSectionIDs
	 */
	function getAllSectionIDs($OrgID, $FormID) {
	    
	    //Application Form Sections
	    $where_info            =   array("OrgID = :OrgID", "FormID = :FormID");
	    $params_info           =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
	    $app_form_sections     =   $this->getApplicationFormSectionsInfo("*", $where_info, "", "", array($params_info));
	    
	    $form_sec_results      =   $app_form_sections['results'];
	    $form_sec_count        =   $app_form_sections['count'];
	    
	    $section_ids           =   array();
	    for($f = 0; $f < $form_sec_count; $f++) {
	        $section_ids[]     =   $form_sec_results[$f]['SectionID'];
	    }
	    
	    return $section_ids;
	}
	
	/**
	 * @method     getNewSectionID
	 */
	function getNewSectionID($OrgID, $FormID, $SectionTitle) {
	    
        $section_ids    =   $this->getAllSectionIDs($OrgID, $FormID);
        $SectionTitle   =   strtoupper(preg_replace(array('/[^a-z]/i') , '', $SectionTitle));
        
        $start          =   0;
        $length         =   2;
        
        while(FALSE !== ($NewSectionID = substr($SectionTitle, $start, $length))) {
            $start      =   $start+2;
            
            if(!in_array($NewSectionID, $section_ids)) {
                break;
            }
        }
        
        if($NewSectionID == "") {
            $start          =   0;
            $length         =   3;
            
            while(FALSE !== ($NewSectionID = substr($SectionTitle, $start, $length))) {
                $start      =   $start+3;
                
                if(!in_array($NewSectionID, $section_ids)) {
                    break;
                }
            }
        }
        
        if($NewSectionID == "") {
            $start          =   0;
            $length         =   4;
            
            while(FALSE !== ($NewSectionID = substr($SectionTitle, $start, $length))) {
                $start      =   $start+4;
                
                if(!in_array($NewSectionID, $section_ids)) {
                    break;
                }
            }
        }
        
        if($NewSectionID == "") {
            $NewSectionID = uniqid();
        }
        
        return $NewSectionID;
	}
	
	
	/**
	 * @method		copyApplicationFormMasterSections
	 */
	public function copyApplicationFormMasterSections($OrgIDToInsert, $OrgIDToCopy, $FormID, $NewFormID) {
	    
        //Insert Application Form Sections Master Sections OrgID
        $ins_form_ques 	=	"INSERT INTO ApplicationFormSections (OrgID, FormID, SectionID, SectionTitle, Active, SortOrder)
						  	SELECT '".$OrgIDToInsert."', '".$NewFormID."', SectionID, SectionTitle, Active, SortOrder
						  	FROM ApplicationFormSections WHERE OrgID = '$OrgIDToCopy' AND FormID = '$FormID'";
        $res_form_ques	=	$this->db->getConnection ( "IRECRUIT" )->insert($ins_form_ques);
	    
	}
	
}
