<?php
/**
 * @class		WOTCDisplayText
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCDisplayText
{
    public $db;

    var $conn_string       =   "WOTC";

    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getOrganizationLetter
     * @return 		text/html
     */
    function getOrganizationLetter($ID) {
    
        $sel_org    =   "SELECT OrganizationLetter FROM WotcDisplayText WHERE ID = :ID";
        $params     =   array(":ID"=>$ID);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_org, array($params) );
    
        return $res_org['OrganizationLetter'];
    }

    /**
     * @method		getiRecruitForm
     * @return 		text/array
     */
    function getiRecruitForm($ID) {
    
        $sel_org    =   "SELECT iRecruitForm FROM WotcDisplayText WHERE ID = :ID";
        $params     =   array(":ID"=>$ID);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_org, array($params) );

	$IRF = json_decode($res_org['iRecruitForm'], true);
    
	return $IRF;
    }

    /**
     * @method		getRetainInfo
     * @return 		text/html
     */
    function getRetainInfo($ID) {
    
        $sel_org    =   "SELECT RetainInfo FROM WotcDisplayText WHERE ID = :ID";
        $params     =   array(":ID"=>$ID);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_org, array($params) );
    
        return $res_org['RetainInfo'];
    }

    /**
     * @method		getEEO
     * @return 		text/html
     */
    function getEEO($ID) {
    
        $sel_org    =   "SELECT EEO FROM WotcDisplayText WHERE ID = :ID";
        $params     =   array(":ID"=>$ID);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_org, array($params) );
    
        return $res_org['EEO'];
    }

    /**
     * @method		getReceipt
     * @return 		text/array
     */
    function getReceipt($ID) {
    
        $sel_org    =   "SELECT Receipt FROM WotcDisplayText WHERE ID = :ID";
        $params     =   array(":ID"=>$ID);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_org, array($params) );

	$RES = json_decode($res_org['Receipt'], true);
    
	return $RES;
    
        return $RES;
    }
    
}    
?>
