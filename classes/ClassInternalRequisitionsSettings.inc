<?php
/**
 * @class       InternalRequisitionsSettings
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class InternalRequisitionsSettings {
    
    var $conn_string        =   "IRECRUIT";
    
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method     insInternalRequisitionsSettings
     * @param      string $OrgID
     * @param      string $MultiOrgID
     * @param      array $indeed_info
     * @return     array
     */
    function insInternalRequisitionsSettings($OrgID, $POST) {
        
        //Set parameters
        $params_info    =   array(
                                ":OrgID"                        =>  $OrgID,
                                ":InternalRequisitionsText"     =>  $POST['InternalRequisitionsText'],
                                ":InternalCode"                 =>  $POST['InternalCode'],
                                ":QuestionID"                   =>  $POST['QuestionID'],
                                ":Question"                     =>  $POST['Question'],
                                ":value"                        =>  $POST['value'],
                                ":UQuestion"                    =>  $POST['Question'],
                                ":UInternalCode"                =>  $POST['InternalCode'],
                                ":UInternalRequisitionsText"    =>  $POST['InternalRequisitionsText']
                            );
        // Insert Applicant Information
        $insert_info  =     "INSERT INTO InternalRequisitionsSettings(OrgID, InternalRequisitionsText, InternalCode, QuestionID, Question, value) VALUES(:OrgID, :InternalRequisitionsText, :InternalCode, :QuestionID, :Question, :value)";
        $insert_info .=     " ON DUPLICATE KEY UPDATE InternalRequisitionsText = :UInternalRequisitionsText, InternalCode = :UInternalCode, Question = :UQuestion";
        $result_info  =     $this->db->getConnection ( "IRECRUIT" )->insert($insert_info, array($params_info));
        
        return $result_info;
    }
    
    /**
     * @method		getInternalRequisitionsSettingsInfo
     * @param 		string $OrgID
     * @return 		associative array
     */
    function getInternalRequisitionsSettingsInfo($OrgID) {
        
        $params_info    =   array(":OrgID"=>$OrgID);
        $sel_info       =   "SELECT * FROM InternalRequisitionsSettings WHERE OrgID = :OrgID";
        $res_info       =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_info, array($params_info));
        
        if($res_info['OrgID'] == '') {
            $ins_settings   =   "INSERT INTO InternalRequisitionsSettings SELECT '$OrgID', InternalRequisitionsText, InternalCode, QuestionID, Question, value FROM InternalRequisitionsSettings WHERE OrgID = 'MASTER'";
            $res_settings   =   $this->db->getConnection ( "IRECRUIT" )->insert($ins_settings, array($params_info));
            
            $res_info       =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_info, array($params_info));
        }
        
        return $res_info;
    }

    /**
	 * @method		getInternalCodeByOrgID
	 * @param 		string $OrgID
	 * @return 		associative array
	 */
	function getInternalCodeByOrgID($OrgID) {
        
        $params_info        =   array(":OrgID"=>$OrgID);
	    $sel_internal_code  =   "SELECT OrgID, InternalCode FROM InternalRequisitionsSettings WHERE OrgID = :OrgID";
	    $row_internal_code  =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_internal_code, array($params_info));
        $InternalCode       =   is_null($row_internal_code['InternalCode']) ? '' : $row_internal_code['InternalCode'];
        
        if($row_internal_code['OrgID'] == '') {
            $ins_settings   =   "INSERT INTO InternalRequisitionsSettings SELECT '$OrgID', InternalRequisitionsText, InternalCode, QuestionID, Question, value FROM InternalRequisitionsSettings WHERE OrgID = 'MASTER'";
            $res_settings   =   $this->db->getConnection ( "IRECRUIT" )->insert($ins_settings);
            
            $row_internal_code  =   $this->db->getConnection ( "IRECRUIT" )->fetchAssoc($sel_internal_code, array($params_info));
            $InternalCode       =   is_null($row_internal_code['InternalCode']) ? '' : $row_internal_code['InternalCode'];
        }
        
	    return $InternalCode;
	}
}
?>
