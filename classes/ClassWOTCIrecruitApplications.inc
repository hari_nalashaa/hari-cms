<?php
/**
 * @class		WOTCIrecruitApplications
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCIrecruitApplications
{
    public $db;
    
    var $conn_string       =   "IRECRUIT";
    
    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }
    
    /**
     * @method		getWotcApplicationsInfo
     * @return		array
     */
    function getWotcApplicationsInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
    
    	$columns = $this->db->arrayToDatabaseQueryString ( $columns );
    
    	$sel_wotc_applications = "SELECT $columns FROM WotcApplications";
    
    	if(count($where_info) > 0) {
    		$sel_wotc_applications .= " WHERE " . implode(" AND ", $where_info);
    	}
    
    	if($order_by != "") $sel_wotc_applications .= " ORDER BY " . $order_by;
    
    	$res_wotc_applications = $this->db->getConnection( $this->conn_string )->fetchAllAssoc ( $sel_wotc_applications, $info );
    
    	return $res_wotc_applications;
    }
    
    /**
     * @method      getWotcApplicationByApplicationID
     * @param       $WotcID, $ApplicationID
     */
    function getWotcApplicationByApplicationID($columns, $wotcID, $IrecruitApplicationID, $IrecruitRequestID, $ApplicationID) {
    
    	$columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
    	$params_info    =   array(":wotcID"=>$wotcID, ":IrecruitApplicationID"=>$IrecruitApplicationID, ":IrecruitRequestID"=>$IrecruitRequestID, ":ApplicationID"=>$ApplicationID);
    	$sel_app_data   =   "SELECT $columns FROM WotcApplications WHERE wotcID = :wotcID AND IrecruitApplicationID = :IrecruitApplicationID AND IrecruitRequestID = :IrecruitRequestID AND ApplicationID = :ApplicationID";
    	$res_app_data   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_app_data, array($params_info) );
    
    	return $res_app_data;
    }
    
    /**
     * @method      getWotcApplicationInfo
     * @param       $WotcID, $ApplicationID
     */
    function getWotcApplicationInfo($columns, $wotcID, $IrecruitApplicationID, $IrecruitRequestID) {
    
        $columns        =   $this->db->arrayToDatabaseQueryString ( $columns );
        $params_info    =   array(":wotcID"=>$wotcID, ":IrecruitApplicationID"=>$IrecruitApplicationID, ":IrecruitRequestID"=>$IrecruitRequestID);
        $sel_app_data   =   "SELECT $columns FROM WotcApplications WHERE wotcID = :wotcID AND IrecruitApplicationID = :IrecruitApplicationID AND IrecruitRequestID = :IrecruitRequestID";
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->fetchAssoc ( $sel_app_data, array($params_info) );
        
        return $res_app_data;
    }

    /**
     * @method      insWotcApplicationInfo
     * @param       $set_info, $where_info, $info
     */
    function insWotcApplicationInfo($info) {
    
        $ins_app_data   =   $this->db->buildInsertStatement("WotcApplications", $info);
        $res_app_data   =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_app_data["stmt"], $ins_app_data["info"] );
        
        return $res_app_data;
    }
    
    /**
     * @method      updWotcApplicationInfo
     * @param       $set_info, $where_info, $info
     */
    function updWotcApplicationInfo($set_info = array(), $where_info = array(), $info) {
    
        $upd_apps_info	=	$this->db->buildUpdateStatement("WotcApplications", $set_info, $where_info);
        $res_apps_info	=	$this->db->getConnection( $this->conn_string )->update($upd_apps_info, $info);
    
        return $res_apps_info;
    }
    
    /**
     * @method      validateWotcApplicationDuplicate
     */
    public function validateWotcApplicationDuplicate()
    {
    	$WOTCID =	G::Obj('WOTCApp')->isWotcIDExists();
    
    	if($WOTCID != "") {
    		
    		// clean up db from older entries
    		$query  =   "DELETE FROM ApplicantLock WHERE EntryDate < DATE_ADD(NOW(), INTERVAL -5 MINUTE)";
    		$this->db->getConnection ( "WOTC" )->delete($query);
    		
    		// check new input to old value
    		$query  =   "SELECT CONCAT(FirstName, LastName, Address, City, State, ZipCode)";
    		$query .=   " FROM ApplicantLock";
    		$query .=   " WHERE FirstName = :FirstName";
    		$query .=   " AND LastName = :LastName";
    		$query .=   " AND Address = :Address";
    		$query .=   " AND City = :City";
    		$query .=   " AND State = :State";
    		$query .=   " AND ZipCode = :ZipCode";
    		$params =   array(
		    				':FirstName'    =>  $_POST['FirstName'],
		    				':LastName'     =>  $_POST['LastName'],
		    				':Address'      =>  $_POST['Address'],
		    				':City'         =>  $_POST['City'],
		    				':State'        =>  $_POST['State'],
		    				':ZipCode'      =>  $_POST['ZipCode']
			    		);
    		list ($appentered) = $this->db->getConnection ( "WOTC" )->fetchRow($query, array($params));
    		
    		$app_info = G::Obj('WOTCApp')->getApplicantData($WOTCID, $_POST['Social1'], $_POST['Social2'], $_POST['Social3']);

    		$rtn	=	"";
    		// if available error otherwise add to lock
    		if ($app_info['wotcID'] != "") {
    		
    			$rtn .= "You previously submitted an application to the WOTC program on " . date('F j, Y', strtotime($app_info['EntryDate'])) . "<br><br>";

			$PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
			$displaycode=$PARENT['DisplayCode'];
    		
    			$codereceipt = "N";
    		
    			if ($app_info['Initiated'] == "callcenter") {
    				$codereceipt = "Y";
    			}
    		
    			if ($_REQUEST['Initiated'] == "callcenter") {
    				$codereceipt = "Y";
    			}
    			if ($displaycode == "Y") {
    				$codereceipt = "Y";
    			}
    		
    			if ($codereceipt == "Y") {
    		
    				$query  =   "SELECT Qualifies FROM ApplicantData";
    				$query .=   " WHERE wotcID = :wotcid AND ApplicationID = :applicationid";
    				$params =   array(':wotcid'=>$WOTCID, ':applicationid'=>$app_info['ApplicationID']);
    				list ($CCQ) =   $this->db->getConnection ( "WOTC" )->fetchRow($query, array($params));
    		
    				$CCCODE = array( 'Y' => '1','N' => '2');
    				$CCID = strtoupper(substr($WOTDID,0,2)) . str_pad(preg_replace("/\D/","",$WOTCID),2,'00', STR_PAD_LEFT) . substr($app_info['ApplicationID'], - 4) . $CCCODE[$CCQ];

    				$rtn .= "Please give this ID to your supervisor: ";
    				$rtn .= "<strong style=\"font-size:14pt;color:red;\">" . $CCID . "</strong>";
    				$rtn .= "\n";
    			} // end codereceipt
    		} else if ($appentered != "") {
    			$rtn	.=	"An application with this information has recently been submitted.<br>\n";
    		} else {
    		
    			$query  =   "INSERT INTO ApplicantLock";
    			$query .=   " (EntryDate, FirstName, LastName, Address, City, State, ZipCode)";
    			$query .=   " VALUES (NOW(), :FirstName, :LastName, :Address, :City, :State, :ZipCode)";
    			
    			$params =   array(
		    					':FirstName'    =>  $_POST['First'],
		    					':LastName'     =>  $_POST['Last'],
		    					':Address'      =>  $_POST['Address'],
		    					':City'         =>  $_POST['City'],
		    					':State'        =>  $_POST['State'],
		    					':ZipCode'      =>  $_POST['ZipCode']
			    			);
    			$this->db->getConnection ( "WOTC" )->insert($query, array($params));
    		}
    		
    		return $rtn;
		exit;
    	}

    	
    } // end function
    
}
?>
