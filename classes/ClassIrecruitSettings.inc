<?php
/**
 * @class		IrecruitSettings
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class IrecruitSettings {
	
    /**
     * @tutorial Constructor to load the default database
     *           and instantiate the Database class
     */
    public function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( "IRECRUIT" );
    }
    
    /**
     * @method  getValue
     */
    public function getValue($key) {
        
        $params = array (":IrecruitSettingsKey" => $key);
        $sel_settings_value = "SELECT * FROM IrecruitSettings WHERE IrecruitSettingsKey = :IrecruitSettingsKey";
        $res_settings_value = $this->db->getConnection ( "IRECRUIT" )->fetchAssoc ( $sel_settings_value, array ($params) );
    
        $irecruit_settings_value = '';
        if($res_settings_value['Type'] == 'JSON') {
            $irecruit_settings_value = json_decode($res_settings_value['IrecruitSettingsValue'], true);
        }
        else if($res_settings_value['Type'] == 'STRING') {
            $irecruit_settings_value = $res_settings_value['IrecruitSettingsValue'];
        } 
        
        return $irecruit_settings_value;
    }
    
    /**
     * @method  setValue
     */
    public function setValue($key, $value, $type) {
        // Set parameters
        $params = array (
                        ":IrecruitSettingsKey"      =>  $key, 
                        ":IIrecruitSettingsValue"   =>  $value, 
                        ":UIrecruitSettingsValue"   =>  $value,
                        ":IType"                    =>  $type,
                        ":UType"                    =>  $type
                        );
        // Update session values
        $ins_settings_value  = 'INSERT INTO IrecruitSettings(IrecruitSettingsKey, IrecruitSettingsValue, Type) VALUES(:IrecruitSettingsKey, :IIrecruitSettingsValue, :IType)';
        $ins_settings_value .= ' ON DUPLICATE KEY UPDATE IrecruitSettingsValue = :UIrecruitSettingsValue, Type = :UType';
        // Settings value
        $res_settings_value  = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_settings_value, array ($params) );
        
        return $res_settings_value;
    }
    
}
?>
