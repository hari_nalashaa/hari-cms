<?php 
/**
 * @class		ProductionLogs
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ProductionLogs {
	
    public $db;

    var $conn_string       =   "IRECRUIT";

    /**
     * @tutorial    Constructor to load the default database
     *              and instantiate the Database class
     */
    function __construct()
    {
        $this->db = Database::getInstance();
        $this->db->getConnection( $this->conn_string );
    }
	
    /**
     * @method      getProductionLogs
     */
    function getProductionLogs($OrgID, $from_date, $to_date, $order_by, $limit) {

        $from_date  =   G::Obj('DateHelper')->getYmdFromMdy($from_date);
        $to_date    =   G::Obj('DateHelper')->getYmdFromMdy($to_date);

        $sel_info   =   "SELECT * FROM ProductionLogs";
        $sel_info  .=   " WHERE date(LastModified) BETWEEN :FromDate AND :ToDate";
        $sel_info  .=   " AND OrgID = :OrgID";
        $sel_info  .=   " ORDER BY :OrderBy";

        $params =   array(":OrgID"=>$OrgID, ":FromDate"=>$from_date, ":ToDate"=>$to_date,":OrderBy"=>$order_by);
        $results =  $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_info, array($params));

        return $results;
        
    }

    /**
     * @method      getProductionOrgList
     */
    function getProductionOrgList() {

        $dbh = new PDO("mysql:host=qs3637.pair.com;dbname=irecruit_irecruitapp", 'irecruit_11', 'n4tHvHNm');

        $sel_info   =   "SELECT OrgID, OrganizationName FROM OrgData";
        $sel_info  .=   " ORDER BY OrganizationName";
        
        $sth = $dbh->prepare($sel_info);

        $sth->execute();
        $result = $sth->fetchAll();
        
        return $result;
    }
}
?>
