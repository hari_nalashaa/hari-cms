<?php
/**
 * @class		WOTCStatePortals
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class WOTCStatePortals
{
    public $db;

    var $conn_string       =   "WOTC";

    /**
     * @tutorial   Constructor to load the default database
     *             and instantiate the Database class
     */
    function __construct() {
        $this->db = Database::getInstance ();
        $this->db->getConnection ( $this->conn_string );
    }

    /**
     * @method		insUpdStatePortals
     * @param		$info, $skip
     * @return		array
     */
    function insUpdStatePortals($info, $skip = array()) {
    
        $ins_info   =   $this->db->buildOnDuplicateKeyUpdateStatement("StatePortals", $info, $skip);
        $res_org    =   $this->db->getConnection ( $this->conn_string )->insert ( $ins_info["stmt"], $ins_info["info"] );
    
        return $res_org;
    }

    /**
    * @method              getStatePortalsInfo
    */
    function getStatePortalsInfo($columns, $where_info = array(), $group_by = '', $order_by = '', $info = array()) {

       $columns = $this->db->arrayToDatabaseQueryString ( $columns );
       $sel_applications = "SELECT $columns FROM StatePortals";

       if(count($where_info) > 0) $sel_applications .= " WHERE " . implode(" AND ", $where_info);

       if($group_by != "") $sel_applications .= " GROUP BY " . $group_by;
       if($order_by != "") $sel_applications .= " ORDER BY " . $order_by;

       $res_applications = $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applications, $info );

       return $res_applications;
    }

    /**
    * @method              getStateList
    * @return              array
    */
    public function getStateList() {

         $sel_states = "SELECT Description, Abbr FROM AddressState ORDER BY Description";
         $res_states = $this->db->getConnection( $this->conn_string )->fetchAllAssoc($sel_states);
	 $results = $res_states['results'];
	 $STATES=array();
	 foreach ($results as $r) {
		 $STATES[$r['Abbr']]=$r['Description'];
	 }

         return $STATES;
    }

}    
?>
