<?php
/**
 * @class		Request
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class Request {
	
	/**
	 * @tutorial 	Constructor to load the default database
	 *           	and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( "IRECRUIT" );
	}
	
	/**
	 * @method		getRequestHistory
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getRequestHistory($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM RequestHistory";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	/**
	 * @method		getEmailApprovers
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getEmailApprovers($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM EmailApprovers";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	
	/**
	 * @method	insRequestInfo
	 * @param	$info
	 */
	function insRequestInfo($table_name, $req_info) {
		
		$ins_requisitions = $this->db->buildInsertStatement($table_name, $req_info);
		$res_requisitions = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_requisitions["stmt"], $ins_requisitions["info"] );
	
		return $res_requisitions;
	}
	
	
	/**
	 * @method	insRequestApprovers
	 * @param	
	 */
	function insRequestApprovers($OrgID, $RequestID, $EmailAddress, $AuthorityLevel) {
	
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":EmailAddress"=>$EmailAddress, ":IAuthorityLevel"=>$AuthorityLevel, ":UAuthorityLevel"=>$AuthorityLevel);
		$ins_req_info = "INSERT INTO RequestApprovers(OrgID, RequestID, EmailAddress, AuthorityLevel) VALUES(:OrgID, :RequestID, :EmailAddress, :IAuthorityLevel) ON DUPLICATE KEY UPDATE AuthorityLevel = :UAuthorityLevel";
		$res_req_info = $this->db->getConnection("IRECRUIT")->insert($ins_req_info, array($params));
	
		return $res_req_info;
	}
	
	
	/**
	 * @method	updRequestApprovers
	 * @param	$set_info, $where_info, $info
	 */
	function updRequestApprovers($set_info = array(), $where_info = array(), $info) {
		
		$upd_req_info = $this->db->buildUpdateStatement('RequestApprovers', $set_info, $where_info);
		$res_req_info = $this->db->getConnection("IRECRUIT")->update($upd_req_info, $info);
	
		return $res_req_info;
	}
	
	
	/**
	 * @method	insDefaultRequestQuestions
	 * @param	$set_info, $where_info, $info
	 */
	function insDefaultRequestQuestions($OrgID) {
	
		$ins_info  = "INSERT INTO RequestQuestions SELECT '$OrgID', FormID, QuestionID, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate FROM RequestQuestions WHERE OrgID = 'MASTER'";
		$ins_info .= " ON DUPLICATE KEY UPDATE Active = 'Y'";
		$res_info  = $this->db->getConnection("IRECRUIT")->insert($ins_info);
	
		return $res_info;
	}
	
	
	/**
	 * @method	insRequestNotificationConfig
	 * @param	$OrgID, $MultiOrgID
	 */
	function insRequestNotificationConfig($OrgID, $MultiOrgID) {
	
		$params_info =	array(
							":OrgID"=>$OrgID,
							":MultiOrgID"=>$MultiOrgID,
							":IEmailsList"=>json_encode($_REQUEST['admin_emails']),
							":UEmailsList"=>json_encode($_REQUEST['admin_emails'])
							);
		$ins_info    =	"INSERT INTO RequestNotificationConfig(OrgID, MultiOrgID, EmailsList, LastUpdated)";
		$ins_info   .=  " VALUES(:OrgID, :MultiOrgID, :IEmailsList, NOW())";
		$ins_info   .=	" ON DUPLICATE KEY UPDATE EmailsList = :UEmailsList";
		$res_info    =	$this->db->getConnection("IRECRUIT")->insert($ins_info, array($params_info));
	
		return $res_info;
	}
	
	
	/**
	 * @method		getRequestNotificationConfigInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getRequestNotificationConfigInfo($OrgID, $MultiOrgID) {
	
		$params_info	=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		$sel_req_not_conf_info = "SELECT * FROM RequestNotificationConfig WHERE OrgID = :OrgID AND MultiOrgID = :MultiOrgID";
		$res_req_not_conf_info  = $this->db->getConnection("IRECRUIT")->fetchAssoc ( $sel_req_not_conf_info, array($params_info) );
	
		return $res_req_not_conf_info;
	}
	
	
	/**
	 * @method		insRequestQuestions
	 * @param		$set_info, $where_info, $info
	 */
	function insRequestQuestions($req_fields, $req_fields_que_typeids, $OrgID, $QuestionID, $value = '') {
	
		$QuestionDescription  =   $req_fields[$QuestionID];
		$QuestionTypeID       =   $req_fields_que_typeids[$QuestionID];
		if($QuestionID == "EEOCode") $QuestionID = "eeocode";

		$ins_info     =   "INSERT INTO RequestQuestions(OrgID, FormID, QuestionID, QuestionTypeID, Question, value, Active) VALUES(:OrgID, :FormID, :QuestionID, :QuestionTypeID, :Question, :value, :Active)";
		$ins_info    .=   " ON DUPLICATE KEY UPDATE Question = :UQuestion";
		$params_info  =   array(
                            ":OrgID"            =>  $OrgID,
                            ":FormID"           =>  "STANDARD",
                            ":QuestionID"       =>  $QuestionID,
                            ":QuestionTypeID"   =>  $QuestionTypeID, 
                            ":Question"         =>  $QuestionDescription,
                            ":UQuestion"        =>  $QuestionDescription,
                            ":Active"           =>  "Y",
                            ":value"            =>  $value
		                  );
		$res_info     =   $this->db->getConnection("IRECRUIT")->insert($ins_info, array($params_info));
	
		return $res_info;
	}

	
	/**
	 * @method		updRequestQuestions
	 * @param		$set_info, $where_info, $info
	 */
	function updRequestQuestions($set_info = array(), $where_info = array(), $info) {
	
		$upd_info = $this->db->buildUpdateStatement('RequestQuestions', $set_info, $where_info);
		$res_info = $this->db->getConnection("IRECRUIT")->update($upd_info, $info);
	
		return $res_info;
	}
	
	
	/**
	 * @method	delRequestQuestions
	 * @return	array
	 */
	function delRequestQuestions($OrgID, $FormID, $QuestionID) {
	
		$del_req_que = "DELETE FROM RequestQuestions WHERE OrgID = :OrgID AND FormID = :FormID AND QuestionID = :QuestionID";
		$params = array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":QuestionID"=>$QuestionID);
		$res_req_que = $this->db->getConnection("IRECRUIT")->delete ( $del_req_que, array($params));
	
		return $res_req_que;
	}
	
	
	/**
	 * @method	insRequestArchive
	 * @return
	 */
	function insRequestArchive($info) {
		
		$ins_req_archieve = "INSERT INTO RequestHistoryArchive 
							 SELECT OrgID, RequestID, UpdateID, Date, UserID, Comments 
							 FROM RequestHistory 
							 WHERE OrgID = :OrgID AND RequestID = :RequestID AND UpdateID = :UpdateID";
		$res_req_archieve = $this->db->getConnection("IRECRUIT")->insert ( $ins_req_archieve, $info );
	
		return $res_req_archieve;
	}	
	
	
	/**
	 * @method	insApproversInfo
	 * @param	$info
	 */
	function insApproversInfo($info) {
		
		$ins_approvers_info = $this->db->buildInsertStatement('EmailApprovers', $info);
		$res_approvers_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_approvers_info["stmt"], $ins_approvers_info["info"] );
	
		return $res_approvers_info;
	}
	
	
	/**
	 * @method	delApproversInfo
	 * @return	array
	 */
	function delApproversInfo($table_name, $where_info = array(), $info = array(), $limit = "") {
		
		$del_req_info = "DELETE FROM $table_name";
		if(count($where_info) > 0) {
			$del_req_info .= " WHERE " . implode(" AND ", $where_info);
		}
		if($limit != "") {
			$del_req_info .= " LIMIT $limit";
		}
	
		$res_req_info = $this->db->getConnection("IRECRUIT")->delete ( $del_req_info, $info );
	
		return $res_req_info;
	}
	
	
	/**
	 * @method	delRequestHistory
	 * @return	array
	 */
	function delRequestHistory($where_info = array(), $info = array(), $limit = "") {
		
		$del_req_info = "DELETE FROM RequestHistory";
		if(count($where_info) > 0) {
			$del_req_info .= " WHERE " . implode(" AND ", $where_info);
		}
		if($limit != "") {
			$del_req_info .= " LIMIT $limit";
		}
	
		$res_req_info = $this->db->getConnection("IRECRUIT")->delete ( $del_req_info, $info );
	
		return $res_req_info;
	}
	
	
	/**
	 * @method		getApproversInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getApproversInfo($table_name, $columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM $table_name";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	
	/**
	 * @method		getRequestApproversInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getRequestApproversInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_requisition_info = "SELECT $columns FROM RequestApprovers";
		if(count($where_info) > 0) {
			$sel_requisition_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_requisition_info .= " ORDER BY " . $order_by;
	
		$row_requisition_info  = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_requisition_info, $info );
	
		return $row_requisition_info;
	}
	
	
	/**
	 * @method		getFilePurposeInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getFilePurposeInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_file_purpose_info = "SELECT $columns FROM FilePurpose";
		if(count($where_info) > 0) {
			$sel_file_purpose_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_file_purpose_info .= " ORDER BY " . $order_by;
	
		$row_file_purpose_info = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_file_purpose_info, $info );
	
		return $row_file_purpose_info;
	}
	
	
	/**
	 * @method	insFilePurposeInfo
	 * @param	$info
	 */
	function insFilePurposeInfo($info) {
		
		$ins_file_purpose_info = $this->db->buildInsertStatement('FilePurpose', $info);
		$res_file_purpose_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_file_purpose_info["stmt"], $ins_file_purpose_info["info"] );
	
		return $res_file_purpose_info;
	}
	
	
	/**
	 * @method	delFilePurposeInfo
	 * @return	array
	 */
	function delFilePurposeInfo($where_info = array(), $info = array()) {
		
		$del_file_purpose_info = "DELETE FROM FilePurpose";

		if(count($where_info) > 0) {
			$del_file_purpose_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		$res_file_purpose_info = $this->db->getConnection("IRECRUIT")->delete ( $del_file_purpose_info, $info );
	
		return $res_file_purpose_info;
	}
	
	
	/**
	 * @method		getActiveMinutesInfo
	 * @param		$columns = "", $where_info = array(), $order_by = "", $info = array()
	 * @return		associative array
	 */
	function getActiveMinutesInfo($columns = "", $where_info = array(), $order_by = "", $info = array()) {
		
		$columns = $this->db->arrayToDatabaseQueryString ( $columns );
	
		$sel_active_minutes_level = "SELECT $columns FROM ActiveMinutes";
		if(count($where_info) > 0) {
			$sel_active_minutes_level .= " WHERE " . implode(" AND ", $where_info);
		}
	
		if($order_by != "") $sel_active_minutes_level .= " ORDER BY " . $order_by;
	
		$row_active_minutes_level = $this->db->getConnection("IRECRUIT")->fetchAllAssoc ( $sel_active_minutes_level, $info );
	
		return $row_active_minutes_level;
	}
	
	
	/**
	 * @method	insActiveMinutesInfo
	 * @param	$info
	 */
	function insActiveMinutesInfo($info) {
		
		$ins_act_min_info = $this->db->buildInsertStatement('ActiveMinutes', $info);
		$res_act_min_info = $this->db->getConnection ( "IRECRUIT" )->insert ( $ins_act_min_info["stmt"], $ins_act_min_info["info"] );
	
		return $res_act_min_info;
	}
	
	
	/**
	 * @method	delActiveMinutesInfo
	 * @return	array
	 */
	function delActiveMinutesInfo($where_info = array(), $info = array()) {
		
		$del_act_min_info = "DELETE FROM ActiveMinutes";
		if(count($where_info) > 0) {
			$del_act_min_info .= " WHERE " . implode(" AND ", $where_info);
		}
	
		$res_act_min_info = $this->db->getConnection("IRECRUIT")->delete ( $del_act_min_info, $info );
	
		return $res_act_min_info;
	}
	
	
}
?>
