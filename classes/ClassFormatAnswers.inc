<?php
/**
 * @class		FormatAnswers
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class FormatAnswers {
	
	var $conn_string       =   "IRECRUIT";

	
	/**
	 * @tutorial   Constructor to load the default database
	 *             and instantiate the Database class
	 */
	function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method     getFormattedAnswer
	 * @param      string $QuestionID
	 * @param      string $QuestionTypeID
	 * @param      string $DISPLAYCHOICES
	 * @param      string $DATA
	 * @return     string
	 */
	function getFormattedAnswer($QuestionID, $QuestionTypeID, $DISPLAYCHOICES, $DATA) {

	   $Ans = "";

	   if ($this->isJson($DISPLAYCHOICES)) {
		//$DISPLAYCHOICES = json_decode($DISPLAYCHOICES,true);
	   }
	   if ($this->isJson($DATA)) {
		$DATA = json_decode($DATA,true);
	   }

	   //  QuestionTypes
	   if ($QuestionTypeID == 2) { // Radio
		$Ans = $this->getDisplayValue($DATA,$DISPLAYCHOICES);
	   } else if ($QuestionTypeID == 3) { // Pulldown
		$Ans = $this->getDisplayValue($DATA,$DISPLAYCHOICES);
	   } else if ($QuestionTypeID == 4) { // Pulldown - Multiple Selection
/* needs display assignment and testing */
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 5) { // Text Area
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 6) { // Text
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 7) { // Display Text Area
		/* is display text only */
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 8) { // File Upload
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 9) { // Checkbox with Year and Comment
		foreach ($DATA AS $key=>$D) {
		    $skill = $QuestionID . "-" . $key;
		    $year = $skill . "-yr";
		    $comment = $skill . "-comments";
		    $Ans .= "Skill: " . $D[$skill] . ", Years: " . $D[$year] . ", Comment: " . $D[$comment] . "<br>";
		}
	   } else if ($QuestionTypeID == 10) { // Hidden
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 11) { // Password
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 12) { // On and Off
		$Ans = $this->getDisplayValue($DATA,$DISPLAYCHOICES);
	   } else if ($QuestionTypeID == 13) { // Phone
		$Ans = "(" . $DATA[0] . ") " . $DATA[1] . "-" . $DATA[2];
	   } else if ($QuestionTypeID == 14) { // Phone w/ext
		$Ans = "(" . $DATA[0] . ") " . $DATA[1] . "-" . $DATA[2] . " ext: " . $DATA[3];
	   } else if ($QuestionTypeID == 15) { // Social Security
		$Ans = $DATA[0] . "-" . $DATA[1] . "-" . $DATA[2];
	   } else if ($QuestionTypeID == 17) { // Date
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 18) { // Checkbox
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 22) { // Radio List
		$Ans = $this->getDisplayValue($DATA,$DISPLAYCHOICES);
	   } else if ($QuestionTypeID == 23) { // Radio - Long Question
		$Ans = $this->getDisplayValue($DATA,$DISPLAYCHOICES);
	   } else if ($QuestionTypeID == 30) { // Signature
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 40) { // Captcha
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 45) { // Divider
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 50) { // Vault
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 60) { // From Merge Content
/* needs development and testing */
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 90) { // Attachment
/* needs development and testing */
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 98) { // Spacer
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 99) { // Instruction
		$Ans = $DATA;
	   } else if ($QuestionTypeID == 100) { //Radio with group of skills
		$DATA = @unserialize($DATA);
		foreach ($DATA AS $row=>$D) {
		    foreach ($D AS $value) {
		   	$Ans .= $row . " - " . $value . "<br>";
		    }
		}
	   } else if ($QuestionTypeID == 120) { // Pulldown for daily shifts
		$DATA = @unserialize($DATA);
		foreach ($DATA AS $key=>$D) {
			for($i=0; $i <= count($D); $i++) {
				$BLOCK[$i][$key] = $D[$i];
			}
		}
		foreach ($BLOCK AS $R) {
			if (($R['days'] != "") && (($R['from_time'] != "") || ($R['to_time'] != ""))) {
	  	   	   $Ans .= $R['days'] . " from " . $R['from_time'] . " to " . $R['to_time'] . "<br>";
			}
		}
	   } else if ($QuestionTypeID == 1818) { // Checkbox List 
		$i=0;
		foreach ($DATA as $D) {
		    $i++;
		    if ($i > 1) { $Ans .= ", "; }
		  $Ans .= $this->getDisplayValue($D,$DISPLAYCHOICES);
		} 
	   }

	    //$Ans .= print_r($DATA,true);
	    //$Ans .= $DATA;
	    //$Ans .= print_r($DISPLAYCHOICES,true);

	   // Defaut for null answers
	   if ($Ans == "") { $Ans = "Not Entered"; }

	   return $Ans;

	} // end function

	function isJson($string) {
   		json_decode($string);
   		return json_last_error() === JSON_ERROR_NONE;
	}

	function getDisplayValue($answer, $displaychoices) {
            $rtn = '';

            if ($answer != '') {
                $Values = explode ( '::', $displaychoices );
                foreach ( $Values as $v => $q ) {
                    list ( $vv, $qq ) = explode ( ":", $q, 2 );
                    if ($vv == $answer) {
                        $rtn .= $qq;
                    }
                }
            }

            return $rtn;

	} // end function

} // end class
