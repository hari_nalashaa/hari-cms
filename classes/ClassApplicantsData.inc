<?php
/**
 * @class		ApplicantsData
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ApplicantsData {
	
	var $conn_string       =   "IRECRUIT";
	
	/**
	 * @tutorial Constructor to load the default database
	 *           and instantiate the Database class
	 */
	public function __construct() {
		$this->db = Database::getInstance ();
		$this->db->getConnection ( $this->conn_string );
	}
	
	/**
	 * @method printSection
	 * @param  string $OrgID
	 * @param  string $FormID
	 * @param  string $Question
	 * @param  string $QuestionID
	 * @param  string $QuestionTypeID
	 * @param  string $SectionID
	 * @param  array $APPDATA
	 * @param  string $DisplayQuestion
	 * @return string
	 */
	function printSection($OrgID, $FormID, $Question, $QuestionID, $QuestionTypeID, $SectionID, $APPDATA, $DisplayQuestion = "true") {
		global $permit, $feature;
		
		$FQ       =   G::Obj('FormQuestionsDetails')->getFormQuestionDetailsInfo("*", $OrgID, $FormID, $QuestionID);
		
		$rtn      =   '';
		
		if ($FQ ['QuestionTypeID'] == 13) {
		    if(G::Obj('GenericLibrary')->isJSON($APPDATA [$FQ['QuestionID']]) === true) {
		        $Answer13  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
		        $APPDATA [$QuestionID]    =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer13[0], $Answer13[1], $Answer13[2], '' );
		    }
		    else if(is_string($APPDATA [$FQ['QuestionID']]) && $APPDATA [$FQ['QuestionID']] != "") {
		        $APPDATA [$QuestionID]    =   $APPDATA [$FQ['QuestionID']];
		    }
		    else {
		        $APPDATA [$QuestionID]    =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA [$FQ['QuestionID'] . '1'], $APPDATA [$FQ['QuestionID'] . '2'], $APPDATA [$FQ['QuestionID'] . '3'], '' );
		    }
		} // end phone without extension
		else if ($FQ ['QuestionTypeID'] == 14) {
		    if(G::Obj('GenericLibrary')->isJSON($APPDATA [$FQ['QuestionID']]) === true) {
		        $Answer14  =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
		        $APPDATA [$QuestionID]    =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer14[0], $Answer14[1], $Answer14[2], $Answer14[3] );
		    }
		    else if(is_string($APPDATA [$FQ['QuestionID']]) && $APPDATA [$FQ['QuestionID']] != "") {
		        $APPDATA [$QuestionID]    =   $APPDATA [$FQ['QuestionID']];
		    }
		    else {
		        $APPDATA [$QuestionID]    =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA [$FQ['QuestionID'] . '1'], $APPDATA [$FQ['QuestionID'] . '2'], $APPDATA [$FQ['QuestionID'] . '3'], $APPDATA [$FQ['QuestionID'] . 'ext'] );
		    }
		} // end phone with extension
		else if ($FQ ['QuestionTypeID'] == 15) {
		    if ($_GET ['emailonly'] == 'Y') {
		        $APPDATA [$QuestionID]    =   'XXX-XX-XXXX';
		    } else {
		        $Answer15                 =   ($APPDATA [$FQ['QuestionID']] != "") ? json_decode($APPDATA [$FQ['QuestionID']], true) : "";
		        $APPDATA [$QuestionID]    =   $Answer15[0] . '-' . $Answer15[1] . '-' . $Answer15[2];
		    }
		}
		else if ($QuestionTypeID == 9) {
			$APPDATA [$QuestionID] = G::Obj('ApplicantsData')->getMultiAnswer ( $APPDATA, $QuestionID, $FQ ['value'], $QuestionTypeID );
		}
		else if ($QuestionTypeID == 18) {
		    $Answer18  = json_decode($APPDATA [$QuestionID], true);
		    foreach ($Answer18 as $Answer18Key=>$Answer18Value) {
		        $APPDATA[$Answer18Key] =   $Answer18Value;
		    }
		    $APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer18);
		
		    $APPDATA [$QuestionID] = G::Obj('ApplicantsData')->getMultiAnswer ( $APPDATA, $QuestionID, $FQ ['value'], $QuestionTypeID );
		}
		else if ($QuestionTypeID == 1818) {
		    $Answer18  = json_decode($APPDATA [$QuestionID], true);
		    foreach ($Answer18 as $Answer18Key=>$Answer18Value) {
		        $APPDATA[$Answer18Key] =   $Answer18Value;
		    }
		    $APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer18);
		    
		    $APPDATA [$QuestionID] = G::Obj('ApplicantsData')->getMultiAnswer ( $APPDATA, $QuestionID, $FQ ['value'], $QuestionTypeID );
		}		
		else if (($QuestionTypeID == 1) || ($QuestionTypeID == 24)) {
			$APPDATA [$QuestionID] = "from " . $APPDATA [$QuestionID . from] . " to " . $APPDATA [$QuestionID . to];
		}
		else if ($FQ ['value'] && $QuestionTypeID != 100 && $QuestionTypeID != 120) {
		    $APPDATA [$QuestionID] = G::Obj('ApplicantsData')->getDisplayValue ( $APPDATA [$QuestionID], $FQ ['value'] );
		}
		
			
		if ($QuestionTypeID == 100) {
			
			if ($APPDATA [$QuestionID] != "") {
				
				$canswer = @unserialize ( $APPDATA [$QuestionID] );
				$radio_cust_que = '';
				if (is_array ( $canswer )) {
					$radio_cust_que .= $FQ ['Question'] . "<br>";
					
					foreach ( $canswer as $cakey => $caval ) {
						$radio_cust_que .= '<strong>' . $cakey . ':';
						
						foreach ( $caval as $cavk => $cavv ) {
							$radio_cust_que .= $cavv . '</strong>, ';
						}
					}
				}
				
				$rtn .= trim ( $radio_cust_que, ", " );
			}
		} else if ($QuestionTypeID == 120) {
			if ($APPDATA [$QuestionID] != "") {
				$canswer = unserialize ( $APPDATA [$QuestionID] );
				
				$available_time = array ();
				if (is_array ( $canswer )) {
					$rtn .= "<br>" . $FQ ['Question'] . "<br>";
					
					$days_count = count ( $canswer ['days'] );
					
					for($ci = 0; $ci < $days_count; $ci ++) {
						$available_time [] = $canswer ['days'] [$ci] . ": " . $canswer ['from_time'] [$ci] . " - " . $canswer ['to_time'] [$ci];
					}
					
					$rtn .= "<strong>" . implode ( ", ", $available_time ) . "</strong>";
				}
			}
		} else if ($QuestionTypeID == 9) {
			$rtn .= '<p>';
			if($DisplayQuestion == "true") {
			    $rtn .=   $FQ ['Question'] . ':';
			}
			$rtn .= "<br><strong>" . str_replace ( "<br>", "&nbsp;&nbsp;", $APPDATA [$QuestionID] ) . "</strong>";
			$rtn .= '</p>';
		}
		else if ($QuestionTypeID == 18) {
		    
		    $answer_18    =   $this->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
		    $answer_18    =   str_replace(array("&#8226;"), "", $answer_18);
		    $answer_18    =   str_replace(array("<br>"), ", ", $answer_18);
		    
		    $rtn .= '<p>';
		    if($DisplayQuestion == "true") {
		        $rtn .=   $FQ ['Question'] . ':';
		    }
		    $rtn .= str_replace(array("&#8226;", "<br>"), "", $answer_18);
		    $rtn .= '</p>';
		}
		else if ($QuestionTypeID == 1818) {
		    $answer_18    =   $this->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
		    $answer_18    =   str_replace(array("&#8226;"), "", $answer_18);
		    $answer_18    =   str_replace(array("<br>"), ", ", $answer_18);
		    
		    $rtn .= '<p>';
		    if($DisplayQuestion == "true") {
		        $rtn .=   $FQ ['Question'] . ':';
		    }
		    $rtn .= str_replace(array("&#8226;", "<br>"), "", $answer_18);
		    $rtn .= '</p>';
		}
		else {
		    if($DisplayQuestion == "true") {
		        $rtn .= '<p>' . $Question . ' <b>' . $APPDATA [$QuestionID] . '</b></p>';
		    }
		    else if($DisplayQuestion == "false") {
		        $rtn .= $APPDATA [$QuestionID] . '</b></p>';
		    }
		}
		
		return $rtn;
	} // end of function
	
	/**
	 * @method printApplicationSection
	 * @param  string $OrgID
	 * @param  string $FormID
	 * @param  string $APPDATA
	 * @param  string $SectionID
	 * @return string
	 */
	function printApplicationSection($OrgID, $FormID, $APPDATA, $SectionID) {
	
		global $permit, $feature;
	
		//Set the parameters
		$params = array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
		//Set columns
		$columns = array("Question", "QuestionID", "QuestionTypeID", "value", "SectionID");
		//Set condition
		$where   = array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
		//Get Questions information
		$rslt = G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));
	
		$rtn = '';
	
		$rowcnt = $rslt['count'];
	    
		foreach ($rslt['results'] as $FQ) {
	
			// format answers
			if ($FQ ['QuestionID'] == 'sin') {
			    $sin_info    =   json_decode($APPDATA['sin'], true);
				if ($APPDATA['sin'] != "") {
					if ($_GET ['emailonly'] == 'Y') {
						$APPDATA [$FQ ['QuestionID']] = 'XXX-XX-XXXX';
					} else {
						$APPDATA [$FQ ['QuestionID']] = $sin_info [0] . '-' . $sin_info [1] . '-' . $sin_info [2];
					}
				} // end if not blank
			} // end sin
	
			if ($FQ ['QuestionID'] == 'social') {
			    $social_info =   json_decode($APPDATA['social'], true);
				if ($APPDATA['social'] != "") {
					if ($_GET ['emailonly'] == 'Y') {
						$APPDATA [$FQ ['QuestionID']] = 'XXX-XX-XXXX';
					} else {
						$APPDATA [$FQ ['QuestionID']] = $social_info[0] . '-' . $social_info[1] . '-' . $social_info[2];
					}
				} // end if not blank
			} // end social
	
			if ($FQ ['QuestionID'] == 'cellphone') {
			    $cellphone_info =   json_decode($APPDATA['cellphone'], true);
			    $APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $cellphone_info[0], $cellphone_info[1], $cellphone_info[2], '' );
			} // end cellphone
	
			if ($FQ ['QuestionID'] == 'homephone') {
			    $homephone_info =   json_decode($APPDATA['homephone'], true);
			    $APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $homephone_info[0], $homephone_info[1], $homephone_info[2], '' );
			} // end homephone
	
			if ($FQ ['QuestionID'] == 'busphone') {
			    $busphone_info =   json_decode($APPDATA['busphone'], true);
			    $APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $busphone_info[0], $busphone_info[1], $busphone_info[2], $busphone_info[3] );
			} // end busphone
	
			if ($FQ ['value'] && $FQ ['QuestionTypeID'] != 100 && $FQ ['QuestionTypeID'] != 120) {
				$APPDATA [$FQ ['QuestionID']] = G::Obj('ApplicantsData')->getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
			}
	
			if ($FQ ['QuestionID'] == 'cur_phone') {
			    $cur_phone_info =   json_decode($APPDATA['cur_phone'], true);
				$APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $cur_phone_info[0], $cur_phone_info[1], $cur_phone_info[2], $cur_phone_info[3] );
			} // end cur_phone
	
			if (($FQ ['QuestionTypeID'] == 18) || ($FQ ['QuestionTypeID'] == 1818) || ($FQ ['QuestionTypeID'] == 9)) {
				$APPDATA [$FQ ['QuestionID']] = G::Obj('ApplicantsData')->getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
			}
	
			if (($FQ ['QuestionTypeID'] == 1) || ($FQ ['QuestionTypeID'] == 24)) {
				$APPDATA [$FQ ['QuestionID']] = "from " . $APPDATA [$FQ ['QuestionID'] . from] . " to " . $APPDATA [$FQ ['QuestionID'] . to];
			}
	
			$print = "Y";
	
			if ($feature ['LimitApplicationToAnswersOnly'] == "Y") {
					
				if (($APPDATA [$FQ ['QuestionID']] == "") || ($APPDATA [$FQ ['QuestionID']] == "from  to ")) {
					$print = "N";
				}
				if (($FQ ['QuestionTypeID'] == 99) || ($FQ ['QuestionTypeID'] == 98)) {
					$print = "N";
				}
			} // end feature
	
			if ($print == "Y") {
					
				if ($FQ ['QuestionTypeID'] == 100) {
						
					if($APPDATA [$FQ ['QuestionID']] != "") {
							
						$canswer = @unserialize($APPDATA [$FQ ['QuestionID']]);
						$radio_cust_que = '';
						if(is_array($canswer)) {
							$radio_cust_que .= $FQ ['Question']."<br>";
								
							foreach ($canswer as $cakey=>$caval) {
								$radio_cust_que .= '<strong>'.$cakey.':';
	
								foreach ($caval as $cavk=>$cavv) {
									$radio_cust_que .= $cavv.'</strong>, ';
								}
	
							}
								
						}
	
						$rtn .= trim($radio_cust_que, ", ");
					}
						
				}
				else if($FQ ['QuestionTypeID'] == 120) {
					if($APPDATA [$FQ ['QuestionID']] != "") {
						$canswer = unserialize($APPDATA [$FQ ['QuestionID']]);
	
						$available_time = array();
						if(is_array($canswer)) {
							$rtn .= "<br>".$FQ ['Question']."<br>";
								
							$days_count = count($canswer['days']);
								
								
							for($ci = 0; $ci < $days_count; $ci++)  {
								$available_time[] = $canswer['days'][$ci] . ": ".$canswer['from_time'][$ci]." - ".$canswer['to_time'][$ci];
							}
	
							$rtn .= "<strong>".implode(", ", $available_time)."</strong>";
						}
					}
						
				}
				else if($FQ ['QuestionTypeID'] == 9) {
					$rtn .= '<p>'.$FQ ['Question'];
					$rtn .= "<br><strong>".str_replace("<br>", "&nbsp;&nbsp;", $APPDATA [$FQ ['QuestionID']])."</strong>";
					$rtn .= '</p>';
				}
				else {
				    if($FQ ['QuestionTypeID'] != "99") {
				        $rtn .= '<p>'.$FQ ['Question'] . ' <b>' . $APPDATA [$FQ ['QuestionID']] . '</b></p>';
				    }
				    else {
				        $rtn .= '<p>'.$FQ ['Question'] . '</p>';
				    }
				}
					
					
			} // end if print
		} // end while
	
		if ($rowcnt == 0) {
			$rtn = '';
		}
	
		return $rtn;
	} // end of function
	
	/**
	 * @method     getDisplayValue
	 * @param      string $answer
	 * @param      string $displaychoices
	 * @return     string
	 */
	function getDisplayValue($answer, $displaychoices) {
	    $rtn = '';
	
	    if ($answer != '') {
	        $Values = explode ( '::', $displaychoices );
	        foreach ( $Values as $v => $q ) {
	            list ( $vv, $qq ) = explode ( ":", $q, 2 );
	            if ($vv == $answer) {
	                $rtn .= $qq;
	            }
	        }
	    }
	    
	    return $rtn;
	    
	} // end of function

	/**
	 * @method     getMultiAnswer
	 * @param      array $APPDATA
	 * @param      string $id
	 * @param      string $displaychoices
	 * @param      string $questiontype
	 * @return     string
	 */
	function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
	    $rtn = '';
	    $val = '';
	    $lex = '';
	
	    if (($id == 'daysavailable') || ($id == 'typeavailable')) {
	
	        $le = ', ';
	        $li = '';
	        $lecnt = - 2;
	    } else {
	
	        $le = '<br>';
	        $li = '&#8226;&nbsp;';
	        $lecnt = - 4;
	    }
	
	    if($questiontype == 18 || $questiontype == 1818) {
	        $displaychoices_vals = explode ( '::', $displaychoices );
	        $cnt   =   count($displaychoices_vals);
	    }
	    else {
	        $cnt   =   $APPDATA [$id . 'cnt'];
	    }
	
	    for($i = 1; $i <= $cnt; $i ++) {
	
	        $val = $this->getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
	        if ($val) {
	            $rtn .= $li . $val;
	            $lex = 'on';
	            $val = '';
	        }
	
	        if ($questiontype == 9) {
	            	
	            $val = $APPDATA [$id . '-' . $i . '-yr'];
	            if ($val) {
	                $rtn .= ',&nbsp;' . $val . ' yrs. ';
	                $lex = 'on';
	                $val = '';
	            }
	            	
	            $val = $APPDATA [$id . '-' . $i . '-comments'];
	            if ($val) {
	                $rtn .= '&nbsp;(' . $val . ') ';
	                $lex = 'on';
	                $val = '';
	            }
	        } // end questiontype
	
	        if ($lex == 'on') {
	            $rtn .= $le;
	            $lex = '';
	        }
	    }
	
	    $rtn = substr ( $rtn, 0, $lecnt );
	
	    return $rtn;
	} // end of function
	
	/**
	 * @method     getApplicantData
	 * @params     $OrgID, $ApplicationID, $QuestionIDs
	 */
	function getApplicantData($OrgID, $ApplicationID, $QuestionIDs) {
	    
        $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
        $QuestionIdsList    =   "'" . implode("', '", $QuestionIDs) . "'";
        $sel_applicant_data =   "SELECT * FROM ApplicantData WHERE OrgID = :OrgID AND ApplicationID = :ApplicationID";
        $sel_applicant_data .=  " AND QuestionID IN ($QuestionIdsList)";
        $res_applicant_data =   $this->db->getConnection ( $this->conn_string )->fetchAllAssoc ( $sel_applicant_data, array($params) );
        $app_data_results   =   $res_applicant_data['results'];
	    
        $app_data           =   array();
        foreach ($app_data_results as $app_info) {
            $app_data[$app_info['QuestionID']] = $app_info['Answer'];
        }
        
	    return $app_data;
	}

	/**
	 * @method		copyApplicantDataTempToApplicantData
	 * @param		$OrgID, $HoldID
	 */
	function copyApplicantDataTempToApplicantData($OrgID, $HoldID, $ApplicationID, $RequestID) {
	    
	    $app_data_results  =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);
	    $app_data_info     =   $app_data_results['results'];
	    
	    for($ad = 0; $ad < count($app_data_info); $ad++) {
	        if($app_data_info[$ad]['QuestionID'] == "ApplicantPicture") {
	            $path_parts            =   pathinfo($app_data_info[$ad]['Answer']);
	            $orig_file_name        =   $path_parts['filename'];
	            $file_extension        =   $path_parts['extension'];
	            $file_name             =   $ApplicationID."-ApplicantPicture".".".$file_extension;
	            
	            //Rename old file to new name
	            @rename(IRECRUIT_DIR . "vault/".$OrgID."/applicant_picture/".$app_data_info[$ad]['Answer'], IRECRUIT_DIR . "vault/".$OrgID."/applicant_picture/".$file_name);

	            $QI                    =   $app_data_info[$ad];
	            $QI['ApplicationID']   =   $ApplicationID;
	            $QI['Answer']          =   $file_name;
	            G::Obj('Applicants')->insUpdApplicantData($QI);
	        }
	        else {
	            $QI                    =   $app_data_info[$ad];
	            $QI['ApplicationID']   =   $ApplicationID;
	            G::Obj('Applicants')->insUpdApplicantData($QI);
	        }
	    }
	    
	    return $res_info;
	}
	
	/**
	 * @method		copyApplicantAttachmentsTempToAppAttachments
	 * @param		$OrgID, $HoldID
	 */
	function copyApplicantAttachmentsTempToAppAttachments($OrgID, $HoldID, $ApplicationID) {
	    
        $app_attach_results =   G::Obj('ApplicantAttachmentsTemp')->getApplicantAttachmentsTempInfo($OrgID, $HoldID);
        $app_attachments    =   $app_attach_results['results'];
	    
        for($ad = 0; $ad < count($app_attachments); $ad++) {
            $HoldID                 =   $app_attachments[$ad]['HoldID'];
            $QI['OrgID']            =   $app_attachments[$ad]['OrgID'];
            $QI['ApplicationID']    =   $ApplicationID;
            $QI['TypeAttachment']   =   $app_attachments[$ad]['TypeAttachment'];
            $QI['PurposeName']      =   $app_attachments[$ad]['PurposeName'];
            $QI['FileType']         =   $app_attachments[$ad]['FileType'];
            
            $old_file_name          =   IRECRUIT_DIR . "vault/".$OrgID."/applicantattachments/".$HoldID."-".$app_attachments[$ad]['PurposeName'].".".$app_attachments[$ad]['FileType'];
            $new_file_name          =   IRECRUIT_DIR . "vault/".$OrgID."/applicantattachments/".$ApplicationID."-".$app_attachments[$ad]['PurposeName'].".".$app_attachments[$ad]['FileType'];
            
            //Rename old file to new name
            rename($old_file_name, $new_file_name);
            
            G::Obj('Attachments')->insApplicantAttachments($QI);
	    }
	    
	    return $res_info;
	}
	
	/**
	 * @method		updApplicantDataInfo
	 * @param		$set_info, $where_info, $info
	 */
	function updApplicantDataInfo($set_info = array(), $where_info = array(), $info) {
	
	    $upd_info = $this->db->buildUpdateStatement("ApplicantData", $set_info, $where_info);
	    $res_info = $this->db->getConnection("IRECRUIT")->update($upd_info, $info);
	
	    return $res_info;
	}
	
}
?>
