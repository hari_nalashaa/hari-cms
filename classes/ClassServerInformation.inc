<?php 
/**
 * @class		ServerInformation
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class ServerInformation {
	
	/**
	 * @tutorial	To identify the device type or the browser type
	 * @return 		string
	 */
	public function getRequestSource() {
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			return 'ajax';
		} else {
			return 'normal';
		}
	}
	
	/**
	 * 
	 * @param unknown $url
	 * @return boolean
	 */
	public function validateUrlContent($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		// don't download content
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if (curl_exec($ch) !== FALSE) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @method getIPAddress()
	 * @return string
	 */
	function getIPAddress()
	{
	    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	    {
	        $ip    =   $_SERVER['HTTP_CLIENT_IP'];
	    }
	    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	    {
	        $ip    =   $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }
	    else
	    {
	        $ip    =   $_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}
	
}
?>