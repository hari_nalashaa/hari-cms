<?php
/**
 * @class		DashboardConfiguration
 * @todo		Use "IRECRUIT", "USERPORTAL", "WOTC" in constructor to access the
 * 				relavent database, otherwise your query will be executed based on 
 * 				the active connection
 * @example		$this->db->getConnection("IRECRUIT")->fetchRow($query);
 * 				$this->db->getConnection("WOTC")->fetchAllAssoc($query);
 * 				$this->db->getConnection("USERPORTAL")->fetchAssoc($query);
 * 				$this->db->getConnection("IRECRUIT")->insert($query);
 * 				$this->db->getConnection("USERPORTAL")->update($query);
 * 				$this->db->getConnection("WOTC")->delete($query);
 */

class DashboardConfiguration {
	
	/**
	 * @tutorial	Constructor to load the default database 
	 * 				and instantiate the Database class
	 */
	function __construct() {
		$this->db =	Database::getInstance();
		$this->db->getConnection("IRECRUIT");
	}
	
	
	/**
	 * @method getDashboardModules
	 * @param $OrgID, $Active, $IsAdmin
	 * @tutorial It will list out all the dashboardmodules
	 * @return array
	 * @type json
	 */
	public function getDashboardModules() {
		
		global $USERID;
		
		//Set parameters for prepared query
		$params		=	array(":UserID"=>$USERID);
		
		$sel_modules = "SELECT DM.* FROM DashboardModules DM 
					   LEFT JOIN DashboardUserModules DU ON DU.ModuleId = DM.ModuleId 
					   WHERE DU.UserID = :UserID 
					   ORDER BY DU.ModuleOrder ASC";
		$row_modules = $this->db->fetchAllAssoc($sel_modules, array($params));
		
		foreach($row_modules['results'] as $key=>$value) {
			$rows[$value['ModuleId']] = $value['Name'];
		} 
		
		return $rows;
	}
	
	/**
	 * @method getUserDashboardModules
	 * @tutorial It will list out all the dashboard user modules
	 * @return array
	 * @type json
	 */
	function getUserDashboardModules() {
		global $USERID;
		
		//Set parameters for prepared query
		$params	=	array(":UserID"=>$USERID);
		
		$sel_modules = "SELECT * FROM DashboardUserModules 
					   WHERE UserID = :UserID 
					   ORDER BY ModuleOrder ASC";
		$row_modules = $this->db->fetchAllAssoc($sel_modules, array($params));

		$numrows	 = $row_modules['count'];		 
		
		foreach($row_modules['results'] as $row) {
			$rows[$row['ModuleId']] = $row['Status'];
			$all[] = $row;
			$modules[$row['ModuleId']] = $row;
		}
		return array($rows, $numrows, $all, $modules);
	}
	
	/**
	 * @method insUserDashboardModules
	 * @tutorial It will insert the default module settings for new user
	 * @return Total affected rows
	 */
	public function insUserDashboardModules() {
		global $USERID;
		
		$dashboardusermodules = $this->getUserDashboardModules();
		$nummodules = $dashboardusermodules[1];
		
		if($nummodules == 0) {
			$insdashusermodules = "INSERT INTO `DashboardUserModules`(ModuleId, UserID, ModuleOrder, Status) 
								   SELECT ModuleId, '$USERID', ModuleId, '1' FROM DashboardModules";
			$resdashusermodules = $this->db->insert($insdashusermodules);
			
			
			$minyear = date("m/d/Y", strtotime("-1 year"));
			$maxyear = date("m/d/Y");
			
			$usermoduleconfig = array("ApplicationMinDate" => $minyear, "ApplicationMaxDate" => $maxyear);

			//Set parameters for prepared query
			foreach($usermoduleconfig as $ukey=>$uval) {
				
				$sel_mod_config = "SELECT * FROM DashboardUserModuleConfig WHERE ModuleId = '2' AND UserID = '".$USERID."' AND ModuleConfigKey = '".$ukey."'";
				$res_mod_config = $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_mod_config);
				
				if(count($res_mod_config) == 0 && (!isset($res_mod_config["UserID"]) || $res_mod_config["UserID"] == "")) {
					$params[":UserID"]	= array($USERID);
					$params[":ModuleConfigKey"]	= array($ukey);
					$params[":ModuleConfigValue"]	= array($uval);
					
					$insusermoduleconfig = "INSERT INTO DashboardUserModuleConfig(ModuleId, UserID, ModuleConfigKey, ModuleConfigValue) VALUES(2, :UserID, :ModuleConfigKey, :ModuleConfigValue)";
					$resusermoduleconfig = $this->db->getConnection("IRECRUIT")->insert($insusermoduleconfig, array($params));
				}
				
			}
			
		}
		
	}

	/**
	 * @method		updDashboardUserModuleStatus
	 * @param		$POST
	 * @tutorial	It will update the settings of active and nonactive dashboard usermodules
	 */
	public function updDashboardUserModuleStatus($USERID, $ModuleId, $Status) {
	
		//Set parameters for prepared query
		$params			=	array(":UserID"=>$USERID, ":ModuleId"=>$ModuleId, ":Status"=>$Status);
		//update module status
		$upd_mod_status	=	"UPDATE `DashboardUserModules` SET Status = :Status WHERE UserID = :UserID AND ModuleId = :ModuleId";
		$res_mod_status =	$this->db->getConnection("IRECRUIT")->update($upd_mod_status, array($params));
		
		return $res_mod_status;
	}
	
	/**
	 * @method updDashboardUserModules
	 * @param $POST
	 * @tutorial It will update the settings of active and nonactive dashboard usermodules
	 */
	public function updDashboardUserModules($POST) {
		global $USERID;
		
		//Set parameters for prepared query
		$params	=	array(":UserID"=>$USERID);
		
		//Deactivate all the modules
		$upddashboardconf = "UPDATE `DashboardUserModules` SET Status = 0 WHERE UserID = :UserID";
		$resdashboardconf = $this->db->getConnection("IRECRUIT")->update($upddashboardconf, array($params));

		//Activate selected modules
		if($POST['modules'] != "") {
			//Based on submitted details activating the required dashboardusermodules
			$upddashboardconf = "UPDATE `DashboardUserModules` SET Status = 1 WHERE UserID = :UserID 
								 AND ModuleId IN ('".implode("','", array_keys($POST['modules']))."')";
			$resdashboardconf = $this->db->getConnection("IRECRUIT")->update($upddashboardconf, array($params));
		}
	}
	
	/**
	 * @method	updDashboardUserModuleConfig
	 * @param	$POST
	 */
	public function updDashboardUserModuleConfig($POST) {
		
		global $USERID;
		
		if($POST['moduleconfiguration'] == 2)
		{
			$usermoduleconfig = array("ApplicationMinDate"	=>	$_POST['ApplicationDate_From'], "ApplicationMaxDate" =>	$_POST['ApplicationDate_To']);
			
			$upddashboardconf = "UPDATE `DashboardUserModuleConfig` SET ModuleConfigValue = :ModuleConfigValue
							 	 WHERE UserID = :UserID
								 AND ModuleId = '2'
								 AND ModuleConfigKey = :ModuleConfigKey";
			
			foreach($usermoduleconfig as $ukey=>$uval) {
				$params[":UserID"]	= array($USERID);
				$params[":ModuleConfigKey"]	= array($ukey);
				$params[":ModuleConfigValue"] = array($uval);
			}
			$resdashboardconf = $this->db->getConnection("IRECRUIT")->update($upddashboardconf, $info);
				
		}
		
		return $resdashboardconf['affected_rows'];
	}
	
	/**
	 * @method updDashboardUserModuleOrder
	 * @param $POST
	 * @tutorial It will update the settings of active and nonactive dashboard usermodules
	 */
	public function updDashboardUserModuleOrder($POST) {
		
		global $USERID;
		
		$params = array(
						":ModuleOrder"	=>	$POST['ModuleOrder'],
						":ModuleId"		=>	$POST['ModuleId'],
						":UserID"		=>	$USERID
					);
		
		//Deactivate all the modules
		$upddashboardconf = "UPDATE `DashboardUserModules` SET ModuleOrder = :ModuleOrder WHERE ModuleId = :ModuleId AND UserID = :UserID";
		$resdashboardconf = $this->db->getConnection("IRECRUIT")->update($upddashboardconf, array($params));
		
		return $resdashboardconf['affected_rows'];
	}
	
	/**
	 * @method insDefaultDashboardTopBlocks
	 * @param $OrgID, $Active, $IsAdmin
	 * @tutorial It will insert the default DashboardTopBlocks for that login user
	 * @return array
	 */
	public function insDefaultDashboardTopBlocks() {
		
		global $USERID, $OrgID;
		
		$insdashusermodules = "INSERT INTO `DashboardTopUserBlocks`(BlockId, BlockPosition, UserID, OrgID, BlockImageClass, BlockPanelColor, ShowImage, BlockOrder, BlockLink, BlockStatus, NewApplicationsInterval) 
							   SELECT BlockId, BlockPosition, '$USERID', '$OrgID', BlockImageClass, BlockPanelColor, 'Yes', BlockOrder, BlockLink, BlockStatus, NewApplicationsInterval
							   FROM DashboardTopUserBlocks WHERE UserID = 'MASTER'";
		$resdashusermodules = $this->db->getConnection("IRECRUIT")->insert($insdashusermodules);
		
		return $resdashusermodules['affected_rows'];
	}
	
	/**
	 * @method getUserDashboardTopBlocks
	 * @param 
	 * @tutorial It will return the blocks related to user
	 * @return array
	 */
	public function getUserDashboardTopBlocks() {
		
		global $USERID, $OrgID;
		
		$params	= array(":UserID"=>$USERID, ":OrgID"=>$OrgID);
		
		$seldashusertopblocks = "SELECT BU.BlockOrder, BU.BlockPanelColor, BU.BlockId, B.BlockText, BU.BlockPosition, BU.BlockImageClass, BU.BlockImage, BU.ShowImage, BU.BlockLink, BU.BlockStatus, BU.NewApplicationsInterval
								 FROM `DashboardTopBlocks` B, DashboardTopUserBlocks BU
								 WHERE BU.BlockId = B.BlockId
								 and BU.UserID = :UserID  
								 and BU.OrgID = :OrgID 
								 ORDER BY BlockOrder ASC";
		$resdashusertopblocks = $this->db->getConnection("IRECRUIT")->fetchAllAssoc($seldashusertopblocks, array($params));
		
		$numdashusertopblocks = $resdashusertopblocks['count'];
		
		foreach($resdashusertopblocks['results'] as $key=>$row) {
			$rows[] = $row;
		}
		
		return array($rows, $numdashusertopblocks);
	}
	
	/**
	 * @method getDashboardTopBlocks
	 * @param
	 * @tutorial It will return all dashboard top blocks
	 * @return array
	 */
	public function getDashboardTopBlocks() {
		
		$seldashtopblocks = "SELECT * FROM DashboardTopBlocks";
		$resdashtopblocks = $this->db->getConnection("IRECRUIT")->fetchAllAssoc($seldashtopblocks);
		
		foreach($resdashtopblocks['results'] as $key => $rowdashmodules) {
			$rows[$rowdashmodules['BlockId']] = $rowdashmodules['BlockText'];
		}
		
		return $rows;
	}
	
	
	/**
	 * @method getDashboardUserTopBlockInfo
	 * @param $POSITION
	 * @tutorial It will return all dashboard top blocks
	 * @return array
	 */
	function getDashboardUserTopBlockInfo($POSITION) {
		
		global $USERID;
		
		$params				=	array(":BlockPosition"=>$POSITION, ":UserID"=>$USERID);
		$seldashtopblocks 	=	"SELECT * FROM DashboardTopUserBlocks 
							 	WHERE BlockPosition = :BlockPosition AND UserID = :UserID";
		$rowdashtopblocks 	=	$this->db->getConnection("IRECRUIT")->fetchAssoc($seldashtopblocks, array($params));
		
		return $rowdashtopblocks;
	}
	
	
	/**
	 * @method updDashboardUserTopBlockInfo
	 * @param $POSITION
	 * @tutorial It will return all dashboard top blocks
	 * @return array
	 */
	public function updDashboardUserTopBlockInfo() {
		
		global $USERID;

		$pathinfo  = pathinfo($_FILES['blockimage']['name']);
		$extension = $pathinfo['extension'];
		
		$imagefiles = array("FIRST"=>"first", "SECOND"=>"two", "THIRD"=>"three", "FOURTH"=>"four");
		$filename  = 'dashboard'.strtolower($imagefiles[$_POST['blockposition']]).".".$extension;
		
		$blockimageclass =  explode(" ", $_POST['blockimageclass']);
		$imageclass = $blockimageclass[1];
		
		$params	=	array(
						":BlockId"					=>	$_POST['blocktype'],
						":BlockPanelColor"			=>	$_POST['blockpanelcolor'],
						":BlockImageClass"			=>	$imageclass,
						":ShowImage"				=>	$_POST['showimage'],
						":BlockStatus"				=>	$_POST['blockstatus'],
						":NewApplicationsInterval"	=>	$_POST['new_applications_interval'],
						":BlockPosition"			=>	$_POST['blockposition'],
						":UserID"					=>	$USERID
					);
		
		$upddashtopblocks  = "UPDATE DashboardTopUserBlocks
						  	  SET BlockId = :BlockId, BlockPanelColor = :BlockPanelColor,
						  	  BlockImageClass = :BlockImageClass, ShowImage = :ShowImage, 
						  	  BlockStatus = :BlockStatus, NewApplicationsInterval = :NewApplicationsInterval 
						  	  WHERE BlockPosition = :BlockPosition AND UserID = :UserID";
		$resdashtopblocks  = $this->db->getConnection("IRECRUIT")->update($upddashtopblocks, array($params));
		
		return $resdashtopblocks['affected_rows'];
	}
	
	
	/**
	 * @method getDashboardTopBlocksCount
	 * @param 
	 * @tutorial It will return all dashboard top blocks count based on top block id
	 * @return integer
	 */	
	public function getDashboardTopBlocksCount($blockid) {
		
		global $ApplicationsObj, $AppointmentsObj, $RequisitionsObj, $RemindersObj, $RequisitionDetailsObj;
		
		if($blockid == 1) {
			$POST['DataType'] = 'REQUISITIONS';
			$POST['Active'] = 'Y';

			$count = $RequisitionDetailsObj->countRequistions(json_encode($POST));
		}
		else if($blockid == 2) {
			$user_top_block_info = $this->getBlockInfoByID($blockid);
			$POST['NewApplicationsInterval'] = $user_top_block_info['NewApplicationsInterval'];
			$new_applicants_info = $ApplicationsObj->getRecentApplicationsCount();
			$count = $new_applicants_info['Count'];
		}
		else if($blockid == 3) {
			
			$POST['DataType'] = 'APPOINTMENTS';
			$POST['AppointmentType'] = 'MONTH';
			$POST['SMonth'] = date('m');
			$POST['SYear'] = date('Y');
			$count = $AppointmentsObj->countAppointments(json_encode($POST));
		}
		else if($blockid == 4) {
			
			$POST['DataType'] = 'APPOINTMENTS';
			$POST['AppointmentType'] = 'DAY';
			$POST['SDay'] = date('d');
			$POST['SMonth'] = date('m');
			$POST['SYear'] = date('Y');
			$count = $AppointmentsObj->countAppointments(json_encode($POST));
		}
		else if($blockid == 5) {
			
			$POST['DataType'] = 'REMINDERS';
			$POST['ReminderType'] = 'MONTH';
			$POST['SMonth'] = date('m');
			$POST['SYear'] = date('Y');
			$count = $RemindersObj->countReminders(json_encode($POST));
		}
		else if($blockid == 6) {
			
			$POST['DataType'] = 'REMINDERS';
			$POST['ReminderType'] = 'DAY';
			$POST['SDay'] = date('d');
			$POST['SMonth'] = date('m');
			$POST['SYear'] = date('Y');
			$count = $RemindersObj->countReminders(json_encode($POST));
		}
		
		return $count;
	}
	
	/**
	 * @method insDefaultBlockOrders
	 * @param $POST(optional)
	 * @tutorial It will default block sorting orders to DashboardBlocksOrder Table
	 */
	function insDefaultBlockOrders($POST='') {
		
		/**
		 * @input json data
		 */
		if($POST == '') $POST = file_get_contents( 'php://input' );
		$JSON = json_decode($POST, true);
		
		global $USERID, $OrgID;
		
		$params[":OrgID"]		= array($OrgID);
		$params[":UserID"]		= array($USERID);
		$params[":BlockID"]		= array($JSON['BlockID']);
		$params[":BlockType"]	= array($JSON['BlockType']);
		$params[":BlockOrder"]	= array($JSON['BlockOrder']);
		$params[":BlockTitle"]	= array($JSON['BlockTitle']);
		
		$insdefblockorders = "INSERT INTO DashboardBlocksOrder(OrgID, UserID, BlockID, BlockType, BlockOrder, BlockTitle) 
		 					  VALUES(:OrgID, :UserID, :BlockID, :BlockType, :BlockOrder, :BlockTitle)";
		$resdefblockorders = $this->db->getConnection("IRECRUIT")->insert($insdefblockorders, array($params));
	}
	
	/**
	 * @method	updBlockOrders
	 * @param	$JSON
	 */
	function updBlockOrders($JSON) {
		
		global $USERID;
		
		$params[":UserID"]		= array($USERID);
		$params[":BlockID"]		= array($JSON['BlockID']);
		$params[":BlockType"]	= array($JSON['BlockType']);
		$params[":BlockOrder"]	= array($JSON['BlockOrder']);
		
		$upddefblockorders = "UPDATE DashboardBlocksOrder 
							  SET BlockOrder = :BlockOrder  
						   	  WHERE UserID = :UserID 
						   	  AND BlockID = :BlockID 
						   	  AND BlockType = :BlockType";
		$resdefblockorders = $this->db->getConnection("IRECRUIT")->update($upddefblockorders, array($params));
	}
	
	/**
	 * @method	updTopBlocksOrder
	 * @param	$JSON
	 */
	function updTopBlocksOrder($JSON) {
		
		global $USERID;
	
		$params[":UserID"]			= array($USERID);
		$params[":BlockOrder"]		= array($JSON['BlockOrder']);
		$params[":BlockId"]			= array($JSON['BlockId']);
		$params[":BlockPanelColor"]	= array($JSON['BlockPanelColor']);
		
		$upddefblockorders = "UPDATE DashboardTopUserBlocks 
							  SET BlockOrder = :BlockOrder, 
							  BlockPanelColor = :BlockPanelColor 
						   	  WHERE UserID = :UserID AND BlockId = :BlockId";
		$resdefblockorders = $this->db->getConnection("IRECRUIT")->update($upddefblockorders, array($params));
	}
	
	/**
	 * @method	selUserBlockOrders
	 * @param	$POST
	 */
	function selUserBlockOrders($POST='') {
		
		global $USERID, $OrgID;
		
		/**
		 * @input json data
		 */
		if($POST == '') $POST = file_get_contents( 'php://input' );
		$JSON = json_decode($POST, true);
		
		$params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":BlockType"=>$JSON['BlockType']);
		
		$seluserblockorders = "SELECT * FROM DashboardBlocksOrder 
							   WHERE BlockType = :BlockType 
							   AND UserID = :UserID  
							   AND OrgID = :OrgID 
							   ORDER BY BlockOrder ASC";
		$resuserblockorders = $this->db->getConnection("IRECRUIT")->fetchAllAssoc($seluserblockorders, array($params));
		$numuserblockorders = $resuserblockorders['count'];
		
		foreach($resuserblockorders['results'] as $key => $row) {
			$rows[$row['BlockID']] = $row;
		}
		
		return array($rows, $numuserblockorders);
	}
	
	/**
	 * @method	selModuleUserConfig
	 */
	function selModuleUserConfig() {
		
		global $USERID, $OrgID;

		$params   =   array(":UserID" => $USERID);
		
		$selusermoduleconfig = "SELECT * FROM DashboardUserModuleConfig WHERE UserID = :UserID";
		$resusermoduleconfig = $this->db->getConnection("IRECRUIT")->fetchAllAssoc($selusermoduleconfig, array($params));
		
		$rows =   array();
		foreach($resusermoduleconfig['results'] as $key => $row) {
			$rows[$row['ModuleId']][$row['ModuleConfigKey']] = $row['ModuleConfigValue'];
		}
		
		return $rows;
	}
	
	/**
	 * @method	updModuleUserConfigByModuleId
	 * @param	$POST
	 */
	function updModuleUserConfigByModuleId($POST='') {
		
		global $USERID, $OrgID;
		
		
		if($POST['ModuleUpdType'] == 'Regular')
		{
			$params[":UserID"]				= array($USERID);
			$params[":ModuleConfigValue"]	= array(date('m/d/Y'));
			$params[":ModuleId"]			= array($POST['ModuleId']);

			$updmoduserconfig = "UPDATE DashboardUserModuleConfig SET ModuleConfigValue = :ModuleConfigValue  
								 WHERE ModuleConfigKey = 'ApplicationMaxDate' 
								 AND UserID = :UserID AND ModuleId = :ModuleId";
			$resmoduserconfig = $this->db->getConnection("IRECRUIT")->update($updmoduserconfig, array($params));
		}
		
	}
	
	/**
	 * @method	getDashboardModuleFilesById
	 */
	function getDashboardModuleFilesById() {
		
		$selmodulefiles = "SELECT * FROM DashboardModules";
		$resmodulefiles = $this->db->getConnection("IRECRUIT")->fetchAllAssoc($selmodulefiles);
		
		foreach($resmodulefiles['results'] as $key => $row) {
			$DahboardMainModules[$row['ModuleId']] = $row['FileName'];
		}
		return $DahboardMainModules;
	}
	
	/**
	 * @method	updateModulePanelHeadingColor
	 * @param	$ModuleId, $PanelColor
	 */
	function updateModulePanelHeadingColor($ModuleId, $PanelColor) {
		
		global $USERID, $OrgID;
		
		$params[":UserID"]		= array($USERID);
		$params[":PanelColor"]	= array($PanelColor);
		$params[":ModuleId"]	= array($ModuleId);
		
		$upd_module_panel_color = "UPDATE DashboardUserModules SET PanelColor = :PanelColor  
								   WHERE ModuleId = :ModuleId AND UserID = :UserID";
		$res_module_panel_color = $this->db->getConnection("IRECRUIT")->update($upd_module_panel_color, array($params));
		
		return $res_module_panel_color['affected_rows'];
		
	}
	
	/**
	 * @method	updateDashboardAvatarImage
	 * @param	$file_name
	 */
	function updateDashboardAvatarImage($file_name) {
		
		global $USERID, $OrgID;
		
		$params[":UserID"]			= array($USERID);
		$params[":DashboardAvatar"]	= array($file_name);
		
		$upd_avatar = "UPDATE UserPreferences SET DashboardAvatar = :DashboardAvatar  
					   WHERE UserID = :UserID";
		$res_avatar = $this->db->getConnection("IRECRUIT")->update($upd_avatar, array($params));
		
		return $res_avatar['affected_rows'];
	}
	
	/**
	 * @method	updateDashboardModuleStatusById
	 * @param	string $ModuleId
	 * @param	string $Status
	 * @return	int
	 */
	function updateDashboardModuleStatusById($ModuleId, $Status) {
		
		global $USERID;
		
		$params[":UserID"]		= array($USERID);
		$params[":ModuleId"]	= array($ModuleId);
		$params[":Status"]		= array($Status);
		
		$update_module_status = "UPDATE DashboardUserModules SET Status = :Status WHERE UserID = :UserID 
								 AND ModuleId = :ModuleId";
		$result_module_status = $this->db->getConnection("IRECRUIT")->update($update_module_status, array($params));
		
		return $result_module_status['affected_rows'];
	}
	
	/**
	 * @method	getBlockInfoByID
	 * @param	int $blockid
	 * @return	Associative array
	 */
	function getBlockInfoByID($blockid) {
		
		global $USERID, $OrgID;
		
		$params[":BlockId"]	= array($blockid);
		$params[":UserID"]	= array($USERID);
		$params[":OrgID"]	= array($OrgID);
		
		$sel_block_info = "SELECT * FROM DashboardTopUserBlocks 
						   WHERE BlockId = :BlockId  
						   AND UserID = :UserID  
						   AND OrgID = :OrgID";
		$res_block_info = $this->db->getConnection("IRECRUIT")->fetchAssoc($sel_block_info, array($params));
		
		return $res_block_info;
	}
	
}
