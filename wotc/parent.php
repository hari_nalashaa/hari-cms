<?php
include 'Configuration.inc';
G::Obj('GenericQueries')->conn_string =   "WOTC";

if ($_GET['c'] != "") {

  $query = "SELECT CompanyID FROM CRM";
  $query .= " WHERE ParentConfig->'$.ParentLinkShortcode' = :Shortcode";
  $query .= " limit 1";
  $params = array(":Shortcode"=>$_GET['c']);
  $results =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
  $CompanyID = $results[0]['CompanyID'];

} else if ($_GET['d'] != "") {
  $CompanyID = $_GET['d'];
}

if ($CompanyID != "") {

$query          =   "SELECT OrgData.wotcID,";
$query         .=   " CRM.Logo,";
$query         .=   " IF(OrgData.CompanyIdentifier != '' ,OrgData.CompanyIdentifier , CRMFEIN.AKADBA) AS AKADBA,";
$query 	       .=   " OrgData.State AS State,";
$query         .=   " concat(OrgData.Address, ', ', OrgData.City, ', ', OrgData.State, ' ', OrgData.ZipCode) AS LocationAddress";
$query         .=   " FROM CRM";
$query         .=   " JOIN OrgData ON OrgData.CompanyID = CRM.CompanyID";
$query         .=   " JOIN CRMFEIN ON CRMFEIN.CompanyID = CRM.CompanyID AND CRMFEIN.EIN1 = OrgData.EIN1 AND CRMFEIN.EIN2 = OrgData.EIN2";
$query         .=   " WHERE OrgData.CompanyID = :CompanyID";
$query         .=   " AND OrgData.Active = 'Y'";
$query         .=   " ORDER BY OrgData.State, AKADBA";
$params = array(":CompanyID"=>$CompanyID);
$ORGS =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));


    
    // Get States List
    $AddressObj->conn_string = "WOTC";
    $ADDRESSES_RES =   $AddressObj->getAddressStateList();
    $ADDRESSES     =   $ADDRESSES_RES['results'];
    
    $STATES = array();
    foreach ($ADDRESSES as $ADD) {
        $STATES[$ADD['Abbr']] = $ADD['Description'];
    } // end foreach
    
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Work Opportunity Tax Credit - <?php echo $ParentCompany; ?></title>
            <!-- Favicons-->
            <link rel="shortcut icon" href="<?php echo WOTC_HOME; ?>images/fav.png?v=3">
            <link rel="icon" href="<?php echo WOTC_HOME; ?>images/fav.png?v=3">
            <link rel="apple-touch-icon" href="<?php echo WOTC_HOME; ?>images/fav.png?v=3">
            <link rel="apple-touch-icon-precomposed" href="icon">
            <link rel="stylesheet" href="<?php echo WOTC_HOME; ?>css/wotc.css?v=2.0">
        </head>
    <body>
	<div style="float: left;">
    <?php
    if ($ORGS[0]['Logo'] != "") {
        echo "<img style=\"margin: 0px 30px 0px 0px;max-height:100px;\" src=\"" . WOTC_HOME . "images/logos/" . $ORGS[0]['Logo'] . "\" border=\"0\">";
    }
    ?>
    </div>
	<div style="line-height: 180%; padding-top: 10px;">
		<b style="font-size: 18pt;"><?php echo $ORGS[0]['CompanyName']; ?></b><br> <b
			style="font-size: 16pt; color: #ba3f3f;">Work Opportunity Tax Credit</b><br>
		<b style="font-size: 14pt;">Please select your location to complete
			the WOTC form.</b><br>
	</div>
	<div style="clear: both;"></div>
	<div style="margin-left: 80px; line-height: 180%; font-size: 12pt; margin-top: 10px;">
    <?php
    foreach ($ORGS as $ORG) {
        
        if ($state != $ORG['State']) {
            echo "<div style=\"font-size:14pt;font-weight:bold;margin-top:10px;\">" . $STATES[$ORG['State']] . "</div>\n";
        }

          echo "<div style=\"margin-left:20px;\">";
          echo "<a href=\"" . WOTC_HOME . "admin.php?wotcID=" . $ORG['wotcID'] . "\" target=\"_blank\">";
          echo $ORG['AKADBA'];
          echo "</a>\n";
          echo " - " . $ORG['LocationAddress'];
          echo "</div>";
        
        $state = $ORG['State'];
    } // end foreach
    
    echo "</div>\n";
    echo "<div style=\"text-align:center;margin-top:30px;margin-bottom:40px;\"><b style=\"font-size:14pt;\">Thank you for your participation.</b></div>";
    echo "</body>\n";
    echo "</html>\n";

} // end if
?>
