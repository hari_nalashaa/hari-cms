<?php
session_start();

include 'Configuration.inc';

//Skip fields on duplicate key
$skip       =   array('wotcID', 'ApplicationID');
//Insert Update Processing
$info       =   array(
                    'wotcID'        =>  $_POST['wotcID'],
                    'ApplicationID' =>  $_POST['ApplicationID'],
                    'OfferDate'     =>  date("Y-m-d",  strtotime($_POST['offerdatep'])),
                    'HiredDate'     =>  date("Y-m-d",  strtotime($_POST['hireddatep'])),
                    'StartDate'     =>  date("Y-m-d",  strtotime($_POST['startdatep'])),
                    'StartingWage'  =>  $_POST['startingwagep'],
                    'Position'      =>  $_POST['positionp'],
                    'Received'      =>  "NOW()",
                    'LastUpdated'   =>  "NOW()"
                );
G::Obj('WOTCProcessing')->insUpdProcessing($info, $skip);

G::Obj('WOTCPdf')->createPDFs($_POST['wotcID'],$_POST['ApplicationID']);

echo G::Obj('WOTC')->wotcHeader($WOTCID);

echo "<div class=\"row\">\n";
echo "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";

echo "No further information is needed.";

echo "</div>\n";
echo "</div>\n";

echo G::Obj('WOTC')->wotcFooter($WOTCID);
?>