<?php
include '../Configuration.inc';

if ($_POST['DOB']) {
    $params       =   array(":ckdate"=>date("Y-m-d", strtotime($_POST['DOB'])));
    $dates_list   =   $MysqlHelperObj->getDatesList("DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), :ckdate)+1), \"%Y\")+0 AS age", array($params));
    echo 'Age: ' . $dates_list['age'];
}
?>