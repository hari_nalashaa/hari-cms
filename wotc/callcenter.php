<?php
include 'Configuration.inc';

echo G::Obj('WOTC')->wotcHeader('');

$ACTIVESTATES = G::Obj('WOTCcrm')->getCallcenterActiveStates();

if ($_POST['process'] == 'Y') {
  $ORGS = G::Obj('WOTCcrm')->getCallcenterCompanies($_POST['State'],$_POST['searchwords']);
}

    echo '<div style="margin-left:20px;">';
    echo 'Which company has asked you to furnish information: ';
    echo '(Company Name, Address, City, or Zip Code)';
    echo '<br><br>';

    echo '<form method="POST" action="callcenter.php">';
    echo 'State: <select name="State" onChange="submit()">' . "\n";
    echo '<option value="">ALL</option>' . "\n";

    foreach ($ACTIVESTATES as $S) {
        
        echo "<option value=\"" . $S['Abbr'] . "\"";
        if ($_POST['State'] == $S['Abbr']) {
            echo " selected";
        }
        echo ">" . $S['Description'] . "</option>\"";

    } // end ORGS

    echo "</select>\n";

    echo "<input type=\"text\" name=\"searchwords\" value=\"" . $_POST['searchwords'] . "\" size=\"20\" maxlength=\"40\">";
    echo "&nbsp;&nbsp;";
    echo "<input type=\"hidden\" name=\"process\" value=\"Y\">\n";
    echo "<input type=\"submit\" value=\"Filter List\">\n";
    echo "</form>\n";
    echo "</div>";


    if (count($ORGS) > 0) {
        echo "<div style=\"margin-left:40px;margin-top:10px;min-width:400px;\">";
        echo "Results:<br>\n";
        echo "<div style=\"margin-left:20px;line-height:240%;font-size:13px;\">";

      foreach ($ORGS as $O) {

       if ($O['AKADBA'] != "") {	      

	if ($O['Active'] == "Y") {
          echo "<a href=\"admin.php?wotcID=";
          echo $O['wotcID'];
          echo "&Initiated=callcenter\">";
	}
	echo $O['State'] . " - ";
        echo $O['AKADBA'] . ", " . $O['Address'] . ", " . $O['City'] . ", " . $O['ZipCode'];

	if ($O['Active'] == "Y") {
          echo "</a>";
	}

	if ($O['Active'] == "N") {
	  echo "&nbsp;&nbsp;";
	  echo "<span style=\"color:red;font-style:italic;\">";
	  echo "(This company is currently not taking applications.)";
	  echo "</span>";
	}
        
	echo "<br>\n";

       } // end CompanyName

      } // end foreach
    
        echo "</div>\n";
        echo "</div>\n";
        
        echo "<div style=\"margin: 20px 0 40px 60px;line-height:250%;width:610px;background-color:#eeeeee;padding:10px;text-align:center;\">";
        echo "<form method=\"POST\" action=\"admin.php\">\n";
        echo "<span style=\"font-size:12pt;color:red;\">Cannot Find a Company?</span><br>";
        echo "Please indicate here any information about the company you cannot find.<br>";
        echo "<textarea name=\"notes\" cols=\"60\" rows=\"6\"></textarea><br>\n";
        echo "<input type=\"hidden\" name=\"state\" value=\"" . $_POST['State'] . "\">\n";
        echo "<input type=\"hidden\" name=\"wotcID\" value=\"callcenter\">\n";
        echo "<input type=\"hidden\" name=\"Initiated\" value=\"callcenter\">\n";
	echo '<div style="margin-top:10px;">';
        echo "<input type=\"submit\" value=\"Go\">\n";
	echo '</div>';
        echo "</form>\n";
        echo "</div>\n";

    } // end count
?>

<?php
echo G::Obj('WOTC')->wotcFooter('');
?>
