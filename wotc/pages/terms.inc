<p><b>Terms and Conditions of Website Usage</b></p>

<p>Welcome to Cost Management Services, LLC (CMS). If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with the privacy policy listed govern Cost Management Services, LLC (CMS) relationship with you in relation to this website. The term "CMS" or "us" or "we" refers to the owner of the website whose registered office is 321 Main Street, Farmington, CT  06032. The term "you" refers to the user or viewer of this website. The use of this website is subject to the following terms of use:</p>
<ul>

<li><p>The content of the pages of this website is for your general information and use only. It is subject to change without notice.</p>

<li><p>Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</p>

<li><p>Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable.  It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</p>

<li><p>This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</p>

<li><p>All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</p>

<li><p>Unauthorized use of this website may give to a claim for damages and/or be a criminal offence.</p>

<li><p>From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</p>

<li><p>Your use of this website and any dispute arising out of such use of the website is subject to the laws of the State of Connecticut, US.</p>

</ul>

<br>

<p><b>Copyright</b></p>

<p>This website content is copyright of Cost Management Services, LLC. Any redistribution or reproduction of part or all of the contents in any form is prohibited other than the following:</p>
<ul>
<li><p>you may print or download to a local hard disk extracts for your personal and non-commercial use only</p>

<li><p>you may copy the content to individual third parties for their personal use, but only if you acknowledge the website as the source of the material.</p>

<li><p> You may not, except with my express written permission, distribute or commercially exploit the content.
Nor may you transmit it or store it in any other website or other form of electronic retrieval system.</p>
</p>
</ul>
 
