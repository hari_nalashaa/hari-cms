<?php
if (isset($_SESSION['post_data'])) {
    $DATA = $_SESSION['post_data'];
    unset($_SESSION['post_data']);

    echo "<div class=\"row\" style=\"color:red;border:.5px solid #000;background-color:#eeeeee;border-radius:5px;text-align:center;\">\n";
    echo "<div class=\"col-lg-12 col-md-12 col-sm-12\">";
    echo $DATA['ERROR'] . "\n";
    echo "</div>\n";
    echo "</div>\n";

    $APPDATA = $DATA;
}

if (($_POST['Initiated'] == "cms") && (sizeof($DATA) == 0)) {
    $APPDATA['SWA2']       =   "N";
    $APPDATA['SWA3']       =   "N";
    $APPDATA['SWA4']       =   "N";
    $APPDATA['Veteran']    =   "N";
    $APPDATA['SNAP']       =   "N";
    $APPDATA['TANF']       =   "N";
    $APPDATA['SSI']        =   "N";
    $APPDATA['Felony']     =   "N";
    $APPDATA['Worked']     =   "N";
    $APPDATA['LTU']        =   "N";
}

//Get Organization Information
$org_info           =   G::Obj('WOTCOrganization')->getOrganizationInfo("*", $WOTCID);
$Nonprofit          =   $org_info['Nonprofit'];
$eSignature         =   $org_info['eSignature'];

$IRECRUITLOOKUP = "N";
if (($_POST['lname']) && ($_POST['iRecruitOrgID']) && ($_POST['IrecruitApplicationID'])) {
    $IRECRUITDATA                =   G::Obj('WOTCApp')->getAppData($_POST['iRecruitOrgID'], $_POST['IrecruitApplicationID'], $_POST['lname']);

    $APPDATA['FirstName']       =   $IRECRUITDATA['first'];
    $APPDATA['LastName']        =   $IRECRUITDATA['last'];
    $APPDATA['MiddleName']      =   $IRECRUITDATA['middle'];
    $APPDATA['Social1']         =   $IRECRUITDATA['social1'];
    $APPDATA['Social2']         =   $IRECRUITDATA['social2'];
    $APPDATA['Social3']         =   $IRECRUITDATA['social3'];
    $APPDATA['Address']         =   $IRECRUITDATA['address'];
    $APPDATA['City']            =   $IRECRUITDATA['city'];
    $APPDATA['State']           =   $IRECRUITDATA['state'];
    $APPDATA['ZipCode']         =   $IRECRUITDATA['zip'];
    $APPDATA['County']          =   $IRECRUITDATA['county'];

    $IRECRUITLOOKUP         	=   "Y";
}

if ($_REQUEST['first'] != "")  {

    $APPDATA = $_REQUEST;

    $APPDATA['FirstName']       =   $_REQUEST['first'];
    $APPDATA['LastName']        =   $_REQUEST['last'];
    $APPDATA['MiddleName']      =   $_REQUEST['middle'];
    $APPDATA['Social1']         =   $_REQUEST['social1'];
    $APPDATA['Social2']         =   $_REQUEST['social2'];
    $APPDATA['Social3']         =   $_REQUEST['social3'];
    $APPDATA['Address']         =   $_REQUEST['address'];
    $APPDATA['City']            =   $_REQUEST['city'];
    $APPDATA['State']           =   $_REQUEST['state'];
    $APPDATA['ZipCode']         =   $_REQUEST['zip'];
    $APPDATA['County']          =   $_REQUEST['county'];

}

//Get States List
$AddressObj->conn_string    =   "WOTC";
$AS_RES                     =   G::Obj('Address')->getAddressStateList();
$AS                         =   $AS_RES['results'];


$where_info                 =   array("WotcID = :WotcID", "WotcFormID = :WotcFormID", "Active = :Active");
$params_info                =   array(":WotcID"=>"MASTER", ":WotcFormID"=>$WotcFormID, ":Active"=>'Y');
$wotc_form_ques_list        =   G::Obj('WOTCFormQuestions')->getWotcFormQuestionsInfo("*", $where_info, "", "QuestionOrder ASC", array($params_info));
$wotc_form_ques             =   $wotc_form_ques_list['results'];

//Set APPDATA array in ApplicationForm instance
G::Obj('ApplicationForm')->APPDATA =	$APPDATA;
?>
