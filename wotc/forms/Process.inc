<?php
//Update ApplicationID
$set_info       =   array("ApplicationID = LAST_INSERT_ID(ApplicationID+1)");
$where_info     =   array("wotcID = :wotcID");
$params         =   array(":wotcID"=>$WOTCID);
G::Obj('WOTCOrganization')->updOrgDataInfo($set_info, $where_info, array($params));

G::Obj('GenericQueries')->conn_string =   "WOTC";
$query          =   "SELECT LAST_INSERT_ID() AS LAST_INSERT_ID";
$last_ins_info  =   G::Obj('GenericQueries')->getRowInfoByQuery($query);
$ID             =   $last_ins_info['LAST_INSERT_ID'];
$ApplicationID  =   'W' . substr('0000000' . $ID, - 8);

$params         =   array(":ckdate"=>date("Y-m-d", strtotime($_POST['DOB_Date'])));
$query          =   "SELECT DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), :ckdate)+1), '%Y')+0 AS AGE";
$age_info       =   G::Obj('GenericQueries')->getRowInfoByQuery($query, array($params));
$age            =   $age_info["AGE"];

$org_info   	=   G::Obj('WOTCOrganization')->getOrganizationInfo("WotcFormID", $WOTCID);
$WotcFormID 	=   $org_info['WotcFormID'];

if (($age >= 18) && ($age <= 39)) {
    $dob = "Y";
} else {
    $dob = "N";
    $_POST['SNAP'] = "N";
}

if ($_POST['Felony'] == "N") {
    $_POST['Felony2'] = "";
    $_POST['FelonyConviction_Date'] = "";
    $_POST['FelonyRelease_Date'] = "";
}

if ($_POST['Worked'] == "N") {
    $_POST['Worked_Date'] = "";
}

// callcenter verfication of ssn
if ($_POST['verify'] != "") {
    $_POST['captcha'] = $_POST['verify'];
}

//Insert Applicant Data
//Consider database column as a $info array key
//It will automatically build the insert query based on array
$info['wotcID']         =   $WOTCID;
$info['ApplicationID']  =   $ApplicationID;
$info['WotcFormID']     =   $WotcFormID;
$info['EntryDate']      =   "NOW()";
$info['FirstName']      =   $_POST['FirstName'];
$info['MiddleName']     =   $_POST['MiddleName'];

$info['LastName']       =   $_POST['LastName'];
$info['Social1']        =   $_POST['Social1'];
$info['Social2']        =   $_POST['Social2'];
$info['Social3']        =   $_POST['Social3'];

$info['Address']        =   $_POST['Address'];
$info['City']           =   $_POST['City'];
$info['State']          =   $_POST['State'];
$info['ZipCode']        =   $_POST['ZipCode'];
$info['County']         =   $_POST['County'];

$info['DOB']            =   $dob;
$info['DOB_Date']       =   $_POST['DOB_Date'];
$info['TANF']           =   $_POST['TANF'];
$info['TANF_Recipient'] =   $_POST['TANF_Recipient'];
$info['TANF_City']      =   $_POST['TANF_City'];
$info['TANF_State']     =   $_POST['TANF_State'];
$info['SNAP']           =   $_POST['SNAP'];
$info['SNAP_Recipient'] =   $_POST['SNAP_Recipient'];
$info['SNAP_City']      =   $_POST['SNAP_City'];
$info['SNAP_State']     =   $_POST['SNAP_State'];

$info['SSI']            =   $_POST['SSI'];

$info['Felony']         =   $_POST['Felony'];
$info['Felony2']        =   $_POST['Felony2'];
$info['FelonyConviction_Date']  =   $_POST['FelonyConviction_Date'];
$info['FelonyRelease_Date']     =   $_POST['FelonyRelease_Date'];

$info['Veteran']        =   $_POST['Veteran'];
$info['Veteran2']       =   $_POST['Veteran2'];
$info['Veteran3']       =   $_POST['Veteran3'];
$info['Veteran4']       =   $_POST['Veteran4'];
$info['Veteran5']       =   $_POST['Veteran5'];
$info['Veteran6']       =   $_POST['Veteran6'];
$info['VET_Recipient']  =   $_POST['VET_Recipient'];
$info['VET_City']       =   $_POST['VET_City'];
$info['VET_State']      =   $_POST['VET_State'];
$info['Veteran7']      	=   $_POST['Veteran7'];

$info['SWA2']           =   $_POST['SWA2'];
$info['SWA3']           =   $_POST['SWA3'];
$info['SWA4']           =   $_POST['SWA4'];

$info['Worked']         =   $_POST['Worked'];
$info['Worked_Date']    =   $_POST['Worked_Date'];
$info['LTU']            =   $_POST['LTU'];
$info['LTU_State']      =   $_POST['LTU_State'];
$info['iRecruitID']     =   $_POST['irecruitlookup'];
$info['Initiated']      =   $_POST['Initiated'];

$info['Processed']      =   'N';
$info['Signature']      =   $_POST['captcha'];
$info['NativeAmerican']	=   $_POST['NativeAmerican'];

$info['Attributes']     =   $_POST['Attributes'];
$info['ApplicationStatus']	=   $_POST['ApplicationStatus'];

G::Obj('WOTCApplicantData')->insApplicantDataInfo($info);

// Insert complete, now read data and analyse to finish processing
list ($PROCESSOR, $ORG, $APPDATA, $PRS, $Signature, $Date, $One, $Two, $Three, $Four, $Five, $Six, $Seven, $workedA, $workedAN, $ck12A, $ck12AN, $ck13A, $ck13AN, $ck13B, $ck13BN, $ck13C, $ck13CN, $ck13D, $ck13DN, $ck13E, $ck13EN, $ck14A, $ck14AN, $ck14B, $ck14BN, $ck15A, $ck15AN, $ck15B, $ck15BN, $ck15C, $ck15CN, $ck16A, $ck16AN, $ck16B, $ck16BN, $ck16C, $ck16CN, $ck16D, $ck16DN, $ck17A, $ck17AN, $ck18A, $ck18AN, $ck19A, $ck19AN, $ck20A, $ck20AN, $ck21A, $ck21AN, $ck22A, $ck22AN, $ck23A, $ck23AN, $items) = G::Obj('WOTCPdf')->getData($WOTCID, $ApplicationID);

$set_info   =   array("DocumentsNeeded = :DocumentsNeeded");
$where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
$params     =   array(":DocumentsNeeded"=>$items, ":wotcID"=>$WOTCID, ':ApplicationID'=>$ApplicationID);
G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));

// preliminary approval
if (($One == "Yes") 
	|| ($Two == "Yes") 
	|| ($Three == "Yes") 
	|| ($Four == "Yes") 
	|| ($Five == "Yes") 
	|| ($Six == "Yes") 
	|| ($Seven == "Yes")) {

    $set_info   =   array("Processed = 'R'", "Processed_Date = NOW()");
    $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
    $params     =   array(":wotcID"=>$WOTCID, ':ApplicationID'=>$ApplicationID);
    G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));

} // end if

// New 2/26/2013
$RCEZType       =   "";
$RCEZName       =   "";
$where_info     =   array("ZipCode = :ZipCode");
$params_info    =   array(':ZipCode'=>$_POST['ZipCode']);
$rrc_ez_info    =   G::Obj('WOTCRRC_EZ_2013')->getRRC_EZ_2013Info("*", $where_info, "ZipCode", array($params_info));
$ZC             =   $rrc_ez_info['results'];
$ENThit         =   sizeof($ZC);

foreach ($ZC as $RCE) {
    $RCEZType = $RCE['ZoneType'];
    if ($RCE['ZoneType'] == 'Rural Renewal County') {
        $RCEZName = $RCE['County'] . " - " . $RCE['CityState'];
    } else {
        $RCEZName = $RCE['ZoneName'];
    }
} // end foreach

if ($ENThit > 0) {
    $set_info   =   array("EnterpriseZone = 'Y'", "Processed_Date = NOW()", "RCEZName = :RCEZName", "RCEZType = :RCEZType");
    $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
    $params     =   array(":wotcID"=>$WOTCID, ":ApplicationID"=>$ApplicationID, ":RCEZName"=>$RCEZName, ":RCEZType"=>$RCEZType);
    G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
} // end if
?>
