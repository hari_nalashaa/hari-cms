<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Language" content="en-us">

<title>Work Opportunity Tax Credits Calculator</title>

<style>
#bodylinks
a { font-size: 10px; color: #3A6DB8; font-weight: bold; }
a:visited { color: #74A8E6;     text-decoration: none; }
a:hover { color: #A8B819;       text-decoration: underline; }
a:active {color: #A8B819; text-decoration: none; }
</style>

</head>

<body topmargin="0" leftmargin="0" bottommargin="0" bgcolor="#FFFFFF" link="#003366" vlink="#339966" alink="#FF6600" tex
t="#333333">

<script language="JAVASCRIPT">
function calcWotr(form) {
   form.qualifyingnewhires.value = Math.round(form.qualifyingratio.value/100 * form.newhires.value);
   var taxcredit = form.qualifyingnewhires.value * 2400;
   document.getElementById("ratio").innerHTML = form.qualifyingratio.value + "%";
   document.getElementById("qualifyingnewhires").innerHTML = Math.round(form.qualifyingratio.value/100 * form.newhires.value);
   document.getElementById("potentialtaxcredit").innerHTML = "$" + formatDollar(taxcredit);
}
function resetWotr(form) {
   form.newhires.value = "50";
   calcWotr(form);
}
function formatDollar(num) {
    var p = num.toFixed(2).split(".");
    return ["", p[0].split("").reverse().reduce(function(acc, num, i) {
        return num + (i && !(i % 3) ? "," : "") + acc;
    }, "."), p[1]].join("");
}
</script>

<form name="wotr" method="POST">

<div style="text-align:center;padding:19px 0 0 0;">

<table align="center" border="0" cellspacing="5" cellpadding="5">
<tbody>
<tr><td colspan="2" bgcolor="#3A6DB8">
<div style="font-family:Arial;font-weight:bold;color:#FFFFFF;font-size:16pt;padding-left:4px;">WOTC Tax Savings Calculator</div>
</td></tr>

<?php
include 'Configuration.inc';

$INDUSTRIES = G::Obj('WOTCAdmin')->getIndustries();
echo '<tr>';
echo '<td colspan="2"><font style="font-family:Arial;font-size:13pt;">Industry:&nbsp;&nbsp;</font>';

echo '<select name="qualifyingratio" onChange="calcWotr(wotr)" style="font-size:14pt;">';
foreach ($INDUSTRIES as $ind) {
   echo "<option value=\"" . $ind['QualifyingRatio'] . "\"";
   echo ">" . $ind['Industry'] . "</option>\n";
} // end foreach
echo '</select>';

echo '</td>';
echo '</tr>';
?>

<tr>
<td><font style="font-family:Arial">Number of New Hires per year:</font></td>
<td><font style="font-family:Arial"><input type="text" name="newhires" size="7" maxlength="5" oninput="calcWotr(wotr)" style="border: 1px solid #125B92; padding-left: 4px; padding-right: 4px; padding-top: 1px; padding-bottom: 1px; font-family:Arial; font-size:14pt"></font></td>
</tr>

<tr>
<td><font style="font-family:Arial">Qualifying Ratio:</font></td>
<td>
<div id="ratio" style="font-size:14pt;font-family:Arial;"></div>
</td>
</tr>

<tr>
<td><font style="font-family:Arial">Qualifying New Hires:</font></td>
<td>
<div id="qualifyingnewhires" style="font-size:14pt;font-family:Arial;"></div>
<input type="hidden" name="qualifyingnewhires" value="">
</td>
</tr>

<tr>
<td><font face="Arial">Average Tax Credit: </font></td>
<td><div style="font-size:14pt;font-family:Arial;">$2,400</div></td>
</tr>

<tr>
<td><font style="font-family:Arial;color:#339966;font-size:16pt;font-weight:bold;">Your Potential Tax Credit:</font></td>
<td>
<div id="potentialtaxcredit" style="font-family:Arial;color:#339966;font-size:16pt;font-weight:bold;"></div>
</td>
</tr>

</tbody>
</table>

<div style="font-family:Arial;font-size:10pt;">
<i>Your companies actual results may vary.</i><br>
<a href="http://www.cmswotc.com/" target="_top">www.cmswotc.com</a>
</div>

</div>

<script type="text/javascript">
resetWotr(wotr);
</script>

</form>
</body>
</html>
