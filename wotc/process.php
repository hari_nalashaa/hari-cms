<?php
session_start();

include 'Configuration.inc';

$WOTCID = G::Obj('WOTCApp')->validatewotcID();

// Validate entries
if (isset($_POST["captcha"])) {
    if ($_SESSION["captcha"] != $_POST["captcha"]) {
        $_POST['ERROR'] = "The 6 symbols representing your signature could not be verified.";
        $_SESSION['post_data'] = $_POST;
        header('Location: ' . WOTC_HOME . 'form.php');
        exit();
    }
} // end captcha session
  
echo G::Obj('WOTC')->wotcHeader($WOTCID);

echo G::Obj('WOTCApp')->processThankYou($_REQUEST['WOTCID'], $_REQUEST['ApplicationID']);

//Get OrgInfo by WOTCID
$columns		=	"iRecruitOrgID, iRecruitMultiOrgID, WotcFormID";
$wotc_org_info	=   G::Obj('WOTCOrganization')->getOrganizationInfo($columns, $_REQUEST['WOTCID']);
$WotcFormID 	=   $wotc_org_info['WotcFormID'];

//Update Qualifies Column
$wotc_app_details =	G::Obj('WOTCIrecruitApplications')->getWotcApplicationByApplicationID("*", $_REQUEST['WOTCID'], $_REQUEST['IrecruitApplicationID'], $_REQUEST['IrecruitRequestID'], $_REQUEST['ApplicationID']);
$wotc_qualifies	=	G::Obj('WOTCApp')->getApplicationInfo($_REQUEST['WOTCID'], $_REQUEST['ApplicationID']);

$set_info		=	array("Qualifies = :Qualifies");
$where_info		=	array(
							"OrgID						=	:OrgID",
							"WotcFormID					=	:WotcFormID",
							"wotcID						=	:wotcID",
							"IrecruitApplicationID		=	:IrecruitApplicationID",
							"IrecruitRequestID			=	:IrecruitRequestID",
							"ApplicationID				=	:ApplicationID"
					);
$params			=	array(
							":Qualifies"				=>	$wotc_qualifies['Qualifies'],
							":OrgID"					=>	$wotc_org_info['iRecruitOrgID'],
							":WotcFormID"				=>	$wotc_app_details['WotcFormID'],
							":wotcID"					=>	$wotc_app_details['wotcID'],
							":IrecruitApplicationID"	=>	$wotc_app_details['IrecruitApplicationID'],
							":IrecruitRequestID"		=>	$wotc_app_details['IrecruitRequestID'],
							":ApplicationID"			=>	$wotc_app_details['ApplicationID']
					);

//Update wotc application qualifies information
G::Obj('WOTCIrecruitApplications')->updWotcApplicationInfo($set_info, $where_info, array($params));

echo G::Obj('WOTC')->wotcFooter($WOTCID);
?>
