<?php 
include 'Configuration.inc';

$WOTCID         =   G::Obj('WOTCApp')->validatewotcID();

if (isset($_GET['ln'])) {
   setcookie ( "LN", $_GET['ln'], time () + 7200, "/" );
   header('Location: ' . WOTC_HOME . 'admin.php?wotcID=' . $WOTCID);
}


echo G::Obj('WOTC')->wotcHeader($WOTCID);

if ($_REQUEST['pg'] == "") {

  if ($_POST['wotcID'] == "callcenter") {

    $state  =   !empty($_POST['state']) ? $_POST['state'] : '';
    $notes  =   !empty($_POST['notes']) ? $_POST['notes'] : '';
    G::Obj('WOTCNotFound')->insNotFound($state, $notes);
    
    $to = "lschneider@cmshris.com";
    $subject = "WOTC Company Not Found";

    $message = "\n" . date( 'Y-m-d h:i:s A') . "\n" . $_POST['state'] . " - " . $_POST['notes'] . "\n\n";

    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
    
    // Set who the message is to be sent to
    $PHPMailerObj->addAddress ( $to );
    // Set the subject line
    $PHPMailerObj->Subject = $subject;
    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );
    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/plain';
    //Send email
    $PHPMailerObj->send ();
  }

  echo G::Obj('WOTCApp')->letterPage($WOTCID);

}

if ($_REQUEST['pg'] == "terms") {
     include 'pages/terms.inc';
}

if ($_REQUEST['pg'] == "policy") {
     include 'pages/policy.inc';
}

echo G::Obj('WOTC')->wotcFooter($WOTCID);
?>
