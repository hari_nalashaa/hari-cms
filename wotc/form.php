<?php
session_start();

include 'Configuration.inc';

//Validate WOTC ID
$WOTCID     =   G::Obj('WOTCApp')->validatewotcID();

//Get OrgInfo by WOTCID
$columns		=	"iRecruitOrgID, iRecruitMultiOrgID, WotcFormID";
$wotc_org_info	=   G::Obj('WOTCOrganization')->getOrganizationInfo($columns, $WOTCID);
$WotcFormID 	=   $wotc_org_info['WotcFormID'];

if ($_COOKIE['LN'] == "sp") {
    $WotcFormID .= "SP";
}

if ($_COOKIE['LN'] == "sp") {
          $language="SPANISH";
        } else {
          $language="MASTER";
}

$IRF = G::Obj('WOTCDisplayText')->getiRecruitForm($language);

$IrecruitApplicationID	= '';

if(isset($_REQUEST['IrecruitApplicationID'])
	&& $_REQUEST['IrecruitApplicationID'] != "") {
	$IrecruitApplicationID	= $_REQUEST['IrecruitApplicationID'];
}

if($IrecruitApplicationID != "") {
	//Get Irecruit Application Information
	$irecruit_app_info = G::Obj('Applications')->getApplicationInfoByApplicationID("RequestID", $wotc_org_info['iRecruitOrgID'], $IrecruitApplicationID);
	
	if(isset($irecruit_app_info['RequestID'])
		&& $irecruit_app_info['RequestID'] != "") {
		$IrecruitRequestID	= $irecruit_app_info['RequestID'];
	}
}

if(isset($_POST['btnSubmit'])) {
    
    $APPDATA    =   $_POST;
    G::Obj('WOTCValidateForm')->FORMDATA['REQUEST'] =   $_REQUEST;
    G::Obj('ApplicationForm')->APPDATA              =   $_POST;
    
    $ERRORS     =  G::Obj('WOTCValidateForm')->validateWotcForm("MASTER", $WotcFormID); //Replace it with WotcID

    if($_POST['Initiated'] != "callcenter" 
		&& $_POST['captcha'] != $_SESSION['captcha']) {
        $ERRORS['captcha']  =   "Captcha";
    }
	
    if(count($ERRORS) == 0) {
    	
    	// Check duplicate entry
    	G::Obj('WOTCApp')->checkDuplicate();
    	 
        require_once WOTC_DIR . 'forms/Process.inc';

        
        if($_REQUEST['IrecruitApplicationID'] != ""
			&& $_REQUEST['IrecruitRequestID'] != "") {
        	
        	//Insert wotc application header information
        	$info	=	array(
        			"OrgID"					=>	$wotc_org_info['iRecruitOrgID'],
        			"MultiOrgID"			=>	$wotc_org_info['iRecruitMultiOrgID'],
        			"WotcFormID"			=>	$wotc_org_info['WotcFormID'],
        			"wotcID"				=>	$WOTCID,
        			"UserID"				=>	'',
        			"IrecruitApplicationID"	=>	$IrecruitApplicationID,
        			"IrecruitRequestID"		=>	$IrecruitRequestID,
        			"ApplicationID"			=>	$ApplicationID,
        			"CreatedDateTime"		=>	"NOW()",
        			"Source"				=>	"WOTC"
        	);
        	G::Obj('WOTCIrecruitApplications')->insWotcApplicationInfo($info);
        }
        
        
		if ($_POST['Initiated'] == "callcenter") {
		  echo G::Obj('WOTC')->wotcHeader($WOTCID);
		  echo G::Obj('WOTCApp')->processThankYou($WOTCID, $ApplicationID);
		  
		  $wotc_app_details =	G::Obj('WOTCIrecruitApplications')->getWotcApplicationByApplicationID("*", $WOTCID, $IrecruitApplicationID, $IrecruitRequestID, $ApplicationID);
		  $wotc_qualifies	=	G::Obj('WOTCApp')->getApplicationInfo($WOTCID, $ApplicationID);
		  
		  $set_info		=	array("Qualifies = :Qualifies");
		  $where_info	=	array(
							  		"OrgID						=	:OrgID",
							  		"WotcFormID					=	:WotcFormID",
							  		"wotcID						=	:wotcID",
							  		"IrecruitApplicationID		=	:IrecruitApplicationID",
							  		"IrecruitRequestID			=	:IrecruitRequestID",
							  		"ApplicationID				=	:ApplicationID"
							  );
		  $params		=	array(
							  		":Qualifies"				=>	$wotc_qualifies['Qualifies'],
							  		":OrgID"					=>	$wotc_org_info['iRecruitOrgID'],
							  		":WotcFormID"				=>	$WotcFormID,
							  		":wotcID"					=>	$WOTCID,
							  		":IrecruitApplicationID"	=>	$IrecruitApplicationID,
							  		":IrecruitRequestID"		=>	$IrecruitRequestID,
							  		":ApplicationID"			=>	$ApplicationID
							  );
		  
		  //Update wotc application qualifies information
		  G::Obj('WOTCIrecruitApplications')->updWotcApplicationInfo($set_info, $where_info, array($params));
		  
		  echo G::Obj('WOTC')->wotcFooter($WOTCID);
		} else {
	      header('Location: ' . WOTC_HOME . 'process.php?WOTCID='.$WOTCID.'&ApplicationID='.$ApplicationID.'&IrecruitApplicationID='.$IrecruitApplicationID.'&IrecruitRequestID='.$IrecruitRequestID);
		}
		
        exit();
        
    }        
}

echo G::Obj('WOTC')->wotcHeader($WOTCID);

require_once WOTC_DIR . 'forms/Form.inc';
?>
<form method="POST" name="govform">


<?php
$DATE_IDS               =   "";
$child_que_list         =   array();
$que_types_list         =   array();
$special_ques           =   array('verify', 'captcha', 'declaration');
$ERROR_KEYS             =   array_keys($ERRORS);

for($wfq = 0; $wfq < $wotc_form_ques_list['count']; $wfq++) {

    if($ques_info['QuestionTypeID'] == 17) {
        $DATE_IDS       .=  "#".$ques_info['QuestionID'].", ";
    }
    
    $QuestionID         =   $wotc_form_ques[$wfq]['QuestionID'];
    $QuestionTypeID     =   $wotc_form_ques[$wfq]['QuestionTypeID'];
    
    $ques_info          =   $wotc_form_ques[$wfq];
    $ques_info['name']  =   $wotc_form_ques[$wfq]['QuestionID'];
    $que_types_list[$wotc_form_ques[$wfq]['QuestionID']]   =   $wotc_form_ques[$wfq]['QuestionTypeID'];
    
    if($wotc_form_ques[$wfq]['ChildQuestionsInfo'] != "") {
        $child_que_list[$wotc_form_ques[$wfq]['QuestionID']]   =   json_decode($wotc_form_ques[$wfq]['ChildQuestionsInfo'], true);
    }
    
    //Set errors related css
    $requiredck =   '';
    $highlight  =   'yellow';
    
    if(in_array($QuestionID, $ERROR_KEYS)) {
        $requiredck = ' style="background-color:' . $highlight . ';"';
    }
    
    $lbl_class = ' class="question_name"';
    if($QuestionTypeID == 99 || $QuestionTypeID == 5) {
        $lbl_class = " class='question_name labels_auto'";
    }
    
    $ques_info ['lbl_class']  =   $lbl_class;
    $ques_info ['requiredck'] =   $requiredck;

     if (($WOTCID == "warrior1") && ($ques_info['QuestionID'] == "Social")) {
	$ques_info['QuestionTypeID'] = "1515";
	$ques_info['Question'] = "Last Four of Social Security Number:";
     } // end WOTCID
    
    //Print the Question Preview
    $que_view = call_user_func(array(G::Obj('ApplicationForm'), 'getQuestionView' . $ques_info['QuestionTypeID']), $ques_info);

        if(in_array($QuestionID, $special_ques)) {
        if ($_POST['Initiated'] == "callcenter") {
            if($QuestionID == "verify") {
                echo $que_view;
            }
        }
        else {
                if($QuestionID != "verify") {
			$PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
        		$OrganizationName=$PARENT['CompanyName'];
                	echo str_replace("{Organization}", $OrganizationName, $que_view);
                }
        }
    }
    else {
        echo $que_view;
    }

}

$DATE_IDS   =   trim($DATE_IDS, ", ");
?>
    <div class="row" style="margin-bottom:10px;">
        <div class="col-lg-3 col-md-3 col-sm-3">
            <input type="hidden" name="Initiated" value="<?php echo $_POST['Initiated']; ?>"> 
            <input type="hidden" name="Nonprofit" value="<?php echo $Nonprofit; ?>"> 
            <input type="hidden" name="irecruitlookup" value="<?php echo $IRECRUITLOOKUP; ?>">
            
            <input type="hidden" name="IrecruitApplicationID" value="<?php echo $IrecruitApplicationID; ?>"> 
            <input type="hidden" name="IrecruitRequestID" value="<?php echo $IrecruitRequestID; ?>">

<?php if ($_REQUEST['Attributes'] != "") { ?>
<input type="hidden" name="Attributes" id="Attributes" value="<?php echo $_REQUEST['Attributes'];?>">
<?php } ?>
            
			<div id="QDONE">
			<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="<? echo $IRF['formsubmit']; ?>">
			</div>
		</div>
	</div>
	<input type="hidden" name="ApplicationStatus" value="H">
</form>
<?php
//Code to set date pickers year range
$wotc_form_ques_res		=	G::Obj('WOTCFormQuestions')->getWotcFormQuestionsListByQueTypeID('MASTER', $WotcFormID, '17');
$wotc_form_ques_list	=	$wotc_form_ques_res['results'];

$validate_dates_info	=	array();
for($o = 0; $o < count($wotc_form_ques_list); $o++) {
	$validate_dates_info[$wotc_form_ques_list[$o]['QuestionID']] 	=	json_decode($wotc_form_ques_list[$o]['Validate'], true);
}

$validate_dates_info	=	json_encode($validate_dates_info);
?>
<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info =   JSON.parse('<?php echo json_encode($child_que_list);?>');
var wotc_home		=	'<?php echo WOTC_HOME;?>';
</script>
<script type="text/javascript" src="<?php echo WOTC_HOME;?>js/child-questions.js"></script>
<script type="text/javascript" src="<?php echo WOTC_HOME;?>js/common.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	getFormIDChildQuestionsInfo();

	$( ".row" ).click(function() {
		$("div[id^='divQue-']").css( "background", "#ffffff" );
	  	var next_divs	=	$(this).nextAll( ".form-group" );

	  	for(i = 0; i < next_divs.length; i++) {

	  		if(document.getElementById(next_divs[i].id).style.display == 'block'
		  		|| document.getElementById(next_divs[i].id).style.display == '') {
	  			$("#"+next_divs[i].id).css( "background", "#f5f5f5" );

	  			i = next_divs.length;	//Break the loop if element found
			}
		}
	});
});
var state=document.getElementById("State").value;
getCounties(state,'<?php echo $WOTCID; ?>','<?php echo $ApplicationID; ?>');
<?php
if ($_REQUEST['County'] != "") {
        $county = preg_replace('/\'/', '\\\'', $_REQUEST['County']);
?>
setCounty('<?php echo $county; ?>');
<?php } ?>
</script>

<?php
if(isset($DATE_IDS)) {
	?>
	<script>
	$(document).ready(function() {
		var lst					=	'<?php echo $DATE_IDS;?>';
		var validate_dates_info	=	'<?php echo $validate_dates_info;?>';
		validate_dates_info		=	JSON.parse(validate_dates_info);

		var date_split_ids		=	lst.split(",");
		var date_objs			=	new Array();

		var i			=	0;
		var date_id		=	"";
		var year_range 	=	"";
		
		for(date_id_key in date_split_ids) {
			date_id		=	date_split_ids[i];
			date_id		=	$.trim(date_id);
			year_range	=	getYearRange(validate_dates_info, date_id);
			date_picker(date_id, 'mm/dd/yy', year_range, '');
			i++;
		}
	});
	</script>
	<?php
}

echo G::Obj('WOTC')->wotcFooter($WOTCID);
?>
