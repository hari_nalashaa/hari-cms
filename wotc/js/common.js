/**
 * @method getYmdFromMdy
 * @param mdy
 * @return string
 * @tutorial This method will convert ymd format to mdy.
 */
function getYmdFromMdy(mdy, input_seperator, output_seperator) {
	if (mdy == "" || mdy == null)
		return null;
	date = mdy.split(input_seperator);
	return date[2] + output_seperator + date[0] + output_seperator + date[1];
}

/**
 * @method ReverseDisplay
 */
function urlParam(name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)')
			.exec(window.location.href);
	if (results == null) {
		return null;
	} else {
		return decodeURI(results[1]) || 0;
	}
}

/**
 * @method ReverseDisplay
 */
function ReverseDisplay(d) {
	if (document.getElementById(d).style.display == "none") {
		document.getElementById(d).style.display = "block";
	} else {
		document.getElementById(d).style.display = "none";
	}
}

/**
 * @method getUrlVars
 */
function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
			function(m, key, value) {
				vars[key] = value;
			});
	return vars;
}

/**
 * @method isEmptyObj
 * @param obj
 * @returns {Boolean}
 */
function isEmptyObj(obj) {
	for ( var prop in obj) {
		if (obj.hasOwnProperty(prop))
			return false;
	}

	return true;
}

/**
 * @method getMdyFromYmd
 * @param ymd
 * @return string
 * @tutorial This method will convert mdy format to ymd.
 */
function getMdyFromYmd(ymd, input_seperator, output_seperator) {
	if (ymd == "" || ymd == null)
		return null; // ymd , mdy
	date = ymd.split(input_seperator);
	return date[1] + output_seperator + date[2] + output_seperator + date[0];
}

// Make the first letter capital in string
function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

function setOptionByElementObject(object, value_to_select) {

	if (typeof (object) != 'undefined' && object != null) {
		if (typeof (object.options) != 'undefined'
				&& typeof (object.options) != null) {
			for ( var j = 0; j < object.options.length; j++) {
				if (object.options[j].value == value_to_select) {
					object.selectedIndex = j;
				}
			}
		}

	}
}

// Set error message based on response
function setError(error_message, display_id) {
	error_message = capitalizeFirstLetter(error_message);
	$(display_id).html("<br>" + "<h3>" + error_message + "</h3>");
}

function sendAjaxErrorMail(error_info_data) {

	$.ajax({
		method : "POST",
		url : "sendAjaxErrorMail.php",
		type : "POST",
		data : {
			"error_info" : error_info_data
		},
		success : function(data) {
			// Nothing to do here
		}
	});

}

// Set ajax error information
function setAjaxErrorInfo(request, display_id) {
	request.done(function() {
		// console.log('success');
	});
	request
			.fail(function(jqXHR, textStatus, errorThrown) {
				if (textStatus === 'timeout') {
					setError(
							'Sorry, unable to complete your request. Please try again.',
							display_id);
					var error_information = 'Sorry, unable to complete your request. Please try again.'
							+ display_id;
					error_information += jqXHR.responseText;
					sendAjaxErrorMail(error_information);
				} else {
					setError(textStatus + ": " + errorThrown, display_id);
					var error_information = textStatus + ": " + errorThrown
							+ display_id;
					error_information += jqXHR.responseText;
					sendAjaxErrorMail(error_information);
				}
			});
	request
			.error(function(jqXHR, textStatus, errorThrown) {
				if (textStatus === 'timeout') {
					setError(
							'Sorry, unable to complete your request. Please try again.',
							display_id);
					var error_information = 'Sorry, unable to complete your request. Please try again.'
							+ display_id;
					error_information += jqXHR.responseText;
					sendAjaxErrorMail(error_information);
				} else {
					setError(textStatus + ": " + errorThrown, display_id);
					var error_information = textStatus + ": " + errorThrown
							+ display_id;
					error_information += jqXHR.responseText;
					sendAjaxErrorMail(error_information);
				}
			});
}

function checkAll(checkname, exby) {
	for ( var i = 0; i < checkname.length; i++)
		checkname[i].checked = exby.checked ? true : false;
}

function check_uncheck(chk_status, class_name) {
	$('.' + class_name).each(function() {
		if (chk_status == true) {
			this.checked = true;
		} else if (chk_status == false) {
			this.checked = false;
		}
	});
}

function validate_zipcode(typ, obj, msg) {
	obj.style.color = '#000000';
	switch (typ) {
	case "numeric":
		if (IsNumeric(obj.value) == false) {
			msg = msg + ' must only be numbers.';
			obj.style.color = '#EE0000';
			alert(msg);
		}
		break;
	case 'decimal':
		if (IsDecimal(obj.value) == false) {
			msg = msg + ' must be numbers or a decimal value.';
			obj.style.color = '#EE0000';
			alert(msg);
		}
		break;
	}
	return msg;
}

$(document).ready(function() {
	$("#loading-div-background").css({
		opacity : 0.8
	});
});

function ShowProgressAnimation() {
	$("#loading-div-background").show();
}

function validate(typ, obj, msg) {
	obj.style.color = '#000000';
	switch (typ) {
	case "numeric":
		if (IsNumeric(obj.value) == false) {
			msg = msg + ' must only be numbers.';
			obj.style.color = '#EE0000';
			// obj.value='';
			alert(msg);
		}
		break;
	case 'decimal':
		if (IsDecimal(obj.value) == false) {
			msg = msg + ' must be numbers or a decimal value.';
			obj.style.color = '#EE0000';
			// obj.value='';
			alert(msg);
		}
		break;
	}
}

// check for valid numeric strings
function IsNumeric(strString) {
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	if (strString.length == 0)
		return false;

	// test strString consists of valid characters listed above
	for ( var i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}

// check for valid numeric strings
function IsDecimal(strString) {
	var strValidChars = "0123456789.";
	var strChar;
	var blnResult = true;

	if (strString.length == 0)
		return false;

	// test strString consists of valid characters listed above
	for ( var i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}

// Modified to support Opera
function bookmarksite(title, url) {
	if (window.sidebar) // firefox
		window.sidebar.addPanel(title, url, "");
	else if (window.opera && window.print) { // opera
		var elem = document.createElement('a');
		elem.setAttribute('href', url);
		elem.setAttribute('title', title);
		elem.setAttribute('rel', 'sidebar');
		elem.click();
	} else if (document.all)// ie
		window.external.AddFavorite(url, title);
}

function ChangeViewColor(sc) {
	var MonthHeadingColor = tvcolors[sc].MonthHeader;
	var DayHeader = tvcolors[sc].DayHeader;
	var WeekEnd = tvcolors[sc].WeekEnd;
	var EventDayBackground = tvcolors[sc].EventDayBackground;
	$(".calendar_top_navigation tr th").css({
		"background-color" : MonthHeadingColor,
		"text-align" : "center",
		"color" : "#FFFFFF"
	});

	$(".calendar tr th").css("background-color", DayHeader);
	$(".weekend").css("background-color", WeekEnd);
	$(".eventday").css("background-color", EventDayBackground);
}

function getYearRange(validate_dates_info, date_id) {
	
	min_years		=	"";
	max_years		=	"";
	is_year_range	=	"false";
	has_min_years	=	"no";
	has_max_years	=	"no";
	year_range		=	"";

	date_id	= $.trim(date_id);
	
	var new_date_id	=	date_id.slice(1);
	
	if(validate_dates_info[new_date_id] != null 
		&& validate_dates_info[new_date_id].min != "") {
			min_years		=	validate_dates_info[new_date_id].min;
			is_year_range	=	"true";	
			has_min_years	=	"yes";
	}

	if(validate_dates_info[new_date_id] != null 
		&& validate_dates_info[new_date_id].max != "") {
			max_years		=	validate_dates_info[new_date_id].max;
			is_year_range	=	"true";
			has_max_years	=	"yes";
	}

	if(has_min_years == "yes" 
		&& has_max_years == "yes"
		&& typeof(min_years) != 'undefined'
		&& typeof(max_years) != 'undefined'	) {
		year_range = "-"+min_years+":+"+max_years;
	}
	
	return year_range;
}

function date_picker(datepicker_ids, date_format, year_range, button_text) {

	if (date_format == '' || typeof (date_format) == 'undefined') {
		date_format = 'mm/dd/yy';
	}
	if (year_range == '' || typeof (year_range) == 'undefined') {
		year_range = '-80:+5';
	}
	if (button_text == '' || typeof (button_text) == 'undefined') {
		button_text = 'Select Date';
	}

	$(datepicker_ids).datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'both',
		dateFormat : date_format,
		buttonImage : wotc_home + 'images/calendar.gif',
		yearRange : year_range,
		buttonImageOnly : true,
		buttonText : button_text,
		autoclose : true
	});

}

function validate_date(formdate, formmessage) {
	var err = 0;
	var errtxt = '';

	var validformat = /^\d{2}\/\d{2}\/\d{4}$/; // Basic check for format //
												// validity

	if (formdate != "") {
		if (!validformat.test(formdate)) {

			errtxt += "Invalid '" + formmessage
					+ "' Format.\n\nEnter as mm/dd/yyyy\n";
			err++;

		} else {

			var monthfield = formdate.split("/")[0];
			var dayfield = formdate.split("/")[1];
			var yearfield = formdate.split("/")[2];
			var dayobj = new Date(yearfield, monthfield - 1, dayfield);

			if ((dayobj.getMonth() + 1 != monthfield)
					|| (dayobj.getDate() != dayfield)
					|| (dayobj.getFullYear() != yearfield)) {
				errtxt += "Invalid '"
						+ formmessage
						+ "' Day, Month, or Year range detected.\n\nEnter as mm/dd/yyyy\n\n";
				err++;
			}
		}
	}
	// general error
	if (err > 0) {
		alert(errtxt);
		return false;
	}
	return true;
}

function CheckDocumentObject(element_id) {
	var element_obj = document.getElementById(element_id);
	return (element_obj !== null && typeof (element_obj) != 'undefined');
}

function ValidateZipCode(zip_input_obj, form_name) {
	var msg = "";
	if (IsNumeric(zip_input_obj.value) == false) {
		msg = 'Zip Code must only be numbers.';
	}

	if (msg != "") {
		$(zip_input_obj).next(".zipmessage").html('<span style="color:red">&nbsp;Zip code must only be numbers.</span>');
		return false;
	}

	$(zip_input_obj).next(".zipmessage").html('<img src="'+wotc_home+'images/loading-small.jpg" alt="Validating Zipcode..Please wait.." />');
}

function validate_date(formdate, formmessage) {

	var err = 0;
	var errtxt = '';

	var validformat = /^\d{2}\/\d{2}\/\d{4}$/ // Basic check for format
												// validity

	if (formdate != "") {
		if (!validformat.test(formdate)) {

			errtxt += "Invalid '" + formmessage
					+ "' Format.\n\nEnter as mm/dd/yyyy\n";
			err++;

		} else {

			var monthfield = formdate.split("/")[0]
			var dayfield = formdate.split("/")[1]
			var yearfield = formdate.split("/")[2]
			var dayobj = new Date(yearfield, monthfield - 1, dayfield)

			if ((dayobj.getMonth() + 1 != monthfield)
					|| (dayobj.getDate() != dayfield)
					|| (dayobj.getFullYear() != yearfield)) {
				errtxt += "Invalid '"
						+ formmessage
						+ "' Day, Month, or Year range detected.\n\nEnter as mm/dd/yyyy\n\n";
				err++;
			}
		}
	}
	// general error
	if (err > 0) {
		alert(errtxt);
		return false;
	}
	return true;

}