function validate(typ, obj, msg) {
	obj.style.color = '#000000';
	switch (typ) {
	case "numeric":
		if (IsNumeric(obj.value) == false) {
			msg = msg + ' must only be numbers.';
			obj.style.color = '#990000';
			obj.value = '';
			alert(msg);
		}
		break;
	case 'date':
		alert('date');
		break;
	case 'decimal':
		if (IsDecimal(obj.value) == false) {
			msg = msg + ' must be numbers.';
			obj.style.color = '#990000';
			obj.value = '';
			alert(msg);
		}
		break;
	}
}

function validate_date(formdate, formmessage) {

	var err = 0;
	var errtxt = '';

	var validformat = /^\d{2}\/\d{2}\/\d{4}$/ // Basic check for format
												// validity

	if (formdate != "") {
		if (!validformat.test(formdate)) {

			errtxt += "Invalid '" + formmessage
					+ "' Format.\n\nEnter as mm/dd/yyyy\n";
			err++;

		} else {

			var monthfield = formdate.split("/")[0]
			var dayfield = formdate.split("/")[1]
			var yearfield = formdate.split("/")[2]
			var dayobj = new Date(yearfield, monthfield - 1, dayfield)

			if ((dayobj.getMonth() + 1 != monthfield)
					|| (dayobj.getDate() != dayfield)
					|| (dayobj.getFullYear() != yearfield)) {
				errtxt += "Invalid '"
						+ formmessage
						+ "' Day, Month, or Year range detected.\n\nEnter as mm/dd/yyyy\n\n";
				err++;
			}
		}
	}
	// general error
	if (err > 0) {
		alert(errtxt);
		return false;
	}
	return true;

}

function IsNumeric(strString)
// check for valid numeric strings
{
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	if (strString.length == 0)
		return false;

	// test strString consists of valid characters listed above
	for (i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}

function validate_hireform(form) {

	var err = 0;
	var errtxt = '';

	if ((form.offerdatep.value == "") || (form.offerdatep.value == null)) {
		errtxt += " - Offer Date\n";
		err++;
	}

	if ((form.hireddatep.value == "") || (form.hireddatep.value == null)) {
		errtxt += " - Hired Date\n";
		err++;
	}

	if ((form.startdatep.value == "") || (form.startdatep.value == null)) {
		errtxt += " - Start Date\n";
		err++;
	}

	if ((form.startingwagep.value == "") || (form.startingwagep.value == null)) {
		errtxt += " - Starting Wage\n";
		err++;
	}

	if ((form.positionp.value == "") || (form.positionp.value == null)) {
		errtxt += " - Position\n";
		err++;
	}

	if (err > 0) {
		errtxt = "The following required fields are missing information:\n\n"
				+ errtxt + "\nPlease correct and submit again.";
		alert(errtxt);
		form.offerdatep.focus();
		return false;
	}

	return true;

}

function validate_form(form) {

	var err = 0;
	var errtxt = '';

	if ((form.first.value == "") || (form.first.value == null)) {
		errtxt += " - First Name\n";
		err++;
	}

	if ((form.last.value == "") || (form.last.value == null)) {
		errtxt += " - Last Name\n";
		err++;
	}

	if ((form.social1.value == "") || (form.social1.value == null)
			|| (form.social1.value.length < 3) || (form.social2.value == "")
			|| (form.social2.value == null) || (form.social2.value.length < 2)
			|| (form.social3.value == "") || (form.social3.value == null)
			|| (form.social3.value.length < 4)) {

		if ((form.social1.value.length < 3) || (form.social2.value.length < 2)
				|| (form.social3.value.length < 4)) {
			errtxt += " - Social Security Number is missing digits\n";
		} else {
			errtxt += " - Social Security Number\n";
		}

		err++;
	}

	if ((form.address.value == "") || (form.address.value == null)) {
		errtxt += " - Street Address w/Apt. #\n";
		err++;
	}

	if ((form.city.value == "") || (form.city.value == null)) {
		errtxt += " - City\n";
		err++;
	}

	if ((form.state.value == "") || (form.state.value == null)) {
		errtxt += " - State\n";
		err++;
	}

	if ((form.zip.value == "") || (form.zip.value == null)) {
		errtxt += " - Zip\n";
		err++;
	}

	if ((form.DOBDate.value == "") || (form.DOBDate.value == null)) {
		errtxt += " - Please give your date of birth.\n";
		err++;
	}

	if (form.DOBDate.value != "") {

		var validformat = /^\d{2}\/\d{2}\/\d{4}$/ // Basic check for format
													// validity

		if (!validformat.test(form.DOBDate.value)) {

			errtxt += " - Invalid Date Format for Date of Birth. Enter as mm/dd/yyyy\n";
			err++;

		} else { // Detailed check for valid date ranges

			var monthfield = form.DOBDate.value.split("/")[0]
			var dayfield = form.DOBDate.value.split("/")[1]
			var yearfield = form.DOBDate.value.split("/")[2]
			var dayobj = new Date(yearfield, monthfield - 1, dayfield)

			if ((dayobj.getMonth() + 1 != monthfield)
					|| (dayobj.getDate() != dayfield)
					|| (dayobj.getFullYear() != yearfield)) {
				errtxt += " - Invalid Day, Month, or Year range detected for Date of Birth. Enter as mm/dd/yyyy\n";
				err++;
			}

		}
	}

	if ((form.SWA2[0].checked == false) && (form.SWA2[1].checked == false)) {
		errtxt += " - Please answer if you were referred to by a Vocational Rehabilitation Agency.\n";
		err++;
	}
	if ((form.SWA3[0].checked == false) && (form.SWA3[1].checked == false)) {
		errtxt += " - Please answer if you were referred to by an Employment Network under the Ticket to Work Program.\n";
		err++;
	}
	if ((form.SWA4[0].checked == false) && (form.SWA4[1].checked == false)) {
		errtxt += " - Please answer if you were referred by the Department of Veterans Affairs.\n";
		err++;
	}

	if ((form.veteran[0].checked == false)
			&& (form.veteran[1].checked == false)) {
		errtxt += " - Please answer if you are a Veteran of the U.S. Armed Forces.\n";
		err++;
	}

	if (form.veteran[0].checked == true) {

		if ((form.veteran2[0].checked == false)
				&& (form.veteran2[1].checked == false)) {
			errtxt += " - Please answer if you are a veteran and have been unemployed for a combination of six(6) months.\n";
			err++;
		}
		if ((form.veteran3[0].checked == false)
				&& (form.veteran3[1].checked == false)) {
			errtxt += " - Please answer if you are a veteran and have been unemployed for a period of four(4) weeks..\n";
			err++;
		}
		if ((form.veteran4[0].checked == false)
				&& (form.veteran4[1].checked == false)) {
			errtxt += " - Please answer if you are a veteran and were discharged or released from active duty.\n";
			err++;
		}
		if ((form.veteran5[0].checked == false)
				&& (form.veteran5[1].checked == false)) {
			errtxt += " - Please answer if you are a veteran and are entitled to compensation for a service-connected disability.\n";
			err++;
		}
		if ((form.veteran6[0].checked == false)
				&& (form.veteran6[1].checked == false)) {
			errtxt += " - Please answer if you are a veteran and are a member of a family that received SNAP benefits.\n";
			err++;
		}

		if (form.veteran6[0].checked == true) {

			if ((form.vetsnaprecipient.value == "")
					|| (form.vetsnaprecipient.value == null)) {
				errtxt += " - Please answer the name of the primary recipient for SNAP benefits.\n";
				err++;
			}
			if ((form.vetsnapcity.value == "")
					|| (form.vetsnapcity.value == null)) {
				errtxt += " - Please answer the city for the SNAP benefits.\n";
				err++;
			}
			if ((form.vetsnapstate.value == "")
					|| (form.vetsnapstate.value == null)) {
				errtxt += " - Please answer the state for the SNAP benefits.\n";
				err++;
			}

		}

	} // end if veteran true

	if (document.govform.Nonprofit.value == "N") {

		if ((form.SNAP[0].checked == false) && (form.SNAP[1].checked == false)) {
			errtxt += " - Please answer if you are a member of a family that received Supplemental Nutrition Assistance Program.\n";
			err++;
		}
		if ((form.SNAP[0].checked == true) || (form.SNAP[1].checked == true)) {

			if (form.SNAP[0].checked == true) {

				if ((form.snaprecipient.value == "")
						|| (form.snaprecipient.value == null)) {
					errtxt += " - Please answer the name of the primary recipient for SNAP benefits.\n";
					err++;
				}
				if ((form.snapcity.value == "")
						|| (form.snapcity.value == null)) {
					errtxt += " - Please answer the city for the SNAP benefits.\n";
					err++;
				}
				if ((form.snapstate.value == "")
						|| (form.snapstate.value == null)) {
					errtxt += " - Please answer the state for the SNAP benefits.\n";
					err++;
				}

			} // end if snap true

		} // end if SNAP answered

		if ((form.TANF[0].checked == false) && (form.TANF[1].checked == false)) {
			errtxt += " - Please answer if are a member of a family that received Temporary Assistance for Needy Families.\n";
			err++;
		}

		if ((form.TANF[0].checked == true) || (form.TANF[1].checked == true)) {

			if (form.TANF[0].checked == true) {

				if ((form.tanfrecipient.value == "")
						|| (form.tanfrecipient.value == null)) {
					errtxt += " - Please answer the name of the primary recipient for TANF benefits.\n";
					err++;
				}
				if ((form.tanfcity.value == "")
						|| (form.tanfcity.value == null)) {
					errtxt += " - Please answer the city for the TANF benefits.\n";
					err++;
				}
				if ((form.tanfstate.value == "")
						|| (form.tanfstate.value == null)) {
					errtxt += " - Please answer the state for the TANF benefits.\n";
					err++;
				}

			} // end
		}

		if ((form.SSI[0].checked == false) && (form.SSI[1].checked == false)) {
			errtxt += " - Please answer if you receive Supplemental Security Income benefits.\n";
			err++;
		}

		if ((form.felony[0].checked == false)
				&& (form.felony[1].checked == false)) {
			errtxt += " - Please answer if you have been convicted of a felony or released from prision.\n";
			err++;
		}

		if (form.felony[0].checked == true) {

			var validformat = /^\d{2}\/\d{2}\/\d{4}$/ // Basic check for
														// format validity

			if ((form.felony2[0].checked == false)
					&& (form.felony2[1].checked == false)) {
				errtxt += " - Please select if this was a Federal or State conviction.\n";
				err++;
			}

			if (!validformat.test(form.felonyconvictiondate.value)) {

				errtxt += " - Invalid Date Format for Felony Conviction Date. Enter as mm/dd/yyyy\n";
				err++;

			} else { // Detailed check for valid date ranges

				var monthfield = form.felonyconvictiondate.value.split("/")[0]
				var dayfield = form.felonyconvictiondate.value.split("/")[1]
				var yearfield = form.felonyconvictiondate.value.split("/")[2]
				var dayobj = new Date(yearfield, monthfield - 1, dayfield)

				if ((dayobj.getMonth() + 1 != monthfield)
						|| (dayobj.getDate() != dayfield)
						|| (dayobj.getFullYear() != yearfield)) {
					errtxt += " - Invalid Day, Month, or Year range detected for Felony Conviction Date. Enter as mm/dd/yyyy\n";
					err++;
				}

			}

			if (!validformat.test(form.felonyreleasedate.value)) {

				errtxt += " - Invalid Date Format for Felony Release Date. Enter as mm/dd/yyyy\n";
				err++;

			} else { // Detailed check for valid date ranges

				var monthfield = form.felonyreleasedate.value.split("/")[0]
				var dayfield = form.felonyreleasedate.value.split("/")[1]
				var yearfield = form.felonyreleasedate.value.split("/")[2]
				var dayobj = new Date(yearfield, monthfield - 1, dayfield)

				if ((dayobj.getMonth() + 1 != monthfield)
						|| (dayobj.getDate() != dayfield)
						|| (dayobj.getFullYear() != yearfield)) {
					errtxt += " - Invalid Day, Month, or Year range detected for Felony Release Date. Enter as mm/dd/yyyy\n";
					err++;
				}

			}

		}

	} // end Nonprofit

	if ((form.worked[0].checked == false) && (form.worked[1].checked == false)) {
		errtxt += " - Please answer if you have worked for this company before.\n";
		err++;
	}

	if (form.worked[0].checked == true) {

		var validformat = /^\d{2}\/\d{2}\/\d{4}$/ // Basic check for format
													// validity

		if (!validformat.test(form.WorkedDate.value)) {

			errtxt += " - Invalid Date Format for Last Date of Employement. Enter as mm/dd/yyyy\n";
			err++;

		} else { // Detailed check for valid date ranges

			var monthfield = form.WorkedDate.value.split("/")[0]
			var dayfield = form.WorkedDate.value.split("/")[1]
			var yearfield = form.WorkedDate.value.split("/")[2]
			var dayobj = new Date(yearfield, monthfield - 1, dayfield)

			if ((dayobj.getMonth() + 1 != monthfield)
					|| (dayobj.getDate() != dayfield)
					|| (dayobj.getFullYear() != yearfield)) {
				errtxt += " - Invalid Day, Month, or Year range detected for Last Date of Employment. Enter as mm/dd/yyyy\n";
				err++;
			}

		}
	}

	if ((form.LTU[0].checked == false) && (form.LTU[1].checked == false)) {
		errtxt += " - Please answer if you have received unemployment for at least 27 consecutive weeks.\n";
		err++;
	}

	if (form.LTU[0].checked == true) {

		if ((form.ltustate.value == "") || (form.ltustate.value == null)) {
			errtxt += " - Please select the state where you received unemployment compensation.\n";
			err++;
		}

	}

	if (err > 0) {
		errtxt = "The following required fields are missing information:\n\n"
				+ errtxt + "\nPlease correct and submit again.";
		alert(errtxt);
		form.first.focus();
		return false;
	}

	return true;
}

function dobOff(init) {
	document.getElementById('dobFilterQuestion').style.display = 'none';
}

function dobOn(init) {
	document.getElementById('dobFilterQuestion').style.display = 'block';
}

function workedOff(init) {
	document.getElementById('workedFilterQuestion').style.display = 'none';
}

function workedOn(init) {
	document.getElementById('workedFilterQuestion').style.display = 'block';
}

function tanfOff(init) {
	document.getElementById('tanfFilterQuestion').style.display = 'none';

	var TANF = '';

	for ( var i = 0; i < document.govform.TANF.length; i++) {
		if (document.govform.TANF[i].checked) {
			TANF = document.govform.TANF[i].value;
		}
	}

	if (TANF == "Y") {
		document.getElementById('tanfFilterQuestion').style.display = 'block';
		highlight('Q704');

	} else if (TANF == "N") {
		document.getElementById('tanfFilterQuestion').style.display = 'none';
		if (init == "") {
			highlight('Q800');
		}
	}

}

function tanfOn(init) {
	document.getElementById('tanfFilterQuestion').style.display = 'block';

	var TANF = '';

	for ( var i = 0; i < document.govform.TANF.length; i++) {
		if (document.govform.TANF[i].checked) {
			TANF = document.govform.TANF[i].value;
		}
	}

	if (TANF == "Y") {
		document.getElementById('tanfFilterQuestion').style.display = 'block';
		highlight('Q704');

	} else if (TANF == "N") {

		document.getElementById('tanfFilterQuestion').style.display = 'block';
		highlight('Q800');
	}

}

function ltuOff(init) {
	document.getElementById('ltuFilterQuestion').style.display = 'none';

	var LTU = '';

	for ( var i = 0; i < document.govform.LTU.length; i++) {
		if (document.govform.LTU[i].checked) {
			LTU = document.govform.LTU[i].value;
		}
	}

	if (LTU == "Y") {
		document.getElementById('ltuFilterQuestion').style.display = 'block';
		highlight('Q301');

	} else if (LTU == "N") {
		document.getElementById('ltuFilterQuestion').style.display = 'none';
		if (init == "") {
			highlight('QDONE');
		}
	}

}

function ltuOn(init) {
	document.getElementById('ltuFilterQuestion').style.display = 'block';

	var LTU = '';

	for ( var i = 0; i < document.govform.LTU.length; i++) {
		if (document.govform.LTU[i].checked) {
			LTU = document.govform.LTU[i].value;
		}
	}

	if (LTU == "Y") {
		document.getElementById('ltuFilterQuestion').style.display = 'block';
		highlight('Q301');

	} else if (LTU == "N") {

		document.getElementById('ltuFilterQuestion').style.display = 'block';

	}

}

function snapOff(init) {
	document.getElementById('snapFilterQuestion').style.display = 'none';

	for ( var i = 0; i < document.govform.SNAP.length; i++) {
		if (document.govform.SNAP[i].checked) {
			value = document.govform.SNAP[i].value;
			if (value == "Y") {
				document.getElementById('snapFilterQuestion').style.display = 'block';
				highlight('Q602');
			}
			if (value == "N") {
				document.getElementById('snapFilterQuestion').style.display = 'none';
				if (init == "") {
					highlight('Q700');
				}
			}
		}
	}

}

function snapOn(init) {
	document.getElementById('snapFilterQuestion').style.display = 'block';
}

function veteranOff(init) {
	document.getElementById('veteranFilterQuestion').style.display = 'none';
}

function veteranOn(init) {
	document.getElementById('veteranFilterQuestion').style.display = 'block';
}

function veteran2Off(init) {
	document.getElementById('veteran2FilterQuestion').style.display = 'none';
}

function veteran2On(init) {
	document.getElementById('veteran2FilterQuestion').style.display = 'block';
}

function felonyOff(init) {
	document.getElementById('felonyFilterQuestion').style.display = 'none';
}

function felonyOn(init) {
	document.getElementById('felonyFilterQuestion').style.display = 'block';
}

function checkWHO() {

	if ((document.govform.first.value !== "")
			&& (document.govform.first.value !== 'null')
			&& (document.govform.last.value !== "")
			&& (document.govform.last.value !== 'null')
			&& (document.govform.social1.value !== "")
			&& (document.govform.social1.value !== 'null')
			&& (document.govform.social1.value.length > 2)
			&& (document.govform.social2.value !== "")
			&& (document.govform.social2.value !== 'null')
			&& (document.govform.social2.value.length > 1)
			&& (document.govform.social3.value !== "")
			&& (document.govform.social3.value !== 'null')
			&& (document.govform.social3.value.length > 3)
			&& (document.govform.address.value !== "")
			&& (document.govform.address.value !== 'null')
			&& (document.govform.city.value !== "")
			&& (document.govform.city.value !== 'null')
			&& (document.govform.state.value !== "")
			&& (document.govform.state.value !== 'null')
			&& (document.govform.zip.value !== "")
			&& (document.govform.zip.value !== 'null')) {
		document.getElementById('QWHO').style.fontWeight = '400';
		document.getElementById('QWHO').style.backgroundColor = '#ffffff';
		highlight('Q200');
	}
}

function highlight(section) {

	document.getElementById('Q200').style.fontWeight = '400';
	document.getElementById('Q200').style.backgroundColor = '#ffffff';

	document.getElementById('Q400').style.fontWeight = '400';
	document.getElementById('Q400').style.backgroundColor = '#ffffff';
	document.getElementById('Q401').style.fontWeight = '400';
	document.getElementById('Q401').style.backgroundColor = '#ffffff';
	document.getElementById('Q402').style.fontWeight = '400';
	document.getElementById('Q402').style.backgroundColor = '#ffffff';

	document.getElementById('Q500').style.fontWeight = '400';
	document.getElementById('Q500').style.backgroundColor = '#ffffff';
	document.getElementById('Q501').style.fontWeight = '400';
	document.getElementById('Q501').style.backgroundColor = '#ffffff';
	document.getElementById('Q502').style.fontWeight = '400';
	document.getElementById('Q502').style.backgroundColor = '#ffffff';
	document.getElementById('Q503').style.fontWeight = '400';
	document.getElementById('Q503').style.backgroundColor = '#ffffff';
	document.getElementById('Q504').style.fontWeight = '400';
	document.getElementById('Q504').style.backgroundColor = '#ffffff';
	document.getElementById('Q505').style.fontWeight = '400';
	document.getElementById('Q505').style.backgroundColor = '#ffffff';
	document.getElementById('Q506').style.fontWeight = '400';
	document.getElementById('Q506').style.backgroundColor = '#ffffff';

	document.getElementById('Q100').style.fontWeight = '400';
	document.getElementById('Q100').style.backgroundColor = '#ffffff';
	document.getElementById('Q101').style.fontWeight = '400';
	document.getElementById('Q101').style.backgroundColor = '#ffffff';

	if (document.govform.Nonprofit.value == "N") {
		document.getElementById('Q400').style.fontWeight = '400';
		document.getElementById('Q400').style.backgroundColor = '#ffffff';
		document.getElementById('Q401').style.fontWeight = '400';
		document.getElementById('Q401').style.backgroundColor = '#ffffff';
		document.getElementById('Q402').style.fontWeight = '400';
		document.getElementById('Q402').style.backgroundColor = '#ffffff';

		document.getElementById('Q600').style.fontWeight = '400';
		document.getElementById('Q600').style.backgroundColor = '#ffffff';
		document.getElementById('Q602').style.fontWeight = '400';
		document.getElementById('Q602').style.backgroundColor = '#ffffff';

		document.getElementById('Q700').style.fontWeight = '400';
		document.getElementById('Q700').style.backgroundColor = '#ffffff';

		document.getElementById('Q704').style.fontWeight = '400';
		document.getElementById('Q704').style.backgroundColor = '#ffffff';

		document.getElementById('Q800').style.fontWeight = '400';
		document.getElementById('Q800').style.backgroundColor = '#ffffff';

		document.getElementById('Q900').style.fontWeight = '400';
		document.getElementById('Q900').style.backgroundColor = '#ffffff';
		document.getElementById('Q901').style.fontWeight = '400';
		document.getElementById('Q901').style.backgroundColor = '#ffffff';
		document.getElementById('Q902').style.fontWeight = '400';
		document.getElementById('Q902').style.backgroundColor = '#ffffff';

		document.getElementById('Q300').style.fontWeight = '400';
		document.getElementById('Q300').style.backgroundColor = '#ffffff';
		document.getElementById('Q301').style.fontWeight = '400';
		document.getElementById('Q301').style.backgroundColor = '#ffffff';

		document.getElementById('QDONE').style.fontWeight = '400';
		document.getElementById('QDONE').style.backgroundColor = '#ffffff';

	} // end Nonprofit

	document.getElementById(section).style.fontWeight = '700';
	document.getElementById(section).style.backgroundColor = '#eeeeee';

}

function determineDOB(DOB) {

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById('DOBAge').innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "ajax/determineDOB.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("DOB=" + DOB + "&d=" + d);

} // end function

function autotab(current, to) {
	if (current.getAttribute
			&& current.value.length == current.getAttribute("maxlength")) {
		to.focus()
	}
}
