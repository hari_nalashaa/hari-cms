function processUserPortalWOTCForm(btn_obj) {
	
	var form_id 	=	$(btn_obj).parents("form").attr('name');
	var input_data	=	$('#'+form_id).serialize();
	
	var request = $.ajax({
		method: "POST",
		url: "processWotcForm.php",
		type: "POST",
		data: input_data,
		beforeSend: function() {
			$("#loading_message").html('Please wait.. <img src="'+wotc_home+'images/loading-small.jpg"/><br><br>');
		},
		success: function(data) {

			let json_data = JSON.parse(data);
			let error_keys = Object.keys(json_data['Errors']);

			if(json_data['Status'] == 'Success' && json_data['WotcStatusCode'] == 'Success') {
				$("#div_wotc_form").html(json_data['Message']);
			}
			else if(json_data['Status'] == 'Failed' && json_data['WotcStatusCode'] == 'Duplicate') {
				$("#div_wotc_form").html(json_data['Message']);
			}
			else {	
				$("#frmWotcForm label").each(function (i) {
					// But it can be done simply with:
					var label_for	=	$(this).attr("for");
					var label		=	$('label[for="' + label_for + '"]');
					
					if(typeof(label_for) != 'undefined') {
						if(error_keys.indexOf(label_for) !== -1) {
							$('label[for="' + label_for + '"]').css({"background-color": "yellow", "padding": "8px"});
						}
						else {
							$('label[for="' + label_for + '"]').removeAttr('style');
						}
					}
			    });

				$("#loading_message").html('');

			}
    	}
	});

}

$(document).ready(function() {
	getFormIDChildQuestionsInfo();

	$( ".row" ).click(function() {
		$("div[id^='divQue-']").css( "background", "#ffffff" );
	  	var next_divs	=	$(this).nextAll( ".form-group" );
	
	  	for(i = 0; i < next_divs.length; i++) {
	
	  		if(document.getElementById(next_divs[i].id).style.display == 'block'
		  		|| document.getElementById(next_divs[i].id).style.display == '') {
	  			$("#"+next_divs[i].id).css( "background", "#f5f5f5" );
	
	  			i = next_divs.length;	//Break the loop if element found
			}
		}
	});

	date_picker(date_picker_ids, '', '', '');
});