<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

if ($RequestID) {
    if($InternalVCode) {
        echo "Employee Application: &nbsp;";
    }    
	echo '<a href="';
	if (FROM_SRC == "USERPORTAL") {
		echo USERPORTAL_HOME;
	} else {
		echo PUBLIC_HOME;
	}
	
	if($InternalVCode != "" && FROM_SRC != "USERPORTAL") {
	    echo 'internalRequisitions.php?';
	}
	else {
	    echo 'index.php?';
	}
	echo 'OrgID=' . $OrgID;
	if ($MultiOrgID) {
	    echo '&MultiOrgID=' . $MultiOrgID;
	}
	if ($InternalVCode) {
	    echo '&InternalVCode=' . $InternalVCode;
	}
	echo '"';
	echo '>';	
	echo 'Back to Category List</a><br><br>';
	
	echo PrintReq ( $OrgID, $RequestID, '' );
} else {
	
    //Set ORGLEVELS array
    $ORGLEVELS  =   array ();
    $results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
    
    for($r = 0; $r < count($results); $r++) {
        $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
    } // end foreach
    
    // Set ORGLEVELDATA array
    $ORGLEVELDATA   =   array ();
    $results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, '1', 'equal');
    for($od = 0; $od < count($results); $od++) {
        $ORGLEVELDATA [$results[$od]['OrgLevelID']] [$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
    }
	
    if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		echo $OrganizationDescription;
		echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
	}
	
	//Set condition
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "Active = 'Y'");
	$where[] = "NOW() BETWEEN PostDate AND ExpireDate";
	//Set parameters
	$params    =   array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID);
	
	if ($Internal == "Y") {
	    $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	} else {
	    $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	}
	
	$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
	$new_job_days = $highlight_req_settings['NewJobDays'];
	
	$columns = "Title, EmpStatusID, RequestID, Description";
	if($new_job_days > 0) {
	    $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
	}
	else {
	    $columns .= ", Highlight";
	}

	//Set condition
	$whereQ        =   array("OrgID = :OrgID", "QuestionID = 'ListingPriority'", "Requisition = 'Y'", "RequisitionFormID in (SELECT RequisitionFormID FROM RequisitionForms where OrgID = :OrgID2 and FormDefault = 'Y')");
	//Set parameters
	$paramsQ       =   array("OrgID"=>$OrgID,"OrgID2"=>$OrgID);
	$results       =   $RequisitionQuestionsObj->getRequisitionQuestions('Requisition', $whereQ, '', array($paramsQ));
	$listpriority  =   $results['count'];

	if ($listpriority > 0) {
	  $order = 'ListingPriority, Title';	
	} else {
	  $order = 'Title';	
	}

	//Get Requisitions Information
	$results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", $order, array($params));

	//Get count
	$hit = $results['count'];

	echo '<div>';	
	if ($hit > 0) {
		
		$color = "#eeeeee";
		
		echo '<div style="background-color:' . $color . ';">';
		
		echo '<div style="padding:3px 0px 3px 10px;">';
		echo '<strong>Position</strong>';
		echo '</div>';
		echo '</div>';
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $REQ) {
			
                $STATUSLEVELS   =   array ();
                //Get Default RequisitionFormID
                $req_det_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
                $STATUSLEVELS   =   G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
			    
				if ($color == "#ffffff") {
					$color = "#eeeeee";
				} else {
					$color = "#ffffff";
				}
				
				echo '<div style="background-color:' . $color . ';">';
				
				echo '<div style="padding:5px 0 5px 10px;">';
				
				echo '<a href="';
				if (FROM_SRC == "USERPORTAL") {
					echo USERPORTAL_HOME;
				} else {
					echo PUBLIC_HOME;
				}
				echo 'index.php?';
				echo 'OrgID=' . $OrgID;
				if ($MultiOrgID) {
					echo '&MultiOrgID=' . $MultiOrgID;
				} 
				echo '&RequestID=' . $REQ ['RequestID'];

				if ($InternalVCode) {
					echo '&InternalVCode=' . $InternalVCode;
				}
				echo '&navpg=listings';
				echo '">';
				echo $REQ ['Title'] . '</a>';

				if($REQ['Highlight'] == "New") {
				    echo '&nbsp;&nbsp;';
				    echo $new_job_image;
				}
				else if($REQ['Highlight'] == "Hot") {
				    echo '&nbsp;&nbsp;';
				    echo $hot_job_image;
				}
				
				echo '&nbsp;&nbsp;';
				
				if ($REQ ['EmpStatusID']) {
					echo '&nbsp;(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
				}
				
				//Set condition
				$where      =   array("OrgID = :OrgID", "RequestID = :RequestID", "OrgLevelID = 1");
				//Set parameters
				$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$REQ ['RequestID']);
				//Get Requisition Org Levels Information
				$resultsIN  =   G::Obj('Requisitions')->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID, SelectionOrder", array($params));
				$hitOL      =   $resultsIN['count'];
				
				$i = 0;
				echo '<br>' . $ORGLEVELS [1] . ': ';
				
				if(is_array($resultsIN['results'])) {
					foreach ($resultsIN['results'] as $ROL) {
						if ($ROL ['OrgLevelID'] == 1) {
							if ($i > 0) {
								echo ', ';
							}
							echo ' ' . $ORGLEVELDATA [$ROL ['OrgLevelID']] [$ROL ['SelectionOrder']];
							$i ++;
						}
					}
				} // end foreach
				
				echo "<br><br>\n";
				
				echo '</div>';
				echo '</div>';
			} // end foreach
		}
	} else { // hit > 0
		
		echo '<p>There are no openings at the current time.</p>';
	}
	
	echo '</div>';
	echo '<div style="clear:both;"></div>';
	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
} // end else
?>
