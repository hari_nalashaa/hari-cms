<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

if ($RequestID) {
    if($InternalVCode) {
        echo "Employee Application: &nbsp;";
    }
	echo '<a href="';
	if (FROM_SRC == "USERPORTAL") {
		echo USERPORTAL_HOME;
	} else {
		echo PUBLIC_HOME;
	}
	
	if($InternalVCode != "" && FROM_SRC != "USERPORTAL") {
	    echo 'internalRequisitions.php?';
	}
	else {
	    echo 'index.php?';
	}
	echo 'OrgID=' . $OrgID;
	if ($MultiOrgID) {
		echo '&MultiOrgID=' . $MultiOrgID;
	}
	if ($InternalVCode) {
	    echo '&InternalVCode=' . $InternalVCode;
	}
	echo '"';
	echo '>';
	echo 'Back to Category List</a><br><br>';
	
	echo PrintReq ( $OrgID, $RequestID, '' ); 
} else {
	
	//Set ORGLEVELS array
    $ORGLEVELS  =   array ();	
    $results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
	
	for($r = 0; $r < count($results); $r++) {
	    $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
	} // end foreach
	
	// Set ORGLEVELDATA array
	$ORGLEVELDATA   =   array ();
	$results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, '1', 'equal');
	for($od = 0; $od < count($results); $od++) {
	    $ORGLEVELDATA [$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
	}
	echo '<div>';
	
    if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	    echo '<style>';
	    echo '.arrow { float:none; }';
	    echo '</style>';
		echo $OrganizationDescription;
		echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
	}
	
	//Set condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "Active = 'Y'");
	$where[] = "NOW() BETWEEN PostDate AND ExpireDate";
	//Set parameters
	$params = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID);
	
	if ($Internal == "Y") {
	    $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	} else {
	    $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	}
	
	$columns = "Title, EmpStatusID, RequestID, Description";
	if($new_job_days > 0) {
	    $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
	}
	else {
	    $columns .= ", Highlight";
	}
	
	//Get Requisitions Information
	$results = G::Obj('Requisitions')->getRequisitionInformation($columns, $where, "", "Title", array($params));
	
	//Get count
	$hit = $results['count'];
	
	if ($hit > 0) {
		
		$color = "#eeeeee";
		
		echo '<div style="min-width:720px;background-color:' . $color . ';">';
		
		echo '<div style="float:left;width:450px;padding:3px 0px 3px 10px;">';
		echo '<strong>Position</strong>';
		echo '</div>';
		
		echo '<div style="float:left;width:250px;padding:3px 0px 3px 10px;">';
		
		//Get Organization Levels Information
		$results_org_level = G::Obj('OrganizationLevels')->getOrgLevelInfo($OrgID, $MultiOrgID, 1);
		$OrganizationLevel = $results_org_level["OrganizationLevel"];
		
		echo '<strong>' . $OrganizationLevel . '</strong>';
		echo '</div>';
		
		echo '<div style="clear:both;"></div>';
		
		echo '</div>';

		if(is_array($results['results'])) {
			foreach($results['results'] as $REQ) {
					
			    //Get Default RequisitionFormID
			    $STATUSLEVELS    =   array ();
			    $req_det_info    =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
			    $STATUSLEVELS    =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
			     
				if ($color == "#ffffff") {
					$color = "#eeeeee";
				} else {
					$color = "#ffffff";
				}
					
				echo '<div style="min-width:720px;background-color:' . $color . ';">';
					
				echo '<div style="float:left;width:450px;padding:5px 0 5px 10px;">';

				echo '<a href="';
				if (FROM_SRC == "USERPORTAL") {
					echo USERPORTAL_HOME;
				} else {
					echo PUBLIC_HOME;
				}
				echo 'index.php?';
				echo 'OrgID=' . $OrgID;
				if ($MultiOrgID) {
					echo '&MultiOrgID=' . $MultiOrgID;
				}
				echo '&RequestID=' . $REQ ['RequestID'];
				if ($InternalVCode) {
					echo '&InternalVCode=' . $InternalVCode;
				}
				echo '&navpg=listings';
				echo '">';
				echo $REQ ['Title'] . '</a>';
		
				if($REQ['Highlight'] == "New") {
				    echo '&nbsp;&nbsp;';
				    echo $new_job_image;
				}
				else if($REQ['Highlight'] == "Hot") {
				    echo '&nbsp;&nbsp;';
				    echo $hot_job_image;
				}
				
				echo '&nbsp;&nbsp;';
					
				if ($REQ ['EmpStatusID']) {
					echo '&nbsp;(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
				}
				echo '</div>';
					
				//Set condition
				$where = array("OrgID = :OrgID", "RequestID = :RequestID", "OrgLevelID = 1");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":RequestID"=>$REQ ['RequestID']);
				//Get Requisition Org Levels Information
				$resultsIN = $RequisitionsObj->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID, SelectionOrder", array($params));
				$hitOL = $resultsIN['count'];
				
				echo '<div style="float:left;width:250px;padding:5px 0 5px 10px;">';
					
				if(is_array($resultsIN['results'])) {
					foreach ($resultsIN['results'] as $ROL) {
						echo $ORGLEVELDATA [$ROL ['SelectionOrder']];
						echo '<br>';
					}
				} // end foreach
					
				if ($hitOL == 0) {
					echo '&nbsp;';
				}
					
				echo '</div>';
					
				echo '<div style="clear:both;"></div>';
					
				echo '</div>';
			} // end foreach
		}
		
		
		//echo '</td></tr></table>';
		echo '</div>';
	} else { // hit > 0
		
		echo '<p>There are no openings at the current time.</p>';
	}
	
	echo '<div style="clear:both;"></div>';
	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
} // end else
?>
