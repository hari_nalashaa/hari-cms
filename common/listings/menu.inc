<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

if ($RequestID) {
    
    if($InternalVCode) {
        echo "Employee Application: &nbsp;";
    }    
	echo '<a href="';
	if (FROM_SRC == "USERPORTAL") {
		echo USERPORTAL_HOME;
	} else {
		echo PUBLIC_HOME;
	}
	
	if($InternalVCode != "" && FROM_SRC != "USERPORTAL") {
	    echo 'internalRequisitions.php?';
	}
	else {
	    echo 'index.php?';
	}
	echo 'OrgID=' . $OrgID;
	if ($MultiOrgID) {
	    echo '&MultiOrgID=' . $MultiOrgID;
	}
	if ($InternalVCode) {
	    echo '&InternalVCode=' . $InternalVCode;
	}
	
	echo '&level=' . htmlspecialchars($_REQUEST['level']);
	echo '"';
	echo '>';	
	echo 'Back to Category List</a><br><br>';
	
	echo PrintReq ( $OrgID, $RequestID, '' );

} else {

	echo '<div class="orgdesc">';
	echo '<style>';
	echo '.arrow { float: none; }';
	echo '</style>';
	if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	  echo $OrganizationDescription;
	  echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
	}

	echo '</div>';

	// Set ORGLEVELS array
	$ORGLEVELS  =   array ();
	$results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
	$orgcnt     =   count($results);
	
	for($r = 0; $r < count($results); $r++) {
	    $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
	} // end foreach
	
	// Set ORGLEVELDATA array
	$ORGLEVELDATA   =   array ();
	$results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, $orgcnt, 'less_equal');
	
	for($od = 0; $od < count($results); $od++) {
	    $ORGLEVELDATA [$results[$od]['OrgLevelID']][$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
	}
	
	// get limited levels to restrict selection if not active
	$results = $OrganizationsObj->getActiveLevels($OrgID, $MultiOrgID, $Internal);

	$ACTIVE = array();
	foreach ($results['results'] as $AL) {
	  $ACTIVE[$AL['OrgLevelID']][$AL['SelectionOrder']]=$AL['cnt'];
	}	

	echo '<div style="float:left;vertical-align:middle;">';

	$MENUS = array_keys($ORGLEVELS);

	if (isset($_GET['level'])) {
	  $MENUS = explode(':',$_GET['level']);
	}

	if(count($MENUS) <= 1) {
	  echo '<div class="menu">';
	  echo $ORGLEVELS[$MENUS[0]];
	  echo '</div>';
	  $_POST['level'] = $_GET['level'];
	  $_REQUEST['level'] = $MENUS[0];
	} else {

	  echo '<div class="menuselect">';
	  echo '<form method="POST">';
	  echo '<input type="hidden" name="level" value="' . $_REQUEST['level'] . '">' . "\n";
	  echo '<select name="level" onchange="submit()">';
	  echo '<option value="">Please Select</option>';

	  foreach($ORGLEVELS AS $ol => $menu) {

	    if (in_array($ol,$MENUS)) {
	      if (in_array($ol,array_keys($ACTIVE))) {
          echo '<option value="' . $ol . '"';
	      if($_REQUEST['level'] == $ol) {
	        echo ' SELECTED';
	      }
	      echo '>' . $menu . '</option>';
	      }
	    }
	  }

	  echo '</select>';
	  echo '</form>';
	  echo '</div>';

	}
	echo '</div>';

	if($_REQUEST['level'] != '') { 
	  // search form

	  list($LEVEL,$SEL) = explode(':',$_REQUEST['menulist']);

	  echo '<div class="menuchoice" style="float:left;margin-left:20px;">';
	  echo '<form method="POST">';
	  echo '<select name="menulist" onchange="submit()">';
	  echo '<option value="">Please Select</option>';

	  foreach ($ORGLEVELDATA[$_REQUEST['level']] as $ii => $label) {
	    if (in_array($ii,array_keys($ACTIVE[$_REQUEST['level']]))) {
        echo '<option value="' . $_REQUEST['level'] . ':' . $ii . '"';
	    if($ii == $SEL) {
	      echo ' SELECTED';
	    }
	    echo '>' . $label . ' (' . $ACTIVE[$_REQUEST['level']][$ii] . ')</option>';
	    }
	  }

	  echo '</select>';
	  echo '<input type="hidden" name="level" value="' . $_REQUEST['level'] . '">';
	  echo '</form>';
	  echo '</div>';
	} // end no level

	echo '<div style="clear:both;"></div>';


	if(isset($SEL)) {
	// results
	echo '<div>';

	// Set condition
	$where = array (
		"OrgID = :OrgID",
		"OrgLevelID = :OrgLevelID",
		"SelectionOrder= :SelectionOrder" 
	);
	// Set parameters
	$params = array (
		":OrgID" => $OrgID,
		":OrgLevelID" => $LEVEL,
		":SelectionOrder" => $SEL
	);
				// Get Requisition Org Levels Information
	$resultsIN = $RequisitionsObj->getRequisitionOrgLevelsInfo ( "*", $where, "OrgLevelID, SelectionOrder", array (
						$params 
	) );

	$REQUESTIDS = "";
	foreach ($resultsIN['results'] as $OL) {
	$REQUESTIDS .= "'" . $OL['RequestID'] . "',";
	}
	$REQUESTIDS = substr($REQUESTIDS,0,-1);

	// Set condition
	$where = array (
			"OrgID = :OrgID",
			"MultiOrgID = :MultiOrgID",
			"Active = 'Y'" 
	);
	$where[] = "NOW() BETWEEN PostDate AND ExpireDate";

	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":MultiOrgID" => $MultiOrgID 
	);

	if($REQUESTIDS !="") {
		$where [] = "RequestID in ($REQUESTIDS)";
	} else {
		$where [] = "RequestID = ''";
	}
	
	if ($Internal == "Y") {
	    $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	} else {
	    $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	}
	
	$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
	$new_job_days = $highlight_req_settings['NewJobDays'];
	
	$columns = "Title, EmpStatusID, RequestID, Description";
	if($new_job_days > 0) {
	    $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
	}
	else {
	    $columns .= ", Highlight";
	}
	
	// Get Requisitions Information
	$results = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", "Title", array (
			$params 
	) );

	// Get count
	$hit = $results ['count'];

	echo '<div style="margin-top:10px;">';

	if ($hit > 0) {

	// ######################## Common where condition code end ################################
	    
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $REQ ) {

	    $STATUSLEVELS          =   array ();
	    //Get Default RequisitionFormID
	    $req_det_info          =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
	    $STATUSLEVELS          =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
		    
		if ($color == "#ffffff") {
			$color = "#eeeeee";
		} else {
			$color = "#ffffff";
		}
				
		echo '<div style="min-width:768px;padding:5px;background-color:' . $color . ';">';
				
		echo '<a href="';
		if (FROM_SRC == "USERPORTAL") {
		   echo USERPORTAL_HOME;
		} else {
		   echo PUBLIC_HOME;
		}
		echo 'index.php?';
		echo 'OrgID=' . $OrgID;
		if ($MultiOrgID) {
		   echo '&MultiOrgID=' . $MultiOrgID;
		} 
		echo '&RequestID=' . $REQ ['RequestID'];
        echo '&level=' . $_REQUEST['level'];
        if ($InternalVCode) {
			echo '&InternalVCode=' . $InternalVCode;
		}
		echo '&navpg=listings';
		echo '">';
		echo $REQ ['Title'] . '</a>';
				
		if($REQ['Highlight'] == "New") {
		    echo '&nbsp;&nbsp;';
		    echo $new_job_image;
		}
		else if($REQ['Highlight'] == "Hot") {
		    echo '&nbsp;&nbsp;';
		    echo $hot_job_image;
		}
		
		if ($REQ ['EmpStatusID']) {
		   echo '&nbsp;&nbsp;';
		   echo '(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
		}
		echo '</div>';

		} // end foreach
	} // end is array

	} else { // hit > 0
		
		echo '<p>There are no openings at the current time.</p>';
	}
	
	  echo '</div>';

	echo '</div>';

	} // end SEL

	echo '<div style="clear:both;"></div>';
	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';

} // end else
?>
