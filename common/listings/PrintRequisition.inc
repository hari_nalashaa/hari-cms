<?php
$rtn = '';
function PrintReq($OrgID, $RequestID, $anchor) {
    global $COLOR, $USERID, $pg, $sl, $ol, $feature, $Internal, $RandID, $rtn, $MonsterRequestURL, $InternalVCode;

	//Set columns
	$columns = "MultiOrgID, Title, Description, FormID, EmpStatusID, DATE_FORMAT(PostDate,'%M %d, %Y') as PostDate, 
				City, State, ZipCode, InternalFormID";
	// Get Requisition Information
	$results = G::Obj('Requisitions')->getRequisitionsDetailInfo($columns, $OrgID, $RequestID);
	
	if(is_array($results)) {
		// Set the Requisition Information to variables
		list ( $MultiOrgID, $Title, $Description, $FormID, $EmpStatusID, $PostDate, $City, $State, $ZipCode, $InternalFormID ) = array_values ( $results );
	}
	
	// Set columns
	$columns   =   "MAX(OrgLevelID) AS MaxOrgLevelID";
	// Set condition and parameters
	$where     =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	$params    =   array (":OrgID" => $OrgID, ":MultiOrgID" => $MultiOrgID);
	// Get Org Levels Information
	$results   =   G::Obj('Organizations')->getOrganizationLevelsInfo ( $columns, $where, '', array ($params) );
	$OrgLevels =   $results ['results'] [0] ['MaxOrgLevelID'];
	
	if ($Internal == "Y") {
		$FormID   =   $InternalFormID;
	} // end Internal
	  
	// sets the application preferences for display buttons to complete application
	$params    =   array (":OrgID" => $OrgID, ":MultiOrgID" => $MultiOrgID);
	// Set condition
	$where     =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	// Get Entry Methods Information
	$results   =   G::Obj('EntryMethods')->getEntryMethodsInfo ( "*", $where, "", array ($params) );
	$EM        =   $results ['results'] [0];

	$Portal    =   '';
	$Public    =   '';
	
	if ($EM ['EntryMethod'] == "P") {
		$Public = 'Y';
	} else if ($EM ['EntryMethod'] == "UP") {
		$Portal = 'Y';
		if (FROM_SRC == "USERPORTAL") {
			$Public = 'Y';
		}
	} else if ($EM ['EntryMethod'] == "B") {
		$Public = 'Y';
		$Portal = 'Y';
	} else {
		$Public = 'Y';
		$Portal = 'N';
	}

	// Get Text From TextBlocks
	$OrgDescription    =   G::Obj('FormFeatures')->getTextFromTextBlocks ( $OrgID, $FormID, 'DepartmentDesc' );
	
	$req_det_info      =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $RequestID);
	$emp_status_levels =   G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($OrgID, $req_det_info["RequisitionFormID"]);
	
	$EmploymentStatusLevel = $emp_status_levels[$EmpStatusID];
	
	if ($OrgDescription != "") {
		$rtn .= '<div id="orgdesc">';
		$rtn .= $OrgDescription;
		$rtn .= '</div>';	
	}
	
	$rtn .= '<p><b>';
	if ($anchor != "") {
		$rtn .= '<a name="job' . $anchor . '">';
	}
	$rtn .= $Title;
	if ($anchor != "") {
		$rtn .= '</a>';
	}
	if ($EmploymentStatusLevel != "") {
		if ($OrgID != "I20120901") { // American Engineering Testing
			$rtn .= '</b>&nbsp;&nbsp;(' . $EmploymentStatusLevel . ')';
		}
	} else {
		$rtn .= '</b>';
	}
	$rtn .= '<br>';
	
	$i = 1;

	if ($OrgID == "I20141112") { // Cable & Wireless
          $OrgLevels = 1;
    }
	
	while ( $i <= $OrgLevels ) {
		
		// Set columns
		$columns      =   "OLD.CategorySelection, OL.OrganizationLevel";
		$table_name   =   "OrganizationLevels OL, RequisitionOrgLevels ROL, OrganizationLevelData OLD";
		// Set condition
		$where = array (
				"OL.OrgID           =   ROL.OrgID",
				"ROL.OrgID          =   OLD.OrgID",
				"OL.MultiOrgID      =   OLD.MultiOrgID",
				"OL.MultiOrgID      =   :MultiOrgID",
				"ROL.OrgLevelID     =   OLD.OrgLevelID",
				"ROL.OrgLevelID     =   OL.OrgLevelID",
				"ROL.SelectionOrder =   OLD.SelectionOrder",
				"ROL.OrgID          =   :OrgID",
				"ROL.RequestID      =   :RequestID",
				"ROL.OrgLevelID     =   :OrgLevelID" 
		);
		// Set params
		$params = array (
				":MultiOrgID"       =>  $MultiOrgID,
				":OrgID"            =>  $OrgID,
				":RequestID"        =>  $RequestID,
				":OrgLevelID"       =>  $i 
		);
		// Get Org Requisition Levels Information
		$resultsIN = G::Obj('Organizations')->getOrgAndReqLevelsInfo ( $table_name, $columns, $where, '', array ($params) );
		
		$SelectionTitle = "";
		$SelectionResults = "";
		
		if (is_array ( $resultsIN ['results'] )) {
			foreach ( $resultsIN ['results'] as $Selection ) {
				$SelectionTitle = $Selection ['OrganizationLevel'];
				$SelectionResults .= $Selection ['CategorySelection'] . ", ";
			} // end foreach
		}
		
		if (($OrgID != "I20111019") && ($OrgID != "I20091101")) {
			if (substr ( $SelectionResults, 0, - 2 ) != "") {
				$rtn .= '<b>' . $SelectionTitle . ':</b> ' . substr ( $SelectionResults, 0, - 2 ) . '<br>';
			}
		} // end if
		
		$i ++;
	} // end OrgLevels
	
	if (($OrgID == "I20110816") || ($OrgID == "I20110815") || ($OrgID == "B12345467") || ($OrgID == "I20121215")) {
		$rtn .= '<b>Date Posted:</b> ' . $PostDate;
	}
	
	$rtn .= '<br>' . $Description . '</p>';

	$rtn .= '<div style="clear:both;height:20px;"></div>';
	$rtn .= '<div id="reqbuttons">';
	
	if(G::Obj('ServerInformation')->getRequestSource() != 'ajax')
	{
		if ($feature ['UserPortal'] == "Y") {
            
			if ((FROM_SRC != "USERPORTAL") && ($Portal == "Y")) {
			     
			    if ($Internal != "Y") {
			        $rtn .= '<div id="register">';
			        $rtn .= '<div class="text">';
			        $rtn .= 'If you would like to set profiles, manage, and track your submissions you can register here<br><br>';
			        $rtn .= '</div>';
			        
			        $rtn .= '<div class="div_btn_register">';
			        $rtn .= '<form method="post" action="' . USERPORTAL_HOME . 'requestHold.php">';
			        $rtn .= '<input type="hidden" name="OrgID" value="' . $OrgID . '">';
			        if ($MultiOrgID) {
			            $rtn .= '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">';
			        } 
			        $rtn .= '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			        $rtn .= '<input type="hidden" name="Task" value="Application">';
			        $rtn .= '<input type="hidden" name="RandID" id="RandID" value="' . $RandID . '">';
			        $rtn .= '<input type="hidden" name="MonsterRequestURL" id="MonsterRequestURL" value="' . $MonsterRequestURL . '">';
			        $rtn .= '<input type="hidden" name="action" value="register">';
			        $rtn .= '<input type="hidden" name="InternalVCode" value="' . $InternalVCode . '">';
			        $rtn .= '<input type="submit" class="public_btn_register" value="register">';
			        $rtn .= '</form>';
			        $rtn .= '</div>';
			        $rtn .= '</div>';
			        	
			        $rtn .= '<div id="login">';
			        $rtn .= '<div class="text">';
			        $rtn .= '...or if you already have an account you may login here<br><br>';
			        $rtn .= '</div>';
			        
			        $rtn .= '<div class="div_btn_login">';
			        $rtn .= '<form method="post" action="' . USERPORTAL_HOME . 'requestHold.php">';
			        $rtn .= '<input type="hidden" name="OrgID" value="' . $OrgID . '">';
			        if ($MultiOrgID) {
			            $rtn .= '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">';
			        } 
			        $rtn .= '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			        $rtn .= '<input type="hidden" name="Task" value="Application">';
			        $rtn .= '<input type="hidden" name="RandID" id="RandID" value="' . $RandID . '">';
			        $rtn .= '<input type="hidden" name="MonsterRequestURL" id="MonsterRequestURL" value="' . $MonsterRequestURL . '">';
			        $rtn .= '<input type="hidden" name="action" value="login">';
			        $rtn .= '<input type="hidden" name="InternalVCode" value="' . $InternalVCode . '">';
			        $rtn .= '<input type="submit" class="public_btn_login" value="login">';
			        $rtn .= '</form>';
			        $rtn .= '</div>';
			        
			        $rtn .= '</div>';
			    }
			}
		} else { // else feature
		
			$Public = "Y";
		} // end feature
		
		if ($Public == "Y" || $Internal == "Y") {
			$rtn .= '<div id="apply">';
			$rtn .= '<div class="text">';
			
			if($Internal != "Y") {
			    if ((FROM_SRC != "USERPORTAL") && ($Portal == "Y") && ($feature ['UserPortal'] == "Y")) {
			        $rtn .= '...or<br><br>';
			    }
			}
			
			$rtn .= '</div>';

			$requisition_url = PUBLIC_HOME;
			
			if (FROM_SRC == "USERPORTAL") {
			    $requisition_url = USERPORTAL_HOME;
			}
			
			$rtn .= '<div class="div_btn_apply">';
			$rtn .= '<form method="GET" action="' . $requisition_url . 'jobApplication.php">';
			$rtn .= '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			$rtn .= '<input type="hidden" name="pg" value="' . $pg . '">';
			$rtn .= '<input type="hidden" name="RandID" id="RandID" value="' . $RandID . '">';
			$rtn .= '<input type="hidden" name="MonsterRequestURL" id="MonsterRequestURL" value="' . $MonsterRequestURL . '">';
			$rtn .= '<input type="hidden" name="process" value="Y">';

			$rtn .= '<input type="hidden" name="OrgID" value="' . $OrgID . '">';
			if ($MultiOrgID) {
				$rtn .= '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">';
			} 
			$rtn .= '<input type="hidden" name="InternalVCode" value="' . $InternalVCode . '">';
			$rtn .= '<input type="submit" class="public_btn_apply" value="apply now">';
			$rtn .= '</form>';
			$rtn .= '</div>';
			
			$rtn .= '</div>';
		}		
	}
	
	$rtn .= '</div>';
	$rtn .= '<div style="clear:both;height:20px;"></div><br><br>';
	
	if (FROM_SRC != "USERPORTAL") {

		// Get Social Media Information
		$SM           =   G::Obj('SocialMedia')->getSocialMediaDetailInfo($OrgID, $MultiOrgID);
		
		$allow        =   array ("P", "B");
		
		if (in_array ( $SM ['Placement'], $allow )) {
			
			// Set columns condition and params
			$columns = "ROL.OrgLevelID, ROL.SelectionOrder";
			// Set condition
			$where = array (
					"ROL.OrgID     =   R.OrgID",
					"ROL.OrgID     =   :OrgID",
					"ROL.RequestID =   :RequestID",
					"ROL.RequestID =   R.RequestID",
					"R.Active      =   'Y'" 
			);
			//Set parameters
			$params = array (
					":OrgID"       =>  $OrgID,
					":RequestID"   =>  $RequestID 
			);
			//Get Requisition Org Levels Information
			$req_org_levels_info = G::Obj('Requisitions')->getReqAndReqOrgLevelsInfo ( $columns, $where, "", "ROL.OrgLevelID, ROL.SelectionOrder LIMIT 1", array (
					$params 
			) );
			
			$SM2 = $req_org_levels_info ['results'] [0];
			
			$smurl = PUBLIC_HOME . 'jobRequest.php?';
			$smurl .= 'OrgID=' . $OrgID;
			if ($MultiOrgID) {
				$smurl .= 'MultiOrgID=' . $MultiOrgID;
			} 
			$smurl .= '&RequestID=' . $RequestID;
			$smurl .= '&RandID=' . $RandID;
			
			$i = 0;
			
			if ($SM ['Facebook'] == "Y") {
				$rtn .= facebook ( $smurl, $Title, "", strip_tags ( $Description ) );
				$i ++;
			}
			
			if ($SM ['Twitter'] == "Y") {
				if ($i > 0) {
					$rtn .= '&nbsp;&nbsp;&#8901;&nbsp;&nbsp;';
				}
				$rtn .= twitter ( $smurl, $Title );
				$i ++;
			}
			
			if ($SM ['LinkedIn'] == "Y") {
				if ($i > 0) {
					$rtn .= '&nbsp;&nbsp;&#8901;&nbsp;&nbsp;';
				}
				$rtn .= linkedin ( $smurl, $Title, strip_tags ( $Description ) );
			}
		} // end Placement
	} // end social media
	
	$rtn .= '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
	
	return $rtn;
} // end function
?>
