<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

// Set ORGLEVELS array
$ORGLEVELS  =   array ();
$results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
$orgcnt     =   count($results);

for($r = 0; $r < count($results); $r++) {
    $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
} // end foreach

// Set ORGLEVELDATA array
$ORGLEVELDATA   =   array ();
$results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, $orgcnt, 'less_equal');

for($od = 0; $od < count($results); $od++) {
    $ORGLEVELDATA [$results[$od]['OrgLevelID']][$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
}

// get limited levels to restrict selection if not active
$results = G::Obj('Organizations')->getActiveLevels($OrgID, $MultiOrgID, $Internal);

$ACTIVE = array();
foreach ($results['results'] as $AL) {
  $ACTIVE[$AL['OrgLevelID']][$AL['SelectionOrder']]=$AL['cnt'];
}	

unset($ORGLEVELS[2]);
unset($ORGLEVELS[3]);
unset($ORGLEVELS[4]);
unset($ORGLEVELS[5]);

echo "<div style=\"margin:10px 0 0 10px;padding-right:60px;float:left;max-width:250px;\">";

for($i=1; $i<=count($ORGLEVELS); $i++) {

    echo "<div style=\"margin-bottom:10px;\">";
    echo "<strong style=\"font-size:14pt;\">" . $ORGLEVELS[$i] . ":</strong>";
    echo "</div>";

    if (count($ORGLEVELS) == 1) {

    	$results   =   getList(1,1);
    	$hit       =   $results ['count'];

        if($hit > 0) {

            if (is_array ( $results ['results'] )) {
                foreach ( $results ['results'] as $REQ ) {
                
                	echo '<div style="padding:5px;">';
                	echo '<a href="';
                    
                    if (FROM_SRC == "USERPORTAL") {
                       echo USERPORTAL_HOME;
                    } else {
                       echo PUBLIC_HOME;
                    }
                    echo 'index.php?';
                    echo 'OrgID=' . $OrgID;
                    if ($MultiOrgID) {
                       echo '&MultiOrgID=' . $MultiOrgID;
                    }                    
                    echo '&RequestID=' . $REQ ['RequestID'];
                    if ($InternalVCode) {
                        echo '&InternalVCode=' . $InternalVCode;
                    }
		    echo '&navpg=listings';
                    echo '">';
                    echo $REQ ['Title'] . '</a>';
                
                    if($REQ['Highlight'] == "New") {
                        echo '&nbsp;&nbsp;';
                        echo $new_job_image;
                    }
                    else if($REQ['Highlight'] == "Hot") {
                        echo '&nbsp;&nbsp;';
                        echo $hot_job_image;
                    }
                
                    echo '</div>';
                
                } //end foreach
                
            } //end if

        } else { // hit > 0
            echo '<p>There are no openings at the current time.</p>';
        }

    } else { 

	   echo "<div style=\"margin:0 0 30px 5px;line-height:200%;\">";

	   foreach($ORGLEVELDATA[$i] as $ii=>$OLD) {
            
	       if($ACTIVE[$i][$ii] > 0) {
                echo '<a href="';
                if (FROM_SRC == "USERPORTAL") {
                   echo USERPORTAL_HOME;
                } else {
                   echo PUBLIC_HOME;
                }
                
        		echo 'index.php?';
        		echo 'OrgID=' . $OrgID;
        		if ($MultiOrgID) {
        		   echo '&MultiOrgID=' . $MultiOrgID;
        		}        		
        		echo '&level=' . $i . ":" . $ii;
        		if ($InternalVCode) {
					echo '&InternalVCode=' . $InternalVCode;
				}
        		echo '">';

    	        echo $OLD . " (" . $ACTIVE[$i][$ii] . ")";
        		echo '</a>';
	      }
	      
  	      echo '<br>';

	   } // end ORGLEVELDATA
	   
	   echo "</div>";

	} // end ORGLEVELS

} 

echo "</div>";

echo "<div style=\"display:inline;float:left;max-width:800px;padding-left:40px;border-left:1px solid #ccc;margin-top:10px;min-height:300px;\">";

//if(0) { // job listings
if(isset($_REQUEST['level']) && $_REQUEST['level'] != '') {

    list($MENU,$LEVEL) = explode(':',$_REQUEST['level']);

    if(isset($LEVEL)) {

        $results= getList($MENU,$LEVEL);
        
        $hit = $results ['count'];
    
        echo '<div style="margin-top:10px;">';
    
        if ($hit > 0) {
    
            // ######################## Common where condition code end ################################
            if (is_array ( $results ['results'] )) {
            	foreach ( $results ['results'] as $REQ ) {

            	    $STATUSLEVELS  =   array ();
            	    //Get Default RequisitionFormID
            	    $req_det_info  =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
            	    $STATUSLEVELS  =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
            	     
                	if ($color == "#ffffff") {
                		$color = "#eeeeee";
                	} else {
                		$color = "#ffffff";
                	}
                			
                	echo '<div style="min-width:768px;padding:5px;background-color:' . $color . ';">';
                
                	echo '<a href="';
                	if (FROM_SRC == "USERPORTAL") {
                	   echo USERPORTAL_HOME;
                	} else {
                	   echo PUBLIC_HOME;
                	}
                	echo 'index.php?';
                	echo 'OrgID=' . $OrgID;
                	if ($MultiOrgID) {
                	   echo '&MultiOrgID=' . $MultiOrgID;
                	} 
                	echo '&RequestID=' . $REQ ['RequestID'];
                	if ($InternalVCode) {
                        echo '&InternalVCode=' . $InternalVCode;
                    }
			echo '&navpg=listings';
                	echo '">';
                	echo $REQ ['Title'] . '</a>';
                
                	if($REQ['Highlight'] == "New") {
                	    echo '&nbsp;&nbsp;';
                	    echo $new_job_image;
                	}
                	else if($REQ['Highlight'] == "Hot") {
                	    echo '&nbsp;&nbsp;';
                	    echo $hot_job_image;
                	}
                			
                	if ($REQ ['EmpStatusID']) {
                	   echo '&nbsp;&nbsp;';
                	   echo '(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
                	}
                	echo '</div>';
            
            	} //end foreach
            
            } //end is array
    
        } else { // hit > 0
        	echo '<p>There are no openings at the current time.</p>';
        }
    
        echo '</div>';
    } // end LEVEL Jobs Loop
// Display individual job position in right pannel
} else if ($RequestID) {
    echo PrintReq ( $OrgID, $RequestID, '' );
} else { // otherwise pring organization description
    echo '<style>';
    echo '.arrow { float:none; }';
    echo '</style>';
    if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
        echo $OrganizationDescription;
    }
}

echo "</div>";


function getList ($MENU,$LEVEL) {

	global $OrgID, $MultiOrgID, $RequisitionsObj, $HighlightRequisitionsObj, $Internal;

    $where = array (
        "OrgID          = :OrgID",
        "OrgLevelID     = :OrgLevelID",
        "SelectionOrder = :SelectionOrder"
    );
    $params = array (
        ":OrgID"            =>  $OrgID,
        ":OrgLevelID"       =>  $MENU,
        ":SelectionOrder"   =>  $LEVEL
    );
    
    // Get Requisition Org Levels Information
    $resultsIN = $RequisitionsObj->getRequisitionOrgLevelsInfo ( "*", $where, "OrgLevelID, SelectionOrder", array ($params) );

    $REQUESTIDS = "";
    foreach ($resultsIN['results'] as $OL) {
       $REQUESTIDS .= "'" . $OL['RequestID'] . "',";
    }
    $REQUESTIDS = substr($REQUESTIDS,0,-1);

    $where  =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "Active = 'Y'");
    $where[] = "NOW() BETWEEN PostDate AND ExpireDate";
    $params =   array (":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);

    if($REQUESTIDS !="") {
            $where [] = "RequestID in ($REQUESTIDS)";
    } else {
            $where [] = "RequestID = ''";
    }

    if ($Internal == "Y") {
        $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    } else {
        $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    }
    
    $highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
    $new_job_days = $highlight_req_settings['NewJobDays'];

    $columns = "Title, EmpStatusID, RequestID, Description";
    if($new_job_days > 0) {
        $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
    }
    else {
        $columns .= ", Highlight";
    }
    // Get Requisitions Information
    $results = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", "Title", array ($params) );

	return $results;

} // end function
?>
