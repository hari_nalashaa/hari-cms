<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

echo '<div style="margin:10px auto;width:1200px;">';

if ($RequestID) {
    if($InternalVCode) {
        echo "Employee Application: &nbsp;";
    }
	echo '<a href="';
	if (FROM_SRC == "USERPORTAL") {
		echo USERPORTAL_HOME;
	} else {
		echo PUBLIC_HOME;
	}
	
	if($InternalVCode != "" && FROM_SRC != "USERPORTAL") {
	    echo 'internalRequisitions.php?';
	}
	else {
	    echo 'index.php?';
	}
	echo 'OrgID=' . $OrgID;
	if ($MultiOrgID) {
	    echo '&MultiOrgID=' . $MultiOrgID;
	}
	if ($InternalVCode) {
	    echo '&InternalVCode=' . $InternalVCode;
	}
	echo '"';
	echo '>';	
	echo 'Back to Category List</a><br><br>';
	
	echo PrintReq ( $OrgID, $RequestID, '' );
} else {
	
	//Set ORGLEVELS array
    $ORGLEVELS  =   array ();	
	//Get Organization Levels Information
    $results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
	
	for($r = 0; $r < count($results); $r++) {
	    $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
	} // end foreach
	
	// Set ORGLEVELDATA array
	$ORGLEVELDATA   =   array ();
	$results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, '1', 'equal');
	for($od = 0; $od < count($results); $od++) {
	    $ORGLEVELDATA [$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
	}
	
	if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	    echo '<style>';
	    echo '.arrow { float:none; }';
	    echo '</style>';
		echo $OrganizationDescription;
		echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
	}

    //Set condition
    $where  =   array("R.OrgID = ROL.OrgID", "R.RequestID = ROL.RequestID",  "ROL.OrgLevelID = 1", "R.OrgID = :OrgID", "R.MultiOrgID = :MultiOrgID", "R.Active = 'Y'");
    $where[] = "NOW() BETWEEN PostDate AND ExpireDate";

    if ($Internal == "Y") {
        $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    } else {
        $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    }
    
    $params =   array(":OrgID"=>$OrgID,":MultiOrgID"=>$MultiOrgID);
	
	$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
	$new_job_days = $highlight_req_settings['NewJobDays'];
	
	$columns = "R.Title, R.EmpStatusID, R.RequestID, R.Description";
	if($new_job_days > 0) {
	    $columns .= ", IF((R.Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(R.PostDate)) <= $new_job_days), 'New', R.Highlight) Highlight";
	}
	else {
	    $columns .= ", R.Highlight";
	}
	
	$results = $RequisitionsObj->getReqAndReqOrgLevelsInfo("*", $where, "", "ROL.SelectionOrder, R.Title", array($params));

	$hit = $results['count'];

	if ($hit > 0) {
		
		$color = "#eeeeee";
		
		echo '<div style="min-width:720px;background-color:' . $color . ';">';
		
		//Set where condition
		$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID = 1");
		//Set the parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		
		//Get Organization Levels Information
		$results_org_level = $OrganizationsObj->getOrganizationLevelsInfo("OrganizationLevel", $where, '', array($params));
		$OrganizationLevel = $results_org_level['results'][0]["OrganizationLevel"];
		
		echo '<div style="float:left;width:250px;padding:3px 0px 3px 10px;">';
		echo '<strong>' . $OrganizationLevel . '</strong>';
		echo '</div>';
		
		echo '<div style="float:left;width:450px;padding:3px 0px 3px 10px;">';
		echo '<strong>Position</strong>';
		echo '</div>';
		
		echo '<div style="clear:both;"></div>';
		
		echo '</div>';
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $REQ) {

			    $STATUSLEVELS          =   array ();
			    //Get Default RequisitionFormID
			    $req_det_info          =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
			    $STATUSLEVELS          =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
			     
				if ($REQ ['SelectionOrder'] != $hold) {
					if ($color == "#ffffff") {
						$color = "#eeeeee";
					} else {
						$color = "#ffffff";
					}
				}
					
				echo '<div style="min-width:720px;background-color:' . $color . ';">';
					
				//Set condition
				$where = array("OrgID = :OrgID", "RequestID = :RequestID", "OrgLevelID = 1");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":RequestID"=>$REQ ['RequestID']);
				//Get Requisition Org Levels Information
				$resultsIN      =   G::Obj('Requisitions')->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID, SelectionOrder", array($params));
				$hitOL          =   $resultsIN['count'];
				$hitOL_List[]   =   $hitOL;
				$max_hitOL      =   max($hitOL_List);
				$hit_value      =   0;
				
				if(is_array($resultsIN['results'])) {
					foreach($resultsIN['results'] as $ROL) {
					    $hit_value++;
						echo '<div style="float:left;min-width:230px;padding:5px 0 5px 10px;">';
						if ($hold != $ROL ['SelectionOrder']) {
							echo $ORGLEVELDATA [$ROL ['SelectionOrder']];
						}
						echo '</div>';
							
						if ($hold != $ROL ['SelectionOrder']) {
							$hold = $ROL ['SelectionOrder'];
						}
					} // end foreach
					
					for($m = $hit_value; $m < $max_hitOL; $m++) {
					    echo '<div style="float:left;min-width:230px;padding:5px 0 5px 10px;">';
					    echo '&nbsp;';
					    echo '</div>';
					}
				}
					
				echo '<div style="float:left;min-width:400px;padding:5px 0 5px 10px;">';
					
				echo '<a href="';
				if (FROM_SRC == "USERPORTAL") {
					echo USERPORTAL_HOME;
				} else {
					echo PUBLIC_HOME;
				}
				echo 'index.php?';
				echo 'OrgID=' . $OrgID;
				if ($MultiOrgID) {
					echo '&MultiOrgID=' . $MultiOrgID;
				} 
				echo '&RequestID=' . $REQ ['RequestID'];
				if ($InternalVCode) {
					echo '&InternalVCode=' . $InternalVCode;
				}		
				echo '&navpg=listings';
				echo '">';
				echo $REQ ['Title'] . '</a>';

				if($REQ['Highlight'] == "New") {
				    echo '&nbsp;&nbsp;';
				    echo $new_job_image;
				}
				else if($REQ['Highlight'] == "Hot") {
				    echo '&nbsp;&nbsp;';
				    echo $hot_job_image;
				}
				
				echo '&nbsp;&nbsp;';
					
				if ($REQ ['EmpStatusID']) {
					echo '&nbsp;(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
				}
				echo '</div>';
					
				echo '<div style="vertical-align:center;padding-top:4px;">';
				echo '<form method="post" action="' . USERPORTAL_HOME . 'requestHold.php">';
				echo '<input type="hidden" name="OrgID" value="' . $OrgID . '">';
				if ($MultiOrgID) {
					echo '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">';
				}
				echo '<input type="hidden" name="RequestID" value="' . $REQ ['RequestID'] . '">';
				echo '<input type="hidden" name="RandID" id="RandID" value="' . $RandID . '">';
				echo '<input type="hidden" name="MonsterRequestURL" id="MonsterRequestURL" value="' . $MonsterRequestURL . '">';
				echo '<input type="hidden" name="action" value="register">';
				echo '<button type="submit">Apply Now</button>';
				echo '</form>';
				echo '</div>';
					
				// } // end if
					
				echo '<div style="clear:both;"></div>';
					
				echo '</div>';
			} // end foreach
		}
		
	} else { // hit > 0
		
		echo '<p>There are no openings at the current time.</p>';
	}
	
	echo '</div>';
	
	echo '<div style="clear:both;"></div>';
	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
} // end else
?>
