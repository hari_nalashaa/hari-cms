<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

$STATES = array (
		"AK" => "Alaska",
		"AL" => "Alabama",
		"AZ" => "Arizona",
		"AR" => "Arkansas",
		"CA" => "California",
		"CO" => "Colorado",
		"CT" => "Connecticut",
		"DE" => "Delaware",
		"FL" => "Florida",
		"GA" => "Georgia",
		"HI" => "Hawaii",
		"ID" => "Idaho",
		"IL" => "Illinois",
		"IN" => "Indiana",
		"IA" => "Iowa",
		"KS" => "Kansas",
		"KY" => "Kentucky",
		"LA" => "Louisiana",
		"ME" => "Maine",
		"MD" => "Maryland",
		"MA" => "Massachusetts",
		"MI" => "Michigan",
		"MN" => "Minnesota",
		"MS" => "Mississippi",
		"MO" => "Missouri",
		"MT" => "Montana",
		"NE" => "Nebraska",
		"NV" => "Nevada",
		"NH" => "New Hampshire",
		"NJ" => "New Jersey",
		"NM" => "New Mexico",
		"NY" => "New York",
		"NC" => "North Carolina",
		"ND" => "North Dakota",
		"OH" => "Ohio",
		"OK" => "Oklahoma",
		"OR" => "Oregon",
		"PA" => "Pennsylvania",
		"RI" => "Rhode Island",
		"SC" => "South Carolina",
		"SD" => "South Dakota",
		"TN" => "Tennessee",
		"TX" => "Texas",
		"UT" => "Utah",
		"VT" => "Vermont",
		"VA" => "Virginia",
		"WA" => "Washington",
		"DC" => "Washington DC",
		"WV" => "West Virginia",
		"WI" => "Wisconsin",
		"WY" => "Wyoming" 
);

if ($OrgID) {
	
	if (($OrgID == "I20090201") 
	   || ($OrgID == "I20090302") 
	   || ($OrgID == "I20100603") 
	   || ($OrgID == "I20120420") 
	   || ($OrgID == "I20130101") 
	   || ($OrgID == "I20140401") 
	   || ($OrgID == "I20101210")) {
		$SinglePageListing = "Y";
	}
	
	// a single page listing with each requisition on its own page
	if (($OrgID == "Bxx12345467") || ($OrgID == "I20130101")) {
		$SinglePageListing = "Y";
		$LinkToNewPage = "Y";
	}

if ($OrgID == "I20100805") { // The Arc of Orange County
    ?>
    <style>
    .table {
    font-size:12pt;
    line-height:160%;
    }
    
    .table a {
    font-size:12pt;
    line-height:160%;
    color:#0000EE;
    }
    .table a:hover {
    color:#551A8B;
    }
    </style>
    <?php
}
if ($OrgID == "I20130725") {
    ?>
    <style>
    .table {
        font-size:13pt;
        line-height:160%;
    }
    .table a {
        font-size:16pt;
        line-height:160%;
    }
    </style>
    <?php
}
?>
<table class="table table-striped hiring">
<?php
	$secondpage = "Y";
	if (($olnew) && ($slnew)) {
		if (($OrgID == "I20090402") || ($OrgID == "I20110214") || ($OrgID == "I20120127") || ($OrgID == "I20120622") || ($OrgID == "I20110815")) {
			$secondpage = "N";
		}
	}
	if ($secondpage == "Y") {
		echo '<tr><td>';
		
		// No Company Description on second page
		$print = "Y";
		if (($RequestID != "") && ($OrgID == "I20151210")) {
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20110901")) {
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20121101")) {
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20140912")) {
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20140708")) {
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20130812")) {
			$print = "N";
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20090903")) {
			$print = "N";
		}
		if (($RequestID != "") && ($OrgID == "I20130619")) {
			$print = "N";
		}
		
		if ($IFRAME != "Y") {
			
			if ($print == "Y") {
			    if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
    				echo $OrganizationDescription;
			    }
			}
			
			echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
		} // end IFRAME
		
		echo '</td></tr>';
    }
    ?>

    <tr>
		<td>
    <?php
	//Set columns
	$columns = "ROL.*, count(*) cnt ";
	//Set condition
	$where 	 = array("R.OrgID = :OrgID", "R.MultiOrgID = :MultiOrgID", "R.OrgID = ROL.OrgID", "R.RequestID = ROL.RequestID", "R.Active = 'Y'");
        $where[] = "NOW() BETWEEN PostDate AND ExpireDate";
	//Set parameters
	$params  = array(":OrgID"=>$OrgID, ":MultiOrgID"=>(string)$MultiOrgID);

	if ($Internal == "Y") {
	    $where[] = "R.PresentOn IN ('INTERNALONLY', 'INTERNALANDPUBLIC')";
	} else {
	    $where[] = "R.PresentOn IN ('PUBLICONLY', 'INTERNALANDPUBLIC')";
	}

	
    if ($OrgID == "I20141112") { // Cable & Wireless
        $where[] = "ROL.OrgLevelID = 1";
    }
	
	if (($olnew) && ($slnew)) {
		$where[] = "ROL.OrgLevelID = :OlNewOrgLevelID AND ROL.SelectionOrder = :OlNewSelectionOrder";
		$params[":OlNewOrgLevelID"]  =  $olnew;
		$params[":OlNewSelectionOrder"]  =  $slnew;
		$categorylist = "Y";
	}
	
	if ($olcat) {
		$olnew = $olcat;
		$where[] = "ROL.OrgLevelID = :OlCatOrgLevelID";
		$params[":OlCatOrgLevelID"]  =  $olnew;
	}

	//Get Requisitions and Requisition Organization Levels Information
	$results = $RequisitionsObj->getReqAndReqOrgLevelsInfo($columns, $where, "ROL.OrgLevelID, ROL.SelectionOrder", "ROL.OrgLevelID, ROL.SelectionOrder", array($params));

	$LOCATION = $OrganizationsObj->displayLocation($OrgID, $MultiOrgID, $results['results'][0]['OrgLevelID'], $results['results'][0]['SelectionOrder']);

	$hit = $results['count'];
	
	if ($hit == 0) {
		if ($OrgID == "I20141101") {
			echo '<p>Holy Cross Energy is currently not accepting applications or resumes.  Please check back for any future openings.</p>';
		} else {
			echo '<p>There are no openings at the current time.</p>';
		}
	}
	
	if ($SinglePageListing == "Y") {
		$hit = 0;
	}
	
	// This customer wants listings for location even if only one job posting
	if (($OrgID == "I20090301") && ($hit == 1)) {
		if (($olnew == "") && ($slnew == "")) {
			$hit = 2;
		}
	}
	
	if ($IFRAME == "Y") {
		if ($hit <= 1) {
			$hit = 2;
		}
	}
	
	if ($OrgID == "I20121101") {
		$hit = 0;
	}
	if ($OrgID == "I20140912") {
		$hit = 0;
	}
	if ($OrgID == "I20140708") {
		$hit = 0;
	}
	if ($OrgID == "I20130812") {
		$hit = 0;
	}
	
	if ($hit > 1) {
		
		$categorylist = "Y";
		
		if(is_array($results['results'])) {
            foreach($results['results'] as $OL) {
					
                    //Set the condition
                    $where		=	array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID = :OrgLevelID", "SelectionOrder = :SelectionOrder");
                    //Set the parameters
                    $params		=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrgLevelID"=>$OL ['OrgLevelID'], ":SelectionOrder"=>$OL ['SelectionOrder']);
                    					
                    //Get OrganizationLevelData Information
                    $resultsIN  =   $OrganizationsObj->getOrganizationLevelDataInfo("CategorySelection", $where, '', array($params));
                    $cat        =   $resultsIN['results'][0]['CategorySelection'];
                    
                    //Set condition
                    $where      =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID = :OrgLevelID");
                    //Set parameters
                    $params	    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrgLevelID"=>$OL['OrgLevelID']);
                    //Get Organization Levels Information				
                    $resultsIN  =   $OrganizationsObj->getOrganizationLevelsInfo("OrganizationLevel", $where, '', array($params));
                    $level      =   $resultsIN['results'][0]['OrganizationLevel'];
						
					if ($label != "N") {
                        echo $level . ": ";
					}
						
					echo '<a href="';
					if (FROM_SRC == "USERPORTAL") {
						echo USERPORTAL_HOME;
					} else {
						echo PUBLIC_HOME;
					}
					echo 'index.php?';
					echo 'OrgID=' . $OrgID;
					if ($MultiOrgID) {
						echo '&MultiOrgID=' . $MultiOrgID;
					} 
					echo '&navpg=listings';
					echo '&olnew=' . $OL ['OrgLevelID'] . '&slnew=' . $OL ['SelectionOrder'];
					if ($InternalVCode) {
						echo '&InternalVCode=' . $InternalVCode;
					}
					echo '"';
					if ($IFRAME == "Y") {
						echo ' target="_blank"';
					}
					echo '>';
						
					echo $cat . "</a> (" . $OL ['cnt'] . ")<br>";
                } // end foreach
			}
			
	} else {
        // hit
		//Set condition
		$where  = array("R.OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "R.OrgID = ROL.OrgID", "R.RequestID = ROL.RequestID", "R.Active = 'Y'");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
			
		if ($RequestID) {
			$where[] = "R.RequestID = :ListingRequestID";
			$params[":ListingRequestID"] = $RequestID;
			$availablepositionlist = "Y";
		}
			

    	if (($olnew) && ($slnew)) {
            $where[] = "ROL.OrgLevelID = :OlNewOrgLevelID AND ROL.SelectionOrder = :OlNewSelectionOrder";
            $params[":OlNewOrgLevelID"]  =  $olnew;
            $params[":OlNewSelectionOrder"]  =  $slnew;
            $categorylist = "Y";
        }
			
        if ($olcat) {
        	$olnew = $olcat;
        	$where[] = "ROL.OrgLevelID = :OlCatOrgLevelID";
        	$params[":OlCatOrgLevelID"]  =  $olnew;
        }

	if ($Internal == "Y") {
	    $where[] = "R.PresentOn IN ('INTERNALONLY', 'INTERNALANDPUBLIC')";
	} else {
	    $where[] = "R.PresentOn IN ('PUBLICONLY', 'INTERNALANDPUBLIC')";
	}

		$where[] = "R.PostDate < NOW() AND R.ExpireDate > NOW()";
			
		$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
		$new_job_days = $highlight_req_settings['NewJobDays'];
				
		$columns = "R.Title, R.EmpStatusID, R.RequestID, R.Description";
		if($new_job_days > 0) {
		    $columns .= ", IF((R.Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(R.PostDate)) <= $new_job_days), 'New', R.Highlight) Highlight";
		}
		else {
		    $columns .= ", R.Highlight";
		}
		
		//Get Requisitions and Requisition Organization Levels Information
		$results = $RequisitionsObj->getReqAndReqOrgLevelsInfo($columns, $where, "R.RequestID", "R.ListingPriority, R.Title", array($params));
			
		//Set the count
		$hit2 = $results['count'];
			
		if ($hit2 == 1) {
			foreach($results['results'] as $JL) {
				$RequestID = $JL ['RequestID'];
			}
			$listreq = "Y";
		}
			
			if ($hit2 > 1) {
				
				$availablepositionlist = "Y";
				
				if ($categorylist == "Y") {
					if ($source != "IND") {
					    if($InternalVCode) {
					        echo "Employee Application: &nbsp;";
					    }
						echo '<a href="';
						if (FROM_SRC == "USERPORTAL") {
							echo USERPORTAL_HOME;
						} else {
							echo PUBLIC_HOME;
						}

						if($InternalVCode != "" && FROM_SRC != "USERPORTAL") {
						    echo 'internalRequisitions.php?';
						}
						else {
						    echo 'index.php?';
						}
						echo 'OrgID=' . $OrgID;
						if ($MultiOrgID) {
						    echo '&MultiOrgID=' . $MultiOrgID;
						}
						if ($InternalVCode) {
						    echo '&InternalVCode=' . $InternalVCode;
						}
						
						echo '"';
						echo '>';						
						echo 'Back to Category List</a><br><br>';
                        if ($OrgID == "I20140922") {
                           $LOCA=explode(':',$LOCATION);
                           echo ' for: ' . $LOCA[1];
                        }

					} // end source
					echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
				} // end categorylist
				
				$anc = 0;

				
				if(is_array($results['results'])) {
				
				foreach($results['results'] as $JL) {
					$anc ++;
					
					$req_det_info          =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $JL['RequestID']);
					$emp_status_levels     =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
					$status                =   $emp_status_levels[$JL ['EmpStatusID']];
					
					if (($SinglePageListing == "Y") && ($LinkToNewPage == "Y")) {
						
						echo '<a href="';
						if (FROM_SRC == "USERPORTAL") {
							echo USERPORTAL_HOME;
						} else {
							echo PUBLIC_HOME;
						}
						echo 'index.php?';
						echo 'OrgID=' . $OrgID;
						if ($MultiOrgID) {
							echo '&MultiOrgID=' . $MultiOrgID;
						} 
						echo '&RequestID=' . $JL ['RequestID'];
						if (($olnew) && ($slnew)) {
							echo '&olnew=' . $olnew . '&slnew=' . $slnew;
						}
						if ($InternalVCode) {
							echo '&InternalVCode=' . $InternalVCode;
						}
						echo '&navpg=listings';
						echo '"';
						if ($IFRAME == "Y") {
							echo ' target="_blank"';
						}
						echo '>';
						echo '<b>' . $JL ['Title'] . '</b>';
						if ($status != "") {
							if ($OrgID != "I20120901") { // American Engineering Testing
								echo '&nbsp;(' . $status . ')';
							}
						}
						echo '</a>';
						echo '<br>';
					} elseif ($SinglePageListing != "Y") {
						
						echo '<a href="';
						if (FROM_SRC == "USERPORTAL") {
							echo USERPORTAL_HOME;
						} else {
							echo PUBLIC_HOME;
						}
						echo 'index.php?';
						echo 'OrgID=' . $OrgID;
						if ($MultiOrgID) {
							echo '&MultiOrgID=' . $MultiOrgID;
						} 
						echo '&RequestID=' . $JL ['RequestID'];
						if (($olnew) && ($slnew)) {
							echo '&olnew=' . $olnew . '&slnew=' . $slnew;
						}
						if ($InternalVCode) {
							echo '&InternalVCode=' . $InternalVCode;
						}
						echo '&navpg=listings';	
						echo '"';
						
						if ($IFRAME == "Y") {
							echo ' target="_blank"';
						}
						echo '>';
						echo '<b>' . $JL ['Title'] . '</b>';
						if ($status != "") {
							if ($OrgID != "I20120901") { // American Engineering Testing
								echo '&nbsp;(' . $status . ')';
							}
						}
						echo '</a>';
						
						if($JL['Highlight'] == "New") {
						    echo '&nbsp;&nbsp;';
						    echo $new_job_image;
						}
						else if($JL['Highlight'] == "Hot") {
						    echo '&nbsp;&nbsp;';
						    echo $hot_job_image;
						}
						
						echo '<br>';
					} else {
						
						echo '<a href="#job' . $anc . '"><b>' . $JL ['Title'] . '</b></a><br>';
						
						//Set columns
						$columns = array("EmpStatusID", "RequestID", "Title", "Description", "FormID", "InternalFormID");
						//Set parameters
						$params = array(":OrgID"=>$OrgID, ":RequestID"=>$JL ['RequestID']);
						//Set condition
						$where  = array("OrgID = :OrgID", "RequestID = :RequestID", "Active = 'Y'");
						
						$where[] = "PostDate < NOW() AND ExpireDate > NOW()";
						
						//Get requisitions information
						$resultsIN = $RequisitionsObj->getRequisitionInformation($columns, $where, "", "Title", array($params));
						
						$delayreqs = "";
						if(is_array($resultsIN['results'])) {
							foreach ($resultsIN['results'] as $REQ) {
								$delayreqs .= PrintReq ( $OrgID, $REQ ['RequestID'], $anc );
							} // end foreach	
						}
						
					} // end if else SinglePageListing
				} // end foreach
			}
			if ($IFRAME != "Y") {
				echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
			}
			
			if ($SinglePageListing == "Y") {
				// echo '<hr noshade color="#' . $COLOR['Heavy'] . '">';
				echo $delayreqs;
			}
		} // end if hit2 > 1
		
		if ($listreq == "Y") {
			
			//Set columns 
			$columns = array("EmpStatusID", "RequestID", "Title", "Description", "FormID", "InternalFormID");
			//Set parameters
			$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
			//Set condition
			$where  = array("OrgID = :OrgID", "RequestID = :RequestID", "Active = 'Y'");
			
			$where[] = "PostDate < NOW() AND ExpireDate > NOW()";
			
			//Get requisitions information
			$results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", "Title", array($params));
			$hit3 = $results['count'];
			
			if ($hit3 > 0) {
				
				if ($categorylist == "Y") {
					if ($source != "IND") {
					    if($InternalVCode) {
					        echo "Employee Application: &nbsp;";
					    }
						echo '<a href="';
						if (FROM_SRC == "USERPORTAL") {
							echo USERPORTAL_HOME;
						} else {
							echo PUBLIC_HOME;
						}
						echo 'index.php?';
						echo 'OrgID=' . $OrgID;
						if ($MultiOrgID) {
							echo '&MultiOrgID=' . $MultiOrgID;
						}
						if ($InternalVCode) {
							echo '&InternalVCode=' . $InternalVCode;
						}
						echo '"';
						echo '>';
						echo 'Back to Category List</a><br><br>';
					} // end source
				} // end categorylist
				
				if ($availablepositionlist == "Y") {
					if ($source != "IND") {
						
						echo '<a href="';
						if (FROM_SRC == "USERPORTAL") {
							echo USERPORTAL_HOME;
						} else {
							echo PUBLIC_HOME;
						}
						echo 'index.php?';
						echo 'OrgID=' . $OrgID;
						if ($MultiOrgID) {
							echo '&MultiOrgID=' . $MultiOrgID;
						} 
						if (($olnew) && ($slnew)) {
							echo '&navpg=listings';
							echo '&olnew=' . $olnew . '&slnew=' . $slnew;
						}
						if ($InternalVCode) {
							echo '&InternalVCode=' . $InternalVCode;
						}
						
						echo '"';
						
						if ($IFRAME == "Y") {
							echo ' target="_blank"';
						}
						
						echo '>';
						echo 'Available Positions</a>&nbsp;>&nbsp;';
					} // end source
				} // end availablepositionlist
				
				echo 'Job Description';
				echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
				
				if(is_array($results['results'])) {
					foreach ($results['results'] as $REQ) {
						echo PrintReq ( $OrgID, $REQ ['RequestID'], '' );
					}	
				}
			} else { // hit
				
				echo "We are sorry. This job posting has expired.";
			} // end hit
		}
	} // end hit
	
	?>
</td>
	</tr>
</table>
<br>
<?php 
} 
?>
