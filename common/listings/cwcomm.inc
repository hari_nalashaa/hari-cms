<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

// Set ORGLEVELS array
$ORGLEVELS  =   array ();
$results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
$orgcnt     =   count($results);

for($r = 0; $r < count($results); $r++) {
    $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
} // end foreach

// Set ORGLEVELDATA array
$ORGLEVELDATA   =   array ();
$results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, $orgcnt, 'less_equal');

for($od = 0; $od < count($results); $od++) {
    $ORGLEVELDATA [$results[$od]['OrgLevelID']][$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
}

// get limited levels to restrict selection if not active
$results    =   G::Obj('Organizations')->getActiveLevels($OrgID, $MultiOrgID, $Internal);

$ACTIVE     =   array();
foreach ($results['results'] as $AL) {
  $ACTIVE[$AL['OrgLevelID']][$AL['SelectionOrder']]=$AL['cnt'];
}

$anyhit     =   count($ACTIVE);

if(isset($_REQUEST['RequestID']) == "") {
?>

<div class="container">
<?php if ($OrgID == "I20141112") { ?>
    <div id="hero" class="carousel slide">
        <div class="carousel-inner">
            <article class="item active">
				<?php
				if ($MultiOrgID == "") { ?>
                	<img class="img-responsive" alt="About Us" src="https://www.irecruit-us.com/images/I20141112/cwc_careers.jpg">
					<?php
                }
                ?>
            </article>
        </div>
    </div>
<?php } ?>

    <br />
    <div class="row">
        <div id="subnav" class="col-sm-3">
<?php
if($_REQUEST['level'] == "") {

	if($anyhit > 0) {
	// high level navigation
    
	for($i=1; $i<=count($ORGLEVELS); $i++) {

	  if($i==1) { $mt=0; } else { $mt=10; }

	  echo "<div style=\"margin:" . $mt . "px 0 5px 0;\">";
	  echo "<strong style=\"font-size:14pt;\">" . $ORGLEVELS[$i] . ":</strong>";
	  echo "</div>";

           foreach($ORGLEVELDATA[$i] as $ii=>$OLD) {
              if($ACTIVE[$i][$ii] > 0) {
                echo "<div style=\"margin:0 0 0 5px;\">";
                echo '<a href="';
                if (FROM_SRC == "USERPORTAL") {
                   echo USERPORTAL_HOME;
                } else {
                   echo PUBLIC_HOME;
                }
                echo 'index.php?';
                echo 'OrgID=' . $OrgID;
                if ($MultiOrgID) {
                   echo '&MultiOrgID=' . $MultiOrgID;
                }
                if ($InternalVCode) {
                    echo '&InternalVCode=' . $InternalVCode;
                }
                echo '&level=' . $i . ":" . $ii;
                echo '">';

                echo $OLD . " (" . $ACTIVE[$i][$ii] . ")";
                echo '</a>';
                echo "</div>";
              }
           } // end foreach
	} // end for

	} else { // if anyhit

        echo '<p>There are no openings at the current time.</p>';

	}

} else { // no request level
    
  list($MENU, $LEVEL) = explode(':', $_REQUEST['level']);

  if(isset($LEVEL)) {

        $results= getList($MENU,$LEVEL);

        $hit = $results ['count'];

        echo "<div style=\"margin:0 0 5px 0;\">";
        echo "<a href=\"javascript:history.back()\">Go Back to All Job Listings</a><br><br>";
        echo "<strong style=\"font-size:14pt;\">" . $ORGLEVELS[$MENU] . ":</strong>";
        echo "&nbsp;";
        echo "<strong style=\"font-size:14pt;\">" . $ORGLEVELDATA[$MENU][$LEVEL] . ":</strong>";
        echo "</div>";

        echo '<div style="margin-top:10px;">';

        if ($hit == 1) {

           if (FROM_SRC == "USERPORTAL") {
             $url = USERPORTAL_HOME;
           } else {
             $url = PUBLIC_HOME;
           }
           $url .= 'index.php?';
           $url .= 'OrgID=' . $OrgID;
           if ($MultiOrgID) {
             $url .= '&MultiOrgID=' . $MultiOrgID;
           } 

           $url .= '&RequestID=' . $results['results'][0]['RequestID'];
           if ($InternalVCode) {
                $url .= '&InternalVCode=' . $InternalVCode;
           }
    	   echo '<script type="text/javascript">';
    	   echo 'window.location = "' . $url . '"';
    	   echo '</script>';

        } else if ($hit > 1) {

            // ######################## Common where condition code end ################################
        	$STATUSLEVELS          =   array ();
        	//Get Default RequisitionFormID
        	$RequisitionFormID     =   G::Obj('RequisitionForms')->getDefaultRequisitionFormID($OrgID);
        	$STATUSLEVELS          =   G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($OrgID, $RequisitionFormID);
                
            if (is_array ( $results ['results'] )) {
    
                foreach ( $results ['results'] as $REQ ) {
    
                    echo '<div style="padding:5px;">';
        
                    echo '<a href="';
                    if (FROM_SRC == "USERPORTAL") {
                       echo USERPORTAL_HOME;
                    } else {
                       echo PUBLIC_HOME;
                    }
                    echo 'index.php?';
                    echo 'OrgID=' . $OrgID;
                    if ($MultiOrgID) {
                       echo '&MultiOrgID=' . $MultiOrgID;
                    } 

                    echo '&RequestID=' . $REQ ['RequestID'];
                    if ($InternalVCode) {
                        echo '&InternalVCode=' . $InternalVCode;
                    }
		    echo '&navpg=listings';
                    echo '">';
                    echo $REQ ['Title'] . '</a>';
        
                    if($REQ['Highlight'] == "New") {
                        echo '&nbsp;&nbsp;';
                        echo $new_job_image;
                    }
                    else if($REQ['Highlight'] == "Hot") {
                        echo '&nbsp;&nbsp;';
                        echo $hot_job_image;
                    }        
        
                    if ($REQ ['EmpStatusID']) {
                       echo '&nbsp;&nbsp;';
                       echo '(' . $STATUSLEVELS [$REQ ['EmpStatusID']] . ')';
                    }
                    
                    echo '</div>';
    
                } // end foreach
    
            } // end is array

        } else { // hit > 0
            echo '<p>There are no openings at the current time.</p>';
        }

        echo '</div>';

  } // end LEVEL

} // end no request level


?>
        </div>
        <div id="main" class="col-sm-9">
        	<style>
        	.arrow {
        	   float: none !important;
        	}
        	</style>
			<?php
			if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
    			 echo $OrganizationDescription;
			}
            ?>
        </div>
    </div>
</div>

<?php

} else if ($RequestID) { // end if navigation needed

    echo "<a href=\"javascript:history.back()\">Go Back to Job Listings</a><br><br>";
    echo PrintReq ( $OrgID, $RequestID, '' );

} // end else if navigation needed

function getList ($MENU, $LEVEL) {

	global $OrgID, $MultiOrgID, $Internal;

	// Get Requisition Org Levels Information
    $where      =   array ("OrgID = :OrgID", "OrgLevelID = :OrgLevelID", "SelectionOrder= :SelectionOrder");
    $params     =   array (":OrgID" => $OrgID, ":OrgLevelID" => $MENU, ":SelectionOrder" => $LEVEL);
    $resultsIN  =   G::Obj('Requisitions')->getRequisitionOrgLevelsInfo ( "*", $where, "OrgLevelID, SelectionOrder", array ($params) );

    $REQUESTIDS =   "";
    foreach ($resultsIN['results'] as $OL) {
       $REQUESTIDS .= "'" . $OL['RequestID'] . "',";
    }
    $REQUESTIDS =   substr($REQUESTIDS,0,-1);

    $where      =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "Active = 'Y'");
    $where[] = "NOW() BETWEEN PostDate AND ExpireDate";
    $params     =   array (":OrgID" => $OrgID, ":MultiOrgID" => $MultiOrgID);

    if($REQUESTIDS !="") {
        $where [] = "RequestID in ($REQUESTIDS)";
    } else {
        $where [] = "RequestID = ''";
    }

    if ($Internal == "Y") {
        $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    } else {
        $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
    }
    
    $highlight_req_settings = G::Obj('HighlightRequisitions')->getHighlightRequisitionSettings($OrgID);
    $new_job_days = $highlight_req_settings['NewJobDays'];

    $columns = "Title, EmpStatusID, RequestID, Description";
    if($new_job_days > 0) {
        $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
    }
    else {
        $columns .= ", Highlight";
    }
    // Get Requisitions Information
    $results    =   G::Obj('Requisitions')->getRequisitionInformation ( $columns, $where, "", "Title", array ($params) );

	return $results;

} // end function
?>
