<?php
$title = "Listings";

require_once COMMON_DIR . 'listings/SocialMedia.inc';
require_once COMMON_DIR . 'listings/PrintRequisition.inc';

//Get Organization Detail Information
$org_detail_info            =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView, OrganizationDescription");
$OrganizationDescription    =   $org_detail_info['OrganizationDescription'];

if ($RequestID) {
    
    if($InternalVCode) {
        echo "Employee Application: &nbsp;";
    }    
	echo '<a href="';
	if (FROM_SRC == "USERPORTAL") {
		echo USERPORTAL_HOME;
	} else {
		echo PUBLIC_HOME;
	}

	if($InternalVCode != "" && FROM_SRC != "USERPORTAL") {
	    echo 'internalRequisitions.php?';
	}
	else {
	    echo 'index.php?';
	}
	echo 'OrgID=' . $OrgID;
	if ($MultiOrgID) {
	    echo '&MultiOrgID=' . $MultiOrgID;
	}
	if ($InternalVCode) {
	    echo '&InternalVCode=' . $InternalVCode;
	}	
	echo '"';
	echo '>';	
	echo 'Back to Category List</a><br><br>';
	
	echo PrintReq ( $OrgID, $RequestID, '' );
} else {

	// Set ORGLEVELS array
	$ORGLEVELS  =   array ();
	$results    =   G::Obj('OrganizationLevels')->getOrgLevels($OrgID, $MultiOrgID);
	
	for($r = 0; $r < count($results); $r++) {
	    $ORGLEVELS [$results[$r]['OrgLevelID']] = $results[$r]['OrganizationLevel'];
	} // end foreach
	
	// Set ORGLEVELDATA array
	$ORGLEVELDATA   =   array ();
	$results        =   G::Obj('OrganizationLevels')->getOrgLevelDataByOrgLevelIDCount($OrgID, $MultiOrgID, '', 'none');
	
	for($od = 0; $od < count($results); $od++) {
	    $ORGLEVELDATA [$results[$od]['OrgLevelID']][$results[$od]['SelectionOrder']] = $results[$od]['CategorySelection'];
	}
	
    echo '<div>';
	
    if ($OrganizationDescription && !preg_match ( '/internalRequisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		echo $OrganizationDescription;
		echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
	}
	
	//Set condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "Active = 'Y'");
	$where[] = "NOW() BETWEEN PostDate AND ExpireDate";
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	
	if ($Internal == "Y") {
	    $where[] = "(PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	} else {
	    $where[] = "(PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	}
	
	$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
	$new_job_days = $highlight_req_settings['NewJobDays'];
	
	$columns = "Title, EmpStatusID, RequestID, Description";
	if($new_job_days > 0) {
	    $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
	}
	else {
	    $columns .= ", Highlight";
	}
	
	//Get Requisitions Information
	$results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", "Title", array($params));
	
	//Get count
	$hit = $results['count'];
	
	if ($hit > 0) {
		
		$color = "#eeeeee";
		
		echo '<div style="min-width:850px;background-color:' . $color . ';">';
		
		echo '<div style="float:left;width:275px;padding:3px 0px 3px 10px;vertical-align:center;">';
		echo '<strong>Position</strong>';
		echo '</div>';
		
		echo '<div style="float:left;width:200px;padding:3px 0px 3px 10px;text-align:center;">';
		echo '<strong>Employment Type</strong>';
		echo '</div>';

		//Set where condition
                $where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
                //Set the parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);

		//Get Organization Levels Information
		$resultsIN = $OrganizationsObj->getOrganizationLevelsInfo("OrgLevelID", $where, 'OrgLevelID', array($params));
		
		foreach($resultsIN['results'] as $ROL) {
			echo '<div style="float:left;width:150px;padding:3px 0px 3px 10px;">';
			echo '<strong>';
			echo $ORGLEVELS [$ROL ['OrgLevelID']];
			echo '</strong>';
			echo '</div>';
		} // end while
		
		echo '<div style="clear:both;"></div>';
		
		echo '</div>';

                if ($OrgID == "I20181115") {
                  if(is_array($results['results'])) {

                    for($r = 0; $r < count($results['results']); $r++) {

                        //Set condition
                        $where      =   array("OrgID = :OrgID", "RequestID = :RequestID", "OrgLevelID = '1'");
                        //Set parameters
                        $params     =   array(":OrgID"=>$OrgID,":RequestID"=>$results['results'][$r]['RequestID']);
                        //Get Requisition Org Levels Information
                        $resultsSORT =   $RequisitionsObj->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID", array($params));

                        if(is_array($resultsSORT['results'])) {
                               foreach($resultsSORT['results'] as $ROL) {
                                    $results['results'][$r]['SortLocation'] = $ORGLEVELDATA [$ROL ['OrgLevelID']] [$ROL ['SelectionOrder']];
                               } // end foreach
                        }

                    } // end foreach
                  }
		  array_multisort( array_column($results['results'], "SortLocation"), SORT_ASC, $results['results']);
                }
		
		//Get Organization Levels Information
		if(is_array($results['results'])) {
			foreach ($results['results'] as $REQ) {
				
			    $STATUSLEVELS          =   array ();
			    //Get Default RequisitionFormID
			    $req_det_info          =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $REQ ['RequestID']);
			    $STATUSLEVELS          =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $req_det_info['RequisitionFormID']);
			     
				if ($color == "#ffffff") {
					$color = "#eeeeee";
				} else {
					$color = "#ffffff";
				}
				
				echo '<div style="background-color:' . $color . ';">';
				
				echo '<div style="float:left;width:325px;padding:5px 0 5px 10px;">';
				
				echo '<a href="';
				if (FROM_SRC == "USERPORTAL") {
					echo USERPORTAL_HOME;
				} else {
					echo PUBLIC_HOME;
				}
				echo 'index.php?';
				echo 'OrgID=' . $OrgID;
				if ($MultiOrgID) {
					echo '&MultiOrgID=' . $MultiOrgID;
				} 
				echo '&RequestID=' . $REQ ['RequestID'];
				if ($InternalVCode) {
					echo '&InternalVCode=' . $InternalVCode;
				}
				echo '&navpg=listings';
				echo '">';
				if ($OrgID == "I20190125") { echo "<strong>"; }
				echo $REQ ['Title'];
				if ($OrgID == "I20190125") { echo "</strong>"; }
				echo '</a>';
				
				if($REQ['Highlight'] == "New") {
				    echo '&nbsp;&nbsp;';
				    echo $new_job_image;
				}
				else if($REQ['Highlight'] == "Hot") {
				    echo '&nbsp;&nbsp;';
				    echo $hot_job_image;
				}
				
				echo '</div>';
				
				echo '<div style="float:left;width:150px;padding:5px 0 5px 10px;">';
				
				if ($REQ ['EmpStatusID']) {
					echo $STATUSLEVELS [$REQ ['EmpStatusID']];
				}
				echo '</div>';
				
				//Set condition
				$where      =   array("OrgID = :OrgID", "RequestID = :RequestID");
				//Set parameters
				$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$REQ ['RequestID']);
				//Get Requisition Org Levels Information
				$resultsIN2 =   $RequisitionsObj->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID", array($params));
				
				if(is_array($resultsIN2['results'])) {
					foreach($resultsIN2['results'] as $ROL) {
						echo '<div style="float:left;width:150px;padding:5px 0 5px 10px;">';
						echo $ORGLEVELDATA [$ROL ['OrgLevelID']] [$ROL ['SelectionOrder']];
						echo '</div>';
					} // end foreach
				}
							
				echo '<div style="clear:both;"></div>';
				
				echo '</div>';
			} // end foreach
		
		}
		//echo '</td></tr></table>';
		echo '</div>';
	} else { // hit > 0
		
		echo '<p>There are no openings at the current time.</p>';
	}
	
	echo '<div style="clear:both;"></div>';
	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
} // end else
?>
