<?php
//Get user information
$user_details   =   $IrecruitUsersObj->getUserInfoByUserID($USERID, "FirstName, LastName, EmailAddress");

$EmailAddress   =   $user_details['EmailAddress'];
$UserName       =   $user_details['LastName'] . " " . $user_details['FirstName'];

$to             =   'lstrong@cmshris.com';
$subject        =   'iRecruit Help Question from: ' . $USERID . ' - ' . $EmailAddress;
$message        =   'The following question was asked from the Help section of iRecruit.' . "\r\n\r\n";
$message       .=   $question;

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

// Set who the message is to be sent to
$PHPMailerObj->addAddress ( $to );
// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML ( $message );
// Set who the message is to be sent from
$PHPMailerObj->setFrom ( $EmailAddress, $UserName );
// Set an alternative reply-to address
$PHPMailerObj->addReplyTo ( $EmailAddress, $UserName );
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/plain';

if ($DEVELOPMENT != "Y") {
	$PHPMailerObj->send ();
}
?>
