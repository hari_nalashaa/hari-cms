<?php
$ERROR .= "";

//Validate the information
G::Obj('GetFormPostAnswer')->POST   =   $REQUEST_DATA;
G::Obj('GetFormPostAnswer')->FILES  =   $FILES_DATA;

//Get Modified POST Answers
$POSTANSWERS    =   G::Obj('GetFormPostAnswer')->getPostDataAnswersOfFormQuestions($OrgID, $FormID, $REQUEST_DATA['SectionID']);

//Validate the information
G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']   =   $POSTANSWERS;
G::Obj('ValidateApplicationForm')->FORMDATA['FILES']     =   $FILES_DATA;

//Validate Required Fields By SectionID
$errors_info    =   G::Obj('ValidateApplicationForm')->validateApplicationForm($OrgID, $FormID, $REQUEST_DATA['SectionID'], $HoldID);
$errors_list    =   $errors_info['ERRORS'];

if(isset($errors_list) && is_array($errors_list)) {
    foreach ($errors_list as $error_que_id=>$error_que_msg) {
        $ERROR  .=  $error_que_msg;
    }
}

############## Check start to validate the mobile number is sms capable or not. ##############
if($_POST['cellphonepermission'] == "Yes") {
    $applicant_cell_number_sms	=	"false";
    
    $applicant_cell_number		=	$_POST['cellphone1'].$_POST['cellphone2'].$_POST['cellphone3'];
    
    $applicant_cell_number		=	str_replace(array("-", "(", ")", " "), "", $applicant_cell_number);
    
    if($applicant_cell_number != "") {
        //Add plus one before number
        if(substr($applicant_cell_number, 0, 2) != "+1") {
            $applicant_cell_number	=	"+1".$applicant_cell_number;
        }
        
        //Get LoopUp Phone number information
        $lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $applicant_cell_number);
        
        //Get the Protected properties from SMS object, by extending the object through ReflectionClass
        $reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
        $property       		=   $reflection->getProperty("properties");
        $property->setAccessible(true);
        $properties				=   $property->getValue($lookup_phone_num_info['Response']);
        
        if($properties['carrier']['type'] == 'mobile'
            || $properties['carrier']['type'] == 'voip') {
                $applicant_cell_number_sms	=	"true";
            }
            
            if($applicant_cell_number_sms == "false") {
                $cellphone_error_msg	=	' - Cell Phone you have you have opt-in to text to this cellphone number.';
                $cellphone_error_msg	.=	' Only sms capable number is be accepted';
                $ERROR .= $cellphone_error_msg . "\\n";
            }
    }
    else {
        $cellphone_error_msg	=	' - Cell Phone you have you have opt-in to text to this cellphone number.';
        $cellphone_error_msg	.=	' Please fill the cell phone with sms capable number';
        $ERROR .= $cellphone_error_msg . "\\n";
    }
}
############## Check start to validate the mobile number is sms capable or not. ##############

return $ERROR;
?>