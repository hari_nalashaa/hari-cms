<?php
function URLProtect($title) {
	$title = str_replace ( "#", "sharp", $title );
	$title = str_replace ( "/", "_", $title );
	$title = str_replace ( "$", "", $title );
	$title = str_replace ( "&amp;", "and", $title );
	$title = str_replace ( "&", "and", $title );
	$title = str_replace ( "+", "plus", $title );
	$title = str_replace ( ",", "", $title );
	$title = str_replace ( ":", "", $title );
	$title = str_replace ( ";", "", $title );
	$title = str_replace ( "=", "equals", $title );
	$title = str_replace ( "?", "", $title );
	$title = str_replace ( "@", "at", $title );
	$title = str_replace ( "<", "", $title );
	$title = str_replace ( ">", "", $title );
	$title = str_replace ( "%", "", $title );
	$title = str_replace ( "{", "", $title );
	$title = str_replace ( "}", "", $title );
	$title = str_replace ( "|", "", $title );
	$title = str_replace ( "\\", "", $title );
	$title = str_replace ( "^", "", $title );
	$title = str_replace ( "~", "", $title );
	$title = str_replace ( "[", "", $title );
	$title = str_replace ( "]", "", $title );
	$title = str_replace ( "`", "", $title );
	$title = str_replace ( "'", "", $title );
	$title = str_replace ( "\"", "", $title );
	$title = str_replace ( "-", "_", $title );
	$title = str_replace ( " ", "_", $title );
	
	return $title;
}
function DisplayRole($str) {
	$str = str_replace ( "master_", "", $str );
	$str = str_replace ( "_", " ", $str );
	$str = ucwords ( $str );
	$str = str_replace ( " W", " w", $str );
	if ($str == '') {
		$str = 'Other';
	}
	return $str;
}
?>