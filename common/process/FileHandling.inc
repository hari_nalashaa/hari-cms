<?php
// FILEHANDLING
$dir = IRECRUIT_DIR . 'vault/' . $OrgID;

if (! file_exists ( $dir )) {
	mkdir ( $dir, 0700 );
	chmod ( $dir, 0777 );
}

$apatdir = $dir . '/applicantattachments';

if (! file_exists ( $apatdir )) {
	mkdir ( $apatdir, 0700 );
	chmod ( $apatdir, 0777 );
}


//Get Purpose names list
$purpose    =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

//Set parameters
$params     =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID);
// Set Form Question Fields
$where      =   array ("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID = 8", "Active = 'Y'");
//Get FormQuestions Information
$resultsIN  =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder", array ($params) );

if (is_array ( $resultsIN ['results'] )) {
	foreach ( $resultsIN ['results'] as $FIL ) {
		
		if ($_FILES [$FIL ['QuestionID']] ['type'] != "") {
			
		    //Get Purpose Names List
            $purpose        =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);
		    
            $path_info      =   pathinfo($_FILES [$FIL ['QuestionID']] ['name']);
            $extension      =   $path_info['extension'];
			
			//Applicant Attachments Information
			$applicant_attach_info   =   array (
                        					"OrgID"            =>  $OrgID,
                        					"ApplicationID"    =>  $ApplicationID,
                        					"TypeAttachment"   =>  $FIL ['QuestionID'],
                        					"PurposeName"      =>  $purpose [$FIL ['QuestionID']],
                            			    "FileType"         =>  $extension
                            			 );
			//Insert applicant attachments information
			G::Obj('Attachments')->insApplicantAttachments ( $applicant_attach_info );
			
			$old_file_name   =   $apatdir . '/' . $HoldID."-".$purpose [$FIL ['QuestionID']].".".$extension;
			$new_file_name   =   $apatdir . '/' . $ApplicationID."-".$purpose [$FIL ['QuestionID']].".".$extension;
			
			//Rename old file to new name
			rename($old_file_name, $new_file_name);
			
			if($FIL ['QuestionID'] == 'resumeupload' && $feature['ResumeParsing'] == "Y") {
			    //User Portal Resume
			    G::Obj('Sovren')->processPublicPortalResumeForm($OrgID, $FormID, $RequestID, $ApplicationID, $filename);
			}
		}
	} // end foreach
}
// END FILEHANDLING
?>