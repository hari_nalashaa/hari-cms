<?php
$monster_cookie_info    =   $MonsterObj->getMonsterSessionCookieByCookieApp($HoldID, $RequestID);
// Posted Data
$data                   =   $CurlObj->get ( $monster_cookie_info ['MonsterRequestURL'] );

if($data != "Bad Request") {

    $data           =   str_replace ( "u000du000a", "<br>", $data );
    $data           =   str_replace ( '\u000d\u000a', "<br>", $data );
	
    // Decode json data
    $POST           =   json_decode ( $data, true );
    
    $message        =   $POST ['CoverLetter'];
    $fileContent    =   $POST ['FileContents'];
    $ext            =   $POST ["FileExt"];
    $vendorfield    =   explode ( "*", $POST ["VendorField"] );
    $OrgID          =   $vendorfield [0];
    $MultiOrgID     =   is_null($vendorfield [1]) ? '' : $vendorfield [1];
    $RequestID      =   $POST ['JobRefID'];
    $FormID         =   $RequisitionDetailsObj->getFormID($OrgID, $MultiOrgID, $RequestID);
    
	if(substr($ext, 0, 1) == ".") $ext = substr($ext, 1);
	
	$dir = IRECRUIT_DIR . 'vault/' . $OrgID;
	if (! file_exists ( $dir )) {
		mkdir ( $dir, 0700 );
		chmod ( $dir, 0777 );
	}
	$apatdir = $dir . '/applicantattachments';
	if (! file_exists ( $apatdir )) {
		mkdir ( $apatdir, 0700 );
		chmod ( $apatdir, 0777 );
	}
	
	//Get Purpose names list
	$purpose        =   $AttachmentsObj->getPurposeNamesList($OrgID, $FormID);
	
	// Converting byte code data to text and writing it into file
	$resumedata = "";
	foreach ( $fileContent as $byte ) {
		$resumedata .= chr ( $byte );
	}
	
	// Insert Applicant Information
	$monster_insert_info   =   array(
                                    'MonsterCandidateInfo'  =>  serialize ( $data ),
                                    'FileContents'          =>  serialize ( $fileContent ),
                                    'CoverLetter'           =>  $POST ['CoverLetter'],
                                    'EmailAddress'          =>  $POST ['EmailAddress'],
                                    'FirstName'             =>  $POST ['FirstName'], 
                                    'JobRefID'              =>  $POST ['JobRefID'],
                                    'OrgID'                 =>  $OrgID, 
                                    'MultiOrgID'            =>  $MultiOrgID, 
                                    'LastName'              =>  $POST ['LastName'],
                                    'PhoneNumber'           =>  $POST ['PhoneNumber'], 
                                    'ApplicationID'         =>  $ApplicationID,
                                    'MethodType'            =>  'REQUEST', 
                                    'CreatedTimeStamp'      =>  'NOW()'
	                               );
	//Set Monster Post Data
	$MonsterObj->insMonsterPostData($monster_insert_info);
	
	// Create resume based on binary data
	if ($resumedata != "" && $ext != "") {
		if(!file_exists($fileName)) {
			$fileName = $apatdir . '/' . $ApplicationID . '-resume' . '.' . $ext;
			touch ( $fileName );
			$fp = fopen ( $fileName, 'w' );
			fwrite ( $fp, $resumedata );
			fclose ( $fp );
	
			chmod ( $fileName, 0666 );
		}
	
		//Set resume attachment where condition
		$monster_att_where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "TypeAttachment = 'resumeupload'");
		//Set parameters
		$monster_att_params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
		//Get Applicant Attachments Information
		$mon_att_info_res     =   $AttachmentsObj->getApplicantAttachments("*", $monster_att_where, '', array($monster_att_params));
	
		if($mon_att_info_res['count'] == 0) {
			//Set Applicant Attachment Information
            $attachment_info = array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "TypeAttachment"=>'resumeupload', "PurposeName"=>'resume', "FileType"=>$ext);
			//Insert Applicant Attachment Information
			$AttachmentsObj->insApplicantAttachments($attachment_info);
		}
	}
	// Create coverletter in text file
	if ($POST ['CoverLetter'] != "") {
		$fileName = $apatdir . '/' . $ApplicationID . '-coverletter.txt';
		touch ( $fileName );
		$fp = fopen ( $fileName, 'w' );
		fwrite ( $fp, str_replace ( array (
		"<br>",
		"<br/>",
		"<br / >"
				), "\n", $POST ['CoverLetter'] ) );
		fclose ( $fp );

		chmod ( $fileName, 0666 );

		//Set Applicant Attachment Information
		$attachment_info = array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "TypeAttachment"=>'coverletterupload', "PurposeName"=>'coverletter', "FileType"=>'txt');
		//Insert Applicant Attachment Information
		$AttachmentsObj->insApplicantAttachments($attachment_info);
	}	
}
?>
