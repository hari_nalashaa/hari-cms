<?php
$ERROR = "";

// backward compatible to verify not requisitionID - jobid
if($REQUEST_DATA['action'] != 'applicationedit') {
    if ($RequestID == "") {
		$ERROR .= " - You must have at least one position selected." . "\\n";
		G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp ($OrgID, $HoldID, 'RequestID', $RequestID );
	} else {
	    $multiorgid_req    =    G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
	    $requisition_title =    G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
	
		if ($requisition_title == "") {
			$ERROR .= " - The position selected is not valid." . "\\n";
			G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp ($OrgID, $HoldID, 'RequestID', $RequestID );
		}
	} // end else
}

if ($REQUEST_DATA['agree'] != "Y") {
	$ERROR .= " - Electronic Signature agreement indication is missing." . "\\n";
	G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp ($OrgID, $HoldID, 'signature', $REQUEST_DATA['signature'] );
}

if (isset($REQUEST_DATA['signature']) && strlen ( $REQUEST_DATA['signature'] ) < 3) {
	$ERROR .= " - Electronic Signature is missing." . "\\n";
	G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp ($OrgID, $HoldID, 'signature', $REQUEST_DATA['signature'] );
}

$req_details_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Active, DATE(ExpireDate) as ExpireDate", $OrgID, $RequestID);
$dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);

if($req_details_info['Active'] == 'N'
    || $dates_diff > 0) {
    header("Location:".PUBLIC_HOME."thankyou.php?RequestID=".$RequestID."&OrgID=".$OrgID.'&msg=reqexpired');
	exit;
}
?>
