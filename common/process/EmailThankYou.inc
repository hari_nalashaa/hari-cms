<?php
function sendThankYou($OrgID, $ApplicationID, $FormID, $OrganizationName) {
	global $DEVELOPMENT, $OrganizationsObj, $ApplicationsObj, $ApplicantsObj, $FormFeaturesObj, $RequisitionsObj, $RequisitionDetailsObj, $OrganizationDetailsObj, $PHPMailerObj;
	
	//Get Applicant Data	
	$APPDATA = $ApplicantsObj->getAppData($OrgID, $ApplicationID);
	
	$positions = '';
	$date = '';
	
	$i = 0;
	
	//Set columns
	$columns	=	array("RequestID", "MultiOrgID", "EntryDate");
	//Set where condition
	$where		=	array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
	//Set parameters
	$params		=	array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
	//Get Job Applications Public Information
	$results	=	$ApplicationsObj->getJobApplicationsInfo($columns, $where, '', '', array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $JA) {
		    $date         = $JA ['EntryDate'];
		    $MultiOrgID   = $JA ['MultiOrgID'];
		    if(is_null($MultiOrgID)) $MultiOrgID = '';
			$positions .= 'Req/JobID: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $MultiOrgID, $JA ['RequestID'] ) . ' ' . $RequisitionDetailsObj->getJobTitle ( $OrgID, $MultiOrgID, $JA ['RequestID'] ) . "\n";
			$i ++;
		} // end foreach
	}
	
	// get display text from main job id	
	$responsemessage = $FormFeaturesObj->getTextFromTextBlocks($OrgID, $FormID, 'ResponseEmail');
	
	$name = $APPDATA ['first'] . ' ' . $APPDATA ['middle'] . ' ' . $APPDATA ['last'];
	$email = $APPDATA ['email'];
	
	$to = $APPDATA ['email'];
	$subject = "Thank you from " . strip_tags ( $OrganizationName );
	$message = "
$responsemessage


-------------------------------------------<br>
Application ID: $ApplicationID<br>
Application Date: $date<br>
Name: $name<br>
Email: $email<br>
$positions<br>";
	
	$message .= "
-------------------------------------------<br>
           ** IMPORTANT **<br>
Please do not try to Reply-To this email.<br>
-------------------------------------------<br>
This is an automatically generated email with a<br>
non-functional email address. Please refer back<br>
to our website for contact email addresses and<br>
phone numbers.<br><br>

   Thank You.<br>
   Have a Great Day!<br>";
	
	$OE = $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);

	$PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
    
	if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
		// Set who the message is to be sent from
		$PHPMailerObj->setFrom ( $OE ['Email'], $OrganizationName );
		// Set an alternative reply-to address
		$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrganizationName );
	}
	
	if (DEVELOPMENT != "Y") {
	    // Set who the message is to be sent to
	    $PHPMailerObj->addAddress ( $to );
	    // Set the subject line
	    $PHPMailerObj->Subject = $subject;
	    // convert HTML into a basic plain-text alternative body
	    $PHPMailerObj->msgHTML ( $message );
	    // Content Type Is HTML
	    $PHPMailerObj->ContentType = 'text/html';
	    //Send email
	    $PHPMailerObj->send ();
	}
} // end function

?>
