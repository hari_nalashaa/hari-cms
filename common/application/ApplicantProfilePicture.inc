<?php
if ($ApplicationID != "") {
$app_profile_picture_val   =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, "ApplicantPicture");
} else {
$app_profile_picture_val    =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $USERID, $_REQUEST['RequestID'], 'ApplicantPicture');
}

$app_profile_img_src = IRECRUIT_HOME . "vault/".$OrgID."/applicant_picture/".$app_profile_picture_val;

$app_profile_picture = '';
if($ServerInformationObj->validateUrlContent($app_profile_img_src)) {
$app_profile_picture = 'Currently Uploaded: ';
$app_profile_picture .= '<img src="' . $app_profile_img_src . '" width="133" height="133">';
}

// Set switch to turn on and of entries //Set columns
$columns    =   "QuestionID";
//Set where condition
$where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$section_id);
//Get Get Social Media Profiles Questions Information
$results    =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));

$nrowsa     =   $results['count'];

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$QuestionID  = $row['QuestionID'];
		$app_profile_picture .= include 'DisplayQuestions.inc';
	} // end foreach
}

if ($nrowsa < 1) { $app_profile_picture = ''; }

return  $app_profile_picture;
?>
