<style>
.row {
    margin-right: 0px !important;
    margin-left: 0px !important;
    border: 1px solid red;
}
</style>
<?php
if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];

if (($ApplicationID) && isset($OrgID) && ($RequestID)) {

    $download_info  =   '';
    
    // get applicant data
    $APPDATA        =   G::Obj('Applicants')->getAppData ($OrgID, $ApplicationID );
    
    $ApplicantInfo  =   G::Obj('Applications')->getJobApplicationsDetailInfo("FormID", $OrgID, $ApplicationID, $RequestID);
    
    //Get MultiOrgID based on OrgID, RequestID
    $MultiOrgID     =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
    
    $applicationsize = count ( $APPDATA );
    
    // validate the FormID
    $FormID         =   $ApplicantInfo['FormID'];
    //Bind parameters
    $params         =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
    //Set condition
    $where          =   array("OrgID = :OrgID", "FormID = :FormID");
    //Get Distinct FormID's count
    $results        =   G::Obj('FormQuestions')->getFormQuestionsInformation(array("DISTINCT(FormID) AS DFormID"), $where, "", array($params));
    $hit            =   $results['count'];
	
	if ($hit < 1) {
		$FormID = 'STANDARD';
	}
	
	$form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
	
	foreach ( $form_sections_info as $form_section_id=>$form_section_info) {
	    $TITLES [$form_section_id] = $form_section_info['SectionTitle'];
	}
	
	//Titles for Affirmative Action, Veteran, Disabled Forms
	$TITLES ["AA"]     =   "Affirmative Action";
	$TITLES ["VET"]    =   "Veteran";
	$TITLES ["DIS"]    =   "Disabled";
	
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where     =   array("OrgID = :OrgID", "FormID = :FormID");
	// Query and Set Text Elements
	$results   =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
			$TextBlocks [$row ['TextBlockID']] = $row ['Text'];
		}	
	}
	
	
	if ($applicationsize < 30) {
		if ($OrgID == 'I20100603') {
			$download_info   .=    '<center><b>Applicant MUST complete an application at the time of interview</b></center><br>';
		}
	}
	
	// Read and Set Logos
	if (! $emailonly) {
		
		//Bind parameters
		$params       =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>(string)$MultiOrgID);
		//Set condition
		$where        =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
		//Get Organization Logos Information
		$resultsOL    =   G::Obj('Organizations')->getOrganizationLogosInformation(array("OrgID"), $where, "", array($params));
		$imgcnt       =   $resultsOL['count'];
		
		if ($imgcnt > 0) {
			$download_info   .=    '<img src="' . IRECRUIT_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br>';
		} // end imgcnt
	} // end email only

	//Bind parameters
	$params        =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where         =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = 'employment'");
	//Get FormQuestions Information
	$results       =   G::Obj('FormQuestions')->getFormQuestionsInformation("Question", $where, "", array($params));

	$Employment    =   $results['results'][0]['Question'];
	
	if ($Employment == '') {
		$Employment = 'Employment Status Desired:';
	}
	
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':ApplicationID'=>$ApplicationID);
	//Set condition
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "ApplicationID = :ApplicationID");
	//Get JobApplications Information
	$results   =   G::Obj('Applications')->getJobApplicationsInfo(array("RequestID", "Distinction", "EntryDate"), $where, '', 'Distinction', array($params));
		
	if(is_array($results['results'])) {
		foreach ($results['results'] as $JA) {
		    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $JA['RequestID']);
		    
			$ED = $JA ['EntryDate'];
			if ($JA ['Distinction'] == 'P') {
				$TT = 'Position Desired';
			} else {
				$TT = 'Other Position Desired';
			}

			$download_info .= '';
				
			$download_info .= "<div class='row'>";
			$download_info .= '<div class="col-lg-3 col-md-4 col-sm-3">'.$TT . ':</div>';
			$download_info .= '<div class="col-lg-9 col-md-8 col-sm-9">';
			$download_info .= '<b>(' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . ')</b>';
			$download_info .= '<b> ' . $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . '</b>';
			$download_info .= '</div>';
			$download_info .= '</div>';
		}
	}
	
	$download_info .= '<div class="row">';
	$download_info .= "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	$download_info .= '</div>';
	
	
	$download_info .= '<div class="row">';
	$download_info .= '<div class="col-lg-3 col-md-4 col-sm-3">Application ID:</div>';
	$download_info .= '<div class="col-lg-9 col-md-8 col-sm-9"><b>' . $ApplicationID . '</b></div>';
	$download_info .= '</div>';

	$download_info .= '<div class="row">';
	$download_info .= "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	$download_info .= '</div>';
	
	$download_info .= '<div class="row">';
	$download_info .= '<div class="col-lg-3 col-md-4 col-sm-3">Application Date:</div>';
	$download_info .= '<div class="col-lg-9 col-md-8 col-sm-9"><b>' . $ED . '</b></div>';
	$download_info .= '</div>';

	$download_info .= '<div class="row">';
	$download_info .= "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";  
	$download_info .= '</div>';
	
	$download_info .= '<div class="row">';
	$download_info .= '<div class="col-lg-3 col-md-4 col-sm-3">'.$Employment.'</div>';
	$download_info .= '<div class="col-lg-9 col-md-8 col-sm-9"><b>' . $APPDATA ['employment'] . '</b></div>';
	$download_info .= '</div>';

	$download_info .= '<div class="row">';
	$download_info .= '<div class="col-lg-12 col-md-12 col-sm-12">';
	$download_info .= '<br><br>';
	$download_info .= '</div>';
	$download_info .= '</div>';
	
	$download_info .= '<div class="row">';
	if ($TITLES [1] != "") {
	    $download_info .= '<div class="col-lg-12 col-md-12 col-sm-12">';
	    $download_info .= '<strong>' . $TITLES [1] . '</strong>';
	    $download_info .= '</div>';
	}
	$download_info .= '</div>';

	$AppQueAnswers =   $ApplicationFormQueAnsObj->getApplicationViewInfo($OrgID, $ApplicationID, $RequestID, $APPDATA);
	
	$personal_info_ques = array("name", "address", "address2", "faddress", "country", "email");
	$personal_info_excludes = array("first", "last", "middle", "county", "city", "state", "name", "address", "address2", "faddress", "country", "email", "zip");
	
	foreach($personal_info_ques as $personal_que) {
	    $personal_que_answer = $AppQueAnswers[1][0][$personal_que]['Answer'];
	    
	    if($personal_que_answer != "") {
	        $download_info .= '<div class="row">';
	        $download_info .= '<div class="col-lg-12 col-md-12 col-sm-12">';
	        if($personal_que == "email") {
	            if($permit ['Applicants_Contact'] >= 1) {
	                $download_info .= 'Email Address: <a href="mailto:' . $personal_que_answer . '">' . $personal_que_answer . '</a>';
	            }
	            else {
	                $download_info .= 'Email Address: ' . $personal_que_answer;
	            }
	        }
	        else {
	            $download_info .= $personal_que_answer;
	        }
	        $download_info .= '</div>';
	        $download_info .= '</div>';
	    }
	}
	
	$download_info .= "<br><br>";

	foreach ($AppQueAnswers as $SectionID=>$QA)
	{
        if($SectionID != "1") {
            $download_info .= "<div class='row'>";
            $download_info .= "<div class='col-lg-12 col-md-12 col-sm-12'>";
            $download_info .= "<br><strong>".$TITLES[$SectionID]."</strong>";
            $download_info .= "</div>";
            $download_info .= "</div>";
        }
        
        $SubSecCount = count($AppQueAnswers[$SectionID]);
        
        for($subseci = 0; $subseci < $SubSecCount; $subseci++) {
            
            $que_section = 0;
            foreach ($QA[$subseci] as $QuestionID=>$QuestionAnswerInfo) {
                
                if(!in_array($QuestionID, $personal_info_excludes)) {
                    if($que_section == 0) {
                        $download_info .= "<div class='row'>";
                        
                        if($subseci == 0) {
                           $download_info .= "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['SubSecInstruction']."</div>";
                        }
                        
                        $download_info .= "<div class='col-lg-12 col-md-12 col-sm-12'><u>".$QuestionAnswerInfo['SubSecTitle']."</u></div>";
                        $download_info .= "</div>";
                    }
                     
                    $que_section++;
                     
                    $download_info .= "<div class='row' style='margin-bottom:1px;'>";
                    
                    if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
                    
                        $download_info .= "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
                         
                        $canswer = $QuestionAnswerInfo['Answer'];
                    
                        $rtn  = '';
                        if(is_array($canswer)) {
                            $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
                    
                            foreach ($canswer as $cakey=>$caval) {
                                $rtn .= '<tr>';
                                $rtn .= '<td style="padding-right:5px !important">'.$cakey.'</td>';
                                foreach ($caval as $cavk=>$cavv) {
                                    $rtn .= '<td style="padding-right:5px !important">'.$cavv.'</td>';
                                }
                                $rtn .= '</tr>';
                            }
                    
                            $rtn .= '</table>';
                        }
                         
                        $download_info .= "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$rtn."</strong></div>";
                    
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
                    
                        $download_info .= "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
                        
                        $canswer = $QuestionAnswerInfo['Answer'];
                    
                        $rtn  = '';
                        if(is_array($canswer)) {
                    
                            $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
                             
                            $days_count = count($canswer['days']);
                             
                            for($ci = 0; $ci < $days_count; $ci++)  {
                                $rtn .= '<tr>';
                                $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
                                $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
                                $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
                                $rtn .= '<td width="5%">to </td>';
                                $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
                                $rtn .= '</tr>';
                            }
                             
                            $rtn .= '</table>';
                        }
                    
                        $download_info .= "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$rtn."</strong></div>";
                    }
                    else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
                        $download_info .= "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
                    }
                    else {
                        $download_info .= "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
                        $download_info .= "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
                    }

                    $download_info .= "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
                    $download_info .= "</div>";
                }
            }
        }
	}
	
	
	if ($TextBlocks ["ApplicantAuthorizations"]) {
	    $ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
	} else {
	    $ApplicantAuthorizations = "";
	}
	
	$Signature = '' . $TextBlocks ["Signature"];
	
	if ($OrgID == "I20100301") { // Meritan
	    $Signature .= ' I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.';
	} else {
	    $Signature .= ' I agree with the above statement.';
	}
	$Signature .= '<br><br>';
	
	$Signature .= 'Electronic Signature: <font style="font-color:red">X</font>&nbsp;<b>' . $APPDATA ['signature'] . '</b>';
	
	$download_info .= "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	
	$download_info .= "<div class='col-lg-12 col-md-12 col-sm-12'>";
	if (($TextBlocks ["Signature"]) || ($ApplicantAuthorizations)) {
	    $download_info .='<table border="0" cellspacing="3" cellpadding="0" class="table">';
	    $download_info .= '<tr><td><b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '</td></tr>';
	    $download_info .= '<tr><td>';
	    $download_info .= $Signature;
	    $download_info .= '<td></tr>';
	    $download_info .= '</table>';
	    $download_info .= '<br>';
	}
	$download_info .= "</div>";

	if (($OrgID == "B12345467xx") || ($OrgID == "I20130510")) {
	    $download_info .='<img src="' . IRECRUIT_HOME . 'images/hiring_managers_only.jpg' . '" style="width:786px;">';
	}
	
} // end if ApplicationID, OrgID, RequestID
?>