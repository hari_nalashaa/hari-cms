<?php
global $APPDATAREQ, $bg_color, $highlight, $HiddenFormObj;

$rtn = "";
if ((isset($action) && $action != "onboardapplicant" && $formtable != "RequisitionQuestions") || (!isset($action) && $formtable == "FormQuestions")) {
	$active = " AND Active = 'Y'";
}

//Set the parameters
$params = array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID);

$refine = "";
if($formtable == "RequisitionQuestions") {
    $params[":RequisitionFormID"]     =   $RequisitionFormID;
    $refine = " AND RequisitionFormID = :RequisitionFormID";
} else if ($formtable == "WebFormQuestions") {
	$params[":WebFormID"]  =   $WebFormID;
	$params[":FormID"]     =   $FormID;
	
	$refine = " AND WebFormID = :WebFormID AND FormID = :FormID";
} else if ($formtable == "PreFilledFormQuestions") {
	$params[":PreFilledFormID"] = $PreFilledFormID;
	$refine = " AND PreFilledFormID = :PreFilledFormID";
} else if ($formtable == "AgreementFormQuestions") {
	$params[":AgreementFormID"] = $AgreementFormID;
	$refine = " AND AgreementFormID = :AgreementFormID";
} else if($formtable == "OnboardQuestions") {
	$params[":OnboardFormID"] = $FormID;
	$refine = " AND OnboardFormID = :OnboardFormID";
} else {
	$params[":FormID"] = $FormID;
	$refine = " AND FormID = :FormID";
}

//Get Question Information
$thisrow        =   $FormQuestionsObj->getQuestionInformation(array($params), $formtable, $refine, $active);

if (($thisrow['PreFilledFormID'] == "FE-I9") 
	&& ($holdOrgID == "I20140912") 
	&& ($thisrow['QuestionID'] == "MiddleName")) { // Community Research Foundation
	    $thisrow['Required']="Y";
}
if (($thisrow['PreFilledFormID'] == "FE-I9-2020") 
	&& ($holdOrgID == "I20140912") 
	&& ($thisrow['QuestionID'] == "MiddleName")) { // Community Research Foundation
	    $thisrow['Required']="Y";
}

$thisrow ['RequestID']  =   $RequestID;

if($formtable != "") {
    $thisrow ['FormTable']  =   $formtable;
}
else {
    $thisrow ['FormTable']  =   "FormQuestions";
}

$name           =   trim ( $thisrow ['QuestionID'] );
$Question       =   $thisrow ['Question'];
$QuestionTypeID =   $thisrow ['QuestionTypeID'];
$size           =   $thisrow ['size'];
$maxlength      =   $thisrow ['maxlength'];
$rows           =   $thisrow ['rows'];
$cols           =   $thisrow ['cols'];
$wrap           =   $thisrow ['wrap'];
$value          =   $thisrow ['value'];
$defaultValue   =   $thisrow ['defaultValue'];
$required       =   $thisrow ['Required'];
$validate       =   $thisrow ['Validate'];

if (isset ( $thisrow ['SectionID'] )) {
	$SID = $thisrow ['SectionID'];
}

$thisrow ['name']   =   $name;

if ($SectionID) {
    $name = $SectionID . $name;
    $thisrow ['name']   =   $name;
}

// set flag for required that is missing
$errck = array ('from', 'to', '1', '2', '3', '');
$requiredck = '';

for($errcnt = 0; $errcnt <= sizeof ( $errck ); $errcnt ++) {
	if ($APPDATAREQ [$SectionID . $QuestionID . $errck [$errcnt]] == "REQUIRED") {
		$requiredck = ' style="background-color:' . $highlight . ';"';
	}
}

if ($APPDATAREQ [trim($SectionID . $QuestionID)] == "REQUIRED") {
	$requiredck = ' style="background-color:' . $highlight . ';"';
}

//This is for requisition custom questions validation
if(is_array($error_inputs) && in_array($QuestionID, $error_inputs)) {
	$requiredck = ' style="background-color:' . $bg_color . ';padding-bottom:10px;"';
}

$lbl_class = ' class="question_name"';
if($QuestionTypeID == 99 || $QuestionTypeID == 5) {
    $lbl_class = " class='question_name labels_auto'";
}

$thisrow ['lbl_class']  =   $lbl_class;
$thisrow ['requiredck'] =   $requiredck;

$HiddenFormObj->APPDATA   =   $APPDATA;

if ($QuestionTypeID == 1) { // Date from - to
    $rtn .= $HiddenFormObj->getQuestionView1($thisrow);
}
else if ($QuestionTypeID == 2) { // radio
    $rtn .= $HiddenFormObj->getQuestionView2($thisrow);
}
else if ($QuestionTypeID == 3) { // pull down
    $rtn .= $HiddenFormObj->getQuestionView3($thisrow);
}
else if ($QuestionTypeID == 5) { // text area
    $rtn .= $HiddenFormObj->getQuestionView5($thisrow);
}
else if ($QuestionTypeID == 6) { // text
    $rtn .= $HiddenFormObj->getQuestionView6($thisrow);
}
else if ($QuestionTypeID == 7) { // display text area
    $rtn .= $HiddenFormObj->getQuestionView7($thisrow);
}
else if ($QuestionTypeID == 8) { // attachment upload
    $rtn .= $HiddenFormObj->getQuestionView8($thisrow);
}
else if ($QuestionTypeID == 9) { // Checkbox with Year and Comment Text
    $rtn .= $HiddenFormObj->getQuestionView9($thisrow);
}
else if ($QuestionTypeID == 10) { // Hidden
    $rtn .= $HiddenFormObj->getQuestionView10($thisrow);
}
else if ($QuestionTypeID == 11) { // Password
    $rtn .= $HiddenFormObj->getQuestionView11($thisrow);
}
else if ($QuestionTypeID == 13) { // Home Phone
    $rtn .= $HiddenFormObj->getQuestionView13($thisrow);
}
else if ($QuestionTypeID == 14) { // Business Phone
    $rtn .= $HiddenFormObj->getQuestionView14($thisrow);
}
else if ($QuestionTypeID == 15) { // Social Security
    $rtn .= $HiddenFormObj->getQuestionView15($thisrow);
}
else if ($QuestionTypeID == 17) { // Date
    $rtn .= $HiddenFormObj->getQuestionView17($thisrow);
}
else if ($QuestionTypeID == 18) { // Checkbox
    $rtn .= $HiddenFormObj->getQuestionView18($thisrow);
}
else if ($QuestionTypeID == 19) { // Social Insurance
    $rtn .= $HiddenFormObj->getQuestionView19($thisrow);
}
else if ($QuestionTypeID == 22) { // radio virtical display
    $rtn .= $HiddenFormObj->getQuestionView22($thisrow);
}
else if ($QuestionTypeID == 23) { // radio long question
    $rtn .= $HiddenFormObj->getQuestionView23($thisrow);
}
else if ($QuestionTypeID == 24) { // Date from - to present
    $rtn .= $HiddenFormObj->getQuestionView24($thisrow);
}
else if ($QuestionTypeID == 30) { // Signature
    $rtn .= $HiddenFormObj->getQuestionView30($thisrow);
}
else if ($QuestionTypeID == 60) { // Form Merge Content
    $rtn .= $HiddenFormObj->getQuestionView60($thisrow);
}
else if ($QuestionTypeID == 90) { // Attachment
    $rtn .= $HiddenFormObj->getQuestionView90($thisrow);
}
else if ($QuestionTypeID == 99) { // information
    $rtn .= $HiddenFormObj->getQuestionView99($thisrow);
}
else if ($QuestionTypeID == 100) { // Radio with group of skills
    $rtn .= $HiddenFormObj->getQuestionView100($thisrow);
} else if ($QuestionTypeID == 120) { // Pulldown for daily shifts
    $rtn .= $HiddenFormObj->getQuestionView120($thisrow);
}
else if ($QuestionTypeID == 1818) { // Checkbox List
    $rtn .= $HiddenFormObj->getQuestionView1818($thisrow);
}

return $rtn;
?>
