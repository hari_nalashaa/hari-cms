<?php
function VeteranSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name) {
	
    $rtnV           =   "";
    $vet_ques_list  =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, 'VET');
    
    if (is_array ( $vet_ques_list )) {
        foreach ( $vet_ques_list as $VET ) {
			
			if ($VET ['QuestionID'] == "vet_enterdate") {
			    $rtnV .= '<div class="form-group row">';
			    
			    $rtnV .= '<div class="col-lg-3 col-md-3 col-sm-3">';
			    $rtnV .= '<label class="question_name">';
			    $rtnV .= 'Name:';
			    $rtnV .= '</label>';
			    $rtnV .= '</div>';
			    
			    $rtnV .= '<div class="col-lg-9 col-md-9 col-sm-9">';
			    $rtnV .= '<span style="font-weight:bold;" id="vet_applicant_name">' . ($applicant_name ? $applicant_name : 'N/A') . '</span>';
			    $rtnV .= '</div>';
			    
			    $rtnV .= '</div>';
			}
			
			$QuestionID  =   $VET ['QuestionID'];
			$ans         =   $APPDATA [$VET ['QuestionID']];
			$formtable   =   "FormQuestions";
			$FormIDhold  =   $FormID;
			$rtnV       .=   include COMMON_DIR . 'application/DisplayQuestions.inc';
			$formtable   =   "FormQuestions";
			$FormID      =   $FormIDhold;
		} // end foreach
	}
	
	if (count($vet_ques_list) < 1) {
		$rtnV = '';
	}

	return $rtnV;
} // end function

$rtnV   =   VeteranSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name);

return $rtnV;
?>