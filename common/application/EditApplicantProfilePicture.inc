<?php
$app_profile_picture_val = $ApplicantDetailsObj->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'ApplicantPicture');
$app_profile_img_src = IRECRUIT_HOME . 'vault/' . $OrgID . '/applicant_picture/' . $app_profile_picture_val;

$app_profile_picture = '';
if($app_profile_picture_val != "") $app_profile_picture = '<img src="'.$app_profile_img_src.'" width="133" height="133">';

// Set switch to turn on and of entries //Set columns
$columns = "QuestionID";
//Set where condition
$where = array("OrgID = :OrgID", "FormID = :FormID", "SectionID = 14", "Active = 'Y'");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
//Get Get Social Media Profiles Questions Information
$rlts = $FormQuestionsObj->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));

$nrowsa =  $rlts['count'];

if(is_array($rlts['results'])) {
	foreach ($rlts['results'] as $row) {
	
		$QuestionID  = $row['QuestionID'];
	
		$app_profile_picture .= include 'DisplayQuestions.inc';
	
	} // end foreach
}

if ($nrowsa < 1) { $app_profile_picture = ''; }

return $app_profile_picture;
?>