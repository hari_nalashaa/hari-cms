<?php
$purpose                =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

$file_names             =   array();
$AttachmentMultiOrgID   =   (is_null($MultiOrgID) || $MultiOrgID == '') ? '' : $MultiOrgID;

$res_attachments_info   =   G::Obj('ApplicantAttachmentsTemp')->getApplicantAttachmentsTempInfo($OrgID, $HoldID);
$attachments_info       =   $res_attachments_info['results'];

$resume_exists          =   "false";
for($ka = 0; $ka < count($attachments_info); $ka++) {
    
    if($attachments_info[$ka]['TypeAttachment'] == 'resumeupload') {
        
        $resume_exists  =   "true";
        $filename       =   IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $HoldID . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
        
        if (!file_exists($filename)) {
            $resume_exists = "false";
        }
        else if (filesize($filename) == 0) {
            $resume_exists = "false";
        }
        
        if($resume_exists == "true") $file_names[$attachments_info[$ka]['TypeAttachment']] = IRECRUIT_HOME . 'vault/'. $OrgID . '/applicantattachments/'. $HoldID . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
    }
    else {
        
        $filename = IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $HoldID . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
        
        if (file_exists($filename) && filesize($filename) > 0) {
            $file_names[$attachments_info[$ka]['TypeAttachment']] = IRECRUIT_HOME . 'vault/'. $OrgID . '/applicantattachments/'. $HoldID . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
        }
    }
}

$file_attachment_types = array_keys($file_names);

echo '<div id="attachments_process_msg"></div>';

foreach($purpose as $purpose_key=>$purpose_value) {
    $attachment_type = $purpose_key;
    if(in_array($purpose_key, $file_attachment_types)) {
        echo 'Currently Uploaded: <a href="downloadTempAttachment.php?OrgID='.$OrgID.'&MultiOrgID='.$AttachmentMultiOrgID.'&RequestID='.$_REQUEST['RequestID'].'&FormID='.$FormID.'&HoldID='.$HoldID.'&attachment_type='.$attachment_type.'">'.ucwords($purpose[$attachment_type]).'</a>';
        //echo ' - <a href=\'javascript:void(0);\' onclick=\'clearApplicantAttachmentsTemp("'.$attachment_type.'")\'>Remove</a>';
        echo '<br><br>';
    }
}

echo '<br>';
?>