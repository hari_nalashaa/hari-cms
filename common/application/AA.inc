<?php
function AffirmativeActionSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name) {

    $rtnAA          =   "";
    $aa_ques_list   =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, 'AA');
    
	//Results for aaf 
    if(is_array($aa_ques_list)) {
        foreach($aa_ques_list as $AAQueID=>$AA) {
		
			if ($AA ['QuestionID'] == "aa_enterdate") {
				$rtnAA .= '<table border="0" cellspacing="0" cellpadding="0" width="100%" style="margin-top:5px; 0px">';
				$rtnAA .= '<tr><td>';
				
				$rtnAA .= '<div class="form-group row">';
				
				$rtnAA .= '<div class="col-lg-3 col-md-3 col-sm-3">';
				$rtnAA .= '<label class="question_name">';
				$rtnAA .= 'Name:';
				$rtnAA .= '</label>';
				$rtnAA .= '</div>';
				
				$rtnAA .= '<div class="col-lg-9 col-md-9 col-sm-9">';
				$rtnAA .= '<span style="font-weight:bold;" id="aa_applicant_name">' . ($applicant_name ? $applicant_name : 'N/A') . '</span>';
				$rtnAA .= '</div>';
				
				$rtnAA .= '</div>';
				
				
				$rtnAA .= '</td></tr>';
				$rtnAA .= '</table>';
			}

			$QuestionID  =   $AA ['QuestionID'];
			$ans         =   $APPDATA [$AA ['QuestionID']];
			$formtable   =   "FormQuestions";
			$FormIDhold  =   $FormID;
			$rtnAA      .=   include COMMON_DIR . 'application/DisplayQuestions.inc';
			$formtable   =   "FormQuestions";
			$FormID      =   $FormIDhold;
		} // end foreach
	}
	
	return $rtnAA;
} // end function

$rtnAA  =   AffirmativeActionSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name);

return $rtnAA;
?>