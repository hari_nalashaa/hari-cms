<?php
if(!isset($APPDATA)) {
    global $APPDATA;
}

if(!isset($bg_color)) {
    global $bg_color;
}

if(!isset($highlight)) {
    global $highlight;
}

if(!isset($APPDATAREQ)) {
    global $APPDATAREQ;
}

if(!isset($RequestID)) {
    global $RequestID;
}

$rtn = "";
if ((isset($action) && $action != "onboardapplicant" && $formtable != "RequisitionQuestions") || (!isset($action) && $formtable == "FormQuestions")) {
	$active = " AND Active = 'Y'";
}

//Set the parameters
$params = array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID);

$refine = "";
if($formtable == "RequisitionQuestions") {
    $params[":RequisitionFormID"]     =   $RequisitionFormID;
    $refine = " AND RequisitionFormID = :RequisitionFormID";
} else if ($formtable == "WebFormQuestions") {
	$params[":WebFormID"]  =   $WebFormID;
	$params[":FormID"]     =   $FormID;
	$refine = " AND WebFormID = :WebFormID AND FormID = :FormID";
} else if ($formtable == "PreFilledFormQuestions") {
	$params[":PreFilledFormID"] = $PreFilledFormID;
	$refine = " AND PreFilledFormID = :PreFilledFormID";
} else if ($formtable == "AgreementFormQuestions") {
	$params[":AgreementFormID"] = $AgreementFormID;
	$refine = " AND AgreementFormID = :AgreementFormID";
} else if($formtable == "OnboardQuestions") {
	$params[":OnboardFormID"] = $FormID;
	$refine = " AND OnboardFormID = :OnboardFormID";
} else {
	$params[":FormID"] = $FormID;
	$refine = " AND FormID = :FormID";
}

//Get Question Information
$thisrow        =   G::Obj('FormQuestions')->getQuestionInformation(array($params), $formtable, $refine, $active);

if (isset($thisrow['PreFilledFormID'])
    && ($thisrow['PreFilledFormID'] == "FE-I9") 
	&& ($holdOrgID == "I20140912") 
	&& ($thisrow['QuestionID'] == "MiddleName")) { // Community Research Foundation
	    $thisrow['Required']="Y";
}
if (isset($thisrow['PreFilledFormID'])
    && ($thisrow['PreFilledFormID'] == "FE-I9-2020") 
	&& ($holdOrgID == "I20140912") 
	&& ($thisrow['QuestionID'] == "MiddleName")) { // Community Research Foundation
	    $thisrow['Required']="Y";
}

$thisrow ['RequestID']  =   $RequestID;

if($formtable != "") {
    $thisrow ['FormTable']  =   $formtable;
}
else {
    $thisrow ['FormTable']  =   "FormQuestions";
}

$name           =   trim ( $thisrow ['QuestionID'] );
$Question       =   $thisrow ['Question'];
$QuestionTypeID =   $thisrow ['QuestionTypeID'];
$size           =   $thisrow ['size'];
$maxlength      =   $thisrow ['maxlength'];
$rows           =   $thisrow ['rows'];
$cols           =   $thisrow ['cols'];
$wrap           =   $thisrow ['wrap'];
$value          =   $thisrow ['value'];
$defaultValue   =   $thisrow ['defaultValue'];
$required       =   $thisrow ['Required'];
$validate       =   $thisrow ['Validate'];

if(!isset($append_alphabet)) $append_alphabet = "";//If alphabet is not set make it empty
$thisrow ['name']   =   $append_alphabet . $name;

// set flag for required that is missing
$errck = array ('from', 'to', '1', '2', '3', '');
$requiredck = '';

for($errcnt = 0; $errcnt < count ( $errck ); $errcnt ++) {
	if ($APPDATAREQ [$QuestionID . $errck [$errcnt]] == "REQUIRED") {
		$requiredck = ' style="background-color:' . $highlight . ';"';
	}
}

if ($APPDATAREQ [trim($QuestionID)] == "REQUIRED") {
	$requiredck = ' style="background-color:' . $highlight . ';"';
}

//This is for requisition custom questions validation
if(isset($error_inputs) 
    && is_array($error_inputs) 
    && in_array($QuestionID, $error_inputs)) {
	$requiredck = ' style="background-color:' . $bg_color . ';padding-bottom:10px;"';
}

$lbl_class = ' class="question_name"';
if($QuestionTypeID == 99 || $QuestionTypeID == 5) {
    $lbl_class = " class='question_name labels_auto'";
}

$thisrow ['lbl_class']  =   $lbl_class;
$thisrow ['requiredck'] =   $requiredck;

if($ApplicationID != "") {
    $thisrow['ApplicationID']   =   $ApplicationID;
}

G::Obj('ApplicationForm')->APPDATA   =   $APPDATA;

if ($QuestionTypeID == 1) { // Date from - to
    $rtn .= G::Obj('ApplicationForm')->getQuestionView1($thisrow);
}
else if ($QuestionTypeID == 2) { // radio
    $rtn .= G::Obj('ApplicationForm')->getQuestionView2($thisrow);
}
else if ($QuestionTypeID == 3) { // pull down
    $rtn .= G::Obj('ApplicationForm')->getQuestionView3($thisrow);
}
else if ($QuestionTypeID == 5) { // text area
    $rtn .= G::Obj('ApplicationForm')->getQuestionView5($thisrow);
}
else if ($QuestionTypeID == 6) { // text
    $rtn .= G::Obj('ApplicationForm')->getQuestionView6($thisrow);
}
else if ($QuestionTypeID == 7) { // display text area
    $rtn .= G::Obj('ApplicationForm')->getQuestionView7($thisrow);
}
else if ($QuestionTypeID == 8) { // attachment upload
    $rtn .= G::Obj('ApplicationForm')->getQuestionView8($thisrow);
}
else if ($QuestionTypeID == 9) { // Checkbox with Year and Comment Text
    $rtn .= G::Obj('ApplicationForm')->getQuestionView9($thisrow);
}
else if ($QuestionTypeID == 10) { // Hidden
    $rtn .= G::Obj('ApplicationForm')->getQuestionView10($thisrow);
}
else if ($QuestionTypeID == 11) { // Password
    $rtn .= G::Obj('ApplicationForm')->getQuestionView11($thisrow);
}
else if ($QuestionTypeID == 13) { // Home Phone
    $rtn .= G::Obj('ApplicationForm')->getQuestionView13($thisrow);
}
else if ($QuestionTypeID == 14) { // Business Phone
    $rtn .= G::Obj('ApplicationForm')->getQuestionView14($thisrow);
}
else if ($QuestionTypeID == 15) { // Social Security
    $rtn .= G::Obj('ApplicationForm')->getQuestionView15($thisrow);
}
else if ($QuestionTypeID == 17) { // Date
    $rtn .= G::Obj('ApplicationForm')->getQuestionView17($thisrow);
}
else if ($QuestionTypeID == 18) { // Checkbox
    $rtn .= G::Obj('ApplicationForm')->getQuestionView18($thisrow);
}
else if ($QuestionTypeID == 19) { // Social Insurance
    $rtn .= G::Obj('ApplicationForm')->getQuestionView19($thisrow);
}
else if ($QuestionTypeID == 22) { // radio virtical display
    $rtn .= G::Obj('ApplicationForm')->getQuestionView22($thisrow);
}
else if ($QuestionTypeID == 23) { // radio long question
    $rtn .= G::Obj('ApplicationForm')->getQuestionView23($thisrow);
}
else if ($QuestionTypeID == 24) { // Date from - to present
    $rtn .= G::Obj('ApplicationForm')->getQuestionView24($thisrow);
}
else if ($QuestionTypeID == 30) { // Signature
    $rtn .= G::Obj('ApplicationForm')->getQuestionView30($thisrow);
}
else if ($QuestionTypeID == 60) { // Form Merge Content
    $rtn .= G::Obj('ApplicationForm')->getQuestionView60($thisrow);
}
else if ($QuestionTypeID == 90) { // Attachment
    $rtn .= G::Obj('ApplicationForm')->getQuestionView90($thisrow);
}
else if ($QuestionTypeID == 99) { // information
    $rtn .= G::Obj('ApplicationForm')->getQuestionView99($thisrow);
}
else if ($QuestionTypeID == 100) { // Radio with group of skills
    $rtn .= G::Obj('ApplicationForm')->getQuestionView100($thisrow);
} else if ($QuestionTypeID == 120) { // Pulldown for daily shifts
    $rtn .= G::Obj('ApplicationForm')->getQuestionView120($thisrow);
}
else if ($QuestionTypeID == 1818) { // Checkbox List
    $rtn .= G::Obj('ApplicationForm')->getQuestionView1818($thisrow);
}

return $rtn;
?>
