<?php
// *** FUNCTIONS ****//
function countrySelect($OrgID, $APPDATA, $International) {
	global $AddressObj;
	
	$retval = "";
	
	if ($APPDATAREQ ['country'] == 'REQUIRED') {
		$APPDATA ['country'] = "";
		$countrymissing = ' style="background-color:' . $highlight . '"';
	} else {
		$countrymissing = "";
	}
	
	$countryorig = $APPDATA ['country'];

	$retval .= <<<END
<div class="col-lg-3 col-md-3 col-sm-3">
<label for="country">Country: <font style="color:red">*</font></label>
</div>
<div class="col-lg-9 col-md-9 col-sm-9">
<input type="hidden" name="countryorig" id="countryorig" value="$countryorig">
<select size="1" name="country" id="country" onchange="submit()" class="form-control width-auto-inline">
END;
	
	if ($International == "Y") {
		$results = $AddressObj->getCountries();
	} else {
		$results = $AddressObj->getCountryByCode('US');
	}
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as	$row) {
			$retval .= "<option value=\"" . $row ['Abbr'] . "\"";
			if (trim ( $APPDATA ["country"] ) == $row ['Abbr']) {
				$retval .= " selected";
			}
			$retval .= ">" . $row ['Description'];
			$retval .= "</option>\n";
		}
	}
	
	$retval .= <<<END
</select>
</div>
END;
	
	return $retval;
}

?>
