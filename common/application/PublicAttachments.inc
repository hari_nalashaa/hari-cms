<?php 
$attachments_form_info = "";

//Set columns
$columns    =   array("Question", "QuestionID", "Required");
//Set where condition
$where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "Active = 'Y'");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
//Get FormQuestions Information
$fresults   =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));
$rowcnt     =   $fresults['count'];
$att_req    =   $fresults['results'];

// Monster application index
$mai        =   0;

$att_req_que = array();
for($fr = 0; $fr < $rowcnt; $fr++) {
    $att_req_que[$att_req[$fr]['QuestionID']] = $att_req[$fr]['Required'];
}

$resume_info            =   G::Obj('FormQuestions')->getQuestionDetails("Required", "FormQuestions", $OrgID, $FormID, "resumeupload");
$parsed_resume_info     =   G::Obj('SovrenParseTemp')->getParsedTempResumeInfo($OrgID, $HoldID);
$purpose                =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

$file_names             =   array();
$AttachmentMultiOrgID   =   (is_null($MultiOrgID) || $MultiOrgID == '') ? '' : $MultiOrgID;

$resume_exists          =   "true";
$filename               =   IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $HoldID . '-Resume-resumeupload' . '.' . $parsed_resume_info['FILEEXT'];

if (!file_exists($filename)) {
    $resume_exists = "false";
}
else if (filesize($filename) == 0) {
    $resume_exists = "false";
}

require_once COMMON_DIR . 'application/PublicUploadedAttachments.inc';

if(is_array($fresults['results'])) {
    foreach ($fresults['results'] as $QUES) {

        $QuestionID         =   $QUES ['QuestionID'];

        if(($feature['ResumeParsing'] == "Y" 
            && ($parsed_resume_info['XML'] != "") 
            && $QuestionID == "resumeupload" 
            && $resume_exists == "true")) {
            $attachments_form_info .= '<input type="hidden" name="resumeparsing" id="resumeparsing" value="resumeparsing">';
        }
        else if(!isset ( $MApplicantInfo ) || (isset ( $MApplicantInfo ) && $QuestionID != "coverletterupload")) {
            $formtable  =   "FormQuestions";
            $FileUpload =   include 'DisplayQuestions.inc';
        }
        	
        if ($urowcnt < 1) {
            if (isset ( $MApplicantInfo )) {
                $attachments_form_info .= 'Your resume, coverletter will be considered from Monster<input type="hidden" name="resumecoverletter" id="resumecoverletter" value="resumecoverletter">';
            }
            if ($FileUpload) {
                $attachments_form_info .= $FileUpload;
            }
        }
    } // end foreach
}

return $attachments_form_info;
?>