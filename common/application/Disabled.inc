<?php
function DisabledSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name) {
    
    $rtnD           =   "";
    $dis_ques_list  =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, 'DIS');

    if (is_array ( $dis_ques_list )) {
        foreach ( $dis_ques_list as $DIS ) {
            
            if ($DIS ['QuestionID'] == "dis_enterdate") {
                
                $rtnD .= '<div class="form-group row">';
                
                $rtnD .= '<div class="col-lg-3 col-md-3 col-sm-3">';
                $rtnD .= '<label class="question_name">';
                $rtnD .= 'Name:';
                $rtnD .= '</label>';
                $rtnD .= '</div>';
                
                $rtnD .= '<div class="col-lg-9 col-md-9 col-sm-9">';
                $rtnD .= '<span style="font-weight:bold;" id="dis_applicant_name">' . ($applicant_name ? $applicant_name : 'N/A') . '</span>';
                $rtnD .= '</div>';
                
                $rtnD .= '</div>';
            }
            
            $QuestionID  =   $DIS ['QuestionID'];
            $ans         =   $APPDATA [$DIS ['QuestionID']];
            $formtable   =   "FormQuestions";
            $FormIDhold  =   $FormID;
            $rtnD       .=   include COMMON_DIR . 'application/DisplayQuestions.inc';
            $formtable   =   "FormQuestions";
            $FormID      =   $FormIDhold;
        } // end foreach
    }
    
    if (count($dis_ques_list) == 0) {
        $rtnD = '';
    }
    
    return $rtnD;
    
} // end function

$rtnD   =   DisabledSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name);

return $rtnD;
?>