<?php 
$attachments_form_info = "";

//Set columns
$columns    =   array("Question", "QuestionID", "Required");
//Set where condition
$where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "Active = 'Y'");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
//Get FormQuestions Information
$fresults   =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));
$rowcnt     =   $fresults['count'];
$att_req    =   $fresults['results'];

// Monster application index
$mai        =   0;

$att_req_que = array();
for($fr = 0; $fr < $rowcnt; $fr++) {
    $att_req_que[$att_req[$fr]['QuestionID']] = $att_req[$fr]['Required'];
}

$resume_info            =   G::Obj('FormQuestions')->getQuestionDetails("Required", "FormQuestions", $OrgID, $FormID, "resumeupload");

$purpose                =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

$file_names             =   array();
$AttachmentMultiOrgID   =   (is_null($MultiOrgID) || $MultiOrgID == '') ? '' : $MultiOrgID;
$attachments_info       =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $AttachmentMultiOrgID, $UpUserID, $_REQUEST['RequestID']);

$resume_exists          =   "false";
for($ka = 0; $ka < count($attachments_info); $ka++) {
    
    if($attachments_info[$ka]['TypeAttachment'] == 'resumeupload') {
        
        $resume_exists  =   "true";        
        $filename       =   IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
        
        if (!file_exists($filename)) {
            $resume_exists = "false";
        }
        else if (filesize($filename) == 0) {
            $resume_exists = "false";
        }
        
        if($resume_exists == "true") $file_names[$attachments_info[$ka]['TypeAttachment']] = IRECRUIT_HOME . 'vault/'. $OrgID . '/applicantattachments/'. $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
    }
    else {        
        
        $filename = IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
        
        if (file_exists($filename) && filesize($filename) > 0) {
            $file_names[$attachments_info[$ka]['TypeAttachment']] = IRECRUIT_HOME . 'vault/'. $OrgID . '/applicantattachments/'. $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
        }
    } 
}

$file_attachment_types = array_keys($file_names);

echo '<div id="attachments_process_msg"></div>';

foreach($purpose as $purpose_key=>$purpose_value) {
    $attachment_type = $purpose_key;
    if(in_array($purpose_key, $file_attachment_types)) {
        echo 'Currently Uploaded: <a href="downloadTempAttachment.php?OrgID='.htmlspecialchars($OrgID).'&RequestID='.htmlspecialchars($_REQUEST['RequestID']).'&MultiOrgID='.htmlspecialchars($AttachmentMultiOrgID).'&attachment_type='.$attachment_type.'">'.ucwords($purpose[$attachment_type]).'</a>';
        //if($att_req_que[$attachment_type] == '') {
            echo ' - <a href=\'javascript:void(0);\' onclick=\'clearApplicantAttachmentsTemp("'.$attachment_type.'")\'>Remove</a>';
        //}
        echo '<br><br>';
    }
}

echo '<br>';

if(is_array($fresults['results'])) {
    foreach ($fresults['results'] as $QUES) {

        $QuestionID = $QUES ['QuestionID'];
        
        $parse_userportal_resume = G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ParseResume");
        
        if(($feature['ResumeParsing'] == "Y" 
            && ($parse_userportal_resume == "Yes") 
            && $QuestionID == "resumeupload" 
            && ($resume_info['Required'] == "Y")
            && $resume_exists == "true")) {
            $attachments_form_info .= '<input type="hidden" name="resumeparsing" id="resumeparsing" value="resumeparsing">';
        }
        else if(!isset ( $MApplicantInfo ) || (isset ( $MApplicantInfo ) && $QuestionID != "coverletterupload")) {
            $formtable  =   "FormQuestions";
            $FileUpload =   include 'DisplayQuestions.inc';
        }
        	
        if (isset ( $MApplicantInfo )) {
            $attachments_form_info .= 'Your resume, coverletter will be considered from Monster<input type="hidden" name="resumecoverletter" id="resumecoverletter" value="resumecoverletter">';
        }
        if ($FileUpload) {
            $attachments_form_info .= $FileUpload;
        }
    } // end foreach
}

return $attachments_form_info;
?>