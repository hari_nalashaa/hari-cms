<?php
if (FROM_SRC == "IRECRUIT") {
	$irecruit_url = IRECRUIT_HOME;
} elseif (FROM_SRC == "PUBLIC") {
	$irecruit_url = PUBLIC_HOME;
} elseif (FROM_SRC == "USERPORTAL") {
	$irecruit_url = USERPORTAL_HOME;
}

require_once IRECRUIT_DIR . 'applicants/GlobalArrays.inc';

if(!isset($action) && isset($_REQUEST['action'])) $action = $_REQUEST['action'];
if(isset($_REQUEST['RequestID'])) $RequestID = $_REQUEST['RequestID'];

$validate_dates_info	=	array();

if (isset($_REQUEST['HoldID'])) {
	$APPDATA = array ();
	
	//Get ApplicantDataTemp Information
	$results = G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $_REQUEST['HoldID']);
	
	//Results as row
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
			$APPDATA [$row ['QuestionID']] = $row ['Answer'];
		} // end foreach
	}
	
	$FormID = $APPDATA ['FormID'];
} // end HoldID


if(isset($OrgID) 
    && isset($_REQUEST['ApplicationID']) 
    && $_REQUEST['ApplicationID'] != ""
    && isset($_REQUEST['RequestID'])
	&& $_REQUEST['RequestID'] != "") {
	
    //Get FormID based on ApplicationID, RequestID
    $app_detail_info    =   $ApplicationsObj->getJobApplicationsDetailInfo("FormID", $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
    $FormID             =   $app_detail_info['FormID'];
}

if (!isset($FormID)) {
	if (isset($OrgID) && isset($RequestID)) {
		//Get requisition information
		$results  =   $RequisitionsObj->getRequisitionsDetailInfo("FormID", $OrgID, $RequestID);
		if(isset($results['FormID']) && $results['FormID'] != "") $FormID  =   $results['FormID'];
	} // end query
} // end FormID



$LST = array ();

$js_formquestions           =	"off";
$js_webformquestions        =	"off";
$js_agreementformquestions  =	"off";
$js_prefilledformquestions  =	"off";
$js_onboardquestions        =	"off";

if (FROM_SRC == "PUBLIC") {
	$js_formquestions 		=	"on";
} else if (FROM_SRC == "USERPORTAL") {
	
	$js_formquestions		=	"on";
	
	if (preg_match ( '/completePreFilledForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_prefilledformquestions = "on";
	}
	
	if (preg_match ( '/completeWebForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_webformquestions = "on";
	}
	
	if (preg_match ( '/completeAgreementForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_agreementformquestions = "on";
	}
} else if (FROM_SRC == "IRECRUIT") {

	if (preg_match ( '/formsInternal.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_webformquestions = "on";
		$js_agreementformquestions = "on";
	}
	
	if (preg_match ( '/completePreFilledForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_prefilledformquestions = "on";
	}
	
	if (preg_match ( '/completeWebForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_webformquestions = "on";
	}
	
	if (preg_match ( '/completeAgreementForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_agreementformquestions = "on";
	}

	if (preg_match ( '/preview_application.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_formquestions = "on";
	}
	
	if (preg_match ( '/forms.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$js_formquestions = "on";
	}
	
	if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] ) 
		|| preg_match ( '/getApplicationEditForm.php$/', $_SERVER ["SCRIPT_NAME"] )
		|| preg_match ( '/getOnboardApplicant.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		
		if ($action == "applicationedit") {
			$js_formquestions = "on";
		}
		
		if ($action == "updateapplicant") {
			array_push ( $LST, "StatusEffectiveDate" );
		}
		
		if ($action == "onboardapplicant") {
			array_push ( $LST, "dob" );
			array_push ( $LST, "hire_date" );
			$js_onboardquestions = "on";
		}
		
		if ($action == "datamanager") {
			array_push ( $LST, "ProcessDate_From" );
			array_push ( $LST, "ProcessDate_To" );
		}
		
		if ($action == "search") {
			array_push ( $LST, "ApplicationDate_From" );
			array_push ( $LST, "ApplicationDate_To" );
		}
		
		if ($action == "downloadlist") {
			array_push ( $LST, "DownloadProcessDate_From" );
			array_push ( $LST, "DownloadProcessDate_To" );
		}
		
		if ($action == "updatestatus") {
			array_push ( $LST, "StatusEffectiveDate" );
		}
		
		if ($action == "status") {
			array_push ( $LST, "ProcessDate_From" );
			array_push ( $LST, "ProcessDate_To" );
		}
	}
	
	if (preg_match ( '/administration.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		if ($action == "onboardquestions") {
			$js_onboardquestions = "on";
		}
	}

	
	if (preg_match ( '/addApplicant.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		array_push ( $LST, "receiveddate" );
		$js_formquestions = "on";
	}
	
	if (preg_match ( '/iRecruitStatus.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		array_push ( $LST, "ApplicationDate_From" );
		array_push ( $LST, "ApplicationDate_To" );
	}
	
    if (preg_match ( '/reports.php$/', $_SERVER ["SCRIPT_NAME"] )) {
        if ($action == "exporter") {
                array_push ( $LST, "ReportDate_From" );
                array_push ( $LST, "ReportDate_To" );
                $modal = "Y";
        }
    }

	if ((preg_match ( '/requisitions.php$/', $_SERVER ["SCRIPT_NAME"] )) 
		|| (preg_match ( '/assign.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/createRequisition.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/createRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/publicRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/publicEditRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/getEditRequisitionForm.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/processRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))) {
		array_push ( $LST, "PostDate" );
		array_push ( $LST, "ExpireDate" );
		array_push ( $LST, "POE_from" );
		array_push ( $LST, "POE_to" );
	}
	
	if ((preg_match ( '/createRequisition.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/assign.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/createRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/publicRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/publicEditRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/getEditRequisitionForm.php$/', $_SERVER ["SCRIPT_NAME"] ))
		|| (preg_match ( '/processRequest.php$/', $_SERVER ["SCRIPT_NAME"] ))) {
		
	    //Get RequisitionFormID
	    $RequisitionFormID     =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);
	    $requisition_details   =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $RequestID);
	    if($requisition_details['RequisitionFormID'] != "" && $RequestID != "") {
	        $RequisitionFormID  =   $requisition_details['RequisitionFormID'];
	    }
	    
		//Set columns
		$columns 	=	"QuestionID, QuestionTypeID, Validate";
		//Set where condition
		$where   	=	array("OrgID = :OrgID", "QuestionTypeID IN ('17','1')", "(Requisition = 'Y' OR Request = 'Y')", "RequisitionFormID = :RequisitionFormID");
		//Set parameters
		$params  	=	array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
		//Get Form Questions
		$resultsCAL	=	$RequisitionQuestionsObj->getRequisitionQuestions($columns, $where, "", array($params));
		
		if(is_array($resultsCAL['results'])) {
			foreach($resultsCAL['results'] as $IFQCAL) {
						
			   if ($IFQCAL ['QuestionTypeID'] == '17') {
					array_push ( $LST, $IFQCAL ['QuestionID'] );
					$validate_dates_info[$IFQCAL ['QuestionID']] = json_decode($IFQCAL['Validate'], true);
			   } else {
					array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
					array_push ( $LST, $IFQCAL ['QuestionID'] . 'to' );
			   }
			} // end foreach
		}
	}

} // end FROM


// Form Questions
if ($js_formquestions == "on") {
	
	$formid_flag = false;
	if(!isset($FormID) || $FormID == '' || is_null($FormID)) {
		$formid_flag = true;
		$FormID 	 = "STANDARD";
	}
	
	//Set columns
	$columns =	array("QuestionID", "QuestionTypeID", "Validate");
	//Set where condition
	$where   =	array("OrgID = :OrgID", "FormID = :FormID", "QuestionTypeID IN ('17','1', '24')", "Active = 'Y'");
	//Set parameters
	$params  =	array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
	
	//Get Form Questions
	$resultsCAL	= $FormQuestionsObj->getQuestionsInformation('FormQuestions', $columns, $where, "", array($params));

	if(is_array($resultsCAL['results'])) {
		foreach($resultsCAL['results'] as $IFQCAL) {			
			if ($IFQCAL ['QuestionTypeID'] == '17') {
				array_push ( $LST, $IFQCAL ['QuestionID'] );
				$validate_dates_info[$IFQCAL ['QuestionID']] = json_decode($IFQCAL['Validate'], true);
			}
			else if ($IFQCAL ['QuestionTypeID'] == '24') {
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
				$validate_dates_info[$IFQCAL ['QuestionID'] . 'from'] = json_decode($IFQCAL['Validate'], true);
			} 
			else {
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'to' );
				$validate_dates_info[$IFQCAL ['QuestionID'] . 'from'] = json_decode($IFQCAL['Validate'], true);
				$validate_dates_info[$IFQCAL ['QuestionID'] . 'to'] = json_decode($IFQCAL['Validate'], true);
			}
		} // end foreach
	} 
	
} // end on
  
// Web Form Questions
if ($js_webformquestions == "on") {
	
	//Set columns
	$columns 	=	array("QuestionID", "QuestionTypeID", "Question", "Validate");
	//Set where condition
	$where   	=	array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionTypeID in ('17','1')", "Active = 'Y'");
	//Set parameters
	$params  	=	array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST['WebFormID']);
	
	//Get WebFormQuestions
	$resultsCAL	=	$FormQuestionsObj->getQuestionsInformation('WebFormQuestions', $columns, $where, "", array($params));
	
	if(is_array($resultsCAL['results'])) {
		foreach($resultsCAL['results'] as $IFQCAL) {
		
			if ($IFQCAL ['QuestionTypeID'] == '17') {
				array_push ( $LST, $IFQCAL ['QuestionID'] );
				$validate_dates_info[$IFQCAL ['QuestionID']] = json_decode($IFQCAL['Validate'], true);
			} else {
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'to' );
			}
		} // end foreach
	}
} // end on
  
// Agreement Form Questions
if ($js_agreementformquestions == "on") {
	
	//Set columns
	$columns 	=	array("QuestionID", "QuestionTypeID", "Question", "Validate");
	//Set where condition
	$where   	=	array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionTypeID in ('17','1')", "Active = 'Y'");
	//Set parameters
	$params  	=	array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST['AgreementFormID']);
	//Get AgreementFormQuestions
	$resultsCAL	=	$FormQuestionsObj->getQuestionsInformation('AgreementFormQuestions', $columns, $where, "", array($params));
	
	if(is_array($resultsCAL['results'])) {
		foreach($resultsCAL['results'] as $IFQCAL) {
			if ($IFQCAL ['QuestionTypeID'] == '17') {
				array_push ( $LST, $IFQCAL ['QuestionID'] );
				$validate_dates_info[$IFQCAL ['QuestionID']] = json_decode($IFQCAL['Validate'], true);
			} else {
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'to' );
			}
		} // end foreach	
	}
	
	
} // end on
  
// PreFilled Form Questions
if ($js_prefilledformquestions == "on") {
	//Set columns
	$columns 	=	array("QuestionID", "QuestionTypeID", "Question", "Validate");
	//Set where condition
	$where   	=	array("OrgID = 'MASTER'", "PreFilledFormID = :PreFilledFormID", "QuestionTypeID in ('17','1')", "Active = 'Y'");
	//Set parameters
	$params  	=	array(":PreFilledFormID"=>$_REQUEST['PreFilledFormID']);
	
	//Get PreFilledFormQuestions
	$resultsCAL =	$FormQuestionsObj->getQuestionsInformation('PreFilledFormQuestions', $columns, $where, "", array($params));
	
	if(is_array($resultsCAL['results'])) {
		foreach ($resultsCAL['results'] as $IFQCAL) {
			if ($IFQCAL ['QuestionTypeID'] == '17') {
				array_push ( $LST, $IFQCAL ['QuestionID'] );
				$validate_dates_info[$IFQCAL ['QuestionID']] = json_decode($IFQCAL['Validate'], true);
			} else {
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'to' );
			}
		} // end foreach	
	}
} // end on
  
// Onboard Questions
if ($js_onboardquestions == "on") {	
	//Set columns
	$columns 	=	array("QuestionID", "QuestionTypeID", "Question", "Validate");
	//Set where condition
	$where   	=	array("OrgID = :OrgID", "QuestionTypeID IN ('17','1')", "Active = 'Y'");
	//Set parameters
	$params  	=	array(":OrgID"=>$OrgID);
	
	//Get PreFilledFormQuestions
	$resultsCAL =	$FormQuestionsObj->getQuestionsInformation('OnboardQuestions', $columns, $where, "", array($params));
	
	if(is_array($resultsCAL['results'])) {
		foreach ($resultsCAL['results'] as $IFQCAL) {
			if ($IFQCAL ['QuestionTypeID'] == '17') {
				array_push ( $LST, $IFQCAL ['QuestionID'] );
				$validate_dates_info[$IFQCAL ['QuestionID']] = json_decode($IFQCAL['Validate'], true);
			} else {
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'from' );
				array_push ( $LST, $IFQCAL ['QuestionID'] . 'to' );
			}
		} // end foreach	
	}
} // end on

$lst = "";
foreach ( $LST as $cal ) {
	$lst .= "#" . $cal . ", ";
}

$lst = substr ( $lst, 0, - 2 );
if(isset($TemplateObj) && is_object($TemplateObj)) $TemplateObj->lst = $lst;
if(isset($TemplateObj) && is_object($TemplateObj)) $TemplateObj->validate_dates_info = json_encode($validate_dates_info);

$validate_dates_info = json_encode($validate_dates_info);