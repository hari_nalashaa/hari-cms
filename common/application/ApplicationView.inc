<?php
if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];

if (($_REQUEST['ApplicationID']) && isset($OrgID) && ($_REQUEST['RequestID'])) {
	
    // get applicant data
    $APPDATA        =   G::Obj('Applicants')->getAppData ($OrgID, $_REQUEST['ApplicationID'] );
    $applicationsize = count ( $APPDATA );
    
    $results        =   G::Obj('Requisitions')->getRequisitionsDetailInfo("MultiOrgID", $OrgID, $_REQUEST['RequestID']);
    $MultiOrgID     =   $results['MultiOrgID'];
	
    // validate the FormID
    $FormID         =   $APPDATA ['FormID'];
    //Bind parameters
    $params         =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
    //Set condition
    $where          =   array("OrgID = :OrgID", "FormID = :FormID");
    //Get Distinct FormID's count
    $results        =   G::Obj('FormQuestions')->getFormQuestionsInformation(array("DISTINCT(FormID) AS DFormID"), $where, "", array($params));
    $hit            =   $results['count'];
	
	if ($hit < 1) {
		$FormID = 'STANDARD';
	}
	
	//Get Application Form Sections
	$form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
	
	foreach ( $form_sections_info as $form_section_id=>$form_section_info) {
	   $TITLES [$form_section_id] = $form_section_info['SectionTitle'];
    } // end foreach
	
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where     =   array("OrgID = :OrgID", "FormID = :FormID");
	// Query and Set Text Elements
	$results   =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
			$TextBlocks [$row ['TextBlockID']] = $row ['Text'];
		}
	}	
	
	if ($applicationsize < 30) {
		if ($OrgID == 'I20100603') {
			echo '<center><b>Applicant MUST complete an application at the time of interview</b></center><br>';
		}
	}
	
	// Read and Set Logos
	if (! $emailonly) {		
		//Bind parameters
		$params       =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>(string)$MultiOrgID);
		//Set condition
		$where        =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
		//Get Organization Logos Information
		$resultsOL    =   G::Obj('Organizations')->getOrganizationLogosInformation(array("OrgID"), $where, "", array($params));
		$imgcnt       =   $resultsOL['count'];
		
		if ($imgcnt > 0) {
			echo '<img src="' . IRECRUIT_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br>';
		} // end imgcnt
	} // end email only

	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where     =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = 'employment'");
	//Get FormQuestions Information
	$results   =   G::Obj('FormQuestions')->getFormQuestionsInformation("Question", $where, "", array($params));

	$Employment = $results['results'][0]['Question'];
	
	if ($Employment == '') {
		$Employment = 'Employment Status Desired:';
	}
	
	$columns   =   "RequestID, Distinction";
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':ApplicationID'=>$_REQUEST['ApplicationID']);
	//Set condition
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "ApplicationID = :ApplicationID");
	//Get JobApplications Information
	$results   =   G::Obj('Applications')->getJobApplicationsInfo($columns, $where, '', 'Distinction', array($params));

	$ED        =   G::Obj('Applications')->getApplicationDate($OrgID, $_REQUEST['ApplicationID']);
	
	echo '<table border="0" cellspacing="3" cellpadding="0" class="table">';
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $JA) {
		    $multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $JA['RequestID']);
		    
			if ($JA ['Distinction'] == 'P') {
				$TT = 'Position Desired';
			} else {
				$TT = 'Other Position Desired';
			}
			echo '<tr>';
			echo '<td width="20%" nowrap="nowrap">'.$TT . ':</td>';
			echo '<td>';
			echo '<b>(' . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . ')</b>';
			echo '&nbsp;&nbsp;';
			echo '<b>' . G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . '</b>';
			echo '</td>';
			echo '</tr>';
		}
	}
	
	echo '<tr><td width="20%" nowrap="nowrap">Application ID:</td><td><strong>' . $_REQUEST['ApplicationID'] . '</strong></td></tr>';
	echo '<tr><td width="20%" nowrap="nowrap">Application Date:</td><td><strong>' . $ED . '</strong></td></tr>';
	echo '</table>';
	
	echo '<br>' . "\n\n";
	
	echo '<table class="table">';
	echo '<tr>';
	echo '<td width="20%" nowrap="nowrap">'.$Employment.'</td>';
	echo '<td><b>' . $APPDATA ['employment'] . '</b></td>';
	echo '</tr>';
	echo '</table>';
	
	echo '<table class="table">';
	
	echo '<tr>';
	if ($TITLES [1] != "") {
		echo '<td><b class="title">' . $TITLES [1] . '</b></td>';
	}
	echo '</tr>';

	echo '<tr><td>';
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<b>' . $APPDATA ["first"] . ' ' . $APPDATA ["middle"] . ' ' . $APPDATA ["last"] . '</b>';
	echo '</td></tr>';
	
	if(isset($APPDATA ["address"]) && trim($APPDATA ["address"]) && $APPDATA ["address"] != "") 
	{
		echo '<tr><td>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		echo $APPDATA ["address"];
		echo '</td></tr>';
	}	
	
	if(isset($APPDATA ["address2"]) && trim($APPDATA ["address2"]) && $APPDATA ["address2"] != "")
	{
		echo '<tr><td>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		echo $APPDATA ["address2"];
		echo '</td></tr>';
	}
	
	$fromat_address = G::Obj('Address')->formatAddress ( $OrgID, $APPDATA ["country"], $APPDATA ["city"], $APPDATA ["state"], $APPDATA ["province"], $APPDATA ["zip"], $APPDATA ["county"] );
	if(isset($fromat_address) && trim($fromat_address) && $fromat_address != "")
	{
		echo '<tr><td>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		echo $fromat_address;
		echo '</td></tr>';
	}

	if(isset($APPDATA ["country"]) && trim($APPDATA ["country"]) && $APPDATA ["country"] != "")
	{
		echo '<tr><td>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		echo $APPDATA ["country"];
		echo '</td></tr>';
	}
	
	
	if ($permit ['Applicants_Contact'] >= 1) {
		echo '<tr><td>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		echo 'Email Address: <a href="mailto:' . $APPDATA ["email"] . '">' . $APPDATA ["email"] . '</a></td></tr>';
	} else {
		echo '<tr><td>';
		echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		echo 'Email Address: ' . $APPDATA ["email"] . '</td></tr>';
	}
	
	echo '</table>';
	
	echo '<br><br>';
	
	echo printSection ( $OrgID, $FormID, $APPDATA, 1 );	
	
	echo '<br><br>';
	
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':HoldID'=>$_REQUEST['ApplicationID'], ':RequestID'=>$_REQUEST['RequestID']);
	//set columns
	$columns   =   array("Question", "Submission");
	//set condition
	$where     =   array("OrgID = :OrgID", "HoldID = :HoldID", "RequestID = :RequestID");

	//Get Prescreen Results
	$resultsIN =   G::Obj('PrescreenQuestions')->getPrescreenResults($columns, $where, "SortOrder", array($params));
	
	// Prescreen Questions
	$hitIN     =   $resultsIN['count'];
	
	if ($hitIN > 0) {
		
		echo '<b class="title">Prescreen Questions</b><br>';
		echo '<table border="0" cellspacing="3" cellpadding="0" class="table">';

		//Display Prescreen results information
		if(is_array($resultsIN['results'])) {
			foreach ($resultsIN['results'] as $PRESCREEN) {
				echo "<tr><td>" . $PRESCREEN ['Question'] . "?&nbsp;&nbsp;";
				echo "<b>" . $PRESCREEN ['Submission'] . "</b></td></tr>";
			} // end foreach
		}
		
		echo "</table>";
		echo '<br>';
	} // end hit Prescreen

	 $SOCIAL = printSection ( $OrgID, $FormID, $APPDATA, 12 );
     if ($SOCIAL) {
     	if ($SOCIAL[12] != "") {
        	echo '<b class="title">' . $TITLES [12] . '</b><br>' . "\n";
        }
        echo $SOCIAL;
        echo '<br>' . "\n\n";
     } // end SOCIAL
	
	$AFW = printSection ( $OrgID, $FormID, $APPDATA, 2 );
	if (($AFW) || ($CustomQuestionsAvailabilityForWork)) {
		if ($TITLES [2] != "") {
			echo '<b class="title">' . $TITLES [2] . '</b><br>' . "\n";
		}
		echo $AFW;
		echo "\n\n";
		
		if ($OrgID == 'I20090304') {
			echo <<<END
<p>You are required to disclose all sealed (sometimes called "expunged") convictions that would bear a direct and substantial relationship to any position for which you want to be considered. However, you are not required to disclose a juvenile criminal record that has been expunged.  We are required by law to obtain an applicant's criminal record from the state authorities and the FBI.  Those records will list sealed and sometimes expunged records from all states.</p>
echo '<br>' . "\n";
END;
		}
		
		echo $CustomQuestionsAvailabilityForWork;
		echo '<br>' . "\n\n";
	} // end if AFW and Custom
	
	$BCK = printSection ( $OrgID, $FormID, $APPDATA, 3 );
	if (($BCK) || ($CustomQuestionsBackground)) {
		if ($TITLES [3] != "") {
			echo '<b class="title">' . $TITLES [3] . '</b><br>' . "\n";
		}
		echo $BCK;
		echo $CustomQuestionsBackground;
		echo '<br>' . "\n\n";
	} // end BCK and Custom Quesitons
	
	$ED = printSection ( $OrgID, $FormID, $APPDATA, 9 );
	if ($ED) {
		if ($TITLES [9] != "") {
			echo '<b class="title">' . $TITLES [9] . '</b><br>' . "\n";
		}
		echo $ED;
		echo '<br>' . "\n\n";
	} // end ED

	$CURR = printSection ( $OrgID, $FormID, $APPDATA, 11 );
	if ($CURR) {
		if ($TITLES [3] != "") {
			echo '<b class="title">' . $TITLES [11] . '</b><br>' . "\n";
		}
		echo $CURR;
		echo '<br>' . "\n\n";
	} // end CURR
	
	$WRK = printSection ( $OrgID, $FormID, $APPDATA, 4 );
	if (($WRK) || ($CustomQuestionsWorkExperience)) {
		if ($TITLES [4] != "") {
			echo '<b class="title">' . $TITLES [4] . '</b><br>' . "\n";
		}
		if ($OrgID == 'I20090304') {
			echo <<<END
<p>Important:  We contact previous employers; it is an important part of our process. For each job, when you state your reason for leaving, do not be vague. State clearly, for example, if you were discharged, quit to avoid being discharged, or quit for some other reason.  If you were discharged, or about to be discharged, state the reason. If you quit, state the reason and the amount of notice you provided.</p>
END;
		}
		
		echo $WRK;
		echo $CustomQuestionsWorkExperience;
		echo '<br>' . "\n\n";
	} // end WRK and Custom Questions

	$MIL = printSection ( $OrgID, $FormID, $APPDATA, 13 );
    if ($MIL) {
            if ($TITLES [13] != "") {
                    echo '<b class="title">' . $TITLES [13] . '</b><br>' . "\n";
            }
            echo $MIL;
            echo '<br>' . "\n\n";
    } // end MIL
	
	$DRE = printSection ( $OrgID, $FormID, $APPDATA, 5 );
	if ($DRE) {
		if ($TITLES [5] != "") {
			echo '<b class="title">' . $TITLES [5] . '</b><br>' . "\n";
		}
		echo $DRE;
		echo '<br>' . "\n\n";
	} // end DRE and Custom Quesitons
	
	$CR = printSection ( $OrgID, $FormID, $APPDATA, 7 );
	if ($CR) {
		if ($TITLES [7] != "") {
			echo '<b class="title">' . $TITLES [7] . '</b><br>' . "\n";
		}
		echo $CR;
		echo '<br>' . "\n\n";
	} // end CR
	
	$RF = printSection ( $OrgID, $FormID, $APPDATA, 8 );
	if ($RF) {
		if ($TITLES [8] != "") {
			echo '<b class="title">' . $TITLES [8] . '</b><br>' . "\n";
		}
		echo $RF;
		echo '<br>' . "\n\n";
	} // end RF
	
	$SA = printSection ( $OrgID, $FormID, $APPDATA, 10 );
	if ($SA) {
		if ($TITLES [10] != "") {
			echo '<b class="title">' . $TITLES [10] . '</b><br>' . "\n";
		}
		echo $SA;
		echo '<br>' . "\n\n";
	} // end SA
	
	if ($TextBlocks ["ApplicantAuthorizations"]) {
		$ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
	} else {
		$ApplicantAuthorizations = "";
	}
	
	$Signature = '' . $TextBlocks ["Signature"];
	
	if ($OrgID == "I20100301") { // Meritan
		$Signature .= ' I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.';
	} else {
		$Signature .= ' I agree with the above statement.';
	}
	$Signature .= '<br><br>';
	
	$Signature .= 'Electronic Signature: <font style="font-color:red">X</font>&nbsp;<b>' . $APPDATA ['signature'] . '</b>';
	
	if (($TextBlocks ["Signature"]) || ($ApplicantAuthorizations)) {
		echo '<table border="0" cellspacing="3" cellpadding="0" class="table">';
		echo '<tr><td><b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '</td></tr>';
		echo '<tr><td>';
		echo $Signature;
		echo '<td></tr>';
		echo '</table>';
		echo '<br>' . "\n\n";
	}
} // end if ApplicationID, OrgID, RequestID

if ($applicationsize < 30) {
	if ($OrgID == 'I20100603') {
		echo '<br><center><b>Applicant MUST complete an application at the time of interview</b></center>';
	}
}

// **** FUNCTIONS **** //
function printSection($OrgID, $FormID, $APPDATA, $SectionID) {
	
	global $permit, $feature;
	
	//Set the parameters
	$params    =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
	//Set columns
	$columns   =   array("Question", "QuestionID", "QuestionTypeID", "value", "SectionID");
	//Set condition
	$where     =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
	
	//Get Questions information
	$rslt      =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));
	
	$rtn       =   '';
	
	$rowcnt = $rslt['count'];
	
	foreach ($rslt['results'] as $FQ) {
		
		// format answers
		if ($FQ ['QuestionID'] == 'sin') {
			if (($APPDATA ['sin1']) || ($APPDATA ['sin2']) || ($APPDATA ['sin3'])) {
				if ($_GET ['emailonly'] == 'Y') {
					$APPDATA [$FQ ['QuestionID']] = 'XXX-XX-XXXX';
				} else {
					$APPDATA [$FQ ['QuestionID']] = $APPDATA ['sin1'] . '-' . $APPDATA ['sin2'] . '-' . $APPDATA ['sin3'];
				}
			} // end if not blank
		} // end sin
		
		if ($FQ ['QuestionID'] == 'social') {
			if (($APPDATA ['social1']) || ($APPDATA ['social2']) || ($APPDATA ['social3'])) {
				if ($_GET ['emailonly'] == 'Y') {
					$APPDATA [$FQ ['QuestionID']] = 'XXX-XX-XXXX';
				} else {
					$APPDATA [$FQ ['QuestionID']] = $APPDATA ['social1'] . '-' . $APPDATA ['social2'] . '-' . $APPDATA ['social3'];
				}
			} // end if not blank
		} // end social
		
		if ($FQ ['QuestionID'] == 'cellphone') {
			
			$APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA ['cellphone1'], $APPDATA ['cellphone2'], $APPDATA ['cellphone3'], '' );
		} // end cellphone
		
		if ($FQ ['QuestionID'] == 'homephone') {
			
			$APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA ['homephone1'], $APPDATA ['homephone2'], $APPDATA ['homephone3'], '' );
		} // end homephone
		
		if ($FQ ['QuestionID'] == 'busphone') {
			
			$APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA ['busphone1'], $APPDATA ['busphone2'], $APPDATA ['busphone3'], $APPDATA ['busphoneext'] );
		} // end busphone
		
		if ($FQ ['value'] && $FQ ['QuestionTypeID'] != 100 && $FQ ['QuestionTypeID'] != 120) {
			$APPDATA [$FQ ['QuestionID']] = getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
		}

		if ($FQ ['QuestionID'] == 'cur_phone') {
			$APPDATA [$FQ ['QuestionID']] = G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $APPDATA ['cur_phone1'], $APPDATA ['cur_phone2'], $APPDATA ['cur_phone3'], '' );
		} // end cur_phone
		
		if (($FQ ['QuestionTypeID'] == 18) || ($FQ ['QuestionTypeID'] == 1818) || ($FQ ['QuestionTypeID'] == 9)) {
			$APPDATA [$FQ ['QuestionID']] = getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
		}
		
		if (($FQ ['QuestionTypeID'] == 1) || ($FQ ['QuestionTypeID'] == 24)) {
			$APPDATA [$FQ ['QuestionID']] = "from " . $APPDATA [$FQ ['QuestionID'] . from] . " to " . $APPDATA [$FQ ['QuestionID'] . to];
		}
		
		$print = "Y";
		
		if ($feature ['LimitApplicationToAnswersOnly'] == "Y") {
			
			if (($APPDATA [$FQ ['QuestionID']] == "") || ($APPDATA [$FQ ['QuestionID']] == "from  to ")) {
				$print = "N";
			}
			if (($FQ ['QuestionTypeID'] == 99) || ($FQ ['QuestionTypeID'] == 98)) {
				$print = "N";
			}
		} // end feature
		
		if ($print == "Y") {
			
			if ($FQ ['QuestionTypeID'] == 100) {
				
				if($APPDATA [$FQ ['QuestionID']] != "") {

					$canswer = @unserialize($APPDATA [$FQ ['QuestionID']]);
					
					if(is_array($canswer)) {
						$rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
						$rtn .= '<tr>';
						$rtn .= '<td width="20%">'.$FQ ['Question'].'</td>';
						$rtn .= '</tr>';
						
						foreach ($canswer as $cakey=>$caval) {
							$rtn .= '<tr><td>'.$cakey.'</td>';
							
							foreach ($caval as $cavk=>$cavv) {
								$rtn .= '<td>'.$cavv.'</td>';
							}
							
							$rtn .= '</tr>';
						}
							
						$rtn .= '</table>';
					}
				}
			}
			else if($FQ ['QuestionTypeID'] == 120) {
				if($APPDATA [$FQ ['QuestionID']] != "") {
					$canswer = unserialize($APPDATA [$FQ ['QuestionID']]);
					
					if(is_array($canswer)) {
						$rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
						$rtn .= '<tr>';
						$rtn .= '<td colspan="5">'.$FQ ['Question'].'</td>';
						$rtn .= '</tr>';
						
						$days_count = count($canswer['days']);
						
						
						for($ci = 0; $ci < $days_count; $ci++)  {
							$rtn .= '<tr>';
							$rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
							$rtn .= '<td width="5%">from</td>';
							$rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
							$rtn .= '<td width="5%">to </td>';
							$rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
							$rtn .= '</tr>';
						}
						
						$rtn .= '</table>';
					}
				}
				
			}
			else if (($FQ ['QuestionTypeID'] == 99) || ($FQ ['QuestionTypeID'] == 98)) {
				$rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
				$rtn .= '<tr><td colspan="100%">';
				$rtn .= $FQ ['Question'];
			} elseif (($FQ ['QuestionID'] != 'salarydesireddefinition') && ($FQ ['QuestionID'] != 'cur_salaryper')) {
				$rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
				$rtn .= '<tr><td width="20%" valign="top">';
				if($FQ ['QuestionID'] != 'middle') $rtn .= $FQ ['Question'];
				$rtn .= '</td><td valign="top">';
			} else {
				$rtn .= '&nbsp;&nbsp;' . $FQ ['Question'] . '&nbsp;&nbsp;';
			}
			
			if (($FQ ['QuestionTypeID'] != 99) && ($FQ ['QuestionTypeID'] != 98) && ($FQ ['QuestionTypeID'] != 100) && ($FQ ['QuestionTypeID'] != 120)) {
				if($FQ ['QuestionID'] != 'middle') $rtn .= '<b>' . $APPDATA [$FQ ['QuestionID']] . '</b>';
			}
			
			//if (($FQ ['QuestionID'] != 'salarydesired') && ($FQ ['QuestionID'] != 'cur_salary')) {
				$rtn .= '</td></tr>';
				$rtn .= '</table>';
			//}
			
		} // end if print
	} // end while
	
	if ($rowcnt == 0) {
		$rtn = '';
	}
	
	return $rtn;
} // end of function

function getDisplayValue($ans, $displaychoices) {
	$rtn = '';
	
	if ($ans != '') {
		$Values = explode ( '::', $displaychoices );
		foreach ( $Values as $v => $q ) {
			list ( $vv, $qq ) = explode ( ":", $q, 2 );
			if ($vv == $ans) {
				$rtn .= $qq;
			}
		}
	}
	return $rtn;
} // end of function

function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
	$rtn = '';
	$val = '';
	$lex = '';
	
	if (($id == 'daysavailable') || ($id == 'typeavailable')) {
		
		$le = ', ';
		$li = '';
		$lecnt = - 2;
	} else {
		
		$le = '<br>';
		$li = '&#8226;&nbsp;';
		$lecnt = - 4;
	}
	
	$cnt = $APPDATA [$id . 'cnt'];
	
	for($i = 1; $i <= $cnt; $i ++) {
		
		$val = getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
		if ($val) {
			$rtn .= $li . $val;
			$lex = 'on';
			$val = '';
		}
		
		if ($questiontype == 9) {
			
			$val = $APPDATA [$id . '-' . $i . '-yr'];
			if ($val) {
				$rtn .= ',&nbsp;' . $val . ' yrs. ';
				$lex = 'on';
				$val = '';
			}
			
			$val = $APPDATA [$id . '-' . $i . '-comments'];
			if ($val) {
				$rtn .= '&nbsp;(' . $val . ') ';
				$lex = 'on';
				$val = '';
			}
		} // end questiontype
		
		if ($lex == 'on') {
			$rtn .= $le;
			$lex = '';
		}
	}
	
	$rtn = substr ( $rtn, 0, $lecnt );
	
	return $rtn;
} // end of function
?>