<?php
/**
 * Build the linkedin requested url
 */
$siteurl  = "";
$siteurl  = ($_SERVER['HTTPS'] == "on") ? "https://" : "http://";
$siteurl .= $_SERVER['HTTP_HOST'];

$_SESSION['I']['LIURL']             =   $siteurl.$_SERVER['REQUEST_URI'];
$_SESSION['I']['LIIRECRUITHOME']    =   IRECRUIT_HOME;

$edlist = array();
if(isset($_SESSION['I']['LIEDUCATION']) && count($_SESSION['I']['LIEDUCATION']) > 0) {
    $edvalues = $_SESSION['I']['LIEDUCATION']['values'];
    
    for($ei = 0; $ei < count($edvalues); $ei++) {
        $eyear                              =   $edvalues[$ei]['endDate']['year'];
        $edlist[$eyear]['year']				= 	$eyear;
        $edlist[$eyear]['degree']			= 	$edvalues[$ei]['degree'];
        $edlist[$eyear]['fieldOfStudy']		= 	$edvalues[$ei]['fieldOfStudy'];
        $edlist[$eyear]['schoolName']		= 	$edvalues[$ei]['schoolName'];
        $edlist[$eyear]['startDate']		= 	$edvalues[$ei]['startDate']['year'];
    }
    
}

if(is_array($edlist)) {
    //Sort array based on key(year)
    ksort($edlist);
    
    foreach($edlist as $evalue) {
        $edulist[] = $evalue;
    }
}

if(count($_SESSION['I']['LIUSER']) > 0) {
    $skillvalues = $_SESSION['I']['LIUSER']['skills']['values'];
    $skillnames = "";
    if(is_array($skillvalues)) {
        for($ski = 0; $ski < count($skillvalues); $ski++) {
            $skills[] = $skillvalues[$ski]['skill']['name'];
        }
        $skillnames = implode(",", $skills);
    }
}


$personalinfo   =   "";

/**
 * Set Information From Linkedin
 */
$LIOBJ          =   $_SESSION['I']['LIUSER'];

$liafirst       =   isset($APPDATA['first']) ? $APPDATA['first'] : "";
$lialast        =   isset($APPDATA['last']) ? $APPDATA['last'] : "";
$liaemail       =   isset($APPDATA['email']) ? $APPDATA['email'] : "";

if(count($_SESSION['I']['LIUSER']) > 0) {
    $liafirst   =   $LIOBJ['firstName'];
    $lialast    =   $LIOBJ['lastName'];
    $liaemail   =   $LIOBJ['emailAddress'];
}

if(count($LIOBJ) == 0 && isset($MApplicantInfo) && count($MApplicantInfo) == 0) {
    /**
     * Set Information From Monster
     */
    $liafirst  = ($MApplicantInfo['FirstName'] == "") ? $APPDATA['first'] : $MApplicantInfo['FirstName'];
    $lialast   = ($MApplicantInfo['LastName'] == "") ? $APPDATA['last'] : $MApplicantInfo['LastName'];
    $liaemail  = ($MApplicantInfo['EmailAddress'] == "") ? $APPDATA['email'] : $MApplicantInfo['EmailAddress'];
}

if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'onboardapplicant') {
    if(isset($_REQUEST['first'])) $liafirst = $_REQUEST['first'];
    if(isset($_REQUEST['middle'])) $APPDATA[middle] = $_REQUEST['middle'];
    if(isset($_REQUEST['last'])) $lialast = $_REQUEST['last'];
    if(isset($_REQUEST['address'])) $APPDATA[address] = $_REQUEST['address'];
    if(isset($_REQUEST['address2'])) $APPDATA[address2] = $_REQUEST['address2'];
    if(isset($_REQUEST['zip'])) $APPDATA[zip] = $_REQUEST['zip'];
    if(isset($_REQUEST['email'])) $liaemail = $_REQUEST['email'];
}


$lbl_class = ' class="question_name"';
if(isset($QuestionTypeID) && ($QuestionTypeID == 99 || $QuestionTypeID == 5)) {
    $lbl_class = " class='question_name labels_auto'";
}

########################

$sec_app_form   =   "";

//Set parameters for prepared query
$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$section_id);
//Set Condition
$where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
//Get FormQuestions
$results    =   G::Obj('FormQuestions')->getFormQuestionsInformation("QuestionID, QuestionTypeID", $where, "QuestionOrder ASC", array($params));

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $QuestionID		=	$row['QuestionID'];
        $QuestionTypeID =	$row['QuestionTypeID'];
        $sec_app_form   .=  include 'DisplayQuestions.inc' ;
    }
}

return $sec_app_form;
?>
