<?php
$getdata = true;

// Get data based on session
if ($_SESSION['P'][$OrgID.$MultiOrgID]['MonsterRequestURL'] != "") {
    $ApplicantJsonData  =   G::Obj('Curl')->get ( $_SESSION['P'][$OrgID.$MultiOrgID]['MonsterRequestURL'] );
    $MApplicantInfo     =   json_decode ( $ApplicantJsonData, true );
    $getdata            =   false;
}
// Get data based on cookie
if ($_COOKIE ['MonsterRequestURL'] != "" && $getdata && count($MApplicantInfo) == 0) {
    $ApplicantJsonData  =   G::Obj('Curl')->get ( $_COOKIE ['MonsterRequestURL'] );
    $MApplicantInfo     =   json_decode ( $ApplicantJsonData, true );
}

// Get monster data based on cookie
if (count($MApplicantInfo) == 0) {
    $MonsterRequestUrlInfo  =   G::Obj('Monster')->getMonsterSessionCookieByCookieApp($HoldID, $RequestID);
	if($MonsterRequestUrlInfo['MonsterRequestURL'] != "") {
        $ApplicantJsonData  =   G::Obj('Curl')->get ( $MonsterRequestUrlInfo['MonsterRequestURL'] );
        $MApplicantInfo     =   json_decode ( $ApplicantJsonData, true );
	}
	
	if (count($MApplicantInfo) == 0) {
		$MonsterRequestUrlInfo = G::Obj('Monster')->getMonsterSessionCookieInfoByRandID($RandID, $RequestID);
		if($MonsterRequestUrlInfo['MonsterRequestURL'] != "") {
		    $ApplicantJsonData  =   G::Obj('Curl')->get ( $MonsterRequestUrlInfo['MonsterRequestURL'] );
            $MApplicantInfo     =   json_decode ( $ApplicantJsonData, true );
		}
	}
}

$multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

$organizationlistings = <<<END
<div class="row">
<div class="col-lg-6">
END;

if ($OrgID == "I20101203") {
	$organizationlistings .= <<<END
Thank you for your interest in employment with the Center For Literacy. Please read the following information carefully.<br><br>
 
Although many fields listed below are optional, it is important to respond to all questions in either the fields below or by attaching supporting schedules.  <br><br>
 
For example: You are encouraged to attach a resume as long as you clearly list your dates of employment.  You may attach a document of references instead of retyping your references. <br><br>
 
CFL only accepts applications in response to posted positions and only through this portal; CFL does not does not accept inquires via e-mail or direct mail.<br><br>
 
Thank you again for your interest in the Center For Literacy.<br>
<br><br>
END;
}

$organizationlistings .= 'Application for <b>' . G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID ) . '</b>.';

$organizationlistings .= 'Items marked with a red asterisk(<font style="color:red">*</font>) are required.';
$organizationlistings .= '</div>';

//Set condition
$where              =   array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params             =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get Post Requisitions Information
$post_req_results   =   G::Obj('Requisitions')->getPostRequisitionsInformation("*", $where, "", array($params));

$rowpostedjob       =   $post_req_results['results'][0];
$num_rows           =   $post_req_results['count'];

//Get requisition information
$requisitioninfo    =   G::Obj('Requisitions')->getReqDetailInfo("*", $OrgID, $MultiOrgID, $RequestID);

//Get User Information
$userinfo           =   G::Obj('IrecruitUsers')->getUserInfoByUserID($requisitioninfo ['Owner'], "*");

$roworginfo         =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "*");


$joblocation        =   $roworginfo ['City'] . ", " . $roworginfo ['State'] . " " . $roworginfo ['ZipCode'];

// Make unique variable to control the monster, linkedin, existing user information
$MRequestID         =   $RequestID;
$MCompanyName       =   $roworginfo ['OrganizationName'];
$MJobTitle          =   $requisitioninfo ['Title'];
$MJobLocation       =   $joblocation;
$MEmail             =   $userinfo ['EmailAddress'];

$MonsterRecords     =   $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
$MonsterInfo        =   $MonsterRecords ['row'];

$FlagAwm            =   $MonsterRecords ['flagawm'];
$NumRowsConfig      =   $MonsterRecords ['numrows'];

if ($feature ['ApplyWithLinkedIn'] == "Y") {
	
	if (FROM_SRC == "PUBLIC") {
		$linkedinaccess = PUBLIC_HOME . "linkedin/linkedInAccess.php";
	} else if (FROM_SRC == "USERPORTAL") {
		$linkedinaccess = USERPORTAL_HOME . "linkedin/linkedInAccess.php";
	} else {
		$linkedinaccess = IRECRUIT_HOME . "linkedin/linkedInAccess.php";
	}
	
	$organizationlistings .= '<div class="col-lg-3">';
	$organizationlistings .= '<a href="' . $linkedinaccess . '"><img src="' . IRECRUIT_HOME . 'images/apply_with_linkedin.png"></a>';
	$organizationlistings .= '</div>';
} // end feature Apply with LinkedIn

if ($feature ['MonsterAccount'] == "Y") {
	/**
	 * This button will display based on 1 conditions
	 * 1) Based on backend configuration
	 */
	if ($FlagAwm == 'yes') {
		
		$vendorfield = $OrgID . "*" . $MultiOrgID;
		
		$organizationlistings .= '<div class="col-lg-3">';
		$organizationlistings .= '<div id="awm_widget" style="margin-top:5px"></div>
								  <script id="awm" type="text/javascript"
									       data-api-key="' . $MonsterInfo ['ApiKey'] . '"
									       data-companyname="' . $MCompanyName . '"
									       data-jobtitle="' . $MJobTitle . '"
									       data-joblocation="' . $MJobLocation . '"
									       data-jobrefcode="' . $MRequestID . '"
									       data-accountkey="' . $MonsterInfo ['SecretKey'] . '"
									       data-emailaddress="' . $MEmail . '"
									       data-deliverymethod="POST"
									       data-deliveryformat="JSON"
									       data-posturl="' . IRECRUIT_HOME . 'monster/saveMonsterApplicantInfo.php"
									       data-onclick=""
									       data-onsuccess=""
									       data-oncontinue="monster_request_on_continue"
									       data-vendorfield="' . $vendorfield . '"
									       src="//login.monster.com/awm/en_US/awm.js">
								  </script>';
		$organizationlistings .= '</div>';
	}
} // end feature Apply with Monster

$organizationlistings .= '</div>';
$organizationlistings .= '<div style="clear:both"></div>';

return $organizationlistings;
?>