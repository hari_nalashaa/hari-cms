<p><b>Purchase Policy</b> - <font size="-1">last updated: October, 2018</font></p>

<p><b>Security</b></p>

Cost Management Services, LLC "iRecruit" uses Intuit merchant services to keep your data safe and secure. We never store your credit card information.<br><br>

Only partial information is kept to associate you with your purchase and our services.<br><br>
 
Please reference your purchase number when inquiring about a purchase.<br><br>

<p><b>Payment Methods</b></p>
<img src="<?php echo IRECRUIT_HOME?>images/creditcards_sm.jpg">

<p>"iRecruit" utilizes the services of Visa, MasterCard, American Express, and Discover. If you find yourself in need of another type of payment method you can make arrangements by calling 1-860-678-4401 or mailing a check or money order to:</p>

&nbsp;&nbsp;&nbsp;Cost Management Services, LLC<br>
&nbsp;&nbsp;&nbsp;"iRecruit"<br>
&nbsp;&nbsp;&nbsp;321 Main Street<br>
&nbsp;&nbsp;&nbsp;Farmington, CT 06032<br><br>

<p><b>Taxes:</b></p>

<p>Software as a service is non-taxable item in CT. You are responsible for paying the taxes in any respective state that does not recognize this method.</p>

<p><b>Shipping and Handling:</b></p>

<p>All services are delivered in real time over the Internet. Delivery of a service is considered instant upon completion of a purchase and is time-period derived.</p>

<p><b>Cancelation and Refunds</b></p>

<p>The Advertiser ("You") must notify .iRecruit. by mid-night of the day of the posting to cancel your sponsor advertisements. After mid night of the same day, the Advertiser ("You") will be responsible for all fees and other charges incurred through the effective date of termination. Flat Fee postings may be removed from our site at any time but are non-refundable. No credit will be given if the posting has been active less than the 15 or 30 days offered in the posting package.</p>
