<?php
$ERROR .= "";


/**
 * @tutorial	This statement is common for all the
 * 				function insUpdApplicantDataTemp() calls
 * 				In this file, if the data or the columns are different
 * 			    Please update the statement parameter with different variable
 * 				so that it will not affect next function calls
 */
//Default update information for all applicant data temp function calls
$on_update 	    =   ' ON DUPLICATE KEY UPDATE Required = :URequired';
$update_info    =   array(":URequired"=>'REQUIRED');
//Default insert information for all applicant data temp conditions in this file
//Remaining fields will be pushed to this array based on the below condition
$insert_info    =   array('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'Answer'=>'', 'Required'=>'REQUIRED');

// check all the required and active FormQuestions
$where          =   array (
                        "OrgID                =   :OrgID",
                        "PreFilledFormID      =   :PreFilledFormID",
                        "Required             =   'Y'",
                        "Active               =   'Y'" 
                    );
$params         =   array (
                        ":OrgID"              =>  'MASTER',
                        ":PreFilledFormID"    =>  $PreFilledFormID 
                    );
//Get Prefilled Form Questions Information
$results        =   $FormQuestionsObj->getQuestionsInformation ( 'PreFilledFormQuestions', "*", $where, "QuestionOrder", array ($params) );

if ((($PreFilledFormID == "FE-I9") || ($PreFilledFormID == "FE-I9-2020")) && ($OrgID == "I20140912")) { // Community Research Foundation
    $results['results'][] = array("OrgID"=>"MASTER","PreFilledFormID"=>$PreFilledFormID,"QuestionID"=>"MiddleName","Question"=>"Middle Initial:","QuestionTypeID"=>"6","Required"=>"Y");
} // end middle inital requirement

if(is_array($results ['results'])) {
	foreach ( $results ['results'] as $PFQCK ) {

	    if($FQCK['QuestionTypeID'] == 100 || $FQCK['QuestionTypeID'] == 120) {
	        if($FQCK['QuestionTypeID'] == 100) {
	            if(!isset($_POST['LabelSelect'])) {
	                $ERROR .= " - " . $FQCK ['Question'] . ' is missing.' . "\\n";
	                G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['LabelSelect'][$FQCK ['QuestionID']]));
	            }
	            else if(isset($_POST['LabelSelect'])) {
	                if(is_array($_POST['LabelSelect'][$FQCK ['QuestionID']])) {
	                    foreach($_POST['LabelSelect'][$FQCK ['QuestionID']] as $cus_que100_label=>$cus_que100_value) {
	                        if(is_array($cus_que100_value)) {
	                            foreach($cus_que100_value as $cus_que100_lbl_info=>$cus_que100_lbl_val) {
	                                if($cus_que100_lbl_val == "") {
	                                    $ERROR .= " - Please select required option in " . $FQCK ['Question'] . "\\n";
	                                    G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['LabelSelect'][$FQCK ['QuestionID']]));
	                                }
	                            }
	                        }
	                         
	                    }
	                }
	            }
	        }
	        if($FQCK['QuestionTypeID'] == 120) {
	            if(!isset($_POST['shifts_schedule_time'])) {
	                $ERROR .= " - " . $FQCK ['Question'] . ' is missing.' . "\\n";
	                G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]) );
	            }
	            else if(isset($_POST['shifts_schedule_time'])) {
	                if(is_array($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['from_time'])) {
	                    $from_available_flag = false;
	                    foreach($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['from_time'] as $cus_que120_from_val) {
	                        if($cus_que120_from_val == "") {
	                            $from_available_flag = true;
	                        }
	                    }
	                    if($from_available_flag == true)
	                    {
	                        G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]));
	                        $ERROR .= " - " . $FQCK ['Question'] . ' from time fields are missing.' . "\\n";
	                    }
	                }
	                if(is_array($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['to_time'])) {
	                    $to_available_flag = false;
	                    foreach($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['to_time'] as $cus_que120_to_val) {
	                        if($cus_que120_to_val == "") {
	                            $to_available_flag = true;
	                        }
	                    }
	                    if($to_available_flag == true) {
	                        G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]));
	                        $ERROR .= " - " . $FQCK ['Question'] . ' to time fields are missing.' . "\\n";
	                    }
	                }
	            }
	        }
	    }
	    else {
	    	
	        if (in_array ( $PFQCK ['QuestionTypeID'], array (
	            '13',
	            '14',
	            '19'
	        ) )) { // three part answer
	        
	            $xi = 1;
	        
	            while ( $xi <= 3 ) {
	                	
	                $value = $PFQCK ['QuestionID'] . $xi;
	                	
	                if ($_POST [$value] == "") {
	                    $ERROR .= " - " . $tsect . ' ' . strip_tags ( $PFQCK ['Question'] ) . ' item ' . $sect . ' is missing.' . "\\n";
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $value;
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                    break;
	                } // end if
	                	
	                $xi ++;
	            } // end while
	        } else if ($PFQCK ['QuestionTypeID'] == 15) { // SSN
	        
	            // take out everything except numbers and trim it.
	            $ssnck = trim ( preg_replace ( "/[^0-9]/", "", $_POST [$PFQCK ['QuestionID'] . '1'] . $_POST [$PFQCK ['QuestionID'] . '2'] . $_POST [$PFQCK ['QuestionID'] . '3'] ) );
	            $ssnlength = strlen ( $ssnck );
	        
	            if ($ssnck == "") {
	                $ERROR .= " - " . strip_tags ( $PFQCK ['Question'] ) . ' is missing.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	            } else if ($ssnlength < 9) {
	                $ERROR .= " - " . strip_tags ( $PFQCK ['Question'] ) . ' is missing digits.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	            } else {
	                $ssnck1 = $_POST [$PFQCK ['QuestionID'] . '1']; // first three
	                $ssnck2 = $_POST [$PFQCK ['QuestionID'] . '3']; // last four
	                $ssnck3 = $_POST [$PFQCK ['QuestionID'] . '2']; // fourth and fith
	                	
	                if ($ssnck1 == "000") {
	                    $erck ++;
	                }
	                if ($ssnck1 == "666") {
	                    $erck ++;
	                }
	                if (($ssnck1 >= "900") && ($ssnck1 <= "999")) {
	                    $erck ++;
	                }
	                if ($ssnck3 == "00") {
	                    $erck ++;
	                }
	                if ($ssnck2 == "0000") {
	                    $erck ++;
	                }
	                	
	                if ($erck > 0) {
	                    $ERROR .= " - Social Security Number is invalid." . "\\n";
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	            }
	        } else if ($PFQCK ['QuestionTypeID'] == 1) { // to and from date
	        
	            $emp_from = $PFQCK ['QuestionID'] . 'from';
	            $emp_to = $PFQCK ['QuestionID'] . 'to';
	        
	            if (($_POST [$emp_to] == "") || ($_POST [$emp_from] == "")) {
	                $ERROR .= " - " . $tsect . " section " . $sect . " - " . strip_tags ( $PFQCK ['Question'] ) . " is missing." . "\\n";
	                if ($_POST [$emp_to] == "") {
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $emp_to;
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	                	
	                if ($_POST [$emp_from] == "") {
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $emp_from;
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	            } // end if
	        } else if (in_array ( $PFQCK ['QuestionTypeID'], array (
	            '18',
	            '9',
	            '1818'
	        ) )) { // checkboxes
	        
	            $value = $PFQCK ['QuestionID'] . 'cnt';
	            $valcnt = $_POST [$value];
	        
	            $i = 0;
	            $hitone = 0;
	            while ( $i < $valcnt ) {
	                $i ++;
	                $entry             =   $PFQCK ['QuestionID'] . '-' . $i;
	                $entry_comments    =   $entry . '-comments';
	                $entry_yr          =   $entry . '-yr';
	                	
	                if (($PFQCK ['QuestionTypeID'] == 9) && ($_POST [$entry] != "") && ($_POST [$entry_comments] != "") && ($_POST [$entry_yr] != "")) {
	                    $hitone ++;
	                }
	                	
	                if (($PFQCK ['QuestionTypeID'] == 18) && ($_POST [$entry] != "")) {
	                    $hitone ++;
	                } // end if
	                	
	                if (($PFQCK ['QuestionTypeID'] == 1818) && ($_POST [$entry] != "")) {
	                    $hitone ++;
	                } // end if
	            } // end while
	        
	            if ($hitone == 0) {
	                $ERROR .= " - " . strip_tags ( $PFQCK ['Question'] ) . ' is missing.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	            }
	        } else if ($PFQCK ['QuestionTypeID'] == 17) { // date
	        
	            if ((($PreFilledFormID == "FE-I9") || ($PreFilledFormID == "FE-I9-2020")) && ($PFQCK ['QuestionID'] == "DOB")) { // DOB
	                	
	                $parts     =   explode ( '/', $_POST [$PFQCK ['QuestionID']] );
	                $newdate   =   "$parts[2]-$parts[0]-$parts[1]";
	                	
	                // Get Date Difference
	                $dobck     =   $MysqlHelperObj->getDateDiffWithNow ( $newdate );
	                $age       =   365 * 16;
	                	
	                if ($dobck < $age) {
	                    $ERROR .= " - " . strip_tags ( $PFQCK ['Question'] ) . ' needs to be at least 16 years ago.' . "\\n";
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	            } else {
	                	
	                if ($_POST [$PFQCK ['QuestionID']] == "") {
	                    $ERROR .= " - " . strip_tags ( $PFQCK ['Question'] ) . ' is missing.' . "\\n";
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	            }
	        } else if ($PFQCK ['QuestionTypeID'] == 30) { // Signature
	            // / - captcha catches this
	        } else {
	        
	            if ($_POST [$PFQCK ['QuestionID']] == "") {
	                $ERROR .= " - " . strip_tags ( $PFQCK ['Question'] ) . ' is missing.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $PFQCK ['QuestionID'];
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	            } // end if Question
	        }	        
	        
	        
	    }

	}	
}


if ($EmailAddress != "") {
	$EMERROR .= $ValidateObj->validate_email_address ( $EmailAddress );
	if ($EMERROR != "") {
		$ERROR .= " - " . $EMERROR;
		//Set QuestionID to insert
		$insert_info['QuestionID'] = 'EmailAddress';
		//Insert Update Applicant Data Temp
		G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	}
}

if(FROM_SRC == 'USERPORTAL') {
    //Validate User Portal Agreement Form Captcha
    $ERROR .= $ValidateFormsCaptchaObj->validateUserPortalPreFilledFormCaptcha($insert_info, $on_update, $update_info);
}
else {
    //Validate Irecruit Agreement Form Captcha
    $ERROR .= $ValidateFormsCaptchaObj->validateIrecruitPreFilledFormCaptcha($insert_info, $on_update, $update_info);
}

return $ERROR;
?>
