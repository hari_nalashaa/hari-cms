<?php
/**
 * This is a temporary code until the final upgrade.
 * Upto that time, we have to keep on adding different conditions to it.
 * After final upgrade we will have to remove the loop through $_POST
 */
$form_que_list         =    G::Obj('PreFilledFormQuestions')->getPreFilledFormQuestionsList('MASTER', $PreFilledFormID);

$ques_to_skip          =    array();
$web_form_data_que_ids =    array();

foreach($form_que_list as $QuestionID=>$QuestionInfo) {
    
    $QI                 =   $QuestionInfo;
    G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
    $values             =   G::Obj('GetFormPostAnswer')->getDisplayValue($QI['value']);
    
    $QI['OrgID']        =   $OrgID;
    
    if($QI['QuestionTypeID'] == 13) {
        //After the complete upgrade have to remove this.
        $ques_to_skip[]        =   $QI['QuestionID']."1";
        $ques_to_skip[]        =   $QI['QuestionID']."2";
        $ques_to_skip[]        =   $QI['QuestionID']."3";
        
        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
        $QI['ApplicationID']   =   $ApplicationID;
        $QI['RequestID']       =   $RequestID;
        $QI['AnswerStatus']    =   1;
        
        // Bind the parameters
        $params                 =   array(
            "OrgID"        => $OrgID,
            "HoldID"       => $HoldID,
            "QuestionID"   => $QuestionID,
            "Answer"       => $QI['Answer'],
        );
        
        //Have to work on web form insert call
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params  );
    }
    else if($QI['QuestionTypeID'] == 14) {
        
        //After the complete upgrade have to remove this.
        $ques_to_skip[]        =   $QI['QuestionID']."1";
        $ques_to_skip[]        =   $QI['QuestionID']."2";
        $ques_to_skip[]        =   $QI['QuestionID']."3";
        $ques_to_skip[]        =   $QI['QuestionID']."ext";
        
        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
        $QI['ApplicationID']   =   $ApplicationID;
        $QI['RequestID']       =   $RequestID;
        $QI['AnswerStatus']    =   1;
        
        // Bind the parameters
        $params                 =   array(
            "OrgID"        => $OrgID,
            "HoldID"       => $HoldID,
            "QuestionID"   => $QuestionID,
            "Answer"       => $QI['Answer'],
        );
        
        //Have to work on web form insert call
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params  );
    }
    else if($QI['QuestionTypeID'] == 15) {
        
        //After the complete upgrade have to remove this.
        $ques_to_skip[]        =   $QI['QuestionID']."1";
        $ques_to_skip[]        =   $QI['QuestionID']."2";
        $ques_to_skip[]        =   $QI['QuestionID']."3";
        $ques_to_skip[]        =   $QI['QuestionID'];
        
        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
        $QI['ApplicationID']   =   $ApplicationID;
        $QI['RequestID']       =   $RequestID;
        $QI['AnswerStatus']    =   1;
        
        // Bind the parameters
        $params                 =   array(
            "OrgID"        => $OrgID,
            "HoldID"       => $HoldID,
            "QuestionID"   => $QuestionID,
            "Answer"       => $QI['Answer'],
        );
        
        //Have to work on web form insert call
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params  );
    }
    else if($QI['QuestionTypeID'] == 9) {
        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
        $QI['ApplicationID']   =   $ApplicationID;
        $QI['RequestID']       =   $RequestID;
        $QI['AnswerStatus']    =   1;
        
        //After the complete upgrade have to remove this.
        $qis  =   0;
        foreach ( $values as $v => $q ) {
            $qis++;
            
            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis;
            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-yr";
            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-comments";
        }
        
        $ques_to_skip[]        =   $QI['QuestionID'];
        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
        
        // Bind the parameters
        $params                 =   array(
            "OrgID"        => $OrgID,
            "HoldID"       => $HoldID,
            "QuestionID"   => $QuestionID,
            "Answer"       => $QI['Answer'],
        );
        
        //Have to work on web form insert call
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params  );
    }
    else if($QI['QuestionTypeID'] == 18) {
        
        //After the complete upgrade have to remove this.
        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
        
        for($c18 = 1; $c18 <= $cnt; $c18++) {
            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
        }
        $ques_to_skip[]        =   $QI['QuestionID'];
        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
        
        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
        $QI['ApplicationID']   =   $ApplicationID;
        $QI['RequestID']       =   $RequestID;
        $QI['AnswerStatus']    =   1;
        
        $que_vals              =   explode ( '::', $QI['value'] );
        $que_vals_cnt          =   count($que_vals);
        
        $que_values_info       =   array();
        for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
            $que_vals_info     =   explode(":", $que_vals[$qvc]);
            $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
        }
        
        // Bind the parameters
        $params                 =   array(
            "OrgID"        => $OrgID,
            "HoldID"       => $HoldID,
            "QuestionID"   => $QuestionID,
            "Answer"       => $QI['Answer'],
        );
        
        //Have to work on web form insert call
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params  );
    }
    else if($QI['QuestionTypeID'] == 1818) {
        
        //After the complete upgrade have to remove this.
        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
        
        for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c1818;
        }
        $ques_to_skip[]        =   $QI['QuestionID'];
        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
        
        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
        $QI['ApplicationID']   =   $ApplicationID;
        $QI['RequestID']       =   $RequestID;
        $QI['AnswerStatus']    =   1;
        
        $que_vals              =   explode ( '::', $QI['value'] );
        $que_vals_cnt          =   count($que_vals);
        
        $que_values_info       =   array();
        for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
            $que_vals_info     =   explode(":", $que_vals[$qvc]);
            $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
        }
        
        // Bind the parameters
        $params                 =   array(
            "OrgID"        => $OrgID,
            "HoldID"       => $HoldID,
            "QuestionID"   => $QuestionID,
            "Answer"       => $QI['Answer'],
        );
        
        //Have to work on web form insert call
        G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params  );
    }
    
}


// Loop through form fields and store
foreach ( $_POST as $question => $answer ) {
    
    if ($question == 'shifts_schedule_time' || $question == 'LabelSelect') {
        foreach ( $_POST [$question] as $cquestion => $canswer ) {
            
            if ($question == 'LabelSelect') {
                $answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
            }
            if ($question == 'shifts_schedule_time') {
                $qa ['from_time'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
                $qa ['to_time'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
                $qa ['days'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
                $answer = serialize ( $qa );
            }
            
            if (($answer != '') && ($question != 'MAX_FILE_SIZE') && ($question != 'submit') && (! preg_match ( '/^countdown/', $question ))) {
                // Bind Parameters
                $params = array ('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'QuestionID'=>$cquestion, 'Answer'=>$answer);
                // Insert applicant data temp
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params );
            }
        }
    }
    else {
        if(!in_array($question, $ques_to_skip)) {
            // Bind the parameters
            $params    =   array(
                "OrgID"        => $OrgID,
                "HoldID"       => $HoldID,
                "QuestionID"   => $question,
                "Answer"       => $answer,
            );
            if (($answer != '') && ($question != 'submit') && (! preg_match ( '/^countdown/', $question ))) {
                G::Obj('ApplicantDataTemp')->insApplicantDataTemp ( $params );
            }
        }
    }
} // end foreach
?>