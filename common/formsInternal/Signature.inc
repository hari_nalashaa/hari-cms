<?php
function Signature($QuestionID) {
	global $COLOR, $TemplateObj, $OrgID, $MultiOrgID, $APPDATAREQ, $OrganizationsObj, $OrganizationDetailsObj;
	
	if(is_object($TemplateObj)) {
		$COLOR = $TemplateObj->COLOR;
	}
	$rtn = "";
	$highlight = '#' . $COLOR ['ErrorHighlight'];
	
	// Get Organization Information
	$columns = "OrganizationName, date_format(now(),'%m/%d/%Y') AS OrgDateNow";
	$org_info = $OrganizationDetailsObj->getOrganizationInformation ( $OrgID, (string)$MultiOrgID, $columns );
	
	// Get Company Name
	$CompanyName = $org_info ['OrganizationName'];
	$Date = $org_info ['OrgDateNow'];
	
	if ($APPDATAREQ ['captcha'] == "REQUIRED") {
		$requiredck = 'background-color:' . $highlight . ';';
	} else {
		$requiredck = '';
	}
	
	$CompanyName = trim ( $CompanyName );
	
	if (FROM_SRC == "USERPORTAL") {
		$cap = USERPORTAL_HOME . "captcha/captcha.php?source_rand=".rand().uniqid(time()).rand(100, 10000);
	} else {
		$cap = IRECRUIT_HOME . "captcha/captcha.php?source_rand=".rand().uniqid(time()).rand(100, 10000);
	}
	
	$rtn .= <<<END
<div class="form-group row">
<div class="col-lg-3 col-md-3 col-sm-3">
<label class="question_name" style="$requiredck">Electronic Signature: <font color="red">*</font></label>
</div>
<div class="col-lg-9 col-md-9 col-sm-9">
<img src="$cap" alt="captcha image" border="0" title="security code" style="margin:0px 3px -4px 0px;"> <input type="text" name="captcha" size="6" maxlength="6">&nbsp;&nbsp;$Date<br><br>
Please enter the 6 symbols as they appear, by doing so I am agreeing that the data on this page identifies me and that the symbols represent my signature as legally valid and binding as if I had signed the document with ink on paper in accordance with the Uniform Electronic Transactions Act (UETA) and the Electronic Signatures in Global and National Commerce Act (E-SIGN) of 2000. I waive my right to a written signature for the purposes of processing this form for employment to $CompanyName.
<input type="hidden" name="$QuestionID" value="$Date">
</div>
</div>
END;
	
	return $rtn;
} // end function
?>