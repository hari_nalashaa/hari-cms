<?php
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

// get applicant data
$APPDATA    =   G::Obj('Applicants')->getAppData ( $OrgID, $ApplicationID );
?>
<script type="text/javascript">
function CloseAndRefresh(link)
{
     opener.window.location = link
     self.close();
}

function processAffirmativeActionInfo() {
	var form = $('#frmAffirmativeAction');
	input_data = form.serialize();
	
	$("#affirmative-action").html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	$('.ra-tab-content').hide();
	$('#affirmative-action').show();
	
	var request = $.ajax({
		method: "POST",
  		url: "applicants/getAffirmativeActionForm.php",
		type: "POST",
		data: input_data,
		success: function(data) {
			$("#affirmative-action").html(data);
    	}
	});
}
</script>
<?php
if (FROM_SRC == "IRECRUIT") {
	$close_link = IRECRUIT_HOME . 'applicantsSearch.php';
	$close_link .= '?ApplicationID=' . $ApplicationID;
	$close_link .= '&RequestID=' . $RequestID;
}

if (FROM_SRC == "USERPORTAL") {
	$close_link = USERPORTAL_HOME . 'index.php';
	$close_link .= '?navpg=profiles';
	$close_link .= '&navsubpg=status';
}

if (!preg_match ( '/getAffirmativeActionForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	echo '<div style="padding: 10px 10px 0px 10px;">';
	echo displayHeader ( $ApplicationID, $RequestID );
	echo '<hr><br>';
}

if ($process == "Y") {
	
	if ($processtype == "AddInformation") {
		
		foreach ( $_POST as $question => $answer ) {
			
			if (($answer != '') && ($question != 'ApplicationID') && ($question != 'RequestID') && ($question != 'process') && ($question != 'processtype')) {
				
				// Default update information
				$on_update      =   ' ON DUPLICATE KEY UPDATE Answer = :UAnswer';
				// insert information
				$insert_info    =   array (
                						'OrgID'           =>  $OrgID,
                						'ApplicationID'   =>  $ApplicationID,
                						'QuestionID'      =>  $question,
                						'Answer'          =>  $answer 
                    				);
				// update information
				$update_info    =   array (":UAnswer" => $answer);
				// Insert Update Applicant Data Temp
				$ApplicantsObj->insUpdApplicantData ( $insert_info, $on_update, $update_info );
			}
		} // end foreach
		
		$message = "Your information has been added to your record.\\n\\nThank you.";
	} else { // else AddInformation
	         
		// Default update information
		$on_update    =   ' ON DUPLICATE KEY UPDATE Answer = NOW()';
		
		// insert information
		$insert_info  =   array(
                				'OrgID'         =>  $OrgID,
                				'ApplicationID' =>  $ApplicationID,
                				'QuestionID'    =>  'aa_enterdate',
                				'Answer'        =>  'NOW()' 
                    		);
		// Insert Update Applicant Data Temp
		G::Obj('Applicants')->insUpdApplicantData ( $insert_info, $on_update );
		
		$message = "Thank you for your participation.";
	} // end processtype
	
	if (!preg_match ( '/getAffirmativeActionForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "');\n";
		echo "CloseAndRefresh('" . $close_link . "')";
		echo '</script>';
	} else {
		echo "<div style='text-align:center'><br><br>Your information has been added to your record.<br>Thank you.<br><br></div>";
	}
} else { // end Process
	require_once COMMON_DIR . 'application/AA.inc';

	// Get AA section Title
	$results       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $FormID, "AA");
	$AATitle       =   $results['SectionTitle'];
	
	if ($results['Active'] == "Y") {
		$AADisplayType = "ON";
	}
	
	$applicant_name    =   G::Obj('ApplicantDetails')->getApplicantName($OrgID, $ApplicationID);
	$AffirmativeAction =   AffirmativeActionSection($OrgID, $FormID, $ApplicationID, $APPDATA, $applicant_name);
	
	if ($AffirmativeAction) {
		
		if ($AATitle) {
			echo '<b class="title">' . $AATitle . '</b><br>';
		}
		
		if (($ApplicationID != "") && ($RequestID != "")) {
			
			echo '<div class="row">';
			echo '<div class="col-lg-12 col-md-12 col-sm-12">';
			
			echo '<form method="post" action="' . $_SERVER ['PHP_SELF'] . '" name="frmAffirmativeAction" id="frmAffirmativeAction">' . "\n";
			echo $AffirmativeAction;
			echo '<table border="0" cellspacing="0" cellpadding="5">';
			echo '<tr><td>';
			echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
			echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			echo '<input type="hidden" name="process" value="Y">';
			echo '<input type="hidden" name="processtype" value="AddInformation">';

			echo '<div>';

			if (preg_match ( '/getAffirmativeActionForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
				echo '<input type="button" class="btn btn-primary" value="Add Information" onclick="processAffirmativeActionInfo();">';
			}
			else {
				echo '<input type="submit" class="btn btn-primary" value="Add Information">';
			}
			
			echo '</form>';
			echo '</div>';

			//Set where condition
			$where   =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID IN ('aa_race','aa_genderselection','aa_veteranstatus')", "Active = 'Y'");
			//Set parameters
			$params  =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
			//Get FormQuestions Information
			$results =   G::Obj('FormQuestions')->getFormQuestionsInformation("*", $where, "", array($params));
			//Get Count
			$quescnt =   $results['count'];
			
			if ($quescnt == 0) {
			    echo '<div class="row">';
    			    echo '<div class="col-lg-12 col-md-12 col-sm-12">';
        				echo '<form method="post" action="' . $_SERVER ['PHP_SELF'] . '">';
        				echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
        				echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
        				echo '<input type="hidden" name="process" value="Y">';
        				echo '<input type="hidden" name="processtype" value="Decline">';
        				echo '<input type="submit" value="Decline to Answer">';
        				echo '</form>';
				    echo '</div>';
				echo '</div>';
			}
			
			echo '</td></tr>';
			echo '</table>';
		} // end ApplicationID && RequestID

		echo '</div>';
		echo '</div>';
	} // end if
} // end ApplicationID & RequestID
?>