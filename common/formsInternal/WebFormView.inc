<style>
#question_space {
     padding: 0px 0px 3px 0px;
}
@media(max-width:768px) {
	 #question_space {
         padding: 0px 0px 3px 0px;
         border-bottom: 1px solid #f5f5f5 !important;
	}
}
</style>
<?php 
if($ServerInformationObj->getRequestSource() != 'ajax' && FROM_SRC == 'IRECRUIT') {
    ?>
	<style>
    .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    </style>
	<?php
}

if (($OrgID) && ($ApplicationID) && ($RequestID)) {

    //Set default web form audit log information
    $audit_app_log  =   array(
                            'OrgID'            =>  $OrgID,
                            'UserID'           =>  $USERID,
                            'Action'           =>  'View',
                            'FormType'         =>  'WebForm',
                            'FormID'           =>  $WebFormID,
                            'PageInfo'         =>  $_SERVER['REQUEST_URI'],
                            'CreatedDateTime'  =>  "NOW()"
                        );
    
	// Set the condition
	$where = array ("OrgID = :OrgID", "WebFormID = :WebFormID");
	// Bind the parameters
	$params = array (":OrgID" => $OrgID, ":WebFormID" => $WebFormID);
	// Get WebFormsInformation
	$web_forms_info = $FormsInternalObj->getWebFormsInfo ( "FormName", $where, "", array ($params) );
	
	list ( $FormName) = array_values ( $web_forms_info ['results'] [0] );
	
	if(isset($displayFormHeader)) {
	    echo $displayFormHeader;
	     
	    echo '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12"><hr><br></div></div>';
	}
	
	echo "<div class='row'>";
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	echo '<strong>Form: ' . $FormName . '</strong><br>';
	echo '</div>';
	echo '</div>';
	
	if(FROM_SRC == 'IRECRUIT' && end(explode("/", $_SERVER['SCRIPT_FILENAME'])) == "viewInternalForm.php") {
		$print_view = IRECRUIT_HOME . "formsInternal/printWebForm.php?ApplicationID=".$ApplicationID."&RequestID=".$RequestID."&WebFormID=".$WebFormID;
		echo "<a href='".$print_view."' target='_blank'>";
		echo '<img src="'.IRECRUIT_HOME.'images/icons/printer.png" title="Printable" border="0"> Print';
		echo "</a>";
	}


	if($FillPDFObj->isCustomPDFForm($OrgID,$WebFormID)) {
	
	    echo "<div class='row'>";
	    echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	    $custom_view = IRECRUIT_HOME . "formsInternal/display_customform.php?";
	    $custom_view .= "OrgID=".$OrgID."&ApplicationID=".$ApplicationID."&RequestID=".$RequestID;
	    $custom_view .= "&WebFormID=".$WebFormID;
        
	    echo '<a href="' . $custom_view . '">';
	    echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_acrobat.png" title="View PDF Form" style="margin:0px 0px -1px 0px;" border="0">';
	    echo '&nbsp;<span style="font-size:10pt;">';
	    echo 'download pdf form</a></span>';
	    echo '<br>';

	    echo '</div>';
	    echo '</div>';
	}
	
    $WebFormQueAns  =    $WebFormQueAnsObj->getWebFormQueAnswersInfo ( $OrgID, $ApplicationID, $RequestID, $WebFormID );
	
    foreach ($WebFormQueAns as $QuestionID=>$QuestionAnswerInfo)
    {
        echo "<div class='row' style='margin-bottom:3px;'>";
         
        if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
             
            echo "<div class='col-lg-3 col-md-3 col-sm-2'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
        
            $canswer = $QuestionAnswerInfo['Answer'];
             
            $rtn  = '';
            if(is_array($canswer)) {
                $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
                 
                foreach ($canswer as $cakey=>$caval) {
                    $rtn .= '<tr>';
                    $rtn .= '<td style="padding-right:5px !important">'.$cakey.'&nbsp;&nbsp;&nbsp;</td>';
                    foreach ($caval as $cavk=>$cavv) {
                        $rtn .= '<td style="padding-right:5px !important">'.$cavv.'&nbsp;&nbsp;&nbsp;</td>';
                    }
                    $rtn .= '</tr>';
                }
                 
                $rtn .= '</table>';
            }
        
            echo "<div class='col-lg-9 col-md-9 col-sm-10'><strong>".$rtn."</strong></div>";
             
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
             
            echo "<div class='col-lg-3 col-md-3 col-sm-2'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
             
            $canswer = $QuestionAnswerInfo['Answer'];
             
            $rtn  = '';
            if(is_array($canswer)) {
                 
                $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
        
                $days_count = count($canswer['days']);
        
                for($ci = 0; $ci < $days_count; $ci++)  {
                    $rtn .= '<tr>';
                    $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
                    $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
                    $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
                    $rtn .= '<td width="5%">to </td>';
                    $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
                    $rtn .= '</tr>';
                }
        
                $rtn .= '</table>';
            }
             
            echo "<div class='col-lg-9 col-md-9 col-sm-10'><strong>".$rtn."</strong></div>";
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
            echo "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
        }
        else {
            echo "<div class='col-lg-3 col-md-3 col-sm-2'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
            echo "<div class='col-lg-9 col-md-9 col-sm-10'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
        }
        
        echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
        echo "</div>";
    }

} // end if ApplicationID, OrgID, RequestID
?>
