<?php
$rtn = "";

//Set the condition
$where = array("OrgID = :OrgID", "SpecificationFormID = :SpecificationFormID");
//Set the parameters
$params = array(":OrgID"=>$OrgID, ":SpecificationFormID"=>$specformid);
//Get Specification Forms Information
$results = $FormsInternalObj->getSpecificationForms("*", $where, "", array($params));

$rtn .= "<h3>".$results['results'][0]['DisplayTitle']."</h3><br>";

if(is_array($results['results'])) {
	foreach($results['results'] as $SF) {
		$rtn .= $SF ['HTML'];
	} // end foreach	
}
echo $rtn;
?>