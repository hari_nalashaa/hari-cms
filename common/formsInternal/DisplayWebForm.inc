<?php
function fillWebFormData($USERID, $HoldID, $ApplicationID, $RequestID, $WebFormID) {
	global $OrgID, $MysqlHelperObj, $ApplicationsObj, $FormsInternalObj, $FormDataObj;
	
    $APPDATA        =   array ();
    $APPDATAREQ     =   array ();
	
	// Start PreFill awareness
	if ((FROM_SRC == "USERPORTAL") && ($USERID) && ($WebFormID)) {
		
		$ap       =   "";
		
		$where    =   array("JA.OrgID = :OrgID", "JA.OrgID = AD.OrgID", "JA.ApplicationID = AD.ApplicationID", "AD.QuestionID = 'PortalUserID'", "AD.Answer = :Answer", "JA.ApplicationID != :ApplicationID");
		$params   =   array(":OrgID"=>$OrgID, ":Answer"=>$USERID, ":ApplicationID"=>$ApplicationID);
		
		$results  =   $ApplicationsObj->getJobApplicationsAndApplicantData("JA.ApplicationID", $where, "", array($params));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $IFA) {
				$ap .= "'" . $IFA ['ApplicationID'] . "',";
			} // end foreach
		}
		
		$ap = substr ( $ap, 0, - 1 );
		
		if ($ap) {
			$ifa = "";
			
			//Set condition
			$where = array("OrgID = :OrgID", "Status >= 3", "ApplicationID IN ($ap)", "UniqueID = :UniqueID");
			//Set parameters
			$params = array(":OrgID"=>$OrgID, ":UniqueID"=>$WebFormID);
			//Set columns
			$columns = array("ApplicationID", "UniqueID");
			//Get Internal Forms Assigned Information
			$results = $FormsInternalObj->getInternalFormsAssignedInfo($columns, $where, "", "ApplicationID DESC LIMIT 1", array($params));
			
			if(is_array($results['results'][0])) list ( $appid, $uniqueid ) = array_values($results['results'][0]);
			
			if (($appid) && ($uniqueid)) {
				
				//Set condition
				$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$appid, ":WebFormID"=>$uniqueid);
				//Get web form data
				$results = $FormDataObj->getWebFormData("QuestionID, Answer", $where, "", "", array($params));
				
				if(is_array($results['results'])) {
					foreach ($results['results'] as $WFD) {
						if (($WebFormID != "516ed675a44d0") && ($WebFormID != "51703f9a9f7f7") && ($WebFormID != "52163c54cfcd0")) { // no I-9 or w-4
							$APPDATA [$WFD ['QuestionID']] = $WFD ['Answer'];
						} // end if
					} // end foreach
				}
				
			} // end appid uniqueid
		} // end ap
	} // end FROM
	  
	// End PreFill portion
	
	if ($HoldID) {
		$results  =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $row) {
				$APPDATA [$row ['QuestionID']] = $row ['Answer'];
				$APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
			}	
		}
		
	} else { // end HoldID
		
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "WebFormID = :WebFormID");
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":WebFormID"=>$WebFormID);
		
		//Get Web Form Data
		$results = $FormDataObj->getWebFormData("*", $where, "", "", array($params));
		$editcnt = $results['count'];
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $WFD) {
				$APPDATA [$WFD ['QuestionID']] = $WFD ['Answer'];
			}	
		}
		
		//Get unique id based on date time
		$HLD = $MysqlHelperObj->getDateTime('%Y%m%d%H%m%s');
		$HoldID = uniqid ( $HLD );
		
	} // end HoldID
	
	return array (
			$APPDATA,
			$APPDATAREQ,
			$HoldID 
	);
} // end function


function displayWebForm($WebFormID, $APPDATA, $APPDATAREQ) {
	global $OrgID, $typeform, $HoldID, $TemplateObj, $COLOR, $WebFormID, $ApplicationID, $FormsInternalObj, $FormQuestionsObj;
	
	if(is_object($TemplateObj)) {
		$COLOR = $TemplateObj->COLOR;
	}
	
	$highlight = '#' . $COLOR ['ErrorHighlight'];
	$formtable = "WebFormQuestions";
	$colwidth = "300";
	
	$infm = "";
	
	//Set the where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
	//Set the parameters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID);
	
	$results = $FormsInternalObj->getWebFormsInfo("FormName", $where, "", array($params));
	list ( $FormName ) = array_values($results['results'][0]);
	
	$infm .= '<div class="form-group row">';
	$infm .= '<div class="col-lg-12 col-md-12 col-sm-12">';
	$infm .= '<strong>';
	$infm .= 'Form: ';
	$infm .= $FormName;
	$infm .= '</strong>';
	$infm .= '</div>';
	$infm .= '</div>';
	
	$columns = array("FormID", "QuestionID", "QuestionTypeID");
	//Set where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "Active = 'Y'");
	//Set the parameters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID);
	
	//Get Questions Information
	$resultsInt = $FormQuestionsObj->getQuestionsInformation($formtable, $columns, $where, "QuestionOrder", array($params));
	
	if(is_array($resultsInt['results'])) {
		foreach($resultsInt['results'] as $WFQ) {
		
			$FormID = $WFQ ['FormID'];
			$QuestionID = $WFQ ['QuestionID'];
		
			$infm .= include COMMON_DIR . 'application/DisplayQuestions.inc';
		} // end foreach
	}
	
	
	return $infm;
} // end function
?>