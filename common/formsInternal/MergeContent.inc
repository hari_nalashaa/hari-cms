<?php
function MergeContent($AgreementFormID, $QuestionID, $ApplicationID, $RequestID) {
	global $OrgID, $MultiOrgID, $FormQuestionsObj, $FormsInternalObj, $ApplicantsObj, $FormDataObj, $AgreementFormQueAnsObj;
	
	$rtn       =   "";
	// Set condition
	$where     =   array ("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
	// Set Parameters
	$params    =   array (":OrgID" => $OrgID, ":AgreementFormID" => $AgreementFormID, ":QuestionID" => $QuestionID);
	// Get QuestionsInformation
	$results   =   $FormQuestionsObj->getQuestionsInformation( "AgreementFormQuestions", "value", $where, "", array ($params) );
	
	if(is_array($results ['results'] [0])) list ( $MergeContent ) = array_values ( $results ['results'] [0] );
	
	//Set where condition
	$where     =   array ("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	//Set parameters
	$params    =   array (":OrgID" => $OrgID, ":AgreementFormID" => $AgreementFormID);
	// Get Agreement Forms Information
	$results   =   $FormsInternalObj->getAgreementFormsInfo ( "LinkedTo", $where, "", array ($params) );
	
	if(is_array($results ['results'] [0])) list ( $LinkedTo ) = array_values ( $results ['results'] [0] );
	
	$MERGE     =   array ();
	
	// Set condition
	$where     =   array ("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionTypeID NOT IN(99,98,30,60)");
	// Set parameters
	$params    =   array (":OrgID" => $OrgID, ":AgreementFormID" => $LinkedTo);
	// Get Question Information
	$results   =   $FormQuestionsObj->getQuestionsInformation ( "AgreementFormQuestions", "*", $where, "", array ( $params ) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $AFQ ) {		
			
			// Set parameters
			$params  =   array (
                                ":OrgID"           => $OrgID,
                                ":ApplicationID"   => $ApplicationID,
                                ":RequestID"       => $RequestID,
                                ":AgreementFormID" => $LinkedTo 
                            );
			// Set condition
			$where   =   array (
                                "OrgID             = :OrgID",
                                "ApplicationID     = :ApplicationID",
                                "RequestID         = :RequestID",
                                "AgreementFormID   = :AgreementFormID" 
                            );
			// Set Agreement Form Data
			$resultsAFD          =   $FormDataObj->getAgreementFormData ( "*", $where, "", "", array ($params) );
			// Set Primary Data Count
			$primarydatacnt      =   $resultsAFD ['count'];

			$AgreementFormQueAns =   $AgreementFormQueAnsObj->getAgreementFormQueAnswersInfo($OrgID, $ApplicationID, $RequestID, $LinkedTo);
				
			if ($primarydatacnt > 0) { // Secondary Form Data View

                //Set Agreement FormData
				foreach($AgreementFormQueAns as $AgreementFormQueID=>$AgreementFormQueInfo) {
				    $AGREEDATA[$AgreementFormQueID] =   $AgreementFormQueInfo['Answer'];
				}
				
				// Set Agreement Form Data
				$params = array (
                        ":OrgID"            =>  $OrgID,
                        ":ApplicationID"    =>  $ApplicationID,
                        ":RequestID"        =>  $RequestID,
                        ":AgreementFormID"  =>  $LinkedTo,
                        ":QuestionID"       =>  $AFQ ['QuestionID'] 
				);
				$where = array (
                        "OrgID              =   :OrgID",
                        "ApplicationID      =   :ApplicationID",
                        "RequestID          =   :RequestID",
                        "AgreementFormID    =   :AgreementFormID",
                        "QuestionID         =   :QuestionID" 
				);
				
				$columns    =   "Answer, value, QuestionTypeID, QuestionID";
				//Get Agreement Form Data
				$results_afd = $FormDataObj->getAgreementFormData ( $columns, $where, "", "", array ( $params ) );
				
				if(is_array($results_afd ['results'] [0])) {
				    list ( $answer, $value, $QuestionTypeID, $QuestionID ) = array_values ( $results_afd ['results'] [0] );
				}
				
				if (($QuestionTypeID == "13") || ($QuestionTypeID == "14") || ($QuestionTypeID == "15")) {
					$answer = $AGREEDATA[$QuestionID];
				}
				else if (($QuestionTypeID == "18") || ($QuestionTypeID == "1818")) {
				    $answer = $AGREEDATA[$QuestionID];
				}
				else if($QuestionTypeID == "9") {
				    $answer = $AGREEDATA[$QuestionID];
				} 
				else if ($value) {
					$answer = getFillValue ( $answer, $value );
				}
			} else { // Admin Preview
				$answer = '<i style="color:#994499;">{' . $AFQ ['Question'] . '}</i>';
			}
			
			$MERGE ['{' . $AFQ ['QuestionID'] . '}'] = $answer;
		} // end foreach
	}
	
	$APPDATA   =   $ApplicantsObj->getAppData ( $OrgID, $ApplicationID );

	$PERSONAL  =   array (
            			'first'      => 'First Name',
            			'last'       => 'Last Name',
            			'address'    => 'Address Line One',
            			'address2'   => 'Address Line Two',
            			'city'       => 'City',
            			'state'      => 'State',
            			'zip'        => 'Zip Code',
            			'email'      => 'Email Address' 
                	   );
	
	foreach ( $PERSONAL as $QID => $Q ) {
		if ($primarydatacnt > 0) { // Secondary Form Data View
			$MERGE ['{' . $QID . '}'] = $APPDATA [$QID];
		} else { // Admin Preview
			$MERGE ['{' . $QID . '}'] = '<i style="color:#994499;">{' . $QID . '}</i>';
		}
	}
	
	foreach ( $MERGE as $MID => $ANS ) {
		$MergeContent = str_replace ( $MID, $ANS, $MergeContent );
	}
	
	$rtn .= $MergeContent;
	
	return $rtn;
} // end function

function getFillValue($ans, $displaychoices) {
	$rtn = '';
	
	if ($ans != '') {
		$Values = explode ( '::', $displaychoices );
		foreach ( $Values as $v => $q ) {
			list ( $vv, $qq ) = explode ( ":", $q, 2 );
			if ($vv == $ans) {
				$rtn .= $qq;
			}
		}
	}
	
	return $rtn;
} // end of function

function getFillMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
	$rtn = '';
	$val = '';
	$lex = '';
	
	$le = '<br>';
	$li = '&#8226;&nbsp;';
	$lecnt = - 4;
	
	$cnt = $APPDATA [$id . 'cnt'];
	
	for($i = 1; $i <= $cnt; $i ++) {
		
		$val = getFillValue ( $APPDATA [$id . '-' . $i], $displaychoices );
		if ($val) {
			$rtn .= $li . $val;
			$lex = 'on';
			$val = '';
		}
		
		if ($questiontype == 9) {
			
			$val = $APPDATA [$id . '-' . $i . '-yr'];
			if ($val) {
				$rtn .= '&nbsp;' . $val . ' yrs. ';
				$lex = 'on';
				$val = '';
			}
			
			$val = $APPDATA [$id . '-' . $i . '-comments'];
			if ($val) {
				$rtn .= '&nbsp;(' . $val . ') ';
				$lex = 'on';
				$val = '';
			}
		} // end questiontype
		
		if ($lex == 'on') {
			$rtn .= $le;
			$lex = '';
		}
	}
	
	$rtn = substr ( $rtn, 0, $lecnt );
	
	$return    =   '<div class="row">';
	$return   .=   '<div class="col-lg-12 col-md-12 col-sm-12">';
	$return   .=   $rtn;
	$return   .=   '</div>';
	$return   .=   '</div>';
	
	return $return;
} // end function
?>