<?php
require_once COMMON_DIR . 'formsInternal/Signature.inc';

$complete_status                =   "";
$GetFormPostAnswerObj->POST     =   $_POST;    //Set Post Data

if (isset($_REQUEST['process']) && $_REQUEST['process'] == "Y") {
    
	// Clear the db of older entries for this holdid
	G::Obj('ApplicantDataTemp')->delApplicantDataTemp ( $OrgID, $HoldID );
	
	include COMMON_DIR . 'formsInternal/ProcessWebFormDataTemp.inc';
	
	include COMMON_DIR . 'formsInternal/WebFormValidateRequired.inc';
	
	if($ERROR) {

		$complete_status = "failure";
		$MESSAGE = "The following entries are missing information.\\n";
		$MESSAGE .= $ERROR;
		$MESSAGE = str_replace(array("\n", "\\n"), "<br>", $MESSAGE);
	}
	else {
		// if there are no errors there is no need to hold temp files
		G::Obj('ApplicantDataTemp')->delApplicantDataTemp ( $OrgID, $HoldID );
		$HoldID = "";
		
		$define = "";
		
		if ($_REQUEST['edit'] == "Y") {

			// Set where condition
			$web_form_data_del_where = array (
					"OrgID 				= :OrgID",
					"ApplicationID 		= :ApplicationID",
					"RequestID 			= :RequestID",
					"WebFormID 			= :WebFormID"
			);
			// Set web form data where condition
			$web_form_data_where = array_merge($web_form_data_del_where, array("QuestionTypeID <> 99", "QuestionTypeID != '10'") );
		
			// Bind the parameters for delete query
			$web_form_data_params = array (
					":OrgID" 			=> $OrgID,
					":ApplicationID"	=> $ApplicationID,
					":RequestID" 		=> $RequestID,
					":WebFormID" 		=> $WebFormID
			);
			
			//web form data results	
			$web_form_data_results 	= $FormDataObj->getWebFormData("*", $web_form_data_where, "", "", array($web_form_data_params));
			
			$web_form_data_que_ids = array();
			if(is_array($web_form_data_results['results'])) {
				foreach($web_form_data_results['results'] as $web_form_que_info) {
					$web_form_data_info_history[$web_form_que_info['QuestionID']]['Question'] 	= $web_form_que_info['Question'];
					$web_form_data_info_history[$web_form_que_info['QuestionID']]['Answer'] 	= $web_form_que_info['Answer'];
					$web_form_data_que_ids[] = $web_form_que_info['QuestionID'];
				}
			}
			
			//Delete webform data before insert
			$FormsInternalObj->delFormData ( 'WebFormData', $web_form_data_del_where, array ( $web_form_data_params ) );
		}
		
		
		/**
		 * This is a temporary code until the final upgrade.
		 * Upto that time, we have to keep on adding different conditions to it.
		 * After final upgrade we will have to remove the loop through $_POST
		 */
        $form_que_list         =   $WebFormQuestionsObj->getWebFormQuestionsList($OrgID, $WebFormID);
		
		$ques_to_skip          =   array();
		foreach($form_que_list as $QuestionID=>$QuestionInfo) {
		
            $QI                 =   $QuestionInfo;
            G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
            $values             =   G::Obj('GetFormPostAnswer')->getDisplayValue($QI['value']);

            $QI['OrgID']        =   $OrgID;
		    
		    if($QI['QuestionTypeID'] == 13) {
		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		         
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;

		        //Data for web form history
		        if(($web_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
    		        && isset($web_form_data_info_history[$QI['QuestionID']]['Question'])
    		        && $web_form_data_info_history[$QI['QuestionID']]['Question'] != ""
		            && in_array($QI['QuestionID'], $web_form_data_que_ids)) {
		            
		            $phone_ans_bef    =   $web_form_data_info_history[$QI['QuestionID']]['Answer'];
		            $app_ans_bef      =   ($phone_ans_bef != "") ? json_decode($phone_ans_bef, true) : "";
		            $app_ans_bef_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], '' );
		            
		            $phone_ans_aft    =   $QI['Answer'];
		            $app_ans_aft      =   ($phone_ans_aft != "") ? json_decode($phone_ans_aft, true) : "";
		            $app_ans_aft_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], '' );
		            
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }
		        
		        //Have to work on web form insert call
		        $WebFormDataObj->insUpdWebFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 14) {

		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		        $ques_to_skip[]        =   $QI['QuestionID']."ext";
		         
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;

		        //Data for web form history
		        if(($web_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
    		        && isset($web_form_data_info_history[$QI['QuestionID']]['Question'])
    		        && $web_form_data_info_history[$QI['QuestionID']]['Question'] != ""
		            && in_array($QI['QuestionID'], $web_form_data_que_ids)) {
		    
		            $phone_ans_bef    =   $web_form_data_info_history[$QI['QuestionID']]['Answer'];
		            $app_ans_bef      =   ($phone_ans_bef != "") ? json_decode($phone_ans_bef, true) : "";
		            $app_ans_bef_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], $app_ans_aft[3] );
		        
		            $phone_ans_aft    =   $QI['Answer'];
		            $app_ans_aft      =   ($phone_ans_aft != "") ? json_decode($phone_ans_aft, true) : "";
		            $app_ans_aft_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], $app_ans_aft[3] );
		        
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }
		    
		        //Have to work on web form insert call
		        $WebFormDataObj->insUpdWebFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 15) {
		    
		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		        
		        //Data for web form history
		        if(($web_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
    		        && isset($web_form_data_info_history[$QI['QuestionID']]['Question'])
    		        && $web_form_data_info_history[$QI['QuestionID']]['Question'] != ""
		            && in_array($QI['QuestionID'], $web_form_data_que_ids)) {
		        
		            $ssn_ans_bef      =   $web_form_data_info_history[$QI['QuestionID']]['Answer'];
		            $app_ans_bef      =   ($ssn_ans_bef != "") ? json_decode($ssn_ans_bef, true) : "";
		            $app_ans_bef_ans  =   $app_ans_bef[0].'-'.$app_ans_bef[1].'-'.$app_ans_bef[2];
		        
		            $ssn_ans_aft      =   $QI['Answer'];
		            $app_ans_aft      =   ($ssn_ans_aft != "") ? json_decode($ssn_ans_aft, true) : "";
		            $app_ans_aft_ans  =   $app_ans_aft[0].'-'.$app_ans_aft[1].'-'.$app_ans_aft[2];
		        
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }		        
		        
		        //Have to work on web form insert call
		        $WebFormDataObj->insUpdWebFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 9) {
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //After the complete upgrade have to remove this.
		        $qis  =   0;
		        foreach ( $values as $v => $q ) {
		            $qis++;
		             
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis;
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-yr";
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-comments";
		        }

		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		        
		        //Have to work on web form insert call
		        $WebFormDataObj->insUpdWebFormData($QI);
		    
		        $wskills_info_old = json_decode($web_form_data_info_history[$QI['QuestionID']]['Answer'], true);
		        $wskills_info_new = json_decode($QI['Answer'], true);
		    
		        $wrtn_old = '';
		        foreach($wskills_info_old as $wskills_key_old=>$wskills_value_old) {
		            $wvalues_old   =  array_values($wskills_value_old);
		            $wrtn_old     .=  '&#8226;&nbsp;<strong>' . $wvalues_old[0] . ", " . $wvalues_old[1] . " yrs. (" . $wvalues_old[2].")</strong><br>";
		        }
		    
		        $wrtn_new = '';
		        foreach($wskills_info_new as $wskills_key_new=>$wskills_value_new) {
		            $wvalues_new   =  array_values($wskills_value_new);
		            $wrtn_new     .=  '&#8226;&nbsp;<strong>' . $wvalues_new[0] . ", " . $wvalues_new[1] . " yrs. (" . $wvalues_new[2].")</strong><br>";
		        }
		    
		        $define .= "Question: " . $QI['Question'] . "<br>";
		        $define .= "&nbsp;&nbsp;Old: " . $wrtn_old . "<br>";
		        $define .= "&nbsp;&nbsp;New: " . $wrtn_new . "<br>\n";
		    
		    }
		    else if($QI['QuestionTypeID'] == 18) {
		    
		        //After the complete upgrade have to remove this.
		        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
		    
		        for($c18 = 1; $c18 <= $cnt; $c18++) {
		            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
		        }
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		         
		        $que_vals              =   explode ( '::', $QI['value'] );
		        $que_vals_cnt          =   count($que_vals);
		         
		        $que_values_info       =   array();
		        for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
		            $que_vals_info     =   explode(":", $que_vals[$qvc]);
		            $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
		        }
		    
		        //Data for web form history
		        if(($web_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
		        && isset($web_form_data_info_history[$QI['QuestionID']]['Question'])
		        && $web_form_data_info_history[$QI['QuestionID']]['Question'] != ""
		            && in_array($QI['QuestionID'], $web_form_data_que_ids)) {
		    
		            $app_ans_bef        =   $web_form_data_info_history[$QI['QuestionID']]['Answer'];
		            $app_ans_aft        =   $QI['Answer'];
		    
		            $ans_bef_ans_jd     =   json_decode($app_ans_bef, true);
		            $ans_aft_ans_jd     =   json_decode($app_ans_aft, true);
		    
		            foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
		                $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
		            }
		             
		            foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
		                $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
		            }
		    
		            $app_ans_bef        =   @implode(", ", array_values($ans_bef_ans_jd));
		            $app_ans_aft        =   @implode(", ", array_values($ans_aft_ans_jd));
		    
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft . "<br>\n";
		        }
		    
		        //Have to work on web form insert call
		        $WebFormDataObj->insUpdWebFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 1818) {
		    
		        //After the complete upgrade have to remove this.
		        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
		    
		        for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
		            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c1818;
		        }
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        $que_vals              =   explode ( '::', $QI['value'] );
		        $que_vals_cnt          =   count($que_vals);
		         
		        $que_values_info       =   array();
		        for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
		            $que_vals_info     =   explode(":", $que_vals[$qvc]);
		            $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
		        }
		    
		        //Data for web form history
		        if(($web_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
		        && isset($web_form_data_info_history[$QI['QuestionID']]['Question'])
		        && $web_form_data_info_history[$QI['QuestionID']]['Question'] != ""
		            && in_array($QI['QuestionID'], $web_form_data_que_ids)) {
		    
		            $app_ans_bef        =   $web_form_data_info_history[$QI['QuestionID']]['Answer'];
		            $app_ans_aft        =   $QI['Answer'];
		    
		            $ans_bef_ans_jd     =   json_decode($app_ans_bef, true);
		            $ans_aft_ans_jd     =   json_decode($app_ans_aft, true);
		    
		            foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
		                $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
		            }
		             
		            foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
		                $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
		            }
		    
		            $app_ans_bef        =   @implode(", ", array_values($ans_bef_ans_jd));
		            $app_ans_aft        =   @implode(", ", array_values($ans_aft_ans_jd));
		    
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft . "<br>\n";
		        }
		    
		        //Have to work on web form insert call
		        $WebFormDataObj->insUpdWebFormData($QI);
		    }
		
		}

		
        foreach ( $_POST as $web_question_id => $answer ) {
		    
			$split_question_id = $web_question_id;
			if(strpos($web_question_id, "-")) {
				$que_id_pieces = explode("-", $web_question_id);
				$split_question_id = $que_id_pieces[0];
			}
			
			// Set the columns
			$columns =   "Question, QuestionID, QuestionOrder, QuestionTypeID, value";
			// Set where condition
			$where   =   array ("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			// set parameters
			$params  =   array (":OrgID" => $OrgID, ":WebFormID" => $WebFormID, ":QuestionID" => $split_question_id);
		
			$results =   $FormQuestionsObj->getQuestionsInformation ( 'WebFormQuestions', $columns, $where, "", array ($params) );
			
			if (is_array ( $results ['results'] [0] )) {
				list ( $question, $questionid, $questionorder, $questiontypeid, $value ) = array_values ( $results ['results'] [0] );
			}
			
		    $temp_question_id = substr($split_question_id, 0, -1);
		    
		    // Set the columns
		    $columns =   "Question, QuestionID, QuestionOrder, QuestionTypeID, value";
		    // Set where condition
		    $where   =   array ("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
		    // set parameters
		    $params  =   array (":OrgID" => $OrgID, ":WebFormID" => $WebFormID, ":QuestionID" => $temp_question_id);
		    
		    $results =   $FormQuestionsObj->getQuestionsInformation ( 'WebFormQuestions', $columns, $where, "", array ($params) );
		    	
		    if (is_array ( $results ['results'] [0] )) {
		        list ( $question, $questionid, $questionorder, $questiontypeid, $value ) = array_values ( $results ['results'] [0] );
		    }
		     
		    $questionid = $split_question_id;
		
			// Set Condition
			$where 		= array ("OrgID = :OrgID", "WebFormID = :WebFormID");
			$params 	= array (":OrgID" => $OrgID, ":WebFormID" => $WebFormID);
			// Get WebForms Information
			$results 	= $FormsInternalObj->getWebFormsInfo ( "FormName", $where, "", array ( $params ) );
			$IFformname = $results ['results'] [0] ['FormName'];
		
			// else edit
			if ($web_question_id == 'shifts_schedule_time' || $web_question_id == 'LabelSelect') {
					
				foreach ( $_POST [$web_question_id] as $cquestion => $canswer ) {
		
				    if(!in_array($web_question_id, $ques_to_skip)) {
				        
    					/**
    					 * @tutorial set columns, condition, params to get WebFormQuestions Information
    					 */
    				    $columns 	= 	"Question, QuestionID, QuestionOrder, QuestionTypeID, value";
    					$where 		= 	array (
    										"OrgID 			= :OrgID",
    										"WebFormID 		= :WebFormID",
    										"QuestionID 	= :QuestionID"
    									);
    					$params 	= 	array (
    										":OrgID" 		=> $OrgID,
    										":WebFormID" 	=> $WebFormID,
    										":QuestionID" 	=> $cquestion
    									);
    		
    					// Get web form questions information
    					$results = $FormQuestionsObj->getQuestionsInformation ( 'WebFormQuestions', $columns, $where, "", array ($params) );
    		
    					if (is_array ( $results ['results'] [0] )) {
    						// Assign values
    						list ( $question, $questionid, $questionorder, $questiontypeid, $value ) = array_values ( $results ['results'] [0] );
    					}
    					
    					if ($web_question_id == 'LabelSelect') {
    						$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
    					}
    					if ($web_question_id == 'shifts_schedule_time') {
    						$qa ['from_time'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
    						$qa ['to_time'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
    						$qa ['days'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
    						$answer = serialize ( $qa );
    					}
    		
    					//Data for web form history
    					if(($web_form_data_info_history[$questionid]['Answer'] != $answer)
    						&& isset($web_form_data_info_history[$questionid]['Question'])
    						&& $web_form_data_info_history[$questionid]['Question'] != ""
    						&& in_array($questionid, $web_form_data_que_ids)) {
    						$define .= "Question: " . $question . "<br>";
    						$define .= "&nbsp;&nbsp;Old: " . $web_form_data_info_history[$questionid]['Answer'] . "<br>";
    						$define .= "&nbsp;&nbsp;New: " . $answer . "<br>\n";
    					}
    					
    					// Information to insert
    					$WIS       =   array (
    										"OrgID" 			=> $OrgID,
    										"ApplicationID" 	=> $ApplicationID,
    										"RequestID" 		=> $RequestID,
    										"WebFormID" 		=> $WebFormID,
    										"QuestionID" 		=> $questionid,
    										"QuestionOrder" 	=> $questionorder,
    										"QuestionTypeID" 	=> $questiontypeid,
    										"Answer" 			=> $answer,
    										"Question" 			=> $question,
    										"value" 			=> $value
    									);
    					if($questionid != 'instructions') {
    					    //Have to work on web form insert call
    					    $WebFormDataObj->insUpdWebFormData($WIS);
    					}
					
				    }
				}
			} else {
			    

                $split_question_id = $web_question_id;
			    if(strpos($web_question_id, "-")) {
			        $que_id_pieces = explode("-", $web_question_id);
			        $split_question_id = $que_id_pieces[0];
			    }
			    	
			    // Set the columns
			    $columns = "Question, QuestionID, QuestionOrder, QuestionTypeID, value";
			    // Set where condition
			    $where = array ("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			    // set parameters
			    $params = array (":OrgID" => $OrgID, ":WebFormID" => $WebFormID, ":QuestionID" => $split_question_id);
			    
			    $results = $FormQuestionsObj->getQuestionsInformation ( 'WebFormQuestions', $columns, $where, "", array ($params) );
			    	
			    $question_exists = "false";
			    if (is_array ( $results ['results'] [0] )) {
			        list ( $question, $questionid, $questionorder, $questiontypeid, $value ) = array_values ( $results ['results'] [0] );
			        $question_exists = "true";
			    }
			    	
			    if($question_exists == "false") {
			         
			        $temp_question_id = substr($split_question_id, 0, -1);
			         
			        // Set the columns
			        $columns =   "Question, QuestionID, QuestionOrder, QuestionTypeID, value";
			        // Set where condition
			        $where   =   array ("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			        // set parameters
			        $params  =   array (":OrgID" => $OrgID, ":WebFormID" => $WebFormID, ":QuestionID" => $temp_question_id);
			         
			        $results =   $FormQuestionsObj->getQuestionsInformation ( 'WebFormQuestions', $columns, $where, "", array ($params) );
			    
			        if (is_array ( $results ['results'] [0] )) {
			            list ( $question, $questionid, $questionorder, $questiontypeid, $value ) = array_values ( $results ['results'] [0] );
			        }
			    
			        $questionid = $split_question_id;
			    }

				if(!in_array($web_question_id, $ques_to_skip)) {
				    
				    //Data for web form history
				    if($web_form_data_info_history[$web_question_id]['Answer'] != $answer
    				    && isset($web_form_data_info_history[$web_question_id]['Question'])
    				    && $web_form_data_info_history[$web_question_id]['Question'] != ""
				        && in_array($web_question_id, $web_form_data_que_ids)
				        && $question_exists == "true") {
				        $define .= "Question: " . $question . "<br>";
				        $define .= "&nbsp;&nbsp;Old: " . $web_form_data_info_history[$web_question_id]['Answer'] . "<br>";
				        $define .= "&nbsp;&nbsp;New: " . $answer . "<br>\n";
				    }
				    
    				// Insert web form data
    				$WIC    =   array (
                						"OrgID" 			=> $OrgID,
                						"ApplicationID" 	=> $ApplicationID,
                						"RequestID" 		=> $RequestID,
                						"WebFormID" 		=> $WebFormID,
                						"QuestionID" 		=> $web_question_id,
                						"QuestionOrder" 	=> $questionorder,
                						"QuestionTypeID" 	=> $questiontypeid,
                						"Answer" 			=> $answer,
                						"Question" 			=> $question,
                						"value" 			=> $value
                    				);
    					
    				if ($questiontypeid != 99 && $questiontypeid != 98 && (substr ( $web_question_id, 0, 9 ) != "countdown") && ($web_question_id != 'FormID') && ($web_question_id != 'ApplicationID') && ($web_question_id != 'RequestID') && ($web_question_id != 'WebFormID') && ($web_question_id != 'WebFormID') && ($web_question_id != 'HoldID') && ($web_question_id != 'edit') && ($web_question_id != "process")) {
    				    if($web_question_id != 'instructions') {
    					    //Have to work on web form insert call
    					    $WebFormDataObj->insUpdWebFormData($WIC);
    					}
    				}
				
				}
			}
		
		} // end foreach
		

		if ($edit == "Y") {
			if (! $define) {
				$define = "No Changes";
			}
			$Comment = "The following form was updated <b>" . $IFformname . "</b><br>" . $define;
		} else {
			$Comment = "The following form was submited: <b>" . $IFformname . "</b><br>";
		}
		
		// Set the parameters
		$params = array (
				":OrgID" 			=> $OrgID,
				":ApplicationID" 	=> $ApplicationID,
				":RequestID" 		=> $RequestID,
				":UniqueID" 		=> $WebFormID
		);
		// Set the condition
		$where = array (
				"OrgID 				= :OrgID",
				"ApplicationID 		= :ApplicationID",
				"RequestID 			= :RequestID",
				"UniqueID 			= :UniqueID"
		);
		// Set the data to update
		$set_info = array (
				"LastUpdated 		= NOW()",
				"Status 			= 3"
		);
		// Update Internal Forms Assigned
		$FormsInternalObj->updInternalFormsAssigned ( $set_info, $where, array ($params) );
		
		if($define == "There were no changes") {
		    $define   =   "";
		}
		
		// History
		$info = array (
				"OrgID" 			=> $OrgID,
				"ApplicationID" 	=> $ApplicationID,
				"RequestID" 		=> $RequestID,
				"InternalFormID" 	=> $WebFormID,
				"Date" 				=> "NOW()",
				"UserID" 			=> $USERID,
				"Comments" 			=> $Comment,
                "UpdatedFields"     => $define	
		);
		// Insert Internal Form History
		$FormsInternalObj->insInternalFormHistory ( $info );
		
		if ($edit == "Y") {
			$MESSAGE = "The form " . $IFformname . " has been updated.";
		} else {
			$MESSAGE = "The form " . $IFformname . " has been submitted.";
		}
		
		if (FROM_SRC == "USERPORTAL") {
		    
		    //Assigned Forms List
		    $assigned_forms_list    =   G::Obj('FormsInternal')->getAssignedFormsInfo($OrgID, $RequestID, $ApplicationID, $_REQUEST['ProcessOrder']);
		    
		    $link                   =   "";
		    $link_hit_status        =   false;
		    
		    for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
		        
		        $assigned_forms_info    =   $assigned_forms_list[$afl];
		        
		        foreach($assigned_forms_info as $FormType=>$IFA) {
		            
		            if($IFA["Status"] == "Pending"
		                && $FormType != "Attachment"
		                && $FormType != "External Link"
		                && $FormType != "HTML Page") {
		                    
		                $form_status        =   $IFA["IFAInfo"]["Status"];
		                $form_status_text   =   $IFA["FormStatusText"];
		                $link               =   $IFA["Link"];
		                
		                $afl                =   count($assigned_forms_list);
		                
		                $link_hit_status    =   true;
		            }
		            
		            if($link_hit_status == true) {
		                break;
		            }
		        }
		    }
		    
	        echo '<script language="JavaScript" type="text/javascript">' . "\n";
	        $redirect_link = 'window.location = "' . USERPORTAL_HOME . 'assignedInternalForms.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&ProcessOrder=' . $_GET ['ProcessOrder'] . '&navpg=status&navsubpg=view&msg=form_suc';
	        if($link != "") {
	            $redirect_link .=  '&redirect_url='.urlencode($link);
	        }
	        $redirect_link .=  '"';
	        echo $redirect_link;
	        echo '</script>' . "\n\n";
		} else {
			$complete_status = "success";
		}
		
		echo '<br><br><br><br><br><br>';
		
	}
}

$msg_color  =   'red';
$style_attr =   'color:'.$msg_color.';';

if($complete_status == 'success') {
	$msg_color     =   '#428bca';
	$style_attr    =   'color:'.$msg_color.';text-align:center;';
}

if(isset($MESSAGE) && $MESSAGE != "") echo "<br><div style='".$style_attr."'>".$MESSAGE."</div><br><br>";

if($complete_status != "success") {
	$Submit = "Submit Form";
	
	$action = "completeWebForm.php?ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID . "&WebFormID=" . $WebFormID . "&HoldID=" . $HoldID;
	echo '<form method="post" name="frmCompleteWebForm" id="frmCompleteWebForm" action="'.$action.'">';
	
	require_once COMMON_DIR . 'formsInternal/DisplayWebForm.inc';
	
	list ( $APPDATA, $APPDATAREQ, $HoldID ) = fillWebFormData ( $USERID, $HoldID, $ApplicationID, $RequestID, $WebFormID );

	echo displayWebForm ( $WebFormID, $APPDATA, $APPDATAREQ );
	
	echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
	echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
	echo '<input type="hidden" name="ProcessOrder" value="' . $ProcessOrder . '">';
	echo '<input type="hidden" name="WebFormID" value="' . $WebFormID . '">';
	echo '<input type="hidden" name="HoldID" value="' . $HoldID . '">';
	
	// Set the condition
	$where		=	array(
						"OrgID				=	:OrgID",
						"ApplicationID		=	:ApplicationID",
						"RequestID			=	:RequestID",
						"WebFormID			=	:WebFormID"
					);
	// Bind the parameters
	$params 	=	array(
						":OrgID"			=>	$OrgID,
						":ApplicationID" 	=>	$ApplicationID,
						":RequestID" 		=>	$RequestID,
						":WebFormID" 		=>	$WebFormID
					);
	// Get the webform data
	$results	=	G::Obj('FormData')->getWebFormData ( "*", $where, "", "", array ($params) );
	
	$editcnt	=	$results ['count'];

	if ($editcnt > 0) {
		$Submit = "Edit Form";
		echo '<input type="hidden" name="edit" value="Y">';
	}
	echo '<input type="hidden" name="process" value="Y">';
	if ($ApplicationID) {
		if ($viewonly != "y") {
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
                && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				echo '<input type="button" value="' . $Submit . '" onclick="processWebForm()">';
			}
			else {
				echo '<input type="submit" value="' . $Submit . '">';
			}
		}
	}

	echo '</form>';
	echo '<br><br>';
}
?>