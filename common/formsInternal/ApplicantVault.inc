<?php
$colwidth = "220";

if($ServerInformationObj->getRequestSource() != 'ajax') {
    ?>
	<style>
    .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    </style>
	<?php
}


if(!isset($_REQUEST['display_app_header'])) {
    $applicant_valut_header = displayHeader($ApplicationID, $RequestID, "No");

    echo '<div class="form-group row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12" style="text-align:right">';
    echo $applicant_valut_header;
    echo '<hr><br>';
    echo '</div>';
    echo '</div>';

    echo '<div class="form-group row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12" style="text-align:right">';
    echo '<a href="javascript:void(0);" onclick="CloseAndRefresh(\'' . $close_link . '\')" style="text-decoration:none;">';
    echo '<img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0">';
    echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>';
    echo '</a>';
    echo '</div>';
    echo '</div>';
}


echo '<div class="form-group row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12">';
echo '<label class="question_name"><b>Applicant Vault</b></label>';
echo '</div>';
echo '</div>';
?>
<script>
function processApplicantVaultInfo(callback_url) {

    var input_data      =   new FormData($('#frmApplicantVault')[0]);
    var ApplicationID   =   document.frmApplicantVault.ApplicationID.value;
    var RequestID       =   document.frmApplicantVault.RequestID.value;
	
	if(callback_url == '') {
		callback_url  =   'applicants/applicantVault.php?ApplicationID='+ApplicationID+'&RequestID='+RequestID+'&action=updateapplicant';
		if(typeof(document.frmApplicationDetailInfo) != 'undefined') {
			var k = document.frmApplicationDetailInfo.AccessCode.value;
			callback_url += '&k='+k;
		}
	}
	
	$("#add-to-vault").html('<br><img src="images/wait.gif"/> Please wait..loading...');

	var request = $.ajax({
		method: "POST",
  		url: callback_url,
		type: "POST",
		processData: false,
		contentType: false,
		data: input_data,
		success: function(data) {
			$("#add-to-vault").html(data);
    	}
	});
}

function deleteApplicantVaultFile(msg, callback_url) {

	if(confirm(msg) == true) {
		$("#add-to-vault").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
		
		var request = $.ajax({
			method: "POST",
	  		url: callback_url,
			type: "POST",
			success: function(data) {
				$("#add-to-vault").html(data);
	    	}
		});

		return true;
	}

	return false;
}
</script>
<?php
if (($_REQUEST ['process'] == "New") || ($_REQUEST ['process'] == "Replace") || ($_GET ['delete']) || ($_GET ['replace'])) {
	
	$dir = IRECRUIT_DIR . 'vault/' . $OrgID;
	
	if (! file_exists ( $dir )) {
		mkdir ( $dir, 0700 );
		chmod ( $dir, 0777 );
	}
	
	$appvaultdir = $dir . '/applicantvault';
	
	if (! file_exists ( $appvaultdir )) {
		mkdir ( $appvaultdir, 0700 );
		chmod ( $appvaultdir, 0777 );
	}
}

if ($_GET ['delete']) {
	
    // Bind parameters
    $params     =   array (':OrgID'=>$OrgID, ':ApplicationID'=>$ApplicationID, ':UpdateID'=>$_GET ['delete']);
    // Set condition
    $where      =   array ("OrgID = :OrgID", "ApplicationID = :ApplicationID", "UpdateID = :UpdateID");
    // Get Applicant Vault Information
    $results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
    $AV         =   $results ['results'] [0];
	
	// Get Date Time
    $HLD        =   $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
	
    $filenameA  =   $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '.' . $AV ['FileType'];
    $filenameB  =   $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '-' . $HLD . 'D.' . $AV ['FileType'];
	
    $Comments   =   "Deleted File from Vault: " . $AV ['FileName'] . '.' . $AV ['FileType'] . "<br>";
    $Comments   .=  '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
    $Comments   .=  '<a href="' . IRECRUIT_HOME . 'applicants/display_applicantVault_File.php?OrgID=' . $OrgID . '&File=' . $filenameB . '">Download</a> previous version.';
	
	// Job Application History
	$job_app_history = array (
			"OrgID"                  =>  $OrgID,
			"ApplicationID"          =>  $ApplicationID,
			"RequestID"              =>  $AV ['RequestID'],
			"ProcessOrder"           =>  "-3",
			"StatusEffectiveDate"    =>  "DATE(NOW())",
			"Date"                   =>  "NOW()",
			"UserID"                 =>  $USERID,
			"Comments"               =>  $Comments 
	);
	
	// Insert Job Application History
	G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
	$command = 'mv ' . $appvaultdir . '/' . $filenameA . ' ' . $appvaultdir . '/' . $filenameB;
	system ( $command );
	
	// Bind parameters
	$params = array (
			':OrgID'                 =>  $OrgID,
			':ApplicationID'         =>  $ApplicationID,
			':UpdateID'              =>  $_GET ['delete'] 
	);
	// Set where condition
	$where = array (
			"OrgID                   =   :OrgID",
			"ApplicationID           =   :ApplicationID",
			"UpdateID                =   :UpdateID" 
	);
	// Delete Applicant Vault Information
	G::Obj('ApplicantVault')->delApplicantVault ( $where, array ($params) );
	
	$MESSAGE = "File deleted: " . $AV ['FileName'] . '.' . $AV ['FileType'];
	echo '<script language="JavaScript" type="text/javascript">' . "\n";
	echo "alert('$MESSAGE')" . "\n";
	echo '</script>' . "\n\n";
} // end GET[delete]

if ($_GET ['edit']) {

	// Bind the parameters
	$params = array (
			":OrgID" => $OrgID,
			":ApplicationID" => $ApplicationID,
			":UpdateID" => $_GET ['edit']
	);
	// Set the condition
	$where = array (
			"OrgID = :OrgID",
			"ApplicationID = :ApplicationID",
			"UpdateID = :UpdateID"
	);

	// Get Applicant Vault Information
	$results   =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
	$AV        =   $results ['results'] [0];

	$typefile  =   "Replace File: <font style=\"color:red;\">" . $AV ['FileName'] . '.' . $AV ['FileType'] . "</fon>";
	$submit    =   "Replace File";
	$add       =   ' <a href="' . IRECRUIT_HOME . 'applicants/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">Add File</a>';
	$process   =   "Replace";
	$UpdateID  =   $AV ['UpdateID'];
	
} else {

	$typefile  =   "Upload File:";
	$submit    =   "Upload File";
	$add       =   "";
	$process   =   "New";
	$UpdateID  =   "";
	
} // end GET[edit]

if ($_FILES ['newfile'] ['type'] != "") {
	
	$e         =   explode ( '.', $_FILES ['newfile'] ['name'] );
	$ecnt      =   count ( $e ) - 1;
	$FileType  =   $e [$ecnt];
	$FileType  =   preg_replace ( "/\s/i", '', $FileType );
	$FileName  =   $e [$ecnt - 1];
	$FileName  =   preg_replace ( "/\s/i", '_', $FileName );
	$FileName  =   preg_replace ( "/,/i", '', $FileName );
	$FileName  =   preg_replace ( "/'/i", '', $FileName );
	$data      =   file_get_contents ( $_FILES ['newfile'] ['tmp_name'] );
	
} // end if FILES

if (($process == "Replace") && ($data != "")) {
	
	// Bind parameters
	$params = array (
			':OrgID'         =>  $OrgID,
			':ApplicationID' =>  $ApplicationID,
			':UpdateID'      =>  $UpdateID 
	);
	
	// Set condition
	$where = array (
			"OrgID           =   :OrgID",
			"ApplicationID   =   :ApplicationID",
			"UpdateID        =   :UpdateID" 
	);
	
	// Get Applicant Vault Information
    $results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
    $AV         =   $results ['results'] [0];
	
	// Bind parameters
	$params = array (
			":FileName"      =>  $FileName,
			":FileType"      =>  $FileType,
			":UserID"        =>  $USERID,
			":OrgID"         =>  $OrgID,
			":ApplicationID" =>  $ApplicationID,
			":RequestID"     =>  $RequestID,
			"UpdateID"       =>  $UpdateID 
	);
	// Update Applicant Vault
	G::Obj('ApplicantVault')->updApplicantVault ( array ($params) );
	
	// Get DateTime
	$HLD = $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
	
	$filenameA = $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '.' . $AV ['FileType'];
	$filenameB = $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '-' . $HLD . 'R.' . $AV ['FileType'];
	
	$Comments = "Replaced File in Vault: " . $AV ['FileName'] . '.' . $AV ['FileType'] . ' with ' . $FileName . '.' . $FileType . "<br>";
	$Comments .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
	$Comments .= '<a href="' . IRECRUIT_HOME . 'applicants/display_applicantVault_File.php?OrgID=' . $OrgID . '&File=' . $filenameB . '">Download</a> previous version.';
	
	// Job Application History
	$job_app_history = array (
        "OrgID"                 =>  $OrgID,
        "ApplicationID"         =>  $ApplicationID,
        "RequestID"             =>  $RequestID,
        "ProcessOrder"          =>  "-3",
        "StatusEffectiveDate"   =>  "DATE(NOW())",
        "Date"                  =>  "NOW()",
        "UserID"                =>  $USERID,
        "Comments"              =>  $Comments 
	);
	
	// Insert Job Application History
	G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
	$command = 'mv ' . $appvaultdir . '/' . $filenameA . ' ' . $appvaultdir . '/' . $filenameB;
	system ( $command );
	
	$filename = $appvaultdir . '/' . $ApplicationID . '-' . $UpdateID . '-' . $FileName . '.' . $FileType;
	$fh = fopen ( $filename, 'w' );
	fwrite ( $fh, $data );
	fclose ( $fh );
	
	chmod ( $filename, 0644 );
	
	$MESSAGE = $AV ['FileName'] . '.' . $AV ['FileType'] . " replaced with: " . $FileName . '.' . $FileType;
	echo '<script language="JavaScript" type="text/javascript">' . "\n";
	echo "alert('$MESSAGE')" . "\n";
	echo '</script>' . "\n\n";
} // end process Replace

if (($_REQUEST ['process'] == "New") && ($data != "")) {
	
	// Bind parameters for Applicant Vault Data
	$applicant_vault_info = array (
			"OrgID"          =>  $OrgID,
			"ApplicationID"  =>  $ApplicationID,
			"RequestID"      =>  $RequestID,
			"FileName"       =>  $FileName,
			"FileType"       =>  $FileType,
			"Date"           =>  "NOW()",
			"UpdateID"       =>  '',
			"UserID"         =>  $USERID 
	);
	// Insert Applicant Vault Information
	G::Obj('ApplicantVault')->insApplicantVault ( $applicant_vault_info );
	
	// Get Applicant Vault Information
	$results = G::Obj('ApplicantVault')->getApplicantVaultInfo ( 'LAST_INSERT_ID() AS last_insert_id', array (), 'OrgID LIMIT 1', array () );
	$AVIR = $results ['results'] [0] ['last_insert_id'];
	
	$Comments = "Added File to Vault: " . $FileName . '.' . $FileType;
	
	// Job Application History
	$job_app_history = array (
			"OrgID"          =>  $OrgID,
			"ApplicationID"  =>  $ApplicationID,
			"RequestID"      =>  $RequestID,
			"ProcessOrder"   =>  "-3",
			"StatusEffectiveDate" => "DATE(NOW())",
			"Date"           =>  "NOW()",
			"UserID"         =>  $USERID,
			"Comments"       =>  $Comments 
	);
	
	// Insert Job Application History
	G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
	$filename = $appvaultdir . '/' . $ApplicationID . '-' . $AVIR . '-' . $FileName . '.' . $FileType;
	$fh = fopen ( $filename, 'w' );
	fwrite ( $fh, $data );
	fclose ( $fh );
	
	chmod ( $filename, 0644 );
	
	$MESSAGE = "File Added: " . $FileName . '.' . $FileType;
	echo '<script language="JavaScript" type="text/javascript">' . "\n";
	echo "alert('$MESSAGE')" . "\n";
	echo '</script>' . "\n\n";
} // end process new
  
// Bind the parameters
$params = array (
		":OrgID"          =>  $OrgID,
		":ApplicationID"  =>  $ApplicationID,
		":RequestID"      =>  $RequestID 
);
// Set the condition
$where = array (
		"OrgID            =   :OrgID",
		"ApplicationID    =   :ApplicationID",
		"RequestID        =   :RequestID" 
);

if (FROM_SRC == "USERPORTAL") {
	$where [] = "CONCAT('', UserID * 1) = UserID";
}

// Set Columns
$columns    =   "FileName, FileType, date_format(Date,'%Y-%m-%d') Date, UpdateID, UserID";
// Get Applicant Vault Information
$results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( $columns, $where, 'FileName', array ($params) );
// Get Count Applicant Vault Information Records Count
$cnt        =   $results ['count'];

if(FROM_SRC == 'USERPORTAL')
	$applicant_vault_url = USERPORTAL_HOME . 'formsInternal/applicantVault.php';
if(FROM_SRC == 'IRECRUIT')
	$applicant_vault_url = IRECRUIT_HOME . 'applicants/applicantVault.php';


echo '<form method="POST" name="frmApplicantVault" id="frmApplicantVault" enctype="multipart/form-data">';

echo '<input type="hidden" name="ApplicationID" value="' . htmlspecialchars($ApplicationID) . '">';
echo '<input type="hidden" name="RequestID" value="' . htmlspecialchars($RequestID) . '">';
echo '<input type="hidden" name="process" value="' . htmlspecialchars($process) . '">';
echo '<input type="hidden" name="display_app_header" value="' . htmlspecialchars($_REQUEST['display_app_header']) . '">';
echo '<input type="hidden" name="UpdateID" value="' . htmlspecialchars($UpdateID) . '">';

if ($action != "") {
    echo '<input type="hidden" name="action" value="' . htmlspecialchars($action) . '">';
}
if ($PreFilledFormID != "") {
    echo '<input type="hidden" name="PreFilledFormID" value="' . htmlspecialchars($PreFilledFormID) . '">';
}

echo '<input type="hidden" name="MAX_FILE_SIZE" value="10485760">';

echo '<div style="float:left;padding-top:10px;">';
echo '<input type="file" name="newfile" value="">';
echo '</div>';

echo '<div>';

if ($ServerInformationObj->getRequestSource() == 'ajax') {
	echo '<input type="button" value="' . $submit . '" class="btn btn-primary" onclick="processApplicantVaultInfo(\'\');">';
}
else {
	echo '<input type="submit" value="' . $submit . '">';
}

echo '</div>';
echo '</form>';

echo '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><br></div></div>';

if ($cnt > 0) {
    echo '<div class="form-group row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12">';
    
    echo '<div class="table-responsive">';
	echo '<table border="0" cellspacing="3" cellpadding="5" align="center" class="table table-striped table-bordered table-hover">';
	
	echo '<tr height="30">';
	
	echo '<td>';
	echo '<b>File Name</b>';
	echo '</td>';
	
	echo '<td>';
	echo '<b>Date</b>';
	echo '</td>';
	
	echo '<td>';
	echo '<b>Uploaded By</b>';
	echo '</td>';
	
	echo '<td align="center" width="60">';
	echo '<b>View</b>';
	echo '</td>';
	
	if (FROM_SRC == "IRECRUIT") {
		
		echo '<td align="center" width="60">';
		echo '<b>Replace</b>';
		echo '</td>';
		
		echo '<td align="center" width="60">';
		echo '<b>Delete</b>';
		echo '</td>';
	} // end FROM IRECRUIT
	
	echo '<tr>';
	
	$rowcolor = "#eeeeee";
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $AV ) {
			
			echo '<tr bgcolor="' . $rowcolor . '">';
			
			echo '<td>';
			echo $AV ['FileName'];
			echo '.';
			echo $AV ['FileType'];
			echo '</td>';
			
			echo '<td>';
			echo $AV ['Date'];
			echo '</td>';
			
			echo '<td>';
			
			if (is_numeric ( $AV ['UserID'] )) {
				
				echo "PORTAL:<br>";
				
				// Set condition
				$where = array("UserID = :UserID");
				// Bind the parameters
				$params = array(":UserID" => $AV ['UserID']);
				
				// Get User Information
				$resultsUP = $UserPortalUsersObj->getUsersInformation("FirstName, LastName, Email", $where, "", "", array ($params) );
				$User = $resultsUP ['results'] [0] ['LastName'].", ".$resultsUP ['results'] [0] ['FirstName'];
				$Em = $resultsUP ['results'] [0] ['Email'];
				
				echo $User . "<br>";
				
				if ($permit ['Applicants_Contact'] == 1) { // if access to view email
					echo '<a href="mailto:' . $Em . '">' . $Em . '</a>';
				} else {
					echo $Em;
				}
			} else {
				echo $AV ['UserID'];
			}
			
			echo '</td>';
			
			echo '<td align="center">';
			echo '<a href="' . IRECRUIT_HOME . 'applicants/display_applicantVault_File.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&UpdateID=' . $AV ['UpdateID'];
			if ($action != "") {
				echo '&action=' . $action;
			}
			if ($PreFilledFormID != "") {
				echo '&PreFilledFormID=' . htmlspecialchars($PreFilledFormID);
			}
			echo '">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
			echo '</a>';
			echo '</td>';
			
			if (FROM_SRC == "IRECRUIT") {
				echo '<td align="center">';
				
				$edit_url = 'applicants/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&edit=' . $AV ['UpdateID'];
				if ($action != "") {
					$edit_url .= '&action=' . $action;
				}
				if ($PreFilledFormID != "") {
					$edit_url .= '&PreFilledFormID=' . $PreFilledFormID;
				}
				
				$delete_url = 'applicants/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&delete=' . $AV ['UpdateID'] . '&display_app_header=no';
				if ($action != "") {
					$delete_url .= '&action=' . $action;
				}
				if ($PreFilledFormID != "") {
					$delete_url .= '&PreFilledFormID=' . $PreFilledFormID;
				}
				
				echo '<a href=\'javascript:void(0);\' onclick=\'processApplicantVaultInfo("'.$edit_url.'")\'>';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
				echo '</td>';
				
				echo '<td align="center">';
				echo '<a href=\'javascript:void(0);\' onclick=\'return deleteApplicantVaultFile("Are you sure you want to delete the file? ' . htmlspecialchars($AV ['FileName']) . '.' . htmlspecialchars($AV ['FileType']) . '", "'.htmlspecialchars($delete_url).'")\'>';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
				echo '</td>';
			} // end FROM IRECRUIT
			
			echo '</tr>' . "\n";
			
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach
	}
	
	echo '</td></tr>';
	echo '</table>';
	echo '</div></div></div>';
} // end $cnt applicantVault items

echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
END;
?>
