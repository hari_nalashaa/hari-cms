<?php 
// ** FUNCTIONS ** //
function printAgreementForm($OrgID, $ApplicationID, $RequestID, $AgreementFormID) {
    global $permit;

    //Get Agreement Form Data
    $results = G::Obj('AgreementFormData')->getAgreementFormData($OrgID, $AgreementFormID, $RequestID, $ApplicationID);

    if(is_array($results['results'])) {
        foreach ($results['results'] as $WFA) {
            $APPDATA [$WFA ['QuestionID']] = $WFA ['Answer'];
        }
    }

    $AgreementFormQueAns  =    G::Obj('AgreementFormQueAns')->getAgreementFormQueAnswersInfo ( $OrgID, $ApplicationID, $RequestID, $AgreementFormID );
    
    $agr_form_view  =   '';
    foreach ($AgreementFormQueAns as $QuestionID=>$QuestionAnswerInfo)
    {
        $agr_form_view .= "<div class='row'>";
        
        if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
            
            $agr_form_view .= "<div class='col-lg-3 col-md-3 col-sm-3' style='width:20%;float:left;'>".$QuestionAnswerInfo['Question']."</div>";
            
            $canswer = $QuestionAnswerInfo['Answer'];
            
            $rtn  = '';
            if(is_array($canswer)) {
                $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
                
                foreach ($canswer as $cakey=>$caval) {
                    $rtn .= '<tr>';
                    $rtn .= '<td style="padding-right:5px !important">'.$cakey.'</td>';
                    foreach ($caval as $cavk=>$cavv) {
                        $rtn .= '<td style="padding-right:5px !important">'.$cavv.'</td>';
                    }
                    $rtn .= '</tr>';
                }
                
                $rtn .= '</table>';
            }
            
            $agr_form_view .= "<div class='col-lg-9 col-md-9 col-sm-9' style='width:78%;float:left;border:1px solid #F5F5F5;'><strong>".$rtn."</strong></div>";
            
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
            
            $agr_form_view .= "<div class='col-lg-3 col-md-3 col-sm-3' style='width:20%;float:left;border:1px solid #F5F5F5;'>".$QuestionAnswerInfo['Question']."</div>";
            
            $canswer = $QuestionAnswerInfo['Answer'];
            
            $rtn  = '';
            if(is_array($canswer)) {
                
                $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
                
                $days_count = count($canswer['days']);
                
                for($ci = 0; $ci < $days_count; $ci++)  {
                    $rtn .= '<tr>';
                    $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
                    $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
                    $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
                    $rtn .= '<td width="5%">to </td>';
                    $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
                    $rtn .= '</tr>';
                }
                
                $rtn .= '</table>';
            }
            
            $agr_form_view .= "<div class='col-lg-9 col-md-9 col-sm-9' style='width:78%;float:left;border:1px solid #F5F5F5;'><strong>".$rtn."</strong></div>";
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
            $agr_form_view .= "<div class='col-lg-12 col-md-12 col-sm-12' style='width:100%;float:left;border:1px solid #F5F5F5;'><strong>".$QuestionAnswerInfo['Question']."</strong></div>";
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 60) {
            $agr_form_view .= "<div class='col-lg-12 col-md-12 col-sm-12' style='width:100%;float:left;border:1px solid #F5F5F5;'>".$QuestionAnswerInfo['Answer']."</div>";
        }
        else {
            $agr_form_view .= "<div class='col-lg-3 col-md-3 col-sm-3' style='width:20%;float:left;border:1px solid #F5F5F5;'>".$QuestionAnswerInfo['Question']."</div>";
            $agr_form_view .= "<div class='col-lg-9 col-md-9 col-sm-9' style='width:78%;float:left;border:1px solid #F5F5F5;'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
        }
        
        $agr_form_view .= "<div class='col-lg-12 col-md-12 col-sm-12' style='clear:both;' id='question_space'></div>";
        
        $agr_form_view .= "</div>";
    }
    
    return $agr_form_view;
} // end of function

function getAgreementDisplayValue($ans, $displaychoices) {
    $rtn = '';

    if ($ans != '') {
        $Values = explode ( '::', $displaychoices );
        foreach ( $Values as $v => $q ) {
            list ( $vv, $qq ) = explode ( ":", $q, 2 );
            if ($vv == $ans) {
                $rtn .= $qq;
            }
        }
    }
    return $rtn;
} // end of function

function getAgreementMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
    $rtn = '';
    $val = '';
    $lex = '';

    if (($id == 'daysavailable') || ($id == 'typeavailable')) {

        $le = ', ';
        $li = '';
        $lecnt = - 2;
    } else {

        $le = '<br>';
        $li = '&#8226;&nbsp;';
        $lecnt = - 4;
    }

    $cnt = $APPDATA [$id . 'cnt'];

    for($i = 1; $i <= $cnt; $i ++) {

        $val = getAgreementDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
        if ($val) {
            $rtn .= $li . $val;
            $lex = 'on';
            $val = '';
        }

        if ($questiontype == 9) {
            	
            $val = $APPDATA [$id . '-' . $i . '-yr'];
            if ($val) {
                $rtn .= '&nbsp;' . $val . ' yrs. ';
                $lex = 'on';
                $val = '';
            }
            	
            $val = $APPDATA [$id . '-' . $i . '-comments'];
            if ($val) {
                $rtn .= '&nbsp;(' . $val . ') ';
                $lex = 'on';
                $val = '';
            }
        } // end questiontype

        if ($lex == 'on') {
            $rtn .= $le;
            $lex = '';
        }
    }

    $rtn = substr ( $rtn, 0, $lecnt );

    return $rtn;
} // end function
?>
