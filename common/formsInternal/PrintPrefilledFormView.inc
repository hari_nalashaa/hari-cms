<?php 
// ** FUNCTIONS ** //
function printPrefilledForm($OrgID, $ApplicationID, $RequestID, $PreFilledFormID) {
    global $permit, $FormsInternalObj, $FormDataObj;

    if ($PreFilledFormID == "FE-I9m") {
        echo I9Data ("FE-I9");
    } // end PreFilledFormID FE-I9m
    if ($PreFilledFormID == "FE-I9m-2020") {
        echo I9Data ("FE-I9-2020");
    } // end PreFilledFormID FE-I9m

    //Set condition
    $where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID");
    //Set parameters
    $params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID);
    //Get Prefilled Forms
    $results = $FormDataObj->getPreFilledFormData(array("QuestionID", "Answer"), $where, "", "", array($params));

    if(is_array($results['results'])) {
        foreach($results['results'] as $WFA) {
            $APPDATA [$WFA ['QuestionID']] = $WFA ['Answer'];
        }
    }

    $rtn = '';

    //Get Prefilled Form Data
    $where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionOrder > 0");
    //set parameters
    $params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID);
    //get prefilled form data
    $rslt = $FormDataObj->getPreFilledFormData("*", $where, "", "QuestionOrder", array($params));
    $rowcnt = $rslt['count'];

    if(is_array($rslt['results'])) {
        foreach ($rslt['results'] as $FQ) {

            if (($FQ ['QuestionTypeID'] == 99) || ($FQ ['QuestionTypeID'] == 98)) {
                $rtn .= '<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="margin-bottom:1px !important">' . "\n";
                $rtn .= '<tr><td>';
                $rtn .= $FQ ['Question'];
            } elseif ($FQ ['QuestionID'] != 'salarydesireddefinition') {
                $rtn .= '<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered" style="margin-bottom:1px !important">' . "\n";
                $rtn .= '<tr><td width="300" valign="top">';
                $rtn .= $FQ ['Question'];
                $rtn .= '</td><td width="10"></td><td valign="top">';
            } else {
                $rtn .= '&nbsp;&nbsp;' . $FQ ['Question'] . '&nbsp;&nbsp;';
            }

            // format answers
            if ($FQ ['value']) {
                $APPDATA [$FQ ['QuestionID']] = getPrefilledDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
            }

            if (($FQ ['QuestionTypeID'] == 18) || ($FQ ['QuestionTypeID'] == 1818)) { // checkbox
                $Answer18  = json_decode($APPDATA[$FQ ['QuestionID']], true);
                foreach ($Answer18 as $Answer18Key=>$Answer18Value) {
                    $APPDATA[$Answer18Key] =   $Answer18Value;
                }
                $APPDATA[$FQ ['QuestionID'].'cnt']   =   count($Answer18);
                
                $APPDATA [$FQ ['QuestionID']] = getPrefilledMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
            }

            if ($FQ ['QuestionTypeID'] == 9) { // mulit checkbox
                $APPDATA [$FQ ['QuestionID']] = getPrefilledMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
            }

            if ($FQ ['QuestionTypeID'] == 15) { // social

                $Answer15   =   ($APPDATA[$FQ['QuestionID']] != "") ? json_decode($APPDATA[$FQ['QuestionID']], true) : "";
                $ck1        =   $Answer15[0];
                $ck2        =   $Answer15[1];
                $ck3        =   $Answer15[2];
                	
                if (($Answer15[0]) || ($Answer15[1]) || ($Answer15[2])) {
                    $APPDATA [$FQ ['QuestionID']] = $Answer15[0] . '-' . $Answer15[1] . '-' . $Answer15[2];
                } // end if not blank
            }

            if ($FQ ['QuestionTypeID'] == 13) { // Phone
                $Answer13   =   ($APPDATA[$FQ['QuestionID']] != "") ? json_decode($APPDATA[$FQ['QuestionID']], true) : "";
                $APPDATA [$FQ ['QuestionID']] = $AddressObj->formatPhone($OrgID, "", $Answer13[0], $Answer13[1], $Answer13[2], '');
            }
            
            if ($FQ ['QuestionTypeID'] == 14) { // Phone
                $Answer14   =   ($APPDATA[$FQ['QuestionID']] != "") ? json_decode($APPDATA[$FQ['QuestionID']], true) : "";
                $APPDATA [$FQ ['QuestionID']] = $AddressObj->formatPhone($OrgID, "", $Answer14[0], $Answer14[1], $Answer14[2], $Answer14[3]);
            }
            
            if (($FQ ['QuestionTypeID'] == 1) || ($FQ ['QuestionTypeID'] == 24)) { // Date from and to
                $APPDATA [$FQ ['QuestionID']] = "from " . $APPDATA [$FQ ['QuestionID'] . from] . " to " . $APPDATA [$FQ ['QuestionID'] . to];
            }

            if ($FQ ['QuestionTypeID'] == 30) { // Signature
                $APPDATA [$FQ ['QuestionID']] = 'Electronically by symbol ' . $APPDATA ['captcha'] . ' on ' . $FQ ['Answer'];
            }

            if (($FQ ['QuestionTypeID'] != 99) && ($FQ ['QuestionTypeID'] != 98)) {
                $rtn .= '<b>';
                if ($APPDATA [$FQ ['QuestionID']] == "0") {
                    $rtn .= $APPDATA [$FQ ['QuestionID']];
                } else {
                    $rtn .= $APPDATA [$FQ ['QuestionID']] ? $APPDATA [$FQ ['QuestionID']] : '<i style="color:#888888;">(not answered)<i>';
                }
                $rtn .= '</b>';
            } // end if

            $rtn .= '</td></tr>';
        } // end foreach
    }


    $rtn .= '</table>';

    if ($rowcnt == 0) {
        $rtn = '';
    }

    return $rtn;
} // end of function
function getPrefilledDisplayValue($ans, $displaychoices) {
    $rtn = '';

    if ($ans != '') {
        $Values = explode ( '::', $displaychoices );
        foreach ( $Values as $v => $q ) {
            list ( $vv, $qq ) = explode ( ":", $q, 2 );
            if ($vv == $ans) {
                $rtn .= $qq;
            }
        }
    }

    return $rtn;
} // end of function
function getPrefilledMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
    $rtn = '';
    $val = '';
    $lex = '';

    if (($id == 'daysavailable') || ($id == 'typeavailable')) {

        $le = ', ';
        $li = '';
        $lecnt = - 2;
    } else {

        $le = '<br>';
        $li = '&#8226;&nbsp;';
        $lecnt = - 4;
    }

    $cnt = $APPDATA [$id . 'cnt'];

    for($i = 1; $i <= $cnt; $i ++) {

        $val = getPrefilledDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
        if ($val) {
            $rtn .= $li . $val;
            $lex = 'on';
            $val = '';
        }

        if ($questiontype == 9) {
            	
            $val = $APPDATA [$id . '-' . $i . '-yr'];
            if ($val) {
                $rtn .= ',&nbsp;' . $val . ' yrs. ';
                $lex = 'on';
                $val = '';
            }
            	
            $val = $APPDATA [$id . '-' . $i . '-comments'];
            if ($val) {
                $rtn .= '&nbsp;(' . $val . ') ';
                $lex = 'on';
                $val = '';
            }
        } // end questiontype

        if ($lex == 'on') {
            $rtn .= $le;
            $lex = '';
        }
    }

    $rtn = substr ( $rtn, 0, $lecnt );

    return $rtn;
} // end of function
?>
