<?php
require_once COMMON_DIR . 'formsInternal/Signature.inc';
require_once COMMON_DIR . 'formsInternal/DisplayAgreementForm.inc';

$complete_status            =   "";
$GetFormPostAnswerObj->POST =   $_POST;    //Set Post Data

if ($_REQUEST['process'] == "Y") {
	
	// Clear the db of older entries for this HoldID
	G::Obj('ApplicantDataTemp')->delApplicantDataTemp($OrgID, $HoldID);
	
	include COMMON_DIR . 'formsInternal/ProcessAgreementFormDataTemp.inc';
	
	include COMMON_DIR . 'formsInternal/AgreementFormValidateRequired.inc';

	if($ERROR) {
		$complete_status = "failure";
		$MESSAGE = "The following entries are missing information.\\n";
		$MESSAGE .= $ERROR;
		$MESSAGE = str_replace(array("\n", "\\n"), "<br>", $MESSAGE);
	}
	else {
		// if there are no errors there is no need to hold temp files
		G::Obj('ApplicantDataTemp')->delApplicantDataTemp($OrgID, $HoldID);
		$HoldID = "";
		
		// process as all data is good
		// determine changes for history
		if ($edit == "Y") {
			$define = agreementHistoryChanged ( $AgreementFormID, $ApplicationID, $RequestID );
			// Clear the db of older entries based on parameters
			$AgreementFormDataObj->delApplicantAgreementFormData($OrgID, $AgreementFormID, $RequestID, $ApplicationID);
		}

		
        /**
		 * This is a temporary code until the final upgrade.
		 * Upto that time, we have to keep on adding different conditions to it.
		 * After final upgrade we will have to remove the loop through $_POST
		 */
        $form_que_list         =   $AgreementFormQuestionsObj->getAgreementFormQuestionsList($OrgID, $AgreementFormID);
		
		$ques_to_skip          =   array();
		foreach($form_que_list as $QuestionID=>$QuestionInfo) {
		
		    $QI                =   $QuestionInfo;
            G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
            $values             =   G::Obj('GetFormPostAnswer')->getDisplayValue($QI['value']);
		    
		    if($QI['QuestionTypeID'] == 13) {
		        //After the complete upgrade have to remove this.

		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		         
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		        
		        //Have to work on web form insert call
		        $AgreementFormDataObj->insUpdAgreementFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 14) {
		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		        $ques_to_skip[]        =   $QI['QuestionID']."ext";
		        
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;

		        //Have to work on web form insert call
		        $AgreementFormDataObj->insUpdAgreementFormData($QI);
		    }
            else if($QI['QuestionTypeID'] == 15) {
		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		        $ques_to_skip[]        =   $QI['QuestionID'];
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //Have to work on web form insert call
		        $AgreementFormDataObj->insUpdAgreementFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 18) {
		        //After the complete upgrade have to remove this.
		        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
		    
		        for($c18 = 1; $c18 <= $cnt; $c18++) {
		            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
		        }
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //Have to work on web form insert call
		        $AgreementFormDataObj->insUpdAgreementFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 1818) {
		        //After the complete upgrade have to remove this.
		        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
		    
		        for($c18 = 1; $c18 <= $cnt; $c18++) {
		            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
		        }
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //Have to work on web form insert call
		        $AgreementFormDataObj->insUpdAgreementFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 9) {
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //After the complete upgrade have to remove this.
		        $qis  =   0;
		        foreach ( $values as $v => $q ) {
		            $qis++;
		             
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis;
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-yr";
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-comments";
		        }

		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";

		        //Have to work on web form insert call
		        $AgreementFormDataObj->insUpdAgreementFormData($QI);
		    }
		}		
		
		foreach ( $_POST as $questionid => $answer ) {
		    
		    if(!in_array($questionid, $ques_to_skip)) {
		
    			//Bind parameters
    			$params  =   array(':OrgID'=>$OrgID, ':AgreementFormID'=>$AgreementFormID);
    			//Set condition
    			$where   =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
    		
    			//Get agreement forms information
    			$results =   $FormsInternalObj->getAgreementFormsInfo(array("FormName", "FormPart"), $where, "", array($params));
    		
    			if(is_array($results['results'][0])) {
    				list ( $AFformname, $AFformpart ) = array_values($results['results'][0]);
    			}
    		
    			if ($questionid != "captcha") {
    					
    				if ($questionid == 'LabelSelect' || $questionid == 'shifts_schedule_time') {
    		
    					foreach ( $_POST [$questionid] as $cquestion_id => $cval ) {
    							
    						//Bind parameters
    						$params   =   array(':OrgID'=>$OrgID, ':AgreementFormID'=>$AgreementFormID, ':QuestionID'=>$cquestion_id);
    						//Set condition
    						$where    =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
    						//Set columns
    						$columns  =   array("Question", "QuestionID", "QuestionOrder", "QuestionTypeID", "value");
    						//Get Agreement Form Questions
    						$results  =   $FormsInternalObj->getAgreementFormQuestions($columns, $where, "", array($params));
    							
    						$agree_form_que_info = $results['results'][0];
    							
    						$question         =   $agree_form_que_info["Question"];
    						$questionid       =   $agree_form_que_info["QuestionID"];
    						$questionorder    =   $agree_form_que_info["QuestionOrder"];
    						$questiontypeid   =   $agree_form_que_info["QuestionTypeID"];
    						$value            =   $agree_form_que_info["value"];
    							
    						$qid = $questionid = $cquestion_id;
    							
    						if ($questiontypeid == 100) {
    							$answer = serialize ( $_POST ['LabelSelect'] [$questionid] );
    						}
    						
    						if ($questiontypeid == 120) {
    							$qa ['from_time']    =   $_POST ['shifts_schedule_time'] [$questionid] [from_time];
    							$qa ['to_time']      =   $_POST ['shifts_schedule_time'] [$questionid] [to_time];
    							$qa ['days']         =   $_POST ['shifts_schedule_time'] [$questionid] [days];
    		
    							$answer = serialize ( $qa );
    						}
    
    						
    						// agreement form data to insert
    						$ins_agree_form_data = array (
    						    "OrgID" 			=> $OrgID,
    						    "ApplicationID" 	=> $ApplicationID,
    						    "RequestID" 		=> $RequestID,
    						    "AgreementFormID" 	=> $AgreementFormID,
    						    "QuestionID" 		=> $questionid,
    						    "QuestionOrder" 	=> $questionorder,
    						    "QuestionTypeID" 	=> $questiontypeid,
    						    "Question" 			=> $question,
    						    "Answer" 			=> $answer,
    						    "value" 			=> $value
    						);
    						
    						if ((substr ( $questionid, 0, 9 ) != "countdown") && ($questionid != 'AgreementFormID') && ($questionid != 'ApplicationID') && ($questionid != 'RequestID') && ($questionid != 'HoldID') && ($questionid != 'edit') && ($questionid != "process")) {
    						    if ($questionid != "") {
    						        // Update on duplicate
    						        $on_update    =   " ON DUPLICATE KEY UPDATE QuestionOrder = :UQuestionOrder, QuestionTypeID = :UQuestionTypeID,
    					                               Question = :UQuestion, Answer = :UAnswer, value = :Uvalue";
    						        $update_info  =   array (
                        						            ":UQuestionOrder" 	=> $questionorder,
                        						            ":UQuestionTypeID" 	=> $questiontypeid,
                        						            ":UQuestion" 		=> $question,
                        						            ":UAnswer" 			=> $answer,
                        						            ":Uvalue" 			=> $value
                        						        );
    						        // Insert Form Data
    						        $FormsInternalObj->insFormData ( 'AgreementFormData', $ins_agree_form_data, $on_update, $update_info );
    						    }
    						}
    						
    					}
    				} else {
    		
    					$split_question_id = $questionid;
    					if(strpos($questionid, "-")) {
    						$que_id_pieces = explode("-", $questionid);
    						$split_question_id = $que_id_pieces[0];
    					}
    		
    					//Bind parameters
    					$params    =   array(':OrgID'=>$OrgID, ':AgreementFormID'=>$AgreementFormID, ':QuestionID'=>$split_question_id);
    					//Set condition
    					$where     =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
    					//Set columns
    					$columns   =   array("Question", "QuestionID", "QuestionOrder", "QuestionTypeID", "value");
    					//Get Agreement Form Questions
    					$results   =   $FormsInternalObj->getAgreementFormQuestions($columns, $where, "", array($params));
    		
    					if(is_array($results['results'][0])) {
    						//Get AgreementFormQuestions Information
    						list ( $question, $qid, $questionorder, $questiontypeid, $value ) = array_values($results['results'][0]);
    					}
    		
    					$answer    =   $_POST [$questionid];
    		
    					
    					// agreement form data to insert
    					$ins_agree_form_data = array (
    					    "OrgID" 			=> $OrgID,
    					    "ApplicationID" 	=> $ApplicationID,
    					    "RequestID" 		=> $RequestID,
    					    "AgreementFormID" 	=> $AgreementFormID,
    					    "QuestionID" 		=> $questionid,
    					    "QuestionOrder" 	=> $questionorder,
    					    "QuestionTypeID" 	=> $questiontypeid,
    					    "Question" 			=> $question,
    					    "Answer" 			=> $answer,
    					    "value" 			=> $value
    					);
    					
    					if ((substr ( $questionid, 0, 9 ) != "countdown") && ($questionid != 'AgreementFormID') && ($questionid != 'ApplicationID') && ($questionid != 'RequestID') && ($questionid != 'HoldID') && ($questionid != 'edit') && ($questionid != "process")) {
    					    if ($questionid != "") {
    					        // Update on duplicate
    					        $on_update     =   " ON DUPLICATE KEY UPDATE QuestionOrder = :UQuestionOrder, QuestionTypeID = :UQuestionTypeID,
    					                           Question = :UQuestion, Answer = :UAnswer, value = :Uvalue";
    					        $update_info   =   array (
                                                        ":UQuestionOrder" 	=> $questionorder,
                                                        ":UQuestionTypeID" 	=> $questiontypeid,
                                                        ":UQuestion" 		=> $question,
                                                        ":UAnswer" 			=> $answer,
                                                        ":Uvalue" 			=> $value
                                                    );
    					        // Insert Form Data
    					        $FormsInternalObj->insFormData ( 'AgreementFormData', $ins_agree_form_data, $on_update, $update_info );
    					    }
    					}
    						
    					
    				}
    			} else {
    					
    				if ($questiontypeid == "30") {
    					$questiontypeid = "0";
    					$questionorder = "0";
    				}
    			}
    			
		    }	
		
		} // end foreach
		
		if ($edit == "Y") {
			if (! $define) {
				$define = "There were no changes.";
			}
			$Comment = "The following form was updated: <b>" . $AFformname . "</b><br>" . $define;
		} else {
			$Comment = "The following form was submited: <b>" . $AFformname . "</b><br>";
		}
		
		//Set the values those are going to update
		$set_info =   array("LastUpdated = NOW()", "Status = 3");
		//Bind the parameters
		$params   =   array(':OrgID'=>$OrgID, ':ApplicationID'=>$ApplicationID, ':RequestID'=>$RequestID, ':UniqueID'=>$AgreementFormID);
		//Set condition
		$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = :UniqueID");
		//Update the internal forms assigned
		$FormsInternalObj->updInternalFormsAssigned($set_info, $where, array($params));
		
		if ($AFformpart == "Primary") {
			//Bind parameters
			$params      =   array(':OrgID'=>$OrgID, ':LinkedTo'=>$AgreementFormID);
			//Set condition
			$where       =   array("OrgID = :OrgID", "LinkedTo = :LinkedTo");
			//Get Agreement Forms Information
			$resultsAF   =   $FormsInternalObj->getAgreementFormsInfo("AgreementFormID", $where, "", array($params));
		
			if($resultsAF['results']) {
				foreach ( $resultsAF['results'] as $AF ) {
		
					//Set the values those are going to update
					$set_info = array("LastUpdated = NOW()", "AssignedDate = NOW()", "DueDate = DATE_ADD(NOW(), INTERVAL 1 day)", "Status = 1");
					//Bind the parameters
					$params = array(':OrgID'=>$OrgID, ':ApplicationID'=>$ApplicationID, ':RequestID'=>$RequestID, ':UniqueID'=>$AF ['AgreementFormID']);
					//Set condition
					$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = :UniqueID");
					//Update the internal forms assigned
					$FormsInternalObj->updInternalFormsAssigned($set_info, $where, array($params));
				} // end foreach
			}
		
		} // end AFformpart
		
		if($define == "There were no changes") {
		    $define   =   "";
		}
		
		// History
		$ins_info 	= 	array (
                                "OrgID"             =>  $OrgID, 
                                "ApplicationID"     =>  $ApplicationID, 
                                "RequestID"         =>  $RequestID,
                                "InternalFormID"    =>  $AgreementFormID, 
                                "Date"              =>  "NOW()", 
                                "UserID"            =>  $USERID,
                                "Comments"          =>  $Comment,
                                "UpdatedFields"     =>  $define		                          
							);
		//Insert Internal Form History
		$FormsInternalObj->insInternalFormHistory($ins_info);
		
		
		if ($edit == "Y") {
			$MESSAGE = "The form " . $AFformname . " has been updated.";
		} else {
			$MESSAGE = "The form " . $AFformname . " has been submitted.";
		}
		
		if(FROM_SRC == "USERPORTAL")
		{
		    //Assigned Forms List
		    $assigned_forms_list    =   G::Obj('FormsInternal')->getAssignedFormsInfo($OrgID, $RequestID, $ApplicationID, $_REQUEST['ProcessOrder']);
		    
		    $link                   =   "";
		    $link_hit_status        =   false;
		    
		    for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
		        
		        $assigned_forms_info    =   $assigned_forms_list[$afl];
		        
		        foreach($assigned_forms_info as $FormType=>$IFA) {
		            
		            if($FormType == "External Link") {
		                echo '<span style="color:red">External Link: </span>';
		            }
		            else if($FormType == "Attachment") {
		                echo '<span style="color:red">Attachment: </span>';
		            }
		            else if($FormType == "HTML Page") {
		                echo '<span style="color:red">HTML Page: </span>';
		            }
		            
		            if($IFA["Status"] == "Pending" 
		                && $FormType != "Attachment"
		                && $FormType != "External Link"
		                && $FormType != "HTML Page") {
		                    
		                $form_status        =   $IFA["IFAInfo"]["Status"];
		                $form_status_text   =   $IFA["FormStatusText"];
		                $link               =   $IFA["Link"];
		                
		                $afl                =   count($assigned_forms_list);
		                
		                $link_hit_status    =   true;
		            }
		            
		            if($link_hit_status == true) {
		                break;
		            }
		        }
		    }
		    
	        echo '<script language="JavaScript" type="text/javascript">' . "\n";
	        $redirect_link = 'window.location = "' . USERPORTAL_HOME . 'assignedInternalForms.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&ProcessOrder=' . $_GET ['ProcessOrder'] . '&navpg=status&navsubpg=view&msg=form_suc';
	        if($link != "") {
	            $redirect_link .=  '&redirect_url='.urlencode($link);
	        }
	        $redirect_link .=  '"';
	        echo $redirect_link;
	        echo '</script>' . "\n\n";
	        
		}
		else {
			$complete_status = "success";
		}
		
		echo '<br><br><br><br><br><br>';
	}
	
}

$msg_color = 'red';
$style_attr = 'color:'.$msg_color.';';
if($complete_status == 'success') {
	$msg_color = '#428bca';
	$style_attr = 'color:'.$msg_color.';text-align:center';
}

if(isset($MESSAGE) && $MESSAGE != "") echo "<div style='".$style_attr."'>".$MESSAGE."</div>";

if($complete_status != "success") {
	$Submit = "Submit Form";
	
	$action = "completeAgreementForm.php?ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID . "&AgreementFormID=" . $AgreementFormID . "&HoldID=" . $HoldID;
	echo '<form method="post" name="frmCompleteAgreementForm" id="frmCompleteAgreementForm" action="'.$action.'">';
	
	list ( $APPDATA, $APPDATAREQ, $HoldID ) = fillAgreementFormData ( $USERID, $HoldID, $ApplicationID, $RequestID, $AgreementFormID );
	
	echo displayAgreementForm ( $AgreementFormID, $APPDATA, $APPDATAREQ );
	
	echo '<br><br>';
	echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
	echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
	echo '<input type="hidden" name="ProcessOrder" value="' . $ProcessOrder . '">';
	echo '<input type="hidden" name="AgreementFormID" value="' . $AgreementFormID . '">';
	echo '<input type="hidden" name="HoldID" value="' . $HoldID . '">';
	if ($APPDATA) {
		$Submit = "Edit Form";
		echo '<input type="hidden" name="edit" value="Y">';
	}
	echo '<input type="hidden" name="process" value="Y">';
	if ($ApplicationID) {
		if (($viewonly != "y") && ($_REQUEST['viewonly'] != "y")) {
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
			&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				echo '<input type="button" value="' . $Submit . '" onclick="processAgreementForm()">';
			}
			else {
				echo '<input type="submit" value="' . $Submit . '">';
			}
		}
	}

	echo '</form>';
	
	echo '<br><br>';
}
?>
