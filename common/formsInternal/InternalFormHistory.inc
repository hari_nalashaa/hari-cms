<?php
if (($OrgID) && ($ApplicationID) && ($RequestID)) {
	
	echo '<b class="title">History for form: ' . $FormName . '</b><br><br>';
	
	if (!preg_match ( '/printIconnectFormsHistory.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	    echo '<a href="printIconnectFormsHistory.php?ApplicationID='.$ApplicationID.'&RequestID='.$RequestID.'&InternalFormID='.$InternalFormID.'&FormName='.$FormName.'&display_app_header=no" target="_blank"><img src="'.IRECRUIT_HOME.'images/icons/printer.png">&nbsp;Print</a><br><br>';
	}
	
	echo printHistory ( $OrgID, $ApplicationID, $RequestID, $InternalFormID );
} // end if ApplicationID, OrgID, RequestID
  
// ** FUNCTIONS ** //
function printHistory($OrgID, $ApplicationID, $RequestID, $InternalFormID) {
	global $permit, $FormsInternalObj, $IrecruitUsersObj, $UserPortalUsersObj;

	?>
	<script>
	var page_url = window.location.href;
	function deleteInternalFormHistory(href_obj, delete_link, inter_form_name) {

		request_url = "formsInternal/";
		if(page_url.indexOf("formsInternal/") > 1) request_url = "";
		
		if(confirm('Are you sure you want to remove the following history entry? ' + inter_form_name)) {
			var tr_obj = $(href_obj).parents('tr').remove();
			$("#del_int_form_his_status").html('Please wait processing...');
			
			$.ajax({
				method: "POST",
		  		url: request_url+delete_link,
				type: "GET",
				success: function(data) {
					$("#del_int_form_his_status").html(data);
		    	}
			});
			
		}
	}
	</script>
	<?php
	echo '<table border="0" cellspacing="3" cellpadding="5" width="100%">';
	
	// Set condition
	$where = array (
			"OrgID 			= :OrgID",
			"ApplicationID 	= :ApplicationID",
			"RequestID 		= :RequestID",
			"InternalFormID = :InternalFormID" 
	);
	// Set parameters
	$params = array (
			":OrgID" 			=> $OrgID,
			":ApplicationID" 	=> $ApplicationID,
			":RequestID" 		=> $RequestID,
			":InternalFormID" 	=> $InternalFormID 
	);
	
	// Get Internal Form History Information
	$rslt = $FormsInternalObj->getInternalFormHistoryInfo ( "*", $where, "Date DESC", array (
			$params 
	) );
	
	echo '<tr><td colspan="4"><div id="del_int_form_his_status"></div></td></tr>';
	
	echo '<tr>';
	echo '<td width="180"><strong>Date</strong></td>';
	echo '<td width="80"><strong>User</strong></td>';
	echo '<td><strong>Comments</strong></td>';
	echo '</tr>';
	
	$rowcolor = "#eeeeee";
	
	if (is_array ( $rslt ['results'] )) {
		foreach ( $rslt ['results'] as $IFH ) {
			
			echo '<tr bgcolor="' . $rowcolor . '">';
			
			echo '<td valign="top">';
			echo $IFH ['Date'] . " EST";
			echo '</td>';
			
			echo '<td valign="top">';
			
			if (is_numeric ( $IFH ['UserID'] )) {
				$columns = "CONCAT('PORTAL:<br>',LastName,', ',FirstName,' ',Email) as user_name";
				$where = array (
						"OrgID 		= :OrgID",
						"UserID 	= :UserID" 
				);
				$params = array (
						":OrgID" 	=> $OrgID,
						":UserID" 	=> $IFH ['UserID'] 
				);
				
				$results = $UserPortalUsersObj->getUsersInformation ( $columns, $where, "", "", array (
						$params 
				) );
			} else { // else is_numeric
				$columns = "CONCAT('iRecruit:<br>',FirstName,' ',LastName) as user_name";
				$where = array ("OrgID = :OrgID", "UserID = :UserID");
				$params = array (":OrgID" => $OrgID, ":UserID" => $IFH ['UserID']);
				
				$results = $IrecruitUsersObj->getUserInformation ( $columns, $where, "", array ($params) );
			} // end is_numeric
			
			list ( $username ) = array_values ( $results ['results'] [0] );
			
			echo $username;
			echo '</td>';
			echo '<td valign="top">';
			echo $IFH ['Comments'];
			echo '</td>';
			echo '</tr>';
			
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		}
	}
	
	echo '</table>';
} // end of function
?>
