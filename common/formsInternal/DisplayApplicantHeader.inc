<?php
function displayHeader($ApplicationID, $RequestID, $is_echo = "Yes") {
	global $OrgID, $PreFilledFormID;
	
	$APPDATA = array ();
	$APPDATA = G::Obj('Applicants')->getAppData ( $OrgID, $ApplicationID );
	
	// set header and application information
	if ($ApplicationID != "") {
		//Set condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
		
		$results = G::Obj('Applications')->getJobApplicationsInfo(array("RequestID", "EntryDate","ReceivedDate"), $where, '', '', array($params));
	} else {
		//Bind the parameters
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		//Set the condition
		$where = array("OrgID = :OrgID", "RequestID = :RequestID");
		
		$results = G::Obj('Requisitions')->getRequisitionInformation("RequestID", $where, "", "", array($params));
	} // end ApplicationID
	
	$header_info = '';
	$header_info .= '<div class="row">';
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $JA) {		    
		    $multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $JA ['RequestID']);
			if (FROM_SRC == "IRECRUIT") {
				if ($feature ['MultiOrg'] == "Y") {
					echo 'Organization: <b>' . G::Obj('OrganizationDetails')->getOrganizationNameByRequestID ( $OrgID, $JA ['RequestID'] ) . "</b><br>";
				} // end feature
			} // end FROM
		
			$header_info .= '<div class="col-lg-6 col-md-6 col-sm-6" style="width:40%;float:left">';
			$header_info .= 'Position: ';
			$header_info .= '<b>(' . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . ')</b>&nbsp;&nbsp;';
			$header_info .= '<b>' . G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . '</b>';
			$header_info .= '<br>';
			
			if ($ApplicationID != "") {

			    	$header_info .= 'Application ID: ';
				$header_info .= '<b>' . $ApplicationID . '</b>';
				$header_info .= '<br>';
				
				$header_info .= 'Application Date: ';
				$header_info .= '<b>' . date("m/d//Y", strtotime( $JA ['EntryDate'])) . '</b>';
				$header_info .= '<br>';

				$FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );
				$OnboardFormID  =   $FEATURES['DataManagerType'];

				G::Obj('GenericQueries')->conn_string =   "IRECRUIT";

				$query = "SELECT Answer FROM OnboardData WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND ApplicationID = :ApplicationID AND QuestionID = 'Sage_HireDate'";
  				$params     =   array(':OrgID'    =>  $OrgID, ':OnboardFormID'    =>  $OnboardFormID, ':ApplicationID'    =>  $ApplicationID);
  				$OD =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));


				if ($OD[0]['Answer'] != "") {
                                  $header_info .= 'Application Target Start Date: ';
				  $header_info .= '<b>' . print_r($OD[0]['Answer'],true) . '</b>';
				  $header_info .= '<br>';
				}


			} // end ApplicationID
			
			$header_info .= '</div>';
		}
	}

	$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);
	
	// use I9 information if available
	//Set where condition
	$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID");
	$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID,":PreFilledFormID"=>$CURRENT['I9']);
	
	//Get the prefilled form data
	$resultsI9 = G::Obj('FormData')->getPreFilledFormData("*", $where, "", "QuestionOrder", array($params));
	$I9cnt = $resultsI9['count'];
	
	if ($I9cnt > 0) {
		
		foreach($resultsI9['results'] as $PFFD) {
			
			if (($PFFD ['QuestionID'] == "FirstName") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['first'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "MiddleName") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['middle'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "LastName") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['last'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "Address1") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['address'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "Address2") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['address2'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "City") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['city'] = $PFFD ['Answer'];
				$APPDATA ['county'] = "";
			}
			if (($PFFD ['QuestionID'] == "State") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['state'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "ZipCode") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['zip'] = $PFFD ['Answer'];
			}
			$APPDATA ['country'] = "US";
			
		} // end while
			  
	} // end I9cnt

	
	$header_info .= '<div class="col-lg-6 col-md-6 col-sm-6" style="width:40%;float:left;">';

	$header_info .= '<b>' . $APPDATA ["first"] . ' ';
	$header_info .= $APPDATA ["middle"] . ' ';
	$header_info .= $APPDATA ["last"];
	$header_info .= '</b>';
	$header_info .= '<br>';

	$header_info .= $APPDATA ["address"];

	if (trim ( $APPDATA ["address2"] )) {
	    if($APPDATA ["address"] != "") $header_info .= ", ";
		$header_info .= $APPDATA ["address2"];
	}

	$header_info .= '<br>';
	$address_info = G::Obj('Address')->formatAddress ( $OrgID, $APPDATA ["country"], $APPDATA ["city"], $APPDATA ["state"], $APPDATA ["province"], $APPDATA ["zip"], $APPDATA ["county"] );

	if($address_info != "") {
	    $header_info .= $address_info;
	    $header_info .= ", ";
	}
	
	$header_info .= $APPDATA ["country"];

	$header_info .= '</div>';
	
	$header_info .= '</div>';

	$header_info .= '<div style="clear:both;height:20px"></div>';
	
	if($is_echo == "No") {
		return $header_info;
	}
	else {
		echo $header_info;
	}
} // end function
?>
