<?php
function fillPreFilledFormData($USERID, $HoldID, $ApplicationID, $PreFilledFormID) {
	global $OrgID, $MysqlHelperObj;
	
    $APPDATA        =   array ();
    $APPDATAREQ     =   array ();
	
	// Start PreFill awareness
	if ((FROM_SRC == "USERPORTAL") && ($USERID) && ($PreFilledFormID)) {
		
        $ap         =   "";
        $columns    =   "JA.ApplicationID";
        $where      =   array (
                            "JA.OrgID           =   :OrgID",
                            "JA.OrgID           =   AD.OrgID",
                            "JA.ApplicationID   =   AD.ApplicationID",
                            "AD.QuestionID      =   'PortalUserID'",
                            "AD.Answer          =   :Answer",
                            "JA.ApplicationID  !=   :ApplicationID" 
                        );
        $params     =   array (
                            ":OrgID"            =>  $OrgID,
                            ":Answer"           =>  $USERID,
                            ":ApplicationID"    =>  $ApplicationID 
                        );
        $results    =   G::Obj('Applications')->getJobApplicationsAndApplicantData ( $columns, $where, "JA.ApplicationID", array ($params) );
		
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $AP ) {
				$ap .= "'" . $AP ['ApplicationID'] . "',";
			} // end foreach
		}
		
        $ap         =   substr ( $ap, 0, - 1 );
		
		if ($ap) {

            $ifa        =   "";
			
            $columns    =   "ApplicationID, RequestID, UniqueID";
            $where      =   array (
                                "OrgID      =   :OrgID",
                                "UniqueID   =   :UniqueID",
                                "Status     >=  3",
                                "ApplicationID IN ($ap)" 
                            );
            $params     =   array (
                        		":OrgID"      =>  $OrgID,
                        		":UniqueID"   =>  $PreFilledFormID 
                            );
			
            $results    =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo ( $columns, $where, "", "LastUpdated DESC LIMIT 1", array ($params) );
			
			if(is_array($results ['results'] [0])) {
				list ( $AppID, $ReqID, $PFFID ) = array_values ( $results ['results'] [0] );
			}
			
			if (($AppID) && ($ReqID) && ($PFFID)) {
				
				// Set condition
				$where = array (
						"OrgID            =   :OrgID",
						"ApplicationID    =   :ApplicationID",
						"RequestID        =   :RequestID",
						"PreFilledFormID  =   :PreFilledFormID" 
				);
				// Set parameters
				$params = array (
                        ":OrgID"            =>  $OrgID,
                        ":ApplicationID"    =>  $AppID,
                        ":RequestID"        =>  $ReqID,
                        ":PreFilledFormID"  =>  $PFFID 
				);
				
				$results = G::Obj('FormData')->getPreFilledFormData ( "QuestionID, Answer", $where, "", "", array (
						$params 
				) );
				if (is_array ( $results ['results'] )) {
					foreach ( $results ['results'] as $WFD ) {
						$APPDATA [$WFD ['QuestionID']] = $WFD ['Answer'];
					} // end foreach
				}
			} // end AppID ReqID PFFID
		} // end ap
	} // end FROM
	// End PreFill portion

	if ($HoldID) {
	    
		$results = G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo ( $OrgID, $HoldID );
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $row ) {
				$APPDATA [$row ['QuestionID']] = $row ['Answer'];
				$APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
			}
		}
		
	} else { // end HoldID
	    
		$where = array (
				"OrgID              =   :OrgID",
				"ApplicationID      =   :ApplicationID",
				"RequestID          =   :RequestID",
				"PreFilledFormID    =   :PreFilledFormID" 
		);
		$params = array (
				":OrgID"            =>  $OrgID,
				":ApplicationID"    =>  $App2licationID,
				":RequestID"        =>  $RequestID,
				":PreFilledFormID"  =>  $PreFilledFormID 
		);
		
		$results  =   G::Obj('FormData')->getPreFilledFormData ( "*", $where, "", "", array ($params) );
		$editcnt  =   $results ['count'];
		
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $WFD ) {
				$APPDATA [$WFD ['QuestionID']] = $WFD ['Answer'];
			}
		}
		
		$HLD      =   $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
		$HoldID   =   uniqid ( $HLD );
	} // end HoldID
	
	return array (
			$APPDATA,
			$APPDATAREQ,
			$HoldID 
	);
} // end function

function displayPreFilledWebForm($PreFilledFormID, $APPDATA, $APPDATAREQ) {
	global $OrgID, $TemplateObj, $COLOR, $ApplicationID, $RequestID;
	
	if(is_object($TemplateObj)) {
		$COLOR = $TemplateObj->COLOR;
	}
	
	$highlight =   '#' . $COLOR ['ErrorHighlight'];
	$formtable =   "PreFilledFormQuestions";
	$colwidth  =   "280";
	
	$infm      =   "";
	
	$columns   =   "TypeForm, FormName, Form";
	$params    =   array (":PreFilledFormID" => $PreFilledFormID);
	$where     =   array ("OrgID = 'MASTER'", "PreFilledFormID = :PreFilledFormID");
	$results   =   G::Obj('FormsInternal')->getPrefilledFormsInfo ( $columns, $where, "", array ($params) );
	
	list ( $TypeForm, $FormName, $Form ) = array_values ( $results ['results'] [0] );
	
	$infm     .=   '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
	$infm     .=   '<label class="question_name"><strong>';
	$infm     .=   'Form: ';
	$infm     .=   $TypeForm . ': ' . $FormName;
	$infm     .=   '</strong></label>';
	$infm     .=   '</div></div>';
	
	$infm     .=   '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
	$infm     .=   '<label class="question_name">Please refer to the PDF instructions for filling out this form.';
	
	$link = IRECRUIT_HOME . "formsInternal/display_prefilledform.php?OrgID=" . $OrgID . "&PreFilledFormID=" . $PreFilledFormID;
	if ($ApplicationID != "") {
		$link .= '&ApplicationID=' . $ApplicationID;
	}

	$infm     .=   '<a href="' . $link . '" target="_blank">';
	$infm     .=   '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_acrobat.png" border="0" title="View PDF Form" style="margin:0px 8px -4px 3px;">';
	$infm     .=   '</a></label>';
	$infm     .=   '</div></div>';
	
	// load Applicant Data
	if ($PreFilledFormID == "FE-I9m") {
		$infm .= I9Data ("FE-I9");
	} // end PreFilledFormID
	if ($PreFilledFormID == "FE-I9m-2020") {
		$infm .= I9Data ("FE-I9-2020");
	} // end PreFilledFormID
	  
	// Set the columns
	$columns = "QuestionID, QuestionTypeID, Question";
	// Set the condition
	$where = array (
			"OrgID               =   :OrgID",
			"PreFilledFormID     =   :PreFilledFormID",
			"Active              =   'Y'" 
	);
	// Set the parameters
	$params = array (
			":OrgID"             =>  'MASTER',
			":PreFilledFormID"   =>  $PreFilledFormID 
	);
	// Get the questions information
	$resultsInt = G::Obj('FormQuestions')->getQuestionsInformation ( $formtable, $columns, $where, "QuestionOrder", array ($params) );
	
	$holdOrgID = $OrgID;
	
	if (is_array ( $resultsInt ['results'] )) {
		foreach ( $resultsInt ['results'] as $WFQ ) {
			
			$QuestionID = $WFQ ['QuestionID'];
			
			if ($WFQ ['QuestionTypeID'] == 50) { // Vault
				
				$columns    =   "FileName, FileType, Date, UserID";
				$where      =   array (
                                    "OrgID          =   :OrgID",
                                    "ApplicationID  =   :ApplicationID",
                                    "RequestID      =   :RequestID" 
                                );
				$params     =   array (
            						":OrgID"          =>  $OrgID,
            						":ApplicationID"  =>  $ApplicationID,
            						":RequestID"      =>  $RequestID 
                				);
				$results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( $columns, $where, '', array ($params) );
				$vaultnum   =   $results ['count'];
				
				if ($vaultnum == 0) {
					$vaulttask = "Add to Vault";
				} else {
					$vaulttask = "View Vault";
				}
				
				if(FROM_SRC == 'IRECRUIT') {
					$viewvault = '<a href="' . IRECRUIT_HOME . 'applicants/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&PreFilledFormID=' . $PreFilledFormID;
				}
				else if(FROM_SRC == 'USERPORTAL') {
					$viewvault = '<a href="' . USERPORTAL_HOME . 'formsInternal/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&PreFilledFormID=' . $PreFilledFormID;
				}
				
				if ($AccessCode != "") {
					$viewvault .= "&k=" . $AccessCode;
				}
				$viewvault .= '" target="_blank">';
				$viewvault .= $vaulttask . '&nbsp;&nbsp;';
				$viewvault .= '<img src="' . IRECRUIT_HOME . 'images/icons/briefcase.png" border="0" title="' . $vaulttask . '" style="margin:0px 3px -4px 0px;">';
				$viewvault .= '</a>';
				
				$infm   .=  '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><label class="question_name">';
				$infm   .=  $WFQ ['Question'] . '&nbsp;&nbsp;&nbsp;&nbsp;' . $viewvault . ' (' . $vaultnum . ')<br>';
				$infm   .=  '</label></div></div>';
			} else if ($WFQ ['QuestionTypeID'] == 30) { // Signature
				
				$infm .= Signature ( $QuestionID );
			} else {

				$OrgID = "MASTER";
				$infm .= include COMMON_DIR . 'application/DisplayQuestions.inc';
				$OrgID = $holdOrgID;
			}
		} // end foreach
	}
	
	return $infm;
} // end function

function I9Data($PreFilledFormID) {
    global $OrgID, $USERID, $ApplicationID, $RequestID;

	//Set default prefilled audit log information
	$audit_app_log     =   array(
                        	    'OrgID'            =>  $OrgID,
                        	    'UserID'           =>  $USERID,
                        	    'Action'           =>  'View',
                        	    'FormType'         =>  'PreFilledForm',
                                'FormID'           =>  $PreFilledFormID,
                        	    'PageInfo'         =>  $_SERVER['REQUEST_URI'],
                        	    'CreatedDateTime'  =>  "NOW()"
                        	);
	
	// Set the parameters
	$params = array (
			":OrgID"            =>  $OrgID,
			":ApplicationID"    =>  $ApplicationID,
			":RequestID"        =>  $RequestID,
			":PreFilledFormID"  =>  $PreFilledFormID
	);
	// Set the condition
	$where = array (
			"OrgID           =   :OrgID",
			"ApplicationID   =   :ApplicationID",
			"RequestID       =   :RequestID",
			"PreFilledFormID =   :PreFilledFormID" 
	);
	
	$resultsI9 = G::Obj('FormData')->getPreFilledFormData ( "*", $where, "", "QuestionOrder", array (
			$params 
	) );
	$I9cnt = $resultsI9 ['count'];
	
	if ($I9cnt > 0) {
		foreach ( $resultsI9 ['results'] as $PFFD ) {
			
			if (($PFFD ['QuestionID'] == "FirstName") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['first'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "MiddleName") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['middle'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "LastName") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['last'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "Address1") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['address'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "Address2") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['address2'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "City") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['city'] = $PFFD ['Answer'];
				$APPDATA ['county'] = "";
			}
			if (($PFFD ['QuestionID'] == "State") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['state'] = $PFFD ['Answer'];
			}
			if (($PFFD ['QuestionID'] == "ZipCode") && ($PFFD ['Answer'] != "")) {
				$APPDATA ['zip'] = $PFFD ['Answer'];
			}
			$APPDATA ['country'] = "US";
			
			if (($PFFD ['QuestionID'] == "OtherNamesUsed") && ($PFFD ['Answer'] != "")) {
				$OtherNamesUsed = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "EmailAddress") && ($PFFD ['Answer'] != "")) {
				$EmailAddress = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "TelephoneNumber") && ($PFFD ['Answer'] != "")) {
				$TelephoneNumber = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "DOB") && ($PFFD ['Answer'] != "")) {
				$DOB = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "AlienExpDate") && ($PFFD ['Answer'] != "")) {
				$alienexpdate = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "AlienNumber") && ($PFFD ['Answer'] != "")) {
				$aliennumber = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "FormI94") && ($PFFD ['Answer'] != "")) {
				$formI94 = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "ForeignPassport") && ($PFFD ['Answer'] != "")) {
				$foreignpassport = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "PassportCountry") && ($PFFD ['Answer'] != "")) {
				$passportcountry = $PFFD ['Answer'];
			}
			
			if (($PFFD ['QuestionID'] == "Status") && ($PFFD ['Answer'] != "")) {
				$value = explode ( '::', $PFFD ['value'] );
				foreach ( $value as $selection ) {
					
					$itm = explode ( ':', $selection );
					if ($itm [0] == $PFFD ['Answer']) {
						$status = $itm [1];
					}
				}
			}
			
			if (($PFFD ['QuestionID'] == "SSN") && ($PFFD ['Answer'] != "")) {
                $Answer15   =   ($PFFD ['Answer'] != "") ? json_decode($PFFD ['Answer'], true) : "";
                $ssn1       =   $Answer15[0];
                $ssn2       =   $Answer15[1];
                $ssn3       =   $Answer15[2];

			}
				
		} // end while
		
        $PFFD ['Answer']    =   $SSN    =   $ssn1 . '-' . $ssn2 . '-' . $ssn3;
		
		$infm     .=  '<div class="form-group row">';
		$infm     .=  '<div class="col-lg-12 col-md-12 col-sm-12">';
		$infm     .=  'Status: <b>' . $status . '</b>';
		$infm     .=  '</div></div>';
		
		if ($alienexpdate != "") {
		    $infm     .=  '<div class="form-group row">';
		    $infm     .=  '<div class="col-lg-12 col-md-12 col-sm-12">';
			$infm     .=  'Alien Expiration Date: <b>' . $alienexpdate . '</b>';
			$infm     .=  '</div></div>';
		}
		if ($aliennumber != "") {
		    $infm     .=  '<div class="form-group row">';
		    $infm     .=  '<div class="col-lg-12 col-md-12 col-sm-12">';
			$infm     .=  'Alien Registration/USCIS Number: <b>' . $aliennumber . '</b>';
			$infm     .=  '</div></div>';
		}
		if ($formI94 != "") {
		    $infm     .=  '<div class="form-group row">';
		    $infm     .=  '<div class="col-lg-12 col-md-12 col-sm-12">';
			$infm     .=  'Form I-94 Admission Number: <b>' . $formI94 . '</b>';
			$infm     .=  '</div></div>';
		}
		if ($foreignpassport != "") {
		    $infm     .=  '<div class="form-group row">';
		    $infm     .=  '<div class="col-lg-12 col-md-12 col-sm-12">';
			$infm     .=  'Foreign Passport Number: <b>' . $foreignpassport . '</b>';
			$infm     .=  '</div></div>';
		}
		if ($passportcountry != "") {
		    $infm     .=  '<div class="form-group row">';
		    $infm     .=  '<div class="col-lg-12 col-md-12 col-sm-12">';
			$infm     .=  'Country of Issuance: <b>' . $passportcountry . '</b><br>';
			$infm     .=  '</div></div>';
		}
		
		$infm     .=  '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
		$infm     .=  'Other Names Used: <b>' . $OtherNamesUsed . '</b>';
		$infm     .=  '</div></div>';
		
		$infm     .=  '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
		$infm     .=  'Date of Birth: <b>' . $DOB . '</b>';
		$infm     .=  '</div></div>';
		
		$infm     .=  '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
		$infm     .=  'Social Security No: <b>' . $SSN . '</b>';
		$infm     .=  '</div></div>';

		$infm     .=  '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
		$infm     .=  'Email Address: <b>' . $EmailAddress . '</b>';
		$infm     .=  '</div></div>';
		
		$infm     .=  '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
		$infm     .=  'Telephone Number: <b>' . $TelephoneNumber . '</b>';
		$infm     .=  '</div></div>';
		
		$infm     .=  '<div style="clear:both;height:20px;"></div>';
	} else { // else I9cnt
		
		$infm .= '<div class="form-group row">';
		$infm .= '<div class="col-lg-12 col-md-12 col-sm-12">';
		$infm .= '<label class="question_name">Applicant has not filled out their portion of this form.</label>';
		$infm .= '</div>';
		$infm .= '</div>';
	} // end I9cnt
	
	return $infm;
} // end function
?>
