<style>
#question_space {
     padding: 0px 0px 3px 0px;
}
@media(max-width:768px) {
	 #question_space {
         padding: 0px 0px 3px 0px;
         border-bottom: 1px solid #f5f5f5 !important;
	}
}
</style>
<?php 
if($ServerInformationObj->getRequestSource() != 'ajax' && FROM_SRC == 'IRECRUIT') {
	?>
	<style>
    .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    </style>
	<?php
}

require_once COMMON_DIR . 'formsInternal/PrintPrefilledFormView.inc';
require_once COMMON_DIR . 'formsInternal/DisplayPreFilledForm.inc';

if (($OrgID) && ($ApplicationID) && ($RequestID)) {
    
    //Set default prefilled audit log information
    $audit_app_log  =   array(
                                'OrgID'            =>  $OrgID,
                                'UserID'           =>  $USERID,
                                'Action'           =>  'View',
                                'FormType'         =>  'PreFilledForm',
                                'FormID'           =>  $PreFilledFormID,
                                'PageInfo'         =>  $_SERVER['REQUEST_URI'],
                                'CreatedDateTime'  =>  "NOW()"
                            );
    
	//Set condition
	$where = array("OrgID = :OrgID", "PreFilledFormID = :PreFilledFormID");
	//Set parameters
	$params = array(":OrgID"=>'MASTER', ":PreFilledFormID"=>$PreFilledFormID);
	//Get Prefilled Forms
	$results = $FormsInternalObj->getPrefilledFormsInfo(array("TypeForm", "FormName"), $where, "", array($params));
	
	list ( $TypeForm, $FormName ) = array_values($results['results'][0]);

	if(isset($displayFormHeader)) {
	    echo $displayFormHeader;
	
	    echo '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12"><hr><br></div></div>';
	}
	
	echo '<div class="row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	echo '<strong>Form: ' . $TypeForm . ': ' . $FormName . '</strong>';
	echo '</div>';
	echo '</div>';
	
	echo '&nbsp;&nbsp;';
	
	if(FROM_SRC == 'USERPORTAL') {
		$link = USERPORTAL_HOME . "formsInternal/display_prefilledform.php?OrgID=" . $OrgID . "&PreFilledFormID=" . $PreFilledFormID;
	}
	else if(FROM_SRC == 'IRECRUIT') {
		$link = IRECRUIT_HOME . "formsInternal/display_prefilledform.php?OrgID=" . $OrgID . "&PreFilledFormID=" . $PreFilledFormID;
	}
	
	if ($ApplicationID != "") {
		$link .= '&ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID;
		$print_view = IRECRUIT_HOME . "formsInternal/printPreFilledForm.php?&ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID . "&PreFilledFormID=" . $PreFilledFormID;
	}
	
	echo '<a href="' . $link . '" target="_blank">';
	echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_acrobat.png" border="0" title="View PDF Form" style="margin:0px 8px -4px 3px;">';
	echo '</a>';
	
	if(FROM_SRC == 'IRECRUIT' && end(explode("/", $_SERVER['SCRIPT_FILENAME'])) == "viewInternalForm.php") {
		echo '<a href="' . $print_view . '" target="_blank" style="margin:0px 8px -4px 3px;">';
		echo '<img src="'.IRECRUIT_HOME.'images/icons/printer.png" title="Printable" border="0"> Print';
		echo '</a>';
	}
	echo '<br><br>' . "\n";
	
	$PreFilledFormQueAns  =    $PreFilledFormQueAnsObj->getPreFilledFormQueAnswersInfo ( $OrgID, $ApplicationID, $RequestID, $PreFilledFormID );
	
	if ($PreFilledFormID == "FE-I9m") {
	    echo I9Data ("FE-I9");
	} // end PreFilledFormID FE-I9m
	if ($PreFilledFormID == "FE-I9m-2020") {
	    echo I9Data ("FE-I9-2020");
	} // end PreFilledFormID FE-I9m

	foreach ($PreFilledFormQueAns as $QuestionID=>$QuestionAnswerInfo)
	{
	    echo "<div class='row' style='margin-bottom:3px;'>";
	
	    if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
	
	        echo "<div class='col-lg-3 col-md-3 col-sm-2'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	
	        $canswer = $QuestionAnswerInfo['Answer'];
	
	        $rtn  = '';
	        if(is_array($canswer)) {
	            $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
	
	            foreach ($canswer as $cakey=>$caval) {
	                $rtn .= '<tr>';
	                $rtn .= '<td style="padding-right:5px !important">'.$cakey.'&nbsp;&nbsp;&nbsp;</td>';
	                foreach ($caval as $cavk=>$cavv) {
	                    $rtn .= '<td style="padding-right:5px !important">'.$cavv.'&nbsp;&nbsp;&nbsp;</td>';
	                }
	                $rtn .= '</tr>';
	            }
	
	            $rtn .= '</table>';
	        }
	
	        echo "<div class='col-lg-9 col-md-9 col-sm-10'><strong>".$rtn."</strong></div>";
	
	    }
	    else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
	
	        echo "<div class='col-lg-3 col-md-3 col-sm-2'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	
	        $canswer = $QuestionAnswerInfo['Answer'];
	
	        $rtn  = '';
	        if(is_array($canswer)) {
	
	            $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
	
	            $days_count = count($canswer['days']);
	
	            for($ci = 0; $ci < $days_count; $ci++)  {
	                $rtn .= '<tr>';
	                $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
	                $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
	                $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
	                $rtn .= '<td width="5%">to </td>';
	                $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
	                $rtn .= '</tr>';
	            }
	
	            $rtn .= '</table>';
	        }
	
	        echo "<div class='col-lg-9 col-md-9 col-sm-10'><strong>".$rtn."</strong></div>";
	    }
	    else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
	        echo "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	    }
	    else {
	        echo "<div class='col-lg-3 col-md-3 col-sm-2'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	        echo "<div class='col-lg-9 col-md-9 col-sm-10'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
	    }
	
	    echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	    echo "</div>";
	}
	
} // end if ApplicationID, OrgID, RequestID
?>
