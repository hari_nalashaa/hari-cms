<style>
#question_space {
     padding: 0px 0px 3px 0px;
}
@media(max-width:768px) {
	 #question_space {
         padding: 0px 0px 3px 0px;
         border-bottom: 1px solid #f5f5f5 !important;
	}
}
</style>
<?php 
if($ServerInformationObj->getRequestSource() != 'ajax' && FROM_SRC == 'IRECRUIT') {
	?>
	<style>
    .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    </style>
	<?php
}

if (($OrgID) && ($ApplicationID) && ($RequestID)) {

    //Set default agreement form audit log information
    $audit_app_log  =   array(
                            'OrgID'            =>  $OrgID,
                            'UserID'           =>  $USERID,
                            'Action'           =>  'View',
                            'FormType'         =>  'AgreementForm',
                            'FormID'           =>  $AgreementFormID,
                            'PageInfo'         =>  $_SERVER['REQUEST_URI'],
                            'CreatedDateTime'  =>  "NOW()"
                        );
    
	//Bind the parameters to query
	$params = array(':OrgID'=>$OrgID, ':AgreementFormID'=>$AgreementFormID);
	//Set the condition	
	$where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	//Set the agreement forms
	$results = $FormsInternalObj->getAgreementFormsInfo('FormName', $where, "", array($params));
	
	//Get Agreement Forms Information
	$FormName = $results['results'][0]['FormName'];
	
	if(isset($displayFormHeader)) {
	    echo $displayFormHeader;
	    
	    echo '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12"><hr><br></div></div>';
	}
	
	echo '<div class="row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	echo '<strong>Form: ' . $FormName . '</strong>';
	echo '</div>';
	echo '</div>';
	
	if(FROM_SRC == 'IRECRUIT' && end(explode("/", $_SERVER['SCRIPT_FILENAME'])) == "viewInternalForm.php") {
		$print_view = IRECRUIT_HOME . "formsInternal/printAgreementForm.php?ApplicationID=".$ApplicationID."&RequestID=".$RequestID."&AgreementFormID=".$AgreementFormID;
		
		echo '<div class="row">';
		echo '<div class="col-lg-12 col-md-12 col-sm-12">';
		echo "<a href='".$print_view."' target='_blank'>";
		echo 'Print <img src="'.IRECRUIT_HOME.'images/icons/printer.png" title="Printable" border="0">';
		echo "</a>";
		echo '<br><br>';
		echo '</div>';
		echo '</div>';
		
	}
	
	$AgreementFormQueAns  =    $AgreementFormQueAnsObj->getAgreementFormQueAnswersInfo ( $OrgID, $ApplicationID, $RequestID, $AgreementFormID );
	
	foreach ($AgreementFormQueAns as $QuestionID=>$QuestionAnswerInfo)
	{
	    echo "<div class='row'>";
	     
	    if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
	         
	        echo "<div class='col-lg-3 col-md-3 col-sm-3'>".$QuestionAnswerInfo['Question']."</div>";
	
	        $canswer = $QuestionAnswerInfo['Answer'];
	         
	        $rtn  = '';
	        if(is_array($canswer)) {
	            $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
	             
	            foreach ($canswer as $cakey=>$caval) {
	                $rtn .= '<tr>';
	                $rtn .= '<td style="padding-right:5px !important">'.$cakey.'</td>';
	                foreach ($caval as $cavk=>$cavv) {
	                    $rtn .= '<td style="padding-right:5px !important">'.$cavv.'</td>';
	                }
	                $rtn .= '</tr>';
	            }
	             
	            $rtn .= '</table>';
	        }
	
	        echo "<div class='col-lg-9 col-md-9 col-sm-9'><strong>".$rtn."</strong></div>";
	         
	    }
	    else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
	         
	        echo "<div class='col-lg-3 col-md-3 col-sm-3'>".$QuestionAnswerInfo['Question']."</div>";
	         
	        $canswer = $QuestionAnswerInfo['Answer'];
	         
	        $rtn  = '';
	        if(is_array($canswer)) {
	             
	            $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
	
	            $days_count = count($canswer['days']);
	
	            for($ci = 0; $ci < $days_count; $ci++)  {
	                $rtn .= '<tr>';
	                $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
	                $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
	                $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
	                $rtn .= '<td width="5%">to </td>';
	                $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
	                $rtn .= '</tr>';
	            }
	
	            $rtn .= '</table>';
	        }
	         
	        echo "<div class='col-lg-9 col-md-9 col-sm-9'><strong>".$rtn."</strong></div>";
	    }
	    else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
	        echo "<div class='col-lg-12 col-md-12 col-sm-12'><strong>".$QuestionAnswerInfo['Question']."</strong></div>";
	    }
	    else if($QuestionAnswerInfo['QuestionTypeID'] == 60) {
	        echo "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['Answer']."</div>";
	    }
	    else {
	        echo "<div class='col-lg-3 col-md-3 col-sm-3'>".$QuestionAnswerInfo['Question']."</div>";
	        echo "<div class='col-lg-9 col-md-9 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
	    }
	
	    echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	    
	    echo "</div>";
	}
} // end if ApplicationID, OrgID, RequestID
?>
