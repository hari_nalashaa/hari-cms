<?php
function fillAgreementFormData($USERID, $HoldID, $ApplicationID, $RequestID, $AgreementFormID) {
	global $OrgID, $FormsInternalObj, $MysqlHelperObj, $FormDataObj, $AddressObj;
	
	$APPDATA       =   array ();
	$APPDATAREQ    =   array ();
	
	if ($HoldID) {
		//Get Applicant Data Information
        $results    =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $row) {
				$APPDATA [$row ['QuestionID']] = $row ['Answer'];
				$APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
			} // end foreach
		}		
	} else { // else HoldID
		
		//Set the condition
		$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "AgreementFormID = :AgreementFormID");
		//Bind the parameters
		$params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":AgreementFormID"=>$AgreementFormID);
		//Get the Agreement Form Data Information
		$results  =   $FormDataObj->getAgreementFormData("*", $where, "", "", array($params));
		
		$editcnt  =   $results['count'];
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $WFD) {
				$APPDATA [$WFD ['QuestionID']] = $WFD ['Answer'];
			} // end foreach
		}
		
		//Set HoldID(Unique)
		$HLD      =   $MysqlHelperObj->getDateTime('%Y%m%d%H%m%s');
		$HoldID   =   uniqid ( $HLD );
		
	} // end HoldID
	
	return array (
			$APPDATA,
			$APPDATAREQ,
			$HoldID 
	);
} // end function

function displayAgreementForm($AgreementFormID, $APPDATA, $APPDATAREQ) {
	global $OrgID, $typeform, $HoldID, $TemplateObj, $COLOR, $ApplicationID, $RequestID, $FormsInternalObj, $FormQuestionsObj, $AddressObj;
	
	if(is_object($TemplateObj)) {
		$COLOR = $TemplateObj->COLOR;
	}
	
	$highlight =   '#' . $COLOR ['ErrorHighlight'];
	$formtable =   "AgreementFormQuestions";
	$colwidth  =   "300";
	
	$infm      =   "";
	
	//Set the parameters
	$params    =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID);
	//Set the condition
	$where     =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	//get agreement forms information
	$results   =   $FormsInternalObj->getAgreementFormsInfo("FormName", $where, "", array($params));
	$FormName  =   $results['results'][0]['FormName'];
	
	$infm     .=   '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12">';
	$infm     .=   '<label class="question_name"><strong>';
	$infm     .=   'Form: ';
	$infm     .=   $FormName;
	$infm     .=   '</strong></label>';
	$infm     .=   '</div></div>';
	
	
	//Set the condition
	$where     =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "Active = 'Y'");
	//Set the parameters
	$params    =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID);
	//Set the columns
	$columns   =   array("AgreementFormID", "QuestionID", "QuestionTypeID");
	
	//Get the question information
	$resultsInt = $FormQuestionsObj->getQuestionsInformation($formtable, $columns, $where, "QuestionOrder", array($params));
	
	if(is_array($resultsInt['results'])) {
		foreach ($resultsInt['results'] as $WFQ) {
			
			$AgreementFormID = $WFQ ['AgreementFormID'];
			$QuestionID      = $WFQ ['QuestionID'];
		
			$infm .= include COMMON_DIR . 'application/DisplayQuestions.inc';
		} // end foreach
	}
	
	return $infm;
} // end function

function agreementHistoryChanged($AgreementFormID, $ApplicationID, $RequestID) {
	global $OrgID, $FormsInternalObj, $FormDataObj, $GetFormPostAnswerObj, $AgreementFormQuestionsObj, $AddressObj;
	
	$rtn = 'The following fields changed:<br>';

	// Set where condition
	$where_info = array (
	    "OrgID 				= :OrgID",
	    "ApplicationID 		= :ApplicationID",
	    "RequestID 			= :RequestID",
	    "AgreementFormID    = :AgreementFormID"
	);
	// Bind the parameters for query
	$params_info = array (
	    ":OrgID" 			=> $OrgID,
	    ":ApplicationID"	=> $ApplicationID,
	    ":RequestID" 		=> $RequestID,
	    ":AgreementFormID"  => $AgreementFormID
	);

	//agreement form data results
	$agr_form_data_results = $FormDataObj->getAgreementFormData("*", $where_info, "", "", array($params_info));
	
	$agr_form_data_que_ids         =   array();
	$agr_form_data_info_history    =   array();
	if(is_array($agr_form_data_results['results'])) {
	    foreach($agr_form_data_results['results'] as $agr_form_que_info) {
	        $agr_form_data_info_history[$agr_form_que_info['QuestionID']]['Question'] 	= $agr_form_que_info['Question'];
	        $agr_form_data_info_history[$agr_form_que_info['QuestionID']]['Answer'] 	= $agr_form_que_info['Answer'];
	        $agr_form_data_que_ids[] = $agr_form_que_info['QuestionID'];
	    }
	}

	
	
	/**
	 * This is a temporary code until the final upgrade.
	 * Upto that time, we have to keep on adding different conditions to it.
	 */
	$GetFormPostAnswerObj->POST =   $_POST;    //Set Post Data
	
	$form_que_list         =   $AgreementFormQuestionsObj->getAgreementFormQuestionsList($OrgID, $AgreementFormID);
	
	$ques_to_skip          =   array();
	foreach($form_que_list as $QuestionID=>$QuestionInfo) {
	    
	    $QI                =   $QuestionInfo;

	    if($QI['QuestionTypeID'] == 13) {
	        //After the complete upgrade have to remove this.
	        $ques_to_skip[]        =   $QI['QuestionID']."1";
	        $ques_to_skip[]        =   $QI['QuestionID']."2";
	        $ques_to_skip[]        =   $QI['QuestionID']."3";
	         
	        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	        $QI['ApplicationID']   =   $ApplicationID;
	        $QI['RequestID']       =   $RequestID;
	        $QI['AnswerStatus']    =   1;
	
	        //Data for web form history
	        if(($agr_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
    	        && isset($agr_form_data_info_history[$QI['QuestionID']]['Question'])
    	        && $agr_form_data_info_history[$QI['QuestionID']]['Question'] != ""
	            && in_array($QI['QuestionID'], $agr_form_data_que_ids)) {
	            
	            $phone_ans_bef    =   $agr_form_data_info_history[$QI['QuestionID']]['Answer'];
	            $app_ans_bef      =   ($phone_ans_bef != "") ? json_decode($phone_ans_bef, true) : "";
	            $app_ans_bef_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], '' );
	            
	            $phone_ans_aft    =   $QI['Answer'];
	            $app_ans_aft      =   ($phone_ans_aft != "") ? json_decode($phone_ans_aft, true) : "";
	            $app_ans_aft_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], '' );

	            $rtn .= "Question: " . $QI['Question'] . "<br>";
	            $rtn .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
	            $rtn .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
	        }
	    }
	    else if($QI['QuestionTypeID'] == 14) {

	        //After the complete upgrade have to remove this.
	        $ques_to_skip[]        =   $QI['QuestionID']."1";
	        $ques_to_skip[]        =   $QI['QuestionID']."2";
	        $ques_to_skip[]        =   $QI['QuestionID']."3";
	        $ques_to_skip[]        =   $QI['QuestionID']."ext";
	        
	        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	        $QI['ApplicationID']   =   $ApplicationID;
	        $QI['RequestID']       =   $RequestID;
	        $QI['AnswerStatus']    =   1;
	    
	        //Data for web form history
	        if(($agr_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
    	        && isset($agr_form_data_info_history[$QI['QuestionID']]['Question'])
    	        && $agr_form_data_info_history[$QI['QuestionID']]['Question'] != ""
	            && in_array($QI['QuestionID'], $agr_form_data_que_ids)) {

	            $phone_ans_bef    =   $agr_form_data_info_history[$QI['QuestionID']]['Answer'];
	            $app_ans_bef      =   ($phone_ans_bef != "") ? json_decode($phone_ans_bef, true) : "";
	            $app_ans_bef_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], $app_ans_bef[3] );
	             
	            $phone_ans_aft    =   $QI['Answer'];
	            $app_ans_aft      =   ($phone_ans_aft != "") ? json_decode($phone_ans_aft, true) : "";
	            $app_ans_aft_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], $app_ans_aft[3] );
	    
	            $rtn .= "Question: " . $QI['Question'] . "<br>";
	            $rtn .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
	            $rtn .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
	        }
	    }
        else if($QI['QuestionTypeID'] == 15) {
            
            //After the complete upgrade have to remove this.
            $ques_to_skip[]        =   $QI['QuestionID']."1";
            $ques_to_skip[]        =   $QI['QuestionID']."2";
            $ques_to_skip[]        =   $QI['QuestionID']."3";
            $ques_to_skip[]        =   $QI['QuestionID'];
             
            $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
            $QI['ApplicationID']   =   $ApplicationID;
            $QI['RequestID']       =   $RequestID;
            $QI['AnswerStatus']    =   1;
             
            //Data for web form history
            if(($agr_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
                && isset($agr_form_data_info_history[$QI['QuestionID']]['Question'])
                && $agr_form_data_info_history[$QI['QuestionID']]['Question'] != ""
                && in_array($QI['QuestionID'], $agr_form_data_que_ids)) {
            
                $ssn_ans_bef      =   $agr_form_data_info_history[$QI['QuestionID']]['Answer'];
                $app_ans_bef      =   ($ssn_ans_bef != "") ? json_decode($ssn_ans_bef, true) : "";
                $app_ans_bef_ans  =   $app_ans_bef[0].'-'.$app_ans_bef[1].'-'.$app_ans_bef[2];
            
                $ssn_ans_aft      =   $QI['Answer'];
                $app_ans_aft      =   ($ssn_ans_aft != "") ? json_decode($ssn_ans_aft, true) : "";
                $app_ans_aft_ans  =   $app_ans_aft[0].'-'.$app_ans_aft[1].'-'.$app_ans_aft[2];
            
                $rtn .= "Question: " . $QI['Question'] . "<br>";
                $rtn .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
                $rtn .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
            }
        }
        else if($QI['QuestionTypeID'] == 18) {
            //After the complete upgrade have to remove this.
            $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
             
            for($c18 = 1; $c18 <= $cnt; $c18++) {
                $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
            }
            $ques_to_skip[]        =   $QI['QuestionID'];
            $ques_to_skip[]        =   $QI['QuestionID']."cnt";
             
            $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
            $QI['ApplicationID']   =   $ApplicationID;
            $QI['RequestID']       =   $RequestID;
            $QI['AnswerStatus']    =   1;
             
            $que_vals              =   explode ( '::', $QI['value'] );
            $que_vals_cnt          =   count($que_vals);
             
            $que_values_info       =   array();
            for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
                $que_vals_info     =   explode(":", $que_vals[$qvc]);
                $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
            }
        
            //Data for web form history
            if(($agr_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
                && isset($agr_form_data_info_history[$QI['QuestionID']]['Question'])
                && $agr_form_data_info_history[$QI['QuestionID']]['Question'] != ""
                && in_array($QI['QuestionID'], $agr_form_data_que_ids)) {
                 
                $app_ans_bef_ans    =   $agr_form_data_info_history[$QI['QuestionID']]['Answer'];
                $app_ans_aft_ans    =   $QI['Answer'];
        
                $ans_bef_ans_jd     =   json_decode($agr_form_data_info_history[$QI['QuestionID']]['Answer'], true);
                $ans_aft_ans_jd     =   json_decode($QI['Answer'], true);
        
                foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
                    $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
                }
                 
                foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
                    $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
                }
        
                $app_ans_bef_ans    =   @implode(", ", array_values($ans_bef_ans_jd));
                $app_ans_aft_ans    =   @implode(", ", array_values($ans_aft_ans_jd));
        
                $rtn .= "Question: " . $QI['Question'] . "<br>";
                $rtn .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
                $rtn .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
            }
        }
        else if($QI['QuestionTypeID'] == 1818) {
            //After the complete upgrade have to remove this.
            $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
             
            for($c18 = 1; $c18 <= $cnt; $c18++) {
                $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
            }
            
            $ques_to_skip[]        =   $QI['QuestionID'];
            $ques_to_skip[]        =   $QI['QuestionID']."cnt";
             
            $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
            $QI['ApplicationID']   =   $ApplicationID;
            $QI['RequestID']       =   $RequestID;
            $QI['AnswerStatus']    =   1;
        
            $que_vals              =   explode ( '::', $QI['value'] );
            $que_vals_cnt          =   count($que_vals);
        
            $que_values_info       =   array();
            for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
                $que_vals_info     =   explode(":", $que_vals[$qvc]);
                $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
            }
             
            //Data for web form history
            if(($agr_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
                && isset($agr_form_data_info_history[$QI['QuestionID']]['Question'])
                && $agr_form_data_info_history[$QI['QuestionID']]['Question'] != ""
                && in_array($QI['QuestionID'], $agr_form_data_que_ids)) {
                 
                $app_ans_bef_ans    =   $agr_form_data_info_history[$QI['QuestionID']]['Answer'];
                $app_ans_aft_ans    =   $QI['Answer'];
        
                $ans_bef_ans_jd     =   json_decode($agr_form_data_info_history[$QI['QuestionID']]['Answer'], true);
                $ans_aft_ans_jd     =   json_decode($QI['Answer'], true);
                 
                foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
                    $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
                }
        
                foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
                    $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
                }
                 
                $app_ans_bef_ans    =   @implode(", ", array_values($ans_bef_ans_jd));
                $app_ans_aft_ans    =   @implode(", ", array_values($ans_aft_ans_jd));
        
                 
                $rtn .= "Question: " . $QI['Question'] . "<br>";
                $rtn .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
                $rtn .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
            }
             
        }
	    else if($QI['QuestionTypeID'] == 9) {
	    
	        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	        $QI['ApplicationID']   =   $ApplicationID;
	        $QI['RequestID']       =   $RequestID;
	        $QI['AnswerStatus']    =   1;
	    
	        //After the complete upgrade have to remove this.
	        $fd_skills_info        =   json_decode($QI['Answer'], true);
	        foreach($fd_skills_info as $fd_skill_key=>$fd_skill_value) {
	            $fd_skill_keys     =   array_keys($fd_skill_value);
	            $ques_to_skip[]    =   $fd_skill_keys[0];
	            $ques_to_skip[]    =   $fd_skill_keys[1];
	            $ques_to_skip[]    =   $fd_skill_keys[2];
	        }
	         
	        $ques_to_skip[]        =   $QI['QuestionID'];
	        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
	    
	        //Data for web form history
	        if(($agr_form_data_info_history[$QI['QuestionID']]['Answer'] != $QI['Answer'])
    	        && isset($agr_form_data_info_history[$QI['QuestionID']]['Question'])
    	        && $agr_form_data_info_history[$QI['QuestionID']]['Question'] != ""
	            && in_array($QI['QuestionID'], $agr_form_data_que_ids)) {
	             
	            $app_ans_bef_ans   =   $agr_form_data_info_history[$QI['QuestionID']]['Answer'];
	            $app_ans_aft_ans   =   $QI['Answer'];
	    
	            $askills_info_old = json_decode($app_ans_bef_ans, true);
	            $askills_info_new = json_decode($app_ans_aft_ans, true);
	             
	            $artn_old = '';
	            foreach($askills_info_old as $askills_key_old=>$askills_value_old) {
	                $avalues_old   =  array_values($askills_value_old);
	                $artn_old     .=  '&#8226;&nbsp;<strong>' . $avalues_old[0] . ", " . $avalues_old[1] . " yrs. (" . $avalues_old[2].")</strong><br>";
	            }
	             
	            $artn_new = '';
	            foreach($askills_info_new as $askills_key_new=>$askills_value_new) {
	                $avalues_new   =  array_values($askills_value_new);
	                $artn_new     .=  '&#8226;&nbsp;<strong>' . $avalues_new[0] . ", " . $avalues_new[1] . " yrs. (" . $avalues_new[2].")</strong><br>";
	            }
	             
	            $rtn .= "Question: " . $QI['Question'] . "<br>";
	            $rtn .= "&nbsp;&nbsp;Old: " . $artn_old . "<br>";
	            $rtn .= "&nbsp;&nbsp;New: " . $artn_new . "<br>\n";
	             
	        }
	    }

	}
	
	foreach ( $_POST as $questionid => $answer ) {
	    if(!in_array($questionid, $ques_to_skip)) {
	        //Data for web form history
	        if(($agr_form_data_info_history[$questionid]['Answer'] != $answer)
    	        && isset($agr_form_data_info_history[$questionid]['Question'])
    	        && $agr_form_data_info_history[$questionid]['Question'] != ""
	            && in_array($questionid, $agr_form_data_que_ids)) {
	            $rtn .= "Question: " . $agr_form_data_info_history[$questionid]['Question'] . "<br>";
	            $rtn .= "&nbsp;&nbsp;Old: " . $agr_form_data_info_history[$questionid]['Answer'] . "<br>";
	            $rtn .= "&nbsp;&nbsp;New: " . $answer . "<br>\n";
	        }
	    }
		
	} // end foreach
	
	return $rtn;
} // end function
?>
