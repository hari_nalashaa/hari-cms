<?php
$ERROR = "";

/**
 * @tutorial	This statement is common for all the 
 * 				function insUpdApplicantDataTemp() calls 
 * 				In this file, if the data or the columns are different
 * 			    Please update the statement parameter with different variable
 * 				so that it will not affect next function calls 
 */

//Default update information
$on_update      =   ' ON DUPLICATE KEY UPDATE Required = :URequired';
$update_info    =   array(":URequired"=>'REQUIRED');
//Default insert information
$insert_info    =   array('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'Answer'=>'', 'Required'=>'REQUIRED');

// check all the required and active FormQuestions

//Set condition
$where          =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "Required = 'Y'", "Active = 'Y'");
//Bind parameters
$params         =   array(':OrgID'=>$OrgID, ':AgreementFormID'=>$AgreementFormID);
//Get Agreement FormQuestions
$results        =   $FormsInternalObj->getAgreementFormQuestions("*", $where, "QuestionOrder", array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $FQCK) {
	    
	    if($FQCK['QuestionTypeID'] == 100 || $FQCK['QuestionTypeID'] == 120) {
	        if($FQCK['QuestionTypeID'] == 100) {
	            if(!isset($_POST['LabelSelect'])) {
	                $ERROR .= " - " . $FQCK ['Question'] . ' is missing.' . "\\n";
	                G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['LabelSelect'][$FQCK ['QuestionID']]));
	            }
	            else if(isset($_POST['LabelSelect'])) {
	                if(is_array($_POST['LabelSelect'][$FQCK ['QuestionID']])) {
	                    foreach($_POST['LabelSelect'][$FQCK ['QuestionID']] as $cus_que100_label=>$cus_que100_value) {
	                        if(is_array($cus_que100_value)) {
	                            foreach($cus_que100_value as $cus_que100_lbl_info=>$cus_que100_lbl_val) {
	                                if($cus_que100_lbl_val == "") {
	                                    $ERROR .= " - Please select required option in " . $FQCK ['Question'] . "\\n";
	                                    G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['LabelSelect'][$FQCK ['QuestionID']]));
	                                }
	                            }
	                        }
	    
	                    }
	                }
	            }
	        }
	        if($FQCK['QuestionTypeID'] == 120) {
	            if(!isset($_POST['shifts_schedule_time'])) {
	                $ERROR .= " - " . $FQCK ['Question'] . ' is missing.' . "\\n";
	                G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]) );
	            }
	            else if(isset($_POST['shifts_schedule_time'])) {
	                if(is_array($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['from_time'])) {
	                    $from_available_flag = false;
	                    foreach($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['from_time'] as $cus_que120_from_val) {
	                        if($cus_que120_from_val == "") {
	                            $from_available_flag = true;
	                        }
	                    }
	                    if($from_available_flag == true)
	                    {
	                        G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]));
	                        $ERROR .= " - " . $FQCK ['Question'] . ' from time fields are missing.' . "\\n";
	                    }
	                }
	                if(is_array($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['to_time'])) {
	                    $to_available_flag = false;
	                    foreach($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]['to_time'] as $cus_que120_to_val) {
	                        if($cus_que120_to_val == "") {
	                            $to_available_flag = true;
	                        }
	                    }
	                    if($to_available_flag == true) {
	                        G::Obj('ApplicantDataTemp')->validateDefInputUpdApplicantDataTemp($OrgID, $HoldID, $FQCK ['QuestionID'], serialize($_POST['shifts_schedule_time'][$FQCK ['QuestionID']]));
	                        $ERROR .= " - " . $FQCK ['Question'] . ' to time fields are missing.' . "\\n";
	                    }
	                }
	            }
	        }
	    }
	    else {
	        
	        if (in_array ( $FQCK ['QuestionTypeID'], array ('15') )) {
	            // ssn check
	        
	            $ssn1 = $FQCK ['QuestionID'] . "1";
	            $ssn2 = $FQCK ['QuestionID'] . "2";
	            $ssn3 = $FQCK ['QuestionID'] . "3";
	        
	            $SSN = $_POST [$ssn1] . $_POST [$ssn2] . $_POST [$ssn3];
	        
	            // take out everything except numbers and trim it.
	            // $ssnck = trim(preg_replace("/[^0-9]/","",$_POST[$FQCK['QuestionID']]));
	            $ssnck = trim ( preg_replace ( "/[^0-9]/", "", $SSN ) );
	            $ssnlength = strlen ( $ssnck );
	        
	            if ($ssnck == "") {
	                
	                $ERROR .= " - " . strip_tags ( $FQCK ['Question'] ) . ' is missing.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $FQCK ['QuestionID'];
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                
	            } else if ($ssnlength < 9) {
	                
	                $ERROR .= " - " . strip_tags ( $FQCK ['Question'] ) . ' is missing digits.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $FQCK ['QuestionID'];
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                
	            } else {
	                	
	                $ssnck1 =  $_POST [$ssn1];
	                $ssnck2 =  $_POST [$ssn2];
	                $ssnck3 =  $_POST [$ssn3];
	                	
	                if ($ssnck1 == "000") {
	                    $erck ++;
	                }
	                if ($ssnck1 == "666") {
	                    $erck ++;
	                }
	                if (($ssnck1 >= "900") && ($ssnck1 <= "999")) {
	                    $erck ++;
	                }
	                if ($ssnck3 == "00") {
	                    $erck ++;
	                }
	                if ($ssnck2 == "0000") {
	                    $erck ++;
	                }
	                	
	                if ($erck > 0) {
	                    $ERROR .= " - Social Security Number is invalid." . "\\n";
	                    	
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $FQCK ['QuestionID'];
	                    	
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	            }
	        } else if (in_array ( $FQCK ['QuestionTypeID'], array (
	            '13',
	            '14',
	            '19'
	        ) )) { // three part answer
	        
	            $xi = 1;
	        
	            while ( $xi <= 3 ) {
	                	
	                $value = $FQCK ['QuestionID'] . $xi;
	                	
	                if ($_POST [$value] == "") {
	        
	                    $ERROR .= " - " . $tsect . ' ' . strip_tags ( $FQCK ['Question'] ) . ' item ' . $sect . ' is missing.' . "\\n";
	                    	
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $value;
	                    	
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                    	
	                    break;
	                } // end if
	                	
	                $xi ++;
	            } // end while
	        } else if ($FQCK ['QuestionTypeID'] == 1) { // to and from date
	        
	            $emp_from = $FQCK ['QuestionID'] . 'from';
	            $emp_to = $FQCK ['QuestionID'] . 'to';
	        
	            if (($_POST [$emp_to] == "") || ($_POST [$emp_from] == "")) {
	                $ERROR .= " - " . $tsect . " section " . $sect . " - " . strip_tags ( $FQCK ['Question'] ) . " is missing." . "\\n";
	                if ($_POST [$emp_to] == "") {
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $emp_to;
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	                	
	                if ($_POST [$emp_from] == "") {
	                    //Set QuestionID to insert
	                    $insert_info['QuestionID'] = $emp_from;
	                    //Insert Update Applicant Data Temp
	                    G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	                }
	            } // end if
	        } else if (in_array ( $FQCK ['QuestionTypeID'], array (
	            '18',
	            '9',
	            '1818'
	        ) )) { // checkboxes
	        
	            $value = $FQCK ['QuestionID'] . 'cnt';
	            $valcnt = $_POST [$value];
	        
	            $i = 0;
	            $hitone = 0;
	            while ( $i < $valcnt ) {
	                $i ++;
	                $entry             =   $FQCK ['QuestionID'] . '-' . $i;
	                $entry_comments    =   $entry . '-comments';
	                $entry_yr          =   $entry . '-yr';
	                	
	                if (($FQCK ['QuestionTypeID'] == 9) && ($_POST [$entry] != "") && ($_POST [$entry_comments] != "") && ($_POST [$entry_yr] != "")) {
	                    $hitone ++;
	                }
	                	
	                if (($FQCK ['QuestionTypeID'] == 18) && ($_POST [$entry] != "")) {
	                    $hitone ++;
	                } // end if
	                	
	                if (($FQCK ['QuestionTypeID'] == 1818) && ($_POST [$entry] != "")) {
	                    $hitone ++;
	                } // end if
	            } // end while
	        
	            if ($hitone == 0) {
	                $ERROR .= " - " . strip_tags ( $FQCK ['Question'] ) . ' is missing.' . "\\n";
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $FQCK ['QuestionID'];
	        
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	            }
	        } else {
	        
	            $errick = "0";
	            if ($_POST [$FQCK ['QuestionID']] == "") {
	                	
	                $errick ++;
	                $ERROR .= " - " . strip_tags ( $FQCK ['Question'] ) . ' is missing.' . "\\n";
	        
	                //Set QuestionID to insert
	                $insert_info['QuestionID'] = $FQCK ['QuestionID'];
	        
	                //Insert Update Applicant Data Temp
	                G::Obj('ApplicantDataTemp')->insUpdApplicantDataTemp ( $insert_info, $on_update, $update_info );
	            } // end if Question
	        }
	    }
		
	}	
}

$questions_info			=	array();
$validate_dates_list	=	array();
	
$columns	=	"Question, QuestionID, QuestionTypeID, Validate";
//Set condition
$where      =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "Active = 'Y'");
//Bind parameters
$params     =   array(':OrgID'=>$OrgID, ':AgreementFormID'=>$AgreementFormID);
//Get Agreement FormQuestions
$results    =   $FormsInternalObj->getAgreementFormQuestions($columns, $where, "QuestionOrder", array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $FQCK) {
		$validate_dates_list[$FQCK['QuestionID']] = json_decode($FQCK['Validate'], true);
		$questions_info[$FQCK['QuestionID']]['Question'] = $FQCK['Question'];
	}
}

foreach ($validate_dates_list as $question_id=>$validate_que_info) {
	if($validate_que_info['years_min'] != "") {

		if($_POST [$question_id] != "") {
			
			//date information
			$date_ans			=	G::Obj('DateHelper')->getYmdFromMdy($_POST [$question_id]);
			$d1 				=	new DateTime(date('Y-m-d'));
			$d2 				=	new DateTime($date_ans);

			$ans_date_diff		=	$d2->diff($d1);

			if($ans_date_diff->y < $validate_que_info['years_min']) {
				$ERROR	.=	" - " . $questions_info[$question_id]['Question'] . ' date needs to be more than '.$validate_que_info['years_min'].' years.'. "\\n";
				
				$ERROR .= " - " . strip_tags ( $FQCK ['Question'] ) . ' is missing.' . "\\n";
			}

		}
	}
}

if(FROM_SRC == 'USERPORTAL') {
    //Validate User Portal Agreement Form Captcha
    $ERROR .= $ValidateFormsCaptchaObj->validateUserPortalAgreementFormCaptcha($insert_info, $on_update, $update_info);
}
else {
    //Validate Irecruit Agreement Form Captcha
    $ERROR .= $ValidateFormsCaptchaObj->validateIrecruitAgreementFormCaptcha($insert_info, $on_update, $update_info);
}

return $ERROR;
?>