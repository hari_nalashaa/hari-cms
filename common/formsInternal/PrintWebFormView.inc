<?php
// ** FUNCTIONS ** //
function printWebForm($OrgID, $ApplicationID, $RequestID, $WebFormID)
{
    global $permit;
    
    $WebFormQueAns  =   G::Obj('WebFormQueAns')->getWebFormQueAnswersInfo ( $OrgID, $ApplicationID, $RequestID, $WebFormID );
    $web_form_view  =   '';
    foreach ($WebFormQueAns as $QuestionID=>$QuestionAnswerInfo)
    {
        $web_form_view  .=   "<div class='row' style='margin-bottom:3px;'>";
        
        if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
            
            $web_form_view  .=   "<div class='col-lg-3 col-md-3 col-sm-2' style='width:20%;float:left;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
            
            $canswer = $QuestionAnswerInfo['Answer'];
            
            $rtn  = '';
            if(is_array($canswer)) {
                $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
                
                foreach ($canswer as $cakey=>$caval) {
                    $rtn .= '<tr>';
                    $rtn .= '<td style="padding-right:5px !important">'.$cakey.'&nbsp;&nbsp;&nbsp;</td>';
                    foreach ($caval as $cavk=>$cavv) {
                        $rtn .= '<td style="padding-right:5px !important">'.$cavv.'&nbsp;&nbsp;&nbsp;</td>';
                    }
                    $rtn .= '</tr>';
                }
                
                $rtn .= '</table>';
            }
            
            $web_form_view  .=   "<div class='col-lg-9 col-md-9 col-sm-10' style='width:78%;float:left;border:1px solid #FFFFFF;'><strong>".$rtn."</strong></div>";
            
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
            
            $web_form_view  .=   "<div class='col-lg-3 col-md-3 col-sm-2' style='width:20%;float:left;border:1px solid #FFFFFF;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
            
            $canswer = $QuestionAnswerInfo['Answer'];
            
            $rtn  = '';
            if(is_array($canswer)) {
                
                $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
                
                $days_count = count($canswer['days']);
                
                for($ci = 0; $ci < $days_count; $ci++)  {
                    $rtn .= '<tr>';
                    $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
                    $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
                    $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
                    $rtn .= '<td width="5%">to </td>';
                    $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
                    $rtn .= '</tr>';
                }
                
                $rtn .= '</table>';
            }
            
            $web_form_view  .=   "<div class='col-lg-9 col-md-9 col-sm-10' style='width:78%;float:left;border:1px solid #FFFFFF;'><strong>".$rtn."</strong></div>";
        }
        else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
            $web_form_view  .=   "<div class='col-lg-12 col-md-12 col-sm-12' style='width:100%;float:left;border:1px solid #FFFFFF;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
        }
        else {
            $web_form_view  .=   "<div class='col-lg-3 col-md-3 col-sm-2' style='width:20%;float:left;border:1px solid #FFFFFF;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
            $web_form_view  .=   "<div class='col-lg-9 col-md-9 col-sm-10' style='width:78%;float:left;border:1px solid #FFFFFF;'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
        }
        
        $web_form_view  .=   "<div class='col-lg-12 col-md-12 col-sm-12' style='clear:both;' id='question_space'></div>";
        $web_form_view  .=   "</div>";
    
    }
    
    return $web_form_view;
} // end of function

function getWebFormDisplayValue($ans, $displaychoices)
{
    $rtn = '';
    
    if ($ans != '') {
        $Values = explode('::', $displaychoices);
        foreach ($Values as $v => $q) {
            list ($vv, $qq) = explode(":", $q, 2);
            if ($vv == $ans) {
                $rtn .= $qq;
            }
        }
    }
    return $rtn;
} // end of function

function getWebFormMultiAnswer($APPDATA, $id, $displaychoices, $questiontype)
{
    $rtn = '';
    $val = '';
    $lex = '';
    
    if (($id == 'daysavailable') || ($id == 'typeavailable')) {
        
        $le = ', ';
        $li = '';
        $lecnt = - 2;
    } else {
        
        $le = '<br>';
        $li = '&#8226;&nbsp;';
        $lecnt = - 4;
    }
    
    $cnt = $APPDATA[$id . 'cnt'];
    
    for ($i = 1; $i <= $cnt; $i ++) {
        
        $val = getWebFormDisplayValue($APPDATA[$id . '-' . $i], $displaychoices);
        if ($val) {
            $rtn .= $li . $val;
            $lex = 'on';
            $val = '';
        }
        
        if ($questiontype == 9) {
            
            $val = $APPDATA[$id . '-' . $i . '-yr'];
            if ($val) {
                $rtn .= '&nbsp;' . $val . ' yrs. ';
                $lex = 'on';
                $val = '';
            }
            
            $val = $APPDATA[$id . '-' . $i . '-comments'];
            if ($val) {
                $rtn .= '&nbsp;(' . $val . ') ';
                $lex = 'on';
                $val = '';
            }
        } // end questiontype
        
        if ($lex == 'on') {
            $rtn .= $le;
            $lex = '';
        }
    }
    
    $rtn = substr($rtn, 0, $lecnt);
    
    return $rtn;
} // end of function
?>