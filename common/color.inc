<?php
//set where condition
$where = array("OrgID = :OrgID");
//set parameters
$params = array(":OrgID"=>$OrgID);
if(isset($MultiOrgID) && $MultiOrgID != "") {
    $where[] = "MultiOrgID = :MultiOrgID";
    $params[":MultiOrgID"]  =   $MultiOrgID;
}
//get organization colors information
$results = $OrganizationsObj->getOrganizationColorsInfo("*", $where, '', array($params));

if(isset($results['results'][0])) $COLOR = $results['results'][0];

if (! isset($COLOR)) {
	//set where condition
	$where = array("OrgID = :OrgID");
	//set parameters
	$params = array(":OrgID"=>'DEFAULT');
	//get organization colors information
	$results = $OrganizationsObj->getOrganizationColorsInfo("*", $where, '', array($params));
	
	$COLOR = $results['results'][0];
}

if(class_exists('CmsTemplate')) {
    if ( ($TemplateObj instanceof CmsTemplate) != true ) {
        $TemplateObj = new CmsTemplate();
        $TemplateObj->COLOR = $COLOR;
    }
    else if ( ($TemplateObj instanceof CmsTemplate) == true ) {
        $TemplateObj->COLOR = $COLOR;
    } 
}
?>
