<?php
if (isset($whitelist) 
    && in_array(ROOT, $whitelist)
    && defined('ROOT')) {
    require_once ROOT . 'traits/TraitAdminConnection.inc';//For Admin Database Connection
    require_once ROOT . 'traits/TraitIrecruitConnection.inc';//For Irecruit Database Connection
    require_once ROOT . 'traits/TraitUserPortalConnection.inc';//For User Database Connection
    require_once ROOT . 'traits/TraitWotcConnection.inc';//For WOTC Database Connection
    require_once ROOT . 'traits/TraitTwilioSettings.inc';
    require_once ROOT . 'traits/TraitG.inc';
    require_once ROOT . 'traits/TraitSovrenParameters.inc';
    require_once ROOT . 'traits/TraitSovrenUI.inc';
}

//Class to call templating system
$class_files[] = "CmsTemplate";
//Class to log all messages to a text file
$class_files[] = "Logger";
//Class has all query processing functions like select, insert, update
$class_files[] = "Database";

//Include all classes in array
if (isset($whitelist) 
    && in_array(ROOT, $whitelist)
    && defined('ROOT')) {
    require_once ROOT . 'classes/ClassCmsTemplate.inc';
    require_once ROOT . 'classes/ClassLogger.inc';
    require_once ROOT . 'classes/ClassDatabase.inc';
}

//Create objects for model classes
$TemplateObj	=	new CmsTemplate ();

//It will autoload all the classes
spl_autoload_register('iRecruitAutoloader');

function iRecruitAutoloader($class_name)
{
    $path = ROOT . 'classes/Class';

    if(file_exists($path.$class_name.'.inc') && $class_name != "Session") {
        require_once $path.$class_name.'.inc';
    }
}

//Create dynamic objects with the class name
$classes_dir        =   ROOT . 'classes/';
$scanned_directory  =   array_diff(scandir($classes_dir), array('..', '.'));

foreach($scanned_directory as $class_file_name) {

    $first_trim =   substr($class_file_name, 5);
    $class_name =   substr($first_trim, 0, -4);

    if(!in_array($class_name, $class_files) && $class_name != "Session") {
        ${$class_name."Obj"} = new $class_name;
    }
}
?>
