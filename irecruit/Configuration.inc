<?php
require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';
require_once IRECRUIT_DIR . 'SessionConfig.inc';
require_once VENDOR_DIR . 'autoload.php';

header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

if(!isset($IRECRUIT_USER_AUTH) && $IRECRUIT_USER_AUTH !== FALSE) $IRECRUIT_USER_AUTH = TRUE;

date_default_timezone_set('America/New_York');
ini_set("allow_url_fopen", 1);

ini_set('memory_limit', '8192M');
ini_set ('display_errors', 0);

//Modified $FROM global variable to FROM_SRC constant
define('FROM_SRC', 'IRECRUIT');

$error_log_file_path = ROOT . "logs/php-errors/php-error-log-".date('Y-m-d').".log";

if(!is_dir(ROOT . "logs/php-errors")) {
	mkdir(ROOT . "logs/php-errors");
	chmod(ROOT . "logs/php-errors", 0777);
}

ini_set("log_errors", 1);
ini_set("error_log", $error_log_file_path);

error_reporting(E_ALL);

//Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';

//It should not execute for login page
if (! preg_match ( '/login.php$/', $_SERVER ["SCRIPT_NAME"] ) && $IRECRUIT_USER_AUTH === TRUE) {
	//Authentication Information
	$irecruit_auth_info    =   G::Obj('IrecruitApplicationFeatures')->getIrecruitUserAuthenticationInfo();
	if(is_array($irecruit_auth_info)) extract($irecruit_auth_info);
}

if((isset($_REQUEST['BOrgID']) && $_REQUEST['BOrgID'] != "") || $OrgID != "") {
    if(isset($_REQUEST['BOrgID']) && $_REQUEST['BOrgID'] != "") {
        $OrgID = $_REQUEST['BOrgID'];
    }
}

//For Autherised User
if($IRECRUIT_USER_AUTH === TRUE) {
	//Irecruit Application Settings
    $irecruit_app_settings = G::Obj('IrecruitApplicationFeatures')->getIrecruitApplicationConfigSettings();
	if(is_array($irecruit_app_settings)) extract($irecruit_app_settings);
}
//For Unauthorized user
if($IRECRUIT_USER_AUTH === FALSE) {
	//Irecruit Application Settings
    $irecruit_app_settings = G::Obj('IrecruitApplicationFeatures')->getIrecruitDefaultSettings($OrgID);
	if(is_array($irecruit_app_settings)) extract($irecruit_app_settings);
}

if((isset($_REQUEST['BOrgID']) && $_REQUEST['BOrgID'] != "") || $OrgID != "") {
    $TemplateObj->brand_info = $brand_info = $OrganizationDetailsObj->getOrganizationInformation($OrgID, "", "BrandID");
    if(!isset($brand_info['BrandID']) || $brand_info['BrandID'] == "") {
        $brand_info['BrandID']      =   "0";
        $TemplateObj->brand_info    =   $brand_info;
    }
    $TemplateObj->brand_org_info  = $brand_org_info = $BrandsObj->getBrandInfo($brand_info['BrandID']);
}
else {
    $brand_info['BrandID']      =   "0";
    $TemplateObj->brand_info    =   $brand_info;
}

if($OrgID != "") {
	//Organization Information
	$MultiOrgID = isset($_REQUEST['MultiOrgID']) ? $_REQUEST['MultiOrgID'] : "";

	if($MultiOrgID == NULL 
	    || $MultiOrgID == "ORIG") {
		$MultiOrgID = "";
	}
	
	//Get organization information
	$organization_info = G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "*");
	if(isset($organization_info) && $organization_info !== false) extract($organization_info);
}


//Set Onboard Features
$onboard_features_list  =   array();
if($feature['MatrixCare'] == 'Y') {
    $onboard_features_list[]    =   'MatrixCare';
}
if($feature['WebABA'] == 'Y') {
    $onboard_features_list[]    =   'WebABA';
}
if($feature['Lightwork'] == 'Y') {
    $onboard_features_list[]    =   'Lightwork';
}
if($feature['Abila'] == 'Y') {
    $onboard_features_list[]    =   'Abila';
}
if($feature['Mercer'] == 'Y') {
    $onboard_features_list[]    =   'Mercer';
}
if($feature['DataManagerType'] != 'N') {
    $onboard_features_list[]    =   $feature['DataManagerType'];
}

$TemplateObj->onboard_features_list =   $onboard_features_list;

include COMMON_DIR . 'application/ApplicationJavaScript.inc';
require_once COMMON_DIR . 'color.inc';
?>
