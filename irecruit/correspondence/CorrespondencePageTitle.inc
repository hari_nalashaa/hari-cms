<?php 
$title      =   'Correspondence';

$action     =   $_REQUEST['action'];

$subtitle   =   array(
                    "packages"      => "Packages",
                    "letters"       => "Email Templates",
                    "attachments"   => "Attachments",
                );

if ($subtitle[$action]) {
	$title .= ' - ' . $subtitle[$action];
}
?>