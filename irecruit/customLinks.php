<?php
require_once 'Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title	= $title = 'Custom Links';

//Include configuration related javascript in footer
$scripts_footer[] = "js/custom_links.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

if(isset($_GET['key']) && $_GET['key'] != "") {
	$CustomLinksObj->deleteCustomLink($_GET['key']);
	
	header("Location:customLinks.php?msg=delsuc");
	exit;
}

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] != "") {
	$CustomLinksObj->insertCustomLink($_POST);
	
	header("Location:customLinks.php?msg=inssuc");
	exit;
}

echo $TemplateObj->displayIrecruitTemplate('views/responsive/CustomLinks');