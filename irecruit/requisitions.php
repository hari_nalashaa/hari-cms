<?php
require_once 'Configuration.inc';

$inventory_details = array ();
$TemplateObj->bg_color = $bg_color = "yellow";

include_once 'requisitions/RequisitionsPageTitles.inc';

if(! $action) {
	header("Location:requisitionsSearch.php");
	exit;
}

//Set page title
$TemplateObj->title	= $title;

// Mandatory questions we can't edit these
$TemplateObj->required_questions = $required_questions = array (
		"requesttitle",
		"importancelevel",
		"howmany",
		"salaryrangefrom",
		"salaryrangeto",
		"whenneeded"
);

$request_questions_list = array();
$request_questions_count = 0;
if ($feature ['RequisitionRequest'] == "Y") {
	$columns = "Question, QuestionID, FormID, QuestionTypeID, Active, Required, QuestionOrder, value";
	$where_info = array ("OrgID = :OrgID", "FormID = 'STANDARD'", "Active = 'Y'");
	$params_info = array (":OrgID" => $OrgID);
	
	$TemplateObj->request_questions_info = $request_questions_info = $FormQuestionsObj->getQuestionsInformation ( "RequestQuestions", $columns, $where_info, "QuestionOrder ASC", array (
			$params_info
	) );
	
	$TemplateObj->request_questions_list = $request_questions_list = $request_questions_info ['results'];
	$TemplateObj->request_questions_count = $request_questions_count = $request_questions_info ['count'];
}

$request_que_ids = array ();
$required_pre_que_ids = array('RequisitionID', 'JobID', 'Title');

$required_pre_que_ids[] = 'PostDate';
$required_pre_que_ids[] = 'ExpireDate';

if($feature ['MonsterAccount'] == "Y") {
	$required_pre_que_ids[] = 'MonsterJobIndustry';
	$required_pre_que_ids[] = 'JobOccupations';
	$required_pre_que_ids[] = 'JobCategory';
	$required_pre_que_ids[] = 'content';
}

for($j = 0; $j < $request_questions_count; $j++) {

	$request_que_ids [] = $request_questions_list [$j] ['QuestionID'];

	if ($request_questions_list [$j] ['Required'] == "Y") {
		$required_pre_que_ids [] = $request_questions_list [$j] ['QuestionID'];
	}

	$req_que_ord[$request_questions_list [$j] ['QuestionID']] = $request_questions_list [$j] ['QuestionOrder'];
	$req_que_value[$request_questions_list [$j] ['QuestionID']] = $request_questions_list [$j] ['value'];
}

foreach($request_que_ids as $req_key=>$req_val) {
	if(in_array($req_val, $required_questions)) {
		unset($request_que_ids[$req_key]);
	}
}

$ADVERTISEOPTIONS = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
$AO = array ();
$SAO = serialize ( $AO );
if(is_array($ADVERTISEOPTIONS)) {
	foreach ( $ADVERTISEOPTIONS as $option ) {
		$AO [$option] = $_POST [$option];
	}
	$SAO = serialize ( $AO );
}
$TemplateObj->SAO = $SAO;
$TemplateObj->AO = $AO;
$TemplateObj->AdvertisingOptions = $AdvertisingOptions = unserialize ( $SAO );

$ad_valid_status = "true";
foreach ($AdvertisingOptions as $AOKeyName=>$AOValueName) {
	if($AdvertisingOptions[$AOKeyName] != "") $ad_valid_status = "false";
}

$TemplateObj->ad_valid_status = $ad_valid_status;

$WORKWEEK = array ("SUN" => "Sunday", "MON" => "Monday", "TUE" => "Tuesday", "WED" => "Wednesday", "THU" => "Thursday", "FRI" => "Friday", "SAT" => "Saturday");

$WW = array_keys ( $WORKWEEK );
$WorkWeekSelected = array ();
foreach ( $WW as $d ) {
	if ($_REQUEST [$d] == "Y") {
		$WorkWeekSelected [$d] = $_REQUEST [$d];
	}
}

$TemplateObj->WorkWeekSelected = $WorkWeekSelected;
$TemplateObj->workdays = $workdays = serialize ( $WorkWeekSelected );

$TemplateObj->yes_no = $yes_no = array(""=>"Please Select", "Y"=>"Yes", "N"=>"No");

if ($feature ['RequisitionRequest'] != "Y") {
	$TemplateObj->request_que_ids = $request_que_ids = array();
}

$TemplateObj->request_que_ids = $request_que_ids;
$TemplateObj->required_pre_que_ids = $required_pre_que_ids;
$TemplateObj->req_que_ord = $req_que_ord;
$TemplateObj->req_que_value = $req_que_value;

$script_vars_footer[]	= 'var datepicker_ids = "#poe_from, #poe_to";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';
$TemplateObj->scripts_vars_footer = $script_vars_footer;

//Include configuration related javascript in header
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$scripts_header[] = "js/loadAJAX.js";
$TemplateObj->page_scripts_header = $scripts_header;

//Set footer scripts
$scripts_footer[] = "js/requisition.js";

$TemplateObj->page_scripts_footer = $scripts_footer;

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/Requisitions');
?>