<?php 
require_once 'Configuration.inc';

// Get Requisition Information
$req_search_results =   $RequisitionsObj->getRequisitionInformation ( "RequestID, OrgID", array(), "", "", array() );
$requisitions_info  = $req_search_results['results'];
$requisitions_count = count($requisitions_info);
for($i = 0, $j = 1; $i < $requisitions_count; $i++) {
    
        $columns_count  =   "COUNT(ApplicationID) AS ApplicantsCount";
        $where_info     =   array("OrgID    =   :OrgID", "RequestID     =   :RequestID");
        $params_info    =   array(":OrgID"  =>  $requisitions_info[$i]['OrgID'],  ":RequestID"   =>  $requisitions_info[$i]['RequestID']);
    
        $applicants_count_info = $ApplicationsObj->getJobApplicationsInfo($columns_count, $where_info, '', ' EntryDate DESC', array($params_info));
        $applicants_count = $applicants_count_info['results'][0]['ApplicantsCount'];
    
        if($applicants_count > 0) {
            $j++;
            
            $set_info = array("ApplicantsCount = :ApplicantsCount");
            $params_info[":ApplicantsCount"] = $applicants_count;
            $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
        }
    
}
?>