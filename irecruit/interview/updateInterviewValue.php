<?php
require_once '../Configuration.inc';


if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "") && ($_REQUEST['QuestionID'] != "")) {

	$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","QuestionID = :QuestionID");
	$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":QuestionID"=>$_REQUEST['QuestionID']);
	$IQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

	$VALUES = json_decode($IQ['results'][0]['value'],true);

   if ($_REQUEST['action'] == "Add") {

	$VALUES[$_REQUEST['row']]['Value']="";
	$VALUES[$_REQUEST['row']]['Display']="New Value";
	$VALUES[$_REQUEST['row']]['Default']="";
	$VALUES[$_REQUEST['row']]['Preferred']="";

   } else if ($_REQUEST['action'] == "AddYesNo") {

	$VALUES[0]['Value']="";
	$VALUES[0]['Display']="Please Select";
	$VALUES[0]['Default']="Y";
	$VALUES[0]['Preferred']="";

	$VALUES[1]['Value']="Y";
	$VALUES[1]['Display']="Yes";
	$VALUES[1]['Default']="";
	$VALUES[1]['Preferred']="";

	$VALUES[2]['Value']="N";
	$VALUES[2]['Display']="No";
	$VALUES[2]['Default']="";
	$VALUES[2]['Preferred']="";

   } else if ($_REQUEST['action'] == "Add15") {

	$VALUES[0]['Value']="0";
	$VALUES[0]['Display']="0";
	$VALUES[0]['Default']="Y";
	$VALUES[0]['Preferred']="";

	$VALUES[1]['Value']="1";
	$VALUES[1]['Display']="1";
	$VALUES[1]['Default']="";
	$VALUES[1]['Preferred']="";

	$VALUES[2]['Value']="2";
	$VALUES[2]['Display']="2";
	$VALUES[2]['Default']="";
	$VALUES[2]['Preferred']="";

	$VALUES[3]['Value']="3";
	$VALUES[3]['Display']="3";
	$VALUES[3]['Default']="";
	$VALUES[3]['Preferred']="";

	$VALUES[4]['Value']="4";
	$VALUES[4]['Display']="4";
	$VALUES[4]['Default']="";
	$VALUES[4]['Preferred']="";

	$VALUES[5]['Value']="5";
	$VALUES[5]['Display']="5";
	$VALUES[5]['Default']="";
	$VALUES[5]['Preferred']="";

   } else if ($_REQUEST['action'] == "Delete") {

	unset($VALUES[$_REQUEST['row']]);
	$VALUES = array_values($VALUES);

   } else if ($_REQUEST['action'] == "Default") {

	foreach ($VALUES AS $k=>$v) {

	   if ($k == $_REQUEST['row']) {
	       if ($VALUES[$k]['Default'] == "Y") {
		    $VALUES[$k]['Default'] = "";
	       } else {
		    $VALUES[$k]['Default'] = "Y";
	       }
	   } else {
	       $VALUES[$k]['Default'] = "";
	   }
	}

	$VALUES[$_REQUEST['row']]['Preferred'] = $_REQUEST['Preferred'];

   } else if ($_REQUEST['action'] == "Preferred") {


	foreach ($VALUES AS $k=>$v) {

	   if ($k == $_REQUEST['row']) {
	       if ($VALUES[$k]['Preferred'] == "Y") {
		    $VALUES[$k]['Preferred'] = "Y";
	       } else {
		    $VALUES[$k]['Preferred'] = "";
	       }
	   } else {
	       $VALUES[$k]['Preferred'] = "";
	   }
	}

	$VALUES[$_REQUEST['row']]['Preferred'] = $_REQUEST['Preferred'];
	

   } else if ($_REQUEST['action'] == "Value") {

	$VALUES[$_REQUEST['row']]['Value'] = $_REQUEST['newvalue'];

   } else if ($_REQUEST['action'] == "Display") {

	$VALUES[$_REQUEST['row']]['Display'] = $_REQUEST['newvalue'];

   } else if ($_REQUEST['action'] == "Clear") {

	foreach ($VALUES AS $k=>$v) {
	       $VALUES[$k]['Default'] = "";
	       $VALUES[$k]['Preferred'] = "";
	}

   }

   $value=json_encode($VALUES);

   $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'            =>  $_REQUEST['QuestionID'],
                        'value'                 =>  $value
                    );

   $skip   =   array('OrgID', 'RequestID','InterviewFormID','QuestionID');

   G::Obj('Interview')->insUpdInterviewFormQuestions($info, $skip);

}

?>
