<?php
require_once '../Configuration.inc';

// Space
echo '<div style="margin-top:20px;"></div>';

//Create Interview Forms 
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';

echo '<tr>';
echo '<td width="700">';
echo '<a href="javascript:ReverseDisplay(\'AddNew\')">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">';
echo 'Add Interview Form';
echo '</b></a>';

echo '<div id="AddNew" style="display:none;">';

echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';
echo '<tr><td>';

echo '&nbsp;&nbsp;';
echo 'Form Name: <input type="text" class="form-control width-auto-inline" id="FormName" size="30" maxlength="45" value="">';

$AVAILABLEREQS = G::Obj('Interview')->getAvailableRequisitions($OrgID, $_REQUEST['RequestID']);

if ($AVAILABLEREQS['count'] > 0) {

  echo '<br><br>&nbsp;&nbsp;';
  echo '<strong style="font-style:italic;">or</strong> copy form set from requisition';
  echo '&nbsp;';
  echo '<select id="CopyRequestID">';
  echo '<option value="">Please Select</option>';

  foreach ($AVAILABLEREQS['results'] AS $AR) {
    echo '<option value="' . $AR['RequestID'] . '">' . $AR['Title'] . '</option>';
  } // end foreach

  echo '</select>';

} else {

  echo '<input type="hidden" id="CopyRequestID">';
	
} // end count

echo '&nbsp;&nbsp;&nbsp;';
echo '<input type="button" value="Add New Form" class="btn btn-primary" onclick="processNewInterviewForm()">';

echo '</td></tr>' . "\n";
echo '</table>';
echo '</div>';

echo '</td></tr>';
echo '</table>';

// Processing
if ($_REQUEST['Action'] == "delete") {

		echo "<script>";
		echo '$(message).html(\'<span style="color:red;font-style:italic;">Form has been deleted.</span>\');';
		echo "</script>";

} else if ($_REQUEST['Action'] == "newinterviewform") {

	if ($_REQUEST['CopyRequestID'] != "") {

		G::Obj('Interview')->copyInterviewFormsSet($OrgID, $_REQUEST['RequestID'], $_REQUEST['CopyRequestID']);

		echo "<script>";
		echo '$(message).html(\'<span style="color:red;font-style:italic;">Forms have been copied.</span>\');';
		echo "</script>";

	} else if ($_REQUEST['FormName'] != "") {

		$info    =   array(
                        'OrgID'         	=>  $OrgID,
                        'RequestID'       	=>  $_REQUEST['RequestID'],
			'InterviewFormID'       =>  strtotime(date('Y-m-d H:i:s')).uniqid(),
                        'FormName'    		=>  $_REQUEST['FormName'],
                        'Target'       		=>  "",
                        'WhenAvailable'      	=>  "",
                        'SortOrder'    		=>  $_POST['ContactZipCode']
                    );

		$skip   =   array('OrgID', 'RequestID','InterviewFormID');

		G::Obj('Interview')->insUpdInterviewForms($info, $skip);

		echo "<script>";
		echo '$(message).html(\'<span style="color:red;font-style:italic;">New form has been created.</span>\');';
		echo "</script>";
	}

} // end Action

echo '<div id="message" style="margin-top:-15px;margin-bottom:5px;"></div>';

//Display Interview Forms 
$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$IFS =   G::Obj('Interview')->getInterviewForms("*", $where_info, "SortOrder ASC", $params);


if (($IFS['count'] > 0) && (is_array($IFS['results']))) {

echo '<div style="margin: 0 0 40px 0;">';
echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" id="sort_interview_forms" class="table table-striped table-bordered">' . "\n";

echo '<thead>';
echo '<tr>';
echo '<td width="330"><b>Form Name</b></td>';
echo '<td width="120"><b>Target Group</b></td>';
echo '<td width="120"><b>When Available</b></td>';
echo '<td align="center" width="100"><b>Questions</b></td>';
echo '<td align="center" width="60"><b>Copy</b></td>';
echo '<td align="center" width="60"><b>Delete</b></td>';
echo '<td align="center" width="60"><b>View<br>Form</b></td>';
echo '</tr>';
echo '</thead>' . "\n";

$WHENAVAIL = G::Obj('Interview')->getInterviewWhenAvailable();
$TARGET = G::Obj('Interview')->getInterviewTarget();

$rowcolor = "#eeeeee";

	$i=0;
	foreach ($IFS['results'] as $F) {
	$i++;

		$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID != ''");
		$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);

		$IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

		echo '<tbody class="ui-sortable">';
	
		echo '<tr id="' . $F['InterviewFormID'] . '" class="ui-sortable-handle">';
		echo '<td bgcolor="'.$rowcolor.'">';
		echo '<input type="text" id="FormName" value="' . $F['FormName'] . '" size="60" onchange="updateInterviewForm(\'FormName\',\''.$F['RequestID'].'\',\''.$F['InterviewFormID'].'\',this.value)">';
		echo '</td>';

		echo '<td bgcolor="'.$rowcolor.'">';
		echo '<select id="Target" onchange="updateInterviewForm(\'Target\',\''.$F['RequestID'].'\',\''.$F['InterviewFormID'].'\',this.value)">';
		echo '<option value="">Inactive</option>';
		foreach ($TARGET AS $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($F['Target'] == $k) { echo " selected"; }
			echo '>' . $v. '</option>';
		}
		echo '</select>';
		echo '</td>';

		echo '<td bgcolor="'.$rowcolor.'">';
		echo '<select id="WhenAvailable" onchange="updateInterviewForm(\'WhenAvailable\',\''.$F['RequestID'].'\',\''.$F['InterviewFormID'].'\',this.value)">';
		echo '<option value="">Inactive</option>';
		foreach ($WHENAVAIL AS $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($F['WhenAvailable'] == $k) { echo " selected"; }
			echo '>' . $v. '</option>';
		}
		echo '</select>';
		echo '</td>';
	
		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<a href="" onclick="getInterviewQuestions(\'' . $F['InterviewFormID'] . '\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit" style="padding:5px;"></a>';
		echo '</td>';
	
		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		if ($IFQ['count'] > 0) {

		echo '<a href="" onclick="copyInterviewForm(\''.$F['RequestID'] . '\',\'' . $F['InterviewFormID'] . '\',\'' . $F['FormName'] . '\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy" style="padding:5px;"></a>';

		} else {
			echo "-";
		}
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<a href="" onclick="deleteInterviewForm(\''.$F['RequestID'] . '\',\'' . $F['InterviewFormID'] . '\',\'' . $F['FormName'] . '\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="padding:5px;"></a>';
		echo '</td>';
	
		echo '<td bgcolor="' . $rowcolor . '" align="center">';
	
		if ($IFQ['count'] > 0) {

		   echo '<a href="#" onclick="getInterviewFormView(\''.$F['RequestID'].'\',\''.$F['InterviewFormID'].'\');">';
		   echo '<img src="' . IRECRUIT_HOME . 'images/icons/application_form.png" border="0" title="View Form" style="margin:0px 3px -4px 0px;"></a>';

		} else {
			echo "-";
		}

		echo '</td>';

		echo '</tr>';
		echo '</tbody>' . "\n";

	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}

	} // end foreach

echo '</table>';
echo '</div>';

}  else { // else count > 0
	
	echo '<div style="margin:25px 0 40px 10px;">';
	echo 'There are no forms assigned to this requisition.';
	echo '</div>';
}

?>
<script>
$(document).ready(function() {
   $("#sort_interview_forms").sortable({

        cursor: 'move',
        placeholder: 'ui-state-highlight',
        stop: function( event, ui ) {

        var RequestID = document.frmRequisitionDetailInfo.RequestID.value;

        var forms_data = {
                forms_info: []
        };

        $(this).find('tr').each(function(i) {

            var SortOrder = i;

            var InterviewFormID = $(this).attr('id');

            if(typeof(InterviewFormID) !== 'undefined') {

                forms_data.forms_info.push({
                         "RequestID" : RequestID,
                         "InterviewFormID" : InterviewFormID,
                         "SortOrder"  : SortOrder
                });

              SortOrder++;
            }

        });

        updateInterviewFormSortOrder(forms_data);
    }
  });
} );
</script>
