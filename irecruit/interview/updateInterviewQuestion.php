<?php
require_once '../Configuration.inc';

if ($_REQUEST['ChangeType'] == "Question") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'       	=>  $_REQUEST['QuestionID'],
                        'Question'              =>  $_REQUEST['Value']
                    );

} else if ($_REQUEST['ChangeType'] == "QuestionTypeID") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'       	=>  $_REQUEST['QuestionID'],
                        'QuestionTypeID'        =>  $_REQUEST['Value'],
			'Required'        	=>  "",
			'value'        		=>  ""
                    );
  if ($_REQUEST['Value'] == "DIVIDER") { $info['Question']=""; }

} else if ($_REQUEST['ChangeType'] == "Active") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'       	=>  $_REQUEST['QuestionID'],
                        'Active'         	=>  $_REQUEST['Value']
                    );

} else if ($_REQUEST['ChangeType'] == "Required") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'       	=>  $_REQUEST['QuestionID'],
                        'Required'         	=>  $_REQUEST['Value']
                    );

}

$skip   =   array('OrgID', 'RequestID','InterviewFormID','QuestionID');

if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "") && ($_REQUEST['QuestionID'] != "")) {
  G::Obj('Interview')->insUpdInterviewFormQuestions($info, $skip);
}

?>
