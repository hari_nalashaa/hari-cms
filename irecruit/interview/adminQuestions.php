<?php
require_once '../Configuration.inc';

// Space
echo '<div style="margin-top:20px;"></div>';

$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);
$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID']);
$IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);
$QUESTIONTYPEIDS = G::Obj('Interview')->getInterviewQuestionTypes();
echo '<input type="hidden" id="QuestionTypeIDs" value=\''.json_encode($QUESTIONTYPEIDS).'\'>';


echo '<div style="margin:0 0 10px 0;">';
echo '<a href="" onclick="addInterviewQuestion(\''.$_REQUEST['RequestID'] . '\',\'' . $_REQUEST['InterviewFormID'] . '\');return false;">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Add Question</b>';
echo '</a>';
echo '</div>';

//Title and Back Bar
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'];
echo '</td>';
echo '<td width="100" align="right">';
echo '<a href="" onclick="getInterviewAdmin();return false;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
echo '</a>';
echo '</td></tr>';
echo '<tr>';
echo '</table>';

echo '<div id="message" style="margin-top:-15px;margin-bottom:10px;"></div>';

if (($IFQ['count'] > 0) && (is_array($IFQ['results']))) {

echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" id="sort_interview_questions" class="table table-striped table-bordered">' . "\n";

echo '<thead>';
echo '<tr>';
echo '<td width="300"><b>Question</b></td>';
echo '<td align="center" width="120"><b>Question Type</b></td>';
echo '<td align="center" width="60"><b>Active</b></td>';
echo '<td align="center" width="10"><b>Required</b></td>';
echo '<td align="center" width="100"><b>Values</b></td>';
echo '<td align="center" width="40"><b>Delete</b></td>';
echo '</tr>';
echo '</thead>' . "\n";

	$rowcolor = "#eeeeee";

	$i=0;
	foreach ($IFQ['results'] as $IQ) {
	$i++;

		echo '<tbody class="ui-sortable">';
		echo '<input type="hidden" id="InterviewFormID" value="'.$IQ['InterviewFormID'].'">';
	
		echo '<tr id="' . $IQ['QuestionID'] . '" class="ui-sortable-handle">';
		echo '<td bgcolor="'.$rowcolor.'">';
		if ($IQ['QuestionTypeID'] == "DIVIDER") {
		  echo '<hr size="1">';
		} else {
		echo '<textarea id="Question" rows="1" cols="90" onchange="updateInterviewQuestion(\'Question\',\''.$IQ['RequestID'].'\',\''.$IQ['InterviewFormID'].'\',\''.$IQ['QuestionID'].'\',this.value,\'\')">';
		echo $IQ['Question'];
		echo '</textarea>';
		}
		echo '</td>';

		echo '<td bgcolor="'.$rowcolor.'">';
		echo '<select id="QuestionTypeID" onchange="updateInterviewQuestion(\'QuestionTypeID\',\''.$IQ['RequestID'].'\',\''.$IQ['InterviewFormID'].'\',\''.$IQ['QuestionID'].'\',this.value,this)">';
		echo '<option value="">Please Select</option>';
		foreach ($QUESTIONTYPEIDS AS $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($IQ['QuestionTypeID'] == $k) { echo " selected"; }
			echo '>' . $v['Description']. '</option>';
		}
		echo '</select>';
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<input type="checkbox" value="Y"';
		if ($IQ['Active'] == "Y") { echo ' checked'; }
	        echo ' onclick="updateInterviewQuestion(\'Active\',\''.$IQ['RequestID'].'\',\''.$IQ['InterviewFormID'].'\',\''.$IQ['QuestionID'].'\',this.value,this)">';
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		if ($QUESTIONTYPEIDS[$IQ['QuestionTypeID']]['Required'] == "Y") {
		  echo '<input type="checkbox" value="Y"';
		  if ($IQ['Required'] == "Y") { echo ' checked'; }
	          echo ' onclick="updateInterviewQuestion(\'Required\',\''.$IQ['RequestID'].'\',\''.$IQ['InterviewFormID'].'\',\''.$IQ['QuestionID'].'\',this.value,this)">';
		} else {
		  echo '-';
		}
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		if ($QUESTIONTYPEIDS[$IQ['QuestionTypeID']]['Values'] == "Y") {
			if ($IQ['QuestionTypeID'] == "TOTAL") {
				echo '<a href="" onclick="determineInterviewTotal(\''.$IQ['RequestID'] . '\',\'' . $IQ['InterviewFormID'] . '\',\'' . $IQ['QuestionID'] . '\',\''.str_replace("\n", ' ', $IQ['Question']).'\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Determine Total Questions" style="padding:5px;"></a>';
			} else {
				echo '<a href="" onclick="getInterviewValues(\''.$IQ['RequestID'] . '\',\'' . $IQ['InterviewFormID'] . '\',\'' . $IQ['QuestionID'] . '\',\''.str_replace("\n", ' ', $IQ['Question']).'\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Add Values" style="padding:5px;"></a>';
			}
		} else {
		  echo '-';
		}
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<a href="" onclick="deleteInterviewQuestion(\''.$IQ['RequestID'] . '\',\'' . $IQ['InterviewFormID'] . '\',\'' . $IQ['QuestionID'] . '\',\''.str_replace("\n", ' ', $IQ['Question']).'\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete Question" style="padding:5px;"></a>';
		echo '</td>';
	

		echo '</tr>';
		echo '</tbody>' . "\n";

		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}

	} // end foreach

echo '</table>';

}  else { // else count > 0
	
	echo '<div style="margin-top:25px;padding-left:10px;">';
	echo 'There are no questions for this form.';
	echo '</div>';
}

?>
<script>
$(document).ready(function() {
   $("#sort_interview_questions").sortable({

        cursor: 'move',
        placeholder: 'ui-state-highlight',
        stop: function( event, ui ) {

        var RequestID = document.frmRequisitionDetailInfo.RequestID.value;
	var InterviewFormID = document.getElementById("InterviewFormID").value;

        var forms_data = {
                forms_info: []
        };


        $(this).find('tr').each(function(i) {

            var QuestionOrder = i;

            var QuestionID = $(this).attr('id');

            if(typeof(QuestionID) !== 'undefined') {

                forms_data.forms_info.push({
                         "RequestID" : RequestID,
                         "InterviewFormID" : InterviewFormID,
                         "QuestionID" : QuestionID,
                         "QuestionOrder"  : QuestionOrder
                });

              QuestionOrder++;
            }

        });

        updateInterviewFormQuestionSortOrder(forms_data,InterviewFormID);
    }
  });
} );
</script>
