<?php
require_once '../Configuration.inc';

if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "")) {

	$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID != ''");
        $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID']);

	$IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

	$row = $IFQ['count'] + 1;

	$info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'       	=>  strtotime(date('Y-m-d H:i:s')).uniqid(),
                        'Question'       	=>  "New Question",
                        'QuestionTypeID'       	=>  "TEXTAREA",
                        'Active'       		=>  "Y",
                        'QuestionOrder'       	=>  $row 
                    );

                $skip   =   array('OrgID', 'RequestID','InterviewFormID','QuestionID');

	G::Obj('Interview')->insUpdInterviewFormQuestions($info, $skip);

}

?>
