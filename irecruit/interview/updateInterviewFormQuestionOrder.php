<?php
require_once '../Configuration.inc';

foreach ($_REQUEST['forms_info'] AS $FD) {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $FD['RequestID'],
                        'InterviewFormID'       =>  $FD['InterviewFormID'],
                        'QuestionID'       	=>  $FD['QuestionID'],
                        'QuestionOrder'         =>  $FD['QuestionOrder']
                    );

  $skip   =   array('OrgID', 'RequestID','InterviewFormID','QuestionID');

  if (($FD['RequestID'] != "") && ($FD['InterviewFormID'] != "")) {
    G::Obj('Interview')->insUpdInterviewFormQuestions($info, $skip);
  }

} // end foreach

?>
