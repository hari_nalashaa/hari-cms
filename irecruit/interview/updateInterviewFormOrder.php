<?php
require_once '../Configuration.inc';

foreach ($_REQUEST['forms_info'] AS $FD) {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $FD['RequestID'],
                        'InterviewFormID'       =>  $FD['InterviewFormID'],
                        'SortOrder'             =>  $FD['SortOrder']
                    );

  $skip   =   array('OrgID', 'RequestID','InterviewFormID');

  if (($FD['RequestID'] != "") && ($FD['InterviewFormID'] != "")) {
    G::Obj('Interview')->insUpdInterviewForms($info, $skip);
  }

} // end foreach

?>
