<?php
require_once '../Configuration.inc';

if (($OrgID != "") && ($_REQUEST['RequestID'] != "")) {

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$IA =   G::Obj('Interview')->getInterviewApplicants("*", $where_info, "", $params);
$VALUES = json_decode($IA['results'][0]['InterviewAppIDs'],true);

$VALUES[]=$_REQUEST['ApplicationID'];

if ($_REQUEST['Value'] != "Y") {
      foreach($VALUES AS $k=>$v) {
        if ($v == $_REQUEST['ApplicationID']) {
           unset($VALUES[$k]);
        }
      }
}

$VALUES = array_values($VALUES);

 $value=json_encode($VALUES);


 $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewAppIDs'       =>  $value
                    );

 $skip   =   array('OrgID', 'RequestID');

 G::Obj('Interview')->insUpdInterviewApplicants($info, $skip);

}

?>
