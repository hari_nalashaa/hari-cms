<?php
require_once '../Configuration.inc';

// Space
echo '<div style="margin-top:20px;"></div>';

//Title and Back Bar
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Interview Results</strong>';
echo '</td>';
echo '<td width="150" align="right">';

//echo '<a href="" onclick="exportInterviewResults(\''.$_REQUEST['RequestID'].'\');return false;">';
echo '<a href="'.IRECRUIT_HOME.'interview/exportResults.php?RequestID='.$_REQUEST['RequestID'].'">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/sitemap_color.png" title="Export Data" style="margin:0px 3px -4px 0px;" border="0">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Export Data</b>';
echo '</a>';

echo '</td></tr>';
echo '</table>';

echo '<div id="message" style="margin-top:-15px;margin-bottom:5px;"></div>';

$MANAGERS =   G::Obj('Interview')->getManagerUserIDs($OrgID, $_REQUEST['RequestID']);

$Owner    =   G::Obj('Interview')->getOwnerID($OrgID, $_REQUEST['RequestID']);

$MultiOrgID = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
$ReqTitle = G::Obj('RequisitionDetails')->getJobTitle($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqJobID = G::Obj('RequisitionDetails')->getReqJobIDs($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqTitle .= ' (' . $ReqJobID . ')';

$where    =   array ("OrgID = :OrgID", "RequestID = :RequestID");
$params   =   array (":OrgID" => $OrgID, ":RequestID" => $_REQUEST['RequestID']);
$results = G::Obj('Applications')->getJobApplicationsInfo ( '*', $where, '', 'ApplicantSortName ASC', array ($params) );
$APPLICANTS = $results['results'];

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$IFS =   G::Obj('Interview')->getInterviewForms("*", $where_info, "SortOrder ASC", $params);

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$IA =   G::Obj('Interview')->getInterviewApplicants("*", $where_info, "", $params);
$INTERVIEW = json_decode($IA['results'][0]['InterviewAppIDs'],true);

echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';

if (count($APPLICANTS) > 0) {

echo '<thead>';

echo '<tr>';

echo '<td colspan="3"><b>' . $ReqTitle . '</b></td>';

foreach ($IFS['results'] AS $F) {

	$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID in ('TOTAL','PREFERRED')");
        $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);
        $IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionTypeID, QuestionOrder", $params);
	if ($IFQ['count'] > 0) {
	  echo '<td align="center"';
	  if ($F['Target'] != "Manager") {
		echo ' colspan="'.($IFQ['count'] + 1) .'"';
	  } else  {
		echo ' colspan="'. ((count($MANAGERS) * $IFQ['count']) + 3) .'"';
	  }
	  echo '>';
	  echo '<b>' . $F['FormName'] . '</b>';
	  echo '</td>';
	}
}
echo '</tr>';

echo '<tr>';

echo '<td align="left">';
echo '<b>Applicant Name</b>';
echo '</td>';

echo '<td align="center">';
echo '<b>ApplicationID</b>';
echo '</td>';

echo '<td align="center">';
echo '<b>Interview</b>';
echo '</td>';

foreach ($IFS['results'] AS $F) {

  $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID in ('TOTAL','PREFERRED')");
  $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);
  $IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionTypeID, QuestionOrder", $params);


 if ($IFQ['count'] > 0) {
 if ($F['Target'] == "Admin") {

    echo '<td align="center" colspan="'.($IFQ['count']+1).'">';
    echo '<b>Interview Admin</b>';
    echo '</td>';

 } else if ($F['Target'] == "Owner") {

    echo '<td align="center" colspan="'.($IFQ['count']+1).'">';
    echo '<b>' . G::Obj('IrecruitUsers')->getUsersName($OrgID, $Owner) . '</b>';
    echo '</td>';

 } else if ($F['Target'] == "Manager") {

  foreach ($MANAGERS AS $M) {
    echo '<td align="center" colspan="'.($IFQ['count']+1).'">';
    echo '<b>' . G::Obj('IrecruitUsers')->getUsersName($OrgID, $M) . '</b>';
    echo '</td>';
  }

  echo '<td align="center">';
  echo '<b>Total</b>';
  echo '</td>';

 }
 }
}

echo '</thead>' . "\n";


foreach ($APPLICANTS AS $A) {
echo '<tr>';

echo '<td align="left">';
echo $A['ApplicantSortName'];
echo '</td>';

echo '<td align="center">';
echo '<a href="'.IRECRUIT_HOME.'applicantsSearch.php?ApplicationID='.$A['ApplicationID'].'&amp;RequestID='.$_REQUEST['RequestID'].'" target="_blank">';
echo $A['ApplicationID'];
echo '</a>';
echo '</td>';

echo '<td align="center">';
echo '<input type="checkbox" value="Y"';
if (in_array($A['ApplicationID'],$INTERVIEW)) { echo ' checked'; }
echo ' onclick="updateInterviewApplicants(\''.$F['RequestID'].'\',\''.$A['ApplicationID'].'\',this.value,this)">';
echo '</td>';

foreach ($IFS['results'] AS $F) {

   $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID in ('TOTAL','PREFERRED')");
   $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);
   $IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionTypeID, QuestionOrder", $params);


 if ($IFQ['count'] > 0) {
 if ($F['Target'] == "Manager") {

     $Total=0;
     foreach ($MANAGERS AS $M) {

   	echo '<td align="center">';
	echo G::Obj('Interview')->displayFormCell($OrgID, $F, $A, $M);
   	echo '</td>';

	foreach ($IFQ['results'] AS $Q) {

    	   echo '<td align="center">';
           list($rtn,$Answer,$t) = G::Obj('Interview')->displayAnswerCell($OrgID, $F,$Q, $A, $M);
	   $Total+=$t;
	   echo $rtn;
    	   echo '</td>';
	} // end foreach

     } // end foreach

     echo '<td align="center">';
     echo $Total;
     echo '</td>';

 } else {

   $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "QuestionID = 'UserID'");
   $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID'],":ApplicationID"=>$A['ApplicationID']);
   $IFD =   G::Obj('Interview')->getInterviewFormData("Answer", $where_info, "", $params);
   $UserID = $IFD['results'][0]['Answer'];

   echo '<td align="center">';
   echo G::Obj('Interview')->displayFormCell($OrgID, $F, $A, $UserID);
   echo '</td>';

   foreach ($IFQ['results'] AS $Q) {

      echo '<td align="center">';
      list($rtn,$Answer, $t) = G::Obj('Interview')->displayAnswerCell($OrgID, $F,$Q, $A, $UserID);
      echo $rtn;
      echo '</td>';

   } // end foreach

 } // end else Manager

 } // end if count

} // end foreach

echo '</tr>';
} // end foreach

echo '</table>' . "\n";

} // end count

echo '<hr>' . "\n";
echo '<br>' . "\n";

?>
