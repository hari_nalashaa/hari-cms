<?php
require_once '../Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$MANAGERS =   G::Obj('Interview')->getManagerUserIDs($OrgID, $_REQUEST['RequestID']);
$Owner    =   G::Obj('Interview')->getOwnerID($OrgID, $_REQUEST['RequestID']);

$where    =   array ("OrgID = :OrgID", "RequestID = :RequestID");
$params   =   array (":OrgID" => $OrgID, ":RequestID" => $_REQUEST['RequestID']);
$results = G::Obj('Applications')->getJobApplicationsInfo ( '*', $where, '', 'ApplicantSortName ASC', array ($params) );
$APPLICANTS = $results['results'];

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$IFS =   G::Obj('Interview')->getInterviewForms("*", $where_info, "SortOrder ASC", $params);

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$IA =   G::Obj('Interview')->getInterviewApplicants("*", $where_info, "", $params);
$INTERVIEW = json_decode($IA['results'][0]['InterviewAppIDs'],true);

$MultiOrgID = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
$ReqTitle = G::Obj('RequisitionDetails')->getJobTitle($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqJobID = G::Obj('RequisitionDetails')->getReqJobIDs($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqTitle .= ' (' . $ReqJobID . ')';

$ALPHA      =   G::Obj('GenericLibrary')->getAlphabets();
$i=26;
foreach ($ALPHA AS $A) {

	$ALPHA[$i]='A' . $A;
        $i++;
}

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("iRecruit")
       ->setLastModifiedBy("System")
       ->setTitle("Interview Export")
       ->setSubject("Excel")
       ->setDescription("Export File")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

// Header Row One
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
$objPHPExcel->getActiveSheet()->getStyle('A1:C1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue($ReqTitle);

$row=2;
foreach ($IFS['results'] AS $F) {

	$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID in ('TOTAL','PREFERRED')");
        $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);
        $IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionTypeID, QuestionOrder", $params);

	if ($IFQ['count'] > 0) {
	  $row++;
	  $startrow=$row;
	  if ($F['Target'] == "Admin") {
		  $row += $IFQ['count'];
  	  } else if ($F['Target'] == "Owner") {
		  $row += ($IFQ['count'] -1);
	  } else if ($F['Target'] == "Manager") {
		  $row += (count($MANAGERS) * $IFQ['count']);
	  } 
	  $endrow=$row;

	  $startcell=$ALPHA[$startrow] . '1';
	  $endcell=$ALPHA[$endrow] . '1';
	  $formname=$F['FormName'];

	  $objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setHorizontal('center');
	  $objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setVertical('center');
          $objPHPExcel->getActiveSheet()->mergeCells($startcell.':'.$endcell);
          $objPHPExcel->getActiveSheet()->getCell($startcell)->setValue($formname);

	}
}

// Header Row Two
$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('B2:C2')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A2:C2')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell('A2')->setValue('Applicant Name');
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue('ApplicationID');
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue('Interview');


$row=2;
foreach ($IFS['results'] AS $F) {

  $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID in ('TOTAL','PREFERRED')");
  $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);
  $IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionTypeID, QuestionOrder", $params);

 if ($IFQ['count'] > 0) {
  $row++;
  if ($F['Target'] == "Admin") {
  	$startrow=$row;
	$row += $IFQ['count'];
	$User = 'Interview Admin';
	$endrow=$row;
	$startcell=$ALPHA[$startrow] . '2';
	$endcell=$ALPHA[$endrow] . '2';

	$objPHPExcel->getActiveSheet()->mergeCells($startcell.':'.$endcell);
	$objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setHorizontal('center');
	$objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setVertical('center');
        $objPHPExcel->getActiveSheet()->getCell($startcell)->setValue($User);


  } else if ($F['Target'] == "Owner") {

  	$startrow=$row;
	$row += ($IFQ['count'] -1);
	$User = G::Obj('IrecruitUsers')->getUsersName($OrgID, $Owner);
	$endrow=$row;
	$startcell=$ALPHA[$startrow] . '2';
	$endcell=$ALPHA[$endrow] . '2';

	$objPHPExcel->getActiveSheet()->mergeCells($startcell.':'.$endcell);
	$objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setHorizontal('center');
	$objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setVertical('center');
        $objPHPExcel->getActiveSheet()->getCell($startcell)->setValue($User);

  } else if ($F['Target'] == "Manager") {

    foreach ($MANAGERS AS $M) {

  	$startrow=$row;
	$row += ($IFQ['count'] -1);
	$User = G::Obj('IrecruitUsers')->getUsersName($OrgID, $M);
	$endrow=$row;
	$startcell=$ALPHA[$startrow] . '2';
	$endcell=$ALPHA[$endrow] . '2';
	$row++;

	$objPHPExcel->getActiveSheet()->mergeCells($startcell.':'.$endcell);
	$objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setHorizontal('center');
	$objPHPExcel->getActiveSheet()->getStyle($startcell.':'.$endcell)->getAlignment()->setVertical('center');
        $objPHPExcel->getActiveSheet()->getCell($startcell)->setValue($User);

    }

    $totalcell=$ALPHA[$row] . '2';

    $objPHPExcel->getActiveSheet()->getCell($totalcell)->setValue('Total');
    $objPHPExcel->getActiveSheet()->getStyle($totalcell)->getAlignment()->setHorizontal('center');
    $objPHPExcel->getActiveSheet()->getStyle($totalcell)->getAlignment()->setVertical('center');

 } // end elses

 } // end count
} // end foreach

for ($i = 'A'; $i != $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
  $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setWidth(20);
}
  $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setWidth(20);

// Applicant and Detail Rows
  $n=2;
  foreach ($APPLICANTS AS $A) {
    $n++;

        $objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($A['ApplicantSortName']);
        $objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($A['ApplicationID']);

	if (in_array($A['ApplicationID'],$INTERVIEW)) { $int = 'Yes'; } else { $int = 'No'; }
        $objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue($int);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$n)->getAlignment()->setHorizontal('center');
        $objPHPExcel->getActiveSheet()->getStyle('C'.$n)->getAlignment()->setHorizontal('center');
        $objPHPExcel->getActiveSheet()->getStyle('C'.$n)->getAlignment()->setVertical('center');

$row=3;
foreach ($IFS['results'] AS $F) {

   $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'","QuestionTypeID in ('TOTAL','PREFERRED')");
   $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID']);
   $IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionTypeID, QuestionOrder", $params);

 if ($IFQ['count'] > 0) {
 if ($F['Target'] == "Manager") {

     $Total=0;
     foreach ($MANAGERS AS $M) {

	foreach ($IFQ['results'] AS $Q) {

        $cell=$ALPHA[$row] . $n;
        $row++;
        list($rtn, $Answer, $t) = G::Obj('Interview')->displayAnswerCell($OrgID, $F,$Q, $A, $M);

        $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($Answer);

        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal('center');
	$Total += $t;

	} // end foreach

     } // end foreach

        $cell=$ALPHA[$row] . $n;
        $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($Total);
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal('center');

 } else {

   $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "QuestionID = 'UserID'");
   $params =   array(":OrgID"=>$OrgID, ":RequestID"=>$F['RequestID'], ":InterviewFormID"=>$F['InterviewFormID'],":ApplicationID"=>$A['ApplicationID']);
   $IFD =   G::Obj('Interview')->getInterviewFormData("Answer", $where_info, "", $params);
   $UserID = $IFD['results'][0]['Answer'];

   if ($F['Target'] == "Admin") {
        $cell=$ALPHA[$row] . $n;
        $row++;
	$User = G::Obj('IrecruitUsers')->getUsersName($OrgID,$UserID);
        $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($User);
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal('center');
   }

   foreach ($IFQ['results'] AS $Q) {
        $cell=$ALPHA[$row] . $n;
        $row++;
        list($rtn, $Answer, $t) = G::Obj('Interview')->displayAnswerCell($OrgID, $F,$Q, $A, $UserID);
        $objPHPExcel->getActiveSheet()->getCell($cell)->setValue($Answer);
        $objPHPExcel->getActiveSheet()->getStyle($cell)->getAlignment()->setHorizontal('center');

   } // end foreach

 } // end else Manager

 } // end if count

} // end foreach


  } // end foreach

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Last Activity");

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="interview_export.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();


?>
