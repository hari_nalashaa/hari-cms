<?php
require_once '../Configuration.inc';

$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active='Y'","QuestionTypeID not in ('TOTAL','PREFERRED')");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID']);
$IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);
$QUESTIONTYPEIDS = G::Obj('Interview')->getInterviewQuestionTypes();
$WHENAVAIL = G::Obj('Interview')->getInterviewWhenAvailable();
$TARGET = G::Obj('Interview')->getInterviewTarget();

// Space
echo '<div style="margin-top:20px;"></div>';

// header and back
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'] . '<br>';
echo '<strong>Target Group:</strong> ' . $TARGET[$IF['results'][0]['Target']] . '<br>';
echo '<strong>When Available:</strong> ' . $WHENAVAIL[$IF['results'][0]['WhenAvailable']];
echo '</td>';
echo '<td width="100" align="right">';
echo '<a href="" onclick="getInterviewAdmin();return false;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
echo '</a>';
echo '</td></tr>';
echo '</table>';

$FORMDATA=array();

echo '<div style="margin:20px 0 40px 40px;line-height:180%">';
foreach ($IFQ['results'] AS $FQ) {

	echo G::Obj('Interview')->displayQuestion($FQ,$FORMDATA);

} // end foreach
echo '</div>' . "\n";
?>
