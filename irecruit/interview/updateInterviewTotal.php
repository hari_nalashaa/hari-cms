<?php
require_once '../Configuration.inc';

if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "") && ($_REQUEST['QuestionID'] != "")) {

   $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","QuestionID = :QuestionID");
   $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":QuestionID"=>$_REQUEST['QuestionID']);
   $IQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

   $VALUES = json_decode($IQ['results'][0]['value'],true);

   if ($_REQUEST['change'] == "add") {
      $VALUES[]=$_REQUEST['ChangeValue'];
   } else if ($_REQUEST['change'] == "clear") {
      foreach($VALUES AS $k=>$v) {
	if ($v == $_REQUEST['ChangeValue']) {
	   unset($VALUES[$k]);
	}
      }
   } 

   $VALUES = array_values($VALUES);

   $value=json_encode($VALUES);

   $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'QuestionID'            =>  $_REQUEST['QuestionID'],
                        'value'                 =>  $value
                    );

   $skip   =   array('OrgID', 'RequestID','InterviewFormID','QuestionID');

   G::Obj('Interview')->insUpdInterviewFormQuestions($info, $skip);

}
?>
