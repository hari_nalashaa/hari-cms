<?php
require_once '../Configuration.inc';

$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active='Y'");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID']);
$IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "UserID = :UserID");
$params =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":ApplicationID"=>$_REQUEST['ApplicationID'], ":UserID"=>$_REQUEST['UserID']);
$IFD =   G::Obj('Interview')->getInterviewFormData("QuestionID, Answer", $where_info, "", $params);
$FORMDATA=array();
foreach ($IFD['results'] AS $D) {
        $FORMDATA[$D['QuestionID']]=$D['Answer'];
}

// Space
echo '<div style="margin-top:20px;"></div>';

// header and back
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Applicant:</strong> ' . $_REQUEST['ApplicationID'] . ' - ' . G::Obj('ApplicantDetails')->getApplicantName($OrgID, $_REQUEST['ApplicationID']) . '<br>';

$MultiOrgID = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
$ReqTitle = G::Obj('RequisitionDetails')->getJobTitle($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqJobID = G::Obj('RequisitionDetails')->getReqJobIDs($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqTitle .= ' (' . $ReqJobID . ')';

echo '<strong>Requisition:</strong> ' . $ReqTitle . '<br>';
echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'] . '<br>';
echo '<strong>Submitted By:</strong> ' . G::Obj('IrecruitUsers')->getUsersName($OrgID,$_REQUEST['UserID']) . '<br>';
echo '<strong>Last Modified:</strong> ' . date_format(date_create($FORMDATA['LastModified']),"m/d/Y g:i A");
echo '</td>';
echo '<td width="100" align="right">';
echo '<a href="" onclick="getInterviewResults();return false;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
echo '</a>';
echo '</td></tr>';
echo '</table>';


echo '<div style="margin:20px 0 40px 40px;line-height:180%">';
foreach ($IFQ['results'] AS $FQ) {

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "UserID = :UserID", "ApplicationID = :ApplicationID", "QuestionID = :QuestionID");
$params =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":UserID"=>$_REQUEST['UserID'], ":ApplicationID"=>$_REQUEST['ApplicationID'], ":QuestionID"=>$FQ['QuestionID']);
$IFD =   G::Obj('Interview')->getInterviewFormData("*", $where_info, "", $params);
$QUESTIONDATA=$IFD['results'][0];

	echo G::Obj('Interview')->displayFormResults($FQ,$QUESTIONDATA);

} // end foreach
echo '</div>' . "\n";


echo '<div style="margin:20px 0 40px 40px;line-height:180%">';
echo '<a href="" onclick="deleteInterviewFormData(\''.$_REQUEST['RequestID'] . '\',\'' . $_REQUEST['InterviewFormID'] . '\',\'' . $_REQUEST['UserID'] . '\',\'' . G::Obj('IrecruitUsers')->getUsersName($OrgID,$_REQUEST['UserID']) . '\',\''.$_REQUEST['ApplicationID'].'\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="padding:5px;">Delete Data</a>';
echo '&nbsp;&nbsp;&nbsp;';
echo '<a href="" onclick="editInterviewForm(\''.$_REQUEST['RequestID'] . '\',\'' . $_REQUEST['InterviewFormID'] . '\',\'' . $_REQUEST['UserID'] . '\',\''.$_REQUEST['ApplicationID'].'\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit Data" style="padding:5px;">Edit Form</a>';
echo '&nbsp;&nbsp;&nbsp;';
echo '<a href="" onclick="viewInterviewFormHistory(\''.$_REQUEST['RequestID'] . '\',\'' . $_REQUEST['InterviewFormID'] . '\',\'' . $_REQUEST['UserID'] . '\',\''.$_REQUEST['ApplicationID'].'\');return false;"><img src="' . IRECRUIT_HOME . 'images/icons/script.png" border="0" title="View History" style="padding:5px;">View History</a>';
echo '</div>' . "\n";


?>
