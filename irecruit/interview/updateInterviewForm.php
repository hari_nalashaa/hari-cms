<?php
require_once '../Configuration.inc';

if ($_REQUEST['ChangeType'] == "FormName") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'FormName'              =>  $_REQUEST['Value']
                    );

} else if ($_REQUEST['ChangeType'] == "Target") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'Target'                =>  $_REQUEST['Value']
                    );

} else if ($_REQUEST['ChangeType'] == "WhenAvailable") {

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'WhenAvailable'         =>  $_REQUEST['Value']
                    );

}

$skip   =   array('OrgID', 'RequestID','InterviewFormID');

if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "")) {
  G::Obj('Interview')->insUpdInterviewForms($info, $skip);
}

?>
