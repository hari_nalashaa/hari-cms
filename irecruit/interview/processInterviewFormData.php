<?php
require_once '../Configuration.inc';

if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "") && ($_REQUEST['ApplicationID'] != "")) {

   if ($_REQUEST['process_status'] == 'edit') {
	   $UID=$_REQUEST['uid'];
   } else {
	   $UID=$USERID;
   }

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","Active = 'Y'");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID']);
$IFQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);
$skip   =   array('OrgID', 'RequestID','InterviewFormID','ApplicationID','QuestionID');

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "UserID = :UserID");
$params =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":ApplicationID"=>$_REQUEST['ApplicationID'], ":UserID"=>$UID);
$IFD =   G::Obj('Interview')->getInterviewFormData("QuestionID, Answer", $where_info, "", $params);
$FORMDATA=array();
foreach ($IFD['results'] AS $D) {
	$FORMDATA[$D['QuestionID']]=$D['Answer'];
}
$updatedFields="";


   foreach ($IFQ['results'] AS $FQ) {

	$Answer=$_REQUEST[$FQ['QuestionID']];

	if ($FQ['QuestionTypeID'] == 'TOTAL') {

		$total=0;
		foreach (json_decode($FQ['value'],true) AS $k=>$V) {
			$total += $_REQUEST[$V];
		}
		$Answer = $total;
	} 
	
	if ($FQ['QuestionTypeID'] == 'PREFERRED') {

		$ck=0;
		foreach ($IFQ['results'] AS $CK) {
		   $VALUE = json_decode($CK['value'],true);
		   if ((is_array($VALUE)) && ($CK['QuestionTypeID'] != 'TOTAL')) {
			foreach ($VALUE AS $k=>$V) {
			  if ($V['Preferred'] != "") {
				  if ($_REQUEST[$CK['QuestionID']] != $V['Value']) {
					  $ck++;
				  }
			  }
			}
		   }
		}

		if ($ck == 0) {
		  $Answer="P";
		} else {
		  $Answer="F";
		}
	}

  	$info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $FQ['RequestID'],
                        'InterviewFormID'       =>  $FQ['InterviewFormID'],
                        'UserID'       		=>  $UID,
                        'ApplicationID'         =>  $_REQUEST['ApplicationID'],
                        'QuestionID'            =>  $FQ['QuestionID'],
                        'Answer'                =>  $Answer
        );

	if ($Answer != "") {

	    if ($FORMDATA[$FQ['QuestionID']] != $Answer) {
		if ($FORMDATA[$FQ['QuestionID']] == "") { $FORMDATA[$FQ['QuestionID']] = "{not answered}"; }
		$updatedFields.=$FQ['Question'] . ' changed from ' . $FORMDATA[$FQ['QuestionID']] . ' to ' . $Answer . "\n";
	    }

	    G::Obj('Interview')->insUpdInterviewData($info, $skip);
	}

   } // end foreach

   $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'UserID'       		=>  $UID,
			'ApplicationID'         =>  $_REQUEST['ApplicationID'],
                        'QuestionID'            =>  "LastModified",
                        'Answer'                =>  "NOW()"
   );

   G::Obj('Interview')->insUpdInterviewData($info, $skip);

   $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'UserID'       		=>  $UID,
			'ApplicationID'         =>  $_REQUEST['ApplicationID'],
                        'QuestionID'            =>  "UserID",
			'Answer'                =>  $UID
   );

   G::Obj('Interview')->insUpdInterviewData($info, $skip);

   $status="";
   if ($_REQUEST['process_status'] == 'edit') {
     $status="final";
   } else {
     $status=$_REQUEST['process_status'];
   }

   $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'UserID'       		=>  $UID,
			'ApplicationID'         =>  $_REQUEST['ApplicationID'],
                        'QuestionID'            =>  "process_status",
			'Answer'                =>  $status
   );

   G::Obj('Interview')->insUpdInterviewData($info, $skip);


   $comments="Form submitted.\n";
   if ($IFD['count'] > 0) {
     $comments="Form updated.\n";
   }
   $comments.="Status: " . $_REQUEST['process_status'] . "\n";

   if ($_REQUEST['process_status'] == 'edit') {
      $comments="Form edited.\n";
   }

   $history_info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'UserID'       		=>  $USERID,
			'ApplicationID'         =>  $_REQUEST['ApplicationID'],
			'Date'         		=>  "NOW()",
                        'Comments'              =>  $comments,
			'UpdatedFields'         =>  $updatedFields
   );

   G::Obj('Interview')->insInterviewFormHistory($history_info);

   if ($_REQUEST['process_status'] == 'edit') {

	$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);

	// Space
	echo '<div style="margin-top:20px;"></div>';

	// header and back
	echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
	echo '<tr>';
	echo '<td valign="top" align="left">';
	echo '<strong>Applicant:</strong> ' . $_REQUEST['ApplicationID'] . ' - ' . G::Obj('ApplicantDetails')->getApplicantName($OrgID, $_REQUEST['ApplicationID']) . '<br>';

	$MultiOrgID = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
	$ReqTitle = G::Obj('RequisitionDetails')->getJobTitle($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
	$ReqJobID = G::Obj('RequisitionDetails')->getReqJobIDs($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
	$ReqTitle .= ' (' . $ReqJobID . ')';

	echo '<strong>Requisition:</strong> ' . $ReqTitle . '<br>';
	echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'] . '<br>';
	echo '<strong>Submitted By:</strong> ' . G::Obj('IrecruitUsers')->getUsersName($OrgID,$UID) . '<br>';
	echo '<strong>Edited By:</strong> ' . G::Obj('IrecruitUsers')->getUsersName($OrgID,$USERID) . '<br>';
	echo '<strong>Last Modified:</strong> ' . date('m/d/Y g:i A');
	echo '</td>';
	echo '<td width="100" align="right">';
	echo '<a href="" onclick="getInterviewResults();return false;">';
	echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
	echo '</a>';
	echo '</td></tr>';
	echo '</table>';

	echo '<div margin-bottom:40px;>';
	echo '<span style="color:red;font-style:italic;">Form has been successfully updated.</span>';
	echo '</div>';

   }


} // end if RequestID and Interview Form



?>
