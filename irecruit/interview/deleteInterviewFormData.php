<?php
require_once '../Configuration.inc';

if (($_REQUEST['RequestID'] != "") && ($_REQUEST['InterviewFormID'] != "") && ($_REQUEST['UserID'] != "")) {
  G::Obj('Interview')->deleteInterviewFormData($OrgID,$_REQUEST['RequestID'],$_REQUEST['InterviewFormID'],$_REQUEST['UserID'],$_REQUEST['ApplicationID']);
}

   $comments="Form data deleted.\n";
   $updatedFields="All data removed.";

   $history_info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['InterviewFormID'],
                        'UserID'                =>  $USERID,
                        'ApplicationID'         =>  $_REQUEST['ApplicationID'],
                        'Date'                  =>  "NOW()",
                        'Comments'              =>  $comments,
                        'UpdatedFields'         =>  $updatedFields
   );

   G::Obj('Interview')->insInterviewFormHistory($history_info);

?>
