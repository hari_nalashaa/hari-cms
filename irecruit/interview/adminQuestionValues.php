<?php
require_once '../Configuration.inc';

// Space
echo '<div style="margin-top:20px;"></div>';

$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);
$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","QuestionID = :QuestionID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":QuestionID"=>$_REQUEST['QuestionID']);
$IQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

echo '<form action="" method="post" name="QuestionValues">';
echo '<input type="hidden" name="RequestID" id="RequestID" value="'.$_REQUEST['RequestID'].'">';
echo '<input type="hidden" name="InterviewFormID" id="InterviewFormID" value="'.$_REQUEST['InterviewFormID'].'">';
echo '<input type="hidden" name="QuestionID" id="QuestionID" value="'.$_REQUEST['QuestionID'].'">';

//Title and Back Bar
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'];
echo '<br><strong>Interview Question:</strong> ' . $IQ['results'][0]['Question'];
echo '</td>';
echo '<td width="100" align="right">';
echo '<a href="" onclick="getInterviewQuestions(\''.$_REQUEST['InterviewFormID'].'\');return false;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
echo '</a>';
echo '</td></tr>';
echo '</table>';

echo '<div id="message" style="margin-top:-15px;margin-bottom:5px;"></div>';

$VALUES = json_decode($IQ['results'][0]['value'], true);
$defaultck="";
$preferck="";

$i=0;
if (count($VALUES) > 0) {

echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" id="sort_interview_values" class="table table-striped table-bordered">' . "\n";

echo '<thead>';
echo '<tr>';
echo '<td width="80"><b>Passed Value</b></td>';
echo '<td width="80"><b>Display Text</b></td>';
echo '<td width="80" align="center"><b>Default Setting</b></td>';
echo '<td width="80" align="center"><b>Preferred Answer</b></td>';
echo '<td width="80" align="center"><b>Delete</b></td>';
echo '</tr>';
echo '</thead>' . "\n";

	$rowcolor = "#eeeeee";

	foreach ($VALUES as $V) {

		echo '<tbody class="ui-sortable">';
		echo '<tr id="' . $V['Value'] . '" class="ui-sortable-handle">';

		echo '<td bgcolor="'.$rowcolor.'">';
		echo '<input type="text" value="' . $V['Value'] . '"';
		echo ' onchange="processInterviewValues(\'Value\',\''.$i.'\',this.value);return false;"';
		echo '>';
		echo '</td>';

		echo '<td bgcolor="'.$rowcolor.'">';
		echo '<input type="text" value="' . $V['Display'] . '"';
		echo ' onchange="processInterviewValues(\'Display\',\''.$i.'\',this.value);return false;"';
		echo '>';
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<input type="radio" name="Default" value="Y"';
		if($V['Default'] == "Y") { $defaultck ="Y"; echo " checked"; }
		echo ' onclick="processInterviewValues(\'Default\',\''.$i.'\',\'\');return false;">';
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<input type="radio" name="Preferred" value="Y"';
		if($V['Preferred'] == "Y") { $preferck="Y"; echo " checked"; }
		echo ' onclick="processInterviewValues(\'Preferred\',\''.$i.'\',\'\');return false;">';
		echo '</td>';

		echo '<td bgcolor="' . $rowcolor . '" align="center">';
		echo '<a href="" onclick="processInterviewValues(\'Delete\',\''.$i.'\',\'\');return false;">';
	        echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="padding:5px;"></a>';
                echo '</td>';

		echo '</tr>';
		echo '</tbody>' . "\n";

		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	$i++;

	} // end foreach

echo '</table>';

} else {
echo '<div style="margin:30px 0 40px 10px;">'; 
echo 'There are no values configured for this question.'; 
echo '</div>';
}

echo '<div style="margin:30px 0 40px 10px;">'; 
echo '<a href="" onclick="processInterviewValues(\'Add\',\''.$i.'\',\'\');return false;">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Value</b></a>';


if (count($VALUES) == 0) {
echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<a href="" onclick="processInterviewValues(\'AddYesNo\',\''.$i.'\',\'\');return false;">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Yes/No Perferred Values</b></a>';

echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<a href="" onclick="processInterviewValues(\'Add15\',\''.$i.'\',\'\');return false;">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add 0-5 Scoring Values</b></a>';
}

if (($defaultck == "Y") || ($preferck == "Y")) {
echo '&nbsp;&nbsp;&nbsp;&nbsp;';
echo '<a href="" onclick="processInterviewValues(\'Clear\',\'\',\'\');return false;">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" title="Delete" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Clear Default Settings</b></a>';
echo '</a>';
}

echo '</div>';

echo '</form>';

?>
<script>
$(document).ready(function() {
   $("#sort_interview_values").sortable({
        cursor: 'move',
        placeholder: 'ui-state-highlight',
        stop: function( event, ui ) {

        var RequestID = document.frmRequisitionDetailInfo.RequestID.value;
        var InterviewFormID = document.getElementById("InterviewFormID").value;
        var QuestionID = document.getElementById("QuestionID").value;

        var forms_data = {
                forms_info: []
        };


        $(this).find('tr').each(function(i) {


            var QuestionOrder = i;

            var NP = $(this).attr('id');

            if(typeof(NP) !== 'undefined') {

                forms_data.forms_info.push({
                         "RequestID" : RequestID,
                         "InterviewFormID" : InterviewFormID,
                         "QuestionID" : QuestionID,
                         "Value" : NP,
                         "QuestionOrder"  : QuestionOrder
                });

              QuestionOrder++;
            }

        });

        updateInterviewFormValuesSortOrder(forms_data,RequestID,InterviewFormID,QuestionID);
    }
  });
} );
</script>
