<?php
require_once '../Configuration.inc';

$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID", "UserID = :UserID");
$params =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":ApplicationID"=>$_REQUEST['ApplicationID'], ":UserID"=>$_REQUEST['UserID']);
$IFD =   G::Obj('Interview')->getInterviewFormData("QuestionID, Answer", $where_info, "", $params);
$FORMDATA=array();
foreach ($IFD['results'] AS $D) {
        $FORMDATA[$D['QuestionID']]=$D['Answer'];
}

// Space
echo '<div style="margin-top:20px;"></div>';

// header and back
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Applicant:</strong> ' . $_REQUEST['ApplicationID'] . ' - ' . G::Obj('ApplicantDetails')->getApplicantName($OrgID, $_REQUEST['ApplicationID']) . '<br>';

$MultiOrgID = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
$ReqTitle = G::Obj('RequisitionDetails')->getJobTitle($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqJobID = G::Obj('RequisitionDetails')->getReqJobIDs($OrgID, $MultiOrgID, $_REQUEST['RequestID']);
$ReqTitle .= ' (' . $ReqJobID . ')';

echo '<strong>Requisition:</strong> ' . $ReqTitle . '<br>';
echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'] . '<br>';
echo '<strong>Submitted By:</strong> ' . G::Obj('IrecruitUsers')->getUsersName($OrgID,$_REQUEST['UserID']) . '<br>';
echo '<strong>Last Modified:</strong> ' . date_format(date_create($FORMDATA['LastModified']),"m/d/Y g:i A");
echo '</td>';
echo '<td width="100" align="right">';
echo '<a href="" onclick="getInterviewFormResults(\''.$_REQUEST['RequestID'] . '\',\'' . $_REQUEST['InterviewFormID'] . '\',\'' . $_REQUEST['UserID'] . '\',\''.$_REQUEST['ApplicationID'].'\');return false;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
echo '</a>';
echo '</td></tr>';
echo '</table>';

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID", "ApplicationID = :ApplicationID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'], ":ApplicationID"=>$_REQUEST['ApplicationID']);
$IFH =   G::Obj('Interview')->getInterviewFormHistory("UserID, Date, Comments, UpdatedFields", $where_info, "Date DESC", $params);
?>
<div class="table-responsive" style="margi-bottom:40px;">
<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover">
<tr><td colspan="4" bgcolor="#eeeeee"><b>Interview Form History</b></td></tr>
<tr>
<td width="100"><b>Updated By</b></td>
<td width="80"><b>Date</b></td>
<td><b>Comments</b></td>
<td><b>Updated Fields</b></td>
</tr>
<?php

$rowcolor = "#eeeeee";
$tr_index = 0;
if (is_array ( $IFH['results'] )) {
        foreach ( $IFH['results'] as $H ) {
                $tr_index++;

                echo '<tr bgcolor="' . $rowcolor . '" class="'.$tr_index.'">';

                echo '<td valign=top>';

		$updated_by_user_avatar_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($H['UserID'], 'DashboardAvatar');

		$user_avatar = IRECRUIT_HOME . "vault/".$OrgID."/".$H['UserID']."/profile_avatars/".$updated_by_user_avatar_info['DashboardAvatar'];

                if($ServerInformationObj->validateUrlContent($user_avatar)) {
                    ?><img src="<?php echo $user_avatar;?>" width="50" height="50">&nbsp;<?php echo G::Obj('IrecruitUsers')->getUsersName($OrgID,$H['UserID']);?><?php
                }
                else {
                    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50">&nbsp;<?php echo G::Obj('IrecruitUsers')->getUsersName($OrgID,$H['UserID']);?><?php
                }

                echo '</td>';

                echo '<td valign=top style="width:70px;">' . date_format(date_create($H['Date']),"m/d/Y g:i A") . '</td>';

                echo '<td valign=top style="width:20%;">';
		echo preg_replace('/[\n\r]/', '<br>', $H['Comments']);
                echo '</td>';

		echo '<td valign=top style="width:50%;">';
		if ($H['UpdatedFields'] != "") {
		  echo 'The following information was updated:' . "<br>\n";
		  echo preg_replace('/[\n\r]/', '<br>', $H['UpdatedFields']);
		}
	        echo '</td>'; 
                echo '</tr>';

                if ($rowcolor == "#eeeeee") {
                        $rowcolor = "#ffffff";
                } else {
                        $rowcolor = "#eeeeee";
                }
        } // end foreach
}
?>
</table>
</div>
