<?php
require_once '../Configuration.inc';

// Space
echo '<div style="margin-top:20px;"></div>';

$IF =   G::Obj('Interview')->getInterviewFormInfo("*", $OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID']);
$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","QuestionID = :QuestionID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":InterviewFormID"=>$_REQUEST['InterviewFormID'],":QuestionID"=>$_REQUEST['QuestionID']);
$IQ =   G::Obj('Interview')->getInterviewFormQuestions("*", $where_info, "QuestionOrder", $params);

$VALUES = json_decode($IQ['results'][0]['value'], true);
$CIQS =   G::Obj('Interview')->getCalculableInterviewQuestions($OrgID, $_REQUEST['RequestID'],$_REQUEST['InterviewFormID'],$_REQUEST['QuestionID']);

echo '<form action="" method="post" name="TotalQuestions">';
echo '<input type="hidden" name="RequestID" id="RequestID" value="'.$_REQUEST['RequestID'].'">';
echo '<input type="hidden" name="InterviewFormID" id="InterviewFormID" value="'.$_REQUEST['InterviewFormID'].'">';
echo '<input type="hidden" name="QuestionID" id="QuestionID" value="'.$_REQUEST['QuestionID'].'">';

//Title and Back Bar
echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td valign="top" align="left">';
echo '<strong>Interview Form:</strong> ' . $IF['results'][0]['FormName'];
echo '<br><strong>Interview Question:</strong> ' . $IQ['results'][0]['Question'];
echo '</td>';
echo '<td width="100" align="right">';
echo '<a href="" onclick="getInterviewQuestions(\''.$_REQUEST['InterviewFormID'].'\');return false;">';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">';
echo '</a>';
echo '</td></tr>';
echo '</table>';

echo '<div id="message" style="margin-top:-15px;margin-bottom:5px;"></div>';

$i=0;
if ($CIQS['count'] > 0) {

echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">' . "\n";

echo '<thead>';
echo '<tr>';
echo '<td><b>Question</b></td>';
echo '<td width="120" align="center"><b>Include in Total</b></td>';
echo '</tr>';
echo '</thead>' . "\n";

	$rowcolor = "#eeeeee";

	foreach ($CIQS['results'] as $C) {

		echo '<tbody>';
		echo '<tr>';

		echo '<td bgcolor="'.$rowcolor.'">';
		echo $C['Question'];
		echo '</td>';

		echo '<td bgcolor="'.$rowcolor.'" align="center">';
		echo '<input type="checkbox" value="'.$C['QuestionID'].'"';
		if (in_array($C['QuestionID'],$VALUES)) { echo " checked"; }
		echo ' onchange="processInterviewTotalValue(this.value,this,\''.preg_replace('/[\n\r]/', ' ', $C['Question']).'\');return false;"';
preg_replace('/[\n\r]/', '<br>', $H['Comments']);
		echo '>';
		echo '</td>';

		echo '</tr>';
		echo '</tbody>' . "\n";

		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	$i++;

	} // end foreach

echo '</table>';

} else {
echo '<div style="margin-top:30px;">'; 
echo 'There are no questions available for calculation.'; 
echo '</div>';
}

echo '</form>';

?>
