<?php
require_once '../Configuration.inc';

echo '<div id="message" style="margin:15px 0 5px 0;"></div>';

$InterviewFormID =   G::Obj('Interview')->getActiveInterviewForm($OrgID,$_REQUEST['RequestID'],$_REQUEST['ApplicationID'],$USERID,$USERROLE,$permit);

if ($InterviewFormID != "") {

   echo G::Obj('Interview')->displayInterviewForm($OrgID,$_REQUEST['RequestID'],$InterviewFormID,$_REQUEST['ApplicationID'],$USERID,'N');

} else {

echo '<div style="margin:20px 0 60px 0;">';
echo 'There are no further forms to complete for this applicant.';
echo '</div>' . "\n";

}

?>

