<?php
require_once '../Configuration.inc';

$where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID", "InterviewFormID = :InterviewFormID","QuestionID = :QuestionID");
$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['forms_info'][0]['RequestID'], ":InterviewFormID"=>$_REQUEST['forms_info'][0]['InterviewFormID'],":QuestionID"=>$_REQUEST['forms_info'][0]['QuestionID']);
$IQ =   G::Obj('Interview')->getInterviewFormQuestions("value", $where_info, "QuestionOrder", $params);

$ORIGVALS = json_decode($IQ['results'][0]['value'],true);
$NEWORDER = $_REQUEST['forms_info'];

$TEMP=array();
foreach ($ORIGVALS AS $OV) {
	$TEMP[$OV['Value']]=$OV;
}

$TEMPORDER=array();
foreach ($NEWORDER AS $k=>$v) {
	$TEMPORDER[$v['Value']]=$v['QuestionOrder']-1;
}

$VALUES=array();
foreach ($TEMPORDER AS $k=>$v) {

	$VALUES[$v]=$TEMP[$k];

}	

$value=json_encode($VALUES);

  $info    =   array(
                        'OrgID'                 =>  $OrgID,
                        'RequestID'             =>  $_REQUEST['forms_info'][0]['RequestID'],
                        'InterviewFormID'       =>  $_REQUEST['forms_info'][0]['InterviewFormID'],
                        'QuestionID'       	=>  $_REQUEST['forms_info'][0]['QuestionID'],
                        'value'         	=>  $value
                    );

$skip   =   array('OrgID', 'RequestID','InterviewFormID','QuestionID');

if (($_REQUEST['forms_info'][0]['RequestID'] != "") && ($_REQUEST['forms_info'][0]['InterviewFormID'] != "") && ($_REQUEST['forms_info'][0]['QuestionID'] != "")) {
  G::Obj('Interview')->insUpdInterviewFormQuestions($info, $skip);
}
?>
