<?php
$PAGE_TYPE = "Email";
$IRECRUIT_USER_AUTH = FALSE;
require_once '../Configuration.inc';

$TemplateObj->title     =   $title      =   'Update Applicant';
$TemplateObj->process   =   $process    =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->fes       =   $fes        =   $_REQUEST['fes'];

echo $TemplateObj->displayIrecruitTemplate('views/emailrespond/UpdateApplicant');
?>