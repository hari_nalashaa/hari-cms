<?php 
require_once 'Configuration.inc';

// Set where condition
$where_info     =   array("QuestionID = 'informed'");
// Get Requisition Information
$applicant_data =   $ApplicantsObj->getApplicantDataInfo("*", $where_info, '', '', array());

$applicant_data_results = $applicant_data['results'];
$applicant_data_count   = $applicant_data['count'];

for($j = 0; $j < $applicant_data_count; $j++) {
    $ReferralOrgID          =   $applicant_data_results[$j]['OrgID'];
    $ReferralApplicationID  =   $applicant_data_results[$j]['ApplicationID'];
    $ReferralAnswer         =   $applicant_data_results[$j]['Answer'];
    
    $set_info               =   array("Informed = :Informed");
    $params_info            =   array(":Informed"=>$ReferralAnswer, ":OrgID"=>$ReferralOrgID, ":ApplicationID"=>$ReferralApplicationID);
    $where_info             =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
    $update_result          =   $ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params_info));
    
    if($update_result['error_info'] != "") {
        echo $ReferralOrgID.", ", $ReferralApplicationID, ", ", $ReferralAnswer, "<br>";
    }
}
?>