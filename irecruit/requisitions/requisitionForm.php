<?php
require_once "../Configuration.inc";
require_once IRECRUIT_DIR . 'requisitions/RequisitionFields.inc';

//Set page title
$TemplateObj->title	= "Requisition Form";

$scripts_header[] = "js/requisition-form.js";

//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header   =   $scripts_header;

//Insert Default RequisitionQuestions
$RequisitionQuestionsObj->insDefaultRequisitionQuestions($OrgID);

//Get RequisitionFormID
$TemplateObj->RequisitionFormID = $RequisitionFormID  =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);

if(isset($_REQUEST['rdCopyForm']) && $_REQUEST['rdCopyForm'] == 'Yes') {
    
    $NewRequisitionFormID           =   strtotime(date('Y-m-d H:i:s')).uniqid();
    
    //Insert default RequisitionForm
    $form_info  =   array(
                            "OrgID"                 =>  $OrgID,
                            "RequisitionFormID"     =>  $NewRequisitionFormID,
                            "FormDefault"           =>  "Y",
                            "CreatedDateTime"       =>  "NOW()"
                         );
    $RequisitionFormsObj->insRequisitionForms($form_info);
    $RequisitionFormsObj->setFormDefault($OrgID, $NewRequisitionFormID);
    //Here first parameter and second parameter is same
    $RequisitionQuestionsObj->copyRequisitionQuestions($OrgID, $OrgID, $RequisitionFormID, $NewRequisitionFormID);
    
    $RequisitionFormID              =   $NewRequisitionFormID;
}

if(isset($_REQUEST['questionid']) && $_REQUEST['questionid'] == 'AdvertisingOptions') {
    header("Location:".IRECRUIT_HOME.'requisitions/requisitionAdvertisingOptions.php?action=requisitionquestions&form='.$RequisitionFormID.'&questionid=AdvertisingOptions&typeform=requisitionform');
    exit;
}

$where_info                         =   array("OrgID = :OrgID", "RequisitionFormID = :RequisitionFormID");
$params_info                        =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
$requisition_questions_info         =   $RequisitionQuestionsObj->getRequisitionQuestions("*", $where_info, "QuestionOrder ASC", array($params_info));
$requisition_questions              =   $requisition_questions_info['results'];

$req_que_rar                        =   array();
for($rqi = 0; $rqi < count($requisition_questions); $rqi++) {
    $rar_question_id                                =   $requisition_questions[$rqi]['QuestionID'];
    $rqid[$rar_question_id]                         =   $requisition_questions[$rqi];
    $req_que_rar[$rar_question_id]['Requisition']   =   $requisition_questions[$rqi]['Requisition'];
    $req_que_rar[$rar_question_id]['Request']       =   $requisition_questions[$rqi]['Request'];
    $req_que_rar[$rar_question_id]['Required']      =   $requisition_questions[$rqi]['Required'];
}

if(substr ( $req_rar_que_id, 0, 4 ) != "CUST" && count($_POST) > 0
            && $_POST['btnChangeStatus'] == 'Change Status') {
    
    foreach($req_que_rar as $req_rar_que_id=>$req_que_rar_info) {

        $history_record = false;
        
        if($_POST[$req_rar_que_id."-RequestStatus"] != $req_que_rar[$req_rar_que_id]['Request']
            || $_POST[$req_rar_que_id."-RequisitionStatus"] != $req_que_rar[$req_rar_que_id]['Requisition']
            || $_POST[$req_rar_que_id."-R"] != $req_que_rar[$req_rar_que_id]['Required']) {
            $history_record = true;
        }
        
        if($history_record == true) {
            
            $que_info = array (
                "OrgID"                 =>	$rqid[$req_rar_que_id]['OrgID'],
                "RequisitionFormID"     =>	$rqid[$req_rar_que_id]['RequisitionFormID'],
                "QuestionID"            =>	$rqid[$req_rar_que_id]['QuestionID'],
                "Question"              =>	$rqid[$req_rar_que_id]['Question'],
                "QuestionTypeID"        =>	$rqid[$req_rar_que_id]['QuestionTypeID'],
                "size"                  =>	$rqid[$req_rar_que_id]['size'],
                "maxlength"             =>	$rqid[$req_rar_que_id]['maxlength'],
                "rows"                  =>	$rqid[$req_rar_que_id]['rows'],
                "cols"                  =>	$rqid[$req_rar_que_id]['cols'],
                "wrap"                  =>	$rqid[$req_rar_que_id]['wrap'],
                "value"                 =>	$rqid[$req_rar_que_id]['value'],
                "defaultValue"          =>	$rqid[$req_rar_que_id]['defaultValue'],
                "Requisition"           =>	$rqid[$req_rar_que_id]['Requisition'],
                "Request"               =>	$rqid[$req_rar_que_id]['Request'],
                "Edit"                  =>	$rqid[$req_rar_que_id]['Edit'],
                "QuestionOrder"         =>	$rqid[$req_rar_que_id]['QuestionOrder'],
                "Required"              =>	$rqid[$req_rar_que_id]['Required'],
                "Validate"              =>	$rqid[$req_rar_que_id]['Validate'],
                "Operation"             =>	"Updated: " . date("F j, Y, g:i a") . " by: " . $USERID,
                "LastModifiedDateTime" 	=>	$rqid[$req_rar_que_id]['LastModifiedDateTime']
            );
            // Insert RequisitionQuestions History
            $RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
            
            //Update LastModifiedDateTime
            $where_info =	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
            $set_info	=	array("LastModifiedDateTime = NOW()");
            $params		=	array (':OrgID'=>$OrgID, ':QuestionID'=>$rqid[$req_rar_que_id]['QuestionID'], ":RequisitionFormID"=>$rqid[$req_rar_que_id]['RequisitionFormID']);
            $res_info   =   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
            
        }
    }

}


$TemplateObj->formtable             =	$formtable		=	"RequisitionQuestions";
$TemplateObj->add                   = 	$add 			= 	isset($_REQUEST['add']) ? $_REQUEST['add'] : '';
$TemplateObj->default               = 	$default 		= 	isset($_REQUEST['default']) ? $_REQUEST['default'] : '';
$TemplateObj->new                   = 	$new 			= 	isset($_REQUEST['new']) ? $_REQUEST['new'] : '';
$TemplateObj->section               = 	$section 		= 	isset($_REQUEST['section']) ? $_REQUEST['section'] : '';
$TemplateObj->direction             = 	$direction 		= 	isset($_REQUEST['direction']) ? $_REQUEST['direction'] : '';
$TemplateObj->ProcessOrder          = 	$ProcessOrder 	= 	isset($_REQUEST['ProcessOrder']) ? $_REQUEST['ProcessOrder'] : '';
$TemplateObj->Required              = 	$Required 		= 	isset($_REQUEST['Required']) ? $_REQUEST['Required'] : '';
$TemplateObj->Clear                 = 	$Clear 			= 	isset($_REQUEST['Clear']) ? $_REQUEST['Clear'] : '';
$TemplateObj->sort                  = 	$sort 			= 	isset($_REQUEST['sort']) ? $_REQUEST['sort'] : '';
$TemplateObj->QuestionID            =	$QuestionID 	= 	$_REQUEST['questionid'];
$TemplateObj->requisition_questions =   $requisition_questions;

if($QuestionID != "") {
    // Get Requisition Question Information
    $req_que_detail_info            =   $RequisitionQuestionsObj->getRequisitionQuestionInfo("*", $OrgID, $QuestionID, $RequisitionFormID);
}

// Get Question Types List
$row_question_types = G::Obj('QuestionTypes')->getQuestionTypesInfo ( "*", array (), "SortOrder", array () );

if (is_array ( $row_question_types ['results'] )) {
	foreach ( $row_question_types ['results'] as $row_question_types ) {
		$row_que_types [] = $row_question_types;
		$ques_types_list [] = $row_question_types ['QuestionTypeID'];
		$question_types_by_id [$row_question_types ['QuestionTypeID']] = $row_question_types ['Description'];
			
		if ($row_question_types ['Editable'] == 'Y') {
			$que_type_id = $row_question_types ['QuestionTypeID'];
			if ($que_type_id != '15' 
					&& $que_type_id != '30' 
					&& $que_type_id != '60'
					&& $que_type_id != '9' 
					&& $que_type_id != '21'
					&& $que_type_id != '25') {
				$edit_que_types [] = $row_question_types;
			}
		}
	}
}

$TemplateObj->row_que_types			=	$row_que_types;
$TemplateObj->ques_types_list		=	$ques_types_list;
$TemplateObj->question_types_by_id	=	$question_types_by_id;
$TemplateObj->edit_que_types		=	$edit_que_types;

if (isset ( $_POST ['process'] ) && $_POST ['process'] == 'Update' && isset($_REQUEST['questionid'])) {
	
	// edit individual entry
	$TemplateObj->FormID		=	$FormID 		= 	$_REQUEST['form'];

	//Pull the question information before update
	$single_que_upd_info   =   $RequisitionQuestionsObj->getRequisitionQuestionInfo("*", $OrgID, $QuestionID, $RequisitionFormID);
	
	require_once IRECRUIT_DIR . 'requisitions/UpdateRequisitionQuestion.inc';
	
	if($res_que_info['affected_rows'] > 0) {
		
	    $que_info = array (
	        "OrgID"                 =>	$single_que_upd_info['OrgID'],
	        "RequisitionFormID"     =>	$single_que_upd_info['RequisitionFormID'],
	        "QuestionID"            =>	$single_que_upd_info['QuestionID'],
	        "Question"              =>	$single_que_upd_info['Question'],
	        "QuestionTypeID"        =>	$single_que_upd_info['QuestionTypeID'],
	        "size"                  =>	$single_que_upd_info['size'],
	        "maxlength"             =>	$single_que_upd_info['maxlength'],
	        "rows"                  =>	$single_que_upd_info['rows'],
	        "cols"                  =>	$single_que_upd_info['cols'],
	        "wrap"                  =>	$single_que_upd_info['wrap'],
	        "value"                 =>	$single_que_upd_info['value'],
	        "defaultValue"          =>	$single_que_upd_info['defaultValue'],
	        "Requisition"           =>	$single_que_upd_info['Requisition'],
	        "Request"               =>	$single_que_upd_info['Request'],
	        "Edit"                  =>	$single_que_upd_info['Edit'],
	        "QuestionOrder"         =>	$single_que_upd_info['QuestionOrder'],
	        "Required"              =>	$single_que_upd_info['Required'],
	        "Validate"              =>	$single_que_upd_info['Validate'],
	        "Operation"             =>	"Updated: " . date("F j, Y, g:i a") . " by: " . $USERID,
	        "LastModifiedDateTime" 	=>	$single_que_upd_info['LastModifiedDateTime']
	    );
	    // Insert RequisitionQuestions History
	    $RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
	}
	
	if(isset($_REQUEST['date_field']) && $_REQUEST['date_field'] != "") {
		//Set information
		$set_info		=	array("Validate = :Validate");
		//Where information
		$where_info		=	array('OrgID = :OrgID', 'RequisitionFormID = :RequisitionFormID', 'QuestionID = :QuestionID');
		//Set parameters
		$params			=	array(':OrgID'=>$OrgID, ':QuestionID'=>$_REQUEST['questionid'], ':RequisitionFormID'=>$RequisitionFormID, ':Validate'=>json_encode($_POST['date_field']));
		//Update the table information based on bind and set values
		$res_que_info	=   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array ($params) );
	}
	
}

if (isset ( $_POST ['process'] ) && $_POST ['process'] == 'Y' && !isset($_REQUEST['questionid'])) {
	$value_ds  =   array();
	$value_ys  =   array();
	foreach ( $_POST as $key => $value ) {
	
		if ($value == 'D') {
	
			$where_info =	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
			$set_info	=	array("Required = ''", "Requisition = ''");
			if($feature['RequisitionRequest'] == "Y") {
				$set_info[]	= "Request = ''";
			}
			$params		=	array (':OrgID'=>$OrgID, ':QuestionID'=>$key, ":RequisitionFormID"=>$RequisitionFormID);
			$res_info   =   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
				
		}
	}
	
	foreach ( $_POST as $key => $value ) {

		if ($value == 'Y') {
				
			$pieces = explode ( "-", $key );
				
			if ($pieces [1] == 'R') {

				// Update the table information based on bind and set values
				$where_info =	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
				$params		=	array (":OrgID"=>$OrgID, ":QuestionID"=>$pieces [0], ":RequisitionFormID"=>$RequisitionFormID);
				$set_info	=	array("Required = 'Y'");
				$res_info   =   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
				
			} else if ($pieces [1] == 'RequestStatus') {
			
				// Update the table information based on bind and set values
				$where_info =	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
				$params		=	array (":OrgID"=>$OrgID, ":QuestionID"=>$pieces [0], ":RequisitionFormID"=>$RequisitionFormID);
				$set_info	=	array("Request = 'Y'");
				$res_info   =   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
				
			} else if ($pieces [1] == 'RequisitionStatus') {
			
				// Update the table information based on bind and set values
				$where_info =	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
				$params		=	array (":OrgID"=>$OrgID, ":QuestionID"=>$pieces [0], ":RequisitionFormID"=>$RequisitionFormID);
				$set_info	=	array("Requisition = 'Y'");
				$res_info   =   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
			}
		}
		
		
		if (strstr ( $key, '-questiontypeid' )) {
			$cqtypeinfo  = explode ( "-", $key );
			$cquestionid = $cqtypeinfo [0];
		
			if (substr ( $cquestionid, 0, 4 ) == "CUST") {
				if (isset ( $_REQUEST [$cquestionid . 'Question'] ) && $_REQUEST [$cquestionid . 'Question'] != "") {
		
				    $req_que_details = $RequisitionQuestionsObj->getRequisitionQuestionInfo("*", $OrgID, $cquestionid, $RequisitionFormID);
				    
					// Get question type information based on QuestionTypeID
					$que_type_def_val = G::Obj('QuestionTypes')->getQuestionTypeInfo ( $_REQUEST [$key] );
		
					// List of parameters to bind the query
					$params = array (
							':Question'          =>  $_REQUEST [$cquestionid . 'Question'],
							':QuestionTypeID'    =>  $_REQUEST [$key],
							':OrgID'             =>  $OrgID,
							':QuestionID'        =>  $cquestionid,
                            ':RequisitionFormID' =>  $RequisitionFormID
					);
					
					$que_info = array (
                            'Question'          =>  $_REQUEST [$cquestionid . 'Question'],
                            'QuestionTypeID'    =>  $_REQUEST [$key],
                            'OrgID'             =>  $OrgID,
                            'QuestionID'        =>  $cquestionid,
                            'RequisitionFormID' =>  $RequisitionFormID
					);
					
					// Set Update Information
					$set_info = array ("Question = :Question", "QuestionTypeID = :QuestionTypeID");

					$set_info[] = 'Edit = :Edit';
					$params[':Edit'] = 'Y';
					
					//If question is text area add rows and cols while updating
					if($_REQUEST [$key] != 100 && $_REQUEST [$key] != 120) {
						//Set parameters
						$params[':rows'] = $que_type_def_val['rows'];
						$params[':cols'] = $que_type_def_val['cols'];
						$params[':size'] = $que_type_def_val['size'];
						$params[':maxlength'] = $que_type_def_val['maxlength'];
						
						//Set the default parameters
						$set_info[] = 'rows = :rows';
						$set_info[] = 'cols = :cols';
						$set_info[] = 'size = :size';
						$set_info[] = 'maxlength = :maxlength';
						$set_info[] = 'LastModifiedDateTime = NOW()';
					}

					// Set where condition
					$where     =   array ("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
					// Update Form Questions Information
					$res_info  =   $RequisitionQuestionsObj->updRequisitionQuestions ( $set_info, $where, array ($params) );
					
					if($res_info['affected_rows'] > 0) {
					
					    if($req_que_details['Question'] != $_REQUEST [$cquestionid . 'Question']
					       || $req_que_details['QuestionTypeID'] != $_REQUEST [$key])
					    {
					        $que_info = array (
					            "OrgID"                 =>	$req_que_details['OrgID'],
					            "RequisitionFormID"     =>	$req_que_details['RequisitionFormID'],
					            "QuestionID"            =>	$req_que_details['QuestionID'],
					            "Question"              =>	$req_que_details['Question'],
					            "QuestionTypeID"        =>	$req_que_details['QuestionTypeID'],
					            "size"                  =>	$req_que_details['size'],
					            "maxlength"             =>	$req_que_details['maxlength'],
					            "rows"                  =>	$req_que_details['rows'],
					            "cols"                  =>	$req_que_details['cols'],
					            "wrap"                  =>	$req_que_details['wrap'],
					            "value"                 =>	$req_que_details['value'],
					            "defaultValue"          =>	$req_que_details['defaultValue'],
					            "Requisition"           =>	$req_que_details['Requisition'],
					            "Request"               =>	$req_que_details['Request'],
					            "Edit"                  =>	$req_que_details['Edit'],
					            "QuestionOrder"         =>	$req_que_details['QuestionOrder'],
					            "Required"              =>	$req_que_details['Required'],
					            "Validate"              =>	$req_que_details['Validate'],
					            "Operation"             =>	"Updated: " . date("F j, Y, g:i a")  . " by: " . $USERID,
					            "LastModifiedDateTime" 	=> $req_que_details['LastModifiedDateTime']	
					        );
					        // Insert RequisitionQuestions History
					        $RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
					        
					        //Update LastModifiedDateTime
					        $where_info =	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
					        $set_info	=	array("LastModifiedDateTime = NOW()");
					        $params		=	array (':OrgID'=>$OrgID, ':QuestionID'=>$req_que_details['QuestionID'], ":RequisitionFormID"=>$req_que_details['RequisitionFormID']);
					        $res_info   =   $RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
					         
					    }
					}
				}
			}
		}
	}

	header ( "Location:requisitionForm.php?action=requisitionquestions&menu=8" );
	exit ();
}

if (isset($_REQUEST ['form']) && $_REQUEST ['form'] != '' && $_REQUEST ['new'] == 'add') {
    
	// Set Columns
	$columns   =   array ('MAX(QuestionOrder) as QuestionOrder');
	// set condition
	$where     =   array ("OrgID = :OrgID", "RequisitionFormID = :RequisitionFormID");
	// set parameters
	$params    =   array (":OrgID" => $OrgID, ":RequisitionFormID"=>$RequisitionFormID);
	// Get FormQuestionsInformation
	$results   =   $RequisitionQuestionsObj->getRequisitionQuestions ( $columns, $where, '', array ($params) );

	$row_que_order = $results ['results'] [0];

	$QuestionOrder = $row_que_order ['QuestionOrder'] + 1;

	$QuestionID = "CUST" . uniqid (strtotime(date('Y-m-d H:i:s')), true);
	
	// Set Insert Information
    $req_que_info = array (
        "OrgID"                 =>	$OrgID,
        "RequisitionFormID"     =>	$RequisitionFormID,
        "QuestionID"            =>	$QuestionID,
        "Question"              =>	'New Question Added.',
        "QuestionTypeID"        =>	'6',
        "size"                  =>	'0',
        "maxlength"             =>	'0',
        "rows"                  =>	'2',
        "cols"                  =>	'2',
        "wrap"                  =>	'',
        "value"                 =>	'',
        "defaultValue"          =>	'',
        "Requisition"           =>	'Y',
        "Request"               =>	'',
        "Edit"                  =>	"Y",
        "QuestionOrder"         =>	$QuestionOrder,
        "Required"              =>	'',
        "Validate"              =>	'',
        "LastModifiedDateTime"  =>  'NOW()'
    );

	// Insert Form Questions
	$result_custom_question = $RequisitionQuestionsObj->insRequisitionQuestions ( $req_que_info );

    $que_info = array (
        "OrgID"                 =>	$OrgID,
        "RequisitionFormID"     =>	$RequisitionFormID,
        "QuestionID"            =>	$QuestionID,
        "Question"              =>	'New Question Added.',
        "QuestionTypeID"        =>	'6',
        "size"                  =>	'0',
        "maxlength"             =>	'0',
        "rows"                  =>	'2',
        "cols"                  =>	'2',
        "wrap"                  =>	'',
        "value"                 =>	'',
        "defaultValue"          =>	'',
        "Requisition"           =>	'Y',
        "Request"               =>	'',
        "Edit"                  =>	"Y",
        "QuestionOrder"         =>	$QuestionOrder,
        "Required"              =>	'',
        "Validate"              =>	'',
        "Operation"             =>	'Added: ' . " by: " . $USERID,
        "LastModifiedDateTime" 	=>	'NOW()'
	);
	// Insert RequisitionQuestions History
	$RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
	
	header ( "Location:requisitionForm.php?action=requisitionquestions&menu=8" );
	exit ();
}

// EDIT A FORM
if (isset ( $_REQUEST ['delete'] ) && $_REQUEST ['delete'] != "" && $_REQUEST['subactiondelete'] == 'yes') {

    // Get Requisition Question Information
    $req_que_detail_info = $RequisitionQuestionsObj->getRequisitionQuestionInfo("*", $OrgID, $_REQUEST ['delete'], $RequisitionFormID);
    // Question Information
    $que_info = array (
        "OrgID"                 =>	$OrgID,
        "RequisitionFormID"     =>	$RequisitionFormID,
        "QuestionID"            =>	$_REQUEST['delete'],
        "Question"              =>	$req_que_detail_info['Question'],
        "QuestionTypeID"        =>	$req_que_detail_info['QuestionTypeID'],
        "size"                  =>	$req_que_detail_info['size'],
        "maxlength"             =>	$req_que_detail_info['maxlength'],
        "rows"                  =>	$req_que_detail_info['rows'],
        "cols"                  =>	$req_que_detail_info['cols'],
        "wrap"                  =>	$req_que_detail_info['wrap'],
        "value"                 =>	$req_que_detail_info['value'],
        "defaultValue"          =>	$req_que_detail_info['defaultValue'],
        "Requisition"           =>	$req_que_detail_info['Requisition'],
        "Request"               =>	$req_que_detail_info['Request'],
        "Edit"                  =>	$req_que_detail_info['Edit'],
        "QuestionOrder"         =>	$req_que_detail_info['QuestionOrder'],
        "Required"              =>	$req_que_detail_info['Required'],
        "Validate"              =>	$req_que_detail_info['Validate'],
        "Operation"             =>	"Deleted:  " . date("F j, Y, g:i a") . " by: " . $USERID,
        "LastModifiedDateTime" 	=>	$req_que_detail_info['LastModifiedDateTime']
    );
    // Insert RequisitionQuestions History
    $RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
    
    
	// delete requisition questions
	$where         =   array ("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
	// set parameters
	$params        =   array (":OrgID"=>$OrgID, ":QuestionID" => $_REQUEST ['delete'], ":RequisitionFormID"=>$RequisitionFormID);
	// Delete FormQuestions Information
	$res_req_ques  =   $RequisitionQuestionsObj->delRequisitionQuestions ( $where, array ($params) );
    
	
	header ( "Location:requisitionForm.php?action=requisitionquestions&menu=8" );
	exit ();
}

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/RequisitionForm');
?>
