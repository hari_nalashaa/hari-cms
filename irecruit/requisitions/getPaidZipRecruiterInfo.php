<?php
require_once '../Configuration.inc';

$columns                =   "Title";
$req_info               =   $RequisitionsObj->getReqDetailInfo($columns, $OrgID, "", $_REQUEST['RequestID']);

$zip_feed_info          =   $ZipRecruiterObj->getZipRecruiterFeed($OrgID, $_REQUEST['RequestID']);

$indeed_purchase_info   =   $IndeedObj->getIndeedPurchaseInformation("*", $OrgID, $zip_feed_info['PurchaseNumber']);
$indeed_purchase_items  =   $IndeedObj->getIndeedPurchaseItemsInfo("*", $OrgID, $_REQUEST['RequestID'], $zip_feed_info['PurchaseNumber']);
?>
<table class="table table-bordered">
    <tr>
        <td>Requisition Title</td>
        <td><?php echo $req_info['Title'];?></td>
    </tr>
    
    <tr>
        <td>Budget Total</td>
        <td><?php echo $zip_feed_info['BudgetTotal'];?></td>
    </tr>
    
    <tr>
        <td>Full Name</td>
        <td><?php echo $zip_feed_info['FullName'];?></td>
    </tr>
    
    <tr>
        <td>Email</td>
        <td><?php echo $zip_feed_info['Email'];?></td>
    </tr>
    
    <tr>
        <td>Sponsored</td>
        <td><?php echo $zip_feed_info['Sponsored'];?></td>
    </tr>
    
    <tr>
        <td>Service Type</td>
        <td><?php echo $indeed_purchase_items['ServiceType'];?></td>
    </tr>
    
    <tr>
        <td>Invoice Number</td>
        <td><?php echo $indeed_purchase_items['InvoiceNo'];?></td>
    </tr>
    
    <tr>
        <td>Comment</td>
        <td><?php echo $indeed_purchase_items['Comment'];?></td>
    </tr>

    <tr>
        <td>Purchased Date and Time</td>
        <td><?php echo date('m/d/Y H:i:s', strtotime($indeed_purchase_info['CreatedDateTime']));?></td>
    </tr>
    
</table>