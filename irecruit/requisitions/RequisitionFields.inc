<?php 
//Requisitions table columns
$requisition_fields = array (
	"OrgID",
	"MultiOrgID",
	"RequestID",
	"ApprovalLevelRequired",
	"ApprovalLevel",
	"Approved",
	"Title",
	"Description",
    "PresentOn",
	"PostDate",
	"ExpireDate",
	"EmpStatusID",
	"Exempt_Status",
	"Salary_Range_From",
	"Salary_Range_To",
	"HowMany",
	"WhenNeeded",
	"RequisitionID",
	"JobID",
	"SalaryGradeID",
	"FormID",
	"EEOCode",
	"JobGroupCode",
	"ListingPriority",
	"Active",
	"Owner",
	"RequisitionManagers",
	"RequesterName",
	"RequesterEmail",
	"RequesterPhone",
	"RequesterTitle",
	"RequesterDepartment",
	"RequesterReason",
	"ImportanceID",
	"Address1",
	"Address2",
	"City",
	"State",
	"ZipCode",
	"Country",
	"AdvertisingOptions",
	"IsAuditor",
	"IsApprover",
	"WorkDays",
	"Hours",
	"Weeks",
	"Program",
	"Replacement",
	"ReportsTo",
	"POE_from",
	"POE_to",
	"Comment",
    "MultiCostCenter"
);

//These fields will be available only if InternalRequisitions feature is disabled
if($feature ['InternalRequisitions'] == "Y") {
	$requisition_fields[] = "InternalFormID";
}

//These fields will be available only if Monster feature is disabled
$monster_fields = array();
if ($feature ['MonsterAccount'] == "Y") {
	$monster_fields[] = "MonsterJobCategory";
	$monster_fields[] = "MonsterJobOccupation";
	$monster_fields[] = "MonsterJobPostType";
	$monster_fields[] = "MonsterJobIndustry";
}

//Questions not available in request process
$request_not_available_ques 	=  array("Active", "MultiOrgID", "Owner", "MonsterJobCategory", "MonsterJobIndustry", "MonsterJobOccupation", "MonsterJobPostType", "RequisitionManagers", "PostDate", "ExpireDate");
//User doesn't have permissions to deactivate these questions
$default_requisition_questions 	=   array("Title", "RequisitionID", "JobID", "PostDate", "ExpireDate", "Active", "Description");
//User doesn't have permissions to deactivate these questions
$default_request_questions 		=  array("Title", "Active", "Description");
//User doesn't have permissions to deactivate these questions
$default_active_questions 		=   array("Title", "RequisitionID", "JobID", "PostDate", "ExpireDate", "Active", "Description");
//Mandatory required questions don't have permissions to deactivate these questions
$default_required_questions 	=  array("Title", "Active", "Description");

if($feature['RequisitionRequest'] == "Y") {
	$requester_info_questions 	= 	array("RequesterName", "RequesterEmail", "RequesterPhone", "RequesterTitle", "RequesterDepartment", "RequesterReason");	

	//Push requester information
	$default_required_questions	= 	array_merge($default_required_questions, $requester_info_questions);
	$default_request_questions	= 	array_merge($default_request_questions, $requester_info_questions);
	$default_active_questions	= 	array_merge($default_active_questions, $requester_info_questions);
}

//Push default requisition questions to active, required questions list
$default_active_questions		= 	array_merge($default_active_questions, $default_requisition_questions);
$default_required_questions		= 	array_merge($default_required_questions, $default_requisition_questions);

//Push monster fields also
$requisition_fields 			= 	array_merge($requisition_fields, $monster_fields);
$default_active_questions 		= 	array_merge($default_active_questions, $monster_fields);
$default_requisition_questions 	= 	array_merge($default_requisition_questions, $monster_fields);
$default_required_questions		=	array_merge($default_required_questions, $monster_fields);


if(is_object($TemplateObj)) {
	$TemplateObj->default_active_questions			=	$default_active_questions;
	$TemplateObj->default_requisition_questions		=	$default_requisition_questions;
	$TemplateObj->default_request_questions			=	$default_request_questions;
	$TemplateObj->default_required_questions		=	$default_required_questions;
	$TemplateObj->requisition_fields				=	$requisition_fields;
	$TemplateObj->request_not_available_ques 		=	$request_not_available_ques;
}
?>
