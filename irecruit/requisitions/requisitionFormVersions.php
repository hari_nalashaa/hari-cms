<?php
require_once "../Configuration.inc";

//Set page title
$TemplateObj->title	= "Requisition Form Versions";

$where = array("OrgID = :OrgID");
$params = array(":OrgID"=>$OrgID);
$req_form_results = $RequisitionFormsObj->getRequisitionForms("*", $where, '', 'CreatedDateTime DESC', array($params));
$req_forms_list = $req_form_results['results'];
$req_forms_count = $req_form_results['count'];

$TemplateObj->req_forms_list = $req_forms_list;
$TemplateObj->req_forms_count = $req_forms_count;

echo $TemplateObj->displayIrecruitTemplate('views/administration/RequisitionFormVersions');
?>