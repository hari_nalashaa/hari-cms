<?php
require_once '../Configuration.inc';

$title = 'Reposted Requisitions';

$subtitle = array (
		"edit"    =>  "Edit",
		"copy"    =>  "Copy",
		"costs"   =>  "Costs",
		"new"     =>  "Create New" 
);

if (($subtitle [$_REQUEST['action']]) && (! $_REQUEST['reqaction'])) {
	$title .= ' - ' . $subtitle [$_REQUEST['action']];
}

if ($_REQUEST['reqaction']) {
	$title .= ' - ' . $subtitle [$_REQUEST['reqaction']];
}

//Set page title
$TemplateObj->title	=	$title;

include COMMON_DIR . 'listings/SocialMedia.inc';

if ($permit ['Requisitions'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

include COMMON_DIR . 'process/FormFunctions.inc';

$scripts_header[] = "js/loadAJAX.js";
$TemplateObj->page_scripts_header = $scripts_header;

$scripts_footer[] = "js/requisition.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

if ($_REQUEST ['updatereq'] != "") {

    ########################## Insert New Requisition #########################
    
    /**
     * @Set Dates
     */
    // Set where condition
    $where		=	array ("OrgID = :OrgID");
    // Set parameters
    $params 	=	array (":OrgID"=>$OrgID);
    
    // Set columns
    $columns	=	"date_format(MIN(DATE(PostDate)), '%m/%d/%Y') as R1,
					date_format(MAX(DATE(PostDate)), '%m/%d/%Y') as R2,
					date_format(DATE_SUB(CURDATE(),INTERVAL 6 MONTH),'%m/%d/%Y') as R3,
					date_format(DATE_ADD(CURDATE(),INTERVAL 6 MONTH), '%m/%d/%Y') as R4,
					YEAR(MIN(DATE(PostDate))) as R5,
					YEAR(MAX(DATE(PostDate))) as R6";
    
    // Get Requisition Information
    $results	=	$RequisitionsObj->getRequisitionInformation ( $columns, $where, "", "", array ($params) );
    
    $row 		=	$results ['results'] [0];
    $StartDate 	=	$row ['R1'];
    $EndDate 	=	$row ['R2'];
    
    if ($StartDate == '00/00/0000' || $EndDate == '00/00/0000') {
        $StartDate = $row ['R3'];
        $EndDate = $row ['R4'];
    }
    
    $sde = explode ( "/", $StartDate );
    $ede = explode ( "/", $EndDate );
    
    if ($row ['R5'] == "0000")
        $row ['R5'] = "2000";
    if ($row ['R6'] == "0000")
        $row ['R5'] = date ( 'Y' );
    
    // Set Minimum, Maximum Dates
    if ($sde [0] == "00" || $sde [1] == "00" || $sde [2] == "0000")
        $StartDate = "01/01/" . $row ['R5'];
    if ($ede [0] == "00" || $ede [1] == "00" || $ede [2] == "0000")
        $EndDate = "12/31/" . $row ['R6'];
    
    // Get Time
    $rowtime	=	$MysqlHelperObj->getTime ();
    
    $rkpid		=  $_REQUEST['OriginalRequestID'];
    $RequestID	=  uniqid () . rand ( 1, 1 );
    
    /**
     * Insert New Requisition
     */
    $rowreq		=	array ();
    
    // Set where condition
    $where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
    // Set parametes
    $params		=	array (":OrgID" => $OrgID, ":RequestID" => $rkpid);
    // Get Requisitions Information
    $results	=	$RequisitionsObj->getRequisitionInformation ( "*", $where, "", "", array ($params) );
    
    $rowreq		=	$results ['results'] [0];
    
    // Insert the same record with new RequisitionID and New Dates
    $rowreq ['RequestID']               =   $RequestID;
    $rowreq ['DateEntered']             =   "NOW()";
    $rowreq ['LastModified']            =   "NOW()";
    $rowreq ['PostDate']                =   $DateHelperObj->getYmdFromMdy ( $StartDate ) . " " . $rowtime;
    $rowreq ['ExpireDate']              =   $DateHelperObj->getYmdFromMdy ( $EndDate ) . " " . $rowtime;
    $rowreq ['ApplicantsCount']         =   0;
    $rowreq ['RequisitionStage']        =   "";
    $rowreq ['RequisitionStageReason']  =   "";
    $rowreq ['RequisitionStageDate']    =   "0000-00-00";

    // Insert requisitions
    $ins_req_result =	$RequisitionsObj->insRequisitionsInfo ( 'Requisitions', $rowreq );

    $RequisitionFormID	=	G::Obj('RequisitionForms')->getDefaultRequisitionFormID($OrgID);
    
    $columns		=	"OrgID, QuestionID, QuestionOrder, Question, QuestionTypeID, value";
    $form_que_list	=   G::Obj('RequisitionQuestions')->getRequisitionQuestionsByQueID($columns, $OrgID, $RequisitionFormID);
    
    $req_data_list	=	G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $rkpid);
    
    $insert_reqdata_info = array();
    if(is_array($req_data_list)) {
        foreach ($req_data_list as $req_que_id=>$req_answer) {
        	//Question Information
        	$QI 	= 	$form_que_list[$req_que_id];
            // Insert requisitions
            $insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$req_que_id, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI['Question'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Answer"=>$req_answer, ":AnswerStatus"=>1);
        }
    
        if(count($insert_reqdata_info)) {
            //Insert requisitions data
            G::Obj('RequisitionsData')->insRequisitionsData($insert_reqdata_info);
        }
    }
    
    $RequisitionsObj->insertInternalPrescreenFormsByRequestID ( $OrgID, $RequestID, $rkpid );
    
    /**
     * Update Existing Requisition Status
     */
    // Set information
    $set_info	=	array ("Active = 'N'");
    // Set where condition
    $where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
    // Set parameters
    $params		=	array (":OrgID" => $OrgID, ":RequestID"=>$rkpid);
    // Update requisitions information
    $req_upd_status	=	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array($params) );
    
    // Set where condition
    $where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
    // Set the parameters
    $params		=	array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
    // Delete from RequisitionManagers table
    $RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where, array ($params) );
    
    // Set where condition
    $where			=	array ("OrgID = :OrgID", "RequestID = :RequestID");
    // set parameters
    $params			=	array (":OrgID" => $OrgID, ":RequestID" => $rkpid);
    // get requisition managers information
    $resreqmanagers	=	$RequisitionsObj->getRequisitionManagersInfo ( "UserID", $where, "", "", array ($params) );
    
    if (is_array ( $resreqmanagers ['results'] )) {
        foreach ( $resreqmanagers ['results'] as $val ) {
            // Requisition managers information
            $req_man_info = array (
                "OrgID" 	=> $OrgID,
                "RequestID" => $RequestID,
                "UserID" 	=> $val ['UserID']
            );
    
            if ($val ['UserID'] != "") {
                // Insert requisitions
                $RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
            }
        } // end foreach
    }
    
    // Set where condition
    $where = array ("OrgID = :OrgID", "RequestID = :RequestID");
    // Set parameters
    $params = array (":OrgID" => $OrgID, ":RequestID" => $rkpid);
    // Get Requisition Org Levels Information
    $resreqorglevels = $RequisitionsObj->getRequisitionOrgLevelsInfo ( "*", $where, "", array ($params) );
    
    // Insert Requisition Org Levels Information
    if (is_array ( $resreqorglevels ['results'] )) {
        foreach ( $resreqorglevels ['results'] as $roworglevels ) {
    
            $req_org_level_info = array (
                "OrgID" 			=> $OrgID,
                "RequestID" 		=> $RequestID,
                "OrgLevelID" 		=> $roworglevels ['OrgLevelID'],
                "SelectionOrder" 	=> $roworglevels ['SelectionOrder']
            );
    
            // Insert requisitions
            $RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_level_info );
        }
    }
    
    ########################## End Insert New Requisition #####################
    
	// PostDate & ExpiryDate
	$postdatetime      =   $DateHelperObj->getYmdFromMdy ( $_REQUEST ['PostDate'] [$_REQUEST['OriginalRequestID']], "/", "-" );
	$expiredatetime    =   $DateHelperObj->getYmdFromMdy ( $_REQUEST ['ExpireDate'] [$_REQUEST['OriginalRequestID']], "/", "-" );

	// Calculate PostDate Hours
	$phrs = $_REQUEST ['PostDateSIHrs'] [$_REQUEST['OriginalRequestID']];
	if ($_REQUEST ['PostDateSIAmPm'] [$_REQUEST['OriginalRequestID']] == "AM") {
		if ($phrs == 12)
			$phrs = "00";
	} else if ($_REQUEST ['PostDateSIAmPm'] [$_REQUEST['OriginalRequestID']] == "PM") {
		$phrs += 12;
		if ($phrs == 24)
			$phrs -= 12;
	}

	// Calculate PostDate Minutes
	$pmins = $_REQUEST ['PostDateSIMins'] [$_REQUEST['OriginalRequestID']];
	if ($_REQUEST ['PostDateSIMins'] [$_REQUEST['OriginalRequestID']] == 0) {
		$pmins = "00";
	}
	$postdatetime .= " " . $phrs . ":" . $pmins . ":00";

	// Calculate ExpireDate Hours
	$ehrs = $_REQUEST ['ExpireDateSIHrs'] [$_REQUEST['OriginalRequestID']];
	if ($_REQUEST ['ExpireDateSIAmPm'] [$_REQUEST['OriginalRequestID']] == "AM") {
		if ($ehrs == 12)
			$ehrs = "00";
	} else if ($_REQUEST ['ExpireDateSIAmPm'] [$_REQUEST['OriginalRequestID']] == "PM") {
		$ehrs += 12;
		if ($ehrs == 24)
			$ehrs -= 12;
	}

	// Calculate ExpireDate Minutes
	$emins = $_REQUEST ['ExpireDateSIMins'] [$_REQUEST['OriginalRequestID']];
	if ($_REQUEST ['ExpireDateSIMins'] [$_REQUEST['OriginalRequestID']] == 0) {
		$emins = "00";
	}
	$expiredatetime .= " " . $ehrs . ":" . $emins . ":00";

	// Set update information
	$set_info = array (
			"Active                  =   'Y'",
			"JobID                   =   :JobID",
			"PostDate                =   :PostDate",
			"ExpireDate              =   :ExpireDate"
	);
	// Set where condition
	$where	=	array (
			"RequestID = :RequestID"
	);
	// Set parameters
	$params = array (
			":JobID" 				=>  $_REQUEST ['JobID'] [$_REQUEST['OriginalRequestID']],
			":PostDate" 			=>  $postdatetime,
			":ExpireDate" 			=>  $expiredatetime,
	        ":RequestID"            =>  $RequestID
	);

	// Update Requisitions information
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array (
			$params
	) );
    
	echo "<h3>Job is successfully reposted</h3>";
	exit;
}
else {
    // Set columns
    $columns = "RequestID, RequisitionID, JobID, date_format(PostDate,'%m/%d/%Y') PostDate,
    		    DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate, Title,
    		    DATEDIFF(NOW(),PostDate) Open, DATEDIFF(ExpireDate,PostDate) Closed,
    		    DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current";
    // Set where condition
    $where	= array ("RequestID = :RequestID");
    // Set params
    $params_info = array (":RequestID"=>$_REQUEST['OriginalRequestID']);
    // Get requisition information
    $requisition_results = $RequisitionsObj->getRequisitionInformation ( $columns, $where,  "", "", array($params_info) );
    
    $value = $_REQUEST['OriginalRequestID'];    	
}

require_once IRECRUIT_DIR . 'requisitions/RepostedRequisitionsInformation.inc';
?>
<script>date_picker('#PostDate<?php echo $value?>, #ExpireDate<?php echo $value?>', 'mm/dd/yy');</script>