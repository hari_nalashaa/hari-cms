<?php
require_once '../Configuration.inc';

$RequestID = $_REQUEST['RequestID'];

$is_subscription_allowed = "No";

$zip_recruiter_subscription_info = $ZipRecruiterObj->getZipRecruiterSubscriptionSettingsInfo($OrgID);

if($zip_recruiter_subscription_info['TotalSubscriptions'] > 0) {
    if($RequestID != "") {
        $zip_feed_requisition = $ZipRecruiterObj->getZipRecruiterFeed($OrgID, $RequestID);
        
        if($zip_feed_requisition['RequestID'] != "") {
            $is_subscription_allowed = "No";             
        }
    }
}

$subscription_options = array("SubscriptionAllowed"=>$is_subscription_allowed);
echo json_encode($subscription_options);
?>