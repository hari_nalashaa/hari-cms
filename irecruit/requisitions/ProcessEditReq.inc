<?php
if ($_POST ['PostMeridian'] == "AM") {
	if ($PostHour == 12) {
		$PostHour = "00";
	}
}
if ($_POST ['PostMeridian'] == "PM") {
	if ($PostHour < 12) {
		$PostHour += 12;
	}
}
if ($_POST ['ExpireMeridian'] == "AM") {
	if ($ExpireHour == 12) {
		$ExpireHour = "00";
	}
}
if ($_POST ['ExpireMeridian'] == "PM") {
	if ($ExpireHour < 12) {
		$ExpireHour += 12;
	}
}

$PostDate = substr ( $PostDate, - 4 ) . '-' . substr ( $PostDate, 0, 2 ) . '-' . substr ( $PostDate, 3, 2 ) . ' ' . $PostHour . ':' . $PostMinute . ':00';
$ExpireDate = substr ( $ExpireDate, - 4 ) . '-' . substr ( $ExpireDate, 0, 2 ) . '-' . substr ( $ExpireDate, 3, 2 ) . ' ' . $ExpireHour . ':' . $ExpireMinute . ':00';

// $RequisitionID = preg_replace("/[^[a-z\d-]/i", "", $RequisitionID);
// $JobID = preg_replace("/[^a-z\d-]/i", "", $JobID);

// Set where condition
$where = array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
// Set parameters
$params = array (":OrgID" => $OrgID, ":MultiOrgID" => $MultiOrgID);
// Organization Levels Information
$results = $OrganizationsObj->getOrganizationLevelsInfo ( "MAX(OrgLevelID) AS MaxOrgLevelID", $where, '', array($params) );
if(is_array($results ['results'] [0])) list ( $Levels ) = array_values ( $results ['results'] [0] );

if(!isset($Levels)) $Levels = 0;

// insert code
if (($action == 'new') || ($action == 'copy')) {
	
	$OriginalRequestID = $_REQUEST ['RequestID'];
	
	$RequestID = uniqid ();
	
	if ($Owner == "") {
		$Owner = $USERID;
	}
	
	// Requisition information to insert
    $requisition_info   =   array(
                                "OrgID"                 =>  $OrgID,
                                "MultiOrgID"            =>  $_REQUEST['MultiOrgID'],
                                "RequisitionID"         =>  $_POST ['RequisitionID'],
                                "RequestID"             =>  $RequestID,
                                "JobID"                 =>  $_POST ['JobID'],
                                "ListingPriority"       =>  $_POST ['ListingPriority'],
                                "EmpStatusID"           =>  $_POST ['EmpStatusID'],
                                "PostDate"              =>  $PostDate,
                                "ExpireDate"            =>  $ExpireDate,
                                "Title"                 =>  $_POST ['Title'],
                                "Description"           =>  $_POST ['content'],
                                "InternalFormID"        =>  $_POST ['InternalFormID'],
                                "FormID"                =>  $_POST ['FormID'],
                                "EEOCode"               =>  $_POST ['EEOCode'],
                                "JobGroupCode"          =>  $_POST ['JobGroupCode'],
                                "Owner"                 =>  $Owner,
                                "DateEntered"           =>  "NOW()",
                                "LastModified"          =>  "NOW()",
                                "City"                  =>  $_POST ['City'],
                                "State"                 =>  strtoupper ( $_POST ['State'] ),
                                "ZipCode"               =>  $_POST ['ZipCode'],
                                "Country"               =>  $_POST ['Country'],
                                "InternalDisplayAA"     =>  $_POST ['InternalDisplayAA'],
                                "DisplayAA"             =>  $_POST ['DisplayAA'],
                                "Address1"              =>  $_POST ['Address1'],
                                "Address2"              =>  $_POST ['Address2'],
                                "AdvertisingOptions"    =>  $SAO,
                                "IsAuditor"             =>  $_POST ['IsAuditor'],
                                "IsApprover"            =>  $_POST ['IsApprover'],
                                "MultiCostCenter"       =>  $_POST ['MultiCostCenter'],
                                "Comment"               =>  $_POST ['Comment'],
                                "Exempt_Status"         =>  $_POST['Exempt_Status'],
                                "WorkDays"              =>  $workdays,
                                "Hours"                 =>  $_POST ['Hours'],
                                "Weeks"                 =>  $_POST ['Weeks'],
                                "Program"               =>  $_POST ['Program'],
                                "Replacement"           =>  $_POST ['Replacement'],
                                "ReportsTo"             =>  $_POST ['ReportsTo'],
                                "POE_from"              =>  $DateHelperObj->getYmdFromMdy($_POST ['POE_from'], "/", "-"),
                                "POE_to"                =>  $DateHelperObj->getYmdFromMdy($_POST ['POE_to'], "/", "-"),
                                "MonsterJobCategory"    =>  $_POST ['JobCategory'],
                                "MonsterJobOccupation"  =>  $_POST ['JobOccupations'],
                                "Duration"              =>  $_POST ['Duration'],
                                "MonsterJobPostedDate"  =>  "NOW()",
                                "MonsterJobPostType"    =>  $_POST ['MonsterJobPostType'],
                                "MonsterJobIndustry"    =>  $_POST ['MonsterJobIndustry'] 
                            );
	
	if($feature['RequisitionApproval'] == "Y")
	{
		$requisition_info["Active"] = "N";
		$requisition_info["Approved"] = "N";
	}
	else {
		$requisition_info["Active"] = "Y";
		$requisition_info["Approved"] = "Y";
	}
	
	//Get RequisitionFormID
	$RequisitionFormID =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);
	//Set Default RequisitionFormID
	$requisition_info['RequisitionFormID'] =   $RequisitionFormID;
	// Insert Requisitions Information
	$RequisitionsObj->insRequisitionsInfo ( 'Requisitions', $requisition_info );
	
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID" 
	);
	// Set parameters information
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID 
	);
	// Delete RequisitionManagers Information
	$RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where, array (
			$params 
	) );
	
	if (is_array ( $_POST ['Managers'] )) {
		while ( list ( $key, $val ) = each ( $_POST ['Managers'] ) ) {
			
			// Requisition Managers Information to insert
			$req_man_info = array (
					"OrgID" => $OrgID,
					"RequestID" => $RequestID,
					"UserID" => $val 
			);
			
			if ($val != "") {
				// Insert RequisitionManagers Information
				$RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
			}
		} // end while
	} // end if Managers is array
	
	
	// set where condition
	$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
	// set parameters
	$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
	// Delete requisition org levels information
	$RequisitionsObj->delRequisitionsInfo ( 'RequisitionOrgLevels', $where, array($params) );
	
	$i = 1;
	while ( $i <= $Levels + 1 ) {
		if (is_array ( $_POST [$i] )) {
			while ( list ( $key, $val ) = each ( $_POST [$i] ) ) {
				if ($val) {
					// RequisitionOrgLevels information to insert
					$req_org_levels_info = array (
							"OrgID" => $OrgID,
							"RequestID" => $RequestID,
							"OrgLevelID" => $i,
							"SelectionOrder" => $val 
					);
					
					// Insert RequisitionOrgLevels
					$RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_levels_info);
				} // end if val
			} // end while
		} // end is_arrayPOST
		$i ++;
	} // end while Levels
	
	if ($OriginalRequestID != "" && $action == "copy") {
		$RequisitionsObj->insertInternalPrescreenFormsByRequestID ( $OrgID, $RequestID, $OriginalRequestID );
	}
	
	$message = 'Added Req/Job ID: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $_REQUEST['MultiOrgID'], $RequestID ) . '.';
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('" . $message . "')";
	echo '</script>';
	
	echo '<meta http-equiv="refresh" content="0;url=requisitionsSearch.php?Active=' . $Active . '">';
	exit ();
}

// update code
if ($action == 'edit') {
	if ($Owner == "") {
		$Owner = $USERID;
	}

	######################
	$ADVERTISEOPTIONS = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
	$AO    =   array();
	$SAO   =   serialize ( $AO );
	if(is_array($ADVERTISEOPTIONS)) {
		foreach ( $ADVERTISEOPTIONS as $option ) {
			$AO [$option] = $_POST [$option];
		}
		$SAO = serialize ( $AO );
	}
	
	$ad_valid_status = "true";
	if(is_array($AdvertisingOptions)) {
		foreach ($AdvertisingOptions as $AOKeyName=>$AOValueName) {
			if($AdvertisingOptions[$AOKeyName] != "") $ad_valid_status = "false";
		}
	}
	
	
	$WORKWEEK  =   array ("SUN" => "Sunday", "MON" => "Monday", "TUE" => "Tuesday", "WED" => "Wednesday", "THU" => "Thursday", "FRI" => "Friday", "SAT" => "Saturday");
	
	$WW        =   array_keys ( $WORKWEEK );
	$WorkWeek  =   array ();
	
	foreach ( $WW as $d ) {
		if ($_POST [$d] == "Y") {
			$WorkWeek [$d] = $_POST [$d];
		}
	}
	
	$workdays = serialize ( $WorkWeek );
	
	######################
	
	$req_approved_info = $RequisitionsObj->getReqDetailInfo("Approved", $OrgID, $_REQUEST['MultiOrgID'], $RequestID);
	
	// Set information
	$set_info = array (
			"RequisitionID           = :RequisitionID",
			"JobID                   = :JobID",
			"ListingPriority         = :ListingPriority",
			"EmpStatusID             = :EmpStatusID",
			"PostDate                = :PostDate",
			"ExpireDate              = :ExpireDate",
			"Active                  = :Active",
			"Title                   = :Title",
			"Description             = :Description",
			"InternalFormID          = :InternalFormID",
			"FormID                  = :FormID",
			"EEOCode                 = :EEOCode",
			"JobGroupCode            = :JobGroupCode",
			"Owner                   = :Owner",
			"City                    = :City",
			"State                   = :State",
			"ZipCode                 = :ZipCode",
			"Country                 = :Country",
			"InternalDisplayAA       = :InternalDisplayAA",
			"DisplayAA               = :DisplayAA",
			"MultiOrgID              = :MultiOrgID",
			"Address1                = :Address1",
			"Address2                = :Address2",
			"AdvertisingOptions      = :AdvertisingOptions",
			"IsAuditor               = :IsAuditor",
			"IsApprover              = :IsApprover",
			"MultiCostCenter         = :MultiCostCenter",
			"Comment                 = :Comment",
			"Exempt_Status           = :Exempt_Status",
			"WorkDays                = :WorkDays",
			"Hours                   = :Hours",
			"Weeks                   = :Weeks",
			"Program                 = :Program",
			"Replacement             = :Replacement",
			"ReportsTo               = :ReportsTo",
			"POE_from                = :POE_from",
			"POE_to                  = :POE_to",
			"MonsterJobCategory      = :MonsterJobCategory",
			"MonsterJobOccupation    = :MonsterJobOccupation",
			"Duration                = :Duration",
			"MonsterJobPostType      = :MonsterJobPostType",
			"MonsterJobIndustry      = :MonsterJobIndustry" 
	);
	// Set where condition
	$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
		
	// Set parameters
	$params = array (
			":RequisitionID"         => $_POST ['RequisitionID'],
			":JobID"                 => $_POST ['JobID'],
			":ListingPriority"       => $_POST ['ListingPriority'],
			":EmpStatusID"           => $_POST ['EmpStatusID'],
			":PostDate"              => $PostDate,
			":ExpireDate"            => $ExpireDate,
			":Title"                 => $_POST ['Title'],
			":Description"           => $_POST ['content'],
			":InternalFormID"        => $_POST ['InternalFormID'],
			":FormID"                => $_POST ['FormID'],
			":EEOCode"               => $_POST ['EEOCode'],
			":JobGroupCode"          => $_POST ['JobGroupCode'],
			":Owner"                 => $Owner,
			":City"                  => $_POST ['City'],
			":State"                 => strtoupper ( $_POST ['State'] ),
			":ZipCode"               => $_POST ['ZipCode'],
			":Country"               => $_POST ['Country'],
			":InternalDisplayAA"     => $_POST ['InternalDisplayAA'],
			":DisplayAA"             => $_POST ['DisplayAA'],
			":Address1"              => $_POST ['Address1'],
			":Address2"              => $_POST ['Address2'],
			":AdvertisingOptions"    => $SAO,
			":IsAuditor"             => $_POST ['IsAuditor'],
			":IsApprover"            => $_POST ['IsApprover'],
			":MultiCostCenter"       => $_POST ['MultiCostCenter'],
			":Comment"               => $_POST ['Comment'],
			":Exempt_Status"         => $_POST['Exempt_Status'],
			":WorkDays"              => $workdays,
			":Hours"                 => $_POST ['Hours'],
			":Weeks"                 => $_POST ['Weeks'],
			":Program"               => $_POST ['Program'],
			":Replacement"           => $_POST ['Replacement'],
			":ReportsTo"             => $_POST ['ReportsTo'],
			":POE_from"              => $DateHelperObj->getYmdFromMdy($_POST ['POE_from'], "/", "-"),
			":POE_to"                => $DateHelperObj->getYmdFromMdy($_POST ['POE_to'], "/", "-"),
			":MonsterJobCategory"    => $_POST ['JobCategory'],
			":MonsterJobOccupation"  => $_POST ['JobOccupations'],
			":Duration"              => $_POST ['Duration'],
			":MonsterJobPostType"    => $_POST ['MonsterJobPostType'],
			":MonsterJobIndustry"    => $_POST ['MonsterJobIndustry'],
			":OrgID"                 => $OrgID,
			":MultiOrgID"            => $_REQUEST['MultiOrgID'],
			":RequestID"             => $_REQUEST ['RequestID'] 
	);
	
	$params[":Active"] = $_POST['Active'];
	
	if($feature['RequisitionApproval'] == "Y" && ($req_approved_info['Approved'] == "" || $req_approved_info['Approved'] == "N")) {
		$params[":Active"] = "N";
	}
	
	// Update Requisitions Information
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array ($params) );
	
	// set where condition to delete RequisitionOrgLevels, RequisitionManagers Information
	$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
	// set parameters to delete the RequisitionOrgLevels, RequisitionManagers Information
	$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
	
	// Delete RequisitionOrgLevels Information
	$RequisitionsObj->delRequisitionsInfo ( 'RequisitionOrgLevels', $where, array ($params) );
	// Delete RequisitionManagers Information
	$RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where, array ($params) );
	
	if (is_array ( $_POST [Managers] )) {
		while ( list ( $key, $val ) = each ( $_POST [Managers] ) ) {
			
			// RequisitionManagers Information to insert
			$req_man_info = array (
					"OrgID" => $OrgID,
					"RequestID" => $RequestID,
					"UserID" => $val 
			);
			
			if ($val != "") {
				// Insert RequisitionManagers Information
				$RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
			}
		} // end while
	} // end if owner an array
	
	$i = 1;
	while ( $i <= $Levels ) {
		if (is_array ( $_POST [$i] )) {
			while ( list ( $key, $val ) = each ( $_POST [$i] ) ) {
				if ($val) {
					// RequisitionOrgLevels information to insert
					$req_org_levels_info   =   array(
                                                    "OrgID" => $OrgID,
                                                    "RequestID" => $RequestID,
                                                    "OrgLevelID" => $i,
                                                    "SelectionOrder" => $val 
                                                );
					// Insert RequisitionOrgLevels
					$RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_levels_info );
				} // end if val
			} // end while
		} // end if is_arrayPOST
		$i ++;
	} // end while Levels
	
	$message = 'Edited Req/Job ID: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $_REQUEST['MultiOrgID'], $RequestID ) . '.';
}