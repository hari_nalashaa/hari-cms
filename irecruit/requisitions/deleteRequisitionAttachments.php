<?php
require_once '../irecruitdb.inc';
require_once '../Configuration.inc';

if ($_REQUEST ['RequestID'] != "" && $_REQUEST ['OrgID'] != "" &&  $_REQUEST ['FileID'] != "") {	
    $set_info       =   array("Deleted = :Deleted");
    $where_info     =   array("OrgID = :OrgID","RequestID = :RequestID","FileID = :FileID");
    $params         =   array(":Deleted"=>1, ":OrgID"=>$_REQUEST ['OrgID'], ":RequestID"=>$_REQUEST ['RequestID'],":FileID"=>$_REQUEST ['FileID']);

    $MultiOrgID =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID); 
    $REQS = $RequisitionsObj->getReqDetailInfo('Title', $_REQUEST ['OrgID'], $MultiOrgID, $_REQUEST ['RequestID']);
   
    //Update Status Category Information
    $updated_res = G::Obj('RequisitionAttachments')->updRequisitionAttachments($set_info, $where_info, array($params));
    $filename = G::Obj('RequisitionAttachments')->getAttachmentFileName($_REQUEST['OrgID'], $_REQUEST['RequestID'], $_REQUEST['FileID']);

	$req_history_delete = array(
 	"OrgID"        	 => $_REQUEST ['OrgID'],
        "RequestID"    	 => $_REQUEST ['RequestID'],
        "UpdatedFields"  =>  [],
	"RequestTitle" 	 => $REQS ['Title'],
        "Comments" 	 => "Deleted Attachment - " . $filename,
         "UserID"        =>  $USERID,
         "CreatedDateTime"   =>  "NOW()",

        );
        // Insert Attachments Information
        $insert_req_history = G::Obj('RequisitionHistory')->insRequisitionHistory($req_history_delete);
	return;

} 

?>
