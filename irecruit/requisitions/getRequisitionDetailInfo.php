<?php

require_once '../Configuration.inc';

require_once COMMON_DIR . 'listings/SocialMedia.inc';

$RequestID 			= $_REQUEST['RequestID'];
$MultiOrgID 		= $_REQUEST['MultiOrgID'];
$GuIDRequisition	= $_REQUEST['GuIDRequisition'];

//SearchQuery Information to insert
$insert_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$USERID, "RequisitionGuID" => $GuIDRequisition, "RequestID"=>$RequestID, "LastUpdated"=>"NOW()");
//Set SearchQuery Information
$on_update = " ON DUPLICATE KEY UPDATE MultiOrgID = :UMultiOrgID, RequestID = :URequestID, LastUpdated = NOW()";
//Update Information
$update_info = array(":UMultiOrgID"=>$MultiOrgID, ":URequestID"=>$RequestID);
//Insert Search Query
$RequisitionsObj->insRequisitionsSearchParams($insert_info, $on_update, $update_info);

$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
$new_job_days = $highlight_req_settings['NewJobDays'];

// Set columns
$columns = "date_format(PostDate,'%m/%d/%Y') PostDate, date_format(ExpireDate,'%m/%d/%Y') ExpireDate,
			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,
			datediff(now(),PostDate) Open, datediff(ExpireDate,PostDate) Closed,
			date_format(date_add(now(), INTERVAL 0 DAY),'%m/%d/%Y') Current,
			RequestID, Title, MultiOrgID, Duration, RequisitionID, JobID,
			Description, MonsterJobPostType, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry, FreeJobBoardLists,
            Active, Approved, RequisitionStage, Address1, Address2, City, State, 
            ZipCode, date_format(RequisitionStageDate,'%m/%d/%Y') as RequisitionStageDate, ZipRecruiterTrafficBoost, ZipRecruiterJobCategory";

if($new_job_days > 0) {
    $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
}
else {
	$columns .= ", Highlight";
}

$REQS = $RequisitionsObj->getReqDetailInfo($columns, $OrgID, $MultiOrgID, $RequestID);

// Get Social Media Information
$SM           =   G::Obj('SocialMedia')->getSocialMediaDetailInfo($OrgID, $MultiOrgID);

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results_stages = $RequisitionsObj->getRequisitionStageInfo("RequisitionStage", "Code, Description", $where, "Description", array($params));
$req_stages = array();

if(is_array($results_stages['results'])) {
    foreach($results_stages['results'] as $row_stage) {
        $stage_code                 =   $row_stage ['Code'];
        $stage_description          =   $row_stage ['Description'];
        $req_stages[$stage_code]    =   $stage_description;
    }
}

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results_stage_reasons = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));
$req_stage_reasons = array();

if(is_array($results_stage_reasons['results'])) {
    foreach($results_stage_reasons['results'] as $row_stage_reason) {
        $stage_reason_code                      =   $row_stage_reason ['Code'];
        $stage_reason_description               =   $row_stage_reason ['Description'];
        $req_stage_reasons[$stage_reason_code]  =   $stage_reason_description;
    }
}

$MonsterObj = new Monster ();
$MonsterInformation = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
$MonsterOrgInfo = $MonsterInformation ['row'];

if ($REQS['Duration'] == '' || $REQS['Duration'] == 0) {
	$REQS['Duration'] = $MonsterOrgInfo ['Duration'];
}

$allow = array ("I", "B");

$smurl = PUBLIC_HOME . 'jobRequest.php?';
$smurl .= 'OrgID=' . $OrgID;
if ($MultiOrgID) {
	$smurl .= '&MultiOrgID=' . $MultiOrgID;
}
$smurl .= '&RequestID=' . $RequestID;

if (in_array ( $SM ['Placement'], $allow )) {

	$facebook = false;
	if ($SM ['Facebook'] == "Y") {
		$facebook = true;
		$REQS['Facebook'] = facebook ( $smurl, $REQS ['Title'], "yes", strip_tags ( $REQS ['Description'] ));
	}

	$twitter = false;
	if ($SM ['Twitter'] == "Y") {
		$twitter = true;
		$REQS['Twitter'] = twitter ( $smurl, $REQS ['Title'], "yes");
	}
	
	$linkedin = false;
	if ($SM ['LinkedIn'] == "Y") {
		$linkedin = true;
		$REQS['Linkedin'] = linkedin ( $smurl, $REQS ['Title'], strip_tags ( $REQS ['Description'] ), "yes");
	}

} // end social media in array

if($facebook == false) $REQS['Facebook'] = "";
if($twitter == false) $REQS['Twitter'] = "";
if($linkedin == false) $REQS['Linkedin'] = "";

$REQS['feature'] = $feature;

// Set where condition
$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
// Set parameters
$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Get Job Applications Information
$resultsCNT = $ApplicationsObj->getJobApplicationsInfo ( "count(*) as cnt", $where, '', '', array ($params) );
$ttcnt = $resultsCNT ['results'] [0] ['cnt'];

$REQS['ApplicantsCount'] = $ttcnt;

$REQS['RequisitionStage']       =   $req_stages[$REQS['RequisitionStage']];
$REQS['RequisitionStageReason'] =   $req_stage_reasons[$REQS['RequisitionStageReason']];

$subscription_settings  =   $ZipRecruiterObj->getZipRecruiterSubscriptionSettingsInfo($OrgID);
$subscriptions_count    =   $ZipRecruiterObj->getZipRecruiterSubscriptionsCount($OrgID);

$REQS['TotalSubscriptions']         =   $subscription_settings['TotalSubscriptions'];
$REQS['SubscriptionsCount']         =   $subscriptions_count;
$REQS['RemainingSubscriptions']     =   $subscription_settings['TotalSubscriptions'] - $subscriptions_count;
$REQS['IsPaid']                     =   $ZipRecruiterObj->isPaidRequisition($OrgID, $RequestID);

$REQS['RequisitionJobBoards']       =   json_decode($RequisitionDetailsObj->getRequisitionJobBoardsStatus($OrgID, $RequestID), true);

/* requisition interview tab control */
$REQS['InterviewAdmin']="False";
if ($permit['Interview_Admin'] == 1) {
	if ($feature['InterviewForms'] == 'Y') {
  	    $REQS['InterviewAdmin']="True";
	}
} 

$REQS['InterviewResults']="False";
$display =   G::Obj('Interview')->presentResults($OrgID,$RequestID,$USERID,$Role,$PERMIT);
if ($display == "Y") {
  $where_info    =   array("OrgID = :OrgID", "RequestID = :RequestID");
  $params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
  $IFS =   G::Obj('Interview')->getInterviewForms("*", $where_info, "SortOrder ASC", $params);
  if ($IFS['count'] > 0) {
    $REQS['InterviewResults']="True";
  } 
}
/* requisition interview tab control */

echo json_encode($REQS);
?>
