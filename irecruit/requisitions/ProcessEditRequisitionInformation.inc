<?php
if ($_POST ['PostMeridian'] == "AM") {
	if ($PostHour == 12) {
		$PostHour = "00";
	}
}
if ($_POST ['PostMeridian'] == "PM") {
	if ($PostHour < 12) {
		$PostHour += 12;
	}
}
if ($_POST ['ExpireMeridian'] == "AM") {
	if ($ExpireHour == 12) {
		$ExpireHour = "00";
	}
}
if ($_POST ['ExpireMeridian'] == "PM") {
	if ($ExpireHour < 12) {
		$ExpireHour += 12;
	}
}

$PostDate = substr ( $PostDate, - 4 ) . '-' . substr ( $PostDate, 0, 2 ) . '-' . substr ( $PostDate, 3, 2 ) . ' ' . $PostHour . ':' . $PostMinute . ':00';
$ExpireDate = substr ( $ExpireDate, - 4 ) . '-' . substr ( $ExpireDate, 0, 2 ) . '-' . substr ( $ExpireDate, 3, 2 ) . ' ' . $ExpireHour . ':' . $ExpireMinute . ':00';

require_once IRECRUIT_DIR . 'requisitions/RequisitionFields.inc';

//Delete Requisitions Data
$del_requisition_status =   G::Obj('RequisitionsData')->delRequisitionsData($OrgID, $RequestID);

######################
/**
 * This is a temporary code until the final upgrade.
 * Upto that time, we have to keep on adding different conditions to it.
 * After final upgrade we will have to remove the loop through $_POST
*/
G::Obj('GetFormPostAnswer')->POST =   $_POST;    //Set Post Data

$columns				=	"OrgID, QuestionID, QuestionOrder, Question, QuestionTypeID, value";
$additional_ques_list	=	G::Obj('RequisitionQuestions')->getAdditionalRequisitionFields($OrgID, $RequisitionFormID);
$form_que_list			=   G::Obj('RequisitionQuestions')->getRequisitionQuestionsByQueID($columns, $OrgID, $RequisitionFormID);

$ques_to_skip          	=   array();
foreach($form_que_list as $QuestionID=>$QuestionInfo) {

	if(in_array($QuestionID, $additional_ques_list)) {
		
		$QI				=   $QuestionInfo;
		G::Obj('GetFormPostAnswer')->QueInfo = $QI;
		$values			=   G::Obj('GetFormPostAnswer')->getDisplayValue($QI['value']);

		if($QI['QuestionTypeID'] == 13) {
			//After the complete upgrade have to remove this.
			$ques_to_skip[]        	=   $QI['QuestionID']."1";
			$ques_to_skip[]        	=   $QI['QuestionID']."2";
			$ques_to_skip[]        	=   $QI['QuestionID']."3";
			
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']		=   1;
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 14) {
			//After the complete upgrade have to remove this.
			$ques_to_skip[]        	=   $QI['QuestionID']."1";
			$ques_to_skip[]        	=   $QI['QuestionID']."2";
			$ques_to_skip[]        	=   $QI['QuestionID']."3";
			$ques_to_skip[]        	=   $QI['QuestionID']."ext";

			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 15) {
			//After the complete upgrade have to remove this.
			$ques_to_skip[]        	=   $QI['QuestionID']."1";
			$ques_to_skip[]        	=   $QI['QuestionID']."2";
			$ques_to_skip[]        	=   $QI['QuestionID']."3";
			$ques_to_skip[]        	=   $QI['QuestionID'];

			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 9) {
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;

			//After the complete upgrade have to remove this.
			$qis  =   0;
			foreach ( $values as $v => $q ) {
				$qis++;
				$ques_to_skip[]    	=   $QI['QuestionID'] . "-" . $qis;
				$ques_to_skip[]    	=   $QI['QuestionID'] . "-" . $qis . "-yr";
				$ques_to_skip[]    	=   $QI['QuestionID'] . "-" . $qis . "-comments";
			}

			$ques_to_skip[]        	=   $QI['QuestionID'];
			$ques_to_skip[]        	=   $QI['QuestionID']."cnt";

			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 18) {
				
			//After the complete upgrade have to remove this.
			$cnt                   	=   G::Obj('GetFormPostAnswer')->POST[$QI['QuestionID'].'cnt'];

			for($c18 = 1; $c18 <= $cnt; $c18++) {
				$ques_to_skip[]    	=   $QI['QuestionID'].'-'.$c18;
			}
			
			$ques_to_skip[]        	=   $QI['QuestionID'];
			$ques_to_skip[]        	=   $QI['QuestionID']."cnt";

			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
			
			$ins_upd_res			=	G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 1818) {
			//After the complete upgrade have to remove this.
			$cnt                   	=   G::Obj('GetFormPostAnswer')->POST[$QI['QuestionID'].'cnt'];

			for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
				$ques_to_skip[]    	=   $QI['QuestionID'].'-'.$c1818;
			}
			$ques_to_skip[]        	=   $QI['QuestionID'];
			$ques_to_skip[]        	=   $QI['QuestionID']."cnt";

			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;

			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}

	}
	
}

$ques_to_skip[]	=	"btnCreateUpdate";
$ques_to_skip[]	=	"ValidateRequestID";
//Skip org levels
for($si = 1; $si < 10; $si++) {
	$ques_to_skip[]	= $si;
}
//Skip work day questions
$work_days_ques =	array("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");
foreach ($work_days_ques as $work_day_que_id) {
	$ques_to_skip[]	= $work_day_que_id;
}
######################

$set_info 	= array("LastModified = NOW()");
$params 	= array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
$insert_reqdata_info = array();

//Skip Duplicate Data Questions - Split Fields Data
$skip_req_data_fields   =   G::Obj('Requisitions')->skip_req_data_fields;
$skip_req_data_fields   =   array_merge($requisition_fields, $skip_req_data_fields);

//Set Insert Parameters
foreach($_POST as $PostKey=>$PostValue) {
	
	if(!in_array($PostKey, $ques_to_skip)) {
		if($PostKey != "RequisitionManagers") {
			if ($PostKey == 'shifts_schedule_time' || $PostKey == 'LabelSelect') {
			
				foreach ( $_POST [$PostKey] as $cquestion => $canswer ) {
			
					$QI = $form_que_list[$PostKey];
						
					if ($PostKey == 'LabelSelect') {
						$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
					}
					if ($PostKey == 'shifts_schedule_time') {
						$qa ['from_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
						$qa ['to_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
						$qa ['days'] 		= $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
						$answer 			= serialize ( $qa );
					}

					$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$cquestion, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI["Question"], ":QuestionTypeID"=>$QI["QuestionTypeID"], ":Answer"=>$answer, ":AnswerStatus"=>1);
				}
			}
			else {
			
				if(in_array($PostKey, $requisition_fields)) {
						
					if($PostKey == "POE_from") {
						$PostValue = $DateHelperObj->getYmdFromMdy($PostValue, "/", "-");
						$set_info[] = "POE_from = '$PostValue'";
					}
					else if($PostKey == "POE_to") {
						$PostValue = $DateHelperObj->getYmdFromMdy($PostValue, "/", "-");
						$set_info[] = "POE_to = '$PostValue'";
					}
					else if($PostKey == "PostDate") {
						$PostValue = $PostDate;
						$set_info[] = "PostDate = '$PostValue'";
					}
					else if($PostKey == "ExpireDate") {
						$PostValue = $ExpireDate;
						$set_info[] = "ExpireDate = '$PostValue'";
					}
					else if($PostKey == "MultiOrgID") {
					    //Do nothing for now, eventually we will delete it.
					    $PostValue = $_REQUEST['MultiOrgID'];
					    $set_info[] = "MultiOrgID = '$PostValue'";
					}
					else if($PostKey == "MultiCostCenter") {
	    				if(isset($_POST['MultiCostCenter']) && $_POST['MultiCostCenter'] != "") {
	                        $set_info[] = "MultiCostCenter = :MultiCostCenter";
	                        $params[":MultiCostCenter"] = 'Y';
	                    }
					}
					else if($PostKey != "RequestID" && $PostKey != "Active") {
						$set_info[] = "$PostKey = :$PostKey";
						$params[":".$PostKey] = $PostValue;
					}
				}
				else if(substr($PostKey, 0, 4) == "CUST") {
					$QI	= $form_que_list[$PostKey];
					$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$PostKey, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI['Question'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Answer"=>$PostValue, ":AnswerStatus"=>1);
				}
				else {
					if(!in_array($PostKey, $skip_req_data_fields)) {
						$QI	= $form_que_list[$PostKey];
						$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$PostKey, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI['Question'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Answer"=>$PostValue, ":AnswerStatus"=>1);
					}
				}
				
			}
		}

	}		
}

if(!isset($_POST['MultiCostCenter'])) {
    $set_info[] = "MultiCostCenter = :MultiCostCenter";
    $params[":MultiCostCenter"] = 'N';
}

$AdvertisementOptions = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
if(count($AdvertisementOptions) > 0) {
	$set_info[] = "AdvertisingOptions = :AdvertisingOptions";
	$params[":AdvertisingOptions"] = $SAO;
}
if(count($WorkWeek) > 0) {
	$set_info[] = "WorkDays = :WorkDays";
	$params[":WorkDays"] = serialize($WorkWeek);
}

$where              =   array ("OrgID = :OrgID", "RequestID = :RequestID");
$set_info[]         =   "Active = :Active";
$params[":Active"]  =   $Active;

// Update Requisitions Information
$RequisitionsObj->updRequisitionsInfo ('Requisitions', $set_info, $where, array ($params) );

//Insert data to RequisitionsData table
if(count($insert_reqdata_info) > 0) G::Obj('RequisitionsData')->insRequisitionsData($insert_reqdata_info);

// Set where condition
$where_del          =   array("OrgID = :OrgID", "RequestID = :RequestID");
// Set parameters information
$params_del         =   array(":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Delete RequisitionManagers Information
$RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where_del, array ($params_del) );

if (is_array ( $_POST ['RequisitionManagers'] )) {
	while ( list ( $key, $val ) = each ( $_POST ['RequisitionManagers'] ) ) {
			
		// Requisition Managers Information to insert
		$req_man_info =   array(
                                "OrgID"     => $OrgID,
                                "RequestID" => $RequestID,
                                "UserID"    => $val
                            );
			
		if ($val != "") {
			// Insert RequisitionManagers Information
			$RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
		}
	} // end while
} // end if Managers is array

if(preg_match ( '/createRequisition.php$/', $_SERVER ["SCRIPT_NAME"] )
	|| preg_match ( '/getEditRequisitionForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {

    $req_new_details    =   G::Obj('Requisitions')->getRequisitionsDetailInfo("*", $OrgID, $RequestID);
    $req_new_data       =   G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);
    
    //Add the master table data also
    foreach($req_new_details as $req_new_key=>$req_new_val) {
        $req_new_data[$req_new_key] =   $req_new_val;
    }    
    
    $updated_fields     =   array();
    $req_old_keys       =   array_keys($req_old_data);
    $req_new_keys       =   array_keys($req_new_data);
    
    foreach ($req_new_data as $req_new_que_id=>$req_new_answer) {
        if(!in_array($req_new_que_id, $req_old_keys)) {
            $updated_fields["Additional Fields"][$req_new_que_id]   =   $req_new_answer;   
        }
        else {
            if($req_old_data[$req_new_que_id] != $req_new_answer) {
                $updated_fields["Updated Fields"][$req_new_que_id]["Original"]  =   $req_old_data[$req_new_que_id];
                $updated_fields["Updated Fields"][$req_new_que_id]["Updated"]   =   $req_new_answer;
            }
        }
    }
    
    $insert_info    =   array(
                            "OrgID"             =>  $OrgID,
                            "RequestID"         =>  $RequestID,
                            "RequestTitle"      =>  $_REQUEST['Title'],
                            "Comments"          =>  "Update Requisition",
                            "UpdatedFields"     =>  json_encode($updated_fields),
                            "CreatedDateTime"   =>  "NOW()",
                            "UserID"            =>  $USERID
                        );
    G::Obj('RequisitionHistory')->insRequisitionHistory($insert_info);
       
}

// set where condition
$where      =   array ("OrgID = :OrgID", "RequestID = :RequestID");
// set parameters
$params     =   array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Delete requisition org levels information
$RequisitionsObj->delRequisitionsInfo ( 'RequisitionOrgLevels', $where, array($params) );

$i = 1;
while ( $i <= 10 ) {
	if (is_array ( $_POST [$i] )) {

		while ( list ( $key, $val ) = each ( $_POST [$i] ) ) {
				
			if ($val) {
				// set org levels information
				$req_org_levels =   array(
                                        "OrgID"             =>  $OrgID,
                                        "RequestID"         =>  $RequestID,
                                        "OrgLevelID"        =>  $i,
                                        "SelectionOrder"    =>  $val
                                    );
				// Insert Requisitions Informations
				$RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_levels );
			} // end if val
		} // end while
	} // end is_array
		
	$i ++;
} // end while Levels

if (! $USERID) {
	$USERID    =   'Requester';
	$Comments  =   $requisition_process_identifier.' has been updated';
	updateRequestHistory($OrgID, $RequestID, $USERID, $Comments);
} else if($USERID) {
	updateRequestHistory($OrgID, $RequestID, $USERID, $Comments);
}
?>
