<?php
if ($action == "delete") {
	
	if ($process == "Y") {
		
		######################## Common where condition code start #############################
		$where = array("RequestID = :RequestID", "OrgID = :OrgID");
		$params = array(":RequestID"=>$RequestID, ":OrgID"=>$OrgID);
		
		//Delete Requisitions Information
		$RequisitionsObj->delRequisitionsInfo('Requisitions', $where, array($params));
		
		//Delete Requisition Org Levels Information
		$RequisitionsObj->delRequisitionsInfo('RequisitionOrgLevels', $where, array($params));
		
		######################## Common where condition code end ###############################
		
		$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
		$message = 'Deleted Req/Job ID: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
		
		echo '<script language="JavaScript" type="type/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
		
		echo '<meta http-equiv="refresh" content="0;url=requisitionsSearch.php?Active=' . $Active . '">';
		exit ();
	}
}

if (! $reqaction) {
	$reqaction = 'listings';
}

if ($reqaction == 'listings') {
	include IRECRUIT_DIR . 'requisitions/EditReq.inc';
}
?>