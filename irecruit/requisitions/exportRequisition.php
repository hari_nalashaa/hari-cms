<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$title = 'Export Requisition';

//Set page title
$TemplateObj->title	= $title;

//Include configuration related javascript in footer
$scripts_header[] = "js/irec_Display.js";
$TemplateObj->page_scripts_header = $scripts_header;


//Declare mostly using super globals over here
if(isset($_REQUEST['k'])) $TemplateObj->k = $k =  $_REQUEST['k'];
if(isset($_REQUEST['RequestID'])) $TemplateObj->RequestID = $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['OrgID'])) $TemplateObj->OrgID = $OrgID =  $_REQUEST['OrgID'];

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/requisitions/ExportRequisition.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/requisitions/ExportRequisition');
}
?>