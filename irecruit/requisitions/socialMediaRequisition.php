<?php 
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$title = 'Social Media Requisition';

//Set page title
$TemplateObj->title	= $title;

//Include configuration related javascript in footer
$scripts_header[] = "js/irec_Display.js";
$TemplateObj->page_scripts_header = $scripts_header;

if(isset($_POST['desc'])){


    $where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
    $params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":MediaDescription"=>$_POST['desc']);
    $set_info       =   array("MediaDescription= :MediaDescription");
    	        
    $result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
    	         

}

    
if(isset($_FILES['Media'])){ 

   
//$fileTypeC = $_FILES ['Media'] ['type'];
//$dataC = file_get_contents ( $_FILES ['Media'] ['tmp_name'] );
	
if(!empty($_FILES['Media']['tmp_name'])){
$fileTmpPath = $_FILES['Media']['tmp_name'];
$fileName = $_FILES['Media']['name'];
$fileSize = $_FILES['Media']['size'];
$fileType = $_FILES['Media']['type'];


$fileNameCmps = explode(".", $fileName);
$fileExtension = strtolower(end($fileNameCmps));

$newFileName = $_REQUEST['RequestID']. '.' . $fileExtension;
if(isset($OrgID) && !empty($OrgID)){
 $uploadFileDir = PUBLIC_DIR.'images/'.$OrgID.'/';

}else{
 $uploadFileDir = PUBLIC_DIR.'images/';

} 
 if(!file_exists($uploadFileDir )){ 
    mkdir($uploadFileDir, 0777, true);

} 
$dest_path = $uploadFileDir.$newFileName; 
    
 $scanimg = array_diff(scandir(PUBLIC_DIR.'images/'.$OrgID), array('.', '..'));
                    foreach($scanimg as $imgck){ 
                             if(!empty($imgck)){  
                              $namecheck = explode(".",$imgck);  
                              $newimg = explode(".",$newFileName);  
                              if($namecheck[0] == $newimg[0]){
				  unlink($uploadFileDir.$imgck);

				}
			     }
                           
                    }

move_uploaded_file($fileTmpPath, $dest_path); 
}
session_start();
$_SESSION['social_tab']=1;  
header("Location: ".IRECRUIT_HOME."jobBoardPosting.php?menu=3&RequestID=".$_REQUEST['RequestID']);

}

//Declare mostly using super globals over here
if(isset($_REQUEST['k'])) $TemplateObj->k = $k =  $_REQUEST['k'];
if(isset($_REQUEST['RequestID'])) $TemplateObj->RequestID = $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['OrgID'])) $TemplateObj->OrgID = $OrgID =  $_REQUEST['OrgID'];

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/requisitions/SocialMediaRequisition.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/requisitions/SocialMediaRequisition');
}
?>