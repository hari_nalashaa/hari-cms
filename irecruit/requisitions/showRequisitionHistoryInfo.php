<?php
require_once '../Configuration.inc';

$req_history    =   G::Obj('RequisitionHistory')->getRequisitionHistoryDetailInfo($OrgID, $_REQUEST['RequestID'], $_REQUEST['RequisitionHistoryID']);
$upd_fields     =   json_decode($req_history['UpdatedFields'], true);
$req_details    =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $_REQUEST['RequestID']);
$req_ques_info  =   G::Obj('RequisitionQuestions')->getRequisitionQuestionsByQueID("QuestionID, value, Question", $OrgID, $req_details['RequisitionFormID']);

$req_ques_ans_vals      =   array();
$skip_req_ques_ans_vals =   array();

foreach($req_ques_info as $que_id=>$que_info) {
    $QueTypeID      =   $req_ques_info[$que_id]['QuestionTypeID'];
    $value          =   $req_ques_info[$que_id]['value'];
    
    if($QueTypeID == 18
        || $QueTypeID == 1818) {
        $values_list    =   explode("::", $value);
        $value_list     =   array();
        
        $i = 1;
        foreach($values_list as $values_list_val) {
            $req_ques_info[$que_id."-".$i]    =   $req_ques_info[$que_id];
            $value_info    =   explode(":", $values_list_val);
            $req_ques_info[$que_id."cnt"]   =    $req_ques_info[$que_id];
            $i++;
        }
        
        $skip_req_ques_ans_vals[]   =   $que_id;
        $skip_req_ques_ans_vals[]   =   $que_id."cnt";
    }
}
?>
<br>
<?php
$updated_fields     =   $upd_fields['Updated Fields'];
$additional_fields  =   $upd_fields['Additional Fields'];

echo "<strong>Updated Fields:<br><br></strong>";
foreach($updated_fields as $upd_field_que_id=>$upd_field_que_ans) { 
    $QueTypeID      =   $req_ques_info[$upd_field_que_id]['QuestionTypeID'];
    $value          =   $req_ques_info[$upd_field_que_id]['value'];
  
     if($req_ques_info[$upd_field_que_id]['Question'] != "") {
        echo "<strong>Question: </strong> ".$req_ques_info[$upd_field_que_id]['Question']."<br>";
    }
    else {
        echo "<strong>Question: </strong> ".$upd_field_que_id."<br>";
    }
    
    if($QueTypeID == 18
        || $QueTypeID == 1818) {
            
        $uns_original_ans   =   unserialize($upd_field_que_ans['Original']);
        $uns_updated_ans    =   unserialize($upd_field_que_ans['Updated']);
        
        $original_ans       =   array();
        foreach($uns_original_ans as $ans_key=>$ans_val) {
            $original_ans[] =   G::Obj('RequisitionQuestions')->getDisplayValue($ans_key, $value);
        }
        
        $updated_ans        =   array();
        foreach($uns_updated_ans as $ans_key=>$ans_val) {
            $updated_ans[]  =   G::Obj('RequisitionQuestions')->getDisplayValue($ans_key, $value);
        }
        
        echo "<strong>Answer Original:</strong> " . implode(", ", $original_ans) . '<br>';
        echo "<strong>Answer Updated:</strong> " . implode(", ", $updated_ans) . '<br><br>';
    }
    else if($QueTypeID == 2
            || $QueTypeID == 3) {
    
        $uns_original_ans   =   $upd_field_que_ans['Original'];
        $uns_updated_ans    =   $upd_field_que_ans['Updated'];

        if($value != "") {
            $original_ans       =   G::Obj('RequisitionQuestions')->getDisplayValue($uns_original_ans, $value);
        }
        else {
            $original_ans       =   $uns_original_ans;
        }
        
        if($value != "") {
            $updated_ans        =   G::Obj('RequisitionQuestions')->getDisplayValue($uns_updated_ans, $value);
        }
        else {
            $updated_ans        =   $uns_updated_ans;
        }
    
        echo "<strong>Answer Original:</strong> " . $original_ans . '<br>';
        echo "<strong>Answer Updated:</strong> " . $updated_ans . '<br><br>';
    }    
    else {
        echo "<strong>Answer Original:</strong> " . $upd_field_que_ans['Original'] . '<br>';
        echo "<strong>Answer Updated:</strong> " . $upd_field_que_ans['Updated'] . '<br><br>';
    }
}

echo "<br><strong>Additional Fields:</strong><br>";
foreach($additional_fields as $add_field_que_id=>$add_field_que_ans) {
    
    $QueTypeID      =   $req_ques_info[$add_field_que_id]['QuestionTypeID'];
    $value          =   $req_ques_info[$add_field_que_id]['value'];
    
    $req_ques_info_keys =   array_keys($req_ques_info);
    
    if($QueTypeID == 18
        || $QueTypeID == 1818) {
    
        if(!in_array($add_field_que_id, $skip_req_ques_ans_vals)) {

            $QuestionID     =   $add_field_que_id;
            $answer         =   G::Obj('RequisitionQuestions')->getDisplayValue($add_field_que_ans, $value);
            
            echo "<br>";
            echo "<strong>Question:</strong> ".$req_ques_info[$add_field_que_id]['Question'];
            echo "&nbsp;<strong>Answer:</strong> ".$answer. '<br>';
        }
    }
    else if($QueTypeID == 2
            || $QueTypeID == 3) {
    
        $new_answer =   $add_field_que_ans;
    
        if($value != "") {
        	$answer     =  G::Obj('RequisitionQuestions')->getDisplayValue($new_answer, $value);
        }
        else {
        	$answer     =  $new_answer;
        }

        echo "<br>";
        echo "<strong>Question:</strong> ".$req_ques_info[$add_field_que_id]['Question']."<br>";
        echo "<strong>Answer:</strong> " . $answer . '<br><br>';
    }
    else {
        echo "<br>";
        echo "<strong>Question:</strong> ".$req_ques_info[$add_field_que_id]['Question'];
        echo "&nbsp;<strong>Answer:</strong> ".$add_field_que_ans. '<br>';
    }
    
}
?>
</table>