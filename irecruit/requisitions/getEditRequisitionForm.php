<?php
require_once '../Configuration.inc';

$TemplateObj->bg_color 	=	$bg_color 	=	"yellow";
$TemplateObj->formtable =	$formtable	=	"RequisitionQuestions";
$TemplateObj->RequestID =	$RequestID 	=	isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->action 	=	$action		=	isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

require_once IRECRUIT_DIR . 'request/EmailLinks.inc';
require_once IRECRUIT_DIR . 'request/UpdateHistory.inc';

// Load Graphs Related Scripts, Only for this page
$scripts_header [] = "js/loadAJAX.js";
$scripts_header [] = "js/irec_Display.js";
$scripts_header [] = "tiny_mce/tinymce.min.js";
$scripts_header [] = "js/irec_Textareas.js";

// Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

$TemplateObj->error_inputs = $error_inputs = array();

$TemplateObj->requisition_process_identifier = $requisition_process_identifier = "Requisition";

if ($USERID) {
	if (! $RequesterName) {
		// set columns
		$columns = "CONCAT(FirstName, ' ', LastName), Phone, EmailAddress, Title";
		// set parameters
		$params = array (":UserID" => $USERID);
		// set where condition
		$where = array ("UserID = :UserID");
		$results = $IrecruitUsersObj->getUserInformation ( $columns, $where, "", array ($params) );
		list ( $RequesterName, $RequesterPhone, $RequesterEmail, $RequesterTitle ) = array_values ( $results ['results'] [0] );

		$TemplateObj->RequesterName 	= 	$RequesterName;
		$TemplateObj->RequesterPhone 	= 	$RequesterPhone;
		$TemplateObj->RequesterEmail 	= 	$RequesterEmail;
		$TemplateObj->RequesterTitle 	= 	$RequesterTitle;
	}
} // end userid

//Get RequisitionFormID
$RequisitionFormID      =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);
$requisition_details    =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $RequestID);
if($requisition_details['RequisitionFormID'] != "" && $RequestID != "") {
    $RequisitionFormID  =   $requisition_details['RequisitionFormID'];
}

$where_info         =   array("OrgID = :OrgID", "Requisition = :Requisition", "RequisitionFormID = :RequisitionFormID");
$params_info        =   array(":OrgID"=>$OrgID, ":Requisition"=>'Y', ":RequisitionFormID"=>$RequisitionFormID);
$columns            =	"OrgID, QuestionID, Question, QuestionTypeID, QuestionOrder, Request, Required, defaultValue";

$requisition_questions_info         =	G::Obj('RequisitionQuestions')->getRequisitionQuestions($columns, $where_info, "QuestionOrder ASC", array($params_info));
$TemplateObj->requisition_questions =	$requisition_questions	=	$requisition_questions_info['results'];

$WorkWeeksList = array ("SUN" => "Sunday", "MON" => "Monday", "TUE" => "Tuesday", "WED" => "Wednesday", "THU" => "Thursday", "FRI" => "Friday", "SAT" => "Saturday");

$TemplateObj->yes_no                =   $yes_no = array(""=>"Please Select", "Y"=>"Yes", "N"=>"No");
$TemplateObj->WorkWeeksList         =   $WorkWeeksList;
$TemplateObj->RequisitionFormID     =   $RequisitionFormID;

// Get Affirmative Action Settings Information
$TemplateObj->req_post_hours		=	$req_post_hours 		= array("12", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11");
$TemplateObj->req_post_mins 		=	$req_post_mins 			= array("00", "15", "30", "45");


$req_old_details                    =   G::Obj('Requisitions')->getRequisitionsDetailInfo("*", $OrgID, $RequestID);
$req_old_data                       =   G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);

//Add the master table data also
foreach($req_old_details as $req_old_key=>$req_old_val) {
    $req_old_data[$req_old_key] =   $req_old_val;
}

//Set old data
$TemplateObj->req_old_data          =    $req_old_data;

if(isset($_REQUEST['process_requisition']) && $_REQUEST['process_requisition'] == 'Y') {
	
	$ADVERTISEOPTIONS  =   preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
	$AO                =   array ();
	$SAO               =   serialize ( $AO );
	
	if(is_array($ADVERTISEOPTIONS)) {
		foreach ( $ADVERTISEOPTIONS as $option ) {
			$AO [$option] = $_POST [$option];
		}
		$SAO = serialize ( $AO );
	}
	
	$TemplateObj->AO = $AO;
	$TemplateObj->AdvertisingOptions = $AdvertisingOptions = unserialize ( $SAO );
	
	$ad_valid_status = "true";
	foreach ($AdvertisingOptions as $AOKeyName=>$AOValueName) {
		if($AdvertisingOptions[$AOKeyName] != "") $ad_valid_status = "false";
	}
	
	//Set columns
	$columns   =   "MAX(OrgLevelID) as MaxOrgLevelID";
	//Set where condition, it is common for below organization levels information function calls
	$where     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters, it is common for below organization levels information function calls
	$params    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID']);
	
	//Get organization levels information
	$results   =   G::Obj('Organizations')->getOrganizationLevelsInfo($columns, $where, '', array($params));
	$Levels    =   $results['results'][0];
	
	$i         =   1;
	$org_error =   "true";
	
	while ( $i <= $Levels ['MaxOrgLevelID'] ) {
	    if(count($_REQUEST[$i]) > 0 && $_REQUEST[$i][0] != "") {
	        $org_error = "false";
	    }
	    $i++;
	}
	
	$WW        =   array_keys ( $WorkWeeksList );
	$WorkWeek  =   array ();
	foreach ( $WW as $d ) {
		if ($_POST [$d] == "Y") {
			$WorkWeek [$d] = $_POST [$d];
		}
	}
	
	$TemplateObj->WorkWeek = $WorkWeek;
	
	for($i = 0; $i < count ( $requisition_questions ); $i ++) {
		
		if($requisition_questions[$i]['Required'] == 'Y') {
			if(($requisition_questions[$i]['QuestionTypeID'] == "18"
				|| $requisition_questions[$i]['QuestionTypeID'] == "1818")) {
					
				$QuestionIDcnt  = $_REQUEST[$requisition_questions[$i]['QuestionID']."cnt"];
		
				$error_answer = true;
				for($chkri = 1; $chkri <= $QuestionIDcnt; $chkri++) {
					if(isset($_POST [$requisition_questions[$i]['QuestionID']."-".$chkri])
					&& $_POST [$requisition_questions[$i]['QuestionID']."-".$chkri] != '') {
						$error_answer = false;
					}
				}
				if($error_answer === true) {
					$error_inputs[] = $requisition_questions[$i]['QuestionID'];
				}
					
			}
			else {
				if($_POST [$requisition_questions[$i]['QuestionID']] == ''
					&& $requisition_questions[$i]['QuestionID'] != "MultiOrgID") {
					$error_inputs[] = $requisition_questions[$i]['QuestionID'];
				}
			}
		}
		
	}
	
	if($org_error == "true") {
	    $error_inputs[] = "OrgLevel";
	}
	
	foreach ($error_inputs as $error_key=>$error_value) {
		if ($error_value == "AdvertisingOptions" && $ad_valid_status == "false") {
			unset($error_inputs[$error_key]);
		}
		if ($error_value == "WorkDays" && count($WorkWeek) > 0) {
			unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobIndustry") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobCategory") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobOccupation") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobPostType") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "ZipRecruiterJobCategory") {
		    unset($error_inputs[$error_key]);
		}

		//This will be applicable only if InternalRequisitions feature is disabled
		if($feature ['InternalRequisitions'] != "Y") {
			
			if ($error_value == "InternalFormID"
				|| $error_value == "InternalDisplayAA") {
				unset($error_inputs[$error_key]);
			}
			
		}
		
		if($feature['RequisitionRequest'] != "Y") {
			if ($error_value == "RequesterName") unset($error_inputs[$error_key]);
			if ($error_value == "RequesterPhone") unset($error_inputs[$error_key]);
			if ($error_value == "RequesterEmail") unset($error_inputs[$error_key]);
			if ($error_value == "RequesterTitle") unset($error_inputs[$error_key]);
		}
	}
	
	if(isset($_REQUEST['ValidateRequestID'])
	   && $_REQUEST['ValidateRequestID'] == $_GET['RequestID']) {

	    if($_POST['City'] == '') {
	        if(!in_array('City', $error_inputs)) {
	            //$error_inputs[] = 'City';
	        }
	    }
	    if($_POST['State'] == '') {
	        if(!in_array('State', $error_inputs)) {
	            $error_inputs[] = 'State';
	        }
	    }
	    if($_POST['ZipCode'] == '') {
	        if(!in_array('ZipCode', $error_inputs)) {
	            //$error_inputs[] = 'ZipCode';
	        }
	    }
	     
	}
	
	if(count($_REQUEST['RequisitionManagers']) > 0) {
		unset($error_inputs['RequisitionManagers']);
	}

	$APPDATA	=	array();
	// Set post values to variables
	foreach ( $_POST as $pkey => $pvalue ) {
	
		if($pkey == "MultiOrgID") $pkey	= "MultiOrgIDRequest";
		if($pkey == "Address1") $pkey = "Address1Request";
		if($pkey == "Address2") $pkey = "Address2Request";
		if($pkey == "City") $pkey = "CityRequest";
		if($pkey == "State") $pkey = "StateRequest";
		if($pkey == "ZipCode") $pkey = "ZipCodeRequest";
		if($pkey == "Country") $pkey = "CountryRequest";
		$TemplateObj->{$pkey} = ${$pkey} = $pvalue;
	
		if ($pkey == 'shifts_schedule_time' || $pkey == 'LabelSelect') {
				
			foreach ( $_POST [$pkey] as $cquestion => $canswer ) {
	
				if ($pkey == 'LabelSelect') {
					$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
				}
				if ($pkey == 'shifts_schedule_time') {
					$qa ['from_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
					$qa ['to_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
					$qa ['days'] 		= $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
					$answer 			= serialize ( $qa );
				}
					
				$APPDATA[$cquestion] = 	$answer;
			}
		}
		else {
			$APPDATA[$pkey]	= $pvalue;
		}
	}
	
	$TemplateObj->APPDATA = $APPDATA;
	
	if(count($error_inputs) > 0) {
		$TemplateObj->errors = $errors = "Please fill all the mandatory fields";
		$TemplateObj->error_inputs = $error_inputs;
	}
	else {
		$edit_insert_msg = "Successfully Updated";
		//Update requisition information
		require_once IRECRUIT_DIR . 'requisitions/ProcessEditRequisitionInformation.inc';
	}
}

$columns        =   "*, date_format(PostDate,'%m/%d/%Y') as PostDate, date_format(ExpireDate,'%m/%d/%Y') as ExpireDate,
                    date_format(PostDate,'%h') as PostHour, date_format(ExpireDate,'%h') as ExpireHour,
                    date_format(PostDate,'%p') as PostMeridian, date_format(ExpireDate,'%p') as ExpireMeridian,
                    date_format(PostDate,'%i') as PostMinute, date_format(ExpireDate,'%i') as ExpireMinute,
                    date_format(POE_from,'%m/%d/%Y') POE_from, date_format(POE_to,'%m/%d/%Y') POE_to";
$results_row 	= 	G::Obj('Requisitions')->getRequisitionsDetailInfo($columns, $OrgID, $RequestID);

if(isset($_REQUEST['ValidateRequestID'])
    && $_REQUEST['ValidateRequestID'] == $_GET['RequestID']
	&& count($_POST) == 0) {

    if($results_row['City'] == '') {
        //$error_inputs[] = 'City';
    }
    if($results_row['State'] == '') {
        $error_inputs[] = 'State';
    }
    if($results_row['ZipCode'] == '') {
        //$error_inputs[] = 'ZipCode';
    }
    
    $TemplateObj->errors = $errors = "Please fill city, state, zipcode";
    $TemplateObj->error_inputs = $error_inputs;
    
    require IRECRUIT_DIR . 'requisitions/SetEditRequisitionDetails.inc';
}


if(count($error_inputs) == 0) {
    require IRECRUIT_DIR . 'requisitions/SetEditRequisitionDetails.inc';
}

$MonsterObj         =   new Monster ();
$MonsterInformation =   $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgIDRequest );
$MonsterOrgInfo     =   $MonsterInformation ['row'];

require_once IRECRUIT_VIEWS . 'requisitions/EditRequisition.inc';

if(isset($lst)) {
	?>
	<script type="text/javascript">
	var lst					=       '<?php echo $lst?>';
	var validate_dates_info =       '<?php echo $validate_dates_info;?>';
	validate_dates_info		=       JSON.parse(validate_dates_info);

	var date_split_ids		=       lst.split(",");
	var date_objs			=       new Array();

	var i                   =       0;
	var date_id     		=       "";
	var year_range  		=       "";
	for(date_id_key in date_split_ids) {
		date_id         =       date_split_ids[i];
		date_id         =       $.trim(date_id);
		year_range      =       getYearRange(validate_dates_info, date_id);
		date_picker(date_id, 'mm/dd/yy', year_range, '');
		i++;
	}
	</script>
	<?php
 }
?>
