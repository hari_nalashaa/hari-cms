<?php 
require_once '../Configuration.inc';

//Set Information
$set_info       =   array("RequisitionStatus = :RequisitionStatus", "RequisitionStatusDate = :RequisitionStatusDate");
$where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":RequisitionStatus"=>$_REQUEST['RequisitionStatus'], ":RequisitionStatusDate"=>$DateHelperObj->getYmdFromMdy($_REQUEST['RequisitionStatusDate']));

$RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
?>