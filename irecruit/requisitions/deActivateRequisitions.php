<?php 
require_once '../Configuration.inc';

$RequisitionIDs = array();
if(is_array($_REQUEST ['chk_req'])) {
	foreach ( $_REQUEST ['chk_req'] as $rkid => $rpvalue ) {
		$RequisitionIDs[] = $rkid;
		// Set the update information
		$set_info = array (
				"Active = :Active",
				"ExpireDate = NOW()"
		);
		// Set the condition
		$where = array (
				"OrgID = :OrgID",
				"RequestID = :RequestID"
		);
		// Set the parameters
		$params = array (
				":Active" => 'N',
				":OrgID" => $OrgID,
				":RequestID" => $rkid
		);
		// Update Requisitions Information
		$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array (
				$params
		) );
	}
}

if(count($RequisitionIDs) > 0) {
	echo "<div class='col-lg-12 alert alert-success' style='text-align:left;'>";
	echo "The following Requisitions have been De-Activated<br>";
	foreach ($RequisitionIDs as $RequestID) {
	    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
		$RequisitionTitle = $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
			
		echo $RequisitionTitle."<br>";
	}
	echo '</div>';
}

echo '<br><br><br><br>';
?>