<?php 
require_once '../Configuration.inc';

require_once IRECRUIT_DIR . 'common/listings/SocialMedia.inc';

$RequestID	=	$_REQUEST['RequestID'];
$MultiOrgID =	$_REQUEST['MultiOrgID'];

// Set columns
$columns	=	"Title, Description";
$REQS		=	$RequisitionsObj->getReqDetailInfo($columns, $OrgID, $MultiOrgID, $RequestID);

// Get Social Media Information
$SM           =   G::Obj('SocialMedia')->getSocialMediaDetailInfo($OrgID, $MultiOrgID);

$allow = array ("I", "B");

if (in_array ( $SM ['Placement'], $allow )) {
		
	if ($SM ['Facebook'] == "Y") {
		echo facebook ( $smurl, $REQS ['Title'], "", strip_tags( $REQS ['Description'] ));
	}
		
	if ($SM ['Twitter'] == "Y") {
		echo twitter ( $smurl, $REQS ['Title'] );
	}
		
	if ($SM ['LinkedIn'] == "Y") {
		echo linkedin ( $smurl, $REQS ['Title'], strip_tags ( $REQS ['Description'] ) );
	}
		
} // end social media in array

$smurl = PUBLIC_HOME . 'jobRequest.php?';
$smurl .= 'OrgID=' . $OrgID;
if ($MultiOrgID) {
	$smurl .= '&MultiOrgID=' . $MultiOrgID;
} 
$smurl .= '&RequestID=' . $RequestID;
?>
