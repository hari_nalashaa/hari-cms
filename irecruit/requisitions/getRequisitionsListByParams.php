<?php 
require_once '../Configuration.inc';

$keyword 			=   $_POST['requisitions_keyword'];
$sort_field         =   $_POST['sort_field'];
$sort_order         =   $_POST['sort_order'];
$active_status      =   $_POST['ddlRequisitionsActiveStatus'];

$requisitions_list 	=   array();

$where              =   array("OrgID = :OrgID");
$params             =   array(":OrgID"=>$OrgID);

if(isset($keyword) && $keyword != "")
{
	$where[] 			= "(Title LIKE :Title OR RequisitionID LIKE :RequisitionID OR JobID LIKE :JobID)";
	$params[":Title"]	= "%".$keyword."%";
	$params[":RequisitionID"]	= "%".$keyword."%";
	$params[":JobID"]	= "%".$keyword."%";
}
if($active_status == "Y"
	|| $active_status == "N"
	|| $active_status == "R") {
	$where[] 			= "Active = :Active";
	$params[":Active"]	= $active_status;
}
else if($active_status == "NA") {
	$where[] = "Approved = :Approved";
	$params[":Approved"] = "N";
}

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$RequestID 			= G::Obj('RequisitionDetails')->getHiringManagerRequestID();
	$where[]       =   "RequestID IN ('" . implode("','",$RequestID) . "')";
}

if($sort_field != "" && $sort_order != "") {
	if($sort_field == "posted_date") {
		$sort_field = "Requisitions.PostDate";
	}
	else if($sort_field == "requisition_id") {
		$sort_field = "RequisitionID";
	}
	else if($sort_field == "job_id") 	{
		$sort_field = "JobID";
	}
	else if($sort_field == "job_title") {
		$sort_field = "Title";
	}
	else {
		$sort_field = "Requisitions.PostDate";
	}

	if($sort_order != "desc" && $sort_order != "asc") $sort_order = "desc";
}
if(!isset($sort_field) 
	|| !isset($sort_order) 
	|| $sort_order == "" 
	|| $sort_field == "") {
	$sort_field = "Requisitions.PostDate";
	$sort_order = "desc";
}


$columns    =   "Title, RequestID, RequisitionID, JobID, DATE_FORMAT(PostDate,'%m/%d/%Y') AS PostDate, Active,
                DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate,
                DATEDIFF(NOW(), PostDate) Open, DATEDIFF(ExpireDate,PostDate) Closed,
                date_format(DATE_ADD(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current, DATEDIFF(ExpireDate, NOW()) ExpireDays,
                (SELECT count(*) FROM JobApplications WHERE OrgID = Requisitions.OrgID AND RequestID = Requisitions.RequestID) AS ApplicationsCount";
$req_info   =   $RequisitionsObj->getRequisitionInformation($columns, $where, "", " $sort_field $sort_order LIMIT 10", array($params));

$req_list   =   $req_info["results"];
echo json_encode($req_list);
?>
