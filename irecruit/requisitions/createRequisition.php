<?php
require_once '../Configuration.inc';

$TemplateObj->bg_color 	= $bg_color 	= "yellow";
$TemplateObj->formtable = $formtable	= "RequisitionQuestions";
$TemplateObj->title 	= $title 		= "Create New Requisition";

require_once IRECRUIT_DIR . 'request/EmailLinks.inc';
require_once IRECRUIT_DIR . 'request/UpdateHistory.inc';

// Load Graphs Related Scripts, Only for this page
$scripts_header [] = "js/create-requisition.js";
$scripts_header [] = "js/loadAJAX.js";
$scripts_header [] = "js/irec_Display.js";
$scripts_header [] = "tiny_mce/tinymce.min.js";
$scripts_header [] = "js/irec_Textareas.js";

// Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

$TemplateObj->requisition_process_identifier = $requisition_process_identifier = "Requisition";

//Get RequisitionFormID
$TemplateObj->RequisitionFormID = $RequisitionFormID  =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);

$where_info 	= 	array("OrgID = :OrgID", "QuestionID IN ('FormID', 'InternalFormID')", "RequisitionFormID = :RequisitionFormID");
$params_info 	= 	array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
$columns		=	"OrgID, QuestionID, Question, QuestionTypeID, QuestionOrder, Request, Required, defaultValue";
$def_que_info   =   $RequisitionQuestionsObj->getRequisitionQuestions($columns, $where_info, "QuestionOrder ASC", array($params_info));
$def_que_res    =   $def_que_info['results'];

$def_vals[$def_que_res[0]['QuestionID']]    =   $def_que_res[0]['defaultValue'];
$def_vals[$def_que_res[1]['QuestionID']]    =   $def_que_res[1]['defaultValue'];
$TemplateObj->def_vals = $def_vals;

$where_info     =   array("OrgID = :OrgID", "Requisition = :Requisition", "RequisitionFormID = :RequisitionFormID");
$params_info    =   array(":OrgID"=>$OrgID, ":Requisition"=>'Y', ":RequisitionFormID"=>$RequisitionFormID);
$columns        =   "OrgID, QuestionID, Question, QuestionTypeID, QuestionOrder, Request, Required, defaultValue";

$requisition_questions_info = $RequisitionQuestionsObj->getRequisitionQuestions($columns, $where_info, "QuestionOrder ASC", array($params_info));
$TemplateObj->requisition_questions = $requisition_questions = $requisition_questions_info['results'];

$WorkWeeksList = array ("SUN" => "Sunday", "MON" => "Monday", "TUE" => "Tuesday", "WED" => "Wednesday", "THU" => "Thursday", "FRI" => "Friday", "SAT" => "Saturday");

$TemplateObj->yes_no 		= $yes_no = array(""=>"Please Select", "Y"=>"Yes", "N"=>"No");
$TemplateObj->WorkWeeksList = $WorkWeeksList;

// Get Affirmative Action Settings Information
$TemplateObj->req_post_hours		=	$req_post_hours 		= array("12", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11");
$TemplateObj->req_post_mins 		=	$req_post_mins 			= array("00", "15", "30", "45");

if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'new') {

	if ($OrgID == "I20140912") { // Community Research Foundation	
	$po_exp_date_columns   =   "DATE_FORMAT(NOW(),'%m/%d/%Y') as RPD, DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 30 DAY),'%m/%d/%Y') as RED";
	} else {
	$po_exp_date_columns   =   "DATE_FORMAT(NOW(),'%m/%d/%Y') as RPD, DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 6 MONTH),'%m/%d/%Y') as RED";
	}
	$post_exp_date_row     =   $MysqlHelperObj->getDatesList ( $po_exp_date_columns );
	
	$TemplateObj->RequisitionID 			= $RequisitionID 			= 	"";
	$TemplateObj->JobID 					= $JobID 					= 	"";
	$TemplateObj->Title 					= $Title 					= 	"";
	$TemplateObj->PostDate 					= $PostDate 				= 	$post_exp_date_row ['RPD'];
	$TemplateObj->PostHour 					= $PostHour 				= 	"12";
	$TemplateObj->PostMinute 				= $PostMinute 				= 	"00";
	$TemplateObj->PostMeridian 				= $PostMeridian 			= 	"AM";
	$TemplateObj->ExpireDate 				= $ExpireDate 				= 	$post_exp_date_row ['RED'];
	$TemplateObj->ExpireHour 				= $ExpireHour 				= 	"11";
	$TemplateObj->ExpireMinute 				= $ExpireMinute 			= 	"00";
	$TemplateObj->ExpireMeridian 			= $ExpireMeridian 			= 	"PM";
	$TemplateObj->Active 					= $Active 					= 	"N";
	$TemplateObj->ListingPriority 			= $ListingPriority 			= 	3;
	$TemplateObj->EEOCode 					= $EEOCode 					= 	"";
	$TemplateObj->JobGroupCode 				= $JobGroupCode 			= 	"";
	$TemplateObj->Description 				= $Description 				= 	"";
	$TemplateObj->DisplayAA 				= $DisplayAA 				= 	"Y";
	$TemplateObj->InternalDisplayAA 		= $InternalDisplayAA 		= 	"Y";
	$TemplateObj->Address1Request 			= $Address1Request 			= 	$Address1;
	$TemplateObj->Address2Request 			= $Address2Request 			= 	$Address2;
	$TemplateObj->CityRequest 				= $CityRequest              = 	$City;
	$TemplateObj->StateRequest 				= $StateRequest 			= 	$State;
	$TemplateObj->ZipCodeRequest 			= $ZipCodeRequest 			= 	$ZipCode;
	$TemplateObj->CountryRequest 		    = $CountryRequest 		    = 	$Country;
}

//Create new requisition
if(isset($_REQUEST['process_requisition']) && $_REQUEST['process_requisition'] == "Y") {
	
	$ADVERTISEOPTIONS = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
	$AO = array ();
	$SAO = serialize ( $AO );
	if(is_array($ADVERTISEOPTIONS)) {
		foreach ( $ADVERTISEOPTIONS as $option ) {
			$AO [$option] = $_POST [$option];
		}
		$SAO = serialize ( $AO );
	}
	
	$TemplateObj->AO = $AO;
	$TemplateObj->AdvertisingOptions = $AdvertisingOptions = unserialize ( $SAO );
	
	$ad_valid_status = "true";
	foreach ($AdvertisingOptions as $AOKeyName=>$AOValueName) {
		if($AdvertisingOptions[$AOKeyName] != "") $ad_valid_status = "false";
	}
	
    //Set columns
    $columns    =   "MAX(OrgLevelID) as MaxOrgLevelID";
    //Set where condition, it is common for below organization levels information function calls
    $where      =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
    //Set parameters, it is common for below organization levels information function calls
    $params     =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID']);
    
    //Get organization levels information
    $results    =   $OrganizationsObj->getOrganizationLevelsInfo($columns, $where, '', array($params));
    $Levels     =   $results['results'][0];

    $i          =   1;
    $org_error  =   "true";
	while ( $i <= $Levels ['MaxOrgLevelID'] ) {
	    if(count($_REQUEST[$i]) > 0 && $_REQUEST[$i][0] != "") {
	        $org_error = "false";
	    }
	    $i++;
	}
	
    $WW         =   array_keys ( $WorkWeeksList );
    $WorkWeek   =   array ();
    
	foreach ( $WW as $d ) {
		if (isset($_POST [$d]) && $_POST [$d] == "Y") {
			$WorkWeek [$d] = $_POST [$d];
		}
	}

    $TemplateObj->WorkWeek      =   $WorkWeek;
    $TemplateObj->error_inputs  =   $error_inputs   =   array();
	
	for($i = 0; $i < count ( $requisition_questions ); $i ++) {
		
		if($requisition_questions[$i]['Required'] == 'Y') {
			if(($requisition_questions[$i]['QuestionTypeID'] == "18"
				|| $requisition_questions[$i]['QuestionTypeID'] == "1818")) {
					
				$QuestionIDcnt  = $_REQUEST[$requisition_questions[$i]['QuestionID']."cnt"];
		
				$error_answer = true;
				for($chkri = 1; $chkri <= $QuestionIDcnt; $chkri++) {
					if(isset($_POST [$requisition_questions[$i]['QuestionID']."-".$chkri])
					&& $_POST [$requisition_questions[$i]['QuestionID']."-".$chkri] != '') {
						$error_answer = false;
					}
				}
				if($error_answer === true) {
					$error_inputs[] = $requisition_questions[$i]['QuestionID'];
				}
					
			}
			else {
				if($_POST [$requisition_questions[$i]['QuestionID']] == ''
						&& $requisition_questions[$i]['QuestionID'] != "MultiOrgID") {
					$error_inputs[] = $requisition_questions[$i]['QuestionID'];
				}
			}
		}
		
	}
	
	if($org_error == "true") {
	    $error_inputs[] = "OrgLevel";
	}
	
	foreach ($error_inputs as $error_key=>$error_value) {
		if ($error_value == "AdvertisingOptions" && $ad_valid_status == "false") {
			unset($error_inputs[$error_key]);
		}
		if ($error_value == "WorkDays" && count($WorkWeek) > 0) {
			unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobIndustry") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobCategory") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobOccupation") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobPostType") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "ZipRecruiterJobCategory") {
		    unset($error_inputs[$error_key]);
		}
		//This will be applicable only if InternalRequisitions feature is disabled
		if($feature ['InternalRequisitions'] != "Y") {

		    $unset_values1    =   array(
                                        'InternalFormID', 
                                        'InternalDisplayAA'
                                    );
			if (in_array($error_value, $unset_values1)) {
				unset($error_inputs[$error_key]);
			}
				
		}
		
		if($feature['RequisitionRequest'] != "Y") {
		    $unset_values2    =   array(
                                        'RequesterName',
                                        'RequesterPhone',
                                        'RequesterEmail',
                                        'RequesterTitle',
                                    );
		    if (in_array($error_value, $unset_values2)) {
		        unset($error_inputs[$error_key]);
		    }
		}

	}
	
	########################### Start Validate Date Picker ###########################
	//Get Requisition Questions
	$results				=   G::Obj('RequisitionQuestions')->getRequisitionQuestionsByQuestionTypeID("Question, QuestionID, Validate", $OrgID, $RequisitionFormID, '17');
	
	$validate_dates_list	=	array();
	$questions_info			=	array();
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $FQCK) {
			$validate_dates_list[$FQCK['QuestionID']] = json_decode($FQCK['Validate'], true);
			$questions_info[$FQCK['QuestionID']]['Question'] = $FQCK['Question'];
		}
	}

	foreach ($validate_dates_list as $question_id=>$validate_que_info) {
		if($validate_que_info['years_min'] != "") {
	
			if($_POST [$question_id] != "") {
					
				//date information
				$date_ans			=	G::Obj('DateHelper')->getYmdFromMdy($_POST [$question_id]);
				$d1 				=	new DateTime(date('Y-m-d'));
				$d2 				=	new DateTime($date_ans);
	
				$ans_date_diff		=	$d2->diff($d1);
	
				if($ans_date_diff->y < $validate_que_info['years_min']) {
					$error_inputs[] =	$question_id;
				}
	
			}
		}
	}
	
	########################### End Validate Date Picker ###########################
	
	
	if(isset($_REQUEST['RequisitionManagers']) && count($_REQUEST['RequisitionManagers']) > 0) {
		unset($error_inputs['RequisitionManagers']);
	}
	
	$APPDATA	=	array();
	// Set post values to variables
	foreach ( $_POST as $pkey => $pvalue ) {
		
		if($pkey == "MultiOrgID") $pkey	= "MultiOrgIDRequest";
		if($pkey == "Address1") $pkey = "Address1Request";
		if($pkey == "Address2") $pkey = "Address2Request";
		if($pkey == "City") $pkey = "CityRequest";
		if($pkey == "State") $pkey = "StateRequest";
		if($pkey == "ZipCode") $pkey = "ZipCodeRequest";
		if($pkey == "Country") $pkey = "CountryRequest";
		$TemplateObj->{$pkey} = ${$pkey} = $pvalue;
		
		if ($pkey == 'shifts_schedule_time' || $pkey == 'LabelSelect') {
			
			foreach ( $_POST [$pkey] as $cquestion => $canswer ) {
		
				if ($pkey == 'LabelSelect') {
					$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
				}
				if ($pkey == 'shifts_schedule_time') {
					$qa ['from_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
					$qa ['to_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
					$qa ['days'] 		= $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
					$answer 			= serialize ( $qa );
				}
					
				$APPDATA[$cquestion] = 	$answer;
			}
		}
		else {
			$APPDATA[$pkey]	= $pvalue;
		}
	}

	$TemplateObj->APPDATA = $APPDATA;
	
	if(count($error_inputs) > 0) {
		
		$TemplateObj->errors = $errors = "Please fill all the mandatory fields";
		$TemplateObj->error_inputs = $error_inputs;
	}
	else {
		//Insert requisition information
		require_once IRECRUIT_DIR . 'requisitions/ProcessNewRequisitionInformation.inc';
		
		header("Location:" . IRECRUIT_HOME . "requisitionsSearch.php?menu=3&action=new&msg=sucins");
		exit();
	}
}

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/CreateRequisition');
?>
