<?php
require_once '../Configuration.inc';

$monster_req_info = $MonsterObj->getMonsterRequisitionInformation("*", $OrgID, "", $_REQUEST['RequestID']);

$MonsterJobIndustries   = $MonsterObj->getMonsterJobIndustries();
$JobCategories          = $MonsterObj->monsterJobCategories("yes");
$JobOccupations 		= $MonsterObj->getMonsterJobCategoriesOccupations();

$columns                = "Title, MonsterJobCategory, MonsterJobOccupation, Duration, MonsterJobPostedDate, MonsterJobPostType, MonsterJobIndustry";
$req_info               = $RequisitionsObj->getReqDetailInfo($columns, $OrgID, "", $RequestID);

?>
<table class="table table-bordered">
    <tr>
        <td>Requisition Title</td>
        <td><?php echo $req_info['Title'];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Category</td>
        <td><?php echo $JobCategories[$req_info['MonsterJobCategory']];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Occupation</td>
        <td><?php echo $JobOccupations[$req_info['MonsterJobOccupation']];?></td>
    </tr>
    
    <tr>
        <td>Duration</td>
        <td><?php echo $req_info['Duration'];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Post Date</td>
        <td><?php echo $req_info['MonsterJobPostedDate'];?></td>
    </tr>
    
    <tr>
        <td>Monster Post Type</td>
        <td><?php echo $req_info['MonsterJobPostType'];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Industry</td>
        <td><?php echo $MonsterJobIndustries[$req_info['MonsterJobIndustry']];?></td>
    </tr>
    
    <tr>
        <td>Inventory Type</td>
        <td><?php echo $req_info['InventoryType'];?></td>
    </tr>
    
    <tr>
        <td>Has Bold</td>
        <td><?php echo $monster_req_info['HasBold'];?></td>
    </tr>
    
    <tr>
        <td>Has Single Refresh</td>
        <td><?php echo $monster_req_info['HasSingleRefresh'];?></td>
    </tr>
    
    <tr>
        <td>Auto Refresh Status</td>
        <td><?php echo $monster_req_info['AutoRefreshStatus'];?></td>
    </tr>
    
    <tr>
        <td>Auto Refresh Duration</td>
        <td><?php echo $monster_req_info['AutoRefreshDuration'];?></td>
    </tr>
    
    <tr>
        <td>CAN Status</td>
        <td><?php echo $monster_req_info['CANStatus'];?></td>
    </tr>
    
    <tr>
        <td>CAN Duration</td>
        <td><?php echo $monster_req_info['CANDuration'];?></td>
    </tr>
    
    <tr>
        <td>Posted Status</td>
        <td><?php echo $monster_req_info['ReturnTypeCode'];?></td>
    </tr>
</table>