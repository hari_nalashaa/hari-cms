<?php
require_once '../Configuration.inc';

$req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $_REQUEST['RequestID']);
$free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);

if(isset($_REQUEST['job_boards_list']) && $_REQUEST['job_boards_list'] != "") {
    $free_jobboard_list[] = $_REQUEST['job_boards_list'];
    $free_jobboard_list = array_unique($free_jobboard_list);
    
    //Unset Indeed Sponsored Option
    if(isset($_REQUEST['job_board_type']) && $_REQUEST['job_board_type'] != "") {
        //Get MultiOrgID based on RequestID
        $IMultiOrgID    =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
        
        //Get MultiOrgID based on RequestID
        $set_info       =   array("Sponsored = 'no'", "LastModifiedDateTime = NOW()");
        $where_info     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID");
        $params_info    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$IMultiOrgID, ":RequestID"=>$_REQUEST['RequestID']);
        
        $IndeedObj->updIndeedFeedInfo($set_info, $where_info, array($params_info));
    }
}
else if(isset($_REQUEST['job_boards_list']) && $_REQUEST['job_boards_list'] == "") {
    $count = count($free_jobboard_list);
    
    for($j = 0; $j < $count; $j++) {
        if(isset($_REQUEST['job_board_type']) && $_REQUEST['job_board_type'] == 'Indeed') {
            if($free_jobboard_list[$j] == "Indeed") unset($free_jobboard_list[$j]);
        }
        else if(isset($_REQUEST['job_board_type']) && $_REQUEST['job_board_type'] == 'ZipRecruiter') {
            if($free_jobboard_list[$j] == "ZipRecruiter") unset($free_jobboard_list[$j]);
        }
        else if(isset($_REQUEST['job_board_type']) && $_REQUEST['job_board_type'] == 'Monster') {
            if($free_jobboard_list[$j] == "Monster") unset($free_jobboard_list[$j]);
        }
    } 
    
    $free_jobboard_list = array_unique($free_jobboard_list);
}

$free_jobboard_list = array_values($free_jobboard_list);

$where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":FreeJobBoardLists"=>json_encode($free_jobboard_list));
$set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists");

if(isset($_REQUEST['job_board_type']) && $_REQUEST['job_board_type'] == 'ZipRecruiter') {
    $params_info[":ZipRecruiterLabelID"]    =   $_REQUEST['ZipRecruiterFreeLabel'];
    $set_info[] = "ZipRecruiterLabelID = :ZipRecruiterLabelID";
}
else if(isset($_REQUEST['job_board_type']) && $_REQUEST['job_board_type'] == 'Indeed') {
    $params_info[":IndeedLabel"] = $_REQUEST['IndeedLabel'];
    $set_info[] = "IndeedLabelID = :IndeedLabel";
    $params_info[":IndeedWFH"] = $_REQUEST['IndeedWFH'];
    $set_info[] = "IndeedWFH = :IndeedWFH";
}
$result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));

//Update Zip Recruiter Job Category
if(isset($_REQUEST['ZipRecruiterJobCategory']) && $_REQUEST['ZipRecruiterJobCategory'] != "") {
    $set_info   = array("ZipRecruiterJobCategory = :ZipRecruiterJobCategory");
    $where_info = array("OrgID = :OrgID", "RequestID = :RequestID");
    $params     = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":ZipRecruiterJobCategory"=>$_REQUEST['ZipRecruiterJobCategory']);
    $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params));
}

echo json_encode($result);
?>
