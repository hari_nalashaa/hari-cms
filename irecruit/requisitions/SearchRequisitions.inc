<?php 
/**
 * Query Search Requisitions
 */

$jobtitleslist = "";
$selrequestids = "";

if(is_array($_REQUEST['RequestIDs'])) $jobtitleslist = "'".implode("','",$_REQUEST['RequestIDs'])."'";
if(is_array($repostall)) $selrequestids = "'".implode("','",$repostall)."'";

$jobtitlecondition = "";

//Set columns
$columns = "RequestID, City, State, ZipCode, Active,
			date_format(PostDate,'%m/%d/%Y') PostDate,
			date_format(ExpireDate,'%m/%d/%Y') ExpireDate,
			Title, datediff(now(),PostDate) Open, datediff(ExpireDate, PostDate) Closed,
			date_format(date_add(now(), INTERVAL 0 DAY),'%m/%d/%Y') Current, MultiOrgID";
//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);

/**
 * Filter Requisitions Based Submitted Data
 */
if($Active != '' && $Active != 'A') {
	$where[] = "Active = :Active";
	$params[":Active"] = $Active;
}

if($_REQUEST['fromdate'] != "") {
	$where[] = "DATE(PostDate) >= :GPostDate";
	$params[":GPostDate"] = $DateHelperObj->getYmdFromMdy($_REQUEST['fromdate']);
}

if($_REQUEST['todate'] != "") {
	$where[] = "DATE(PostDate) <= :LPostDate";
	$params[":LPostDate"] = $DateHelperObj->getYmdFromMdy($_REQUEST['todate']);
}

if($_REQUEST['searchkeywords'] != "") {
	$words = explode(",",$_REQUEST['searchkeywords']);
	
	if($words[0] != "") {
		$query_words = "(";
		
		$words_flag = false;
		for($w = 0; $w < count($words); $w++) {
			$words_flag = true;
			
			$word = trim($words[$w]);
			
			$wordsquery[] = "(JobID LIKE '%$word%' 
							  OR Title LIKE '%$word%' 
							  OR City LIKE '%$word%' 
							  OR State LIKE '%$word%' 
							  OR ZipCode LIKE '%$word%')";
		
		}
		
		$query_words .= implode(" or ", $wordsquery);
		
		$query_words .= ")";
		
		if($words_flag) {
			$where[] = $query_words;
		}
	}
	
}


if(count($_REQUEST['RequestIDs']) > 0 && ($_REQUEST['refreshoptions'] == "" || $_REQUEST['refreshoptions'] == "inactive"))
{
	$where[] = "RequestID IN($jobtitleslist)";
}

if(count($repostall) > 0) {
	$where[] = "RequestID IN($selrequestids)";
}
	
if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$params[":RMOrgID"] = $OrgID;
	$params[":RMUserID"] = $USERID;
	$where[] = "RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :RMOrgID AND UserID = :RMUserID)";
} // end hiring manager


$sord = 'asc';
if($_REQUEST['sord'] == 'desc') $sord = 'desc';
if($_REQUEST['sord'] == 'asc') $sord = 'asc';
$skey = $_REQUEST['skey'];


//Sorting Results Based On Selection
if($_REQUEST['skey'] == 'date_opened')
	$order_by = "PostDate $sord";
if($_REQUEST['skey'] == 'reqid')
	$order_by = "RequisitionID $sord";
if($_REQUEST['skey'] == 'jobid')
	$order_by = "JobID $sord";
if($_REQUEST['skey'] == 'title')
	$order_by = "Title $sord";
if($_REQUEST['skey'] == 'city')
	$order_by = "City $sord";
if($_REQUEST['skey'] == 'state')
	$order_by = "State $sord";
if($_REQUEST['skey'] == 'zip')
	$order_by = "ZipCode $sord";
if($_REQUEST['skey'] == 'status') 
	$order_by = "Active $sord";

//Execute Final Query
$req_search_results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", $order_by, array($params));

//Get Total Rows Count
$cnt = $req_search_results['count'];