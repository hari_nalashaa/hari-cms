<?php
require_once '../Configuration.inc';

$where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
$params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
$req_his_res    =   G::Obj('RequisitionHistory')->getRequisitionHistoryInfo("*", $where_info, 'CreatedDateTime DESC', array($params_info));
$req_history    =   $req_his_res['results'];//echo "<pre>"; print_r(json_decode($req_history[5]['UpdatedFields']) );die;
?>
<br>
<table class="table table-striped table-bordered table-hover">
<tr>
    <td colspan="4"><strong>Requisition History</strong></td>
</tr>
<tr>
    <td colspan="4"><br></td>
</tr>
<tr>
	<td><strong>Updated By</strong></td>
    <td><strong>Requisition Title</strong></td>
    <td><strong>Comments</strong></td>
    <td><strong>Date</strong></td>
</tr>
<?php
if(count($req_history) > 0) { 
    for($r = 0; $r < count($req_history); $r++) {
        ?>
        <tr>
        	<td>
        	<?php 
        	$updated_by_user_avatar_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($req_history[$r]['UserID'], 'DashboardAvatar');
        	$user_avatar = IRECRUIT_HOME . "vault/".$OrgID."/".$req_history[$r]['UserID']."/profile_avatars/".$updated_by_user_avatar_info['DashboardAvatar'];
        	if($ServerInformationObj->validateUrlContent($user_avatar)) {
        	    ?><img src="<?php echo $user_avatar;?>" width="50" height="50">&nbsp;
        	    <?php $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $req_history[$r]['UserID']);
        	    echo $userName;?><?php
    		}
    		else {
    		    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50">&nbsp;
    		    <?php $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $req_history[$r]['UserID']);
        	    echo $userName;?><?php
    		}
    		?>
        	</td>
            <td><?php echo $req_history[$r]['RequestTitle'];?></td>
            <td>
                <?php echo $req_history[$r]['Comments'];?><br>
		<?php

		$upd_fields     =   json_decode($req_history[$r]['UpdatedFields'], true);
		$updated_fields     =   isset($upd_fields['Updated Fields']);
		$additional_fields  =   isset($upd_fields['Additional Fields']);

		if (($updated_fields > 0) || ($additional_fields > 0)) {

		?>
                    <img src="images/icons/page_white_magnify.png" title="View" border="0">&nbsp;
                    <a href="#" onclick="javascript:window.open('<?php echo IRECRUIT_HOME;?>requisitions/showRequisitionHistoryInfo.php?OrgID=<?php echo $OrgID?>&RequestID=<?php echo $RequestID;?>&RequisitionHistoryID=<?php echo $req_history[$r]['RequisitionHistoryID'];?>','_blank','location=yes,toolbar=no,height=400,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes');">
                        Updated fields information
                    </a>
                <?php } ?>
            </td>
            <td><?php echo date('m/d/Y H:i T',strtotime($req_history[$r]['CreatedDateTime']));?></td>
        </tr>
        <?php	
    }
}
else {
    ?>
    <tr>
        <td colspan="3">No records found.</td>
    </tr>
    <?php	
}
?>
</table>
