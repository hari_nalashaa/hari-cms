<?php
// Set columns
$columns = "NOW() AS CurrentDateTime, DATE_FORMAT(PostDate,'%m/%d/%Y') PostDate,DATE_FORMAT(DateEntered,'%m/%d/%Y') DateEntered, DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate,
			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,";

$columns .= "IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate)) Open, DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate) Closed,";

$columns .= "date_format(date_add(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current,
			 RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID,
			 Description, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry";

$columns_count = "COUNT(RequestID) AS RequisitionsCount";

// Set parameters
$params = array (":OrgID" => $OrgID);
if($Active != "A") $params[":Active"] = $Active;

// Set condition
$where = array ("OrgID = :OrgID");
if($Active != "A") $where[] = "Active = :Active";

if($Active == "Y") $where[] = "Approved = 'Y'";


if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$params [":RMOrgID"] = $OrgID;
	$params [":RMUserID"] = $USERID;
	$where [] = " RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :RMOrgID AND UserID = :RMUserID)";
} // end hiring manager


// Get Requisition Information
$req_search_results_list = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", '', array ($params) );
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
	$where[] = "RequestID = :RequestID";
	$params [":RequestID"] = $_REQUEST['RequestID'];
}

$req_search_results = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", '', array ($params) ); 
// Get Requisitions Count
$total_results_count = $RequisitionsObj->getRequisitionInformation ( $columns_count, $where, "", "", array (
		$params 
) );
$total_requisitions_count = '1';
$cnt = '1';


$req_search_list = array ();

/**
 * @full details information
 */
if (is_array ( $req_search_results ['results'] )) {
	
	$r = 0;
	foreach ( $req_search_results ['results'] as $REQS ) {
		
		$req_search_list [] = array ("RequestID" => $REQS ['RequestID'], "MultiOrgID" => $REQS ['MultiOrgID']);
		
		$req_search_results ['results'] [$r] ['OrganizationName'] = "";
		if ($feature ['MultiOrg'] == "Y") {
			$req_search_results ['results'] [$r] ['OrganizationName'] = $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $REQS ['RequestID'] );
		}
		
		$req_search_results ['results'] [$r] ['PostedInternal'] = "";
		
		if ($Active == "N") {
			$req_search_results ['results'] [$r] ['Open'] = $REQS ['Closed'];
		}
		
		// Set where condition
		$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
		// Set parameters
		$params = array (":OrgID" => $OrgID, ":RequestID" => $REQS ['RequestID']);
		// Get Job Applications Information
		$resultsCNT = $ApplicationsObj->getJobApplicationsInfo ( "COUNT(*) AS cnt", $where, '', '', array ($params) );
		$req_search_results ['results'] [$r] ['ApplicantsCount'] = $resultsCNT ['results'] [0] ['cnt'];
		
		if ($permit ['Reports'] == 1) {
			
			$reportlink = "generateReport=View Applicants";
			$reportlink .= "&selectedReport=applicantsbyrequisition";
			$reportlink .= "&ReportDate_From=" . $REQS ['PostDate'];
			$reportlink .= "&ReportDate_To=" . $REQS ['Current'];
			$reportlink .= "&RequestIDs=" . $REQS ['RequestID'];
			$reportlink .= "&processreport=Y";
			
			$req_search_results ['results'] [$r] ['ReportLink'] = $reportlink;
		}
		
		if ($permit ['Requisitions_Edit'] == 1) { 
			
			$linkc = IRECRUIT_HOME . 'request/assign.php?RequestID=' . $REQS ['RequestID'] . '&action=copy&Active=' . $Active . '&MultiOrgID=' . $REQS ['MultiOrgID'];
			$req_search_results ['results'] [$r] ['Linkc'] = $linkc;
		} else { // else action != Requisition_Edit
			$link = IRECRUIT_HOME . "requisitions/viewListing.php?RequestID=" . $REQS ['RequestID'];
			if ($AccessCode != "") {
				$link .= "&k=" . $AccessCode;
			}
			$req_search_results ['results'] [$r] ['ViewListing'] = $link;
		} // end else Requisition_Edit
		
		$r ++;
	} // end foreach
}
    
//If TemplateObj exists
if (is_object ( $TemplateObj )) {
	$TemplateObj->total_requisitions_count = $total_requisitions_count;
	$TemplateObj->req_search_results       = $req_search_results;
	$TemplateObj->req_search_results_list  = $req_search_results_list;
	$TemplateObj->cnt                      = $cnt;
	$TemplateObj->req_search_list          = $req_search_list;
	$TemplateObj->Start                    = 0;
}
?>