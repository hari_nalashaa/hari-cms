<?php 
$title = 'Reports';

include IRECRUIT_DIR . 'applicants/DisplayTitles.inc';


$subtitle = array("applicantsbyrequisition" => "Applicants by Requisition",
				"applicantstatusbyrequisition" => "Applicant Status by Req",
				"applicantsbyowner" => "Applicants by Owner",
				"applicantsbymanager" => "Applicants by Manager",
				"applicantsbydistance" => "Applicants by Distance",
				"applicantsbyreferral" => "Applicants by Referral",
				"requisitionstatus" => "Requisition Status",
				"requisitionsbyowner" => "Requisitions by Owner",
				"requisitionsbymanager" => "Requisitions by Manager",
				"requisitioncosts" => "Requisition Costs",
				"eeobyjobclass" => "EEO by Job Classification",
				"eeoappsummary" => "EEO Applicant Summary",
				"eeoposdesired" => "EEO by Position Desired",
				"eeostatus" => "EEO by Status",
				"excelexport" => "Export to Excel");

if ($subtitle[$selectedReport]) {
	$title .= ' - ' . $subtitle[$selectedReport];
}

if($selectedReport!="applicantsbyowner") { $OwnerManager[] = ""; }
if($selectedReport!="applicantsbymanager") { $OwnerManager[] = ""; }
if($selectedReport!="applicantsbydistance") { $Radius = ""; $ZipCode = ""; }
if($selectedReport!="applicantsbyreferral") { $Refferal = ""; }
if($selectedReport!="requisitionstatus") { $Active = ""; }
if($selectedReport=="requisitionstatus") { $ReportDate_From = ""; $ReportDate_To = ""; }
if($selectedReport!="requisitionsbyowner") { $OwnerManager[] = ""; }
if($selectedReport=="requisitionsbyowner") { $ReportDate_From = ""; $ReportDate_To = ""; }
if($selectedReport!="requisitionsbymanager") { $OwnerManager[] = ""; }
if($selectedReport=="requisitionsbymanager") { $ReportDate_From = ""; $ReportDate_To = ""; }
?>