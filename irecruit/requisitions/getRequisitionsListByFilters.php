<?php 
require_once '../Configuration.inc';

$LimitRequisitions 	=   $preferences['ApplicantsSearchResultsLimit'];
$Active 		 	=   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$GuIDRequisition    =   isset($_REQUEST['GuIDRequisition']) ? $_REQUEST['GuIDRequisition'] : '';
$FilterOrganization =   $_REQUEST['FilterOrganization'];
$PresentOn          =   $_REQUEST['PresentOn'];

$IsRequisitionApproved = "Y";
if($Active == "NA") {
	$Active = "N";
	$IsRequisitionApproved = "N";
}
 
require_once IRECRUIT_DIR . 'requisitions/GetRequisitions.inc';
?>