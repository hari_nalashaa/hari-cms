<?php
require_once '../Configuration.inc';

// Set Columns
$columns    =   "FileName,FileID,Deleted, date_format(Date,'%m/%d/%Y %I:%i %p') Date, OrgID,RequestID,UserID,Notes";
$where 	    =   array("OrgID = :OrgID", "RequestID = :RequestID", "Deleted = 0");
$params      =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
// Get Applicant Vault Information
$results    =   G::Obj('RequisitionAttachments')->getRequisitionAttachmentsInfo( $columns, $where, 'Date DESC', array ($params) );
// Get Count Applicant Vault Information Records Count
$cnt        =   $results ['count'];

    if(FROM_SRC == 'IRECRUIT')
        
        echo '<form method="POST" name="frmAttachmentVault" id="frmAttachmentVaultrepo" enctype="multipart/form-data">';

        echo '<input type="hidden" name="RequestID" value="' . htmlspecialchars($RequestID) . '">';
        echo '<input type="hidden" name="OrgID" value="' . htmlspecialchars($OrgID) . '">';
        echo '<input type="hidden" name="process" value="' . htmlspecialchars($process) . '">';
        echo '<input type="hidden" name="display_app_header" value="' . htmlspecialchars($_REQUEST['display_app_header']) . '">';
        echo '<input type="hidden" name="UpdateID" value="' . htmlspecialchars($UpdateID) . '">';
        
        if ($action != "") {
            echo '<input type="hidden" name="action" value="' . htmlspecialchars($action) . '">';
        }
        if ($PreFilledFormID != "") {
            echo '<input type="hidden" name="PreFilledFormID" value="' . htmlspecialchars($PreFilledFormID) . '">';
        }
        
        echo '<input type="hidden" name="MAX_FILE_SIZE" value="10485760">';
        
        echo '<div class="form-group row" style="margin-top:20px;">
               <div class="col-lg-3 col-md-3 col-sm-3"><label for="newfile" class="newfile">Upload Attachment:</label></div>
               <div class="col-lg-9 col-md-9 col-sm-9">
               <input type="file" name="newfile" id="newfile" size="30" maxlength="255" value="WaterPurifier" class="form-control width-auto-inline">
               </div></div>';
        
        echo '<div class="form-group row">
               <div class="col-lg-3 col-md-3 col-sm-3"><label for="notes" class="newfile">Notes:</label></div>
               <div class="col-lg-9 col-md-9 col-sm-9">
               <textarea rows="4" cols="80" name="notes" id="notes"></textarea>
               </div></div>';
        
        echo '<div class="col-lg-8 col-md-8 col-sm-7" style="padding-left:0px;">';
        echo '<input type="submit" value="Submit" class="btn btn-primary button" id="btnSubmitRequisitionRepo" style="width: 100px;">';
        echo '</div>';

        echo '</form>';
        
        echo '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><br></div></div>';
          
        if ($cnt > 0) {
            echo '<div class="form-group row" style="margin-bottom:30px;">';
            echo '<div class="col-lg-12 col-md-12 col-sm-12">';
            
            echo '<div class="table-responsive">';
            echo '<table border="0" cellspacing="3" cellpadding="5" align="center" class="table table-striped table-bordered table-hover">';
            
            echo '<tr height="30">';
            
            echo '<td><strong>Date</strong></td>';
            echo '<td><strong>File Name</strong></td>';
            echo '<td><strong>Notes</strong></td>';
            echo '<td><strong>Updated By</strong></td>';
            echo '<td align="center" width="60"><strong>Actions</strong></td>';
            echo '<tr>';
           
            $rowcolor = "#eeeeee";
                        if (is_array ( $results ['results'] )) {
                foreach ( $results ['results'] as $AV ) { 
                    echo '<tr bgcolor="' . $rowcolor . '">';
                    
                    echo '<td>';
                    echo $AV ['Date'];
                    echo '</td>';

                    echo '<td>';
                    echo $AV ['FileName'];
                    echo '</td>';

                    echo '<td>';
                    echo $AV ['Notes'];
                    echo '</td>';
                    
                    echo '<td>';

                $updated_by_user_avatar_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($AV ['UserID'], 'DashboardAvatar');
                $user_avatar = IRECRUIT_HOME . "vault/".$OrgID."/".$AV ['UserID']."/profile_avatars/".$updated_by_user_avatar_info['DashboardAvatar'];
                if($ServerInformationObj->validateUrlContent($user_avatar)) {
                    ?><img src="<?php echo $user_avatar;?>" width="50" height="50">&nbsp;
                    <?php $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $AV ['UserID']);
                    echo $userName;?><?php
                }
                else {
                    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50">&nbsp;
                    <?php $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $AV ['UserID']);
                    echo $userName;?><?php
                }
                    
                    echo '</td>';
                    
                    echo '<td align="center">';
                    echo '<a href="' . IRECRUIT_HOME . 'requisitions/viewRequisitionAttachments.php?OrgID=' . $OrgID . '&RequestID=' . $RequestID . '&FileID=' . $AV ['FileID'];
                    if ($action != "") {
                        echo '&action=' . htmlspecialchars($action);
                    }
                    if ($PreFilledFormID != "") {
                        echo '&PreFilledFormID=' . htmlspecialchars($PreFilledFormID);
                    }
                    echo '">';
                    echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
                    echo '</a>';
                    
                    echo '<a class="delete_requisition_vault" style="cursor:pointer;">';
                    
                    $UpdatedFile = $AV ['FileID'];
                    $UpdatedFileName = $AV ['FileName'];
                    echo '<img style="width:16px;padding-top:5px;margin-left:5px;" src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px; height:21px;">';
                    echo '</a><input type="hidden" name="FileID" class="UpdatedFile" value="'.$UpdatedFile.'"><input type="hidden" name="OrgID" id="OrgID" value="'.$OrgID.'"><input type="hidden" name="RequestID" id="RequestID" value="'.$RequestID.'"><input type="hidden" name="FileName" id="FileName" value="'.$UpdatedFileName.'">';

                    echo '</td>';
                    
                    echo '</tr>' . "\n";
                    
                    if ($rowcolor == "#eeeeee") {
                        $rowcolor = "#ffffff";
                    } else {
                        $rowcolor = "#eeeeee";
                    }
                } // end foreach
            }
            
            echo '</td></tr>';
            echo '</table>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
        } // end $cnt requisitionattachments items

?>
<script type="text/javascript">
$(document).ready(function(){
$(".delete_requisition_vault").click(function(){
		var FileID = $(this).next(".UpdatedFile").val();
		var OrgID      		= $("#OrgID").val();
		var RequestID      	= $("#RequestID").val();
		var FileName 		= $("#FileName").val();
		var result = confirm("Are you sure do you want to delete "+FileName+"?");
        if (result) {
            $.ajax({
    		type: "POST",
    		url: "requisitions/deleteRequisitionAttachments.php",
    		data:'OrgID='+ OrgID+'&RequestID='+RequestID+'&FileID='+FileID ,
    		success: function(data){
		getRequisitionAttachments();
    		}
		});
        }else{
        	return false;
        }
	});



$("#frmAttachmentVaultrepo").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
    	url: "requisitions/CreateRequisitionAttachments.php",
        type: 'POST',
        data: formData,
        success: function (data) {
	getRequisitionAttachments();
        },
        cache: false,
        contentType: false,
        processData: false
    });
});

});

</script>
