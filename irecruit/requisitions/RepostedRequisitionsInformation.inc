<form method="post" name="RepostedRequisitions"
	id="RepostedRequisitions"
	onsubmit="return validateCreateNewRequisitions()">
	<table width="100%" cellpadding="5" cellspacing="1"
		class="table table-striped table-bordered table-hover">
		<tr>
			<td><strong>Date Opened</strong></td>
			<td><strong>Expiration Date</strong></td>
			<td><strong>RequisitionID/JobID</strong></td>
			<td><strong>Days Open</strong></td>
		</tr>
		<tbody>
	    <?php
	    if(is_array($requisition_results ['results'])) {
	    	foreach ( $requisition_results ['results'] as $row )
			{
				?>
				<tr class="odd">
					<td><?php echo $row['PostDate']?></td>
                    <td><?php echo $row['ExpireDate']?></td>
					<td>
						<?php echo $row['Title']?><br>
	                    <?php echo $row['JobID']."/".$row['RequisitionID']?><br>
					</td>
					<td><?php echo $row['Open']?></td>
				</tr>
	            <?php
			}
	    }
		?>
        </tbody>
	</table>
	<table width="100%" cellpadding="5" cellspacing="5"
		class="table table-striped table-bordered table-hover">
		<tr>
			<th colspan="2">Update Requisition</th>
		</tr>
		<tr>
			<td>Job ID</td>
			<td><input type="text" name="JobID[<?php echo $value?>]"
				class="jobid">
			</td>
		</tr>
		<tr>
			<td>Post Date</td>
			<td><input type="text" name="PostDate[<?php echo $value?>]"
				id="PostDate<?php echo $value?>" class="postexpiredate"> 
				<select name="PostDateSIHrs[<?php echo $value?>]" id="PostDateSIHrs<?php echo $value?>">
				<?php
				for($h = 1; $h <= 12; $h ++) {
					?><option value="<?php echo $h;?>"><?php echo $h;?></option><?php
				}
				?>
				</select>
				<select name="PostDateSIMins[<?php echo $value?>]" id="PostDateSIMins<?php echo $value?>">
				<?php
				for($m = 0; $m <= 45; $m += 15) {
					?><option value="<?php echo $m;?>"><?php echo ($m == 0) ? "00" : $m;?></option><?php
				}
				?>
				</select> &nbsp; 
				<select name="PostDateSIAmPm[<?php echo $value?>]" id="PostDateSIAmPm[<?php echo $value?>]">
					<option value="AM">AM</option>
					<option value="PM">PM</option>
				</select>&nbsp;EST
			</td>
		</tr>		
		<tr>
			<td>Expire Date</td>
			<td><input type="text" name="ExpireDate[<?php echo $value?>]" id="ExpireDate<?php echo $value?>" class="postexpiredate"> 
			    <select name="ExpireDateSIHrs[<?php echo $value?>]" id="ExpireDateSIHrs<?php echo $value?>">
				<?php
				for($h = 1; $h <= 12; $h ++) {
					?><option value="<?php echo $h;?>"><?php echo $h;?></option><?php
				}
				?>
				</select>
				<select name="ExpireDateSIMins[<?php echo $value?>]" id="ExpireDateSIMins<?php echo $value?>">
					<?php
					for($m = 0; $m <= 45; $m += 15) {
						?><option value="<?php echo $m;?>"><?php echo ($m == 0) ? "00" : $m;?></option><?php
					}
					?>
				</select> &nbsp; 
				<select name="ExpireDateSIAmPm[<?php echo $value?>]" id="ExpireDateSIAmPm[<?php echo $value?>]">
					<option value="AM">AM</option>
					<option value="PM">PM</option>
				</select>&nbsp;EST
			</td>
		</tr>
		<tr>
			<td colspan="2">
    			<input type="hidden" name="OriginalRequestID" id="OriginalRequestID" value="<?php echo $_REQUEST['OriginalRequestID'];?>">
    			<?php
    			if($ServerInformationObj->getRequestSource() == 'ajax') {
    				?>
    				<input type="hidden" name="updatereq" id="updatereq" value="Submit" class="btn btn-primary">
    				<input type="button" name="updatereq" id="updatereq" value="Submit" class="btn btn-primary" onclick="repostSingleRequisition(this)">
    				<?php
    			}
    			else {
    				?><input type="submit" name="updatereq" id="updatereq" value="Submit" class="btn btn-primary"><?php
    			}
    			?>
			</td>
		</tr>
	</table>
</form>

<script>
function repostSingleRequisition(btn_obj) {
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var request_url = "requisitions/repostRequisition.php";

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#repost-requisition").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#repost-requisition").html(data);
			getRequisitionsListByFilters('yes');
    	}
	});
	setAjaxErrorInfo(request, "#repost-requisition");
}
</script>