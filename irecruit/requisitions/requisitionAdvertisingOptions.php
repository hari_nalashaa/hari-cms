<?php
require_once '../Configuration.inc';

$TemplateObj->title	= $title = 'Requisition Form';

// Load Graphs Related Scripts, Only for this page
$scripts_header [] = "js/loadAJAX.js";
$scripts_header [] = "js/irec_Display.js";
$scripts_header [] = "tiny_mce/tinymce.min.js";
$scripts_header [] = "js/irec_Textareas.js";

// Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

//Declare mostly using super globals over here
$TemplateObj->k         =   $k            =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->RequestID =   $RequestID    =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->action    =   $action       =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->Active    =   $Active       =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';

//Get RequisitionFormID
$RequisitionFormID      =   $_REQUEST['form'];

if (isset($_REQUEST['process']) && $_REQUEST['process'] == "Y") {

    $where_info                         =   array("OrgID = :OrgID", "RequisitionFormID = :RequisitionFormID");
    $params_info                        =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
    $requisition_questions_info         =   $RequisitionQuestionsObj->getRequisitionQuestions("*", $where_info, "QuestionOrder ASC", array($params_info));
    $requisition_questions              =   $requisition_questions_info['results'];
    
    $req_que_rar                        =   array();
    for($rqi = 0; $rqi < count($requisition_questions); $rqi++) {
        $rar_question_id                                =   $requisition_questions[$rqi]['QuestionID'];
        $rqid[$rar_question_id]                         =   $requisition_questions[$rqi];
        $req_que_rar[$rar_question_id]['Requisition']   =   $requisition_questions[$rqi]['Requisition'];
        $req_que_rar[$rar_question_id]['Request']       =   $requisition_questions[$rqi]['Request'];
        $req_que_rar[$rar_question_id]['Required']      =   $requisition_questions[$rqi]['Required'];
    }
    
    
    $cnt = count ( $_POST ) / 3;

    $values_list   =   array();
    for($ii = 1, $k = 0; $ii < $cnt; $ii ++, $k++) {

        $Site = 'Site' . $ii;
        $Price = 'Price' . $ii;
        $Period = 'Period' . $ii;

        if ($_POST [$Site] != "" && $_POST [$Price] !=  "" && $_POST [$Period] != "") {
            $values_list[$k]['Site']      =   $_POST [$Site];
            $values_list[$k]['Price']     =   $_POST [$Price];
            $values_list[$k]['Period']    =   $_POST [$Period];
        }
    } // end $ii
    
    
    $set_info      =   array("value = :value", "LastModifiedDateTime = NOW()");
    $where_info    =   array("OrgID = :OrgID", "RequisitionFormID = :RequisitionFormID", "QuestionID = :QuestionID");
    $params_info   =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$_REQUEST['form'], ":QuestionID"=>'AdvertisingOptions', ":value"=>json_encode($values_list));

    $upd_adv_res   =   $RequisitionsObj->updRequisitionsInfo("RequisitionQuestions", $set_info, $where_info, array($params_info));

    if($upd_adv_res['affected_rows'] > 0) {
        $que_info = array (
            "OrgID"                 =>	$rqid[$_REQUEST['questionid']]['OrgID'],
            "RequisitionFormID"     =>	$rqid[$_REQUEST['questionid']]['RequisitionFormID'],
            "QuestionID"            =>	$rqid[$_REQUEST['questionid']]['QuestionID'],
            "Question"              =>	$rqid[$_REQUEST['questionid']]['Question'],
            "QuestionTypeID"        =>	$rqid[$_REQUEST['questionid']]['QuestionTypeID'],
            "size"                  =>	$rqid[$_REQUEST['questionid']]['size'],
            "maxlength"             =>	$rqid[$_REQUEST['questionid']]['maxlength'],
            "rows"                  =>	$rqid[$_REQUEST['questionid']]['rows'],
            "cols"                  =>	$rqid[$_REQUEST['questionid']]['cols'],
            "wrap"                  =>	$rqid[$_REQUEST['questionid']]['wrap'],
            "value"                 =>	$rqid[$_REQUEST['questionid']]['value'],
            "defaultValue"          =>	$rqid[$_REQUEST['questionid']]['defaultValue'],
            "Requisition"           =>	$rqid[$_REQUEST['questionid']]['Requisition'],
            "Request"               =>	$rqid[$_REQUEST['questionid']]['Request'],
            "Edit"                  =>	$rqid[$_REQUEST['questionid']]['Edit'],
            "QuestionOrder"         =>	$rqid[$_REQUEST['questionid']]['QuestionOrder'],
            "Required"              =>	$rqid[$_REQUEST['questionid']]['Required'],
            "Validate"              =>	$rqid[$_REQUEST['questionid']]['Validate'],
            "Operation"             =>	"Updated: " . date("F j, Y, g:i a") . " by: " . $USERID,
            "LastModifiedDateTime" 	=>	$rqid[$_REQUEST['questionid']]['LastModifiedDateTime']
        );
        // Insert RequisitionQuestions History
        $RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
    }
    
    
    
    header("Location:".IRECRUIT_HOME."requisitions/requisitionAdvertisingOptions.php?action=requisitionquestions&form=".$_REQUEST['form']."&questionid=AdvertisingOptions&typeform=requisitionform&msg=updsuc");
    exit;
} // end process

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/RequisitionAdvertisingOptions');
?>
