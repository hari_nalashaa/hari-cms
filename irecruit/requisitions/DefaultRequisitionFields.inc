<?php
//Requisitions table columns
$def_requisition_fields = array (
		"OrgID",
		"MultiOrgID",
		"RequestID",
		"ApprovalLevelRequired",
		"ApprovalLevel",
		"Approved",
		"Title",
		"Description",
		"PostDate",
		"ExpireDate",
		"EmpStatusID",
		"Exempt_Status",
		"Salary_Range_From",
		"Salary_Range_To",
		"HowMany",
		"WhenNeeded",
		"RequisitionID",
		"JobID",
		"SalaryGradeID",
		"FormID",
		"EEOCode",
		"JobGroupCode",
		"ListingPriority",
		"Active",
		"Owner",
		"RequisitionManagers",
		"RequesterName",
		"RequesterEmail",
		"RequesterPhone",
		"RequesterTitle",
		"RequesterDepartment",
		"RequesterReason",
		"ImportanceID",
		"Address1",
		"Address2",
		"City",
		"State",
		"ZipCode",
		"Country",
		"AdvertisingOptions",
		"IsAuditor",
		"IsApprover",
		"WorkDays",
		"Hours",
		"Weeks",
		"Program",
		"Replacement",
		"ReportsTo",
		"POE_from",
		"POE_to",
		"Comment",
		"MultiCostCenter"
);

//These fields will be available only if InternalRequisitions feature is disabled
$def_requisition_fields[] = "InternalFormID";
//$def_requisition_fields[] = "InternalDisplayAA";

//These fields will be available only if Monster feature is disabled
$def_requisition_fields[] = "MonsterJobCategory";
$def_requisition_fields[] = "MonsterJobOccupation";
$def_requisition_fields[] = "MonsterJobPostType";
$def_requisition_fields[] = "MonsterJobIndustry";
?>