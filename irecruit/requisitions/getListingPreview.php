<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= $title = 'View Listing';

//Declare mostly using super globals over here
$TemplateObj->k = $k = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

include COMMON_DIR . 'listings/PrintRequisition.inc';
include COMMON_DIR . 'listings/SocialMedia.inc';

if ($permit ['Requisitions'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

$view_listing = PrintReq ( $OrgID, $RequestID, '', '' );

$requisition_detail_info = $RequisitionsObj->getReqDetailInfo("*", $OrgID, $MultiOrgID, $_REQUEST['RequestID']);
echo json_encode(array("view_listing"=>$view_listing, "requisition_info"=>$requisition_detail_info));
?>