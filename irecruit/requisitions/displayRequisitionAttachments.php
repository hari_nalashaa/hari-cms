<?php
require_once '../irecruitdb.inc';

if ($_REQUEST ['RequestID'] != "") {
	
	// Set where condition
	$where     =   array(
                        "OrgID          =   :OrgID",
                        "RequestID  =   :RequestID",
                        "File           =   :File" 
                    );
	// Set parameters
	$params    =   array (
                        ":OrgID"            =>  $_REQUEST ['OrgID'],
                        ":RequestID"    =>  $_REQUEST ['RequestID'],
                        ":File"             =>  $_REQUEST ['File'] 
                    );
	// Get Applicant Vault Information
	$results    =   G::Obj('RequisitionAttachments')->getRequisitionAttachmentsInfo ( "*", $where, '', array ($params) );
	
	$RV         =   $results ['results'] [0];
	
	$filename   =   $RV ['File'];

} else {
    
    $filename   =   $File;
    
} // end else ApplicationID

if ($filename) {
	
	$dfile = '';
	
	$dfile = IRECRUIT_DIR . 'vault/requisitionattachments/' . $_REQUEST ['OrgID'] . '/'. $_REQUEST ['RequestID'].'/' . $filename;
	
	if (file_exists ( $dfile )) {
		header ( 'Content-Description: File Transfer' );
		header ( 'Content-Type: application/octet-stream' );
		header ( 'Content-Disposition: attachment; filename=' . basename ( $dfile ) );
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		header ( 'Content-Length: ' . filesize ( $dfile ) );
		readfile ( $dfile );
		exit ();
	}
} // end if FileName
?>
