<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= $title = 'View Listing';

//Declare mostly using super globals over here
$TemplateObj->k = $k = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

if ($MultiOrgID) {
	$page_styles["header"][] = "css/public.php?OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID;
} else {
	$page_styles["header"][] = "css/public.php?OrgID=".$OrgID;
}

$TemplateObj->page_styles = $page_styles;


echo $TemplateObj->displayIrecruitTemplate('views/requisitions/ViewListing');
?>
