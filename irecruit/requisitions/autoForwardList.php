<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title     =   $title      =   'Auto Forward';

//Declare mostly using super globals over here
$TemplateObj->k         =   $k          =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->RequestID =   $RequestID  =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->action    =   $action     =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->Active    =   $Active     =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';


if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/requisitions/AutoForwardList.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/requisitions/AutoForwardList');
}
?>