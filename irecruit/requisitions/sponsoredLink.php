<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title = $title = 'Sponsored Links';

//Set Request Variables
$TemplateObj->action = $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->Sponsored = $Sponsored = isset($_REQUEST['Sponsored']) ? $_REQUEST['Sponsored'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/SponsoredLink');
?>