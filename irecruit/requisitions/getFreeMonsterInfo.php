<?php
require_once '../Configuration.inc';

$monster_req_info = $MonsterObj->getMonsterRequisitionInformation("*", $OrgID, "", $_REQUEST['RequestID']);

$MonsterJobIndustries   = $MonsterObj->getMonsterJobIndustries();
$JobCategories          = $MonsterObj->monsterJobCategories("yes");
$JobOccupations 		= $MonsterObj->getMonsterJobCategoriesOccupations();

$columns                = "Title, MonsterJobCategory, MonsterJobOccupation, Duration, MonsterJobPostedDate, MonsterJobPostType, MonsterJobIndustry";
$req_info               = $RequisitionsObj->getReqDetailInfo($columns, $OrgID, "", $RequestID);

?>
<table class="table table-bordered">

    <tr>
        <td>Requisition Title</td>
        <td><?php echo $req_info['Title'];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Category</td>
        <td><?php echo $JobCategories[$req_info['MonsterJobCategory']];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Occupation</td>
        <td><?php echo $JobOccupations[$req_info['MonsterJobOccupation']];?></td>
    </tr>
    
    <tr>
        <td>Duration</td>
        <td><?php echo $req_info['Duration'];?></td>
    </tr>
    
    <tr>
        <td>Monster Post Type</td>
        <td><?php echo $req_info['MonsterJobPostType'];?></td>
    </tr>
    
    <tr>
        <td>Monster Job Industry</td>
        <td><?php echo $MonsterJobIndustries[$req_info['MonsterJobIndustry']];?></td>
    </tr>

</table>