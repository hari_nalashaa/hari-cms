<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	=   "Requisition Form";

//Get RequisitionFormID
$RequisitionFormID  =   $_REQUEST['form'];

if(isset($_POST['btnSubmitDefaultForm']) 
	&& $_POST['btnSubmitDefaultForm'] == "Submit" 
    && $RequisitionFormID != "") {

    $where_info                         =   array("OrgID = :OrgID", "RequisitionFormID = :RequisitionFormID");
    $params_info                        =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
    $requisition_questions_info         =   $RequisitionQuestionsObj->getRequisitionQuestions("*", $where_info, "QuestionOrder ASC", array($params_info));
    $requisition_questions              =   $requisition_questions_info['results'];
    
    $req_que_rar                        =   array();
    for($rqi = 0; $rqi < count($requisition_questions); $rqi++) {
        $rar_question_id                                =   $requisition_questions[$rqi]['QuestionID'];
        $rqid[$rar_question_id]                         =   $requisition_questions[$rqi];
        $req_que_rar[$rar_question_id]['Requisition']   =   $requisition_questions[$rqi]['Requisition'];
        $req_que_rar[$rar_question_id]['Request']       =   $requisition_questions[$rqi]['Request'];
        $req_que_rar[$rar_question_id]['Required']      =   $requisition_questions[$rqi]['Required'];
    }
    
	$set_info          = 	array("defaultValue = :defaultValue", "LastModifiedDateTime = NOW()");
	$where_info        = 	array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
	$params            = 	array(":defaultValue"=>$_REQUEST['FormChoice'], ":QuestionID"=>$_REQUEST['questionid'], ":RequisitionFormID"=>$RequisitionFormID, ":OrgID"=>$OrgID);
	$upd_def_que       =	$RequisitionQuestionsObj->updRequisitionQuestions($set_info, $where_info, array($params));
	
	if($upd_def_que['affected_rows'] > 0) {
            $que_info = array (
                "OrgID"                 =>	$rqid[$_REQUEST['questionid']]['OrgID'],
                "RequisitionFormID"     =>	$rqid[$_REQUEST['questionid']]['RequisitionFormID'],
                "QuestionID"            =>	$rqid[$_REQUEST['questionid']]['QuestionID'],
                "Question"              =>	$rqid[$_REQUEST['questionid']]['Question'],
                "QuestionTypeID"        =>	$rqid[$_REQUEST['questionid']]['QuestionTypeID'],
                "size"                  =>	$rqid[$_REQUEST['questionid']]['size'],
                "maxlength"             =>	$rqid[$_REQUEST['questionid']]['maxlength'],
                "rows"                  =>	$rqid[$_REQUEST['questionid']]['rows'],
                "cols"                  =>	$rqid[$_REQUEST['questionid']]['cols'],
                "wrap"                  =>	$rqid[$_REQUEST['questionid']]['wrap'],
                "value"                 =>	$rqid[$_REQUEST['questionid']]['value'],
                "defaultValue"          =>	$rqid[$_REQUEST['questionid']]['defaultValue'],
                "Requisition"           =>	$rqid[$_REQUEST['questionid']]['Requisition'],
                "Request"               =>	$rqid[$_REQUEST['questionid']]['Request'],
                "Edit"                  =>	$rqid[$_REQUEST['questionid']]['Edit'],
                "QuestionOrder"         =>	$rqid[$_REQUEST['questionid']]['QuestionOrder'],
                "Required"              =>	$rqid[$_REQUEST['questionid']]['Required'],
                "Validate"              =>	$rqid[$_REQUEST['questionid']]['Validate'],
                "Operation"             =>	"Updated: " . date("F j, Y, g:i a") . " by: " . $USERID,
                "LastModifiedDateTime" 	=>	$rqid[$_REQUEST['questionid']]['LastModifiedDateTime']
            );
            // Insert RequisitionQuestions History
            $RequisitionQuestionsHistoryObj->insRequisitionQuestionsHistory($que_info);
	    
	}
	
	header("Location:setDefaultFormChoice.php?questionid=".$_REQUEST['questionid']."&form=".$RequisitionFormID."&msg=suc");
	exit;
}

// Set condition
$where              =   array ("OrgID = :OrgID");
// Set parameters
$params             =   array (":OrgID" => $OrgID);
// Get FormQuestions Information
$columns            =   "DISTINCT(FormID) as FormID";
// Get FormQuestions Information
$results_forms      =   $FormQuestionsObj->getFormQuestionsInformation ( $columns, $where, "FormID", array ( $params ) );

//Get Default value
$req_que_info       =   $RequisitionQuestionsObj->getRequisitionQuestionInfo("defaultValue", $OrgID, $_REQUEST['questionid'], $RequisitionFormID);

$TemplateObj->req_que_info  =   $req_que_info;
$TemplateObj->results_forms =   $results_forms;

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/SetDefaultFormChoice');
?>
