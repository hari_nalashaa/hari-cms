<?php
$cnt 		=    $_POST ['cnt'];
$Question 	=    $_POST ['Question'];
$Required 	=    $_POST ['Required'];

$def        =   '';
$i          =   0;
$ii         =   0;

for($i; $i < $cnt; $cnt) {
	$i ++;
	$v     =   'value-' . $i;
	$d     =   'display-' . $i;
	$de    =   'default-' . $i;
	
	if ($_POST [$d] != '') {
		if ($default == $de) {
			$def = $_POST [$v];
		}
		$answer .= $_POST [$v] . ':' . $_POST [$d] . '::';
		$ii ++;
		$data [] = array (
				'value' => $_POST [$v],
				'name' => $_POST [$d] 
		);
	}
}

if ($_POST ['AttachmentName']) {
	if ($_FILES ['AttachmentFile'] ['type'] != "") {
		
		$dir = IRECRUIT_DIR . 'vault/' . $OrgID;
		
		if (! file_exists ( $dir )) {
			mkdir ( $dir, 0700 );
			chmod ( $dir, 0777 );
		}
		
		$apatdir = $dir . '/formattachments';
		
		if (! file_exists ( $apatdir )) {
			mkdir ( $apatdir, 0700 );
			chmod ( $apatdir, 0777 );
		}
		
		$data =   file_get_contents ( $_FILES ['AttachmentFile'] ['tmp_name'] );
		$e    =   explode ( '.', $_FILES ['AttachmentFile'] ['name'] );
		$ecnt =   count ( $e ) - 1;
		$ext  =   $e [$ecnt];
		$ext  =   preg_replace ( "/\s/i", '', $ext );
		$ext  =   substr ( $ext, 0, 5 );
		
		$answer = $_POST ['AttachmentName'] . "::" . $ext;
		
		$filename = $apatdir . '/' . $FormID . '-' . $QuestionID . '.' . $ext;
		$fh       = fopen ( $filename, 'w' );
		fwrite ( $fh, $data );
		fclose ( $fh );
		
		chmod ( $filename, 0644 );
	} // end if FILES
} // end if AttachmentName

if ($sort == 'Y') {
	
	if ($_REQUEST ['QuestionTypeID'] != "100") {
		$answer = "";
		
		foreach ( $data as $key => $row ) {
			$value [$key] = $row ['value'];
			$name [$key] = $row ['name'];
		}
		array_multisort ( $name, SORT_ASC, $data );
		
		foreach ( $data as $key => $row ) {
			$answer .= $row ['value'] . ':' . $row ['name'] . '::';
			$ii ++;
		}
	}
}

if ($ii > 0) {
	$answer = substr ( "$answer", 0, - 2 );
}

if ($mrpCount) {
	$answer = $mrpCount . "";
}

if ($Clear == 'Y') {
	$def = '';
}

$params = array ();
$set_info = array ();

if ($_REQUEST ['QuestionTypeID'] == "100") {
	$question_answers ['RVal'] = $_REQUEST ['RVal'];
	$question_answers ['LabelValRow'] = $_REQUEST ['LabelValRow'];
	
	if ($sort == 'Y') {
		// Sort based on alphabetical order
		asort ( $question_answers ['LabelValRow'] );
		
		if (is_array ( $question_answers ['LabelValRow'] )) {
			$q100_i = 1;
			foreach ( $question_answers ['LabelValRow'] as $lable_key_name => $label_val_name ) {
				$question_answers ['LabelValRow'] ["LabelValRow" . $q100_i] = $label_val_name;
				
				$q100_i ++;
			}
		}
	}
	
	if (is_array ( $question_answers )) {
		$answer = serialize ( $question_answers );
	}
	
	if ($_REQUEST ['rows'] != "" && $_REQUEST ['cols'] != "") {
		$params [':rows'] = $_REQUEST ['rows'];
		$params [':cols'] = $_REQUEST ['cols'];
		$set_info [] = "rows = :rows";
		$set_info [] = "cols = :cols";
	}
}

if ($_REQUEST ['QuestionTypeID'] == "120") {
	$question_answers ['day_names'] = $_REQUEST ['day_names'];
	if (is_array ( $question_answers )) {
		$answer = serialize ( $question_answers );
	}
	
	if ($_REQUEST ['number_of_days'] != "") {
		$params [':rows'] 	= $_REQUEST ['number_of_days'];
		$set_info [] 		= "rows = :rows";
	}
}

// Bind Parameters
$params [':OrgID'] 		= $OrgID;
$params [':QuestionID'] = $QuestionID;
$params [':RequisitionFormID'] = $RequisitionFormID;

// set where condition
$where_info = array (
		"OrgID 		= :OrgID",
		"QuestionID = :QuestionID",
        "RequisitionFormID = :RequisitionFormID" 
);

$params [':Question'] 		= $Question;
$params [':value'] 			= $answer;
$params [':defaultValue'] 	= $def;
$params [':Required'] 		= $Required;

// Set the parameters those are going to update
$set_info [] = "Question        =   :Question";
$set_info [] = "value           =   :value";
$set_info [] = "defaultValue    =   :defaultValue";
$set_info [] = "Required        =   :Required";
$set_info [] = "LastModifiedDateTime = NOW()";

// Update the table information based on bind and set values
$res_que_info = $RequisitionQuestionsObj->updRequisitionQuestions ( $set_info, $where_info, array ($params) );

$QuestionIDOrig = $QuestionID; // preserve original $QuestionID
$QuestionID 	= $QuestionIDOrig; // preserve original $QuestionID


?>
