<?php 
if ($permit['Reports'] < 1) { die (include IRECRUIT_DIR . 'irecruit_permissions.err'); }

if(isset($_REQUEST['generateReport'])) {
	// get report selected
	$selectedReport = $_REQUEST['selectedReport'];
	$ReportDate_From = $_REQUEST['ReportDate_From'];
	$StartDate = $ReportDate_From;
	$ReportDate_To = $_REQUEST['ReportDate_To'];
	$EndDate = $ReportDate_To;
	$RequestIDs = $_REQUEST['RequestIDs'];
	if(isset($_REQUEST['Report_Class']))
		$Report_Class = $_REQUEST['Report_Class'];
	else $Report_Class = "";
	if(isset($_REQUEST['ProcessOrder']))
		$ProcessOrder = $_REQUEST['ProcessOrder'];
	else $ProcessOrder = "";
	if(isset($_REQUEST['ZipCode']))
		$ZipCode = $_REQUEST['ZipCode'];
	else $ZipCode = "";
	if(isset($_REQUEST['Radius']))
		$Radius = $_REQUEST['Radius'];
	else $Radius = "";
	if(isset($_REQUEST['OrderBy']))
		$OrderBy = $_REQUEST['OrderBy'];
	else $OrderBy = "";
	if(isset($_REQUEST['OrderDir']))
		$OrderDir = $_REQUEST['OrderDir'];
	else $OrderDir = "";
	if(isset($_REQUEST['OwnerManager']))
		$OwnerManager = $_REQUEST['OwnerManager'];
	else $OwnerManager = "";
	if(isset($_REQUEST['Referral']))
		$Referral = $_REQUEST['Referral'];
	else $Referral = "";
} else {
	//$selectedReport = "";

	$ReportDate_From = "";
	$ReportDate_To = "";
	$RequestIDs = "";
	$Report_Class = "";
	$ProcessOrder = "";
	$ZipCode = "";
	$Radius = "";
	$OrderBy = "";
	$OrderDir = "";
	$OwnerManager = "";
	$Referral = "";
}
if(!isset($printable)) $printable='no';
?>