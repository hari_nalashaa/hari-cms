<?php 
require_once '../Configuration.inc';

$req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $_REQUEST['MonsterRequestID']);
$free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);

if(isset($_REQUEST['job_boards_list']) && $_REQUEST['job_boards_list'] != "") {
    $free_jobboard_list[]   = $_REQUEST['job_boards_list'];
    $free_jobboard_list     = array_unique($free_jobboard_list);    
}
else if(isset($_REQUEST['job_boards_list']) && $_REQUEST['job_boards_list'] == "") {
    for($j = 0; $j < count($free_jobboard_list); $j++) {
        if($free_jobboard_list[$j] == "Monster") unset($free_jobboard_list[$j]);
    }
    $free_jobboard_list = array_unique($free_jobboard_list);
}

$free_jobboard_list = array_values($free_jobboard_list);

$where_info =   array("OrgID = :OrgID", "RequestID = :RequestID");
$set_info   =   array("MonsterJobCategory = :MonsterJobCategory", "MonsterJobIndustry = :MonsterJobIndustry", "MonsterJobOccupation = :MonsterJobOccupation", "MonsterJobPostType = :MonsterJobPostType", "FreeJobBoardLists = :FreeJobBoardLists");
$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['MonsterRequestID'], ":MonsterJobCategory"=>$_REQUEST['MonsterJobCategory'], ":MonsterJobIndustry"=>$_REQUEST['MonsterJobIndustry'], ":MonsterJobOccupation"=>$_REQUEST['MonsterJobOccupation'], ":MonsterJobPostType"=>$_REQUEST['MonsterJobPostType'], ":FreeJobBoardLists"=>json_encode($free_jobboard_list));
$RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params));
?>