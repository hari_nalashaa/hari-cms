<?php
$APPDATA		= 	array();
foreach($results_row  as $edit_field_key=>$edit_field_value)
{
    if($edit_field_key == "MultiOrgID") {
        $edit_field_key	= "MultiOrgIDRequest";
    }
    if($edit_field_key == "Address1") {
        $edit_field_key = "Address1Request";
    }
    if($edit_field_key == "Address2") {
        $edit_field_key = "Address2Request";
    }
    if($edit_field_key == "City") {
        $edit_field_key = "CityRequest";
    }
    if($edit_field_key == "State") {
        $edit_field_key = "StateRequest";
    }
    if($edit_field_key == "ZipCode") {
        $edit_field_key = "ZipCodeRequest";
    }
    if($edit_field_key == "Country") {
        $edit_field_key = "CountryRequest";
    }
    if($edit_field_key == "AdvertisingOptions") {
        $edit_field_value = $AdvertisingOptions = unserialize ( $edit_field_value );
    }
    if($edit_field_key == "WorkDays") {
        $edit_field_value = $WorkWeek = unserialize ( $edit_field_value );
    }
    $TemplateObj->{$edit_field_key} = ${$edit_field_key} = $edit_field_value;

    $APPDATA[$edit_field_key]	=	$edit_field_value;
}

$requisitions_data 	= G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);

foreach($requisitions_data as $req_data_que=>$req_data_ans) {
    $APPDATA[$req_data_que]	=	$req_data_ans;
}
?>