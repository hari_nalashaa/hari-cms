<?php 
require_once '../Configuration.inc';

if ($_FILES ['newfile'] ['type'] != "" && $_POST['OrgID'] != "" && $_POST['RequestID'] != "") {
   
    $RequestID = $_POST['RequestID'];
    $OrgID =$_POST['OrgID'];
    $dir = IRECRUIT_DIR . 'vault/'.$_POST['OrgID'];
    if (! file_exists ( $dir )) {
        mkdir ( $dir, 0700 );
        chmod ( $dir, 0777 );
    }
    
   $dir1 = $dir.'/requisitionattachments/';
    if (! file_exists ( $dir1 )) {
        mkdir ( $dir1, 0700 );
        chmod ( $dir1, 0777 );
    }


    $dir2 = $dir1. $RequestID.'/';
    if (! file_exists ( $dir2 )) { 
        mkdir ( $dir2 , 0700 );
        chmod ( $dir2 , 0777 );
    } 
    if ($_FILES ['newfile'] ['type'] != "") {
        $e         =   explode ( '.', $_FILES ['newfile'] ['name'] );
        $ecnt      =   count ( $e ) - 1;
        $FileType  =   $e [$ecnt];
        $FileType  =   preg_replace ( "/\s/i", '', $FileType );  
    }
    $ext = $FileType;
    $timestamp  = time();
    $basename   = $timestamp.'.'.$ext;
    $uploadfile = $dir2.$basename;
    $target_file = str_replace(' ', '',$uploadfile );
    //file upload and store into DB
    if (move_uploaded_file($_FILES["newfile"]["tmp_name"], $target_file)){
        // Bind parameters for Attachments Data
        $repository_vault_info = array (
            "OrgID"         =>  $OrgID,
            "RequestID"     =>  $RequestID,
            "FileName"      =>  basename($_FILES["newfile"]["name"]),
            "FileID"        =>  str_replace(' ', '', $basename),
            "FileExt"       => $ext,
            "Date"          =>  "NOW()",
            "Notes"         =>  $_POST['notes'],
            "UserID"        =>  $USERID,
            "Deleted"       =>  0
        );
	$MultiOrgID =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
        $REQS = G::Obj('Requisitions')->getReqDetailInfo('Title', $OrgID, $MultiOrgID, $RequestID);
        $req_history = array(
         "OrgID"             =>  $OrgID,
         "RequestID"         =>  $RequestID,
	 "RequestTitle"      =>  $REQS['Title'],
         "Comments"          =>  "Added New Attachment - " . basename($_FILES["newfile"]["name"]),
	 "UpdatedFields"     =>  json_encode(array()),
         "CreatedDateTime"   =>  "NOW()",
         "UserID"            =>  $USERID
        ); 
        // Insert Attahments Information
        $insert_req_vault = G::Obj('RequisitionAttachments')->insRequisitionAttachments($repository_vault_info );
        $insert_req_history = G::Obj('RequisitionHistory')->insRequisitionHistory($req_history);
        return;     
       }
}
?>
