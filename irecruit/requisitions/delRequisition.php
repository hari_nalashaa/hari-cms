<?php
require_once '../Configuration.inc';

//Declare mostly using super globals over here
$TemplateObj->action    =   $action     = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->process   =   $process    = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->RequestID =   $RequestID  = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

$multiorgid_req         =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
$del_req_title          =   $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
$del_req_title         .=   " - " . $RequisitionDetailsObj->getJobTitle($OrgID, $multiorgid_req, $RequestID);

if ($action == "delete") {
	
	if ($process == "Y") {
		
		// ###################### Common where condition code start #############################
		$where = array ("RequestID = :RequestID", "OrgID = :OrgID");
		$params = array (":RequestID" => $RequestID, ":OrgID" => $OrgID);
		
		// Delete Requisitions Information
		$del_req_status = $RequisitionsObj->delRequisitionsInfo ( 'Requisitions', $where, array ($params) );
		
		// Delete Requisition Org Levels Information
		$RequisitionsObj->delRequisitionsInfo ( 'RequisitionOrgLevels', $where, array ($params) );
		// ###################### Common where condition code end ###############################
		
		$message = 'Deleted Req/Job ID: ' . $del_req_title;
		
		if($del_req_status['affected_rows'] > 0) {
			$delete_status = "success";
			$color = $user_preferences['TableRowHeader'];
		}
		else {
			$delete_status = "failed";
			$color = "red";
		}
		
		echo json_encode(array("message"=>$message, "status"=>$delete_status, "color"=>$color));
	}
}
?>