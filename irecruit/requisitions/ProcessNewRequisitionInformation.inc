<?php
//Get RequisitionFormID
$RequisitionFormID      =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);

if ($_POST ['PostMeridian'] == "AM") {
	if ($PostHour == 12) {
		$PostHour = "00";
	}
}
if ($_POST ['PostMeridian'] == "PM") {
	if ($PostHour < 12) {
		$PostHour += 12;
	}
}
if ($_POST ['ExpireMeridian'] == "AM") {
	if ($ExpireHour == 12) {
		$ExpireHour = "00";
	}
}
if ($_POST ['ExpireMeridian'] == "PM") {
	if ($ExpireHour < 12) {
		$ExpireHour += 12;
	}
}

$PostDate = substr ( $PostDate, - 4 ) . '-' . substr ( $PostDate, 0, 2 ) . '-' . substr ( $PostDate, 3, 2 ) . ' ' . $PostHour . ':' . $PostMinute . ':00';
$ExpireDate = substr ( $ExpireDate, - 4 ) . '-' . substr ( $ExpireDate, 0, 2 ) . '-' . substr ( $ExpireDate, 3, 2 ) . ' ' . $ExpireHour . ':' . $ExpireMinute . ':00';

require_once IRECRUIT_DIR . 'requisitions/RequisitionFields.inc';


######################
$RequestID              =   uniqid(time()).rand(1, 6000);

/**
 * This is a temporary code until the final upgrade.
 * Upto that time, we have to keep on adding different conditions to it.
 * After final upgrade we will have to remove the loop through $_POST
 */
G::Obj('GetFormPostAnswer')->POST =   $_POST;    //Set Post Data

$columns				=	"OrgID, QuestionID, QuestionOrder, Question, QuestionTypeID, value";
$additional_ques_list	=	G::Obj('RequisitionQuestions')->getAdditionalRequisitionFields($OrgID, $RequisitionFormID);
$form_que_list			=   G::Obj('RequisitionQuestions')->getRequisitionQuestionsByQueID($columns, $OrgID, $RequisitionFormID);

$ques_to_skip          	=   array();
foreach($form_que_list as $QuestionID=>$QuestionInfo) {

	if(in_array($QuestionID, $additional_ques_list)) {

		$QI				=   $QuestionInfo;

		G::Obj('GetFormPostAnswer')->QueInfo = $QI;
		$values			=   G::Obj('GetFormPostAnswer')->getDisplayValue($QI['value']);
		
		if($QI['QuestionTypeID'] == 13) {
			//After the complete upgrade have to remove this.
			$ques_to_skip[]        	=   $QI['QuestionID']."1";
			$ques_to_skip[]        	=   $QI['QuestionID']."2";
			$ques_to_skip[]        	=   $QI['QuestionID']."3";
				
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']		=   1;
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 14) {
			//After the complete upgrade have to remove this.
			$ques_to_skip[]        	=   $QI['QuestionID']."1";
			$ques_to_skip[]        	=   $QI['QuestionID']."2";
			$ques_to_skip[]        	=   $QI['QuestionID']."3";
			$ques_to_skip[]        	=   $QI['QuestionID']."ext";
		
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 15) {
			//After the complete upgrade have to remove this.
			$ques_to_skip[]        	=   $QI['QuestionID']."1";
			$ques_to_skip[]        	=   $QI['QuestionID']."2";
			$ques_to_skip[]        	=   $QI['QuestionID']."3";
			$ques_to_skip[]        	=   $QI['QuestionID'];
		
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 9) {
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
		
			//After the complete upgrade have to remove this.
			$qis  =   0;
			foreach ( $values as $v => $q ) {
				$qis++;
				$ques_to_skip[]    	=   $QI['QuestionID'] . "-" . $qis;
				$ques_to_skip[]    	=   $QI['QuestionID'] . "-" . $qis . "-yr";
				$ques_to_skip[]    	=   $QI['QuestionID'] . "-" . $qis . "-comments";
			}
		
			$ques_to_skip[]        	=   $QI['QuestionID'];
			$ques_to_skip[]        	=   $QI['QuestionID']."cnt";
		
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 18) {

			//After the complete upgrade have to remove this.
			$cnt                   	=   G::Obj('GetFormPostAnswer')->POST[$QI['QuestionID'].'cnt'];
		
			for($c18 = 1; $c18 <= $cnt; $c18++) {
				$ques_to_skip[]    	=   $QI['QuestionID'].'-'.$c18;
			}
			$ques_to_skip[]        	=   $QI['QuestionID'];
			$ques_to_skip[]        	=   $QI['QuestionID']."cnt";
		
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
				
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		else if($QI['QuestionTypeID'] == 1818) {
			//After the complete upgrade have to remove this.
			$cnt                   	=   G::Obj('GetFormPostAnswer')->POST[$QI['QuestionID'].'cnt'];
		
			for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
				$ques_to_skip[]    	=   $QI['QuestionID'].'-'.$c1818;
			}
			$ques_to_skip[]        	=   $QI['QuestionID'];
			$ques_to_skip[]        	=   $QI['QuestionID']."cnt";
		
			$QI['Answer']          	=   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
			$QI['RequestID']   		=   $RequestID;
			$QI['AnswerStatus']    	=   1;
				
			G::Obj('RequisitionsData')->insUpdRequisitionsData($QI);
		}
		
	}
}

$ques_to_skip[]	=	"btnCreateUpdate";
$ques_to_skip[]	=	"ValidateRequestID";

//Skip org levels
for($si = 1; $si < 10; $si++) {
	$ques_to_skip[]	= $si;
}
//Skip work day questions
$work_days_ques =	array("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");
foreach ($work_days_ques as $work_day_que_id) {
	$ques_to_skip[]	= $work_day_que_id;
}
######################

$insert_req_info        =   array("OrgID"=>$OrgID, "RequestID"=>$RequestID, "DateEntered"=>"NOW()", "LastModified"=>"NOW()");
$insert_reqdata_info    =   array();

$insert_req_info['FormID']          =   $def_vals['FormID'];
$insert_req_info['InternalFormID']  =   $def_vals['InternalFormID'];

//Skip Duplicate Data Questions - Split Fields Data
$skip_req_data_fields   =   G::Obj('Requisitions')->skip_req_data_fields;
$skip_req_data_fields   =   array_merge($requisition_fields, $skip_req_data_fields);

//Set Insert Parameters
foreach($_POST as $PostKey=>$PostValue) {
	
	if(!in_array($PostKey, $ques_to_skip)) {

		if($PostKey != "RequisitionManagers") {
			
			if ($PostKey == 'shifts_schedule_time' || $PostKey == 'LabelSelect') {
			
				foreach ( $_POST [$PostKey] as $cquestion => $canswer ) {
					$QI	= $form_que_list[$PostKey];
					
					if ($PostKey == 'LabelSelect') {
						$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
					}
					if ($PostKey == 'shifts_schedule_time') {
						$qa ['from_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
						$qa ['to_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
						$qa ['days'] 		= $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
						$answer 			= serialize ( $qa );
					}
						
					$APPDATA[$cquestion] = 	$answer;
						
					$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$cquestion, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI['Question'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Answer"=>$answer, ":AnswerStatus"=>1);
				}
			}
			else {
				if(in_array($PostKey, $requisition_fields)) {

					if($PostKey == "POE_from" || $PostKey == "POE_to") {
						$PostValue = $DateHelperObj->getYmdFromMdy($PostValue, "/", "-");
					}
					else if($PostKey == "PostDate") {
						$PostValue = $PostDate;
					}
					else if($PostKey == "ExpireDate") {
						$PostValue = $ExpireDate;
					}
					else if($PostKey == "MultiCostCenter") {
					    if(isset($_POST['MultiCostCenter']) && $_POST['MultiCostCenter'] != "") {
					        $PostValue = 'Y';
					    }
					}	
					
					$APPDATA[$PostKey] = $PostValue;
					$insert_req_info[$PostKey] = $PostValue;
				}
				else if(substr($PostKey, 0, 4) == "CUST") {
					$QI	= $form_que_list[$PostKey];
					$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$PostKey, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI['Question'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Answer"=>$PostValue, ":AnswerStatus"=>1);
				}
				else {
					$APPDATA[$PostKey] = $PostValue;
					
					if(!in_array($PostKey, $skip_req_data_fields)) {
						$QI	= $form_que_list[$PostKey];
						$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$PostKey, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI['Question'], ":QuestionTypeID"=>$QI['QuestionTypeID'], ":Answer"=>$PostValue, ":AnswerStatus"=>1);
					}
				}
			}
			
		}
			
	}
	
}

if(!isset($_POST['MultiCostCenter'])) {
    $insert_req_info['MultiCostCenter'] = 'N';
}


$AdvertisementOptions = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
if(count($AdvertisementOptions) > 0) {
	$insert_req_info['AdvertisingOptions'] = $SAO;
}
if(count($WorkWeek) > 0) {
	$insert_req_info['WorkDays'] = serialize($WorkWeek);
}


if(preg_match ( '/createRequest.php$/', $_SERVER ["SCRIPT_NAME"] )
	|| preg_match ( '/publicRequest.php$/', $_SERVER ["SCRIPT_NAME"] )) { //While creating new request 
	if($feature['RequisitionApproval'] == "Y") {
		$insert_req_info["Active"] 			= "R";
		$insert_req_info["Approved"] 		= "N";
	}
	else if($feature['RequisitionApproval'] != "Y") {
	
		if($feature['RequisitionRequest'] == "Y") {
			$insert_req_info["Active"] 		= "R";
			$insert_req_info["Approved"] 	= "Y";
		}
		else {
			$insert_req_info["Active"] 		= "Y";
			$insert_req_info["Approved"] 	= "Y";
		}
	}
}
else if(preg_match ( '/createRequisition.php$/', $_SERVER ["SCRIPT_NAME"] )) {	//While creating requisition
	if($feature['RequisitionApproval'] == "Y") {
		$insert_req_info["Active"] 			= "N";
		$insert_req_info["Approved"] 		= "N";
	} else {
		$insert_req_info["Active"] 			= $_REQUEST['Active'];
		$insert_req_info["Approved"] 		= "Y";
	}
}
else if(preg_match ( '/assign.php$/', $_SERVER ["SCRIPT_NAME"] )) { //While copying requisition
	
	if($feature['RequisitionApproval'] == "Y") {
		$insert_req_info["Active"] 			= "N";
		$insert_req_info["Approved"] 		= "N";
	} else {
		$insert_req_info["Active"] 			= $_REQUEST['Active'];
		$insert_req_info["Approved"] 		= "Y";
	}
}

//Insert data to Requisitions table
if(count($insert_req_info) > 0) {
	
    //Set Default RequisitionFormID
    $insert_req_info['RequisitionFormID'] = $RequisitionFormID;
    //Create Requsition
    $RequisitionsObj->insRequisitionsInfo("Requisitions", $insert_req_info);
}

//Insert data to RequisitionsData table
if(count($insert_reqdata_info) > 0) G::Obj('RequisitionsData')->insRequisitionsData($insert_reqdata_info);

//Insert RequisitionManagers Information
if (is_array ( $_POST ['RequisitionManagers'] )) {	
	// Set where condition
	$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
	// Set parameters information
	$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
	// Delete RequisitionManagers Information
	$RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where, array ($params) );
	
	while ( list ( $key, $val ) = each ( $_POST ['RequisitionManagers'] ) ) {
			
		// Requisition Managers Information to insert
		$req_man_info =   array(
                            	"OrgID" 		=> $OrgID,
                            	"RequestID" 	=> $RequestID,
                            	"UserID" 		=> $val
                            );
			
		if ($val != "") {
			// Insert RequisitionManagers Information
			$RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
		}
	} // end while
} // end if Managers is array

$insert_info    =   array(
                        "OrgID"             =>  $OrgID,
                        "RequestID"         =>  $RequestID,
                        "RequestTitle"      =>  $_REQUEST['Title'],
                        "Comments"          =>  "Added New Requisition",
                        "UpdatedFields"     =>  json_encode(array()),
                        "CreatedDateTime"   =>  "NOW()",
                        "UserID"            =>  $USERID
                    );
G::Obj('RequisitionHistory')->insRequisitionHistory($insert_info);

// set where condition
$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
// set parameters
$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Delete requisition org levels information
$RequisitionsObj->delRequisitionsInfo ( 'RequisitionOrgLevels', $where, array($params) );

$i = 1;
while ( $i <= 10 ) {
	if (is_array ( $_POST [$i] )) {

		while ( list ( $key, $val ) = each ( $_POST [$i] ) ) {

			if ($val) {
				// set org levels information
				$req_org_levels = array (
						"OrgID" 			=> $OrgID,
						"RequestID" 		=> $RequestID,
						"OrgLevelID" 		=> $i,
						"SelectionOrder" 	=> $val
				);
				// Insert Requisitions Informations
				$RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_levels );
			} // end if val
			
		} // end while
	} // end is_array
	
	$i ++;
} // end while Levels


if(preg_match ( '/createRequest.php$/', $_SERVER ["SCRIPT_NAME"] )
	|| preg_match ( '/publicRequest.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	if (! $USERID) {
		$USERID   =   'Requester';
		sendEditLink ( $OrgID, $RequestID );
		$Comments =   'New Request from ' . $_REQUEST['RequesterEmail'];
		updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );
	} else if($USERID) {
		sendEditLink ( $OrgID, $RequestID );
		$Comments =   'New Request from ' . $_REQUEST['RequesterEmail'];
		updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );
	}
	
	sendAdminAlert ( $OrgID, $RequestID, "Start Request" );
}	
 ?>
