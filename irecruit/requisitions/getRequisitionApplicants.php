<?php
require_once '../Configuration.inc';

// Get Applicants based on RequestID
$columns        =   "ApplicationID, RequestID, ApplicantSortName, ProcessOrder, date_format(EntryDate, '%m/%d/%Y %H:%i:%s') as EntryDate";
$where_info     =   array("OrgID    =   :OrgID");
$params_info    =   array(":OrgID"  =>  $OrgID);

if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
    $where_info[] = "RequestID = :RequestID";
    $params_info[":RequestID"] = $_REQUEST['RequestID'];
}else{
	$_REQUEST['RequestID'] = "";
	$where_info[] = "RequestID = :RequestID";
    $params_info[":RequestID"] = $_REQUEST['RequestID'];
}

$app_results = $ApplicationsObj->getJobApplicationsInfo($columns, $where_info, '', 'EntryDate DESC', array(
    $params_info
));

$applicants_list = $app_results['results'];

for ($i = 0; $i < count($applicants_list); $i ++) {
    $ApplicationID = $applicants_list[$i]['ApplicationID'];
    $applicants_list[$i]['ToTime'] 			=	strtotime($applicants_list[$i]['EntryDate']);
    $applicants_list[$i]['Email'] 			=	$ApplicantDetailsObj->getAnswer($OrgID, $ApplicationID, 'email');
    $applicants_list[$i]['ProcessOrder'] 	=	$ApplicantDetailsObj->getProcessOrderDescription($OrgID, $applicants_list[$i]['ProcessOrder']);
}

// Applications list
echo json_encode($applicants_list);
?>