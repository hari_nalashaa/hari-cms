<?php
require_once '../Configuration.inc';

$columns = "date_format(PostDate,'%m/%d/%Y') PostDate, date_format(ExpireDate,'%m/%d/%Y') ExpireDate, 
			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,  
			datediff(now(),PostDate) Open, datediff(ExpireDate,PostDate) Closed, 
			date_format(date_add(now(), INTERVAL 0 DAY),'%m/%d/%Y') Current, 
			RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID, 
			Description, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry";
$REQS = $RequisitionsObj->getReqDetailInfo($columns, $OrgID, $_REQUEST['MultiOrgID'], $_REQUEST['RequestID']);

if(!isset($REQS['RequestID']) || $REQS['RequestID'] == "") {
	echo "Failed";exit;
}
if ($feature['MonsterAccount'] != "Y") {
	echo "Failed";exit;
}

$separator = "#aaaaaa";

//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID", "ReturnTypeCode = 'success'");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID'], ":RequestID"=>$_REQUEST['RequestID']);
//Get PostRequisitions Information
$respostinfo    =   $RequisitionsObj->getPostRequisitionsInformation("*", $where, "", array($params));
$postinfocount  =   $respostinfo['count'];
$rowpostinfo    =   $respostinfo['results'][0];
$PostingId      =   $rowpostinfo['PostingId'];

$MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();

if($REQS['MonsterJobIndustry'] == 0 || $REQS['MonsterJobIndustry'] == "0") 
	$MonsterJobIndustry = "All";
else 
	$MonsterJobIndustry = $MonsterJobIndustries[$REQS['MonsterJobIndustry']];

$JobCategories  = $MonsterObj->monsterJobCategories('yes');
$JobOccupations = $MonsterObj->getMonsterJobCategoriesOccupations();

if($REQS['Duration'] == '' || $REQS['Duration'] == '0') {
	$MonsterObj = new Monster ();
	$MonsterInformation = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
	$MonsterOrgInfo = $MonsterInformation ['row'];
	
	$REQS['Duration'] = $MonsterOrgInfo ['Duration'];
}


$MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();
$Duration = $MonsterOrgInfo ['Duration'];
if($Duration == '') $Duration = 90;
	
echo '<br><table class="table table-bordered">';
echo '<tr>';
echo '<td>';
echo '<div id="requisition_monster_information">';
echo '<strong>Monster Job Post Type: </strong>';
echo '<span id="header_monster_job_post_type">'.$REQS['MonsterJobPostType'].'</span><br>';

echo '<strong>Monster Job Post Duration: </strong>';
echo '<span id="header_monster_job_post_duration">'.$REQS['Duration'].'</span><br>';

echo '<strong>Monster Job Industry: </strong><span id="header_monster_job_post_industry">'.$MonsterJobIndustry.'</span><br>';
echo '<strong>Monster Job Category: </strong><span id="header_monster_job_post_category">'.$JobCategories[$REQS['MonsterJobCategory']].'</span><br>';
echo '<strong>Monster Job Occupation: </strong><span id="header_monster_job_post_occupation">'.$JobOccupations[$REQS['MonsterJobOccupation']].'</span>';
echo '</div>';
echo '</td>';
echo '</tr>';
echo '</table>';

if ($postinfocount == 0) {
	
	$MonsterInformation    =   $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $_REQUEST['MultiOrgID'] );
	$MonsterOrgInfo        =   $MonsterInformation ['row'];
	require_once IRECRUIT_DIR . 'monster/InventoryDetails.inc';
	if ($MonsterOrgInfo ['RecruiterReferenceUserName'] != "" && $MonsterOrgInfo ['Password'] != "" && $fault_string == 'False') {
		include IRECRUIT_DIR . 'monster/SelectInventoryInfo.inc';
	} // end user and password for inventory
	
	if($fault_string == 'True') {
		echo "<h4>There is no inventory related to your credentials.</h4>";
	}
			
} else {
	
	if($REQS['RemainingDays'] > 0) {
		$monster_jobview_url = 'http://jobview.monster.com/getjob.aspx?JobID='.$PostingId;
		echo "<h4><a href='".$monster_jobview_url."' target='_blank'>Click here to open your existing job</a></h4>";
	}
	else if (isset($REQS ['RemainingDays']) && $REQS ['RemainingDays'] < 0) {
		echo "<h4>Sorry this job is already expired.<br> Please repost this requisition using Job Board Refresh<br> Post the job to monster in new requisition page</h4>";
	}
}
?>