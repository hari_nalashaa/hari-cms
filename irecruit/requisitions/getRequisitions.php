<?php
require_once '../Configuration.inc';

$where_info     =   array("OrgID = :OrgID", "Approved = :Approved");
$params_info    =   array(":OrgID"=>$OrgID, ":Approved"=>'Y');

if(isset($_REQUEST['RequisitionStatus']) && $_REQUEST['RequisitionStatus'] == "Y") {
    $where_info[]   =   "Active = 'Y'";
}
else if(isset($_REQUEST['RequisitionStatus']) && $_REQUEST['RequisitionStatus'] == "N") {
    $where_info[]   =   "Active = 'N'";
}

if(isset($_REQUEST['IsFinal']) && $_REQUEST['IsFinal'] == 'Y') {
    $where_info[]   =   "getRequisitionFinalStageStatus(OrgID, RequisitionStage) = 'Y'";
}

$requisition_results    =   $RequisitionsObj->getRequisitionInformation("RequestID, Title, RequisitionID, JobID", $where_info, "", "Title ASC", array($params_info));
$requisitions_list      =   $requisition_results['results'];

echo json_encode($requisitions_list);
?>