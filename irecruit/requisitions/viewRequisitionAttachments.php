<?php
require_once '../irecruitdb.inc';

if ($_REQUEST ['RequestID'] != "") {
	
	// Set where condition
	$where     =   array(
                        "OrgID          =   :OrgID",
                        "RequestID  =   :RequestID",
                        "FileID           =   :FileID" 
                    ); 
	// Set parameters
	$params    =   array (
                        ":OrgID"            =>  $_REQUEST ['OrgID'],
                        ":RequestID"    =>  $_REQUEST ['RequestID'],
                        ":FileID"             =>  $_REQUEST ['FileID'] 
                    );
	// Get Applicant Vault Information
	$results    =   G::Obj('RequisitionAttachments')->getRequisitionAttachmentsInfo ( "*", $where, '', array ($params) );
	
	$RV         =   $results ['results'] [0];
	
	//$filename   =   $RV['FileID'].'_'.$RV['FileName'];
	$filename   =   $RV['FileID'].'.'.$RV['FileExt'];

	$downloadfilename   =   $RV['FileName'];

} else {
    
    $filename   =   $File;
    
} // end else ApplicationID

if ($filename) {
	
	$dfile = '';
	$dfile = IRECRUIT_DIR . 'vault/' . $_REQUEST ['OrgID'] . '/requisitionattachments/'. $_REQUEST ['RequestID'].'/' . $filename;
	
	if (file_exists ( $dfile )) {
		header ( 'Content-Description: File Transfer' );
		header ( 'Content-Type: application/octet-stream' );
		header ( 'Content-Disposition: attachment; filename=' . basename ( $downloadfilename  ) );
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		header ( 'Content-Length: ' . filesize ( $dfile ) );
		readfile ( $dfile );
		exit ();
	}
} // end if FileName
?>
