<?php
require_once '../Configuration.inc';

$columns    =   "date_format(PostDate,'%m/%d/%Y') PostDate, date_format(ExpireDate,'%m/%d/%Y') ExpireDate, 
    			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,  
    			datediff(now(),PostDate) Open, datediff(ExpireDate,PostDate) Closed, 
    			date_format(date_add(now(), INTERVAL 0 DAY),'%m/%d/%Y') Current, 
    			RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID, 
    			Description, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry";
$REQS       =   G::Obj('Requisitions')->getReqDetailInfo($columns, $OrgID, $_REQUEST['MultiOrgID'], $_REQUEST['RequestID']);

if(!isset($REQS['RequestID']) || $REQS['RequestID'] == "") {
	echo "Failed";exit;
}
if ($feature['MonsterAccount'] != "Y") {
	echo "Failed";exit;
}

$separator = "#aaaaaa";

$req_info               =   G::Obj('Requisitions')->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $_REQUEST['RequestID']);
$free_jobboard_list     =   json_decode($req_info['FreeJobBoardLists'], true);


//Set where condition
$where  =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID", "ReturnTypeCode = 'success'");
//Set parameters
$params =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID'], ":RequestID"=>$_REQUEST['RequestID']);
//Get PostRequisitions Information
$respostinfo    =   $RequisitionsObj->getPostRequisitionsInformation("*", $where, "", array($params));
$postinfocount  =   $respostinfo['count'];
$rowpostinfo    =   $respostinfo['results'][0];
$PostingId      =   $rowpostinfo['PostingId'];

$MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();

if($REQS['MonsterJobIndustry'] == 0 || $REQS['MonsterJobIndustry'] == "0") 
	$MonsterJobIndustry = "All";
else 
	$MonsterJobIndustry = $MonsterJobIndustries[$REQS['MonsterJobIndustry']];

$JobCategories  = $MonsterObj->monsterJobCategories('yes');
$JobOccupations = $MonsterObj->getMonsterJobCategoriesOccupations();

if($REQS['Duration'] == '' || $REQS['Duration'] == '0') {
	$MonsterObj = new Monster ();
	$MonsterInformation = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
	$MonsterOrgInfo = $MonsterInformation ['row'];
	
	$REQS['Duration'] = $MonsterOrgInfo ['Duration'];
}


$MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();
$Duration = $MonsterOrgInfo ['Duration'];
if($Duration == '') $Duration = 90;
	
echo '<img src="'.IRECRUIT_HOME.'images/monster_jobboard.jpg"><br><br>';
echo '<form name="frmMonsterInfo" id="frmMonsterInfo" method="post">';
echo '<input type="hidden" name="Duration" id="Duration" value="' . $Duration . '">';

echo '<div class="form-group">';
echo '<label style="font-weight:normal">Job Industry: </label> ';
echo '<select name="MonsterJobIndustry" id="MonsterJobIndustry" class="form-control width-auto-inline" onchange=\'saveMonsterInfo(this, "'.$REQS['RequestID'].'", "", "paid")\'>';
echo '<option value=""';
if($REQS['MonsterJobIndustry'] == "") echo ' selected="selected"';
echo '>Select Job Industry</option>';
echo '<option value="0"';
if($REQS['MonsterJobIndustry'] == "0") echo ' selected="selected"';
echo '>All</option>';

foreach ( $MonsterJobIndustries as $JobIndustryId => $JobIndustryName ) {
    $selected = (($JobIndustryId == $REQS['MonsterJobIndustry']) && ($REQS['MonsterJobIndustry'] != "")) ? "selected='selected'" : "";
    echo '<option value="'.$JobIndustryId.'"'.$selected.'>'.$JobIndustryName.'</option>';
    unset ( $selected );
}

echo '</select>';
echo '</div>';

echo '<div class="form-group">';
echo '<label style="font-weight:normal">Job Category: </label> ';
	
$JobCategories = $MonsterObj->monsterJobCategories('yes');

echo '<select name="MonsterJobCategory" id="MonsterJobCategory" class="form-control width-auto-inline" onchange=\'saveJobOccupationAndInfo(this.value, "", this, "'.$REQS['RequestID'].'", "", "paid")\'>';
echo '<option value="">Select Job Category</option>';

foreach($JobCategories as $JobCategoryId=>$JobCategoryAlias) {
	$selected = ($JobCategoryId == $REQS['MonsterJobCategory']) ? "selected='selected'" : "";
	echo '<option value="'.$JobCategoryId.'"'.$selected.'>'.$JobCategoryAlias.'</option>';
	unset ( $selected );
}

echo '</select>';
echo '</div>';

echo '<div class="form-group">';
echo '<label style="font-weight:normal">Job Occupation: </label> ';
echo '<span id="JobOccupationsList">';
echo '<select name="MonsterJobOccupation" id="MonsterJobOccupation" class="form-control width-auto-inline" onchange=\'saveMonsterInfo(this, "'.$REQS['RequestID'].'", "", "paid")\'>';
echo '<option value="">Select</option>';
foreach($JobOccupations as $JobOccupationId=>$JobOccupationName) {
    $selected = ($JobOccupationId == $REQS['MonsterJobOccupation']) ? "selected='selected'" : "";
    echo '<option value="'.$JobOccupationId.'" '.$selected.'>'.$JobOccupationName.'</option>';
}
echo '</select>';
echo '</span>';
echo '</div>';

echo '<div class="form-group">';
echo '<label style="font-weight:normal">';
?>
<div class="new_tooltip">Job Post Type:
    <div class="new_tooltiptext">About 'POST' Method: (minimal information captured)  
1) A user applies for a position using their Monster login and information only. If user has successfully applied for a job in Monster their details will be saved to your iRecruit Account.
<br>
About 'REQUEST' Method: (maximum information captured)
1) A user applies for a position using their Monster login and information. Once they have successfully entered their Monster information they are redirected to the iRecruit job application form.
2) The user will need to fill out the iRecruit application form with additional information and submit it before their information is saved to your iRecruit Account.
    </div>
</div>
<?php
echo ' </label> ';
echo '<select name="MonsterJobPostType" id="MonsterJobPostType" class="form-control width-auto-inline" onchange=\'saveMonsterInfo(this, "'.$REQS['RequestID'].'", "", "paid")\'>';
echo '<option value="REQUEST"';
if($REQS['MonsterJobPostType'] == "REQUEST") echo ' selected="selected"';
echo '>REQUEST</option>';
echo '<option value="POST"';
if($REQS['MonsterJobPostType'] == "POST") echo ' selected="selected"';
echo '>POST</option>';
echo '</select>';
echo '</div>';

echo '<div class="form-group">';
echo '<input type="hidden" name="MonsterRequestID" id="MonsterRequestID" value="'.$REQS['RequestID'].'">';
echo '</div>';

echo '</form>';

echo '<br>';

if($REQS['MonsterJobPostType'] != ""
    && $MonsterJobIndustry != ""
    && $REQS['MonsterJobCategory'] != ""
    && $REQS['MonsterJobOccupation'] != "") {
    
    $free_monster_checked = "";
    if(in_array("Monster", $free_jobboard_list)) $free_monster_checked = ' checked="checked"';
    
    echo "<input type=\"radio\" name=\"Monster\" value=\"Free\" class=\"monster_fp_sel\"";
    echo $free_monster_checked;
    echo " onclick=\"getFreeMonster('free_paid_monster_info')\">&nbsp;Post your job to Monster - Free <br>";
    echo "<input type=\"radio\" name=\"Monster\" value=\"Paid\" class=\"monster_fp_sel\" onclick=\"getPaidMonster('free_paid_monster_info')\">&nbsp;Post your job to Monster - Sponsored ";
    
    echo '<div id="free_paid_monster_info">';
    if(in_array("Monster", $free_jobboard_list)) {
        echo '<br><br>';
        echo '<input class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="updateFreeJobBoardListings(\'Monster\');" value="Update" type="button">';
    }
    
}
else {
	echo '<span style="font-weight:bold;color:red;">Please select all the monster fields, before posting a job.</span>';
}
echo '</div>';
echo '<br>';
echo '<br>';
echo '<br>';
echo '<br>';
?>