<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= 'Key';
$TemplateObj->k = $k = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/requisitions/Key.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/requisitions/Key');
}
?>