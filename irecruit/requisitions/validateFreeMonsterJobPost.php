<?php
require_once '../Configuration.inc';

$columns = "date_format(PostDate,'%m/%d/%Y') PostDate, date_format(ExpireDate,'%m/%d/%Y') ExpireDate, 
			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,  
			datediff(now(),PostDate) Open, datediff(ExpireDate,PostDate) Closed, 
			date_format(date_add(now(), INTERVAL 0 DAY),'%m/%d/%Y') Current, 
			RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID, 
			Description, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry, FreeJobBoardLists";
$REQS = $RequisitionsObj->getReqDetailInfo($columns, $OrgID, $_REQUEST['MultiOrgID'], $_REQUEST['RequestID']);
$free_job_board_lists = json_decode($REQS['FreeJobBoardLists'], true);

if(!isset($REQS['RequestID']) || $REQS['RequestID'] == "") {
	echo "Failed";exit;
}
if ($feature['MonsterAccount'] != "Y") {
	echo "Failed";exit;
}

$separator = "#aaaaaa";

//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID", "ReturnTypeCode = 'success'");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID'], ":RequestID"=>$_REQUEST['RequestID']);
//Get PostRequisitions Information
$respostinfo    =   $RequisitionsObj->getPostRequisitionsInformation("*", $where, "", array($params));
$postinfocount  =   $respostinfo['count'];
$rowpostinfo    =   $respostinfo['results'][0];
$PostingId      =   $rowpostinfo['PostingId'];

$MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();

if($REQS['MonsterJobIndustry'] == 0 || $REQS['MonsterJobIndustry'] == "0") 
	$MonsterJobIndustry = "All";
else 
	$MonsterJobIndustry = $MonsterJobIndustries[$REQS['MonsterJobIndustry']];

$JobCategories  = $MonsterObj->monsterJobCategories('yes');
$JobOccupations = $MonsterObj->getMonsterJobCategoriesOccupations();

if($REQS['Duration'] == '' || $REQS['Duration'] == '0') {
	$MonsterObj = new Monster ();
	$MonsterInformation = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
	$MonsterOrgInfo = $MonsterInformation ['row'];
	
	$REQS['Duration'] = $MonsterOrgInfo ['Duration'];
}

$MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();
$Duration = $MonsterOrgInfo ['Duration'];
if($Duration == '') $Duration = 90;
	
echo '<br>';

echo '<input class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="updateFreeJobBoardListings(\'Monster\');" value="Update" type="button">';
?>