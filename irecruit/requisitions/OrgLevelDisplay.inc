<?php
//Set columns
$columns = "MAX(OrgLevelID) as MaxOrgLevelID";
//Set where condition, it is common for below organization levels information function calls
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters, it is common for below organization levels information function calls
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID']);

//Get organization levels information
$results = $OrganizationsObj->getOrganizationLevelsInfo($columns, $where, '', array($params));
$Levels  = $results['results'][0];

//Set columns
$columns = array("OrganizationLevel", "OrgLevelID");
//Get organization levels information
$results = $OrganizationsObj->getOrganizationLevelsInfo($columns, $where, 'OrgLevelID', array($params));

$ii = 0;

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$ii ++;
	
		if ($ii <= $Levels ['MaxOrgLevelID']) {
			$OrganizationLevel [$ii] = $row ['OrganizationLevel'];
			$OrgLevelID [$ii] = $row ['OrgLevelID'];
		}
	}
}

$i = 1;
$OrganizationLevelDisplay = '';
$org_level_error = FALSE;
while ( $i <= $Levels ['MaxOrgLevelID'] ) {
	
	$OL = '';
	
	$selcnt = 0;
	$SEL = array ();
	
	//Set condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID", "OrgLevelID = :OrgLevelID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":OrgLevelID"=>$i);
	
	//Get organization levels information
	$results = $RequisitionsObj->getRequisitionOrgLevelsInfo("SelectionOrder", $where, "", array($params));
	$selcnt  = $results['count'];
	$iii = 0;

	if(is_array($results['results'])) {
		foreach($results['results'] as $SelectionOrder) {
			$iii ++;
			$SEL [$iii] = $SelectionOrder ['SelectionOrder'];
		} // end foreach
	}
	
	//Set columns
	$columns = array("CategorySelection", "SelectionOrder");
	//Set condition
	$where	 = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID = :OrgLevelID");
	//Set parameters
	$params  = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID'], ":OrgLevelID"=>$i);
	
	//Get organization level data information
	$resultsIN = $OrganizationsObj->getOrganizationLevelDataInfo($columns, $where, 'SelectionOrder', array($params));
	$rowcnt = $loccnt = $resultsIN['count'];
	
	/**
	 * ription overwrite the existing array if $_POST is not empty
	 * either in edit mode as well as in copy and new application mode
	 */
	if (isset ( $_POST ) && count ( $_POST ) > 0) {
		$SEL = array ();
		$SEL = $_POST [$OrgLevelID [$i]];
		$selcnt = count ( $_POST [$OrgLevelID [$i]] );
	}
	
	$cond = 'Please Select';
	/*
	if ($selcnt == 0) {
	    $cond = 'Please Select';
		$selected = " selected";
	} else {
		$cond = 'None';
		$selected = "";
	}
	*/
	
	if ($loccnt >= 1) {
		$OL .= '<option value="">' . $cond . '</option>';
	}
	
	if(is_array($resultsIN['results'])) {
		foreach ($resultsIN['results'] as $row) {
			if (is_array($SEL) && in_array ( $row ['SelectionOrder'], $SEL )
				|| (is_array($_REQUEST[$OrganizationLevel [$i]]) 
				    && in_array ( $row ['SelectionOrder'], $_REQUEST[$OrganizationLevel [$i]] ))) {
				$selected = ' selected="selected"';
			} else {
				$selected = '';
			}
			$OL .= '<option value="' . $row ['SelectionOrder'] . '"' . $selected . '>' . $row ['CategorySelection'] . '</option>';
		}	
	}
	
	
	if ($rowcnt > 1) {
		$multiple = ' multiple size="4" ';
	} else {
		$multiple = ' ';
	}

	$requiredck = '';
	if(is_array($error_inputs) && in_array("OrgLevel", $error_inputs) && $OrgLevelID [$i] == "1") {
	    $org_level_error = TRUE;
	    $requiredck = ' style="background-color:' . $bg_color . ';padding-bottom:10px;margin-bottom:10px"';
	}
	
	$OrganizationLevelDisplay .= $OrganizationLevel [$i].'<br>';
	$OrganizationLevelDisplay .= '<select' . $multiple . 'name="' . $OrgLevelID [$i] . '[]" class="form-control width-auto-inline">';
	$OrganizationLevelDisplay .= $OL;
	$OrganizationLevelDisplay .= '</select>'.'<br>';

	$i ++;
}
?>