<?php
$title = 'Advertise Requisitions';

//Declare mostly using super globals over here
$TemplateObj->action = $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->process = $process = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->Active = $Active = isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

$TemplateObj->PostDate = $PostDate = isset($_REQUEST['PostDate']) ? $_REQUEST['PostDate'] : '';
$TemplateObj->ExpireDate = $ExpireDate = isset($_REQUEST['ExpireDate']) ? $_REQUEST['ExpireDate'] : '';

$TemplateObj->PostHour = $PostHour = isset($_REQUEST['PostHour']) ? $_REQUEST['PostHour'] : '';
$TemplateObj->PostMinute = $PostMinute = isset($_REQUEST['PostMinute']) ? $_REQUEST['PostMinute'] : '';
$TemplateObj->ExpireHour = $ExpireHour = isset($_REQUEST['ExpireHour']) ? $_REQUEST['ExpireHour'] : '';
$TemplateObj->ExpireMinute = $ExpireMinute = isset($_REQUEST['ExpireMinute']) ? $_REQUEST['ExpireMinute'] : '';

$TemplateObj->poe_from = $poe_from = isset($_REQUEST['poe_from']) ? $_REQUEST['poe_from'] : '';
$TemplateObj->poe_to = $poe_to = isset($_REQUEST['poe_to']) ? $_REQUEST['poe_to'] : '';
$TemplateObj->hours = $hours = isset($_REQUEST['hours']) ? $_REQUEST['hours'] : '';
$TemplateObj->weeks = $weeks = isset($_REQUEST['weeks']) ? $_REQUEST['weeks'] : '';
$TemplateObj->program = $program = isset($_REQUEST['program']) ? $_REQUEST['program'] : '';
$TemplateObj->replacement = $replacement = isset($_REQUEST['replacement']) ? $_REQUEST['replacement'] : '';
$TemplateObj->reportsto = $reportsto = isset($_REQUEST['reportsto']) ? $_REQUEST['reportsto'] : '';
$TemplateObj->Owner = $Owner = isset($_REQUEST['Owner']) ? $_REQUEST['Owner'] : '';
$TemplateObj->LimitRequisitions = $LimitRequisitions = $preferences['ApplicantsSearchResultsLimit'];

$subtitle = array (
		"edit" => "Edit",
		"copy" => "Copy",
		"costs" => "Costs",
		"new" => "Create New"
);
if (($subtitle [$action]) && (! $reqaction)) {
	$title .= ' - ' . $subtitle [$action];
}

if ($reqaction) {
	$title .= ' - ' . $subtitle [$reqaction];
}
?>