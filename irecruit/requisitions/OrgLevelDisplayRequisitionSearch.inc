<?php
//Set columns
$columns = "MAX(OrgLevelID) as MaxOrgLevelID";
//Set where condition, it is common for below organization levels information function calls
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters, it is common for below organization levels information function calls
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);

//Get organization levels information
$results = $OrganizationsObj->getOrganizationLevelsInfo($columns, $where, '', array($params));
$Levels  = $results['results'][0];

//Set columns
$columns = array("OrganizationLevel", "OrgLevelID");
//Get organization levels information
$results = $OrganizationsObj->getOrganizationLevelsInfo($columns, $where, 'OrgLevelID', array($params));

$ii = 0;
if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$ii ++;
		
		if ($ii <= $Levels ['MaxOrgLevelID']) {
			
			$OrganizationLevel [$ii] = $row ['OrganizationLevel'];
			$OrgLevelID [$ii] = $row ['OrgLevelID'];
		}
	}
}

$i = 1;
while ( $i <= $Levels ['MaxOrgLevelID'] ) {
	
	$OL = '';
	
	$selcnt = 0;
	$SEL = array ();
	
	//Set condition
	$where = array("OrgID = :OrgID", "OrgLevelID = :OrgLevelID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":OrgLevelID"=>$i);
	
	//Get organization levels information
	$results = $RequisitionsObj->getRequisitionOrgLevelsInfo("SelectionOrder", $where, "", array($params));
	$selcnt  = $results['count'];
	
	$iii = 0;
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $SelectionOrder) {
			$iii ++;
			$SEL [$iii] = $SelectionOrder ['SelectionOrder'];
		} // end foreach
	}
	
	//Set columns
	$columns = array("CategorySelection", "SelectionOrder");
	//Set condition
	$where	 = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID = :OrgLevelID");
	//Set parameters
	$params  = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrgLevelID"=>$i);
	
	//Get organization level data information
	$resultsIN = $OrganizationsObj->getOrganizationLevelDataInfo($columns, $where, 'SelectionOrder', array($params));
	$rowcnt = $loccnt = $resultsIN['count'];
	
	if ($selcnt == 0) {
		$cond = 'Please Select';
		$selected = " selected";
	} else {
		$cond = 'None';
		$selected = "";
	}
	
	if ($loccnt >= 1) {
		$OL .= '<option value=""' . $selected . '>' . $cond;
	}
	
	if(is_array($resultsIN['results'])) {
		foreach ($resultsIN as $row) {
			if (in_array ( $row ['SelectionOrder'], $SEL )) {
				$selected = ' selected';
			} else {
				$selected = '';
			}
			$OL .= '<option value="' . $row ['SelectionOrder'] . '"' . $selected . '>' . $row ['CategorySelection'];
		}	
	}
	
	echo '<tr><td align=left width="23%">' . $OrganizationLevel [$i] . '</td>';
	echo '<td width="3%">:</td>';
	
	if ($rowcnt > 1) {
		$multiple = ' multiple size="4" ';
	} else {
		$multiple = ' ';
	}
	echo '<td><select' . $multiple . 'name="' . $OrgLevelID [$i] . '[]">';
	echo $OL;
	echo '</select>';
	echo '</td></tr>';
	
	$i ++;
}
?>