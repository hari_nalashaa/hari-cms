<?php  
// Set columns
$columns = "NOW() AS CurrentDateTime, DATE_FORMAT(PostDate,'%m/%d/%Y') PostDate,DATE_FORMAT(DateEntered,'%m/%d/%Y') DateEntered, DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate,
			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,";

$columns .= "IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate)) Open, DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate) Closed,";

$columns .= "DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current,
			RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID,
			Description, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry, Active, Approved, FreeJobBoardLists, City, State, ZipCode, PresentOn";

if($new_job_days > 0) {
    $columns .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) Highlight";
}
else {
    $columns .= ", Highlight";
}

$columns_count = "COUNT(RequestID) AS RequisitionsCount";

if($FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
    // Set condition
    $where = array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
    // Set parameters
    $params = array (":OrgID"=>$FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
}
else {
    // Set condition
    $where = array ("OrgID = :OrgID");
    // Set parameters
    $params = array (":OrgID" => $OrgID);
}

if (preg_match ( '/jobBoardPosting.php$/', $_SERVER ["SCRIPT_NAME"] )
    || (isset($_REQUEST['PageSource']) && $_REQUEST['PageSource'] == 'JobBoard')) {
    $where[] = "ExpireDate > NOW()";
}

if ($feature ['RequisitionApproval'] != "Y") {
	if($Active == "A") {
		$where[] = "Active != :Active";
		$params[":Active"] = 'R';
	}
	else {
		$where[] = "Active = :Active";
		$params[":Active"] = $Active;
	}
}
else if ($feature ['RequisitionApproval'] == "Y") {
	if($Active != "A") {
		$where[] = "Active = :Active";
		$params[":Active"] = $Active;
	}
}

if(isset($IsRequisitionApproved) && $IsRequisitionApproved == "N") {
	$where[] = "Approved = 'N'";
}

if(isset($PresentOn) && $PresentOn != "") {
	if(($PresentOn == "INTERNALONLY") || ($PresentOn == "PUBLICONLY")) {
		$where[] = "PresentOn = :PresentOn";
		$params[":PresentOn"] = $PresentOn;
	}
}

if($Active == "Y") $where[] = "Approved = 'Y'";

if(isset($_REQUEST['keyword']) && $_REQUEST['keyword'] != "") {
	if($_REQUEST['keyword_match'] == "S") {
		$params[":STitle"] = "".Database::escape(trim($_REQUEST['keyword']))."%";
		$params[":SRequisitionID"] = "".Database::escape(trim($_REQUEST['keyword']))."%";
		$params[":SJobID"] = "".Database::escape(trim($_REQUEST['keyword']))."%";
		$where [] = "(Title LIKE :STitle OR RequisitionID LIKE :SRequisitionID OR JobID LIKE :SJobID)";
	}
	else if($_REQUEST['keyword_match'] == "W") {
		$params[":STitle"] = "%".Database::escape(trim($_REQUEST['keyword']))."%";
		$params[":SRequisitionID"] = "%".Database::escape(trim($_REQUEST['keyword']))."%";
		$params[":SJobID"] = "%".Database::escape(trim($_REQUEST['keyword']))."%";
		$where [] = "(Title LIKE :STitle OR RequisitionID LIKE :SRequisitionID OR JobID LIKE :SJobID)";
	}

	$keyword_match = $_REQUEST['keyword_match'];
	$keyword = trim($_REQUEST['keyword']);
}

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$params [":RMOrgID"] = $OrgID;
	$params [":RMUserID"] = $USERID;
	$where [] = " RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :RMOrgID AND UserID = :RMUserID)";
} // end hiring manager

if ($OrgID == "I20131020") {
    
	$order_by       = " Title, Requisitions.PostDate DESC";
	$sort_by_key    = "Title, Requisitions.PostDate";
	$sort_type      = "DESC";
	
} else {
	
	if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
		$sort_type = "ASC";
	} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
		$sort_type = "DESC";
	}	
	
	if (isset ( $_REQUEST ['to_sort'] )) {
		if ($_REQUEST ['to_sort'] == "date_opened") {
		    $order_by = " Requisitions.PostDate $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "date_entered") {
		    $order_by = " Requisitions.DateEntered $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "req_id") {
		    $order_by = " RequisitionID $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "job_id") {
		    $order_by = " JobID $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "req_title") {
		    $order_by = " Title $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "applicants_count") {
		    $order_by =   " ApplicantsCount $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "days_open") {
			$order_by = " IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate))  $sort_type";
		}
		else if ($_REQUEST ['to_sort'] == "expire_date") {
		     $order_by = " DATEDIFF(NOW(), Requisitions.ExpireDate) $sort_type";
		}
		
		$sort_by_key = $_REQUEST ['to_sort'];
	} else {
		$order_by = " Requisitions.PostDate DESC";
		$sort_by_key = "Requisitions.PostDate";
		$sort_type = "DESC";
	}
}
  
$Start = 0;
if (isset ( $_REQUEST ['IndexStart'] ) && is_numeric ( $_REQUEST ['IndexStart'] )) {
	$Start = $_REQUEST ['IndexStart'];
}
	

$req_limit = " LIMIT $Start, $LimitRequisitions";

$req_params_where = array ("OrgID = :OrgID", "UserID = :UserID", "RequisitionGuID = :RequisitionGuID");
$req_params_list = array (":OrgID" => $OrgID, ":UserID" => $USERID, ":RequisitionGuID"=>$GuIDRequisition);
$qi = $RequisitionsObj->getRequisitionsSearchParams ( "*", $req_params_where, '', array ($req_params_list) );

if ($ServerInformationObj->getRequestSource () != 'ajax' && $qi ['Query'] != "") {
	
	$search_params_info = json_decode($qi['SearchParams'], true);
	if($search_params_info[":Active"] != "") {
		$Active = $search_params_info[":Active"];
		if(is_object($TemplateObj)) $TemplateObj->Active = $search_params_info[":Active"];
	}
	
	$query_req_param = $qi ['Query'] . " WHERE " . $qi ['MainSearchWhere'];
	if (trim($qi ['SortBy']) != "") $query_req_param .= " ORDER BY " . trim($qi ['SortBy']);

	$query_req_param .= " LIMIT 0, $LimitRequisitions";
	
	$query_cnt_param = "SELECT COUNT(RequestID) AS RequisitionsCount FROM Requisitions WHERE " . $qi ['MainSearchWhere'];
	
	$sort_type = $qi ['SortByType'];
	$keyword_match = $qi ['KeywordMatch'];
	$keyword = $qi ['Keyword'];	
	
	if(isset($keyword_match) && $keyword_match != "") {
		if($keyword_match == "S") {
			$params[":STitle"] = "".Database::escape($keyword)."%";
			$params[":SRequisitionID"] = "".Database::escape($keyword)."%";
			$params[":SJobID"] = "".Database::escape($keyword)."%";
			$where [] = "(Title LIKE :STitle OR RequisitionID LIKE :SRequisitionID OR JobID LIKE :SJobID)";
		}
		else if($keyword_match == "W") {
			$params[":STitle"] = "%".Database::escape($keyword)."%";
			$params[":SRequisitionID"] = "%".Database::escape($keyword)."%";
			$params[":SJobID"] = "%".Database::escape($keyword)."%";
			$where [] = "(Title LIKE :STitle OR RequisitionID LIKE :SRequisitionID OR JobID LIKE :SJobID)";
		}
	}	
	
	// Get Requisition Information
	$req_search_results = $RequisitionsObj->getInfo ( $query_req_param, array (
			json_decode ( $qi ['SearchParams'], true ) 
	) );
	$total_results_count = $RequisitionsObj->getInfo ( $query_cnt_param, array (
			json_decode ( $qi ['SearchParams'], true ) 
	) );
	$total_requisitions_count = $total_results_count ['results'] [0] ['RequisitionsCount'];
	$cnt = $req_search_results ['count'];
} else {
	// Get Requisition Information
	$req_search_results = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", $order_by . $req_limit, array (
			$params 
	) );
	// Get Requisitions Count
	$total_results_count = $RequisitionsObj->getRequisitionInformation ( $columns_count, $where, "", "", array (
			$params 
	) );
	$total_requisitions_count = $total_results_count ['results'] [0] ['RequisitionsCount'];
	$cnt = $req_search_results ['count'];
}

$main_search_query = "SELECT $columns FROM Requisitions";

// SearchQuery Information to insert
$insert_info = array (
		"OrgID"               =>  $OrgID,
		"UserID"              =>  $USERID,
		"RequisitionGuID"     =>  $GuIDRequisition,
		"Query"               =>  $main_search_query,
		"MainSearchWhere"     =>  implode ( " AND ", $where ),
		"SearchParams"        =>  json_encode ( $params ),
		"SortBy"              =>  $order_by,
		"SortByKey"           =>  $sort_by_key,
		"SortByType"          =>  $sort_type,
		"IndexStart"          =>  $Start,
		"RequisitionsLimit"   =>  $req_limit,
		"KeywordMatch"        =>  $keyword_match,
		"Keyword"             =>  $keyword,
		"LastUpdated"         =>  "NOW()" 
);
// Set SearchQuery Information
$on_update = " ON DUPLICATE KEY UPDATE Query = :UQuery, MainSearchWhere = :UMainSearchWhere, SearchParams = :USearchParams, RequisitionsLimit = :URequisitionsLimit, SortBy = :USortBy, SortByKey = :USortByKey, SortByType = :USortByType, IndexStart = :UIndexStart, KeywordMatch = :UKeywordMatch, Keyword = :UKeyword, LastUpdated = NOW()";
// Update Information
$update_info = array (
		":UQuery"             =>  $main_search_query,
		":UMainSearchWhere"   =>  implode ( " AND ", $where ),
		":USearchParams"      =>  json_encode ( $params ),
		":URequisitionsLimit" =>  $req_limit,
		":USortBy"            =>  $order_by,
		":USortByKey"         =>  $sort_by_key,
		":USortByType"        =>  $sort_type,
		":UIndexStart"        =>  $Start,
		":UKeyword"           =>  $keyword,
		":UKeywordMatch"      =>  $keyword_match,
);
// Insert Search Query
$RequisitionsObj->insRequisitionsSearchParams ( $insert_info, $on_update, $update_info );

$req_search_list = array ();

/**
 * @full details information
 */
if (is_array ( $req_search_results ['results'] )) {
	
	$r = 0;
	foreach ( $req_search_results ['results'] as $REQS ) {
		
	    // Set where condition
	    $where = array (
	        "OrgID = :OrgID",
	        "RequestID = :RequestID"
	    );
	    // Set parameters
	    $params = array (
	        ":OrgID" => $OrgID,
	        ":RequestID" => $REQS ['RequestID']
	    );
	    // Get Job Applications Information
	    $resultsCNT = $ApplicationsObj->getJobApplicationsInfo ( "COUNT(*) as cnt", $where, '', '', array (
	        $params
	    ) );
	    $ttcnt = $resultsCNT ['results'] [0] ['cnt'];
	    
		$req_search_list [] = array (
				"RequestID" => $REQS ['RequestID'],
				"MultiOrgID" => $REQS ['MultiOrgID'] 
		);
		
		$req_search_results ['results'] [$r] ['OrganizationName'] = "";
		if ($feature ['MultiOrg'] == "Y") {
			$req_search_results ['results'] [$r] ['OrganizationName'] = $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $REQS ['RequestID'] );
		}
		
		$req_search_results ['results'] [$r] ['PostedInternal'] = "";
		
		if ($Active == "N") {
			$req_search_results ['results'] [$r] ['Open'] = $REQS ['Closed'];
		}
		
		$req_search_results ['results'] [$r] ['ApplicantsCount'] = $ttcnt;
		
		if(is_array(json_decode($REQS ['FreeJobBoardLists']))) {
		    $req_search_results ['results'] [$r] ['FreeJobBoardLists'] = implode(", ", json_decode($REQS ['FreeJobBoardLists']));
		}
		else {
		    $req_search_results ['results'] [$r] ['FreeJobBoardLists'] = "";					    
		}
		
		if ($permit ['Reports'] == 1) {
			
			$reportlink = "reports.php?";
			$reportlink .= "action=ap";
			$reportlink .= "&OrgID=" . $OrgID;
			if($REQS ['MultiOrgID'] != "") $reportlink .= "&MultiOrgID=" . $REQS ['MultiOrgID'];
			$reportlink .= "&RequestID=" . $REQS['RequestID'];
			$reportlink .= "&menu=7";
			
			$req_search_results ['results'] [$r] ['ReportLink'] = $reportlink;
		}
		
		if ($permit ['Requisitions_Edit'] == 1) {
			
			$linkc = IRECRUIT_HOME . 'request/assign.php?OrgID=' . $OrgID . '&RequestID=' . $REQS ['RequestID'] . '&action=copy&Active=' . $Active . '&MultiOrgID=' . $REQS ['MultiOrgID'];
			
			$req_search_results ['results'] [$r] ['Linkc'] = $linkc;
		} else { // else action != Requisition_Edit
			$link = IRECRUIT_HOME . "requisitions/viewListing.php?RequestID=" . $REQS ['RequestID'];
			if ($AccessCode != "") {
				$link .= "&k=" . $AccessCode;
			}
			$req_search_results ['results'] [$r] ['ViewListing'] = $link;
		} // end else Requisition_Edit
		
		$r ++;
	} // end foreach
}

//If TemplateObj exists
if (is_object ( $TemplateObj )) { 
	$TemplateObj->total_requisitions_count = $total_requisitions_count;
	$TemplateObj->req_search_results       = $req_search_results; 
        $TemplateObj->req_search_results_list  = $req_search_results;
	$TemplateObj->cnt                      = $cnt;
	$TemplateObj->req_search_list          = $req_search_list;
	$TemplateObj->qi                       = $qi;
	$TemplateObj->Start                    = $Start;
	$TemplateObj->sort_type                = $sort_type;
}

// Print requisitions information if the request is by using ajax
if ($ServerInformationObj->getRequestSource () == 'ajax') {
	$left_nav_info = $PaginationObj->getPageNavigationInfo ( $Start, $LimitRequisitions, $total_requisitions_count, '', '' );
	 
                  $location = array();
                     if (is_array ( $req_search_results ['results'] )) {
					foreach ( $req_search_results ['results'] as $REQS ) { 
                        $multiorgid_req         =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $REQS ['RequestID']);
                        $RequisitionTitle       =   $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
						
						####################

                        // Set columns
                        $columns    =   "MAX(OrgLevelID) AS MaxOrgLevelID";
                        // Set condition and parameters
                        $where      =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
                        $params     =   array (":OrgID" => $OrgID, ":MultiOrgID" => $multiorgid_req);
                        
                        // Get Org Levels Information
                        $results    =   $OrganizationsObj->getOrganizationLevelsInfo ( $columns, $where, '', array ($params) );
                        $OrgLevels  =   $results ['results'] [0] ['MaxOrgLevelID'];
                        
						$i = 1;
						if ($OrgID == "I20141112") { // Cable & Wireless
						    $OrgLevels = 1;
						}
						
						while ( $i <= $OrgLevels ) {
						
						    // Set columns
						    $columns      =   "OLD.CategorySelection, OL.OrganizationLevel";
						    $table_name   =   "OrganizationLevels OL, RequisitionOrgLevels ROL, OrganizationLevelData OLD";
						
						    // Set condition
						    $where        =   array (
                    						        "OL.OrgID           =   ROL.OrgID",
                    						        "ROL.OrgID          =   OLD.OrgID",
                    						        "OL.MultiOrgID      =   OLD.MultiOrgID",
                    						        "OL.MultiOrgID      =   :MultiOrgID",
                    						        "ROL.OrgLevelID     =   OLD.OrgLevelID",
                    						        "ROL.OrgLevelID     =   OL.OrgLevelID",
                    						        "ROL.SelectionOrder =   OLD.SelectionOrder",
                    						        "ROL.OrgID          =   :OrgID",
                    						        "ROL.RequestID      =   :RequestID",
                    						        "ROL.OrgLevelID     =   :OrgLevelID"
                    						    );
						    // Set params
						    $params       =   array (
                    						        ":MultiOrgID"       =>  $multiorgid_req,
                    						        ":OrgID"            =>  $OrgID,
                    						        ":RequestID"        =>  $REQS ['RequestID'],
                    						        ":OrgLevelID"       =>  $i
                    						    );
						    // Get Org Requisition Levels Information
						    $resultsIN            =   $OrganizationsObj->getOrgAndReqLevelsInfo ( $table_name, $columns, $where, '', array ($params) );
						   
						    $SelectionTitle       =   "";
						    $SelectionResults     =   "";
						
						    if (is_array ( $resultsIN ['results'] )) {
						        foreach ( $resultsIN ['results'] as $Selection ) {
						            $location[ $REQS ['RequestID']][] = $Selection ['OrganizationLevel'].':'.$Selection ['CategorySelection']. '<br>';
						           // $SelectionResults .= $Selection ['CategorySelection'] . ", ";
						        } // end foreach
						    }
                          
						    /*if (($OrgID != "I20111019") && ($OrgID != "I20091101")) {
						        if (substr ( $SelectionResults, 0, - 2 ) != "") {
						            $location[ $REQS ['RequestID']]= $SelectionTitle . ': ' . substr ( $SelectionResults, 0, - 2 ) . '<br>';
						        }
						    } */// end if
						
						    $i ++;
						} // end OrgLevels
						
												
					}
				}


	echo json_encode ( 
                array (
                	'results'                        => $req_search_results ['results'],
                	'permit_Requisitions_Edit'       => $permit ['Requisitions_Edit'],
                	'permit_Reports'                 => $permit ['Reports'],
                	'permit_Admin'                   => $permit ['Admin'],
                	'feature_RequisitionRequest'     => $feature ['RequisitionRequest'],
                	'feature_InternalRequisitions'   => $feature ['InternalRequisitions'],
                	'cnt'                            => $cnt,
                	'previous'                       => $left_nav_info ['previous'],
                	'next'                           => $left_nav_info ['next'],
                	'total_pages'                    => $left_nav_info ['total_pages'],
                	'current_page'                   => $left_nav_info ['current_page'],
                	'requisitions_count'             => $total_requisitions_count,
                	'requisitions_search_list'       => json_encode ( $req_search_list ),
                        'tooltip' => $location
                  ) 
       );
}
?>
