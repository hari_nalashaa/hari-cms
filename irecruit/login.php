<?php
$PAGE_TYPE	= "Default";
$IRECRUIT_USER_AUTH	=	FALSE;
require_once 'Configuration.inc';

// Process login information
if (isset ( $_POST ['process'] ) == "Y") { 
	include 'process/LoginProcess.inc';
} // end process

if (isset($_GET['k'])) {
	include 'process/LoginProcess.inc';
}

//Set the query string to goto variable
$TemplateObj->goto  =   $goto   =   "";
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto	=   $goto   =   $_SERVER ['QUERY_STRING'];
}

// Set page title
$TemplateObj->title	=	"iRecruit Login";
    
echo $TemplateObj->displayIrecruitTemplate('views/Login');
?>
