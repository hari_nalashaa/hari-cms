<?php
header('Content-Type: text/html; charset=utf-8');
require_once 'irecruitdb.inc';

if (isset($_REQUEST['OrgID']) && isset($_REQUEST['Type'])) {
	
	// $OrgID="B12345467";
	// $MultiOrgID="xcxcvxv";
	// $Type="Secondary"; //or Primary
	
	//Set columns
	$columns = array('OrgID', 'PrimaryLogoType', 'PrimaryLogo', 'SecondaryLogoType', 'SecondaryLogo');
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$_REQUEST['OrgID'], ":MultiOrgID"=>(string)$_REQUEST['MultiOrgID']);
	//Get Organization Logos Information
	$results = $OrganizationsObj->getOrganizationLogosInformation($columns, $where, '', array($params));
	
	$img = $results['results'][0];
	
	$orgid = $img ['OrgID'];
	$type1 = $img ['PrimaryLogoType'];
	$data1 = $img ['PrimaryLogo'];
	$type2 = $img ['SecondaryLogoType'];
	$data2 = $img ['SecondaryLogo'];

	
	if (($type1) && ($_REQUEST['Type'] == "Primary")) {
		header ( "Content-type: $type1" );
		echo $data1;
		exit ();
	}
	
	if (($type2) && ($_REQUEST['Type'] == "Secondary")) {
		header ( "Content-type: $type2" );
		echo $data2;
		exit ();
	}

} else {
	
	echo <<<END
<html>
<head><title></title></head>
<body></body>
</html>
END;
}
?>
