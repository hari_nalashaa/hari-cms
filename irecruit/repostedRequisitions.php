<?php
require_once 'Configuration.inc';

$title = 'Reposted Requisitions';

$subtitle = array (
	"edit"     =>  "Edit",
	"copy"     =>  "Copy",
	"costs"    =>  "Costs",
	"new"      =>  "Create New" 
);

if (($subtitle [$_REQUEST['action']]) && (! $_REQUEST['reqaction'])) {
	$title .= ' - ' . $subtitle [$_REQUEST['action']];
}

if ($_REQUEST['reqaction']) {
	$title .= ' - ' . $subtitle [$_REQUEST['reqaction']];
}

//Set page title
$TemplateObj->title	=	$title;

include COMMON_DIR . 'listings/SocialMedia.inc';

if ($permit ['Requisitions'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

include COMMON_DIR . 'process/FormFunctions.inc';

$scripts_header[] = "js/loadAJAX.js";
$TemplateObj->page_scripts_header = $scripts_header;

$scripts_footer[] = "js/requisition.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

if ($_REQUEST ['updatereq'] != "") {

	$requestids    =   ''; // String to be used in where condition
	$key           =   $_SESSION['I'][$_GET['req_guid']] ['JB' . $OrgID] [0];
	$requestids    =   implode ( '","', $_SESSION['I'][$_GET['req_guid']] ['JB' . $OrgID] );
	$requestids    =   '"' . $requestids . '"';

	// PostDate & ExpiryDate
	$postdatetime	=   G::Obj('DateHelper')->getYmdFromMdy ( $_REQUEST ['PostDate'] [$key], "/", "-" );
	$expiredatetime	=   G::Obj('DateHelper')->getYmdFromMdy ( $_REQUEST ['ExpireDate'] [$key], "/", "-" );

	// Calculate PostDate Hours
	$phrs = $_REQUEST ['PostDateSIHrs'] [$key];
	if ($_REQUEST ['PostDateSIAmPm'] [$key] == "AM") {
		if ($phrs == 12) $phrs = "00";
	} else if ($_REQUEST ['PostDateSIAmPm'] [$key] == "PM") {
		$phrs += 12;
		if ($phrs == 24) $phrs -= 12;
	}

	// Calculate PostDate Minutes
	$pmins = $_REQUEST ['PostDateSIMins'] [$key];
	if ($_REQUEST ['PostDateSIMins'] [$key] == 0) {
		$pmins = "00";
	}
	$postdatetime .= " " . $phrs . ":" . $pmins . ":00";

	// Calculate ExpireDate Hours
	$ehrs = $_REQUEST ['ExpireDateSIHrs'] [$key];
	if ($_REQUEST ['ExpireDateSIAmPm'] [$key] == "AM") {
		if ($ehrs == 12)
			$ehrs = "00";
	} else if ($_REQUEST ['ExpireDateSIAmPm'] [$key] == "PM") {
		$ehrs += 12;
		if ($ehrs == 24)
			$ehrs -= 12;
	}

	// Calculate ExpireDate Minutes
	$emins = $_REQUEST ['ExpireDateSIMins'] [$key];
	if ($_REQUEST ['ExpireDateSIMins'] [$key] == 0) {
		$emins = "00";
	}
	$expiredatetime .= " " . $ehrs . ":" . $emins . ":00";

	// Set update information
	$set_info = array (
			"Active                  = 	'Y'",
			"JobID                   = 	:JobID",
			"PostDate                = 	:PostDate",
			"ExpireDate              = 	:ExpireDate"
	);
	// Set where condition
	$where = array (
			"RequestID IN($requestids)"
	);
	// Set parameters
	$params = array (
			":JobID" 				=>	$_REQUEST ['JobID'] [$key],
			":PostDate" 			=>	$postdatetime,
			":ExpireDate" 			=>	$expiredatetime
	);

	// Update Requisitions information
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array ( $params ) );

	header("Location:requisitionsSearch.php?menu=3");
	exit();
}


if ($_REQUEST ['ddlProcessRequisitions'] == 'Repost') {
	
	############################ Set Dates
	// Set where condition
	$where		=	array ("OrgID = :OrgID");
	// Set parameters
	$params		=	array (":OrgID"=>$OrgID);
	
	// Set columns
	$columns	=	"date_format(MIN(DATE(PostDate)), '%m/%d/%Y') as R1,
					date_format(MAX(DATE(PostDate)), '%m/%d/%Y') as R2,
					date_format(DATE_SUB(CURDATE(),INTERVAL 6 MONTH),'%m/%d/%Y') as R3,
					date_format(DATE_ADD(CURDATE(),INTERVAL 6 MONTH), '%m/%d/%Y') as R4,
					YEAR(MIN(DATE(PostDate))) as R5,
					YEAR(MAX(DATE(PostDate))) as R6";
	// Get Requisition Information
	$results	=	$RequisitionsObj->getRequisitionInformation ( $columns, $where, "", "", array ($params) );
	
	$row 		=	$results ['results'] [0];
	$StartDate 	=	$row ['R1'];
	$EndDate 	=	$row ['R2'];
	
	if ($StartDate == '00/00/0000' || $EndDate == '00/00/0000') {
		$StartDate = $row ['R3'];
		$EndDate = $row ['R4'];
	}
	
	$sde = explode ( "/", $StartDate );
	$ede = explode ( "/", $EndDate );
	
	if ($row ['R5'] == "0000")
		$row ['R5'] = "2000";
	if ($row ['R6'] == "0000")
		$row ['R5'] = date ( 'Y' );
	
	// Set Minimum, Maximum Dates
	if ($sde [0] == "00" || $sde [1] == "00" || $sde [2] == "0000")
		$StartDate = "01/01/" . $row ['R5'];
	if ($ede [0] == "00" || $ede [1] == "00" || $ede [2] == "0000")
		$EndDate = "12/31/" . $row ['R6'];
	
	// Get Time
	$rowtime = $MysqlHelperObj->getTime ();
	############################ 

	if (is_array ( $_REQUEST ['chk_req'] ) && (count ( $_REQUEST ['chk_req'] ) > 0)) {
		foreach ( $_REQUEST ['chk_req'] as $rkpid => $rpvalue ) {
			$RequestID = uniqid () . rand ( 1, 1 );
			$_SESSION['I'][$_GET['req_guid']]["JB" . $OrgID] [] = $RequestID;
		
			/**
			 * Insert New Requisition
			 */
			$rowreq		=	array ();
		
			// Set where condition
			$where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
			// Set parametes
			$params		=	array (":OrgID" => $OrgID, ":RequestID" => $rkpid);
			// Get Requisitions Information
			$results	=	$RequisitionsObj->getRequisitionInformation ( "*", $where, "", "", array ($params) );
		
			$rowreq		=	$results ['results'] [0];
		
			// Insert the same record with new RequisitionID and New Dates
            $rowreq ['RequestID']               =   $RequestID;
            $rowreq ['DateEntered']             =   "NOW()";
            $rowreq ['LastModified']            =   "NOW()";
            $rowreq ['PostDate']                =   $DateHelperObj->getYmdFromMdy ( $StartDate ) . " " . $rowtime;
            $rowreq ['ExpireDate']              =   $DateHelperObj->getYmdFromMdy ( $EndDate ) . " " . $rowtime;
            $rowreq ['ApplicantsCount']         =   0;
            $rowreq ['RequisitionStage']        =   "";
            $rowreq ['RequisitionStageReason']  =   "";
            $rowreq ['RequisitionStageDate']    =   "0000-00-00";
            
            //Get RequisitionFormID
            $TemplateObj->RequisitionFormID		=   $RequisitionFormID  =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);
            //Set Default RequisitionFormID
            $rowreq['RequisitionFormID']    	=   $RequisitionFormID;
			// Insert requisitions
			$RequisitionsObj->insRequisitionsInfo ( 'Requisitions', $rowreq );
		
			$columns		=	"OrgID, QuestionID, QuestionOrder, Question, QuestionTypeID, value";
			$form_que_list	=   G::Obj('RequisitionQuestions')->getRequisitionQuestionsByQueID($columns, $OrgID, $RequisitionFormID);
			
			$req_data_list	=	G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $rkpid);
			
			$insert_reqdata_info = array();
			if(is_array($req_data_list)) {
				foreach ($req_data_list as $req_que_id=>$req_answer) {
					$QI 	=	$form_que_list[$req_que_id];
					// Insert requisitions
					$insert_reqdata_info[] = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":QuestionID"=>$req_que_id, ":QuestionOrder"=>$QI["QuestionOrder"], ":Question"=>$QI["Question"], ":QuestionTypeID"=>$QI["QuestionTypeID"], ":Answer"=>$req_answer, ":AnswerStatus"=>1);
				}
				
				if(count($insert_reqdata_info)) {
					//Insert requisitions data
					G::Obj('RequisitionsData')->insRequisitionsData($insert_reqdata_info);
				}
			}
			
			$RequisitionsObj->insertInternalPrescreenFormsByRequestID ( $OrgID, $RequestID, $rkpid );
		
			/**
			 * Update Existing Requisition Status
			*/
			// Set information
			$set_info	=	array ("Active = 'N'");
			// Set where condition
			$where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
			// Set parameters
			$params		=	array (":OrgID" => $OrgID, ":RequestID"=>$rkpid);
			// Update requisitions information
			$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array($params) );
		
			// Set where condition
			$where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
			// Set the parameters
			$params		=	array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
			// Delete from RequisitionManagers table
			$RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where, array ($params) );
		
			// Set where condition
			$where		=	array ("OrgID = :OrgID", "RequestID = :RequestID");
			// set parameters
			$params		=	array (":OrgID" => $OrgID, ":RequestID" => $rkpid);
			// get requisition managers information
			$resreqmanagers = $RequisitionsObj->getRequisitionManagersInfo ( "UserID", $where, "", "", array ($params) );
		
			if (is_array ( $resreqmanagers ['results'] )) {
				foreach ( $resreqmanagers ['results'] as $val ) {
					// Requisition managers information
					$req_man_info = array (
							"OrgID" 	=> $OrgID,
							"RequestID" => $RequestID,
							"UserID" 	=> $val ['UserID']
					);
		
					if ($val ['UserID'] != "") {
						// Insert requisitions
						$RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
					}
				} // end foreach
			}
		
			// Set where condition
			$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
			// Set parameters
			$params = array (":OrgID" => $OrgID, ":RequestID" => $rkpid);
			// Get Requisition Org Levels Information
			$resreqorglevels = $RequisitionsObj->getRequisitionOrgLevelsInfo ( "*", $where, "", array ($params) );
		
			// Insert Requisition Org Levels Information
			if (is_array ( $resreqorglevels ['results'] )) {
				foreach ( $resreqorglevels ['results'] as $roworglevels ) {
		
					$req_org_level_info = array (
							"OrgID" 			=> $OrgID,
							"RequestID" 		=> $RequestID,
							"OrgLevelID" 		=> $roworglevels ['OrgLevelID'],
							"SelectionOrder" 	=> $roworglevels ['SelectionOrder']
					);
		
					// Insert requisitions
					$RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_level_info );
				}
			}
		
		}
	}
	

	header("Location:repostedRequisitions.php?req_guid=".$_REQUEST['req_guid']);
	exit;
}

//
if(is_array($_SESSION['I'][$_GET['req_guid']]["JB" . $OrgID])) {
	//Implode requestid's to string
	$fields = "'" . implode ( "','", $_SESSION['I'][$_GET['req_guid']]["JB" . $OrgID] ) . "'";
	// Set columns
	$columns = "RequestID, RequisitionID, JobID, date_format(PostDate,'%m/%d/%Y') PostDate,
			   DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate, Title,
			   DATEDIFF(NOW(),PostDate) Open, DATEDIFF(ExpireDate,PostDate) Closed,
			   DATE_FORMAT(DATE_ADD(now(), INTERVAL 0 DAY),'%m/%d/%Y') Current";
	// Set where condition
	$where = array ("RequestID IN($fields)");
	
	// Get requisition information
	$TemplateObj->requisition_results = $requisition_results = $RequisitionsObj->getRequisitionInformation ( $columns, $where,  "", "", array() );
	
	$TemplateObj->value = $value = $_SESSION['I'][$_GET['req_guid']] ['JB' . $OrgID] [0];
}

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/RepostedRequisitions');
?>