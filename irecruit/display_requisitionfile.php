<?php
require_once 'Configuration.inc';

if (($OrgID) && isset ( $_REQUEST ['requestid'] ) && isset ( $_REQUEST ['fileid'] )) {
	
	// Set where condition
	$where     =   array (
            			"RF.OrgID            =   FP.OrgID",
            			"RF.RequestID        =   RF.RequestID",
            			"FP.FilePurposeID    =   RF.FilePurposeID",
            			"RF.OrgID            =   :OrgID",
            			"RF.RequestID        =   :RequestID",
            			"RF.FileID           =   :FileID" 
                	);
	// Set parameters
	$params    =   array (
            			":OrgID"             =>  $OrgID,
            			":RequestID"         =>  $_REQUEST ['requestid'],
            			":FileID"            =>  $_REQUEST ['fileid'] 
                	);
	// Set columns
	$columns   =   "RF.FileID, RF.FileType, FP.PurposeTitle";
	// Get RequisitionFiles and FilePurpose
	$results   =   $RequisitionsObj->getRequisitionFilesAndFilePurpose ( $columns, $where, "", "", array ($params) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $formdoc ) {
			$fileid          =   $formdoc ['FileID'];
			$filetype        =   $formdoc ['FileType'];
			$purposetitle    =   $formdoc ['PurposeTitle'];
		}
	}
	
	$dfile = '';
	if (($fileid != "") && ($filetype != "")) {
		$dfile = IRECRUIT_DIR . 'vault/' . $OrgID . '/requisitionfiles/' . $_REQUEST ['requestid'] . '-' . $_REQUEST ['fileid'] . '.' . $filetype;
	}
	
	$filename = $purposetitle . "." . $filetype;
	$filename = preg_replace ( "/\s/i", '', $filename );
	
	if (file_exists ( $dfile )) {
		
		header ( 'Content-Description: File Transfer' );
		header ( 'Content-Type: application/octet-stream' );
		
		if ($purposetitle != '') {
			header ( 'Content-Disposition: attachment; filename=' . $filename );
		} else {
			header ( 'Content-Disposition: attachment; filename=' . basename ( $dfile ) );
		}
		
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		header ( 'Content-Length: ' . filesize ( $dfile ) );
		readfile ( $dfile );
		exit ();
	}
} // end if OrgID, and UpdateID
?>