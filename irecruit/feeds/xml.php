<?php
require_once "../irecruitdb.inc";

header('Content-type: text/xml');

$SOURCE =   $_REQUEST['source'];

$FEED   =   "";

if ($_REQUEST['mOrgID'] != "") {
    $org_info   =   G::Obj('OrganizationDetails')->getOrgIDMultiOrgIDByMultiOrgID($_REQUEST['mOrgID']);
    list ($OrgID, $MultiOrgID) = array_values($org_info);
    
    $FEED = "?OrgID=".$OrgID."&MultiOrgID=" . $MultiOrgID;

} else if (isset($_REQUEST['OrgID']) && isset($_REQUEST['MultiOrgID'])) {

        $org_information        =       $OrganizationDetailsObj->getOrganizationInformation($_REQUEST ['OrgID'], $_REQUEST ['MultiOrgID'], "OrgID, MultiOrgID");

        $OrgID                          =       $org_information['OrgID'];
        $MultiOrgID                     =       $org_information['MultiOrgID'];

    $FEED = "?OrgID=".$OrgID."&MultiOrgID=" . $MultiOrgID;

} else {
    $org_info = G::Obj('OrganizationDetails')->getOrganizationInformation($_REQUEST['OrgID'], "", "OrgID, MultiOrgID");
    list ($OrgID, $MultiOrgID) = array_values($org_info);
    
    $FEED = "?OrgID=" . $OrgID;
}

$FEED .= "&source=" . $SOURCE;

$TS_INFO    =   G::Obj('MysqlHelper')->getDatesList("DATE_FORMAT(NOW(), '%a, %d %b %Y %H:%i:%s EST') AS FormattedDate");
$TS         =   $TS_INFO['FormattedDate'];

$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

if (($OrgID != "") && ($SOURCE != "")) {
    $xml .= "<source>\n";
    $xml .= "<publisher>iRecruit ATS</publisher>\n";
    $xml .= "<publisherurl><![CDATA[";
    $xml .= IRECRUIT_HOME . "feeds/xml.php" . $FEED;
    $xml .= "]]></publisherurl>\n";
    $xml .= "<lastBuildDate>" . $TS . "</lastBuildDate>\n";
    $xml .= getLinks($OrgID, $MultiOrgID, $SOURCE);
    $xml .= "</source>\n";
    $xml .= "\n";
}

echo $xml;

function getLinks($OrgID, $MultiOrgID, $SOURCE)
{
    $HOME = "https://www.irecruit-us.com/";
    
    $query = "SELECT *,date_format(R.PostDate,'%a, %d %b %Y %H:%i:%s EST') PostDate";
    $query .= " FROM Requisitions R";
    $query .= " WHERE R.Active = 'Y'";
    $query .= " AND R.PostDate < NOW() AND R.ExpireDate > now()";
    $query .= " AND R.OrgID = :orgid";
    $query .= " AND R.MultiOrgID = :multiorgid";
    if ($_REQUEST['int'] == "Y") {
      $query .= " AND R.PresentOn IN ('INTERNALONLY','INTERNALANDPUBLIC')";
    } else {
      $query .= " AND R.PresentOn in ('PUBLICONLY','INTERNALANDPUBLIC')";
    }
    $query .= " GROUP BY R.OrgID, R.MultiOrgID, R.RequestID";
    $query .= " ORDER BY R.OrgID, R.Title";
    //$query .= " LIMIT 5";
    $params = array(':orgid' => $OrgID, ':multiorgid' => $MultiOrgID);
    $RESULTS = G::Obj('GenericQueries')->getInfoByQuery($query, array($params));
    
    $i = 0;
    $xml = "";
    
    foreach ($RESULTS as $REQ) {
        $i ++;
        
        // Get Default RequisitionFormID
        $STATUSLEVELS   =   array();
        $STATUSLEVELS   =   G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($OrgID, $REQ['RequisitionFormID']);
        $EmploymentStatusLevel = $STATUSLEVELS[$REQ['EmpStatusID']];
        
        $where_info =   array("OrgID = :OrgID", "Requisition = 'Y'", "RequisitionFormID = :RequisitionFormID");
        $params =   array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$REQ['RequisitionFormID']);
        $RequistionQuesInfo = G::Obj('RequisitionQuestions')->getRequisitionQuestions("*", $where_info, "QuestionOrder ASC", array($params));
        
        $RequistionFormQueAnsInfo = G::Obj('RequisitionFormQueAns')->getRequisitionFormQueAnswersInfo($REQ['OrgID'], $REQ['RequisitionFormID'], $REQ['RequestID']);

        $res_frm_ques       =   $RequistionQuesInfo['results'];
        $req_questions      =   array();
        for($r = 0; $r < count($res_frm_ques); $r++) {
            $req_questions[$res_frm_ques[$r]['QuestionID']]  =   $res_frm_ques[$r];
            if($REQ[$res_frm_ques[$r]['QuestionID']]==""){
                $REQ[$res_frm_ques[$r]['QuestionID']] = $RequistionFormQueAnsInfo[$res_frm_ques[$r]['QuestionID']]['Answer'];
            }
        }
        
        $columns        =   "OrganizationName, DemoAccount";
        $org_info       =   G::Obj('OrganizationDetails')->getOrganizationInformation($REQ['OrgID'], $REQ['MultiOrgID'], $columns);
        list ($OrganizationName, $DemoAccount) = array_values($org_info);

        // Required
        if (($REQ['Title']) 
            && ($REQ['PostDate']) 
            && ($REQ['RequestID']) 
            && ($REQ['Description']) 
            && ($DemoAccount != 'Y')) {
            
            $xml .= "<job>\n";
            //$xml .= "<requestid>".$REQ['RequestID']."</requestid>";
            
            $xml .= "   <url><![CDATA[";
            
            $xml .= $HOME . "jobRequest.php?";
            $xml .= "OrgID=" . $REQ['OrgID'];
            if ($REQ['MultiOrgID'] != "") {
                $xml .= "&MultiOrgID=" . $REQ['MultiOrgID'];
            }
            $xml .= "&RequestID=";
            $xml .= $REQ['RequestID'];
            
            $xml .= "&source=" . $SOURCE;
            
            $xml .= "]]></url>\n";
            
            if ($OrganizationName) {
                $xml .= "   <company><![CDATA[";
                //$xml .= preg_replace("/[^A-Z0-9a-z_ <>#\-()\/%-&]/i", '', $OrganizationName);
                $xml .= $OrganizationName;
                $xml .= "]]></company>\n";
            }
            
            if ($EmploymentStatusLevel) {
                $xml .= "   <jobtype><![CDATA[";
                $xml .= $EmploymentStatusLevel;
                $xml .= "]]></jobtype>\n";
            }
                    
	    $Managers = G::Obj('Requisitions')->getRequisitionManagersNames( $OrgID, $REQ['RequestID'] );

            if ($Managers) {
                $xml .= "   <requisition_managers><![CDATA[";
                $xml .= $Managers;
                $xml .= "]]></requisition_managers>\n";
            }
            
            $where          =   array ("OrgID = :OrgID", "RequestID = :RequestID");
            // Set parameters
            $params         =   array (":OrgID" => $OrgID, ":RequestID" => $REQ['RequestID']);
            // Get Requisition Org Levels Information
            $REQORGLEVELS   =   G::Obj('Requisitions')->getRequisitionOrgLevelsInfo ( "*", $where, "", array ($params) );
            
            $ORGL           =   array();
            foreach ($REQORGLEVELS['results'] AS $ROL) {
                $query = "SELECT OL.OrganizationLevel, OLD.CategorySelection";
                $query .= " FROM OrganizationLevelData OLD";
                $query .= " JOIN OrganizationLevels OL ON OLD.OrgID = OL.OrgID";
                $query .= " AND OLD.MultiOrgID = OL.MultiOrgID";
                $query .= " AND OLD.OrgLevelID = OL.OrgLevelID";
                $query .= " WHERE OL.OrgID = :orgid AND OL.MultiOrgID = :multiorgid";
                $query .= " AND OLD.OrgLevelID = :orglevelid AND OLD.SelectionOrder = :selectionorder";
                $query .= " ORDER BY OLD.OrgLevelID, OLD.SelectionOrder";

                $params     =   array(':orgid' => $OrgID, ':multiorgid' => $MultiOrgID, ':orglevelid' => $ROL['OrgLevelID'], ':selectionorder' => $ROL['SelectionOrder']);
                $RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));

                foreach ($RESULTS as $R) {
                   $OrgLevel = preg_replace ( '/\s/', '_', strtolower($R['OrganizationLevel']) );
                   $OrgLevel = preg_replace ( "/[^A-Za-z0-9\/|_]/", "", $OrgLevel);
                   $ORGL[$OrgLevel] .= $R['CategorySelection'] . ", ";

                }
            }
            
            foreach ($ORGL as $name=>$value) {
		if ($name != "") {
                  $xml .= "   <" . $name . "><![CDATA[";
                  $xml .= substr($value, 0,-2);
                  $xml .= "]]></" . $name . ">\n";
		}
            }
            
            foreach($req_questions AS $questionId => $req_question){
                  $reqQues = preg_replace('/[^A-Za-z0-9 ]/', '', strtolower($req_question['Question']) );
                  $reqQues = preg_replace('/\s/', '_', strtolower($reqQues) );
                  $reqAns = $REQ[$questionId];
		if ($reqQues != "requisition_managers") {
                  $xml .= "   <".$reqQues."><![CDATA[";
                  $xml .= $reqAns;
                  $xml .= "]]></".$reqQues.">\n";
		}
            }
            
            $xml .= "</job>\n";
                
        } // end if all required fields are populated
    } // end foreach
    
    return $xml;
} // end function
?>
