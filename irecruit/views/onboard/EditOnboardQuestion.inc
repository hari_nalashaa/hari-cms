<?php
// handle Question Detail updates
if ($process) {
    $cnt        =   $_POST['cnt'];
    $Question   =   $_POST['Question'];
    $Active     =   $_POST['Active'];
    $Required   =   $_POST['Required'];
        
    if(isset($_REQUEST['ddlPrefilledQueData']) && $_REQUEST['ddlPrefilledQueData'] != "") {
    
        if($_REQUEST['ddlPrefilledQueData'] ==  "States")
        {
            $address_states_results =   $AddressObj->getAddressStateList();
            $address_states_count   =   $address_states_results['count'];
            $address_states         =   $address_states_results['results'];
    
            $def                    =   '';
    
            $address_states_list    =   array();
            $address_states_list[]  =   "Select";
            $answers_info[]         =   "" . ":" . "Select";
            for($asl = 0; $asl < $address_states_count; $asl++) {
                $address_states_list[$address_states[$asl]['Abbr']] = $address_states[$asl]['Description'];
                if($address_states[$asl]['Description'] != "") {
                    $answers_info[] =  $address_states[$asl]['Abbr'] . ':' . $address_states[$asl]['Description'];
                }
            }
	} 
	else if($_REQUEST['ddlPrefilledQueData'] ==  "Countries")
        {
            $address_countries_results =   $AddressObj->getCountries();
            $address_countries_count   =   $address_countries_results['count'];
            $address_countries         =   $address_countries_results['results'];
    
            $def                    =   '';
    
            $address_countries_list    =   array();
            $address_countries_list[]  =   "Select";
            $answers_info[]         =   "" . ":" . "Select";
            for($asl = 0; $asl < $address_countries_count; $asl++) {
                $address_countries_list[$address_countries[$asl]['Abbr']] = $address_countries[$asl]['Description'];
                if($address_countries[$asl]['Description'] != "") {
                    $answers_info[] =  $address_countries[$asl]['Abbr'] . ':' . $address_countries[$asl]['Description'];
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] ==  "Days")
        {
            $days_list              =   array("SUN"=>"Sunday", "MON"=>"Monday", "TUE"=>"Tuesday", "WED"=>"Wednesday", "THU"=>"Thursday", "FRI"=>"Friday", "SAT"=>"Saturday");
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($days_list as $day_key=>$day_value) {
                if($day_value != "") {
                    $answers_info[] =   $day_key . ':' . $day_value;
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] ==  "Months")
        {
            $months_list            =   array(
                "JAN"   =>  "January",
                "FEB"   =>  "February",
                "MAR"   =>  "March",
                "APR"   =>  "April",
                "MAY"   =>  "May",
                "JUN"   =>  "June",
                "JUL"   =>  "July",
                "AUG"   =>  "August",
                "SEP"   =>  "September",
                "OCT"   =>  "October",
                "NOV"   =>  "November",
                "DEC"   =>  "December"
            );
    
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($months_list as $month_key=>$month_value) {
                if($month_value != "") {
                    $answers_info[] =   $month_key . ':' . $month_value;
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] == 'SocialMedia') {
            $social_media   =   array(
                "Facebook"  => "Facebook",
                "Twitter"   => "Twitter",
                "Linkedin"  => "Linkedin",
                "Google+"   => "Google+",
                "YouTube"   => "YouTube",
                "Pintrest"  => "Pintrest",
                "Instagram" => "Instagram",
                "Digg"	    => "Digg"
            );
    
    
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($social_media as $socialmedia_key=>$socialmedia_value) {
                if($socialmedia_value != "") {
                    $answers_info[] =   $socialmedia_key . ':' . $socialmedia_value;
                }
            }
        }
    
        $answer = implode("::", $answers_info);
    }
    else {
        $def    =   '';
        $i      =   0;
        $ii     =   0;
        
        for($i; $i < $cnt; $cnt) {
            $i ++;
            $v  =   'value-' . $i;
            $d  =   'display-' . $i;
            $de =   'default-' . $i;
        
            if ($_POST [$d] != '') {
                
                if ($default == $de) {
                    $def = $_POST [$v];
                }
                
                $answer .= $_POST [$v] . ':' . $_POST [$d] . '::';
                $ii ++;
                
                $data [] = array (
                    'value' =>  $_POST [$v],
                    'name'  =>  $_POST [$d]
                );
            }
        }
        
        if ($_POST ['AttachmentName']) {
            if ($_FILES ['AttachmentFile'] ['type'] != "") {
                	
                $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
                	
                if (! file_exists ( $dir )) {
                    mkdir ( $dir, 0700 );
                    chmod ( $dir, 0777 );
                }
                	
                $apatdir = $dir . '/formattachments';
                	
                if (! file_exists ( $apatdir )) {
                    mkdir ( $apatdir, 0700 );
                    chmod ( $apatdir, 0777 );
                }
                	
                $data   =   file_get_contents ( $_FILES ['AttachmentFile'] ['tmp_name'] );
                $e      =   explode ( '.', $_FILES ['AttachmentFile'] ['name'] );
                $ecnt   =   count ( $e ) - 1;
                $ext    =   $e [$ecnt];
                $ext    =   preg_replace ( "/\s/i", '', $ext );
                $ext    =   substr ( $ext, 0, 5 );
                	
                $answer =   $_POST ['AttachmentName'] . "::" . $ext;
                	
                $filename = $apatdir . '/' . $FormID . '-' . $QuestionID . '.' . $ext;
                $fh = fopen ( $filename, 'w' );
                fwrite ( $fh, $data );
                fclose ( $fh );
                	
                chmod ( $filename, 0644 );
            } // end if FILES
        } // end if AttachmentName
        
        if ($sort == 'Y') {
        
            if($_REQUEST ['QuestionTypeID'] != "100") {
                $answer = "";
                	
                foreach ( $data as $key => $row ) {
                    if($row ['name'] == "Select") $row ['name'] = "1Select";
                    $value [$key] = $row ['value'];
                    $name [$key] = $row ['name'];
                }
                array_multisort ( $name, SORT_ASC, $data );
                	
                foreach ( $data as $key => $row ) {
                    if($row ['name'] == "1Select")  $row ['name'] = "Select";
                    $answer .= $row ['value'] . ':' . $row ['name'] . '::';
                    $ii ++;
                }
            }
        
        }
        
        if ($ii > 0) {
            $answer =   substr ( "$answer", 0, - 2 );
        }
        
        if ($Clear == 'Y') {
            $def    =   '';
        }
        
        if (($questionid == "Sage_HireDate") || ($questionid == "Sage_PayPeriodHRS") || ($questionid == "Sage_Rate")) {
            $def    =   $default;
        } // end questionid
        
        if ($questionid == "COB_WCgroup") {
            $def    =   $default;
        } // end questionid
        
        $params     =   array ();
        $set_info   =   array ();
        
        if ($_REQUEST ['QuestionTypeID'] == "100") {
            $question_answers ['RVal'] = $_REQUEST ['RVal'];
            $question_answers ['LabelValRow'] = $_REQUEST ['LabelValRow'];
        
            if ($sort == 'Y') {
                //Sort based on alphabetical order
                asort($question_answers ['LabelValRow']);
        
                if(is_array($question_answers ['LabelValRow'])) {
                    $q100_i = 1;
                    foreach($question_answers ['LabelValRow'] as $lable_key_name=>$label_val_name) {
                        $question_answers ['LabelValRow']["LabelValRow".$q100_i] = $label_val_name;
        
                        $q100_i++;
                    }
                }
            }
        
            if (is_array ( $question_answers )) {
                $answer = serialize ( $question_answers );
            }
        
            if ($_REQUEST ['rows'] != "" && $_REQUEST ['cols'] != "") {
                $params [':rows'] = $_REQUEST ['rows'];
                $params [':cols'] = $_REQUEST ['cols'];
                $set_info [] = "rows = :rows";
                $set_info [] = "cols = :cols";
            }
        }
        
        if ($_REQUEST ['QuestionTypeID'] == "120") {
            $question_answers ['day_names'] = $_REQUEST ['day_names'];
            if (is_array ( $question_answers )) {
                $answer = serialize ( $question_answers );
            }
        
            if ($_REQUEST ['number_of_days'] != "") {
                $params [':rows'] = $_REQUEST ['number_of_days'];
                $set_info [] = "rows = :rows";
            }
        }
    } 
		
	//Bind Parameters
	$params[':OrgID']          =   $OrgID;
	$params[':OnboardFormID']  =   $form;
	$params[':QuestionID']     =   $questionid;
	$params[':Question']       =   $Question;
	$params[':value']          =   $answer;
	$params[':defaultValue']   =   $def;
	$params[':Active']         =   $Active;
	$params[':Required']       =   $Required;
	
	//set where condition
	$where_info = array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
	
	//Set the parameters those are going to update
	$set_info[]                =   "Question       =   :Question";
	$set_info[]                =   "value          =   :value";
	$set_info[]                =   "defaultValue   =   :defaultValue";
	$set_info[]                =   "Active         =   :Active";
	$set_info[]                =   "Required       =   :Required";

	//Update the table information based on bind and set values
	$res_que_info              =   $FormQuestionsObj->updQuestionsInfo ( $formtable, $set_info, $where_info, array ($params) );
	
	$QuestionIDOrig            =   $QuestionID; // preserve original $QuestionID
	$QuestionID                =   $QuestionIDOrig; // preserve original $QuestionID
	
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Update completed!')";
	echo '</script>';
}


if(isset($_REQUEST['date_field']) && $_REQUEST['date_field'] != "") {
	//Set information
	$set_info		=	array("Validate = :Validate");
	//Where information
	$where_info		=	array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
	//Set parameters
	$params			=	array(":OrgID"=>$OrgID, ":OnboardFormID"=>$form, ":QuestionID"=>$questionid, ":Validate"=>json_encode($_POST['date_field']));
	//Update the table information based on bind and set values
	$res_que_info	=   $FormQuestionsObj->updQuestionsInfo ( "OnboardQuestions", $set_info, $where_info, array ($params) );
}

// Display Change form
if (isset($_REQUEST['questionid']) || isset($_REQUEST['process'])) {
	?>
	<form method="post" action="<?php echo $formscript;?>"
		enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
		<div class="table-responsive">
			<table
				class="formtable table table-striped table-bordered table-hover"
				border="0" cellspacing="3" cellpadding="5" width="100%">
				<tr>
					<td><b>Edit Question</b></td>
				<tr>
				<?php
				if($QuestionID != "cloud_PayCode"
				    && $QuestionID != "cloud_EmployeeType"
				    && $QuestionID != "cloud_Position"
				    && $QuestionID != "cloud_Status"
				    && $QuestionID != "cloud_WorkSite"
				    && $QuestionID != "cloud_Division"
				    && $QuestionID != "cloud_Department"
				    && $QuestionID != "cloud_PayGroup"
				    && $QuestionID != "cloud_WorkShift"
                    && $QuestionID != "matrixcare_OfficeID") {

                        if($_REQUEST['QuestionTypeID'] == '3') {
                            ?>
                            <tr>
                                <td>
                                    <strong>Note*:</strong> If you want to prefill the data with the existing list of options available, please select any option in below pull down.
                                    <br>
                                    Prefill with existing data:
                                    <select name="ddlPrefilledQueData" id="ddlPrefilledQueData">
                                        <option value="">Select</option>
                                        <option value="States">States</option>
                                        <option value="Countries">Countries</option>
                                        <option value="Days">Days</option>
                                        <option value="Months">Months</option>
                                        <option value="SocialMedia">SocialMedia</option>
                                    </select>
                                    <br><br>
                                </td>
                            <tr>
                            <?php
                        }
                
                }
                ?>
				
				<tr>
					<td>
					<?php include IRECRUIT_VIEWS . 'onboard/OnboardQuestionView.inc';?>
					</td>
				</tr>

				<tr>
					<td align="center" height="60" valign="middle">
						<input type="hidden" name="questionid" value="<?php echo htmlspecialchars($QuestionID); ?>" /> 
						<input type="hidden" name="form" value="<?php echo htmlspecialchars($form);?>" /> 
						<input type="hidden" name="section" value="<?php echo htmlspecialchars($section); ?>" /> 
						<input type="hidden" name="action" value="<?php echo htmlspecialchars($action); ?>" />
						<input type="hidden" name="QuestionTypeID" id="QuestionTypeID" value="<?php echo htmlspecialchars($_REQUEST['QuestionTypeID']); ?>" />
                        <input type="hidden" name="OnboardSource" value="<?php echo htmlspecialchars($_REQUEST['OnboardSource']);?>">
						
						<?php
						if($QuestionID != "cloud_PayCode"
                            && $QuestionID != "cloud_EmployeeType"
                            && $QuestionID != "cloud_Position"
                            && $QuestionID != "cloud_Status"
                            && $QuestionID != "cloud_WorkSite"
                            && $QuestionID != "cloud_Division"
                            && $QuestionID != "cloud_Department"
                            && $QuestionID != "cloud_PayGroup"
                            && $QuestionID != "cloud_WorkShift"
                            && $QuestionID != "matrixcare_OfficeID") {

                            echo '<input type="submit" name="process" value="Update" class="btn btn-primary" />&nbsp;';
                            
                            if ($process) {
                                echo '<input type="submit" name="finish" value="Finish" class="btn btn-primary"/>';
                            } else {
                                echo '<input type="submit" name="finish" value="Cancel" class="btn btn-primary"/>';
                            }
                        }
						?>
					</td>
				</tr>
				
				
				</form>
	

<?php
// display preview
if ($Active == 'Y') {
	
	echo '<tr><td colspan="100%"><hr size="1"/>';
	echo '<p><b>Preview</b></p>';
	$colwidth = "220";
	
	if($QuestionID == "matrixcare_OfficeID") {
        $office_ids_info = $MatrixCareObj->getMatrixCareOfficeIds($OrgID, "");
        $branches_info = json_decode($office_ids_info['BranchesInfo'], true);
        
        if(is_array($branches_info)) {
            echo '<select class="form-control width-auto-inline">';
            foreach ($branches_info as $branch_id=>$branch_name)
            {
                echo '<option value="'.$branch_id.'">'.$branch_name.'</option>';
            }
            echo '</select>';
        }
                    
    }
	else {
        $SectionID = "";
        $FormID = $form;
        echo include COMMON_DIR . 'application/DisplayQuestions.inc';
	}
	
	
	echo '</td></tr>';
}
// this php block prevents the questions from listing when editing details
}
?>
</table>
</div>
