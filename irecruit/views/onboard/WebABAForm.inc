<div class="col-lg-12 col-md-12 col-sm-12">
    <h4>WebABA</h4>
    
    <form method="post" action="applicants.php" name="appedit" id="appedit_onboard">
        <?php
        //Get Onboard Questions List based on FormID
        $onboard_questions  =   $OnboardQuestionsObj->getOnboardQuestionsList($OrgID, $_REQUEST['OnboardSource']);
        
        if (count($onboard_questions) > 0) {
        	
            echo '<div class="row">';
            echo '<div class="col-lg-12 col-md-12 col-sm-12">';
            echo '<div style="background-color:#F5F5F5;padding:6px 0px 6px 2px;border:1px solid #ddd;font-weight:bold">';
            echo 'Onboard Fields';
            echo '</div><br>';
            echo '</div>';
            echo '</div>';
        	?>
        	
        	<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
            	<?php
            	if (is_array ( $onboard_questions )) {
            		foreach ( $onboard_questions as $OnboardQuestionID=>$OQ ) {
                        
                        if($OQ['Active'] == 'Y') {
                			$QuestionID  =   $OnboardQuestionID;
                			$FormID      =   $_REQUEST['OnboardSource'];
                			$formtable   =   "OnboardQuestions";
                			$section     =   1;
                			
            				echo include COMMON_DIR . 'application/DisplayQuestions.inc';
        				}
        				else {
        				
        				    $QuestionID  =   $OnboardQuestionID;
        				
        				    $FormID      =   $_REQUEST['OnboardSource'];
        				    $formtable   =   "OnboardQuestions";
        				    $section     =   1;
        				     
        				    echo include COMMON_DIR . 'application/HiddenQuestions.inc';
        				}
        				
            		} // end foreach
            	}
            	?>
        		</div>
        	</div>
        	<?php
        } // end OnboardQuestions count
    
        echo '<div class="row">';
        echo '<div class="col-lg-12 col-md-12 col-sm-12">';
        echo '<br>';
        
	if ($APPDATA['Status'] == 'downloaded') {
        	echo 'This applicant has already been downloaded. Do you want to reset it to download again?';
        	echo '&nbsp;&nbsp;&nbsp;';
        } else {
        	echo 'Do you want to onboard this applicant?';
        	echo '&nbsp;&nbsp;&nbsp;';
        }
        ?>
		<select name="onboard">
			<option value="No" <?php if ($onboard == 'No') { echo ' selected'; } ?>>No</option>
			<option value="Yes" <?php if ($onboard == 'Yes') { echo ' selected'; } ?>>Yes</option>
		</select>
    		<br><br>
    		</div>
    	</div>
    
    	<div class="row">
    	   <div class="col-lg-12 col-md-12 col-sm-12">
            <input type="button" onclick="getOnboardApplicant('process', '<?php echo $_REQUEST['OnboardSource'];?>')" value="Confirm Applicant Onboard Information" class="btn btn-primary">
           </div>            
    	</div>
    
    	<input type="hidden" name="ApplicationID" value="<?php echo $ApplicationID; ?>"> 
    	<input type="hidden" name="RequestID" value="<?php echo $RequestID; ?>"> 
    	<input type="hidden" name="action" value="onboardapplicant"> 
    	<input type="hidden" name="process" id="process" value="Y"> 
    	<input type="hidden" name="process_onboard" id="process_onboard" value="YES">
    	
    </form>

</div>
