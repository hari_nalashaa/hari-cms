<?php
//Get Onboard Questions List based on FormID
$onboard_questions  =   $OnboardQuestionsObj->getOnboardQuestionsList($OrgID, $_REQUEST['OnboardSource']);

$onboard_active_ques    =   array();
foreach($onboard_questions as $onboard_que_id=>$onboard_que_info) {
    if($onboard_que_info['Active'] == "Y") {
        $onboard_active_ques[]  =   $onboard_que_id;
    }
}

$calc_ques_check    =   array("Sage_PayFreq", "Sage_Rate", "Sage_RatePer", "Sage_PayPeriodHRS");

$intersect_ques_cnt =   count(array_intersect($calc_ques_check, $onboard_active_ques));

if($feature['DataManagerType'] != 'XML'
    && $intersect_ques_cnt == 4) { 
        ?>
        <style>
        .salary_calculations {
            position:fixed;
            right:10%;
            top:65%;
            border:1px solid grey;
            z-index:1000;
            width:300px;
            float:right;
            background-color:#DDDDDD;
            padding:5px;
            color:black;
            border-radius: 15px;
        }
        </style>
        
        <div class="salary_calculations">
            <h4>Sage Salary Calculations:</h4><br>
            <div id="salary_calculations">
            </div>
            <br>
            <strong>Note:* </strong> Confirm onboard button will be enabled only when these calculated values are greater than zero.
        </div>
        <?php 
} 
?>

<div class="col-lg-12 col-md-12 col-sm-12">
    <h4>
    <?php
    if($feature['DataManagerType'] == 'XML') {
        echo 'XML';
    }
    else if($feature['DataManagerType'] == 'Exporter') {
        echo 'Sage SQL';
    }
    else if($feature['DataManagerType'] == 'HRMS') {
        echo 'Sage HRMS';
    }
    ?>
    </h4>

    <form method="post" action="applicants.php" name="appedit" id="appedit_onboard">
        <?php
        //Content will be loaded dynamically
        echo '<div class="row">';
        echo '<div class="col-lg-12 col-md-12" id="verify_onboard_info">';
        include IRECRUIT_DIR . 'onboard/ValidateDataManagerForm.inc';
        echo '</div>';
        echo '</div>';

        echo '<div class="row">';
        echo '<div class="col-lg-12 col-md-12">';

        if ($i > 0) {
            echo 'Onboard Check: <a href=\'javascript:void(0);\' onclick=\'verifyOnboard("'.$OrgID.'", "'.$RequestID.'", "'.$ApplicationID.'")\'>';
            if(defined('IRECRUIT_HOME')) {
                echo '<img src="' . IRECRUIT_HOME . 'images/icons/bug.png" border="0" width="18" title="Verify" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Check/Verify Fields</b></a>';
                echo '&nbsp;(' . htmlspecialchars($i) . ')';
            }
        } else {
            echo 'Onboard Check: <font style="color:red">All fields for this applicant are ok.</font>';
        } //end i
        
        echo '<br><br>';
        echo '</div>';
        echo '</div>';

	if ($OrgID == 'I20201101') { // Polly Management
            unset($APPDATA['social']);
            unset($APPDATA['dob']);
            unset($APPDATA['Sage_Rate']);
        } // end Polly Management
        
        if (($OrgID == "I20140912") && (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager')) { // Community Research Foundation
        	$feature ['DataManagerType'] = "NoDisplay";
        }
    
        if ($feature ['DataManagerType'] == "XML") {
        	
        	// set values from form or i9 if available^M
        	if ($APPDATA ['dob'] != "") {
        		$dob = $APPDATA ['dob'];
        	}
        	
        	if (($APPDATA ['social1'] != "") && ($APPDATA ['social2'] != "") && ($APPDATA ['social3'] != "")) {
        		$social1 = $APPDATA ['social1'];
        		$social2 = $APPDATA ['social2'];
        		$social3 = $APPDATA ['social3'];
        	}
        	
        	if ($feature ['PreFilledForms'] == "Y") {
        		
        	    // Community Research Foundation
        	    if ($OrgID == "I20140912") {
        	    
        	        // Set where condition
        	        $where = array (
                                "OrgID           =   :OrgID",
                                "WebFormID       =   '5ba53223e8813'",
                                "ApplicationID   =   :ApplicationID",
                                "RequestID       =   :RequestID",
                                "QuestionID IN ('CUST5422dc29bda5d','CUST5422dc2cd85951','CUST5422dc2cd85952','CUST5422dc2cd85953')"
                                );
        	        // Set parameters
        	        $params = array (
                                ":OrgID"            =>  $OrgID,
                                ":ApplicationID"    =>  $ApplicationID,
                                ":RequestID"        =>  $RequestID
                	           );
        	        // Get WebFormData Object
        	        $resultsCRF = $FormDataObj->getWebFormData ( "QuestionID, Answer", $where, "", "", array ($params) );
        	    
        	        if (is_array ( $resultsCRF ['results'] )) {
        	            foreach ( $resultsCRF ['results'] as $CHECK ) {
        	                if ($CHECK ['QuestionID'] == "CUST5422dc29bda5d" && $CHECK ['Answer'] != "") {
        	                    $APPDATA[$CHECK ['QuestionID']]    =   $CHECK ['Answer'];
        	                    $APPDATA['social1']                =   $CHECK ['Answer']; //For static question social1
        	                }
        	                if ($CHECK ['QuestionID'] == "CUST5422dc2cd85951" && $CHECK ['Answer'] != "") {
        	                    $APPDATA[$CHECK ['QuestionID']]    =   $CHECK ['Answer'];
        	                    $APPDATA['social2']                =   $CHECK ['Answer']; //For static question social2
        	                }
        	                if ($CHECK ['QuestionID'] == "CUST5422dc2cd85952" && $CHECK ['Answer'] != "") {
        	                    $APPDATA[$CHECK ['QuestionID']]    =   $CHECK ['Answer'];
        	                    $APPDATA['social3']                =   $CHECK ['Answer']; //For static question social3
        	                }
        	                if ($CHECK ['QuestionID'] == "CUST5422dc2cd85953" && $CHECK ['Answer'] != "") {
        	                    $APPDATA[$CHECK ['QuestionID']]    =   $CHECK ['Answer'];
        	                    $APPDATA['dob']                    =   $CHECK ['Answer']; //For dob question
        	                }
        	            }
        	        }
        	         
        	    }
        	    else {

					$CURRENT	=	G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);

        	        // set where condition
        	        $where = array (
        	            "OrgID              = :OrgID",
        	            "ApplicationID      = :ApplicationID",
        	            "RequestID          = :RequestID",
        	            "PreFilledFormID    = :PreFilledFormID",
        	            "QuestionID IN ('DOB','SSN')"
        	        );
        	        // set parameters
        	        $params = array (
        	            ":OrgID"            => $OrgID,
        	            ":ApplicationID"    => $ApplicationID,
        	            ":RequestID"        => $RequestID,
        	            ":PreFilledFormID"  => $CURRENT['I9']
        	        );
        	        // Get Prefilled form data information
        	        $resultsIN = $FormDataObj->getPreFilledFormData ( "*", $where, "", "", array ($params) );
        	        
        	        if (is_array ( $resultsIN ['results'] )) {
        	            foreach ( $resultsIN ['results'] as $PFFD ) {
        	                if (($dob == "") && ($PFFD ['QuestionID'] == "DOB") && $PFFD ['Answer'] != "") {
        	                    $APPDATA[strtolower($PFFD ['QuestionID'])] = $PFFD ['Answer'];
        	                }
        	                
        	                if(($social1 == "" 
        	                   || $social2 == "" 
        	                   || $social3 == "") 
        	                   && ($PFFD ['QuestionID'] == "SSN") 
        	                   && $PFFD ['Answer'] != "")  {

        	                    $APPDATA[$PFFD ['QuestionID']] = $PFFD ['Answer'];
        	                    
        	                    $Answer15  =   json_decode($PFFD ['Answer'], true);
        	                    
        	                    $APPDATA['social1'] = $Answer15[0]; //For static question social1
        	                    $APPDATA['social2'] = $Answer15[1]; //For static question social1        	                            	      
        	                    $APPDATA['social3'] = $Answer15[2]; //For static question social1
        	                    
        	                }
        	                
        	            } // end foreach
        	        }
        	    }
        	}
        } // end Feature DataManagerType = XML

	$AJAX_QUESTIONS = array();

        $formtable = "OnboardQuestions";
        $active = " AND Active = 'Y'";
        $params = array(":QuestionID"=>'Sage_OrgID', ":OrgID"=>$OrgID);
        $params[":OnboardFormID"] = $feature['DataManagerType'];
        $refine = " AND OnboardFormID = :OnboardFormID";
        $row = G::Obj('FormQuestions')->getQuestionInformation(array($params), $formtable, $refine, $active);
        $COMPANYCHECK = explode('::',$row['value']);

        if (count($COMPANYCHECK) > 1) {
           $AJAX_QUESTIONS = array('Sage_JobCode', 'Sage_OrgID', 'Sage_OrgLevel1', 'Sage_OrgLevel2', 'Sage_OrgLevel3', 'Sage_OrgLevel4', 'Sage_OrgLevel4', 'Sage_OrgLevel5', 'Sage_Supervisor', 'Sage_Supervisor2');
	}
    
        if (count($onboard_questions) > 0) {
        	
            echo '<div class="row">';
            echo '<div class="col-lg-12 col-md-12 col-sm-12">';
            echo '<div style="background-color:#F5F5F5;padding:6px 0px 6px 2px;border:1px solid #ddd;font-weight:bold">';
            echo 'Onboard Fields';
            echo '</div><br>';
            echo '</div>';
            echo '</div>';
        	?>
        	
        	<div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8">
            	<?php
            	if (is_array ( $onboard_questions )) {
            		foreach ( $onboard_questions as $OnboardQuestionID=>$OQ ) {

                        if($OQ['Active'] == 'Y') {
                            $QuestionID  =   $OnboardQuestionID;
                            $FormID      =   $_REQUEST['OnboardSource'];
                            $formtable   =   "OnboardQuestions";
                            $section     =   1;

			    if (in_array($QuestionID,$AJAX_QUESTIONS)) {

$formtable = "OnboardQuestions";
$active = " AND Active = 'Y'";
$params = array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID);
$params[":OnboardFormID"] = $feature['DataManagerType'];
$refine = " AND OnboardFormID = :OnboardFormID";
$thisrow = G::Obj('FormQuestions')->getQuestionInformation(array($params), $formtable, $refine, $active);

$thisrow['ApplicationID']   =   $ApplicationID;
$thisrow ['RequestID']  =   $RequestID;
$thisrow ['FormTable']  =   $formtable;
$thisrow ['name']   =   trim ( $thisrow ['QuestionID'] );
$thisrow ['lbl_class']  =   ' class="question_name"';

// set flag for required that is missing
$errck = array ('from', 'to', '1', '2', '3', '');
$requiredck = '';

for($errcnt = 0; $errcnt < count ( $errck ); $errcnt ++) {
        if ($APPDATAREQ [$QuestionID . $errck [$errcnt]] == "REQUIRED") {
                $requiredck = ' style="background-color:' . $highlight . ';"';
        }
}

if ($APPDATAREQ [trim($QuestionID)] == "REQUIRED") {
        $requiredck = ' style="background-color:' . $highlight . ';"';
}

//This is for requisition custom questions validation
if(isset($error_inputs)
    && is_array($error_inputs)
    && in_array($QuestionID, $error_inputs)) {
        $requiredck = ' style="background-color:' . $bg_color . ';padding-bottom:10px;"';
}

$thisrow ['requiredck'] =   $requiredck;

G::Obj('ApplicationForm')->APPDATA   =   $APPDATA;

if ($QuestionID == "Sage_OrgID") {
echo G::Obj('ApplicationForm')->getQuestionView3($thisrow);
} else {
$values         =   explode ( "::", $thisrow['value']);
$newvalues = "";
foreach ( $values as $v => $q ) {

	 list ( $vv, $qq ) = explode ( ":", $q, 2 );
	 if((preg_match("/{$APPDATA['Sage_OrgID']}/i", $qq)) || (preg_match("/\*\*\*/i", $qq))) {
	    $newvalues .= $q . "::";
	 }
}
$thisrow['value'] = substr($newvalues,0,-2);
echo G::Obj('ApplicationForm')->getQuestionView3($thisrow);
} // end Sage_OrgID




			    } else { 
                                echo include COMMON_DIR . 'application/DisplayQuestions.inc';
			    }
                        }
                        else {
                        
                            $QuestionID  =   $OnboardQuestionID;
                        
                            $FormID      =   $_REQUEST['OnboardSource'];
                            $formtable   =   "OnboardQuestions";
                            $section     =   1;
                            	
                            echo include COMMON_DIR . 'application/HiddenQuestions.inc';
                        }

            		} // end foreach
            	}
            	?>
        		</div>
        	</div>
        	<?php
        } // end OnboardQuestions count
    
        if ($feature ['DataManagerType'] == "XML") {
        	?>
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
        		Employee Number: 
        		<input type="text" name="Sage_EmployeeNumber" value="<?php echo $APPDATA['Sage_EmployeeNumber']; ?>" size="20" maxlength="25">
                &nbsp;&nbsp;<?php if ($APPDATA['Sage_Override'] == "Y") { $checked = ' checked'; } else { $checked = ''; } ?>
                &nbsp;&nbsp;&nbsp;
                <input type="checkbox" name="Sage_Override" value="Y" <?php echo $checked; ?>>Override my system with this number.
        		</div>
            </div>        		
        	<?php
        } // end Feature DataManagerType = XML
        ?>

    	<div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <br>
                <?php
                if ($APPDATA['Status'] == 'downloaded') {
                	echo 'This applicant has already been downloaded. Do you want to reset it to download again?';
                	echo '&nbsp;&nbsp;&nbsp;';
                } else {
                	echo 'Do you want to onboard this applicant?';
                	echo '&nbsp;&nbsp;&nbsp;';
                }
                ?>
            	<select name="onboard">
                    <option value="No" <?php if ($APPDATA['onboard'] == 'No') { echo ' selected'; } ?>>No</option>
                    <option value="Yes" <?php if ($APPDATA['onboard'] == 'Yes') { echo ' selected'; } ?>>Yes</option>
        		</select>
        		<br><br>
    		</div>
    	</div>
    	
    	<div class="row">
    	   <div style="float:left;margin-right:20px;">
            <input type="button" name="btnConfirmOnboard" id="btnConfirmOnboard" onclick="getOnboardApplicant('process', '<?php echo htmlspecialchars($_REQUEST['OnboardSource']);?>')" value="Confirm Applicant Onboard Information" class="btn btn-primary">
           </div>            
<?php 
	if (($OrgID == 'I20210401') && ($APPDATA['onboard'] == 'Yes')) { 
	    echo "<div><a href=\"" . IRECRUIT_HOME . "onboard/GANewHireForm.php?ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID . "\" target=\"_blank\"><img src=\"" . IRECRUIT_HOME . "images/icons/page_white_acrobat.png\" title=\"GA New Hire Form\" style=\"margin: 0px 3px -4px 0px;\" border=\"0\"><b style=\"FONT-SIZE: 8pt; COLOR: #000000;\">GA New Hire Form</b></a></div>";
	}
?>
    	</div>
    
    	<input type="hidden" name="ApplicationID" value="<?php echo $ApplicationID; ?>"> 
    	<input type="hidden" name="RequestID" value="<?php echo $RequestID; ?>"> 
    	<input type="hidden" name="action" value="onboardapplicant"> 
    	<input type="hidden" name="process" id="process" value="Y"> 
    	<input type="hidden" name="process_onboard" id="process_onboard" value="YES">
    	
    </form>

</div>

<?php if($feature['DataManagerType'] != 'XML') { ?>
<?php if($OrgID != 'I20120605') { ?>
    <script>
    var intersect_ques_cnt = '<?php echo $intersect_ques_cnt;?>';
    $(document).ready(function() {
        
        if(intersect_ques_cnt == 4) {

        	$("#btnConfirmOnboard").attr("disabled", "disabled");
            
        }
    
    	$("#Sage_PayFreq, #Sage_Rate, #Sage_RatePer, #Sage_PayPeriodHRS").change(function() {
    		var salary_calculations = setCalculatedSageSalaryInfo();
    		$("#salary_calculations").html(salary_calculations);
    	});	
    
    	var salary_calculations = setCalculatedSageSalaryInfo();
    	$("#salary_calculations").html(salary_calculations);
    });                

    function setCalculatedSageSalaryInfo() {
    
    	var Sage_PayFreq       =   $("#appedit_onboard #Sage_PayFreq").val();
    	var Sage_Rate          =   $("#appedit_onboard #Sage_Rate").val();	
    	var Sage_RatePer       =   $("#appedit_onboard #Sage_RatePer").val();
    	var Sage_PayPeriodHRS  =   $("#appedit_onboard #Sage_PayPeriodHRS").val();
    
        if(typeof(Sage_PayFreq) == "undefined") Sage_PayFreq = 0;
        if(typeof(Sage_Rate) == "undefined") Sage_Rate = 0;
        if(typeof(Sage_RatePer) == "undefined") Sage_RatePer = 0;
        if(typeof(Sage_PayPeriodHRS) == "undefined") Sage_PayPeriodHRS = 0;
    	
    	var sagePayUnit          =   0;
    	var sagePayPeriodAmt     =   0;
    	var sagePayPeriodAnnual  =   0;
    	var sagePayPeriodCalcAmt =   0;
    
    
    	if(Sage_PayFreq != 0
    		&& Sage_Rate != 0
    		&& Sage_RatePer != 0
    		&& Sage_PayPeriodHRS != 0) {
    
        	if (Sage_RatePer == "Y")
        	{
        	    sagePayUnit = Sage_Rate / 2080;
        	}
        	if (Sage_RatePer == "M")
        	{
        		sagePayUnit = Sage_Rate / parseFloat("173.34");
        	}
        	if (Sage_RatePer == "W")
        	{  
        		sagePayUnit = Sage_Rate / 40;
        	}
        	if (Sage_RatePer == "H")
        	{
        	    sagePayUnit = Sage_Rate / 1;
        	}
        	if (Sage_PayFreq == "W")
        	{
        	    sagePayPeriodCalcAmt = 52;
        	}
        	if (Sage_PayFreq == "B")
        	{
        	    sagePayPeriodCalcAmt = 26;
        	}
        	if (Sage_PayFreq == "S")
        	{
        	    sagePayPeriodCalcAmt = 24;
        	}
        	if (Sage_PayFreq == "M")
        	{
        	    sagePayPeriodCalcAmt = 12;
        	}
        	
        	sagePayPeriodAmt = sagePayUnit * Sage_PayPeriodHRS;
        
        	if (Sage_RatePer == "Y")
        	{
        	    sagePayPeriodAnnual = Sage_Rate;
        	}
        	else
        	{
        	    sagePayPeriodAnnual = sagePayUnit * (Sage_PayPeriodHRS * sagePayPeriodCalcAmt);
        	}
        
    	}
    	
    	if(intersect_ques_cnt == 4 
        	&& (sagePayUnit == 0
        	|| sagePayPeriodAmt == 0
        	|| sagePayPeriodAnnual == 0
        	|| sagePayPeriodCalcAmt == 0)) {
    	    $("#btnConfirmOnboard").attr("disabled", "disabled");
    	}
    	else {
    		$("#btnConfirmOnboard").removeAttr("disabled", "disabled");
    	}
    
    	var salary_calculations    =   'Pay Unit: $ '+Number.parseFloat(sagePayUnit).toFixed(2)+'<br>';
    	salary_calculations    +=   'Pay Period Amount: $ '+Number.parseFloat(sagePayPeriodAmt).toFixed(2)+'<br>';
    	salary_calculations    +=   'Pay Period Annual: $ '+Number.parseFloat(sagePayPeriodAnnual).toFixed(2)+'<br>';
    	salary_calculations    +=   'Pay Period Calc Amount: $ '+Number.parseFloat(sagePayPeriodCalcAmt).toFixed(2)+'<br>';
    
    	return salary_calculations;
    }
    </script>
<?php } ?>
<?php } ?>
