<div class="col-lg-12 col-md-12 col-sm-12">
    <h4>WOTC Hire Information</h4>
    
    <form method="post" action="applicants.php" name="appedit" id="appedit_onboard">
        <?php
        //Get Onboard Questions List based on FormID
        $onboard_questions  	=   $OnboardQuestionsObj->getOnboardQuestionsList($OrgID, $_REQUEST['OnboardSource']);
        
        if (count($onboard_questions) > 0) {
        	
            echo '<div class="row">';
            echo '<div class="col-lg-12 col-md-12 col-sm-12">';
            echo '<div style="background-color:#F5F5F5;padding:6px 0px 6px 2px;border:1px solid #ddd;font-weight:bold">';
            echo 'Onboard Fields';
            echo '</div><br>';
            echo '</div>';
            echo '</div>';
        	?>        	
        	<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
            	<?php
            	if (is_array ( $onboard_questions )) {
            		foreach ( $onboard_questions as $OnboardQuestionID=>$OQ ) {
                        
                        if($OQ['Active'] == 'Y') {
                			$QuestionID  =   $OnboardQuestionID;
                			$FormID      =   $_REQUEST['OnboardSource'];
                			$formtable   =   "OnboardQuestions";
                			$section     =   1;
                			
            				echo include COMMON_DIR . 'application/DisplayQuestions.inc';
        				}
        				else {
        				
        				    $QuestionID  =   $OnboardQuestionID;
        				
        				    $FormID      =   $_REQUEST['OnboardSource'];
        				    $formtable   =   "OnboardQuestions";
        				    $section     =   1;
        				     
        				    echo include COMMON_DIR . 'application/HiddenQuestions.inc';
        				}
        				
            		} // end foreach
            	}
            	?>
        		</div>
        	</div>
        	<?php
        } // end OnboardQuestions count
    
        echo '<div class="row">';
        echo '<div class="col-lg-12 col-md-12 col-sm-12">';
        echo '<br>';
        
        ?>
    		</div>
    	</div>
    
    	<div class="row">
    	   <div class="col-lg-12 col-md-12 col-sm-12">
            <input type="button" onclick="getOnboardApplicant('process', '<?php echo $_REQUEST['OnboardSource']?>')" value="Add New Hire Information" class="btn btn-primary">
           </div>            
    	</div>
    
		<input type="hidden" name="WotcApplicationID" value="<?php echo $WotcApplicationID; ?>"> 
    	<input type="hidden" name="wotcID" value="<?php echo $wotcid_info['wotcID']; ?>"> 
    	<input type="hidden" name="ApplicationID" value="<?php echo $ApplicationID; ?>"> 
    	<input type="hidden" name="RequestID" value="<?php echo $RequestID; ?>"> 
    	<input type="hidden" name="action" value="onboardapplicant"> 
    	<input type="hidden" name="process" id="process" value="Y"> 
    	<input type="hidden" name="process_onboard" id="process_onboard" value="YES">
    	
    </form>
	
	<script type="text/javascript">
		$("#StartingWage").bind("keypress, keyup, keydown", function(){
			var starting_wage = $("#StartingWage").val();
			starting_wage = starting_wage.replace(/[^0-9.\d]+/gi, "");
			$("#StartingWage").val(starting_wage);
		});
	</script>
</div>
