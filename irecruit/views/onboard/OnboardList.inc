<?php
global $feature;

$DMStatusTitles     =   array (
                    		'ready'       => 'Applicants ready for onboard.',
                    		'downloaded'  => 'Applicants that have been onboarded.' 
                        );

$user_pref_results  =   G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
$DR                 =   $user_pref_results['DefaultSearchDateRange'];

if ($DR == "") {
	$DR = "90";
}

//Get Dates List
$columns        =   "DATE_FORMAT(NOW(),'%m/%d/%Y'), 
                    DATE_FORMAT(DATE_SUB(NOW(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'), 
                    DATE_FORMAT(NOW(),'%Y-%m-%d 23:59:59'), 
                    DATE_FORMAT(DATE_SUB(NOW(), INTERVAL " . $DR . " DAY),'%Y-%m-%d 00:00:00')";
$dates_list     =   $MysqlHelperObj->getDatesList($columns);

if(is_array($dates_list)) list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($dates_list);

if ($process == 'Y') {
	$StartDate = $_REQUEST['ProcessDate_From'];
	$EndDate   = $_REQUEST['ProcessDate_To'];
	$BeginDate = substr ( $_REQUEST['ProcessDate_From'], - 4 ) . '-' . substr ( $_REQUEST['ProcessDate_From'], 0, 2 ) . '-' . substr ( $_REQUEST['ProcessDate_From'], 3, 2 ) . ' 00:00:00';
	$FinalDate = substr ( $_REQUEST['ProcessDate_To'], - 4 ) . '-' . substr ( $_REQUEST['ProcessDate_To'], 0, 2 ) . '-' . substr ( $_REQUEST['ProcessDate_To'], 3, 2 ) . ' 23:59:59';
}
?>
<form method="post" action="applicants.php">
    <table border="0" cellspacing="3" cellpadding="5" class="table table-borderless">
        <tr>
            <td valign="middle" align="center" colspan="3">
                Processed Date From:
                <input type="text" id="ProcessDate_From" name="ProcessDate_From" value="<?php echo $StartDate;?>" size="10" onBlur="validate_date(this.value, ' From Date')"/>
                To:
                <input type="text" id="ProcessDate_To" name="ProcessDate_To" value="<?php echo $EndDate?>" size="10" onBlur="validate_date(this.value, ' To Date')"/>
                <input type="hidden" name="action" value="<?php echo $_REQUEST['action']?>">
                <input type="hidden" name="OnboardSource" value="<?php echo $_REQUEST['OnboardSource']?>">
                <input type="hidden" name="process" value="Y">
                <input type="submit" value="Update List" class="btn btn-primary btn-sm">
            </td>
        </tr>
    </table>
</form>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Application ID</th>
            <th>Applicant Name</th>
            <th>Requisition Title</th>
            <th>RequisitionID / JobID</th>
            <th style="text-align:center">Hire Date</th>
            <?php 
		$colspan="8";
                if($_REQUEST['OnboardSource'] == 'MatrixCare') {
                    echo '<th style="text-align:center">MatrixCare</th>';
		    $colspan="9";
                }
                if($_REQUEST['OnboardSource'] == 'WebABA') {
                    echo '<th style="text-align:center">EmployeeID</th>';
		    $colspan="9";
                }
            ?>
	    <th>Status</th>
            <th>Date</th>
        </tr>
    </thead>

    <tbody>
    <?php
    //Get Onboard Applicants
    $results    =   $OnboardFormDataObj->getOnboardApplicantsList($OrgID, $USERROLE, $USERID, $FinalDate, $BeginDate, $_REQUEST['OnboardSource']);
    $i          =   0;
    
    if(is_array($results['results'])) {
    	foreach($results['results'] as $row) {
    		
    	    $i ++;
            $ApplicationID      =   $row ['ApplicationID'];
            $RequestID          =   $row ['RequestID'];
            $multiorgid_req	=   $row ['MultiOrgID'];

	    $HD = array('Abila' => 'Sage_HireDate', 'CloudPayrollHR' => 'cloud_HireDate', 'Exporter' => 'Sage_HireDate', 'HRMS' => 'Sage_HireDate', 'Lightwork' => 'Sage_HireDate', 'Mercer' => 'hire_date', 'VFP' => 'Sage_HireDate', 'WebABA' => 'HiredDate', 'WOTC' => 'HiredDate', 'XML' => 'Sage_HireDate');
            
            $onboard_app_info   =   $OnboardApplicationsObj->getOnboardApplicationDetails($OrgID, $ApplicationID, $RequestID, $_REQUEST['OnboardSource']);
            $first_name         =   $OnboardFormDataObj->getApplicantOnboardAnswer($OrgID, $_REQUEST['OnboardSource'], $ApplicationID, 'first');
            $last_name          =   $OnboardFormDataObj->getApplicantOnboardAnswer($OrgID, $_REQUEST['OnboardSource'], $ApplicationID, 'last');
            $HiredDate 		=   $OnboardFormDataObj->getApplicantOnboardAnswer($OrgID, $_REQUEST['OnboardSource'], $ApplicationID, $HD[$_REQUEST['OnboardSource']]);
            $Status             =   $onboard_app_info['Status'];
            $ProcessDate        =   $onboard_app_info['ProcessDate'];
            $ReturnData		=   $onboard_app_info['ReturnData'];

            $app_details        =   $ApplicationsObj->getJobApplicationsDetailInfo("MatrixCaregiverID", $OrgID, $ApplicationID, $RequestID);
            
            if($first_name != "") {
            	
                // get applicant data
        		$PersonalInformation  =   $first_name. ' ' . $last_name;
        		
        		echo '<tr>';

        		
                echo '<td>';
                echo '<a href="applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&tab_action=onboard-applicant&OnboardSource='.$_REQUEST['OnboardSource'].'">';
                echo $ApplicationID;
                echo '</a>';
                echo '</td>';

                echo '<td>';
                echo $PersonalInformation;
                echo '</td>';
                
                echo '<td>';
                echo $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
                echo '</td>';
                
                echo '<td>';
                echo $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
                echo '</td>';

                echo '<td style="text-align:center">';
                    if($HiredDate != "")
                    {
		       echo $HiredDate;
		       //echo date_format(date_create($HiredDate),'Y-m-d');
                    }
                    else {
                        echo "-";
                    }
                echo '</td>';
                
                if($_REQUEST['OnboardSource'] == 'MatrixCare') {
                    echo '<td style="text-align:center">';
                    if($app_details['MatrixCaregiverID'] != "")
                    {
                        echo $app_details['MatrixCaregiverID'];
                    }
                    else {
                        echo "N/A";
                    }
                    echo '</td>';
                }
                if($_REQUEST['OnboardSource'] == 'WebABA') {
                    echo '<td style="text-align:center">';
                    if($ReturnData != "")
                    {
                        echo $ReturnData;
                    }
                    else {
                        echo "N/A";
                    }
                    echo '</td>';
                }

                echo '<td>';
                echo $Status;
                echo '</td>';
                
                echo '<td>';
                echo $ProcessDate;
                echo '</td>';
                
                echo '</tr>';
        		
            }	
    	} // end foreach
    }
    
    if ($i == 0) {
    	echo '<tr><td colspan="' . $colspan . '">There are no entries in this data range.</td></tr>';
    }
    ?>
    </tbody>
</table>
