<div class="col-lg-12 col-md-12 col-sm-12">
    <h4>Matrix Care</h4>
    <?php
    $params_web_form_id     =   array(":OrgID"=>$OrgID,":sOrgID"=>$OrgID);
    $sel_web_form_id        =   "SELECT WebFormID FROM WebForms WHERE OrgID = :OrgID and WebFormID IN (
                                SELECT DISTINCT(WebFormID) FROM WebFormQuestions WHERE OrgID = :sOrgID AND FormID = 'emergencycontact') and FormStatus = 'Active' LIMIT 1";
    $res_web_form_id        =   G::Obj('GenericQueries')->getRowInfoByQuery($sel_web_form_id, array($params_web_form_id));
    
    // Set where condition
    $where                  =   array ("OrgID = :OrgID", "WebFormID = :WebFormID", "ApplicationID = :ApplicationID");
    // Set parameters
    $params                 =   array (":OrgID" => $OrgID, ":WebFormID"=>$res_web_form_id['WebFormID'], ":ApplicationID" => $ApplicationID);
    // Get WebFormData Object
    $results_web_form_data  =   $FormDataObj->getWebFormData ( "QuestionID, Answer", $where, "", "", array ($params) );
    $web_form_data_res      =   $results_web_form_data['results'];
    $web_form_data_cnt      =   $results_web_form_data['count'];
    
    $WEBFORMDATA			=	array();
    for($wd = 0; $wd < $web_form_data_cnt; $wd++) {
        $WEBFORMDATA[$web_form_data_res[$wd]['QuestionID']] = $web_form_data_res[$wd]['Answer'];
    }
    
    $params_que_info        =   array(":OrgID"=>$OrgID, ":WebFormID"=>$res_web_form_id['WebFormID']);
    $where_que_info         =   array("OrgID = :OrgID", "Required = 'Y'", "WebFormID = :WebFormID");
    $res_que_results        =   $FormQuestionsObj->getQuestionsInformation("WebFormQuestions", "QuestionID, Required", $where_que_info, "", array($params_que_info));
    $res_web_que_results    =   $res_que_results['results'];
    $res_web_que_count      =   $res_que_results['count'];
    
    $ERROR_MSG_WEB_FORM     =   "";
    for($wq = 0; $wq < $res_web_que_count; $wq++) {
        if($res_web_que_results[$wq]['Required'] == 'Y') {
            if($res_web_que_results[$wq]['QuestionID'] == "ecphone1a") {
                
                $Answer13   =   ($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']] != "") ? json_decode($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']], true) : "";
                
                $WEBFORMDATA["ecphone1a1"]  =   $Answer13[0];   
                $WEBFORMDATA["ecphone1a2"]  =   $Answer13[1];
                $WEBFORMDATA["ecphone1a3"]  =   $Answer13[2];
                
                if($WEBFORMDATA["ecphone1a1"] == ""
                    || $WEBFORMDATA["ecphone1a2"] == ""
                    || $WEBFORMDATA["ecphone1a3"] == "") {
                    $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
                }
            }
            else if($res_web_que_results[$wq]['QuestionID'] == "ecphone1b") {
                
                $Answer13   =   ($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']] != "") ? json_decode($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']], true) : "";
                
                $WEBFORMDATA["ecphone1b1"]  =   $Answer13[0];
                $WEBFORMDATA["ecphone1b2"]  =   $Answer13[1];
                $WEBFORMDATA["ecphone1b3"]  =   $Answer13[2];
                
                if($WEBFORMDATA["ecphone1b1"] == ""
                    || $WEBFORMDATA["ecphone1b2"] == ""
                    || $WEBFORMDATA["ecphone1b3"] == "") {
                    $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
                }
            }
            else if($res_web_que_results[$wq]['QuestionID'] == "ecphone1c") {
                
                $Answer13   =   ($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']] != "") ? json_decode($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']], true) : "";
                
                $WEBFORMDATA["ecphone1c1"]  =   $Answer13[0];
                $WEBFORMDATA["ecphone1c2"]  =   $Answer13[1];
                $WEBFORMDATA["ecphone1c3"]  =   $Answer13[2];
                
                if($WEBFORMDATA["ecphone1c1"] == ""
                    || $WEBFORMDATA["ecphone1c2"] == ""
                    || $WEBFORMDATA["ecphone1c3"] == "") {
                    $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
                }
            }
            else if($res_web_que_results[$wq]['QuestionID'] == "ecphone2a") {
                
                $Answer13   =   ($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']] != "") ? json_decode($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']], true) : "";
                
                $WEBFORMDATA["ecphone2a1"]  =   $Answer13[0];
                $WEBFORMDATA["ecphone2a2"]  =   $Answer13[1];
                $WEBFORMDATA["ecphone2a3"]  =   $Answer13[2];
                
                if($WEBFORMDATA["ecphone2a1"] == ""
                    || $WEBFORMDATA["ecphone2a2"] == ""
                    || $WEBFORMDATA["ecphone2a3"] == "") {
                    $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
                }
            }
            else if($res_web_que_results[$wq]['QuestionID'] == "ecphone2b") {
                
                $Answer13   =   ($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']] != "") ? json_decode($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']], true) : "";
                
                $WEBFORMDATA["ecphone2b1"]  =   $Answer13[0];
                $WEBFORMDATA["ecphone2b2"]  =   $Answer13[1];
                $WEBFORMDATA["ecphone2b3"]  =   $Answer13[2];
                
                if($WEBFORMDATA["ecphone2b1"] == ""
                    || $WEBFORMDATA["ecphone2b2"] == ""
                    || $WEBFORMDATA["ecphone2b3"] == "") {
                    $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
                }
            }
            else if($res_web_que_results[$wq]['QuestionID'] == "ecphone2c") {
                
                $Answer13   =   ($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']] != "") ? json_decode($WEBFORMDATA [$res_web_que_results[$wq]['QuestionID']], true) : "";
                
                $WEBFORMDATA["ecphone2c1"]  =   $Answer13[0];
                $WEBFORMDATA["ecphone2c2"]  =   $Answer13[1];
                $WEBFORMDATA["ecphone2c3"]  =   $Answer13[2];
                
                if($WEBFORMDATA["ecphone2c1"] == ""
                    || $WEBFORMDATA["ecphone2c2"] == ""
                    || $WEBFORMDATA["ecphone2c3"] == "") {
                    $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
                }
            }
            else if($WEBFORMDATA[$res_web_que_results[$wq]['QuestionID']] == "") {
                $ERROR_MSG_WEB_FORM = "Required fields are missing in Emergency contacts information.";
            }
        }
    }
     
    if($ERROR_MSG_WEB_FORM != "" && $feature ['MatrixCare'] == "Y") {
    	echo "<span style='color:red'>".$ERROR_MSG_WEB_FORM."</span>";
    }
    ?>
    <form method="post" action="applicants.php" name="appedit" id="appedit_onboard">
        <?php
        //Get Onboard Questions List based on FormID
        $onboard_questions  =   $OnboardQuestionsObj->getOnboardQuestionsList($OrgID, $_REQUEST['OnboardSource']);
        
        if (count($onboard_questions) > 0) {
                	
            $matrixcare_companies_res   =   $MatrixCareObj->getMatrixCareCompanies($OrgID);
            $matrixcare_companies_list  =   $matrixcare_companies_res['results'];
            $matrixcare_companies_cnt   =   $matrixcare_companies_res['count'];
            
            $office_ids_info            =   $MatrixCareObj->getMatrixCareOfficeIds($OrgID, $MatrixCareCompanyID);
            $branches_info              =   json_decode($office_ids_info['BranchesInfo'], true);
            
            echo '<div class="row">';
            echo '<div class="col-lg-12 col-md-12 col-sm-12">';
            echo '<div style="background-color:#F5F5F5;padding:6px 0px 6px 2px;border:1px solid #ddd;font-weight:bold">';
            echo 'Onboard Fields';
            echo '</div><br>';
            echo '</div>';
            echo '</div>';
        	?>
        	
        	<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
            	<?php    
                if($matrixcare_companies_cnt > 0 && $feature ['MatrixCare'] == "Y") {
                    echo '<div class="form-group row">';
                    echo '<div class="col-lg-3 col-md-3 col-sm-3">';
                    echo '<label for="ddlMatrixCareCompaniesList" class="question_name">Matrix Care Companies List: &nbsp;<font style="color:red">*</font></label>';
                    echo '</div>';
                    
                    echo '<div class="col-lg-9 col-md-9 col-sm-9">';
                    echo '<select name="ddlMatrixCareCompaniesList" id="ddlMatrixCareCompaniesList" class="form-control width-auto-inline" onchange="getMatrixCareOfficeIds(this.value)">';
    
                    echo '<option value="">Please select</option>';
                    
                    for($mc = 0; $mc < $matrixcare_companies_cnt; $mc++) {
                        ?>
                        <option value="<?php echo $matrixcare_companies_list[$mc]['CompanyID'];?>" <?php if(($cloud_application_info['MatrixCareCompanyID'] == $matrixcare_companies_list[$mc]['CompanyID']) || ($MatrixCareCompanyID == $matrixcare_companies_list[$mc]['CompanyID'])) echo ' selected="selected"';?>>
                            <?php echo $matrixcare_companies_list[$mc]['CompanyName'];?>
                        </option>
                        <?php
                	}
                	
                	echo '</select>';
                	echo '</div>';
                	echo '</div>';
                	
                	if(isset($APPDATA['matrixcare_OfficeID']) && $APPDATA['matrixcare_OfficeID'] != "") {
                	    $default_value_office_id = $APPDATA['matrixcare_OfficeID'];
                	}
                	else {
                	    $default_value_office_id = $office_ids_info['DefaultValue'];
                	}
                	
                	echo '<div class="form-group row">';
                	echo '<div class="col-lg-3 col-md-3 col-sm-3">';
                	echo '<label for="matrixcare_OfficeID" class="question_name">Matrix Care Office ID: &nbsp;<font style="color:red">*</font></label>';
                	echo '</div>';
                	
                	echo '<div class="col-lg-9 col-md-9 col-sm-9">';
                	echo '<select name="matrixcare_OfficeID" id="matrixcare_OfficeID" class="form-control width-auto-inline" size="1">';
                	echo '<option value="">Select</option>';
                	
                	if(is_array($branches_info)) {
                	    foreach ($branches_info as $branch_id=>$branch_name)
                	    {
                	        $office_id_selected = '';
                	        if($default_value_office_id == $branch_id) $office_id_selected = ' selected="selected"';
                	        ?>
    			            <option value="<?php echo $branch_id;?>" <?php echo $office_id_selected;?>><?php echo $branch_name;?></option>
    				    	<?php
    				    }
    				}
    				
    				echo '</select>';
    				echo '</div>';
    				echo '</div>';
                	
                }
                
            	if (is_array ( $onboard_questions )) {
            		foreach ( $onboard_questions as $OnboardQuestionID=>$OQ ) {
                        
                        if($OQ['Active'] == 'Y') {
                            $QuestionID  =   $OnboardQuestionID;
                            $FormID      =   $_REQUEST['OnboardSource'];
                            $formtable   =   "OnboardQuestions";
                            $section     =   1;

							echo include COMMON_DIR . 'application/DisplayQuestions.inc';
                        }
                        else {
                        
                            $QuestionID  =   $OnboardQuestionID;
                        
                            $FormID      =   $_REQUEST['OnboardSource'];
                            $formtable   =   "OnboardQuestions";
                            $section     =   1;
                            	
                            echo include COMMON_DIR . 'application/HiddenQuestions.inc';
                        }
                        
            		} // end foreach
            	}
            	?>
        		</div>
        	</div>
        	<?php
        } // end OnboardQuestions count

		//Get JobApplicationsDetail Information after process the data
		$columns                =   "MatrixCaregiverID, MatrixCaregiverStatus, MatrixCaregiverContactStatus, MatrixCaregiverContacts";
		$rowjobapp              =   $ApplicationsObj->getJobApplicationsDetailInfo($columns, $OrgID, $ApplicationID, $RequestID);
        ?>
    
    	<div class="row">
    		<?php
            echo '<div id="divMatrixCareButton" class="col-lg-8 col-md-8 col-sm-8">';
            if ($matrixcare_companies_cnt > 0 && $feature ['MatrixCare'] == "Y") {
                
                if ($rowjobapp['MatrixCaregiverStatus'] == '0' || $rowjobapp['MatrixCaregiverStatus'] == 0) {
                    if($ERROR_MSG_WEB_FORM == "") {
                        ?>&nbsp;&nbsp; <input type="button" id="btnOnboardMatrixCare" name="btnOnboardMatrixCare" onclick="getOnboardApplicant('process', 'MatrixCare')" value="Send to MatrixCare" class="btn btn-primary"><?php
                    }
                }
                else {
                    echo '<h4>You have onboard this applicant to MatrixCareGiver.
                          <br>Care Giver ID - ' . $rowjobapp ['MatrixCaregiverID'] . '.</h4>';
            
                    $mcc_list = json_decode($rowjobapp['MatrixCaregiverContacts'], true);
                    if($mcc_list['C1'] == "" || $mcc_list['C2'] == "" || $mcc_list['C3'] == "") {
                        echo '&nbsp;&nbsp;<input type=\'button\' onclick=\'editMatrixCareContacts("'.$ApplicationID.'", "'.$RequestID.'", "'.$rowjobapp['MatrixCaregiverID'].'", "'.$cloud_application_info['MatrixCareCompanyID'].'")\' id="btnUpdateMatrixCareContacts" value=\'Update Emergency Contacts\' class=\'btn btn-primary\'>';
                    }
                }
            }
            echo '</div>';
    		?>
    	</div>
    
    	<input type="hidden" name="ApplicationID" value="<?php echo $ApplicationID; ?>"> 
    	<input type="hidden" name="RequestID" value="<?php echo $RequestID; ?>"> 
    	<input type="hidden" name="action" value="onboardapplicant"> 
    	<input type="hidden" name="process" id="process" value="Y"> 
    	<input type="hidden" name="onboard" id="onboard" value="Yes"><!-- Only For Cloud Payroll and Matrix Care -->
    	<input type="hidden" name="process_onboard" id="process_onboard" value="YES">
    </form>
    
    <script type="text/javascript">    		
    function getMatrixCareOfficeIds(company_id) {
    	var ApplicationID  =   document.forms['frmApplicationDetailInfo'].ApplicationID.value;
    	var RequestID      =   document.forms['frmApplicationDetailInfo'].RequestID.value;
    
    	$.ajax({
    		method: "POST",
    		dataType: "JSON",
      		url: 'applicants/getMatrixCareOfficeIds.php?CompanyID='+company_id+'&ApplicationID='+ApplicationID+'&RequestID='+RequestID,
    		success: function(data) {
    
    			var branches_info = data[0];
    			var branches_data = data[2];
    
    		    if(typeof(branches_info) != 'undefined' && typeof(branches_info) != null) {
    
    			    $("#matrixcare_OfficeID").children('option').remove();
    		        for(j in branches_info) {
    		        	$("#matrixcare_OfficeID").append("<option value='"+j+"'>"+branches_info[j]+"</option>");
    				}
    			}
    		    else {
    		    	$("#matrixcare_OfficeID").children('option').remove();
    			}
    		    
    		    if(data[1] == '' || data[1] == null) {
    		    	$("#btnOnboardMatrixCare").attr('disabled', 'disabled');
    		    }
                else {
                	$("#btnOnboardMatrixCare").removeAttr('disabled');
    		    }
        	}
    	});
    }
    
    function editMatrixCareContacts(app_id, req_id, caregiver_id, matrixcare_companyid) {
    
    	$.ajax({
    		method: "POST",
      		url: 'applicants/editMatrixCareContacts.php?caregiver_id='+caregiver_id+'&app_id='+app_id+'&req_id='+req_id+'&matrixcare_companyid='+matrixcare_companyid,
    		success: function(data) {
    		    if(data == 'SUCCESS') {
    		        $("#btnUpdateMatrixCareContacts").hide();
    		    }
    		    else {
    		    	$("#btnUpdateMatrixCareContacts").show();
    		    }
        	}
    	});
    
    }
    </script>
</div>
