<?php 
if ($message) {
    if($ERROR != "") {
        echo "<div class='col-lg-12 col-md-12 col-sm-12' style='color:red'>".$message."</div>";
    } else {
        echo "<div class='col-lg-12 col-md-12 col-sm-12' style='color:blue'>".$message."</div>";
    }
}
?>
<div class="col-lg-12 col-md-12 col-sm-12">
    <br>
    <h4>Select Onboard Process Type:</h4>
    <form name="frmOnboardProcessesList" id="frmOnboardProcessesList" method="post">
    <?php
    if($feature['MatrixCare'] == 'Y') {
        ?><input type="radio" name="rdOnboardType" value="MatrixCare" onclick="getOnboardApplicant('', 'MatrixCare');" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'MatrixCare') echo 'checked="checked"';?>> MatrixCare <?php
    }
    if($feature['WebABA'] == 'Y') {
        ?><input type="radio" name="rdOnboardType" value="WebABA" onclick="getOnboardApplicant('', 'WebABA');" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'WebABA') echo 'checked="checked"';?>> WebABA <?php
    }
    if($feature['Lightwork'] == 'Y') {
        ?><input type="radio" name="rdOnboardType" value="Lightwork" onclick="getOnboardApplicant('', 'Lightwork');" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'Lightwork') echo 'checked="checked"';?>> Lightwork <?php
    }
    if($feature['Abila'] == 'Y') {
        ?><input type="radio" name="rdOnboardType" value="Abila" onclick="getOnboardApplicant('', 'Abila');" <?php if (isset($_REQUEST['OnboardSource']) && ($_REQUEST['OnboardSource'] == 'Abila')) echo 'checked="checked"';?>> Abila <?php
    }
    if($feature['Mercer'] == 'Y') {
        ?><input type="radio" name="rdOnboardType" value="Mercer" onclick="getOnboardApplicant('', 'Mercer');" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'Mercer') echo 'checked="checked"';?>> Mercer <?php
    }

    if($feature['DataManagerType'] != 'N') {
        if($feature['DataManagerType'] == 'XML') {
            $data_manager_desc = 'XML';
        }
        else if($feature['DataManagerType'] == 'Exporter') {
            $data_manager_desc = 'Sage SQL';
        }
        else if($feature['DataManagerType'] == 'HRMS') {
            $data_manager_desc = 'Sage HRMS';
        }
    
        if($data_manager_desc != '') {
            ?>
            <input type="radio" name="rdOnboardType" value="<?php echo $feature['DataManagerType'];?>" onclick="getOnboardApplicant('', '<?php echo $feature['DataManagerType'];?>');" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == $feature['DataManagerType']) echo 'checked="checked"';?>> 
            <?php 
            echo $data_manager_desc;
        }
    }

	if($wotc_app_header_info['ApplicationID'] != "") {
		?>
		<input type="radio" name="rdOnboardType" value="WOTC" onclick="getOnboardApplicant('', 'WOTC');" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'WOTC') echo 'checked="checked"';?>> CMS WOTC 
		<?php
	}
	?>
    </form>
    <br>
</div>

<?php
if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] != "") {
    if($_REQUEST['OnboardSource'] == 'MatrixCare') {
    	require_once IRECRUIT_VIEWS . 'onboard/MatrixCareForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'WebABA') {
    	require_once IRECRUIT_VIEWS . 'onboard/WebABAForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'Lightwork') {
    	require_once IRECRUIT_VIEWS . 'onboard/LightworkForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'Abila') {
    	require_once IRECRUIT_VIEWS . 'onboard/AbilaForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'Mercer') {
    	require_once IRECRUIT_VIEWS . 'onboard/MercerForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'WOTC') {
    	require_once IRECRUIT_VIEWS . 'onboard/WOTCOnboardForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == $feature['DataManagerType']) {
        require_once IRECRUIT_VIEWS . 'onboard/DataManagerForm.inc';
    }
}

//Get Child Questions Information
$personal_ques_info     =   G::Obj('OnboardQuestions')->getOnboardPersonalQuestionsInfo($OrgID, $_REQUEST['OnboardSource']);
$child_ques_info        =   G::Obj('OnboardQuestions')->getOnboardFormChildQuesForParentQues($OrgID, $_REQUEST['OnboardSource']);

//Get Question Types List
$que_types_list     =   array();
if(is_array($personal_ques_info)) {
    foreach($personal_ques_info as $personal_que_id=>$personal_que_info) {
        $que_types_list[$personal_que_id]   =   $personal_que_info['QuestionTypeID'];
    }
}

?>
<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info	=	JSON.parse('<?php echo json_encode($child_ques_info);?>');
</script>
<script type="text/javascript">
function getFormIDChildQuestionsInfo() {
	for (question_id in child_ques_info) {
		  var child_que_info =  child_ques_info[question_id];
		  for (question_id_option in child_que_info) {
			  if(que_types_list[question_id] == 2
				|| que_types_list[question_id] == 22
				|| que_types_list[question_id] == 23) {
					
                    var checked_radio = $("input[name='"+question_id+"']:checked").val();
                    
                    if(checked_radio == question_id_option)
                    {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                        	if(child_ques_list[child_ques_ids] == "show") {
                        		$("#divQue-"+child_ques_ids).show();
                        	}
                        	else if(child_ques_list[child_ques_ids] == "hidden") {
                        		$("#divQue-"+child_ques_ids).hide();
                        	}							
                        }
                    }
                    else if(typeof(checked_radio) == 'undefined') {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                            $("#divQue-"+child_ques_ids).hide();
                        }
                   }
			 }
			 else if(que_types_list[question_id] == 3) {
				  var checked_radio = $("#"+question_id).val();
				  //Exception for country question, have to handle it in simple way
				  if(question_id == 'country' && typeof(checked_radio) != 'undefined') {
					  if(checked_radio != 'US' && checked_radio != 'CA') {
						  	child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
					  }
					  else if(checked_radio == question_id_option)
	                  {
		                    child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
	                  }
				  }
				  else if(checked_radio == question_id_option)
                  {
	                    child_ques_list = child_que_info[question_id_option];
	                    for(child_ques_ids in child_ques_list) {
	                    	if(child_ques_list[child_ques_ids] == "show") {
	                    		$("#divQue-"+child_ques_ids).show();
	                    	}
	                    	else if(child_ques_list[child_ques_ids] == "hidden") {
	                    		$("#divQue-"+child_ques_ids).hide();
	                    	}							
	                    }
                  }
                  else if(typeof(checked_radio) == 'undefined') {
                      child_ques_list = child_que_info[question_id_option];
                      
                      for(child_ques_ids in child_ques_list) {
                      		$("#divQue-"+child_ques_ids).hide();
                      }
                 }
			 }
		  }
	}
}

$(document).ready(function() {
	getFormIDChildQuestionsInfo();
});
</script>
