<?php
$all_sections_list      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $_REQUEST ['form'], "", "No");
$available_sections     =   array_keys($all_sections_list);
$available_sections     =   G::Obj('ArrayHelper')->removeElement("6", $available_sections);

if ($questionid) {
	// edit individual entry
	$FormID        =   $_REQUEST['OnboardSource'];
	$QuestionID    =   $questionid;
	
	include IRECRUIT_VIEWS . 'onboard/EditOnboardQuestion.inc';
} else {
	// Get Question Types List
    $row_question_types = G::Obj('QuestionTypes')->getQuestionTypesInfo ( "*", array (), "SortOrder", array () );
	
	if (is_array ( $row_question_types ['results'] )) {
		foreach ( $row_question_types ['results'] as $row_question_types ) {
			$row_que_types [] = $row_question_types;
			$ques_types_list [] = $row_question_types ['QuestionTypeID'];
			$question_types_by_id [$row_question_types ['QuestionTypeID']] = $row_question_types ['Description'];
			
			if ($row_question_types ['Editable'] == 'Y') {
				$que_type_id = $row_question_types ['QuestionTypeID'];
				if ($que_type_id != '30' 
					&& $que_type_id != '60'
					&& $que_type_id != '9'
					&& $que_type_id != '21'
					&& $que_type_id != '25') {
				    
				    if($section != 6 && $que_type_id != 8) {
                        $edit_que_types [] = $row_question_types;
				    }
				    else if($section == 6) {
				        $edit_que_types [] = $row_question_types;
				    }
				}
			}
		}
	}
	
	if ($action == 'onboardquestions') {
		
		echo '<br>';
		
		echo '<div class="table-responsive">';

		?>
		<form name="frmOnboardProcessesList" id="frmOnboardProcessesList" method="get">
		<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">
    		<tr>
        		<td colspan="6"><h4>Select Onboard Process Type:</h4></td>
    		</tr>
    		<tr>
        		<td colspan="6">
                    <?php
                    if($feature['MatrixCare'] == 'Y') {
                        ?><input type="radio" name="rdOnboardType" value="MatrixCare" onclick="location.href='administration.php?action=onboardquestions&OnboardSource=MatrixCare&menu=8'" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'MatrixCare') echo 'checked="checked"';?>> MatrixCare <?php
                    }
                    if($feature['WebABA'] == 'Y') {
                        ?><input type="radio" name="rdOnboardType" value="WebABA" onclick="location.href='administration.php?action=onboardquestions&OnboardSource=WebABA&menu=8'" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'WebABA') echo 'checked="checked"';?>> WebABA <?php
                    }
                    if($feature['Lightwork'] == 'Y') {
                        ?><input type="radio" name="rdOnboardType" value="Lightwork" onclick="location.href='administration.php?action=onboardquestions&OnboardSource=Lightwork&menu=8'" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'Lightwork') echo 'checked="checked"';?>> Lightwork <?php
                    }
                    if($feature['Abila'] == 'Y') {
                        ?><input type="radio" name="rdOnboardType" value="Abila" onclick="location.href='administration.php?action=onboardquestions&OnboardSource=Abila&menu=8'" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'Abila') echo 'checked="checked"';?>> Abila <?php
                    }
                    if($feature['Mercer'] == 'Y') {
                        ?><input type="radio" name="rdOnboardType" value="Mercer" onclick="location.href='administration.php?action=onboardquestions&OnboardSource=Mercer&menu=8'" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == 'Mercer') echo 'checked="checked"';?>> Mercer <?php
                    }
                    if($feature['DataManagerType'] != 'N') {
                        if($feature['DataManagerType'] == 'XML') {
                            $data_manager_desc = 'XML';	
                        }
                        else if($feature['DataManagerType'] == 'Exporter') {
                        	$data_manager_desc = 'Sage SQL';
                        }
                        else if($feature['DataManagerType'] == 'HRMS') {
                        	$data_manager_desc = 'Sage HRMS';
                        }
                        
                        if($data_manager_desc != '') {
                        	?>
                        	<input type="radio" name="rdOnboardType" value="<?php echo $feature['DataManagerType'];?>" onclick="location.href='administration.php?action=onboardquestions&OnboardSource=<?php echo $feature['DataManagerType']?>&menu=8'" <?php if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] == $feature['DataManagerType']) echo 'checked="checked"';?>>
                        	<?php echo $data_manager_desc;
                        	
                        }
                    }
                    ?>
        		</td>
    		</tr>
		</table>
        </form>
        
        <table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">
    		<tr>
        		<td colspan="6" id="sort_onboard_questions_msg" style="color:blue"></td>
    		</tr>
		</table>
        
		<?php
		if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] != "") {

            echo '<form method="post" action="' . $formscript . '">';
            
            echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" id="sort_onboard_questions" class="table table-striped table-bordered">';
            
            echo '<thead>';
            echo '<tr>';
            echo '<td width="570"><b>Question</b></td>';
            echo '<td><b>Question Type</b></td>';
            echo '<td align=center width="90"><b>Edit Details</b></td>';
            echo '<td align=center width="60"><b>Active</b>';
            echo '<input type="checkbox" name="active_check_uncheck" onclick=\'check_uncheck(this.checked, "chk_active")\'>';
            echo '</td>';
            
            $act = "on";
            if (($formtable == "FormQuestions") && ($section == "0")) {
                $act = "off";
            }
            
            if ($act == "on") {
                echo '<td align=center width="60"><b>Required</b>';
                echo '<input type="checkbox" name="required_check_uncheck" onclick=\'check_uncheck(this.checked, "chk_required")\'>';
                echo '</td>';
            } // end section
            
            echo '<td><b>Action</b></td>';
            echo '</tr>';
            
            echo '</thead>';
            
            echo '<tbody>';
            
            $results  =   G::Obj('Forms')->getFormTableInfo ( $formtable, $OrgID, $form, $section );
            $cnt      =   $results ['count'];
            $i        =   0;
            $rowcolor =   "#eeeeee";
            
            
            if (is_array ( $results ['results'] )) {
                foreach ( $results ['results'] as $row ) {
            
                    $i ++;
                    $QuestionID     =   $row ['QuestionID'];
                    $Question       =   $row ['Question'];
                    $Active         =   $row ['Active'];
                    $Required       =   $row ['Required'];
                    $QuestionTypeID =   $row ['QuestionTypeID'];
            
                    if (isset ( $row ['SectionID'] )) {
                        $SectionID  =   $row ['SectionID'];
                    }
            
                    $QuestionOrder  =   $row ['QuestionOrder'];
            
                    if (isset ( $row ['SageLock'] )) {
                        $SageLock   =   $row ['SageLock'];
                    }
            
                    $ckd1           =   ($Active == "Y") ? ' checked' : '';
                    $ckd2           =   ($Required == "Y") ? ' checked' : '';
            
                    echo '<tr bgcolor="' . $rowcolor . '" id="'.$OrgID."*".$row['OnboardFormID']."*".$QuestionID.'">';
                    echo '<td>';
            
                    if ($QuestionTypeID == 98) {
                        $Question = "&nbsp;&nbsp;&nbsp;&nbsp;[<i>Spacer</i>]";
                    } // end if
            
                    // if (($QuestionTypeID == 100) || ($QuestionTypeID == 120)) {
                    if (substr ( $QuestionID, 0, 4 ) == "CUST") {
                        echo "<input type='text' name='" . $QuestionID . "Question' id='" . $QuestionID . "Question' value=\"".htmlentities($Question)."\" size='40'>";
                    } else if (($QuestionTypeID == 99) || ($QuestionTypeID == 90)) {
                        echo '<b>Instruction</b>: ';
                        echo substr ( strip_tags ( $Question ), 0, 40 ) . '...';
                    } else {
                        echo $Question;
                    }
                    
                    echo '</td>';
            
                    $QuestionTypeName = $QuestionID . "-questiontypeid";
            
                    echo "<td>";
                    
                    if (substr ( $QuestionID, 0, 4 ) == "CUST") {

                        echo "<select name='$QuestionTypeName' style='width:120px;'>";
                        echo "<option value=''>Select</option>";
                        	
                        for($qt = 0; $qt < count ( $edit_que_types ); $qt ++) {
    						echo "<option value='" . $edit_que_types [$qt] ['QuestionTypeID'] . "'";
    						if ($QuestionTypeID == $edit_que_types [$qt] ['QuestionTypeID']) echo ' selected="selected"';
            				echo ">" . $edit_que_types [$qt] ['Description'] . "</option>";
                        }
                        
                        echo "</select>";
                        
                    } else {

                        echo $question_types_by_id [$QuestionTypeID];
                        
                    }
				    
                    echo "</td>";
            
                    echo '<td align=center>';
                    
                    if ($SageLock == "Y") {

    					echo '<a href="' . IRECRUIT_HOME . 'administration.php?action=onboardquestions&OnboardSource='.$_REQUEST['OnboardSource'].'&menu=8&unlock=' . $QuestionID . '" onclick="return confirm(\'Are you sure you want to unlock the following question?\n\n' . $Question . '\n\n\')">';
    					echo '<img src="' . IRECRUIT_HOME . 'images/icons/lock_edit.png" border="0" title="Lock Edit">';
                        echo '</a>';
                        
                    } else {
                        if ($QuestionTypeID != 98) {
    						echo '<a href="' . $formscript . '?action=' . $action . '&form=' . $form;
                            if ($formtable == "FormQuestions") {
        					   echo '&section=' . $section;
                            }
                            echo '&questionid=' . $QuestionID . '&QuestionTypeID='.$QuestionTypeID.'&OnboardSource='.$_REQUEST['OnboardSource'].'"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
                        } else {
                            echo '&nbsp;';
                        }
                    }
                    
                    echo '</td>';
            
        		    echo '<td align=center>';
                            
        		    echo '<input type="hidden" name="' . $QuestionID . '" value="D">';
        		    echo '<input type="checkbox" class="chk_active" name="' . $QuestionID . '-A" id="' . $QuestionID . '-A" value="Y"' . $ckd1 . '>';
                    
                    echo '</td>';
            
                    if ($act == "on") {
                        echo '<td align=center>';
                        if (($QuestionTypeID != 99) && ($QuestionTypeID != 98)) {
                            echo '<input type="checkbox" name="' . $QuestionID . '-R" id="' . $QuestionID . '-R" value="Y"' . $ckd2 . '  class="chk_required">';
                        }
                        echo '</td>';
                    } // end act on
            
                    echo "<td align='center'>";
            
                    if (substr ( $QuestionID, 0, 4 ) == "CUST") {
            	
                        $script_source = pathinfo ( $_SERVER ['SCRIPT_NAME'] );
                	
                        if ($script_source ['basename'] == "administration.php")
                            echo "<a onclick=\"return confirmDeleteFormQuestion('administration', '" . $form . "', '" . $_REQUEST ['action'] . "', '" . $section . "', '" . $QuestionID . "', '" . $Question . "', '".$_REQUEST['OnboardSource']."');\" href=\"javascript:void(0);\"><img border='0' title='Delete' src='" . IRECRUIT_HOME . "images/icons/cross.png'></a>";
                        else
                            echo "<a onclick=\"return confirmDeleteFormQuestion('forms', '" . $form . "', '" . $_REQUEST ['action'] . "', '" . $section . "', '" . $QuestionID . "', '" . $Question . "');\" href=\"javascript:void(0);\"><img border='0' title='Delete' src='" . IRECRUIT_HOME . "images/icons/cross.png'></a>";
    				} else {
                        echo "-";
    				}
    				
    				echo "</td>";
            
                    echo '</tr>';
            
                    if ($rowcolor == "#eeeeee") {
                        $rowcolor = "#ffffff";
                    } else {
                        $rowcolor = "#eeeeee";
                    }
                } // end foreach
            }
            
            echo '<tr id="'.htmlspecialchars($OrgID)."*NoSort*".rand().uniqid().'">';
            echo '<td valign="middle" align="left" colspan="6" height="60">';
    	    echo '<a href="' . $formscript . '?form=' . $form . '&action=onboardquestions&new=add&OnboardSource='.htmlspecialchars($_REQUEST['OnboardSource']).'">';
    	    echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Custom Question</b>';
    	    echo '</a>';
    	    echo '</td>';
    	    echo '</tr>';
            
            echo '<tr id="'.$OrgID."*NoSort*".rand().uniqid().'">';
            echo '<td valign="middle" align="center" colspan="6" height="60">';
            
    		echo '<div style="text-align:center;color:red" id="synchronize_msg_cloudpayroll"></div>';
        
    		echo '<input type="hidden" name="action" value="onboardquestions">';
    		if ($formtable == "FormQuestions") {
    			echo '<input type="hidden" name="section" value="' . $section . '">';
    		}
    		
    		echo '<input type="hidden" name="form" value="' . $form . '">';
    		echo '<input type="hidden" name="process" value="Y">';
        	echo '<input type="hidden" name="OnboardSource" value="'.$_REQUEST['OnboardSource'].'">';
            
            echo '<input type="submit" value="Change Status" class="btn btn-primary">';
            
            echo '</td>';
            echo '</tr>';
            
            echo '</tbody>';
    		echo '</table>';
    		echo '</form>';
            
		}
		
		
		echo '</div>';
	} // end form questions
	
	if ($action == 'textblocks') {
		include IRECRUIT_VIEWS . 'forms/TextBlocks.inc';
	} // end text blocks
} // end else questionid
?>
