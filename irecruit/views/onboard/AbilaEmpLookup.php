<?php
require_once '../../Configuration.inc';

$url = "https://headstart.mylightworksoftware.com/lightworkapitest";
$userID = "iRecruitApps";
$password = "iRecruitAppsPass1!";
$csrfToken = "05e2973a4e02a718d6bb8170be63a141db33e92f7997e38079ea84c69253a175";

$ApplicationID = $_REQUEST['ApplicationID'];
$RequestID = $_REQUEST['RequestID'];
$firstName = $_REQUEST['first'];
$middleName = $_REQUEST['middle'];
$lastName = $_REQUEST['last'];
//$companyID = $OrgID;

$empNo = "Error: ";

$ch = curl_init();
$post_fields = '{ "userId": "' . $userID . '", "password": "' . $password . '", "csrfToken": "' . $csrfToken . '"}';
curl_setopt($ch, CURLOPT_URL, $url . "/api/v1/LwSystem/Sessions/Login");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
$result = curl_exec($ch);
$response = json_decode($result,true);
$sessionHash = "";
if ($response['Success'] == 1) {
        $sessionHash = $response[ResponseDetail][SessionInfo][SessionHash];
        //echo "\n" . $response['ResponseHeader'] . "\n";
}
curl_close($ch);


if ($sessionHash != "") {

$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $requestID, $ApplicationID);

//Set where condition
$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$requestID, ":PreFilledFormID"=>$CURRENT['I9']);
//Get PrefilledFormData
$results = G::Obj('FormData')->getPreFilledFormData("*", $where, "", "", array($params));

$DATA = array ();

if(is_array($results['results'])) {
  foreach ($results['results'] as $FORMDATA) {
    $DATA [$FORMDATA ['QuestionID']] = $FORMDATA ['Answer'];
  }
}

$ssn = implode("-",json_decode($DATA['SSN']));
$dob = $DATA['DOB'];

  $ch = curl_init();
  $post_fields = '{ "sessionHash": "' . $sessionHash . '", "csrfToken": "' . $csrfToken . '", "employees": [ { "recordID": "' . $ApplicationID. '", "firstName": "' . $firstName . '", "middleName": "' . $middleName . '", "lastName": "' . $lastName . '", "ssn": "' . $ssn . '", "dateOfBirth": "' . $dob . '", "companyID": "' . $companyID . '"} ]}';
  curl_setopt($ch, CURLOPT_URL, $url . "/api/v1/LwHumanResources/Employees/GetNextEmployeeNumbers");
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
  $result = curl_exec($ch);
  $response = json_decode($result,true);
  if ($response['ResponseDetail'][0]['Error'] != "") {
	$empNo .= $response['ResponseDetail'][0]['Error'];
  } else {
        $empNo = $response[ResponseDetail][0]['EmployeeNumber'];
        $newHire = $response[ResponseDetail][0]['IsNewHire'] . "\n";
	//echo "\nEmp No. " . $empNo;
	//echo "  New Hire: " . $newHire . "\n";
  }
  curl_close($ch);

  $ch = curl_init();
  $post_fields = '{ "sessionHash": "' . $sessionHash . '", "csrfToken": "' . $csrfToken . '" }';
  curl_setopt($ch, CURLOPT_URL, $url . "/api/v1/LwSystem/Sessions/Logout");
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/json'));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
  $result = curl_exec($ch);
  $response = json_decode($result,true);
  if ($response['Success'] == 1) {
        //echo "\n" . $response['ResponseHeader'] . "\n";
  }
  curl_close($ch);

} // end sessionHash

$dupcnt = G::Obj('OnboardFormData')->checkDuplicateOnboardAnswer($OrgID, "Abila", "Sage_EmployeeNumber", $empNo);

if ($dupcnt > 0) {
	$empNo = "DUPLICATE-".$empNo;
} 

echo $empNo;

?>
