<div class="panel-body">
	<div class="row">
		<div class="col-lg-6">
			<form role="form" method="post">
				<input type="hidden" name="tab_action" value="userconfig"> 
				<div class="form-group">
					<label><font color="red">*</font>UserID:</label>
					<p class="form-control-static">
	            	<?php
						global $OrgID;
						echo $USERID;
					?>
		            </p>
				</div>
				<div class="form-group">
					<label><font color="red">*</font>Verification:</label>
					<p class="form-control-static">
						<input type="text" name="UsersVerification" maxlength="45" size="20" class="form-control width-auto-inline">
					</p>
				</div>
						<input type="hidden" name="Role" value="<?php echo $user_info['Role']; ?>">
				<div class="form-group">
					<label><font color="red">*</font>Processing View:</label> 
					<p class="form-control-static">
						<select name="UsersAppView" class="form-control width-auto-inline">
							<option value="M" <?php if($user_info['ApplicationView'] == "M") echo 'selected="selected"';?>>Multiple</option>
							<option value="S" <?php if($user_info['ApplicationView'] == "S") echo 'selected="selected"';?>>Single</option>
						</select>
					</p>
				</div>
				<div class="form-group">
					<label><font color="red">*</font>First Name:</label> 
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['FirstName'];?>" name="UsersFirstName" maxlength="45" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label><font color="red">*</font>Last Name:</label> 
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['LastName'];?>" name="UsersLastName" maxlength="60" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>Title:</label> 
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['Title'];?>" name="UsersTitle" maxlength="60" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label><font color="red">*</font>Email Address:</label>
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['EmailAddress'];?>" name="UsersEmailAddress" maxlength="60" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>Address 1:</label> 
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['Address1'];?>" name="UsersAddress1" maxlength="45" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>Address 2:</label> 
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['Address2'];?>" name="UsersAddress2" maxlength="45" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>City:</label>
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['City'];?>" name="UsersCity" maxlength="45" size="45" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>State:</label>
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['State'];?>" name="UsersState" maxlength="2" size="2" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>Zip Code:</label>
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['ZipCode'];?>" name="UsersZipCode" maxlength="10" size="12" class="form-control width-auto-inline">
					</p>
				</div>
				<div class="form-group">
					<label>Phone:</label>
					<p class="form-control-static">
						<input type="text" value="<?php echo $user_info['Phone'];?>" name="UsersPhone" maxlength="20" size="12" class="form-control width-auto-inline">
					</p>
				</div>
				<?php
				//Get Twilio Account Information
				$account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
				
				if($account_info['AccountSid'] != "") {
				    ?><input type=hidden size=12 maxlength=20 name=UsersCell id=UsersCell value="<?php echo $user_info['Cell'];?>" /><?php
				}
				else {
				    ?>
					<div class="form-group">
    					<label>Cell:</label>
    					<p class="form-control-static">
    						<input type="text" value="<?php echo $user_info['Cell'];?>" name="UsersCell" maxlength="20" size="12" class="form-control width-auto-inline">
    					</p>
    				</div>
					<?php
				}
				?>
				<div class="form-group">
					<label>Activation Date:</label>
					<p class="form-control-static">
					<?php echo $user_info['ActivationDate'];?>
					</p>
				</div>
				<div class="form-group">
					<label>Last Access::</label>
					<p class="form-control-static">
					<?php echo $user_info['LastAccess'];?>
					</p>
				</div>
				<input type="hidden" value="<?php echo $USERID;?>" name="UserIDedit">
				<input type="hidden" value="<?php echo $OrgID;?>" name="OrgID"> 
				<input type="hidden" value="Users" name="process"> 
				<input type="hidden" value="useredit" name="action">
				<input type="hidden" value="profile" name="useraction">
				<input type="submit" class="btn btn-primary" value="Update User Information">
			</form>
		</div>
	</div>
</div>
