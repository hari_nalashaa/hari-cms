<div class="col-lg-6 col-md-6 col-sm-6 mdsortable"
	id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-bordered table-hover"
			id="widget_body_<?php echo $ModuleId;?>">
			<tr>
				<td>
					<P>
						Please <a
							href="http://help.irecruit-us.com/questions/" target="_blank">click
							here to email iRecruit Support</a> or call us at 1-800-517-9099.
					</P>
					<P>
						Click here to join a <a target="_blank"
							href="https://cmshris.webex.com">WebEx Support</a> session.
					</P>
					<P>
						<a target="_blank" href="http://help.irecruit-us.com/"><i
							class="fa fa-info-circle"></i> Click here for the iRecruit Help
							File.</a>
					</P>
					<P>
						<a target="_blank" href="http://www.irecruit-software.com">www.irecruit-software.com</a>
					</P>
					<P>
						Connect with us: <a
							href="https://www.linkedin.com/company/irecruit" target="_blank"><img
							src="<?php echo IRECRUIT_HOME;?>images/linkedin.png"
							style="margin: 0 4px -3px 0; width: 30px;"></a><a
							href="https://www.facebook.com/pages/Irecruit/143250849064469"
							target="_blank"><img src="<?php echo IRECRUIT_HOME;?>images/facebook.png"
							style="margin: 0 4px -3px 0; width: 30px;"></a><a
							href="https://twitter.com/cmsirecruit" target="_blank"><img
							src="<?php echo IRECRUIT_HOME;?>images/twitter.png"
							style="margin: 0 4px -3px 0; width: 30px;"></a>
					</P>
				</td>
			</tr>
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->