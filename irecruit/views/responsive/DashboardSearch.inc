<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-hover" id="widget_body_<?php echo $ModuleId;?>">
			
				<tr class="odd">
					<td>
						<div>
							<form method="post" name="frmQuickNameSearch" id="frmQuickNameSearch">
								<input type="hidden" name="sort_field" id="sort_field" value="entry_date">
								<input type="hidden" name="sort_order" id="" value="desc">
								
								<input type="text" name="applicants_keyword" id="applicants_keyword" class="form-control width-auto-inline" placeholder="Enter, name or keyword to search">
								<?php 
								if($user_preferences['DashboardWidgetsDateRange'] == "") $user_preferences['DashboardWidgetsDateRange'] = "30";
								$columns = "Title, RequestID";
								if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
									$RequestID 			= G::Obj('RequisitionDetails')->getHiringManagerRequestID();
									$quick_search_where  = array("OrgID = :OrgID", "Active = 'Y'", "RequestID IN ('" . implode("','",$RequestID) . "')");
								}else{
									$quick_search_where  = array("OrgID = :OrgID", "Active = 'Y'");
								}
								
								$quick_search_params = array(":OrgID"=>$OrgID);
								$active_requisitions = $RequisitionsObj->getRequisitionInformation($columns, $quick_search_where, "", "DateEntered DESC, Title DESC", array($quick_search_params));
								$ar_list = $active_requisitions['results'];
								?>
								<select name="ddlActiveRequisitions" id="ddlActiveRequisitions" class="form-control width-auto-inline" style="max-width:300px;">
									<option value="">All Requisitions</option>
									<?php
									for($ari = 0; $ari < count($ar_list); $ari++) {
										?><option value="<?php echo $ar_list[$ari]['RequestID']?>"><?php echo $ar_list[$ari]['Title']?></option><?php
									}
									?>
								</select>
								
								From: <input type="text" name="quicksrch_from_date" id="quicksrch_from_date" value="<?php echo date("m/d/Y", strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));?>" class="form-control width-auto-inline">
								To: <input type="text" name="quicksrch_to_date" id="quicksrch_to_date" value="<?php echo date("m/d/Y", strtotime("+1 day"));?>" class="form-control width-auto-inline">
								
								<input type="button" name="search" id="search" class="btn btn-primary" value="Search" onclick="getApplicantsByKeyword();">
							</form>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding: 0px">
						<div id="applicants_list" style="text-align: center"></div>
					</td>
				</tr>
		</table>
		
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->