<?php
global $whitelist;

$mindex = 0;

$VBarChartStatus        =   false;
$HBarChartStatus        =   false;
$include_home_modules   =   [];

/*
for($mi = 0; $mi < count ( $usermodulesinfo ); $mi ++) {
    $ModuleId       =   $usermodulesinfo [$mi] ['ModuleId'];
    $FileName       =   $DahboardMainModules [$ModuleId];

    //Get Twilio Account Information
    $account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
    
    if($FileName == 'DashboardTwilioConversationApplicants.inc') {
        if($account_info['AccountSid'] != "") {
            $include_home_modules[]    =   IRECRUIT_VIEWS . 'responsive/'. $FileName;
        }
    }
    else {
        $include_home_modules[]    =   IRECRUIT_VIEWS . 'responsive/'. $FileName;
    }
}
*/

for($mi = 0; $mi < count ( $usermodulesinfo ); $mi ++) {
	$ModuleId = $usermodulesinfo [$mi] ['ModuleId'];
	$FileName = $DahboardMainModules [$ModuleId];
	
	if($FileName != "") {
		if($ModuleId == 10 && $usermodules [$ModuleId] == 1) {
			$VBarChartStatus = true;
		}
		if($ModuleId == 11 && $usermodules [$ModuleId] == 1) {
			$HBarChartStatus = true;
		}
		if ($usermodules [$ModuleId] == 1) {
			$mindex ++;
			
			$blocktitle = $dashboardmodules [$ModuleId];
			if ($FileName == "DashboardPieChartApplications.inc") {
				$listitem = "listitem_" . $ModuleId;
				$blocktitle = str_replace ( "Pie Chart ", "", $dashboardmodules [$ModuleId] );
					
				/*
				* Already array is there with blockid keys of 1,2,3,4
				* i don't wan't to duplicate that because of keys,
				* so i have removed 4 to make it work based on id's
				* to make the javascript validation i am using this selected block id's
				* in array to make it work perfectly
				*/
				$blockid = $ModuleId - 4;
				$blockidslist[] = $blockid;
					
				$piechartid = $piechartids [$blockid];
			}
		
			//Get Twilio Account Information
			$account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

			//Include Home Module File Name
			//include $include_home_modules [$ModuleId];
			
            if($FileName == 'DashboardTwilioConversationApplicants.inc') {
                 if($account_info['AccountSid'] != "") {
                	 if (isset($whitelist)
                		 && in_array(IRECRUIT_VIEWS, $whitelist)
                		 && in_array($FileName, $DahboardMainModules)
                		 && defined('IRECRUIT_VIEWS')
                		 && isset($FileName)
                		 && file_exists(IRECRUIT_VIEWS . 'responsive/'. $FileName)) {
                	     include IRECRUIT_VIEWS . 'responsive/'. $FileName;
                	 }
                 }
            }
            else {
                 if (isset($whitelist)
                	 && in_array(IRECRUIT_VIEWS, $whitelist)
                	 && in_array($FileName, $DahboardMainModules)
                	 && defined('IRECRUIT_VIEWS')
                	 && isset($FileName)
                	 && file_exists(IRECRUIT_VIEWS . 'responsive/'. $FileName)) {
                         include IRECRUIT_VIEWS . 'responsive/'. $FileName;
                 }
            }
			
			if ($mindex == 2)
				echo "<div class='clearfix'></div>";
		}
	}
	
}?>
