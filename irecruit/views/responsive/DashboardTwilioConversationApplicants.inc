<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-hover" id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
			
				<tr class="odd">
					<td>
						<div>
							<form method="post" name="frmTwilioQuickNameSearch" id="frmTwilioQuickNameSearch">
								<input type="hidden" name="twilio_sort_field" id="twilio_sort_field" value="entry_date">
								<input type="hidden" name="twilio_sort_order" id="twilio_sort_order" value="desc">
								<input type="text" name="twilio_applicants_keyword" id="twilio_applicants_keyword" class="form-control width-auto-inline" placeholder="Enter, name or keyword to search">
								
								From: <input type="text" name="twilio_search_from_date" id="twilio_search_from_date" value="<?php echo date("m/d/Y", strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));?>" class="form-control width-auto-inline">
								To: <input type="text" name="twilio_search_to_date" id="twilio_search_to_date" value="<?php echo date("m/d/Y", strtotime("+1 day"));?>" class="form-control width-auto-inline">
								
								<input type="button" name="btnTwilioApplicantsSearch" id="btnTwilioApplicantsSearch" class="btn btn-primary" value="Search" onclick="getTwilioApplicants();">
							</form>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding: 0px">
						<div id="twilio_applicants_list" style="text-align: center"></div>
					</td>
				</tr>
		</table>
		
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->