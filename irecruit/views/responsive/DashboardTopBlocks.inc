<?php
$JSON['Active'] = 'Y';
$requisitionscount = $RequisitionDetailsObj->countRequistions(json_encode($JSON));
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12" id="drag_drop_notification">
		<div class="alert alert-info alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
				You can drag and drop the colored top blocks to sort. 
		</div>
	</div>
	
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div id="msgtopsortable" class="alert alert-success" style="display: none"></div>
	</div>	
</div>
<div class="row" id="topblocks">
<?php
$dutbi = 0;
$panelcolori = 0;
$block_sizes = array(3, 4, 3, 2);
shuffle($block_sizes);

while($dutbi < 4) {
	$blockorder        =   $dashboardusertopblocks[$dutbi]['BlockOrder'];
	$blockpanelcolor   =   $dashboardusertopblocks[$dutbi]['BlockPanelColor'];
	$panelcolor        =   array("1"=>"panel-primary", "2"=>"panel-green", "3"=>"panel-yellow", "4"=>"panel-red");
	$blockid           =   $dashboardusertopblocks[$dutbi]['BlockId'];
	$block_text        =   $dashboardusertopblocks[$dutbi]['BlockText'];
	
	/**
	 * @tutorial	Please update this text code also while you are updating the 
	 * 				blocks information in database. 
	 * 				It will be applicable for only new applicants
	 */
	
	if($block_text == "New Applications") {
		$new_applications_interval = $dashboardusertopblocks[$dutbi]['NewApplicationsInterval'];
	}
	if($dashboardusertopblocks[$dutbi]['BlockStatus'] == "Active") {
		$panelcolori++;

		/**
		 * @tutorial Link to that block
		 */
		if($blockid == 1) {
			$view_details = "location.href='".IRECRUIT_HOME . 'requisitionsSearch.php?Active=Y'."'";
		}
		else if($blockid == 2) {
			$view_details = "document.forms['frmTopBlockDetails'].submit()";
		}
		else if($blockid == 4) {
			$view_details = "location.href='". IRECRUIT_HOME . 'responsive/dailyAppointments.php'."'";
		}
		else if($blockid == 6) {
			$view_details = "location.href='". IRECRUIT_HOME . 'responsive/dailyReminders.php'."'";
		}
		else if($blockid == 7) {
			$view_details = "location.href='". IRECRUIT_HOME . 'responsive/hrToolbox.php'."'";
		}
		else {
			$view_details = "location.href='". IRECRUIT_HOME . 'applicants.php?action=calendartableview&menu=2'."'";
		}
		
		if($block_text == "New Applications") {
			$new_applicants_info = $ApplicationsObj->getRecentApplicationsCount();
			?>
			<form name="frmTopBlockDetails" id="frmTopBlockDetails" method="post" action="<?php echo IRECRUIT_HOME?>applicantsSearch.php">
				<input type="hidden" name="criteria" id="criteria" value="">
				<input type="hidden" name="OrgID" id="OrgID" value="">
				<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="">
				<input type="hidden" name="SearchWords" id="SearchWords" value="">
				<input type="hidden" name="type1" id="type1" value="1">
				<input type="hidden" name="refinereq" id="refinereq" value="no">
				<input type="hidden" name="Active" id="Active" value="B">
				<input type="hidden" name="ProcessOrder" id="ProcessOrder" value="1">
				<input type="hidden" name="Code" id="Code" value="">
				<input type="hidden" name="ApplicationDate_From" id="ApplicationDate_From" value="<?php echo $new_applicants_info['ApplicationDate_From'];?>">
				<input type="hidden" name="ApplicationDate_To" id="ApplicationDate_To" value="<?php echo $new_applicants_info['ApplicationDate_To'];?>">
				<input type="hidden" name="WSList" id="WSList" value="">
				<input type="hidden" name="LimitProcessed" id="LimitProcessed" value="N">
				<input type="hidden" name="action" id="action" value="search">
				<input type="hidden" name="process" id="process" value="Y">
				<input type="hidden" name="SearchName" id="SearchName" value="">
			</form>
			<?php
		}
		?>
		<div class="col-lg-<?php echo $block_sizes[$dutbi]?> col-md-6" id="listitem_<?php echo $blockid.$blockpanelcolor;?>" onclick="<?php echo $view_details;?>">
			<div class="panel <?php echo $panelcolor[$blockpanelcolor];?>">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-6" style="height:80px">
							<?php 
								if($dashboardusertopblocks[$dutbi]['ShowImage'] == "Yes") {
									?><i class="fa <?php echo $dashboardusertopblocks[$dutbi]['BlockImageClass']?> fa-4x"<?php if($dashboardusertopblocks[$dutbi]['BlockId'] == 2) echo ' style="opacity:0.5"'?>></i><?php
								}
							?>
						</div>
						<div class="col-xs-6 text-right">
							<div class="huge">
								<?php 
									echo $DashboardConfigurationObj->getDashboardTopBlocksCount($blockid);
								?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 text-right">
							<?php echo $dashboardusertopblocks[$dutbi]['BlockText'];?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	
	$dutbi++;
}
?>
</div>
