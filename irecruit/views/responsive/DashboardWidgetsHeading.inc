<div class="panel-heading panel-heading-color">
	<span>
		<?php echo $blocktitle;?>
	</span>
	<?php 
	if(!isset($flag_to_close)) {
		?>
		<span class="pull-right toggle-arrow">
			<a href="javascript:void(0);" class="fa fa-chevron-down fa-2" id="widget_body_navigation_<?php echo $ModuleId;?>"></a>
			<a href="javascript:void(0);" class="fa fa-times fa-1" id="widget_body_close_<?php echo $ModuleId;?>"></a>
		</span>
		<?php
	}
	?>
</div>