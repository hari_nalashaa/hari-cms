<div id="openFormWindow" class="openForm">
	<div>
		<p style="text-align:right;">
			<a href="#closeForm" title="Close Window">
				<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
				<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
			</a>
		</p>
		
		<!-- 
			 This popup id is common for all widgets, 
			 So please don't change it based on type of widget 
		 -->	
		<div id="widget_popup_info">
		      
		
		</div>
	</div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
        
		<div id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
		<?php 
		$subscription_settings = $ZipRecruiterObj->getZipRecruiterSubscriptionSettingsInfo($OrgID);
		$subscriptions_count = $ZipRecruiterObj->getZipRecruiterSubscriptionsCount($OrgID);
		$remaining_subscriptions = $subscription_settings['TotalSubscriptions'] - $subscriptions_count;
		?>
		<br>
        <form name="frmZipRecruiterReqFilters" id="frmZipRecruiterReqFilters" method="post">
		    <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
		    <input type="hidden" name="OrgID" id="OrgID" value="<?php echo htmlspecialchars($OrgID);?>">
            <input type="hidden" name="IndexStart" id="IndexStart" value="0">
            <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo htmlspecialchars($ZipRecruiterReqLimit);?>">
            <input type="hidden" name="CurrentPage" id="CurrentPage" value="1"> 
			&nbsp;From: 
			<input type="text" name="zip_recruiter_req_from_date" id="zip_recruiter_req_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
			To: 
			<input type="text" name="zip_recruiter_req_to_date" id="zip_recruiter_req_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
			<input type="text" name="search_word" id="search_word" class="form-control width-auto-inline" placeholder="Title,RequisitionID,JobID">

			<select name="ddlSelZipRecruiter" id="ddlSelZipRecruiter" class="form-control width-auto-inline">
			     <option value="All">All</option>
			     <option value="Paid">Paid</option>
			     <option value="Free">Free</option>
			     <option value="Subscribed">Subscribed</option>
			</select>
			
			<input type="button" class="btn btn-primary" name="btnRefineRequisitions" id="btnRefineRequisitions" onclick="getRequisitionsList()" value="Refine">
			
			<br>
		</form>
	    <form id="frmRequisitionsSortOptions" name="frmRequisitionsSortOptions" method="post">
	         <input type="hidden" name="ddlToSort" id="ddlToSort" value="date_opened">
	         <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
	    </form>
        <?php //echo '<h4>&nbsp;Subscriptions available '.$remaining_subscriptions.' out of '.$subscription_settings['TotalSubscriptions'].'.</h4>';?>
        <br>
	    <form name="frmDashboardZipRecruiter" id="frmDashboardZipRecruiter">
		<table class="table table-striped table-bordered table-hover" id="data-zip-recruiter-req">
        			<?php
        			$zip_recruiter_req_count     =   count($zip_recruiter_req_info);
        			$zip_recruiter_req_results   =   $zip_recruiter_req_info['results'];
        			?>
					<thead>
            			<tr> 
            			    <td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('req_title')"><strong>Title</strong></a></td>
            			    <td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('req_id')"><strong>RequisitionID</strong></a> / <a href="javascript:void(0)" onclick="sortRequisitions('job_id')"><strong>JobID</strong></a></td>
                			<td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('date_opened')"><strong>Date<br>Opened</strong></a></td>
                			<td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('expire_date')"><strong>Expire Date</strong></a></td>
                            <td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('days_open')"><strong>Address Information</strong></a></td>
                            <td align="center"><strong>Free ZipRecruiter</strong></td>
                            <td align="center"><strong>Paid ZipRecruiter</strong></td>
                        </tr>
                    </thead>
                    
                    <tbody>
                    <?php
                    if (is_array($zip_recruiter_req_results)) {

                        foreach ($zip_recruiter_req_results as $REQS) {
                            
                            $req_multi_org_id       =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $REQS['RequestID']);
                            $zip_recruiter_info     =   $ZipRecruiterObj->getZipRecruiterFeed($OrgID, $REQS['RequestID']);
                            
                            echo '<tr>';

                            echo "<td>";
                            echo "<a href='".IRECRUIT_HOME."jobBoardPosting.php?menu=3&RequestID=".$REQS['RequestID']."' target='_blank'>";
                            echo $RequisitionDetailsObj->getJobTitle($OrgID, $req_multi_org_id, $REQS['RequestID']);
                            echo "</a>";
                            echo "</td>";

                            echo "<td>";
                            echo $RequisitionDetailsObj->getReqJobIDs($OrgID, $req_multi_org_id, $REQS['RequestID']);
                            echo "</td>";
                            
							echo "<td>";
							echo $REQS['PostDate'];
							echo "</td>";
						
                            echo "<td>";
                            echo $REQS['ExpireDate'];
                            echo "</td>";

                            if(!isset($REQS['Province'])) $REQS['Province'] = '';
                            echo "<td align='left'>".$AddressObj->formatAddress($OrgID, $REQS['Country'], $REQS['City'], $REQS['State'], $REQS['Province'], $REQS['ZipCode'], '')."</td>";
                            
                            $free_jobboard_lists = json_decode($REQS['FreeJobBoardLists'], true);
                            
                            $free_ziprecruiter = "0";
                            
                            if(is_array($free_jobboard_lists) && in_array("ZipRecruiter", $free_jobboard_lists)) {
                                $free_ziprecruiter = "1";
                            }

                            if($free_ziprecruiter == "1") {
                                echo "<td align='center'>Yes</td>";
                            }
                            else {
                                echo "<td align='center'>&nbsp;</td>";
                            }
                            
                            if(isset($zip_recruiter_info['Email']) && $zip_recruiter_info['Email'] != "") {
                                echo "<td align='center'><a href='#openFormWindow' onclick=\"getPaidZipRecruiterInfo('".$REQS['RequestID']."')\">Details</a></td>";
                            }
                            else {
                                echo "<td align='center'>&nbsp;</td>";
                            }
                            
                            echo "</tr>";
                            
                        } // end foreach
                    }
                    ?>
                   </tbody>
	             </table>
			 <input type="hidden" name="sort_dashboard_req" id="sort_dashboard_req" value="A">
		</form>
		 
		  </div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<script>
function sortRequisitions(to_sort) {
	var IndexStart =   document.frmZipRecruiterReqFilters.IndexStart.value;

	document.frmRequisitionsSortOptions.ddlToSort.value = to_sort;
	
	var sort_type  =   document.frmRequisitionsSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmRequisitionsSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmRequisitionsSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var from_date  =   document.frmZipRecruiterReqFilters.zip_recruiter_req_from_date.value;
	var to_date    =   document.frmZipRecruiterReqFilters.zip_recruiter_req_to_date.value;
	var keyword    =   document.frmZipRecruiterReqFilters.search_word.value;
				
	var REQUEST_URL = irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-zip-recruiter-req").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-zip-recruiter-req").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
}

function getRecordsByPage(IndexStart) {
	document.frmZipRecruiterReqFilters.IndexStart.value = IndexStart;

	var to_sort    =   document.frmRequisitionsSortOptions.ddlToSort.value;
	var sort_type  =   document.frmRequisitionsSortOptions.ddlSortType.value;
	var from_date  =   document.frmZipRecruiterReqFilters.zip_recruiter_req_from_date.value;
	var to_date    =   document.frmZipRecruiterReqFilters.zip_recruiter_req_to_date.value;
	var keyword    =   document.frmZipRecruiterReqFilters.search_word.value;

	var REQUEST_URL = irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-zip-recruiter-req").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
		
			$("#data-zip-recruiter-req").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
	
}

function getRequisitionsByPageNumber(page_number) {
	
	var RecordsLimit = document.frmZipRecruiterReqFilters.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}


function setRequisitionsList(data) {
	var RecordsLimit = document.frmZipRecruiterReqFilters.RecordsLimit.value;
	
	var app_prev         = data.previous;
	var app_next         = data.next;
	var total_pages      = data.total_pages;
	var current_page     = data.current_page;
	var requisitions_count = data.requisitions_count;
	
	$("#data-zip-recruiter-req").find("tr:gt(0)").remove();
	
	var data_length = data['results'].length;
    var requisitions_list = data['results'];
    var req_results = "";
    
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {
            
			var Title                    =   requisitions_list[i].Title;
			var ReqIDJobID               =   requisitions_list[i].RequisitionID + "/" + requisitions_list[i].JobID;
			var PostDateByFeature        =   requisitions_list[i].PostDateByFeature;
			var ExpireDate               =   requisitions_list[i].ExpireDate;
			var Open                     =   requisitions_list[i].Open;
			var InternalRequisition      =   requisitions_list[i].InternalRequisition;
			var AddressInformation       =   requisitions_list[i].AddressInformation;
			var FreeZipRecruiter         =   requisitions_list[i].FreeZipRecruiter;
			var PaidZipRecruiter         =   requisitions_list[i].PaidZipRecruiter;
			var SubscribedZipRecruiter   =   requisitions_list[i].ZipRecruiterSubscription;
			var RequestID                =   requisitions_list[i].RequestID;
			var internal_style           =   '';
						
			if(Title == null) Title = '';
			if(ReqIDJobID == null) ReqIDJobID = '';
			if(ExpireDate == null) ExpireDate = '';
			if(Open == null) Open = '0';
			if(typeof(PostDateByFeatureYMD) == 'undefined' || PostDateByFeatureYMD == null) PostDateByFeatureYMD = '';
			if(typeof(ExpireDateYMD) == 'undefined' || ExpireDateYMD == null) ExpireDateYMD = '';
			if(InternalRequisition == 'Y') internal_style = ' style="color:red"';
			
            req_results  = "<tr>";
            req_results += "<td valign='top'>";
            req_results += "<a href='"+irecruit_home+"jobBoardPosting.php?menu=3&RequestID="+RequestID+"' target='_blank'>";
            req_results += Title;
            req_results += "</a>";
            req_results += "</td>";
            req_results += "<td valign='top'>"+ReqIDJobID+"</td>";
            req_results += "<td valign='top'"+internal_style+">"+PostDateByFeature+"</td>";
            req_results += "<td valign='top'>"+ExpireDate+"</td>";
            req_results += "<td valign='top'>"+AddressInformation+"</td>";

			req_results += "<td valign='top'>"+FreeZipRecruiter+"</td>";
			if(PaidZipRecruiter == "Yes") {
				req_results += "<td valign='center'><a href='#openFormWindow' onclick=\"getPaidZipRecruiterInfo('"+RequestID+"')\">Details</a></td>";
			}
			else {
				req_results += "<td valign='center'></td>";
			}
			
			if(SubscribedZipRecruiter == "Yes") {
				req_results += "<td valign='center'>Yes</td>";
			}
			else {
				req_results += "<td valign='center'></td>";
			}
			
			req_results += "</tr>";

			$("#data-zip-recruiter-req").append(req_results);
		}

	}
	else {

		req_results  = "<tr>";
	    req_results += "<td colspan='8' align='center'>No records found</td>";
		req_results += "</tr>";

		$("#data-zip-recruiter-req").append(req_results);
	}

}

function getPaidZipRecruiterInfo(RequestID) {
	var REQUEST_URL = irecruit_home + "requisitions/getPaidZipRecruiterInfo.php?RequestID="+RequestID;

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		beforeSend: function(data) {
    			$("#widget_popup_info").html("Please wait...");
        	},
        	success: function(data) {
        		$("#widget_popup_info").html(data);
        	}
	});
}

function getRequisitionsList() {
	var RecordsLimit   =   document.getElementById('RecordsLimit').value;

	var IndexStart     =   document.frmZipRecruiterReqFilters.IndexStart.value = 0;
	var to_sort        =   document.frmRequisitionsSortOptions.ddlToSort.value;
	var sort_type      =   document.frmRequisitionsSortOptions.ddlSortType.value;
	var from_date      =   document.frmZipRecruiterReqFilters.zip_recruiter_req_from_date.value;
	var to_date        =   document.frmZipRecruiterReqFilters.zip_recruiter_req_to_date.value;
	var keyword        =   document.frmZipRecruiterReqFilters.search_word.value;
	var sel_zip_rec    =   document.frmZipRecruiterReqFilters.ddlSelZipRecruiter.value;;
	
	document.frmZipRecruiterReqFilters.CurrentPage.value = 1;	
	
	var REQUEST_URL = irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?IndexStart="+IndexStart+"&RecordsLimit="+RecordsLimit+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword;
    
	if(sel_zip_rec == 'All') {
		REQUEST_URL += '&SubscribeZipRecruiter=Yes&PaidZipRecruiter=Yes&FreeZipRecruiter=Yes';
	}
	else if(sel_zip_rec == 'Paid') {
		REQUEST_URL += '&PaidZipRecruiter=Yes';
	}
	else if(sel_zip_rec == 'Free') {
		REQUEST_URL += '&FreeZipRecruiter=Yes';
	}
	else if(sel_zip_rec == 'Subscribed') {
		REQUEST_URL += '&SubscribeZipRecruiter=Yes';
	}

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		dataType: 'json',
    		beforeSend: function() {
    			
    			$("#data-zip-recruiter-req").find("tr:gt(0)").remove();
    			requisition_details_loading_status	 =	"<tr>";
    			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
    			requisition_details_loading_status	+=	"</tr>";
    			
    			$("#data-zip-recruiter-req").append(requisition_details_loading_status);
    			
    	},
    	success: function(data) {
    		setRequisitionsList(data);
    	}
	});
}
</script>