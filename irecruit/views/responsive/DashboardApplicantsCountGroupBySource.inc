<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include IRECRUIT_DIR . 'views/responsive/DashboardWidgetsHeading.inc';?>
	       <?php 
	       if($user_preferences['DashboardWidgetsDateRange'] == "") $user_preferences['DashboardWidgetsDateRange'] = "30";
	       $where_info         =   array("OrgID = :OrgID");
	       $params_info        =   array(":OrgID"=>$OrgID);
	       $req_results        =   $RequisitionsObj->getRequisitionInformation ( "RequestID, Title, RequisitionID, JobID", $where_info, "", " Requisitions.PostDate", array ($params_info) );
	       $requisitions_list  =   $req_results['results'];
	       ?>
	       <table class="table table-striped table-bordered table-hover">
	           <tr>
	               <td colspan="2">
	                   Status:
        			   <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline" onchange="getDashboardModuleReqByStatus(this.value)">
        					<option value="A">All</option>
        					<option value="Y">Active</option>
        					<option value="N">Inactive</option>
        			    </select>
                        Requisitions: 
                        <select name="ddlRequisitionsList" id="ddlRequisitionsList" class="form-control width-auto-inline" style="max-width:200px">
                          <option value="">Select</option>
                          <?php
            	           for($r = 0; $r < count($requisitions_list); $r++) {
            	           	   ?><option value="<?php echo $requisitions_list[$r]['RequestID']?>"><?php echo $requisitions_list[$r]['Title']." - ".$requisitions_list[$r]['RequisitionID']." / ".$requisitions_list[$r]['JobID'];?></option><?php
            	           }
            	           ?>
                        </select>
	               </td>
	           </tr>
	           
	           <tr>
	               <td colspan="2">
       	               <strong>Date Range:</strong>
        			   &nbsp;
        			   <input type="text" name="app_src_from_date" id="module_from_date<?php echo htmlspecialchars($ModuleId);?>" style="width:100px" value="<?php echo date('m/d/Y', strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));?>"> - 
        			   <input type="text" name="app_src_to_date" id="module_to_date<?php echo htmlspecialchars($ModuleId);?>" style="width:100px" value="<?php echo date('m/d/Y')?>">
        			   <input type="button" name="generate_app_src_graph" id="generate_app_src_graph" value="Submit" class="btn btn-primary btn-sm" onclick="getApplicantsSourceCountByRequestID($('#ddlRequisitionsList').val(), $('#module_from_date<?php echo htmlspecialchars($ModuleId);?>').val(), $('#module_to_date<?php echo htmlspecialchars($ModuleId);?>').val(), $('#ddlActive').val());">
	               </td>
	           </tr>
	           <tr>
	               <td colspan="2" style="padding: 0px">
	                  <div class="flot-chart">
        				<div class="flot-chart-content" id="applicants_count_by_req_and_source"></div>
        			  </div>
	               </td>
	           </tr>
	       </table>		
	</div>
</div>