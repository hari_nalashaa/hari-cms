<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-hover" id="widget_body_<?php echo $ModuleId;?>">
			<?php
				$news_feed_url = "https://help.irecruit-us.com/feed/";
				$news_feed_xml = $CurlObj->get($news_feed_url);
				$irecruit_news = simplexml_load_string ( $news_feed_xml, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
				$items_list = $irecruit_news->channel->item;
				
				for($ni = 0; $ni < 5; $ni++) {
					?>
					<tr class="odd">
						<td>
						<a href="<?php echo $items_list[$ni]->link;?>" target="_blank"><strong><?php echo $items_list[$ni]->title;?></strong></a>
						<br>
						<strong><?php 
							$date_info = explode("+", $items_list[$ni]->pubDate);
							echo trim($date_info[0]);
						?></strong><br>
						<?php echo substr($items_list[$ni]->description, 0, 200)."..";?>
						</td>
					</tr>
					<?php
				}
			?>
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->
