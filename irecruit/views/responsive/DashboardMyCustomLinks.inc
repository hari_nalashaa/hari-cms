<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<?php
		$custom_links = $custom_links_info['results'];
		?>
		<table class="table table-striped table-bordered table-hover" id="widget_body_<?php echo $ModuleId;?>">
			<?php
				if(count ( $custom_links ) == 0) {
					?>
    				<tr class="odd">
                    	<td align="center">
                    		You haven't created any custom links.
                    	</td>
    				</tr>
    				<?php
				}
				else {
					for($r = 0; $r < count ( $custom_links ); $r ++) {
						?>
                       	<tr class="odd">
                        	<td>
		                    	<div>
                                    <a href="<?php echo $custom_links[$r]['LinkTarget']?>" target="_blank"><?php echo $custom_links[$r]['LinkTitle']?></a>
	                    		</div> 
							</td>
						</tr>
						<?php
					}	
				}
				?>
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->