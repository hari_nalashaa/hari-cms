<?php
$msg = '';

?>
<div class="table-responsive">
<form name="frmUserPreferences" id="frmUserPreferences" method="post">
	<input type="hidden" name="tab_action" value="userconfig"> 
	<input type="hidden" name="action" id="action" value="userpreferences">
	<table width="100%" cellpadding="5" cellspacing="5" class="table table-striped table-bordered table-hover">
		<?php 
		if(isset($_GET['msg']) && $_GET['msg'] != "") {
			?>
			<tr>
				<td colspan="2" style="color: red">
					<?php
						echo 'Preferences Updated Successfully';
					?>
				</td>
			</tr>
			<?php
		}
		?>
		<tr>
			<td width="35%">Theme</td>
			<td>
				<select name="ThemesList" id="ThemesList" class="form-control" style="width: 150px;">
					<?php
					for($i = 0; $i < count($themesList); $i++) {
						?><option value="<?php echo $themesList[$i]['ThemeID']?>" <?php if($userPreferences['ThemeID'] == $themesList[$i]['ThemeID']) echo "selected='selected'";?>><?php echo $themesList[$i]['ThemeName']?></option><?php
					}
					?>
				</select>				
			</td>
		</tr>
		
		<tr>
			<td>Add reminder to preferred calendar?</td>
			<td>
				Yes&nbsp;<input type="radio" name="ReminderStatus" id="ReminderStatusYes" value="Yes" <?php if($userPreferences['ReminderTypeStatus'] == "Yes") echo "checked='checked'";?>/>&nbsp;
				No&nbsp;<input type="radio" name="ReminderStatus" id="ReminderStatusNo" value="No" <?php if($userPreferences['ReminderTypeStatus'] == "No") echo "checked='checked'";?>/>
			</td>
		</tr>
		<tr>
			<td>Calendar Type</td>
			<td>
				<select name="ReminderType" id="ReminderType" class="form-control" style="width: 150px;">
					<option value="Google" <?php if($userPreferences['ReminderType'] == "Google") echo "selected='selected'";?>>Google</option>
					<option value="OutLook" <?php if($userPreferences['ReminderType'] == "OutLook") echo "selected='selected'";?>>OutLook</option>
					<option value="Ical" <?php if($userPreferences['ReminderType'] == "Ical") echo "selected='selected'";?>>Ical</option>
					<option value="Lotus" <?php if($userPreferences['ReminderType'] == "Lotus") echo "selected='selected'";?>>Lotus</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Duration</td>
			<td>
				<select name="AppointmentDuration" id="AppointmentDuration" class="form-control" style="width: 150px;">
					<option value="15" <?php if($userPreferences['AppointmentDuration'] == "15") echo "selected='selected'";?>>15 Minutes</option>
					<option value="30" <?php if($userPreferences['AppointmentDuration'] == "30") echo "selected='selected'";?>>30 Minutes</option>
					<option value="45" <?php if($userPreferences['AppointmentDuration'] == "45") echo "selected='selected'";?>>45 Minutes</option>
					<option value="60" <?php if($userPreferences['AppointmentDuration'] == "60") echo "selected='selected'";?>>1 Hour</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Default Calendar View</td>
			<td>
				<select name="DefaultCalendarView" id="DefaultCalendarView" class="form-control" style="width: 150px;">
					<option value="Table" <?php if($userPreferences['DefaultCalendarView'] == "Table") echo "selected='selected'";?>>Table</option>
					<option value="List" <?php if($userPreferences['DefaultCalendarView'] == "List") echo "selected='selected'";?>>List</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Default Time Zone</td>
			<td>
				<select name="DefaultTimeZone" id="DefaultTimeZone" class="form-control" style="width: 150px;">
					<option value="America/New_York" <?php if($userPreferences['DefaultTimeZone'] == "America/New_York") echo 'selected="selected"';?>>EST</option>
					<option value="America/Chicago" <?php if($userPreferences['DefaultTimeZone'] == "America/Chicago") echo 'selected="selected"';?>>CST</option>
					<option value="America/Denver" <?php if($userPreferences['DefaultTimeZone'] == "America/Denver") echo 'selected="selected"';?>>MST</option>
					<option value="America/Los_Angeles" <?php if($userPreferences['DefaultTimeZone'] == "America/Los_Angeles") echo 'selected="selected"';?>>PST</option>
					<option value="America/Anchorage" <?php if($userPreferences['DefaultTimeZone'] == "America/Anchorage") echo 'selected="selected"';?>>AKST</option>
					<option value="America/Adak" <?php if($userPreferences['DefaultTimeZone'] == "America/Adak") echo 'selected="selected"';?>>HST</option>
					<option value="Atlantic/South_Georgia" <?php if($userPreferences['DefaultTimeZone'] == "Atlantic/South_Georgia") echo 'selected="selected"';?>>AST</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Search Date Range</td>
			<td>
				<select name="SearchDateRange" id="SearchDateRange" class="form-control" style="width: 150px;float:left">
					<option value="7" <?php if($userPreferences['DefaultSearchDateRange'] == "7") echo 'selected="selected"';?>>7</option>
					<option value="14" <?php if($userPreferences['DefaultSearchDateRange'] == "14") echo 'selected="selected"';?>>14</option>
					<option value="30" <?php if($userPreferences['DefaultSearchDateRange'] == "30") echo 'selected="selected"';?>>30</option>
					<option value="60" <?php if($userPreferences['DefaultSearchDateRange'] == "60") echo 'selected="selected"';?>>60</option>
					<option value="90" <?php if($userPreferences['DefaultSearchDateRange'] == "90") echo 'selected="selected"';?>>90</option>
					<option value="120" <?php if($userPreferences['DefaultSearchDateRange'] == "120") echo 'selected="selected"';?>>120</option>
					<option value="180" <?php if($userPreferences['DefaultSearchDateRange'] == "180") echo 'selected="selected"';?>>180</option>
					<option value="240" <?php if($userPreferences['DefaultSearchDateRange'] == "240") echo 'selected="selected"';?>>240</option>
					<option value="300" <?php if($userPreferences['DefaultSearchDateRange'] == "300") echo 'selected="selected"';?>>300</option>
					<option value="365" <?php if($userPreferences['DefaultSearchDateRange'] == "365") echo 'selected="selected"';?>>365</option>
				</select>&nbsp;&nbsp;<div style="float: left;padding-top:8px;padding-left:5px;">days</div>
			</td>
		</tr>
		<tr>
			<td>Monster Notification Email</td>
			<td>
				<select name="MonsterNotificationEmail" id="MonsterNotificationEmail" class="form-control" style="width: 150px;float:left">
					<option value="Yes" <?php if($userPreferences['MonsterNotificationEmail'] == "Yes") echo 'selected="selected"';?>>Yes</option>
					<option value="No" <?php if($userPreferences['MonsterNotificationEmail'] == "No") echo 'selected="selected"';?>>No</option>
				</select>&nbsp;&nbsp;
			</td>
		</tr>
		
		<tr>
			<td>Applicants Search Results Limit</td>
			<td>
				<select name="ApplicantsSearchResultsLimit" id="ApplicantsSearchResultsLimit" class="form-control" style="width: 150px;float:left">
				    <option value="30" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "30") echo "selected='selected'";?>>30</option>
				    <option value="40" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "40") echo "selected='selected'";?>>40</option>
				    <option value="50" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "50") echo "selected='selected'";?>>50</option>
				    <option value="60" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "60") echo "selected='selected'";?>>60</option>
				    <option value="75" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "75") echo "selected='selected'";?>>75</option>
				    <option value="90" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "90") echo "selected='selected'";?>>90</option>
				    <option value="100" <?php if($userPreferences['ApplicantsSearchResultsLimit'] == "100") echo "selected='selected'";?>>100</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Reports Search Results Limit</td>
			<td>
				<select name="ReportsSearchResultsLimit" id="ReportsSearchResultsLimit" class="form-control" style="width: 150px;float:left">
				    <option value="20" <?php if($userPreferences['ReportsSearchResultsLimit'] == "20") echo "selected='selected'";?>>20</option>
				    <option value="30" <?php if($userPreferences['ReportsSearchResultsLimit'] == "30") echo "selected='selected'";?>>30</option>
				    <option value="40" <?php if($userPreferences['ReportsSearchResultsLimit'] == "40") echo "selected='selected'";?>>40</option>
				    <option value="50" <?php if($userPreferences['ReportsSearchResultsLimit'] == "50") echo "selected='selected'";?>>50</option>
				    <option value="75" <?php if($userPreferences['ReportsSearchResultsLimit'] == "75") echo "selected='selected'";?>>75</option>
				    <option value="90" <?php if($userPreferences['ReportsSearchResultsLimit'] == "90") echo "selected='selected'";?>>90</option>
				    <option value="100" <?php if($userPreferences['ReportsSearchResultsLimit'] == "100") echo "selected='selected'";?>>100</option>
				</select>
			</td>
		</tr>

		<tr>
			<td>Dashboard Widgets Date Range (in days)</td>
			<td>
				<select name="DashboardWidgetsDateRange" id="DashboardWidgetsDateRange" class="form-control" style="width: 150px;float:left">
				    <option value="30" <?php if($userPreferences['DashboardWidgetsDateRange'] == "30") echo "selected='selected'";?>>30</option>
				    <option value="60" <?php if($userPreferences['DashboardWidgetsDateRange'] == "60") echo "selected='selected'";?>>60</option>
				    <option value="90" <?php if($userPreferences['DashboardWidgetsDateRange'] == "90") echo "selected='selected'";?>>90</option>
				    <option value="120" <?php if($userPreferences['DashboardWidgetsDateRange'] == "120") echo "selected='selected'";?>>120</option>
				    <option value="180" <?php if($userPreferences['DashboardWidgetsDateRange'] == "180") echo "selected='selected'";?>>180</option>
				    <option value="240" <?php if($userPreferences['DashboardWidgetsDateRange'] == "240") echo "selected='selected'";?>>240</option>
				    <option value="300" <?php if($userPreferences['DashboardWidgetsDateRange'] == "300") echo "selected='selected'";?>>300</option>
				    <option value="365" <?php if($userPreferences['DashboardWidgetsDateRange'] == "365") echo "selected='selected'";?>>365</option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td colspan="2">
				<input type="submit" name="Save" id="Save" value="Save" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton';?>">
			</td>
		</tr>
	</table>
</form>
</div>
