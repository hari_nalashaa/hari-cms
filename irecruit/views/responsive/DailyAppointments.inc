<div id="page-wrapper">
	<div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
			<h3 class="page-header">Daily Appointments</h3>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Daily Appointments</b>
				</div>
				<div class="panel-body">
						<div class="table-responsive">
							<?php 
								if($count == 0) {
									?>
									<div class="alert alert-danger alert-dismissable">
		                                You don't have any appointments for this day.
		                            </div>
									<?php
								}
								else {
									?>
									<table class="table table-striped table-bordered table-hover">
									<?php
									for($da = 0; $da < count($appointments); $da++) {
										?>
										<tr>
											<td width="5%"><strong>ApplicationID</strong></td>
											<td>
												<a href="<?php echo IRECRUIT_HOME;?>applicantsSearch.php?ApplicationID=<?php echo $appointments[$da]['SIApplicationID'];?>&RequestID=<?php echo $appointments[$da]['SIRequestID'];?>">
													<?php echo $appointments[$da]['SIApplicationID'];?>
												</a>
											</td>
										</tr>
										<tr>
											<td><strong>Name</strong></td>
											<td><?php echo $appointments[$da]['SIRecipient'];?></td>
										</tr>
										<tr>
											<td><strong>Email</strong></td>
											<td><?php echo $appointments[$da]['SIEmail'];?></td>
										</tr>
										<tr>
											<td><strong>Subject</strong></td>
											<td><?php echo $appointments[$da]['SISubject'];?></td>
										</tr>
										<tr>
											<td><strong>Time</strong></td>
											<td><?php echo $appointments[$da]['SITime'];?></td>
										</tr>
										<tr>
											<td colspan="2" height="40"></td>
										</tr>
										<?php
									}
									?>
									</table>
									<?php
								}
							?>
							</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
</div>