<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-bordered table-hover" id="widget_body_<?php echo $ModuleId;?>">
			<?php
				$remindex = 0;
				if(is_array($reminders)) {
					foreach ( $reminders as $rday => $reminderslist ) {
						for($r = 0; $r < count ( $reminderslist ); $r ++) {
							if($remindex < 10) {
								echo "<tr><td>";
								echo "<div class='col-sm-4' style='padding:1px;'>".$reminderslist[0]['RDate']." ".$reminderslist[$r]['RTime']."</div>";
								echo "<div>".$reminderslist[$r]['Subject']."</strong></div>";
								echo "</td></tr>";
							}
							$remindex++;
						}
					}	
				}
				if($remindex == 0) {
					?><tr>
                     	<td align='center'>There are no upcoming reminders in this month.</td>
					  </tr>
					<?php
				}
				?>
				<tr>
					<td align="right">
						
						<div style="vertical-align: top;margin-bottom:6px;float:left">
							<a href="<?php echo IRECRUIT_HOME?>applicants.php?action=remindappointments" style="color: black">
								<img src="<?php echo IRECRUIT_HOME?>images/blue-bell-1-16.png" alt="Click Here To Create Reminder" style="vertical-align: bottom">
							<strong>Create Reminder</strong></a>
						</div>
						
						<div style="vertical-align: top;margin-bottom:6px;float:right">
						<?php 
						if($remindex > 0) {
							?><a href="<?php echo IRECRUIT_HOME;?>applicants.php?action=calendartableview">View More>></a><?php
						}
						?>
						</div>
					</td>
				</tr>	
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->