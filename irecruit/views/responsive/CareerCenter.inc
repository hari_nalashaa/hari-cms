<div id="page-wrapper">
	<?php include_once 'DashboardPageTitle.inc';?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
                    <?php
                    //Set condition
                    $where          =       array("OrgID = :OrgID");
                    
                    if (isset ( $feature ['MultiOrg'] ) && $feature ['MultiOrg'] != 'Y') {
                        array_push($where,"MultiOrgID = ''");
                    }
                    
                    //Set parameters
                    $params         =       array(":OrgID"=>$OrgID);
                    //Get organization information
                    $org_info       =       G::Obj('Organizations')->getOrgDataInfo ( array("MultiOrgID", "OrganizationName"), $where, '', array($params) );
                    
                    //Organizations Information
                    if(is_array($org_info ['results'])) {
                        foreach ( $org_info ['results'] as $OD ) {
                
                            $OID = 'OrgID=' . $OrgID;
                
                            if ($OD ['MultiOrgID'] != '') {
                                $OID .= '&MultiOrgID=' . $OD ['MultiOrgID'];
                            }
                
                            $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
                
                            //Get internal requisitions count
                            $internal_requisitions_count    =   G::Obj('RequisitionDetails')->getInternalRequisitionsCount($OrgID, $OD ['MultiOrgID']);
                
                            //Get public requisitions count
                            $public_requisitions_count      =   G::Obj('RequisitionDetails')->getPublicRequisitionsCount($OrgID, $OD ['MultiOrgID']);
                
                            echo '<b>' . $OD ['OrganizationName'] . '</b><br>';
                
                            echo '&nbsp;&nbsp;&nbsp;';
                            echo '<a target="_blank" href="' . PUBLIC_HOME . 'index.php?';
                            echo $OID;
                            echo '&R=Y';
                            echo '">Career Center</a>';
                            echo '&nbsp;&nbsp;(' . $public_requisitions_count . ')';
                            echo '<br>';
                
                            if ($feature ['InternalRequisitions'] == "Y") {
                                    echo '&nbsp;&nbsp;&nbsp;';
                                    echo '<a target="_blank" href="'.PUBLIC_HOME.'internalRequisitions.php?';
                            	    echo $OID;
				    echo '&InternalVCode='.$InternalCode;
                            	    echo '&R=Y';
				    echo '">Internal Requisitions</a>';
                                    echo '&nbsp;&nbsp;(' . $internal_requisitions_count . ')';
                                    echo '<br>';
                            } // end feature
                
                            //Userportal users information based on OrgID, MultiOrgID
                            $result_users_portal    =   G::Obj('UserPortalUsers')->getUsersInfoByOrgIDMultiOrgID($OrgID, $OD ['MultiOrgID']);
                
                            if ($feature ['UserPortal'] == "Y") {
                                    echo '&nbsp;&nbsp;&nbsp;';
                                    echo '<a target="_blank" href="' . USERPORTAL_HOME . 'index.php?';
                                    echo $OID;
                                    echo '">User Portal</a>';
                                    echo '&nbsp;&nbsp;(' . $result_users_portal['count'] . ' users)';
                                    echo '<br>';
                            } // end feature
                
                            $OI = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID('wotcID', $OrgID, $OD ['MultiOrgID']);
                
                            if ($OI['wotcID'] != "") {
                                echo '&nbsp;&nbsp;&nbsp;';
                                echo '<a href="' . WOTC_HOME . 'admin.php?wotcID=' . $OI['wotcID'] . '" target="_blank">CMS WOTC Application Form</a>';
                                echo '<br>';
                            }
                
                            echo '<br>';
                        } // end while
                    }
                    ?>
				</div>
					</div>
			</table>
		</div>
	</div>
	<!-- /.row -->
</div>
