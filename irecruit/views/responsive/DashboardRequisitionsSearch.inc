<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-hover" id="widget_body_<?php echo $ModuleId;?>">
			
				<tr class="odd">
					<td>
						<div>
							<form method="post" name="frmRequisitionsSearch" id="frmRequisitionsSearch">
								<input type="hidden" name="sort_field" id="sort_field" value="posted_date">
								<input type="hidden" name="sort_order" id="" value="desc">
								
								<input type="text" name="requisitions_keyword" id="requisitions_keyword" class="form-control width-auto-inline" placeholder="Enter, name or keyword to search">
								<select name="ddlRequisitionsActiveStatus" id="ddlRequisitionsActiveStatus" class="form-control width-auto-inline">
									<option value="A">All</option>
									<option value="Y" selected="selected">Active</option>
									<option value="N">Inactive</option>
									<option value="R">Pending</option>
									<option value="NA">Not Approved</option>
								</select>
                                <?php
                                if($user_preferences['DashboardWidgetsDateRange'] == "") $user_preferences['DashboardWidgetsDateRange'] = "30";
                                ?>    
								From: <input type="text" name="req_search_from_date" id="req_search_from_date" value="<?php echo date("m/d/Y", strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));?>" class="form-control width-auto-inline">
								To: <input type="text" name="req_search_to_date" id="req_search_to_date" value="<?php echo date("m/d/Y", strtotime("+1 day"));?>" class="form-control width-auto-inline">
								
								<input type="button" name="req_search" id="req_search" class="btn btn-primary" value="Search" onclick="getRequisitionsListByParams();">
							</form>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding: 0px">
						<div id="requisitions_list" style="text-align: center"></div>
					</td>
				</tr>
		</table>
		
		
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->