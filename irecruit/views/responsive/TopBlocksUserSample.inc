<div class="row" id="topblocksusersample" style='display: none'>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="row">
					<div style="height: 80px" class="col-xs-6">
						<i class="fa fa-briefcase fa-5x"></i>
					</div>
					<div class="col-xs-6 text-right">
						<div class="huge">8</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-right">Active Requisitions</div>
				</div>
			</div>
			<a href="#">
				<div class="panel-footer">
					<span class="pull-left">View Details</span> <span
						class="pull-right"></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-green">
			<div class="panel-heading">
				<div class="row">
					<div style="height: 80px" class="col-xs-6">
						<i style="opacity: 0.5" class="fa fa-briefcase fa-5x"></i>
					</div>
					<div class="col-xs-6 text-right">
						<div class="huge">11</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-right">Inactive Requisitions</div>
				</div>
			</div>
			<a href="#">
				<div class="panel-footer">
					<span class="pull-left">View Details</span> <span
						class="pull-right"></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-yellow">
			<div class="panel-heading">
				<div class="row">
					<div style="height: 80px" class="col-xs-6">
						<i class="fa fa-calendar fa-5x"></i>
					</div>
					<div class="col-xs-6 text-right">
						<div class="huge">0</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-right">Daily Appointments</div>
				</div>
			</div>
			<a href="#">
				<div class="panel-footer">
					<span class="pull-left">View Details</span> <span
						class="pull-right"></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-6">
		<div class="panel panel-red">
			<div class="panel-heading">
				<div class="row">
					<div style="height: 80px" class="col-xs-6">
						<i class="fa fa-bell-o fa-5x"></i>
					</div>
					<div class="col-xs-6 text-right">
						<div class="huge">0</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-right">Daily Reminders</div>
				</div>
			</div>
			<a href="#">
				<div class="panel-footer">
					<span class="pull-left">View Details</span> <span
						class="pull-right"></span>
					<div class="clearfix"></div>
				</div>
			</a>
		</div>
	</div>
</div>