<div id="wrapper">
	<div id="page-wrapper">
		<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
		<?php
		if (isset ( $_GET ['msg'] ) && ($_GET ['msg'] == 'suc' || $_GET ['msg'] == 'tsuc' || $_GET ['msg'] == 'msuc' || $_GET ['msg'] == 'usuc' || $_GET['msg'] == 'maxsize')) {
			?>
			<div class="row">
				<div class="col-lg-12">
					<div class="alert alert-success alert-dismissable">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
						<?php 
						if($_GET ['msg'] == "usuc") echo 'Your profile info updated successfully.';
						else if($_GET ['msg'] == "maxsize") echo 'Your file size should not be greater than 12000';
						else  echo 'Configuration settings updated successfully.';
						?>
					</div>
				</div>
			</div>
			<?php
		}
		
		$tab_ids_titles   =   array(
                                "userconfig"            =>  "User Configuration Settings",
                                "userprofile"           =>  "User Profile",
                                "userpreferences"       =>  "User Preferences",
                                "maintainemaillists"    =>  "Maintain Email Lists"
		                      );
		$tab_ids_files    =   array(
                                "userconfig"            =>  'responsive/UserConfiguration.inc',
                    		    "userprofile"           =>  'responsive/UserEditProfile.inc',
                    		    "userpreferences"       =>  'responsive/UserPreferences.inc',
                    		    "maintainemaillists"    =>  'users/EmailLists.inc'
		                      );
		$tab_active       =   ($_REQUEST['tab_action'] != "") ? "class='active'" : "";
		
		$tab_action       =   ($_REQUEST['tab_action'] != "") ? $_REQUEST['tab_action'] : "userconfig";
		?>
		<div class="row">
			<div class="col-lg-12">
			    <ul class="nav nav-tabs">
			    	<?php
			    	foreach($tab_ids_titles as $tab_id=>$tab_title) {
			    	    ?>
    					<li <?php if($tab_action == $tab_id) echo " class='active'"; ?>><a data-toggle="tab" href="#<?php echo $tab_id;?>"><?php echo $tab_title;?></a></li>
			    	    <?php
			    	}
			    	?>
			    </ul>
			    
			    <div class="tab-content">
					<?php
					foreach($tab_ids_files as $tab_id=>$tab_file_name) {
			    	    ?>
						<div id="<?php echo $tab_id;?>" class="tab-pane fade <?php if($tab_action == $tab_id) echo "in active"; ?>">
			            	<?php include IRECRUIT_VIEWS . $tab_file_name;?>
			        	</div>
			    	    <?php
			    	}
			    	?>
			    </div>
			    			    
			</div>		    

		</div>
		
		<!-- /.row -->
	</div>
	<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->