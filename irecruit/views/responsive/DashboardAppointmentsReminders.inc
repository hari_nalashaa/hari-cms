<style>
.app_btn
{
background-color: #d23232;
-moz-user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
    color:#fff;
    padding:3px 12px;;
}
td select,input[type="text"] {
background-color:#ddd;
   width: 200px;
   padding: 5px;
   font-size: 16px;
   line-height: 1;
   border: 0;
   border-radius: 0;
   height: 27px;
   -webkit-appearance: none;
   }
td input[type="text"] {
width:160px;
   }
.ui-datepicker-trigger
{
margin-left:5px;
}
</style>
<script type="text/javascript">
$(document).ready(function() {
    $("#button").click(function(){
           var searchdates = $("#appointment_reminder_widget").val();
          var typeofreminder=$("#type_of_reminder").val();
          $.ajax({
              type:"POST",
              url:irecruit_home+"responsive/GetValueFromSearchReminder.php",
              data:"searchdate="+searchdates+"&typeofreminder="+typeofreminder,
              success:function(data){
                 $("#info").html(data);
              }

          });

    });
});
</script>
<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-bordered table-hover" id="widget_body_<?php echo $ModuleId;?>">
		<form>
		<tr>
		<td><input type="text" name="appointment_reminder_widget" id="appointment_reminder_widget"></td>
		<td>
		<select name="type_of_reminder" id="type_of_reminder">
		  <option value="Select">Select</option>
		  <option value="Appoinment">Appoinment</option>
		  <option value="Reminder">Reminder</option>
		</select>
		</td>
	    <td><input type="button" value="Submit" id="button" class="app_btn"></td>
		</tr>
		</form> 
		</table>
		 <div id="info"></div>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->