<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<div style="width: 100%;" id="widget_body_<?php echo $ModuleId;?>">
			<form name="frmDashboardCalendarNavigation" id="frmDashboardCalendarNavigation" method="post">
				<input type="hidden" name="selected_year" id="selected_year" value="<?php echo date('Y');?>">
				<input type="hidden" name="selected_month" id="selected_month" value="<?php echo date('m');?>">
				<input type="hidden" name="selected_day" id="selected_day" value="<?php echo date('d');?>">
				<input type="hidden" name="selected_week_start_date" id="selected_week_start_date" value="<?php echo date('Y-m-d');?>">
				<input type="hidden" name="selected_calendar_view" id="selected_calendar_view" value="month">
				<input type="hidden" name="today_date" id="today_date" value="<?php echo date('Y-m-d');?>">
				<table class="table table-striped table-bordered table-hover calendar_top_navigation">
					<tr>
						<th align="center" id="tcviewprevious" onclick="loadCalendarView('previous')"><span class="fa fa-caret-left"></span></th>
						<th align="center" id="tcviewnext" onclick="loadCalendarView('next')"><span class="fa fa-caret-right"></span></th>
						<th align="center" id="tcviewtoday" onclick="loadTodayCalendar();"><span>Today</span></th>
						<th align="center" colspan="4" align="center"><span id="calendar_date_label" class="dashboard_calendar_date_label"><?php echo date('M')." ".date('Y')?></span></th>
						<th align="center" class="dashbaord_cal_active_view" id="tcviewmonth" onclick="dashboardCalendarDataView(this.id)"><span>Month</span></th>
						<th align="center" id="tcviewweek" onclick="dashboardCalendarDataView(this.id)"><span>Week</span></th>
						<th align="center" id="tcviewday" onclick="dashboardCalendarDataView(this.id)"><span>Day</span></th>
					</tr>
				</table>
			</form>
			
			
			<div id="cdatamonth" class='calendardataview'>
				<?php 
				echo $AppointmentsObj->calendarTableView(date('Y'), date('m'), date('d'), false);
				?>
			</div>
			
			<div id="cdataweek" class='calendardataview' style="display:none">
				<?php
				echo $AppointmentsObj->calendarWeekView(date('Y'), date('m'), date('d'));
				?>
			</div>
			
			<div id="cdataday" class='calendardataview' style="display:none">
				<?php
				echo $AppointmentsObj->calendarDayView(date('Y'), date('m'), date('d'));
				?>
			</div>

		</div>

		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->