<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-bordered table-hover" id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
				<?php
				$appindex = 0;
				if(is_array($appointments)) {
					foreach ( $appointments as $aday => $appointmentslist ) {
						for($a = 0; $a < count ( $appointmentslist ); $a ++) {
							if ($appindex < 10) {
								$strlength = false;
								if (strlen ( $appointmentslist [$a] ['SISubject'] ) > 60)
									$strlength = true;
								echo "<tr><td class='col-md-4'>";
								echo "<div class='col-md-4' style='padding:1px;'>" . $appointmentslist [0] ['SIDate'] . " " . $appointmentslist [$a] ['SITime'] . "</div>";
								echo "<div>" . ucfirst ( substr ( $appointmentslist [$a] ['SISubject'], 0, 40 ) ) . "</strong></div>";
								echo "</td></tr>";
							}
							$appindex++;
						}
					}	
				}
				if($appindex == 0) {
					?>
					<tr class="odd" align="center">
                    	<td>
                    		There are no upcoming appointments in this month.
                    	</td>
					</tr>
					<?php
				}
				else {
					?>
					<tr>
						<td align="right"><a href="<?php echo IRECRUIT_HOME;?>applicants.php?action=calendartableview">View More>></a></td>
					</tr>
					<?php
				}
				?>
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->