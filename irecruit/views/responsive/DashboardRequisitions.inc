<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<form name="frmDashboardRequisitions" id="frmDashboardRequisitions">
		<table class="table table-striped table-bordered table-hover" id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
			<?php 
			if(count ( $requisitions ) == 0) {
				?>
				<tr>
					<td colspan="4" align="center">No requisitions found.</td>
				</tr>
				<?php
			}
			else {
				?>
				<tr>
					<th><a href="javascript:void(0)" onclick="getWidgetRequisitions('getWidgetRequisitions.php?lbl=jobtitle', 'widget_body_<?php echo htmlspecialchars($ModuleId);?>');">Job Title</a></th>
					<th align="center"><a href="javascript:void(0)" onclick="getWidgetRequisitions('getWidgetRequisitions.php?lbl=dateopened', 'widget_body_<?php echo htmlspecialchars($ModuleId);?>');">Date Opened</a></th>
					<th align="center">Applicants</th>
					<th align="center">Expiring Days</th>
				</tr>
				<?php 
				for($r = 0; $r < count ( $requisitions ); $r ++) {
					?>
					<tr>
						<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?>><a href="<?php echo IRECRUIT_HOME;?>requisitionsSearch.php?menu=3&RequestID=<?php echo $requisitions[$r]['RequestID'];?>&Active=Y"><?php echo $requisitions[$r]['Title']?></td>
						<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?> width="13%" align="center"><?php echo $requisitions[$r]['PostDate']?></td>
						<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?> width="5%" align="center"><?php echo $requisitions[$r]['ApplicantsCount']?></td>
						<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?> width="13%" align="center"><?php echo $requisitions[$r]['ExpireDays'];?></td>
					</tr>
					<?php
				}
			}
			if(count ( $requisitions ) > 0) {
				?>
				<tr>
					<td align="right" colspan="4"><a href="<?php echo IRECRUIT_HOME;?>requisitions.php?Active=Y">View More>></a></td>
				</tr>
				<?php
			}
			?>
		</table>
		<input type="hidden" name="sort_dashboard_req" id="sort_dashboard_req" value="A">
		</form>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->