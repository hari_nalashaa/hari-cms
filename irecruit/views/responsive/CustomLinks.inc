<div id="page-wrapper">
	<?php include_once 'DashboardPageTitle.inc';?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<form name="frmCustomLinks" id="frmCustomLinks" method="post" onsubmit="return validate_custom_links()">
						<table class="table table-bordered table-hover">
							<tr>
								<td colspan="2">
									<?php 
									if(isset($_GET['msg']) && $_GET['msg'] == "inssuc") {
										?>
										<div class="alert alert-success alert-dismissable">
											<?php echo "Inserted Successfully";?>
										</div>
										<?php
									}
									if(isset($_GET['msg']) && $_GET['msg'] == "delsuc") {
									?>
									<div class="alert alert-danger alert-dismissable" id="custom_links_error">
											<?php echo "Deleted Successfully";?>	
									</div>
									<?php
									}
									?>
									<div id="custom_links_error"></div>
								</td>
							</tr>
							<tr>
								<td width="10%"><strong>Note:*</strong></td>
								<td>Please type Link Href with "http://". For example http://google.com</td>
							</tr>
							<tr>
								<td width="10%">Link Title</td>
								<td><input type="text" name="LinkTitle" id="LinkTitle" class="form-control"></td>
							</tr>
							
							<tr>
								<td width="10%">Link Href</td>
								<td><input type="text" name="LinkTarget" id="LinkTarget" class="form-control"></td>
							</tr>
							
							<tr>
								<td><input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" class="btn btn-primary"></td>
							</tr>
						
						</table>
					</form>
					<!-- /.row (nested) -->
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>

	<div class="row">
		<div class="col-lg-12">
			<strong>Note*:</strong> Please drag and drop the rows to sort the links<br>
			<div id="custom_links_sort_message"></div>
		</div>
	</div>	
	<div class="row">
		<div class="col-lg-12">
			<table class="table table-striped table-bordered table-hover" id="my_custom_links">
			<thead>
				<tr>
					<td><strong>Link Title</strong></td>
					<td><strong>Link Target</strong></td>
					<td><strong>Action</strong></td>
				</tr>
			</thead>
			<tbody>
			<?php 
				$custom_links = $CustomLinksObj->getCustomLinks();
				$custom_links_info = $custom_links["results"];
				$custom_links_count = count($custom_links["results"]);
				if($custom_links_count > 0) {
					for($cl = 0; $cl < $custom_links_count; $cl++) {
						?>
					  <tr id="<?php echo $custom_links_info[$cl]["LinkID"]?>">
						<td><?php echo $custom_links_info[$cl]["LinkTitle"]?></td>
						<td><?php echo $custom_links_info[$cl]["LinkTarget"]?></td>
						<td><a href="customLinks.php?key=<?php echo $custom_links_info[$cl]["LinkID"]?>">Delete</a></td>
					  </tr><?php
					}	
				}
				else {
					?>
					<tr>
						<td colspan="3">No records found</td>
					  </tr>
					<?php
				}
			?>
			</tbody>
			</table>
		</div>
	</div>
	<!-- /.row -->
</div>