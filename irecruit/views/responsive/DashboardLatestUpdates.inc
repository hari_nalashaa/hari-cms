<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<?php
        $where_info     =   array("OrgID = :OrgID");
        $params_info    =   array(":OrgID"=>$OrgID);
        
        if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
            $params_info[":ROrgID"] =   $OrgID;
            $params_info[":UserID"] =   $USERID;
            $where_info[]           =   "RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :ROrgID AND UserID = :UserID)";
        } // end hiring manager
        
        $columns        =   "*, getApplicantSortName(OrgID, ApplicationID) as ApplicantSortName, DATE_FORMAT(Date, '%m/%d/%Y %H:%i:%s') AS FDate";
        $app_upd_res    =   G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo($columns, $where_info, '', 'Date DESC LIMIT 5', array($params_info));
        $app_updates    =   $app_upd_res['results'];
		?>
		<table class="table table-striped table-hover" id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
			<tr>
                <td colspan="6"><strong>Applicant Updates:</strong></td>
			</tr>
			<tr>
                <td><strong>ApplicationID</strong></td>
                <td><strong>Applicant Name</strong></td>
                <td><strong>Update</strong></td>
                <td><strong>Updated By</strong></td>
                <td><strong>Scheduled Interview Date</strong></td>
                <td><strong>Date Updated</strong></td>
			</tr>
			<?php 
			 for($a = 0; $a < count($app_updates); $a++) {
                
                $columns        =   "DATE_FORMAT(SIDateTime, '%m/%d/%Y %H:%i:%s') AS SIDateTime";
                $interview_res  =   G::Obj('Appointments')->getApplicantInterviewDate($columns, $app_updates[$a]['OrgID'], $app_updates[$a]['ApplicationID']);
 			 	?>
                <tr>
                    <td><a href="<?php echo IRECRUIT_HOME;?>applicantsSearch.php?ApplicationID=<?php echo $app_updates[$a]['ApplicationID'];?>&RequestID=<?php echo $app_updates[$a]['RequestID'];?>"><?php echo $app_updates[$a]['ApplicationID']?></a></td>
                    <td><?php echo $app_updates[$a]['ApplicantSortName']?></td>
                    <td>
                        <a href="<?php echo IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID='.$app_updates[$a]['ApplicationID'].'&RequestID='.$app_updates[$a]['RequestID'].'&tab_action=applicant-history';?>">
                        <?php 
                            echo "Notes Added";
                        ?>
                        </a>
                        <?php 
                            echo "<br>";
                            echo "Status updated to: ". $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $app_updates[$a]['ProcessOrder'] );
                            echo "<br>";
                        ?>
                    </td>
                    <td>
                        <?php
                            if(is_numeric($app_updates[$a]['UserID'])) {
                                $user_info  =   G::Obj('UserPortalUsers')->getUserDetailInfoByUserID("UserID, FirstName, LastName", $app_updates[$a]['UserID']);
                                echo $user_info['FirstName'] . ' ' . $user_info['LastName'];
                            }
                            else {
                                $user_info  =   G::Obj('IrecruitUsers')->getUserInfoByUserID($app_updates[$a]['UserID'], "UserID, FirstName, LastName");
                            	echo $user_info['FirstName'] . ' ' . $user_info['LastName'];
                            }
    
                            if(!isset($user_info['UserID'])) {
                                echo $app_updates[$a]['UserID'];
                            }
                        ?>
                    </td>
                    <td><?php echo $interview_res['SIDateTime'];?></td>
                    <td><?php echo $app_updates[$a]['FDate']?></td>
                </tr>
			 	<?php
			 }
			?>
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->
