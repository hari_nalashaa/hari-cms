<div class="col-lg-12 col-md-12 col-sm-12 mdsortable" id="listitem_<?php echo htmlspecialchars($ModuleId);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-hover" id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
			
				<tr class="odd">
					<td>
						<div>
							<form method="post" name="frmWotcApplicantsSearch" id="frmWotcApplicantsSearch">
								<input type="hidden" name="wotc_sort_field" id="wotc_sort_field" value="entry_date">
								<input type="hidden" name="wotc_sort_order" id="" value="desc">
								<input type="hidden" name="wotc_applicants_keyword" id="wotc_applicants_keyword" class="form-control width-auto-inline" placeholder="Enter, name or keyword to search">
								<?php
								if($user_preferences['DashboardWidgetsDateRange'] == "") $user_preferences['DashboardWidgetsDateRange'] = "30";
								?>
								<select name="ddlQualifies" id="ddlQualifies" class="form-control width-auto-inline" style="max-width:300px;">
									<option value="">Select</option>
									<option value="Y">Potentially Qualified</option>
									<option value="N">Not Qualified</option>
								</select>
								
								From: <input type="text" name="woct_quicksrch_from_date" id="woct_quicksrch_from_date" size="10" value="<?php echo date("m/d/Y", strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));?>" class="form-control width-auto-inline">
								To: <input type="text" name="woct_quicksrch_to_date" id="woct_quicksrch_to_date" size="10" value="<?php echo date("m/d/Y", strtotime("+1 day"));?>" class="form-control width-auto-inline">
								
								<input type="button" name="search" id="search" class="btn btn-primary" value="Search" onclick="getWotcApplicantsByKeyword();">
							</form>
						</div>
					</td>
				</tr>
				<tr>
					<td style="padding: 0px">
						<div id="wotc_applicants_list" style="text-align: center"></div>
					</td>
				</tr>
		</table>
		
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->
