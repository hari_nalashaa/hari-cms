<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="<?php echo htmlspecialchars($listitem);?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<div class="panel-body" id="widget_body_<?php echo htmlspecialchars($ModuleId);?>">
			<div style="margin: 5px;text-align: center">
				<div id="module_status_message<?php echo htmlspecialchars($ModuleId);?>" style="color:red;height:25px;"></div>
				<?php 
					if($ModuleId == "7") $query_type = "APO";	//Applicants by status
					else if($ModuleId == "8") $query_type = "ADC";	//Applicants by Disposition code
				?>
				<strong>Date Range:</strong>
				&nbsp;
				<input type="text" name="module_from_date" id="module_from_date<?php echo htmlspecialchars($ModuleId);?>" style="width:100px" value="<?php echo htmlspecialchars($POST['BeginDate'])?>"> - 
				<input type="text" name="module_to_date" id="module_to_date<?php echo htmlspecialchars($ModuleId);?>" style="width:100px" value="<?php echo $POST['FinalDate']?>">
				<input type="button" name="generate_graph" id="generate_graph" value="Submit" class="btn btn-primary btn-sm" onclick="filter_applicants('<?php echo htmlspecialchars($query_type);?>', $('#module_from_date<?php echo htmlspecialchars($ModuleId);?>').val(), $('#module_to_date<?php echo $ModuleId;?>').val(), '<?php echo $ModuleId?>');">
			</div>
			<div class="flot-chart">
				<div class="flot-chart-content" id="<?php echo $piechartid;?>"></div>
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>