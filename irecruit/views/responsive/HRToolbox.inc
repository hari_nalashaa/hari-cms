<div id="page-wrapper">
	<div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
			<h3 class="page-header">HR Toolbox</h3>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>HR Toolbox</b>
				</div>
				<div class="panel-body">
						<div class="table-responsive">
							<?php
							echo G::Obj('HRToolbox')->displayHRToolboxFiles($OrgID,'');
							?>						
							</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
</div>
