<div id="page-wrapper">
	<div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
			<h3 class="page-header">Daily Reminders</h3>
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
		<div class="col-xs-12 col-lg-12 col-md-12 col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<b>Daily Reminders</b>
				</div>
				<div class="panel-body">
						<div class="table-responsive">
							<?php
							if($count == 0) {
								?>
								<div class="alert alert-danger alert-dismissable">
	                                You don't have any reminders for this day.
	                            </div>
								<?php
							}
							else {
								?>
								<table class="table table-striped table-bordered table-hover">
								<?php
								for($da = 0; $da < $count; $da++) {
										?>
										
										<tr>
											<td><strong>Subject</strong></td>
											<td><?php echo $reminders[$da]['Subject'];?></td>
										</tr>
										<tr>
											<td><strong>Time</strong></td>
											<td><?php echo $reminders[$da]['RTime'];?></td>
										</tr>
										<tr>
											<td colspan="2" height="20"></td>
										</tr>
										<?php
									}
								?>
								</table>
								<?php
							}							
							?>						
							</div>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
</div>