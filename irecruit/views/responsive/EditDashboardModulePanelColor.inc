<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Edit Dashboard Modules Panel Heading Color:</div>
			<div class="panel-body">
				<div class="table-responsive">
					<form name="frmDashboardPanelHeadingColor" id="frmDashboardPanelHeadingColor" method="post">
						<table width="100%" class="table table-striped table-bordered table-hover">
							<tbody>
								<tr>
									<td width="20%">
										<select name="ModuleName" id="ModuleName" class="form-control" onchange="change_panel_color(this.value)">
											<option value="">Select Module</option>
											<?php 
											foreach ( $dashboardmodules as $dashid => $dashmodname ) {
												$udashmodname = strtolower ( str_replace ( " ", "", $dashmodname ) );
												?><option value="<?php echo $dashid."***".$usermoduleidinfo[$dashid]['PanelColor']?>">
													<?php echo $dashmodname;?>
												  </option><?php
											}
											?>
										</select>
									</td>
									<td width="20%">
										<select name="PanelColor" id="PanelColor" class="form-control">
											<option value="">Select Panel Color</option>
											<option value="panel-heading-blue">Blue</option>
											<option value="panel-heading-green">Green</option>
											<option value="panel-heading-red">Red</option>
											<option value="panel-heading-yellow">Yellow</option>
										</select>
									</td>
									<td align="left">
										<input type="submit" name="update_panel_color" id="update_panel_color" value="Update Panel Color" class="btn btn-primary">
									</td>
								</tr>
	                        </tbody>
						</table>
					</form>					
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->