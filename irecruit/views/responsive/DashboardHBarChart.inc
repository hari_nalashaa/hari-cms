<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include 'DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<div class="panel-body" id="widget_body_<?php echo $ModuleId;?>">
			<div class="flot-chart">
				<div id="horibarchartplaceholder"></div>
			</div>
		</div>
		<!-- /.panel-body -->
	</div>
	<!-- /.panel -->
</div>