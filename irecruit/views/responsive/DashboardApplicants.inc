<div class="col-lg-6 col-md-6 col-sm-6 mdsortable" id="listitem_<?php echo $ModuleId;?>">
	<div class="panel panel-default">
		<?php include IRECRUIT_DIR . 'views/responsive/DashboardWidgetsHeading.inc';?>
		<!-- /.panel-heading -->
		<table class="table table-striped table-bordered table-hover" id="widget_body_<?php echo $ModuleId;?>">
			<tr class="odd">
				<td class="col-md-4">
					<div class="col-md-3" style="padding: 0px">
						<strong>Date Range</strong>
					</div>
					<?php
                        if($user_preferences['DashboardWidgetsDateRange'] == "") $user_preferences['DashboardWidgetsDateRange'] = "30";
						
						if(is_array($applicants) && count($applicants) > 0) {
							echo '<div>';
							echo date('m/d/Y', strtotime($applicants[0]['EntryDate']))." - ".date('m/d/Y', strtotime($applicants[count($applicants) - 1]['EntryDate']));
							echo '</div>';
						}
						echo '</tr>';
						$ai = 0;
						
						if(is_array($applicants))
						{
							foreach ( $applicants as $applicantinfo ) {
								if ($ai < 5) {
									?>
									<tr class="odd">
										<td class="col-md-4">
											<div class="col-md-3" style="padding: 0px">
												<strong>ApplicationID</strong>
											</div>
											<div>
												<a
													href="<?php echo IRECRUIT_HOME;?>applicantsSearch.php?ApplicationID=<?php echo $applicantinfo['ApplicationID'];?>&RequestID=<?php echo $applicantinfo['RequestID'];?>">
																		<?php echo $applicantinfo['ApplicationID'];?>
																	</a>
											</div>
											<div class="col-md-3" style="padding: 0px">
												<strong>Name</strong>
											</div>
											<div><?php echo $applicantinfo['FirstName'], " " , $applicantinfo['LastName']; ?> </div>
											<div class="col-md-3" style="padding: 0px">
												<strong>Email</strong>
											</div>
											<div><?php echo $applicantinfo ['Email']; ?></div>
										</td>
									</tr>
									<?php
								}
								$ai ++;
							}
				
							if ($ai == 0) {
								?>
								<tr class="odd">
									<td align="center">
										<h5 align='center'>No applicants found</h5>
									</td>
								</tr>
								<?php
							} else {
								?>
								<tr>
									<td align="right">
										<form method="post" name="frmApplicants" id="frmApplicants" action="<?php echo IRECRUIT_HOME?>applicantsSearch.php">
											<input type="hidden" name="criteria" id="criteria" value=""> 
											<input type="hidden" name="OrgID" id="OrgID" value=""> 
											<input type="hidden" name="MultiOrgID" id="MultiOrgID" value=""> 
											<input type="hidden" name="SearchWords" id="SearchWords" value=""> 
											<input type="hidden" name="type1" id="type1" value="1"> 
											<input type="hidden" name="refinereq" id="refinereq" value="no"> 
											<input type="hidden" name="Active" id="Active" value="Y"> 
											<input type="hidden" name="ProcessOrder" id="ProcessOrder" value=""> 
											<input type="hidden" name="Code" id="Code" value=""> 
											<input type="hidden" name="ApplicationDate_From" id="ApplicationDate_From" value="<?php echo date("m/d/Y", strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"))?>"> 
											<input type="hidden" name="ApplicationDate_To" id="ApplicationDate_To" value="<?php echo date("m/d/Y")?>"> 
											<input type="hidden" name="WSList" id="WSList" value=""> 
											<input type="hidden" name="LimitProcessed" id="LimitProcessed" value="Y"> 
											<input type="hidden" name="action" id="action" value="search"> 
											<input type="hidden" name="process" id="process" value="Y"> 
											<input type="hidden" name="SearchName" id="SearchName" value=""> 
											<a href="javascript:void(0);" onclick="document.forms['frmApplicants'].submit();">View More>></a>
										</form>
									</td>
								</tr>
								<?php
							}
						}	
			?>
		</table>
		<!-- /.panel-body -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.panel -->
