<?php
if (isset ( $_REQUEST['BlockPosition'] ) && $_REQUEST['BlockPosition'] != "") {
			
	//Get DashboardUserTopBlock Information
	$dashusertopblockinfo  =   G::Obj('DashboardConfiguration')->getDashboardUserTopBlockInfo ( $_REQUEST['BlockPosition'] );
	$positions             =   array (
                					"FIRST",
                					"SECOND",
                					"THIRD",
                					"FOURTH" 
                				);
			
	include IRECRUIT_VIEWS . 'responsive/TopBlocksUserSample.inc';
?>
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Edit Top Block</div>
			<div class="panel-body">
				<div class="alert alert-success alert-dismissable">
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
					You can edit the block information here. Dashboard home page top
					blocks color will change based on the background. <br> 
					<a href="javascript:void(0);" onclick="$('#topblocksusersample').toggle('slow');"><strong>Click Here</strong></a>
					to see the sample.
				</div>
				<form role="form" name="frmdashtopblockconf" id="frmdashtopblockconf" enctype="multipart/form-data" method="post">
					<div class="form-group col-lg-4">
						<label>Block Position : </label>&nbsp;<?php echo $_GET['BlockPosition'];?>
						<input type="hidden" name="blockposition" id="blockposition" value="<?php echo $_GET['BlockPosition'];?>">
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<label>Show Image</label> 
						<select name="showimage" id="showimage" class="form-control">
							<option value="Yes" <?php if($dashusertopblockinfo['ShowImage'] == "Yes") echo 'selected="selected"';?>>Yes</option>
							<option value="No" <?php if($dashusertopblockinfo['ShowImage'] == "No") echo 'selected="selected"';?>>No</option>
						</select>
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<?php 
						$user_top_blocks_info = $DashboardConfigurationObj->getUserDashboardTopBlocks();
						$user_top_blocks = $user_top_blocks_info[0];
						
						for($ut = 0; $ut < count($user_top_blocks); $ut++) {
							$user_top_block_names[$user_top_blocks[$ut]['BlockId']] = $user_top_blocks[$ut]['BlockText'];
						}
						?>
						<label>Block Type</label>
						<select name="blocktype" id="blocktype" class="form-control">
						<option value="">Select</option>
	                    <?php
						foreach ( $dashboardtopblocks as $blockid => $blockvalue ) {
							if(!in_array($blockvalue, $user_top_block_names) && $_GET['BlockPosition'] != "") {
								?><option value="<?php echo $blockid;?>"><?php echo $blockvalue;?></option><?php	
							}
						}
						?>
						<option value="<?php echo $dashusertopblockinfo['BlockId'];?>" selected="selected">
							<?php echo $dashboardtopblocks[$dashusertopblockinfo['BlockId']];?>
						</option>
	                    </select>
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4" id="div_new_applicants_interval">
						<label>New Applicants Interval</label>
						<select name="new_applications_interval" id="new_applications_interval" class="form-control">
							<option value="1" <?php if($dashusertopblockinfo['NewApplicationsInterval'] == '1') echo 'selected="selected"';?>>1 Month</option>
							<option value="2" <?php if($dashusertopblockinfo['NewApplicationsInterval'] == '2') echo 'selected="selected"';?>>2 Months</option>
							<option value="3" <?php if($dashusertopblockinfo['NewApplicationsInterval'] == '3') echo 'selected="selected"';?>>3 Months</option>
							<option value="4" <?php if($dashusertopblockinfo['NewApplicationsInterval'] == '4') echo 'selected="selected"';?>>4 Months</option>
							<option value="5" <?php if($dashusertopblockinfo['NewApplicationsInterval'] == '5') echo 'selected="selected"';?>>5 Months</option>
							<option value="6" <?php if($dashusertopblockinfo['NewApplicationsInterval'] == '6') echo 'selected="selected"';?>>6 Months</option>
						</select>
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<label>Block Image</label> <select name="blockimageclass"
							id="blockimageclass" class="form-control" 
							onchange="$('#blockimagesrc').attr('class', this.value)">
							<option value="fa fa-briefcase fa-5x"
								<?php if($dashusertopblockinfo['BlockImageClass'] == "fa-briefcase") echo 'selected="selected"';?>>Breifcase</option>
							<option value="fa fa-calendar fa-5x"
								<?php if($dashusertopblockinfo['BlockImageClass'] == "fa-calendar") echo 'selected="selected"';?>>Appointments
								Calendar1</option>
							<option value="fa fa-calendar-o fa-5x"
								<?php if($dashusertopblockinfo['BlockImageClass'] == "fa-calendar-o") echo 'selected="selected"';?>>Appointments
								Calendar2</option>
							<option value="fa fa-bell-o fa-5x"
								<?php if($dashusertopblockinfo['BlockImageClass'] == "fa-bell-o") echo 'selected="selected"';?>>Reminders
								Bell1</option>
							<option value="fa fa-bell fa-5x"
								<?php if($dashusertopblockinfo['BlockImageClass'] == "fa-bell") echo 'selected="selected"';?>>Reminders
								Bell2</option>
							<option value="fa fa-user fa-5x"
								<?php if($dashusertopblockinfo['BlockImageClass'] == "fa-user") echo 'selected="selected"';?>>User
								</option>
						</select>
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<i class="fa <?php echo $dashusertopblockinfo['BlockImageClass']?> fa-5x" id="blockimagesrc"></i>
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<label>Panel Color</label>
						<select name="blockpanelcolor" id="blockpanelcolor" class="form-control" onchange="$('#blockimagesrc').attr('class', this.value)">
							<option value="1" <?php if($dashusertopblockinfo['BlockPanelColor'] == '1') echo 'selected="selected"';?>>Blue</option>
							<option value="2" <?php if($dashusertopblockinfo['BlockPanelColor'] == '2') echo 'selected="selected"';?>>Green</option>
							<option value="3" <?php if($dashusertopblockinfo['BlockPanelColor'] == '3') echo 'selected="selected"';?>>Yellow</option>
							<option value="4" <?php if($dashusertopblockinfo['BlockPanelColor'] == '4') echo 'selected="selected"';?>>Red</option>
						</select>
					</div>
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<label>Status</label>
						<select name="blockstatus" id="blockstatus" class="form-control">
							<option value="1" <?php if($dashusertopblockinfo['BlockStatus'] == 'Active') echo 'selected="selected"';?>>Active</option>
							<option value="2" <?php if($dashusertopblockinfo['BlockStatus'] == 'Inactive') echo 'selected="selected"';?>>Inactive</option>
						</select>
					</div>
					
					<div class="clearfix"></div>
					<div class="form-group col-lg-4">
						<input type="submit" class="btn btn-primary" value="Save"
							id="topblock" name="topblock">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
}
?>

<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Upload Profile Photo:</div>
			<div class="panel-body">
				<div class="table-responsive">
					<form name="frmProfilePhoto" id="frmProfilePhoto" method="post" enctype='multipart/form-data'>
						<table width="100%" class="table table-striped table-bordered table-hover">
							<tbody>
								<tr>
									<td width="15%" style="vertical-align:middle">
										Photo:(Size 50 x 50 px)
									</td>
									<td width="20%" style="vertical-align:middle">
										<input type="file" name="profile_photo" id="profile_photo">
									</td>
									<td align="left">
										<input type="submit" name="photo_upload" id="photo_upload" value="Upload Photo" class="btn btn-primary">
									</td>
								</tr>
	                        </tbody>
						</table>
					</form>					
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->


<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Dashboard Top Blocks</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>Block Position</th>
								<th>Block Type</th>
								<th>Image</th>
								<th>Show Image</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
							for($r = 0; $r < count ( $dashboardusertopblocks ); $r ++) {
							?>
                            <tr>
								<td><?php echo $dashboardusertopblocks[$r]['BlockPosition']?></td>
								<td><?php echo $dashboardusertopblocks[$r]['BlockText']?></td>
								<td><i class="fa <?php echo $dashboardusertopblocks[$r]['BlockImageClass']?> fa-5x"
									<?php if($dashboardusertopblocks[$r]['BlockId'] == 2) echo ' style="opacity:0.5"'?>></i>
								</td>
								<td><?php echo $dashboardusertopblocks[$r]['ShowImage'];?></td>
								<td><?php echo $dashboardusertopblocks[$r]['BlockStatus'];?></td>
								<td>
									<a href="configuration.php?BlockPosition=<?php echo $dashboardusertopblocks[$r]['BlockPosition'];?>">Edit</a>
								</td>
							</tr>
                            <?php
							}
						?>
                        </tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- /.row -->
<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Dashboard Modules</div>
			<div class="panel-body">
				<div class="row">
					<!-- /.col-lg-6 (nested) -->
					<div class="col-lg-12">
						<label>Please select the modules, that are going to display in dashboard.</label>
						<form role="form" method="post" name="frmDashboardModules" id="frmDashboardModules">
							<input type="hidden" name="tab_action" value="userconfig"> 
							<input type="hidden" name="hidemodules" id="hidemodules" value="1">
							<div class="alert alert-info alert-dismissable">
								<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
							Drag and drop widgets into place in the order you would like them to display on your Dashboard.	
							</div>
							<div id="msgsortable" class="alert alert-success" style="display: none"></div>
							<div class="form-group" id="moduleslistconfiguration">

							<?php
						   //Get WOTC cnt 
                            $where_info =   array("iRecruitOrgID = :irecruitorgid");
                            $params     =   array(':irecruitorgid'=>$OrgID);
                            $wotc_res   =   G::Obj('WOTCOrganization')->getOrgDataInfo("wotcID", $where_info, '', '', array($params));
                            $wotc_cnt   =   $wotc_res['count'];
						
						    if($wotc_cnt < 1) {
								unset($dashboardmodules[21]);
						    }
						    
						    //Get Twilio Account Information
                            $account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
                            $display_module =   true;
							foreach ( $dashboardmodules as $dashid => $dashmodname ) {
								$udashmodname = strtolower ( str_replace ( " ", "", $dashmodname ) );
								
								if($dashid == 20 && $account_info['AccountSid'] == "") {
								    $display_module = false;
								}
								
								if($display_module === true) {
								    ?>
                                    <div class="checkbox checkbox_div" id="listitem_<?php echo $dashid;?>">
    									<label>
    										<input type="checkbox" name="modules[<?php echo $dashid;?>]" id="<?php echo $udashmodname;?>" value="1"
    										<?php if($usermodules[$dashid] == 1) echo 'checked="checked"';?>><?php echo $dashmodname;?>
    		                            </label>
    								</div>
                                    <?php
								}
								
								$display_module = true;
							}
							?>
                            <input type="submit" name="modulesubmit" id="modulesubmit" value="Save" class="btn btn-primary">
							</div>
						</form>
					</div>
					<!-- /.col-lg-6 (nested) -->
				</div>
				<!-- /.row (nested) -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
