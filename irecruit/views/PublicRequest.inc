<?php
$ImagePresent	=	0;

//Set where condition
$where 			=	array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
//Set parameters
$params 		=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>'');
$results		=	$OrganizationsObj->getOrganizationLogosInformation("OrgID", $where, '', array($params));

$ImagePresent	=	$results['count'];

if ($ImagePresent > 0) {
	echo '<div id="topDoc" class="row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	echo '<img src="' . PUBLIC_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary">';
	echo '</div>';
	echo '</div>';
}

$yes_no = array(""=>"Please Select", "Y"=>"Yes", "N"=>"No");

if(isset($_REQUEST['action']) 
	&& ($_REQUEST['action'] == 'new' || $_REQUEST['action'] == 'edit')) {
	require_once IRECRUIT_VIEWS . 'requisitions/CreateEditRequisitionForm.inc';
} else if(isset($_REQUEST['action']) && $_REQUEST['action'] == "view") {
	echo '<div class="row"><div class="col-lg-12"><div class="panel panel-default"><div class="panel-body">';
	require_once IRECRUIT_DIR . 'request/ViewRequest.inc';
	echo displayRequestInfo ( $OrgID, $_REQUEST['RequestID'], '', '' );
	echo '</div></div></div></div>';
}
?>