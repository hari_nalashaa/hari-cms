<div class="container">
	<div class="row">
		<div class="col-sm-16 col-md-6">
			<br>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-16 col-md-6">
			<?php
			if($brand_info['BrandID'] == "0") {
                echo '<img border="0" src="images/iRecruit-Logo.png"><br><br>';
			}
			else {
			    $brand_logo_url =  IRECRUIT_HOME . "/vault/brands/".$brand_org_info['Logo'];
			    echo '<img src="'.$brand_logo_url.'" alt="logo">';
			}
			?>			
		</div>
	</div>
	<div class="row">
		<div class="col-sm-16 col-md-6">
			<br> <br> <br>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-4 col-md-offset-4">
			
			<div class="account-wall" id="login_form">
				<h4 class="text-center login-title">Sign in to continue to iRecruit</h4>
				<form method="post" action="login.php" class="form-signin">
					<input name="user" size="23" maxlength="45" type="text" class="form-control" placeholder="Username" required autofocus> <br>
					<input type="password" name="password" class="form-control" placeholder="Password" required> <br> 
					<?php 
					   if(isset($_GET['BOrgID']) && $_GET['BOrgID'] != "") {
					   	   ?><input type="hidden" name="goto" value=""><?php
					   }
					   else {
					       ?><input type="hidden" name="goto" value="<?php echo htmlspecialchars($goto);?>"><?php
					   }
					?>
					
					&nbsp;&nbsp; <label class="checkbox pull-left" style="margin:0px"> &nbsp;&nbsp; 
					&nbsp;&nbsp;<input type="checkbox" name="keep" value="yes"> Remember me</label> 
					<label class="pull-right"><a href="javascript:void(0);" onclick="$('#login_form').hide();$('#forgot_form').show();">Forgot password?</a></label>
					<input type="submit" value="Please Login" class="btn btn-lg btn-primary btn-block" /> 
					<input type="hidden" name="process" value="Y">
				</form>
			</div>
			<br>
			<div class="account-wall" id="forgot_form" style="display:none">
				<h4 class="text-center login-title">Please enter your username</h4>
				<form method="post" action="forgotPassword.php" class="form-signin">
					<input name="user" size="23" maxlength="45" type="text"
						class="form-control" placeholder="Username" required autofocus> <br>
					<input type="submit" value="Submit"
						class="btn btn-lg btn-primary btn-block" name="ForgotPassword" id="ForgotPassword" />
					<input type="hidden" name="process_forgot_password" id="process_forgot_password" value="Y">
				</form>
			</div>
		</div>
	</div>
	
	<br><br><br><br><br><br>

	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align: center;">
			<?php
			if($brand_info['BrandID'] == "0") {
			    ?>Powered by <a href="http://www.irecruit-software.com" target="_blank">iRecruit</a>.<?php
            }
            else {
                if($brand_org_info['Url'] != "") {
                    ?><a href="<?php echo $brand_org_info['Url'];?>" target="_blank"><?php echo $brand_org_info['FooterText'];?></a><?php
                }
                else {
                    echo $brand_org_info['FooterText'];
                }
            }
			?>
			&nbsp;|&nbsp; <a href="javascript:bookmarksite('iRecruit', 'https://<?php echo $_SERVER['SERVER_NAME']?>');">Bookmark Page</a>
		</div>
	</div>
</div>

<?php
if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'forgot_password') {
    echo "<script>";
    echo "$('#login_form').hide();";
    echo "$('#forgot_form').show();";
    echo "</script>";
}
?>	
