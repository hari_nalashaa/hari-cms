<?php
// Build the given mockup dropdown values array
$from_to_time ["12:00am"] = "12:00am";
for($i = 1; $i <= 12; $i ++) {
	$ap = "am";
	if ($i == 12)
		$ap = "pm";
	$from_to_time ["$i:00$ap"] = "$i:00$ap";
}
for($j = 1; $j <= 11; $j ++) {
	$from_to_time ["$j:00pm"] = "$j:00pm";
}

$days = array (
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
		"Sunday" 
);

$shifts = unserialize ( $value );

/**
 * Temporary fix to update override the existing rows and columns
 */
if ($rows == 0 && count ( $shifts ['day_names'] ) > 0) {
	$rows = count ( $shifts ['day_names'] );
	
	if ($_REQUEST ['typeform'] == 'webforms') {
		$form_table = "WebFormQuestions";
	}
	else if ($_REQUEST ['typeform'] == 'agreementforms') {
		$form_table = "AgreementFormQuestions";
	}
	else if ($_REQUEST ['typeform'] == 'requisitionform') {
		$form_table = "RequisitionQuestions";
	}
	else if ($_REQUEST ['action'] == 'formquestions') {
		$form_table = "FormQuestions";
	}
	
	//Set update query fields
	$set = array("rows = :rows");
	//Bind parameters
	$params = array(':rows'=>$rows, ':OrgID'=>$OrgID, ':QuestionID'=>$_REQUEST ['QuestionID'], ':QuestionTypeID'=>'120');
	//Set Condition
	$where = array("OrgID = :OrgID", "QuestionID = :QuestionID", "QuestionTypeID = :QuestionTypeID");
	//Update question information
	$FormQuestionsObj->updQuestionsInfo($form_table, $set, $where, array($params));
}
?>
<br>
<br>
<script>
//Build the given mockup dropdown values array for javascript by using php array
var from_to_time = '<?php echo json_encode($from_to_time);?>';
var days_list = '<?php echo json_encode($days);?>';
var days = JSON.parse(days_list);

//Validate and display shift data
function validate_num_days(obj) {
	if(isNaN(obj.value) || obj.value > 7) {
		obj.value = 7;
	}
	shifts_data(obj);
}

function shifts_data(obj) {
	var rows = obj.value;
	var days_pulldown_options = '';
	var available_shifts_data = '';
	var scheduled_time_options = '';
	var scheduled_time = JSON.parse(from_to_time);
	
	
	//Create days option values
	for(day_index = 0; day_index <= 6; day_index++) {
		days_pulldown_options += '<option value="'+days[day_index]+'">'+days[day_index]+'</option>';
	}
	
	for (var key in scheduled_time)
	{
	    scheduled_time_options += '<option value="'+key+'">'+scheduled_time[key]+'</option>';
	}


	available_shifts_data = '<table width="50%">';
	for(data_index = 1; data_index <= rows; data_index++) {
		available_shifts_data += '<tr>';

		available_shifts_data += '<td>';
		available_shifts_data += '<select name="day_names[]">';
		available_shifts_data += days_pulldown_options;
		available_shifts_data += '</select>';
		available_shifts_data += '</td>';

		available_shifts_data += '<td>from</td>';

		available_shifts_data += '<td>';
		available_shifts_data += '<select name="from_time[]">';
		available_shifts_data += '<option value="">Select</option>';
		available_shifts_data += scheduled_time_options;
		available_shifts_data += '</select>';
		available_shifts_data += '</td>';


		available_shifts_data += '<td>to</td>';
		
		available_shifts_data += '<td>';
		available_shifts_data += '<select name="to_time[]">';
		available_shifts_data += '<option value="">Select</option>';
		available_shifts_data += scheduled_time_options;
		available_shifts_data += '</select>';
		available_shifts_data += '</td>';
		
		available_shifts_data += '</tr>';	
	}
	available_shifts_data += '</table>';
	
	document.getElementById('available_shift_data').innerHTML = available_shifts_data;	
}
</script>
<strong>Question:</strong> <input type="text" name="Question" value="<?php echo $Question;?>"><br><br>
<input type="hidden" name="QuestionTypeID" value="<?php echo $QuestionTypeID;?>">
Please enter number of days:
<input type="text" name="number_of_days" id="number_of_days"
	onkeyup="validate_num_days(this)" onkeydown="validate_num_days(this)"
	maxlength="1" size="1"
	value="<?php echo count($shifts['day_names']);?>">
<br>
<br>
<div id="available_shift_data">
	<?php
	if (count ( $shifts ['day_names'] ) > 0) {
		$available_shifts_data = '<table width="50%">';
		for($data_index = 0; $data_index < count ( $shifts ['day_names'] ); $data_index ++) {
			$available_shifts_data .= '<tr>';
			
			$available_shifts_data .= '<td>';
			$available_shifts_data .= '<select name="day_names[]">';
			
			for($day_index = 0; $day_index <= 6; $day_index ++) {
				if ($shifts ['day_names'] [$data_index] == $days [$day_index])
					$selected = 'selected="selected"';
				$available_shifts_data .= '<option value="' . $days [$day_index] . '" ' . $selected . '>' . $days [$day_index] . '</option>';
				unset ( $selected );
			}
			
			$available_shifts_data .= '</select>';
			$available_shifts_data .= '</td>';
			
			$available_shifts_data .= '<td>from</td>';
			
			$available_shifts_data .= '<td>';
			$available_shifts_data .= '<select name="from_time[]">';
			$available_shifts_data .= '<option value="">Select</option>';
			
			foreach ( $from_to_time as $ft_time ) {
				$available_shifts_data .= '<option value="' . $ft_time . '">' . $ft_time . '</option>';
			}
			
			$available_shifts_data .= '</select>';
			$available_shifts_data .= '</td>';
			
			$available_shifts_data .= '<td>to</td>';
			
			$available_shifts_data .= '<td>';
			$available_shifts_data .= '<select name="to_time[]">';
			$available_shifts_data .= '<option value="">Select</option>';
			
			foreach ( $from_to_time as $to_time ) {
				$available_shifts_data .= '<option value="' . $to_time . '">' . $to_time . '</option>';
			}
			
			$available_shifts_data .= '</select>';
			$available_shifts_data .= '</td>';
			
			$available_shifts_data .= '</tr>';
		}
		$available_shifts_data .= '</table>';
	}
	echo $available_shifts_data;
	?>
</div>