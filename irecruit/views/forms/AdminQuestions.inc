<?php
if ($formtable == "WebFormQuestions") {
	//Bind Parameters
	$params    =   array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":FormID"=>$FormID);
	//Set where condition
	$where     =   array("QuestionID = :QuestionID", "OrgID = :OrgID", "WebFormID = :WebFormID", "FormID = :FormID");
	//Get WebFormQuestions Information
	$results   =   G::Obj('FormQuestions')->getQuestionsInformation($formtable, "*", $where, "", array($params));
	
	//Get Questions Information
	$thisrow   =   $results['results'][0];
	
} else if ($formtable == "AgreementFormQuestions") {
	
	//Bind Parameters
	$params    =   array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID']);
	//Set where condition
	$where     =   array("QuestionID = :QuestionID", "OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	//Get WebFormQuestions Information
	$results   =   G::Obj('FormQuestions')->getQuestionsInformation($formtable, "*", $where, "", array($params));
	//Get Questions Information
	$thisrow   =   $results['results'][0];
	
} else if ($formtable == "RequisitionQuestions") {
	
	//Bind Parameters
	$params    =   array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID);
	//Set where condition
	$where     =   array("QuestionID = :QuestionID", "OrgID = :OrgID");
	//Get WebFormQuestions Information
	$results   =   G::Obj('FormQuestions')->getQuestionsInformation($formtable, "*", $where, "", array($params));
	//Get Questions Information
	$thisrow   =   $results['results'][0];

} else if ($formtable == "OnboardQuestions") {

	//Bind Parameters
	$params    =   array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID, ":OnboardFormID"=>$FormID);
	//Set where condition
	$where     =   array("QuestionID = :QuestionID", "OrgID = :OrgID", "OnboardFormID = :OnboardFormID");
	//Get WebFormQuestions Information
	$results   =   G::Obj('FormQuestions')->getQuestionsInformation($formtable, "*", $where, "", array($params));
	//Get Questions Information
	$thisrow   =   $results['results'][0];
} else {

	//Bind Parameters
	$params    =   array(":QuestionID"=>$QuestionID, ":OrgID"=>$OrgID, ":FormID"=>$FormID);
	//Set where condition
	$where     =   array("QuestionID = :QuestionID", "OrgID = :OrgID", "FormID = :FormID");
	//Get WebFormQuestions Information
	$results   =   G::Obj('FormQuestions')->getQuestionsInformation($formtable, "*", $where, "", array($params));
	//Get Questions Information
	$thisrow   =   $results['results'][0];
}

$name               =   "Value";
$WebFormID          =   $thisrow ['WebFormID'];
$AgreementFormID    =   $thisrow ['AgreementFormID'];
$FormID             =   $thisrow ['FormID'];
$Question           =   $thisrow ['Question'];
$QuestionTypeID     =   $thisrow ['QuestionTypeID'];
$size               =   $thisrow ['size'];
$maxlength          =   $thisrow ['maxlength'];
$rows               =   $thisrow ['rows'];
$cols               =   $thisrow ['cols'];
$wrap               =   $thisrow ['wrap'];
$value              =   $thisrow ['value'];
$defaultValue       =   $thisrow ['defaultValue'];
$Active             =   $thisrow ['Active'];
$Required           =   $thisrow ['Required'];
$SectionID          =   $thisrow ['SectionID'];
$Validate          	=   $thisrow ['Validate'];

$Values = explode ( "::", $value );


echo '<input type="hidden" name="Active" value="' . $Active. '">' . "\n";
echo '<input type="hidden" name="Required" value="' . $Required . '">' . "\n";

if (($QuestionTypeID == 18) || ($QuestionTypeID == 1818)) { // Checkbox
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';
	
	echo '<b>Answers</b>:<br>';
	
	if ($add == 'y') {
		array_push ( $Values, "" );
	}
	
	$cnt = 0;
	foreach ( $Values as $v => $q ) {
		$cnt ++;
		list ( $vv, $qq ) = explode ( ":", $q, 2 );
		if ($vv == $defaultValue) {
			$ck = ' checked';
		} else {
			$ck = '';
		}
		
		echo 'Passed Value: <input type="text" name="value' . '-' . $cnt . '" size="15" maxlength="15" value="' . $vv . '">';
		echo '&nbsp;&nbsp;&nbsp;Display Text: <input type="text" name="display' . '-' . $cnt . '" size="45" maxlength="500" value="' . $qq . '"><br>';
	}
	
	echo '<p>Sort Display Text Alphabetically: <input type="checkbox" name="sort" value="Y"></p>';
	
	if ($_REQUEST ['AgreementFormID']) {
		echo '<p><a href="' . $formscript . '?QuestionID=' . $QuestionID;
		if ($typeform) {
		    echo '&typeform=' . htmlspecialchars($typeform);
		}
		echo '&action=' . htmlspecialchars($action) . '&AgreementFormID=' . htmlspecialchars($_REQUEST ['AgreementFormID']) . '&add=y">Add a value pair.</a>';
	} else if ($_REQUEST ['WebFormID']) {
		echo '<p><a href="' . $formscript . '?QuestionID=' . $QuestionID;
		if ($typeform) {
		    echo '&typeform=' . htmlspecialchars($typeform);
		}
		echo '&action=' . htmlspecialchars($action) . '&WebFormID=' . htmlspecialchars($_REQUEST ['WebFormID']) . '&FormID=' . htmlspecialchars($FormID) . '&add=y">Add a value pair.</a>';
	} else {
		echo '<p><a href="' . $formscript . '?questionid=' . $QuestionID;
		
		if ($typeform) {
		    echo '&typeform=' . htmlspecialchars($typeform);
		}
		
		echo '&action=' . htmlspecialchars($action) . '&form=' . htmlspecialchars($form) . '&section=' . htmlspecialchars($section) . '&add=y">Add a value pair.</a>';
	}
	
	echo '&nbsp;&nbsp;&nbsp;&nbsp;To remove a value pair blank the "Display Text" field and select update.</p>';
	
	echo '<input type="hidden" name="cnt" value="' . $cnt . '">';
} else if ($QuestionTypeID == 17) { // Date
	
	echo '<br><br>';
	if ($formtable == "OnboardQuestions") {
		echo '<b>Default</b>: <input type="checkbox" name="default" value="Y"';
		if ($defaultValue == "Y") {
			echo ' checked';
		}
		echo '>Always Use Current Date<br><br>';
	}
	
	echo '<b>Question</b>: <input type="text" name="Question" size="65" maxlength="500" value="' . $Question . '"><br><br>';

	echo "<strong>Note: </strong>The following settings control the number of years available in the date picker and the validation required for this question.";

	echo "<br><br>";

	if($QuestionTypeID == '17') {

		$validate_date_field = json_decode($Validate, true);

		echo "Validate if date is more than # of years from current year: ";
		echo "<select name='date_field[years_min]'>";
		echo '<option value=\'\'>No Validation</option>';
		for($min_age = 1; $min_age < 60; $min_age++) {
			if($validate_date_field['years_min'] != "" 
				&& $validate_date_field['years_min'] == $min_age) {
				$min_checked = ' selected="selected"';
			}
			echo '<option value='.$min_age.' '.$min_checked.'>'.$min_age.'</option>';
			unset($min_checked);
		}
		echo "</select>";

		echo "<br><br>";
		
		echo "The number of years past: ";
		echo "<select name='date_field[min]'>";
		echo '<option value=\'\'>Select</option>';
		for($min_years = 1; $min_years <= 100; $min_years++) {
			if($validate_date_field['min'] == "") { $validate_date_field['min'] = 80; }
			if ($validate_date_field['min'] == $min_years) {
				$min_checked = ' selected="selected"';
			}
			echo '<option value='.$min_years.' '.$min_checked.'>'.$min_years.'</option>';
			unset($min_checked);
		}
		echo "</select>";

		echo "<br><br>";
		
		echo "The number of years in the future: ";
		echo "<select name='date_field[max]'>";
		echo '<option value=\'\'>Select</option>';
		for($max_years = 1; $max_years <= 100; $max_years++) {
			if($validate_date_field['max'] == "") { $validate_date_field['max'] = 5; }
			if ($validate_date_field['max'] == $max_years) {
				$max_checked = ' selected="selected"';
			}
			echo '<option value='.$max_years.' '.$max_checked.'>'.$max_years.'</option>';
			unset($max_checked);
		}
		echo "</select>";

		echo "<br><br>";

	}
	
} else if ($QuestionTypeID == 15) { // Social Security
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 19) { // Social Insurance
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 14) { // Business Phone
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 13) { // Home Phone
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 12) { // On Off
	
	echo '<input type="hidden" name="Question" value="' . $Question . '">';
	echo '<br><br>';
	echo $Question . '<br/><br/>';
} else if ($QuestionTypeID == 11) { // Password	
    ?>
	<input type="password" name="<?php echo $name ?>" size="<?php echo $size ?>" maxlength="<?php echo $maxlength ?>">
	<?php
} else if ($QuestionTypeID == 10) { //Hidden
    ?>
	<input type="hidden" name="<?php echo $name ?>" size="<?php echo $size ?>" value="<?php echo $value ?>">
	<?php
} else if ($QuestionTypeID == 9) { // Checkbox with Year and Comment Text
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';
	
	echo '<b>Answers</b>:<br>';
	
	if ($add == 'y') {
		array_push ( $Values, "" );
	}
	
	$cnt = 0;
	foreach ( $Values as $v => $q ) {
		$cnt ++;
		list ( $vv, $qq ) = explode ( ":", $q, 2 );
		if ($vv == $defaultValue) {
			$ck = ' checked';
		} else {
			$ck = '';
		}
		
		echo 'Passed Value: <input type="text" name="value' . '-' . $cnt . '" size="15" maxlength="15" value="' . $vv . '">';
		echo '&nbsp;&nbsp;&nbsp;Display Text: <input type="text" name="display' . '-' . $cnt . '" size="25" maxlength="35" value="' . $qq . '"><br>';
	}
	
	echo '<p>Sort Display Text Alphabetically: <input type="checkbox" name="sort" value="Y"></p>';
	
	if ($_REQUEST ['AgreementFormID']) {
	    echo '<p><a href="' . $formscript . '?QuestionID=' . htmlspecialchars($QuestionID);
		if ($typeform) {
		    echo '&typeform=' . htmlspecialchars($typeform);
		}
		echo '&action=' . htmlspecialchars($action) . '&AgreementFormID=' . htmlspecialchars($_REQUEST ['AgreementFormID']) . '&add=y">Add a value pair.</a>';
	} else if ($_REQUEST ['WebFormID']) {
	    echo '<p><a href="' . $formscript . '?QuestionID=' . htmlspecialchars($QuestionID);
		if ($typeform) {
		    echo '&typeform=' . htmlspecialchars($typeform);
		}
		echo '&action=' . htmlspecialchars($action) . '&WebFormID=' . htmlspecialchars($_REQUEST ['WebFormID']) . '&FormID=' . htmlspecialchars($FormID) . '&add=y">Add a value pair.</a>';
	} else {
	    echo '<p><a href="' . $formscript . '?questionid=' . htmlspecialchars($QuestionID);
		if ($typeform) {
		    echo '&typeform=' . htmlspecialchars($typeform);
		}
		echo '&action= ' . htmlspecialchars($action) . '&form=' . htmlspecialchars($form) . '&section=' . htmlspecialchars($section) . '&add=y">Add a value pair.</a>';
	}
	
	echo '&nbsp;&nbsp;&nbsp;&nbsp;To remove a value pair blank the "Display Text" field and select update.</p>';
	
	echo '<input type="hidden" name="cnt" value="' . $cnt . '">';
} else if ($QuestionTypeID == 8) { // file upload
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="65" maxlength="500" value="' . $Question . '"><br><br>';
	echo '<b>Type of Document</b>: <input type="text" name="answer" size="20" maxlength="35" value="' . $value . '"><br><br>';
} else if ($QuestionTypeID == 7) { // display text area
	//Set where condition
    $where      =   array("OrgID = :OrgID", "TextBlockID = :TextBlockID");
	//Set parameters for prepared query
    $params     =   array(":OrgID"=>$OrgID, ":TextBlockID"=>$QuestionID);
    $thisrow    =   G::Obj('FormFeatures')->getTextBlocksInfo(array('Text'), $where, "", array($params));
    $value      =   $thisrow['results'][0]['Text'];
    ?>
	<td valign="top" width="150">
		<?php echo $Question?>
	</td>
	<td>
		<textarea name="<?php echo $name ?>" id="<?php echo $name ?>" rows="<?php echo $rows ?>" cols="<?php echo $cols ?>"
		wrap="<?php echo $wrap ?>" class="mceEditor"><?php echo $value ?></textarea>
	</td>
	<?php
} else if ($QuestionTypeID == 6) { // text
	
	echo '<br><br>';
	if ($formtable == "OnboardQuestions") {
		echo '<b>Default</b>: <input type="text" name="default" size="5" maxlength="8" value="' . $defaultValue . '"><br><br>';
	}
	echo '<b>Question</b>: <input type="text" name="Question" size="65" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 5) { // text area
	
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="65" maxlength="500" value="' . $Question . '"><br><br>';
} else if (($QuestionTypeID == 23) || ($QuestionTypeID == 22) || ($QuestionTypeID == 2) || ($QuestionTypeID == 4) || ($QuestionTypeID == 3)) {
	
	if($QuestionID != "cloud_PayCode" 
        && $QuestionID != "cloud_EmployeeType"
        && $QuestionID != "cloud_Position"
        && $QuestionID != "cloud_Status"
        && $QuestionID != "cloud_WorkSite"
        && $QuestionID != "cloud_Division"
        && $QuestionID != "cloud_Department"
        && $QuestionID != "cloud_PayGroup"
        && $QuestionID != "cloud_WorkShift"
        && $QuestionID != "matrixcare_OfficeID") {

        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Clear Default</b>: <input type="checkbox" name="Clear" value="Y">';
        echo '<br><br>';

        echo '<b>Question</b>: <input type="text" name="Question" size="60" maxlength="500" value="' . $Question . '"><br><br>';

        echo '<b>Answers</b>:<br>';
        
        if ($add == 'y') {
            array_push ( $Values, "" );
        }
        
        $cnt = 0;
        foreach ( $Values as $v => $q ) {
            $cnt ++;
            list ( $vv, $qq ) = explode ( ":", $q, 2 );
            if ($vv == $defaultValue) {
                $ck = ' checked';
            } else {
                $ck = '';
            }
        
            $maxl = "100";
            
            echo 'Passed Value: <input type="text" name="value' . '-' . $cnt . '" size="15" maxlength="15" value="' . $vv . '">';
            echo '&nbsp;&nbsp;&nbsp;Display Text: <input type="text" name="display' . '-' . $cnt . '" size="45" maxlength="' . $maxl . '" value="' . $qq . '">';
            echo '&nbsp;&nbsp;&nbsp;Default: <input type="radio" name="default" value="default-' . $cnt . '"' . $ck . '><br>';
        }
        
        echo '<p>Sort Display Text Alphabetically: <input type="checkbox" name="sort" value="Y"></p>';
        
        if ($_REQUEST ['AgreementFormID']) {
            echo '<p><a href="' . $formscript . '?QuestionID=' . $QuestionID;
            if ($typeform) {
                echo '&typeform=' . $typeform;
            }
            echo '&action=' . $action . '&AgreementFormID=' . $_REQUEST ['AgreementFormID'] . '&add=y">Add a value pair.</a>';
        } else if ($_REQUEST ['WebFormID']) {
            echo '<p><a href="' . $formscript . '?QuestionID=' . $QuestionID;
            if ($typeform) {
                echo '&typeform=' . $typeform;
            }
            echo '&action=' . $action . '&WebFormID=' . $_REQUEST ['WebFormID'] . '&FormID=' . $FormID . '&add=y">Add a value pair.</a>';
        } else {
            echo '<p><a href="' . $formscript . '?questionid=' . $QuestionID;
            if ($typeform) {
                echo '&typeform=' . $typeform;
            }
            echo '&action=' . $action . '&form=' . $form . '&section=' . $section . '&add=y">Add a value pair.</a>';
        }
        
        echo '&nbsp;&nbsp;&nbsp;&nbsp;To remove a value pair blank the "Display Text" field and select update.</p>';
        
        echo '<input type="hidden" name="cnt" value="' . $cnt . '">';
	}
	else if($QuestionID == "matrixcare_OfficeID") {
        echo '<b>Question</b>: ' . $Question . '<br><br>';
        echo "You can't edit this information, as it is automatically updated by matrix care api. By default preview shows first company.";
	}
	else {
        echo '<b>Question</b>: ' . $Question . '<br><br>';
		echo "You can't edit this information, as it is automatically updated by cloud payroll api.";
	}
} else if ($QuestionTypeID == 24) { // Date / From to present
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="65" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 1) { // Date / From to
	echo '<br><br>';
	echo '<b>Question</b>: <input type="text" name="Question" size="65" maxlength="500" value="' . $Question . '"><br><br>';
} else if ($QuestionTypeID == 90) { // instructions
	echo '<br><br>';
	echo '<b>Instruction</b>:<br><br>';
	echo '<textarea name="Question" cols="' . $cols . '" rows="' . $rows . '" wrap="' . $wrap . '"';
	// echo ' class="mceEditor"';
	echo '>' . $Question . '</textarea><br><br>';
	echo 'Attach file to pop-up supporting information';
	echo '&nbsp;&nbsp;';
	echo '<input type="file" name="AttachmentFile"> <font style="font-size:8pt">(Please limit file to 200 kb)</font><br><br>';
	echo 'Text to display pop-up link';
	echo '&nbsp;&nbsp;';
	echo '<input type="text" name="AttachmentName" size="30" maxlength"60" value="' . $Values [0] . '"><br><br>';
} else if ($QuestionTypeID == 99) { // instructions
	echo '<br><br>';
	echo '<b>Instruction</b>:<br><br>';
	echo '<textarea name="Question" cols="' . $cols . '" rows="' . $rows . '" wrap="' . $wrap . '"';
	echo ' class="mceEditor"';
	echo '>' . $Question . '</textarea><br>';
} else if ($QuestionTypeID == 98) { // spacer
} else if ($QuestionTypeID == 99) { // instructions
} else if ($QuestionTypeID == 30) { // Signature
	echo '<input type="hidden" name="Question" value="' . $Question . '">';
} else if ($QuestionTypeID == 60) { // Form Merge Content
	echo '<input type="hidden" name="Question" value="' . $Question . '">';
	echo '<br><br>';
	echo '<div id="twopartform"></div>' . "\n";
	echo '<script>' . "\n";
	echo "loadTwoPartFormQuestions('','','" . $AgreementFormID . "','" . $AccessCode . "');" . "\n";
	echo '</script>' . "\n";
	echo '<textarea name="mergecontent" cols="70" rows="10" class="mceEditor">' . $value . '</textarea>' . "\n";
} else if ($QuestionTypeID == 100) { // Radio with group of skills
	include 'RadioWithGroupOfSkills.inc';
} else if ($QuestionTypeID == 120) { // Pull down with custom
	include 'PullDownWithShifts.inc';

} ?>
