<?php
if ($process == 'Y') {
	
	// lookup the display text for Text Block Section
	$params = array(':TextBlockID'=>$section);
	//Get textblock section information by textblock id
	$results = $FormSectionsObj->getTextBlockSectionInfoByID(array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
			$text_section = $row ['Section'];
		}	
	}	
	
	if ($formscript == "formAA.php") {
		$text_section = "Affirmative Action";
	}
	
	//Set parameters for prepared query
	$params = array(":OrgID"=>$OrgID, ":FormID"=>$form, ":TextBlockID"=>$section, ":IText"=>$_REQUEST['content'], ":UText"=>$_REQUEST['content']);
	// Update Text here
	$FormFeaturesObj->insTextBlocksUpdateOnDuplicate(array($params));
	
	// print success message
	$message = $text_section . ' text has been configured!';
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('" . $message . "')";
	echo '</script>';
} // end process
  
//Set parameters for prepared query
$params = array(":OrgID"=>$OrgID, ":FormID"=>$form, ":TextBlockID"=>$section);
//Set where condition
$where = array("OrgID = :OrgID", "FormID = :FormID", "TextBlockID = :TextBlockID");
$results = $FormFeaturesObj->getTextBlocksInfo(array("Text"), $where, "", array($params));

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$content = $row ['Text'];
	}	
}
?>
<form action="<?php echo $formscript?>" method="post"> 
<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" width="100%"
		class="table table-bordered">
		<tr>
			<td><p>
					<b>Edit Text</b>
				</p></td>
		</tr>
		<tr>
			<td align="center">
				<?php
				echo '<textarea name="content" cols="90" rows="10"';
				echo ' class="mceEditor"';
				echo '>' . $content . '</textarea>';
				?>
			</td>
		</tr>
		<tr>
			<td height="60" valign="middle" align="center">
				<input type="hidden" name="action" value="<?php echo $action ?>" /> 
				<input type="hidden" name="section" value="<?php echo $section ?>" /> 
				<input type="hidden" name="form" value="<?php echo $form ?>" /> 
				<input type="hidden" name="process" value="Y" /> 
				<input type="submit" name="submit" value="Submit" class="btn btn-primary" />
			</td>
		</tr>
		<tr>
			<td colspan="100%">
				<hr size=1>
				<p>
					<b>Preview</b>
				</p>
				<?php echo $content ?>
			</td>
		</tr>
	</table>
</div>
</form>