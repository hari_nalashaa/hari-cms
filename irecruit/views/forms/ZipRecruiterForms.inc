<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- /.row -->
	<div class="row">
	   <div class="col-lg-12">
            <h5><i><u>Note</u>:</i> Add pre-screen interview questions to save time and ensure you only see the most qualified candidates from ZipRecruiter.</h5>
	   </div>
	</div>
	<div class="row" style="color:blue">
	   <div id="manage_label_info" class="col-lg-12">
	       <?php 
	           if(isset($_GET['msg']) && $_GET['msg'] == 'updsuc') {
	               echo 'Updated Successfully.';	
	           }
	           else if(isset($_GET['msg']) && $_GET['msg'] == 'inssuc') {
	               echo 'Inserted Successfully.';
	           }
	           else if(isset($_GET['msg']) && $_GET['msg'] == 'copysuc') {
	               echo 'Copied Successfully.';
	           }
	           else if(isset($_GET['msg']) && $_GET['msg'] == 'delsuc') {
	               echo 'Deleted Successfully.';
	           }
	           else if(isset($_GET['msg']) && $_GET['msg'] == 'defsuc') {
	               echo 'Default Status Updated Successfully.';
               }
	       ?>
        </div>
	</div>

	<div id="openFormWindow" class="openForm">
    	<div>
    		<p style="text-align:right;">
    			<a href="#closeForm" title="Close Window">
    				<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
    				<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
    			</a>
    		</p>
    			
    		<div id="ziprecruiter_view_form_questions"></div>
    	</div>
    </div>
	
	
    <div class="row">
        <div class="col-lg-12">
            <h4>Manage Templates:</h4>
        </div>
        <div class="col-lg-12">
            <form name="frmManageLabels" id="frmManageLabels" method="post">
            Templates:
            <select name="ddlZipRecruiterIQLabels" id="ddlZipRecruiterIQLabels" onchange="manageLabels();" class="form-control width-auto-inline">
                <option value="">Select</option>
                <?php
                    foreach($labels_list as $label_id=>$label_info) {
                        ?><option value="<?php echo $label_id;?>"><?php echo $label_info['LabelName'];?></option><?php
                    }
                ?>
            </select> &nbsp;
            <?php
                $btn_label = "Add";
                $clabel_id = "";
                if(isset($_REQUEST['clabel_id']) && $_REQUEST['clabel_id'] != "") {
                	$btn_label = "Copy";
                	$clabel_id = $_REQUEST['clabel_id'];
                }
            ?>
            Template Name: <input type="text" name="txtLabel" id="txtLabel" class="form-control width-auto-inline">
            <input type="button" name="btnAddEdit" id="btnAddEdit" class="btn btn-primary" value="<?php echo htmlspecialchars($btn_label);?>" onclick="addUpdateLabel()">
            <input type="hidden" name="hidCLabelID" id="hidCLabelID" value="<?php echo htmlspecialchars($clabel_id);?>">
            </form>
        </div>
    </div>
	
	<div class="row">

	<div class="col-lg-12">
        <h4>Manage Questions:</h4>
    </div>
    
    <div class="col-lg-12">
    <form method="post" name="frmLabelsList" id="frmLabelsList">
    <table class="table table-bordered">
      <tr>
        <td colspan="7">
        Add pre-screen interview questions to save time and ensure you only see the most qualified candidates from ZipRecruiter. <br>
        <u><i>Note:</i></u> ZipRecruiter recommends having 1-5 screening questions to make it easy to apply.<br>
        <u><i>Note:</i></u> Drag and drop the questions to update the display order.
        </td>
      </tr>
      <tr>
        <td><strong>Template Name</strong></td>
        <td><strong>Question</strong></td>
        <td><strong>View Form</strong></td>
        <td><strong>Default</strong></td>
        <td><strong>Copy</strong></td>
        <td><strong>Edit</strong></td>
        <td><strong>Delete</strong></td>
      </tr>
    <?php
    if(count($labels_list) > 0) {
    	foreach($labels_list as $list_label_id=>$label_info) {
    		?>
    		  <tr>
    		    <td><?php echo $label_info['LabelName'];?></td>
    		    <td>
    		          <?php 
    		              $l = 1;
    		              if(is_array($zip_rec_intw_que_lbl[$list_label_id])) {
        		              	foreach($zip_rec_intw_que_lbl[$list_label_id] as $zip_label_id=>$zip_label_info) {
                                    echo $l.") ".$zip_label_info['Question']."<br>";
                                    $l++;
                                }
    		              }
    		          ?>
    		    </td>
                <td>
                    <a href="#openFormWindow" onclick="zipRecruiterViewForm('<?php echo $list_label_id;?>')">
                        <img src="<?php echo IRECRUIT_HOME;?>images/icons/page_white_magnify.png" title="View" border="0">
                    </a>                        
                </td>
                <td>
                    <input type="radio" name="rdIsDefault" id="rdIsDefault" value="<?php echo $list_label_id;?>" <?php if(isset($label_info['IsDefault']) && $label_info['IsDefault'] == 'Y') echo 'checked="checked"';?> onclick="document.forms['frmLabelsList'].submit();">
                </td>
                <td>
                    <a onclick="copyLabel('<?php echo $list_label_id;?>')">
                        <img src="<?php echo IRECRUIT_HOME;?>images/icons/application_cascade.png" title="Copy" border="0">
                    </a>
                </td>
                <td>
                    <?php 
                    if($list_label_id != 'DEFAULT') {
                    	?>
                        <a href="zipRecruiterInterviewQuestions.php?elabel_id=<?php echo $list_label_id;?>&menu=4">
                            <img src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png" title="Edit" border="0">
                        </a>
                        <?php
                    }
                    ?>
                </td>
                <td>
                    <?php 
                    if($list_label_id != 'DEFAULT') {
                    	?>
                        <a href="zipRecruiterForms.php?menu=4&dlabel_id=<?php echo $list_label_id;?>&menu=4">
                            <img src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png" title="Delete" border="0">
                        </a>
                        <?php
                    }
                    ?>
                </td>
    		  </tr>
    		<?php
    	}
    }
    else {
    	?>
    	<tr><td colspan="7" align="center">You haven't added any labels</td></tr>
    	<?php
    }
    ?>
    </table>
    </form>
    </div>
</div>
	
	
	<!-- /.row -->
</div>
<script>
    function zipRecruiterViewForm(label_id) {
        
    	var request_url = irecruit_home + 'zipRecruiterViewForm.php?vlabel_id='+label_id;
	    $.ajax({
			method: "POST",
	  		url: request_url,
			type: "POST",
			success: function(data) {
			    $("#ziprecruiter_view_form_questions").html(data);
	    	}
		});
        	
    }

    function copyLabel(label_id) {

    	var copy_form = confirm("Please update the label name and click on copy button.");
    	if (copy_form == true) {
    		document.forms['frmManageLabels'].hidCLabelID.value = label_id;
    		document.getElementById('btnAddEdit').value = 'Copy';
    		$('#ddlZipRecruiterIQLabels>option[value="' + label_id + '"]').prop('selected', true);
    		var sel_lbl_txt = $("#ddlZipRecruiterIQLabels option:selected").text();
    		document.getElementById('txtLabel').value = sel_lbl_txt + ' Copy';
    	}
    }
    
	function addUpdateLabel() {
		var label_name = $("#txtLabel").val();
		var e = document.getElementById("ddlZipRecruiterIQLabels");
		var label_id = e.options[e.selectedIndex].value;

		var clabel_id = document.forms['frmManageLabels'].hidCLabelID.value;
		
		if(clabel_id != "" && label_name != "") {
			request_url = "manageLabels.php?identifier=zil&operation=copy&label_name="+label_name+"&label_id="+clabel_id;
	    	operation = "copy";
		}
		else if(label_name != "" && label_id != "") {
	    	request_url = "manageLabels.php?identifier=zil&operation=update&label_name="+label_name+"&label_id="+label_id;
	    	operation = "update";
		}
	    else if(label_name != "") {
	    	request_url = "manageLabels.php?identifier=zil&operation=insert&label_name="+label_name;
	    	operation = "insert";
		}    
		
    	if(operation != "") {
    	    $.ajax({
    			method: "POST",
    	  		url: request_url,
    			type: "POST",
    			dataType: 'json',
    			success: function(data) {
        			
        			if(operation == "update") {
        				location.href = 'zipRecruiterForms.php?menu=4&msg=updsuc';
        			}
        			else if(operation == "insert") {
        				location.href = 'zipRecruiterForms.php?menu=4&msg=inssuc';
                	}
        			else if(operation == "copy") {
        				location.href = 'zipRecruiterForms.php?menu=4&msg=copysuc';
                	}
                	
    	    	}
    		});
    	}    
	}

	function getQuestionsByLabelID(label_id) {
		location.href = 'zipRecruiterForms.php?menu=4&label_id='+label_id;
	}
</script>