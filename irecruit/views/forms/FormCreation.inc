<?php
if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'copyform') {
	
	if (isset($_REQUEST['process']) && $_REQUEST['process'] == 'Y') {
		
		if (isset($_REQUEST['newname'])) {
			
			// Remove all characters except letter and numbers from a string
			$new_name = preg_replace ( "/[^A-Za-z0-9]/", "", $_REQUEST['newname'] );
			
			//Copy Application Form Sections
			G::Obj('ApplicationFormSections')->copyApplicationFormMasterSections($OrgID, $OrgID, $form, $new_name);
			
			//Insert Default Form Questions By Select Stmt
			$FormQuestionsObj->insDefaultFormQuestions($OrgID, $new_name, $form);
			
			//Insert Text Blocks
			$FormFeaturesObj->insDefaultTextBlocks($OrgID, $new_name, $form);
			
			$message = 'Form ' . $new_name . ' has been added!';
			
			echo '<script language="JavaScript" type="text/javascript">';
			echo "alert('" . $message . "')";
			echo '</script>';
		}
	} else {
		
		echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered">';
		echo '<tr><td width="100%" height="35">&nbsp;</td></tr>';
		echo '<tr><td>';
		echo '<form method="post" action="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '">';
		echo 'What name do you want to call your new form?';
		echo '&nbsp;&nbsp;<input type="text" name="newname" size="20">';
		echo '&nbsp;<i>(Please do not use spaces)</i></td></tr>';
		echo '<tr><td align="center" height="60" valign="middle">';
		echo '<input type="hidden" name="process" value="Y">';
		echo '<input type="hidden" name="action" value="' . htmlspecialchars($action) . '">';
		echo '<input type="hidden" name="form" value="' . htmlspecialchars($form) . '">';
		echo '<input type="submit" value="Copy ' . htmlspecialchars($form) . ' form" class="btn btn-primary">';
		echo '</td></tr>';
		echo '</form>';
		echo '</table>';
	}
}

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'resetform') {
	
	if ($_REQUEST['process'] == 'Y') {
		
		//Insert Form Questions Master Data
		$FormQuestionsObj->insMasterFormQuestions($OrgID);
		
		//Insert Master Text Blocks
		$FormFeaturesObj->insMasterTextBlocks($OrgID);
		
		//Set MultiOrgID
		if(!isset($MultiOrgID)) $MultiOrgID = '';
		
		$results = $OrganizationDetailsObj->getOrganizationInformation($OrgID, $MultiOrgID, "OrganizationName");;
		$OrganizationName = $results ['OrganizationName'];
		
		//Set where condition
		$where = array("OrgID = :OrgID", "FormID = :FormID");
		
		//Set parameters for prepared query
		$params = array(":OrgID"=>$OrgID, ":FormID"=>'STANDARD');
		$results = $FormFeaturesObj->getTextBlocksInfo(array('TextBlockID', 'Text'), $where, "", array($params));
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $row) {
				$newText = preg_replace ( "/\{COMPANY\-NAME\}/i", $OrganizationName, $row ['Text'] );
				$FormFeaturesObj->updTextBlocks($OrgID, 'STANDARD', $row ['TextBlockID'], $newText);
			}	
		}
		
		
		$message = 'The ' . $form;
		$message .= ' form has been reset to its defaults.';
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
	}
}


if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'deleteform') {
	
	if (isset($_REQUEST['process']) && $_REQUEST['process'] == 'Y') {

		// check to see if the form is still in use.
		
		//Set Condition
		$where	=  array("OrgID = :OrgID", "FormID = :FormID");
		// Bind the parameters to below query based on above condition
		$params = array(':OrgID'=>$OrgID, ':FormID'=>$form);
		$results  =  $RequisitionsObj->getRequisitionInformation(array('COUNT(*) AS ReqCount'), $where, "", "", array($params));
		if(is_array($results['results'])) {
			foreach ($results['results'] as $row) {
				$hit = $row ['ReqCount'];
			}
		}
		
		
		// if the form is in use do not delete.
		if ($hit > 0) {
			echo '<center>Form <b>' . htmlspecialchars($form);
			echo '</b> is still in use and cannot be deleted.</center>';
		} else {
			$result_del_info = array();
			
			//Delete From CustomQuestions
			$result_del_info[] = $FormFeaturesObj->delFormCreationInfo('CustomQuestions', $OrgID, $form);
			
			//Delete From TextBlocks
			$result_del_info[] = $FormFeaturesObj->delFormCreationInfo('TextBlocks', $OrgID, $form);
			
			//Delete From FormQuestions
			$result_del_info[] = $FormFeaturesObj->delFormCreationInfo('FormQuestions', $OrgID, $form);
			
			$message = 'Form ' . $form;
			$message .= ' has been deleted.';
			
			echo '<script language="JavaScript" type="text/javascript">';
			echo "alert('" . $message . "')";
			echo '</script>';
		}
	}
}

echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td><b>Form</b></td>';
echo '<td align="center" style="width:100px;"><b>View Form</b></td>';
if ($permit ['Forms_Edit'] == 1) {
	echo '<td align="center" style="width:100px;"><b>Copy</b></td>';
	echo '<td align="center" style="width:100px;"><b>Edit</b></td>';
	echo '<td align="center" style="width:100px;"><b>Delete</b></td>';
}
echo '</tr>';

//Get FormQuestions Information
$columns	=  'DISTINCT(FormID) AS FormID';
$where		=  array("OrgID = :OrgID", "FormID != 'MASTER'");

//Bind the parameters array to above condition and also in below condition in loop
$params     =   array(':OrgID'=>$OrgID);
$results    =   $FormQuestionsObj->getFormQuestionsInformation($columns, $where, '', array($params));
$rowcolor   =   "#eeeeee";

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		
        $form       =   $row ['FormID'];
		
		//Set Columns Count To Get The Questions Information
        $columns    =	'COUNT(*) AS notstandard';
		
        $params     =   array(':OrgID'=>$OrgID);
		
		//Set Where Condition
		$where		=	array();
		$where[]	=	"OrgID = :OrgID";
		$where[]	=	"QuestionID NOT IN (SELECT QuestionID FROM FormQuestions WHERE OrgID='MASTER' AND FormID='STANDARD')";
		
		$notstandard_result = $FormQuestionsObj->getFormQuestionsInformation($columns, $where, '', array($params));
		
		//Get Count Result
		$notstandard =	$notstandard_result['results'][0]['notstandard'];
		
		echo '<tr bgcolor="' . $rowcolor . '">';
		
		echo '<td valign=top>' . htmlspecialchars($form) . '</td>';
		
		$onclickex = "onclick=\"javascript:window.open('" . IRECRUIT_HOME . "forms/preview_application.php?OrgID=" . $OrgID . "&FormID=" . $form . "&View=Y";
		if ($AccessCode != "") {
			$onclickex .= "&k=" . $AccessCode;
		}
		$onclickex .= "','_blank','location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";
		
		echo '<td valign="top" align="center"><a href="#" ' . $onclickex . '><img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View"></a></td>';
		
		if ($permit ['Forms_Edit'] == 1) {
			
			// if CUSTOM forms
			if ($form != 'STANDARD') {
				echo '<td valign="top" align="center"><a href="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '?form=' . htmlspecialchars($form) . '&action=copyform"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a></td>';
				
				echo '<td valign="top" align="center"><a href="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '?form=' . htmlspecialchars($form) . '&action=formquestions&section=0&form_home=yes"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
				if ($form != 'STANDARD') {
					echo '</td>';
					echo '<td valign="top" align="center"><a href="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '?form=' . htmlspecialchars($form) . '&action=deleteform&process=Y"  onclick="return confirm(\'Are you sure you want to delete the following form?\n\nForm: ' . $form . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a></td>';
				}
				// the STANDARD form
			} else {
				
				echo '<td valign="top" align="center"><a href="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '?form=' . htmlspecialchars($form) . '&action=copyform"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a></td>';
				
				echo '<td valign="top" align="center"><a href="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '?form=' . htmlspecialchars($form) . '&action=formquestions&section=0&form_home=yes"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
				if (! $notstandard) {
					echo '<td>&nbsp;&nbsp;</td>';
					echo '</td>';
					
					if ($notstandard) {
						echo '<td>&nbsp;&nbsp;</td>';
					} // end permit edit
				} else {
					echo '<td valign=top align=center><a href="' . htmlspecialchars($_SERVER ['SCRIPT_NAME']) . '?form=' . htmlspecialchars($form) . '&action=resetform&process=Y" onclick="return confirm(\'Are you sure you want to reset the STANDARD form?\n\n\')"><img src="images/icons/resultset_previous.png" border="0" title="Reset"></a></td>';
				}
			}
		}
		
		echo '</tr>';
		
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}
?>
</table>
