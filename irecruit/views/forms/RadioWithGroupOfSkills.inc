<?php
$rows_empty = "False";
$cols_empty = "False";

$rows_count = $rows;
$cols_count = $cols;

$group_skills = unserialize ( $value );

/**
 * Temporary fix to update override the existing rows and columns
 */
if ($rows_count == 0 || $cols_count == 0) {
	$rows = $rows_count = count ( $group_skills ['LabelValRow'] );
	$cols = $cols_count = count ( $group_skills ['RVal'] );
	
	if ($_REQUEST ['typeform'] == 'webforms') {
		$form_table = "WebFormQuestions";
	}
	if ($_REQUEST ['typeform'] == 'agreementforms') {
		$form_table = "AgreementFormQuestions";
	}
	if ($_REQUEST ['typeform'] == 'requisitionform') {
		$form_table = "RequisitionQuestions";
	}
	if ($_REQUEST ['action'] == 'formquestions') {
		$form_table = "FormQuestions";
	}
	
	//Set update query fields
	$set = array("rows = :rows", "cols = :cols");
	//Bind the parameters
	$params = array(':rows'=>$rows, ':cols'=>$cols, ':OrgID'=>$OrgID, ':QuestionID'=>$_REQUEST ['QuestionID'], ':QuestionTypeID'=>'100');
	//Set Condition
	$where = array("OrgID = :OrgID", "QuestionID = :QuestionID", "QuestionTypeID = :QuestionTypeID");
	//Update Question Information
	$FormQuestionsObj->updQuestionsInfo($form_table, $set, $where, array($params));
}

if ($rows == "" || $rows == 0 || $rows == "0") {
	$rows_empty = "True";
	$rows_count = 2;
}
if ($cols == "" || $cols == 0 || $cols == "0") {
	$cols_empty = "True";
	$cols_count = 2;
}

?>
<script type="text/javascript">
var text_box_name;
var text_box_value;

$(document).ready(function() {
    $("form").submit(function() {
    	var error = false;	
    	$('form input[type="text"]').each(function(){
    		text_box_name = $(this).attr("name");
    		text_box_value = $(this).val();
    		if(text_box_name.indexOf("LabelValRow") == 0 || text_box_name.indexOf("RVal") == 0)
    		{
    			if(!validate_label_name(text_box_value)) error = true;
			}
	    });
	    if(error) return false;
    })
});
function validate_rows_cols(obj) {
	if(isNaN(obj.value)) {
		obj.value = 2;
	}
}
function update_radio_value(radio_value, column_index) {
	var rows_count = $("#rows").val();

	for(j = 1; j <= rows_count; j++) {
		$('#LabelSelect'+j+column_index).val( radio_value );
	}
}
function validate_label_name(str) {
	var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(14); //Shift Out
    specialKeys.push(15); //Shift In
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right
    specialKeys.push(32); //Right
    

    len = str.length;
    for (i = 0;  i < len; i++) {
    	keyCode = str.charCodeAt(i);
        //var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32);
        if (!ret) {
          alert("Only alphanumeric characters are allowed");
          return false;
        }
    }

    return true;
}
function createTable() {
	var rows_count = $("#rows").val();
	var cols_count = $("#cols").val();
	
	var table_data = '';
	for(row_index = 0; row_index <= rows_count; row_index++) {

		table_data += '<tr>';

		
		for(col_index = 0; col_index <= cols_count; col_index++) {
			if(row_index == 0) {
				if(col_index == 0) {
					table_data += '<td>&nbsp;</td>';
				}
				else {
					table_data += '<td><input type=\'text\' name=\'RVal[RVal'+col_index+']\' size=\'5\' onchange=\'update_radio_value(this.value, "'+col_index+'")\' onkeydown=\'return validate_label_name(event)\' onkeyup=\'return validate_label_name(event)\'></td>';
				}
			}
			else {
				if(col_index == 0) {
					table_data += '<td><input type="text" name="LabelValRow[LabelValRow'+row_index+']" id="LabelValRow'+row_index+'" onkeydown=\'return validate_label_name(event)\' onkeyup=\'return validate_label_name(event)\'></td>';
				}
				else {
					table_data += '<td><input type="radio" name="LabelSelect[LabelSelect'+row_index+']" id="LabelSelect'+row_index+col_index+'"></td>';
				}
				
			}
			
		}

		table_data += '</tr>';
	} 

	$('#skillsTable').html(table_data);
} 
</script>

<br>
<br>
<strong>Note:</strong>
Please don't give spaces for values
<br>
<br>

<strong>Question:</strong> <input type="text" name="Question" value="<?php echo $Question;?>"><br>

<br>
Please enter number of rows and columns:

Rows:
<input type="text" name="rows" id="rows" size="2" maxlength="2"
	value="<?php echo $rows_count;?>" onchange="createTable()"
	onkeyup="validate_rows_cols(this)" onkeydown="validate_rows_cols(this)">
Columns:
<input type="text" name="cols" id="cols" size="2" maxlength="2"
	value="<?php echo $cols_count;?>" onchange="createTable()"
	onkeyup="validate_rows_cols(this)" onkeydown="validate_rows_cols(this)">
<br>
<br>


<input type="hidden" name="QuestionTypeID" value="<?php echo $QuestionTypeID;?>">
<table cellspacing="5" cellpadding="5" border="0"
	id="skillsTable">
	<?php
	$table_data = '';
	for($row_index = 0; $row_index <= $rows_count; $row_index ++) {
		
		$table_data .= '<tr>';
		
		for($col_index = 0; $col_index <= $cols_count; $col_index ++) {
			if ($row_index == 0) {
				if ($col_index == 0) {
					$table_data .= '<td>&nbsp;</td>';
				} else {
					$table_data .= '<td><input type="text" name="RVal[RVal' . $col_index . ']" size="5" value="' . $group_skills ["RVal"] ["RVal" . $col_index] . '" onchange=\'update_radio_value(this.value, "' . $col_index . '")\' onkeydown=\'return validate_label_name(event)\' onkeyup=\'return validate_label_name(event)\'></td>';
				}
			} else {
				if ($col_index == 0) {
					$table_data .= '<td><input type="text" name="LabelValRow[LabelValRow' . $row_index . ']" id="LabelValRow' . $row_index . '" value="' . $group_skills ["LabelValRow"] ["LabelValRow" . $row_index] . '" onkeydown=\'return validate_label_name(event)\' onkeyup=\'return validate_label_name(event)\'></td>';
				} else {
					$table_data .= '<td><input type="radio" name="LabelSelect[LabelSelect' . $row_index . ']"';
					
					if ($group_skills ["RVal"] ["RVal" . $col_index] != "" && ($group_skills ['LabelSelect'] ['LabelSelect' . $row_index] == $group_skills ["RVal"] ["RVal" . $col_index]))
						$table_data .= ' checked="checked"';
					
					$table_data .= ' value="' . $group_skills ["RVal"] ["RVal" . $col_index] . '"';
					
					$table_data .= '></td>';
				}
			}
		}
		
		$table_data .= '</tr>';
	}
	
	echo $table_data;
	?>
</table><br>
<table>
	<tr>
		<td>
			<p>
				Sort Display Text Alphabetically: 
				<input type="checkbox" name="sort" value="Y">
			</p>
		</td>
	</tr>
</table>
<?php
if ($rows_empty == "True" || $cols_empty == "True") {
	?><script>createTable();</script><?php
}
?>