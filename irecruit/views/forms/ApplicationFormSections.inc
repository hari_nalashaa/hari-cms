<?php
$i = 0;

if(isset($_REQUEST['btnSubmit']) && $_REQUEST['btnSubmit'] != "")
{
	foreach($_REQUEST['section_title'] as $SectionID=>$SectionTitle) {
	    if($SectionTitle != "") {
	        $set_info      =   array("SectionTitle = :SectionTitle");
	        $where_info    =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID");
	        $params_info   =   array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST['form'], ":SectionID"=>$SectionID, ":SectionTitle"=>$SectionTitle);
	        
	        G::Obj('ApplicationFormSections')->updApplicationFormSections($set_info, $where_info, array($params_info));
	    }
	}

	//Get max sort order
	$new_sort_order        =   G::Obj('ApplicationFormSections')->getMaxSortOrderByFormID($OrgID, $_REQUEST['form']);

	$NewSectionID          =   G::Obj('ApplicationFormSections')->getNewSectionID($OrgID, $_REQUEST['form'], $_REQUEST['new_section_title']);
	
	//Create new section title
	if(isset($_REQUEST['new_section_title']) && $_REQUEST['new_section_title'] != "") {
        $info               =   array(
                                    "OrgID"             =>  $OrgID,
                                    "FormID"            =>  $_REQUEST['form'],
                                    "SectionID"         =>  $NewSectionID,
                                    "SectionTitle"      =>  $_REQUEST['new_section_title'],
                                    "Active"            =>  "N",
                                    "ViewPrintStatus"   =>  "",
                                    "IsDeleted"         =>  "No",
                                    "SortOrder"         =>  ($new_sort_order+1)
                                );
	    G::Obj('ApplicationFormSections')->insApplicationFormSections($info);
	}
	
	$OrgFormID = $_REQUEST['form'];
	echo "<script>";
	echo "location.href='forms.php?form=$OrgFormID&action=formquestions&section=0&form_home=yes&OrgFormID=$OrgFormID&msg=sucupd';";
	echo "</script>";
}


$where_info		=	array("OrgID = :OrgID", "FormID = :FormID", "IsDeleted = :IsDeleted");
$params_info    =	array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST['form'], ":IsDeleted"=>"No");
$results		=	G::Obj('ApplicationFormSections')->getApplicationFormSectionsInfo("*", $where_info, "", "SortOrder", array($params_info));

//Get Form ID's List
$form_ids_list	=	G::Obj('FormQuestions')->getFormIdsByOrgID($OrgID);

echo '<div class="panel-body">'; //Panel Body Start

    echo "<div class='row'>";
        echo "<div class='col-lg-12'>";
            echo "<strong>Note:* </strong>Drag and drop the sub sections in Application section to update the order in User Portal Application page";
        echo "</div>";
    echo "</div>";

    echo '<br>';
    
    echo "<div class='row'>";
        echo "<div class='col-lg-12'>";
            echo '<div id="form_section_titles_status_message">';
            if(isset($_GET['msg']) && $_GET['msg'] == 'sucupd') {
            	echo '<span style="color:blue">Application form updated successfully</span>';
            }
            echo '</div>';
        echo '</div>';
    echo '</div>';
    
    echo '<br><br>';

    echo "<div class='row'>";
    echo "<div class='col-lg-12'>";
    
        echo '<form action="forms.php?form='.$_REQUEST['form'].'&action=formquestions&section=0&form_home=yes" name="frmApplicationFormSections" id="frmApplicationFormSections" method="post">';
        echo '<table class="table table-bordered" id="sort_form_section_titles">';
        echo '<tbody>';
        if(is_array($results['results'])) {
        	foreach($results['results'] as $OAP) {
        		$i ++;
        		$checked = ($OAP ['Active'] == "Y") ? ' checked="checked"' : '';
        		$view_print_checked = ($OAP ['ViewPrintStatus'] == "Y") ? ' checked="checked"' : '';
        		echo '<tr id="'.htmlspecialchars($_REQUEST['form']).'*'.htmlspecialchars($OAP ['SectionID']).'">';
        		echo '<td width="40%">Section Title: <input type="text" name="section_title['.$OAP ['SectionID'].']" size="30" value="'.$OAP ['SectionTitle'].'"></td>';
        		echo '<td>Active: <input type="checkbox" id="section_'.$OAP ['SectionID'].'" name="section_'.htmlspecialchars($OAP ['SectionID']).'" '.htmlspecialchars($checked).' value="'.htmlspecialchars($OAP ['SectionID']).'" onclick=\'changeActiveStatus("'.$_REQUEST['form'].'", "'.$OAP['SectionID'].'", this.checked);\'></td>';
        		if($OAP ['SectionID'] != 14 && $OAP ['SectionID'] != 6) {
        		    echo '<td>View & Print: <input type="checkbox" id="view_print_'.$OAP ['SectionID'].'" name="view_print_'.htmlspecialchars($OAP ['SectionID']).'" '.htmlspecialchars($view_print_checked).' value="'.htmlspecialchars($OAP ['SectionID']).'" onclick=\'changeViewPrintStatus("'.$_REQUEST['form'].'", "'.$OAP['SectionID'].'", this.checked);\'></td>';
        		}
        		echo '</tr>';
        		unset($checked);
        		unset($view_print_checked);
        	}
        }

        echo '</tbody>';
        echo '</table>';
        
        echo '<table class="table table-bordered" id="tbl_add_section" style="display:none;">';
        echo '<tbody>';
        echo '<tr>';
        echo '<td width="40%">Section Title: <input type="text" name="new_section_title" size="30"></td>';
        echo '</tr>';
        echo '</tbody>';
        echo '</table>';
    
        echo '<table class="table table-bordered">';
        echo '<tr>';
        echo '<td>';
        echo '<input type="button" name="btnAddSection" id="btnAddSection" class="btn btn-primary" onclick="showHideAddSection()" value="Add Section"> &nbsp;';
        echo '<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit">';
        echo '</td>';
        echo '</tr>';
        echo '</table>';
        
        echo '<input type="hidden" name="OrgFormID" id="OrgFormID" value="'.htmlspecialchars($_REQUEST['form']).'">';
        
    echo '</form>';
    
    echo '</div>';
    echo '</div>';
    
echo '</div>'; //Panel Body End
?>
<script type="text/javascript" src="js/application-form-sections.js"></script>
