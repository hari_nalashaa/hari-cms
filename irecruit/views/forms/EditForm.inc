<?php
$all_sections_list      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $_REQUEST ['form'], "", "No");
$available_sections     =   array_keys($all_sections_list);
$available_sections     =   G::Obj('ArrayHelper')->removeElement("6", $available_sections);

// Update Custom Question Information
if (isset ( $action ) && $action == 'formquestions') {
	if (isset ( $process ) && $process == 'Y') {
		foreach ( $_REQUEST as $custqkey => $custqval ) {
			if (strstr ( $custqkey, '-questiontypeid' )) {
				$cqtypeinfo = explode ( "-", $custqkey );
				$cquestionid = $cqtypeinfo [0];
				
				if (substr ( $cquestionid, 0, 4 ) == "CUST") {
					if (isset ( $_REQUEST [$cquestionid . 'Question'] ) && $_REQUEST [$cquestionid . 'Question'] != "") {
						
						// Get question type information based on QuestionTypeID
						$que_type_def_val = G::Obj('QuestionTypes')->getQuestionTypeInfo ( $_REQUEST [$custqkey] );
						
						if ($formtable == "OnboardQuestions") {
							// List of parameters to bind the query
							$params = array (
									':Question'        =>  $_REQUEST [$cquestionid . 'Question'],
									':QuestionTypeID'  =>  $_REQUEST [$custqkey],
									':OrgID'           =>  $OrgID,
									':FormID'          =>  $_REQUEST ['form'],
									':QuestionID'      =>  $cquestionid
							);
							// Set update information
							$set_info = array (
									"Question          =   :Question",
									"QuestionTypeID    =   :QuestionTypeID"
							);
							//If question is text area add rows and cols while updating
							if($_REQUEST [$custqkey] != 100 && $_REQUEST [$custqkey] != 120) {
								//Set parameters
								$params[':rows']        =   $que_type_def_val['rows'];
								$params[':cols']        =   $que_type_def_val['cols'];
								$params[':size']        =   $que_type_def_val['size'];
								$params[':maxlength']   =   $que_type_def_val['maxlength'];
								//Set the default parameters
								$set_info[]             =   'rows = :rows';
								$set_info[]             =   'cols = :cols';
								$set_info[]             =   'size = :size';
								$set_info[]             =   'maxlength = :maxlength';
							}
							// Set where condition
							$where = array (
									"OrgID             =   :OrgID",
									"FormID            =   :FormID",
									"QuestionID        =   :QuestionID" 
							);
							//Update Onboard Questions Information
							$res_question = $FormQuestionsObj->updQuestionsInfo ( $formtable, $set_info, $where, array (
									$params 
							) );
						}
						if ($formtable == "FormQuestions") {
						    
							// List of parameters to bind the query
							$params = array (
									':Question'        =>  $_REQUEST [$cquestionid . 'Question'],
									':QuestionTypeID'  =>  $_REQUEST [$custqkey],
									':OrgID'           =>  $OrgID,
									':FormID'          =>  $_REQUEST ['form'],
									':QuestionID'      =>  $cquestionid,
									':SectionID'       =>  $section,
							);
							// Set Update Information
							$set_info = array (
									"Question          =   :Question",
									"QuestionTypeID    =   :QuestionTypeID"
							);
							
							$frm_que_detail_info = $FormQuestionsObj->getQuestionDetails("value", $formtable, $OrgID, $_REQUEST ['form'], $cquestionid);
							
							if($frm_que_detail_info['value'] == "")
							{
							    $purpose_name_value       =   '';
							    if($section == '6' && $_REQUEST [$custqkey] == '8') {
							        $purpose_name_value   =   $cquestionid;

							        $params[':value']        =   $purpose_name_value;
							        $set_info[]              =   "value = :value";
							    }
							}
							
							//If question is text area add rows and cols while updating
							if($_REQUEST [$custqkey] != 100 && $_REQUEST [$custqkey] != 120) {
								//Set parameters
								$params[':rows']        =   $que_type_def_val['rows'];
								$params[':cols']        =   $que_type_def_val['cols'];
								$params[':size']        =   $que_type_def_val['size'];
								$params[':maxlength']   =   $que_type_def_val['maxlength'];
								//Set the default parameters
								$set_info[]             =   'rows = :rows';
								$set_info[]             =   'cols = :cols';
								$set_info[]             =   'size = :size';
								$set_info[]             =   'maxlength = :maxlength';
							}
							
							// Set where condition
							$where = array (
									"OrgID         =   :OrgID",
									"FormID        =   :FormID",
									"QuestionID    =   :QuestionID",
									"SectionID     =   :SectionID"
							);
							// Update Form Questions Information
							$res_question = $FormQuestionsObj->updQuestionsInfo ( $formtable, $set_info, $where, array (
									$params 
							) );
						}
					}
				}
			}
		}
	}
}

if ($questionid) {
	// edit individual entry
	$FormID = $form;
	$QuestionID = $questionid;
	
	include IRECRUIT_VIEWS . 'forms/EditQuestion.inc';
} else {
	// Get Question Types List
    $row_question_types = G::Obj('QuestionTypes')->getQuestionTypesInfo ( "*", array (), "SortOrder", array () );
	
	if (is_array ( $row_question_types ['results'] )) {
		foreach ( $row_question_types ['results'] as $row_question_types ) {
			$row_que_types [] = $row_question_types;
			$ques_types_list [] = $row_question_types ['QuestionTypeID'];
			$question_types_by_id [$row_question_types ['QuestionTypeID']] = $row_question_types ['Description'];
			
			if ($row_question_types ['Editable'] == 'Y') {
				$que_type_id = $row_question_types ['QuestionTypeID'];
				if ($que_type_id != '15' 
					&& $que_type_id != '30' 
					&& $que_type_id != '60'
					//&& $que_type_id != '9'
					&& $que_type_id != '21'
					&& $que_type_id != '25') {
				    
				    if($section != 6 && $que_type_id != 8) {
                        $edit_que_types [] = $row_question_types;
				    }
				    else if($section == 6) {
				        $edit_que_types [] = $row_question_types;
				    }
				}
			}
		}
	}
	
	if ($action == 'formquestions') {
		
		if (isset ( $_REQUEST ['process'] ) && $_REQUEST ['process'] == 'Y') {
			
			foreach ( $_POST as $key => $value ) {
				
				if ($value == 'D') {
					
					// Bind QuestionID
					$params    =   array (':OrgID'=>$OrgID, ':FormID'=>$form, ':QuestionID'=>$key);
					// Set the parameters those are going to update
					$set       =   "Active = '', Required = ''";
					// Update the table information based on bind and set values
					G::Obj('Forms')->updFormTableInfo ( $formtable, $set, array($params) );
				}
				
				if ($value == 'Y') {
					
					$pieces = explode ( "-", $key );
					
					if ($pieces [1] == 'A') {
						
						// Bind Parameters
						$params   =   array(
        								':OrgID'        =>  $OrgID,
        								':FormID'       =>  $form,
        								':QuestionID'   =>  $pieces [0] 
            						  );
						// Set the parameters those are going to update
						$set      =   "Active = 'Y'";
						// Update the table information based on bind and set values
						G::Obj('Forms')->updFormTableInfo ( $formtable, $set, array ($params) );
					}
					else if ($pieces [2] == 'A') {
					    
					    // Bind Parameters
					    $params   =   array(
                                            ':OrgID'        =>  $OrgID,
                                            ':FormID'       =>  $form,
                                            ':QuestionID'   =>  $pieces[0]."-".$pieces [1]
                                        );
					    // Set the parameters those are going to update
					    $set      =   "Active = 'Y'";
					    // Update the table information based on bind and set values
					    G::Obj('Forms')->updFormTableInfo ( $formtable, $set, array ($params) );
					}
					else if ($pieces [1] == 'R') {
						
						// Bind Parameters
						$params   =   array(
        								':OrgID'        =>  $OrgID,
        								':FormID'       =>  $form,
        								':QuestionID'   =>  $pieces [0] 
            						  );
						// Set the parameters those are going to update
						$set      =   "Required = 'Y'";
						// Update the table information based on bind and set values
						G::Obj('Forms')->updFormTableInfo ( $formtable, $set, array ($params) );
					}
				}
			}
			
			echo '<script language="JavaScript" type="text/javascript">';
			echo "alert('Form Questions have been completed!')";
			echo '</script>';
		}
		
		echo '<br>';
		echo '<div class="panel-body">';
		
		//To Display messages
		echo '<div class="row">';
		echo '<div class="col-lg-12 col-md-12 col-sm-12" id="sort_form_ques_msg" style="color:blue">';
		echo '</div>';
		echo '</div>';

		echo '<div class="table-responsive">';
		echo '<form method="post" action="' . $formscript . '">';
		
		echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" id="sort_form_questions" class="table table-striped table-bordered">';
        echo '<thead>';
		echo '<tr>';
		echo '<td width="570"><b>Question</b></td>';
		
		echo '<td>';
		echo '<b>Question Type</b>';
		echo '</td>';
		
		echo '<td align=center width="90"><b>Edit Details</b></td>';
		echo '<td align=center width="60"><b>Active</b>';
		echo '<input type="checkbox" name="active_check_uncheck" onclick=\'check_uncheck(this.checked, "chk_active")\'>';
		echo '</td>';
		
		$act = "on";
		if (($formtable == "FormQuestions") && ($section == "0")) {
			$act = "off";
		}
		
		if ($act == "on") {
			echo '<td align=center width="60"><b>Required</b>';
			echo '<input type="checkbox" name="required_check_uncheck" onclick=\'check_uncheck(this.checked, "chk_required")\'>';
			echo '</td>';
		} // end section
		echo '<td><b>Action</b></td>';
		echo '</tr>';
		echo '</thead>';
		
		echo '<tbody>';
		$results  =   G::Obj('Forms')->getFormTableInfo ( $formtable, $OrgID, $form, $section );
		$cnt      =   $results ['count'];
		$i        =   0;
		$rowcolor =   "#eeeeee";
		
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $row ) {
				
				$i ++;
				$QuestionID     =   $row ['QuestionID'];
				$Question       =   $row ['Question'];
				$Active         =   $row ['Active'];
				$Required       =   $row ['Required'];
				$QuestionTypeID =   $row ['QuestionTypeID'];
				
				if (isset ( $row ['SectionID'] )) {
                    $SectionID  =   $row ['SectionID'];
				}
				
				$QuestionOrder  =   $row ['QuestionOrder'];

				if (isset ( $row ['SageLock'] )) {
                    $SageLock   =   $row ['SageLock'];
				}
				
				if ($Active == "Y") {
                    $ckd1       =   ' checked';
				} else {
                    $ckd1       =   '';
				}
				
				if ($Required == "Y") {
                    $ckd2       =   ' checked';
				} else {
                    $ckd2       =   '';
				}
				
				echo '<tr id="' . $form . '*' . $section . '*' . $QuestionID . '" bgcolor="' . $rowcolor . '">';
				echo '<td' . $span . '>';
				
				if ($QuestionTypeID == 98) {
					$Question = "&nbsp;&nbsp;&nbsp;&nbsp;[<i>Spacer</i>]";
				} // end if
				  
				if (substr ( $QuestionID, 0, 4 ) == "CUST") {
					echo "<input type='text' name='" . $QuestionID . "Question' id='" . $QuestionID . "Question' value=\"".htmlentities($Question)."\" size='40'></td>";
				} else if (($QuestionTypeID == 99) || ($QuestionTypeID == 90)) {
					echo '<b>Instruction</b>: ';
					echo substr ( strip_tags ( $Question ), 0, 40 ) . '...</td>';
				} else {
					echo $Question . '</td>';
				}
				
				$QuestionTypeName = $QuestionID . "-questiontypeid";
				
				echo "<td>";
				
				if (substr ( $QuestionID, 0, 4 ) == "CUST") {
					echo "<select name='$QuestionTypeName' style='width:120px;'>";
					echo "<option value=''>Select</option>";
					
					for($qt = 0; $qt < count ( $edit_que_types ); $qt ++) {
						echo "<option value='" . $edit_que_types [$qt] ['QuestionTypeID'] . "'";
						if ($QuestionTypeID == $edit_que_types [$qt] ['QuestionTypeID'])
							echo ' selected="selected"';
						echo ">" . $edit_que_types [$qt] ['Description'] . "</option>";
					}
					echo "</select>";
				} else {
					echo $question_types_by_id [$QuestionTypeID];
				}
				echo "</td>";
				
				echo '<td align=center>';
				if ($SageLock == "Y") {
					
					echo '<a href="' . IRECRUIT_HOME . 'administration.php?action=onboardquestions&unlock=' . $QuestionID . '" onclick="return confirm(\'Are you sure you want to unlock the following question?\n\n' . $Question . '\n\n\')">';
					echo '<img src="' . IRECRUIT_HOME . 'images/icons/lock_edit.png" border="0" title="Lock Edit">';
					echo '</a>';
				} else {
					if ($QuestionTypeID != 98) {
						echo '<a href="' . $formscript . '?action=' . $action . '&form=' . $form;
						if ($formtable == "FormQuestions") {
							echo '&section=' . $section;
							echo '&formtable='. $formtable;
						}
						echo '&questionid=' . $QuestionID . '&QuestionTypeID='.$QuestionTypeID.'"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
					} else {
						echo '&nbsp;';
					}
				}
				echo '</td>';
				
				echo '<td align=center>';
				echo '<input type="hidden" name="' . $QuestionID . '" value="D">';
			    
                echo '<input type="checkbox" class="chk_active" name="' . $QuestionID . '-A" id="' . $QuestionID . '-A" value="Y"' . $ckd1 . '>';
				
                echo '</td>';
				
				if ($act == "on") {
					
					echo '<td align=center>';
					if (($QuestionTypeID != 99) && ($QuestionTypeID != 98)) {
						echo '<input type="checkbox" name="' . $QuestionID . '-R" id="' . $QuestionID . '-R" value="Y"' . $ckd2 . '  class="chk_required">';
					}
					echo '</td>';
				} // end act on
				
				echo "<td align='center'>";
				
				if (substr ( $QuestionID, 0, 4 ) == "CUST") {
					
					$script_source = pathinfo ( $_SERVER ['SCRIPT_NAME'] );
					
					if ($script_source ['basename'] == "administration.php")
						echo "<a onclick=\"return confirmDeleteFormQuestion('administration', '" . $form . "', '" . htmlspecialchars($_REQUEST ['action']) . "', '" . htmlspecialchars($section) . "', '" . htmlspecialchars($QuestionID) . "', '" . htmlspecialchars($Question) . "');\" href=\"javascript:void(0);\"><img border='0' title='Delete' src='" . IRECRUIT_HOME . "images/icons/cross.png'></a>";
					else
						echo "<a onclick=\"return confirmDeleteFormQuestion('forms', '" . $form . "', '" . htmlspecialchars($_REQUEST ['action']) . "', '" . htmlspecialchars($section) . "', '" . htmlspecialchars($QuestionID) . "', '" . htmlspecialchars($Question) . "');\" href=\"javascript:void(0);\"><img border='0' title='Delete' src='" . IRECRUIT_HOME . "images/icons/cross.png'></a>";
				} else {
					echo "-";
				}
				echo "</td>";
				
				echo '</tr>';
				
				if ($rowcolor == "#eeeeee") {
					$rowcolor = "#ffffff";
				} else {
					$rowcolor = "#eeeeee";
				}
			} // end foreach
		}
		
		echo '</tbody>';
		
		if (in_array ( $section, $available_sections )) {
			echo '<tr>';
			echo '<td valign="middle" align="left" colspan="100%" height="60">';
			echo '<a href="' . $formscript . '?form=' . $form . '&action=formquestions&section=' . $section . '&new=add">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Custom Question</b>';
			echo '</a>';
			echo '</td>';
			echo '</tr>';
		}
		
		echo '</table>';
		
		echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
		echo '<tr>';
		echo '<td valign="middle" align="center" colspan="100%" height="60">';
		
		echo '<div style="text-align:center;color:red" id="synchronize_msg_cloudpayroll"></div>';
		
		echo '<input type="hidden" name="action" value="formquestions">';
		if ($formtable == "FormQuestions") {
	        echo '<input type="hidden" name="section" value="' . $section . '">';
	        echo '<input type="hidden" name="formtable" value="' . $formtable . '">';
        }
		echo '<input type="hidden" name="form" value="' . $form . '">';
		echo '<input type="hidden" name="process" value="Y">';
		
		echo '<input type="submit" value="Change Status" class="btn btn-primary">';
		
		echo '</td></tr>';
		
		echo '</table>';
		echo '</form>';
		echo '</div>';
		echo '</div>';
	} // end form questions
	
	if ($action == 'textblocks') {
		include IRECRUIT_VIEWS . 'forms/TextBlocks.inc';
	} // end text blocks
} // end else questionid
?>
