<?php
require_once IRECRUIT_DIR . 'forms/ProcessEditQuestion.inc';

// Display Change form
if (isset($_REQUEST['questionid']) || isset($_REQUEST['process'])) {
	?>
<div class="panel-body">
	<form method="post" action="<?php echo $formscript;?>"
		enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
		<div class="table-responsive">
			<table
				class="formtable table table-striped table-bordered table-hover"
				border="0" cellspacing="3" cellpadding="5" width="100%">
				<tr>
					<td><b>Edit Question</b></td>
				<tr>
				<?php
				if($QuestionID != "cloud_PayCode"
				    && $QuestionID != "cloud_EmployeeType"
				    && $QuestionID != "cloud_Position"
				    && $QuestionID != "cloud_Status"
				    && $QuestionID != "cloud_WorkSite"
				    && $QuestionID != "cloud_Division"
				    && $QuestionID != "cloud_Department"
				    && $QuestionID != "cloud_PayGroup"
				    && $QuestionID != "cloud_WorkShift"
                    && $QuestionID != "matrixcare_OfficeID") {

                        if($_REQUEST['QuestionTypeID'] == '3') {
                            ?>
                            <tr>
                                <td>
                                    <strong>Note*:</strong> If you want to prefill the data with the existing list of options available, please select any option in below pull down.
                                    <br>
                                    Prefill with existing data:
                                    <select name="ddlPrefilledQueData" id="ddlPrefilledQueData">
                                        <option value="">Select</option>
                                        <option value="States">States</option>
                                        <option value="Days">Days</option>
                                        <option value="Months">Months</option>
                                        <option value="SocialMedia">SocialMedia</option>
                                    </select>
                                    <br><br>
                                </td>
                            <tr>
                            <?php
                        }
                
                }
                ?>
				
				<tr>
					<td>
					<?php include IRECRUIT_VIEWS . 'forms/AdminQuestions.inc';?>
					</td>
				</tr>

				<tr>
					<td align="center" height="60" valign="middle">
						<input type="hidden" name="questionid" value="<?php echo htmlspecialchars($QuestionID); ?>" /> 
						<input type="hidden" name="form" value="<?php echo htmlspecialchars($FormID); ?>" /> 
						<input type="hidden" name="section" value="<?php echo htmlspecialchars($section); ?>" /> 
						<input type="hidden" name="formtable" value="<?php echo htmlspecialchars($formtable); ?>" /> 
						<input type="hidden" name="action" value="<?php echo htmlspecialchars($action); ?>" />
						<input type="hidden" name="QuestionTypeID" id="QuestionTypeID" value="<?php echo htmlspecialchars($_REQUEST['QuestionTypeID']); ?>" />
						<?php
						if($QuestionID != "cloud_PayCode"
                            && $QuestionID != "cloud_EmployeeType"
                            && $QuestionID != "cloud_Position"
                            && $QuestionID != "cloud_Status"
                            && $QuestionID != "cloud_WorkSite"
                            && $QuestionID != "cloud_Division"
                            && $QuestionID != "cloud_Department"
                            && $QuestionID != "cloud_PayGroup"
                            && $QuestionID != "cloud_WorkShift"
                            && $QuestionID != "matrixcare_OfficeID") {

                            echo '<input type="submit" name="process" value="Update" class="btn btn-primary" />&nbsp;';
                            
                            if ($process) {
                                echo '<input type="submit" name="finish" value="Finish" class="btn btn-primary"/>';
                            } else {
                                echo '<input type="submit" name="finish" value="Cancel" class="btn btn-primary"/>';
                            }
                        }
						?>
					</td>
				</tr>
				
				
				</form>
	

<?php
	// display preview
	if ($Active == 'Y') {
		
		echo '<tr><td colspan="100%"><hr size="1"/>';
		echo '<p><b>Preview</b></p>';
		$colwidth = "220";
		
		if($QuestionID == "matrixcare_OfficeID") {
            $office_ids_info = $MatrixCareObj->getMatrixCareOfficeIds($OrgID, "");
            $branches_info = json_decode($office_ids_info['BranchesInfo'], true);
            
            if(is_array($branches_info)) {
                echo '<select class="form-control width-auto-inline">';
                foreach ($branches_info as $branch_id=>$branch_name)
                {
                    echo '<option value="'.$branch_id.'">'.$branch_name.'</option>';
                }
                echo '</select>';
            }
                        
        }
		else {
            $SectionID = "";
            echo include COMMON_DIR . 'application/DisplayQuestions.inc';
		}
		
		echo '</td></tr>';
	}
	// this php block prevents the questions from listing when editing details
}
?>
</table>
	</div>
</div>
