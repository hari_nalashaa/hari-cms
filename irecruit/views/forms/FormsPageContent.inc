<?php
if ($permit ['Forms'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered">';
echo '<form method="post" name="frmSectionsList" id="frmSectionsList" action="' . $_SERVER ['SCRIPT_NAME'] . '">';
echo '<tr><td valign="top" width="80%"><b>';
echo $title;
echo '</b></td><td valign="top" align="right">';

if ($questionid) {
    echo '<a href="forms.php?action=formquestions&form=' . $form . '&section=' . $section  . '&formtable=' . $formtable . '">';
	echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b>';
	echo '<img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
} else {
	
	if (($form) && ($action != "copyform")) {
		
		$where_info     =       array("OrgID = :OrgID", "FormID = :FormID", "IsDeleted = :IsDeleted", "Active = 'Y'");
		$params_info    =       array(":OrgID"=>$OrgID, ":FormID"=>$form, ":IsDeleted"=>"No");
		$app_form_sections_info  =       G::Obj('ApplicationFormSections')->getApplicationFormSectionsInfo("SectionID, SectionTitle", $where_info, "", "SortOrder", array($params_info));

		?>
		<select name="actionsection" id="actionsection" onChange="handleSectionsChange(this, this.value, '<?php echo $form;?>')" class="form-control">
		<?php
		echo '<option value="x:x">Available Forms</option>';

		echo '<option value="formquestions:AFS"';
		if($_REQUEST['form_home'] == 'yes') echo ' selected="selected"';
		echo '>Application Form Sections</option>';
		
		foreach ($app_form_sections_info['results'] as $section_info) {
		    echo '<option value="formquestions:' . $section_info['SectionID'] . '"';
		    if (($action == "formquestions") && ($section == $section_info['SectionID'])) {
		        echo ' selected';
		    }
		    echo '>' . $section_info['SectionTitle'] . '</option>' . "\n";
		}
		
		//Get all text block sections
		$results = $FormSectionsObj->getAllTextBlockSections();
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $row) {
				$i ++;
				echo '<option value="textblocks:' . $row ['TextBlockID'] . '"';
				if (($action == "textblocks") && ($section == $row ['TextBlockID'])) {
					echo ' selected';
				}
				echo '>' . $row ['Section'] . '</option>' . "\n";
			}	
		}
		
		echo '</select>';
		echo '<input type="hidden" name="form" value="' . $form . '">';
	}
} // end else
echo '</td></tr>';
echo '</form>';
echo '</table>';
?>
</div>
<?php
if ((! $action) 
    || ($action == 'copyform') 
    || ($action == 'resetform') 
    || ($action == 'deleteform')) {
	include IRECRUIT_VIEWS . 'forms/FormCreation.inc';
} else {
	$i = 1;
	if(isset($_REQUEST['form_home']) && $_REQUEST['form_home'] == "yes") {
	    include IRECRUIT_VIEWS . 'forms/ApplicationFormSections.inc';
	}
	else {
	    include IRECRUIT_VIEWS . 'forms/EditForm.inc';
	}
}
?>
<script src="<?php echo IRECRUIT_HOME;?>js/forms-page-content.js" type="text/javascript"></script>