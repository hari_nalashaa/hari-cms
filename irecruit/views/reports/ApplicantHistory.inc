<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="page-wrapper">

    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
    
    <div class="row">
		<div class="col-lg-12">
		  <form name="frmApplicantHistoryParams" id="frmApplicantHistoryParams" method="post">
                <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
                <input type="hidden" name="IndexStart" id="IndexStart" value="0">
                <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $user_preferences['ReportsSearchResultsLimit'];?>">
                <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
				<?php
				if($feature['MultiOrg'] == "Y") {
				    $where_info     =   array("OrgID = :OrgID");
				    $params_info    =   array(":OrgID"=>$OrgID);
				    $columns        =   "OrgID, MultiOrgID, OrganizationName";
				    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
				    $orgs_list      =   $orgs_results['results'];
				    
				    echo 'Organizations List:';
				    echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
				    echo '<option value="">All</option>';
				    for($ol = 0; $ol < count($orgs_list); $ol++) {
				        if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
				            $selected = ' selected="selected"';
				        }
				        echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
				        unset($selected);
				    }
				    echo '</select>';
				}
				else {
				    echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
				}
				?>
			    Status:
			    <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline" onchange="getRequisitionsByStatus(this.value)">
					<option value="A">All</option>
					<option value="Y">Active</option>
					<option value="N">Inactive</option>
				</select>
    			<br><br>
    			Applicant Status:
    			<?php
    			echo '<select name="ProcessOrder" id="ProcessOrder" class="form-control width-auto-inline">';
    			echo '<option value="">Select a Status</option>';
    			//Set Condition For Applicant ProcessFlow
                $where      =   array("OrgID = :OrgID", "ProcessOrder != ''", "Active = 'Y'");
                //Bind the parameters
                $params     =   array(':OrgID'=>$OrgID);
                //Get Applicant Process Flow Information
                $results    =   G::Obj('Applicants')->getApplicantProcessFlowInfo(array('ProcessOrder', 'Description'), $where, 'ProcessOrder', array($params));
    			
    			if(is_array($results['results'])) {
    			    foreach ($results['results'] as $row) {
    			        
    			        if ($ProcessOrder == $row ['ProcessOrder']) {
    			            $selected = ' selected';
    			        } else {
    			            $selected = '';
    			        }
    			        
    			        echo '<option value="' . $row ['ProcessOrder'] . '"' . $selected . '>' . $row ['Description'];
    			    }
    			}
    			echo '</select>&nbsp;';
    			
    			$results =	G::Obj('Applicants')->getApplicantDispositionCodes($OrgID,'Y');
    			
    			$disposition_rows = $results['count'];
    			
    			if ($disposition_rows > 0) {
    			    echo 'Disposition Code: ';
    			    echo '<select name="DispositionCode" id="DispositionCode" class="form-control width-auto-inline" style="max-width:350px;">';
    			    echo '<option value="">Select a Code</option>';
    			    
    			    if(is_array($results['results'])) {
    			        foreach ($results['results'] as $row) {
    			            if ($DispositionCode == $row ['Code']) {
    			                $selected = ' selected';
    			            } else {
    			                $selected = '';
    			            }
    			            echo '<option value="' . $row ['Code'] . '"' . $selected . '>' . $row ['Description'];
    			        }
    			    }
    			    
    			    echo '</select>';
    			} // end if disposition set up
    			?>
    			<br><br>
                Requisitions: 
                <select name="ddlRequisitionsList" id="ddlRequisitionsList" class="form-control width-auto-inline" style="max-width:300px" onchange="getApplicantsByRequisition(this.value)">
                  <option value="">Select</option>
                  <?php
                      if(is_array($requisitions_results)) {
                            for($i = 0; $i < $requisitions_results_count; $i++) {
                            	?><option value="<?php echo $requisitions_results[$i]['RequestID'];?>"><?php echo $requisitions_results[$i]['Title'] . " - ". $requisitions_results[$i]['RequisitionID'] . "/" . $requisitions_results[$i]['JobID'];?></option><?php
                            }
                      }
                  ?>
                </select>
                <br><br>
                <input type="hidden" name="ddlApplicantsList" id="ddlApplicantsList">
		Applicant Name: 
                <input type="text" name="search_word" id="search_word" class="form-control width-auto-inline">
                From: 
    			<input type="text" name="applicants_from_date" id="applicants_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
    			To: 
    			<input type="text" name="applicants_to_date" id="applicants_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
                
                &nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-primary" name="btnSetApplicantHistory" id="btnSetApplicantHistory" onclick="setApplicantHistory()" value="Refine">
                
                <input type="button" class="btn btn-primary" name="btnExportApplicantHistory" id="btnExportApplicantHistory" onclick="exportApplicantHistory()" value="Export">
                
                <span id="applicants_list_status"></span><br><br>
		  
		  </form>
		  
		  <form name="frmSortOptions" id="frmSortOptions" method="post">
		         <input type="hidden" name="ddlToSort" id="ddlToSort" value="entry_date">
    	         <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
		   </form>
	   </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<form name="frmRequisitionsSearchResults" id="frmRequisitionsSearchResults" method="post">
					<div class="table-responsive">
    					<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover" id="data-applicant-history">
    						<thead>
                    			<tr>
                        			<td><a href="javascript:void(0)" onclick="sortApplicants('applicant_name')"><strong>Applicant Name</strong></a></td>
                                    <td><a href="javascript:void(0)" onclick="sortApplicants('application_id')"><strong>Application ID</strong></a></td>
                                    <td><strong>Job Title</strong></td>
                                    <td><a href="javascript:void(0)" onclick="sortApplicants('entry_date')"><strong>Application Date</strong></a></td>
                                    <td><a href="javascript:void(0)" onclick="sortApplicants('status')"><strong>Status</strong></a></td>
                                    <td><a href="javascript:void(0)" onclick="sortApplicants('disposition_code')"><strong>Disposition Code</strong></a></td>
                                    <td><a href="javascript:void(0)" onclick="sortApplicants('last_updated')"><strong>Last Updated</strong></a></td>
                                    <td><strong>Comments/Notes</strong></td>
                                    <td><a href="javascript:void(0)" onclick="sortApplicants('updated_by')"><strong>Updated By</strong></a></td>
                                    <?php
                                    if($OrgID == 'I20121215') { //United States Conference of Catholic Bishops
                                        ?>
                                    	<td><strong>EEO Code</strong></td>        
                                    	<td><strong>AA Ethnicity</strong></td>
                                        <td><strong>AA Veteran</strong></td>
                                        <td><strong>AA Veteran Status</strong></td>
                                        <td><strong>Sage HireDate</strong></td>
                                        <td><strong>Department Office Secretariat</strong></td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
    
                            <tbody>
                    			<tr> 
                        			<td colspan="9" align="left">Please select any applicant to see the history information.</td>
                                </tr>
                            </tbody>
    					</table>
				</div>
			</form>
    	</div>
	</div>
    <div class="row">
        <div class="col-lg-12" id="applicants_history_pagination"></div>
    </div>
</div>

<script>
function sortApplicants(to_sort) {
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var ApplicationID  		=   document.getElementById('ddlApplicantsList').value;
	var RecordsLimit   		=   document.getElementById('RecordsLimit').value;
	var FromDate       		=   document.frmApplicantHistoryParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantHistoryParams.applicants_to_date.value;
	var IndexStart     		=   document.frmApplicantHistoryParams.IndexStart.value = 0;
	var keyword        		=   document.frmApplicantHistoryParams.search_word.value;
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;
	var ProcessOrder		=	document.frmApplicantHistoryParams.ProcessOrder.value;
	var DispositionCode		=	document.frmApplicantHistoryParams.DispositionCode.value;

    document.frmSortOptions.ddlToSort.value = to_sort;
	
	var sort_type  =   document.frmSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	document.frmApplicantHistoryParams.CurrentPage.value = 1;	
			
	var REQUEST_URL = irecruit_home + "reports/getApplicantHistoryByApplicantID.php?FilterOrganization="+FilterOrganization+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&IndexStart="+IndexStart+"&RecordsLimit="+RecordsLimit+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&ProcessOrder="+ProcessOrder+"&DispositionCode="+DispositionCode;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-applicant-history").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-applicant-history").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setApplicantsHistoryList(data);
    	}
	});
}
                  
function getRequisitionsByStatus(RequisitionStatus) {
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;
	
    $('#ddlRequisitionsList').children('option:not(:first)').remove();

    var option_text		=	'';
    var option_value	=	'';
    var options_list    =   '';
    
	$.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?FilterOrganization="+FilterOrganization+"&RequisitionStatus="+RequisitionStatus,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#applicants_list_status").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		+= " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#applicants_list_status").html("");
			$("#ddlRequisitionsList").append(options_list);
    	}
	});
}

function getApplicantsByRequisition(RequestID) {
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;
	
	$('#ddlApplicantsList').children('option:not(:first)').remove();
	
	var REQUEST_URL	= irecruit_home + "applicants/getApplicantsByRequestID.php?FilterOrganization="+FilterOrganization+"&RequestID="+RequestID;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			//Nothing to update
			$("#applicants_list_status").html("Please wait.. Applicants list is updating.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			$("#applicants_list_status").html("");
			var data_length = data['applicants_list'].length;
			var options_list = "";				
			
			if(data_length > 0) {
				for (var i = 0; i < data_length; i++) {
					
					var ApplicationID 		=	data['applicants_list'][i].ApplicationID;
					var ApplicantSortName 	=	data['applicants_list'][i].ApplicantSortName;

					options_list += "<option value='"+ApplicationID+"'>"+ApplicationID + " - " + ApplicantSortName+"</option>";
				}
				
			}
			
			$("#ddlApplicantsList").append(options_list);
    	}
	});
}

function getRecordsByPage(IndexStart) {
	document.frmApplicantHistoryParams.IndexStart.value = IndexStart;
	
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var ApplicationID  		=   document.getElementById('ddlApplicantsList').value;
	var FromDate       		=   document.frmApplicantHistoryParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantHistoryParams.applicants_to_date.value;
	var keyword        		=   document.frmApplicantHistoryParams.search_word.value;

	var to_sort        		=   document.frmSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmSortOptions.ddlSortType.value;
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;
	var ProcessOrder		=	document.frmApplicantHistoryParams.ProcessOrder.value;
	var DispositionCode		=	document.frmApplicantHistoryParams.DispositionCode.value;

	var REQUEST_URL			=	irecruit_home + "reports/getApplicantHistoryByApplicantID.php?FilterOrganization="+FilterOrganization+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&IndexStart="+IndexStart+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&ProcessOrder="+ProcessOrder+"&DispositionCode="+DispositionCode;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-applicant-history").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-applicant-history").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setApplicantsHistoryList(data);
    	}
	});
}

function getApplicantsByPageNumber(page_number) {
	
	var RecordsLimit = document.frmApplicantHistoryParams.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}


function setApplicantsHistoryList(data) {
	var RecordsLimit		=	document.frmApplicantHistoryParams.RecordsLimit.value;
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;
	
	var app_prev         	=	data.previous;
	var app_next         	=	data.next;
	var total_pages      	=	data.total_pages;
	var current_page     	=	data.current_page;
	var applicants_count 	=	data.applicants_count;
	
	$("#data-applicant-history").find("tr:gt(0)").remove();
	$("#applicants_history_pagination").html("");
	
	var data_length			=	data['applicants_list'].length;
    var applicants_list		=	data['applicants_list'];
    var app_results			=	"";
    var app_his_pagination	=	"";

	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {

			var ApplicantName			=   applicants_list[i].ApplicantName;
			var ApplicationID         	=   applicants_list[i].ApplicationID;
			var JobTitle              	=   applicants_list[i].JobTitle;
			var EntryDate             	=   applicants_list[i].EntryDate;
			var ProcessOrder          	=   applicants_list[i].ProcessOrder;
		    var DispositionCode       	=   applicants_list[i].DispositionCode;
			var StatusEffectiveDate   	=   applicants_list[i].StatusEffectiveDate;
			var UserID                	=   applicants_list[i].UserID;
			var Comments              	=   applicants_list[i].Comments;

			var EEOCode					=	applicants_list[i].EEOCode;
			var AAEthnicity				=	applicants_list[i].AAEthnicity;
            var AAVeteran				=	applicants_list[i].AAVeteran;
            var AAVeteranStatus			=	applicants_list[i].AAVeteranStatus;
            var SageHireDate			=	applicants_list[i].SageHireDate;
            var DOS						=	applicants_list[i].DOS;
            	
			if(ApplicantName == null) ApplicantName = '';
			if(ApplicationID == null) ApplicationID = '';
			if(JobTitle == null) JobTitle = '';
			if(EntryDate == null) EntryDate = '';
			if(ProcessOrder == null) ProcessOrder = '';
			if(DispositionCode == null) DispositionCode = '';
			if(StatusEffectiveDate == null) StatusEffectiveDate = '';
			if(UserID == null) UserID = '';
			if(Comments == null) Comments = '';

			if(EEOCode == null) EEOCode = '';
			if(AAEthnicity == null) AAEthnicity = '';
			if(AAVeteran == null) AAVeteran = '';
			if(AAVeteranStatus == null) AAVeteranStatus = '';
			if(SageHireDate == null) SageHireDate = '';
			if(DOS == null) DOS = '';
			
			app_results += "<tr>";

			app_results += "<td valign='top'>"+ApplicantName+"</td>";
			app_results += "<td valign='top'>"+ApplicationID+"</td>";
			app_results += "<td valign='top'>"+JobTitle+"</td>";
			app_results += "<td valign='top'>"+EntryDate+"</td>";
			app_results += "<td valign='top'>"+ProcessOrder+"</td>";
			app_results += "<td valign='top'>"+DispositionCode+"</td>";
			app_results += "<td valign='top'>"+StatusEffectiveDate+"</td>";
			app_results += "<td valign='top'>"+Comments+"</td>";
			app_results += "<td valign='top'>"+UserID+"</td>";

			if(applicants_list[i].OrgID == "I20121215") { //United States Conference of Catholic Bishops
				app_results += "<td valign='top'>"+EEOCode+"</td>";
				app_results += "<td valign='top'>"+AAEthnicity+"</td>";
				app_results += "<td valign='top'>"+AAVeteran+"</td>";
				app_results += "<td valign='top'>"+AAVeteranStatus+"</td>";
				app_results += "<td valign='top'>"+SageHireDate+"</td>";
				app_results += "<td valign='top'>"+DOS+"</td>";
			}
			
			app_results += "</tr>";
			
		}

		if(parseInt(applicants_count) > parseInt(RecordsLimit)) {
			
			app_his_pagination += "<div class='row'>";
			
			app_his_pagination += "<div class='col-lg-6'>";
			app_his_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			app_his_pagination += "</div>";

			app_his_pagination += "<div class='col-lg-6' style='text-align:right'>";
			app_his_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			app_his_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getApplicantsByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			app_his_pagination += "</div>";
			
			app_his_pagination += "</div>";
			
			app_his_pagination += "<div class='row'>";
			
			app_his_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			app_his_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			app_his_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			app_his_pagination += "</div>";
			
			app_his_pagination += "</div>";

		}	

		$("#applicants_history_pagination").html(app_his_pagination);
		
	}
	else {

		app_results += "<tr>";
		app_results += "<td colspan='9' align='center'>No records found</td>";
		app_results += "</tr>";

	}

	$("#data-applicant-history").append(app_results);
}

function setApplicantHistory() {
	
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var ApplicationID  		=   document.getElementById('ddlApplicantsList').value;
	var RecordsLimit   		=   document.getElementById('RecordsLimit').value;
	var FromDate       		=   document.frmApplicantHistoryParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantHistoryParams.applicants_to_date.value;
	var IndexStart     		=   document.frmApplicantHistoryParams.IndexStart.value = 0;
	var keyword        		=   document.frmApplicantHistoryParams.search_word.value;
	var to_sort        		=   document.frmSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmSortOptions.ddlSortType.value;
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;
	var ProcessOrder		=	document.frmApplicantHistoryParams.ProcessOrder.value;
	var DispositionCode		=	document.frmApplicantHistoryParams.DispositionCode.value;

	document.frmApplicantHistoryParams.CurrentPage.value = 1;	
			
	var REQUEST_URL			=	irecruit_home + "reports/getApplicantHistoryByApplicantID.php?FilterOrganization="+FilterOrganization+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&IndexStart="+IndexStart+"&RecordsLimit="+RecordsLimit+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&ProcessOrder="+ProcessOrder+"&DispositionCode="+DispositionCode;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#data-applicant-history").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			$("#data-applicant-history").append(requisition_details_loading_status);
		},
		success: function(data) {
			setApplicantsHistoryList(data);
    	}
	});
}                            

function exportApplicantHistory() {
	var RequestID          	=   document.getElementById('ddlRequisitionsList').value;
	var ApplicationID      	=   document.getElementById('ddlApplicantsList').value;
	var RecordsLimit       	=   document.getElementById('RecordsLimit').value;
	var IndexStart         	=   document.frmApplicantHistoryParams.IndexStart.value = 0;
	var FromDate           	=   document.frmApplicantHistoryParams.applicants_from_date.value;
	var ToDate             	=   document.frmApplicantHistoryParams.applicants_to_date.value;
	var keyword            	=   document.frmApplicantHistoryParams.search_word.value;
	var to_sort            	=   document.frmSortOptions.ddlToSort.value;
	var sort_type          	=   document.frmSortOptions.ddlSortType.value;
	var FilterOrganization	=	document.frmApplicantHistoryParams.ddlOrganizationsList.value;	
	var ProcessOrder		=	document.frmApplicantHistoryParams.ProcessOrder.value;
	var DispositionCode		=	document.frmApplicantHistoryParams.DispositionCode.value;

	document.frmApplicantHistoryParams.CurrentPage.value = 1;
			
	var REQUEST_URL			=	irecruit_home + "reports/getApplicantHistoryByApplicantID.php?FilterOrganization="+FilterOrganization+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&IndexStart="+IndexStart+"&RecordsLimit="+RecordsLimit+"&Export=YES&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&ProcessOrder="+ProcessOrder+"&DispositionCode="+DispositionCode;

	document.frmApplicantHistoryParams.action = REQUEST_URL;
	document.frmApplicantHistoryParams.submit();
}

$(document).ready(function() {
	setApplicantHistory();
});
</script>
