<?php
echo '<div id="page-wrapper">';

include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';

echo '<div class="row">';
echo '<div class="col-lg-12">';
echo '<div class="panel panel-default">';
echo '<div class="panel-body">';

echo '<div class="row">';
echo '<div class="col-lg-12">';

echo '<form method="GET" name="frmApplicantsByDistance" id="frmApplicantsByDistance">';

if($feature['MultiOrg'] == "Y") {
    $where_info     =   array("OrgID = :OrgID");
    $params_info    =   array(":OrgID"=>$OrgID);
    $columns        =   "OrgID, MultiOrgID, OrganizationName";
    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
    $orgs_list      =   $orgs_results['results'];

    echo 'Organizations List:'; 
    echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
    echo '<option value="">All</option>';
	for($ol = 0; $ol < count($orgs_list); $ol++) {
	    if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
	        $selected = ' selected="selected"';
	    }
	    echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
	    unset($selected);
	}
    echo '</select>';
}
else {
    echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
}

echo '<br><br>';
echo 'From: ';
echo ' <input type="text" name="from_date" id="from_date" class="form-control width-auto-inline" value="'.$FromDate.'">';
echo '&nbsp;&nbsp;To: ';
echo ' <input type="text" name="to_date" id="to_date" class="form-control width-auto-inline" value="'.$ToDate.'">';
echo '<br><br>';
echo 'Zip Code: ';
echo '<input type="text" name="zip_code" id="zip_code" class="form-control width-auto-inline" value="'.$zip_code.'">';
echo '&nbsp;&nbsp;Distance in Miles: ';
echo '<input type="text" name="distance" id="distance" class="form-control width-auto-inline" value="'.$distance.'" size="5">';
echo '&nbsp;';
echo '<input type="hidden" name="export_apps" id="export_apps" class="form-control width-auto-inline" value="No">';
echo '&nbsp;<input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" class="btn btn-primary">';

if ($zip_code) {
    if ($distance) {
        if(count($app_results) > 0) {
            echo '&nbsp;&nbsp;';
            echo '<input type="button" class="btn btn-primary" name="btnExportApplicants" id="btnExportApplicants" onclick="exportApplicants()" value="Export">';
        }
    }        
}

echo '</form>';
echo '</div>';
echo '</div>';

echo '<div class="row">';
echo '<div class="col-lg-12">';
echo '<br><br>';
echo '</div>';
echo '</div>';

echo '<div class="row">';
echo '<div class="col-lg-12">';

if ($zip_code) {
    
    if ($distance) {
        
        if(count($app_results) > 0) {

            echo '<div class="table-responsive">';
            echo '<table class="table table-bordered" id="data-applicants-by-distance">';
            
            echo '<thead>';
            echo '<tr>';
            echo '<td align="left"><b>Requisition Title</b></td>';
            echo '<td align="left"><b>RequisitionID / JobID</b></td>';
            echo '<td align="left"><b>Application ID</b></td>';
            echo '<td align="left"><b>Name</b></td>';
            echo '<td align="left"><b>Status</b></td>';
            echo '<td align="left"><b>Application Date</b></td>';
            echo '<td align="left"><b>Address</b></td>';
            echo '<td align="right"><b>Distance</b></td>';
            echo '</tr>';
            echo '</thead>';
            
            echo '<tbody>';

            foreach($app_results as $CRD) {
            
                $req_details        =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionID, JobID", $OrgID, $CRD['RequestID']);
            
                $ApplicationID      =   $CRD['ApplicationID'];
                $RequestID          =   $CRD['RequestID'];
            
                $QuestionIDs        =   array('first', 'last', 'email', 'cellphone', 'city', 'state', 'province', 'zip', 'country', 'county');
                $app_data           =   G::Obj('ApplicantsData')->getApplicantData($OrgID, $ApplicationID, $QuestionIDs);
                $phone_ans          =   ($app_data['cellphone'] != "") ? json_decode($app_data['cellphone'], true) : "";
                $ReqMultiOrgID      =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
            
                $RequisitionID      =   $req_details['RequisitionID'];
                $JobID              =   $req_details['JobID'];
                $Date               =   $CRD['EntryDate'];
                $Status             =   G::Obj('ApplicantDetails')->getProcessOrderDescription($OrgID, $CRD['ProcessOrder']);
                $Name               =   $app_data['first'] . ' ' . $app_data['last'];
                $Email              =   $app_data['email'];
                $Phone              =   G::Obj('Address')->formatPhone($OrgID, $CRD['country'], $phone_ans[0], $phone_ans[1], $phone_ans[2], '');
            
                $city               =   $app_data['city'];
                $state              =   $app_data['state'];
                $province           =   $app_data['province'];
            
                $zipcode            =   $app_data['zip'];
                $country            =   $app_data['country'];
                $county             =   $app_data['county'];
            
                $locaddr            =   G::Obj('Address')->formatAddress($OrgID, $country, $city, $state, $province, $zipcode, $county);
            
                $app_distance       =   $CRD['Distance'];
                $DispositionCode    =   G::Obj('ApplicantDetails')->getDispositionCodeDescription($OrgID, $CRD['DispositionCode']);
                $EffDate            =   $CRD['StatusEffectiveDate'];
                $RequisitionTitle   =   G::Obj('RequisitionDetails')->getJobTitle($OrgID, $ReqMultiOrgID, $CRD['RequestID']);

                echo '<tr>';
                echo '<td align="left">'.$RequisitionTitle.'</td>';
                echo '<td align="left">'.$RequisitionID . "/" . $JobID . '</td>';
                echo '<td valign="top">';
                echo '<a href="' . IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '">' . $ApplicationID . '</a></td>';
                echo '<td valign="top">';
                echo $Name;
                echo '<br>';
                echo $Email;
                echo '<br>'.$Phone.'</td>';
                echo '<td valign="top" align="left">'.$Status;
            
                if ($DispositionCode) {
                    echo '<br>Disposition: ' . $DispositionCode;
                }
                if ($EffDate) {
                    echo '<br>Effective: ' . $EffDate;
                }
            
                $Date = preg_replace("/ /", "<br>", $Date, 1);
                echo '</td>';
            
                echo '<td valign="top" align="left">'.$Date.'</td>';
                echo '<td valign="top" align="left">'.$locaddr.'</td>';
                echo '<td valign="top" align="right">'.sprintf("%01.1f", str_replace(",", "", $app_distance)).'</td>';
            
                echo '</tr>';

            } // end foreach
            
            echo '<tbody>';
            echo '</table>';
            echo '</div>';
        }
        else {
            echo '<div class="table-responsive">';
            echo '<table class="table table-bordered" id="data-applicants-by-distance">';
            echo '<thead>';
            echo '<tr>';
            echo '<td align="left"><b>Requisition Title</b></td>';
            echo '<td align="left"><b>RequisitionID / JobID</b></td>';
            echo '<td align="left"><b>Application ID</b></td>';
            echo '<td align="left"><b>Name</b></td>';
            echo '<td align="left"><b>Status</b></td>';
            echo '<td align="left"><b>Application Date</b></td>';
            echo '<td align="left"><b>Address</b></td>';
            echo '<td align="right"><b>Distance</b></td>';
            echo '</tr>';
            echo '</thead>';
        
            echo '<tbody>';
            echo '<tr>';
            echo '<td colspan="8">No Applications Found.</td>';
            echo '</tr>';
            echo '</tbody>';
            echo '</table>';
            echo '</div>';
        }
        
    } // end if dist
}

echo '</div>';
echo '</div>';

echo '</div>';
echo '</div>';
echo '</div>';
echo '</div>';
echo '</div>';

if(count($app_results) > 0) {
    ?>
    <script>
    $(document).ready(function() {
        $('#data-applicants-by-distance').DataTable({
            responsive: true,
            aLengthMenu: [
              [50, 100, -1],
              [50, 100, "All"]
            ]
        });
    });

    function exportApplicants() {
    	var export_apps = document.getElementById('export_apps').value = 'Yes';
        
    	if(export_apps == "Yes") {
    	    document.forms['frmApplicantsByDistance'].submit();
    	}
    }
    </script>
    <?php
}
?>