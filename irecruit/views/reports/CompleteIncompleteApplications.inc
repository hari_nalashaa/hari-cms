<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="page-wrapper">

    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
    
    <div class="row">
		
    		<div class="col-lg-12">  
                <form name="frmUserApplicationsFilter" id="frmUserApplicationsFilter" method="post">
                    <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
                    <input type="hidden" name="IndexStart" id="IndexStart" value="0">
                    <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $Limit;?>">
                    <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
					<?php
					if($feature['MultiOrg'] == "Y") {
					    $where_info     =   array("OrgID = :OrgID");
					    $params_info    =   array(":OrgID"=>$OrgID);
					    $columns        =   "OrgID, MultiOrgID, OrganizationName";
					    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
					    $orgs_list      =   $orgs_results['results'];
					    
					    echo 'Organizations List:';
					    echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
					    echo '<option value="">All</option>';
					    for($ol = 0; $ol < count($orgs_list); $ol++) {
					        if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
					            $selected = ' selected="selected"';
					        }
					        echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
					        unset($selected);
					    }
					    echo '</select>';
					}
					else {
					    echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
					}
					?>
					<br><br>
                    From: 
                    <input type="text" name="applicants_from_date" id="applicants_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-6 months", strtotime(date('Y-m-d')))); ?>">
                    To: 
                    <input type="text" name="applicants_to_date" id="applicants_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
                    &nbsp;
                    Application Status: <select name="application_status" id="application_status" class="form-control width-auto-inline">
                        <option value="All">All</option>
                        <option value="Pending" selected="selected">Pending</option>
                        <option value="Deleted">Deleted</option>
                        <option value="Finished">Finished</option>
                    </select>
                    &nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-primary" name="btnRefineRequisitions" id="btnRefineRequisitions" onclick="getCompleteIncompleteApplications()" value="Refine">
                    <input type="button" class="btn btn-primary" name="btnExportApplicantHistory" id="btnExportApplicantHistory" onclick="exportRequisitions()" value="Export"><br><br>
                </form>

                <form id="frmApplicantFlowSortOptions" name="frmApplicantFlowSortOptions" method="post">
                    <input type="hidden" name="ddlToSort" id="ddlToSort" value="application_date">
                    <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
                </form>
        	 </div>
		 
    </div>
	<div class="row">
		<div class="col-lg-12">

				<form name="frmUserApplications" id="frmUserApplications" method="post">
					<div class="table-responsive">
					<table border="0" cellspacing="0" cellpadding="5"
						class="table table-striped table-bordered table-hover"
						id="data-complete-incomplete-applications">
						<thead>
                			<tr> 
                			    <td align="left"><strong>Application ID</strong></td>
                			    <td align="left"><strong><a href="javascript:void(0)" onclick="sortUserApplications('req_title')">Requisition Title</a></strong></td>
                            	<td align="left"><strong><a href="javascript:void(0)" onclick="sortUserApplications('first_name')">First Name</a></strong></td>
                            	<td align="left"><strong><a href="javascript:void(0)" onclick="sortUserApplications('last_name')">Last Name</a></strong></td>
                            	<td align="left"><strong><a href="javascript:void(0)" onclick="sortUserApplications('email')">Email</a></strong></td>
                            	<td align="left"><strong>Phone Number</strong></td>
                            	<td align="left"><strong><a href="javascript:void(0)" onclick="sortUserApplications('status')">Status</a></strong></td>
                            	<td align="left"><strong><a href="javascript:void(0)" onclick="sortUserApplications('lastupdateddate')">Last Updated</a></strong></td>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php
                            $user_applications_count = count($user_applications);

                            if($user_applications_count > 0) {
                            	for($k = 0; $k < count($user_applications); $k++) {
                            	    $ApplicationOrgID      =   $user_applications[$k]['OrgID'];
                            	    $ApplicationMultiOrgID =   $user_applications[$k]['MultiOrgID'];
                            	    $ApplicationUserID     =   $user_applications[$k]['UserID'];
                            	    $ApplicationRequestID  =   $user_applications[$k]['RequestID'];
                                    ?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo IRECRUIT_HOME?>applicantsSearch.php?action=view-application&ApplicationID=<?php echo $user_applications[$k]['ApplicationID']; ?>&RequestID=<?php echo $user_applications[$k]['RequestID']; ?>" target="_blank"><?php echo $user_applications[$k]['ApplicationID'];?></a>
                                        </td>
                                        <td><?php echo $user_applications[$k]['RequisitionTitle'];?></td>
										<td><?php echo $user_applications[$k]['FirstName'];?></td>
										<td><?php echo $user_applications[$k]['LastName'];?></td>
										<td><?php echo $user_applications[$k]['UserEmail'];?></td>
										<td>
                                        	<?php
                                        	$cell_phone_json   =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($ApplicationOrgID, $ApplicationMultiOrgID, $ApplicationUserID, $ApplicationRequestID, 'cellphone');
                                        	echo implode("-",json_decode($cell_phone_json, true));
                                        	?>
                                        </td>
										<td><?php echo $user_applications[$k]['Status'];?></td>
										<td><?php echo $user_applications[$k]['LastUpdatedDate'];?></td>
									</tr><?php
                                }
                            }
                            else {
                                ?><tr><?php
                            	?><td colspan="6">No records found</td><?php
                            	?></tr><?php
                            }
                            ?>
                        </tbody>
					</table>
					</div>
				</form>

		</div>
	</div>
	<div class="row">
        <div class="col-lg-12" id="user-applications-pagination">
        <?php
	        if($total_count > $Limit) {
               $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $Limit, $total_count, '', '');
               ?> 
    			<div class="row">
    			  	<div class="col-lg-6">
    			  		<strong>Per page:</strong> <?php echo $Limit;?>
    			  	</div>
    			  	<div class="col-lg-6" style="text-align:right">
    				  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
    				  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getCompleteIncompleteApplicationsPageNumber(document.getElementById('current_page_number').value)">
    			  	</div>
    			</div>
    			  
    			<div class="row">
    				  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
    				  	<?php echo $left_pagination_info['previous'];?>
    				  </div>
    				  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
    				  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
    				  </div>
    				  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
    				  	<?php echo $left_pagination_info['next'];?>
    				  </div>
    			</div>
    			<?php
           }
        ?>
        </div>
    </div>
</div>

<script>
function sortUserApplications(to_sort) {
	var IndexStart     		=   document.frmUserApplicationsFilter.IndexStart.value;
	var FilterOrganization	=	document.forms['frmUserApplicationsFilter'].ddlOrganizationsList.value;	
	
    document.frmApplicantFlowSortOptions.ddlToSort.value = to_sort;
    
	var sort_type  =   document.frmApplicantFlowSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmApplicantFlowSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmApplicantFlowSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var from_date      =   document.frmUserApplicationsFilter.applicants_from_date.value;
	var to_date        =   document.frmUserApplicationsFilter.applicants_to_date.value;
	
	var REQUEST_URL = irecruit_home + "reports/getCompleteIncompleteApplications.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&to_sort="+to_sort+"&sort_type="+sort_type;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-complete-incomplete-applications").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='19' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-complete-incomplete-applications").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setCompleteIncompleteApplications(data);
    	}
	});
}
        		      
function getRecordsByPage(IndexStart) {
	document.frmUserApplicationsFilter.IndexStart.value = IndexStart;

	var FilterOrganization	=	document.forms['frmUserApplicationsFilter'].ddlOrganizationsList.value;	
	var from_date      		=   document.frmUserApplicationsFilter.applicants_from_date.value;
	var to_date        		=   document.frmUserApplicationsFilter.applicants_to_date.value;
	var Status         		=   document.frmUserApplicationsFilter.application_status.value;
	
	var REQUEST_URL    		=   irecruit_home + "reports/getCompleteIncompleteApplications.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&Status="+Status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-complete-incomplete-applications").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='19' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-complete-incomplete-applications").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setCompleteIncompleteApplications(data);
    	}
	});
	
}

function getCompleteIncompleteApplicationsPageNumber(page_number) {
	
	var RecordsLimit = document.frmUserApplicationsFilter.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}

function setCompleteIncompleteApplications(data) {
    
	var RecordsLimit = document.frmUserApplicationsFilter.RecordsLimit.value;
	
	var app_prev         = data.previous;
	var app_next         = data.next;
	var total_pages      = data.total_pages;
	var current_page     = data.current_page;
	var total_count      = data.total_count;

	$("#data-complete-incomplete-applications").find("tr:gt(0)").remove();
	$("#user-applications-pagination").html("");

	var data_length;
	if(data['user_applications_list'] == null) {
		data_length = 0;
	}
	else {
		data_length = data['user_applications_list'].length;
	}
	
    var applicants_list = data['user_applications_list'];
    var req_results = "";
    var requisitions_pagination = "";
    
	$("#data-complete-incomplete-applications").find("tr:gt(0)").remove();
	
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {

			var RequisitionTitle	=	applicants_list[i].RequisitionTitle;
			var CellPhone			=	applicants_list[i].CellPhone;
			var FirstName			=	applicants_list[i].FirstName;
			var LastName			=	applicants_list[i].LastName;
			var UserEmail			=	applicants_list[i].UserEmail;
			var Status				=	applicants_list[i].Status;
			var LastUpdatedDate		=	applicants_list[i].LastUpdatedDate;
			var ApplicationID		=	applicants_list[i].ApplicationID;
			var RequestID			=	applicants_list[i].RequestID;

			if(CellPhone == null) CellPhone = '';
			if(FirstName ==	null) FirstName = '';
			if(LastName ==	null) LastName = '';
			if(UserEmail == null) UserEmail = '';
			if(Status == null) Status = '';
			if(RequisitionTitle == null) RequisitionTitle = '';
			if(LastUpdatedDate == null) LastUpdatedDate = '';
			if(ApplicationID == null) ApplicationID = '';
			if(RequestID == null) RequestID = '';
						
			req_results  = "<tr>";
			req_results += "<td align='left' valign='top'>";
			req_results += '<a href="'+irecruit_home+'applicantsSearch.php?action=view-application&ApplicationID='+ApplicationID+'&RequestID='+RequestID+'" target="_blank">'+ApplicationID+'</a>';
			req_results += "</td>";
			req_results += "<td align='left' valign='top'>"+RequisitionTitle+"</td>";
			req_results += "<td align='left' valign='top'>"+FirstName+"</td>";
			req_results += "<td align='left' valign='top'>"+LastName+"</td>";
			req_results += "<td align='left' valign='top'>"+UserEmail+"</td>";
			req_results += "<td align='left' valign='top'>"+CellPhone+"</td>";
			req_results += "<td align='left' valign='top'>"+Status+"</td>";
			req_results += "<td align='left' valign='top'>"+LastUpdatedDate+"</td>";
			req_results += "</tr>";

			$("#data-complete-incomplete-applications").append(req_results);

		}

		
	    if(parseInt(total_count) > parseInt(RecordsLimit)) {
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div class='col-lg-6'>";
			requisitions_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			requisitions_pagination += "</div>";

			requisitions_pagination += "<div class='col-lg-6' style='text-align:right'>";
			requisitions_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			requisitions_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getCompleteIncompleteApplicationsPageNumber(document.getElementById(\"current_page_number\").value)'>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			requisitions_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			requisitions_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";

		}

		$("#user-applications-pagination").html(requisitions_pagination);
		
	}
	else {

		req_results = "<tr>";
		req_results += "<td colspan='19' align='center'>No records found</td>";
		req_results += "</tr>";

		$("#data-complete-incomplete-applications").append(req_results);
	}
}

function exportRequisitions() {
	var FilterOrganization	=	document.forms['frmUserApplicationsFilter'].ddlOrganizationsList.value;	
	var from_date      		=   document.frmUserApplicationsFilter.applicants_from_date.value;
	var to_date        		=   document.frmUserApplicationsFilter.applicants_to_date.value;
	var to_sort        		=   document.frmApplicantFlowSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmApplicantFlowSortOptions.ddlSortType.value;
	var Status         		=   document.frmUserApplicationsFilter.application_status.value;
	
	var REQUEST_URL    		=   irecruit_home + "reports/getCompleteIncompleteApplications.php?FilterOrganization="+FilterOrganization+"&Export=YES"+"&FromDate="+from_date+"&ToDate="+to_date+"&to_sort="+to_sort+"&sort_type="+sort_type+"&Status="+Status;

	document.frmUserApplicationsFilter.action = REQUEST_URL;
	document.frmUserApplicationsFilter.submit();
}
		      
function getCompleteIncompleteApplications() {
	var FilterOrganization	=	document.forms['frmUserApplicationsFilter'].ddlOrganizationsList.value;	
	var from_date      		=   document.frmUserApplicationsFilter.applicants_from_date.value;
	var to_date        		=   document.frmUserApplicationsFilter.applicants_to_date.value;
	var IndexStart     		=   document.frmUserApplicationsFilter.IndexStart.value;
	var Status         		=   document.frmUserApplicationsFilter.application_status.value;
	
	var REQUEST_URL			=	irecruit_home + "reports/getCompleteIncompleteApplications.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&Status="+Status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#data-complete-incomplete-applications").find("tr:gt(0)").remove();
			req_results	 = "<tr>";
			req_results	+= "<td colspan='19' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			req_results	+= "</tr>";
			
			$("#data-complete-incomplete-applications").append(req_results);
		},
		success: function(data) {
			setCompleteIncompleteApplications(data);
    	}
	});
}
</script>
