<?php
$months_list    =   G::Obj('GenericLibrary')->getMonths();
?>
<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- row -->
	<div class="row">
    	<div class="col-lg-12">
			<input type="button" name="btnExportReqApproval" id="btnExportReqApproval" class="btn btn-primary" value="Export" onclick="exportApplicantsCountList('getApplicantsCountByMonthAndYearExport.php');">
			<span id="apps_export_loading_message"></span>
			<br><br>
    	</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                            <th>&nbsp;</th>
                            <?php
                                for($m = 1; $m <= count($months_list); $m++) {
                                    echo '<th>'.$months_list[$m].'</th>';
                                }
                            ?>	
                            <th>Annual Total</th>
                            </tr>
                            <?php
                            for($y = 0; $y < count($apps_years); $y++) {
                                ?>
                        		<tr>
                                	<th><?php echo $apps_years[$y];?></th>
                                	<?php
                                	$year_count = 0;
                                	for($m = 1; $m <= count($months_list); $m++) {
                                	    $year_count += $apps_list[$apps_years[$y]][$months_list[$m]];
                                	    echo '<td>'.$apps_list[$apps_years[$y]][$months_list[$m]].'</td>';
                                	}
                                	?>
                                	<td><?php echo $year_count;?></td>
                        		</tr>
                            	<?php
                            }
                            ?>
                        </table>
					<!-- /.row (nested) -->
				</div>
			</div>
			<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script>
	function exportApplicantsCountList(export_url) {
		$("#apps_export_loading_message").html('Please wait..');
		setTimeout(function() {
			location.href = export_url;
			$("#apps_export_loading_message").html('');
		 }, 2000);
	}
</script>