<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="openFormWindow" class="openForm">
	<div>
		<p style="text-align:right;">
			<a href="#closeForm" title="Close Window">
				<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
				<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
			</a>
		</p>
			
		<div id="monster_indeed_zip_information"></div>
	</div>
</div>


<div id="page-wrapper">

    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>

    <div class="row">
		<div class="col-lg-12">
			<form name="frmRequisitionFilters" id="frmRequisitionFilters" method="post">
			    <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
			    <input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
                <input type="hidden" name="IndexStart" id="IndexStart" value="0">
                <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $user_preferences['ReportsSearchResultsLimit'];?>">
                <input type="hidden" name="CurrentPage" id="CurrentPage" value="1"> 
				<?php
				if($feature['MultiOrg'] == "Y") {
				    $where_info     =   array("OrgID = :OrgID");
				    $params_info    =   array(":OrgID"=>$OrgID);
				    $columns        =   "OrgID, MultiOrgID, OrganizationName";
				    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
				    $orgs_list      =   $orgs_results['results'];
				    
				    echo 'Organizations List:';
				    echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
				    echo '<option value="">All</option>';
				    for($ol = 0; $ol < count($orgs_list); $ol++) {
				        if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
				            $selected = ' selected="selected"';
				        }
				        echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
				        unset($selected);
				    }
				    echo '</select>';
				}
				else {
				    echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
				}
				?>
				&nbsp;From: 
    			<input type="text" name="requisitions_from_date" id="requisitions_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
    			&nbsp;To: 
    			<input type="text" name="requisitions_to_date" id="requisitions_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
    			&nbsp;Search Words: <input type="text" name="search_word" id="search_word" class="form-control width-auto-inline" placeholder="Title,RequisitionID,JobID">
    			
				&nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-primary" name="btnRefineRequisitions" id="btnRefineRequisitions" onclick="getRequisitionsList()" value="Refine">
				
				<input type="button" class="btn btn-primary" name="btnExportApplicantHistory" id="btnExportApplicantHistory" onclick="exportRequisitions()" value="Export">
				<br><br>
			</form>
	         <form id="frmRequisitionsSortOptions" name="frmRequisitionsSortOptions" method="post">
    	         <input type="hidden" name="ddlToSort" id="ddlToSort" value="date_opened">
    	         <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
	        </form>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">

			<form name="frmRequisitionsSearchResults"
				id="frmRequisitionsSearchResults" method="post">
				<table border="0" cellspacing="0" cellpadding="5" width="100%"
					class="table table-striped table-bordered table-hover"
					id="data-requisitions">
					<thead>
            			<tr> 
            			    <td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('req_title')"><strong>Title</strong></a></td>
            			    <td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('req_id')"><strong>RequisitionID</strong></a> / <a href="javascript:void(0)" onclick="sortRequisitions('job_id')"><strong>JobID</strong></a></td>
                			<td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('date_opened')"><strong>Date<br>Opened</strong></a></td>
                			<td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('expire_date')"><strong>Expire Date</strong></a></td>
                            <td align="left"><a href="javascript:void(0)" onclick="sortRequisitions('days_open')"><strong>Address Information</strong></a></td>
                            
                            <td align="center"><strong>Free ZipRecruiter</strong></td>
                            <td align="center"><strong>Paid ZipRecruiter</strong></td>
                            <td align="center"><strong>Free Indeed</strong></td>
                            <td align="center"><strong>Paid Indeed</strong></td>
                            <td align="center"><strong>Free Monster</strong></td>
                            <td align="center"><strong>Paid Monster</strong></td>
                        </tr>
                    </thead>
                    
                    <tbody>
                    <?php 
                    if (is_array($requisition_search_results)) {

                        foreach ($requisition_search_results as $REQS) {
                            
                            $req_multi_org_id       =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
                            $indeed_feed_info       =   $IndeedObj->getIndeedFeedInformation("Email", $OrgID, "", $REQS['RequestID']);
                            $zip_recruiter_info     =   $ZipRecruiterObj->getZipRecruiterFeed($OrgID, $REQS['RequestID']);
                            $monster_req_info       =   $MonsterObj->getMonsterRequisitionInformation("RequestID", $OrgID, "", $REQS['RequestID']);
                            
                            echo '<tr>';

                            echo "<td>";
                            echo $RequisitionDetailsObj->getJobTitle($OrgID, $req_multi_org_id, $REQS['RequestID']);
                            echo "</td>";

                            echo "<td>";
                            echo $RequisitionDetailsObj->getReqJobIDs($OrgID, $req_multi_org_id, $REQS['RequestID']);
                            echo "</td>";
                            
							echo "<td>";
							echo $REQS['PostDate'];
							echo "</td>";
                            
                            echo "<td>";
                            echo $REQS['ExpireDate'];
                            echo "</td>";
                            
                            echo "<td align='left'>".$AddressObj->formatAddress($OrgID, $REQS['Country'], $REQS['City'], $REQS['State'], $REQS['Province'], $REQS['ZipCode'], '')."</td>";
                            
                            $free_jobboard_lists = json_decode($REQS['FreeJobBoardLists'], true);
                            
                            $free_monster =  "0";
                            $free_indeed = "0";
                            $free_ziprecruiter = "0";
                            
                            if(is_array($free_jobboard_lists) && in_array("Indeed", $free_jobboard_lists)) {
                                $free_indeed = "1";
                            }
                            if(is_array($free_jobboard_lists) && in_array("Monster", $free_jobboard_lists)) {
                                $free_monster = "1";
                            }
                            if(is_array($free_jobboard_lists) && in_array("ZipRecruiter", $free_jobboard_lists)) {
                                $free_ziprecruiter = "1";
                            }

                            if($free_ziprecruiter == "1") {
                                echo "<td align='center'>Yes</td>";
                            }
                            else {
                                echo "<td align='center'>&nbsp;</td>";
                            }
                            
                            if(isset($zip_recruiter_info['Email']) && $zip_recruiter_info['Email'] != "") {
                                echo "<td align='center'><a href='#openFormWindow' onclick=\"getPaidZipRecruiterInfo('".$REQS['RequestID']."')\">Details</a></td>";
                            }
                            else {
                                echo "<td align='center'>&nbsp;</td>";
                            }
                            
                            if($free_indeed == "1") {
                            	echo "<td align='center'>Yes</td>";
                            }
                            else {
                                echo "<td align='center'>&nbsp;</td>";
                            }
                            
                            if(isset($indeed_feed_info['Email']) && $indeed_feed_info['Email'] != "") {
                            	echo "<td align='center'><a href='#openFormWindow' onclick=\"getPaidIndeedInfo('".$REQS['RequestID']."')\">Details</a></td>";
                            }
                            else {
                            	echo "<td align='center'>&nbsp;</td>";
                            }

                            if($free_monster == "1") {
                                echo "<td align='center'><a href='#openFormWindow' onclick=\"getFreeMonsterInfo('".$REQS['RequestID']."')\">Details</a></td>";
                            }
                            else {
                                echo "<td align='center'>&nbsp;</td>";
                            }
                            
                            if(isset($monster_req_info['RequestID']) && $monster_req_info['RequestID'] != "") {
                                echo "<td align='center'><a href='#openFormWindow' onclick=\"getPaidMonsterInfo('".$REQS['RequestID']."')\">Details</a></td>";
                            }
                            else {
                            	echo "<td align='center'>&nbsp;</td>";
                            }
                            echo "</tr>";
                            
                        } // end foreach
                    }
                    ?>
                   </tbody>
				</table>
			</form>

		</div>
	</div>


	<div class="row">
        <div class="col-lg-12" id="requisitions-pagination">
		   <?php
	        if($total_count > $Limit) {
	           $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $Limit, $total_count, '', '');
	           ?> 
    			<div class="row">
				  	<div class="col-lg-6">
				  		<strong>Per page:</strong> <?php echo $Limit;?>
				  	</div>
				  	<div class="col-lg-6" style="text-align:right">
					  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
					  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getRequisitionsByPageNumber(document.getElementById('current_page_number').value)">
				  	</div>
    			</div>
    			  
    			<div class="row">
    				  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
    				  	<?php echo $left_pagination_info['previous'];?>
    				  </div>
    				  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
    				  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
    				  </div>
    				  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
    				  	<?php echo $left_pagination_info['next'];?>
    				  </div>
    			</div>
    			<?php
           }
	       ?>
        </div>		  	 
	</div>
</div>

<script>
function sortRequisitions(to_sort) {
	var IndexStart			=   document.frmRequisitionFilters.IndexStart.value;
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;
	
	document.frmRequisitionsSortOptions.ddlToSort.value = to_sort;
	
	var sort_type  =   document.frmRequisitionsSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmRequisitionsSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmRequisitionsSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var from_date  	=   document.frmRequisitionFilters.requisitions_from_date.value;
	var to_date    	=   document.frmRequisitionFilters.requisitions_to_date.value;
	var keyword    	=   document.frmRequisitionFilters.search_word.value;
				
	var REQUEST_URL =	irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-requisitions").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-requisitions").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
}

function getRecordsByPage(IndexStart) {
	document.frmRequisitionFilters.IndexStart.value = IndexStart;

	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var to_sort    			=   document.frmRequisitionsSortOptions.ddlToSort.value;
	var sort_type  			=   document.frmRequisitionsSortOptions.ddlSortType.value;
	var from_date  			=   document.frmRequisitionFilters.requisitions_from_date.value;
	var to_date    			=   document.frmRequisitionFilters.requisitions_to_date.value;
	var keyword    			=   document.frmRequisitionFilters.search_word.value;

	var REQUEST_URL			=	irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-requisitions").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
		
			$("#data-requisitions").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
	
}

function getRequisitionsByPageNumber(page_number) {
	
	var RecordsLimit = document.frmRequisitionFilters.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}


function setRequisitionsList(data) {
	var RecordsLimit 		=	document.frmRequisitionFilters.RecordsLimit.value;
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	
	var app_prev         = data.previous;
	var app_next         = data.next;
	var total_pages      = data.total_pages;
	var current_page     = data.current_page;
	var requisitions_count = data.requisitions_count;
	
	$("#data-requisitions").find("tr:gt(0)").remove();
	$("#requisitions-pagination").html("");
	
	var data_length = data['results'].length;
    var requisitions_list = data['results'];
    var req_results = "";
    var requisitions_pagination = "";
    
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {
            
			var Title                    =   requisitions_list[i].Title;
			var ReqIDJobID               =   requisitions_list[i].RequisitionID + "/" + requisitions_list[i].JobID;
			var PostDateByFeature        =   requisitions_list[i].PostDateByFeature;
			var ExpireDate               =   requisitions_list[i].ExpireDate;
			var Open                     =   requisitions_list[i].Open;
			var InternalRequisition      =   requisitions_list[i].InternalRequisition;
			var AddressInformation       =   requisitions_list[i].AddressInformation;
			var FreeIndeed               =   requisitions_list[i].FreeIndeed;
			var FreeZipRecruiter         =   requisitions_list[i].FreeZipRecruiter;
			var FreeMonster              =   requisitions_list[i].FreeMonster;
			var PaidIndeed               =   requisitions_list[i].PaidIndeed;
			var PaidMonster              =   requisitions_list[i].PaidMonster;
			var PaidZipRecruiter         =   requisitions_list[i].PaidZipRecruiter;
			
			var RequestID                =   requisitions_list[i].RequestID;
			var internal_style           =   '';
						
			if(Title == null) Title = '';
			if(ReqIDJobID == null) ReqIDJobID = '';
			if(ExpireDate == null) ExpireDate = '';
			if(Open == null) Open = '0';
			if(typeof(PostDateByFeatureYMD) == 'undefined' || PostDateByFeatureYMD == null) PostDateByFeatureYMD = '';
			if(typeof(ExpireDateYMD) == 'undefined' || ExpireDateYMD == null) ExpireDateYMD = '';
			if(InternalRequisition == 'Y') internal_style = ' style="color:red"';
			
			req_results  = "<tr>";
			req_results += "<td valign='top'>"+Title+"</td>";
			req_results += "<td valign='top'>"+ReqIDJobID+"</td>";
			req_results += "<td valign='top'"+internal_style+">"+PostDateByFeature+"</td>";
			req_results += "<td valign='top'>"+ExpireDate+"</td>";
			req_results += "<td valign='top'>"+AddressInformation+"</td>";

			req_results += "<td valign='top'>"+FreeZipRecruiter+"</td>";
			if(PaidZipRecruiter == "Yes") {
				req_results += "<td valign='center'><a href='#openFormWindow' onclick=\"getPaidZipRecruiterInfo('"+RequestID+"')\">Details</a></td>";
			}
			else {
				req_results += "<td valign='center'></td>";
			}
			
			req_results += "<td valign='top'>"+FreeIndeed+"</td>";
			if(PaidIndeed == "Yes") {
			    req_results += "<td valign='center'><a href='#openFormWindow' onclick=\"getPaidIndeedInfo('"+RequestID+"')\">Details</a></td>";
			}
			else {
				req_results += "<td valign='center'></td>";
			}
			
			if(FreeMonster == "Yes") {
				req_results += "<td valign='center'><a href='#openFormWindow' onclick=\"getFreeMonsterInfo('"+RequestID+"')\">Details</a></td>";
			}
			else {
				req_results += "<td valign='center'>&nbsp;</td>";
			}
			
			if(PaidMonster == "Yes") {
				req_results += "<td valign='center'><a href='#openFormWindow' onclick=\"getPaidMonsterInfo('"+RequestID+"')\">Details</a></td>";
			}
			else {
				req_results += "<td valign='center'></td>";
			}
			
			req_results += "</tr>";

			$("#data-requisitions").append(req_results);
		}

		if(parseInt(requisitions_count) > parseInt(RecordsLimit)) {
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div class='col-lg-6'>";
			requisitions_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			requisitions_pagination += "</div>";

			requisitions_pagination += "<div class='col-lg-6' style='text-align:right'>";
			requisitions_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			requisitions_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getRequisitionsByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			requisitions_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			requisitions_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";
			
		}

		$("#requisitions-pagination").html(requisitions_pagination);
		
	}
	else {

		req_results  = "<tr>";
	    req_results += "<td colspan='8' align='center'>No records found</td>";
		req_results += "</tr>";

		$("#data-requisitions").append(req_results);
	}

}

function getPaidMonsterInfo(RequestID) {
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var REQUEST_URL			=	irecruit_home + "requisitions/getPaidMonsterInfo.php?FilterOrganization="+FilterOrganization+"&RequestID="+RequestID;

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		beforeSend: function(data) {
    			$("#monster_indeed_zip_information").html("Please wait...");
        	},
        	success: function(data) {
        		$("#monster_indeed_zip_information").html(data);
        	}
	});
}

function getFreeMonsterInfo(RequestID) {
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var REQUEST_URL			=	irecruit_home + "requisitions/getFreeMonsterInfo.php?FilterOrganization="+FilterOrganization+"&RequestID="+RequestID;

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		beforeSend: function(data) {
    			$("#monster_indeed_zip_information").html("Please wait...");
        	},
        	success: function(data) {
        		$("#monster_indeed_zip_information").html(data);
        	}
	});
}

function getPaidIndeedInfo(RequestID) {
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var REQUEST_URL			=	irecruit_home + "requisitions/getPaidIndeedInfo.php?FilterOrganization="+FilterOrganization+"&RequestID="+RequestID;

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		beforeSend: function(data) {
    			$("#monster_indeed_zip_information").html("Please wait...");
        	},
        	success: function(data) {
        		$("#monster_indeed_zip_information").html(data);
        	}
	});
}

function getPaidZipRecruiterInfo(RequestID) {
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var REQUEST_URL			=	irecruit_home + "requisitions/getPaidZipRecruiterInfo.php?FilterOrganization="+FilterOrganization+"&RequestID="+RequestID;

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		beforeSend: function(data) {
    			$("#monster_indeed_zip_information").html("Please wait...");
        	},
        	success: function(data) {
        		$("#monster_indeed_zip_information").html(data);
        	}
	});
}

function getRequisitionsList() {
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var RecordsLimit   		=   document.getElementById('RecordsLimit').value;

	var IndexStart     		=   document.frmRequisitionFilters.IndexStart.value = 0;
	var to_sort        		=   document.frmRequisitionsSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmRequisitionsSortOptions.ddlSortType.value;
	var from_date      		=   document.frmRequisitionFilters.requisitions_from_date.value;
	var to_date        		=   document.frmRequisitionFilters.requisitions_to_date.value;
	var keyword        		=   document.frmRequisitionFilters.search_word.value;
	
	document.frmRequisitionFilters.CurrentPage.value = 1;	
			
	var REQUEST_URL			=	irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&RecordsLimit="+RecordsLimit+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword;

	$.ajax({
    		method: "POST",
      		url:	REQUEST_URL,
    		type: 	"POST",
    		dataType: 'json',
    		beforeSend: function() {
    			
    			$("#data-requisitions").find("tr:gt(0)").remove();
    			requisition_details_loading_status	 =	"<tr>";
    			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
    			requisition_details_loading_status	+=	"</tr>";
    			
    			$("#data-requisitions").append(requisition_details_loading_status);
    			
    	},
    	success: function(data) {
    		setRequisitionsList(data);
    	}
	});
}                            

function exportRequisitions() {
	var FilterOrganization	=	document.forms['frmRequisitionFilters'].ddlOrganizationsList.value;	
	var from_date      		=   document.frmRequisitionFilters.requisitions_from_date.value;
	var to_date        		=   document.frmRequisitionFilters.requisitions_to_date.value;
	var keyword        		=   document.frmRequisitionFilters.search_word.value;
	var to_sort        		=   document.frmRequisitionsSortOptions.ddlToSort.value;
    var sort_type      		=   document.frmRequisitionsSortOptions.ddlSortType.value;
	
	var REQUEST_URL			=	irecruit_home + "reports/getRequisitionsByJobBoardStatus.php?FilterOrganization="+FilterOrganization+"&Export=YES&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type;

	document.frmRequisitionFilters.action = REQUEST_URL;
    document.frmRequisitionFilters.submit();
}
</script>
