<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- row -->
    <div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<form name="frmReqApproval" id="frmReqApproval" method="GET">
			Application Date From: 
			<input type="text" name="from_date" id="from_date" class="form-control width-auto-inline" value="<?php echo $from_date; ?>">
			Application Date To: 
			<input type="text" name="to_date" id="to_date" class="form-control width-auto-inline" value="<?php echo $to_date; ?>">
			&nbsp;
			<input type="submit" name="btnRefineApplicants" id="btnRefineApplicants" class="btn btn-primary" value="Refine Requisitions">
			&nbsp;
			<input type="button" name="btnExportReqApproval" id="btnExportReqApproval" class="btn btn-primary" value="Export" onclick="exportRequisitionsApproval('requisitionApprovalExport.php');">
			<span id="req_export_loading_message"></span>
			
            </form>
			<br><br>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php
					if ($feature ['RequisitionApproval'] != "Y") {
						die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
					}
					
					$ii = 0;
					
					echo '<div class="table-responsive">';
					echo '<table border="0" cellspacing="0" cellpadding="5" id="table_req_app_list" class="table table-striped table-bordered table-hover" width="100%">' . "\n";
                    echo '<thead>';
					echo '<tr>';
					$t = 0;
					foreach ($map_columns as $map_column_id=>$map_column_heading) {
					    if($map_column_id == "MaxApprovers") {
					        for($maxa = 0; $maxa < $map_column_heading; $maxa++) {
					            echo '<th>Approver</th>';
					            echo '<th>Authority Level</th>';
					            echo '<th>Approved On</th>';
					            $t = $t+2;
					        }
					    }
					    else {
					        echo '<th>'.$map_column_heading.'</th>';
					        $t = $t+1;
					    }
					}
					echo '</tr>';
					echo '</thead>';
					
					echo '<tbody>';
					if (is_array ( $req_approval_list )) {
					    foreach ( $req_approval_list as $RR ) {
							
							echo '<tr>';
							
							echo '<td valign="top">';
							echo $RR['DateEntered'];
							echo '</td>';
							
							echo '<td>';
							echo $RR ['RequesterName'];
							echo '</td>';

							echo '<td>';
							echo $RR ['Title'];
							echo '</td>';
							
							echo '<td>';
							echo $RR ['RequisitionID'];
							echo '</td>';

							echo '<td>';
							echo $RR ['JobID'];
							echo '</td>';

							echo '<td>';
							echo $RR ['RequesterReason'];
							echo '</td>';
							
							echo '<td>';
							echo $RR ['PostDate'];
							echo '</td>';
							
							echo '<td>';
							echo $RR ['ExpireDate'];
							echo '</td>';
							
							echo '<td>';
							echo $RR ['ApprovalLevelRequired'];
							echo '</td>';
							
							for($maxa = 0; $maxa < $map_columns["MaxApprovers"]; $maxa++) {
							    echo '<td>'.$RR['RequestApprovers'][$maxa]['EmailAddress'].'</td>';
							    echo '<td style="text-align:center;">'.$RR['RequestApprovers'][$maxa]['AuthorityLevel'].'</td>';
							    echo '<td>'.$RR['RequestApprovers'][$maxa]['DateApproval'].'</td>';
					        }
							
							echo '<td>';
							echo $RR ['ApprovalStatus']."&nbsp;";
							echo '</td>';
							
							echo '<td>';
							echo $RR ['DaysOpen']."&nbsp;";
							echo '</td>';
							
							echo '<td>';
							echo $RR ['ApplicantsCount']."&nbsp;";
							echo '</td>';
							
							echo '</tr>';
							
							$ii++;
						}
					}
					
					echo '</tbody>';
					
					if ($ii == 0) {
						echo '<tr>';
						echo '<td colspan="'.$t.'" align="center">There are no requests to approve.</td>';
						echo '</tr>';
					}
					
					echo '</table>';
					echo '</div>';
					?>
					<!-- /.row (nested) -->
				</div>
			</div>
			<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script>
    $(document).ready(function() {
        $('#table_req_app_list').DataTable({
            responsive: true,
            aLengthMenu: [
              [50, 100, -1],
              [50, 100, "All"]
            ]
        });
    });

	function exportRequisitionsApproval(export_url) {
		var from_date = document.forms['frmReqApproval'].from_date.value;
		var to_date = document.forms['frmReqApproval'].to_date.value;
		
		$("#req_export_loading_message").html('Please wait..');
		setTimeout(function() {
			location.href = export_url+"?from_date="+from_date+"&to_date="+to_date;
			$("#req_export_loading_message").html('');
		 }, 2000);
	}
</script>
