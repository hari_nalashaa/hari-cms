<?php 
include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';

echo '<div id="page-wrapper">';
include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';

$where          =       array ("UserID = :UserID");
$params         =       array (":UserID" => $USERID);
$results        =       G::Obj('IrecruitUsers')->getUserInformation ( "EmailAddress", $where, "", array ($params) );
$EmailAddress = $results['results'][0]['EmailAddress'];

if (($USERID != "") && ($EmailAddress != "")) {

   $script = IRECRUIT_DIR . 'reports/piperReqReport.php ' . $USERID . ' > /dev/null 2>&1 &';
   exec($script);

   echo 'Update Initiated. An email will be sent to ' . $EmailAddress . ' when it completes.';

} else {

   echo 'Not able to determine an email address to send to. Please update your user profile.';

}
echo '</div>';

?>


