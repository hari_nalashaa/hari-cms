<style>
.btn-simple-custom {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	border:2px solid <?php echo $navbar_color;?>;	
}
.pagination_link {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
	padding:5px;
}
.pagination_link:hover {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
	padding:5px;
}
.btn-small {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
}
#span_left_page_nav_previous, #span_fullview_page_nav_previous {
	border:0px solid #f5f5f5;
	text-align:left;
	float:left;
	padding-top:5px;
	padding-bottom:5px;
	height:30px;
	text-align:center;
	margin-top:5px;
}
#span_left_page_nav_next, #span_fullview_page_nav_next {
	text-align:right;
	float:right;
	border:0px solid #f5f5f5;
	padding-top:5px;
	padding-bottom:5px;
	height:30px;
	text-align:center;
	margin-top:5px;
}
.current_page_and_total_pages {
	text-align:center;
	float:left;
	border:0px solid #f5f5f5;
	padding-top:10px;
	padding-bottom:0px;
}
</style>