<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="page-wrapper">

<style>
th, td {
  padding: 3px;
}
</style>
    
    <div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title;?></h1>
		</div>
	</div>
    
    <div class="row">
        <div class="col-lg-12" id="status_message"></div>
    </div>
    <div class="row">
	   <div class="col-lg-12">
	       <form name="frmApplicantsParams" id="frmApplicantsParams" method="post">
               <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
               <input type="hidden" name="IndexStart" id="IndexStart" value="0">
               <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $user_preferences['ReportsSearchResultsLimit'];?>">
               <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">

		<table>
                <?php
                if($feature['MultiOrg'] == "Y") {
                	$where_info     =   array("OrgID = :OrgID");
                    $params_info    =   array(":OrgID"=>$OrgID);
                    $columns        =   "OrgID, MultiOrgID, OrganizationName";
                    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
                    $orgs_list      =   $orgs_results['results'];
                    ?>
                <tr>
                    <td style="text-align:right">
                    Organizations List: 
		    </td>
		    <td>
                    <select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">
                    	<option value="">Select</option>
                    	<?php
                    for($ol = 0; $ol < count($orgs_list); $ol++) {
                        ?><option value="<?php echo $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'];?>"><?php echo $orgs_list[$ol]['OrganizationName']?></option><?php
                    }
                    ?>	
                    </select>
		    </td>
		</tr>
                    <?php
                }
                else {
                    ?><input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value=""><?php
                }
                ?>
                <tr>
                    <td style="text-align:right">
                        Requisitions Status:
		    </td>
		    <td>
        			    <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline" onchange="getRequisitionsByStatus(this.value)">
        					<option value="A">All</option>
        					<option value="Y">Active</option>
        					<option value="N">Inactive</option>
        				</select>
    				</td>
				</tr>
				<tr>    				
                    		<td valign="top" style="text-align:right">
                       				Requisitions:&nbsp;
                       			</td>
                       			<td>
									<select name="ddlRequisitions" id="ddlRequisitions" multiple="multiple" class="form-control width-auto-inline" style="max-width: 300px">
                        	           <option value="">All Requisitions</option>
                        	           <?php 
                        	           for($r = 0; $r < count($requisitions_list); $r++) {
                        	               if($requisitions_list[$r]['RequestID'] == $_REQUEST['RequestID']) $ddl_selected = ' selected="selected"';
                        	               ?><option value="<?php echo $requisitions_list[$r]['RequestID']?>" <?php echo $ddl_selected;?>><?php echo $requisitions_list[$r]['Title']." - ".$requisitions_list[$r]['RequisitionID']." / ".$requisitions_list[$r]['JobID'];?></option><?php
                        	               unset($ddl_selected);
                        	           }
                        	           ?>
                        	       </select>
                       			</td>
                       		</tr>
				<tr>
                     		<td style="text-align:right">
        	       From: 
				</td>
				<td>
        		   <input type="text" name="applicants_from_date" id="applicants_from_date" class="form-control width-auto-inline" value="<?php echo $from_date; ?>">
        		   &nbsp;To: 
        		   <input type="text" name="applicants_to_date" id="applicants_to_date" class="form-control width-auto-inline" value="<?php echo $to_date; ?>">
					</td>
				</tr>
				<tr>		
				   <td style="text-align:right">
					Applicant Name:
				   </td>
				   <td>
                   <input type="text" name="search_word" id="search_word" class="form-control width-auto-inline"> 
                   &nbsp;Applicant Status: 
                   <select name="ProcessOrder" id="ProcessOrder" class="form-control width-auto-inline">
            			<option value="" <?php if($ProcessOrder == "") echo ' selected="selected"'?>>All</option>
            			<?php
            			//Set where condition
            			$where           =   array("OrgID = :OrgID","Active = 'Y'");
            			//Set parameters
            			$params          =   array(":OrgID"=>$OrgID);
            			$org_status_res  =   G::Obj('Applicants')->getApplicantProcessFlowInfo("*", $where, 'ProcessOrder', array($params));
            			$orgstatuslist   =   $org_status_res['results'];
            			
            			foreach ( $orgstatuslist as $APF ) {
            				$APFPO = $APF ['ProcessOrder'];
            				$APFDESC = $APF ['Description'];
            
            				if ($ProcessOrder === $APFPO) {
            					$selected = ' selected';
            				} else {
            					$selected = '';
            				}
            				echo '<option value="' . $APFPO . '"' . $selected . '>' . $APFDESC;
            			}
            			?>
            		</select>
            		</td>
                   </tr>
                   <tr>
                   <td>
		   </td>
		   <td>
                   <input type="button" class="btn btn-primary" name="btnApplicantStatusByRequisition" id="btnApplicantStatusByRequisition" onclick="getApplicantsByRequestID()" value="Refine">
        	       
        	       <input type="button" class="btn btn-primary" name="btnExportApplicants" id="btnExportApplicants" onclick="exportApplicants()" value="Export">
	    	       </td>
	    	       </tr>
    	       </table>
	       </form>

			<form name="frmSortOptions" id="frmSortOptions" method="post">
				<input type="hidden" name="ddlToSort" id="ddlToSort" value="entry_date">
    	        <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
			</form>
	   </div>
	   
	</div>   

    <div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12">
	       <form name="frmApplicants" id="frmApplicants" method="post">
	               <h4>Requisition Details</h4>
	               <p>Note*: If No requistion or All Requisitions is selected then the requisition details will be empty.</p>
	               <table class="table table-bordered" id="requisition_detail_information">
                        <tr>
                            <th>Title</th>
                            <th>ReqID / JobID</th>
                            <th>Date Opened</th>
                            <th>Expiration Date</th>
                            <th>Days Open</th>
                            <th>Applicant Count</th>
                            <th>Organization</th>
                        </tr>
        	       </table>
	       </form>
	   </div>
	</div>
	
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12">
	       <form name="frmApplicants" id="frmApplicants" method="post">
	               <h4>Applicants Status By Requisition</h4>
	               <div class="table-responsive">
    	               <table class="table table-bordered" id="applicants_results">
    	                   <thead>
                            <tr>
                                <th><input type="checkbox" onclick="check_uncheck(this.checked, 'applicants_list')"></th>
                                <th>Title</th>
                                <th>ReqID / JobID</th>
                                <th><a href="javascript:void(0)" onclick="sortApplicants('application_id')"><strong>ApplicationID</strong></a></th>
                                <th><a href="javascript:void(0)" onclick="sortApplicants('applicant_name')"><strong>Name</strong></a></th>
                                <th><a href="javascript:void(0)" onclick="sortApplicants('entry_date')"><strong>Entry Date</strong></a></th>
                                <th><a href="javascript:void(0)" onclick="sortApplicants('status')"><strong>Status</strong></a></th>
                                <?php
                                if ($feature ['InternalRequisitions'] == "Y") {
                                    ?>
                                    <th>
                                    	<a href="javascript:void(0)" onclick="sortApplicants('is_internal_application')">
                                    		<strong>
                                    			Internal Application
                                    		</strong>
                                    	</a>
                                    </th>
                                    <?php
                                }
                                ?>
                                <th><strong>Disposition Code</strong></th>
                                <th><a href="javascript:void(0)" onclick="sortApplicants('last_updated')"><strong>Last Updated</strong></a></th>
                                <th>Email</th>
                                <th>Phone Number</th>
                            </tr>
                           </thead>
                           <tbody> 
                                <tr>
                                    <td colspan="10">Please select any requisition to view the results.</td>
                                </tr>	
                           </tbody> 
            	       </table>
	               </div>
	       </form>
	       
	   </div>
	</div>   
	
	<div class="row">
        <div class="col-lg-12" id="applicants-pagination"></div>
    </div>
</div>

<script>
var FeatureInternalRequisitions = '<?php echo $feature ['InternalRequisitions'];?>';
</script>

<script src="<?php echo IRECRUIT_HOME?>js/applicants-status-by-requisition.js" type="text/javascript"></script>
