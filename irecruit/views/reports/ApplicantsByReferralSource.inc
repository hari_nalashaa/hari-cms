<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="page-wrapper">
    
    <div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title;?></h1>
		</div>
	</div>

	<div class="row">
	     <div class="col-lg-9">
    	   <form name="frmApplicantsParams" id="frmApplicantsParams" method="post">
    	       <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
               <input type="hidden" name="IndexStart" id="IndexStart" value="0">
               <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $user_preferences['ReportsSearchResultsLimit'];?>">
               <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
               
               Requisitions Status:
			   <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline" onchange="getRequisitionsByStatus(this.value)">
					<option value="A">All</option>
					<option value="Y" selected="selected">Active</option>
					<option value="N">Inactive</option>
			   </select>
                   
    	       &nbsp;Requisition: 
    	       <select name="ddlRequisitions" id="ddlRequisitions" class="form-control width-auto-inline" style="max-width: 350px">
    	           <option value="">Select</option>
    	           <?php 
    	           for($r = 0; $r < count($requisitions_list); $r++) {
    	           	   ?><option value="<?php echo $requisitions_list[$r]['RequestID']?>"><?php echo $requisitions_list[$r]['Title']." - ".$requisitions_list[$r]['RequisitionID']." / ".$requisitions_list[$r]['JobID'];?></option><?php
    	           }
    	           ?>
    	       </select> &nbsp; <br><br>
               Organization: 
    	       <select name="MultiOrgID" id="MultiOrgID" class="form-control width-auto-inline" style="max-width: 350px">
    			<?php 
    			// Set condition
    			$where = array ("OrgID = :OrgID");
    			// Set parameters for prepared query
    			$params = array (":OrgID" => $OrgID);
    			// Get organization information
    			$results = $OrganizationsObj->getOrgDataInfo ( "MultiOrgID, OrganizationName", $where, '', array ($params) );
    				
    			if (is_array ( $results ['results'] )) {
    				foreach ( $results ['results'] as $OD ) {
    					echo '<option value="' . $OD ['MultiOrgID'] . '">';
    					echo $OD ['OrganizationName'];
    					echo '</option>';
    				} // end foreach
    			}
    			?>
    			</select>
    			&nbsp;&nbsp;Search Words: <input type="text" name="search_word" id="search_word" class="form-control width-auto-inline" placeholder="Applicant Name, Source">
    			<br><br>			
    			Application Date From: 
    			<input type="text" name="application_from_date" id="application_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
    			&nbsp;Application Date To: 
    			<input type="text" name="application_to_date" id="application_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
				<br><br>
    			<input type="button" name="btnRefineApplicants" id="btnRefineApplicants" class="btn btn-primary" value="Refine Applicants" onclick="getApplicantsListByOptions()">
    			<input type="button" class="btn btn-primary" name="btnExportApplicants" id="btnExportApplicants" onclick="exportApplicants()" value="Export">			
    			
    	   </form>
		   <form name="frmSortOptions" id="frmSortOptions" method="post">
		         <input type="hidden" name="ddlToSort" id="ddlToSort" value="entry_date">
    	         <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
		   </form>
	   </div>
	</div>
	
	<div class="row">
        <div class="col-lg-12" id="status_message"></div>
    </div>
	
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12">
	       <form name="frmApplicants" id="frmApplicants" method="post">
	               <br><br>
	               <table class="table table-bordered" id="applicants_results">
	                   <thead>
                        <tr>
                            <th><input type="checkbox" onclick="check_uncheck(this.checked, 'applicants_list')"></th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('application_id')"><strong>ApplicationID</strong></a></th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('req_title')"><strong>Requisition Title</strong></a></th>
                            <th>
                                <a href="javascript:void(0)" onclick="sortApplicants('req_id')"><strong>Requisition ID</strong></a> / 
                                <a href="javascript:void(0)" onclick="sortApplicants('job_id')"><strong>Job ID</strong></a>
                            </th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('applicant_name')"><strong>Name</strong></a></th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('entry_date')"><strong>Entry Date</strong></a></th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('status')"><strong>Status</strong></a></th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('last_updated')"><strong>Last Updated</strong></a></th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th><a href="javascript:void(0)" onclick="sortApplicants('referral')"><strong>Source</strong></a></th>
                        </tr>
                       </thead>
                       <tbody> 
                            <tr>
                                <td colspan="10">Please select any requisition to view the results.</td>
                            </tr>	
                       </tbody> 
        	       </table>
	               
	       </form>
	       
	   </div>
	</div>   

	<div class="row">
        <div class="col-lg-12" id="applicants-pagination"></div>
    </div>
</div>

<script>
function getRequisitionsByStatus(RequisitionStatus) {
    $('#ddlRequisitions').children('option:not(:first)').remove();

    var option_text		=	'';
    var option_value	=	'';
    var options_list    =   '';

	$.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?RequisitionStatus="+RequisitionStatus,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#status_message").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		+= " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#status_message").html("");
			$("#ddlRequisitions").append(options_list);
    	}
	});
}


function sortApplicants(to_sort) {
	var FromDate       =   document.getElementById('application_from_date').value;
	var ToDate         =   document.getElementById('application_to_date').value;
	var Organization   =   document.getElementById('MultiOrgID').value;
	var RequestID      =   document.getElementById('ddlRequisitions').value;
	var keyword        =   document.frmApplicantsParams.search_word.value;
	var IndexStart     =   document.frmApplicantsParams.IndexStart.value;

    document.frmSortOptions.ddlToSort.value = to_sort;
	
	var sort_type  =   document.frmSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}

	var REQUEST_URL = irecruit_home + "services/getApplicants.php?FromDate="+FromDate+"&ToDate="+ToDate+"&RequestID="+RequestID+"&Organization="+Organization+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#applicants_results").find("tr:gt(0)").remove();
			applicants_status    =   "<tr>";
			applicants_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			applicants_status	+=	"</tr>";
			
			$("#applicants_results").append(applicants_status);
			
		},
		success: function(data) {
			setApplicantsList(data);
    	}
	});
}
    			
function getApplicantsListByOptions() {
	var FromDate       =   document.getElementById('application_from_date').value;
	var ToDate         =   document.getElementById('application_to_date').value;
	var RequestID      =   document.getElementById('ddlRequisitions').value;
	var Organization   =   document.getElementById('MultiOrgID').value;

	var to_sort        =   document.frmSortOptions.ddlToSort.value;
	var sort_type      =   document.frmSortOptions.ddlSortType.value;
	
	var RecordsLimit   =   document.getElementById('RecordsLimit').value;
	var IndexStart     =   document.frmApplicantsParams.IndexStart.value = 0;
	document.frmApplicantsParams.CurrentPage.value = 1;	

	var keyword        =   document.frmApplicantsParams.search_word.value;
	
	var REQUEST_URL    =   irecruit_home + "services/getApplicants.php?FromDate="+FromDate+"&ToDate="+ToDate+"&RequestID="+RequestID+"&Organization="+Organization+"&RecordsLimit="+RecordsLimit+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#applicants_results").find("tr:gt(0)").remove();
			app_full_view_results	 = "<tr>";
			app_full_view_results	+= "<td colspan='9' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			app_full_view_results	+= "</tr>";
			
			$("#applicants_results").append(app_full_view_results);
		},
		success: function(data) {
			setApplicantsList(data);
    	}
	});
}

function getRecordsByPage(IndexStart) {
	document.frmApplicantsParams.IndexStart.value = IndexStart;

	var RecordsLimit   =   document.frmApplicantsParams.RecordsLimit.value;
	var FromDate       =   document.getElementById('application_from_date').value;
	var ToDate         =   document.getElementById('application_to_date').value;
	var RequestID      =   document.getElementById('ddlRequisitions').value;
	var Organization   =   document.getElementById('MultiOrgID').value;

	var to_sort        =   document.frmSortOptions.ddlToSort.value;
	var sort_type      =   document.frmSortOptions.ddlSortType.value;
	var keyword        =   document.frmApplicantsParams.search_word.value;
	
	var REQUEST_URL    =   irecruit_home + "services/getApplicants.php?FromDate="+FromDate+"&ToDate="+ToDate+"&RequestID="+RequestID+"&Organization="+Organization+"&RecordsLimit="+RecordsLimit+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&Keyword="+keyword;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#applicants_results").find("tr:gt(0)").remove();
			applicants_list	 =	"<tr>";
			applicants_list	+=	"<td colspan='9' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			applicants_list	+=	"</tr>";
			
			$("#applicants_results").append(applicants_list);
			
		},
		success: function(data) {
			setApplicantsList(data);
    	}
	});
	
}

function getApplicantsByPageNumber(page_number) {
	
	var RecordsLimit = document.frmApplicantsParams.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}

function setApplicantsList(data) {

    var RecordsLimit = document.frmApplicantsParams.RecordsLimit.value;
	
	var app_prev         = data.previous;
	var app_next         = data.next;
	var total_pages      = data.total_pages;
	var current_page     = data.current_page;
	var applicants_count = data.applicants_count;
	
	$("#applicants_results").find("tr:gt(0)").remove();
	$("#applicants-pagination").html("");
	
	var app_full_view_results = '';
	var applications_pagination = "";
	var data_length = data['applicants_list'].length;
	var applicants_list = data['applicants_list'];
	
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {
			
			var ApplicationID 		=	applicants_list[i].ApplicationID;
			var Title 		        =	applicants_list[i].Title;
			var RequisitionID 		=	applicants_list[i].RequisitionID;
			var JobID         		=	applicants_list[i].JobID;
			var ApplicantSortName 	=	applicants_list[i].ApplicantSortName;
			var ApplicantEmail 		=	applicants_list[i].Email;
			var EntryDate 			=	applicants_list[i].EntryDateMDY;
			var LastModified 		=	applicants_list[i].LastModifiedMDY;
			var ProcessOrder 		=	applicants_list[i].ProcessOrder;
			var RequestID 			=	applicants_list[i].RequestID;
			var HomePhone			=	applicants_list[i].HomePhone;
			var ReferralSource		=	applicants_list[i].ReferralSource;

			if(ApplicantSortName == null || typeof(ApplicantSortName) == 'undefined') ApplicantSortName = '';
			if(ApplicantEmail == null || typeof(ApplicantEmail) == 'undefined') ApplicantEmail = '';
			if(EntryDate == null || typeof(EntryDate) == 'undefined') EntryDate = '';
			if(LastModified == null || typeof(LastModified) == 'undefined') LastModified = '';
			if(ProcessOrder == null || typeof(ProcessOrder) == 'undefined') ProcessOrder = '';
			if(RequestID == null || typeof(RequestID) == 'undefined') RequestID = '';
			if(HomePhone == null || typeof(HomePhone) == 'undefined') HomePhone = '';
			if(ReferralSource == null || typeof(ReferralSource) == 'undefined') ReferralSource = '';
			if(Title == null || typeof(Title) == 'undefined') Title = 'N/A';
			if(RequisitionID == null || typeof(RequisitionID) == 'undefined') RequisitionID = 'N/A';
			if(JobID == null || typeof(JobID) == 'undefined') JobID = 'N/A';
			
			var app_request_id		=	ApplicationID+":"+RequestID;
			
			app_full_view_results += "<tr>";
			app_full_view_results += "<td><input type='checkbox' name='appids_list[]' id='app_req_id_"+app_request_id+"' value='"+app_request_id+"'></td>";
			app_full_view_results += "<td valign='top'><a href='"+irecruit_home+"applicantsSearch.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID+"' target='_blank'>"+ApplicationID+"</a></td>";
			app_full_view_results += "<td valign='top'>"+Title+"</td>";
			app_full_view_results += "<td valign='top'>"+RequisitionID+" / " +JobID+"</td>";
			app_full_view_results += "<td valign='top'>"+ApplicantSortName+"</td>";
			app_full_view_results += "<td valign='top'>"+EntryDate+"</td>";
			app_full_view_results += "<td valign='top'>"+ProcessOrder+"</td>";
			app_full_view_results += "<td valign='top'>"+LastModified+"</td>";
			app_full_view_results += "<td valign='top'>"+ApplicantEmail+"</td>";
			app_full_view_results += "<td valign='top'>"+HomePhone+"</td>";
			app_full_view_results += "<td valign='top'>"+ReferralSource+"</td>";
			app_full_view_results += "</tr>";
			
		}

	    if(parseInt(applicants_count) > parseInt(RecordsLimit)) {
			
			applications_pagination += "<div class='row'>";
			
			applications_pagination += "<div class='col-lg-6'>";
			applications_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			applications_pagination += "</div>";

			applications_pagination += "<div class='col-lg-6' style='text-align:right'>";
			applications_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			applications_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getApplicantsByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			applications_pagination += "</div>";
			
			applications_pagination += "</div>";
			
			applications_pagination += "<div class='row'>";
			
			applications_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			applications_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			applications_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			applications_pagination += "</div>";
			
			applications_pagination += "</div>";

		}	

	    $("#applicants-pagination").html(applications_pagination);
	}
	else {

		app_full_view_results += "<tr>";
		app_full_view_results += "<td colspan='9' align='center'>No records found</td>";
		app_full_view_results += "</tr>";

	}
	
	$("#applicants_results").append(app_full_view_results);
}

function exportApplicants() {
	var IndexStart         =   document.frmApplicantsParams.IndexStart.value = 0;
	var FromDate           =   document.getElementById('application_from_date').value;
	var ToDate             =   document.getElementById('application_to_date').value;
	var RequestID          =   document.getElementById('ddlRequisitions').value;
	var Organization       =   document.getElementById('MultiOrgID').value;
	var keyword            =   document.frmApplicantsParams.search_word.value;
	var to_sort            =   document.frmSortOptions.ddlToSort.value;
	var sort_type          =   document.frmSortOptions.ddlSortType.value;
	
	document.frmApplicantsParams.CurrentPage.value = 1;	
			
	var REQUEST_URL = irecruit_home + "services/getApplicants.php?FromDate="+FromDate+"&ToDate="+ToDate+"&RequestID="+RequestID+"&Organization="+Organization+"&RecordsLimit="+RecordsLimit+"&IndexStart="+IndexStart+"&Export=YES&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&PageSource=ApplicantsByReferralSource";
	
	document.frmApplicantsParams.action = REQUEST_URL;
	document.frmApplicantsParams.submit();
}

$(document).ready(function() {
	getApplicantsListByOptions();
});
</script>
