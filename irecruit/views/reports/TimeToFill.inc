<?php 
include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';

$reports_settings   =   G::Obj('Reports')->getReportsSettings($OrgID, "");
?>

<div id="page-wrapper">
    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
    <div class="row">
        <div class="col-lg-12" id="message_status"></div>
    </div>
    
    <div class="row">
    		<div class="col-lg-12">  
                <form name="frmTimeToFill" id="frmTimeToFill" method="post">
                    <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
                    <input type="hidden" name="IndexStart" id="IndexStart" value="0">
                    <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $Limit;?>">
                    <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
                    <?php
                    if($feature['MultiOrg'] == "Y") {
                        $where_info     =   array("OrgID = :OrgID");
                        $params_info    =   array(":OrgID"=>$OrgID);
                        $columns        =   "OrgID, MultiOrgID, OrganizationName";
                        $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
                        $orgs_list      =   $orgs_results['results'];
                        
                        echo 'Organizations List:';
                        echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
                        echo '<option value="">All</option>';
                        for($ol = 0; $ol < count($orgs_list); $ol++) {
                            if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
                                $selected = ' selected="selected"';
                            }
                            echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
                            unset($selected);
                        }
                        echo '</select>';
                    }
                    else {
                        echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
                    }
                    ?>
                    Status:
    			    <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline" onchange="getRequisitionsByStatus(this.value)">
    					<option value="A">All</option>
    					<option value="Y">Active</option>
    					<option value="N">Inactive</option>
    				</select>
    				<br><br>
                    Requisitions: 
                    <select name="ddlRequisitionsList" id="ddlRequisitionsList" class="form-control width-auto-inline" style="max-width:300px">
                      <option value="">Select</option>
                      <?php
                          if(is_array($requisitions_results)) {
                                for($i = 0; $i < $requisitions_results_count; $i++) {
                                	?><option value="<?php echo $requisitions_results[$i]['RequestID'];?>"><?php echo $requisitions_results[$i]['Title'] . " - ". $requisitions_results[$i]['RequisitionID'] . "/" . $requisitions_results[$i]['JobID'];?></option><?php
                                }
                          }
                      ?>
                    </select>
                    From: 
                    <input type="text" name="applicants_from_date" id="applicants_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
                    To: 
                    <input type="text" name="applicants_to_date" id="applicants_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
                    &nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-primary" name="btnRefineRequisitions" id="btnRefineRequisitions" onclick="getTimeToFill()" value="Refine">
                    <input type="button" class="btn btn-primary" name="btnExportApplicantHistory" id="btnExportApplicantHistory" onclick="exportRequisitions()" value="Export"><br><br>
                </form>

                <form id="frmTimeToFillSortOptions" name="frmTimeToFillSortOptions" method="post">
                    <input type="hidden" name="ddlToSort" id="ddlToSort" value="application_date">
                    <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
                </form>
    	 </div>
    </div>
    
	<div class="row">
		<div class="col-lg-12">
				<form name="frmRequisitionsSearchResults"
					id="frmRequisitionsSearchResults" method="post">
					<div class="table-responsive">
					<table border="0" cellspacing="0" cellpadding="5" width="100%"
						class="table table-striped table-bordered table-hover"
						id="data-time-to-fill-log">
						<thead>
                			<tr> 
                                <td><strong>Open Date</strong></td>
                                <td><strong>Expire Date</strong></td>
                                <td><strong>Final Stage</strong></td>
                                <td align="center"><strong>Days Open</strong></td>
                                <td align="center"><strong>Title</strong></td>
                                <td align="center"><strong>RequisitionID</strong></td>
                                <td align="center"><strong>JobID</strong></td>
                                <td align="center"><strong>EEO Code</strong></td>
                                <td align="center"><strong>First Name</strong></td>
                                <td align="center"><strong>Last Name</strong></td>
                                <td align="center"><strong>Application ID</strong></td>
                                <td align="center"><strong>Entry Date</strong></td>
                            	<td align="center"><strong>Date Hired</strong></td>
                            	<td align="center"><strong>Time to Hire</strong></td>
                            	<?php 
                            	if(isset($reports_settings['Column1Title'])
                                    && $reports_settings['Column1Title'] != "") {
                            	   ?><td align="center"><strong><?php echo $reports_settings['Column1Title'];?></strong></td><?php
                            	}
                            	if(isset($reports_settings['Column2Title'])
                            	   && $reports_settings['Column2Title'] != "") {
                            	    ?><td align="center"><strong><?php echo $reports_settings['Column2Title'];?></strong></td><?php
                            	}
                            	if(isset($reports_settings['Column3Title'])
                            	   && $reports_settings['Column3Title'] != "") {
                            	    ?><td align="center"><strong><?php echo $reports_settings['Column3Title'];?></strong></td><?php
                            	}                            	
                            	?>
                            </tr>
                        </thead>
                        
                        <tbody>
                                <?php
                                for($k = 0; $k < count($applicants_flow_log); $k++) {
                                    ?><tr><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['PostDate'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['ExpireDate'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['FinalStageDate'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['Open'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['JobTitle'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['RequisitionID'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['JobID'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['EEO'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['FirstName'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['LastName'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['ApplicationID'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['EntryDate'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['DateHired'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['TimeToHire'];?></td><?php
                                	if(isset($reports_settings['Column1Title'])
                                	   && $reports_settings['Column1Title'] != "") {
                                	    ?><td align="center"><?php echo $applicants_flow_log[$k]['Column1'];?></td><?php
	                            	}
	                            	if(isset($reports_settings['Column2Title'])
	                            	   && $reports_settings['Column2Title'] != "") {
	                            	    ?><td align="center"><?php echo $applicants_flow_log[$k]['Column2'];?></td><?php
	                            	}
	                            	if(isset($reports_settings['Column3Title'])
	                            	   && $reports_settings['Column3Title'] != "") {
	                            	    ?><td align="center"><?php echo $applicants_flow_log[$k]['Column3'];?></td><?php
	                            	}
                                	?></tr><?php
                                }
                                ?>
                        </tbody>
					</table>
					</div>
				</form>

		</div>
	</div>
	<div class="row">
        <div class="col-lg-12" id="time-to-fill-pagination">
        <?php
	        if($total_count > $Limit) {
               $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $Limit, $total_count, '', '');
               ?> 
    			<div class="row">
    			  	<div class="col-lg-6">
    			  		<strong>Per page:</strong> <?php echo $Limit;?>
    			  	</div>
    			  	<div class="col-lg-6" style="text-align:right">
    				  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
    				  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getTimeToFillByPageNumber(document.getElementById('current_page_number').value)">
    			  	</div>
    			</div>
    			  
    			<div class="row">
    				  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
    				  	<?php echo $left_pagination_info['previous'];?>
    				  </div>
    				  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
    				  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
    				  </div>
    				  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
    				  	<?php echo $left_pagination_info['next'];?>
    				  </div>
    			</div>
    			<?php
           }
        ?>
        </div>
    </div>
</div>

<script>
var column1title = 'false';
var column2title = 'false';
var column3title = 'false';
<?php
if(isset($reports_settings['Column1Title'])
   && $reports_settings['Column1Title'] != "") {
   ?>column1title = 'true';<?php
}
if(isset($reports_settings['Column2Title'])
   && $reports_settings['Column2Title'] != "") {
   ?>column2title = 'true';<?php
}
if(isset($reports_settings['Column3Title'])
   && $reports_settings['Column3Title'] != "") {
   ?>column3title = 'true';<?php
}
?>        
function getRequisitionsByStatus(RequisitionStatus) {
	var FilterOrganization	=	document.forms['frmTimeToFill'].ddlOrganizationsList.value;
	
    $('#ddlRequisitionsList').children('option:not(:first)').remove();

    var option_text		=	'';
    var option_value	=	'';
    var options_list    =   '';
    
	$.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?FilterOrganization="+FilterOrganization+"&RequisitionStatus="+RequisitionStatus+"&IsFinal=Y",
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#message_status").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		+= " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#message_status").html("");
			$("#ddlRequisitionsList").append(options_list);
    	}
	});
}
        
function sortTimeToFill(to_sort) {
	var FilterOrganization	=	document.forms['frmTimeToFill'].ddlOrganizationsList.value;
	var IndexStart     		=   document.frmTimeToFill.IndexStart.value;

    document.frmTimeToFillSortOptions.ddlToSort.value = to_sort;
    
	var sort_type  =   document.frmTimeToFillSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmTimeToFillSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmTimeToFillSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var from_date      	=   document.frmTimeToFill.applicants_from_date.value;
	var to_date        	=   document.frmTimeToFill.applicants_to_date.value;
    var RequestID      	=   document.getElementById('ddlRequisitionsList').value;
	var REQUEST_URL		=	irecruit_home + "reports/getTimeToFill.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&to_sort="+to_sort+"&sort_type="+sort_type+"&RequestID="+RequestID;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-time-to-fill-log").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='18' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-time-to-fill-log").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setTimeToFill(data);
    	}
	});
}
        		      
function getRecordsByPage(IndexStart) {
	document.frmTimeToFill.IndexStart.value = IndexStart;
	
	var from_date      		=   document.frmTimeToFill.applicants_from_date.value;
	var to_date        		=   document.frmTimeToFill.applicants_to_date.value;
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var Status         		=   document.getElementById('ddlActive').value;
	var FilterOrganization	=	document.forms['frmTimeToFill'].ddlOrganizationsList.value;
	var REQUEST_URL    		=   irecruit_home + "reports/getTimeToFill.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&RequestID="+RequestID+"&Status="+Status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-time-to-fill-log").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='18' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-time-to-fill-log").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setTimeToFill(data);
    	}
	});
	
}

function getTimeToFillByPageNumber(page_number) {
	
	var RecordsLimit = document.frmTimeToFill.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}


function setTimeToFill(data) {
    
	var RecordsLimit = document.frmTimeToFill.RecordsLimit.value;
	
	var app_prev         = data.previous;
	var app_next         = data.next;
	var total_pages      = data.total_pages;
	var current_page     = data.current_page;
	var total_count      = data.total_count;
	
	$("#data-time-to-fill-log").find("tr:gt(0)").remove();
	$("#time-to-fill-pagination").html("");

	var data_length;
	if(data['applicants_list'] == null) {
		data_length = 0;
	}
	else {
		data_length = data['applicants_list'].length;
	}
	
    var applicants_list = data['applicants_list'];
    var req_results = "";
    var requisitions_pagination = "";
    
	$("#data-time-to-fill-log").find("tr:gt(0)").remove();
	
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {
			
			var PostDate                 =	applicants_list[i].PostDate;
			var ExpireDate               =	applicants_list[i].ExpireDate;
			var FinalStageDate           =	applicants_list[i].FinalStageDate;
			var Open                     =	applicants_list[i].Open;
			var JobTitle                 =	applicants_list[i].JobTitle;
			var RequisitionID            =	applicants_list[i].RequisitionID;
			var JobID                    =	applicants_list[i].JobID;
			var EEO                      =	applicants_list[i].EEO;
			var FirstName                =	applicants_list[i].FirstName;
			var LastName                 =	applicants_list[i].LastName;
			var ApplicationID            =	applicants_list[i].ApplicationID;
			var EntryDate                =	applicants_list[i].EntryDate;
			var DateHired                =	applicants_list[i].DateHired;
			var TimeToHire               =	applicants_list[i].TimeToHire;
			var Column1                  =	applicants_list[i].Column1;
			var Column2                  =	applicants_list[i].Column2;
			var Column3                  =	applicants_list[i].Column3;
			
			if(PostDate == null) PostDate = '';
			if(ExpireDate == null) ExpireDate = '';
			if(FinalStageDate == null) FinalStageDate = '';
			if(Open == null) Open = '';
			if(JobTitle == null) JobTitle = '';
			if(RequisitionID == null) RequisitionID = '';
			if(JobID == null) JobID = '';
			if(EEO == null) EEO = '';
			if(FirstName == null) FirstName = '';
			if(LastName == null) LastName = '';
			if(ApplicationID == null) ApplicationID = '';
			if(EntryDate == null) EntryDate = '';
			if(DateHired == null) DateHired = '';
			if(TimeToHire == null) TimeToHire = '';
			if(Column1 == null) Column1 = '';
			if(Column2 == null) Column2 = '';
			if(Column3 == null) Column3 = '';
			
			
			req_results  = "<tr>";
			req_results += "<td align='left' valign='top'>"+PostDate+"</td>";
			req_results += "<td align='left' valign='top'>"+ExpireDate+"</td>";
			req_results += "<td align='left' valign='top'>"+FinalStageDate+"</td>";
			req_results += "<td align='center' valign='top'>"+Open+"</td>";
			req_results += "<td align='center' valign='top'>"+JobTitle+"</td>";
			req_results += "<td align='center' valign='top'>"+RequisitionID+"</td>";
			req_results += "<td align='center' valign='top'>"+JobID+"</td>";
			req_results += "<td align='center' valign='top'>"+EEO+"</td>";
			req_results += "<td align='center' valign='top'>"+FirstName+"</td>";
			req_results += "<td align='center' valign='top'>"+LastName+"</td>";
			req_results += "<td align='center' valign='top'>"+ApplicationID+"</td>";
			req_results += "<td align='center' valign='top'>"+EntryDate+"</td>";
			req_results += "<td align='center' valign='top'>"+DateHired+"</td>";
			req_results += "<td align='center' valign='top'>"+TimeToHire+"</td>";

			if(column1title == 'true') {
				req_results += "<td align='center' valign='top'>"+Column1+"</td>";
			}
			if(column2title == 'true') {
				req_results += "<td align='center' valign='top'>"+Column2+"</td>";
			}
			if(column3title == 'true') {
				req_results += "<td align='center' valign='top'>"+Column3+"</td>";
			}
			
			req_results += "</tr>";

			$("#data-time-to-fill-log").append(req_results);

		}

	    if(parseInt(total_count) > parseInt(RecordsLimit)) {
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div class='col-lg-6'>";
			requisitions_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			requisitions_pagination += "</div>";

			requisitions_pagination += "<div class='col-lg-6' style='text-align:right'>";
			requisitions_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			requisitions_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getTimeToFillByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			requisitions_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			requisitions_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";

		}

		$("#time-to-fill-pagination").html(requisitions_pagination);
		
	}
	else {

		req_results = "<tr>";
		req_results += "<td colspan='18' align='center'>No records found</td>";
		req_results += "</tr>";

		$("#data-time-to-fill-log").append(req_results);
	}
}

function exportRequisitions() {
	var FilterOrganization	=	document.forms['frmTimeToFill'].ddlOrganizationsList.value;
	var from_date      		=   document.frmTimeToFill.applicants_from_date.value;
	var to_date        		=   document.frmTimeToFill.applicants_to_date.value;
	var to_sort        		=   document.frmTimeToFillSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmTimeToFillSortOptions.ddlSortType.value;
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var Status         		=   document.getElementById('ddlActive').value;
		
	var REQUEST_URL    		=   irecruit_home + "reports/getTimeToFill.php?FilterOrganization="+FilterOrganization+"&Export=YES"+"&FromDate="+from_date+"&ToDate="+to_date+"&RequestID="+RequestID+"&to_sort="+to_sort+"&sort_type="+sort_type+"&Status="+Status;

	document.frmTimeToFill.action = REQUEST_URL;
	document.frmTimeToFill.submit();
}
		      
function getTimeToFill() {

	var FilterOrganization	=	document.forms['frmTimeToFill'].ddlOrganizationsList.value;
	var from_date      		=   document.frmTimeToFill.applicants_from_date.value;
	var to_date        		=   document.frmTimeToFill.applicants_to_date.value;
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var IndexStart     		=   document.frmTimeToFill.IndexStart.value;
	var Status         		=   document.getElementById('ddlActive').value;
	
	var REQUEST_URL			=	irecruit_home + "reports/getTimeToFill.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&RequestID="+RequestID+"&Status="+Status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#data-time-to-fill-log").find("tr:gt(0)").remove();
			req_results	 = "<tr>";
			req_results	+= "<td colspan='18' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			req_results	+= "</tr>";
			
			$("#data-time-to-fill-log").append(req_results);
		},
		success: function(data) {
			setTimeToFill(data);
    	}
	});
}
</script>
