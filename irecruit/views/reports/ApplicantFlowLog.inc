<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="page-wrapper">

    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
    
    <div class="row">
        <div class="col-lg-12" id="message_status"></div>
    </div>
    <div class="row">
		<div class="col-lg-12">  
            <form name="frmApplicantFlowLog" id="frmApplicantFlowLog" method="post">
                <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
                <input type="hidden" name="IndexStart" id="IndexStart" value="0">
                <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $user_preferences['ReportsSearchResultsLimit'];?>">
                <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
                <?php
                if($feature['MultiOrg'] == "Y") {
                    $where_info     =   array("OrgID = :OrgID");
                    $params_info    =   array(":OrgID"=>$OrgID);
                    $columns        =   "OrgID, MultiOrgID, OrganizationName";
                    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
                    $orgs_list      =   $orgs_results['results'];
                    
                    echo 'Organizations List:&nbsp;';
                    echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
                    echo '<option value="">All</option>';
                    for($ol = 0; $ol < count($orgs_list); $ol++) {
                        if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
                            $selected = ' selected="selected"';
                        }
                        echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
                        unset($selected);
                    }
                    echo '</select>&nbsp;&nbsp;';
                }
                else {
                    echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
                }
                
                echo 'Application Status:&nbsp;';
				echo '<select name="ddlApplicationProcessOrder" id="ddlApplicationProcessOrder" class="form-control width-auto-inline">';
				echo '<option value="">Select a Status</option>';
				//Set Condition For Applicant ProcessFlow
				$where      =   array("OrgID = :OrgID", "ProcessOrder != ''", "Active = 'Y'");
				//Bind the parameters
				$params     =   array(':OrgID'=>$OrgID);
				//Get Applicant Process Flow Information
				$results    =   G::Obj('Applicants')->getApplicantProcessFlowInfo(array('ProcessOrder', 'Description'), $where, 'ProcessOrder', array($params));
				
				if(is_array($results['results'])) {
				    foreach ($results['results'] as $row) {
				        
				        if ($ProcessOrder == $row ['ProcessOrder']) {
				            $selected = ' selected';
				        } else {
				            $selected = '';
				        }
				        
				        echo '<option value="' . $row ['ProcessOrder'] . '"' . $selected . '>' . $row ['Description'];
				    }
				}
				echo '</select>&nbsp;';
				?>
                &nbsp;&nbsp;Requisition Status:
			    <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline" onchange="getRequisitionsByStatus(this.value)">
					<option value="A">All</option>
					<option value="Y">Active</option>
					<option value="N">Inactive</option>
				</select>
				<br><br>
                Requisitions: 
                <select name="ddlRequisitionsList" id="ddlRequisitionsList" class="form-control width-auto-inline" style="max-width:300px">
                  <option value="">Select</option>
                  <?php
                      if(is_array($requisitions_results)) {
                            for($i = 0; $i < $requisitions_results_count; $i++) {
                            	?><option value="<?php echo $requisitions_results[$i]['RequestID'];?>"><?php echo $requisitions_results[$i]['Title'] . " - ". $requisitions_results[$i]['RequisitionID'] . "/" . $requisitions_results[$i]['JobID'];?></option><?php
                            }
                      }
                  ?>
                </select>
                &nbsp;&nbsp;EEO Classification:&nbsp;
                <select name="EEOCode" id="EEOCode" class="form-control width-auto-inline">
				<option value="">Select</option>
				<?php
				$results = $OrganizationsObj->getEEOClassificationsInfo ( "Code, Description", array(), 'ListOrder');
					
				if (is_array ( $results ['results'] )) {
				    foreach ( $results ['results'] as $row ) {
				        echo '<option value="' . $row ['Code'] . '">' . $row ['Description'] . '</option>';
				    }
				}
				?>	
				</select>
                <br><br>
                From: 
                <input type="text" name="applicants_from_date" id="applicants_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
                To: 
                <input type="text" name="applicants_to_date" id="applicants_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
                <input type="button" class="btn btn-primary" name="btnRefineRequisitions" id="btnRefineRequisitions" onclick="getApplicantFlowLog()" value="Refine">
                <input type="button" class="btn btn-primary" name="btnExportApplicantHistory" id="btnExportApplicantHistory" onclick="exportRequisitions()" value="Export"><br><br>
            </form>

            <form id="frmApplicantFlowSortOptions" name="frmApplicantFlowSortOptions" method="post">
                <input type="hidden" name="ddlToSort" id="ddlToSort" value="application_date">
                <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
            </form>
    	 </div>
		 
    </div>
	<div class="row">
		<div class="col-lg-12">

				<form name="frmRequisitionsSearchResults"
					id="frmRequisitionsSearchResults" method="post">
					<div class="table-responsive">
					<table border="0" cellspacing="0" cellpadding="5"
						class="table table-striped table-bordered table-hover"
						id="data-applicant-flow-log">
						<thead>
                			<tr> 
								<td><strong><a href="javascript:void(0)" onclick="sortApplicantFlowLog('application_id')">Application ID</a></strong></td>
                                <td><strong><a href="javascript:void(0)" onclick="sortApplicantFlowLog('applicant_name')">Applicant Name</a></strong></td>
                                <td><strong><a href="javascript:void(0)" onclick="sortApplicantFlowLog('req_title')">Job Title</a></strong></td>
                                <td><strong><a href="javascript:void(0)" onclick="sortApplicantFlowLog('eeo_code')">EEO</a></strong></td>
                                <td><strong><a href="javascript:void(0)" onclick="sortApplicantFlowLog('application_date')">Application Date</a></strong></td>
                                <td align="center"><strong>Black/African American</strong></td>
                                <td align="center"><strong>Hispanic</strong></td>
                                <td align="center"><strong>Asian/Pacific Islander</strong></td>
                                <td align="center"><strong>American Indians/Alaskan Natives</strong></td>
                                <td align="center"><strong>White(not Hispanic)</strong></td>
                                <td align="center"><strong>Two or More Races</strong></td>
                                <td align="center"><strong>Male</strong></td>
                                <td align="center"><strong>Female</strong></td>
                                <td align="center"><strong>Disabled Yes</strong></td>
                            	<td align="center"><strong>Disabled No</strong></td>
                            	<td align="center"><strong>Disabled Not Disclosed</strong></td>
                            	<td align="center"><strong>Veteran Identified</strong></td>
                            	<td align="center"><strong>Veteran Not Protected</strong></td>
                            	<td align="center"><strong>Veteran Not Disclosed</strong></td>
                            	<td align="center"><strong>Status</strong></td>
                            	<td align="center"><strong>Disposition Code</strong></td>
                            	<td align="center"><strong>Start Date</strong></td>
                            </tr>
                        </thead>
                        
                        <tbody>
                                <?php
                                for($k = 0; $k < count($applicants_flow_log); $k++) {
                                    ?><tr><?php
                                    ?><td><?php echo $applicants_flow_log[$k]['ApplicationID'];?></td><?php
                                	?><td><?php echo $applicants_flow_log[$k]['ApplicantSortName'];?></td><?php
                                	?><td><?php echo $applicants_flow_log[$k]['JobTitle'];?></td><?php
                                	?><td><?php echo $applicants_flow_log[$k]['EEO'];?></td><?php
                                	?><td><?php echo $applicants_flow_log[$k]['EntryDate'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['RaceBlack'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['Hispanic'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['RaceAsian'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['RaceIndian'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['RaceWhite'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['RaceTwo'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['Male'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['Female'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['DisabilityYes'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['DisabilityNo'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['DisabilityNonDisclose'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['VeteranIntentified'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['VeteranNotProtected'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['VeteranNonDisclose'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['ProcessOrder'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['DispositionCode'];?></td><?php
                                	?><td align="center"><?php echo $applicants_flow_log[$k]['StartDate'];?></td><?php
                                	?></tr><?php
                                }
                                
                        		echo "<tr>";
                        		echo  "<td valign='top' colspan='21' align='left'>Total Applicants Count: ".$count_list['ApplicantsCount']."</td>";
                        		echo  "</tr>";
                        
                        		echo  "<tr>";
                        		echo  "<td align='left' valign='top' colspan='5'>Total of all pages: </td>";
                        		echo  "<td align='center' valign='top'>".$count_list['RaceBlackCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['HispanicCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['RaceAsianCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['RaceIndianCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['RaceWhiteCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['RaceTwoCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['MaleCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['FemaleCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['DisabilityYesCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['DisabilityNoCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['DisabilityNonDiscloseCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['VeteranIntentifiedCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['VeteranNotProtectedCount']."</td>";
                        		echo  "<td align='center' valign='top'>".$count_list['VeteranNonDiscloseCount']."</td>";
                        		echo  "<td align='center' valign='top'>&nbsp;</td>";
                        		echo  "<td align='center' valign='top'>&nbsp;</td>";
                        		echo  "<td align='center' valign='top'>&nbsp;</td>";
                        		echo  "</tr>";
                                
                                ?>
                        </tbody>
					</table>
					</div>
				</form>

		</div>
	</div>
	<div class="row">
        <div class="col-lg-12" id="applicant-flow-log-pagination">
        <?php
	        if($total_count > $Limit) {
               $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $Limit, $total_count, '', '');
               ?> 
    			<div class="row">
    			  	<div class="col-lg-6">
    			  		<strong>Per page:</strong> <?php echo $Limit;?>
    			  	</div>
    			  	<div class="col-lg-6" style="text-align:right">
    				  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
    				  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getApplicantFlowLogByPageNumber(document.getElementById('current_page_number').value)">
    			  	</div>
    			</div>
    			  
    			<div class="row">
    				  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
    				  	<?php echo $left_pagination_info['previous'];?>
    				  </div>
    				  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
    				  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
    				  </div>
    				  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
    				  	<?php echo $left_pagination_info['next'];?>
    				  </div>
    			</div>
    			<?php
           }
        ?>
        </div>
    </div>
</div>

<script src="<?php echo IRECRUIT_HOME;?>js/applicant-flow-log-report.js" type="text/javascript"></script>
