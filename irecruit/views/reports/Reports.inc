<?php
$multi_org_reporting_info   =   G::Obj('Reports')->getMultiOrgReporting();

Reports::$ORGID        =   $multi_org_reporting_info['OrgID'];
Reports::$MULTIORGID   =   $multi_org_reporting_info['MultiOrgID'];
?>
<div id="page-wrapper">
    <?php
    include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
    ?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php include_once IRECRUIT_VIEWS . "reports/ReportsPageContent.inc";?>
					<!-- /.row (nested) -->
				</div>
			</div>
			<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
</div>
