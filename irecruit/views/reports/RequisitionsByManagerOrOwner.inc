<?php include_once IRECRUIT_VIEWS . 'reports/ReportsCustomCss.inc';?>

<div id="page-wrapper">
    <?php 
    include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
    ?>
    <div class="row">
        <div class="col-lg-12">  
              <form name="frmRequisitionsByManagerOrOwner" id="frmRequisitionsByManagerOrOwner" method="post">
                <input type="hidden" name="RecordsCount" id="RecordsCount" value="0">
                <input type="hidden" name="IndexStart" id="IndexStart" value="0">
                <input type="hidden" name="RecordsLimit" id="RecordsLimit" value="<?php echo $user_preferences['ReportsSearchResultsLimit'];?>">
                <input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
                <?php
                if($feature['MultiOrg'] == "Y") {
					$where_info     =   array("OrgID = :OrgID");
                    $params_info    =   array(":OrgID"=>$OrgID);
                    $columns        =   "OrgID, MultiOrgID, OrganizationName";
                    $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
                    $orgs_list      =   $orgs_results['results'];
                    ?>
                    Organizations List: 
                    <select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">
                    	<option value="">All</option>
                    	<?php
                    	for($ol = 0; $ol < count($orgs_list); $ol++) {
                    	    ?><option value="<?php echo $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'];?>"><?php echo $orgs_list[$ol]['OrganizationName']?></option><?php
                    	}
                    	?>	
                    </select>
                    <?php
                }
                else {
                    ?><input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value=""><?php
                }
                ?>
    		    &nbsp;Status: <select name="ddlActive" id="ddlActive" class="form-control width-auto-inline">
				<option value="A" <?php if($Active == "A") echo 'selected="selected"';?>>All</option>
				<option value="Y" <?php if($Active == "Y") echo 'selected="selected"';?>>Active</option>
				<option value="N" <?php if($Active == "N") echo 'selected="selected"';?>>Inactive</option>
				<?php
				if ($feature ['RequisitionApproval'] == "Y") {
					?>
					<option value="R" <?php if($Active == "R") echo 'selected="selected"';?>>Pending</option>
					<option value="NA" <?php if($Active == "NA") echo 'selected="selected"';?>>Not Approved</option>
					<?php
				}
				?>
				</select>
				<br><br>
    		    Requisition Owners:
    		    <select name="ddlRequisitionOwners" id="ddlRequisitionOwners" class="form-control width-auto-inline">
    		      <option value="">Select</option>
    		      <?php
    		          if(is_array($requisition_owners_list)) {
        		          	foreach($requisition_owners_list as $req_owner_user) {
                                if($req_owner_user['Owner'] != "") {
                                    $user_details_info = $IrecruitUsersObj->getUserInfoByUserID($req_owner_user['Owner'], "FirstName, LastName");
                                    if($user_details_info['LastName'] != ""
                                       || $user_details_info['FirstName']) {
                                        ?><option value="<?php echo $req_owner_user['Owner'];?>">
                                        <?php echo $user_details_info['LastName']." ".$user_details_info['FirstName'];?>
                                        </option><?php
                                    }
                                	
                                }
                            }
    		          }
    		      ?>
    		  </select>
              &nbsp;Requisition Managers:
    		  <select name="ddlRequisitionManagers" id="ddlRequisitionManagers" class="form-control width-auto-inline">
    		      <option value="">Select</option>
    		      <?php
    		          if(is_array($requisition_managers_list)) {
        		          	foreach($requisition_managers_list as $req_manager_info) {
                                if($users_list[$req_manager_info['UserID']]['FirstName'] != ""
                                    || $users_list[$req_manager_info['UserID']]['LastName'] != "") {
                                    ?><option value="<?php echo $req_manager_info['UserID'];?>"><?php echo $users_list[$req_manager_info['UserID']]['FirstName'] . " " . $users_list[$req_manager_info['UserID']]['LastName'];?></option><?php
                                }
                            }
    		          }
    		      ?>
    		  </select>
    		  <br><br>
    		  From: 
			  <input type="text" name="requisitions_from_date" id="requisitions_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))); ?>">
			  &nbsp;To: 
			  <input type="text" name="requisitions_to_date" id="requisitions_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
			  &nbsp;Search Words: <input type="text" name="search_word" id="search_word" class="form-control width-auto-inline" placeholder="Title,RequisitionID,JobID">
    		  <br><br>
			  
			  <?php 
                //Set where condition
                $where = array("OrgID = :OrgID");
                //Set parameters
                $params = array(":OrgID"=>$OrgID);
                //Get Request Reasons Information
                $results = $RequisitionsObj->getRequisitionStageInfo("RequisitionStage", "Code, Description, IsFinal", $where, "Description", array($params));
                
                echo 'Requisition Stage: <select name="ddlRequisitionStage" id="ddlRequisitionStage" class="form-control width-auto-inline">';
                echo '<option value="">Select</option>';
                
                if(is_array($results['results'])) {
                    foreach($results['results'] as $row) {
                        $code = $row ['Code'];
                        $description = $row ['Description'];
                
                        echo '<option value="' . $code . '">' . $description . '</option>';
                    }
                }
                
                echo '</select>';
                ?> 
              <input type="hidden" name="txtRequisitionStageDate" id="txtRequisitionStageDate" class="form-control width-auto-inline">

			  &nbsp;&nbsp;&nbsp;<input type="button" class="btn btn-primary" name="btnRefineRequisitions" id="btnRefineRequisitions" onclick="getRequisitionsByOwnerOrManager()" value="Refine">
    		  
    		  <input type="button" class="btn btn-primary" name="btnExportApplicantHistory" id="btnExportApplicantHistory" onclick="exportRequisitions()" value="Export"><br><br>
    		  </form>

              <form id="frmRequisitionsSortOptions" name="frmRequisitionsSortOptions" method="post">
                <input type="hidden" name="ddlToSort" id="ddlToSort" value="date_opened">
                <input type="hidden" name="ddlSortType" id="ddlSortType" value="DESC">
              </form>
    	 </div>
    </div>
	<div class="row">
		<div class="col-lg-12">

				<form name="frmRequisitionsSearchResults"
					id="frmRequisitionsSearchResults" method="post">
					<table border="0" cellspacing="0" cellpadding="5" width="100%"
						class="table table-striped table-bordered table-hover"
						id="data-requisitions-by-owner-manager">
						<thead>
            			<tr>
            			    <?php 
            			    if($feature['MultiOrg'] == 'Y') {
            			    	?><td><a href="javascript:void(0)" onclick="sortRequisitions('org_title')"><strong>Organization Title</strong></a></td><?php
            			    }
            			    else {
            			    	?><td>&nbsp;</td><?php
            			    }
            			    ?>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('req_title')"><strong>Title</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('req_id')"><strong>RequisitionID</strong></a> </td>
			    <td><a href="javascript:void(0)" onclick="sortRequisitions('job_id')"><strong>JobID</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('status')"><strong>Status</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('requisition_stage')"><strong>Requisition Stage</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('requisition_stage')"><strong>Post Options</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('requisition_owner')"><strong>Owner</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('requisition_manager')"><strong>Managers</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('date_opened')"><strong>Date<br>Opened</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('expire_date')"><strong>Expire Date</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('days_open')"><strong>Days<br>Open</strong></a></td>
                            <td><a href="javascript:void(0)" onclick="sortRequisitions('applicants_count')"><strong>Applicants<br>Count</strong></a></td>
                        </tr>
                        </thead>
                        
                        <tbody>
                            <tr> 
                                <td colspan="12"></td>
                            </tr>
                        </tbody>
					</table>
				</form>

		</div>
	</div>
	<div class="row">
        <div class="col-lg-12" id="requisitions-pagination">
        <?php
	        if($total_count > $Limit) {
               $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $Limit, $total_count, '', '');
               ?> 
    			<div class="row">
    			  	<div class="col-lg-6">
    			  		<strong>Per page:</strong> <?php echo $Limit;?>
    			  	</div>
    			  	<div class="col-lg-6" style="text-align:right">
    				  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
    				  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getRequisitionsByPageNumber(document.getElementById('current_page_number').value)">
    			  	</div>
    			</div>
    			  
    			<div class="row">
    				  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
    				  	<?php echo $left_pagination_info['previous'];?>
    				  </div>
    				  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
    				  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
    				  </div>
    				  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
    				  	<?php echo $left_pagination_info['next'];?>
    				  </div>
    			</div>
    			<?php
           }
        ?>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
	getRequisitionsByOwnerOrManager();
});

function sortRequisitions(to_sort) {
	var IndexStart     =   document.frmRequisitionsByManagerOrOwner.IndexStart.value;

    document.frmRequisitionsSortOptions.ddlToSort.value = to_sort;
	
	var sort_type  =   document.frmRequisitionsSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmRequisitionsSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmRequisitionsSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var req_owner      =   document.getElementById('ddlRequisitionOwners').value;
	var req_manager    =   document.getElementById('ddlRequisitionManagers').value;
	var from_date      =   document.frmRequisitionsByManagerOrOwner.requisitions_from_date.value;
	var to_date        =   document.frmRequisitionsByManagerOrOwner.requisitions_to_date.value;
	var keyword        =   document.frmRequisitionsByManagerOrOwner.search_word.value;
	var Active         =   document.getElementById('ddlActive').value;
	var FilterOrganization = document.forms['frmRequisitionsByManagerOrOwner'].ddlOrganizationsList.value;
	
	var RequisitionStage  =   document.getElementById('ddlRequisitionStage').value;
	var RequisitionStageDate  =   document.getElementById('txtRequisitionStageDate').value;
	
	var REQUEST_URL = irecruit_home + "reports/getRequisitionsByOwnerOrManager.php?FilterOrganization="+FilterOrganization+"&RequisitionManager="+req_manager+"&RequisitionOwner="+req_owner+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword+"&RequisitionStage="+RequisitionStage+"&RequisitionStageDate="+RequisitionStageDate+"&Active="+Active;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-requisitions-by-owner-manager").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-requisitions-by-owner-manager").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
}
        		      
function getRecordsByPage(IndexStart) {
	document.frmRequisitionsByManagerOrOwner.IndexStart.value = IndexStart;
	
	var req_owner      =   document.getElementById('ddlRequisitionOwners').value;
	var req_manager    =   document.getElementById('ddlRequisitionManagers').value;
	var to_sort        =   document.frmRequisitionsSortOptions.ddlToSort.value;
	var from_date      =   document.frmRequisitionsByManagerOrOwner.requisitions_from_date.value;
	var to_date        =   document.frmRequisitionsByManagerOrOwner.requisitions_to_date.value;
	var sort_type      =   document.frmRequisitionsSortOptions.ddlSortType.value;
	var keyword        =   document.frmRequisitionsByManagerOrOwner.search_word.value;
	var Active         =   document.getElementById('ddlActive').value;
	var FilterOrganization = document.forms['frmRequisitionsByManagerOrOwner'].ddlOrganizationsList.value;
	
	var RequisitionStage  =   document.getElementById('ddlRequisitionStage').value;
	var RequisitionStageDate  =   document.getElementById('txtRequisitionStageDate').value;
	
	var REQUEST_URL = irecruit_home + "reports/getRequisitionsByOwnerOrManager.php?FilterOrganization="+FilterOrganization+"&RequisitionManager="+req_manager+"&RequisitionOwner="+req_owner+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword+"&RequisitionStage="+RequisitionStage+"&RequisitionStageDate="+RequisitionStageDate+"&Active="+Active
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-requisitions-by-owner-manager").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='8' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-requisitions-by-owner-manager").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
	
}

function getRequisitionsByPageNumber(page_number) {
	
	var RecordsLimit = document.frmRequisitionsByManagerOrOwner.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}


function setRequisitionsList(data) {
	var RecordsLimit           =   document.frmRequisitionsByManagerOrOwner.RecordsLimit.value;
	
	var app_prev               =   data.previous;
	var app_next               =   data.next;
	var total_pages            =   data.total_pages;
	var current_page           =   data.current_page;
	var requisitions_count     =   data.requisitions_count;
	var feature_multi_org_id   =   data.feature_multi_org_id;
	
	$("#data-requisitions-by-owner-manager").find("tr:gt(0)").remove();
	$("#requisitions-pagination").html("");

	var data_length;
	if(data['requisitions_list'] == null) {
		data_length = 0;
	}
	else {
		data_length = data['requisitions_list'].length;
	}
	
    var requisitions_list = data['requisitions_list'];
    var req_results = "";
    var requisitions_pagination = "";
    
	$("#data-requisitions-by-owner-manager").find("tr:gt(0)").remove();

	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {
			
			var OrganizationTitle            =	requisitions_list[i].OrganizationTitle;
			var Title                        =	requisitions_list[i].Title;
			var RequisitionOwnersName        =	requisitions_list[i].RequisitionOwnersName;
			var RequisitionManagersNames     =	requisitions_list[i].RequisitionManagersNames;
			var ReqID                        =	requisitions_list[i].RequisitionID;
			var JobID                        =	requisitions_list[i].JobID;
			var PostDate                     =	requisitions_list[i].PostDate;
			var ExpireDate                   =	requisitions_list[i].ExpireDate;
			var DaysOpen                     =	requisitions_list[i].Open;
			var ApplicantsCount              =	requisitions_list[i].ApplicantsCount;
			var RequisitionStage             =	requisitions_list[i].RequisitionStage;
			var PostOptions              	 =	requisitions_list[i].PresentOn;

             var strcnt = PostOptions.length
   
             if(strcnt >=15){
                 var  sptstr = PostOptions.split("AND");

                 var toTitleCase1 =  sptstr[0].replace(/(^\w|\s\w)(\S*)/g, (_,m1,m2) => m1.toUpperCase()+m2.toLowerCase())
                 var toTitleCase2 =  sptstr[1].replace(/(^\w|\s\w)(\S*)/g, (_,m1,m2) => m1.toUpperCase()+m2.toLowerCase())

                 var PresentOn = toTitleCase1 +" and "+toTitleCase2;

               }

         if(strcnt < 14){
                   var  sptstr = PostOptions.split("ONLY");
                   var toTitleCase1 =  sptstr[0].replace(/(^\w|\s\w)(\S*)/g, (_,m1,m2) => m1.toUpperCase()+m2.toLowerCase())
                   var PresentOn = toTitleCase1 +" Only";

               }


			var StatusActive                 =  '';
            if(requisitions_list[i].Active == "Y") StatusActive = "Active";
            else if(requisitions_list[i].Active == "N") StatusActive = "In-Active";
            else if(requisitions_list[i].Active == "R") StatusActive = "Pending";
			
			if(Title == null || typeof(Title) == 'undefined') Title = '';
			if(ReqID == null || typeof(ReqID) == 'undefined') ReqID = '';
			if(JobID == null || typeof(JobID) == 'undefined') JobID = '';
			if(PostDate == null || typeof(PostDate) == 'undefined') PostDate = '';
			if(ExpireDate == null || typeof(ExpireDate) == 'undefined') ExpireDate = '';
			if(DaysOpen == null || typeof(DaysOpen) == 'undefined') DaysOpen = '';
			if(ApplicantsCount == null || typeof(ApplicantsCount) == 'undefined') ApplicantsCount = '';
			
			req_results  = "<tr>";
			if(feature_multi_org_id == 'Y') {
				req_results += "<td valign='top'>"+OrganizationTitle+"</td>";
			}
			else {
				req_results += "<td valign='top'></td>";
			}
			req_results += "<td valign='top'>"+Title+"</td>";
			req_results += "<td valign='top'>"+ReqID+"</td>";
			req_results += "<td valign='top'>"+JobID+"</td>";
			req_results += "<td valign='top'>"+StatusActive+"</td>";
			req_results += "<td valign='top'>"+RequisitionStage+"</td>";
			req_results += "<td valign='top'>"+PresentOn+"</td>";
			req_results += "<td valign='top'>"+RequisitionOwnersName+"</td>";
			req_results += "<td valign='top'>"+RequisitionManagersNames+"</td>";
			req_results += "<td valign='top'>"+PostDate+"</td>";
			req_results += "<td valign='top'>"+ExpireDate+"</td>";
			req_results += "<td valign='top'>"+DaysOpen+"</td>";
			req_results += "<td valign='top'>"+ApplicantsCount+"</td>";
			req_results += "</tr>";

			$("#data-requisitions-by-owner-manager").append(req_results);

		}


	    if(parseInt(requisitions_count) > parseInt(RecordsLimit)) {
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div class='col-lg-6'>";
			requisitions_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			requisitions_pagination += "</div>";

			requisitions_pagination += "<div class='col-lg-6' style='text-align:right'>";
			requisitions_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			requisitions_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getRequisitionsByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			requisitions_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			requisitions_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";

		}	

		$("#requisitions-pagination").html(requisitions_pagination);
		
	}
	else {

		req_results = "<tr>";
		req_results += "<td colspan='6' align='center'>No records found</td>";
		req_results += "</tr>";

		$("#data-requisitions-by-owner-manager").append(req_results);
	}

}

function exportRequisitions() {
	var req_owner      =   document.getElementById('ddlRequisitionOwners').value;
	var req_manager    =   document.getElementById('ddlRequisitionManagers').value;
	var from_date      =   document.frmRequisitionsByManagerOrOwner.requisitions_from_date.value;
	var to_date        =   document.frmRequisitionsByManagerOrOwner.requisitions_to_date.value;
	var keyword        =   document.frmRequisitionsByManagerOrOwner.search_word.value;
	var to_sort        =   document.frmRequisitionsSortOptions.ddlToSort.value;
	var sort_type      =   document.frmRequisitionsSortOptions.ddlSortType.value;
	var Active         =   document.getElementById('ddlActive').value;
	var FilterOrganization = document.forms['frmRequisitionsByManagerOrOwner'].ddlOrganizationsList.value;
	
	var RequisitionStage  =   document.getElementById('ddlRequisitionStage').value;
	var PostOptions =   document.getElementById('ddlRequisitionStage').value;
	var RequisitionStageDate  =   document.getElementById('txtRequisitionStageDate').value;
	
	var REQUEST_URL    =   irecruit_home + "reports/getRequisitionsByOwnerOrManager.php?FilterOrganization="+FilterOrganization+"&RequisitionManager="+req_manager+"&RequisitionOwner="+req_owner+"&Export=YES"+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&RequisitionStage="+RequisitionStage+"&RequisitionStageDate="+RequisitionStageDate+"&Active="+Active

	document.frmRequisitionsByManagerOrOwner.action = REQUEST_URL;
	document.frmRequisitionsByManagerOrOwner.submit();
}
		      
function getRequisitionsByOwnerOrManager() {
	
	var req_owner      =   document.getElementById('ddlRequisitionOwners').value;
	var req_manager    =   document.getElementById('ddlRequisitionManagers').value;
	var to_sort        =   document.frmRequisitionsSortOptions.ddlToSort.value;
	var sort_type      =   document.frmRequisitionsSortOptions.ddlSortType.value;
	var from_date      =   document.frmRequisitionsByManagerOrOwner.requisitions_from_date.value;
	var to_date        =   document.frmRequisitionsByManagerOrOwner.requisitions_to_date.value;
	var IndexStart     =   document.frmRequisitionsByManagerOrOwner.IndexStart.value = 0;
	var keyword        =   document.frmRequisitionsByManagerOrOwner.search_word.value;
	var Active         =   document.getElementById('ddlActive').value;
	var FilterOrganization = document.forms['frmRequisitionsByManagerOrOwner'].ddlOrganizationsList.value;
	
	var RequisitionStage  =   document.getElementById('ddlRequisitionStage').value;
	var PostOptions =   document.getElementById('ddlRequisitionStage').value;
	var RequisitionStageDate  =   document.getElementById('txtRequisitionStageDate').value;
	
	var REQUEST_URL = irecruit_home + "reports/getRequisitionsByOwnerOrManager.php?FilterOrganization="+FilterOrganization+"&RequisitionManager="+req_manager+"&RequisitionOwner="+req_owner+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+from_date+"&ToDate="+to_date+"&Keyword="+keyword+"&RequisitionStage="+RequisitionStage+"&RequisitionStageDate="+RequisitionStageDate+"&Active="+Active
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#data-requisitions-by-owner-manager").find("tr:gt(0)").remove();
			req_results	 = "<tr>";
			req_results	+= "<td colspan='12' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			req_results	+= "</tr>";
			
			$("#data-requisitions-by-owner-manager").append(req_results);
		},
		success: function(data) {
			setRequisitionsList(data);
    	}
	});
}
</script>
