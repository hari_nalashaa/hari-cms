<?php 
$display = 1;

if (($_REQUEST['override'] == "Y") && ($_REQUEST['process'] == "Y")) {

	if ($_REQUEST['Active'] == "N") {
		$active = "Y";
	}
	if ($_REQUEST['Active'] == "Y") {
		$active = "N";
	}

	//set information
	$set_info = array("Active = :Active");
	//set where condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":Active"=>$active, ":RequestID"=>$_REQUEST['RequestID']);
	//Update Requisitions Information
	$RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where, array($params));

	$display = 0;
} // end process

if ($feature ['RequisitionApproval'] != "Y") {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

echo '<p><b>Requisition Override</b></p>';

//set where condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
//Get Requisition Information
$results = $RequisitionsObj->getRequisitionInformation("Title, Active", $where, "", "", array($params));
list ( $title, $active ) = array_values($results['results'][0]);

$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $_REQUEST['RequestID'] ) . "</b>";
echo "&nbsp;&nbsp;&nbsp;";
echo "Title: <b>" . htmlspecialchars($title) . "</b><br><br>";

if ($display == 1) {

	if ($active == "Y") {
		echo 'This action will <b><u>deactivate</u></b> this requisition before its expiration date.<br><br>';
	} else {
		echo 'This action will <b><u>ACTIVATE</u></b> this requisition.<br><br>';
	}
	?>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
		Do you want to change this setting. 
		<select name="override">
			<option value="Y">Yes</option>
			<option value="N">No</option>
		</select><br> <br> 
		<input type="hidden" name="process" value="Y"> 
		<input type="hidden" name="Active" value="<?php echo $active?>"> 
		<input type="hidden" name="RequestID" value="<?php echo htmlspecialchars($_REQUEST['RequestID']);?>">
		<center> <input type="submit" value="Enter"> </center>
	</form>
	<?php
} else { // else display
	
	echo '<br><center>This requisition has been updated.</center>';
} // end display

echo '<br><br><p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
?>