<?php 
include IRECRUIT_DIR . 'request/EmailLinks.inc';

echo '<div style="margin-top:45px;padding-left:10px">';
echo '<p><b>Requisition Approval</b></p>';

// Set Tables
$table_name =	"EmailApprovers EA, RequestApprovers RA, Requisitions R";
// Set Columns
$columns    =	"RA.OrgID, RA.RequestID, RA.AuthorityLevel, RA.DateApproval, RA.EmailAddress, EA.FirstName, EA.LastName, R.Approved, R.ApprovalLevelRequired";
// Set parameters
$params		=	array(
					":OrgID"          =>  $OrgID,
					":RequestID"      =>  $RequestID,
					":EmailAddress"   =>  $EmailAddress
				);
// Set where condition
$where = array (
		"EA.OrgID         =   RA.OrgID",
		"R.OrgID          =   RA.OrgID",
		"RA.RequestID     =   R.RequestID",
		"EA.EmailAddress  =   RA.EmailAddress",
		"EA.OrgID         =   :OrgID",
		"RA.RequestID     =   :RequestID",
		"RA.EmailAddress  =   :EmailAddress",
		"RA.DateApproval  =   '0000-00-00 00:00:00'",
		"R.Approved       =   'N'"
);

// Get Approvers Information
$results    =   $RequestObj->getApproversInfo ( $table_name, $columns, $where, "", array ($params) );
$JA         =   $results ['results'] [0];
$hit        =   $results ['count'];

// if criteria is not in db than exit
if ($hit < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

// for approve condition
if ($_REQUEST['process'] == "Approve") {

	// Set where condition
	$where = array (
			"OrgID           =   :OrgID",
			"RequestID       =   :RequestID",
			"EmailAddress    =   :EmailAddress"
	);
	// Set parameters
	$params = array (
			":OrgID"         =>  $OrgID,
			":RequestID"     =>  $RequestID,
			":EmailAddress"  =>  $EmailAddress
	);
	// Get RequestApprovers Information
	$results = $RequestObj->getRequestApproversInfo ( "AuthorityLevel", $where, "", array ($params) );

	list ( $authlevel ) = array_values ( $results ['results'] [0] );

	$authlevel ++;

	$email = "";

	// Set condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID",
			"AuthorityLevel = :AuthorityLevel"
	);
	// set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":AuthorityLevel" => $authlevel
	);
	
	// Get Request ApproversInfo
	$results = $RequestObj->getRequestApproversInfo ( "EmailAddress", $where, "", array (
			$params
	) );

	if(is_array($results ['results'] [0])) list ( $email ) = array_values ( $results ['results'] [0] );

	// Set information
	$set_info = array (
			"DateApproval = NOW()"
	);
	// Set where condition
	$where = array (
			"OrgID  = :OrgID",
			"RequestID = :RequestID",
			"EmailAddress = :EmailAddress"
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":EmailAddress" => $EmailAddress
	);
	// Update RequestApprovers
	$RequestObj->updRequestApprovers ( $set_info, $where, array (
			$params
	) );

	$USERID = 'Approver';
	include IRECRUIT_DIR . 'request/UpdateHistory.inc';
	$Comments = 'Approved by: ' . $EmailAddress;
	$Comments .= "<br>Approval Level: " . $JA ['AuthorityLevel'];
	if ($Comment) {
		$Comments .= "<br>Comment: " . $Comment;
	}
	updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );

	// Set Information
	$set_info = array (
			"ApprovalLevel = :ApprovalLevel",
			"LastModified = NOW()"
	);
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID"
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":ApprovalLevel" => $JA ['AuthorityLevel']
	);

	// Update Requisition
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array (
			$params
	) );

	if ($email != "") {
		sendApproveLink ( $OrgID, $RequestID, $email );
	}

	// Set Information
	$set_info = array (
			"Approved = 'Y'",
			"Active = 'Y'"
	);
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID",
			"ApprovalLevelRequired = :ApprovalLevelRequired"
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":ApprovalLevelRequired" => $JA ['AuthorityLevel']
	);

	// Update Requisition
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array (
			$params
	) );

	$message = "Approval Complete\\n\\nThank You.";
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('" . $message . "')";
	echo '</script>';

	echo 'Approval task complete.<br><br><br><br>';

	// for reject condition
} else if ($_REQUEST['process'] == "Reject") {

	$USERID = 'Approver';
	include IRECRUIT_DIR . 'request/UpdateHistory.inc';
	$Comments = 'This request was rejected for the following reason.';
	$Comments .= '<br>Approver: ' . $EmailAddress;
	if ($Comment) {
		$Comments .= "<br>Comment: " . $Comment;
	}
	updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );

	$message = "Approval rejected with comments\\n\\nThank You.";
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('" . $message . "')";
	echo '</script>';

	echo 'Approval task complete.<br><br><br><br>';

	// for deny condition
} else if ($_REQUEST['process'] == "Deny") {

	// Set Information
	$set_info = array (
			"DateApproval = NOW()"
	);
	// Set where condition
	$where = array (
			"OrgID  = :OrgID",
			"RequestID = :RequestID",
			"EmailAddress = :EmailAddress"
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":EmailAddress" => $EmailAddress
	);
	// Update Request Approvers
	$RequestObj->updRequestApprovers ( $set_info, $where, array (
			$params
	) );

	$USERID = 'Approver';
	include IRECRUIT_DIR . 'request/UpdateHistory.inc';
	$Comments = 'This request was denied for the following reason.';
	$Comments .= '<br>Approver: ' . $EmailAddress;
	if ($Comment) {
		$Comments .= "<br>Comment: " . $Comment;
	}
	updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );

	// Set Information
	$set_info = array (
			"ApprovalLevel = :ApprovalLevel",
			"LastModified = NOW()"
	);
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID"
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":ApprovalLevel" => $JA ['AuthorityLevel']
	);

	// Update Requisition
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array (
			$params
	) );

	// Set Information
	$set_info = array ("Approved = 'D'", "Active = 'N'");
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID",
			"ApprovalLevelRequired = :ApprovalLevelRequired"
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID,
			":ApprovalLevelRequired" => $JA ['AuthorityLevel']
	);

	// Update Requisition
	$RequisitionsObj->updRequisitionsInfo ( 'Requisitions', $set_info, $where, array (
			$params
	) );

	$message = "Approval Denied\\n\\nThank You.";
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('" . $message . "')";
	echo '</script>';

	echo 'Approval task complete.<br><br><br><br>';
} else { // if not process

	include IRECRUIT_DIR . 'request/ViewRequest.inc';
	echo displayRequestInfo ( $OrgID, $RequestID, $EmailAddress, $EmailAddress );
}

echo '</div>';
?>