<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div id="del_process_message">
						<?php 
						if(isset($_GET['msg']) && $_GET['msg'] == 'suc_req_app') {
							echo "<span style='color:#428bca;font-weight:bold;'>Request Completed, please assign approver(s):</span>"; 
						}
						?>
					</div>
					<?php
					echo '<p><b>Requisition Requests</b></p>';
					if ($permit ['Requisitions_Edit'] < 1) {
						die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
					}
					if ($feature ['RequisitionApproval'] != "Y") {
						die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
					}
					
					echo '<table border="0" cellspacing="0" cellpadding="5" class="table table-striped table-bordered table-hover" width="100%">' . "\n";
					
					$ii = 0;
					
					// set where conditon
					$where     =   array ("OrgID = :OrgID", "Approved = 'N'", "Active != 'R'");
					// set parameters
					$params    =   array (":OrgID"=>$OrgID);
					$columns   =   "*, date_format(DateEntered,'%m/%d/%Y') DE, date_format(LastModified,'%m/%d/%Y') LM";
					$resultsR  =   $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", "DateEntered DESC", array ($params) );
					
					if (is_array ( $resultsR ['results'] )) {
						foreach ( $resultsR ['results'] as $RR ) {
							$ii ++;
							
							$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RR ['RequestID']);
							echo '<tr>';
							
							echo '<td valign="top" width="220">';
							if ($feature ['MultiOrg'] == 'Y') {
								echo $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RR ['RequestID'] ) . "<br>";
							}
							echo 'Entered: ' . $RR ['DE'] . '<br>Last Modified:' . $RR ['LM'] . '<br>Title: <span id="request-title-'.$RR ['RequestID'].'">' . $RR ['Title'] . '</span>';
							echo '</td>';
							
							
							echo '<td valign="top" align="center" width="60">';
							echo '<a href="' . IRECRUIT_HOME . 'request/assign.php?RequestID=' . $RR ['RequestID'] . '&action=view">';
							echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px 0px 0px;">';
							echo '</a>';
							echo '<br><b style="font-size:8pt;">View<br>Request</b>';
							echo '</td>';
							
							echo '<td valign="top" align="center" width="60">';
							echo '<a href="'.IRECRUIT_HOME.'request/processRequest.php?RequestID='.$RR ['RequestID'].'&action=edit&page=needsapproval" style="color:black">';
							echo '<img src="'.IRECRUIT_HOME.'images/icons/pencil.png" title="Edit"><br>Edit';
							echo '</a>';
							echo '</td>';
							
							if (($RR ['RequisitionID'] != '') || ($RR ['JobID'] != '')) {
								
								//set where condition
								$where = array("OrgID = :OrgID", "RequestID = :RequestID", "AuthorityLevel = :AuthorityLevel");
								//set parameters
								$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RR ['RequestID'], ":AuthorityLevel"=>$RR ['ApprovalLevelRequired']);
								//get Request approvers information
								$resultsIN = $RequestObj->getRequestApproversInfo("*", $where, "", array($params));

								$requests_without_approvers = $resultsIN['count'];
								
								$assign_approver_txt = '<td valign="top" align="center" width="60"><a href="' . IRECRUIT_HOME . 'request/setApprovalLevels.php?RequestID=' . $RR ['RequestID'] . '&action=reassign">';
								if ($requests_without_approvers == 0) {
									$mode = 'Assign';
									$assign_approver_txt .= '<img src="' . IRECRUIT_HOME . 'images/icons/status_offline.png" border="0" title="Assign" style="margin:0px 3px 0px 0px;">';
									$assign_approver_txt .= '</a>';
									$assign_approver_txt .= '<br><b style="font-size:8pt;">' . $mode . '<br>Approver</b>';
								} else {
									$mode = 'Reassign';
									$assign_approver_txt .= '<img src="' . IRECRUIT_HOME . 'images/icons/status_online.png" border="0" title="Reassign" style="margin:0px 3px 0px 0px;">';
									$assign_approver_txt .= '</a>';
									$assign_approver_txt .= '<br><b style="font-size:8pt;">' . $mode . '<br>Approver</b>';
								}
								$assign_approver_txt .= '</td>';
								
								//set where condition
								$where  = array("OrgID = :OrgID", "RequestID = :RequestID", "DateApproval != '0000-00-00 00:00:00'");
								//set parameters
								$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RR ['RequestID']);
								
								//get Request approvers information
								$resultsIN2 = $RequestObj->getRequestApproversInfo("*", $where, "", array($params));
								
								$approval_started = $resultsIN2['count'];
								
								if ($approval_started > 0) {
									$assign_approver_txt = '<td valign="top" align="center" width="60">';
									$assign_approver_txt .= '<img src="' . IRECRUIT_HOME . 'images/icons/status_busy.png" border="0" title="Approval Started" style="margin:0px 3px 0px 0px;">';
									$assign_approver_txt .= '<br><b style="font-size:8pt;color:666666">approval<br>started</b></td>';
								}
								
								echo $assign_approver_txt;
								
								$onclickex = "onclick=\"javascript:window.open('" . IRECRUIT_HOME . "request/approverStatus.php?RequestID=" . $RR ['RequestID'];
								if ($AccessCode != "") {
									$onclickex .= "&k=" . $AccessCode;
								}
								$onclickex .= "','_blank','location=yes,toolbar=no,height=400,width=850,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";
								
								if ($requests_without_approvers > 0) {
									echo '<td valign="top" align="center" width="60">';
									echo '<a href="#"' . $onclickex . '>';
									echo '<img src="' . IRECRUIT_HOME . 'images/icons/thumb_up.png" border="0" title="Approval Status" style="margin:0px 3px 0px 0px;">';
									echo '</a>';
									echo '<br><b style="font-size:8pt;">Approval<br>Status</b>';
									echo '</td>';
								}
							}
							
							if ($permit ['Admin'] == 1) {
								$req_del_title = $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RR ['RequestID'] );
								$req_del_title .= " - " . $RR ['Title'];
								echo '<td valign="top" align="center" width="60">';
								echo '<a href=\'javascript:void(0);\' onclick=\'delRequest("'.$RR ['RequestID'].'", this)\'>';
								echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px;">';
								echo '</a>';
								echo '</td>';
							} // end permit
							
							echo '</tr>';
						}
					}
					
					if ($ii == 0) {
						echo '<tr>';
						echo '<td colspan="4" align="center">There are no requests to approve.</td>';
						echo '</tr>';
					}
					
					echo '</table>';
					?>
					<!-- /.row (nested) -->
				</div>
			</div>
			<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>