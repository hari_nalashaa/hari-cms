<?php 
if (($_REQUEST['clear'] == "Y") && ($OrgID) && ($_REQUEST['RequestID']) && ($_REQUEST['EmailAddress'])) {
	
	$set_info = array("DateApproval = '0000-00-00 00:00:00'");
	//Set where condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID", "EmailAddress = :EmailAddress");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":EmailAddress"=>$_REQUEST['EmailAddress']);
	//Update Request Approvers
	$req_approver = $RequestObj->updRequestApprovers($set_info, $where, array($params));
	
	$Comments = "Cleared approval for " . $_REQUEST['EmailAddress'];

	//RequestHistory
	$req_info = array("OrgID"=>$OrgID, "RequestID"=>$_REQUEST['RequestID'], "Date"=>"NOW()", "UserID"=>$USERID, "Comments"=>$_REQUEST['Comment']);
	$RequestObj->insRequestInfo('RequestHistory', $req_info);
} // end if clear

echo '<div style="margin:10px;">';
echo '<table border="0" cellspacing="0" cellpadding="3" width="100%">';
echo '<form method="post" action="' . IRECRUIT_HOME . 'request/approverStatus.php" name="approvernotify">';
echo '<tr><td colspan="6" valign="top">';
echo '<b>Current status of request</b>:';
echo '</td></tr>';

if ($process == "Y") {
	
	include IRECRUIT_DIR . 'request/EmailLinks.inc';
	
	$count = count ( $notify );
	
	for($i = 0; $i < $count; $i ++) {
		
		sendApproveLink ( $OrgID, $_REQUEST['RequestID'], $notify [$i] );
		
		$lst1 .= $notify [$i] . '\\n';
		$lst2 .= $notify [$i] . '<br>';
	}
	
	if ($lst1) {
		
		$Comments = "Email notification for approval has been sent to:<br>" . $lst2;
		
		//Insert into RequestHistory
		$req_info = array("OrgID"=>$OrgID, "RequestID"=>$_REQUEST['RequestID'], "Date"=>"NOW()", "UserID"=>$USERID, "Comments"=>$Comments);
		$RequestObj->insRequestInfo('RequestHistory', $req_info);
		
		$message = 'Email notification for approval has been sent to:\\n\\n' . $lst1;
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
	}
} // end process

echo '<tr><td height="10" colspan="6"></td></tr>';

if ($_REQUEST['RequestID']) {
	
	//Set columns
	$columns = "*, date_format(PostDate,'%m/%d/%Y %h %p') PD, date_format(ExpireDate,'%m/%d/%Y %h %p') ED";
	//Set where condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
	
	$results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", "", array($params));
	$REQ = $results['results'][0];
	
	echo '<tr><td colspan="6">';
	
	if ($feature ['MultiOrg'] == "Y") {
		echo 'Organization: ' . $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $REQ ['RequestID'] ) . "<br>";
	} // end feature
	
	echo 'Title: ' . $REQ ['Title'] . '<br>';
	echo 'External Post Date: ' . $REQ ['PD'] . '<br>';
	echo 'External Expire Date: ' . $REQ ['ED'] . '<br>';
	echo 'Level of Approval Required: ' . $REQ ['ApprovalLevelRequired'] . '<br>';
	
	echo '</td></tr>';
}

echo '<tr><td height="20" colspan="6"></td></tr>';

echo '<tr>';
echo '<td></td>';
echo '<td><strong>Approver<br>Name</strong></td>';
echo '<td><strong>Email<br>Address</strong></td>';
echo '<td><strong>Authority<br>Level</strong></td>';
echo '<td><strong>Approval<br>Date</strong></td>';
echo '<td><strong>Clear<br>Approver</strong></td>';
echo '</tr>';

$rowcolor = "#eeeeee";

if ($_REQUEST['RequestID']) {
	//Set table names
	$table_name = "RequestApprovers RA, EmailApprovers EA";
	//Set columns
	$columns = "RA.EmailAddress, RA.AuthorityLevel, date_format(RA.DateApproval,'%Y-%m-%d %H:%i') DA, EA.FirstName, EA.LastName";
	//Set where condition
	$where = array("RA.OrgID = EA.OrgID", "RA.EmailAddress = EA.EmailAddress", "RA.OrgID = :OrgID",  "RA.RequestID = :RequestID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
	//Get Approvers Information
	$results = $RequestObj->getApproversInfo($table_name, $columns, $where, "RA.AuthorityLevel, RA.EmailAddress", array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $RA) {
			if ($RA ['DA'] == '0000-00-00 00:00:00' || $RA ['DA'] == '0000-00-00 00:00' || is_null($RA ['DA'])) {
				$DA = "Approval Pending";
			} else {
				$DA = "Approved: " . $RA ['DA'];
			}
			echo '<tr bgcolor="' . $rowcolor . '">';
			echo '<td align="left">';
			if ($RA ['DA'] == '0000-00-00 00:00:00' || $RA ['DA'] == '0000-00-00 00:00' || is_null($RA ['DA'])) {
				echo '<input type="checkbox" class="chk_active" id="notify" name="notify[]" value="' . $RA ['EmailAddress'] . '">';
			}
			echo '&nbsp;&nbsp;</td>';
			echo '<td align="left">' . $RA ['LastName'] . ', ' . $RA ['FirstName'] . '&nbsp;&nbsp;&nbsp;</td>';
			echo '<td align="left">' . $RA ['EmailAddress'] . '&nbsp;&nbsp;&nbsp;</td>';
			echo '<td align="left">' . $RA ['AuthorityLevel'] . '</td>';
			echo '<td align="left">' . $DA . '&nbsp;&nbsp;&nbsp;</td>';
			echo '<td align="left">';
			if ($RA ['DA'] != '0000-00-00 00:00:00' &&  $RA ['DA'] != '0000-00-00 00:00' && !is_null($RA ['DA'])) {
				echo '<a href="approverStatus.php?RequestID=' . $_REQUEST['RequestID'] . '&EmailAddress=' . $RA ['EmailAddress'] . '&clear=Y" onclick="return confirm(\'Are you sure you want to clear this approval?\n\nName: ' . $RA ['LastName'] . ', ' . $RA ['FirstName'] . '\nEmail Address: ' . $RA ['EmailAddress'] . '\n\n\')"><font style="color:red">&#10008;</font></a>';
			}
			echo '</td>';
			echo '</tr>';
		
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach	
	}
	
} // end RequestID

echo '<tr><td colspan="6" align="left" valign="bottom" height="40"><input type="submit" value="Notify Approvers"></td></tr>';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="RequestID" value="' . $_REQUEST['RequestID'] . '">';

echo '</form>';
echo '</table>';
echo '</div>';

echo '<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
?>
