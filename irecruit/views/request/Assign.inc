<?php
if ($action == "view") {
    ?>
    <div id="page-wrapper">
    	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
    	<!-- row -->
    	<div class="row">
    		<div class="col-lg-12">
    				<div class="panel panel-default">
    					<div class="panel-body">
    					<?php 
    					    include IRECRUIT_DIR . 'request/ViewRequest.inc';
                            echo displayRequestInfo ( $OrgID, $RequestID, $USERID, '' );
    					?>
    					<!-- /.row (nested) -->
    					</div>
    				</div>
    				<!-- /.panel-body -->
    			<!-- /.panel -->
    		</div>
    		<!-- /.col-lg-12 -->
    	</div>
    	<!-- /.row -->
    </div>
    <?php
}
else {
    include IRECRUIT_VIEWS . 'requisitions/CreateEditRequisitionForm.inc';
}    
?>