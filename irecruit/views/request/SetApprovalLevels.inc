<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc'; ?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					
						<table class="table table-bordered">
							<tr>
								<td align="left" valign="top" width="20%">
									<strong>How many people are needed to approve this position?:</strong>
								</td>
								<td>
									<select name="levelrequired" id="levelrequired" onchange="updApprovalLevels('<?php echo $_REQUEST['RequestID']?>', this.value)">
									<option value="">Select Number of Approvers</option>
									<?php
										$cnt = 0;
										while ( $cnt < $cnt_approvers ) {
											$cnt ++;
											
											echo '<option value="' . $cnt . '"';
											if ($levelrequired == $cnt) {
												echo " selected";
											}
											echo '>' . $cnt . '</option>';
										}
									?>
									</select>
								</td>
							</tr>
						</table>
						
						<div id="approvers_request_form">
						
						</div>
						
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	
	
	<!-- /.row -->
</div>

<script>
function updApprovalLevels(req_id, approval_level) {
	$("#approvers_request_form").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');

	var request_listing_url = "getApproversRequestForm.php?RequestID="+req_id+"&ApproversLevel="+approval_level;
	
	$.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		success: function(data) {
			$("#approvers_request_form").html(data);
    	}
	});
}

$(document).ready(function() {
	updApprovalLevels('<?php echo $_REQUEST['RequestID'];?>', document.getElementById('levelrequired').value)
});
</script>