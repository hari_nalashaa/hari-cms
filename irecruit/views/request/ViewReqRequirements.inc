<?php
echo '<p><b>Requisition Request Information</b></p>';

$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);

//set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//set where condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Get Requisition Information
$results = $RequisitionsObj->getRequisitionInformation("Title, Active", $where, "", "", array($params));

list ($title, $active) = array_values($results['results'][0]);

echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs($OrgID, $multiorgid_req, $RequestID) . "</b>";
echo "&nbsp;&nbsp;&nbsp;";
echo "Title: <b>" . $title . "</b><br><br>";

include IRECRUIT_DIR . 'request/ViewRequest.inc';
echo displayRequestInfo($OrgID, $RequestID, $USERID, '');

echo '<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
?>