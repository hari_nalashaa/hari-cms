<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<div id="del_process_message"></div>
					<table class="table table-striped table-bordered table-hover">
						<?php 
						if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == "fail") {
							?><tr>
								<td colspan="5" style="color: #D8000C;background-color: #FFBABA;">
									Sorry unable to process this request
								</td>
							  </tr>
							 <?php
						}
						else if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == "suc") {
							?><tr>
								<td colspan="5" style=" color: #4F8A10;background-color: #DFF2BF">
									Your request successfully submitted.
								</td>
							</tr><?php
						}?>
						<tr>
							<th><a href="requestsList.php?sort=title&sort_type=<?php echo $sort_type?>">Title</a></th>
							<th><a href="requestsList.php?sort=email&sort_type=<?php echo $sort_type?>">Requester Email</a></th>
							<th><a href="requestsList.php?sort=date&sort_type=<?php echo $sort_type?>">Received Date</a></th>
							<th>View</th>
							<th>Action</th>
						</tr>
						<?php
						$requests_list_count = $requests_list['count'];
						$requests_list_info = $requests_list['results'];
						
						if($requests_list_count > 0) {
							for($i = 0; $i < $requests_list_count; $i++) {
							?>
							<tr>
								<td id="request-title-<?php echo $requests_list_info[$i]['RequestID'];?>"><?php echo $requests_list_info[$i]['Title'];?></td>
								<td>
									<?php echo $requests_list_info[$i]['RequesterEmail'];?>
								</td>
								<td>
									<?php echo $requests_list_info[$i]['DateEntered'];?>
								</td>
								<td>
									<a href="processRequest.php?RequestID=<?php echo $requests_list_info[$i]['RequestID'];?>&action=edit">Process Request</a>
								</td>
							<?php
								echo '<td>';
								if ($permit ['Admin'] == 1) {
                                    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $requests_list_info[$i]['RequestID']);
									//$req_del_title = $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $requests_list_info[$i]['RequestID'] );
									$req_del_title .= " - " . $requests_list_info[$i]['Title'];
									
									$req_del_title = str_replace(array("'", '"'), "\\\'", $req_del_title);
									echo '<a href=\'javascript:void(0);\' onclick=\'delRequest("'.$requests_list_info[$i]['RequestID'].'", this)\'><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px;"></a>';
								} // end permit
								echo '</td>';
							}
						}
						else {
							?>
							<tr><td colspan="5" align="center">No pending requests.</td></tr>
							<?php
						}
						?>	
					</table>
					
					<!-- /.row (nested) -->
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>