<div id="page-wrapper">
	<?php include_once IRECRUIT_DIR . 'DashboardPageTitle.inc';?>
	
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					
						<form name="frmConversation" id="frmConversation" method="post">
							<table width="100%" border="0" cellpadding="3" cellspacing="3" class="table table-bordered">
								<tr>
									<td colspan="2">
										Please enter numbers to check for the existing conversations. 
										If in case there are no conversations, one will be created automatically.
									</td>
								</tr>
								
								<tr>
									<td width="10%"><strong>From Number: </strong></td>
									<td>
										<input type="text" name="NUM[NID1]" id="NID1" value="<?php echo $_POST['NUM']['NID1'];?>">
									</td>
								</tr>
									
								<tr>
									<td width="10%"><strong>To Number: </strong></td>
									<td>
										<input type="text" name="NUM[NID2]" id="NID2" value="<?php echo $_POST['NUM']['NID2'];?>">
									</td>
								</tr>
								
								<tr>
									<td colspan="2" align="left">
										<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Search">
									</td>
								</tr>
							</table>
						</form>

						<br><br>
						<?php
						if(isset($user_conversations_list) && count($user_conversations_list) > 0) {
							?>
							<table width="100%" border="0" cellpadding="1" cellspacing="1">
								<tr>
									<td><strong>Resource ID</strong></td>
									<td><strong>Mobile Numbers</strong></td>
									<td><strong>Created Date Time</strong></td>
									<td><strong>Action</strong></td>
								</tr>
								<?php 
								for($cd = 0; $cd < count($user_conversations_list); $cd++) {
									$con_resource_id	=	$user_conversations_list[$cd]['ResourceID'];
									?>
									<tr>
										<td valign="top"><?php echo $user_conversations_list[$cd]['ResourceID'];?></td>
										<td valign="top">
											<?php 
												$mobile_numbers_list		=	json_decode($user_conversations_list[$cd]['MobileNumbers'], true);
												$mobile_numbers_list_count	=	count($mobile_numbers_list);
												
												for($m = 0; $m < count($mobile_numbers_list); $m++) {
													echo $mobile_numbers_list[$m][0].' - '.$mobile_numbers_list[$m][1]."<br>";
												}
											?>
										</td>
										<td valign="top"><?php echo $user_conversations_list[$cd]['CreatedDateTime'];?></td>
										<td valign="top">
											<a href="sendConversationMessage.php?ResourceID=<?php echo $con_resource_id;?>">Continue Conversation</a>
											<?php 
											if($mobile_numbers_list_count < 2) {
												?>
												/
												<a href="addSmsParticipant.php?ResourceID=<?php echo $con_resource_id;?>">Add Participant</a>
												<?php
											}
											?>
										</td>
									</tr>
									<?php
								}
								?>
							</table>
							<?php
						}
						?>
					</div>
				</div>
		</div>
	</div>
</div>							