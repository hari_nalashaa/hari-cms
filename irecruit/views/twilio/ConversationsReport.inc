<div id="openFormWindow" class="openForm">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">	

			<p style="text-align:right;">
				<a href="#closeForm" title="Close Window">
					<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
					<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
				</a>
			</p>
		
			<div id="divConversationInfo">
			
			</div>
		</div>
	</div>
</div>

<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php
						//Get twilio number
						$user_details			=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
						$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);
						
						//Add plus one before number
						if(substr($user_details['Cell'], 0, 2) != "+1") {
							$user_cell_number	=	"+1".$user_cell_number;
						}

						$picker_from_date	    =	date("m/d/Y", strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));
						if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] != "") {
						    $picker_from_date	=	$_REQUEST['from_date'];
						}
						
						$picker_to_date		=	date("m/d/Y", strtotime("+1 day"));
						if(isset($_REQUEST['to_date']) && $_REQUEST['to_date'] != "") {
						    $picker_to_date	=	$_REQUEST['to_date'];
						}
						
						//Set from, to dates
						$from_date 			= 	G::Obj('DateHelper')->getYmdFromMdy($picker_from_date);
						$to_date 			= 	G::Obj('DateHelper')->getYmdFromMdy($picker_to_date);
												
						$keyword 			=	$_POST['twilio_applicants_keyword'];
						$sort_field 		=	$_POST['twilio_sort_field'];
						$sort_order 		=	$_POST['twilio_sort_order'];
						$applicants_info    =   G::Obj('Applications')->getTwilioApplicantsByKeyword($FilterOrganization, $OrgID, $keyword, $sort_field, $sort_order, $from_date, $to_date, "0, 100");
						$applicants_list    =   $applicants_info["results"];
						
						echo '<form name="frmRefineFormStatus" id="frmRefineFormStatus" method="GET">';
						echo '<table class="table table-bordered">';
						echo '<tr>';
						echo '<td colspan="5">';
                        if($feature['MultiOrg'] == "Y") {
                            $where_info     =   array("OrgID = :OrgID");
                            $params_info    =   array(":OrgID"=>$OrgID);
                            $columns        =   "OrgID, MultiOrgID, OrganizationName";
                            $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
                            $orgs_list      =   $orgs_results['results'];
                        
                            echo 'Organizations List: '; 
                            echo '<select name="ddlOrganizationsList" id="ddlOrganizationsList" class="form-control width-auto-inline">';
                            echo '<option value="">All</option>';
                        	for($ol = 0; $ol < count($orgs_list); $ol++) {
                        	    if($FilterOrganization == $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID']) {
                        	        $selected = ' selected="selected"';
                        	    }
                        	    echo '<option value="'.$orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'].'" '.$selected.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
                        	    unset($selected);
                        	}
                            echo '</select>';
                        }
                        else {
                            echo '<input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value="">';
                        }
						echo '&nbsp;From: <input type="text" class="form-control width-auto-inline" name="from_date" id="from_date" value="'.$picker_from_date.'">';
						echo '&nbsp;To: <input type="text" class="form-control width-auto-inline" name="to_date" id="to_date" value="'.$picker_to_date.'">';
						echo '&nbsp;&nbsp;&nbsp;<input type="submit" name="btnRefine" id="btnRefine" class="btn btn-primary" value="Refine">';
						echo  '</td>';
						echo '</tr>';
						echo '</table>';
						echo '</form>';
						
						echo '<table class="table table-bordered">';
						echo '<tr>';
						echo '<td colspan="5">';
						echo "<i>Note:*</i> To make the filter efficient please don't use it for large dates";
						echo '</td>';
						echo '</tr>';
						echo '</table>';
						
						
						echo '<form name="frmReminderSendNotification" id="frmReminderSendNotification" method="POST">';
						echo '<table class="table table-bordered" id="data-form-status">';
						echo '<thead>';
						echo '<tr>';
						echo '<th valign="top">Application ID</th>';
						echo '<th valign="top">Applicant Name</th>';
						echo '<th valign="top">RequisitionID/JobID</th>';
						echo '<th valign="top">Entry Date</th>';
						echo '<th valign="top">iRecruit Text</th>';
						echo '</tr>';
						echo '</thead>';

                        $texting_agreement_info     =   G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($OrgID);
						
						echo '<tbody>';
						for($a = 0; $a < count($applicants_list); $a++) {
							
							$ApplicationID			=	$applicants_list[$a]['ApplicationID'];
							$RequestID				=	$applicants_list[$a]['RequestID'];
							
							//Get applicant cellphone
							$cell_phone_info		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphone');
							$cell_phone				=	json_decode($cell_phone_info, true);
							$applicant_number		=	"+1".$cell_phone[0].$cell_phone[1].$cell_phone[2];
							
							$conversation_info		=	G::Obj('TwilioConversationInfo')->getUserConversationResources($OrgID, $USERID, $applicant_number, $user_cell_number);
							$ResourceID				=	$conversation_info[0]['ResourceID'];
							
							echo '<tr>';
							echo '<td valign="top">'.$applicants_list[$a]['ApplicationID'].'</td>';
							echo '<td valign="top">'.$applicants_list[$a]['ApplicantSortName'].'</td>';
							echo '<td valign="top">'.$applicants_list[$a]['Title'].'</td>';
							echo '<td valign="top">'.$applicants_list[$a]['EntryDate'].'</td>';
							echo '<td valign="top">';
							if($texting_agreement_info['OrgID'] == "") {
								echo "-";

							} else if(count($conversation_info) > 0) {
								echo "<a href=\"#openFormWindow\" onclick=\"getConversationInfo('".$ResourceID."', '".$ApplicationID."', '".$RequestID."', 'divConversationInfo')\">View Conversation</a>";
							}
							else {
								echo "<a href=\"#openFormWindow\" onclick=\"getConversationInfo('".$ResourceID."', '".$ApplicationID."', '".$RequestID."', 'divConversationInfo')\">Create Conversation</a>";
							}
							echo '</td>';
							echo '</tr>';
						}
						echo '</tbody>';
						
						echo '</table>';
						echo '</form>';
						?>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>

<script>
$(document).ready(function() {
    $('#data-form-status').DataTable({
        responsive: true,
        aLengthMenu: [
          [50, 100, -1],
          [50, 100, "All"]
        ]
    });
});

function getUrlParameter(urlString, sParam) {
	var vars = [], hash;
    var sPageURL = decodeURIComponent(urlString);
	
    var hashes = sPageURL.slice(sPageURL.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    
    return vars[sParam];
}
</script>