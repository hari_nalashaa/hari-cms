<?php


?>


<div id="openFormWindow" class="openForm">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">	
    		<p style="text-align:right;">
    			<a href="#closeForm" title="Close Window">
    				<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
    				<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
    			</a>
    		</p>
    		<div id="terms_of_service_acceptable_privacy_policy"></div>
		</div>
	</div>
</div>

<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<div class="row">
    	<div class="col-lg-12">
    		<?php
    		if($ERROR != "") {
                echo "<br><span style='color:red'>".$ERROR."</span><br><br>";    
    		}
    		else if($_GET['msg'] == 'suc') {
    		    echo '<br>';
    		    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    		    echo '<tr><td height="10" style="color:blue;">';
    		    echo 'Thank you for your subscription. The order number for your records is ' . $_REQUEST['PurchaseNumber'] . '. An email confirmation of your order has been sent to your account email address. ';
    		    echo '</table>';
    		}
    		else if($_GET['msg'] == 'failed') {
    		    echo '<br>';
    		    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    		    echo '<tr>';
    		    echo '<td height="150" style="color: red;">';
    		    echo 'There has been an error with your credit card information. Please update the information and submit again.';
    		    echo '</td>';
    		    echo '</tr>';
    		    echo '</table>';
    		}
    		else if($_GET['msg'] == 'failedcard') {
    		    echo '<br><span style="color:blue">Sorry there is an issue with the card. Please re-submit the details.</span><br><br>';
    		}
    		?>
    	</div>
	</div>
	<div class="row">
    	<div class="col-lg-12" style="color: blue">
        	<div style="text-align:right;padding:10px;border:1px solid #ddd;margin-bottom:10px">
        		<a href="<?php echo IRECRUIT_HOME;?>adminDashboard.php?menu=8">Back to Admin</a>
        	</div>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<form name="frmTextAgreement" id="frmTextAgreement" method="post">

                            <strong>Monthly Licensing Terms:</strong><br><br>
                            
                        <table class="table table-bordered">
                            	<tr>
                            		<th>Number of Credits</th>
                            		<th>Monthly Fee</th>
                            	</tr>
				<tr>
					<td>
						<?php echo $twilio_account_info['Credits']; ?>
					</td>
					<td>
						<?php echo "$" . $twilio_account_info['BillAmt']; ?>
					</td>
				</tr>
								
				<tr>
					<td align="right">
						<strong>Monthly Charges: </strong>
					</td>
					<td>
						<?php echo "<strong>$" . $twilio_account_info['BillAmt'] . "</strong>"; ?> 
					</td>
				</tr>
			</table>                            
                            
Your license includes the following:<br>
<ul>
<li>Unlimited iRecruit users.</li>
<li>Unlimited text messages.</li>
<li><?php echo $twilio_account_info['Credits']; ?> credits.</li>
<li>One credit is calculated as an active text communication between an iRecruit user and one applicant that has ongoing correspondence within the last 30 days.</li>
<li>Once you reach your credit limit new conversations will be suspended until credits become available.</li>
<li>Consent must be obtained before texting any applicant or candidate from iRecruit.</li>
</ul>
                            
                            <strong>Agreement:</strong><br><br>
                            
                            <?php
                            if($texting_agreement_info['OrgID'] != "") {
                                echo "Yes, ";
                            }
                            else {
                                ?>
                                <input type="checkbox" name="chkUnderstandIrecruitText" id="chkUnderstandIrecruitText" value="Y" <?php if($texting_agreement_info['AgreeToIrecruitPaidText'] == "Y") echo ' checked="checked"';?>> <font style="color: red">*</font> 
                                <?php
                            }
                            ?>
                            I understand that iRecruit Text is an additional paid service, and agree to pay monthly fees. <br><br>
                            <?php
                            if($texting_agreement_info['OrgID'] != "") {
                                echo "Yes, ";
                            }
                            else {
                                ?>
								<input type="checkbox" name="chkAgreeToTermsOfService" id="chkAgreeToTermsOfService" value="Y" <?php if($texting_agreement_info['AgreeToTermsOfService'] == "Y") echo ' checked="checked"';?>> <font style="color: red">*</font> 
                                <?php
                            }
                            ?>
                            I have read and agree to the <a href="#openFormWindow" onclick="getTermsOfServiceAcceptablePrivacyPolicy('<?php echo IRECRUIT_HOME.'twilio/termsOfService.php?menu=8';?>');">Terms of Service</a>. <br><br>

                            <?php
                            if($texting_agreement_info['OrgID'] != "") {
                                echo "Yes, ";
                            }
                            else {
                                ?>
                            	<input type="checkbox" name="chkAgreeToAcceptableUsePolicy" id="chkAgreeToAcceptableUsePolicy" value="Y" <?php if($texting_agreement_info['AgreeToAcceptableUsePolicy'] == "Y") echo ' checked="checked"';?>> <font style="color: red">*</font> 
                                <?php
                            }
                            ?>
                            I have read and agree to the <a href="#openFormWindow" onclick="getTermsOfServiceAcceptablePrivacyPolicy('<?php echo IRECRUIT_HOME.'twilio/acceptableUserPolicy.php?menu=8';?>');">Acceptable Use Policy</a>. <br><br>
                            
                            <table class="table table-bordered">
                            	<tr>
                            		<td colspan="2">
                            			<?php
                            			if($texting_agreement_info['OrgID'] != "") {
                            			    echo "Subscription Began: ".date('m/d/Y', strtotime($texting_agreement_info['SubscriptionDate']))."<br><br>";
                            			    echo "Last Billed: ".date('m/d/Y', strtotime($texting_agreement_info['LastBilledDateTime']))."<br>";
                            			}
                            			else {
                            			    include_once IRECRUIT_DIR . 'twilio/TwilioPaymentForm.inc';
                            			}
                            			?>
                            		</td>
                            	</tr>
                            	<?php
                            	if($texting_agreement_info['OrgID'] == "") {
                            	    ?>
									<tr>
                                		<td colspan="2"><input type="submit" name="btnSave" id="btnSave" class="btn btn-primary" value="Purchase"></td>
                                	</tr>
                            	    <?php
                            	}
                            	?>                            	
                            </table>
							<input type="hidden" name="chkSubscribe" id="chkSubscribe" value="Y">
							<input type="hidden" name="agree" id="agree" value="Y">
							<input type="hidden" name="monthly_fee" id="monthly_fee" value="<?php echo $twilio_account_info['BillAmt']; ?>">
							<input type="hidden" name="amount" id="amount" value="<?php echo $twilio_account_info['BillAmt']; ?>">

						</form>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script type="text/javascript">
function calculateMonthlyFee(total_fee) {

        $("#payment_form_formatted_amt").html("$" + total_fee + ".00");

}
function getTermsOfServiceAcceptablePrivacyPolicy(url) {
	var request = $.ajax({
		method: "POST",
  		url: url,
		type: "POST",
		beforeSend: function() {
			$("#terms_of_service_acceptable_privacy_policy").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#terms_of_service_acceptable_privacy_policy").html(data);
    	}
	});
}
calculateMonthlyFee(<?php echo $twilio_account_info['BillAmt']; ?>);
date_picker("#txtDate");
</script>
