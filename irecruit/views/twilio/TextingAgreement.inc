<?php
if($twilio_account_info['OrgID'] != "") {
    //Get Last Month Records Usage For All Categories
    $last_month_records_info	=	G::Obj('TwilioUsageRecordsApi')->getLastMonthUsageForAllCategories($twilio_account_info['OrgID']);
    $last_month_records			=	$last_month_records_info['Response'];
    
    for($c = 0; $c < count($last_month_records); $c++) {
        
        $last_month_obj	=	$last_month_records[$c];
        
        //Get the Protected properties from SMS object, by extending the object through ReflectionClass
        $reflection     =   new ReflectionClass($last_month_obj);
        $property       =   $reflection->getProperty("properties");
        $property->setAccessible(true);
        $records_info[]	=   $property->getValue($last_month_obj);
    }
}

$total_usage = 0;
$segment_categories = array(
                            "sms-inbound",
                            "sms-outbound",
                            "mms-inbound",
                            "mms-outbound",
);

if(count($records_info) > 0) {
    for($r = 0; $r < count($records_info); $r++) {
        if(in_array($records_info[$r]['category'], $segment_categories)) {
            $total_usage += $records_info[$r]['usage'];
        }
    }
}
?>
<div id="openFormWindow" class="openForm">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">	
    		<p style="text-align:right;">
    			<a href="#closeForm" title="Close Window">
    				<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
    				<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
    			</a>
    		</p>
    		<div id="terms_of_service_acceptable_privacy_policy"></div>
		</div>
	</div>
</div>

<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<div class="row">
    	<div class="col-lg-12">
    		<?php
    		if($ERROR != "") {
                echo "<br><span style='color:red'>".$ERROR."</span><br><br>";    
    		}
    		else if($_GET['msg'] == 'suc') {
    		    echo '<br>';
    		    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    		    echo '<tr><td height="10" style="color:blue;">';
    		    echo 'Thank you for your subscription. The order number for your records is ' . $_REQUEST['PurchaseNumber'] . '. An email confirmation of your order has been sent to your account email address. ';
    		    echo '</table>';
    		}
    		else if($_GET['msg'] == 'failed') {
    		    echo '<br>';
    		    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    		    echo '<tr>';
    		    echo '<td height="150" style="color: red;">';
    		    echo 'There has been an error with your credit card information. Please update the information and submit again.';
    		    echo '</td>';
    		    echo '</tr>';
    		    echo '</table>';
    		}
    		else if($_GET['msg'] == 'failedcard') {
    		    echo '<br><span style="color:blue">Sorry there is an issue with the card. Please re-submit the details.</span><br><br>';
    		}
    		?>
    	</div>
	</div>
	<div class="row">
    	<div class="col-lg-12" style="color: blue">
        	<div style="text-align:right;padding:10px;border:1px solid #ddd;margin-bottom:10px">
        		<a href="<?php echo IRECRUIT_HOME;?>adminDashboard.php?menu=8">Back to Admin</a>
        	</div>
        </div>
	</div>
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<form name="frmTextAgreement" id="frmTextAgreement" method="post">
                            <strong>Monthly Estimate:</strong><br><br>
                            
                            <table class="table table-bordered">
                            	<tr>
                            		<th>Account Options</th>
                            		<th>Cost Per User</th>
                            		<th>Number of Credits</th>
                            		<th>Monthly Fee</th>
                            	</tr>
                            	<tr>
                            		<td>1st User</td>
                            		<td>$59.00</td>
                            		<td>1000</td>
    								<td>$59.00</td>
                            	</tr>
								<tr>
									<td>
										<?php
										$texting_agreement_info['AdditionalUsers'] = $twilio_account_info['Licenses'] - 1;
										echo $texting_agreement_info['AdditionalUsers'];?> Additional Users 
										<input type="hidden" name="additional_users" id="additional_users" value="<?php echo $texting_agreement_info['AdditionalUsers'];?>">
									</td>
									<td>
										$20.00
									</td>
									<td>
										500
									</td>
									<td>
										<span id="additional_users_fee"></span>
									</td>
								</tr>
								
								<tr>
									<td align="right">
										<strong>Expected Monthly Fee:</strong>
									</td>
									<td>
										&nbsp;
									</td>
									<td>
										<span id="total_credits" style="font-weight: bold">1,000</span>
									</td>
									<td>
										<span id="formatted_monthly_fee" style="font-weight: bold"></span>
										
									</td>
								</tr>
								
							</table>                            
                            
                            <br>
                            
                            Messages translate to 1-3 credits based on message size.<br>
                            
                            Credits in excess of the monthly total of credits will be billed in increments of 100 at a rate of $.59.<br>
                            
                            Consent must be obtained before texting any applicant or candidate from iRecruit.<br>
                            
                            <br>
                            
                            <strong>Agreement:</strong><br><br>
                            
                            <?php
                            if($texting_agreement_info['OrgID'] != "") {
                                echo "Yes, ";
                            }
                            else {
                                ?>
                                <input type="checkbox" name="chkUnderstandIrecruitText" id="chkUnderstandIrecruitText" value="Y" <?php if($texting_agreement_info['AgreeToIrecruitPaidText'] == "Y") echo ' checked="checked"';?>> <font style="color: red">*</font> 
                                <?php
                            }
                            ?>
                            I understand that iRecruit Text is an additional paid service, and agree to pay monthly fees which may differ from above based on your usage. <br><br>
                            <?php
                            if($texting_agreement_info['OrgID'] != "") {
                                echo "Yes, ";
                            }
                            else {
                                ?>
								<input type="checkbox" name="chkAgreeToTermsOfService" id="chkAgreeToTermsOfService" value="Y" <?php if($texting_agreement_info['AgreeToTermsOfService'] == "Y") echo ' checked="checked"';?>> <font style="color: red">*</font> 
                                <?php
                            }
                            ?>
                            I have read and agree to the <a href="#openFormWindow" onclick="getTermsOfServiceAcceptablePrivacyPolicy('<?php echo IRECRUIT_HOME.'twilio/termsOfService.php?menu=8';?>');">Terms of Service</a>. <br><br>

                            <?php
                            if($texting_agreement_info['OrgID'] != "") {
                                echo "Yes, ";
                            }
                            else {
                                ?>
                            	<input type="checkbox" name="chkAgreeToAcceptableUsePolicy" id="chkAgreeToAcceptableUsePolicy" value="Y" <?php if($texting_agreement_info['AgreeToAcceptableUsePolicy'] == "Y") echo ' checked="checked"';?>> <font style="color: red">*</font> 
                                <?php
                            }
                            ?>
                            I have read and agree to the <a href="#openFormWindow" onclick="getTermsOfServiceAcceptablePrivacyPolicy('<?php echo IRECRUIT_HOME.'twilio/acceptableUserPolicy.php?menu=8';?>');">Acceptable Use Policy</a>. <br><br>
                            
                            <table class="table table-bordered">
                            	<tr>
                            		<td colspan="2">
                            			<?php
                            			if($texting_agreement_info['OrgID'] != "") {
                            			    echo "Subscription Began: ".date('m/d/Y', strtotime($texting_agreement_info['SubscriptionDate']))."<br><br>";
                            			    echo "Last Billed: ".date('m/d/Y', strtotime($texting_agreement_info['LastBilledDateTime']))."<br>";
                            			}
                            			else {
                            			    include_once IRECRUIT_DIR . 'twilio/TwilioPaymentForm.inc';
                            			}
                            			?>
                            		</td>
                            	</tr>
                            	<?php
                            	if($texting_agreement_info['OrgID'] == "") {
                            	    ?>
									<tr>
                                		<td colspan="2"><input type="submit" name="btnSave" id="btnSave" class="btn btn-primary" value="Purchase"></td>
                                	</tr>
                            	    <?php
                            	}
                            	?>                            	
                            </table>
							<input type="hidden" name="chkSubscribe" id="chkSubscribe" value="Y">
							<input type="hidden" name="agree" id="agree" value="Y">
                            <input type="hidden" name="monthly_fee" id="monthly_fee">
						</form>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script type="text/javascript">
var number_of_users = '<?php echo $texting_agreement_info['AdditionalUsers'];?>';
function calculateMonthlyFee(number_of_users) {
	var total_fee = (number_of_users * 20) + 59;
	var additional_users_fee = (number_of_users * 20);
	$("#monthly_fee").val(total_fee);
	$("#amount").val(total_fee);
	$("#formatted_monthly_fee").html("$" + total_fee + ".00");
	$("#payment_form_formatted_amt").html("$" + total_fee + ".00");
	$("#additional_users_fee").html("$" + additional_users_fee + ".00");
	
	var total_credits 	=	(number_of_users * 500) + 1000;

	const formatter		=	new Intl.NumberFormat('en-US', {
                        		  style: 'currency',
                        		  currency: 'USD',
                        		  minimumFractionDigits: 0
                        	});

	total_credits = formatter.format(total_credits);
	total_credits = total_credits.slice(1);
	$("#total_credits").html(total_credits);
}

function getTermsOfServiceAcceptablePrivacyPolicy(url) {
	var request = $.ajax({
		method: "POST",
  		url: url,
		type: "POST",
		beforeSend: function() {
			$("#terms_of_service_acceptable_privacy_policy").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#terms_of_service_acceptable_privacy_policy").html(data);
    	}
	});
}

calculateMonthlyFee(number_of_users);
date_picker("#txtDate");
</script>
