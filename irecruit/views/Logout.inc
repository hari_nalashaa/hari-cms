<style>
body {
	background: none;
}
</style>

<table border="0" class="table" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td style="border: 0px">
			<div class="row">
				<div class="col-sm-16 col-md-6">
    			<?php
    			if($brand_info['BrandID'] == "0") {
                    echo '<img border="0" src="images/iRecruit-Logo.png"><br><br>';
    			}
    			else {
    			    $brand_logo_url =  IRECRUIT_HOME . "/vault/brands/".$brand_org_info['Logo'];
    			    echo '<img src="'.$brand_logo_url.'" alt="logo">';
    			}
    			?>			
				</div>
			</div>
		</td>
	</tr>

	<tr>
		<td align="center" valign="bottom" style="border: 0px">
			<a href="javascript:window.close();">
				<img src="images/logout.png" border="0" width="90">
			</a>
		</td>
	</tr>

	<tr>
		<td align="center" style="border: 0px">

			<?php if (isset($inactive) && $inactive == "y") { ?>
			<p>
				You have been logged out of the iRecruit application due to
				inactivity.<br> Viewed pages during this session may still be seen
				in the browsers cache.<br> Please be sure to close your browser
				completely to ensure data privacy.
			</p>

			<?php } else { ?>

			<p>
				You have successfully logged out of the iRecruit application.<br>
				Viewed pages during this session may still be seen in the browsers
				cache.<br> Please be sure to close your browser completely to ensure
				data privacy.
			</p>

			<?php } ?>

			<p>Thank you for using iRecruit.</p>

			<p>
				Click <a href="<?php echo IRECRUIT_HOME.'index.php?menu=1&OrgID='.$OrgID?>">here</a> to
				log back in.
			</p>
		</td>
	</tr>
	<tr><td style="border: 0px;margin: 0px"><br><br><br><br><br><br><br><br></td></tr>
	<tr>
		<td>
			<div class="row">
				<div class="col-sm-12 col-md-12" style="text-align: center;">
					<?php 
					if($brand_info['BrandID'] == "0") {
					    ?>Powered by <a href="https://www.irecruit-software.com" target="_blank">iRecruit</a>.<?php
                    }
                    else {
                         if($brand_org_info['Url'] != "") {
                            ?><a href="<?php echo $brand_org_info['Url'];?>" target="_blank"><?php echo $brand_org_info['FooterText'];?></a><?php
                         }
                         else {
                         	echo $brand_org_info['FooterText'];
                         }
                    }
					?>
					&nbsp;|&nbsp; 
					<a href="javascript:bookmarksite('iRecruit', 'https://<?php echo $_SERVER['SERVER_NAME']?>');">Bookmark Page</a>
		       </div>
			</div>
		</td>
	</tr>
</table>
