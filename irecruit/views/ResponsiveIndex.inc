<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!--  Row -->
    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardTopBlocks.inc';?>
    <!--  Row -->
	<div class="row" id="moduleslist">
    	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardHomeModules.inc';?>
	</div>
</div>

<script type="text/javascript">
var datepicker_ids = '#module_from_date5, #module_from_date6, #module_to_date5';
datepicker_ids += ', #module_to_date6, #module_from_date7, #module_from_date8';
datepicker_ids += ', #module_to_date7, #module_to_date8, #module_from_date17';
datepicker_ids += ', #module_to_date17, #quicksrch_from_date, #quicksrch_to_date';
datepicker_ids += ', #req_search_from_date, #req_search_to_date, #zip_recruiter_req_from_date';
datepicker_ids += ', #zip_recruiter_req_to_date, #twilio_search_from_date, #twilio_search_to_date';
datepicker_ids += ', #woct_quicksrch_from_date, #woct_quicksrch_to_date';
var date_format = 'mm/dd/yy';
</script>

<!-- /#page-wrapper -->
<script type="text/javascript">
$( document ).ready(function() {
	$('.fa-times').click(function() {
		$(this).parents('.mdsortable').hide();
		var widget_id = $(this).parents('.mdsortable').attr('id');
		var module_id = widget_id.substr(9);
		
		$.ajax({
    		url: irecruit_home+'responsive/updateModuleStatus.php',
    		type: "GET",
    		data: "ModuleId="+module_id,
    		success: function() {
    			
    		}
    	});
	});
	$('.toggle-arrow .fa-2').click(function() {
		if($(this).attr('class') == 'fa fa-chevron-down fa-2') {
			var widget_id = this.id;
			var module_id = widget_id.substr(23);
			
			$(this).attr('class', 'fa fa-chevron-up fa-2');
			$("#widget_body_"+module_id).hide();
		}
		else {
			var widget_id = this.id;
			var module_id = widget_id.substr(23);
			
			$(this).attr('class', 'fa fa-chevron-down fa-2');
			$("#widget_body_"+module_id).show();
		}		
	});
	$('#theme-change').click(function() {
		if($('#theme-change').width() > 28) 
			$('#theme-change').css({"width": "30px", "height": "28px"});
		else 	
			$('#theme-change').css({"width": "212px", "height": "70px", "transition": "1s", "-webkit-transition": "1s"});
	});

	setTimeout(function(){ $("#drag_drop_notification").fadeOut(); }, 3000);

	setDashboardDate();

	date_picker(datepicker_ids, date_format);

    getApplicantsByKeyword();
    if(wotcID != "") {
    	getWotcApplicantsByKeyword();
    }
    getRequisitionsListByParams();
    getTwilioApplicants();
});

var app_by_distinction      =   '';
var app_by_processorder     =   '';
var app_by_disposition_code =   '';
var app_by_searchable       =   '';
var app_by_source           =   '';
<?php
if(is_array($blockidslist)) {
	foreach ($blockidslist as $jblockid) {
		if($jblockid == 1) {
			if($ADInfo != "") {
				?>var app_by_distinction = <?php echo $ADInfo;?>;<?php
				echo "\n";
			}
		}
		if($jblockid == 3) {
			if($APOInfo != "") {
				?>var app_by_processorder  = <?php echo $APOInfo;?>;<?php
				echo "\n";
			}
		}
		if($jblockid == 4) {
			if($ADCInfo != "") {
				?>var app_by_disposition_code = <?php echo $ADCInfo;?>;<?php
				echo "\n";
			}
		}	
		if($jblockid == 2) {
			if($ASInfo != "") {
				?>var app_by_searchable  = <?php echo $ASInfo;?>;<?php
				echo "\n";
			}	
		}
	}
}

if(isset($applicants_by_source) && $applicants_by_source != "") {
	?>var app_by_source = <?php echo $applicants_by_source;?>;<?php
}
?>
</script>

<!-- /#wrapper -->
<script type="text/javascript">
var vbargraphdata = '';
var vbargraphxaxis  = '';
var hbargraphdata = '';
var hbargraphyaxis = '';
var dashboard_vbargraph = false;
<?php
if($usermodules[10] == 1) {
	?>
	dashboard_vbargraph = true;
	<?php
}
if(isset($barvgraphdata) && $barvgraphdata != "" && isset($barvgraphxaxis) && $barvgraphxaxis != "") {
	?>
	var vbargraphdata = <?php echo $barvgraphdata; ?>;
	var vbargraphxaxis  = <?php echo $barvgraphxaxis; ?>;
	<?php
}
if(isset($barhgraphdata) && $barhgraphdata != "" && isset($barhgraphyayis) && $barhgraphyayis != "") {
	?>
	var hbargraphdata = <?php echo $barhgraphdata; ?>;
	var hbargraphyaxis = <?php echo $barhgraphyayis; ?>;
	<?php
}
?>
</script>
<script type="text/javascript">
	if(app_by_distinction == '') {
		if (CheckDocumentObject('applications-by-distinction')) $("#applications-by-distinction").parents('.mdsortable').hide();
	}
	if(app_by_searchable == '') {
		if (CheckDocumentObject('applications-searchable')) $("#applications-searchable").parents('.mdsortable').hide();
	}
	if(app_by_processorder == '') {
		if (CheckDocumentObject('applications-by-processorder')) $("#applications-by-processorder").parents('.mdsortable').hide();
	}
    if(app_by_disposition_code == '') {
    	if (CheckDocumentObject('applications-by-dispositioncode')) $("#applications-by-dispositioncode").parents('.mdsortable').hide();
	}
	
    if(vbargraphdata == '' || vbargraphxaxis == '') $("#vertbarchartplaceholder").parents('.mdsortable').hide();
    if(hbargraphdata == '' || hbargraphyaxis == '') $("#horibarchartplaceholder").parents('.mdsortable').hide();

    $( document ).ready(function() {
	    ChangeViewColor(0);
	    $( "#tcviewmonth" ).css({"background-color":'grey', "text-align":"center", "color":"#FFFFFF"});
    });
</script>
<?php
$ISVBARGRAPH = false;
$ISHBARGRAPH = false;
if(isset($barvgraphdata) && $barvgraphdata != "" && isset($barvgraphxaxis) && $barvgraphxaxis != "") $ISVBARGRAPH = true;
if(isset($barhgraphdata) && $barhgraphdata != "" && isset($barhgraphyayis) && $barhgraphyayis != "") $ISHBARGRAPH = true;

$ISVBARGRAPH = $VBarChartStatus;
$ISHBARGRAPH = $HBarChartStatus;
?>