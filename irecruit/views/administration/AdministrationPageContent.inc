<?php
if ($permit ['Admin'] < 1) { // user admin to maintain login only

	echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered">';
	echo '<tr><td class="title" valign="top" width="400">';
	echo $title;
	echo '</td><td></td></tr>';
	echo '</table></div>';
	
	if (($action == "useredit") && ($useraction == "profile") && ($UserIDedit)) {
		$UserIDedit = $USERID;

		if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/Users.inc';
		}
	} else if ($action == 'userpreferences') {
		if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/UserPreferences.inc';
		}
	} else if (substr ( $USERROLE, 0, 21 ) != 'master_admin') {
		if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'irecruit_permissions.err';
		}
	}
	else {
		if (defined('IRECRUIT_DIR')) {
	    		include IRECRUIT_DIR . 'irecruit_permissions.err';
		}
	}
	
} else { // has full admin rights

    
    echo '<div style="text-align:right;padding:10px;border:1px solid #ddd;margin-bottom:10px"><a href="'.IRECRUIT_HOME.'adminDashboard.php?menu=8">Back to Admin</a></div>';
    
	if ($action == 'multiorg') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/MultiOrgData.inc';
	    }
	}
	
	if ($action == 'savecreditcardinfo') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/SaveIntuitCreditCardInfo.inc';
	    }
	}
	
	if ($action == 'generateintuitcustomerid') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/GenerateIntuitCustomerID.inc';
	    }
	}
	
	if ($action == 'addorganization') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/AddOrganization.inc';
	    }
	}
	
	if ($action == 'applicantsummaryquestions') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/ApplicantSummaryQuestions.inc';
	    }
	}
	if ($action == 'comparativeanalysisquestions') {
	    if (defined('IRECRUIT_DIR')) {
	        include IRECRUIT_DIR . 'administration/ComparativeAnalysisQuestions.inc';
	    }
	}

	if ($action == 'applicantlabels') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/ApplicantLabels.inc';
	    }
	}
	
	if ($action == 'processleademailtemplate') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/ProcessLeadEmailTemplate.inc';
	    }
	}
	
	if ($action == 'applicationleads') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/ApplicationLeads.inc';
	    }
	}

	if ($action == 'irecruittextmessagetemplates') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_VIEWS . 'applicants/IrecruitTextMessageTemplates.inc';
	    }
	}
	
	if ($action == 'applicationnumbers') {
	    if (defined('IRECRUIT_DIR')) {
    		include IRECRUIT_DIR . 'administration/ApplicationNumbers.inc';
	    }
	}
	
	if ($action == 'orgdata') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/OrgData.inc';
	    }
	}
	
	if ($action == 'orgdescription') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/OrgDescription.inc';
	    }
	}
	
	if ($action == 'orgtaxnumbers') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/OrgTaxNumbers.inc';
	    }
	}
	
	if ($action == 'orgcolors') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/OrgColors.inc';
	    }
	}
	
	if ($action == 'orgemail') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/OrgEmail.inc';
	    }
	}
	
	if ($action == 'entrymethods') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/EntryMethods.inc';
	    }
	}
	
	if ($action == 'internalreqtextques') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/InternalRequisitionsTextQuestion.inc';
	    }
	}
	
	
	if ($action == 'socialmedia') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/SocialMedia.inc';
	    }
	}

	if ($action == 'requisitionfeeds') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/RequisitionFeeds.inc';
	    }
	}
	
	if ($action == 'publishreqs') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/PublishRequisitions.inc';
	    }
	}
	
	if ($action == 'portalinfo') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/UserPortalInfo.inc';
	    }
	}
	
	if ($action == 'documentszipdownload') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_VIEWS . 'administration/DocumentsZipDownload.inc';
	    }
	}
	
	if ($action == 'comparativeanalysisconfig') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/ComparativeAnalysisConfig.inc';
	    }
	}
	
	if ($action == 'userportalstepcolors') {
	    if (defined('IRECRUIT_VIEWS')) {
	    	include IRECRUIT_VIEWS . 'administration/UserPortalStepColors.inc';
	    }
	}
	if ($action == 'reports_settings') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/ReportsSettings.inc';
	    }
	}
	if ($action == 'highlight') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/HighlightRequisition.inc';
	    }
	}
	
	if ($action == 'updatelistingsview') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/UpdateListingsView.inc';
	    }
	}
	
	if ($action == 'indeedconfig') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/IndeedConfig.inc';
	    }
	}

	if ($action == 'formquestiondependencies') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/FormQuestionDependencies.inc';
	    }
	}
	
	if ($action == 'onboardquestiondependencies') {
	    if (defined('IRECRUIT_DIR')) {
	    	include IRECRUIT_DIR . 'administration/OnboardQuestionDependencies.inc';
	    }
	}
	
	if ($action == 'applicationformsections') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/ApplicationFormSections.inc';
	    }
	}
	if ($action == 'managetextcapableusers') {
	    if (defined('IRECRUIT_DIR')) {
		include IRECRUIT_DIR . 'administration/ManageTextCapableUsers.inc';
	    }
	}
	
	if ($action == 'portalinfohtml') {
		
		echo '<p><a href="administration.php?action=portalinfo"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 3px -4px 0px;"></a></p>';
		
		// set parameters
		$params = array (
				":OrgID" => $OrgID,
				":UpdateID" => $_GET ['updateid'] 
		);
		// set condition
		$where = array (
				"UpdateID = :UpdateID",
				"OrgID = :OrgID" 
		);
		// get userportal information
		$results = $UserPortalInfoObj->getUserPortalInfo ( "*", $where, '', array (
				$params 
		) );
		
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $PTL ) {
				echo $PTL ['HTML'];
			}
		}
	} // end html
	
	if ($action == 'orglogos') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/OrgLogos.inc';
		}
	}
	
	if ($action == 'useredit') {
		if ($useraction == 'permissions') {
	        	if (defined('IRECRUIT_DIR')) {
				include IRECRUIT_DIR . 'administration/Permissions.inc';
			}
		} else if ($useraction == 'delete') {
			// set where condition
			$where 	=	array ("UserID = :UserID");
			// set parameters
			$params	=	array (":UserID" => $UserIDedit);
			// delete application permissions
			$IrecruitApplicationFeaturesObj->delApplicationPermissions ( $where, array ($params) );
			// Delete Irecruit Users
			$IrecruitUsersObj->delIrecruitUsers ( $where, array ($params) );
			
			$useraction = '';
			$UserIDedit = '';
	        	if (defined('IRECRUIT_DIR')) {
				include IRECRUIT_DIR . 'administration/Users.inc';
			}
		} else {
	        	if (defined('IRECRUIT_DIR')) {
				include IRECRUIT_DIR . 'administration/Users.inc';
			}
		}
	}
	
	if ($action == 'userpreferences') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/UserPreferences.inc';
		}
	}
	
	if ($action == 'scheduleinterviewmail') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/ScheduleInterviewMail.inc';
		}
	}
	
	if ($action == 'activeminutes') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/ActiveMinutes.inc';
		}
	}

	if ($action == 'international') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/Internationalization.inc';
		}
	}
	if ($action == 'hrtoolbox') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/HRToolbox.inc';
		}
	}
	
	if ($action == 'ofccptext') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/Prescreentext.inc';
		}
	}
	
	if ($_REQUEST ['action'] == 'backgroundchecklinks') {
		echo $BackgroundCheckLinksObj->getAdminPage ();
	}
	
	if ($action == 'processflow') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/ApplicantProcessFlow.inc';
		}
	}
	
	if ($action == 'disposition') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/DispositionCodes.inc';
		}
	}
	
	if ($action == 'requisitionstage') {
	        if (defined('IRECRUIT_DIR')) {
	    		include IRECRUIT_DIR . 'administration/RequisitionStage.inc';
		}
	}
	
	if ($action == 'requisitionstagereason') {
	        if (defined('IRECRUIT_DIR')) {
	    		include IRECRUIT_DIR . 'administration/RequisitionStageReason.inc';
		}
	}
	
	if ($action == 'orglevels') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/OrgLevels.inc';
		}
	}
	
	
	if ($action == 'autoforwardlist') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/AutoForwardList.inc';
		}
	}
	
	if ($action == 'filepurpose') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/FilePurposes.inc';
		}
	}
	
	if ($action == 'emailapprovers') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/EmailApprovers.inc';
		}
	}
	
	if ($action == 'emailresponses') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/EmailResponses.inc';
		}
	}
	
	if ($action == 'iconnectemailtemplate') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/IconnectEmailTemplate.inc';
		}
	}
	
	if ($action == 'userportalemailtemplate') {
	        if (defined('IRECRUIT_DIR')) {
	    		include IRECRUIT_DIR . 'administration/UserPortalEmailTemplate.inc';
		}
	}
	
	if ($action == 'datamanagersetup') {
		if (($feature ['DataManagerType'] == "Exporter") || ($feature ['DataManagerType'] == "HRMS")) {
	        	if (defined('IRECRUIT_DIR')) {
				include IRECRUIT_DIR . 'administration/DataManagerExporter.inc';
			}
		}
	}

	if ($action == 'newrequestnotificationconfig') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/NewRequestNotificationConfig.inc';
		}
	}
	
	if ($action == 'onboardquestions') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/OnboardQuestions.inc';
		}
	}
	
	if ($action == 'setupmatrixcare') {
	    if ($feature ['MatrixCare'] == "Y") {
	        if (defined('IRECRUIT_DIR')) {
	        	include IRECRUIT_DIR . 'administration/ConfigMatrixCare.inc';
		}
	    } else {
	        echo '<meta http-equiv="refresh" content="0;url=' . IRECRUIT_HOME . '">';
	    }
	}
	
	if ($action == 'settingforinitialcaps') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/SettingForInitialCaps.inc';
		}
	}
	
	if ($action == 'datamanager') {
		if (($feature ['DataManagerType'] == "Exporter") || ($feature ['DataManagerType'] == "HRMS")) {
			echo '<meta http-equiv="refresh" content="0;url=datamanager/exporter/publish.htm">';
		}
	}
	
	if ($action == 'purchases') {
	        if (defined('IRECRUIT_DIR')) {
			include IRECRUIT_DIR . 'administration/ViewPurchases.inc';
		}
	}
	
	if (($action == "useredit") && ($useraction == "") && ($feature ['BillingPage'] == "Y")) {
		echo '<br><div class="table-responsive"><table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';
		echo '<tr><td colspan="100%"><b>Active Users with Applicant Counts For the Last 3 Months</b></td></tr>';
		
		$table_name   =   "JobApplications JA, Users U, Requisitions R";
		$columns      =   "DATE_FORMAT(MAX(JA.EntryDate), '%M, %Y') 'MonthYear', DATE_FORMAT(max(JA.EntryDate), '%Y-%m') 'Year-Month', DATE_FORMAT(max(JA.EntryDate), '%m') 'Month', count(JA.ApplicationID) 'Count', U.UserID, U.FirstName, U.LastName, U.LastAccess";
		$where        =   array (
            				"JA.OrgID       =   U.OrgID",
            				"JA.OrgID       =   R.OrgID",
            				"JA.OrgID       =   :OrgID",
            				"JA.RequestID   =   R.RequestID",
            				"R.UserID       =   U.UserID",
            				"DATE(JA.EntryDate) > LAST_DAY(DATE_SUB(NOW(), INTERVAL 3 MONTH))" 
                    		);
		$group_by     =   "YEAR(JA.EntryDate), MONTH (JA.EntryDate), U.UserID";
		$order_by     =   "YEAR(JA.EntryDate), MONTH(JA.EntryDate), U.UserID";
		$params       =   array (":OrgID" => $OrgID);

		$results      =   $ApplicationsObj->getJobApplicationsRequisitionsUsersInfo ( $table_name, $columns, $where, $group_by, $order_by, array ($params) );

		
		$i    =   0;
		$moi  =   0;
		$st   =   0;
		$ur   =   0;
		$moi  =   "";
		
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $rpt ) {
				
				if ($rpt ['Month'] != $moi) {
					if ($i > 0) {
						echo '<tr bgcolor="eeeeee"><td align="right"><b>Monthly Total</b>&nbsp;&nbsp;<b>' . $st . '</b></td><td colspan="3"><b>' . $ur . '</b> <i>Active Users</i></td></tr>';
						$st = 0;
						$ur = 0;
					}
					echo '<tr><td colspan="100%">' . $rpt ['MonthYear'] . '</td></tr>';
					echo '<tr>';
					echo '<td align="right"><b>Apps.</b></td>';
					echo '<td><b>User</b></td>';
					echo '<td><b>Name</b></td>';
					echo '<td><b>Last Access</b></td>';
					echo '</tr>';
				}
				
				$moi = $rpt ['Month'];
				$ur ++;
				$st = $st + $rpt ['Count'];
				
				echo '<tr>';
				echo '<td align="right">' . $rpt ['Count'] . '</td>';
				echo '<td>' . $rpt ['UserID'] . '</td>';
				echo '<td>' . $rpt ['FirstName'] . ' ' . $rpt ['LastName'] . '</td>';
				echo '<td>' . $rpt ['LastAccess'] . '</td>';
				echo '</tr>';
				
				$i ++;
			}
		}
		
		echo '<tr bgcolor="eeeeee"><td align="right"><b>Monthly Total</b>&nbsp;&nbsp;<b>' . $st . '</b></td><td colspan="3"><b>' . $ur . '</b> <i>Active Users</i></td></tr>';
		echo '</table></div>';
	} // end Active User Report
} // end if permit
?>
