<div id="page-wrapper">
	<?php 
		if (defined('IRECRUIT_VIEWS')) {
			include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
		}

	?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					   <form name="frmAddressList" id="frmAddressList" method="post">
					        <?php
					        $a = 1;
					        $d = 0;
					        if(count($address_list) > 0) {
                                foreach ($address_list as $address) {
                                    ?>
    					        	<br>
                                    <?php echo "Address" . $a++;?>: <input type="text" name="AddressList[]" value="<?php echo $address['Address'];?>" size="60"> 
                                    &nbsp; Make Default: <input type="radio" name="AddressDefault" value="<?php echo $d; ?>" <?php if($address['Default'] == "Y") echo ' checked="checked"';?> size="60"> 
                                    <br>
    					        	<?php
    					        	$d++;
    					        }
					        }
				        	?>
				        	<br>
                            <?php echo "Address" . $a++;?>: <input type="text" name="AddressList[]" size="60">
                            &nbsp; Make Default: <input type="radio" name="AddressDefault" value="<?php echo $d; ?>" size="60"> 
                            <br><br>
                            <input type="submit" name="btnSubmit" class="btn btn-primary" value="Submit">
					   </form>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
