<style type="text/css">
    .col_sec{display:inline-block;padding:10px;}
    .child_sec input{margin-left:5px;}
    .child_sec input[type="radio"]{margin-right:5px;}
    .add_checklist_section{padding:15px 0 20px 0px;}
    .add_checklist_section a{font-size:16px;cursor:pointer;text-decoration:none;}
    #add_checklist_form_sec{display:none;}
</style>
<div id="page-wrapper">
	<?php 
		if (defined('IRECRUIT_VIEWS')) {
			include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
		}
		if ($_REQUEST['active'] == 'n') { 
			$a = 'n'; 
		} else if ($_REQUEST['active'] == 'p') { 
			$a = 'p'; 
		} else { 
			$a = 'y'; 
		}

	?>
	<div class="add_checklist_section">
		<a class="add_checklist"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/add.png" title="Add" /> Add Checklist</a>
	</div>
	<div class="row" id="add_checklist_form_sec">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="checklist.php" name="frmAddChecklist" id="frmAddChecklist" method="post">
    					<div class="">
    	           			<label for="">Checklist Name</label>
    	           			<input type="text" name="ChecklistName" placeholder="ChecklistName" value="<?php echo $checklist[ChecklistName]; ?>"/>
    	           			<input type="submit" name="btnSubmit" value="Add Checklist" class="btn btn-primary">
    	           		</div>
    	           	</form>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="checklist.php" name="frmAddChecklist" id="frmAddChecklist" method="post">
    					<div class="" style="display:inline-block;">
    	           			<label for="">Apply Checklist at Status:</label>
    	           			<select name="process_order" id="process_order">
    	           				<option value="">Select</option>
    	           				<?php foreach($get_applicant_process_results AS $get_applicant_process){
    	           				    if($get_checklist_process_data['ProcessOrder']==$get_applicant_process['ProcessOrder']){
    	           				        $selected = "selected=selected";
    	           				    }else{
    	           				        $selected = "";
    	           				    }?>
    	           				
    	           					<option value="<?php echo $get_applicant_process['ProcessOrder'];?>" <?php echo $selected; ?>><?php echo $get_applicant_process['Description'];?></option>
    	           				<?php } ?>
    	           			</select>
    	           		</div>
    	           		<div class="" style="display:inline-block;margin-left:20px;">
    	           			<label for="">Make checklist visible to applicants:</label>
    	           			<select name="display-checklist-userportal" id="display-checklist-userportal">
    	           				<option value="">Select</option>
	           					<option value="Yes" <?php echo $get_checklist_process_data['DisplayOnUserPortal']=='Yes'?'selected=selected':''; ?>>Yes</option>
	           					<option value="No" <?php echo $get_checklist_process_data['DisplayOnUserPortal']=='No'?'selected=selected':''; ?>>No</option>
    	           			</select>
    	           		</div>
    	           		<div class="" style="display:inline-block;margin-left:20px;">
    	           			<label for="">Show Checklists:</label>
    	           			<select name="display-active" id="display-active">
	           					<option value="y" <?php echo $a=='y'?'selected=selected':''; ?>>Active</option>
	           					<option value="p" <?php echo $a=='p'?'selected=selected':''; ?>>Pending</option>
	           					<option value="n" <?php echo $a=='n'?'selected=selected':''; ?>>Inactive</option>
    	           			</select>
    	           		</div>
    	           	</form>


				</div>
			</div>
		</div>
	</div>
	
	<?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'copyform'){?>
		<div class="row" id="">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="checklist.php" name="frmAddChecklist" id="frmAddChecklist" method="post">
    					<div class="">
    	           			<label for="">What name do you want to call your new Checklist Name?</label>
    	           			<input type="text" name="NewChecklistName" placeholder="ChecklistName" value="<?php echo $checklist[ChecklistName]; ?>"/>
    	           			<input type="hidden" name="NewChecklistID" value="<?php echo $_REQUEST["ChecklistID"];?>" />
    	           			<input type="hidden" name="action" value="<?php echo $_REQUEST["action"];?>" />
    	           			<input type="hidden" name="active" value="<?php echo $_REQUEST["active"];?>" />
    	           			<input type="submit" name="btnSubmit" value="Copy Form" class="btn btn-primary">
    	           		</div>
    	           	</form>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					   <table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">
					   		<thead>
					   			<tr>
					   				<th style="text-align:left">Checklist Name</th>
					   				<th style="text-align:center">Edit Checklist</th>
					   				<th style="text-align:center">Edit Status Category</th>
					   				<th style="text-align:center">Copy</th>
					   				<th style="text-align:center">Lock</th>
					   				<th style="text-align:center">
									<?php
									if (($a == "n") || ($a == "p")) {
										echo 'Activate';
									} else {
										echo 'Inactivate';
								        }
									?>
									</th>
					   			</tr>
					   		</thead>
					   		<tbody>
					   			<?php 
								if (count($checklist_info) == 0) {
									echo "<tr><td colspan=\"6\">There are no checklists configured.</td></tr>";
								}
					   			foreach ($checklist_info as $checklist) { ?>
    					   			<tr>
    					   				<td style="text-align:left"><span><?php echo $checklist[ChecklistName]; ?></span> <a class="checklist_name" id="checklist_<?php echo $checklist[ChecklistID]; ?>"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/pencil.png" title="Edit Checklist Name" /></a></td>
    					   				<td style="text-align:center">
									<?php
									if ($checklist[Locked] == "N") {
									?>
									<a href="<?php echo IRECRUIT_HOME;?>administration/checklistItems.php?ChecklistID=<?php echo $checklist[ChecklistID]; ?>&active=<?php echo $a; ?>"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/pencil.png" title="Edit Checklist" /></a>
									<?php
									} else {
									  echo "-";
									} 
										if ($checklist[Checklist] == "") {
										  echo " <i>(not set)</i> ";
										}
									?>
									</td>
    					   				<td style="text-align:center">
									<?php
									if ($checklist[Locked] == "N") {
									?>
									<a href="<?php echo IRECRUIT_HOME;?>administration/statusCategories.php?ChecklistID=<?php echo $checklist[ChecklistID]; ?>&active=<?php echo $a; ?>"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/pencil.png" title="Edit Status Category" /></a>
									<?php
									} else {
									  echo "-";
									} 
										if ($checklist[StatusCategory] == "") {
										  echo " <i>(not set)</i> ";
										}
									?>
									</td>
    					   				<td style="text-align:center">
									<a href="<?php echo IRECRUIT_HOME;?>administration/checklist.php?ChecklistID=<?php echo $checklist[ChecklistID]; ?>&action=copyform&active=<?php echo $a; ?>"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/application_cascade.png" title="Copy" /></a>
									</td>
    					   				<td style="text-align:center">
									<?php
									if ($checklist[Locked] == "N") {
									?>
									<a href="<?php echo IRECRUIT_HOME;?>administration/lockChecklist.php?ChecklistID=<?php echo $checklist[ChecklistID]; ?>&active=<?php echo $a; ?>" onclick="return confirm('Are you sure you want to lock this checklist?\n\n - <?php echo $checklist[ChecklistName]; ?>\n\n')"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/lock_edit.png" title="Lock Checklist" /></a>
									<?php
									} else {
									?>
									<a href="<?php echo IRECRUIT_HOME;?>administration/lockChecklist.php?ChecklistID=<?php echo $checklist[ChecklistID]; ?>&unlock=y&active=<?php echo $a; ?>" onclick="return confirm('Are you sure you want to unlock this checklist?\nAny assigned checklists will be affected.\n\n - <?php echo $checklist[ChecklistName]; ?>\n\n')"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/lock_edit.png" title="Unlock Checklist" /></a>

									<?php
									} 
									?>
									</td>
    					   				<td style="text-align:center">
									<?php
									if (($checklist[Active] == "N") || ($checklist[Active] == "P")) {
										if (($checklist[Checklist] != "") && ($checklist[StatusCategory] != "")) {
									?>
									<a href="<?php echo IRECRUIT_HOME;?>administration/activateChecklist.php?activate=y&ChecklistID=<?php echo $checklist[ChecklistID]; ?>&active=<?php echo $a; ?>" onclick="return confirm('Are you sure you want to change the activation for this checklist?\n\n - <?php echo $checklist[ChecklistName]; ?>\n\n')"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/go.png" title="Activate Checklist" /></a>
									<?php
										} else {
										   echo ' - ';
										}

									} else {
									?>
									<a href="<?php echo IRECRUIT_HOME;?>administration/activateChecklist.php?activate=n&ChecklistID=<?php echo $checklist[ChecklistID]; ?>&active=<?php echo $a; ?>" onclick="return confirm('Are you sure you want to change the activation for this checklist?\n\n - <?php echo $checklist[ChecklistName]; ?>\n\n')"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/stop.png" title="Inactivate Checklist" /></a>
									<?php
									} 
									?>
									</td>
    					   			</tr>
					   			<?php } ?>
					   		</tbody>
					   </table>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>

<script type="text/javascript">
$(document).ready(function(){
	
	$(".add_checklist").on("click",function(){
		$("#add_checklist_form_sec").toggle();
	});

	$(".checklist_name").click(function(){
		var get_checklist_id = $(this).attr('id');
		var checklist_arr  = get_checklist_id.split("_");
		var ChecklistID      = checklist_arr[1];
		var checklist_val = $(this).prev().text();
		$(this).parent().html("<input type='text' name='ChecklistName' id='ChecklistName' value='"+checklist_val+"' />");
		$("#ChecklistName").change(function(){
			var ChecklistName = $(this).val();
			console.log(ChecklistName);
			$.ajax({
        		type: "POST",
        		url: "updateChecklistName.php",
        		data:'ChecklistID='+ ChecklistID+'&ChecklistName='+ChecklistName,
        		success: function(data){
        			//$("#ChecklistName").parent().html("<span>"+ChecklistName+"</span> <a class='checklist_name' id='checklist_"+ChecklistID+"'><img src='https://dev.irecruit-us.com/hari/irecruit/images/icons/pencil.png' title='Edit Checklist Name'></a>");
        			location.reload();
        		}
        	});
		});
	});
	
	$("#process_order").change(function(){
		var ProcessOrder = $(this).val();
		$.ajax({
    		type: "POST",
    		url: "updateChecklistProcessOrder.php",
    		data:'ProcessOrder='+ ProcessOrder,
    		success: function(data){
    			location.reload();
    		}
    	});
	});
	
	$("#display-checklist-userportal").change(function(){
		var getVal = $(this).val();
		$.ajax({
    		type: "POST",
    		url: "displayChecklistOnUserPortal.php",
    		data:'getVal='+ getVal,
    		success: function(data){
    			//location.reload();
    		}
    	});
	});
	$("#display-active").change(function(){
		var getVal = $(this).val();
		var url = window.location.href;
    		var qs = "?active=" + encodeURIComponent(getVal);
    		window.location.href = 'checklist.php' + qs;
	});


});
</script>
