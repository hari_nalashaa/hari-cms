<?php
    $UserPortalInfoObj->updOrgUserPortalMainDefaultTheme($OrgID);
    $userportal_app_step_themes         =   $UserPortalInfoObj->getUserPortalApplicationStepThemes();
    
    $up_results =   $userportal_app_step_themes['results'];
    $userportal_app_step_themes_count   =   $userportal_app_step_themes['count'];
    
    if(isset($_REQUEST['rdTheme']) && $_REQUEST['rdTheme'] != "") {
        //Update Org UserPortal Application 
        $UserPortalInfoObj->updOrgUserPortalApplicationDefaultTheme($OrgID, $_REQUEST['rdTheme']);
        
        echo "<script>location.href='administration.php?action=userportalstepcolors&menu=8';</script>";
    }
    
    $org_data_theme_id = $OrganizationDetailsObj->getOrganizationInformation($OrgID, "", "UserPortalThemeID");
?>
Note: Please add theme for userportal application steps.
<form name="frmThemes" id="frmThemes" method="post">
<table class="table table-bordered">
    <tr>
        <th>Theme Name</th>
        <th>Navbar Color</th>
        <th>Pending Tab Color</th>
        <th>Active Tab Color</th>
        <th>Completed Tab Color</th>
        <th>Action</th>
    </tr>
    <?php 
        if($userportal_app_step_themes_count > 0) {
            
        	for($j = 0; $j < $userportal_app_step_themes_count; $j++) {
                ?>
            	<tr>
            	   <td><?php echo $up_results[$j]['Name'];?></td>
            	   <td><div style="background-color:<?php echo "#".$up_results[$j]['Navbar'];?>;color: <?php echo "#".$up_results[$j]['Navbar']?>;width:40px;height:40px"></div></td>
            	   <td><div style="background-color:<?php echo "#".$up_results[$j]['PendingTab'];?>;color: <?php echo "#".$up_results[$j]['PendingTab']?>;width:40px;height:40px"></div></td>
            	   <td><div style="background-color:<?php echo "#".$up_results[$j]['ActiveTab'];?>;color: <?php echo "#".$up_results[$j]['ActiveTab'];?>;width:40px;height:40px"></div></td>
            	   <td><div style="background-color:<?php echo "#".$up_results[$j]['CompletedTab'];?>;color: <?php echo "#".$up_results[$j]['CompletedTab'];?>;width:40px;height:40px"></div></td>
            	   <td>
            	       <input type="radio" name="rdTheme" value="<?php echo $up_results[$j]['ThemeID'];?>" <?php if($org_data_theme_id['UserPortalThemeID'] == $up_results[$j]['ThemeID']) echo ' checked="checked';?> onclick="document.forms['frmThemes'].submit()">
            	   </td>
            	</tr>
            	<?php
            }

        }
        else {
        	?>
        	<tr>
        	   <td class="5">No themes found</td>
        	</tr>
        	<?php
        }
    ?>
</table>
</form>
