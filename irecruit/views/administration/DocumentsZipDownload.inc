<?php 
$zip_options    =   array("resumeupload", "coverletterupload", "otherupload", "custom", "appview", "apphistory", "iconnectforms","checklist");

if(isset($_REQUEST['UpdateOptions']) && $_REQUEST['UpdateOptions'] == 'Update Options') {
	$documents_list    =   $_REQUEST['dzipoptions'];
	$params            =   array("OrgID"=>$OrgID, "UserID"=>$USERID, "DocumentsList"=>json_encode($documents_list));
	
	$IrecruitApplicationFeaturesObj->insDocumentsZipDownloadOptions($params);
}

$get_params     =   array("OrgID"=>$OrgID, "UserID"=>$USERID);
$where_params   =   array("OrgID = :OrgID", "UserID = :UserID");
$doc_option_res =   $IrecruitApplicationFeaturesObj->getDocumentsZipDownloadOptions("DocumentsList", $where_params, "", array($get_params));

if(isset($doc_option_res['results'][0]['DocumentsList'])
	&& $doc_option_res['results'][0]['DocumentsList'] != "") {
	$zip_options = json_decode($doc_option_res['results'][0]['DocumentsList'], true);
}
?>
<form name="frmDocumentsZipDownload" id="frmDocumentsZipDownload" method="post">
<table>
	<tr>
		<td valign="top">Documents Zip Download Options: &nbsp;</td>
		<td>
			<input type="checkbox" name="dzipoptions[]" id="dzip_resume" value="resumeupload" <?php if(in_array('resumeupload', $zip_options)) echo ' checked="checked"'; ?>> Resume <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_coverletter" value="coverletterupload" <?php if(in_array('coverletterupload', $zip_options)) echo ' checked="checked"'; ?>> CoverLetter <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_other" value="otherupload" <?php if(in_array('otherupload', $zip_options)) echo ' checked="checked"'; ?>> Other <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_custom" value="custom" <?php if(in_array('custom', $zip_options)) echo ' checked="checked"'; ?>> Custom <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_app_view" value="appview" <?php if(in_array('appview', $zip_options)) echo ' checked="checked"'; ?>> Application View <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_app_history" value="apphistory" <?php if(in_array('apphistory', $zip_options)) echo ' checked="checked"'; ?>> Applicant History <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_iconnectforms" value="iconnectforms" <?php if(in_array('iconnectforms', $zip_options)) echo ' checked="checked"'; ?>> iConnect Forms <br>
			<input type="checkbox" name="dzipoptions[]" id="dzip_applicant_vault" value="applicant_vault" <?php if(in_array('applicant_vault', $zip_options)) echo ' checked="checked"'; ?>> Applicant Vault <br>
<?php
$checklist_info      =   G::Obj('Checklist')->getCheckListByOrgID($OrgID,'Y');
if ($checklist_info[count] > 0) {
?>

			<input type="checkbox" name="dzipoptions[]" id="dzip_checklist" value="checklist" <?php if(in_array('checklist', $zip_options)) echo ' checked="checked"'; ?>> Checklist<br>
<?php } ?>

		</td>
	</tr>
	<tr>
		<td><input type="submit" class="btn btn-primary" name="UpdateOptions" id="UpdateOptions" value="Update Options"></td>
	</tr>
</table>
</form>
