<div id="page-wrapper">
	<?php 
		if (defined('IRECRUIT_VIEWS')) {
			include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
		}
	?>

	<!-- row -->
	<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">
		      <div class="col-lg-12">
                <?php
echo "<table border='0' width='100%'>";
echo "<tr>";
echo "<th align='left'>Requisition Form Name</th>";
echo "<th align='left'>Form Default</th>";
echo "<th align='left'>Created Date Time</th>";
echo "</tr>";
for($r = 0; $r < $req_forms_count; $r++) {
    $OrgID = $req_forms_list[$r]['OrgID'];
    
    $org_info = $OrganizationDetailsObj->getOrganizationInformation($OrgID, "", "OrganizationName");
    echo "<tr>";
    echo "<td>".$req_forms_list[$r]['RequisitionFormName']."</td>";
    if($req_forms_list[$r]['FormDefault'] == "Y")
    {
        echo "<td>Yes</td>";
    }
    else {
        echo "<td>No</td>";
    }
    echo "<td>".$req_forms_list[$r]['CreatedDateTime']."</td>";
    echo "</tr>";
}
echo "</table>";
?>
</div></div></div></div></div>
