<style>
.links_block {
	float: left;
}
@media(min-width:768px) {
    .sub_section {
        min-height: 320px;
    }
}
</style>
<div id="page-wrapper">
	<?php 
		if (defined('IRECRUIT_VIEWS')) {
			include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
		}
	?>
	<!-- row -->
	<div class="panel panel-default">
		<div class="panel-body">
		    <div class="row">
		      <div class="col-lg-12">
		          <?php 
		          $multi_org_order    =   'MultiOrgID ASC LIMIT 1';
		          if($feature['MultiOrg'] == "Y") {
		              $multi_org_order    =   '';
		          }
		          
		          if($feature['MultiOrg'] == "Y") {
		              echo '<a href="administration.php?action=addorganization&OrgID=' . $OrgID;
		              echo '">';
		              echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px 4px 0px;"><span style="font-size:14px;">Add Organization</span></a><br>';
		          }
		          ?>
		      </div>
		    </div>

				<?php
                // Set where condition
                $where		=	array("OrgID = :OrgID");
                // Set parameters
                $params		=	array(":OrgID" => $OrgID);
                // Get Organization Data Information
                $results	=	G::Obj('Organizations')->getOrgDataInfo("*", $where, $multi_org_order, array($params));
                
                if (is_array($results['results'])) {
                    foreach ($results['results'] as $OD) {
                        
                        echo '<div class="col-lg-3 sub_section">';
                        echo '<h3>Organization</h3>';
                        
                        echo '<b><i>' . $OD['OrganizationName'] . "</i></b><br>";
                        foreach ($orgtitle as $key => $desc) {

                            if ($key != "multiorg") {
                                echo '&nbsp;&nbsp;&nbsp;';
                                echo '<a href="administration.php?action=' . $key . '&OrgID=' . $OD['OrgID'];
                                if ($OD['MultiOrgID'] != "") {
                                    echo '&MultiOrgID=' . $OD['MultiOrgID'];
                                }
                                echo '">';
                                echo $desc;
                                echo '</a><br>';
                            } // end if key
                            
                        } // end foreach
                        
                        echo '</div>';
                    } // end foreach
                }

			    if ($permit ['Admin'] == 1) {
			    	?>
			    	<div class="col-lg-3 sub_section">
					<h3>User</h3>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=useredit&status=1&menu=8">Manage Users</a><br> 
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=activeminutes&menu=8">Active Minutes</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=hrtoolbox&menu=8">HR Toolbox</a><br>
    				</div>
    				
    				<div class="col-lg-3 sub_section">
    					<h3>Applicant Profile Settings</h3>
                        <?php
                        /*
                        removed 2/6/2019 ApplicationID will need to be set manually if a customer wants to update this sequence
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=applicationnumbers&menu=8">ApplicationID</a><br>
                        */
                        ?>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=processflow&menu=8">ApplicantProcess Flow</a><br> 
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=disposition&menu=8">Disposition Codes</a><br>
						<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=applicantsummaryquestions&menu=8">Applicant Summary Questions</a><br> 
						<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=applicantlabels&menu=8">Applicant Labels</a><br> 
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=applicationleads&menu=8">Application Leads</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=processleademailtemplate&menu=8">Process Lead Email Template</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=backgroundchecklinks&menu=8">Background Check Links</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=documentszipdownload&menu=8">DocumentsZip Download Options</a><br> 
    				</div>
    
    				<div class="col-lg-3 sub_section">
    					<h3>Requisition Settings</h3>
    					<a href="<?php echo IRECRUIT_HOME;?>requisitions/requisitionForm.php?menu=8">Requisition Form</a><br> 
    					<a href="<?php echo IRECRUIT_HOME;?>requisitions/requisitionFormVersions.php?menu=8">Requisition Form Versions</a><br> 
                        <?php
                        if ($feature ['RequisitionApproval'] == "Y") {
                            ?><a href="<?php echo IRECRUIT_HOME;?>administration.php?action=emailapprovers&menu=8">Email Approvers</a><br><?php
                        }
                        ?>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=ofccptext&menu=8">Prescreening Questions</a><br>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=autoforwardlist&menu=8">Auto Forward List</a><br> 
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=requisitionstage&menu=8">Requisition Stage</a><br> 
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=requisitionstagereason&menu=8">Requisition Stage Reason</a><br> 
                        <?php
						if ($feature['RequisitionRequest'] == "Y") {
                            ?>
        				    <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=newrequestnotificationconfig&menu=8">New Request Notification Configuration</a><br>
                            <?php
                        }
                        ?>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=highlight&menu=8">Highlight</a><br>
    				</div>
    				
					<div class="col-lg-3 sub_section">
    					<h3>Advertise Requisitions</h3>
                        <?php
                        if ($feature['MonsterAccount'] == "Y") {
                            ?><a href="<?php echo IRECRUIT_HOME;?>monster/setUpMonsterInfo.php?menu=8">Setup Monster</a><br><?php
                        }
			            echo '<a href="' . IRECRUIT_HOME . 'administration.php?action=requisitionfeeds&menu=8">Active Requisition Feeds</a><br>';
                        // Set where condition
                        $where = array("OrgID = :OrgID");
                        // Set parameters
                        $params = array(":OrgID" => $OrgID);
                        // Get Organization Data Information
                        $results = G::Obj('Organizations')->getOrgDataInfo("*", $where, $multi_org_order, array($params));
                        
                        if (is_array($results['results'])) {
                            foreach ($results['results'] as $OD) {
                        
                                echo '<br><b><i>' . $OD['OrganizationName'] . "</i></b><br>";
                        
                                if ($key != "multiorg") {
                                    echo '&nbsp;&nbsp;&nbsp;';
                                    echo '<a href="administration.php?action=socialmedia&menu=8&OrgID=' . $OD['OrgID'];
                                    if ($OD['MultiOrgID'] != "") {
                                        echo '&MultiOrgID=' . $OD['MultiOrgID'];
                                    }
                                    echo '">Social Media Sharing</a>';
                                    
                                    echo '<br>';
                                    echo '&nbsp;&nbsp;&nbsp;';
                                    
                                } // end if key

                                if ($key != "multiorg") {
                                    echo '<a href="administration.php?action=indeedconfig&menu=8&OrgID=' . $OD['OrgID'];
                                    if ($OD['MultiOrgID'] != "") {
                                        echo '&MultiOrgID=' . $OD['MultiOrgID'];
                                    }
                                    echo '">Indeed Configuration</a>';
                                    
                                    echo '<br>';
                                    
                                } // end if key
                                
                            } // end foreach
                        }
                        ?>
    				</div>
    				
                    <?php
    				if($feature['InternalRequisitions'] == "Y") {
    				    ?>
						<div class="col-lg-3 sub_section">
            				<h3>Internal Application Settings</h3>
            				<?php
                            echo '<a href="administration.php?action=internalreqtextques&OrgID=' . $OD['OrgID'];
                            echo '">';
                            echo "Update Question and Text";
                            echo '</a><br>';
                            ?>
        				</div>
    				    <?php
    				}
    				?>

    				<div class="col-lg-3 sub_section">
    					<h3>Email Notifications</h3>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=scheduleinterviewmail&menu=8">Appointment Email Message</a><br>
						<?php
						if (($feature ['WebForms'] == "Y")
						    || ($feature ['AgreementForms'] == "Y")
						    || ($feature ['SpecificationForms'] == "Y")
						    || ($feature ['PreFilledForms'] == "Y")) {
						        
					        if ($permit ['Internal_Forms'] == 1) {
                                ?>
        					    <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=iconnectemailtemplate&menu=8">iConnect Email Template</a><br>
                                <?php
                            }
                            
                        }
                        ?>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=userportalemailtemplate&menu=8">Pending Email Template</a><br>
                        <?php
                        if ($feature['EmailRespond'] == "Y") {
                            ?>
							<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=emailresponses&menu=8">Email Responses to Forwarded Applications</a><br>
                            <?php
                        }                        
						?>
    				</div>

					<div class="col-lg-3 sub_section">
    					<h3>Reports Settings</h3>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=reports_settings&menu=8">Settings</a><br>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=comparativeanalysisquestions&menu=8">Comparative Analysis Questions</a><br>
                    </div>

                    <div class="col-lg-3 sub_section">
    					<h3>Onboarding Settings</h3>
    					<?php 
    					if($feature['MatrixCare'] == 'Y'
                            || $feature['WebABA'] == 'Y'
                            || $feature['Lightwork'] == 'Y'
                            || $feature['Abila'] == 'Y'
                            || $feature['Mercer'] == 'Y'    
                            || $feature['DataManagerType'] != 'N') {

                            $single_onboard_process             =   '';
                            
                            if($feature['MatrixCare'] == 'Y') {
                                $single_onboard_process         =   'MatrixCare';
                            }
                            if($feature['WebABA'] == 'Y') {
                                $single_onboard_process         =   'WebABA';
                            }
                            if($feature['Lightwork'] == 'Y') {
                                $single_onboard_process         =   'Lightwork';
                            }
                            if($feature['Abila'] == 'Y') {
                                $single_onboard_process         =   'Abila';
                            }
                            if($feature['Mercer'] == 'Y') {
                                $single_onboard_process         =   'Mercer';
                            }
                            if($feature['DataManagerType'] != 'N') {
                                $single_onboard_process         =   $feature['DataManagerType'];
                            }
                            ?>
        					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=onboardquestions&OnboardSource=<?php echo $single_onboard_process;?>&menu=8">Onboard Questions</a><br>
                            <?php
					    }
                        if (($feature['DataManagerType'] != "N") && ($feature['DataManagerType'] != "XML")) {
                            ?>
                            <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=datamanagersetup&menu=8">Data Manager Setup</a><br> 
                            <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=datamanager&menu=8">Download Data Manager</a><br>
                            <?php
                        }
                        if($feature['MatrixCare'] == "Y") {
                        	?>
                        	<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=setupmatrixcare&menu=8">Setup Matrix Care</a><br>
                        	<?php
                        }
                        ?>
                        <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=settingforinitialcaps&menu=8">Setting for Initial Caps</a><br>
                    </div>						
    				
    				<div class="col-lg-3 sub_section">
    					<h3>iRecruit TXT Settings</h3>
    					<?php
    					$twilio_account_info	=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

    					if($twilio_account_info['OrgID'] != "") {
    						?>
    						<a href="<?php echo IRECRUIT_HOME;?>twilio/textingAgreement.php?menu=8">Texting Agreement</a><br>
							<a href="<?php echo IRECRUIT_HOME;?>twilio/textingUsage.php?menu=8">Texting Usage</a><br>
    						<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=managetextcapableusers&menu=8">Manage Text Capable Users</a><br>
    						<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=irecruittextmessagetemplates&menu=8">iRecruit TXT Message Templates</a><br>
    						<?php
    					}
    					else {
    					    ?>
        					<a href="http://help.irecruit-us.com/irecruit-txt/" target="_blank">I Want iRecruit TXT</a><br>
        					<?php
    					}
    					?>
    					<a href="<?php echo IRECRUIT_HOME;?>twilio/termsOfService.php?menu=8">Terms of Service</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>twilio/acceptableUserPolicy.php?menu=8">Acceptable Use Policy</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>twilio/privacyPolicy.php?menu=8">Privacy Policy</a><br>
    				</div>

    				<div class="col-lg-3 sub_section">
    					<h3>Other</h3>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=purchases&menu=8">Purchases</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=formquestiondependencies&menu=8">Form Question Dependencies</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=onboardquestiondependencies&menu=8">Onboard Question Dependencies</a><br>
						<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=savecreditcardinfo&menu=8">Save Credit Card Information</a><br>
						<a href="<?php echo IRECRUIT_HOME;?>administration/getOrgAddressList.php?menu=8">Addresses List</a><br>
    					<a href="<?php echo IRECRUIT_HOME;?>public/publicTheme.php?menu=1">Custom Code</a><br>
                    </div>
                    
                    <div class="col-lg-3 sub_section">
    					<h3>Checklist</h3>
    					<a href="<?php echo IRECRUIT_HOME;?>administration/checklist.php?menu=8">Checklist</a><br>
                    </div>

					<?php
                    if ($feature['UserPortal'] == "Y") {
                        ?>
                        <div class="col-lg-3 sub_section">
        					<h3>Userportal</h3>
                            <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=userportalstepcolors&menu=8">Step Colors</a><br>
                        </div>
                        <?php
                    }
			    }
                else {
                	?>
                	<div class="col-lg-12 sub_section">
					<h3>User</h3>
					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=useredit&useraction=profile&UserIDedit=currentuser&menu=8">Manage Login</a><br> 
					<a href="<?php echo IRECRUIT_HOME;?>administration.php?action=userpreferences&menu=8">User Preferences</a><br>
					<!-- <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=archive_requisitions&menu=8">Archive Requisitions</a><br>  -->
					<!-- <a href="<?php echo IRECRUIT_HOME;?>administration.php?action=archive_applications&menu=8">Archive Applications</a><br>  -->
    				</div>
                	<?php
                }
			    ?>
			</div>
		</div>
		<!-- /.panel-body -->
		<!-- /.panel -->
</div>

<script>
$(document).ready(function() {
	$(".menu_links > .col-lg-12").hide();
	$("#user_links").show('slow');
});

function showLinks(menu_id) {
	$(".menu_links > .col-lg-12").hide();
	$("#"+menu_id+"_links").show('slow');
	$(".btn").attr("class", "btn btn-primary");
	$("#"+menu_id).attr("class", "btn");
}
</script>
