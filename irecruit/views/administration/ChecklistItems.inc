<style type="text/css">
    .col_sec{display:inline-block;padding:10px;}
    .child_sec input{margin-left:5px;}
    .child_sec input[type="radio"]{margin-right:5px;}
</style>
<div id="page-wrapper">
	<?php 
		if (defined('IRECRUIT_VIEWS')) {
			include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
		}
	?>
	<div style="float:right;margin-top:5px;margin-bottom:15px;">
		<a href="<?php echo IRECRUIT_HOME;?>administration/checklist.php?active=<?php echo $_REQUEST['active']; ?>">Back to Checklist</a>
	</div>
	<div style="clear:both;"></div>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					   <form action="checklistItems.php" name="frmApplicationFormSections" id="frmApplicationFormSections" method="post">
					        <?php
					        $a = 1;
					        $d = 0;
					        if(count($checklist_items) > 0) {
					            $checklist_count = count($checklist_items);?>
					        	<div class="field_wrapper" id="sort_form_section_titles">
    					           	<div id="category_section">
    					        	<?php 
    					        	foreach ($checklist_items AS $checklist_item) { ?>

        					        	<div id="category_<?php echo $a;?>">
        					           		<div class="col_sec">
        					           			<label for="">Checklist Step</label>
        					           			<input type="text" name="ChecklistItem[]" size="60" placeholder="Checklist" value="<?php echo $checklist_item['ChecklistItem'];?>"/>
        					           		</div>
        					           		<input type="hidden" name="passed_value[]" value="<?php echo $checklist_item['passed_value']; ?>" />
        					           		<img src="<?php echo IRECRUIT_HOME; ?>images/icons/delete.png" class="remove_button" title="Remove" /> Remove
                                        </div>
                                        
        					        	<?php
        					        	$d++;
        					        	$a++;
        					        }?>
            					        
        					       </div>
					         	</div>
					         	
					         	<div style="text-align:center;margin-top:10px;">
                                        	<div class="btn btn-primary" style="inline-block;">
                                            <a href="javascript:void(0);" class="add_button" title="Add field" style="color:#fff;"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/add.png" title="Add" /> Add</a>
                                            </div>
                                            <input type="hidden" name="ChecklistID" value="<?php echo $ChecklistID;?>" />
                                            <input type="hidden" name="active" value="<?php echo $_REQUEST['active'];?>" />
                                            <input type="submit" name="btnSubmit" value="Update" class="btn btn-primary">
                                        </div> 
    					    <?php 
					        }else{ 
					            $checklist_count = 0;
					            ?>
					           <div class="field_wrapper" id="sort_form_section_titles">
					           	<div id="category_section">
					           		<div id="category_1">
    					           		<div class="col_sec">
    					           			<label for="">Checklist Step</label>
    					           			<input type="text" name="ChecklistItem[]" size="60" placeholder="Checklist" value=""/>
    					           			<input type="hidden" name="passed_value[]" value="<?php echo uniqid(); ?>" />
    					           		</div>
                                    </div>
					           	</div>
                                    
                                </div> 
                                <div style="text-align:center;margin-top:10px;">
                                	<div class="btn btn-primary" style="inline-block;">
                                    <a href="javascript:void(0);" class="add_button" title="Add field" style="color:#fff;"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/add.png" title="Add" /> Add</a>
                                    </div>
                                    <input type="hidden" name="ChecklistID" value="<?php echo $ChecklistID;?>" />
                                    <input type="hidden" name="active" value="<?php echo $_REQUEST['active'];?>" />
                                    
                                    <input type="submit" name="btnSubmit" value="Update" class="btn btn-primary">
                                </div>
                                
					        <?php }
				        	?>
					   </form> 
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>

<script type="text/javascript">
$(document).ready(function(){

	$("#sort_form_section_titles #category_section").sortable({
    	cursor: 'move',
    	placeholder: 'ui-state-highlight',
        	stop: function( event, ui ) {
    
        	var forms_data = {
        		forms_info: []
        	};
        		      		   
            $(this).find('tr').each(function(i) {

            	var SortOrder = i;
                SortOrder++;
               
                var ChecklistItemID = $(this).attr('id');
    			var ChecklistItemIDInfo	= ChecklistItemID.split("_");
    			var Checklist_item_ID = ChecklistItemIDInfo[1];
                
                if(typeof(CategoryID) !== 'undefined') {
                	forms_data.forms_info.push({
    					 "ChecklistItemID" : Checklist_item_ID
                   	});
                }
            	
            });
    
        }
    });

    var maxField = 30; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper #category_section'); //Input field wrapper
    
    var x = <?php echo $checklist_count; ?>; //Initial field counter is 1      
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            var y= x-1;
            var uniqid_x = "<?php echo uniqid();?>";console.log(uniqid_x);
            var uniqid1 = "<?php echo uniqid();?>";console.log(uniqid1);
            var fieldHTML = '<div id="category_'+x+'" class="ui-sortable-handle child_sec"><div class="col_sec"><label for="">Checklist Step</label><input type="text" name="ChecklistItem[]" size="60" placeholder="Checklist" value=""/></div><img src="<?php echo IRECRUIT_HOME; ?>images/icons/delete.png" class="remove_button" title="Remove" /></a> Remove</div>'; //New input field html 
 
            $(wrapper).append(fieldHTML); //Add field html
            console.log(x);
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});
</script>
