<style>
#frmForgotPwd .short{
font-weight:bold;
color:#FF0000;
font-size:larger;
}
#frmForgotPwd .weak{
font-weight:bold;
color:orange;
font-size:larger;
}
#frmForgotPwd .good{
font-weight:bold;
color:#2D98F3;
font-size:larger;
}
#frmForgotPwd .strong{
font-weight:bold;
color: limegreen;
font-size:larger;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-16 col-md-6">
			<br>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-16 col-md-6">
			<img border="0" src="images/iRecruit-Logo.png">
		</div>
	</div>

	<div class="row">
		<div style="margin: 0 auto; width: 60%">
			<?php
			if ($username_exists == "false") {
				echo "<br><br><br><br><br><br>";
				echo "<h4>Username doesn't exist in our records</h4>";
			}
			if ($username_exists == "true") {
				echo "<br><br><br><br><br><br>";
				echo "<h4>An email has been sent to the email address associated with your account. <br>Please use the reset password link in the email to update your password.</h4>";
			}
			if ($_GET ['msg'] == 'suc') {
				echo "<br><br><br><br><br><br>";
				echo "<h4>You have successfully updated your password. <br>Please <a href='" . IRECRUIT_HOME . "login.php'>click here to login.</a></h4>";
			}
			if (count ( $_REQUEST ) == 0) {
				echo "<br><br><br><br><br><br>";
				echo "<h4>Sorry you are in wrong page. Please <a href='" . IRECRUIT_HOME . "login.php'>click here to login.</a></h4>";
			}
			if (isset ( $_GET ['ctact'] ) && $_GET ['ctact'] != "" && $user_info_by_session ['count'] == 0) {
				echo "<br><br><br><br><br><br>";
				echo "<h4>Sorry you are in wrong page or your reset password link has expired. Please <a href='" . IRECRUIT_HOME . "login.php'>click here to login.</a></h4>";
			}
			?>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="row">
		<div class="col-sm-6 col-md-4 col-md-offset-4">
		<?php
		if (isset ( $_GET ['ctact'] ) && $_GET ['ctact'] != "" && $user_info_by_session ['count'] > 0) {
			?>
			<div class="account-wall" id="forgot_form">
				<h4 class="text-center login-title">Please enter your new password</h4>
				<br> <span style="color: red"><?php echo $error_message;?></span>
				<form method="post" class="form-signin" id="frmForgotPwd" name="frmForgotPwd">
					<input name="password" id="password" size="23" maxlength="25"
						type="password" class="form-control" placeholder="Password"
						required autofocus> &nbsp;<span id="result"></span> <br> <input name="confirmpassword"
						id="confirmpassword" size="23" maxlength="25" type="password"
						class="form-control" placeholder="Confirm Password" required
						autofocus> <br> <input type="submit" value="Submit"
						class="btn btn-lg btn-primary btn-block"
						name="ProcessForgotPassword" id="ProcessForgotPassword" /> <input
						type="hidden" name="update_password" id="update_password"
						value="Y">
				</form>
			</div>
			<?php
		}
		?>
		</div>
	</div>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<div class="row">
		<div class="col-sm-12 col-md-12" style="text-align: center;">
			<script type="text/javascript">
			$(document).ready(function() {
				$('#password').keyup(function() {
				$('#result').html(checkStrength($('#password').val()))
				})
				function checkStrength(password) {
				var strength = 0
				if (password.length < 6) {
				$('#result').removeClass()
				$('#result').addClass('short')
				return 'too short'
				}
				if (password.length > 7) strength += 1
				// If password contains both lower and uppercase characters, increase strength value.
				if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1
				// If it has numbers and characters, increase strength value.
				if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1
				// If it has one special character, increase strength value.
				if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
				// If it has two special characters, increase strength value.
				if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1
				// Calculated strength value, we can return messages
				// If value is less than 2
				if (strength < 2) {
				$('#result').removeClass()
				$('#result').addClass('weak')
				return 'weak'
				} else if (strength == 2) {
				$('#result').removeClass()
				$('#result').addClass('good')
				return 'good'
				} else {
				$('#result').removeClass()
				$('#result').addClass('strong')
				return 'strong'
				}
				}
				});

			</script>
			<?php echo date('l, F j, Y');?>
			&nbsp;|&nbsp; Powered by <a href="https://www.irecruit-software.com"
				target="_blank">iRecruit</a>. &nbsp;|&nbsp; <a
				href="javascript:bookmarksite('iRecruit', 'https://<?php echo $_SERVER['SERVER_NAME']?>');">Bookmark
				Page</a>
&nbsp;|&nbsp;
Copyright <?php echo date("Y")?> Cost Management Services.
		</div>
	</div>
</div>
