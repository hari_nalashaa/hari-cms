<?php
if ($permit ['Reports'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}
?>
<div style="clear:both;" id="container">Loading...</div>

<script>
visualize({
   auth: {
     name: "<?php echo USER; ?>",
     password: "<?php echo PASS; ?>"
   }
}, function (v) {

 var OrderBy = "ApplicationCount desc";

var report = v.report({
            resource: "/organizations/organization_1/reports/<?echo $Report; ?>",
            params: { "OrgID":["<?echo $OrgID; ?>"],
                  "MultiOrgID":["<?echo $MultiOrgID; ?>"],
                  "Filter":["<?echo $Filter; ?>"],
                  "OrderBy": [OrderBy]
                } ,
    container: "#container"
});

}, function(err){
    alert(err.message);
});

?>
