<?php
//Set Condition
$where      =   array("OrgID = :OrgID");
//Bind the parameters
$params     =   array(':OrgID'=>$OrgID);
$results    =   G::Obj('Correspondence')->getCorrespondenceAttachments(array('UpdateID', 'Filename'), $where, 'Filename', array($params));

if($results['count'] > 0) {
    echo '<tr>';
    echo '<td valign="top" align="right">Attachments:</td>';
    echo '<td>';
    echo '<select name="attachments[]" id="attachments" multiple size="10" STYLE="width: 300px">';
    
    if(is_array($results['results'])) {
        foreach ($results['results'] as $CL) {
            $updateid =   $CL ['UpdateID'];
            $filename =   $CL ['Filename'];
            
            $selected =   '';
            $selected =   G::Obj('Correspondence')->checkIfSelected ( $pkg_info ['Attachments'], $updateid );
            echo '<option value="' . $updateid . '" '.$selected.'>' . $filename . '</option>';
            unset($selected);
        }
    }
    
    echo '</select>';
    echo '</td>';
    echo '</tr>';
}

if (isset($_REQUEST['duplicate']) && $_REQUEST['duplicate'] == "Y") {
	$type = 'duplicate';
	$subbutton = 'Copy Package';
} else if (isset($_REQUEST['edit']) && $_REQUEST['edit'] == "Y") {
	
	$type = 'edit';
	$subbutton = 'Edit Package';
} else if (isset($contact) && $contact == "Y") {
	
	$type = 'contact';
	$subbutton = 'Send Email';
} else {
	
	$type = 'new';
	$subbutton = 'Build New Package';
}

echo <<<END
<tr><td colspan="100%" height="60" valign="middle" align="center">
<input type="hidden" name="movepattern1" value="">
<input type="hidden" name="movepattern2" value="">
<input type="hidden" name="action" value="$action">
<input type="hidden" name="process" value="Y">
<input type="hidden" name="type" value="$type">
<input type="hidden" name="UpdateID" value="$item">
END;

if($results['count'] == 0) {
    echo '<input type="hidden" name="attachments[]" id="attachments" multiple size="10">';
}

if($type == "contact") {
	echo '<input type="button" value="'.$subbutton.'" class="btn btn-primary" onclick="processContactApplicant(this);">';
}
else {
	echo '<input type="submit" value="'.$subbutton.'" class="btn btn-primary">';
}

echo '<br><br><div id="contact_applicant_msg_bottom" style="color:#398ab9"></div>
</td></tr>';
?>