<?php
if ($permit ['Correspondence'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

$action = $_REQUEST['action'];
if(isset($_REQUEST['item'])) $item = $_REQUEST['item'];
if(isset($_REQUEST['UpdateID'])) $UpdateID = $_REQUEST['UpdateID'];

if ($action == '') {
	$action = 'packages';
}

echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered">';
echo '<form method="post" action="' . $_SERVER ['SCRIPT_NAME'] . '">';
echo '<tr><td valign="top"><b>' . $title . '</b></td><td valign="top" align="right">';

$options_packages_list  =   array(
                                "packages"      =>  "Packages",
                                "letters"       =>  "Email Templates",
                                "attachments"   =>  "Attachments"
                            );

echo '<select name="action" onChange="submit()" class="form-control width150">';

foreach($options_packages_list as $package_key=>$package_val) {
    echo '<option value="'.$package_key.'"';
    if ($action == $package_key) {
        echo " selected";
    }
    echo '>'.$package_val.'</option>';
}


echo '</select>';
echo '</td></tr>';
echo '</form>';
echo '</table>';

if ($action == 'packages') {
	include IRECRUIT_DIR . 'views/correspondence/Packages.inc';
}
if ($action == 'letters') {
	include IRECRUIT_DIR . 'views/correspondence/TemplateEmails.inc';
}
if ($action == 'attachments') {
    include IRECRUIT_DIR . 'views/correspondence/Attachments.inc';
}
?>