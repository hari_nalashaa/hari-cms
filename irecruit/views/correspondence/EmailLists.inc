<?php
$submit = 'New Contact';

if ($_REQUEST['process'] == 'Y') {
	
	$user_email_info = array("OrgID"=>$OrgID, "UserID"=>$USERID, "EmailAddress"=>$_REQUEST['emailaddress'], 
					  		 "FirstName"=>$_REQUEST['firstname'], "LastName"=>$_REQUEST['lastname']);
	
	if (($_REQUEST['emailaddress'] != "") && ($_REQUEST['firstname'] != "") && ($_REQUEST['input'] == 'New Contact')) {
		//Insert Email Lists
		$IrecruitUsersObj->insUserEmailLists($user_email_info);
	} else if (isset($_REQUEST['emailaddress']) && isset($_REQUEST['firstname']) && ($_REQUEST['input'] = 'Edit Contact')) {
		
		$user_email_info['OrgEmailAddress'] = $_REQUEST['origemail'];
		//Update UserEmail Lists
		$IrecruitUsersObj->updUserEmailLists($user_email_info);
	}
	
	//Delete User Email Lists
	if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'delete') {
		$IrecruitUsersObj->delUserEmailLists($OrgID, $USERID, $_REQUEST['emailaddress']);
	} // end delete
	
} // end process

if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'edit') {
	
	//Set Email Lists Condition
	$where     =   array("OrgID = :OrgID", "UserID = :UserID", "EmailAddress = :EmailAddress");
	//Bind Parameters
	$params    =   array(':OrgID'=>$OrgID, ':UserID'=>$USERID, ':EmailAddress'=>$_REQUEST['emailaddress']);
	$results   =   $IrecruitUsersObj->getUserEmailLists(array('FirstName', 'LastName'), $where, '', array($params));
	$EDITREC   =   $results['results'][0];
	
	$fn = $EDITREC ['FirstName'];
	$ln = $EDITREC ['LastName'];
	$em = $_REQUEST['emailaddress'];
	$submit = 'Edit Contact';
}

echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
echo '<tr><td colspan="100%"><a href="correspondence.php?action=emaillists"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add</b></a></td></tr>';
echo '<tr><td><b>Name</b></td><td><b>Email Address</b></td><td width="20">&nbsp;</td><td align="center"width="60"><b>Edit</b></td><td align="center" width="60"><b>Delete</b></td></tr>';

//Set Email Lists
$where  = 	array("OrgID = :OrgID", "UserID = :UserID");
//Bind Parameters
$params = array(':OrgID'=>$OrgID, ':UserID'=>$USERID);
//Get User Email Lists
$results = $IrecruitUsersObj->getUserEmailLists(array('FirstName', 'LastName', 'EmailAddress'), $where, 'FirstName', array($params));

$i = 0;
$rowcolor = "#eeeeee";

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
	
		$Name = $row ['FirstName'] . ' ' . $row ['LastName'];
		$Email = $row ['EmailAddress'];
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>' . $Name . '</td>';
		echo '<td><a href="mailto:' . $Email . '">' . $Email . '</a></td>';
		echo '<td>&nbsp;</td>';
		echo '<td align="center">';
        echo '<a href="correspondence.php?action=emaillists&type=edit&emailaddress=' . $Email . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
        echo '</td>';
		echo '<td align="center">';
		echo '<a href="correspondence.php?action=emaillists&process=Y&type=delete&emailaddress=' . $Email . '" onclick="return confirm(\'Are you sure you want to delete the following contact?\n\n' . $Name . ' (' . $Email . ')\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
		echo '</td>';
		echo '</tr>';
		$i ++;
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	}
}

if ($i == 0) {
	echo '<tr><td colspan="100%">There are curently no saved email entries.</td></tr>';
}

echo '<tr><td colspan="100%" height="20">&nbsp;</td></tr>';
echo '</table>';
?>
<form method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>">
	<table border="0" cellspacing="3" cellpadding="3" width="770" class="table table-striped table-bordered">
		<tr>
			<td align="right" width="200">
			     <font style="color: red">*</font>&nbsp;First Name:
            </td>
			<td>
			     <input type="text" name="firstname" size="20" maxlength="30" value="<?php echo $fn;?>">
            </td>
		</tr>
		<tr>
			<td align="right">Last Name:</td>
			<td><input type="text" name="lastname" size="30" maxlength="45" value="<?php echo $ln;?>"></td>
		</tr>
		<tr>
			<td align="right">
                <font style="color: red">*</font>&nbsp; Email Address:
			</td>
			<td>
			     <input type="text" name="emailaddress" size="40" maxlength="60" value="<?php echo $em;?>">
            </td>
		</tr>
		<tr>
			<th colspan="100%" height="60" valign="middle">
    			<input type="hidden" name="process" value="Y"> 
    			<input type="hidden" name="action" value="emaillists"> 
    			<input type="hidden" name="origemail" value="<?php echo $em; ?>"> 
    			<input type="submit" name="input" value="<?php echo $submit;?>" class="btn btn-primary">
			</th>
		</tr>
	</table>
</form>