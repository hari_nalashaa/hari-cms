<?php
include IRECRUIT_VIEWS . 'correspondence/DirectoryStructure.inc';

if ($_REQUEST['process'] == "Y") {
	
	if ($_REQUEST['delete'] == "Y") {
		
		if (isset($item)) {
			/**
			 * @tutorial	Get attachments
			 * @param		Columns, Where Condition, Order By
			 */
			//Set where condition
			$where		=	array("OrgID = :OrgID", "UpdateID = :UpdateID");
			//Bind Parameters
			$params		=	array(':OrgID'=>$OrgID, ':UpdateID'=>$item);
			//Get Correspondence Attachments Information			
			$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('Filename'), $where, '', array($params));
			
			$CAD		=	$results['results'][0];
			$filenamed	=	$attachmentsdir . '/' . $CAD ['Filename'];
			// unlink($filenamed);
			
			//Delete Attachments
			G::Obj('Correspondence')->delCorrespondenceAttachments($OrgID, $item);
		}
	}
	
	if (isset($_REQUEST['type']) && $_REQUEST['type'] == "new" || $_REQUEST['type'] == "replace") {
		
		if ($_FILES ['document'] ['type']) {
			
			if (($UpdateID) && ($_REQUEST['type'] == "replace")) {
				// remove the older file
				$where		=	array("OrgID = :OrgID", "UpdateID = :UpdateID");
				//Bind Parameters
				$params		=	array(':OrgID'=>$OrgID, ':UpdateID'=>$UpdateID);
				//Get correspondence attachments information
				$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('Filename'), $where, '', array($params));

				$CAD	 	=	$results['results'][0];
				$filenamed 	=	$attachmentsdir . '/' . $CAD ['Filename'];

				unlink ( $filenamed );
			}

			//Once the insert or update is done, we have to create file with new UpdateID
			$name = $_FILES ['document'] ['name'];
			$name = preg_replace ( '/\s/', '_', $name );
			$name = preg_replace ( '/\&/', '', $name );
			$data = file_get_contents ( $_FILES ['document'] ['tmp_name'] );
			
			$filename = $attachmentsdir . '/' . $name;
			
			$fh = fopen ( $filename, 'w' );
			if (! $fh) {
			    $ERROR = 'Cannot open file' . $filename;
			    die ( include IRECRUIT_DIR . 'irecruit.err' );
			}
			fwrite ( $fh, $data );
			fclose ( $fh );
			
			chmod ( $filename, 0644 );
			
			//Attachments Information
			$ca_info	=	array("OrgID"=>$OrgID, "UserID"=>$USERID, "Filename"=>$name);
			if ($_REQUEST['type'] == "new") {
				//Insert Correspondence Attachments
				$insert_info    =   G::Obj('Correspondence')->insCorrespondenceAttachments($ca_info);
                $UpdateID       =   $insert_info['insert_id'];
			} else if ($_REQUEST['type'] == "replace") {
				//Update Correspondence Attachments
				G::Obj('Correspondence')->updCorrespondenceAttachments($ca_info, $UpdateID);
			}
		}
	} // end if new or replace
	
	echo "<script>location.href='".IRECRUIT_HOME."correspondence.php?action=attachments&menu=6';</script>";
}

echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
echo '<tr>';
echo '<td><b>File Name</b></td>';
echo '<td width="20">&nbsp;</td>';
echo '<td align="center" width="20"><b>Replace</b></td>';
echo '<td align="center" width="20"><b>Delete</b></td>';
echo '</tr>';

$i = 0;

//Set Attachments Condition
$where		=	array("OrgID = :OrgID");
//Bind Parameters
$params		=	array(':OrgID'=>$OrgID);
//Get Correspondence Attachments Condition
$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('UpdateID', 'Filename'), $where, 'Filename', array($params));

$rowcolor	=	"#eeeeee";

if(is_array($results['results'])) {
	foreach ($results['results'] as $CA) {
	
		$updateid =   $CA ['UpdateID'];
		$file     =   $CA ['Filename'];
		$i ++;
	
		if (isset($_REQUEST['replace']) && ($_REQUEST['replace'] == "Y") && ($item == $updateid)) {
			$type            =   'replace';
			$subtype         =   'Replace Document: ' . $file;
			$updateidedit    =   $updateid;
		}
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>';
		echo '<a href="display_employmentdocument.php?UpdateID=' . $updateid . '">' . $file . '</a>';
		echo '</td>';
		echo '<td>&nbsp;</td>';
		echo '<td align="center">';
		echo '<a href="correspondence.php?action=attachments&replace=Y&item=' . $updateid . '">';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/arrow_rotate_anticlockwise.png" border="0" title="Replace">';
		echo '</a>';
		echo '</td>';
		echo '<td align="center">';
		echo '<a href="correspondence.php?action=attachments&process=Y&delete=Y&item=' . $updateid . '" onclick="return confirm(\'Are you sure you want to delete document?\n\nFile name: ' . $file . '\n\n\')">';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete">';
		echo '</a>';
		echo '</td>';
		echo '</tr>';
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}

if ($subtype == "") {
	$type = 'new';
	$subtype = 'Add New Document';
}

echo <<<END
<tr><td colspan="100%" height="20">&nbsp;</td></tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered">
<form method="post" action="correspondence.php" enctype="multipart/form-data">
<tr><td colspan="100%" valign="bottom">
<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
<input type="file" name="document" size="35"><br><br>
<input type="hidden" name="process" value="Y">
<input type="hidden" name="action" value="attachments">
<input type="hidden" name="type" value="$type">
<input type="hidden" name="UpdateID" value="$updateidedit">
<input type="submit" value="$subtype" class="btn btn-primary">
<input type="submit" value="Clear" class="btn btn-primary">
</td></tr>
</form>
</table>
END;
?>