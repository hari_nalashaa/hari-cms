<?php
if (($_GET['delete'] == "Y") && ($_GET['item'])) {
	G::Obj('Correspondence')->delCorrespondencePackages($OrgID, $_GET['item']);
}

if (isset($_POST['process']) && $_POST['process'] == "Y") {
	
    $companyattachments = '';
    if (is_array ( $attachments )) {
        while ( list ( $key, $val ) = each ( $attachments ) ) {
            $companyattachments .= $val . ':';
        }
    }
    
	//Package name
	if (isset($_POST['packagename']) && $_POST['packagename'] != "") {
		
		/**
		 * @tutorial		Correspondence package information is to insert and update
		 */
		$corres_pkg_info	=	array(
                                    'OrgID'             =>  $OrgID, 
                                    'UserID'            =>  $USERID,
                                    'PackageName'       =>  $_POST['packagename'], 
                                    'Letters'           =>  $_POST['letter'],
                                    'Attachments'       =>  $companyattachments, 
                                    'ProcessOrder'      =>  $_POST['ProcessOrder'],
                                    'DispositionCode'   =>  $_POST['DispositionCode'],
                                    'FromEmail'         =>  $_POST['ddlFromEmail']
                                );
		
		if (isset($_REQUEST['type']) && (($_REQUEST['type'] == "duplicate") || ($_REQUEST['type'] == "new"))) {
			//Insert into correspondence packages
			G::Obj('Correspondence')->insCorrespondencePackages($corres_pkg_info);
		} else if (isset($_REQUEST['type']) && ($_REQUEST['type'] == "edit") && ($UpdateID)) {
			//Update correspondence packages
		    G::Obj('Correspondence')->updCorrespondencePackages($corres_pkg_info, $UpdateID);
		}
	} // end if packagename
} // end if process

include IRECRUIT_DIR . 'views/correspondence/DirectoryStructure.inc';

echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
echo '<tr><td colspan="100%">';
echo '<a href="correspondence.php?action=packages"><img src="'.IRECRUIT_HOME.'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add</b></a>';
echo '</td></tr>';
echo '<tr>';
echo '<td><b>Package Name</b></td>';
echo '<td>&nbsp;</td>';
echo '<td align="center" width="60"><b>Copy</b></td>';
echo '<td align="center" width="60"><b>Edit</b></td>';
echo '<td align="center" width="60"><b>Delete</b></td>';
echo '</tr>';

// create a listing
$where		=	array("OrgID = :OrgID");
$columns	=	"*";

//Bind the parameters to Correspondence packages queries
$params     =   array(':OrgID'=>$OrgID);
$results    =   G::Obj('Correspondence')->getCorrespondencePackages($columns, $where, 'PackageName', array($params));

$rowcolor = "#eeeeee";
if(is_array($results['results'])) {
	foreach ($results['results'] as $CP) {
	
		$updateid     =   $CP ['UpdateID'];
		$packagename  =   $CP ['PackageName'];
		$print        =   'Y';
	
		if (($OrgID == "I20140912") && (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager')) { // Community Research Foundation
			if (substr ( $packagename, 0, 2 ) == 'HR') {
				$print = 'N';
			} // end packagename
		} // end CRF
	
		if ($print == "Y") {
	
			echo '<tr bgcolor="' . $rowcolor . '">';
			echo '<td>';
			echo $packagename;
			echo '</td><td width="20">&nbsp;</td><td align="center">';
	
			echo '<a href="correspondence.php?action=packages&duplicate=Y&item=' . $updateid . '"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a>';
	
			echo '</td><td align="center">';
	
			echo '<a href="correspondence.php?action=packages&edit=Y&item=' . $updateid . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
	
			echo '</td><td align="center">';
	
			echo '<a href="correspondence.php?action=packages&delete=Y&item=' . $updateid . '" onclick="return confirm(\'Are you sure you want to delete the following employment package?\n\nPackage Name: ' . $packagename . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
	
			echo '</td></tr>';
	
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end if print
	} // end while
}


// select data if editing or copy
if (($_REQUEST['duplicate'] == "Y" || $_REQUEST['edit'] == "Y") && isset($_REQUEST['item'])) {
	//Set columns
	$columns  	=	"*";
	//Set Correspondence Package Condition
	$where	  	= 	array("OrgID = :OrgID", "UpdateID = :UpdateID");
	//Bind Parameters
    $params     =   array(':OrgID'=>$OrgID, ':UpdateID'=>$_REQUEST['item']);
	//Get Correspondence Packages Inforamtion
	$cpkg_info  =	G::Obj('Correspondence')->getCorrespondencePackages($columns, $where, '', array($params));
	
	$pkg_info 	=	$cpkg_info['results'][0];
} // end if edit or copy

echo '</table>';

echo '<br><br>';

echo '<table border="0" cellspacing="3" cellpadding="5" align="left" class="table table-striped table-bordered">';
echo '<form method="post" action="correspondence.php">';
echo '<tr>';
echo '<td width="130" align="right">Package Name:</td>';
echo '<td><input type="text" name="packagename" class="form-control width150" value="'.$pkg_info['PackageName'].'" size="25" maxlength="45">';
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td align="right">Template Email:</td>';
echo '<td>';

//Set the columns and condition to fetch the correspondence letters
$columns  	=	array('UpdateID', 'Classification', 'Subject');
$where	  	= 	array("OrgID = :OrgID");

//Bind the parameters
$params         =   array(':OrgID'=>$OrgID);
//Set the fetching order
$order          =   'Classification, Subject';
//Get Correspondence Letters
$corres_letters =   G::Obj('Correspondence')->getCorrespondenceLetters($columns, $where, $order, array($params));

echo '<select name="letter" class="form-control width150">';

if(is_array($corres_letters['results'])) {
	foreach ($corres_letters['results'] as $CL) {
		
		$updateid				= $CL ['UpdateID'];
		$classification 		= $CL ['Classification'];
		$pkg_subject_info 		= $CL ['Subject'];
	
		if ($updateid == $pkg_info ['Letters']) {
			$selected = ' selected';
		} else {
			$selected = '';
		}
		
		if(strlen($pkg_subject_info) > 50) $readmore = substr($pkg_subject_info, 0, 50)."...";
		else $readmore = $pkg_subject_info;
		echo '<option value="' . $updateid . '"' . $selected . '>' . $classification . ' - ' . $readmore . '</option>';
	}	
}

echo '</select>';
echo '</td></tr>';

$ProcessOrder		=	$pkg_info ['ProcessOrder'];
$DispositionCode	=	$pkg_info ['DispositionCode'];

include IRECRUIT_DIR . 'views/applicants/StatusFormNoComment.inc';

$action = "packages";
include IRECRUIT_DIR . 'views/correspondence/SelectPackage.inc';

echo '</form>';
echo '</table>';
?>