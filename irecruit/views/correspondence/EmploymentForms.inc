<?php
include IRECRUIT_VIEWS . 'correspondence/DirectoryStructure.inc';

if (isset($_REQUEST['process']) && $_REQUEST['process'] == "Y") {
	
	if (isset($_REQUEST['delete']) && $_REQUEST['delete'] == "Y") {
		
		if (isset($item)) {
			
			//Set Correspondence Attachment Condition
			$where		=	array("OrgID = :OrgID", "UpdateID = :UpdateID");
			//Bind Parameters
			$params		=	array(':OrgID'=>$OrgID, ':UpdateID'=>$item);
			//Get Correspondence Attachments
			$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('Filename'), $where, '', array($params));
			
			$CAD		=	$results['results'][0];
			
			$filenamed	=	$formsdir . '/' . $CAD ['Filename'];
			
			// unlink($filenamed);
			
			//Delete Correspondence Attachments
			G::Obj('Correspondence')->delCorrespondenceAttachments($OrgID, $item);
		}
	}
	
	if (isset($_REQUEST['type']) && ($_REQUEST['type'] == "new" || $_REQUEST['type'] == "replace")) {
		
		if ($_FILES ['document'] ['type']) {
			
			if (($UpdateID) && ($_REQUEST['type'] == "replace")) {
				
				//Set Correspondence Attachment Condition
				$where		=	array("OrgID = :OrgID", "UpdateID = :UpdateID");
				//Bind Parameters
				$params		=	array(':OrgID'=>$OrgID, ':UpdateID'=>$UpdateID);
				//Get Correspondence Attachments Information
				$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('Filename'), $where, '', array($params));
				
				$CAD		=	$results['results'][0];
				
				$filenamed	=	$formsdir . '/' . $CAD ['Filename'];
				
				unlink ( $filenamed );
			}
			
			$name = $_FILES ['document'] ['name'];
			$name = preg_replace ( '/\s/', '_', $name );
			$name = preg_replace ( '/\&/', '', $name );
			$data = file_get_contents ( $_FILES ['document'] ['tmp_name'] );
			
			$filename = $classdir . '/' . $name;
			
			$fh = fopen ( $filename, 'w' );
			if (! $fh) {
				$ERROR = 'Cannot open file' . $filename;
				echo $fh;exit;
				die ( include 'irecruit.err' );
			}
			fwrite ( $fh, $data );
			fclose ( $fh );
			
			chmod ( $filename, 0644 );
			
			//Attachments Information
			$ca_info	=	array(
								"OrgID"			=>	$OrgID,
								"UserID"		=>	$USERID,
								"TypeDocument"	=>	"forms",
								"Filename"		=>	$name
							);
			
			if (isset($_REQUEST['type']) && ($_REQUEST['type'] == "new")) {
				//Insert Correspondence Attachments
				G::Obj('Correspondence')->insCorrespondenceAttachments($ca_info);
			} else if (isset($_REQUEST['type']) && ($_REQUEST['type'] == "replace")) {
				//Update Correspondence Attachments
				G::Obj('Correspondence')->updCorrespondenceAttachments($ca_info, $UpdateID);
			}
		}
	} // end if new or replace
}

$IRECRUIT_HOME = IRECRUIT_HOME;
echo <<<END
<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">
<tr><td colspan="100%"><a href="correspondence.php?action=forms"><img src="{$IRECRUIT_HOME}images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add</b></a></td></tr>
<tr><td><b>Classification</b></td><td><b>File Name</b></td><td width="20">&nbsp;</td><td align="center" width="20"><b>Replace</b></td><td align="center" width="20"><b>Delete</b></td></tr>
END;

//Set Correspondence Attachment Condition
$where		=	array("OrgID = :OrgID", "TypeDocument = 'forms'");
//Bind Parameters
$params		=	array(':OrgID'=>$OrgID);
//Get Correspondence attachments
$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('UpdateID', 'Filename'), $where, 'Filename', array($params));

$rowcolor	=	"#eeeeee";

if(is_array($results['results'])) {
	foreach ($results['results'] as $CA) {
	
		$updateid 	=	$CA ['UpdateID'];
		$file		=	$CA ['Filename'];
	
		if (($_REQUEST['replace'] == "Y") && ($item == $updateid)) {
			$type			=	'replace';
			$subtype		=	'Replace Document: ' . $file;
			$updateidedit	=	$updateid;
		}
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>';
		echo '<a href="display_employmentdocument.php?UpdateID=' . $updateid . '">' . $file . '</a>';
		echo '</td>';
		echo '<td width="20">&nbsp;</td>';
		echo '<td align="center">';
		echo '<a href="correspondence.php?action=forms&replace=Y&process=Y&item=' . $updateid . '"><img src="' . IRECRUIT_HOME . 'images/icons/arrow_rotate_anticlockwise.png" border="0" title="Replace"></a>';
		echo '</td>';
		echo '<td align="center">';
		echo '<a href="correspondence.php?action=forms&process=Y&delete=Y&item=' . $updateid . '" onclick="return confirm(\'Are you sure you want to delete document?\n\nClassification: ' . $classification . '\nFile name: ' . $file . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
		echo '</td>';
		echo '</tr>';
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}


if ($subtype == "") {
	$type = 'new';
	$subtype = 'Add New Document';
}

echo <<<END
<tr><td colspan="100%" height="20">&nbsp;</td></tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered">
<form method="post" action="correspondence.php" enctype="multipart/form-data">
<tr><td colspan="100%" valign="bottom">
<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
<input type="file" name="document" size="35"><br><br>
<input type="hidden" name="action" value="forms">
<input type="hidden" name="process" value="Y">
<input type="hidden" name="type" value="$type">
<input type="hidden" name="UpdateID" value="$updateidedit">
<input type="submit" value="$subtype" class="btn btn-primary">
<input type="submit" value="Clear" class="btn btn-primary">
</td></tr>
</form>
</table>
END;
?>