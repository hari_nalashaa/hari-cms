<?php
$dir = IRECRUIT_DIR . 'vault/' . $OrgID;

$empdocdir = $dir . '/employmentdocuments';
$formsdir  = $dir . '/employmentdocuments/forms';

if (isset($_REQUEST['classification'])) {
	$classdir = $formsdir . '/' . preg_replace ( '/\s/', '_', $_REQUEST['classification'] );
} else {
	$classdir = $formsdir;
}

$policiesdir = $dir . '/employmentdocuments/policies';
$attachmentsdir = $dir . '/employmentdocuments/attachments';

if (! file_exists ( $dir )) {
	mkdir ( $dir, 0700 );
	chmod ( $dir, 0777 );
}

if (! file_exists ( $empdocdir )) {
	mkdir ( $empdocdir, 0700 );
	chmod ( $empdocdir, 0777 );
}

if (! file_exists ( $formsdir )) {
	mkdir ( $formsdir, 0700 );
	chmod ( $formsdir, 0777 );
}

if (! file_exists ( $policiesdir )) {
	mkdir ( $policiesdir, 0700 );
	chmod ( $policiesdir, 0777 );
}

if (! file_exists ( $classdir )) {
	mkdir ( $classdir, 0700 );
	chmod ( $classdir, 0777 );
}

if (! file_exists ( $attachmentsdir )) {
    mkdir ( $attachmentsdir, 0700 );
    chmod ( $attachmentsdir, 0777 );
}
?>