<?php
$submit = 'Generate New Letter';
$updateidedit = '';
$identifieredit = '';
$subjectedit = '';
$bodyedit = '';

if (isset($item) && ($_REQUEST['delete'] == "Y")) {
	$CorrespondenceObj->delCorrespondenceLetters ( $OrgID, $item );
}

if ($_REQUEST['process'] == "Y") {
	if (isset($_POST['subject']) && isset($_POST['body'])) {
		
		$cl_info = array (
				"OrgID" => $OrgID,
				"UserID" => $USERID,
				"Classification" => $_POST['classification'],
				"Subject" => $_POST['subject'],
				"Body" => $_POST['body'] 
		);
		
		if ($UpdateID) {
			// Update Correspondence Letters based on UpdateID
			$CorrespondenceObj->updCorrespondenceLetters ( $cl_info, $UpdateID );
		} else {
			// Insert Correspondence Letters information
			$CorrespondenceObj->insCorrespondenceLetters ( $cl_info );
		} // end else Update ID
	} // end if subject and body
} // end if process

$IRECRUIT_HOME = IRECRUIT_HOME;
echo <<<END
<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">
<tr><td colspan="100%"><a href="correspondence.php?action=letters"><img src="{$IRECRUIT_HOME}images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add</b></a></td></tr>
<tr><td><b>Classification</b></td><td><b>Subject</b></td><td width="20">&nbsp;</td><td align="center"><b>View</b></td><td align="center"><b>Copy</b></td><td align="center"><b>Edit</b></td><td align="center"><b>Delete</b></td></tr>
END;

//Get Correspondence Letters Information
$columns = 'UpdateID, Classification, Subject, Body';
//Set where condition
$where = array ("OrgID = :OrgID");
//Bind the parameters to Correspondence packages queries
$params = array(':OrgID'=>$OrgID);
//Get correspondence letters
$results = $CorrespondenceObj->getCorrespondenceLetters ( $columns, $where, 'Classification, Subject', array($params) );

$rowcolor = "#eeeeee";

if(is_array($results ['results'])) {
	foreach ( $results ['results'] as $CL ) {
	
		$updateid = $CL ['UpdateID'];
		$classification = $CL ['Classification'];
		$subject = $CL ['Subject'];
		$body = $CL ['Body'];
	
		if (($_REQUEST['edit'] == "Y" || $_REQUEST['duplicate'] == "Y") && ($item == $updateid)) {
	
			if ($_REQUEST['duplicate'] == "Y") {
				$updateidedit = '';
				$submit = 'Create New Letter';
			} else {
				$updateidedit = $updateid;
				$submit = 'Edit Letter';
			}
	
			$classificationedit = $classification;
			$subjectedit = $subject;
			$bodyedit = $body;
		}
	
		$onclickex = " onclick=\"javascript:window.open('" . IRECRUIT_HOME . "correspondence/viewLetter.php?OrgID=" . $OrgID . "&item=" . $updateid;
		if ($AccessCode != "") {
			$onclickex .= "&k=" . $AccessCode;
		}
		$onclickex .= "','_blank','location=yes,toolbar=no,height=400,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>';
		echo $classification;
		echo '</td>';
		echo '<td>';
		echo $subject;
		echo '</td>';
		echo '<td width="20">&nbsp;';
		echo '</td>';
		echo '<td align="center">';
	
		echo '<a href="#"' . $onclickex . '><img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View"></a>';
	
		echo '</td>';
		echo '<td align="center">';
	
		echo '<a href="correspondence.php?action=letters&duplicate=Y&item=' . $updateid . '"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a>';
	
		echo '</td>';
		echo '<td align="center">';
	
		echo '<a href="correspondence.php?action=letters&edit=Y&item=' . $updateid . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a>';
	
		echo '</td>';
		echo '<td align="center">';
	
		echo '<a href="correspondence.php?action=letters&delete=Y&item=' . $updateid . '" onclick="return confirm(\'Are you sure you want to delete the following email letter?\n\nClassification: ' . $classification . '\nSubject: ' . $subject . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
	
		echo '</td>';
		echo '</tr>';
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}


echo <<<END
</table>
<form method="post" action="correspondence.php">
<table border="0" cellspacing="0" cellpadding="4" class="table table-striped table-bordered">
<tr><td colspan="2" height="15">
<P><B>Mail Merge Fields</B></P>
<P>You can use the following mail merge fields in your correspondence to address applicants directly:<br>
Please Note: Mail merge fields must be written exactly as shown above in order to use.</P>
</td></tr>
<tr style="background-color:#eeeeee;border-color:#eeeeee;">
	<th colspan="2" align="left">Insert Code Generator</th>
</tr>
<tr style="background-color:#eeeeee;border-color:#eeeeee;">
	<td>Applicant Data:
		<select name="codegenrator" id="codegenrator" onchange="$('#codevalue').html(this.value);">
			<option value="">Select</option>
			<option value="{first}">First</option>
			<option value="{middle}">Middle</option>
			<option value="{last}">Last</option>
			<option value="{address}">Address</option>
			<option value="{address2}">Address2</option>
			<option value="{city}">City</option>
			<option value="{state}">State</option>
			<option value="{zip}">Zip</option>
			<option value="{country}">Country</option>
			<option value="{ApplicationID}">ApplicationID</option>
			<option value="{ApplicationDate}">ApplicationDate</option>
			<option value="{JobTitle}">JobTitle</option>
		</select>
		<span id="codevalue"></span>
</tr>
</table>
<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">
<tr><td valign="top" align="right">Classification:</td><td><input type="text" name="classification" value="$classificationedit" size="40" maxlength="45"></td></tr>
<tr><td valign="top" align="right">Subject:</td><td><input type="text" name="subject" value="$subjectedit" size="40" maxlength="255"></td></tr>
<tr><td valign="top" align="right">Body:</td><td><textarea name="body" cols="75" rows="15" class="mceEditor">$bodyedit</textarea></td></tr>
<tr><td>
<tr><td colspan="100%" height="60" valign="middle" align="center">
<input type="hidden" name="action" value="letters">
<input type="hidden" name="UpdateID" value="$updateidedit">
<input type="hidden" name="process" value="Y">
<input type="submit" value="$submit" class="btn btn-primary">
</td></tr>
</table>
</form>
END;
?>