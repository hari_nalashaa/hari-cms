<?php 
echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered table-hover">';
echo '<tr><td valign="top" width="80%"><b>';
echo "Applicant Management";
echo '</b></td><td valign="top" align="left">';


//str_replace
echo '<form method="post" action="' . str_replace('appointments/scheduleAppointment.php', 'applicants.php', $_SERVER ['SCRIPT_NAME']) . '">';
echo '<select name="action" onChange="submit()" class="form-control">';

$iii = 0;


echo '<option value="search"';
if ($action == "search") {
	echo " selected";
	$iii ++;
}
echo '>Applicant Search</option>';

if ($feature ['WeightedSearch'] == "Y") {
	echo '<option value="weightedsearch"';
	if ($action == "weightedsearch") {
		echo " selected";
		$iii ++;
	}
	echo '>Weighted Search</option>';
}
echo '<option value="status"';
if ($action == "status") {
	echo " selected";
	$iii ++;
}
echo '>Applicants by Status</option>';

echo '<option value="maintainsearch"';
if ($action == "maintainsearch") {
	echo " selected";
	$iii ++;
}
echo '>Maintain Saved Searches</option>';

if($action == "scheduleinterview") {
	echo '<option value="scheduleinterview" selected>Schedule Interview</option>';
	$iii ++;
}

echo '<option value="calendartableview"';
if ($action == "remindappointments" || $action == "manageappointments" || $action == "calendartableview" || $action == "calendarlistview") {
	echo ' selected="selected"';
	$iii++;
}
echo '>Appointment Calendar</option>';

if ($iii == 0) {
	echo '<option value="" selected>' . $subtitle [$action] . '</option>';
}

echo '</select>';

if ($action == "manageappointments" || $action == "calendartableview" || $action == "calendarlistview" || $action == "remindappointments") {
	echo '<br><select name="appointmentaction" id="appointmentaction" onChange="submit()" style="margin-top:10px">';
	echo '<option value="calendartableview"';
	if($action == 'calendartableview') echo ' selected="selected"';
	echo '>Table View</option>';
	echo '<option value="calendarlistview"';
	if($action == 'calendarlistview') echo ' selected="selected"';
	echo '>List View</option>';
	echo '</select>';
}
	


echo '</td></tr>';
echo '</form>';
echo '</table>';
?>
