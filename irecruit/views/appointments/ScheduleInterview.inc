<form name="frmScheduleInterview" id="frmScheduleInterview" method="post">
<!--  onsubmit="return validateAppointmentScheduling()" -->
	<?php
		if($isbcc == 'Y') {
			?>
			<input type="hidden" name="sibcc" id="sibcc" value="<?php echo $templateinfo['Bcc'];?>"/>
			<?php
		}
	?>
	<input type="hidden" name="sifrom" id="sifrom" value="<?php echo $UserEmail;?>"/>
	<input type="hidden" name="ApplicationID" id="ApplicationID" value="<?php echo $ApplicationID;?>">
	<input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>">
	<table border="0" width="100%" cellpadding="5" cellspacing="5" class="table table-striped table-bordered">
		<tr><td colspan="2"><span id="si_loading_top"></span></td></tr>
		<tr>
			<td colspan="2"><b>Schedule Interview</b></td>
		</tr>
		<tr>
			<td colspan="2">
				<?php
				//global $user_preferences;
				$timezoneinfo = $AppointmentsObj->defaultTimeZone($user_preferences['DefaultTimeZone']);
				?>
				<strong>Note*:</strong> Based on your user preferences settings, your timezone is <?php echo $timezoneinfo[2];?>.
				So your appointment is going to be created with <?php echo $timezoneinfo[2];?> timezone.
			</td>
		</tr>
		<tr>
			<td width="20%">Recipient</td>
			<td><span id="sche_intw_first_last"><?php echo $First." ".$Last;?></span></td>
		</tr>
		<tr>
			<td>Recipient Email Address</td>
			<td><a href="mailto:<?php echo $ApplicantEmail;?>" id="sche_intw_email"><?php echo $ApplicantEmail;?></a></td>
		</tr>
		<tr>
			<td>Date & Time</td>
			<td>
			<div class="div-sidate div-date-time-block">
				<input name="sidate" id="sidate" value="<?php if($_REQUEST['hidescheduleinterview'] == '') echo $_REQUEST['sidate'];?>" 
				class="form-control input-text" style="width: 100px;float:left;">
			</div>
			<div class="div-sihrs-simins-siampm div-date-time-block">
			<select name="sihrs" id="sihrs" class="form-control drop-down80">
				<?php
				for($h = 1; $h <= 12; $h ++) {
					if($_REQUEST['hidescheduleinterview'] == '') 
						$sihrssel = ($_REQUEST['sihrs'] == $h) ? 'selected="selected"' : '';
					$h = ($h < 10) ? '0'.$h : $h;
					?><option value="<?php echo $h;?>" <?php echo $sihrssel;?>><?php echo $h;?></option><?php
					unset($sihrssel);
				}
				?>
			</select>
			</div>
			<div class="div-sihrs-simins-siampm div-date-time-block">
			<select name="simins" id="simins" class="form-control drop-down80">
			<?php
			for($m = 0; $m <= 60; $m += 5) {
				if($_REQUEST['hidescheduleinterview'] == '')
					$siminssel = ($_REQUEST['simins'] == $m) ? 'selected="selected"' : '';
				$m = ($m == 0) ? '00' : $m;
				?><option value="<?php echo str_pad($m,2,"0",STR_PAD_LEFT);?>" <?php echo $siminssel;?>><?php echo str_pad($m,2,"0",STR_PAD_LEFT);?></option><?php
				unset($siminssel);
			}
			?>
			</select>
			</div>
			<div class="div-sihrs-simins-siampm div-date-time-block">
			<select name="siampm" id="siampm" class="form-control drop-down80">
				<option value="AM" <?php if($_REQUEST['hidescheduleinterview'] == '' && $_REQUEST['siampm'] == 'AM') echo 'selected="selected"';?>>AM</option>
				<option value="PM" <?php if($_REQUEST['hidescheduleinterview'] == '' && $_REQUEST['siampm'] == 'PM') echo 'selected="selected"';?>>PM</option>
			</select>
			</div>
			<div class="div-siest div-date-time-block">
			<?php 
			echo $timezoneinfo[2];
			?>
			</div>
			</td>
		</tr>
		<tr>
			<td>Subject</td>
			<td><input type="text" name="sisubject" id="sisubject" value="<?php if($_REQUEST['hidescheduleinterview'] == '') echo $_REQUEST['sisubject'];?>" class="form-control input-text width-auto"></td>
		</tr>
		<tr>
			<td>&nbsp;</td><td><span style="color:brown;font-size:10px;">Note*: This address will be linked to Google Map in Email Body</span></td>
		</tr>
		<tr>
			<td>Location</td>
			<td>
			     <?php 
			     $org_address_info   =   G::Obj('OrgAddressList')->getAddressList($OrgID);
			     $address_list       =   json_decode($org_address_info['AddressList'], true);
			     ?>
				<select name="ddlLocation" id="ddlLocation" onchange="$('#silocation').val(this.value)" class="form-control drop-down margin-right5 width-auto pull-left" style="width:150px;">
					<option value="">Select</option>
                    <?php
                    $default_address    =   "";
                    $a = 1;
                    if(count($address_list) > 0) {
                        for ($i = 0; $i < count($address_list); $i++) {
                            
                            if($address_list[$i]['Default'] == "Y")
                            {
                                $default_address    =   $address_list[$i]['Address'];
                            }
                            ?>
                            <option value="<?php echo $address_list[$i]['Address'];?>" <?php if($address_list[$i]['Default'] == "Y") echo ' selected="selected"';?>>
                                <?php echo $address_list[$i]['Address'];?>
                            </option> 
                        	<?php
                        }
                    }
                    ?>
					<option value="">Other</option>
				</select>
				<?php
    				if($_REQUEST['hidescheduleinterview'] == '') $si_address = $_REQUEST['silocation'];
                    if($default_address != "") $si_address = $default_address;
				?>
				<input type="text" name="silocation" id="silocation" value="<?php echo $si_address; ?>" class="form-control input-text width-auto pull-left margin-2px-left">
			</td>
		</tr>
		<tr>
			<td>Status</td>
			<td>
				<select name="sistatus" id="sistatus" class="form-control drop-down width-auto">
					<option value="">Please Select</option>
					<?php 
						for($sl = 0; $sl < count($statuslist); $sl++) {
							$slselected = ($statuslist[$sl]['ProcessOrder'] == $_REQUEST['sistatus']) ? 'selected="selected"' : '';
							?><option value="<?php echo $statuslist[$sl]['ProcessOrder']?>" <?php echo $slselected;?>><?php echo $statuslist[$sl]['Description']?></option><?php
							unset($slselected);
						}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Disposition Code</td>
			<td>
				<select name="sidispositioncode" id="siispositioncode" class="form-control drop-down width-auto">
					<option value="">Select a Code</option>
					<?php 
					    for($dc = 0; $dc < count($dispositioncodes); $dc++) {
							$dcselected = (isset($_REQUEST['siispositioncode']) && ($dispositioncodes[$sl]['Code'] == $_REQUEST['siispositioncode'])) ? 'selected="selected"' : '';
							?><option value="<?php echo $dispositioncodes[$dc]['Code']?>" <?php echo $dcselected?>><?php echo $dispositioncodes[$dc]['Description']?></option><?php
							unset($dcselected);
						}
					?>
				</select>
			</td>
		</tr>
		<?php 
		//Set where condition
		$where = array("OrgID = :OrgID", "UserID = :UserID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID,  ":UserID"=>$USERID);
		
		$resuseremaillists = $IrecruitUsersObj->getUserEmailLists("*", $where, 'FirstName', array($params));
		if($resuseremaillists['count'] > 0) {
			?>
			<tr>
				<td valign="top">Cc</td>
				<td>
					<select name="sicc[]" id="sicc" multiple="multiple" class="form-control drop-down width-auto">
						<?php
						if(is_array($resuseremaillists['results'])) {
							foreach($resuseremaillists['results'] as $rowemaillist) {
								$siccsel = "";
								if(is_array($_REQUEST['sicc']) 
									&& in_array($rowemaillist['FirstName']." ".$rowemaillist['LastName']." - ".$rowemaillist['EmailAddress'], $_REQUEST['sicc'])) $siccsel = 'selected="selected"';
								?><option value="<?php echo $rowemaillist['OrgID']."**-**".$rowemaillist['UserID']."**-**".$rowemaillist['EmailAddress'];?>" <?php echo $siccsel;?>><?php echo $rowemaillist['FirstName']. " " .$rowemaillist['LastName']." - ".$rowemaillist['EmailAddress']?></option><?php
								unset($siccsel);
							}
						}
						?>
					</select>
				</td>
			</tr>
			<?php
		}
		?>
		
		<tr>
			<td>Body Template</td>
			<td>
				<?php 
				if (preg_match ( '/scheduleAppointment.php$/', $_SERVER ["SCRIPT_NAME"] )) {
					$body_temp_callback_url = "getScheduleInterviewEmailTemplate.php";
				}
				else if (preg_match ( '/applicantsSearch.php$/', $_SERVER ["SCRIPT_NAME"] )) {
					$body_temp_callback_url = "appointments/getScheduleInterviewEmailTemplate.php";
				}
				?>
				<select name="emailbodytemplate" id="emailbodytemplate" onchange="modifyEmailBody('<?php echo $body_temp_callback_url;?>', this.value)" class="form-control drop-down width-auto">
					<option value="at" <?php if($_REQUEST['emailbodytemplate'] == 'at') echo 'selected="selected"';?>>Appointment Template</option>
					<?php 
					//Set parameters
					$params = array(":OrgID"=>$OrgID);
					//Set where condition
					$where = array("OrgID = :OrgID");
					//Get Correspondence Letters Information
					$results = $CorrespondenceObj->getCorrespondenceLetters('UpdateID, Classification', $where, 'Classification, Subject', array($params));
					
					if(is_array($results)) {
						foreach($results['results'] as $row) {
							?><option value="<?php echo $row['UpdateID'];?>" <?php if($_REQUEST['emailbodytemplate'] == $row['UpdateID']) echo 'selected="selected"';?>><?php echo $row['Classification'];?></option><?php
						}
					}					
					?>
				</select>
			</td>
		</tr>
		
		<tr>
			<td valign="top">Email Body</td>
			<td>
				<textarea rows="8" cols="70" name="sidescription" id="sidescription" wrap="virtual" class="mceEditor"><?php echo $message;?></textarea>
			</td>
		</tr>
		
		<tr>
			<td colspan="2" align="right"><a href="javascript:void(0)" onclick="$('#viewmessage').toggle();">Sample Email Format</a></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td>
				<div id="viewmessage">
					<?php 
						$viewmessage = $message;
						
						if($_REQUEST['emailbodytemplate'] == 'at' || $_REQUEST['emailbodytemplate'] == '')
						{
							$calendarsection = "<p><span style=\"font-size: 10pt;\">Please click on icon below to add this appointment to your calendar.</span></p><p>{CalendarIcons}</p><p>or</p><p>{Location}</p><p>{RequestNewTime}</p><p>&nbsp;</p>";
						}
						else {
							$calendarsection = '<p><span style="color: #7aa1e6; font-weight: bold;">Appointment Confirmation:</span> <br /><br /><span style="font-size: 10pt;">Your appointment is confirmed</span><br /> <br /><span style="font-size: 10pt;">Date:</span> {Date}<br /><span style="font-size: 10pt;">Time:</span> {Time}<br /><span style="font-size: 10pt;">Instructions:</span> Please give me a call at the appointed time.<br /><br /></p>
							<p><span style="font-size: 10pt;">Please click on icon below to add this appointment to your calendar.</span></p>
							<p><span style="display: block;">{CalendarIcons}<br /> </span></p>
							<p><span style="font-size: 10pt;">&nbsp;</span></p>
							<p><span id="codevalue">{Location}</span></p>
							<p><span id="codevalue">{RequestNewTime}</span></p>';
						}
						
						$viewmessage = str_replace("{CalendarSection}", $calendarsection, $viewmessage);
						
						if($templateinfo['GoogleCalendar'] == '1') $calendaricons['{AddToGoogleCalendar}'] = '<a href={AddToGoogleCalendar}><img src="'.IRECRUIT_HOME.'images/google.png" alt="Google" style="margin-right:10px;" /></a>';
						if($templateinfo['OutlookCalendar'] == '1') $calendaricons['{AddToOutlookCalendar}'] = '<a href={AddToOutlookCalendar}><img src="'.IRECRUIT_HOME.'images/outlook.png" alt="Outlook" style="margin-right:10px;" /></a>';
						if($templateinfo['IcalCalendar'] == '1') $calendaricons['{AddToIcalCalendar}'] = '<a href={AddToLotusCalendar}><img src="'.IRECRUIT_HOME.'images/lotus.png" alt="Lotus" style="margin-right:10px;" /></a>';
						if($templateinfo['LotusCalendar'] == '1') $calendaricons['{AddToLotusCalendar}'] = '<a href={AddToIcalCalendar}><img src="'.IRECRUIT_HOME.'images/ical.png" alt="Ical" style="margin-right:10px;" /></a>';
						
						if(is_array($calendaricons)) $selectedcicons = implode("\n", $calendaricons);
						
						$viewmessage = str_replace("{CalendarIcons}", $selectedcicons, $viewmessage);
						
						$viewmessage = str_replace("{RequestNewTime}", '<a href={RequestNewTime}>Request New Time</a>', $viewmessage);
						$viewmessage = str_replace("{Location}", '<a href={Location}>Click Here To See The Location In Google Map</a>', $viewmessage);
						//echo html_entity_decode(str_replace("&nbsp;", " ", $viewmessage));
						echo html_entity_decode($viewmessage);
					?>
				</div>
			</td>
		</tr>
		
		<tr>
			<td align="left" colspan="2">
				<?php
				if (preg_match ( '/scheduleAppointment.php$/', $_SERVER ["SCRIPT_NAME"] )) {
					$sche_intw_callback_url = "processAppointmentInfo.php";
				}
				else if (preg_match ( '/applicantsSearch.php$/', $_SERVER ["SCRIPT_NAME"] )) {
					$sche_intw_callback_url = "appointments/processAppointmentInfo.php";
				}
				?>
				<input type="button" name="scheduleinterview" id="scheduleinterview" onclick="scheduleAppointment('<?php echo $sche_intw_callback_url;?>')" value="Schedule Interview and Send Email" class="<?php if($BOOTSTRAP_SKIN) echo 'btn btn-primary'; else echo 'custombutton';?>"/>
				<span id="si_loading_bottom"></span>
			</td>
		</tr>

	</table>
</form>
