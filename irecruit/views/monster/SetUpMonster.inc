<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<?php
                    require_once COMMON_DIR . 'listings/SocialMedia.inc';
                    
                    $MonsterRecords = $MonsterObj->getMonsterInfoByOrgID($OrgID, $MultiOrgID);
                    $MonsterInfo 	= $MonsterRecords['row'];
                    
                    if($MonsterInfo['RecruiterReferenceUserName'] != "" && $MonsterInfo['Password'] != "") {
                    	include_once IRECRUIT_DIR . 'monster/MonsterPaidCredentialsCheck.inc';
                    }
                    
                    include IRECRUIT_DIR . 'monster/MonsterInformation.inc';
                    ?>
					<!-- /.row (nested) -->
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>