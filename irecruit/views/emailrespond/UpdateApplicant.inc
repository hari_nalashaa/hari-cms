<?php
if (($_REQUEST ['ApplicationID']) && ($_REQUEST ['RequestID']) && ($_REQUEST ['type'])) {

	$OrgID         =   $_REQUEST ['OrgID'];
	$ApplicationID =   $_REQUEST ['ApplicationID'];
	$RequestID     =   $_REQUEST ['RequestID'];
	$type          =   $_REQUEST ['type'];
	$comment       =   $_REQUEST['comment'];
	
	// Set parameters
	$params = array (":OrgID" => $OrgID);
	// Set where condition
	$where = array ("OrgID = :OrgID");
	// Get EmailResponses Information
	$results = G::Obj('Email')->getEmailResponsesInfo ( "*", $where, "", array ($params) );
	// Get EmailResponses Information
	$ER = $results ['results'] [0];

	if ($ER ['FormComments'] == "Y") {
		
		if ($process == "Y") {
			processApproval ( $OrgID, $ProcessOrder, $DispositionCode, $ApplicationID, $RequestID, $comment, $type, $ER, $fes );
		} else { // else process
			
			$APPDATA = $ApplicantsObj->getAppData ( $OrgID, $ApplicationID );
			
			$multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
			// Get JobTitle Information
			$Title = G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
			
			echo '<div style="width:600px;margin-left: 50px;margin-top:20px;">';
			echo '<form method="POST" action="' . IRECRUIT_HOME . 'emailrespond/updateApplicant.php">';
			
			echo 'ApplicationID: ' . $ApplicationID . ' for ' . $Title . "<br><br>";
			echo $APPDATA ['first'] . ' ' . $APPDATA ['last'] . "<br>";
			echo $APPDATA ['address'] . "<br>";
			echo $APPDATA ['city'] . ', ' . $APPDATA ['state'] . '&nbsp;&nbsp;' . $APPDATA ['zip'] . '&nbsp;&nbsp;' . $APPDATA ['country'] . "<br><br>";
			
			echo 'Comment:<br><input type="text" name="comment" value="" size="60" maxlength="255">';
			echo '<br><br>';
			echo '<input type="hidden" name="OrgID" value="' . htmlspecialchars($OrgID) . '">';
			echo '<input type="hidden" name="ApplicationID" value="' . htmlspecialchars($ApplicationID) . '">';
			echo '<input type="hidden" name="RequestID" value="' . htmlspecialchars($RequestID) . '">';
			echo '<input type="hidden" name="process" value="Y">';
			echo '<input type="hidden" name="fes" id="fes" value="' . $fes . '">';
			echo '</div>';
			echo '<div style="width:600px;margin-left: 100px;margin-bottom:80px;">';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;';
			echo '<input type="submit" name="type" value="Accept">';
			echo '&nbsp;&nbsp;&nbsp;&nbsp;';
			echo '<input type="submit" name="type" value="Reject">';
			echo '</form>';
			echo '</div>';
		} // end process
	} else {
		
		if (($type == "Reject") || ($type == "Accept")) {
			processApproval ( $OrgID, $ProcessOrder, $DispositionCode, $ApplicationID, $RequestID, '', $type, $ER, $fes );
		}
	} // end formcomment
} // end if REQUEST

function processApproval($OrgID, $ProcessOrder, $DispositionCode, $ApplicationID, $RequestID, $comment, $type, $ER, $fromemailsource) {
	global $PHPMailerObj;
	
	$fromemailsource = base64_decode($fromemailsource);
	
	if ($type == "Accept") {
		
		$ProcessOrder = $ER ['ProcessOrder'];
		$DispositionCode = $ER ['DispositionCode'];
	} else if ($type == "Reject") {
		
		$ProcessOrder = $ER ['RejectProcessOrder'];
		$DispositionCode = $ER ['RejectDispositionCode'];
	}
	
	// Set Update Information
	$set_info = array (
			"ProcessOrder        = :ProcessOrder",
			"DispositionCode     = :DispositionCode",
			"StatusEffectiveDate = NOW()" 
	);
	// Set where condition
	$where = array (
			"OrgID               = :OrgID",
			"ApplicationID       = :ApplicationID",
			"RequestID           = :RequestID" 
	);
	// Set parameters
	$params = array (
			":OrgID"             => $OrgID,
			":ApplicationID"     => $ApplicationID,
			":RequestID"         => $RequestID,
			":ProcessOrder"      => $ProcessOrder,
			":DispositionCode"   => $DispositionCode 
	);
	// Update JobApplications Information
	G::Obj('Applications')->updApplicationsInfo ( 'JobApplications', $set_info, $where, array ($params) );
	
	// Insert into ApplicantHistory
	$Comments = "Status update from email link.<br>";
	$Comments .= "Updated by: ".$fromemailsource;
	if ($comment != "") {
		$Comments .= "<br><br>" . $comment;
	}
	
	$ReqMultiOrgID =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
	//Get Email and EmailVerified
	$OE            =   G::Obj('OrganizationDetails')->getEmailAndEmailVerified($OrgID, $ReqMultiOrgID);
	//Get Org Name
	$OrgName       =   G::Obj('OrganizationDetails')->getOrganizationName($OrgID, $ReqMultiOrgID);
	
	// Multiple recipients
	$to = $OE ['Email']; // note the comma
	
	// Subject
	$subject = 'Process order updated';
	
	// Message
	$message = 'Hi,';
	$message .= '<br><br>';
	$message .= 'Applicant status has been update by user email: '.$fromemailsource;
	$message .= '<br>ApplicationID: '.$ApplicationID;
	$message .= '<br><br>Thanks<br>iRecruit Support Team.';
	
	if($to != "" && $OrgID == "I20180301") {

        //Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
	    
	    // Set who the message is to be sent to
	    $PHPMailerObj->addAddress ( $to );
	    // Set the subject line
	    $PHPMailerObj->Subject = $subject;
	    // convert HTML into a basic plain-text alternative body
	    $PHPMailerObj->msgHTML ( $message );
	    // Content Type Is HTML
	    $PHPMailerObj->ContentType = 'text/html';
	    //Send email
	    $PHPMailerObj->send ();	     
	}
	
	//Job Application History Information
	$job_app_history = array (
			"OrgID"              =>  $OrgID,
			"ApplicationID"      =>  $ApplicationID,
			"RequestID"          =>  $RequestID,
			"ProcessOrder"       =>  $ProcessOrder,
			"DispositionCode"    =>  $DispositionCode,
			"StatusEffectiveDate"=>  "NOW()",
			"Date"               =>  "NOW()",
			"UserID"             =>  'Application',
			"Comments"           =>  $Comments
	);
	// Insert JobApplicationHistory
	G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
	echo '<div style="width:400px;margin: 30px auto 60px auto;">';
	echo 'You have successfully ' . $type . 'ed applicant ' . $ApplicationID . '.';
	echo '</div>';
	
	echo '<br><br><br><br><br>';
} // end function
?>
