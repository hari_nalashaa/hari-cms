<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php 
						if(isset($_GET['msg']) && ($_GET['msg'] == 'csuc' || $_GET['msg'] == 'jsuc')) {
							?>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12" style="color:blue;">
									<br>
									<?php echo 'Successfully Updated';?>
								</div>
							</div>	
							<?php
						}
						?>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<h4>CSS:</h4>		
							</div>
						</div>	
												
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<form name="frmCssEditor" id="frmCssEditor" method="post">
									<textarea rows="30" cols="100" style="color:#398AB9" name="taCssInfo" id="taCssInfo" spellcheck="false"><?php echo $css_content;?></textarea>
									<br><br>
									<input type="submit" name="btnCssSubmit" id="btnCssSubmit" class="btn btn-primary" value="Submit">								
								</form>
							</div>
						</div>	
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<br>
								<h4>CODE:</h4>		
							</div>
						</div>	
												
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<form name="frmFooterCodeInfo" id="frmFooterCodeInfo" method="post">
									<textarea rows="15" cols="100" style="color:#398AB9" name="taFooterCodeInfo" id="taFooterCodeInfo" spellcheck="false"><?php echo $footer_code;?></textarea>
									<br><br>
									<input type="submit" name="btnFooterCodeSubmit" id="btnFooterCodeSubmit" class="btn btn-primary" value="Submit">								
								</form>
							</div>
						</div>							
						
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>