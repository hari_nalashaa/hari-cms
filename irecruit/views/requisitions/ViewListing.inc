<?php
include COMMON_DIR . 'listings/PrintRequisition.inc';
include COMMON_DIR . 'listings/SocialMedia.inc';

if ($permit ['Requisitions'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

echo '<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr><td>';

echo PrintReq ( $OrgID, $RequestID, '', '' );
?>
</td>
</tr>
</table>
<br>
<br>
<p align="center">
	<a href="#" onclick="window.close()" style="text-decoration: none;"> 
		<img src="<?php echo IRECRUIT_HOME?>images/icons/photo_delete.png" title="Close Window" border="0" style="margin: 0px 3px -4px 0px;">
		<b style="FONT-SIZE: 8pt; COLOR: #000000">Close Window</b>
	</a>
</p>