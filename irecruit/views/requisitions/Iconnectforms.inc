<div>
	<?php 
require_once 'Configuration.inc';
		
require_once COMMON_DIR 		. 'formsInternal/IconnectFormStatus.inc';
require_once IRECRUIT_DIR 		. 'applicants/EmailApplicant.inc';
require_once COMMON_DIR 		. 'formsInternal/DisplayApplicantHeader.inc';
//require_once IRECRUIT_DIR 		. 'formsInternal/AddInternalForm.inc';
require_once IRECRUIT_DIR 		. 'formsInternal/AddIconnectForm.inc';

?>

      
<?php 
//if (($RequestID != "") && ($OrgID != "")) {
	$request_source = G::Obj('ServerInformation')->getRequestSource();
	if($request_source == 'normal') {
		?>
		
			<div id="page-wrapper">
				<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
				<!-- row -->
				<div class="row">
					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
		<?php
	}

	if ($_REQUEST['IconnectPackagesHeaderID'] != "") {
		echo '<div style="text-align:right;">';
		echo '<a href="formsInternal.php?typeform=packages"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
		echo '</div>';
	}
	
	echo '<div style="clear:both;"></div>';
	
	$cnt = 0;
	
	if ($feature ['PreFilledForms'] == "Y") {
		if ($ApplicationID != "") {
			
			//set where condition
			$where = array("OrgID = :OrgID");
			//set parameters
			$params = array(":OrgID"=>$OrgID);
			//Get PrefilledForms Information
			$results = G::Obj('FormsInternal')->getPrefilledFormsInfo("*", $where, "", array($params));
			
			$cnt += $results['count'];
		} // end ApplicationID
	} // end feature
	
	if ($feature ['WebForms'] == "Y") {
		
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//Get WebForms Information
		$results = G::Obj('FormsInternal')->getWebFormsInfo("*", $where, "", array($params));
		
		$cnt += $results['count'];
	} // end feature
	
	if ($feature ['AgreementForms'] == "Y") {
		
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//get agreement forms information
		$results = G::Obj('FormsInternal')->getAgreementFormsInfo("*", $where, "", array($params));
		$cnt += $results['count'];
	} // end feature
	
	if ($feature ['SpecificationForms'] == "Y") {
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//get specification forms information
		$results = G::Obj('FormsInternal')->getSpecificationForms("*", $where, "", array($params));

		$cnt += $results['count'];
	} // end feature
	
	if ($cnt > 0) {
		addIconnectForm();
		include IRECRUIT_DIR . 'formsInternal/IconnectFormsReminderSettings.inc';
	}
	
	if (! $ApplicationID) {
		displayAssignedForms ( $OrgID, $ApplicationID, $RequestID );
	}
	
	if ($ApplicationID) {
		$statuspresent = displayApplicantFormStatus ( $OrgID, $ApplicationID, $RequestID );
	
		//set where condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "Status = '3'");
		//set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
		//get Internal Forms Assigned
		$results = G::Obj('FormsInternal')->getInternalFormsAssignedInfo("*", $where, "", "", array($params));
	
		$completecnt = $results['count'];
	
		if (($statuspresent) && ($completecnt > 0)) {
			echo '<div style="text-align:center;margin-top:20px;">';
			echo '<form method="POST" name="frmFinalizeAllCompletedForms'.$ApplicationID.$RequestID.'" id="frmFinalizeAllCompletedForms'.$ApplicationID.$RequestID.'" action="getIconnectpackage.php">';
			echo '<input type="button" value="Finalize All Completed Forms" name="btnFinalizeAllComForms" id="btnFinalizeAllComForms" onclick="finalizeAllCompletedForms(this)">';
			echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
			echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			echo '<input type="hidden" name="process" value="F">';
			echo '</form>';
			echo '</div>';
		}
	
		if (! $statuspresent) {
			echo '<div style="text-align:center;margin-top:40px;margin-bottom:50px;font-size:10pt;">There are no forms assigned for this applicant.</div>';
		}
	} // end ApplicationID
	
	if($request_source == 'normal') {
		?>
							</div>
						</div>
				</div>
			</div>
		</div>		
		<?php
	} 
//} // end if RequestID
?>
	
</div>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME?>js/validate_monster_info.js"></script>
