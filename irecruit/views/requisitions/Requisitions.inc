<?php
include_once IRECRUIT_DIR . 'requisitions/PostRequisitionsToMonster.inc';

include_once IRECRUIT_DIR . 'requisitions/RequisitionsIncludesBelowHeader.inc';
?>
<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<?php include_once IRECRUIT_VIEWS . "requisitions/RequisitionsPageContent.inc";?>
					<!-- /.row (nested) -->
				</div>
			</div>
			<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
<?php
if ($feature ['MonsterAccount'] == "Y") {
	
	$MonsterInformation = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
	$MonsterOrgInfo = $MonsterInformation ['row'];
	require_once IRECRUIT_DIR . 'monster/InventoryDetails.inc';
	
	global $fault_string;

	if ($MonsterOrgInfo ['RecruiterReferenceUserName'] != "" 
		&& $MonsterOrgInfo ['Password'] != "" 
		&& $fault_string == 'False') {
		?>
		<div class="row">
			<div class="col-lg-12">
				<div id="openFormWindow" class="openForm">
					<div>
						<p style="text-align: right;">
							<a href="#closeForm" title="Close Window"></a>
						</p>
							<?php include IRECRUIT_DIR . 'monster/SelectInventoryInfo.inc'; ?>
						</div>
				</div>
			</div>
		</div>
	
		<script type="text/javascript" src="<?php echo IRECRUIT_HOME?>js/validate_monster_info.js"></script>
		<?php
	} // end user and password for inventory

} // end MonsterAccount
?>
</div>