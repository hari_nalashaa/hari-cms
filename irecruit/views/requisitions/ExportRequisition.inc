<?php 
//Set columns
$columns = "ROL.OrgLevelID, ROL.SelectionOrder, R.MultiOrgID, R.RequestID, R.RequisitionID, R.MultiOrgID, R.JobID, 
            R.EmpStatusID, date_format(R.PostDate,'%m/%d/%Y') PostDate, date_format(R.ExpireDate,'%m/%d/%Y') ExpireDate, 
            R.Title, R.Description, R.EEOCode, R.JobGroupCode, R.City, R.State, R.ZipCode, R.Country, R.RequisitionFormID";
//Set where condition
$where  =   array("ROL.OrgID = R.OrgID", "ROL.OrgID = :OrgID", "ROL.RequestID = :RequestID", "ROL.RequestID = R.RequestID", "R.Active='Y'");
//Set order by and limit
$order_by = "ROL.OrgLevelID, ROL.SelectionOrder LIMIT 1";
//Set the parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);

//Get Requisitions and Requisition Orgnization Levels Information
$results = $RequisitionsObj->getReqAndReqOrgLevelsInfo($columns, $where, "", $order_by, array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $REQ) {
	
	    $emp_status_levels  =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $REQ['RequisitionFormID']);
	    
		$url = PUBLIC_HOME . "jobRequest.php?";
		$url .= "OrgID=";
		$url .= $OrgID;
		if ($REQ ['MultiOrgID']) {
			$url .= "&MultiOrgID=";
			$url .= $REQ ['MultiOrgID'];
		} 
		$url .= "&RequestID=";
		$url .= $REQ ['RequestID'];
		$url .= "&source=XML";
	
		echo '<div style="width:830px;padding:10px 30px 0px 20px;">';
		echo 'URL: <a href="' . $url . '" target="_blank">' . $url . '</a><br><br>';
	
		$xml .= "   <url><![CDATA[";
		$xml .= $url;
		$xml .= "]]></url>\n";
	
		echo '<font style="color:red">Note:</font> Changing the source variable (e.g. source=XML to source=Email) in the above URL link will allow you to track and identify when the above link is being used.<br><br>';
	
		echo 'RequisitionID: ' . $REQ ['RequisitionID'] . '<br>';
		$xml .= "  <requisitionid><![CDATA[";
		$xml .= $REQ ['RequisitionID'];
		$xml .= "]]></requisitionid>\n";
	
		echo 'JobID: ' . $REQ ['JobID'] . '<br><br>';
		$xml .= "  <jobid><![CDATA[";
		$xml .= $REQ ['JobID'];
		$xml .= "]]></jobid>\n";
	
		if ($REQ ['City'] != "") {
			echo 'Job Location City: ' . $REQ ['City'] . '<br>';
			$xml .= "  <city><![CDATA[";
			$xml .= $REQ ['City'];
			$xml .= "]]></city>\n";
		}
	
		if ($REQ ['State'] != "") {
			echo 'Job Location State: ' . $REQ ['State'] . '<br>';
			$xml .= "  <state><![CDATA[";
			$xml .= $REQ ['State'];
			$xml .= "]]></state>\n";
		}
	
		if ($REQ ['ZipCode'] != "") {
			echo 'Job Location ZIP Code: ' . $REQ ['ZipCode'] . '<br>';
			$xml .= "  <zipcode><![CDATA[";
			$xml .= $REQ ['ZipCode'];
			$xml .= "]]></zipcode>\n";
		}
	
		if ($REQ ['Country'] != "") {
			echo 'Job Location Country: ' . $REQ ['Country'] . '<br><br>';
			$xml .= "  <country><![CDATA[";
			$xml .= $REQ ['Country'];
			$xml .= "]]></country>\n";
		}
	
		echo 'External Post Date: ' . $REQ ['PostDate'] . '<br>';
		$xml .= "  <postdate><![CDATA[";
		$xml .= $REQ ['PostDate'];
		$xml .= "]]></postdate>\n";
	
		echo 'External Expire Date: ' . $REQ ['ExpireDate'] . '<br><br>';
		$xml .= "  <expiredate><![CDATA[";
		$xml .= $REQ ['ExpireDate'];
		$xml .= "]]></expiredate>\n";
	
		echo 'Job Title: ' . $REQ ['Title'] . '<br>';
		$xml .= "  <title><![CDATA[";
		$xml .= $REQ ['Title'];
		$xml .= "]]></title>\n";
	
		echo 'Description: ' . $REQ ['Description'] . '<br><br>';
		$xml .= "  <desc><![CDATA[";
		$xml .= $REQ ['Description'];
		$xml .= "]]></desc>\n";
	
		//Set columns
		$columns = "Code, Description";
		//Set where condition
		$where   = array("Code = :Code");
		//Set parameters
		$params = array(":Code"=>$REQ ['EEOCode']);
		//Get eeo classifications information
		$resultsIN = $OrganizationsObj->getEEOClassificationsInfo($columns, $where, "", array($params));
		$EEOClass = $resultsIN['results'][0];
	
		echo 'EEO Classification: ' . $EEOClass ['Description'] . '<br>';
		$xml .= "  <eeoclass><![CDATA[";
		$xml .= $EEOClass ['Description'];
		$xml .= "]]></eeoclass>\n";
	
		echo 'Employment Status: ' . $emp_status_levels[$REQ ['EmpStatusID']] . '<br><br>';
		$xml .= "  <empstatus><![CDATA[";
		$xml .= $emp_status_levels[$REQ ['EmpStatusID']];
		$xml .= "]]></empstatus>\n";
	
		echo 'Category Listings: <br>';
		$xml .= "  <categories>\n";

		//Set tables
		$table_name   =   "OrganizationLevels OL, OrganizationLevelData OLDD, RequisitionOrgLevels ROL";
		//Set columns
		$columns      =   "OL.OrganizationLevel, OLDD.CategorySelection";
		//Set order by
		$order_by     =   "ROL.OrgLevelID, ROL.SelectionOrder";
		//Set condition
		$where        =   array(
                                "OL.OrgID           =   OLDD.OrgID",
                                "OLDD.OrgID         =   ROL.OrgID",
                                "OL.MultiOrgID      =   OLDD.MultiOrgID",
                                "OL.OrgID           =   :OrgID",
                                "OL.MultiOrgID      =   :MultiOrgID",
                                "ROL.RequestID      =   :RequestID",
                                "ROL.OrgLevelID     =   OL.OrgLevelID",
                                "OL.OrgLevelID      =   OLDD.OrgLevelID", 
                                "ROL.SelectionOrder =   OLDD.SelectionOrder"
                            );
		//Set parameters
		$params       =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$REQ ['MultiOrgID'], ":RequestID"=>$RequestID);
		//Get Org and Requisition Levels Information
		$resultsIN    =   $OrganizationsObj->getOrgAndReqLevelsInfo($table_name, $columns, $where, $order_by, array($params));
	
		$i = 0;
		if(is_array($resultsIN['results'])) {
			foreach($resultsIN['results'] as $REQLEVELS) {
				$i ++;
				echo 'Org Level: ' . $REQLEVELS ['OrganizationLevel'] . ': ' . $REQLEVELS ['CategorySelection'] . "<br>";
				$xml .= "    <level-$i><![CDATA[";
				$xml .= $REQLEVELS ['OrganizationLevel'];
				$xml .= "]]></level-$i>\n";
				$xml .= "    <section-$i><![CDATA[";
				$xml .= $REQLEVELS ['CategorySelection'];
				$xml .= "]]></section-$i>\n";
			}
		}
	
		$xml .= "  </categories>\n";
	} // end foreach
}


echo '<p><a href="javascript:ReverseDisplay(\'xml\')">View XML file</a></p>' . "\n";
echo '<div id="xml" style="display:none;">' . "\n";
echo '<textarea cols=60 rows=20>';

echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
echo '<requisition>' . "\n";
echo $xml;
echo '</requisition>' . "\n";
echo '</textarea>';
echo '</div>' . "\n";

if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';	
}
echo '</div>';
?>
