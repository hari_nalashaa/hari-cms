<?php
$close_link = IRECRUIT_HOME . 'requisitions.php';
$close_link .= '?RequestID=' . $RequestID;
$close_link .= '&action=' . $action;
$close_link .= '&Active=' . $Active;
if ($_REQUEST ['AccessCode'] != "") {
	$close_link .= '&k=' . $_REQUEST ['AccessCode'];
}

echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
END;

if ($_REQUEST ['process'] == "Y") {
	
	if (isset($OrgID) 
	   && isset($RequestID) 
	   && $OrgID != "" 
       && $RequestID != "") {
		// Set where condition
		$where    =   array ("OrgID = :OrgID", "RequestID = :RequestID");
		// Set parameters
		$params   =   array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
		// Delete RequisitionForward Information
		$RequisitionsObj->delRequisitionsInfo ( 'RequisitionForward', $where, array ($params) );
		
		// Delete RequisitionForward Information
		$RequisitionForwardSpecsObj->delRequisitionForwardSpecsInfo ( $where, array ($params) );
	}
	
	$cnt   =   sizeof ( $_POST ['to'] ) - 1;
	foreach ( $_POST ['to'] as $key => $value ) {
		if (isset($OrgID) && isset($RequestID) && isset($value) && $value != "") {
			// Insert RequistionForward Information
			$req_forward_info    =   array (
                    					"OrgID"        =>  $OrgID,
                    					"RequestID"    =>  $RequestID,
                    					"EmailAddress" =>  $value 
                                    );
			$RequisitionsObj->insRequisitionsInfo ( 'RequisitionForward', $req_forward_info );
		} // if value
	} // end foreach

	if (count( $_POST ['ForwardSpecs']) > 0 ) {	
          $ForwardSpecsList      =   array();
          $ForwardSpecsList      =   array_keys($_POST['ForwardSpecs']);
	  $ForwardSpecsList      =   json_encode($ForwardSpecsList);
	} else {
	  $ForwardSpecsList      =   "[]";
	}
	
        $req_forward_specs_info =  array (
                                    "OrgID"             =>  $OrgID,
                                    "RequestID"         =>  $RequestID,
                                    "ProcessOrder"      =>  $_POST ['ProcessOrder'],
                                    "DispositionCode"   =>  $_POST ['DispositionCode'],
                                    "Download"          =>  $_POST ['download'],
                                    "ForwardSpecsList"  =>  $ForwardSpecsList
                            	);
	
	// Requisition Forward Specs Information
       	$RequisitionForwardSpecsObj->insRequisitionForwardSpecsInfo ( $req_forward_specs_info );
	
	$message .= 'Auto Forward has been updated.\\n';
	
	if ($message) {
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "');\n";
		if($ServerInformationObj->getRequestSource() != 'ajax') {
			echo "CloseAndRefresh('" . $close_link . "');\n";
		}
		echo '</script>';
	}
} // end process

echo '<form name="frmUpdateAutoForwardList" id="frmUpdateAutoForwardList" method="post" action="autoForwardList.php">';

// Get RequisitionsForwardSpecs Information
$RFS        =   $RequisitionForwardSpecsObj->getRequisitionForwardSpecsDetailInfo($OrgID, $RequestID);

if($ServerInformationObj->getRequestSource() != 'ajax') {
    $multiorgid_req =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
	echo '<table border="0" cellspacing="0" cellpadding="0">';
	echo '<tr><td>';
	if ($feature ['MultiOrg'] == "Y") {
		echo "Organization: <b>" . $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . "</b>";
		echo "<br>";
	}
	echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo "<br>";
	echo "Title: <b>" . $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo '</td></tr>';
	echo '</table>';
}

echo '<br>';

// Get autoforward list information records count
$results    =   $FormFeaturesObj->getAutoForwardList ( $OrgID );
$aflcnt     =   $results ['count'];

if ($aflcnt > 0) {
	
	echo '<strong>Set applications to automatically forward via email.</strong><br><br>';
	
	echo '<table>';
	include IRECRUIT_DIR . 'request/DisplayFormQuestions.inc';
	echo displayAutoForwardList ( $OrgID, $RequestID );
	echo '</table>';
	
	
	echo '<br><strong>Set this application to the following codes when autoforwarding:</strong><br>';
	
	echo 'Status: ';
	echo '<select name="ProcessOrder">';
	echo '<option value="">None</option>';

	// Set where condition
	$where     =   array (
            			"OrgID           =   :OrgID",
            			"ProcessOrder    >   1",
            			"Type            =   'S'",
            			"Active		 =   'Y'" 
            	   );
	// Set parameters
	$params    =   array (
            			":OrgID"         =>  $OrgID 
                	);
	if ($permit ['Applicants_Update_Final_Status'] == 0) {
		$where [] = "Searchable != 'N'";
	}
	// Get ApplicantProcessFlow
	$results   =   $ApplicantsObj->getApplicantProcessFlowInfo ( "ProcessOrder, Description", $where, 'ProcessOrder', array (
			$params 
	) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $row ) {
			
			if ($row ['ProcessOrder'] == $RFS ['ProcessOrder']) {
				$selected = ' selected';
			} else {
				$selected = '';
			}
			echo '<option value="' . $row ['ProcessOrder'] . '"' . $selected . '>' . $row ['Description'] . '</option>';
		}
		echo '</select>';
	}
	
	// Get ApplicantDisposition Codes
	$results           =   $ApplicantsObj->getApplicantDispositionCodes ( $OrgID, 'Y' );
	$disposition_rows  =   $results ['count'];
	
	if ($disposition_rows > 0) {
		
		echo '&nbsp;&nbsp;&nbsp;&nbsp;Disposition: ';
		echo '<select name="DispositionCode">';
		echo '<option value="">Select a Code</option>';
		
		foreach ( $results ['results'] as $row ) {
			
			if ($row ['Code'] == $RFS ['DispositionCode']) {
				$selected = ' selected';
			} else {
				$selected = '';
			}
			
			echo '<option value="' . $row ['Code'] . '"' . $selected . '>' . $row ['Description'];
		}
		
		echo '</select>';
	} // end if disposition set up
	
	echo '<br><br><strong>What information would you like to forward (if available): </strong><br>';
	
	echo '&nbsp;&nbsp;&nbsp;';
	echo '<input type="checkbox" name="ForwardSpecs[application]" value="Y"';
	if (in_array('application', json_decode($RFS ['ForwardSpecsList'], true))) {
		echo ' checked';
	}
	echo '> Application<br>';
	
	echo '&nbsp;&nbsp;&nbsp;';
	echo '<input type="checkbox" name="ForwardSpecs[history]" value="Y"';
	if (in_array('history', json_decode($RFS ['ForwardSpecsList'], true))) {
		echo ' checked';
	}
	echo '> Applicant History<br>';

	$multiorgid_req    =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
	$ReqAppFormID      =   $RequisitionDetailsObj->getFormID($OrgID, $multiorgid_req, $RequestID);
	
	// Set where condition
	$where     =   array (
            			"OrgID           =   :OrgID",
            			"FormID          =   :FormID",
            			"SectionID       =   6",
            			"QuestionTypeID !=   99",
            			"Active          =   'Y'" 
                	);
	// Set parameters
	$params    =   array (
                        ":OrgID"        =>  $OrgID,
                        ":FormID"       =>  $ReqAppFormID
                	);
	// Get form questions information
	$results   =   $FormQuestionsObj->getFormQuestionsInformation ( "QuestionID, value", $where, "QuestionOrder", array (
			$params 
	) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $Attachments ) {
			
			echo '&nbsp;&nbsp;&nbsp;';
			echo '<input type="checkbox" name="ForwardSpecs['.$Attachments ["QuestionID"].']" value="Y"';
			
			$forward_specs_list =   json_decode($RFS ['ForwardSpecsList'], true);
			if (is_array($forward_specs_list) && in_array($Attachments["QuestionID"], $forward_specs_list)) {
				echo ' checked';
			}
			
			if($Attachments ['value'] == "") $Attachments ['value'] = $Attachments["QuestionID"];
			
			echo '> ' . $Attachments ['value'] . "<br>";
		} // end foreach
	}
} // end aflcnt

echo '&nbsp;&nbsp;&nbsp;';
echo '<input type="checkbox" name="ForwardSpecs[related_applications]" value="Y"';
if (in_array('related_applications', json_decode($RFS ['ForwardSpecsList'], true))) {
	echo ' checked';
}
echo '> Related Applications<br>';

echo '<br>';
echo '<center>';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="OrgID" value="'.$OrgID.'">';
echo '<input type="hidden" name="RequestID" value="'.$RequestID.'">';
echo '<input type="hidden" name="action" value="'.$action.'">';
echo '<input type="hidden" name="Active" value="'.$Active.'">';
if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<input type="submit" value="Update Auto Forward List">';
}
else if($ServerInformationObj->getRequestSource() == 'ajax') {
	echo '<input type="button" class="btn btn-primary" name="btnUpdateAutoForwardList" id="btnUpdateAutoForwardList" value="Save" onclick="updateAutoForwardListForm(this)"><br><br><br>';
}
if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<br><br><p align="center"><a href="#" onclick="CloseAndRefresh(\'' . $close_link . '\')" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
}
echo '</center>';

echo '</form>';
?>
