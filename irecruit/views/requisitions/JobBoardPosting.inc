<div id="page-wrapper">
<style>
.btn-small {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
}
#vertical-text-full-view-results {
	cursor:pointer;
	background-color: <?php echo $navbar_color;?>;
    border: 1px solid transparent;
	border-radius: 4px;
    color: white;
    font-size: 12px;
    padding: 6px;
    z-index: 999;
    float: left;
}
#vertical-text-detail-view-results {
	cursor:pointer;
	background-color: <?php echo $navbar_color;?>;
    border: 1px solid transparent;
	border-radius: 4px;
    color: white;
    font-size: 12px;
    padding: 6px;
    z-index: 999;
    float: left;
}
.pagination_link {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
	padding:6px;
}
.pagination_link:hover {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
	padding:6px;
}
#span_left_page_nav_previous, #span_fullview_page_nav_previous {
	border:0px solid #f5f5f5;
	text-align:left;
	float:left;
	padding-top:5px;
	padding-bottom:5px;
	height:30px;
	text-align:center;
	margin-top:5px;
}
#span_left_page_nav_next, #span_fullview_page_nav_next {
	text-align:right;
	float:right;
	border:0px solid #f5f5f5;
	padding-top:5px;
	padding-bottom:5px;
	height:30px;
	text-align:center;
	margin-top:5px;
}
.current_page_and_total_pages {
	text-align:center;
	float:left;
	border:0px solid #f5f5f5;
	padding-top:5px;
	padding-bottom:5px;
}
ul.tab-content-tabs {
	margin: 0px;
	padding: 0px;
	list-style: none;
}
ul.tab-content-tabs li {
	border-top-left-radius: 6px;
  	border-top-right-radius: 6px;
	background: <?php echo $navbar_color;?>;
	color: #ffffff;
	display: inline-block;
	padding: 7px;
	margin-top: 10px;
	cursor: pointer;
}
ul.tab-content-tabs li a {
	color: #ffffff;
	padding: 7px;
}
ul.side-tabs {
	margin: 0px;
	padding: 0px;
	list-style: none;
}
ul.side-tabs li {
	border-top-left-radius: 6px;
  	border-top-right-radius: 6px;
    border: 1px solid <?php echo $navbar_color;?>;
	background: <?php echo $navbar_color;?>;
	color: #ffffff;
	display: inline-block;
	padding: 7px;
	margin-top: 10px;
	cursor: pointer;
	width: auto;
	margin-right: 1px;
}
ul.side-tabs li.active {
	border-top-left-radius: 6px;
  	border-top-right-radius: 6px;
	border: 1px solid <?php echo $navbar_color;?>;
	background: #F5F5F5 !important;
	color: black !important;
	display: inline-block;
	padding: 7px;
	margin-top: 10px;
	cursor: pointer;
	width: auto;
	margin-right: 1px;
}
ul.side-tabs li.active a {
	color: black !important;
	padding: 7px;
}
ul.side-tabs li a {
	color: #ffffff;
	padding: 7px;
}
.free_paid_tab {
    background-color:<?php echo $navbar_color;?>;width: 100px;padding: 6px;float: left;color: white;border:1px solid <?php echo $navbar_color;?>;cursor: pointer;
}
.fpactive {
    background-color:#f4f5f5;width: 100px;padding: 6px;float: left;color: black;border:1px solid <?php echo $navbar_color;?>;cursor: pointer;
}
.export_repost {
    background-color:<?php echo $navbar_color;?>;width: 100px;padding: 6px;float: left;color: white;border:1px solid <?php echo $navbar_color;?>;cursor: pointer;margin: 2px;
}
.new_tooltip {
    position: relative;
    display: inline-block;
}
.new_tooltip .new_tooltiptext {
    visibility: hidden;
    width: 500px;
    background-color: #555;
    color: #fff;
    text-align: left;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 50%;
    opacity: 0;
    transition: opacity 0.3s;
    padding: 5px;
}
.new_tooltip .new_tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}
.new_tooltip:hover .new_tooltiptext {
    visibility: visible;
    opacity: 1;
}
</style>

	<?php 
	include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
	
	$full_view_display_style = 'none';
	$detail_view_display_style = 'block';
	
	if(isset($_GET['msg']) 
        && ($_GET['msg'] == 'appsucindeed' 
            || $_GET['msg'] == 'appsucmonster'
            || $_GET['msg'] == 'appsucindeedcleared'
            || $_GET['msg'] == 'appsucmonstercleared')) {

            $full_view_display_style = 'block';
            $detail_view_display_style = 'none';

		?>
		<div class="row">
    		<div class="col-lg-12 alert alert-success" style="text-align: left">
    			<?php
    			 if(isset($_GET['msg']) && $_GET['msg'] == 'appsucindeed') {
    			 	echo "Successfully added to Indeed";
    			 }
    			 else if(isset($_GET['msg']) && $_GET['msg'] == 'appsucmonster') {
                    if(isset($_REQUEST['rs']) && count($_REQUEST['rs']) > 0) {
                    	echo "Please update monster information for below requisitions<br>";
                    	foreach ($_REQUEST['rs'] as $rs_key=>$rs_value) {
                    		$req_det_info = $RequisitionsObj->getRequisitionsDetailInfo("Title, MultiOrgID", $OrgID, $rs_value);
                            echo '<a href=\'javascript:void(0);\' onclick=\'freeMonsterTabInfo("'.$rs_value.'", "'.$req_det_info['MultiOrgID'].'")\'>'.$req_det_info['Title']."</a><br>";
                    	}
                    }
                    else {
                    	echo "Successfully added to Monster";
                    }
    			 }
    			 else if(isset($_GET['msg']) && $_GET['msg'] == 'appsucindeedcleared') {
    			     echo "Successfully cleared Indeed status";
    			 }
    			 else if(isset($_GET['msg']) && $_GET['msg'] == 'appsucmonstercleared') {
    			     echo "Successfully cleared Monster status";
    			 }
    			?>
    		</div>
    	</div>
		<?php
	}
	?>
	<div class="row" id="full-view-navigation-bar" style="display:<?php echo $full_view_display_style;?>">
		<div class="col-lg-12">
			<div class="vertical-text" id="vertical-text-detail-view-results">Requisitions detail view</div>
			<span id="results_loading"></span>
			<br>
			<br>
		</div>
	</div>
	
	<div id="openFormWindow" class="openForm">
		<div>
			<p style="text-align:right;">
				<a href="#closeForm" id="ahref_close_form" title="Close Window" onclick="getAssignInternalInfo();">
					<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
					<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
				</a>
			</p>
			
			<div id="iconnect_change_status" class="iconnect_forms_data"></div>
			<div id="iconnect_view_history" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_agree_form" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_pref_form" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_web_form" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_spec_form" class="iconnect_forms_data"></div>	
		</div>
	</div>
	
	<div style="text-align:center" id="del_process_message"></div>
	
	<div id="search-results-detail" class="table-responsive" style="display:<?php echo $full_view_display_style;?>">
	<form name="frmRequisitionsSearchResults" id="frmRequisitionsSearchResults" method="post">
			<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover" id="requisitions_full_view_results">
			<?php
			echo "<tr>";

			echo "<td valign=\"bottom\" align=\"left\" width=\"20\"><b>Title</b></td>";
			
			echo "<td valign=\"bottom\" align=\"left\" width=\"20\"><b>Free Job Board Lists</b></td>";
					
			echo "<td valign=\"bottom\" align=\"left\" width=\"20\"><b>Remarks on Applying to Job Board</b></td>";
			
			echo "<td width=\"20\"><strong>Check All </strong><input type='checkbox' name='chk_req_all' id='chk_req_all' onclick='check_uncheck(this.checked, \"check_requisitions\")'></td>";
			
			echo "</tr>";
			
			if (is_array ( $req_search_results ['results'] )) {
			
				foreach ( $req_search_results ['results'] as $REQS ) {
						
					echo '<tr style="background-color:' . $rowcolor . ';border-color:#ffffff">';

					echo "<td valign=\"top\">";
					if ($feature ['MultiOrg'] == "Y") {
						echo $OrganizationDetailsObj->getOrganizationName ( $OrgID, $REQS ['RequestID'] ) . "<br>";
					} // end feature
					
					$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $REQS ['RequestID']);
					echo $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
						
					echo "<br>";
					echo $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
					echo "</td>";


					echo '<td align="left">'.$REQS ['FreeJobBoardLists'].'</td>';
					
					if($REQS ['City'] == "" || $REQS ['State'] == "" || $REQS ['ZipCode'] == "") {
						echo "<td valign=\"bottom\" align=\"left\">";
						echo "Please complete city, state, zipcode";
						echo '<br><a href="'.IRECRUIT_HOME.'requisitionsSearch.php?menu=3&RequestID='.$REQS ['RequestID'].'&active_tab=edit-requisition&validate_errors_list=csz">Click here to update requisition information</a>';
						echo "</td>";
						echo "<td>&nbsp;</td>";
					}
					else {
                        if($feature['MonsterAccount'] == 'Y') {
                        	echo "<td valign=\"bottom\" align=\"left\">";

                        	if($REQS ['MonsterJobPostType'] === ''
                               || $REQS ['MonsterJobIndustry'] === ''
                               || $REQS ['MonsterJobCategory'] === ''
                               || $REQS ['MonsterJobCategory'] === 0
                               || $REQS ['MonsterJobOccupation'] === 0
                               || $REQS ['MonsterJobOccupation'] === '') {
                               echo "Please fill monster information"; 
                            }
                            else {
                            	echo 'N/A';
                            }
                        	
                        	echo "</td>";
                        }
                        else {
                        	echo "<td valign=\"bottom\" align=\"left\">N/A</td>";
                        }
                        echo "<td><input type='checkbox' name='chk_req[".$REQS ['RequestID']."]' id='chk_req_".$REQS ['RequestID']."' class='check_requisitions'></td>";
					}
					
					
					echo "</tr>";				
						
					if ($rowcolor == "#eeeeee") {
						$rowcolor = "#ffffff";
					} else {
						$rowcolor = "#eeeeee";
					}
				} // end foreach
			}
			
			if($total_requisitions_count > $LimitRequisitions) {
			  	  	$left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $LimitRequisitions, $total_requisitions_count, '', '');
		
					echo '<tr>';
					echo '<td colspan="100%">';
					echo '<div style="padding:0px;margin-top:0px;overflow:hidden;">';
					echo '<div style="text-align:center;width:100%;float: left;border:0px solid #f5f5f5;padding-top:5px;padding-bottom:5px;height:40px">';
					echo '<div style="float:left">&nbsp;<strong>Per page:</strong> '.$LimitRequisitions.'</div>';
					echo '<div style="float:right">';
					echo '<input type="text" name="current_page_fullview_number" id="current_page_fullview_number" value="'.$left_pagination_info['current_page'].'" style="width:50px;text-align:center" maxlength="4">';
					echo ' <input type="button" name="btnFullViewPageNumberInfo" id="btnFullViewPageNumberInfo" value="Go" style="background-color:'.$navbar_color.';color:white;border:2px solid '.$navbar_color.'" onclick="getRequisitionsByPageNumber(document.getElementById(\'current_page_fullview_number\').value, \'\', \'\', \'no\')">';
					echo '</div>';
					echo '</div>';
					echo '<div id="span_fullview_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left;padding-left:0px;">'.$left_pagination_info['previous'].'</div>';
					echo '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
					echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];
					echo '</div>';
					echo '<div id="span_fullview_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right;padding-right:0px;">'.$left_pagination_info['next'].'</div>';
					echo '</td>';
					echo '</tr>';
			}

			if($feature['JobBoardRefresh'] == 'Y') {
				?>
				<tr>
    				<td colspan="100%">
    					<select name="ddlProcessRequisitions" id="ddlProcessRequisitions" class="form-control width-auto-inline">
    						<option value="">Select Action</option>
    						<option value="Repost">Repost</option>
    					</select>
    					&nbsp;
    					<input type="button" name="btnProcessRequisitions" id="btnProcessRequisitions" value="Process" class="btn btn-primary" onclick="processRequisitions(document.getElementById('ddlProcessRequisitions').value, this);">
    				</td>
    			</tr>		  
				<?php
			}
			?>
			</table>
	</form>

	<div id="process_requisitions"><br><br><br><br></div>
	
	</div>

	<div class="row">
		
		<div class="col-lg-3" id="requisitions-minified-search-results" style="border: 1px solid #F5F5F5;display:<?php echo $detail_view_display_style;?>;">
			<div class="row">
				<div class="col-lg-12"><h4>Requisitions List</h4></div>
			</div>

			<?php
			if($feature['JobBoardRefresh'] == 'Y') {
			?>
			<div class="row">
				<div class="col-lg-12">
					<input type="button" class="btn btn-primary vertical-text" id="vertical-text-full-view-results" value="Requisitions full view">
				</div>
			</div>
			<br>
			<?php
			}
			?>
			<div class="row">
				<div class="col-lg-12">
					<form name="frmFilters" id="frmFilters" method="post">
						<input type="hidden" name="ddlFilterActiveStatus" id="ddlFilterActiveStatus" value="Y">
					</form>
				
					<form name="frmSortOptions" id="frmSortOptions" method="post">
    					<input type="hidden" name="ddlReqDetailSort" id="ddlReqDetailSort" value="date_opened">
    					<input type="hidden" name="ddlReqDetailSortType" id="ddlReqDetailSortType" value="DESC">
    
    					<input type="text" name="txtKeyword" id="txtKeyword" value="<?php echo $qi['Keyword'];?>">
    					<input type="button" name="btnSearch" id="btnSearch" value="Search" class="btn-small" onclick="getRequisitionsListByFilters('yes');">
					    <br>
    					(Requisition Title, RequisitionID, JobID)<br>
    					Match with:<input type="radio" name="rdKeywordType" id="rdKeywordType" value="W" <?php if(!isset($qi['KeywordMatch']) || $qi['KeywordMatch'] == "" || $qi['KeywordMatch'] == "W") echo 'checked="checked"';?>>
    					Start with:<input type="radio" name="rdKeywordType" id="rdKeywordType" value="S" <?php if($qi['KeywordMatch'] == "S") echo 'checked="checked"';?>>
					</form>
				</div>
			</div>
			<br>			
	
			<div id="requisitions-minified-results" class="row">
				<?php
				if (is_array ( $req_search_results_list['results'] )) {
					foreach ( $req_search_results_list['results'] as $REQS ) {
                        $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $REQS ['RequestID']);
						$RequisitionTitle = $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
						echo "<div class='col-lg-12 col-md-12 col-sm-12'><a href='javascript:(0):void(0);'  onclick=\"getRequisitionTabInfo('".$REQS ['RequestID']."', '".$REQS ['MultiOrgID']."')\">".$RequisitionTitle."</a></div>";
					}
				}

				if($total_requisitions_count > $LimitRequisitions) {
				  	  $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $LimitRequisitions, $total_requisitions_count, '', '');
					  ?>
					  <div class="col-lg-12">
						  <div class="row">
							  	<div class="col-lg-6">
							  		<strong>Per page:</strong> <?php echo $LimitRequisitions;?>
							  	</div>
							  	<div class="col-lg-6" style="text-align:right">
								  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
								  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getRequisitionsByPageNumber(document.getElementById('current_page_number').value)">
							  	</div>
						  </div>
						  
						  <div class="row">
							  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
							  	<?php echo $left_pagination_info['previous'];?>
							  </div>
							  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
							  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
							  </div>
							  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
							  	<?php echo $left_pagination_info['next'];?>
							  </div>
						  </div>
					  </div>
				  	  <?php
				}
				?>
			</div>
			
		</div>
			
		<div class="col-lg-9" id="applicant-detail-view" style="border: 1px solid #F5F5F5;display:<?php echo $detail_view_display_style;?>;">
			<div id="process_message_req_details" style="text-align: center"></div>
			
			<div style="text-align: center;" id="req_detail_pagination_navigation">
					<span id='nav_previous' style="float: left; font-weight: bold;">
						<a href="javascript:void(0);" class="pagination_link" id="req_detail_prev" onclick="getPrevNextApplicant('previous')">
							<i class='fa fa-arrow-left' aria-hidden='true'></i> Previous
						</a>
					</span>
					<span id='nav_next' style="float: right; font-weight: bold;">
						<a href="javascript:void(0);" class="pagination_link" id="req_detail_next" onclick="getPrevNextApplicant('next')">
							Next <i class='fa fa-arrow-right' aria-hidden='true'></i>
						</a>
					</span>
			</div>

			<div class="table-responsive" id="req_detail_header_info">
	
					<strong>Title: </strong>
					<span id="header_requisition_title"></span><br>
					<strong>RequisitionID / JobID: </strong>
					<span id="header_requisition_requisition_id"></span>/<span id="header_requisition_job_id"></span><br>
					<strong>Date Posted: </strong><span id="header_requisition_post_date"></span>
					<strong>Expiration Date: </strong><span id="header_requisition_expire_date"></span>
					<span id="header_requisition_internal"></span><br>
					<strong>Applicants Count: </strong><span id="header_number_of_applicants"></span><br>
					<strong>Assign Forms Count: </strong><span id="header_number_of_assign_forms"></span>
						
			</div>
			
			<form name="frmRequisitionDetailInfo" id="frmRequisitionDetailInfo" method="post">
				<input type="hidden" name="GuIDRequisition" id="GuIDRequisition" value="<?php echo htmlspecialchars($GuIDRequisition);?>">
				<input type="hidden" name="Active" id="Active" value="<?php echo htmlspecialchars($Active);?>">
				<input type="hidden" name="RequestID" id="RequestID" value="<?php echo htmlspecialchars($req_search_results['results'][0]['RequestID']);?>">
				<input type="hidden" name="OrgID" id="OrgID" value="<?php echo htmlspecialchars($OrgID);?>">
				<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo htmlspecialchars($req_search_results['results'][0]['MultiOrgID']);?>">
				<input type="hidden" name="RequisitionsCount" id="RequisitionsCount" value="<?php echo htmlspecialchars($total_requisitions_count);?>">
				<input type="hidden" name="IndexStart" id="IndexStart" value="<?php echo htmlspecialchars($Start);?>">
				<input type="hidden" name="RequisitionsLimit" id="RequisitionsLimit" value="<?php echo htmlspecialchars($LimitRequisitions);?>">
				<input type="hidden" name="AccessCode" id="AccessCode" value="<?php echo htmlspecialchars($AccessCode);?>">
				<input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
				<input type="hidden" name="FeatureMonsterAccount" id="FeatureMonsterAccount" value="<?php echo htmlspecialchars($feature['MonsterAccount']);?>">
				<input type="hidden" name="current_sel_tab" id="current_sel_tab" value="<?php echo "#".$active_tab;?>">

				<input type="hidden" name="Address1" id="Address1">
				<input type="hidden" name="Address2" id="Address2">
				<input type="hidden" name="City" id="City">
				<input type="hidden" name="State" id="State">
				<input type="hidden" name="ZipCode" id="ZipCode">
				
				
				<input type="hidden" name="MonsterJobPostType" id="MonsterJobPostType">
				<input type="hidden" name="MonsterJobPostDuration" id="MonsterJobPostDuration">
				<input type="hidden" name="MonsterJobIndustry" id="MonsterJobIndustry">
				<input type="hidden" name="MonsterJobCategory" id="MonsterJobCategory">
				<input type="hidden" name="MonsterJobOccupation" id="MonsterJobOccupation">
			</form>	
			
			<div id="job_boards_processing_msg"></div>
			
			<div class="row">
		         <div class="col-lg-12">
					<ul class="side-tabs">
						<?php
					    echo '<li style="background-color:#C3EA33;border-color:#C3EA33;" id="tab_paid_ziprecruiter"><a href="#purchase-zip-recruiter-feed" class="requisition-tabs">ZipRecruiter</a></li>';
						echo '<li style="background-color:#3363F1;border-color:#3363F1;" id="tab_paid_indeed"><a href="#purchase-premium-feed" class="requisition-tabs">Indeed</a></li>';

						if($feature['MonsterAccount'] == "Y") {
							?><li style="background-color:#8F2763;border-color:#8F2763;" id="tab_paid_monster"><a href="#post-job-to-monster" class="requisition-tabs">Monster</a></li><?php
						}
						
						if($feature['JobBoardRefresh'] == 'Y') {
						    echo '<li><a href="#repost-requisition" class="requisition-tabs">Repost</a></li>';
						}
						
						echo '<li class="active"><a href="#export-requisition" id="export_requisition" class="requisition-tabs">Export</a></li>';
						echo '<li><a href="#socialmedia-requisition" id="social_requisition" class="requisition-tabs">Social Media</a></li>';
						?>
					</ul><br>
		         </div>
			</div>

			<div class="tab-content">
			     <div class="row">
			         <div class="col-lg-12" style="border:1px solid #f5f5f5;min-height: 350px;">
				      <div id="purchase-premium-feed" class="ra-tab-content table-responsive">
						      Purchase Premium Feed
    					</div>
    					
    					<div id="export-requisition" class="ra-tab-content table-responsive">
						      Export Requisition
    					</div>
    					<div id="socialmedia-requisition" class="ra-tab-content table-responsive">
						<div id="socialmedia-requisition-body">
       					        	<div id="req_detail_social_nav">
                    					<span id="share_facebook"></span>
                    					<span id="share_twitter"></span>
                    					<span id="share_linkedin"></span>
						</div>
                                                
                    		        </div>
    					</div>
    					
    					<div id="repost-requisition" class="ra-tab-content table-responsive">
						      Repost Requisition
    					</div>
    					
    					
    					<?php
    					if($feature['MonsterAccount'] == "Y") {
    						?>
    						<div id="post-job-to-monster" class="ra-tab-content table-responsive">Post jobs to monster</div>
    						<?php
    					}
    					?>
    					
    					<div id="purchase-zip-recruiter-feed" class="ra-tab-content table-responsive">
    					    Purchase ZipRecruiter Feed
    					</div>
    					
			         </div>
			    </div>
			</div>		
	
		</div>
		<!-- container -->

	</div>

</div>

<script type="text/javascript">
var requisitions_list       = '<?php echo json_encode($req_search_list);?>';
var MonsterJobIndustries    = '<?php echo json_encode($MonsterJobIndustries);?>';
var JobCategories           = '<?php echo json_encode($JobCategories);?>';
var JobOccupations          = '<?php echo json_encode($JobOccupations);?>';
var LastRequestID           = '<?php echo $qi['RequestID'];?>';
var LastMultiOrgID          = '<?php echo $qi['MultiOrgID'];?>';

function joboccupations(jobcat, RequestID, joboccupation) {
	if(jobcat != "") {
		$.ajax({
			type: "POST",
			url: "<?php echo IRECRUIT_HOME;?>monster/monsterJobOccupations.php",
			data: { "cat_id": jobcat, "joboccupation":joboccupation, "RequestID":RequestID },
			success: function(data) {
				$("#JobOccupationsList").html(data);
			}
		});
	}
}

function saveJobOccupationAndInfo(jobcat, joboccupation, btn_obj, RequestID, is_checked, free_or_paid) {
	if(jobcat != "") {
		$.ajax({
			type: "POST",
			url: "<?php echo IRECRUIT_HOME;?>monster/monsterJobOccupations.php",
			data: { "cat_id": jobcat, "joboccupation":joboccupation, "RequestID":RequestID },
			success: function(data) {
				$("#JobOccupationsList").html(data);

				if(is_checked == true) {
					job_boards_list = "Monster";
			    }
				else if(is_checked == false) {
					job_boards_list = "";
				}
				var form_obj = $(btn_obj).parents('form:first');
				var input_data = $(form_obj).serialize();

				var save_url  = "<?php echo IRECRUIT_HOME;?>requisitions/saveRequisitionMonsterFields.php";
				if(free_or_paid == 'free') {
					save_url  += "?job_boards_list="+job_boards_list;
				}
				
				$.ajax({
					type: "POST",
					url: save_url,
					data: input_data,
					success: function(data) {
						$("#job_boards_processing_msg").attr("class", "alert alert-info");
						$("#job_boards_processing_msg").html("Successfully Updated");
						
						postJobToMonster('post-job-to-monster');
					}
				});	
							
			}
		});
	}
}

function saveMonsterInfo(btn_obj, RequestID, is_checked, free_or_paid) {

	if(is_checked == true) {
		job_boards_list = "Monster";
    }
	else if(is_checked == false) {
		job_boards_list = "";
	}
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var save_url  = "<?php echo IRECRUIT_HOME;?>requisitions/saveRequisitionMonsterFields.php";
	if(free_or_paid == 'free') {
		save_url  += "?job_boards_list="+job_boards_list;
	}
	
	$.ajax({
		type: "POST",
		url: save_url,
		data: input_data,
		success: function(data) {
			$("#job_boards_processing_msg").attr("class", "alert alert-info");
			$("#job_boards_processing_msg").html("Successfully Updated");
			
			postJobToMonster('post-job-to-monster');
		}
	});
}

<?php
echo 'joboccupations("' . $REQS['MonsterJobCategory'] . '", "' . $REQS['RequestID'] . '", "' . $REQS['MonsterJobOccupation'] . '")';
?>

</script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME?>js/validate_monster_info.js"></script>
