<?php 
//Set columns
$columns = "ROL.OrgLevelID, ROL.SelectionOrder, R.MultiOrgID, R.RequestID, R.RequisitionID, R.MultiOrgID, R.JobID, 
            R.EmpStatusID, date_format(R.PostDate,'%m/%d/%Y') PostDate, date_format(R.ExpireDate,'%m/%d/%Y') ExpireDate, 
            R.Title, R.Description, R.EEOCode, R.JobGroupCode, R.City, R.State, R.ZipCode, R.Country, R.RequisitionFormID,R.MediaDescription";
//Set where condition
$where  =   array("ROL.OrgID = R.OrgID", "ROL.OrgID = :OrgID", "ROL.RequestID = :RequestID", "ROL.RequestID = R.RequestID", "R.Active='Y'");
//Set order by and limit
$order_by = "ROL.OrgLevelID, ROL.SelectionOrder LIMIT 1";
//Set the parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);

//Get Requisitions and Requisition Orgnization Levels Information
$results = $RequisitionsObj->getReqAndReqOrgLevelsInfo($columns, $where, "", $order_by, array($params));
if(is_array($results['results'])) {
	foreach($results['results'] as $REQ) {
	
	       $emp_status_levels  =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $REQ['RequisitionFormID']);
		$url = PUBLIC_HOME . "jobRequest.php?";

		$url .= "OrgID=";
		$url .= $OrgID;

		if ($REQ ['MultiOrgID']) {
			$url .= "&MultiOrgID=";
			$url .= $REQ ['MultiOrgID'];

		} 
		$url .= "&RequestID=";
		$url .= $REQ ['RequestID'];
		$facebook_url = $url."&source=facebook";
		$twitter_url = $url."&source=twitter";
		$linkedin_url = $url."&source=linkedin";
		$facebook_url_encode = urlencode($facebook_url);

		echo '<div style="width:830px;padding:10px 30px 0px 20px;">';
			//  <div  class="fb-share-button fb_iframe_widget" data-href="'.$facebook_url.'" data-layout="button_count" data-size="large"></div><br><br> 
   $fb_url = "window.open('https://www.facebook.com/sharer/sharer.php?kid_directed_site=0&sdk=joey&u=".$facebook_url_encode."&display=popup&ref=plugin&src=share_button','', 'width=500,height=800')";		
		echo 'Facebook:<a  target="" href="#" onclick="'.$fb_url.'" class="fb-xfbml-parse-ignore" style="vertical-align:middle;"><img src="https://dev.irecruit-us.com/hari/irecruit/images/icons/fb.jpg" /></a><br><br>';
		//echo 'Facebook2: <div class="fb-share-button" data-href="'.$facebook_url.'" data-layout="button_count" data-size="large"></div><br><br>';


	echo 'Twitter: <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-size="large" data-url="'.$twitter_url.'" data-hashtags="iRecruitATS" data-show-count="true" style="verticle-align:bottom;">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script><br><br>'; 

		echo 'LinkedIn:  <script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>
		<script type="IN/Share" data-url="'.$linkedin_url.'"></script>';

        echo '<form enctype="multipart/form-data" method="post"	action="requisitions/socialMediaRequisition.php">';
	         
		echo '<br /><br />';
		echo '<textarea id="desc" name="desc" rows="4" cols="100">'.$REQ['MediaDescription'].'</textarea>';
                $scan = array_diff(scandir(PUBLIC_DIR.'images/'.$OrgID), array('.', '..')); //print_r( $scan);die;
                   $imgname=''; 
                    foreach($scan as $imgck){ 
                             if(!empty($imgck)){  
                              $namecheck = explode(".",$imgck);  
                              if($namecheck[0] == $_REQUEST['RequestID']){
				  $imgname=$imgck; 
				}
			     }
                           
                    }
                    $imgurl = '';
                  if(!empty($imgname)){ 
                    $imgurl =   PUBLIC_HOME.'images/'.$OrgID.'/'.$imgname;
                    } 
		   echo '<br>Media: <img src="'.$imgurl.'" style="max-width:10%"><br>';
                  
        echo '<input type="file"name="Media" size="20">
        <small>The allowed images can be png, jpg, and gif. The recommend maximum image should indicate 1200 x 630 pixels.</small><br><br>
		      				
		<input type="submit" value="Upload " class="btn btn-primary"> 
		<input type="hidden" name="RequestID" value="'.$REQ['RequestID'].'"> 
		</form>';
	} // end foreach
}


if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';	
}
echo '</div>';
?>

