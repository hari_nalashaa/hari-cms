<?php
$pagewidth = "600";
$display = "Y";

if ($process == "Y") {
	
	// Set IndeedFeed Update Information
	$set_info = array (
			"Sponsored   =   :Sponsored" 
	);
	// Set where condition
	$where = array (
			"OrgID       =   :OrgID",
			"RequestID   =   :RequestID" 
	);
	// Set parameters
	$params = array (
			":OrgID"     =>  $OrgID,
			":RequestID" =>  $RequestID,
			":Sponsored" =>  $Sponsored 
	);
	
	// Update Indeed Information
	$IndeedObj->updIndeedFeedInfo ( $set_info, $where, array (
			$params 
	) );
	
	echo '<table border="0" cellspacing="0" cellpadding="3" width="' . $pagewidth . '">';
	echo '<tr><td width="50"></td><td height="100" valign="middle">';
	if ($Sponsored == "no") {
		echo 'This sponsored link has been suspended.<br><br>';
	} else {
		echo 'This sponsored link has been activated.<br><br>';
	}
	echo '</td><td width="50"></td></tr>';
	echo '</table>';
	
	$display = "N";
} // end process

if ($display == "Y") {
    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
	echo '<table border="0" cellspacing="0" cellpadding="3" width="' . $pagewidth . '">';
	
	echo '<tr><td colspan="2">';
	echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo "&nbsp;&nbsp;&nbsp;";
	echo "Title: <b>" . $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo '</td></tr>';
	
	echo '<tr><td colspan="2">';
	
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"RequestID = :RequestID" 
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":RequestID" => $RequestID 
	);
	// Get Indeed Information
	$results = $IndeedObj->getIndeedInformation ( "*", $where, "", array (
			$params 
	) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $IND ) {
			
			echo "Budget Total: $" . money_format ( '%i', $IND ['BudgetTotal'] ) . "<br>";
			echo "Purchase Number: " . $IND ['PurchaseNumber'] . "<br>";
			
			// Set where condition
			$where = array (
					"OrgID = :OrgID",
					"PurchaseNumber = :PurchaseNumber" 
			);
			// Set parameters
			$params = array (
					":OrgID" => $OrgID,
					":PurchaseNumber" => $IND ['PurchaseNumber'] 
			);
			
			// Get purchases information
			$purchases_info = $IndeedObj->getPurchasesInfo ( "*", $where, "", array (
					$params 
			) );
			
			if (is_array ( $purchases_info ['results'] )) {
				foreach ( $purchases_info ['results'] as $PUR ) {
					echo "Posted On: " . $PUR ['PurchaseDate'] . "<br>";
				} // end foreach
			}
			
			echo '<br>';
			echo '<form method="post" action="sponsoredLink.php">';
			echo 'Do you want this link to be active: ';
			echo '<input type="radio" name="Sponsored" value="yes"';
			if ($IND ['Sponsored'] == "yes") {
				echo " checked";
			}
			echo ' onClick="submit()">Yes';
			echo '<input type="radio" name="Sponsored" value="no"';
			if ($IND ['Sponsored'] == "no") {
				echo " checked";
			}
			echo ' onClick="submit()">No';
			echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			echo '<input type="hidden" name="process" value="Y">';
			echo '</form>';
		} // end foreach
	}
	
	echo '</td></tr>';
	
	echo '</table>';
} // end display

echo '<br><p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
?>