<style>
.btn-small {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
}
#vertical-text-full-view-results {
	cursor:pointer;
	background-color: <?php echo $navbar_color;?>;
    border: 1px solid transparent;
	border-radius: 4px;
    color: white;
    font-size: 12px;
    padding: 6px;
    z-index: 999;
    float: left;
}
#vertical-text-detail-view-results {
	cursor:pointer;
	background-color: <?php echo $navbar_color;?>;
    border: 1px solid transparent;
	border-radius: 4px;
    color: white;
    font-size: 12px;
    padding: 6px;
    z-index: 999;
    float: left;
}
.pagination_link {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
	padding:6px;
}
.pagination_link:hover {
	background-color:<?php echo $navbar_color;?>;
	color:white;
	-moz-user-select:none;
	border: 1px solid transparent;
	border-radius: 4px;
	padding:6px;
}
#span_left_page_nav_previous, #span_fullview_page_nav_previous {
	border:0px solid #f5f5f5;
	text-align:left;
	float:left;
	padding-top:5px;
	padding-bottom:5px;
	height:30px;
	text-align:center;
	margin-top:5px;
}
#span_left_page_nav_next, #span_fullview_page_nav_next {
	text-align:right;
	float:right;
	border:0px solid #f5f5f5;
	padding-top:5px;
	padding-bottom:5px;
	height:30px;
	text-align:center;
	margin-top:5px;
}
.current_page_and_total_pages {
	text-align:center;
	float:left;
	border:0px solid #f5f5f5;
	padding-top:5px;
	padding-bottom:5px;
}
ul.tab-content-tabs {
	margin: 0px;
	padding: 0px;
	list-style: none;
}
ul.tab-content-tabs li {
	border-top-left-radius: 6px;
  	border-top-right-radius: 6px;
	background: <?php echo $navbar_color;?>;
	color: #ffffff;
	display: inline-block;
	padding: 7px;
	margin-top: 10px;
	cursor: pointer;
}
ul.tab-content-tabs li.active {
	border-top-left-radius: 6px;
  	border-top-right-radius: 6px;
	background: #F5F5F5;
	border:1px solid <?php echo $navbar_color;?>;
	color: black !important;
	display: inline-block;
	padding: 7px;
	cursor: pointer;
	margin-top: 5px;
}
ul.tab-content-tabs li a {
	color: #ffffff;
	padding: 7px;
}
ul.tab-content-tabs li.active a {
	color: black !important;
	padding: 7px;
	text-decoration: none;
}
.modal {
    display: none;
    position: fixed;
    z-index: 1;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgb(0,0,0);
    background-color: rgba(0,0,0,0.4);
}
.modal-content {
    background-color: #fefefe;
    margin: 15% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 40%;
}
.modal-close {
    color: #aaa;
    float: right;
}
.modal-close:hover,
.modal-close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
.tooltip {
	background: none !important;
	margin-left: 10px !important;
}
.tooltip-inner {
    max-width: 350px;
    width: 350px; 
}
</style>

<div id="page-wrapper">
	<?php 
	include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
	
	if(isset($_GET['msg']) 
        && ($_GET['msg'] == 'applyhotsuc' || $_GET['msg'] == 'clearhotsuc')) {
		?>
		<div class="row">
    		<div class="col-lg-12 alert alert-success" style="text-align: center">
    			<?php 
    			 if(isset($_GET['msg']) && $_GET['msg'] == 'applyhotsuc') {
    			 	echo "Hot status applied successfully";
    			 }
    			 else if(isset($_GET['msg']) && $_GET['msg'] == 'clearhotsuc') {
    			    echo "Hot status cleared successfully"; 	
    			 }
    			?>
    		</div>
    	</div>
		<?php
	}
	else if(isset($_GET['msg']) && $_GET['msg'] == 'sucins') {
        ?>
		<div class="alert alert-success">
            <strong>Success!</strong> Your requisition inserted successfully.
             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
             </button>
        </div>
        <?php
	}
	?>
	
	<div class="row" id="full-view-navigation-bar">
		<div class="col-lg-12">
			<div class="vertical-text" id="vertical-text-detail-view-results">Requisitions detail view</div>
			<span id="results_loading"></span>
			<br>
			<br>
		</div>
	</div>
	
	<div id="openFormWindow" class="openForm">
		<div>
			<p style="text-align:right;">
				<a href="#closeForm" id="ahref_close_form" title="Close Window" onclick="getAssignInternalInfo();">
					<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
					<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
				</a>
			</p>
			
			<div id="iconnect_change_status" class="iconnect_forms_data"></div>
			<div id="iconnect_view_history" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_agree_form" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_pref_form" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_web_form" class="iconnect_forms_data"></div>
			<div id="iconnect_complete_spec_form" class="iconnect_forms_data"></div>
		</div>
	</div>
	
	<div style="text-align:center" id="del_process_message"></div>
	
	<div id="search-results-detail" class="table-responsive" style="display:none">
	<form name="frmRequisitionsSearchResults" id="frmRequisitionsSearchResults" method="post">
			<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover" id="requisitions_full_view_results">
			<?php 
			echo "<tr>";
			
			echo "<td valign=\"bottom\" width=\"110\">";
			echo "<b><a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"date_opened\")'>Date<br>Opened</a></b>";
			echo "</td>";

                        echo "<td valign=\"bottom\" width=\"110\">";
			echo "<b><a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"date_entered\")'>Date<br>Created</a></b>";
			echo "</td>";

			
			echo "<td valign=\"bottom\" width=\"560\">";
			echo "<b>";
			
			echo "<a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"req_id\")'>";
			echo "RequisitionID";
			echo "</a>";
			
			echo "/";
			
			echo "<a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"job_id\")'>";
			echo "JobID, ";
			echo "</a>";
			
			echo "<a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"req_title\")'>";
			echo "Title";
			echo "</a>";
			
			echo "</b>";
			if (isset ( $_GET ['sort'] ) && isset ( $_GET ['field'] ) && ($_GET ['field'] == 'jobid' || $_GET ['field'] == 'reqid' || $_GET ['field'] == 'title')) {
				echo $sort_arrow;
			}
			echo "</td>";
			
			echo "<td valign=\"bottom\" align=\"center\" width=\"20\">";
			echo "<b><a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"days_open\")'>Days<br>Open</a></b>";
			if (isset ( $_GET ['sort'] ) && isset ( $_GET ['field'] ) && $_GET ['field'] == 'days') {
				echo $sort_arrow;
			}
			echo "</td>";
			
			echo "<td valign=\"bottom\" align=\"center\" width=\"20\">";
			echo "<a href='javascript:void(0);' onclick='sortRequisitionsFullView(\"expire_date\")'>";
			echo "<strong>Expire Date</strong>";
			echo "</a>";
			echo "</td>";
			
			echo "<td valign=\"bottom\" align=\"center\" width=\"20\"><b>Applicants</b></td>";
			
			echo "<td valign=\"bottom\" align=\"center\" width=\"20\"><b>Highlight</b></td>";
			
			if ($permit ['Requisitions_Edit'] == 1) {
			
				echo "<td valign=\"bottom\" align=\"center\" width=\"20\"><b>Copy</b></td>";
			
				
				echo "<td valign=\"bottom\" align=\"center\" width=\"20\"><b>Edit</b></td>";
				if ($permit ['Admin'] == 1) {
					echo "<td valign=\"bottom\" align=\"center\" width=\"20\"><b>Delete</b></td>";
				} // end permit
		
			} else { // permit
			
				echo "<td valign=\"bottom\" align=\"center\" width=\"20\"><b>View</b></td>";
			} // end Edit items
			
			echo "<td width=\"20\"><strong>Check All </strong><input type='checkbox' name='chk_req_all' id='chk_req_all' onclick='check_uncheck(this.checked, \"check_requisitions\")'></td>";
			
			echo "</tr>";
		          
			if (is_array ( $req_search_results ['results'] )) {
			
				foreach ( $req_search_results ['results'] as $REQS ) { 
					$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $REQS ['RequestID']);	
					echo '<tr style="background-color:' . $rowcolor . ';border-color:#ffffff">';
                                    
					echo "<td valign=\"top\">" . $REQS ['PostDate'] . "</td>";
					echo "<td valign=\"top\">" . $REQS ['DateEntered'] . "</td>";
					echo "<td valign=\"top\">";
					if ($feature ['MultiOrg'] == "Y") {
						echo $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $REQS ['RequestID'] ) . "<br>";
					} // end feature
					echo $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
						
					echo "<br>";
					echo $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
					echo "</td>";
						
					if ($Active == "N") {
						$REQS ['Open'] = $REQS ['Closed'];
					}
					
					if($REQS ['Open'] < 0) $REQS ['Open'] = 0;
					
					echo "<td align=\"center\" valign=\"middle\">" . $REQS ['Open'] . "</td>";
					
					echo "<td align=\"center\" valign=\"middle\">" . $REQS ['ExpireDate'] . "</td>";
					
					echo '<td align="center" valign="middle">';
						
					// Set where condition
					$where = array (
							"OrgID       = :OrgID",
							"RequestID   = :RequestID"
					);
					// Set parameters
					$params = array (
							":OrgID"     => $OrgID,
							":RequestID" => $REQS ['RequestID']
					);
					// Get Job Applications Information
					$resultsCNT = $ApplicationsObj->getJobApplicationsInfo ( "count(*) as cnt", $where, '', '', array (
							$params
					) );
					$ttcnt = $resultsCNT ['results'] [0] ['cnt'];
					if ($ttcnt > 0) {
		
						//$reportlink = "action=ap";
						//$reportlink .= "&OrgID=" . $OrgID;
						//if($REQS ['MultiOrgID'] != "") $reportlink .= "&MultiOrgID=" . $REQS ['MultiOrgID'];
						//$reportlink .= "&RequestID=" . $REQS['RequestID'];
						//$reportlink .= "&menu=7";
			
					    $min_max_dates = G::Obj('Applications')->getMinAndMaxDateOfApplicationsByRequestID($OrgID, $REQS ['RequestID']);
					    
						$reportlink = IRECRUIT_HOME . "reports/getApplicantsStatusByRequisition.php?menu=7";
						$reportlink .= "&RequestID=" . $REQS['RequestID'];
						$reportlink .= "&FromDate=" . $min_max_dates['FromDate'];
						$reportlink .= "&ToDate=" . $min_max_dates['ToDate'];
						
						echo $ttcnt . '&nbsp;';
						if (substr ( $USERROLE, 0, 21 ) != 'master_hiring_manager') {
							echo '<a href="' . $reportlink . '">';
							echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_text.png" border="0" title="Applicant Report" style="margin:0px 0px 0px 0px;">';
							echo '</a>';
						}
					} else { // end permit
			
						echo $ttcnt;
					}
						
					echo '</td>';

					$highlight_image = "";
					if($REQS ['Highlight'] == 'Hot') {
						$highlight_image = $hot_job_image;
					}
					else if($REQS ['Highlight'] == 'New') {
						$highlight_image = $new_job_image;
					}
					
					echo "<td valign=\"bottom\" align=\"center\" width=\"20\">".$highlight_image."</td>";
					
					if ($permit ['Requisitions_Edit'] == 1) {
			
						if ($action != 'copy') {
								
							$linkc = IRECRUIT_HOME . 'request/assign.php?RequestID=' . $REQS ['RequestID'] . '&MultiOrgID=' . $multiorgid_req . '&action=copy&Active=' . $Active;
							
							echo '<td align="center">';
							echo '<a href="' . $linkc . '">';
							echo '<img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy" style="margin:0px 3px -4px 0px;"></a></td>';
						}

						if ($action != 'edit') {
							?>
							<td align="center">
								<a href="javascript:void(0);" onclick='getEditRequisition("<?php echo $REQS ['RequestID'];?>", "<?php $REQS ['MultiOrgID'];?>")'>
									<img src="<?php echo IRECRUIT_HOME ?>images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;">
								</a>
							</td>
							<?php
						}
							
						if ($permit ['Admin'] == 1) {
							$req_del_title = $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
							echo '<td align="center"><a href=\'javascript:void(0);\' onclick=\'delRequisition("'.$REQS ['RequestID'].'", "'.$req_del_title.'", this)\'><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px;"></a></td>';
						} // end permit
		
						
					} else { // else action != Requisition_Edit
			
						$link = IRECRUIT_HOME . "requisitions/viewListing.php?RequestID=" . $REQS ['RequestID'];
						if ($AccessCode != "") {
							$link .= "&k=" . $AccessCode;
						}
						$onclickex = ' onClick="window.open(\'' . $link . '\',\'\',\'width=800,height=600,resizable=yes,scrollbars=yes\'); return false;"';
			
						echo '<td align="center"><a href="' . $link . '" target="_blank"' . $onclickex . '><img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View Requisition" style="margin:0px 3px -4px 0px;"></a></td>';
					} // end else Requisition_Edit
		
					echo "<td><input type='checkbox' name='chk_req[".$REQS ['RequestID']."]' id='chk_req_".$REQS ['RequestID']."' class='check_requisitions'></td>";
					
					echo "</tr>";				
						
					if ($rowcolor == "#eeeeee") {
						$rowcolor = "#ffffff";
					} else {
						$rowcolor = "#eeeeee";
					}
				} // end foreach
			}
			
			if($total_requisitions_count > $LimitRequisitions) {
			  	  	$left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $LimitRequisitions, $total_requisitions_count, '', '');
		
					echo '<tr>';
					echo '<td colspan="100%">';
					echo '<div style="padding:0px;margin-top:0px;overflow:hidden;">';
					echo '<div style="text-align:center;width:100%;float: left;border:0px solid #f5f5f5;padding-top:5px;padding-bottom:5px;height:40px">';
					echo '<div style="float:left">&nbsp;<strong>Per page:</strong> '.$LimitRequisitions.'</div>';
					echo '<div style="float:right">';
					echo '<input type="text" name="current_page_fullview_number" id="current_page_fullview_number" value="'.$left_pagination_info['current_page'].'" style="width:50px;text-align:center" maxlength="4">';
					echo ' <input type="button" name="btnFullViewPageNumberInfo" id="btnFullViewPageNumberInfo" value="Go" style="background-color:'.$navbar_color.';color:white;border:2px solid '.$navbar_color.'" onclick="getRequisitionsByPageNumber(document.getElementById(\'current_page_fullview_number\').value, \'\', \'\', \'no\')">';
					echo '</div>';
					echo '</div>';
					echo '<div id="span_fullview_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left;padding-left:0px;">'.$left_pagination_info['previous'].'</div>';
					echo '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
					echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];
					echo '</div>';
					echo '<div id="span_fullview_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right;padding-right:0px;">'.$left_pagination_info['next'].'</div>';
					echo '</td>';
					echo '</tr>';
			}
			?>
			
			<tr>
				<td colspan="100%">
					<select name="ddlProcessRequisitions" id="ddlProcessRequisitions" class="form-control width-auto-inline">
						<option value="">Select Action</option>
						<option value="Inactive">Inactive</option>
						<?php 
						if($highlight_req_settings['HighlightHotJob'] == 'Yes') {
							?>
							<option value="ApplyHotJob">Apply Hot Job</option>
						    <option value="ClearHotJob">Clear Hot Job</option>
							<?php
						}
						?>
					</select>
					&nbsp;
					<input type="button" name="btnProcessRequisitions" id="btnProcessRequisitions" value="Process" class="btn btn-primary" onclick="processRequisitions(document.getElementById('ddlProcessRequisitions').value, this);">
				</td>
			</tr>		  
			</table>
	</form>

	<div id="process_requisitions"><br><br><br><br></div>
	
	</div>

	
	<div class="row">
		
		<div class="col-lg-3" id="requisitions-minified-search-results" style="border: 1px solid #F5F5F5;display: block">
			<div class="row">
				<div class="col-lg-12"><span style="font-size:16px;padding-top:10px;font-weight:bold">Requisitions List</span></div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-12">
					<input type="button" class="btn btn-primary vertical-text" id="vertical-text-full-view-results" value="Requisitions full view">
				</div>
			</div>
			<br>
			<form name="frmFilters" id="frmFilters" method="post">
    			<div class="row">
    				<div class="col-lg-12">
                        <?php 
                        if($feature['MultiOrg'] == "Y") {
							$where_info     =   array("OrgID = :OrgID");
                            $params_info    =   array(":OrgID"=>$OrgID);
                            $columns        =   "OrgID, MultiOrgID, OrganizationName";
                            $orgs_results   =   G::Obj('Organizations')->getOrgDataInfo($columns, $where_info, '', array($params_info));
                            $orgs_list      =   $orgs_results['results'];
                            ?>
                            Organizations List: 
                            <select name="ddlOrganizationsList" id="ddlOrganizationsList">
                            	<option value="">All</option>
                            	<?php
                            	for($ol = 0; $ol < count($orgs_list); $ol++) {
                            	    ?><option value="<?php echo $orgs_list[$ol]['OrgID']."-".$orgs_list[$ol]['MultiOrgID'];?>"><?php echo $orgs_list[$ol]['OrganizationName']?></option><?php
                            	}
                            	?>	
                            </select>
                            <?php
                        }
                        else {
                            ?><input type="hidden" name="ddlOrganizationsList" id="ddlOrganizationsList" value=""><?php
                        }
                        ?>
    				</div>
    			</div>
    			<br>
    
				<div class="row">
    				<div class="col-lg-12">
    						Status: <select name="ddlFilterActiveStatus" id="ddlFilterActiveStatus" onchange="getRequisitionsListByFilters('yes');">
    							<option value="A" <?php if($Active == "A") echo 'selected="selected"';?>>All</option>
    							<option value="Y" <?php if($Active == "Y") echo 'selected="selected"';?>>Active</option>
    							<option value="N" <?php if($Active == "N") echo 'selected="selected"';?>>Inactive</option>
    							<?php 
    							if ($feature ['RequisitionApproval'] == "Y") {
    								?>
    								<option value="R" <?php if($Active == "R") echo 'selected="selected"';?>>Pending</option>
    								<option value="NA" <?php if($Active == "NA") echo 'selected="selected"';?>>Not Approved</option>
    								<?php
    							}
    							?>
    						</select>
    						<span id="req_list_process_msg"></span>
    				</div>
    			</div>
    			<br>
    			<div class="row">
    				<div class="col-lg-12">
    					<?php
    					if ($feature ['InternalRequisitions'] == "Y") {
    					  ?>
    						Listed On: <select name="PresentOn" id="PresentOn" onchange="getRequisitionsListByFilters('yes');">
    							<option value="INTERNALANDPUBLIC" <?php if($_REQUEST['PresentOn'] == "INTERNALANDPUBLIC" || $_REQUEST['PresentOn'] == '') echo 'selected="selected"';?>>INTERNAL AND PUBLIC</option>
								<option value="INTERNALONLY" <?php if($_REQUEST['PresentOn'] == "INTERNALONLY") echo 'selected="selected"';?>>INTERNAL ONLY</option>
								<option value="PUBLICONLY" <?php if($_REQUEST['PresentOn'] == "PUBLICONLY") echo 'selected="selected"';?>>PUBLIC ONLY</option>
    						</select>
    					  <?php
    					}
    					else {
    					  ?>
    					  <input type="hidden" name="PresentOn" id="PresentOn" value="">
    					  <?php
    					}
    					?>
    				</div>
    			</div>
    			<br>
			</form>

			<div class="row">
				<div class="col-lg-12">
					<form name="frmSortOptions" id="frmSortOptions" method="post">
						<select name="ddlReqDetailSort" id="ddlReqDetailSort">
							<option value="">Sort by</option>
							<option value="date_opened" <?php if(!isset($qi['SortByKey']) || $qi['SortByKey'] == 'date_opened' || $qi['SortByKey'] == "" || $qi['SortByKey'] == "Requisitions.PostDate") echo 'selected="selected"'; ?>>Date Opened</option>
							<option value="date_entered" <?php if($qi['SortByKey'] == 'date_entered') echo 'selected="selected"'; ?>>Date Created</option>
							<option value="req_id" <?php if($qi['SortByKey'] == 'req_id') echo 'selected="selected"'; ?>>RequisitionID</option>
							<option value="job_id" <?php if($qi['SortByKey'] == 'job_id') echo 'selected="selected"'; ?>>JobID</option>
							<option value="req_title" <?php if($qi['SortByKey'] == 'req_title') echo 'selected="selected"'; ?>>Requisition Title</option>
							<option value="days_open" <?php if($qi['SortByKey'] == 'days_open') echo 'selected="selected"'; ?>>Days Open</option>
						</select>
						<select name="ddlReqDetailSortType" id="ddlReqDetailSortType">
							<option value="ASC" <?php if($sort_type == 'ASC') echo 'selected="selected"'; ?>>Ascending</option>
							<option value="DESC" <?php if($sort_type == 'DESC' || !isset($sort_type) || $sort_type == "") echo 'selected="selected"'; ?>>Descending</option>
						</select>
						<input type="button" name="btnSort" id="btnSort" value="Sort" class="btn-small" onclick="getRequisitionsListByFilters('no');">
						<br><br>
						<input type="text" name="txtKeyword" id="txtKeyword" value="<?php echo $qi['Keyword'];?>">
						<input type="button" name="btnSearch" id="btnSearch" value="Search" class="btn-small" onclick="getRequisitionsListByFilters('yes');">
						<br>
						(Requisition Title, RequisitionID, JobID)<br>
						Match with:<input type="radio" name="rdKeywordType" id="rdKeywordType" value="W" <?php if(!isset($qi['KeywordMatch']) || $qi['KeywordMatch'] == "" || $qi['KeywordMatch'] == "W") echo 'checked="checked"';?>>
						Start with:<input type="radio" name="rdKeywordType" id="rdKeywordType" value="S" <?php if($qi['KeywordMatch'] == "S") echo 'checked="checked"';?>>
					</form>
				</div>
			</div>
			<br>			
	                
			<div id="requisitions-minified-results" class="row">
				<?php
				if (is_array ( $req_search_results ['results'] )) {
					foreach ( $req_search_results ['results'] as $REQS ) {
                        $multiorgid_req         =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $REQS ['RequestID']);
                        $RequisitionTitle       =   $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $REQS ['RequestID'] );
                        $RequisitionIDJobID     =   "RequisitionID: ". $REQS['RequisitionID']."<br>";
                        $RequisitionIDJobID    .=   "JobID: ". $REQS['JobID']."<br>";
						
						####################

                        // Set columns
                        $columns    =   "MAX(OrgLevelID) AS MaxOrgLevelID";
                        // Set condition and parameters
                        $where      =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
                        $params     =   array (":OrgID" => $OrgID, ":MultiOrgID" => $multiorgid_req);
                        
                        // Get Org Levels Information
                        $results    =   $OrganizationsObj->getOrganizationLevelsInfo ( $columns, $where, '', array ($params) );
                        $OrgLevels  =   $results ['results'] [0] ['MaxOrgLevelID'];
                        
						$i = 1;
						if ($OrgID == "I20141112") { // Cable & Wireless
						    $OrgLevels = 1;
						}
						
						while ( $i <= $OrgLevels ) {
						
						    // Set columns
						    $columns      =   "OLD.CategorySelection, OL.OrganizationLevel";
						    $table_name   =   "OrganizationLevels OL, RequisitionOrgLevels ROL, OrganizationLevelData OLD";
						
						    // Set condition
						    $where        =   array (
                    						        "OL.OrgID           =   ROL.OrgID",
                    						        "ROL.OrgID          =   OLD.OrgID",
                    						        "OL.MultiOrgID      =   OLD.MultiOrgID",
                    						        "OL.MultiOrgID      =   :MultiOrgID",
                    						        "ROL.OrgLevelID     =   OLD.OrgLevelID",
                    						        "ROL.OrgLevelID     =   OL.OrgLevelID",
                    						        "ROL.SelectionOrder =   OLD.SelectionOrder",
                    						        "ROL.OrgID          =   :OrgID",
                    						        "ROL.RequestID      =   :RequestID",
                    						        "ROL.OrgLevelID     =   :OrgLevelID"
                    						    );
						    // Set params
						    $params       =   array (
                    						        ":MultiOrgID"       =>  $multiorgid_req,
                    						        ":OrgID"            =>  $OrgID,
                    						        ":RequestID"        =>  $REQS ['RequestID'],
                    						        ":OrgLevelID"       =>  $i
                    						    );
						    // Get Org Requisition Levels Information
						    $resultsIN            =   $OrganizationsObj->getOrgAndReqLevelsInfo ( $table_name, $columns, $where, '', array ($params) );
						
						    $SelectionTitle       =   "";
						    $SelectionResults     =   "";
						
						    if (is_array ( $resultsIN ['results'] )) {
						        foreach ( $resultsIN ['results'] as $Selection ) {
						            $SelectionTitle = $Selection ['OrganizationLevel'];
						            $SelectionResults .= $Selection ['CategorySelection'] . ", ";
						        } // end foreach
						    }
						
						    if (($OrgID != "I20111019") && ($OrgID != "I20091101")) {
						        if (substr ( $SelectionResults, 0, - 2 ) != "") {
						            $RequisitionIDJobID .= $SelectionTitle . ': ' . substr ( $SelectionResults, 0, - 2 ) . '<br>';
						        }
						    } // end if
						
						    $i ++;
						} // end OrgLevels
						
						
						####################
						?> 
						<div class="col-lg-12 col-md-12 col-sm-12">
						  <a href="javascript:void(0);" 
					        data-toggle="tooltip" 
							data-placement="right" 
							data-html="true" 
							title="<?php echo $RequisitionIDJobID;?>" 
							onclick='getRequisitionTabInfo("<?php echo $REQS ['RequestID']; ?>", "<?php echo $REQS ['MultiOrgID'];?>")'>
							 <?php echo $RequisitionTitle;?>
							</a>
						</div>
						<?php 
					}
				}

				if($total_requisitions_count > $LimitRequisitions) {
				  	  $left_pagination_info = $PaginationObj->getPageNavigationInfo($Start, $LimitRequisitions, $total_requisitions_count, '', '');
					  ?>
					  <div class="col-lg-12">
						  <div class="row">
							  	<div class="col-lg-6">
							  		<strong>Per page:</strong> <?php echo $LimitRequisitions;?>
							  	</div>
							  	<div class="col-lg-6" style="text-align:right">
								  	<input type="text" name="current_page_number" id="current_page_number" value="<?php echo $left_pagination_info['current_page'];?>" style="width:50px;text-align:center" maxlength="4">
								  	<input type="button" class="btn-small" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" onclick="getRequisitionsByPageNumber(document.getElementById('current_page_number').value)">
							  	</div>
						  </div>
						  
						  <div class="row">
							  <div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">
							  	<?php echo $left_pagination_info['previous'];?>
							  </div>
							  <div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">
							  	<?php echo $left_pagination_info['current_page'] . " - " . $left_pagination_info['total_pages'];?>
							  </div>
							  <div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">
							  	<?php echo $left_pagination_info['next'];?>
							  </div>
						  </div>
					  </div>
				  	  <?php
				}
				
				?>
			</div>
			
		</div>
			
		<div class="col-lg-9" id="applicant-detail-view" style="border: 1px solid #F5F5F5;display:block;">
			<div id="process_message_req_details" style="text-align: center"></div>
			
			<div style="text-align: center;" id="req_detail_pagination_navigation">
					<span id='nav_previous' style="float: left; font-weight: bold;">
						<a href="javascript:void(0);" class="pagination_link" id="req_detail_prev" onclick="getPrevNextApplicant('previous')">
							<i class='fa fa-arrow-left' aria-hidden='true'></i> Previous
						</a>
					</span>
					<span id='nav_next' style="float: right; font-weight: bold;">
						<a href="javascript:void(0);" class="pagination_link" id="req_detail_next" onclick="getPrevNextApplicant('next')">
							Next <i class='fa fa-arrow-right' aria-hidden='true'></i>
						</a>
					</span>
			</div>

			<div class="row">
                <div class="col-lg-12" id="req_detail_header_info">
                    <table>
                        <tr>
                            <td>
                                <strong>Title: </strong>
    				            <span id="header_requisition_title"></span>&nbsp;<br>
    				            <strong>RequisitionID / JobID: </strong>
    				            <span id="header_requisition_requisition_id"></span>/<span id="header_requisition_job_id"></span>&nbsp;<br>
    				            <strong>Date Posted: </strong><span id="header_requisition_post_date"></span><br>
                				<strong>Expiration Date: </strong><span id="header_requisition_expire_date"></span><br>
                                <strong>Applicants Count: </strong><a href="#applicants" class="requisition-tabs" id="header_number_of_applicants"></a><br>
                                <strong>Assign Forms Count: </strong><a href="#assign-iconnect-form" class="requisition-tabs" id="header_number_of_assign_forms"></a>&nbsp;<br>
                                <span id="requisition_stage"></span>
                				<span id="header_highlight_requisition"></span><br>
                				<!-- <span id="header_requisition_internal"></span>  -->                				
                            </td>
                        </tr>
                        <tr><td colspan="3" height="30"></td></tr>
                    </table>
    			</div>
						    
    			<div class="col-lg-4" id="advert_header_trafficboost">
    				<img src="<?php echo IRECRUIT_HOME;?>images/ziprecruiter_jobboard.png">
                    <br><input type="radio" onclick="moveToJobBoardPosting('purchase-zip-recruiter-feed')">&nbsp; Sponsor job on ZipRecruiter.com
                </div>    				
                <div class="col-lg-4" id="advert_header_indeed">
                    <img src="<?php echo IRECRUIT_HOME;?>images/indeed_jobboard.png"><br>
                    <input type="radio" onclick="moveToJobBoardPosting('purchase-premium-feed')">&nbsp; Sponsor job on Indeed.com
                </div>
                <div class="col-lg-4" id="advert_header_monster">
                    <img src="<?php echo IRECRUIT_HOME;?>images/monster_jobboard.jpg"><br>
                    <input type="radio" onclick="moveToJobBoardPosting('post-job-to-monster')">&nbsp; Sponsor job on Monster.com
    			</div>
    						 
			</div>
			
			<div style="text-align: left;margin-top: 10px;margin-bottom: 10px;" id="req_detail_social_nav">
				<span id="share_facebook"></span>
				<span id="share_twitter"></span>
				<span id="share_linkedin"></span>
			</div>
			
			<form name="frmRequisitionDetailInfo" id="frmRequisitionDetailInfo"
				method="post">
				<input type="hidden" name="GuIDRequisition" id="GuIDRequisition" value="<?php echo $GuIDRequisition;?>">
				<input type="hidden" name="Active" id="Active" value="<?php echo $Active?>">
				<input type="hidden" name="RequestID" id="RequestID" value="<?php echo $req_search_results['results'][0]['RequestID'];?>">
				<input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
				<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $req_search_results['results'][0]['MultiOrgID'];?>">
				<input type="hidden" name="RequisitionsCount" id="RequisitionsCount" value="<?php echo $total_requisitions_count;?>">
				<input type="hidden" name="IndexStart" id="IndexStart" value="<?php echo $Start;?>">
				<input type="hidden" name="RequisitionsLimit" id="RequisitionsLimit" value="<?php echo $LimitRequisitions;?>">
				<input type="hidden" name="AccessCode" id="AccessCode" value="<?php echo $AccessCode;?>">
				<input type="hidden" name="CurrentPage" id="CurrentPage" value="1">
				<input type="hidden" name="FeatureMonsterAccount" id="FeatureMonsterAccount" value="<?php echo $feature['MonsterAccount'];?>">
				<input type="hidden" name="current_sel_tab" id="current_sel_tab" value="<?php echo "#".$active_tab;?>">
				<input type="hidden" name="JobBoardsStatus" id="JobBoardsStatus">
				
				<input type="hidden" name="MonsterJobPostType" id="MonsterJobPostType">
				<input type="hidden" name="MonsterJobPostDuration" id="MonsterJobPostDuration">
				<input type="hidden" name="MonsterJobIndustry" id="MonsterJobIndustry">
				<input type="hidden" name="MonsterJobCategory" id="MonsterJobCategory">
				<input type="hidden" name="MonsterJobOccupation" id="MonsterJobOccupation">
			</form>	
			<div class="tab-content">
					<ul class="tab-content-tabs">
						<li class="active"><a href="#view-requisition" class="requisition-tabs">View Requisition</a></li>
						<?php 
						if ($permit ['Requisitions_Edit'] == 1) {
							?><li><a href="#edit-requisition" class="requisition-tabs">Edit Requisition</a></li><?php
						}
						?>
						<li><a href="#assign-iconnect-form" class="requisition-tabs">Assign Forms</a></li>
						<?php
						$desc = G::Obj('PrescreenQuestions')->getPrescreenTextInfo($OrgID, array("OrgID = :OrgID"));
						if($desc['Intro'] != ""
						  && $desc['Disclaimer'] != ""
						  && $desc['Rejection'] != "") {
                            ?><li><a href="#prescreen-questions" class="requisition-tabs">Prescreen Questions</a></li><?php
                        }
						?>						
						<li><a href="#auto-forward-list" class="requisition-tabs">Auto Forward</a></li>
						<li><a href="#requisition-notes" class="requisition-tabs">Requisition Stage</a></li>
						<li><a href="#applicants" class="requisition-tabs">Applicants</a></li>
						<li><a href="#job-board-posting" id="header_job_board_posting">Advertise Requisitions</a></li>
						<li><a href="#requisition-history" class="requisition-tabs">Requisition History</a></li>
						<li><a href="#requisition-attachments" class="requisition-tabs">Requisition Attachments</a></li>
						<li id="tab-interview-admin" style="display: none;"><a href="#interview-admin" class="requisition-tabs">Interview/Criteria</a></li>
						<li id="tab-interview-results" style="display: none;"><a href="#interview-results" class="requisition-tabs">Interview Results</a></li>
					</ul>
					
					<div id="view-requisition" class="ra-tab-content">
						View Requisition
					</div>
					
					<?php 
						if ($permit ['Requisitions_Edit'] == 1) {
							echo '<div id="edit-requisition" class="ra-tab-content">
									Edit Requisition
								  </div>';
						}
					?>					
					
					<div id="assign-iconnect-form" class="ra-tab-content">
						Assign Forms
					</div>
					
					<div id="prescreen-questions" class="ra-tab-content">
						Prescreen Questions
					</div>
					
					<div id="auto-forward-list" class="ra-tab-content">
						Auto Forward
					</div>
					
					<div id="requisition-notes" class="ra-tab-content">
			            Requisition Stage:
						&nbsp;
						<input type="button" name="btnUpdReqStage" id="btnUpdReqStage" class="btn btn-primary" value="Update" onclick="updateRequisitionStage()">
					</div>
					
					<div id="applicants" class="ra-tab-content">
						Applicants
					</div>
					
					<div id="requisition-history" class="ra-tab-content">
						Requisition History
					</div>
					
					<div id="requisition-attachments" class="ra-tab-content">
						Requisition Attachments
					</div>

					<div id="interview-admin" class="ra-tab-content"></div>
					<div id="interview-results" class="ra-tab-content"></div>
					
			</div>		
	
		</div>

	</div>

</div>

<script type="text/javascript">
var requisitions_list       =   '<?php echo json_encode($req_search_list);?>';

var MonsterJobIndustries    =   '<?php echo json_encode($MonsterJobIndustries);?>';
var JobCategories           =   '<?php echo json_encode($JobCategories);?>';
var JobOccupations          =   '<?php echo json_encode($JobOccupations);?>';

var LastRequestID           =   '<?php echo $qi['RequestID'];?>';
var LastMultiOrgID          =   '<?php echo $qi['MultiOrgID'];?>';

function moveToJobBoardPosting(jobboard_selection) {
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	location.href = irecruit_home + 'jobBoardPosting.php?menu=3&active_tab='+jobboard_selection+'&RequestID='+req_id+'&job_board_type=paid';
}

$(document).ready(function() {
$('body').tooltip({selector: '[data-toggle="tooltip"]'});
	//$('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function() { 
    $('#applicants-list-by-req').DataTable({
        responsive: true,
        aLengthMenu: [
          [50, 100, -1],
          [50, 100, "All"]
        ]
    });
});
</script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME?>js/validate_monster_info.js"></script>
