<div class="panel-body">
	<form method="post" enctype="multipart/form-data">
			<input type="hidden" name="MAX_FILE_SIZE" value="10485760">
			<div class="table-responsive">
			<table
				class="formtable table table-striped table-bordered table-hover"
				border="0" cellspacing="3" cellpadding="5" width="100%">
				
				<tr>
					<td valign="top" align="right">
						<a href="<?php echo IRECRUIT_HOME;?>requisitions/requisitionForm.php?menu=8">
							<b style="font-size:8pt;COLOR:#000000">Go Back</b><img src="<?php echo IRECRUIT_HOME;?>images/icons/arrow_undo.png" title="Back" style="margin:0px 0px -4px 3px;" border="0">
						</a>
					</td>
				</tr>
				
				<tr>
					<td><b>Edit Question</b></td>
				</tr>

				<tr>
					<td>
					<?php include IRECRUIT_VIEWS . 'forms/AdminQuestions.inc';?>
					</td>
				</tr>

				<tr>
					<td align="center" height="60" valign="middle">
						<input type="hidden" name="questionid" value="<?php echo $QuestionID ?>" /> 
						<input type="hidden" name="form" value="<?php echo $FormID ?>" /> 
						<input type="hidden" name="section" value="<?php echo $section ?>" /> 
						<input type="hidden" name="action" value="<?php echo $action ?>" /> 
						<input type="submit" name="process" value="Update" class="btn btn-primary" />
						<?php
						if ($process) {
							echo '<input type="submit" name="finish" value="Finish" class="btn btn-primary"/>';
						} else {
							echo '<input type="submit" name="finish" value="Cancel" class="btn btn-primary"/>';
						}
						?>
					</td>
				</tr>
				
				</table>
				</div>
				</form>
	
				<div class="table-responsive">
				<p><b>Preview</b></p>
				<table class="formtable table table-striped table-bordered table-hover" border="0" cellspacing="3" cellpadding="5" width="100%">
				<?php
				echo '<tr><td colspan="100%">';
				$colwidth = "220";
				$SectionID = "";
				echo include COMMON_DIR . 'application/DisplayQuestions.inc';
				echo '</td></tr>';
				?>
				</table>
				</div>

</div>