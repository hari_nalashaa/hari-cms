<style type="text/css">
    .col_sec{display:inline-block;padding:10px;}
    .child_sec input{margin-left:5px;}
    .child_sec input[type="radio"]{margin-right:5px;}
    .add_checklist_section{padding:15px 0 20px 0px;}
    .add_checklist_section a{font-size:16px;cursor:pointer;text-decoration:none;}
    #add_checklist_form_sec{display:none;}
</style>

<div id="page-wrapper">
	<?php 
		if (defined('IRECRUIT_VIEWS')) {
			include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';
		}
		if ($_REQUEST['active'] == 'n') { 
			$a = 'n'; 
		} else if ($_REQUEST['active'] == 'p') { 
			$a = 'p'; 
		} else { 
			$a = 'y'; 
		}

	?>
	<?php 
	 include 'formsInternal/FormsInternalTitle.inc';
	 include IRECRUIT_DIR . 'formsInternal/FormsInternalPageContent.inc';
?>

	<div class="add_checklist_section">
		<a class="add_checklist"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/add.png" title="Add" /> Add Package</a>


</div>
	<div class="row" id="add_checklist_form_sec">
		<div class="col-lg-12">

			<div class="panel panel-default">

				<div class="panel-body">
					<form action="iconnectpackage.php" name="frmAddChecklist" id="frmAddChecklist" method="post">
    					<div class="">
    	           			<label for="">Package Name</label>
    	           			<input type="text" name="PackageName" placeholder="PackageName" value="<?php echo $package[PackageName]; ?>"/ required>
    	           			<input type="submit" name="btnSubmit" value="Add Package" class="btn btn-primary">
    	           		</div>
    	           	</form>
				</div>
			</div>
		</div>
	</div>
	
		
	<?php if(isset($_REQUEST['action']) && $_REQUEST['action'] == 'copyform'){?>
		<div class="row" id="">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form action="checklist.php" name="frmAddChecklist" id="frmAddChecklist" method="post">
    					<div class="">
    	           			<label for="">What name do you want to call your new Checklist Name?</label>
    	           			<input type="text" name="NewChecklistName" placeholder="ChecklistName" value="<?php echo $checklist[ChecklistName]; ?>"/>
    	           			<input type="hidden" name="NewChecklistID" value="<?php echo $_REQUEST["ChecklistID"];?>" />
    	           			<input type="hidden" name="action" value="<?php echo $_REQUEST["action"];?>" />
    	           			<input type="hidden" name="active" value="<?php echo $_REQUEST["active"];?>" />
    	           			<input type="submit" name="btnSubmit" value="Copy Form" class="btn btn-primary">
    	           		</div>
    	           	</form>
				</div>
			</div>
		</div>
	</div>
	<?php }?>
	<!-- row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					   <table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">
					   		<thead>
					   			<tr>
					   				<th style="text-align:left">Package Name</th>
					   				<th style="text-align:center">Edit Package </th>
					   				<!--<th style="text-align:center">Assign Package</th>
					   				<th style="text-align:center">Copy</th>
					   				<th style="text-align:center">Lock</th>-->
					   				<th style="text-align:center">Delete</th> 
																		
					   			</tr>
					   		</thead>
					   		<tbody>
					   			<?php 
								if (count($package_info) == 0) {
									echo "<tr><td colspan=\"6\">There are no Packages configured.</td></tr>";
								}  
					   			foreach ($package_info as $package) { ?>
    					   			<tr>
    					   				<td style="text-align:left"><span><?php echo $package[PackageName]; ?></span></td>
    					   				<td style="text-align:center">
									<a href="<?php echo IRECRUIT_HOME;?>iconnectpackage.php?IconnectPackagesHeaderID=<?php echo $package[IconnectPackagesHeaderID]; ?>"><img src="<?php echo IRECRUIT_HOME; ?>images/icons/pencil.png" title="Edit Package" /></a>
									</td>
    					   				
									<td style="text-align:center"><?php
                                                                      
	                                                                $deletedata =  G::Obj('IconnectPackages')->geticonnectPackagesHeader($package['IconnectPackagesHeaderID']);
                                                                       
                                                                        if(empty($deletedata)){
					                                $del_link = 'formsInternal/getIconnectpackage.php?IconnectPackagesHeaderID='.$package['IconnectPackagesHeaderID'];
									$del_link .= '&headerdelete=' . $package['IconnectPackagesHeaderID'];
					                                 $confirm_msg = 'Are you sure you want to remove the following Package?\n\n'. $package[PackageName];
					                                        echo '<a href="javascript:void(0);"';
										echo ' onclick=\'deleteAssignInternalInfo("'.$del_link.'", "'.$confirm_msg.'")\'>';
										echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>'; 
									    }else{
                                                                                echo '-';
                                                                            } ?>

                                                                         </td>
    					   			</tr>
					   			<?php } ?>
					   		</tbody>
					   </table>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>

<script type="text/javascript">


function deleteAssignInternalInfo(callback_url, confirm_msg) { 
	
	$("#assign-iconnect-form").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	if(confirm(confirm_msg)) {
		var request = $.ajax({
			method: "POST",
	  		url: callback_url+'&display_app_header=no',
			type: "POST",
			success: function(data) {
				$("#assign-iconnect-form").html(data);
                                self.location.reload();

	    	}
		});
	}
}


$(document).ready(function(){
	
	$(".add_checklist").on("click",function(){
		$("#add_checklist_form_sec").toggle();
	});

	$(".checklist_name").click(function(){
		var get_checklist_id = $(this).attr('id');
		var checklist_arr  = get_checklist_id.split("_");
		var ChecklistID      = checklist_arr[1];
		var checklist_val = $(this).prev().text();
		$(this).parent().html("<input type='text' name='ChecklistName' id='ChecklistName' value='"+checklist_val+"' />");
		$("#ChecklistName").change(function(){
			var ChecklistName = $(this).val();
			console.log(ChecklistName);
			$.ajax({
        		type: "POST",
        		url: "updateChecklistName.php",
        		data:'ChecklistID='+ ChecklistID+'&ChecklistName='+ChecklistName,
        		success: function(data){
        			//$("#ChecklistName").parent().html("<span>"+ChecklistName+"</span> <a class='checklist_name' id='checklist_"+ChecklistID+"'><img src='https://dev.irecruit-us.com/hari/irecruit/images/icons/pencil.png' title='Edit Checklist Name'></a>");
        			location.reload();
        		}
        	});
		});
	});
	
	$("#process_order").change(function(){
		var ProcessOrder = $(this).val();
		$.ajax({
    		type: "POST",
    		url: "updateChecklistProcessOrder.php",
    		data:'ProcessOrder='+ ProcessOrder,
    		success: function(data){
    			location.reload();
    		}
    	});
	});
	
	$("#display-checklist-userportal").change(function(){
		var getVal = $(this).val();
		$.ajax({
    		type: "POST",
    		url: "displayChecklistOnUserPortal.php",
    		data:'getVal='+ getVal,
    		success: function(data){
    			//location.reload();
    		}
    	});
	});
	$("#display-active").change(function(){
		var getVal = $(this).val();
		var url = window.location.href;
    		var qs = "?active=" + encodeURIComponent(getVal);
    		window.location.href = 'checklist.php' + qs;
	});


});
</script>
