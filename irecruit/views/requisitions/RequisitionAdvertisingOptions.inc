<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<div class="row">
        <div class="panel-body">
        		<div class="table-responsive">
        			<table class="formtable table table-striped table-bordered table-hover" border="0" cellspacing="3" cellpadding="5" width="100%">
        				<tr>
        					<td valign="top" align="right"><a
        						href="<?php echo IRECRUIT_HOME;?>requisitions/requisitionForm.php?menu=8">
        							<b style="font-size: 8pt; COLOR: #000000">Go Back</b><img
        							src="<?php echo IRECRUIT_HOME;?>images/icons/arrow_undo.png"
        							title="Back" style="margin: 0px 0px -4px 3px;" border="0">
        					</a></td>
        				</tr>
        
        				<tr>
        					<td><b>Edit Question</b></td>
        				</tr>
        				
        				<?php 
        				if(isset($_GET['msg']) && $_GET['msg'] == 'updsuc') {
        				    ?>
        				    <tr>
            					<td style="color:blue;">Updated Successfully.</td>
            				</tr>
        				    <?php
        				}
        				?>
        
        				<tr>
        					<td>
        					<?php
                                echo '<form method="POST" name="frmAdvertisingOptions" id="frmAdvertisingOptions">';
                                
                                echo '<div style="float:left;width:230px;padding:3px;margin-bottom:10px;background-color:#eeeeee;">';
                                echo 'Job Posting Site';
                                echo '</div>';
                                echo '<div style="float:left;width:100px;padding:3px;margin-bottom:10px;background-color:#eeeeee;">';
                                echo 'Price';
                                echo '</div>';
                                echo '<div style="float:left;width:230px;padding:3px;margin-bottom:10px;background-color:#eeeeee;">';
                                echo 'Posting Period';
                                echo '</div>';
                                echo '<div style="clear:both;"></div>';
                                
                                $advertising_option_results = $RequisitionQuestionsObj->getRequisitionQuestionInfo("*", $OrgID, 'AdvertisingOptions', $_REQUEST['form']);
                                $adv_options = json_decode($advertising_option_results['value'], true);
                                
                                $i = 1;
                                if (is_array($adv_options)) {
                                    
                                    for ($r = 0; $r < count($adv_options); $r ++) {
                                        
                                        echo '<div style="float:left;width:230px;padding:3px;">';
                                        echo '<input type="text" name="Site' . $i . '" value="' . $adv_options[$r]['Site'] . '" size="30" maxlength="80">';
                                        echo '</div>';
                                        echo '<div style="float:left;width:100px;padding:3px;">';
                                        echo '<input type="text" name="Price' . $i . '" value="' . $adv_options[$r]['Price'] . '" size="10" maxlength="30">';
                                        echo '</div>';
                                        echo '<div style="float:left;width:230px;padding:3px;">';
                                        echo '<input type="text" name="Period' . $i . '" value="' . $adv_options[$r]['Period'] . '" size="30" maxlength="80">';
                                        echo '</div>';
                                        echo '<div style="clear:both;"></div>';
                                        
                                        $i ++;
                                    } // end foreach
                                }
                                echo '<div style="float:left;width:230px;padding:3px;">';
                                echo '<input type="text" name="Site' . $i . '" value="' . $RCC['Site'] . '" size="30" maxlength="80">';
                                echo '</div>';
                                echo '<div style="float:left;width:100px;padding:3px;">';
                                echo '<input type="text" name="Price' . $i . '" value="' . $RCC['Price'] . '" size="10" maxlength="30">';
                                echo '</div>';
                                echo '<div style="float:left;width:230px;padding:3px;">';
                                echo '<input type="text" name="Period' . $i . '" value="' . $RCC['Period'] . '" size="30" maxlength="80">';
                                echo '</div>';
                                echo '<div style="clear:both;"></div>';
                                
                                echo '<div style="width:560px;padding:3px;text-align:center;margin-top:10px;">';
                                echo '<input type="hidden" name="process" value="Y">';
                                echo '<input type="hidden" name="action" value="advertisingoptions">';
                                echo '<input type="submit" value="Update Requisition Advertising Options" class="btn btn-primary">';
                                echo '</div>';
                                
                                echo '</form>';
                                
                                echo '<hr size=1>';
                                echo '<p><b>Preview</b></p>';
                                
                                include IRECRUIT_DIR . 'request/AdvertisingOptions.inc';
                                echo $advertising_options;
                                ?>
        					</td>
        				</tr>
        
        			</table>
        		</div>
        </div>
	</div>
</div>