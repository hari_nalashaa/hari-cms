<?php
echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered table-hover">';
echo '<form method="post" action="' . $_SERVER ['PHP_SELF'] . '">';
echo '<tr><td class="title" valign="top" height="40"><strong>';
echo $title;
echo '</strong></td><td valign="top" align="right" width="20%">';

if ($action) {
	if (($feature ['RequisitionApproval'] == "Y") && ($permit ['Requisitions_Edit'] == 1)) {
		echo '<a href="' . IRECRUIT_HOME . 'request/needsApproval.php"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
	} elseif (($feature ['RequisitionApproval'] == "Y") && ($permit ['Requisitions_Edit'] == 0)) {
	} else { // else feature
		echo '<a href="requisitions.php?Active=' . $Active . '"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
	} // end else feature
} else {
	if (! $Active) {
		$Active = 'Y';
	}
	?>
<select name="Active" onChange="submit()" class="form-control">
	<option value="Y" <?php if ($Active== "Y") { echo ' selected'; } ?>>Active Requisitions</option>
	<option value="N" <?php if ($Active== "N") { echo ' selected'; } ?>>Non-Active Requisitions</option>
</select>
<input type="hidden" name="action" value="">

<?php
	if ($refinereq) {
		echo '<input type="hidden" name="refinereq" value="' . $refinereq . '">';
	} // end refinereq
}

echo '</td></tr>';
echo '</form>';
echo '</table>';

include COMMON_DIR . 'process/FormFunctions.inc';

if ($feature ['RequisitionRequest'] == "Y") {
	$link = IRECRUIT_HOME . 'publicRequest.php?OrgID=' . $OrgID;
	$rqstlk = ' onClick="window.open(\'' . $link . '\',\'\',\'width=800,height=600,resizable=yes,scrollbars=yes\'); return false;"';
	echo 'Public link: <a href="' . $link . '" target="_blank"' . $rqstlk . '>' . $link . '</a><br>';
	include IRECRUIT_DIR . 'request/Request.inc';
} else {
	include IRECRUIT_DIR . 'requisitions/ProcessRequisition.inc';
} ?>
