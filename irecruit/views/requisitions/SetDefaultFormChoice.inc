<div id="page-wrapper">
    <?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
    <div class="row">
        <div class="panel-body">
        		<div class="table-responsive">
        			<table class="formtable table table-striped table-bordered table-hover" border="0" cellspacing="3" cellpadding="5" width="100%">
        				<tr>
        					<td valign="top" align="right">
        					   <a href="<?php echo IRECRUIT_HOME;?>requisitions/requisitionForm.php?menu=8">
        							<b style="font-size: 8pt; COLOR: #000000">Go Back</b><img
        							src="<?php echo IRECRUIT_HOME;?>images/icons/arrow_undo.png"
        							title="Back" style="margin: 0px 0px -4px 3px;" border="0">
        					   </a>
        					</td>
        				</tr>
        
        				<tr>
        					<td><b>Edit Question</b></td>
        				</tr>
        
                        <?php 
            			if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
            				echo "<tr><td style='color:blue'>Successfully Updated</td></tr>";
            			}
                		?>
        				
        				<tr>
        					<td>
                   				<form name="frmDefaultFormChoice" id="frmDefaultFormChoice" method="post">
                					<?php 
                					if($_REQUEST['questionid'] == 'InternalFormID') {
                						echo 'Select Internal Form Default: ';
                					}
                					else if($_REQUEST['questionid'] == 'FormID') {
                						echo 'Select Form Default: ';
                					}
                					?>
                					<select name="FormChoice" id="FormChoice" class="form-control width-auto-inline">
                					<option value="">Select</option>
                					<?php
                					if (is_array ( $results_forms ['results'] )) {
                						foreach ( $results_forms ['results'] as $row ) {
                							$sel = ($req_que_info ['defaultValue'] == $row ['FormID']) ? " selected" : "";
                							echo '<option value="' . $row ['FormID'] . '"' . $sel . '>' . $row ['FormID'] . '</option>';
                						}
                					}
                					?>
                					</select>
                					
                					<input type="submit" class="btn btn-primary" name="btnSubmitDefaultForm" id="btnSubmitDefaultForm" value="Submit">
                				</form>
        					</td>
        				</tr>
        			</table>
        		</div>
        </div>
    </div>
</div>        