<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<div class="row">
		<?php 
			if(isset($_REQUEST['questionid']) && $_REQUEST['questionid'] != "") {

				$QuestionID = $_REQUEST['questionid'];
				$FormID 	= $_REQUEST['form'];
				$action 	= $_REQUEST['action'];
				
				include_once IRECRUIT_VIEWS . 'requisitions/EditRequisitionQuestion.inc';
			}
			else {
				include_once IRECRUIT_VIEWS . 'requisitions/RequisitionFormQuestions.inc';
			}
		?>
	</div>
</div>