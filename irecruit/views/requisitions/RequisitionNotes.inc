<?php
$close_link = IRECRUIT_HOME . 'requisitions.php';
$close_link .= '?RequestID=' . $RequestID;
$close_link .= '&action=' . $action;
$close_link .= '&Active=' . $Active;
if ($_REQUEST['AccessCode'] != "") {
	$close_link .= '&k=' . $_REQUEST['AccessCode'];
}

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results_stages = $RequisitionsObj->getRequisitionStageInfo("RequisitionStage", "Code, Description", $where, "Description", array($params));

$req_stages = array();

if($results_stages['count'] == 0) {
    $RequisitionsObj->insDefaultRequisitionStageInfo($OrgID);
    $results_stages = $RequisitionsObj->getRequisitionStageInfo("RequisitionStage", "Code, Description", $where, "Description", array($params));
}

if(is_array($results_stages['results'])) {
    foreach($results_stages['results'] as $row_stage) {
        $stage_code                 =   $row_stage ['Code'];
        $stage_description          =   $row_stage ['Description'];
        $req_stages[$stage_code]    =   $stage_description;
    }
}

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results_stage_reasons = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));
$req_stage_reasons = array();

if($results_stage_reasons['count'] == 0) {
    $RequisitionsObj->insDefaultRequisitionStageReasonsInfo($OrgID);
    $results_stage_reasons = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));
}
if(is_array($results_stage_reasons['results'])) {
    foreach($results_stage_reasons['results'] as $row_stage_reason) {
        $stage_reason_code                      =   $row_stage_reason ['Code'];
        $stage_reason_description               =   $row_stage_reason ['Description'];
        $req_stage_reasons[$stage_reason_code]  =   $stage_reason_description;
    }
}

if($ServerInformationObj->getRequestSource() != 'ajax') {
	
echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
END;
	
}

if ($_GET ['updateid'] != "") {
	//Set where condition to delete RequisitionNotes
	$where = array("OrgID = :OrgID", "RequestID = :RequestID", "UpdateID = :UpdateID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":UpdateID"=>$_GET ['updateid']);
	//Delete RequisitionNotes
	$RequisitionsObj->delRequisitionsInfo('RequisitionNotes', $where, array($params));
} // end GET updateid

if ($_REQUEST['process'] == 'Y') {

        $RequisitionStageDate   = $_REQUEST['RequisitionStageDate'];
        if($RequisitionStageDate == "") {
            $current_date_time = $MysqlHelperObj->getDateTime();
            $RequisitionStageDate = $current_date_time;
        }
        else {
            $RequisitionStageDate = $_REQUEST['RequisitionStageDate'];
        }
	    
	    if($_REQUEST['RequisitionStage'] != "") {
	        //Update Requisition Stage and Requisition Stage Reason
	        $set_info       =   array("RequisitionStage = :RequisitionStage", "RequisitionStageReason = :RequisitionStageReason", "RequisitionStageDate = :RequisitionStageDate");
	        $where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
	        $params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":RequisitionStage"=>$_REQUEST['RequisitionStage'], ":RequisitionStageReason"=>$_REQUEST['RequisitionStageReason'], ":RequisitionStageDate"=>$DateHelperObj->getYmdFromMdy($RequisitionStageDate));
	        
	        $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
	    }
	    
	    if($_REQUEST['RequisitionStage'] != "") {
	        $RequisitionNotes  = "Requisition Stage changed to: " . $req_stages[$_REQUEST['RequisitionStage']]. ", ". $RequisitionStageDate . "<br>";
	        if($req_stage_reasons[$_REQUEST['RequisitionStageReason']] != "") {
	            $RequisitionNotes .= "Reason for Change: " . $req_stage_reasons[$_REQUEST['RequisitionStageReason']] . "<br>";
	        }
	        $RequisitionNotes .= $_REQUEST['Comments'];
	    }
	    else {
	        $RequisitionNotes = $_REQUEST['Comments'];
	    }
	    
		//Insert RequisitionNotes information
		$req_notes_info = array("OrgID"=>$OrgID, "RequestID"=>$RequestID, "Date"=>"NOW()", "UserID"=>$USERID, "Comments"=>$RequisitionNotes);
		$RequisitionsObj->insRequisitionsInfo('RequisitionNotes', $req_notes_info);

		if($_REQUEST['RequisitionStage'] != "") {
		  $message = 'Stage has been updated';
		}
		else {
          $message = 'Note has been updated';
		}
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "');\n";
		echo '</script>';
	
} // end process

//Set columns
$columns = array("RequisitionID", "JobID", "Title");
//Set where condition to get the requisitions
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get Requisitions Information
$results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", "", array($params));
$REQ = $results['results'][0];

echo '<form name="frmRequisitionNotes" id="frmRequisitionNotes" method="post" action="requisitionNotes.php">';
echo '<table border="0" cellspacing="0" cellpadding="0">';

echo '<tr><td align="left" colspan="3"><br></td></tr>';

echo '<tr><td colspan="3">';

echo 'Stage:&nbsp;<select name="ddlRequisitionStage" id="ddlRequisitionStage" class="form-control width-auto-inline">';



echo '<option value="">Select</option>';

foreach($req_stages as $stage_code=>$stage_description) {
    echo '<option value="' . $stage_code . '">' . $stage_description . '</option>';
}

echo '</select>';


echo '&nbsp;Reason for Change:<select name="ddlRequisitionStageReason" id="ddlRequisitionStageReason" class="form-control width-auto-inline">';

echo '<option value="">Select</option>';

foreach($req_stage_reasons as $stage_reason_code=>$stage_reason_description) {
    echo '<option value="' . $stage_reason_code . '">' . $stage_reason_description . '</option>';
}

echo '</select>';

echo '&nbsp;';
echo 'Date:<input type="text" name="txtRequisitionStageDate" id="txtRequisitionStageDate" class="form-control width-auto-inline">';
echo '</td></tr>';

if($ServerInformationObj->getRequestSource() != 'ajax') {
    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
	echo '<tr><td height="40" valign="top" colspan="3">';
	if ($feature ['MultiOrg'] == "Y") {
		echo "Organization: <b>" . $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . "</b>";
		echo "<br>";
	}
	echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo "<br>";
	echo "Title: <b>" . $REQ ['Title'] . "</b><br><br>";
	echo '</td></tr>';
}

echo '<tr><td align="left" colspan="3">';
echo '<br><strong>Add notes to your requisition.</strong>';
echo '</td></tr>';

echo '<tr><td colspan="3">';
echo '<br><textarea name="Comments" id="RequisitionStageComments" rows="3" cols="60" wrap="virtual"></textarea>';
echo '</td></tr>';

echo '<tr><td height="40" valign="bottom" colspan="3">';
echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="action" value="' . $action . '">';
echo '<input type="hidden" name="Active" value="' . $Active . '">';


if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<input type="submit" value="Update Stage">';
}
else if($ServerInformationObj->getRequestSource() == 'ajax') {
	echo '<input type="button" class="btn btn-primary" name="btnAddRequisitionNotes" id="btnAddRequisitionNotes" value="Update Stage" onclick="addRequisitionNotes(this)">';
}

echo '</td></tr>';

echo '</table>';
echo '</form>';

// report here

//set the columns
$columns = "UserID, Comments, date_format(Date,'%Y-%m-%d %H:%i EST') Date, UpdateID";
//set where condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get Requisitions Information
$results = $RequisitionsObj->getRequisitionNotesInfo($columns, $where, "RequisitionNotes.Date DESC", array($params));

if ($results['count'] > 0) {

	echo '<br><table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
	echo '<tr>';
	echo '<td width="90"><b>Updated Date</b></td>';
	echo '<td width="80" align="center"><b>UserID</b></td>';
	echo '<td><b>Notes</b></td>';
	if (($permit ['Admin'] == 1) && ($permit ['Requisitions_Edit'] == 1)) {
		echo '<td align="center"><b>Delete</b></td>';
	}
	echo '</tr>';
} // end if cnt

$rowcolor = "#eeeeee";

if(is_array($results['results'])) {
	foreach($results['results'] as $RH) {

		echo '<tr style="background-color:' . $rowcolor . ';border-color:#ffffff">';
		echo '<td valign="top">' . $RH ['Date'] . '</td>';
		echo '<td valign="top" align="center">' . $RH ['UserID'] . '</td>';
		echo '<td valign="top">' . $RH ['Comments'] . '</td>';
		if (($permit ['Admin'] == 1) && ($permit ['Requisitions_Edit'] == 1)) {
			echo '<td align="center">';
			echo '<a href=\'javascript:void(0);\' onclick=\'deleteRequisitionNotes("'.$RH ['UpdateID'].'")\'>';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete">';
			echo '</a>';
			echo '</td>';
		}
		echo '</tr>';

		$i ++;

		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}


if ($results['count'] > 0) {
	echo '</table>';
}

if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<br><br>';
	echo '<p align="center"><a href="#" onclick="CloseAndRefresh(\'' . $close_link . '\')" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
}
?>
<script type="text/javascript">date_picker('#txtRequisitionStageDate', 'mm/dd/yy');</script>