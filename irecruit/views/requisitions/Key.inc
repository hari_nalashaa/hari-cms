<table border="0" cellspacing="5" cellpadding="2"
	class="table table-bordered"
	align="center" width="100%">
	<tr>
		<td bgcolor="#D7d7d7" colspan="3"><b>Key</b></td>
	</tr>

	<!-- Line One -->
	<tr>
		<td colspan="3" align="center">
			<b style="font-size: 8pt; color: #AAAAAA">Add Functions</b>
		</td>
	</tr>
<?php
if (($feature ['WebForms'] == "Y") || ($feature ['AgreementForms'] == "Y") || ($feature ['SpecificationForms'] == "Y") || ($feature ['PreFilledForms'] == "Y")) {
	if ($permit ['Internal_Forms'] == 1) {
		?>
		<tr>
			<td>Assign Forms</td>
			<td><img border="0"
				src="<?php echo IRECRUIT_HOME?>images/icons/application_edit.png" width="16"
				height="16" title="Assign Forms" style="margin: 0px 3px -4px 0px;"></td>
			<td></td>
		</tr>
		<?php
	} // end permit InternalForms
} // end feature Forms

if ($permit ['Requisitions_Edit'] == 1) {
	
	//Set where condition
	$where     =   array("OrgID = :OrgID", "Rejection != ''");
	//Get PrescreenText Information
	$resultsPT =   G::Obj('PrescreenQuestions')->getPrescreenTextInfo($OrgID, $where, "OrgID LIMIT 1");
	
	if (isset($resultsPT['OrgID']) && $resultsPT['OrgID'] != "") {
		?>
		<tr>
			<td>PreScreening Questions</td>
			<td><img border="0" src="<?php echo IRECRUIT_HOME?>images/icons/reqqon.png"
				width="16" height="16" title="PreScreening Questions"
				style="margin: 0px 3px -4px 0px;"></td>
			<td><img border="0" src="<?php echo IRECRUIT_HOME?>images/icons/reqqoff.png"
				width="16" height="16" title="PreScreening Questions"
				style="margin: 0px 3px -4px 0px;"></td>
		</tr>
		<?php
	} // end ORCCPText
	
	//Get auto forward list information
	$results_afl = $FormFeaturesObj->getAutoForwardList($OrgID);
	
	if($results_afl['count'] > 0) {
		
		?>
		<tr>
			<td>Auto Forward</td>
			<td><img border="0"
				src="<?php echo IRECRUIT_HOME?>images/icons/user_green.png" width="16"
				height="16" title="Auto Forward" style="margin: 0px 3px -4px 0px;"></td>
			<td><img border="0" src="<?php echo IRECRUIT_HOME?>images/icons/user_red.png"
				width="16" height="16" title="Auto Forward"
				style="margin: 0px 3px -4px 0px;"></td>
		</tr>
		<?php
	} // end Auto Forward
} // end permit Edit

if ($permit ['Requisitions_Costs'] == 1) {
	?>
	<tr>
		<td>Costs</td>
		<td><img border="0"
			src="<?php echo IRECRUIT_HOME?>images/icons/money_dollar.png" width="16"
			height="16" title="Cost" style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>
<?php
} // end permit costs
?>
	<tr>
		<td>Notes</td>
		<td><img border="0"
			src="<?php echo IRECRUIT_HOME?>images/icons/page_edit.png" width="16"
			height="16" title="Cost" style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>



	<!-- Line Two -->
	<tr>
		<td colspan="3" align="center"><hr>
			<b style="font-size: 8pt; color: #AAAAAA">Share</b></td>
	</tr>
	<tr>
		<td>Publish Requisition</td>
		<td><img border="0"
			src="<?php echo IRECRUIT_HOME?>images/icons/world_add.png" width="16"
			height="16" title="Export XML" style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>

	<tr>
		<td>Purchase Premium Feed</td>
		<td><img border="0"
			src="<?php echo IRECRUIT_HOME?>images/icons/basket_put.png" width="16"
			height="16" title="URL Links" style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>

<?php
// Get Social Media Information
$SM		=   G::Obj('SocialMedia')->getSocialMediaDetailInfo($OrgID, $MultiOrgID);
$allow	=	array ("I", "B");

if (in_array ( $SM ['Placement'], $allow )) {
	
	if ($SM ['Facebook'] == "Y") {
		
		?>
	<tr>
		<td>Share on Facebook</td>
		<td><img border="0" src="<?php echo PUBLIC_HOME?>images/f_logo.png"
			width="16" height="16" title="Share on Facebook"
			style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>

<?php
	} // end Facebook
	
	if ($SM ['Twitter'] == "Y") {
		
		?>

	<tr>
		<td>Share on Twitter</td>
		<td><img border="0" src="<?php echo PUBLIC_HOME?>images/twitter.png"
			width="16" height="16" title="Share on Twitter"
			style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>

<?php
	} // end if Twitter
	
	if ($SM ['LinkedIn'] == "Y") {
		?>

	<tr>
		<td>Share on LinkedIn</td>
		<td><img border="0" src="<?php echo PUBLIC_HOME?>images/linkedin.png"
			width="16" height="16" title="Share on LinkedIn"
			style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>

<?php
	} // end if LinkedIn
} // end if Social Media allowed

?>

<!-- Line Three -->
	<tr>
		<td colspan="3" align="center"><hr>
			<b style="font-size: 8pt; color: #AAAAAA">View</b></td>
	</tr>

	<tr>
		<td>View Requisition</td>
		<td><img border="0"
			src="<?php echo IRECRUIT_HOME?>images/icons/page_white_magnify.png"
			width="16" height="16" title="View Requisition"
			style="margin: 0px 3px -4px 0px;"></td>
		<td></td>
	</tr>
</table>
<br>

<?php 
if(G::Obj('ServerInformation')->getRequestSource() != 'ajax') {
	?>
	<p align="center">
		<a href="#" onclick="window.close()" style="text-decoration: none;"><img
			src="<?php echo IRECRUIT_HOME?>images/icons/photo_delete.png"
			title="Close Window" border="0" style="margin: 0px 3px -4px 0px;"><b
			style="FONT-SIZE: 8pt; COLOR: #000000">Close Window</b></a>
	</p>
	<?php
}
?>