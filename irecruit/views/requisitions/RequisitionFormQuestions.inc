<form name="frmRequisitionForm" id="frmRequisitionForm" method="post">
	<table class="table table-bordered" id="sort_requisition_questions">
		<tr class="exclude_this_row">
			<td colspan="100%">
			<strong>Note:*</strong><br>
			N/A: It represents these questions are not available in request process.<br>
			Yes: It represents these questions are mandatory. Users don't have permission to turn off.
			</td>
		</tr>
		<?php 
		$req_def_form_info = $RequisitionFormsObj->getDefaultRequisitionFormInfo($OrgID);
		?>
		<tr class="exclude_this_row">
    		<td colspan="7">
    		    <div class="row">
        		    <div class="col-lg-4 col-md-4 col-sm-4" style="font-size:18px">
        		      Current Version: <?php echo $req_def_form_info['RequisitionFormName'];?>
        		    </div>
            		<div class="col-lg-8 col-md-8 col-sm-8" style="text-align:right">
                    <?php 
                    if (preg_match ( '/requisitionForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
                        ?>
                        &nbsp; Do you want to make new copy of this form. If yes you cannot edit the old copy.
                        <input type="hidden" name="rdCopyForm" id="rdCopyForm">&nbsp;
                        <input type="button" name="btnCopyForm" id="btnCopyForm" value="Copy Form" class="btn btn-primary" onclick="copyRequisitionForm()">
                        <?php
                    }
                    ?>
        		    </div>
    		    </div>
    		</td>
	    </tr>
	    
	    <?php 
	    if(isset($_GET['msg']) && $_GET['msg'] == 'updsuc') {
	    	?>
	    	<tr class="exclude_this_row">
        		<td style="color: blue" colspan="7">
        		  Successfully Updated.
        		</td>
    	    </tr>		
	    	<?php
	    }
	    ?>
	    		
		<tr class="exclude_this_row">
			<td>Question</td>
			<td>Question Type</td>
			<td>Edit Details</td>
			<td>
				Requisition<br>
				<input type="checkbox" name="request_check_uncheck" onclick="check_uncheck(this.checked, 'chk_requisition')">
			</td>
			<?php
			if($feature['RequisitionRequest'] == "Y") 
			{
				?><td>
					Request<br>
					<input type="checkbox" name="request_check_uncheck" onclick="check_uncheck(this.checked, 'chk_request')">
				  </td><?php
			}
			?>
			<td>
				Required<br>
				<input type="checkbox" name="required_check_uncheck" onclick="check_uncheck(this.checked, 'chk_required')">
			</td>
			<td>Action</td>
		</tr>
			
		<?php
		$req_ques_count = count ( $requisition_questions );
		for($ri = 0; $ri < $req_ques_count; $ri ++) {
			$QuestionID              =   $requisition_questions [$ri] ['QuestionID'];
			$Required                =   $requisition_questions [$ri] ['Required'];
			$Edit                    =   $requisition_questions [$ri] ['Edit'];
			$QueOrder                =   $requisition_questions [$ri] ['QuestionOrder'];
			$QueTypeID               =   $requisition_questions [$ri] ['QuestionTypeID'];
			$Question                =   $requisition_questions [$ri] ['Question'];
			$RequestQueStatus        =   $requisition_questions [$ri] ['Request'];
			$RequisitionQueStatus    =   $requisition_questions [$ri] ['Requisition'];
			
			if ($feature ['MonsterAccount'] == "N") {
				if ($QuestionID	== 'MonsterJobCategory' 
					|| $QuestionID == 'MonsterJobOccupation' 
					|| $QuestionID == 'MonsterJobPostedDate'
					|| $QuestionID == 'MonsterJobPostType'
					|| $QuestionID == 'MonsterJobIndustry') {
					$QuestionID = '';
				}
			}
			
			if($QueTypeID != "10" && $QuestionID != "") {
				?>
				<tr id="<?php echo $QuestionID;?>" class="question_id_row">
					<td>
						<?php
						if (substr ( $QuestionID, 0, 4 ) == "CUST") {
							echo "<input type='text' name='" . $QuestionID . "Question' id='" . $QuestionID . "Question' value='$Question' size='40'></td>";
						} else if (($QueTypeID == 99) || ($QueTypeID == 90)) {
							echo '<b>Instruction</b>: ';
							echo substr ( strip_tags ( $Question ), 0, 40 ) . '...';
						} else {
							echo $Question;
						}
						?>
					</td>
					<td>
						<?php
						$QuestionTypeName = $QuestionID . "-questiontypeid";
						if (substr ( $QuestionID, 0, 4 ) == "CUST") {
							echo "<select name='$QuestionTypeName' style='width:120px;'>";
							echo "<option value=''>Select</option>";
							for($qt = 0; $qt < count ( $edit_que_types ); $qt ++) {
								echo "<option value='" . $edit_que_types [$qt] ['QuestionTypeID'] . "'";
								if ($QueTypeID == $edit_que_types [$qt] ['QuestionTypeID'])
									echo ' selected="selected"';
								echo ">" . $edit_que_types [$qt] ['Description'] . "</option>";
							}
							echo "</select>";
						} else {
							echo $question_types_by_id [$requisition_questions [$ri] ['QuestionTypeID']];
						}
						?>
					</td>
					<td>
					<?php
					if ($Edit == "Y") {
						?>
						<a href="requisitionForm.php?action=requisitionquestions&form=<?php echo $requisition_questions[$ri]['RequisitionFormID'];?>&questionid=<?php echo $requisition_questions[$ri]['QuestionID'];?>&typeform=requisitionform">
							<img src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png" title="Edit" border="0">
						</a>
						<?php
					} else if($requisition_questions[$ri]['QuestionID'] == "FormID"
							|| $requisition_questions[$ri]['QuestionID'] == "InternalFormID") {
						?>
						<a href="setDefaultFormChoice.php?questionid=<?php echo $requisition_questions[$ri]['QuestionID'];?>&form=<?php echo $requisition_questions[$ri]['RequisitionFormID'];?>">
							<img src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png" title="Edit" border="0">
						</a>
						<?php
					}  
					else {
						echo "-";
					}
					?>		
					</td>
					<td>
					<?php 
					if(!in_array($QuestionID, $default_requisition_questions)) {
						?>
						<input class="chk_requisition" type="checkbox" name="<?php echo $QuestionID;?>-RequisitionStatus" id="<?php echo $QuestionID;?>-RequisitionStatus" value="Y"
						<?php if($RequisitionQueStatus == "Y") echo 'checked="checked"';?>>
						<?php
					}
					else {
						echo "Yes";
						?><input type="hidden" name="<?php echo $QuestionID;?>-RequisitionStatus" id="<?php echo $QuestionID;?>-RequisitionStatus" value="Y"><?php
					}
					?>
					</td>
					<?php
					if($feature['RequisitionRequest'] == "Y") {
						echo '<td>';
						if(!in_array($QuestionID, $request_not_available_ques) && !in_array($QuestionID, $default_request_questions))
						{
							?>
							<input class="chk_request" type="checkbox" name="<?php echo $QuestionID;?>-RequestStatus" id="<?php echo $QuestionID;?>-RequestStatus" value="Y"
							<?php if($RequestQueStatus == "Y") echo 'checked="checked"';?>>
							<?php
						}
						else if(in_array($QuestionID, $request_not_available_ques)) {
							echo "N/A";
						}
						else if(in_array($QuestionID, $default_request_questions)) {
							echo 'Yes';
							?><input type="hidden" name="<?php echo $QuestionID;?>-RequestStatus" id="<?php echo $QuestionID;?>-RequestStatus" value="Y"><?php
						}
						echo '</td>';
					}
					?>
					<td>
						<input type="hidden" name="<?php echo $QuestionID;?>" value="D">
						<?php
						if(!in_array($QuestionID, $default_required_questions)) {
							?>
							<input class="chk_required" type="checkbox" name="<?php echo $QuestionID;?>-R" id="<?php echo $QuestionID;?>-R" value="Y"
							<?php if($Required == "Y") echo 'checked="checked"';?>>
							<?php
						}
						else {
							echo "Yes";
							?><input type="hidden" name="<?php echo $QuestionID;?>-R" id="<?php echo $QuestionID;?>-R" value="Y"><?php
						}
						?>
					</td>
					<td>
					<?php
					if (substr ( $QuestionID, 0, 4 ) == "CUST") {
						echo "<a href='requisitionForm.php?delete=" . $QuestionID . "&subactiondelete=yes&form=".$requisition_questions[$ri]['RequisitionFormID']."'><img border='0' title='Delete' src='" . IRECRUIT_HOME . "images/icons/cross.png'></a>";
					} else {
						echo "-";
					}
					?>
					</td>
				</tr>
				<?php
			}
		}
		?>
		<tr>
		<td colspan="7" class="exclude_this_row">
			<a href="requisitionForm.php?form=<?php echo $requisition_questions[0]['RequisitionFormID'];?>&action=requisitionquestions&new=add">
				<img src="<?php echo IRECRUIT_HOME;?>images/icons/add.png" title="Add" style="margin: 0px 3px -4px 0px;" border="0"> 
				<b style="font-size: 8pt; COLOR: #000000">Add Custom Question</b>
			</a>
		</td>
		</tr>
		<tr>
			<td colspan="7" align="center" class="exclude_this_row">
				<input value="Change Status" class="btn btn-primary" type="submit" name="btnChangeStatus" id="btnChangeStatus">
			</td>
		</tr>
	</table>

	<input type="hidden" name="process" id="process" value="Y">
</form>
<script>
form_sections = {"QuestionIDs":[]};
$( "#sort_requisition_questions tbody" ).sortable({
	cursor: 'move',
	items: 'tr:not(.exclude_this_row)',
	placeholder: 'ui-state-highlight',
    stop: function( event, ui ) {
        
    	$(this).find('tr.question_id_row').each(function(i) {

        	var SortOrder = i;
            SortOrder++;
            var tr_id = $(this).attr('id');

            form_sections['QuestionIDs'][i]    = tr_id;
        });
        
    	updateSectionsSortOrder(form_sections);
    }
});

function updateSectionsSortOrder(form_sections) {
	$.ajax({
		method: "POST",
  		url: "updateRequisitionQuestionsSortOrder.php?form_id=<?php echo $requisition_questions[0]['RequisitionFormID'];?>",
		type: "POST",
		data: form_sections,
		beforeSend: function() {
			$("#form_section_titles_status_message").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			$("#form_section_titles_status_message").css("text-align","center");
			$("#form_section_titles_status_message").html(data);
		}
	});
}
</script>
