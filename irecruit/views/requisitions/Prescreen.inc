<?php 
$close_link = IRECRUIT_HOME . 'requisitions.php';
$close_link .= '?RequestID=' . $RequestID;
$close_link .= '&action=' . $action;
$close_link .= '&Active=' . $Active;
if ($_REQUEST['AccessCode'] != "") {
	$close_link .= '&k=' . $_REQUEST['AccessCode'];
}

if ($_REQUEST['process'] == "Y") {

	if (($OrgID) && ($RequestID)) {

		//Set where condition
		$where = array("OrgID = :OrgID", "RequestID = :RequestID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		//Delete PrescreenQuestions Data
		$FormsInternalObj->delFormData('PrescreenQuestions', $where, array($params));
           
		$ii = 1;
		while ( $ii <= 20 ) {
				
			$ckq = "q" . $ii;
			$cka = "a" . $ii;
			$cki = "i" . $ii;
				
			if ($_POST [$ckq] != "") {
				
				$question_value =   isset($_POST [$ckq]) ? $_POST [$ckq] : '';
				$answer_value   =   isset($_POST [$cka]) ? $_POST [$cka] : '';
				$internal_value =   isset($_POST [$cki]) ? $_POST [$cki] : '';

				$prescreen_que_info = array(
                                            "OrgID"     =>  $OrgID,
                                            "RequestID" =>  $RequestID,
                                            "Question"  =>  $question_value,
                                            "Answer"    =>  $answer_value,
                                            "SortOrder" =>  $ii,
                                            "Internal"  =>  $internal_value
				                        );
				//Get PrescreenQuestions Information
				G::Obj('PrescreenQuestions')->insPrescreenQuestions($prescreen_que_info);

			} // end post check
			$ii ++;
		} // end while

		$message = "Prescreening questions have been updated.\\n\\nThank You.";
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "');\n";
		echo '</script>';
	} // end org,req,jobid check
} // end submit

echo '<form name="frmPrescreenQuestions" id="frmPrescreenQuestions" method="post" action="prescreen.php">';

if(G::Obj('ServerInformation')->getRequestSource() != 'ajax') {
    $multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
	// Header for Requisition Applied for
	echo '<table border="0" cellspacing="0" cellpadding="3">';
	
	echo '<tr><td>';
	if ($feature ['MultiOrg'] == "Y") {
		echo "Organization: <b>" . G::Obj('OrganizationDetails')->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . "</b>";
		echo "<br>";
	}
	echo "Req/Job ID: <b>" . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo "<br>";
	echo "Title: <b>" . G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	echo '</td></tr>';
	
	echo '</table>';	
}


echo '<br>';

echo '<strong>Add Pre-Screening (knock-out) questions for the requisition here. </strong><br><br>';

echo '<table border="0" cellspacing="0" cellpadding="3">';

$i = 1;

//Set where condition
$where      =   array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get PrescreenQuestions Information
$results    =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation("*", $where, "SortOrder", array($params));

if(is_array($results['results'])) {
	foreach ($results['results'] as $PRESCREEN) {

	    echo '<tr><td align="right">Question ' . htmlspecialchars($i) . ':</td><td><input type="text" name="q' . htmlspecialchars($i) . '" value="' . htmlspecialchars($PRESCREEN ['Question']) . '" size="50" maxlength="500">?';
		echo '&nbsp;&nbsp;';
		echo '<input type="radio" name="a' . htmlspecialchars($i) . '" value="Y"';
		if ($PRESCREEN ['Answer'] == "Y") {
			echo ' checked';
		}
		echo '>&nbsp;Yes';
		echo '&nbsp;&nbsp;';
		echo '<input type="radio" name="a' . htmlspecialchars($i) . '" value="N"';
		if ($PRESCREEN ['Answer'] == "N") {
			echo ' checked';
		}
		echo '>&nbsp;No</td></tr>';
		if ($feature ['InternalRequisitions'] == "Y") {
			if ($PRESCREEN ['Internal'] == "") {
				$PRESCREEN ['Internal'] = "N";
			}
			echo '<tr><td align="right" valign="top" height="30">';
			echo 'Internal:</td><td valign="top">';
			echo '&nbsp;&nbsp;';
			echo '<input type="radio" name="i' . htmlspecialchars($i) . '" value="Y"';
			if ($PRESCREEN ['Internal'] == "Y") {
				echo ' checked';
			}
			echo '>&nbsp;Yes';
			echo '&nbsp;&nbsp;';
			echo '<input type="radio" name="i' . htmlspecialchars($i) . '" value="N"';
			if ($PRESCREEN ['Internal'] == "N") {
				echo ' checked';
			}
			echo '>&nbsp;No';
			echo '&nbsp;&nbsp;';
			echo '<input type="radio" name="i' . htmlspecialchars($i) . '" value="B"';
			if ($PRESCREEN ['Internal'] == "B") {
				echo ' checked';
			}
			echo '>&nbsp;Both';
			echo '</td></tr>';
		}

		$i ++;
	}
}


if ($i == 1) {
	$end = 4;
} else {
	$end = $i + 1;
}

while ( $i <= $end ) {
    echo '<tr><td align="right">Question ' . $i . ':</td><td><input type="text" name="q' . htmlspecialchars($i) . '" value="" size="50" maxlength="500">?';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="a' . htmlspecialchars($i) . '" value="Y" checked>&nbsp;Yes';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="a' . htmlspecialchars($i) . '" value="N">&nbsp;No</td></tr>';
	if ($feature ['InternalRequisitions'] == "Y") {
		echo '<tr><td align="right" valign="top" height="30">';
		echo 'Internal:</td><td valign="top">';
		echo '&nbsp;&nbsp;';
		echo '<input type="radio" name="i' . htmlspecialchars($i) . '" value="Y">&nbsp;Yes';
		echo '&nbsp;&nbsp;';
		echo '<input type="radio" name="i' . htmlspecialchars($i) . '" value="N" checked>&nbsp;No';
		echo '&nbsp;&nbsp;';
		echo '<input type="radio" name="i' . htmlspecialchars($i) . '" value="B">&nbsp;Both';
		echo '</td></tr>';
	}
	$i ++;
}

echo '</table>';

echo '<br>';
echo '<center>';
echo '<input type="hidden" name="RequestID" value="'.$RequestID.'">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="action" value="'.htmlspecialchars($action).'">';
echo '<input type="hidden" name="Active" value="'.htmlspecialchars($Active).'">';

if(G::Obj('ServerInformation')->getRequestSource() != 'ajax') {
	echo '<input type="submit" name="submit" value="Update Prescreening Questions">';
}
else if(G::Obj('ServerInformation')->getRequestSource() == 'ajax') {
	echo '<input type="button" class="btn btn-primary" name="btnUpdatePrescreenQuestions" id="btnUpdatePrescreenQuestions" value="Save">';
}

echo '</center>';

echo '</form>';


if(G::Obj('ServerInformation')->getRequestSource() != 'ajax') {
	echo '<br><br>';
	echo '<p align="center"><a href="#" onclick="CloseAndRefresh(\'' . $close_link . '\')" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';	
}
?>
<script>
$('#btnUpdatePrescreenQuestions').on('click', function(e) {

		var form_obj = $(this).parents('form:first');
		var input_data = $(form_obj).serialize();
		
		var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
		var req_id = document.frmRequisitionDetailInfo.RequestID.value;
		var active = document.frmRequisitionDetailInfo.Active.value;
		
		var request_listing_url = "requisitions/prescreen.php?RequestID="+req_id+"&Active="+active;
		if(access_code != "") {
			request_listing_url += "&k="+access_code;
		}
		
		var request = $.ajax({
			method: "POST",
			url: request_listing_url,
			type: "POST",
			data: input_data,
			beforeSend: function(){
				$("#prescreen-questions").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
			},
			success: function(data) {
				$("#prescreen-questions").html(data);
			}
		});
		
		setAjaxErrorInfo(request, "#prescreen-questions");
		
});
</script>