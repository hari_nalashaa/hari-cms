<?php
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

if (($_REQUEST ['ApplicationID'] != "") || ($_REQUEST ['RequestID'] != "")) {
	$close_link    =   IRECRUIT_HOME . "formsInternal/assignInternal.php";
	$close_link   .=   "?ApplicationID=" . $_REQUEST ['ApplicationID'];
	$close_link   .=   "&RequestID=" . $_REQUEST ['RequestID'];
} else {
	$close_link = IRECRUIT_HOME . "formsInternal.php";
	$close_link   .=   "?typeform=webforms";
}

if(!isset($_REQUEST['display_app_header'])) {
	echo "<style>";
	echo ".table-bordered {
			    border: 0px solid #ddd !important;
			}
			.table {
			    margin-bottom: 0px !important;
				margin:4px !important;
			    max-width: 100%;
			    width: 100%;
			}
			.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
			    border: 0px solid #ddd !important;
			}
			.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
			    border-top: 0px solid #ddd;
			    line-height: 1.42857;
			    padding: 0px !important;
			    vertical-align: top;
			}";
	echo "</style>";
}

echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
END;

if(!isset($_REQUEST['display_app_header'])) {
	echo displayHeader ( $_REQUEST ['ApplicationID'], $_REQUEST ['RequestID'] );
	echo '<hr><br>';
}

echo '<div style="padding: 20px 10px 0px 10px;">';

if ($typeform == "specforms") {
	include COMMON_DIR . 'formsInternal/SpecificationFormView.inc';
} elseif ($typeform == "webform") {
	include COMMON_DIR . 'formsInternal/WebFormView.inc';
} elseif ($typeform == "prefilledform") {
	include COMMON_DIR . 'formsInternal/PreFilledFormView.inc';
} elseif ($typeform == "agreementform") {
	include COMMON_DIR . 'formsInternal/AgreementFormView.inc';
}

echo '</div>';
?>