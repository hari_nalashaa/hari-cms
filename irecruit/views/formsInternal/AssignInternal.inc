<?php 
require_once COMMON_DIR 		. 'formsInternal/ApplicantFormStatus.inc';
require_once IRECRUIT_DIR 		. 'applicants/EmailApplicant.inc';
require_once COMMON_DIR 		. 'formsInternal/DisplayApplicantHeader.inc';
require_once IRECRUIT_DIR 		. 'formsInternal/AddInternalForm.inc';
require_once IRECRUIT_DIR 		. 'formsInternal/AddIconnectPackage.inc';
require_once IRECRUIT_DIR 		. 'formsInternal/AddIconnectAssignedPackage.inc';

echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
END;

if ($_GET ['delete'] != "") {

	// set parameters
	$params        =   array (
                			":OrgID"		=> $OrgID,
                			":RequestID" 	=> $_GET ['RequestID'],
                			":UniqueID" 	=> $_GET ['delete']
                	   );
	// Set where condition
	$where_delete  =   array (
                			"OrgID 			= :OrgID",
                			"RequestID 		= :RequestID",
                			"UniqueID 		= :UniqueID",
                			"FormStatus 	= 'Deleted'"
                	   );
	//Delete Forms having status is 'Deleted'
	G::Obj('Forms')->delFormsInfo('InternalForms', $where_delete, array($params));
	
	// set update information
	$set_info  =   array ("FormStatus = 'Deleted'");
	// Set where condition
	$where     =   array (
            			"OrgID 			= :OrgID",
            			"RequestID 		= :RequestID",
            			"UniqueID 		= :UniqueID" 
                    );
	// Update InternalForms
	G::Obj('Forms')->updFormsInfo ( 'InternalForms', $set_info, $where, array ($params) );
	
}


if (($RequestID != "") && ($OrgID != "")) {
	$request_source = G::Obj('ServerInformation')->getRequestSource();
	if($request_source == 'normal') {
		?>
			<div id="openFormWindow" class="openForm">
				<div>
					<p style="text-align:right;">
						<a href="#closeForm" title="Close Window" onclick="getAssignInternalInfo('<?php echo $RequestID;?>');">
							<img src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">
							<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
						</a>
					</p>
						
					<div id="iconnect_change_status" class="iconnect_forms_data"></div>
					<div id="iconnect_view_history" class="iconnect_forms_data"></div>
					<div id="iconnect_complete_agree_form" class="iconnect_forms_data"></div>
					<div id="iconnect_complete_pref_form" class="iconnect_forms_data"></div>
					<div id="iconnect_complete_web_form" class="iconnect_forms_data"></div>
					<div id="iconnect_complete_spec_form" class="iconnect_forms_data"></div>
				</div>
			</div>
		
			<div id="page-wrapper">
				<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
				<!-- row -->
				<div class="row">
					<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
		<?php
	}
	
	if(!isset($_REQUEST['display_app_header'])) {
		echo displayHeader ( $ApplicationID, $RequestID );
		echo '<hr><br>';
	}
	
	echo '<div style="clear:both;"></div>';
	
	$cnt = 0;
	
	if ($feature ['PreFilledForms'] == "Y") {
		if ($ApplicationID != "") {
			
			//set where condition
			$where = array("OrgID = :OrgID");
			//set parameters
			$params = array(":OrgID"=>$OrgID);
			//Get PrefilledForms Information
			$results = G::Obj('FormsInternal')->getPrefilledFormsInfo("*", $where, "", array($params));
			
			$cnt += $results['count'];
		} // end ApplicationID
	} // end feature
	
	if ($feature ['WebForms'] == "Y") {
		
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//Get WebForms Information
		$results = G::Obj('FormsInternal')->getWebFormsInfo("*", $where, "", array($params));
		
		$cnt += $results['count'];
	} // end feature
	
	if ($feature ['AgreementForms'] == "Y") {
		
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//get agreement forms information
		$results = G::Obj('FormsInternal')->getAgreementFormsInfo("*", $where, "", array($params));
		$cnt += $results['count'];
	} // end feature
	
	if ($feature ['SpecificationForms'] == "Y") {
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//get specification forms information
		$results = G::Obj('FormsInternal')->getSpecificationForms("*", $where, "", array($params));

		$cnt += $results['count'];
	} // end feature
	     
	if ($cnt > 0) {
               
		addInternalForm ();
                if ($ApplicationID) {
                  addiconnectassignedpackage();
                }else{
                addiconnectpackage();
               }
		include IRECRUIT_DIR . 'formsInternal/IconnectFormsReminderSettings.inc';
	}
	         
 

	if (! $ApplicationID) {
		displayAssignedForms ( $OrgID, $ApplicationID, $RequestID );
	}
	
	    //displayIconnectpackages($OrgID, $ApplicationID, $RequestID );

	if ($ApplicationID) {
		$statuspresent = displayApplicantFormStatus ( $OrgID, $ApplicationID, $RequestID );
	
		//set where condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "Status = '3'");
		//set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
		//get Internal Forms Assigned
		$results = G::Obj('FormsInternal')->getInternalFormsAssignedInfo("*", $where, "", "", array($params));
	
		$completecnt = $results['count'];
	
		if (($statuspresent) && ($completecnt > 0)) {
			echo '<div style="text-align:center;margin-top:20px;">';
			echo '<form method="POST" name="frmFinalizeAllCompletedForms'.$ApplicationID.$RequestID.'" id="frmFinalizeAllCompletedForms'.$ApplicationID.$RequestID.'" action="getAssignInternal.php">';
			echo '<input type="button" value="Finalize All Completed Forms" name="btnFinalizeAllComForms" id="btnFinalizeAllComForms" onclick="finalizeAllCompletedForms(this)">';
			echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
			echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
			echo '<input type="hidden" name="process" value="F">';
			echo '</form>';
			echo '</div>';
		}
	
		if (! $statuspresent) {
			echo '<div style="text-align:center;margin-top:40px;margin-bottom:50px;font-size:10pt;">There are no forms assigned for this applicant.</div>';
		}
	} // end ApplicationID
	
	if($request_source == 'normal') {
		?>
							</div>
						</div>
				</div>
			</div>
		</div>		
		<?php
	}
} // end if RequestID
?>