<?php
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

if (($ApplicationID != "") || ($RequestID != "")) {
	$close_link = IRECRUIT_HOME . "formsInternal/assignInternal.php";
	$close_link .= "?ApplicationID=" . $ApplicationID;
	$close_link .= "&RequestID=" . $RequestID;
} else {
	$close_link = IRECRUIT_HOME . "formsInternal.php?typeform=prefilledforms";
}

echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
<style>
.form-group label {
    width: 25%;
}
</style>
END;

if($ServerInformationObj->getRequestSource() != 'ajax') {
    echo '<style>
            .row {
                margin-right: 0px !important;
                margin-left: 0px !important;
            }
          </style>';
}

if(!isset($_REQUEST['display_app_header'])) {
	echo displayHeader ( $ApplicationID, $RequestID );
	echo '<hr><br>';
	
	echo '<div class="form-group row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12" style="text-align:right">';
	   echo '<a href="javascript:void(0);" onclick="CloseAndRefresh(\'' . $close_link . '\')" style="text-decoration:none;">';
	   echo '<img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0">';
	   echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>';
	   echo '</a>';
	echo '</div>';
	echo '</div>';
	
}

include COMMON_DIR . 'formsInternal/CompletePreFilledForm.inc';
?>