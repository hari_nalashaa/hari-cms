<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					   <?php
                        if (($feature ['WebForms'] != "Y") && ($feature ['AgreementForms'] != "Y") && ($feature ['SpecificationForms'] != "Y") && ($feature ['PreFilledForms'] != "Y")) {
                        	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
                        } // end feature
                        if ($permit ['Internal_Forms'] < 1) {
                        	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
                        }
                        
                        $used = 1;
                        include IRECRUIT_DIR . 'formsInternal/InternalFormStatus.inc';
                        ?>
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script type="text/javascript">
$(function(){
    // Apply to each table individually and make sure nothing is doubleclassed
    // if you run this multiple times.
    $('table').each(function() {
    	$("tr:even").css("background-color", "#eeeeee");
    	$("tr:odd").css("background-color", "#ffffff");
    });
});
</script>