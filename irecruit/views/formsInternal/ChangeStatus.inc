<?php
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

$close_link = IRECRUIT_HOME . 'formsInternal/assignInternal.php?ApplicationID=' . $ApplicationID;
$close_link .= '&RequestID=' . $RequestID;
if ($_REQUEST ['AccessCode'] != "") {
	$close_link .= '&k=' . $_REQUEST ['AccessCode'];
}

echo <<<END
<script language="JavaScript" type="text/javascript">
 function CloseAndRefresh(link)
  {
     opener.window.location = link
     self.close();
  }
</script>
END;

if(!isset($_REQUEST['display_app_header'])) {
	echo displayHeader ( $ApplicationID, $RequestID );
	echo '<hr><br>';
}

if(!isset($_REQUEST['display_app_header'])) {
	echo '<div style="margin-top:-15px;margin-right:80px;float:right;"><a href="#" onclick="CloseAndRefresh(\'' . $close_link . '\')" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></div>';
}

if (($feature ['WebForms'] != "Y") && ($feature ['AgreementForms'] != "Y") && ($feature ['SpecificationForms'] != "Y") && ($feature ['PreFilledForms'] != "Y")) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
} // end feature

$STATUS = array ();

$results = $FormsInternalObj->getInternalFormStatusCodesInfo ( "*", array (), "StatusCode", array () );

if (is_array ( $results ['results'] )) {
	foreach ( $results ['results'] as $IFSC ) {
		$STATUS [$IFSC ['StatusCode']] = $IFSC ['Description'];
	} // end foreach
}

// process
if ($process == "Y") {
    
    // set where condition
    $upd_inter_forms_assigned_where = array (
        "OrgID           = :OrgID",
        "ApplicationID   = :ApplicationID",
        "RequestID       = :RequestID",
        "UniqueID        = :UniqueID"
    );
    // set update information
    $upd_inter_forms_assigned_set_info = array ("LastUpdated = NOW()", "Status = :Status");
    // set parameters
    $upd_inter_forms_assigned_params = array (
        ":Status"        => $StatusCode,
        ":OrgID"         => $OrgID,
        ":ApplicationID" => $ApplicationID,
        ":RequestID"     => $RequestID,
        ":UniqueID"      => $UniqueID
    );
    

    if ($_POST ['FormType'] == "Agreement") {
        // set where condition
        $where = array (
            "OrgID = :OrgID",
            "AgreementFormID = :AgreementFormID",
            "FormStatus != 'Deleted'"
        );
        // set parameters
        $params = array (
            ":OrgID" => $OrgID,
            ":AgreementFormID" => $UniqueID
        );
        // get agreeement forms information
        $results = $FormsInternalObj->getAgreementFormsInfo ( "FormName, FormPart, LinkedTo", $where, "", array (
            $params
        ) );
        
        if (is_array ( $results ['results'] [0] )) {
            list ( $CheckFormName, $CheckFormPart, $CheckLinkedTo ) = array_values ( $results ['results'] [0] );
        }
        
        if($CheckFormPart == "Secondary") {
        
            // set where condition
            $where = array (
                "OrgID         = :OrgID",
                "ApplicationID = :ApplicationID",
                "RequestID     = :RequestID",
                "UniqueID      = :UniqueID"
            );
            // set parameters
            $params = array (
                ":OrgID"           => $OrgID,
                ":ApplicationID"   => $ApplicationID,
                ":RequestID"       => $RequestID,
                ":UniqueID"        => $CheckLinkedTo
            );
            $results = $FormsInternalObj->getInternalFormsAssignedInfo ( "Status", $where, "", "", array (
                $params
            ) );
            
            if($results ['results'] [0] ['Status'] == "0"
                || $results ['results'] [0] ['Status'] == ""
                || !isset($results ['results'] [0] ['Status'])
                || $results ['results'] [0] ['Status'] < "3") {
                $upd_inter_forms_assigned_params[":Status"]  =   "0";
            }
        }
    }
    else if($_POST['FormType'] == "PreFilled") {

	$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);

        if($UniqueID == $CURRENT['I9m']) {
            // set where condition
            $where = array (
                "OrgID         = :OrgID",
                "ApplicationID = :ApplicationID",
                "RequestID     = :RequestID",
                "UniqueID      = :UniqueID"
            );
            // set parameters
            $params = array (
                ":OrgID"           => $OrgID,
                ":ApplicationID"   => $ApplicationID,
                ":RequestID"       => $RequestID,
                ":UniqueID"        => $CURRENT['I9']
            );
            $results = $FormsInternalObj->getInternalFormsAssignedInfo ( "Status", $where, "", "", array (
                $params
            ) );
            
            if($results ['results'] [0] ['Status'] == "0"
               || $results ['results'] [0] ['Status'] == ""
               || !isset($results ['results'] [0] ['Status'])
               || $results ['results'] [0] ['Status'] < "3") {
                $upd_inter_forms_assigned_params[":Status"]  =   "0";
            }
        }
    }
    
	if(isset($StatusCode) && $StatusCode == "1") {
	    $upd_inter_forms_assigned_set_info[]   =   "PresentedTo = :PresentedTo";
	    $upd_inter_forms_assigned_params[":PresentedTo"]   =   "1";
	}
	else if(isset($StatusCode) && $StatusCode == "2") {
	    $upd_inter_forms_assigned_set_info[]   =   "PresentedTo = :PresentedTo";
	    $upd_inter_forms_assigned_params[":PresentedTo"]   =   "2";
	}

	//Update Internal Forms Assigned
	$upd_result = $FormsInternalObj->updInternalFormsAssigned ( $upd_inter_forms_assigned_set_info, $upd_inter_forms_assigned_where, array($upd_inter_forms_assigned_params) );
	// add History
	$Comment = "Changed status to: " . $STATUS [$StatusCode];
	
	// Internal Form History
	$internal_form_history = array (
			"OrgID"          => $OrgID,
			"ApplicationID"  => $ApplicationID,
			"RequestID"      => $RequestID,
			"InternalFormID" => $UniqueID,
			"Date"           => "NOW()",
			"UserID"         => $USERID,
			"Comments"       => $Comment 
	);
	$FormsInternalObj->insInternalFormHistory ( $internal_form_history );
	
	// reverse assignment PreFilled
	if ($_POST ['FormType'] == "PreFilled") {
	
		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);
	
		if (($UniqueID == $CURRENT['I9']) && ($StatusCode == "1")) {
		    	
		    // set where condition
		    $where = array (
		        "OrgID         = :OrgID",
		        "ApplicationID = :ApplicationID",
		        "RequestID     = :RequestID",
		        "UniqueID      = :UniqueID"
		    );
		    // set parameters
		    $params = array (
		        ":OrgID"           => $OrgID,
		        ":ApplicationID"   => $ApplicationID,
		        ":RequestID"       => $RequestID,
		        ":UniqueID"        => $UniqueID
		    );
		    $results = $FormsInternalObj->getInternalFormsAssignedInfo ( "FormName", $where, "", "", array (
		        $params
		    ) );
		    $FormName = $results ['results'] [0] ['FormName'];
		    	
		    // set update information
		    $set_info = array ( "LastUpdated = NOW()", "Status = 0" );
		    // set where condition
		    $where = array (
		        "OrgID         = :OrgID",
		        "ApplicationID = :ApplicationID",
		        "RequestID     = :RequestID",
		        "UniqueID      = :PreFilledFormID"
		    );
		    // set parameters
		    $params = array (
		        ":OrgID"           => $OrgID,
		        ":ApplicationID"   => $ApplicationID,
		        ":RequestID"       => $RequestID,
		        ":PreFilledFormID" => $CURRENT['I9m']
		    );
		    // Update Internal Forms Assigned
		    $FormsInternalObj->updInternalFormsAssigned ( $set_info, $where, array (
		        $params
		    ) );
		    	
		    // add History
		    $Comment = "Changed assignment of: " . $FormName;
		    	
		    $info = array (
		        "OrgID"            => $OrgID,
		        "ApplicationID"    => $ApplicationID,
		        "RequestID"        => $RequestID,
		        "InternalFormID"   => $CURRENT['I9m'],
		        "Date"             => "NOW()",
		        "UserID"           => $USERID,
		        "Comments"         => $Comment
		    );
		    // Insert internal form history
		    $FormsInternalObj->insInternalFormHistory ( $info );
		    
		} // end changeID
		
	} // end if
	
	
	
	  
	// reverse assignment Agreement
	if ($_POST ['FormType'] == "Agreement") {
		
		// set where condition
		$where = array (
				"OrgID = :OrgID",
				"AgreementFormID = :AgreementFormID",
		        "FormStatus != 'Deleted'"
		);
		// set parameters
		$params = array (
				":OrgID" => $OrgID,
				":AgreementFormID" => $UniqueID
		);
		// get agreeement forms information
		$results = $FormsInternalObj->getAgreementFormsInfo ( "FormName, FormPart", $where, "", array (
				$params 
		) );
		
		if (is_array ( $results ['results'] [0] )) {
			list ( $FormName, $FormPart ) = array_values ( $results ['results'] [0] );
		}
		
		// if primary form and change is back to Internal Staff then put all linked forms on hold
		if (($FormPart == "Primary") && ($StatusCode == "2")) {
			
			// set where condition
			$where = array (
					"OrgID     = :OrgID",
					"LinkedTo  = :LinkedTo",
			        "FormStatus != 'Deleted'"
			);
			// set parameters
			$params = array (
					":OrgID"       => $OrgID,
					":LinkedTo"    => $UniqueID
			);
			// get agreeement forms information
			$results = $FormsInternalObj->getAgreementFormsInfo ( "*", $where, "", array (
					$params 
			) );
			
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $IFA ) {
					
					// set information
					$set_info = array ("LastUpdated = NOW()", "Status = 0");
					// set where condition
					$where = array (
							"OrgID = :OrgID",
							"ApplicationID = :ApplicationID",
							"RequestID = :RequestID",
							"UniqueID = :UniqueID" 
					);
					// set parameters
					$params = array (
							":OrgID"         => $OrgID,
							":ApplicationID" => $ApplicationID,
							":RequestID"     => $RequestID,
							":UniqueID"      => $IFA ['AgreementFormID'] 
					);
					// Update Internal Forms Assigned
					$FormsInternalObj->updInternalFormsAssigned ( $set_info, $where, array (
							$params 
					) );
					
					// add History
					$Comment = "Changed assignment of: " . $FormName;
					
					// Internal Form History Information
					$info = array (
							"OrgID"          => $OrgID,
							"ApplicationID"  => $ApplicationID,
							"RequestID"      => $RequestID,
							"InternalFormID" => $IFA ['AgreementFormID'],
							"Date"           => "NOW()",
							"UserID"         => $USERID,
							"Comments"       => $Comment 
					);
					// Insert internal form history
					$FormsInternalObj->insInternalFormHistory ( $info );
				} // end foreach
			}
		} // end FormPart and Status Code
		  
		// if primary form and change is marked completed or finalized than activate all linked forms
		if (($FormPart == "Primary") && ($StatusCode > 2)) {
			
			// set where condition
			$where = array (
					"OrgID = :OrgID",
					"LinkedTo = :LinkedTo",
			        "FormStatus != 'Deleted'"
			);
			// set parameters
			$params = array (
					":OrgID" => $OrgID,
					":LinkedTo" => $UniqueID
			);
			// get agreeement forms information
			$results = $FormsInternalObj->getAgreementFormsInfo ( "*", $where, "", array (
					$params 
			) );
			
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $IFA ) {
					
					// set information
					$set_info = array ("LastUpdated = NOW()", "Status = 1", "DueDate = DATE_ADD(NOW(), INTERVAL 1 day)");
					// set where condition
					$params = array (
							":OrgID"         => $OrgID,
							":ApplicationID" => $ApplicationID,
							":RequestID"     => $RequestID,
							":UniqueID"      => $IFA ['AgreementFormID'] 
					);
					// set parameters
					$where = array (
							"OrgID           = :OrgID",
							"ApplicationID   = :ApplicationID",
							"RequestID       = :RequestID",
							"UniqueID        = :UniqueID" 
					);
					// Update Internal Forms Assigned
					$FormsInternalObj->updInternalFormsAssigned ( $set_info, $where, array ($params) );
					
					// add History
					$Comment = "Changed assignment of: " . $FormName;
					
					// Insert Internal Form History
					$info = array (
							"OrgID"          => $OrgID,
							"ApplicationID"  => $ApplicationID,
							"RequestID"      => $RequestID,
							"InternalFormID" => $IFA ['AgreementFormID'],
							"Date"           => "NOW()",
							"UserID"         => $USERID,
							"Comments"       => $Comment 
					);
					$FormsInternalObj->insInternalFormHistory ( $info );
				} // end foreach
			}
		} // end FormPart and Status Code
	} // end if
	
} // end process

$form_assignee_options = array();
if ($PreFilledFormID) {
	$UniqueID = $PreFilledFormID;
	
	$form_assignee_options = $FormFeaturesObj->getChangeStatusOptionsByFormType($OrgID, 'PreFilledForm', $UniqueID);
} else if ($AgreementFormID) {
    $UniqueID = $AgreementFormID;
	
	$form_assignee_options = $FormFeaturesObj->getChangeStatusOptionsByFormType($OrgID, 'AgreementForm', $UniqueID);
} else if ($WebFormID) {
	$UniqueID = $WebFormID;
	
	$form_assignee_options = $FormFeaturesObj->getChangeStatusOptionsByFormType($OrgID, 'WebForm', $UniqueID);
} else if ($SpecificationFormID) {
	$UniqueID = $SpecificationFormID;
	
	$form_assignee_options = $FormFeaturesObj->getChangeStatusOptionsByFormType($OrgID, 'SpecificationForm', $UniqueID);
}

// set where condition
$where = array (
		"OrgID = :OrgID",
		"ApplicationID = :ApplicationID",
		"RequestID = :RequestID",
		"UniqueID = :UniqueID" 
);
// set parameters
$params = array (
		":OrgID" => $OrgID,
		":ApplicationID" => $ApplicationID,
		":RequestID" => $RequestID,
		":UniqueID" => $UniqueID 
);
// get internal forms assigned information
$results = $FormsInternalObj->getInternalFormsAssignedInfo ( "*", $where, "", "", array (
		$params 
) );
$IFA = $results ['results'] [0];

echo '<div style="width:400px;margin:0px auto;font-size:10pt;">';

echo 'Form: <strong style="font-size:11pt;">';
if ($IFA ['FormType'] != "PreFilled") {
	echo $IFA ['FormType'] . ': ';
}
echo $IFA ['FormName'] . '</strong>';
echo '<br>';

echo 'Current Status: ';
echo '<b>' . $STATUS [$IFA ['Status']] . '</b>';
echo '<br>';

echo 'Last Updated: ';
echo '<b>' . $IFA ['LastUpdated'] . '</b>';
echo '<br>';

// if hiring manager they cannot assign a form to themselves that is originally asigned to Applicant
// if hiring manager they cannot delete a form
if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	if ($IFA ['PresentedTo'] == "1") {
		unset ( $STATUS [2] ); // Assigned with Internal Staff
		unset ( $STATUS [5] ); // Deleted
	}
} // end hiring manager
  
// Forms that assigned to internal staff cannot be assigned to the applicant
if(!in_array("Applicant", $form_assignee_options)) {
    unset ( $STATUS [1] ); // Assigned with Applicant
}

// remove the current status
//unset ( $STATUS [$IFA ['Status']] );

// remove Finalize if less than completed
if ($IFA ['Status'] != 3) {
	unset ( $STATUS [4] ); // Finalized
}

if (sizeof ( $STATUS ) > 0) {
	?>
	<script>
	var page_url = window.location.href;
	
	function processChangeStatus() {
		var form = $('#frmChangeStatus');
		var input_data = form.serialize();
		var form_action = $('#frmChangeStatus').attr( 'action' );

		var request_url = "formsInternal/"
		if(page_url.indexOf("formsInternal/") > 1) request_url = "";
		
		if(confirm('Are you sure you want to change the status of this form?') == true) {
			var request = $.ajax({
				method: "POST",
		  		url: request_url+form_action+'?display_app_header=no',
				type: "POST",
				data: input_data,
				success: function(data) {
					$("#iconnect_change_status").html(data);
		    	}
			});
		}
		
	}
	</script>
	<?php
	echo '<form method="post" name="frmChangeStatus" id="frmChangeStatus" action="changeStatus.php">';
	echo '<br>Change Status To: <select name="StatusCode">';
	foreach ( $STATUS as $code => $desc ) {
		
		echo '<option value="' . $code . '">' . $desc . "</option>";
	}
	echo '</select>';
	echo '&nbsp;&nbsp;&nbsp;';
	echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
	echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
	echo '<input type="hidden" name="UniqueID" value="' . $IFA ['UniqueID'] . '">';
	echo '<input type="hidden" name="FormType" value="' . $IFA ['FormType'] . '">';
	echo '<input type="hidden" name="process" value="Y">';
	if(isset($_REQUEST['display_app_header'])) {
		echo '<input type="hidden" name="display_app_header" value="'.$_REQUEST['display_app_header'].'">';
	}
	echo '<input type="button" value="change" onClick="return processChangeStatus()">';
	echo '</form>';
} else { // else sizeof STATUS
	
	echo '<br><br>You cannot change the status of this form.';
} // end sizeof STATUS

echo '</div>';
?>
