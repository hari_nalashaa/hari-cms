<?php
$display    =   "Y";
$pagewidth  =   "600";

//$test     =   "Y";

// Get year
$year       =   G::Obj('MysqlHelper')->getDateTime ('%Y');

if ($_REQUEST['delete'] != "") {

	// set where condition
	$where = array (
            "OrgID          =   :OrgID",
            "UserID         =   :UserID",
            "LineNo         =   :LineNo"
	);
	// set the parameters
	$params = array (
            ":OrgID"        =>  $OrgID,
            ":UserID"       =>  $USERID,
            ":LineNo"       =>  $_REQUEST['delete']    
	);
	// Delete basket information
	G::Obj('Purchases')->delBasketAndPurchaseInfo ( 'Basket', $where, array ($params) );
	
} // end delete

if ($_REQUEST['process'] == "Y") {
    
    include IRECRUIT_DIR . 'shopping/ValidateBasketInfo.inc';

	if ($ERROR) {
		$MESSAGE = "The following entries are missing information.\\n\\n";
		$MESSAGE .= $ERROR;

		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('$MESSAGE')" . "\n";
		echo '</script>' . "\n\n";
	} else { // else ERROR

	    $intuit_access_info   =   G::Obj('Intuit')->getUpdatedAccessTokenInfo();
        
	    //Failed to get the token
	    if($intuit_access_info['TokenError'] == "Yes" || $intuit_access_info['AccessTokenValue'] == "") {
	        echo '<script language="JavaScript" type="text/javascript">' . "\n";
	        echo "alert('Sorry there is a issue with authorization. Please contact irecruit support team.')" . "\n";
	        echo '</script>' . "\n\n";
	    }
	    else { 
	        //After getting the successful token
	        require_once IRECRUIT_DIR . 'shopping/ProcessBasketInfo.inc';
	    }  //condition after getting the successfull token
		
	} // end else ERROR
} // end process

if ($display == "Y") {
	if ($test == "Y") {
        $_REQUEST['CCtype']             =   "visa";
        $_REQUEST['CCnumber']           =   "4111111111111111"; 
        $_REQUEST['CCexpmo']            =   "02";
        $_REQUEST['CCexpyear']          =   "2020";
        $_REQUEST['CCidentifier1']      =   "433"; 
        $_REQUEST['FullName']           =   "David Edgecomb"; 
        $_REQUEST['Address1']           =   "462 Long Bow Lane East"; 
        $_REQUEST['Address2']           =   "";
        $_REQUEST['City']               =   "Becket"; 
        $_REQUEST['State']              =   "MA"; 
        $_REQUEST['ZipCode']            =   "01223"; 
        $_REQUEST['Country']            =   "US"; 
        $_REQUEST['Email']              =   "dedgecomb@irecruit-software.com"; 
        $_REQUEST['Phone']              =   "(413) 426-8215";
	} // end test
	
	if(isset($_SESSION['I']['payment_information'][$USERID]) && count($_SESSION['I']['payment_information'][$USERID]) > 0)
	{
        $_REQUEST['CCSavedCards']       =   $_SESSION['I']['payment_information'][$USERID]['CCSavedCards'];
        $_REQUEST['CCtype']             =   $_SESSION['I']['payment_information'][$USERID]['CCtype'];
        $_REQUEST['CCnumber']           =   $_SESSION['I']['payment_information'][$USERID]['CCnumber'];
        $_REQUEST['CCexpmo']            =   $_SESSION['I']['payment_information'][$USERID]['CCexpmo'];
        $_REQUEST['CCexpyear']          =   $_SESSION['I']['payment_information'][$USERID]['CCexpyear'];
        $_REQUEST['CCidentifier1']      =   $_SESSION['I']['payment_information'][$USERID]['CCidentifier1'];
        $_REQUEST['FullName']           =   $_SESSION['I']['payment_information'][$USERID]['FullName'];
        $_REQUEST['Address1']           =   $_SESSION['I']['payment_information'][$USERID]['Address1'];
        $_REQUEST['Address2']           =   $_SESSION['I']['payment_information'][$USERID]['Address2'];
        $_REQUEST['City']               =   $_SESSION['I']['payment_information'][$USERID]['City'];
        $_REQUEST['State']              =   $_SESSION['I']['payment_information'][$USERID]['State'];
        $_REQUEST['ZipCode']            =   $_SESSION['I']['payment_information'][$USERID]['ZipCode'];
        $_REQUEST['Country']            =   $_SESSION['I']['payment_information'][$USERID]['Country'];
        $_REQUEST['Email']              =   $_SESSION['I']['payment_information'][$USERID]['Email'];
        $_REQUEST['Phone']              =   $_SESSION['I']['payment_information'][$USERID]['Phone'];
    }
	 
	// basket here
	include IRECRUIT_DIR . 'shopping/BasketItems.inc';

	if ($itmcnt > 0) {
		echo $basket;
	} else {
		echo '<p align="center"><br>There are no items in your shopping basket.</p>';
	}

	echo '<br>';

	echo <<<END
<style>
i { font-style: normal; }
</style>
END;

	$missing = " style=\"color: red; font-weight: bold;\"";

	if ($amount > 1) {
	    require_once IRECRUIT_DIR . 'shopping/PaymentForm.inc';
	} // if amount > 1
} // end display
