<?php 
echo '<table border="0" cellspacing="0" cellpadding="3" width="350">';
echo '<tr><td>';
echo "<p><b>" . htmlspecialchars($title) . "</b></p>";

echo 'The Card Security Code ("CSC" for short) is a three or four-digit number printed on the credit card for security purposes.<br><br>On most cards, the CSC is three digits long and is printed on the back of the card (usually in the signature field). Other numbers may appear to the left of the CSC. For American Express the code is located on the front of the card.';

echo '<p align="center"><img src="' . IRECRUIT_HOME . 'images/CSC_en_US_all.jpg"></p>';

echo '<p>Please note: The security code is not the last 3 or 4 digits of your credit card number.</p>';
echo '</td></tr>';
echo '</table>';

echo '<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
?>