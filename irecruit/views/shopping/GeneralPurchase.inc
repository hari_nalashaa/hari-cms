<?php 
$display    = "Y";
$att        = "block";

// set columns
$columns = "count(*) as COUNT";
// set where condition
$where = array ("OrgID = :OrgID", "UserID = :UserID");
// set parameters
$params = array (":OrgID" => $OrgID, ":UserID" => $USERID);
// get basket information
$results = G::Obj('Purchases')->getBasketInfo ( $columns, $where, "", array ($params) );

$cnt = $results['results'][0]['COUNT'];

if ($_REQUEST['process'] == "Y") {

	// qualify fields
	$ERROR = "";
	$error = "";

	if ($_REQUEST['ServiceType'] == "Invoice") {
		$att = "block";
	}
	if ($_REQUEST['ServiceType'] == "General") {
		$att = "none";
		$InvoiceNo = "";
	}

	if ($_REQUEST['BudgetTotal'] == "") {
		if ($error != "Y") {
			$ERROR .= " - Budget Total is missing." . "\\n";
			$BudgetTotal_error = "Y";
			$error = "Y";
		}
	}

	if (! is_numeric ( preg_replace ( "/\./", '', $_REQUEST['BudgetTotal'] ) )) {
		if ($error != "Y") {
			$ERROR .= " - Budget Total needs to contain only digits separated only by a decimal point." . "\\n";
			$BudgetTotal_error = "Y";
		}
	}

	if ($ERROR) {
		$MESSAGE = "The following entries are missing information.\\n\\n";
		$MESSAGE .= $ERROR;

		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('$MESSAGE')" . "\n";
		echo '</script>' . "\n\n";
	} else { // else ERROR

		// Basket information
		$basket_info = array (
				"OrgID"         => $OrgID,
				"UserID"        => $USERID,
				"LineNo"        => uniqid (),
				"ServiceType"   => $_REQUEST['ServiceType'],
				"InvoiceNo"     => $_REQUEST['InvoiceNo'],
				"BudgetTotal"   => $_REQUEST['BudgetTotal'],
				"Comment"       => $_REQUEST['Comment']
		);
		//Insert basket data
		G::Obj('Purchases')->insPurchaseInfo ( 'Basket', $basket_info );

		$display = "N";
	} // end ERROR
} // end process

if ($display == "Y") {

	echo <<<END
<style>
i { font-style: normal; }
</style>
END;

	$missing = " style=\"color: red; font-weight: bold;\"";

	// Header for Requisition Applied for
	echo '<form method="post" action="shopping/generalPurchase.php" id="aform">';
	echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';

	echo '<tr>';
	echo '<td colspan="3" valign="bottom" height="30">';
	echo '<strong>Use this form to add your payment to the shopping basket.</strong>';
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td align="right" width="150">Select a payment type:</td>';
	
	echo '<td colspan="2">';
    echo '<div style="width:100px;float:left">';
	echo '<select name="ServiceType" id="ServiceType" onchange="ReverseDisplay(\'emailcheck\')" style="margin:0px;padding:0px;">';
	echo '<option value="Invoice"';
	if ($_REQUEST['ServiceType'] == "Invoice") {
		echo ' selected';
	}
	echo '>Invoice</option>';
	echo '<option value="General"';
	if ($_REQUEST['ServiceType'] == "General") {
		echo ' selected';
	}
	echo '>General</option>';
	echo '</select>';
	echo '</div>';
	echo '<div id="emailcheck" style="display:' . $att . ';">';
	echo '<i';
	if ($InvoiceNo_error == "Y") {
		echo $missing;
	}
	echo '>Invoice Number</i>: ';

	echo '<input type="text" name="InvoiceNo" size="20" maxlength="20" value="' . htmlspecialchars($_REQUEST['InvoiceNo']) . '">';
	echo '</div>';
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td colspan="3" valign="bottom" height="30">';
	echo 'Please enter the amount you would like to pay.';
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td align="right" width="150">';
	echo '<i';
	if ($BudgetTotal_error == "Y") {
		echo $missing;
	}
	echo '>Amount:</i> $</td>';
	echo '<td colspan="2"><input type="text" name="BudgetTotal" value="' . htmlspecialchars($_REQUEST['BudgetTotal']) . '" size="6" maxlength="10">';
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td colspan="3" valign="bottom" height="30">';
	echo 'Please enter any additional comment you would like to make concerning this payment.';
	echo '</td>';
	echo '</tr>';

	echo '<tr>';
	echo '<td colspan="3">';
	echo '<textarea name="Comment" cols="60" rows="5">' . $_REQUEST['Comment'] . '</textarea>';
	echo '</td>';
	echo '</tr>';

	echo '<tr><td colspan="3" align="center" valign="bottom" height="30">';
	echo '<input type="hidden" name="process" value="Y">';
	echo '<input type=\'button\' class=\'btn btn-primary\' value=\'add to basket\' class=\'btn btn-primary\' onclick=\'createInvoice(this, "'.$AccessCode.'")\'>';
	echo '</td></tr>';

	echo '</table>';
	echo '</form>';

} // end display
?>