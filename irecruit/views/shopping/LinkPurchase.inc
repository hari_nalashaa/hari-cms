<?php 
$pagewidth  =   "600";
$display    =   "Y";
$prices     =   array ('450.00', '900.00', '1500.00');


//Get MultiOrgID based on RequestID
$IndeedMultiOrgID       =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $_REQUEST['RequestID']);

$onclickbasketA = " onclick=\"viewBasket('shopping/basket.php";
if ($AccessCode != "") {
	$onclickbasketA .= "?k=" . $AccessCode;
}
$onclickbasketA .= "')\"";

$onclickbasketB = " onclick=\"viewBasket('shopping/basket.php";
if ($AccessCode != "") {
	$onclickbasketB .= "?k=" . $AccessCode;
}
$onclickbasketB .= "')\"";

if ($_REQUEST['process'] == "Y") {

	// qualify fields
	$ERROR = "";
	$error = "";

	if (!isset($_REQUEST['Indeed'])) {
		$ERROR .= " - A post type must be selected." . "\\n";
		$error = "Y";
	} 

	if ($_REQUEST['Indeed'] == "Sponsored") {
		if ($_REQUEST['BudgetTotal'] == "") {
			if ($error != "Y") {
				$ERROR .= " - Budget Total is missing." . "\\n";
				$BudgetTotal_error = "Y";
				$error = "Y";
			}
		}

		if($indeed_config_info['IndeedApply'] == "Y" && $_REQUEST['IndeedLabel'] == "") {
		    $ERROR .= " - Form Choice must be selected." . "\\n";
		    $error = "Y";
		}

		
		if (! is_numeric ( preg_replace ( "/\./", '', $_REQUEST['BudgetTotal'] ) )) {
			if ($error != "Y") {
				$ERROR .= " - Budget Total needs to contain only digits separated only by a decimal point." . "\\n";
				$BudgetTotal_error = "Y";
			}
		}
		
		if(isset($_REQUEST['txtBudgetTotal']) 
		  && $_REQUEST['txtBudgetTotal'] != ""
          && $_REQUEST['txtBudgetTotal'] < 200) {
		    $ERROR .= " - Budget Total needs to be at least \$200." . "\\n";
		}
	} 

	if ($ERROR) {
		$MESSAGE = "The following entries are missing information.\\n\\n";
		$MESSAGE .= $ERROR;

		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('$MESSAGE')" . "\n";
		echo '</script>' . "\n\n";
	} else { // else ERROR

		if ($_REQUEST['SponsoredInfo'] == "Sponsored") {
		    
			$ServiceType = "Indeed";
				
            //Get MultiOrgID based on RequestID
            $indeed_config_info =   $IndeedObj->getIndeedConfigInfo($OrgID, $IndeedMultiOrgID);
            
            $indeed_feed_info   =   array(
                                        'OrgID'             =>  $OrgID,
                                        'RequestID'         =>  $_REQUEST['RequestID'],
                                        'MultiOrgID'        =>  $indeed_config_info['MultiOrgID'],
                                        'BudgetTotal'       =>  $_REQUEST['BudgetTotal'],
                                        'ContactName'       =>  $indeed_config_info['ContactName'],
                                        'Email'             =>  $indeed_config_info['Email'],
                                        'Phone'             =>  $indeed_config_info['Phone'],
                                        );
            $IndeedObj->insIndeedFeedInfo($indeed_feed_info);
						
			####################################################################################
			$req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $RequestID);
			$free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);
			$free_count         =   count($free_jobboard_list);
			
			for($j = 0; $j < $free_count; $j++) {
			    if($free_jobboard_list[$j] == "Indeed") unset($free_jobboard_list[$j]);
			}
			
			$free_jobboard_list = @array_unique($free_jobboard_list);
			
			$free_jobboard_list = @array_values($free_jobboard_list);
			
			$where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
			$params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":FreeJobBoardLists"=>json_encode($free_jobboard_list), ":IndeedLabelID"=>$_REQUEST['IndeedLabel'], ":IndeedWFH"=>$_REQUEST['IndeedWFH']);
			$set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists", "IndeedLabelID = :IndeedLabelID", "IndeedWFH= :IndeedWFH");
			
			$result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
			####################################################################################

            echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
            echo '<tr><td height="100" valign="middle">';
            echo '<br>';
            echo '<span style="color:#4D7191;font-weight:bold;font-size:24px;">';
            echo 'Thank you for sponsoring your job on Indeed.<br><br>';
            echo '</span>';
            echo '<strong>Note:</strong> Sponsored Job campaigns will not go live without a payment method connected to your Indeed account.</strong>';
            echo ' You will receive an email from Indeed within a few hours with instructions for setting this up for the first time. If you do not receive this email, please visit <a href="https://billing.indeed.com" target="_blank">billing.indeed.com</a>.';
            echo '<br><br>';
            echo '</td></tr>';
            echo '</table>';
            
        }
        else if ($_REQUEST['SponsoredInfo'] == "UnSponsored") {
            //Get MultiOrgID based on RequestID
            $set_info       =   array("Sponsored = 'no'", "LastModifiedDateTime = NOW()");
            $where_info     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID");
            $params_info    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$IndeedMultiOrgID, ":RequestID"=>$_REQUEST['RequestID']);
            
            $IndeedObj->updIndeedFeedInfo($set_info, $where_info, array($params_info));
            
            echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
            echo '<tr><td height="100" valign="middle">';
            echo '<br><span style="color:#4D7191;font-weight:bold;font-size:24px;">Successfully removed this job from sponsored list.</span><br><br>';
            echo '</td></tr>';
            echo '</table>';
        }

		$display = "N";
	} // end ERROR
} // end process

//Get MultiOrgID based on RequestID
$indeed_sponsored_info  =   $IndeedObj->getIndeedFeedInformation("*", $OrgID, $IndeedMultiOrgID, $_REQUEST['RequestID']);

//If display is not yes
if($indeed_sponsored_info['Sponsored'] == 'yes' && $_REQUEST['display'] != 'yes') {
    $display = "N";
}

//If Error is not empty set display as yes
if($ERROR != "") {
    $display = "Y";
}

echo '<form name="frmLinkPurchase" id="frmLinkPurchase" method="post" action="linkPurchase.php">';

if ($display == "Y") {

    //set table name
	$table_name =  "IndeedFeed INF, Purchases P, PurchaseItems PI";
	//set columns
	$columns   =   "DATEDIFF(DATE(NOW()),DATE(P.PurchaseDate)), INF.PurchaseNumber, PI.Canceled, P.Processed";
	//set where condition
	$where     =   array(
                        "INF.OrgID          =   P.OrgID", 
                        "INF.OrgID          =   PI.OrgID", 
                        "INF.PurchaseNumber =   P.PurchaseNumber", 
                        "INF.PurchaseNumber =   PI.PurchaseNumber",
                        "PI.ServiceType     =   'Indeed'", 
                        "INF.OrgID          =   :OrgID", 
                        "INF.RequestID      =   :RequestID"
	               );
	//set parameters
	$params    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	
	//get purchases info feed details
	$resultsIN =   G::Obj('Purchases')->getPurchasesAndPurchaseItems ( $table_name, $columns, $where, "", array ($params) );
	
	$INcnt     =   $resultsIN['count'];
	
	if(is_array($resultsIN['results'][0])) {
		list ( $INallowcancel, $INPurchaseNumber, $INcanceled, $INProcessed ) = array_values($resultsIN['results'][0]);
	}
	
	// set columns
	$columns   =   "count(*) as COUNT";
	// set where condition
	$where     =   array ("OrgID = :OrgID",  "UserID = :UserID");
	// set parameters
	$params    =   array (":OrgID" => $OrgID, ":UserID" => $USERID);
	// get basket information
	$results   =   G::Obj('Purchases')->getBasketInfo ( $columns, $where, "", array ($params) );
	$cnt       =   $results['results'][0]['COUNT'];
	

	if (($cnt > 0) && ($_REQUEST['process'] != "Y")) {
		echo '<br><table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
		echo '<tr><td align="right" valign="bottom" height="30">';
		echo '<a href="javascript:void(0);" ' . $onclickbasketB . '>';
		echo '<b style="FONT-SIZE:8pt;COLOR:#000000">There are items to view in your basket</b>';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/basket.png" border="0" title="Shopping Basket" style="margin:0px 0px -3px 3px;">';
		echo '</a>';
		echo '</td></tr>';
		echo '</table>';
	} // end if
	
	$missing = " style=\"color: red; font-weight: bold;\"";

	echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
	if ($INPurchaseNumber != "") {
		$PurchaseNumber = $INPurchaseNumber;
	}
	if ($PurchaseNumber != "") {
		echo '<input type="hidden" name="PurchaseNumber" id="PurchaseNumber" value="' . $PurchaseNumber . '">';
	}
	echo '<input type="hidden" name="OrgID" id="OrgID" value="' . $OrgID . '">';
	echo '<input type="hidden" name="RequestID" id="RequestID" value="' . $RequestID . '">';
	echo '<input type="hidden" name="SponsoredInfo" id="SponsoredInfo"';
	if($indeed_sponsored_info['Sponsored'] == 'yes') {
		echo ' value="Sponsored"';
	}
	echo '>';
	echo '<input type="hidden" name="process" id="process" value="Y">';

	if($ServerInformationObj->getRequestSource() != 'ajax') {
		echo '<tr><td colspan="2" align="top" height="40">';
		if ($feature ['MultiOrg'] == "Y") {
			echo "Organization: <b>" . $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . "</b>";
			echo "<br>";
		}
		echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $IndeedMultiOrgID, $RequestID ) . "</b>";
		echo "<br>";
		echo "Title: <b>" . $RequisitionDetailsObj->getJobTitle ( $OrgID, $IndeedMultiOrgID, $RequestID ) . "</b>";
		echo '</td></tr>';
	}
	
	echo '<tr><td colspan="2">';
	
	echo '<br>';

	$dis_info_req = "false";
	if ($INcnt > 0) {
		
		echo '<br><img src="' . IRECRUIT_HOME . 'images/indeed_jobboard.png">';
		echo '&nbsp;&nbsp;&nbsp;';
		
		if (($INProcessed == null) && ($INcanceled != null)) {
			echo '<font style="color:red;">Order Canceled.</font><br><br><br>';
		} else {
			echo 'This item is already sponsored on Indeed.<br><br><br>';
		}
		
		if (($INallowcancel == 0) && ($INProcessed == null)) {
			
			if ($INcanceled == null) {
				echo '<button type="submit" name="cancelIN" value="Y" onclick="return confirm(\'Are you sure you want to cancel this order?\n\nIndeed\')">Cancel this order.</button><br><br>';
			}
		} // end INallowcancel && INProcessed
		
	} else {
	    $dis_info_req = "true";
	    $free_indeed_checked = "";
	    if(in_array("Indeed", $free_jobboard_list)) $free_indeed_checked = ' checked="checked"';

		echo '<img src="' . IRECRUIT_HOME . 'images/indeed_jobboard.png"><br><br>';
		
		//Get MultiOrgID based on RequestID
		$indeed_sponsored_info  =   $IndeedObj->getIndeedFeedInformation("*", $OrgID, $IndeedMultiOrgID, $_REQUEST['RequestID']);
		$indeed_config_info     =   $IndeedObj->getIndeedConfigInfo($OrgID, $IndeedMultiOrgID);
	
		if($indeed_config_info['IndeedApply'] == 'Y') {
		    echo '&nbsp;&nbsp;&nbsp;Form Choice: ';
		    echo '<select name="IndeedLabel" id="IndeedLabel" class="form-control width-auto-inline">';
		    echo '<option value="">Please Select</option>';
		    foreach($labels_list as $label_name) {
		        echo '<option value="'.$label_name.'"';
		        if($req_info['IndeedLabelID'] == $label_name) {
		            echo ' selected="selected"';
		        }
		        echo '>'.$label_name.'</option>';
		    }
		    echo '</select>';
		    echo '<br><br>';

		}
		else {
		    echo '<input type="hidden" name="IndeedLabel" id="IndeedLabel" value="">';
		}

		    echo '&nbsp;&nbsp;&nbsp;Differentiate work from home capability: ';
		    echo '<select name="IndeedWFH" id="IndeedWFH" class="form-control width-auto-inline">';
		    echo '<option value="">Please Select</option>';

		    $LABELTYPE = array();
		    $LABELTYPE[] = array("value"=>"COVID-19","name"=>"Temporarily remote due to COVID-19");
		    $LABELTYPE[] = array("value"=>"WFH Flexible","name"=>"Work from home flexibility");
		    $LABELTYPE[] = array("value"=>"Fully remote","name"=>"Fully remote");

		    foreach($LABELTYPE as $LT) {
		        echo '<option value="'.$LT['value'].'"';
		        if($req_info['IndeedWFH'] == $LT['value']) {
		            echo ' selected="selected"';
		        }
		        echo '>'.$LT['name'].'</option>';
		    }

		    echo '</select>';
		    echo '<br><br>';
		
		if($indeed_config_info['Email'] != ''
		  && $indeed_config_info['ContactName'] != ''
		  && $indeed_config_info['Phone'] != '') {
		    
		    echo '<br>Sponsor this job on Indeed for prominent placement on the world\'s #1 job site.<br>';
		    
		    echo '<br>Indeed Sponsored Jobs get up to 5X more clicks and can help you hire quickly and build a strong candidate pipeline.<br>';
		    
		    echo '<br><strong>Select a fixed budget per job for 30 days to get started:</strong><br><br>';
		}
		
		
		echo '&nbsp;&nbsp;<input type="radio" name="Indeed" value="Free"';
		echo $free_indeed_checked;
		echo ' class="indeed_fp_sel">&nbsp;Post your job to Indeed - Free';
		echo '<br>';

		/* This code in place of below so javascript works */
		//echo '<input type="hidden" name="BudgetTotal">';
        
        if($indeed_config_info['Email'] != ''
		  && $indeed_config_info['ContactName'] != ''
		  && $indeed_config_info['Phone'] != '') {

            
            echo '&nbsp;&nbsp;<input type="radio" name="Indeed" value="Sponsored"';
            if($indeed_sponsored_info['Sponsored'] == 'yes') {
                echo ' checked="checked"';
            }
            echo ' class="indeed_fp_sel">&nbsp;';
            echo 'Post your job to Indeed - Sponsored';
            echo '&nbsp;&nbsp;';
            echo '<i';
            if ($BudgetTotal_error == "Y") {
                echo $missing;
            }

            echo '>Budget Total:</i>';
            $b = 1;
            foreach ( $prices as $price ) {
                echo '&nbsp;<input type="radio" name="BudgetTotal" id="BudgetTotal'.$b.'" value="'.$price.'"';
                if($indeed_sponsored_info['BudgetTotal'] == $price) echo ' checked="checked"';
                echo '> $' . $price;
                $b++;
            }

            $other_budget   =   "";
            if(!in_array($indeed_sponsored_info['BudgetTotal'], $prices)) {
                $other_budget   =   $indeed_sponsored_info['BudgetTotal'];
            }
            
            echo '&nbsp;<input type="radio" name="BudgetTotal" id="BudgetTotal4" value="'.$other_budget.'"';
            if(($indeed_sponsored_info['BudgetTotal'] != "") 
                && !in_array($indeed_sponsored_info['BudgetTotal'], $prices)) echo ' checked="checked"';
            echo '> $';
            
            echo '<input type="text" name="txtBudgetTotal" id="txtBudgetTotal" maxlength="6" size="6" value="'.$other_budget.'" onkeyup="validateNumeric(this.value)" onkeydown="validateNumeric(this.value)">';
            
            echo '<br><br>You will only be billed for the unique clicks on your job, and if you close or remove the job from iRecruit, the sponsored campaign will stop.<br><br>';
            

            echo '<strong style="color:red;">Important:</strong> Sponsored Job campaigns will not go live without an active Indeed account. 
                 If you do not have an Indeed account one will be created for you using the email address connected to iRecruit. You\'ll receive an account activation email from Indeed to finish a one time setup of your Indeed account at the address connected to your iRecruit account within a few hours. Please review that email to ensure Indeed has the information they need to start your first campaign.<br><br>';
            echo "<span style='font-style:italic'>Once Indeed gets your Sponsored Job from iRecruit, and your account is activated, your Sponsored Job campaign will be live within a few hours</span>";
            echo '.<br><br>';

            echo '<a href="https://youtu.be/LcCCHeSVhqE" target="_blank">Watch this video</a> to learn more or <a href="https://www.indeed.com/hire/contact" target="_blank">contact Indeed</a> to discuss more options.<br><br>';

            echo 'By sponsoring this job and creating an account, you agree to Indeed\'s <a href="https://www.indeed.com/legal?hl=en_US#tos" target="_blank">Terms of Service</a> and consent to our <a href="https://www.indeed.com/legal?hl=en_US#privacy" target="_blank">Privacy Policy</a> and <a href="https://www.indeed.com/legal?hl=en_US#cookies" target="_blank">Cookie Policy</a>.';
            
        }
           
 
		

	} // end Indeedcnt
	
	echo '<hr size="1">';
	
	if ($INcnt == 0) {

		echo 'Job posting sites reserve the right to reject requisitions pending content review.<br>';
		
		echo '</td></tr>';
		
		echo '<tr><td colspan="2" align="left" valign="bottom" height="30">';
		
		if($ServerInformationObj->getRequestSource() == 'ajax') {
		    if(in_array("Indeed", $free_jobboard_list)) {
		        echo '<input type="hidden" name="IndeedConfigApply" id="IndeedConfigApply" value="'.$indeed_config_info['IndeedApply'].'">';
		        echo '<input class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="updateFreeJobBoardListings(\'Indeed\');" value="Update" type="button">';
		    }
		    else {
		        if($indeed_config_info['Email'] != ''
    	            && $indeed_config_info['ContactName'] != ''
	                && $indeed_config_info['Phone'] != '') {
		            echo '<input type="hidden" name="IndeedConfigApply" id="IndeedConfigApply" value="'.$indeed_config_info['IndeedApply'].'">';
		            echo '<input type="button" class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="sponsorIndeedRequisition(this);" value="Sponsor Job on Indeed now">';
		        }
		        else {
		            echo '<input type="hidden" name="IndeedConfigApply" id="IndeedConfigApply" value="'.$indeed_config_info['IndeedApply'].'">';
		            echo '<input class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="updateFreeJobBoardListings(\'Indeed\');" value="Update" type="button">';
		        }
		    }
		}
		else {
			echo '<input type="submit" value="add to basket">';
		}
		
		
		echo '</td></tr>';
	} // end Indeedcnt
		
    if($dis_info_req == "true") {
        echo '<tr><td>';
        echo '<font color="#FC1F1F">Is this position over 30 days old?  We recommend <u>ONLY</u> sponsoring brand new positions.</font>';
        echo '</td></tr>';
    }
	
	echo '</table>';

} // end display
else if ($display == "N" && $indeed_sponsored_info['Sponsored'] == 'yes') {

    echo '<input type="hidden" name="OrgID" id="OrgID" value="' . $OrgID . '">';
    echo '<input type="hidden" name="RequestID" id="RequestID" value="' . $RequestID . '">';
    echo '<input type="hidden" name="SponsoredInfo" id="SponsoredInfo" value="UnSponsored">';
    echo '<input type="hidden" name="process" id="process" value="Y">';
    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    echo '<tr><td height="100" valign="middle" align="center">';
    echo '<br><img src="'.IRECRUIT_HOME.'images/indeed_jobboard.png">.<br>';
    echo '<br><span style="color:#A4A4A4;font-weight:bold;font-size:24px;">This post is managed by Indeed</span>.<br>
            <span style="color:#4D7191;font-weight:bold;font-size:24px;">Check the status</span>, 
            <span style="color:#A4A4A4;font-weight:bold;font-size:24px;">or </span>
            <br><span style="color:#4D7191;font-weight:bold;font-size:24px;">contact Indeed<span>
            <span style="color:#A4A4A4;font-weight:bold;font-size:24px;"> for details.</span><br>';
    echo '<br><input type="button" name="btnEndCampaign" id="btnEndCampaign" onclick="endIndeedCampaign(this)" style="background-color:#2D67EA;border-color:#2D67EA;color:white;font-weight:bold;border:1px solid transparent;border-radius:6px;padding:12px;" value="End Campaign"><br>';
    echo '<br><input type="button" name="btnAddBudget" id="btnAddBudget" onclick="getPurchasePremiumFeedInfo(\'no\', \'yes\')" style="background-color:#2D67EA;border-color:#2D67EA;color:white;font-weight:bold;border:1px solid transparent;border-radius:6px;padding:12px;" value="Add budget"><br><br>';
    echo '</td></tr>';
    echo '</table>';
        
}

    echo '<div style="border-radius:15px;background: #eeeeee;padding: 10px;">';
    echo 'Indeed differentiates "remote" or "work from home" jobs in the US in three distinct ways:<br><br>';

    echo '<ol>';
    echo '<li><strong>"Temporarily remote due to COVID-19"</strong> means the job is fully remote, but only for the duration of the COVID-19 pandemic, and will transition back to a non-remote job after the pandemic has ended. These jobs will be labeled as remote so that it\'s visible on a job and will appear in relevant searches for remote work.</li>';

    echo '<li><strong>"Work from home flexibility"</strong> means the employer is flexible with letting employees work from home for periods of time, but there is still an office setting to report to. This flexibility is treated as a benefit or perk to employees, not a feature of the job setting. These jobs will NOT be labeled as remote but will still appear in relevant searches for work from home jobs.</li>';


    echo '<li><strong>"Fully remote"</strong> means the job can be performed completely from home. This could be because there is no office to report to or because the employer is willing to consider remote candidates. Jobs that require regular local commutes to job sites (such as installation technicians or home health nurses) are NOT considered fully remote. Fully remote jobs will be labeled as remote so that it\'s visible on your job and will appear in relevant searches for remote work.</li>';

    echo '</ol>';

    echo 'Additionally, in order to identify roles in one of these three distinct ways, there are certain phrases employers can include in job descriptions:';

    echo '<ol>';
    echo '<li>To be considered a "<strong>temporarily remote due to COVID-19</strong>" job, please include one of the following:';
    echo '<ul>';
    echo '<li>"Work remotely temporarily due to COVID-19"</li>';
    echo '<li>"Work from home during coronavirus"</li>';
    echo '<li>"Can start remotely due to COVID-19"</li>';
    echo '</ul>';

    echo '<li>To be considered as a job having "<strong>work from home flexibility</strong>", please include one of the following:';

    echo '<ul>';
    echo '<li>"Flexible work from home options available"</li>';
    echo '<li>"Work from home days possible"</li>';
    echo '<li>"Options for working from home"</li>';
    echo '</ul>';
    echo '<li>To be considered a "<strong>fully remote</strong>" job, please include one of the following:';
    echo '<ul>';
    echo '<li>"This is a remote position"</li>';
    echo '<li>"Employees will be working remotely"</li>';
    echo '<li>"Remote work allowed"</li>';
    echo '</ul>';
    echo '</ol>';

   echo '</li>';

    echo 'In summary, there are three differentiated options that Indeed uses to accurately identify jobs: "<strong>"fully remote</strong>" should be used when the employer intends all work for the job to be done remotely or from home, and "<strong>work from home flexibility</strong>" should be used when the employer gives employees the option to work from home occasionally as a benefit. For jobs that are temporarily remote during the pandemic, please use the "<strong>Temporarily remote due to COVID-19</strong>" options.';

    echo '</div>';

echo '</form>';


if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<br><p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
}
?>
