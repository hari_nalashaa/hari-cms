<?php
$pagewidth  =   "600";
$display    =   "Y";

$onclickbasketA = " onclick=\"viewZipRecruiterBasket('shopping/zipRecruiterBasket.php";
if ($AccessCode != "") {
	$onclickbasketA .= "?k=" . $AccessCode;
}
$onclickbasketA .= "')\"";

$onclickbasketB = " onclick=\"viewZipRecruiterBasket('shopping/zipRecruiterBasket.php";
if ($AccessCode != "") {
	$onclickbasketB .= "?k=" . $AccessCode;
}
$onclickbasketB .= "')\"";

if ($_REQUEST['process'] == "Y") {

	// qualify fields
	$ERROR = "";
	$error = "";

	$ZipRecruiterPrices        =   $IrecruitSettingsObj->getValue('ZipRecruiterPrices');
	$ZipRecruiterBudgetTotal   =   $ZipRecruiterPrices[$_REQUEST['BudgetTotal']];
	
	if ($ZipRecruiterBudgetTotal == "") {
		$ERROR .= " - Please select an item to add to your basket." . "\\n";
	}

	if ($cancelZIP == "Y") {

		// set information
		$set_info = array (
				"Canceled           =   NOW()"
		);
		// set where condition
		$where = array (
				"OrgID              =   :OrgID",
				"RequestID          =   :RequestID",
				"PurchaseNumber     =   :PurchaseNumber",
				"ServiceType        =   'ZipRecruiter'"
		);
		// set params
		$params = array (
				":OrgID"            =>  $OrgID,
				":RequestID"        =>  $RequestID,
				":PurchaseNumber"   =>  $PurchaseNumber
		);
		// update purchase information
		G::Obj('Purchases')->updPurchaseInfo ( 'PurchaseItems', $set_info, $where, array ($params) );

		$ERROR = " - Your ZipRecruiter order has been canceled." . "\\n";
	}

	if ($cancelZIPRECRUITER == "Y") {

		// set information
		$set_info = array (
				"Canceled           =   NOW()"
		);
		// set where condition
		$where = array (
				"OrgID              =   :OrgID",
				"RequestID          =   :RequestID",
				"PurchaseNumber     =   :PurchaseNumber",
				"ServiceType        =   'ZipRecruiter'"
		);
		// set params
		$params = array (
				":OrgID"            =>  $OrgID,
				":RequestID"        =>  $RequestID,
				":PurchaseNumber"   =>  $PurchaseNumber
		);
		// update purchase information
		G::Obj('Purchases')->updPurchaseInfo ( 'PurchaseItems', $set_info, $where, array ($params) );

		$ERROR = " - Your ZipRecruiter order has been canceled." . "\\n";
	}
    
	if ($ERROR) {
		$MESSAGE = "The following entries are missing information.\\n\\n";
		$MESSAGE .= $ERROR;

		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('$MESSAGE')" . "\n";
		echo '</script>' . "\n\n";
	} else { // else ERROR

		$ServiceType = "ZipRecruiter";
		
		// set where condition
		$where = array (
				"OrgID         =   :OrgID",
				"RequestID     =   :RequestID",
				"ServiceType   =   :ServiceType"
		);
		// set parameters
		$params = array (
				":OrgID"       =>  $OrgID,
				":RequestID"   =>  $RequestID,
				":ServiceType" =>  $ServiceType
		);
		// Delete basket information
		G::Obj('Purchases')->delBasketAndPurchaseInfo ( 'Basket', $where, array ($params) );
			
		// Basket information
		$basket_info = array (
				"OrgID"                     =>  $OrgID,
				"UserID"                    =>  $USERID,
				"LineNo"                    =>  uniqid (),
				"ServiceType"               =>  $ServiceType,
				"RequestID"                 =>  $RequestID,
				"BudgetTotal"               =>  $ZipRecruiterBudgetTotal,
                "ZipRecruiterTrafficBoost"  =>  $_REQUEST['TrafficBoost']
		);
		
		// Insert basket information
		G::Obj('Purchases')->insPurchaseInfo ( 'Basket', $basket_info );
		
		####################################################################################
		$req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $RequestID);
		$free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);
        $free_count         =   count($free_jobboard_list);
	
	    for($j = 0; $j < $free_count; $j++) {
            if($free_jobboard_list[$j] == "ZipRecruiter") unset($free_jobboard_list[$j]);
	    }
	
	    $free_jobboard_list = @array_unique($free_jobboard_list);
		$free_jobboard_list = @array_values($free_jobboard_list);
		
		$where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
		$params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":FreeJobBoardLists"=>json_encode($free_jobboard_list), ":ZipRecruiterLabelID"=>$_REQUEST['ZipRecruiterLabel']);
		$set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists", "ZipRecruiterLabelID = :ZipRecruiterLabelID");
		
		$result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
		####################################################################################

		echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
		echo '<tr><td height="100" valign="middle">';
		echo 'Your entry has been entered into the shopping basket.<br><br>';
		echo '<a href="javascript:void(0);"' . $onclickbasketA . '>';
		echo '<b style="FONT-SIZE:8pt;COLOR:#000000">view basket</b>';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/basket.png" border="0" title="Shopping Basket" style="margin:0px 0px -3px 3px;">';
		echo '</a>';
		echo '</td></tr>';
		echo '</table>';

		$display = "N";
	} // end ERROR
} // end process

if ($display == "Y") {

	echo <<<END
<style>
i { font-style: normal; }
</style>
END;

	//set table name
	$table_name    = "ZipRecruiterFeed INF, Purchases P, PurchaseItems PI";
	//set columns
	$columns       = "DATEDIFF(DATE(NOW()),DATE(P.PurchaseDate)), INF.PurchaseNumber, PI.Canceled, P.Processed";
	//set where condition
	$where         =   array(
                                "INF.OrgID              =   P.OrgID", 
                                "INF.OrgID              =   PI.OrgID", 
                                "INF.PurchaseNumber     =   P.PurchaseNumber", 
                                "INF.PurchaseNumber     =   PI.PurchaseNumber",
                                "PI.ServiceType         =   'ZipRecruiter'", 
                                "INF.OrgID              =   :OrgID", 
                                "INF.RequestID          =   :RequestID"
        	               );
	//set parameters
	$params    =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	
	//get purchases info feed details
	$resultsIN =   G::Obj('Purchases')->getPurchasesAndPurchaseItems ( $table_name, $columns, $where, "", array ($params) );
	$INcnt     =   $resultsIN['count'];
	
	if(is_array($resultsIN['results'][0])) {
		list ( $INallowcancel, $INPurchaseNumber, $INcanceled, $INProcessed ) = array_values($resultsIN['results'][0]);
	}
	
	// set columns
	$columns = "count(*) as COUNT";
	// set where condition
	$where = array ("OrgID = :OrgID",  "UserID = :UserID");
	// set parameters
	$params = array (":OrgID" => $OrgID, ":UserID" => $USERID);
	// get basket information
	$results = G::Obj('Purchases')->getBasketInfo ( $columns, $where, "", array ($params) );
	
	$cnt = $results['results'][0]['COUNT'];

	if (($cnt > 0) && ($_REQUEST['process'] != "Y")) {
		echo '<br><table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
		echo '<tr><td align="right" valign="bottom" height="30">';
		echo '<a href="javascript:void(0);" ' . $onclickbasketB . '>';
		echo '<b style="FONT-SIZE:8pt;COLOR:#000000">There are items to view in your basket</b>';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/basket.png" border="0" title="Shopping Basket" style="margin:0px 0px -3px 3px;">';
		echo '</a>';
		echo '</td></tr>';
		echo '</table>';
	} // end if
	
	$missing = " style=\"color: red; font-weight: bold;\"";

	$subscription_info = $ZipRecruiterObj->getZipRecruiterSubscriptionInfo($OrgID, $RequestID);
	
	if(isset($subscription_info['RequestID']) && $subscription_info['RequestID'] != "") {
	    echo '<br><br><input type="button" class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="subscribeToZipRecruiter();" value="Unsubscribe"><br>';
	}
	else {
	    echo '<form name="frmZipRecruiterPurchase" id="frmZipRecruiterPurchase" method="post" action="zipRecruiterPurchase.php">';
	    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
	    if ($INPurchaseNumber != "") {
	        $PurchaseNumber = $INPurchaseNumber;
	    }
	    if ($PurchaseNumber != "") {
	        echo '<input type="hidden" name="PurchaseNumber" id="PurchaseNumber" value="' . $PurchaseNumber . '">';
	    }
	    echo '<input type="hidden" name="OrgID" id="OrgID" value="' . $OrgID . '">';
	    echo '<input type="hidden" name="RequestID" id="RequestID" value="' . $RequestID . '">';
	    echo '<input type="hidden" name="process" id="process" value="Y">';
	    
	    if($ServerInformationObj->getRequestSource() != 'ajax') {
	        $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
	        echo '<tr><td colspan="2" align="top" height="40">';
	        if ($feature ['MultiOrg'] == "Y") {
	            echo "Organization: <b>" . $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . "</b>";
	            echo "<br>";
	        }
	        echo "Req/Job ID: <b>" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	        echo "<br>";
	        echo "Title: <b>" . $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID ) . "</b>";
	        echo '</td></tr>';
	    }
	    
	    echo '<tr><td colspan="2">';
	    
	    echo '<br>';

	    $dis_info_req = "false";
	    if ($INcnt > 0) {
	    
	        echo '<br><img src="' . IRECRUIT_HOME . 'images/ziprecruiter_jobboard.png">';
	        echo '&nbsp;&nbsp;&nbsp;';
	    
	        if (($INProcessed == null) && ($INcanceled != null)) {
	            echo '<font style="color:red;">Order Canceled.</font><br><br><br>';
	        } else {
	            echo 'This item is already sponsored on ZipRecruiter.<br><br><br>';
	        }
	    
	        if (($INallowcancel == 0) && ($INProcessed == null)) {
	            	
	            if ($INcanceled == null) {
	                echo '<button type="submit" name="cancelZIP" value="Y" onclick="return confirm(\'Are you sure you want to cancel this order?\n\nZipRecruiter\')">Cancel this order.</button><br><br>';
	            }
	        } // end INallowcancel && INProcessed
	    
	    } else {
	        $dis_info_req = "true";
	        $free_zip_rec_checked = "";
	        if(is_array($free_jobboard_list) && in_array("ZipRecruiter", $free_jobboard_list)) $free_zip_rec_checked = ' checked="checked"';
	        
	        echo '<img src="'.IRECRUIT_HOME.'images/ziprecruiter_jobboard.png"><br><br>';
	        echo '<input type="radio" name="TrafficBoost" value="free"';
	        echo $free_zip_rec_checked;
	        echo ' class="zip_rec_paid_tf">&nbsp; Free<br>';
            echo '<input type="radio" name="TrafficBoost" value="boost" class="zip_rec_paid_tf">&nbsp; Apply TrafficBoost to your job $299<br>';
            
            $subscription_settings      =   $ZipRecruiterObj->getZipRecruiterSubscriptionSettingsInfo($OrgID);
            $subscriptions_count        =   $ZipRecruiterObj->getZipRecruiterSubscriptionsCount($OrgID);
            $remaining_subscriptions    =   $subscription_settings['TotalSubscriptions'] - $subscriptions_count;
            
            if($subscription_settings['TotalSubscriptions'] > 0) {
                if($subscriptions_count < $subscription_settings['TotalSubscriptions']) {
                    echo '<input type="radio" name="TrafficBoost" value="subscribe" class="zip_rec_paid_tf">&nbsp; ZipRecruiter.com TrafficBoost Subscription<br>';
                    echo '<h4>Subscriptions available '.$remaining_subscriptions.' out of '.$subscription_settings['TotalSubscriptions'].'.</h4>';
                }
                else {
                    echo '<h4>Subscriptions are currently in use.</h4>';
                }
            }
            else {
            	//echo "<h4>TrafficBoost is available by subscription from iRecruit. Please contact support for options.</h4>";
            }
            
            echo '<br>Form Choice: ';
            echo '<select name="ZipRecruiterLabel" id="ZipRecruiterLabel" class="form-control width-auto-inline">';
            echo '<option value="">Please Select</option>';
            foreach($labels_list as $label_name) {
                echo '<option value="'.$label_name.'"';
                if($req_info_zip_rec['ZipRecruiterLabelID'] == $label_name) {
                	echo ' selected="selected"';
                }
                echo '>'.$label_name.'</option>';
            }
            
            echo '</select><br>';

            echo '<br>ZipRecruiter Job Category: '; 
            echo '<select name="ZipRecruiterJobCategory" id="ZipRecruiterJobCategory" class="form-control width-auto-inline">';
            echo '<option value="">Select Job Category</option>';
            for($jc = 0; $jc < count ( $ZipRecruiterJobCategories ); $jc ++) {
                $selected = ($ZipRecruiterJobCategories [$jc]  == $ZipRecruiterJobCategory) ? "selected='selected'" : "";
                echo '<option value="'.$ZipRecruiterJobCategories [$jc].'"'.$selected.'>'.$ZipRecruiterJobCategories [$jc].'</option>';
                unset ( $selected );
            }
            echo '</select>';
            
            echo '<input type="hidden" name="BudgetTotal" id="BudgetTotal">';
	    echo '<br><br>';
	    echo 'Job sponsorship via a 30-day TrafficBoost; the sponsorship will end after 30 days or 100 visits, whichever occurs first.';
	    echo '<br><br>';
	    echo 'For urgent or hard-to-fill hiring needs, make your job more competitive and apply a TrafficBoost®. With ZipRecruiter TrafficBoost, your jobs get prioritized on our site, in our network of 100+ job boards, and in daily candidate emails.';
	    echo '<br><br>';
            echo '<h4><a href="https://www.ziprecruiter.com/job-boards" target="_blank">ZipRecruiter one-click posting to 100+ leading job boards</a></h4>';
	    
	    } // end ZipRecruitercnt
	    
	    echo '<hr size="1">';
	    
	    if ($INcnt == 0) {
	    
	        echo '<u>Note:</u> Job posting sites reserve the right to reject requisitions pending content review. ';
	    
	        echo '</td></tr>';
	    
	        echo '<tr><td colspan="2" align="left" valign="bottom" height="30">';
	    
	        if($ServerInformationObj->getRequestSource() == 'ajax') {
	            if(is_array($free_jobboard_list) && in_array("ZipRecruiter", $free_jobboard_list)) {
	            	echo '<input class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="updateFreeJobBoardListings(\'ZipRecruiter\');" value="Update" type="button">';
	            }
	            else {
	                echo '<input type="button" class="btn btn-primary" name="btnAddToBasket" id="btnAddToBasket" onclick="addToBasketReqZipRecruiterFeed(this);" value="add to basket">';
	            }
	        }
	        else {
	            echo '<input type="submit" value="add to basket">';
	        }
	    
	        echo '</td></tr>';
	    } // end ZipRecruitercnt
	    
	    if($dis_info_req == "true") {
	        echo '<tr><td>';
	        echo '<font color="#FC1F1F">Is this position over 30 days old?  We recommend <u>ONLY</u> sponsoring brand new positions.</font>';
	        echo '</td></tr>';
	    }
	    
	    echo '</table>';
	    echo '</form>';
	}

} // end display

if($ServerInformationObj->getRequestSource() != 'ajax') {
	echo '<br><p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
}
?>
