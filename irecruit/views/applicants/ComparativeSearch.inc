<div id="page-wrapper">
    
    <div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title;?></h1>
		</div>
	</div>

    
    <div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12">
	       Requisitions: 
	       <select name="ddlRequisitions" id="ddlRequisitions" class="form-control width-auto-inline" style="max-width: 350px">
	           <option value="">Select</option>
	           <?php 
	           for($r = 0; $r < count($requisitions_list); $r++) {
	           	   ?><option value="<?php echo $requisitions_list[$r]['RequestID']?>"><?php echo $requisitions_list[$r]['Title']." - ".$requisitions_list[$r]['RequisitionID']." / ".$requisitions_list[$r]['JobID'];?></option><?php
	           }
	           ?>
	       </select>&nbsp;&nbsp;
	       <input type="radio" name="rdRequisitionStatus" id="RequisitionStatusActive" value="Active" checked="checked" onclick="getRequisitionsByStatus('Y')">&nbsp;Active
	       <input type="radio" name="rdRequisitionStatus" id="RequisitionStatusActive" value="InActive" onclick="getRequisitionsByStatus('N')">&nbsp;In-Active
	       &nbsp;&nbsp;<span id="refresh_requisitions_list"></span><br><br>
	   </div>
	</div>   
	
    <div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			Application Date From: 
			<input type="text" name="application_from_date" id="application_from_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y', strtotime("-3 months", strtotime(date('Y-m-d')))); ?>">
			Application Date To: 
			<input type="text" name="application_to_date" id="application_to_date" class="form-control width-auto-inline" value="<?php echo date('m/d/Y'); ?>">
			&nbsp;
			<input type="button" name="btnRefineApplicants" id="btnRefineApplicants" class="btn btn-primary" value="Refine Applicants" onclick="getApplicantsByRequestID()">
			<br><br>
		</div>
	</div>
	
   <?php 
   if(isset($_GET['msg']) && $_GET['msg'] == 'maxlimitreached') {
        echo '<div class="row" style="margin:0 auto"><br>';
        echo '<div class="col-lg-12 alert alert-danger">';
        echo "You cannot add more than 100 applicants to a Label";
        echo '</div>';
        echo '</div>';
   }
   else if(isset($_GET['msg']) && $_GET['msg'] == 'succ') {
        echo '<div class="row" style="margin:0 auto"><br><div class="col-lg-12 alert alert-success">';
        echo "Successfully added the applicants to the Label";
        echo '</div></div>';
   }
   ?>
	
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12">
	       <form name="frmComparativeSearchApplicants" id="frmComparativeSearchApplicants" method="post">
	               
	               <table class="table table-bordered" id="applicants_results">
                        <tr>
                            <th><input type="checkbox" onclick="check_uncheck(this.checked, 'applicants_list')"></th>
                            <th>ApplicationID</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>Entry Date</th>
                        </tr>
                        
                        <?php 
                            for($i = 0; $i < count($applications_list); $i++) {
        
                                $ProcessOrder   = $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $applications_list[$i]['ProcessOrder'] );
                                $APPDATA        = $ApplicantsObj->getAppData($OrgID, $applications_list[$i]['ApplicationID']);
                                ?>
                                <tr>
                                    <td><input type="checkbox" name="appids_list[]" id="app_req_id_<?php echo $applications_list[$i]['ApplicationID'].":".$applications_list[$i]['RequestID']?>" class="applicants_list" value="<?php echo $applications_list[$i]['ApplicationID'].":".$applications_list[$i]['RequestID']?>"></td>
                                    <td><a href="<?php echo IRECRUIT_HOME?>applicantsSearch.php?ApplicationID=<?php echo $applications_list[$i]['ApplicationID'];?>&RequestID=<?php echo $applications_list[$i]['RequestID'];?>" target="_blank"><?php echo $applications_list[$i]['ApplicationID'];?></a></td>
                                    <td><a href="mailto:<?php echo $APPDATA['email']?>" target="_blank"><?php echo $applications_list[$i]['ApplicantSortName'];?></a></td>
                                    <td><?php echo $ProcessOrder;?></td>
                                    <td><?php echo $APPDATA['email'];?></td>
                                    <td><?php echo date('m/d/Y H:i:s', strtotime($applications_list[$i]['EntryDate']));?></td>
                                </tr>	
                            	<?php
                            }
                        ?>
        	       </table>
        	       
	               <input type="hidden" name="LabelID" id="LabelID">
	               
	       </form>
	       
	   </div>
	</div>   
	
	<div class="row">
	   <div class="col-lg-6 col-md-6 col-sm-6">
	       <?php 
	       if(count($comp_lbls_list) > 0) {
	       	   ?>
	       	   <form name="frmTagApplicants" id="frmTagApplicants" method="post">
                    <select name="ddlTagApplicants" id="ddlTagApplicants" class="form-control width-auto-inline">
    	               <option value="">Select</option>
    	               <?php
    			         for($c = 0; $c < count($comp_lbls_list); $c++) {
    			         	?><option value="<?php echo $comp_lbls_list[$c]['LabelID'];?>"><?php echo $comp_lbls_list[$c]['LabelName'];?></option><?php
    			         }
    			       ?>
    	            </select>
    	            <input type="button" class="btn btn-primary" name="btnTagApplicants" id="btnTagApplicants" value="Tag Applicants" onclick="processCompSearchApplicants(document.frmTagApplicants.ddlTagApplicants.value);">
    	       </form>
	       	   <?php
	       }
	       ?>
	   </div>
	   <div class="col-lg-6 col-md-6 col-sm-6" style="text-align: right">
	       <input type="button" class="btn btn-primary" name="btnCompareSelectedApplicants" id="btnCompareSelectedApplicants" value="Compare Selected Applicants" onclick="compareSelectedApplicants();">
	   </div>
	</div>
	
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12"><br><br></div>
	</div>
</div>

<script>
function compareSelectedApplicants() {
	var frmobj = document.frmComparativeSearchApplicants;
	frmobj.action = 'comparativeAnalysis.php?menu=10';
	frmobj.submit();
}
</script>