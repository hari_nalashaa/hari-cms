<style>
.tooltip {
	background: none !important;
	margin-left: 10px !important;
}
</style>
<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<div class="row">
		<div class="col-lg-12">
			<?php include_once IRECRUIT_VIEWS . 'applicants/ApplicantsTopRightNavigation.inc';?>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel-default">
					<?php include_once IRECRUIT_VIEWS . 'applicants/ApplicantsPageContent.inc';?>
					<!-- /.row (nested) -->
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>