<?php
$formtable      =   "FormQuestions";
$colwidth       =   "220";

$APPDATA        =   G::Obj('Applicants')->getAppData ( $OrgID, $ApplicationID );
$applicant_info =   "";

if ($_REQUEST['process'] != "Y") {
	echo $applicant_info;
}

// Get JobApplications and FormQuestions Information
$columns    =   "FQ.value, JA.RequestID, JA.FormID";
$where      =   array (
            		"JA.OrgID             =   FQ.OrgID",
            		"JA.FormID            =   FQ.FormID",
            		"JA.OrgID             =   :OrgID",
            		"JA.ApplicationID     =   :ApplicationID",
            		"FQ.SectionID         =   6",
            		"FQ.QuestionTypeID    =   8",
            		"QuestionID           =   :QuestionID" 
                );
$params     =   array (
            		":OrgID"              =>  $OrgID,
            		":ApplicationID"      =>  $ApplicationID,
            		":QuestionID"         =>  $Type 
                );
$results    =   G::Obj('Applications')->getJobApplicationsAndFormQuestions ( $columns, $where, '', array ($params) );

if (is_array ( $results ['results'] )) {
	foreach ( $results ['results'] as $REQ ) {
		$RequestID        =   $REQ ['RequestID'];
		$FormID           =   $REQ ['FormID'];
		$AttachmentName   =   $REQ ['value'];
		
		if($AttachmentName == "") $AttachmentName = "File";
	}
}

if ($_REQUEST['process'] == "Y") {
    
    // Upload Attachments
    require_once IRECRUIT_DIR . 'applicants/ProcessApplicationAttachmentsTemp.inc';
    
	if ($_FILES [$Type] ['type'] != "") {
		
		if ($edit == "Y") {
			
            $apatdir    =   IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments';
            
            // Set where condition
            $where      =   array (
                				"OrgID             =   :OrgID",
                				"ApplicationID     =   :ApplicationID",
                				"TypeAttachment    =   :TypeAttachment" 
            			     );
            // Set parameters
            $params     =   array (
                				":OrgID"           =>  $OrgID,
                				":ApplicationID"   =>  $ApplicationID,
                				":TypeAttachment"  =>  $Type 
                            );
            // Get Applicant Attachments
            $results    =   G::Obj('Attachments')->getApplicantAttachments ( "*", $where, '', array ($params) );
            $AA         =   $results ['results'] [0];
			
            $filename   =   $apatdir . '/' . $ApplicationID . '-' . $AA ['PurposeName'] . '.' . $AA ['FileType'];
				
			if(file_exists($filename)) {
			    @unlink($filename);
				// Delete Applicant Attachments
				G::Obj('Attachments')->delApplicantAttachments ( $OrgID, $ApplicationID, $Type );
			}
		}
		
		include COMMON_DIR . 'process/FileHandling.inc';
		
		// Insert into ApplicantHistory
		$editaction   =   array (
            				'Y'     => 'Updated',
            				'New'   => 'Added' 
                		  );
		$Comments     =   'Attachment ' . $editaction [$edit] . ': ' . $AttachmentName . "<br>";
		
		if ($edit == "Y") {
			$Comments .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
			$Comments .= '<a href="' . IRECRUIT_HOME . 'applicants/display_attachment_History.php?OrgID=' . $OrgID . '&File=' . $filenameB . '">Download</a> previous version.';
		}
		
		// update all applications that this has been changed//Set where condition
		$where    =   array ("OrgID = :OrgID", "ApplicationID = :ApplicationID");
		// Set parameters
		$params   =   array (":OrgID" => $OrgID, ":ApplicationID" => $ApplicationID);
		// Get RequestID's list
		$results_req_ids = G::Obj('Applications')->getJobApplicationsInfo ( 'RequestID', $where, '', '', array ($params) );
		
		if (is_array ( $results_req_ids ['results'] )) {
			foreach ( $results_req_ids ['results'] as $row ) {
				// Job Application History Information
				$job_app_history = array (
						"OrgID"               =>  $OrgID,
						"ApplicationID"       =>  $ApplicationID,
						"RequestID"           =>  $row ['RequestID'],
						"ProcessOrder"        =>  "-3",
						"StatusEffectiveDate" =>  "DATE(NOW())",
						"Date"                =>  "NOW()",
						"UserID"              =>  $USERID,
						"Comments"            =>  $Comments 
				);
				// Insert Job Application History Information
				G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
			} // end foreach
		}
		
		echo json_encode(array("applicant_info"=>$applicant_info, 'message'=>'<br><br><center>You have successfully updated the ' . $AttachmentName . '.</center><br><br>', 'edit_type'=>$edit, 'status'=>'success'));
	} else { // no file
		echo json_encode(array("applicant_info"=>$applicant_info, 'message'=>'<br><br><center><font style="color:red">Error: No attachement was selected. Please try again.</font><br><br>', 'edit_type'=>$edit, 'status'=>'failure'));
	} // end no file check
} else { // end process

	echo '<table border="0" cellspacing="0" cellpadding="0" width="100%">';
	echo '<tr><td>';
	
	if ($edit == "New") {
		echo $AttachmentName . ":<br>";
		$submit = "Add a " . $AttachmentName;
	}
	
	if ($edit == "Y") {
		echo $AttachmentName . ":<br>";
		$submit = "Submit Replacement " . $AttachmentName;
	}
	
	echo '<form action="editAttachments.php" name="frmEditAttachments" id="frmEditAttachments" method="post" enctype="multipart/form-data">';
	
	if (($OrgID == "I20130812") || ($OrgID == "xB12345467")) {
		$max_file_size = "10485760";
	} else {
		$max_file_size = "1048576";
	}
	
	echo '<input type="hidden" name="MAX_FILE_SIZE" value="' . $max_file_size . '">';
	
	$QuestionID    =   $Type;
	$FileUpload    =   include COMMON_DIR . 'application/DisplayQuestions.inc';
	
	if ($FileUpload) {
		echo $FileUpload;
	}
	
	echo '<br><center>';
	echo '<input type="hidden" name="ApplicationID" value="' . $ApplicationID . '">';
	echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
	echo '<input type="hidden" name="Type" value="' . $Type . '">';
	echo '<input type="hidden" name="edit" value="' . $edit . '">';
	echo '<input type="hidden" name="action" value="' . $action . '">';
	echo '<input type="hidden" name="process" value="Y">';
	echo '<input type="button" value="' . $submit . '" onclick="processAttachments();">';
	echo '<span id="msg_process_attachments_info"></span>';
	echo '</center>';
	echo '</form>';
	
	echo '<br><br>';
	
	echo '</td></tr>';
	echo '</table>';
} // end if process
?>