<style>
#question_space {
     padding: 0px 0px 3px 0px;
}
@media(max-width:768px) {
	 #question_space {
         padding: 0px 0px 3px 0px;
         border-bottom: 1px solid #f5f5f5 !important;
	}
}
@media print {
    .col-sm-3 {
        width: 35%;
        padding: 0px;
    }
    .col-sm-9 {
        width: 65%;
    }
    .col-sm-12 {
        width: 70%;
        padding: 0px;
    }
    #question_space {
        padding: 0px;
    }
}
</style>
<?php
if($ServerInformationObj->getRequestSource() != 'ajax') {
	?>
	<style>
    .row {
        margin-right: 0px !important;
        margin-left: 0px !important;
    }
    </style>
	<?php
}

if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];

if (($_REQUEST['ApplicationID']) && isset($OrgID) && ($_REQUEST['RequestID'])) {
    
    $ApplicationID  =   $_REQUEST['ApplicationID'];
    $RequestID      =   $_REQUEST['RequestID'];
    
    // get applicant data
    $APPDATA        =   G::Obj('Applicants')->getAppData ($OrgID, $_REQUEST['ApplicationID'] );
    
    $ApplicantInfo  =   G::Obj('Applications')->getJobApplicationsDetailInfo("FormID", $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
    
    //Get MultiOrgID based on OrgID, RequestID
    $MultiOrgID     =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
    
    $applicationsize    =   count ( $APPDATA );
	
	// validate the FormID
	$FormID            =   $ApplicantInfo['FormID'];
	
	//Set default applicant data audit log information
	$audit_app_log     =   array(
                            	'OrgID'            =>  $OrgID,
                            	'UserID'           =>  $USERID,
                            	'Action'           =>  'View',
                            	'FormType'         =>  'ApplicationForm',
                            	'FormID'           =>  $FormID,
                            	'PageInfo'         =>  $_SERVER['REQUEST_URI'],
                            	'CreatedDateTime'  =>  "NOW()"
                            );
	
	//Bind parameters
	$params        =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where         =   array("OrgID = :OrgID", "FormID = :FormID");
	//Get Distinct FormID's count
	$results       =   G::Obj('FormQuestions')->getFormQuestionsInformation(array("DISTINCT(FormID) AS DFormID"), $where, "", array($params));
	$hit           =   $results['count'];
	
	if ($hit < 1) {
		$FormID = 'STANDARD';
	}
	
	$form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
	
	foreach ( $form_sections_info as $form_section_id=>$form_section_info) {
	   $TITLES [$form_section_id] = $form_section_info['SectionTitle'];
    } // end foreach
	    
	//Titles for Affirmative Action, Veteran, Disabled Forms
    $TITLES ["AA"]  =   "Affirmative Action";
    $TITLES ["VET"] =   "Veteran";
    $TITLES ["DIS"] =   "Disabled";
	
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where     =   array("OrgID = :OrgID", "FormID = :FormID");
	// Query and Set Text Elements
	$results   =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
			$TextBlocks [$row ['TextBlockID']] = $row ['Text'];
		}	
	}
	
	
	if ($applicationsize < 30) {
		if ($OrgID == 'I20100603') {
			echo '<center><b>Applicant MUST complete an application at the time of interview</b></center><br>';
		}
	}
	
	// Read and Set Logos
	if (! $emailonly) {		
        //Bind parameters
        $params     =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>(string)$MultiOrgID);
        //Set condition
        $where      =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
        //Get Organization Logos Information
        $resultsOL  =   G::Obj('Organizations')->getOrganizationLogosInformation(array("OrgID"), $where, "", array($params));
        $imgcnt     =   $resultsOL['count'];
		
		if ($imgcnt > 0) {
			echo '<img src="' . IRECRUIT_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br>';
		} // end imgcnt
	} // end email only

    //Bind parameters
    $params     =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':ApplicationID'=>$_REQUEST['ApplicationID']);
    //Set condition
    $where      =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "ApplicationID = :ApplicationID");
    //Get JobApplications Information
    $results    =   G::Obj('Applications')->getJobApplicationsInfo(array("RequestID", "Distinction", "EntryDate"), $where, '', 'Distinction', array($params));
		
	if(is_array($results['results'])) {
		foreach ($results['results'] as $JA) {
		    $multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $JA['RequestID']);
		    
			$ED = $JA ['EntryDate'];
			if ($JA ['Distinction'] == 'P') {
				$TT = 'Position Desired';
			} else {
				$TT = 'Other Position Desired';
			}

			echo '';
				
			echo "<div class='row'>";
			echo '<div class="col-lg-3 col-md-4 col-sm-3">'.$TT . ':</div>';
			echo '<div class="col-lg-9 col-md-8 col-sm-9">';
			echo '<b>(' . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . ')</b>';
			echo '<b> ' . G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . '</b>';
			echo '</div>';
			echo '</div>';
		}
	}
	
	echo '<div class="row">';
	echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	echo '</div>';
	
	
	echo '<div class="row">';
	echo '<div class="col-lg-3 col-md-4 col-sm-3">Application ID:</div>';
	echo '<div class="col-lg-9 col-md-8 col-sm-9"><b>' . $_REQUEST['ApplicationID'] . '</b></div>';
	echo '</div>';

	echo '<div class="row">';
	echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	echo '</div>';
	
	echo '<div class="row">';
	echo '<div class="col-lg-3 col-md-4 col-sm-3">Application Date:</div>';
	echo '<div class="col-lg-9 col-md-8 col-sm-9"><b>' . $ED . '</b></div>';
	echo '</div>';

	echo '<div class="row">';
	echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";  
	echo '</div>';
	
	echo '<br>';
	
	echo '<div class="row">';
	if ($TITLES [1] != "") {
	    echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	    echo '<strong>' . $TITLES [1] . '</strong>';
	    echo '</div>';
	}
	echo '</div>';

	$AppQueAnswers             =   G::Obj('ApplicationFormQueAns')->getApplicationViewInfo($OrgID, $ApplicationID, $RequestID, $APPDATA);
	$personal_info_ques        =   array("name", "address", "address2", "faddress", "country", "email");
	$personal_info_excludes    =   array("first", "last", "middle", "county", "city", "state", "province", "name", "address", "address2", "faddress", "country", "email", "zip");
	
	foreach($personal_info_ques as $personal_que) {
	    $personal_que_answer   =   $AppQueAnswers[1][0][$personal_que]['Answer'];
	    
	    if($personal_que_answer != "") {
	        echo '<div class="row">';
	        echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	        if($personal_que == "email") {
	            if($permit ['Applicants_Contact'] >= 1) {
	                echo 'Email Address: <a href="mailto:' . $personal_que_answer . '">' . $personal_que_answer . '</a>';
	            }
	            else {
	                echo 'Email Address: ' . $personal_que_answer;
	            }
	        }
	        else {
	            echo $personal_que_answer;	             
	        }
	        echo '</div>';
	        echo '</div>';
	    }
	}
	
	echo "<br>";	
	
	//Bind parameters
	$params    =   array(':OrgID'=>$OrgID, ':HoldID'=>$_REQUEST['ApplicationID'], ':RequestID'=>$_REQUEST['RequestID']);
	//set columns
	$columns   =   array("Question", "Submission");
	//set condition
	$where     =   array("OrgID = :OrgID", "HoldID = :HoldID", "RequestID = :RequestID");
	//Get Prescreen Results
	$resultsIN =   G::Obj('PrescreenQuestions')->getPrescreenResults($columns, $where, "SortOrder", array($params));
	// Prescreen Questions
	$hitIN     =   $resultsIN['count'];
	
	if ($hitIN > 0) {
	    echo "<div class='row'>";
	    echo "<div class='col-lg-12 col-md-12 col-sm-12'>";
	    echo "<br><strong>Prescreen Questions</strong>";
	    echo "</div>";
	    echo "</div>";	     
	
	    //Display Prescreen results information
	    if(is_array($resultsIN['results'])) {
	        foreach ($resultsIN['results'] as $PRESCREEN) {
	            echo "<div class='row'>";
	            echo "<div class='col-lg-12 col-md-12 col-sm-12'>";
	            echo $PRESCREEN ['Question'] . "?&nbsp;&nbsp;";
	            echo "<b>" . $PRESCREEN ['Submission'] . "</b>";
	            echo "</div>";
	            echo "</div>";
	        } // end foreach
	    }
	
	} // end hit Prescreen
	
	echo "<br>";
	
	foreach ($AppQueAnswers as $SectionID=>$QA)
	{
	    if($form_sections_info[$SectionID]['ViewPrintStatus'] == "Y") {
	        if($SectionID != "1") {
	            echo "<div class='row'>";
	            echo "<div class='col-lg-12 col-md-12 col-sm-12'>";
	            echo "<br><strong>".$TITLES[$SectionID]."</strong>";
	            echo "</div>";
	            echo "</div>";
	        }
	        
	        $SubSecCount = count($AppQueAnswers[$SectionID]);
	        
	        for($subseci = 0; $subseci < $SubSecCount; $subseci++) {
	            
	            $que_section = 0;
	            foreach ($QA[$subseci] as $QuestionID=>$QuestionAnswerInfo) {
	                
	                if(!in_array($QuestionID, $personal_info_excludes)) {
	                    if($que_section == 0) {
	                        echo "<div class='row'>";
	                        
	                        if($subseci == 0) {
	                            echo "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['SubSecInstruction']."</div>";
	                        }
	                        
	                        echo "<div class='col-lg-12 col-md-12 col-sm-12'><u>".$QuestionAnswerInfo['SubSecTitle']."</u></div>";
	                        echo "</div>";
	                    }
	                    
	                    $que_section++;
	                    
	                    echo "<div class='row' style='margin-bottom:1px;'>";
	                    
	                    if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
	                        
	                        echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                        
	                        $canswer = $QuestionAnswerInfo['Answer'];
	                        
	                        $rtn  = '';
	                        if(is_array($canswer)) {
	                            $rtn .= '<table border="0" cellspacing="3" cellpadding="3">';
	                            
	                            foreach ($canswer as $cakey=>$caval) {
	                                $rtn .= '<tr>';
	                                $rtn .= '<td style="padding-right:5px !important">'.$cakey.'</td>';
	                                foreach ($caval as $cavk=>$cavv) {
	                                    $rtn .= '<td style="padding-right:5px !important">'.$cavv.'</td>';
	                                }
	                                $rtn .= '</tr>';
	                            }
	                            
	                            $rtn .= '</table>';
	                        }
	                        
	                        echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$rtn."</strong></div>";
	                        
	                    }
	                    else if($QuestionAnswerInfo['QuestionTypeID'] == 9) {
	                        
	                        echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                        echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
	                        
	                    } else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
	                        
	                        echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                        
	                        $canswer = $QuestionAnswerInfo['Answer'];
	                        
	                        $rtn  = '';
	                        if(is_array($canswer)) {
	                            
	                            $rtn .= '<table border="0" cellspacing="3" cellpadding="0">';
	                            
	                            $days_count = count($canswer['days']);
	                            
	                            for($ci = 0; $ci < $days_count; $ci++)  {
	                                $rtn .= '<tr>';
	                                $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
	                                $rtn .= '<td width="5%">from&nbsp;&nbsp;</td>';
	                                $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
	                                $rtn .= '<td width="5%">to </td>';
	                                $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
	                                $rtn .= '</tr>';
	                            }
	                            
	                            $rtn .= '</table>';
	                        }
	                        
	                        echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$rtn."</strong></div>";
	                    }
	                    else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
	                        echo "<div class='col-lg-12 col-md-12 col-sm-12'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                    }
	                    else {
	                        echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                        echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
	                    }
	                    
	                    echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";
	                    echo "</div>";
	                }
	            }
	        }
	    }
	}
	
	
	if ($TextBlocks ["ApplicantAuthorizations"]) {
	    $ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
	} else {
	    $ApplicantAuthorizations = "";
	}
	
	$Signature = '' . $TextBlocks ["Signature"];
	
	if ($OrgID == "I20100301") { // Meritan
	    $Signature .= ' I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.';
	} else {
	    $Signature .= ' I agree with the above statement.';
	}
	$Signature .= '<br><br>';
	
	if($APPDATA ['signature'] != "") {
	    $Signature .= 'Electronic Signature: <b>' . $APPDATA ['signature'] . '</b>';
	}
	else {
	    $Signature .= 'Electronic Signature: <font style="font-color:red">X</font>';
	}
	
	if (($TextBlocks ["Signature"]) || ($ApplicantAuthorizations)) {
	    echo '<table border="0" cellspacing="3" cellpadding="0" class="table">';
	    echo '<tr><td><b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '</td></tr>';
	    echo '<tr><td>';
	    echo $Signature;
	    echo '<td></tr>';
	    echo '</table>';
	    echo '<br>';
	}

	if (($OrgID == "B12345467xx") || ($OrgID == "I20130510")) {
	    echo '<img src="' . IRECRUIT_HOME . 'images/hiring_managers_only.jpg' . '" style="width:786px;">';
	}
	
} // end if ApplicationID, OrgID, RequestID
?>
