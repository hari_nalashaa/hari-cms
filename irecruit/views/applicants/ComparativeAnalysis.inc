<div id="page-wrapper">

	<div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title;?></h1>
		</div>
	</div>

	<div class="row">
	    <div class="col-lg-6" style="text-align: left">
	       <form name="frmExportToExcel" id="frmExportToExcel" method="post" action="comparativeAnalysisExport.php">
	           <?php 
	               for($etei = 0; $etei < count($applicants_list); $etei++) {
	                   ?><input type="hidden" name="export_appids_list[]" class="cls_export_appids_list" value="<?php echo $applicants_list[$etei]['ApplicationID'] . ":" .$applicants_list[$etei]['RequestID'];?>"><?php
	               }
	           ?>
	           <input type="button" name="btnExportToExcel" id="btnExportToExcel" class="btn btn-primary" value="Export to Excel">
	       </form>
	    </div>
		<div class="col-lg-6" style="text-align: right">
		    <?php 
		    if(count($comp_lbls_list) > 0) {
                ?>
                <form name="frmLabelsList" id="frmLabelsList" method="post">
    			<select name="ddlComparativeLabels" id="ddlComparativeLabels" class="form-control width-auto-inline">
    			     <option value="">Select Tag</option>
    			     <?php
    			         for($c = 0; $c < count($comp_lbls_list); $c++) {
                            if(isset($_REQUEST['label_id']) && $_REQUEST['label_id'] == $comp_lbls_list[$c]['LabelID'])
                            {
                                $selected_label = 'selected="selected"';
                            }    
    			         	?><option value="<?php echo $comp_lbls_list[$c]['LabelID'];?>" <?php echo $selected_label;?>><?php echo $comp_lbls_list[$c]['LabelName'];?></option><?php
    			         	unset($selected_label);
    			         }
    			     ?>
    			</select>
    			<input type="button" class="btn btn-primary" name="btnCompare" id="btnCompare" value="Compare" onclick="forwardToComparativeAnalysis()">
    			</form>
                <?php
            }
		    ?>
		</div>
	</div><br>
	
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12 table-responsive">
	       <form name="frmComparativeAnalysis" id="frmComparativeAnalysis" method="post">
                     	           
	       <table class="table table-bordered" id="data-comparative-analysis">
	            <thead>
                <tr>
                    <th valign='top'>Application ID</th>
                    <th valign='top'>Applicant Name</th>
                    
                    <?php                     
                        if($feature['InternalRequisitions'] == "Y") {
                            echo '<th valign="top">Internal Application</th>';    
                        }

                        if(is_array($config_info['JobApplicationsColumnsList'])) {
                        	foreach ($config_info['JobApplicationsColumnsList'] as $column_id) {
                                echo "<th valign='top'>".$config_columns_list[$column_id]."</th>";
                            }
                        }
                          
                        if(is_array($config_info['ComparativeAnalysisQuestionColumnsList'])) {
                            foreach ($config_info['ComparativeAnalysisQuestionColumnsList'] as $app_summary_que_id) {
                               echo '<th valign="top">'.$app_sum_que_list[$app_summary_que_id].'</td>';
                            }
                        }
                         
                        if(is_array($config_info['WeightedSearchColumnsList'])) { 
                            foreach ($config_info['WeightedSearchColumnsList'] as $weighted_search_id) { 
								if($weighted_search_id == 'wotc_application_status') {
									echo "<th valign='top'>CMS WOTC Application Status</th>";
								}
								else {
									$weighted_search_name = $weight_search_names[$weighted_search_id];
									echo "<th valign='top'>".$weighted_search_name."</th>";
								}
                            }
                        }
                        
                        if(isset($config_info['WeightedSearchColumnsListSum'])
                            && $config_info['WeightedSearchColumnsListSum'] == "cumulative") {
                                echo "<th valign='top'>Total</th>";
                        }
                        
                        if(isset($config_info['ApplicantDistance'])
                            && $config_info['ApplicantDistance'] == "yes") {
                            echo "<th valign='top'>Applicant Distance</th>";
                        }
                    ?>
                </tr>
                </thead>
                <tbody>
                <?php 
                $where_info = array("OrgID = :OrgID", "UserID = :UserID");
                $params_info = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
                $weighted_searches_results = $WeightedSearchObj->getWeightSaveSearchInfo("FormID, SaveSearchName, WeightedSearchRandId, SectionID", $where_info, "WeightedSearchRandId", "", array($params_info));
                $weighted_searches_info = $weighted_searches_results['results'];
                
                $ws_frm_types = array();
                $ws_sections = array();
                for($wsi = 0; $wsi < count($weighted_searches_info); $wsi++) {
                    $ws_frm_types[$weighted_searches_info[$wsi]['WeightedSearchRandId']] = $weighted_searches_info[$wsi]['FormID'];
                    $ws_sections[$weighted_searches_info[$wsi]['WeightedSearchRandId']] = $weighted_searches_info[$wsi]['SectionID'];
                }
                
                for($i = 0; $i < count($applicants_list); $i++) {
                    $ApplicationID                  =   $applicants_list[$i]['ApplicationID'];
                    $ApplicantName                  =   $applicants_list[$i]['ApplicantSortName'];
                    $RequestID                      =   $applicants_list[$i]['RequestID'];
                    $ProcessOrder                   =   $applicants_list[$i]['ProcessOrder'];
                    $ApplicantConsiderationStatus1  =   $applicants_list[$i]['ApplicantConsiderationStatus1'];
                    $ApplicantConsiderationStatus2  =   $applicants_list[$i]['ApplicantConsiderationStatus2'];
                    $ApplicantEmail                 =   $applicants_list[$i]['Email'];
                    $InternalApplication            =   $applicants_list[$i]['IsInternalApplication'];
                    $DispositionCode                =   $disposition_codes[$applicants_list[$i]['DispositionCode']];
                    
                    $ApplicantConsiderationLabels   =	json_decode($organization_info['ApplicantConsiderationLabels'], true);
                    $app_req_id						=	$ApplicationID . ':' . $RequestID;
                    $multiorgid_req                 =	$RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
                    
                    echo '<tr>';
                    
                    echo '<td>';
                    echo '<a href="'.IRECRUIT_HOME.'applicantsSearch.php?ApplicationID='.$ApplicationID.'&RequestID='.$RequestID.'" target="_blank">'.$ApplicationID.'</a>';
                    echo '</td>';
                    
                    echo '<td>';
                    echo '<a href="mailto:'.$ApplicantEmail.'" target="_blank">'.$ApplicantName.'</a>';
                    echo '</td>';
                    
                    if($feature['InternalRequisitions'] == "Y") {
                        echo '<td>';
                        echo $InternalApplication;
                        echo '</td>';
                    }
                    
                    if(is_array($config_info['JobApplicationsColumnsList'])) {
                    	foreach ($config_info['JobApplicationsColumnsList'] as $column_id) {
                            if($column_id == "ProcessOrder") {
                                $status = $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $ProcessOrder );
                                echo '<td>'.$status.'</td>';
                            }
                            else if($column_id == "DispositionCode") {
                                echo '<td>'.$DispositionCode.'</td>';
                            }
                            else if($column_id == "RequestID") {
                                $job_title = $RequisitionDetailsObj->getJobTitle($OrgID, $multiorgid_req, $RequestID);
                                echo '<td>'.$job_title.'</td>';
                            }
                            else if($column_id == "ApplicantConsiderationStatus1") {
                                if($ApplicantConsiderationStatus1 == "ThumbsUp") {
                                    echo '<td><i class="fa fa-thumbs-up fa-2x" aria-hidden="true" style="color:'.$user_preferences['TableRowHeader'].'"></i></td>';
                                }
                                else if($ApplicantConsiderationStatus1 == "ThumbsDown") {
                                    echo '<td><i class="fa fa-thumbs-down fa-2x" aria-hidden="true" style="color:'.$user_preferences['TableRowHeader'].'"></i></td>';
                                }
                                else {
                                    echo '<td>&nbsp;</td>';
                                }
                            }
                            else if($column_id == "ApplicantConsiderationStatus2") {
                                $applicant_label = $ApplicantConsiderationLabels[$ApplicantConsiderationStatus2];
                                echo '<td>'.$applicant_label.'</td>';
                            }
                            else if($column_id == "Rating") {
                                $rating_value = $applicants_list[$i][$column_id];

                                $star_span = '';
                                for($star_index = 1; $star_index <= 5; $star_index++) {
                                    if($star_index <= $rating_value) {
                                        $stars_class = 'style="color:'.$user_preferences['TableRowHeader'].'"';	
                                    }
                                    else {
                                        $stars_class = 'style="color:#c8cbd1;"';	
                                    }
                                    
                                    $star_span .= '<span class="fa fa-star fa" aria-hidden="true" '.$stars_class.'></span>';
                                }
                                
                            	echo '<td nowrap="nowrap">'.$star_span;
                            	echo '<span style="color:white;">'.$rating_value.'</span>';
                            	echo '</td>';
                            }
                            else {
                                echo '<td>'.$applicants_list[$i][$column_id].'</td>';
                            }
                        }
                    }
                    
                    if(is_array($config_info['ComparativeAnalysisQuestionColumnsList'])) {
                        foreach ($config_info['ComparativeAnalysisQuestionColumnsList'] as $app_summary_que_id) {
                            $app_sum_que_ans = $ComparativeAnalysisObj->getApplicantQuestionsSummary($OrgID, $ApplicationID, $app_summary_que_id, $app_sum_que_info[$app_summary_que_id]['QuestionTypeID'], $app_sum_que_list[$app_summary_que_id], $app_sum_que_info[$app_summary_que_id]['SectionID']);
                            echo '<td valign="top" align="left">'.$app_sum_que_ans.'</td>';
                        }
                    }
                    
                    $total_weighted_search_score = 0;
                    if(is_array($config_info['WeightedSearchColumnsList'])) {
                        foreach ($config_info['WeightedSearchColumnsList'] as $weighted_search_id) {
                            $weighted_search_name = $weight_search_names[$weighted_search_id];
                            
                            if($weighted_search_id == "wotc_application_status") {

								//Get Wotc Ids list
								$wotcid_info			=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $multiorgid_req);
								//Get Wotc Application Header Information
								$wotc_app_header_info	=	G::Obj('WOTCIrecruitApplications')->getWotcApplicationInfo("*", $wotcid_info['wotcID'], $ApplicationID, $RequestID);
								//Get Wotc ApplicationID
								$WotcApplicationID		=	$wotc_app_header_info['ApplicationID'];
								$WotcFormID				=	$wotc_app_header_info['WotcFormID'];
								$Qualifies				=	$wotc_app_header_info['Qualifies'];
								
								$WotcApplicationQualify	=	"";
								
								if(isset($wotcid_info['wotcID']) && $wotcid_info['wotcID']) {
									$WotcApplicationQualify	= "Application Pending";
								}
								
								if($Qualifies == "Y")
								{
									$WotcApplicationQualify = "Potentially Qualified";
								}
								if($Qualifies == "N")
								{
									$WotcApplicationQualify = "Not Qualified";
								}

                            	echo '<td valign="top" align="left">';
                            	echo $WotcApplicationQualify;
                            	echo '</td>';
                            }
                            else if($ws_frm_types[$weighted_search_id] == "WebForms") {
                            	$applicant_score = $WeightedSearchObj->getScoreOfApplicantForWebForm($OrgID, $ApplicationID, $ws_sections[$weighted_search_id], $weighted_search_id);
                            	$total_weighted_search_score += $applicant_score;
                            	echo '<td valign="top" align="left"><a href=\'javascript:void(0)\' onclick=\'javascript:window.open("'.IRECRUIT_HOME.'applicants/weightedSearchWebFormScoreBreakUp.php?WSList='.$weighted_search_id.'&AppId='.$ApplicationID.'&OrgID='.$OrgID.'","_blank","location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes")\'>'.$applicant_score.'</a></td>';
                            }
                            else {
                            	$applicant_score = $WeightedSearchObj->getWeightedScoreOfApplicant($OrgID, $ApplicationID, $weighted_search_id);
                            	$total_weighted_search_score += $applicant_score;
                            	echo '<td valign="top" align="left"><a href=\'javascript:void(0)\' onclick=\'javascript:window.open("'.IRECRUIT_HOME.'applicants/weightedSearchScoreBreakUp.php?WSList='.$weighted_search_id.'&AppId='.$ApplicationID.'&OrgID='.$OrgID.'","_blank","location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes")\'>'.$applicant_score.'</a></td>';
                            }
                        }
                    }
                    
                    if(isset($config_info['WeightedSearchColumnsListSum'])
                        && $config_info['WeightedSearchColumnsListSum'] == "cumulative") {
                        echo "<th valign='top'>".$total_weighted_search_score."</th>";
                    }

                    if(isset($config_info['ApplicantDistance'])
                        && $config_info['ApplicantDistance'] == "yes") {

                        $applicant_distance = $ComparativeAnalysisObj->getApplicantDistance($OrgID, $multiorgid_req, $RequestID, $ApplicationID);
                        $applicant_distance = sprintf("%01.1f", str_replace(",", "", $applicant_distance));
                        
                        echo "<th valign='top'>".$applicant_distance."</th>";
                    }
                    
                    echo '</tr>';
                }
                ?>
                </tbody>
	       </table>
	       
	       </form>
	   </div>
	</div>   
	
</div>
<script>
$(document).ready(function() {
    $('#data-comparative-analysis').DataTable({
        responsive: true,
        aLengthMenu: [
          [50, 100, -1],
          [50, 100, "All"]
        ]
    });
});

function getUrlParameter(urlString, sParam) {
	var vars = [], hash;
    var sPageURL = decodeURIComponent(urlString);
	
    var hashes = sPageURL.slice(sPageURL.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    
    return vars[sParam];
}

$("#btnExportToExcel").click(function() {
	
    $( ".cls_export_appids_list" ).remove();
	$('#data-comparative-analysis tr').each(function() {
	    var ahref_app_id = $(this).find("td:first").html();

	    if(typeof(ahref_app_id) != 'undefined') {
	    	var urlString = $(ahref_app_id).attr("href");  

	    	var ApplicationID = getUrlParameter(urlString, 'ApplicationID');
	    	var RequestID = getUrlParameter(urlString, 'RequestID');
	        var export_app_reqid = ApplicationID + ":" + RequestID;  
	       
	    	$( "#frmExportToExcel").append ('<input type="hidden" name="export_appids_list[]" class="cls_export_appids_list" value="'+export_app_reqid+'">');

	    }
	});

	document.forms['frmExportToExcel'].submit();
	
});

function forwardToComparativeAnalysis() {
	var comparative_label_id  = document.getElementById('ddlComparativeLabels').value;

	location.href = irecruit_home + 'applicants/comparativeAnalysis.php?label_id='+comparative_label_id+'&menu=10';
}
</script>
