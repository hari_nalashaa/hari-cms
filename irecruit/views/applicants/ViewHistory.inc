<p>
<b>Applicant History</b>
</p>
<?php
if (($_REQUEST['OrgID']) && ($_REQUEST['item'])) {

	//Set where condition
	$where		=	array("OrgID = :OrgID", "UpdateID = :UpdateID");
	//Set parameters
	$params		=	array(":OrgID"=>$_REQUEST['OrgID'], ":UpdateID"=>$_REQUEST['item']);
	//Set columns
	$columns	=	"ProcessOrder, date_format(Date,'%M %d, %Y; %H:%i EST') as JAHDate, UserID, Comments, ApplicationID, RequestID, DispositionCode";
	//Get JobApplicationHistory Information
	$results	=	G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo($columns, $where, '', '', array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
		
			$ProcessOrder    =   $ApplicantDetailsObj->getProcessOrderDescription ( $_REQUEST['OrgID'], $row ['ProcessOrder'] );
			$Date            =   $row ['JAHDate'];
		
			if (is_numeric ( $row ['UserID'] )) {
		
				$UserID = "PORTAL:<br>";
		
				//Get User, Email From Users
				$resultsUP = $UserPortalUsersObj->getUserDetailInfoByUserID("FirstName, LastName, Email", $row ['UserID']);
				if(is_array($resultsUP)) {
				    $User    =   $resultsUP['LastName'] . ", " . $resultsUP['FirstName'];
				    $Em      =   $resultsUP['Email'];
				}
				
				$UserID .= $User . "<br>";
		
				if ($permit ['Applicants_Contact'] == 1) { // if access to view email
					$UserID .= '<a href="mailto:' . $Em . '">' . $Em . '</a>';
				} else {
					$UserID .= $Em;
				}
			} else {
		
				$UserID = $row ['UserID'];
			} // end is_numeric
		
			$ApplicationID   =   $row ['ApplicationID'];
			$RequestID       =   $row ['RequestID'];
			$FullName        =   $ApplicantsObj->getApplicantInfo ( $OrgID, $ApplicationID, $RequestID );
			$disposition     =   $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] );
			$ApplicantEmail  =   $ApplicantDetailsObj->getAnswer($OrgID, $ApplicationID, 'email');
			
			if ($row ['ProcessOrder'] == -1) {
				$Comments = preg_replace ( "/; /i", "<br><br>", $row ['Comments'] );
			} else if ($row ['ProcessOrder'] == -17) {
			    $Comments = "<br>Email: ". $ApplicantEmail;
				$Comments .= "<br><br>".preg_replace ( "/; /i", "<br><br>", $row ['Comments'] );
			} else {
				$Comments = $row ['Comments'];
			}
$Comments = preg_replace('/<pre>/i','',$Comments);		

			echo <<<END
<p>$FullName<br>$ProcessOrder - $Date</p>
$Comments
END;
		}
	}
	
}

echo '<br><br>';
echo '<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" border="0" style="margin:0px 3px -4px 0px;">&nbsp;<font face="Arial"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></font></a></p>';
?>
