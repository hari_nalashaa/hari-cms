<?php
// start update status
if ($permit ['Applicants_Update_Status'] == 1) {
	?>
	<script type="text/javascript">
	function processApplicantStatusForm() { 		
		var form = $('#frmStatusForm');
		$("#upd_app_status_msg_top").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
		
		$.ajax({
			method: "POST",
	  		url: 'applicants/processApplicantStatusForm.php', 
			type: "POST",
			data: form.serialize(),
			success: function(data) {

				const resultobj = JSON.parse(data);

				var option_text = $("#ProcessOrder option:selected").text();

				$("#update_status__applicants span").append("&nbsp; Changed to: "+option_text);
								
				if(resultobj['ProcessOrder'] == "") {
					$("#upd_app_status_msg_top").html("<strong>Unable to update the applicant status</strong>");
				}
				else {
					$("#upd_app_status_msg_top").html("<strong>You have updated the following applicants:</strong>");
				
				}
                               $('#ProcessOrder option[value=""]').attr("selected", "selected");
				$('#DispositionCode option[value=""]').attr("selected", "selected");
				$('#StatusEffectiveDate').val('');

				var editor = tinyMCE.get('Comment');
				editor.setContent('');
				$("#Comment").text('');

				if(typeof(document.frmStatusForm) != 'undefined'
					&& typeof(document.frmStatusForm.ApplicationID) != 'undefined') {
					var app_id = document.frmStatusForm.ApplicationID.value;
					var req_id = document.frmStatusForm.RequestID.value;

					//Hide the current tab and display the applicant history
					$('.ra-tab-content').hide();
			    	$('#applicant-history').show();

				if(resultobj['StatusMatch'] == "Y") {
			    	  $('#checklist-tab').show();
				} else {
			    	  $('#checklist-tab').hide();
				}

			    	document.frmApplicationDetailInfo.current_sel_tab.value = "#applicant-history";
					getApplicantHistory(app_id, req_id);;
				}
					
	    	}
		});
	}
	</script>
	<?php
	echo <<<END
<form method="post" action="applicants.php" name="frmStatusForm" id="frmStatusForm">
<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered table-hover" width="100%">
<tr><td colspan="4" bgcolor="#eeeeee"><b>Update Applicant Status</b></td></tr>
<tr><td colspan="4"><span id="upd_app_status_msg_top"></span></td></tr>
<tr><td align="right" width="150">
New Status:
</td><td>
<select name="ProcessOrder" id="ProcessOrder" class="form-control width-auto">
<option value="CO">No Change</option>
END;
	
	//Set where condition
	$where_app_process = array("OrgID = :OrgID", "ProcessOrder > 1", "Type = 'S'", "Active = 'Y'");
	//Set parameters
	$params_app_process = array(":OrgID"=>$OrgID);
	
	if ($permit ['Applicants_Update_Final_Status'] == 0) {
		$where_app_process[] = "Searchable != 'N'";
	}
	
	//Get applicant process flow
	$results = $ApplicantsObj->getApplicantProcessFlowInfo("ProcessOrder, Description", $where_app_process, 'ProcessOrder', array($params_app_process));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
			$selected = '';
			echo '<option value="' . $row ['ProcessOrder'] . '"' . $selected . '>' . $row ['Description'] . '</option>';
		}
	}
	echo <<<END
</select>
</td>
END;
	
	//Get ApplicantDispositionCodes
	$results = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'Y');
	
	$disposition_rows = $results['count'];
	
	if ($disposition_rows > 0) {
		echo <<<END
<td align="right" width="150">
Disposition:
</td><td>
<select name="DispositionCode" id="DispositionCode" class="form-control">
<option value="">Select a Code</option>
END;
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $row) {
				$selected = '';
				echo '<option value="' . $row ['Code'] . '"' . $selected . '>' . $row ['Description'];
			}
		}
		
		echo <<<END
</select>
</td>
END;
	} // end if disposition set up
	
	echo <<<END
<tr><td align="right">
Effective:
</td><td colspan="4"><input type="text" id="StatusEffectiveDate" name="StatusEffectiveDate" value="" size="10" onBlur="validate_date(this.value,'Effective Date');"  /></td></tr>
END;
	
	echo <<<END
<tr><td align="right" valign="top">
Comments:
</td><td colspan="4">
<textarea name="Comment" rows="2" cols="60" wrap="virtual" class="mceEditor"></textarea>
</td></tr>
<tr><td colspan="4" height="60" valign="middle" align="center">
<input type="hidden" name="action" value="$action">
END;
	
	if ($action == "updatestatus") {

		echo '<input type="hidden" name="multipleapps" value="'.$multipleapps.'">
			  <input type="hidden" name="multi" value="Y">
			  <input type="hidden" name="process_type" value="multiple">';
	}
	else {

		echo '<input type="hidden" name="ApplicationID" value="'.$ApplicationID.'">
			  <input type="hidden" name="RequestID" value="'.$RequestID.'">
			  <input type="hidden" name="process_type" value="single">';
	}
	
	echo '<input type="hidden" name="process" value="Y">
<input type="button" value="Update Status" class="btn btn-primary" onclick="processApplicantStatusForm()">
</td></tr>
</table>
</form>';
} // end update status
?>
