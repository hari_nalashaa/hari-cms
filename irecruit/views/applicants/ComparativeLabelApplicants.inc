<div id="page-wrapper">
    
    <div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title;?></h1>
		</div>
	</div>
	
	<div class="row">
	   <div class="col-lg-6">
		    <form name="frmLabelsList" id="frmLabelsList" method="get">
			Select Tag: <select name="label_id" id="label_id" class="form-control width-auto-inline" onchange="if(this.value != '') document.forms['frmLabelsList'].submit();">
			     <option value="">Select</option>
			     <?php 
			         for($c = 0; $c < count($comp_lbls_list); $c++) {
                        if($comp_lbls_list[$c]['LabelID'] == $LabelID) $selected = 'selected="selected"';
			         	?><option value="<?php echo $comp_lbls_list[$c]['LabelID'];?>" <?php echo $selected;?>><?php echo $comp_lbls_list[$c]['LabelName'];?></option><?php
			         	unset($selected);
			         }
			     ?>
			</select>
			</form>
		</div>
		
		<div class="col-lg-6" style="text-align: right">
    		<form name="frmCreateLabel" id="frmCreateLabel" method="post">
        		Create Tag:
        		<input type="text" name="txtCreateLabel" id="txtCreateLabel" class="form-control width-auto-inline">
        		<input type="button" class="btn btn-primary" name="btnCreateLabel" id="btnCreateLabel" value="Create" onclick="createComparativeLabels(document.forms['frmCreateLabel'].txtCreateLabel.value)">
    		</form><br>
    	</div>
	</div>
	
	<div class="row">
       <div class="col-lg-6">
            <div id="lbl_creation_deletion_status_msg"></div>
       </div>
	   <div class="col-lg-6" style="text-align: right">
		    <form name="frmLabelsList" id="frmLabelsList" method="get">
			Delete Tag: <select name="ddlDelLabels" id="ddlDelLabels" class="form-control width-auto-inline">
			     <option value="">Select</option>
			     <?php 
			         for($c = 0; $c < count($comp_lbls_list); $c++) {
			         	?><option value="<?php echo $comp_lbls_list[$c]['LabelID'];?>"><?php echo $comp_lbls_list[$c]['LabelName'];?></option><?php
			         }
			     ?>
			</select>
			<input type="button" class="btn btn-primary" name="btnDeleteLabel" id="btnDeleteLabel" value="Delete" onclick="delComparativeLabelAndApplicants(document.getElementById('ddlDelLabels').value)">
			</form><br>
		</div>
	</div>
	
	<div class="row">
	   <div class="col-lg-12 col-md-12 col-sm-12">
	       <form name="frmComparativeSearchApplicants" id="frmComparativeSearchApplicants" method="post">
	               
	               <table class="table table-bordered" id="applicants_results">
                        <tr>
                            <th>ApplicationID</th>
                            <th>Application Date</th>
                            <th>ReqID/JobID Title</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Last Modified</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                        
                        <?php 
                            if(count($applications_list) > 0) {
                            	for($i = 0; $i < count($applications_list); $i++) {
                                    $ApplicationID  =   $applications_list[$i]['ApplicationID'];
                                    $RequestID      =   $applications_list[$i]['RequestID'];
                                    $APPDATA        =   $ApplicantsObj->getAppData($OrgID, $ApplicationID);
				    $where  =   array ("OrgID = :OrgID", "RequestID= :RequestID");
    				    $params =   array (":OrgID" => $OrgID, ":RequestID"=>$RequestID);
				    $REQINFO        =   $RequisitionsObj->getRequisitionInformation ( 'Title, RequisitionID, JobID', $where, "", ''. '1', array ($params) );
				     $job_app_info          =   $ApplicationsObj->getJobApplicationsDetailInfo("DATE_FORMAT(EntryDate,'%m/%d/%Y %I:%i %p') AS EntryDate, DATE_FORMAT(LastModified,'%m/%d/%Y %I:%i %p') AS LastModified", $OrgID, $ApplicationID, $RequestID);
				    $REQ=$REQINFO['results'][0];
                                    ?>
                                    <tr>
                                        <td><a href="<?php echo IRECRUIT_HOME?>applicantsSearch.php?ApplicationID=<?php echo $ApplicationID;?>&RequestID=<?php echo $RequestID;?>" target="_blank"><?php echo $ApplicationID;?></a></td>
                                        <td><?php echo $job_app_info['EntryDate']; ?></td>
                                        <td><?php echo $REQ['RequisitionID'] . "/" . $REQ['JobID'] . " - " . $REQ['Title']; ?></td>
                                        <td><?php echo $APPDATA['last'] ." ". $APPDATA['first'];?></td>
                                        <td><?php echo $APPDATA['email']?></td>
                                        <td><?php echo $job_app_info['LastModified']; ?></td>
                                        <td style="text-align:center;">
                                            <a href="?action=delete&label_id=<?php echo $applications_list[$i]['LabelID'];?>&ApplicationID=<?php echo $ApplicationID;?>"><img src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png" title="Delete" style="margin:0px 3px -4px 0px;" border="0"></a>
                                        </td>
                                    </tr>	
                                	<?php
                                }
                            }
                            else {
                            	?>
                                    <tr>
                                        <td colspan="4">No Records Found</td>
                                    </tr>
                            	<?php
                            }
                        ?>
        	       </table>
	               
	       </form>
	       
	   </div>
	</div>   
	
</div>
