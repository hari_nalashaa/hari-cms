<p>
	<b>Applicant Correspondence</b>
</p>
<?php
if (($OrgID) && ($ApplicationID) && ($RequestID)) {
	
	$where     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "ProcessOrder = -4");
	$params    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
	$columns   =   "ProcessOrder, date_format(Date,'%M %d, %Y; %H:%i EST') Date, UserID, Comments, ApplicationID, RequestID, DispositionCode";
	$results   =   G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo($columns, $where, '', 'Date DESC', array($params));
	
	$cnt = 0;
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $JAH) {
			$cnt ++;
			$ProcessOrder    = $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $JAH ['ProcessOrder'] );
			$Date            = $JAH ['Date'];
			$UserID          = $JAH ['UserID'];
			$ApplicationID   = $JAH ['ApplicationID'];
			$RequestID       = $JAH ['RequestID'];
			$FullName        = $ApplicantsObj->getApplicantInfo ( $OrgID, $ApplicationID, $RequestID );
			$disposition     = $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $JAH ['DispositionCode'] );
			$Comments        = $JAH ['Comments'];
		
			echo '<p>';
			if ($cnt == 1) {
				echo $FullName . '<br>';
			} else {
				echo '<hr>';
			}
			echo $ProcessOrder . ' - ' . $Date . '</p>';
			echo $Comments;
		}
	}
	
}

echo <<<END
<br><br>
<p align="center"><a href="#" onclick="window.close()" style="text-decoration:none;"><img src="../images/icons/photo_delete.png"
title="Close Window" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>
END;
?>