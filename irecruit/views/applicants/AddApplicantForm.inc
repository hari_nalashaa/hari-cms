<?php
$formtable  =   "FormQuestions";
$pg         =   "application";
$colwidth   =   "220";
$highlight  =   '#' . $COLOR ['ErrorHighlight'];

if (isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {

	//Bind parameters for requisitions
	$params    =   array(':OrgID'=>$OrgID, ':RequestID'=>$_REQUEST['RequestID']);
	//Set fields to get the Requisitions
	$columns   =   'RequestID, Title, FormID, MultiOrgID';
	//Set where condition
	$where     =   array("OrgID = :OrgID", "RequestID = :RequestID", "Active = 'Y'");
	//Get the requisitions based on the fields
	$req_info  =   G::Obj('Requisitions')->getRequisitionInformation($columns, $where, "", "", array($params));
	
	//if requisition information is array
	if(is_array($req_info['results']) && $req_info['count'] > 0) {
		foreach ($req_info['results'] as $REQ) {
			$RequestID   =   $REQ ['RequestID'];
			$MultiOrgID  =   $REQ ['MultiOrgID'];
			$Position    =   $REQ ['Title'];
			$FormID      =   $REQ ['FormID'];
		} // end foreach
	}
} // end RequestID

$personal_ques_info =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, "1");
$child_ques_info    =   G::Obj('ApplicationFormQuestions')->getFormChildQuesForParentQues($OrgID, $FormID, "1");

$que_types_list     =   array();
if(is_array($personal_ques_info)) {
    foreach($personal_ques_info as $personal_que_id=>$personal_que_info) {
        $que_types_list[$personal_que_id]   =   $personal_que_info['QuestionTypeID'];
    }
}

if ($RequestID == "") {
	echo '<form action="addApplicant.php" method="get">' . "\n";
	echo '<div class="table-responsive">';
	echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover">';
	echo '<tr><td>';
	echo '<div class="col-lg-8"><b>Please select a position</b>:</div>';
	echo '</td></tr>';
	echo '<tr><td>';
	echo '<div class="col-lg-8">';
	
	//Bind parameters
	$params		= 	array(":OrgID"=>$OrgID);
	//Set Columns and condition and sort order
	$columns	= 	array('RequestID', 'Title', 'RequisitionID', 'JobID');
	//Set where condition
	$where		=	array("OrgID = :OrgID", "Active = 'Y'");
	//Get Requisitions Information
    $req_info   =   G::Obj('Requisitions')->getRequisitionInformation($columns, $where, "", "Title, MultiOrgID, ListingPriority", array($params));
	
	echo '<select name="RequestID" id="RequestID" class="form-control width-auto-inline">';
	
	if(is_array($req_info['results']) && $req_info['count'] > 0) {
		foreach ($req_info['results'] as $REQ) {
			echo '<option value="' . $REQ ['RequestID'] . "\">";
			if ($feature ['MultiOrg'] == "Y") {
				echo $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $REQ ['RequestID'] ) . " - ";
			} // end feature
			echo $REQ ['RequisitionID'] . '/' . $REQ ['JobID'];
			echo '&nbsp;-&nbsp;';
			echo $REQ ['Title'];
		} // end while
	}
	
	echo '</select>';
	echo '</div>';
	echo '</td>';
	echo '</tr>';
	
	echo '<tr>';
	echo '<td>';
	echo '<div class="col-lg-2" style="float:left">';
	echo '<input type="hidden" name="HoldID" id="HoldID" value="'.$HoldID.'">';
	echo '<input type="submit" value="Submit" class="btn btn-primary">';
	echo '</div>';
	echo '</td></tr>';
	
	echo '</table>';
	echo '</div>';
	echo '</form>';
} else { // end no Req Job ID's
	if ($HoldID) {
		$applicant_data_temp_info = G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);

		if(is_array($applicant_data_temp_info['results'])) {
			foreach ($applicant_data_temp_info['results'] as $row) {
				$APPDATA [$row ['QuestionID']] = $row ['Answer'];
				$APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
			} // end foreach
		}
	} // end HoldID
	
	echo '<form name="application" method="POST" enctype="multipart/form-data">' . "\n";
	echo '<input type="hidden" name="MAX_FILE_SIZE" value="10485760">';
	
	echo 'Add Applicant for <b>';
	if ($feature ['MultiOrg'] == "Y") {
		echo G::Obj('OrganizationDetails')->getOrganizationName ( $OrgID, $MultiOrgID ) . " - ";
	}
	echo $Position . '</b><br><br>';
	
	echo '<div class="form-group">';
	echo '<label for="paperless">iConnect: </label>
		  <select name="Paperless" id="Paperless">
			<option value="No">Electronic Forms</option>
			<option value="Yes">Paper Forms</option>
 		  </select>
		  </div>';
	
	echo '<div id="app_err_messages" class="errormessage" style="float:left;width:100%;text-align:left;margin-bottom:20px"></div>';
	
    $section_id     =   1;	
    $PersonalInfo   =   include COMMON_DIR . "application/ApplicationSectionInfo.inc";
	if ($PersonalInfo) {
		echo '<p><b class="title">Personal Information</b></p>';
		echo $PersonalInfo;
	}
	
    // Attachment section
    $APP        =   $APPDATA;
    $APPREQ     =   $APPDATAREQ;

    echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
    echo '<b class="title">Attachments</b><br>';
    
    $section_id     =   6;
    $FileUpload     =   include COMMON_DIR . "application/ApplicationSectionInfo.inc";
    
    if ($FileUpload) {
        echo $FileUpload;
    }
    	
	echo '<table border="0" cellspacing="3" cellpadding="0" width="650" class="table table-striped table-bordered table-hover">';
	echo '<tr>';
	echo '<td height="50" valign="middle" align="center">' . "\n";
	echo '<input type="hidden" name="agree" value="Y">' . "\n";
	echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">' . "\n";
	echo '<input type="hidden" name="signature" value="Internal:' . $USERID . '">' . "\n";
	echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">' . "\n";
	echo '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">' . "\n";
	echo '<input type="hidden" name="FormID" value="' . $FormID . '">' . "\n";
	echo '<input type="hidden" name="HoldID" id="HoldID" value="' . $HoldID . '">' . "\n";
	echo '<input type="hidden" name="AddApplicant" id="AddApplicant" value="AddApplicant">' . "\n";
	
	if ($OrgID == 'I20100603') {
		echo '<center><b>Applicant MUST complete an application at the time of interview</b></center><br>';
	}
	// echo '<input type="submit" value="Submit Applicant">';
	echo '<button id="show" class="btn btn-primary">Submit Applicant</button>';
	echo '</td>';
	echo '</tr>' . "\n\n";
	echo '</table>' . "\n";
	echo '<input type="hidden" name="error_exists" id="error_exists" value="F">';
	echo '</form>';
	
	$wait = IRECRUIT_HOME . 'images/wait.gif';
	
	echo <<<END
<div id="loading-div-background">
    <div id="loading-div">
      <img style="height:30px;margin:30px 0px 10px 0px;" src="$wait" alt="Loading.."/><br>
     </div>
</div>
END;
} // end no Req Job ID's
?>
<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info	=	JSON.parse('<?php echo json_encode($child_ques_info);?>');
</script>
<script src="<?php echo IRECRUIT_HOME;?>js/application-edit.js" type="text/javascript"></script>