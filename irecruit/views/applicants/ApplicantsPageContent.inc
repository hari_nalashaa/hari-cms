<?php
if($action == 'scheduleinterview') {
	include IRECRUIT_DIR . 'appointments/ScheduleInterview.inc';
} else if($action == 'savedweightedsearch') {
	include IRECRUIT_DIR . 'applicants/SavedWeightedSearch.inc';
} else if($action == 'manageappointments') {
	include IRECRUIT_DIR . 'appointments/ManageAppointments.inc';
} else if($action == 'remindappointments') {
	include IRECRUIT_DIR . 'appointments/RemindAppointments.inc';
} else if($action == 'calendartableview') {
	include IRECRUIT_DIR . 'appointments/CalendarTableView.inc';
} else if($action == 'calendarlistview') {
	include IRECRUIT_DIR . 'appointments/CalendarListView.inc';
} else if ($action == 'status') {
	include IRECRUIT_DIR . 'applicants/ViewByStatus.inc';
} else if ($action == 'search') {
	include IRECRUIT_DIR . 'applicants/SearchForm.inc';
} else if ($action == 'weightedsearch') {
	include IRECRUIT_DIR . 'applicants/WeightedSearch.inc';
} else if ($action == 'backgroundcheck') {
	echo $BackgroundCheckLinksObj->getPage();
} else if ($action == 'maintainsearch') {
	include IRECRUIT_DIR . 'applicants/MaintainSearch.inc';
} else if ($action == 'forwardapplicant') {
	include IRECRUIT_DIR . 'views/applicants/ForwardApplicant.inc';
} else if ($action == 'onboardapplicant') {
	include IRECRUIT_VIEWS . 'onboard/OnboardApplicant.inc';
} else if ($action == 'MatrixCare'
            || $action == 'WebABA'
            || $action == 'Lightwork'
            || $action == 'Abila'
            || $action == 'Mercer'
            || $action == 'XML'
            || $action == 'Exporter'
            || $action == 'HRMS') {
	include IRECRUIT_VIEWS . 'onboard/OnboardList.inc';
} else if ($action == 'applicationedit') {
	include IRECRUIT_VIEWS . 'applicants/ApplicationEdit.inc';
} else if ($action == 'updatestatus') {
	include IRECRUIT_DIR . 'applicants/UpdateStatus.inc';
} else if ($action == 'contactapplicant') {
	include IRECRUIT_DIR . 'applicants/ContactApplicant.inc';
} else if ($action == 'deleteapplicant') {
	include IRECRUIT_DIR . 'applicants/DeleteApplicant.inc';
} else if ($action == 'managerequisition') {
	include IRECRUIT_DIR . 'applicants/ManageRequisition.inc';
} else if ($action == 'downloaddocumentszip') {
	include IRECRUIT_DIR . 'applicants/DownloadApplicantsDocumentsZip.inc';
}
?>
