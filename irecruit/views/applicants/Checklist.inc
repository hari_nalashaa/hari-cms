<?php 

$ApplicationID  =   $_REQUEST['ApplicationID'];
$RequestID      =   $_REQUEST['RequestID'];
$checklists	=   G::Obj('Checklist')->getCheckListByOrgID($OrgID);

$get_checklist_header =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);
$ChecklistID = $get_checklist_header[ChecklistID];

if ($get_checklist_header[StatusMatch] != "Y") {  die(); } 

if ($ChecklistID != "") {
?>
        <div style="float:right;">
    	<a href="#openFormWindow" onClick="LoadChecklistHistory('<?php echo $OrgID;?>','<?php echo $ApplicationID;?>','<?php echo $RequestID;?>','<?php echo $ChecklistID;?>','<?php echo IRECRUIT_HOME;?>')" style="cursor:pointer;font-weight:bold;margin-right:10px;" class="checklist_history"><img src="<?php echo IRECRUIT_HOME; ?>/images/icons/script.png"> Checklist History</a>
        <a href="<?php echo IRECRUIT_HOME.'/printChecklist.php?ApplicationID='. $ApplicationID.'&RequestID='.$RequestID; ?>" target="_blank" style="font-weight:bold;">
        <img src="<?php echo IRECRUIT_HOME; ?>/images/icons/printer.png"> Print Checklist</a>
        <a href="<?php echo IRECRUIT_HOME.'/export_checklist.php?ApplicationID='. $ApplicationID.'&RequestID='.$RequestID; ?>"  style="font-weight:bold;">
        <img src="<?php echo IRECRUIT_HOME; ?>/images/icons/export_img.png" style="width: 19px;height: 17px;"> Export Checklist</a>

        </div>

<?php 
} // end if ChecklistID

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {

  echo G::Obj('ChecklistData')->getChecklistView($OrgID, $ApplicationID, $RequestID, $ChecklistID);

} else { 
?>
<div style="margin-bottom:15px;float:left;margin-right:10px;">
        <select name="ChecklistID" id="ChecklistID">
        	<option value="">Please select the checklist</option>
        	<?php foreach($checklists['results'] AS $C){ 
                 $selected = '';
                 if($C['ChecklistID'] == $ChecklistID ){
                 $selected = 'selected';
                 }
                ?>
        	<option value="<?php echo $C['ChecklistID']; ?>" <?php  echo $selected; ?>><?php echo $C['ChecklistName']; ?></option>
        	<?php }?>
        </select>
</div>
<div class='update_checklist_text' style='color:green;display:none;'>Checklist Updated</div>
<?php 

if ($ChecklistID != "") {
  echo G::Obj('ChecklistData')->getChecklistForm($OrgID, $ApplicationID, $RequestID, $ChecklistID);
} // end if ChecklistID
?>

<script type="text/javascript">
	$("#ChecklistID").change(function(){
		var e = document.getElementById("ChecklistID");
		var ChecklistID = e.value;
		var ChecklistName = e.options[e.selectedIndex].text;
		var RequestID = "<?php echo $_REQUEST['RequestID'];?>";
		var ApplicationID = "<?php echo $_REQUEST['ApplicationID'];?>";
		var result = "";
		if (ChecklistID != "") { 
		  result = confirm("Are you sure do you want to set this applicant to " + ChecklistName + "?");
		}
        if (result) {
    		$.ajax({
        		type: "POST",
        		url: "applicants/setChecklist.php",
        		data:'ChecklistID='+ ChecklistID+'&ApplicationID='+ ApplicationID+'&RequestID='+ RequestID,
			success: function(data){
				getChecklist(ApplicationID, RequestID);
        		}
        	});
        }
	});

	$(".update_checklist_data").click(function(){
		$(".update_checklist_text").hide();
		var update_checklist_data_id_info = $(this).attr("id");
		var get_update_checklist_data_id = update_checklist_data_id_info.split("update_checklist_data_");
		var update_checklist_data_id = get_update_checklist_data_id[1];
		var StatusCategory = $(this).parent().prev().prev().children("#statusCategory").val();
		var Comments = $(this).parent().prev().children("textarea").val();
		var ChecklistID = $("#ChecklistID").val();
		var ApplicationID = $("#ApplicationID").val();
		var RequestID = $("#RequestID").val();
		var OrgID = $("#OrgID").val();
		var ChecklistName = $(this).parent().prev().prev().prev().prev().prev().text();console.log(ChecklistName);

		$.ajax({
    		type: "POST",
    		url: "applicants/updateChecklistData.php",
    		data:'Checklist='+ update_checklist_data_id+'&StatusCategory='+ StatusCategory+'&Comments='+ Comments+'&ChecklistID='+ ChecklistID+'&ApplicationID='+ ApplicationID+'&RequestID='+ RequestID+'&OrgID='+ OrgID+'&ChecklistName='+ ChecklistName,
    		success: function(data){
			getChecklist(ApplicationID, RequestID);
			/*
    			$(".update_checklist_text").show();
    			var obj = jQuery.parseJSON(data);
    			$("#"+update_checklist_data_id_info).parent().prev().prev().prev().prev().html(obj.LastUpdated);
    			$("#"+update_checklist_data_id_info).parent().prev().prev().prev().html(obj.CreatedBy);
			 */
    		}
    	});
	});
</script>

<?php
} // end else hiring manager 
?>
