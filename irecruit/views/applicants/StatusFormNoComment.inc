<?php
// Status Update Form
if ($permit ['Applicants_Update_Status'] == 1) {
	
	echo '<tr>';
	echo '<td valign="top" width="70" align="right" nowrap="nowrap">';
	echo 'Status:';
	echo '</td>';
	echo '<td width="670">';
	echo '<select name="' . $ADMINTYPE . 'ProcessOrder" id="ProcessOrder" class="form-control" style="max-width:150px;">';
	echo '<option value="">Select a Status</option>';
	
	//Set Condition For Applicant ProcessFlow
	$where   = array();
	$where[] = "OrgID = :OrgID";
	$where[] = "ProcessOrder > 1";
	$where[] = "Type in ('C','S')";
	$where[] = "Active = 'Y'";
	if ($permit ['Applicants_Update_Final_Status'] == 0) {
		$where[] = "Searchable != 'N'";
	}

	//Bind the parameters
	$info = $params = array();
	$params[':OrgID'] = array($OrgID);
	$info[] = $params;
	
	//Get Applicant Process Flow Information
	$results = $ApplicantsObj->getApplicantProcessFlowInfo(array('ProcessOrder', 'Description'), $where, 'ProcessOrder', $info);
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
		
			if ($ProcessOrder == $row ['ProcessOrder']) {
				$selected = ' selected';
			} else {
				$selected = '';
			}
		
			echo '<option value="' . $row ['ProcessOrder'] . '"' . $selected . '>' . $row ['Description'];
		}
	}
	echo '</select>' . "\n";
	
	$results =	$ApplicantsObj->getApplicantDispositionCodes($OrgID,'Y');
	
	$disposition_rows = $results['count'];
	
	echo '</td>';
	echo '</tr>';
	
	if ($disposition_rows > 0) {
		echo '<tr><td align="right" nowrap="nowrap">Disposition Code: </td><td>';
		echo '<select name="' . $ADMINTYPE . 'DispositionCode" id="DispositionCode" class="form-control" style="max-width:350px;">';
		echo '<option value="">Select a Code</option>';
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $row) {
				if ($DispositionCode == $row ['Code']) {
					$selected = ' selected';
				} else {
					$selected = '';
				}
				echo '<option value="' . $row ['Code'] . '"' . $selected . '>' . $row ['Description'];
			}
		}
		
		echo '</select>';
		echo '</td></tr>';
	} // end if disposition set up
	
	
	if (!preg_match ( '/affectedApplicants.php$/', $_SERVER ["SCRIPT_NAME"] )
	   &&
	   !preg_match ( '/applicantsSearch.php$/', $_SERVER ["SCRIPT_NAME"] )
	   &&
	   !preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	    
	    echo '<tr>';
	    echo '<td align="right">Default From Email:</td>';
	    echo '<td><select name="ddlFromEmail" id="ddlFromEmail" class="form-control width-auto-inline">';
	    
	    $options_list = array();
	    //Set where users email
	    $where_users_email = array("OrgID = :OrgID", "UserID = :UserID");
	    //Set parameters
	    $params_users_email = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
	    //Get UsersInformation
	    $results = $IrecruitUsersObj->getUserInformation("UserID, EmailAddress", $where_users_email, "", array($params_users_email));
	    
	    $options_list[""] = "Current User";
	    $options_list["NoReply@iRecruit-us.com"] = 'NoReply@iRecruit-us.com';
	    
	    //Set where condition
	    $where_orgemail = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	    //Set parameters
	    $params_orgemail = array(":OrgID"=>$OrgID, ":MultiOrgID"=>"");
	    //Set columns
	    $columns = "Email,cast(EmailVerified as unsigned int) EmailVerified";
	    //Get Organizations Email Information
	    $resultsIN = $OrganizationsObj->getOrganizationEmailInfo($columns, $where_orgemail, '', array($params_orgemail));
	    $OE = $resultsIN['results'][0];
	    
	    if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
	        $options_list[$OE ['Email']] = $OE ['Email'];
	    }
	    
	    if(is_array($results['results'])) {
	        foreach($results['results'] as $USERS) {
	            $options_list[$USERS ['EmailAddress']] = $USERS ['EmailAddress'];
	        } // end foreach
	    }
	    
	    foreach ($options_list as $option_key=>$option_value) {
	        $opt_selected = ($option_key == $pkg_info['FromEmail']) ? ' selected="selected"' : '';
	        echo '<option value="'.$option_key.'" '.$opt_selected.'>'.$option_value.'</option>';
	    }
	    
	    echo '</select>';
	    
	    echo '</td>';
	    echo '</tr>';
	}

} // end status update
?>
