<div id="page-wrapper">
    
	<div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo htmlspecialchars($title);?></h1>
		</div>
	</div>

	<div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
		<table class="table table-bordered">
		<?php
		for($s = 0; $s < count($_SESSION['SmsApplicants']); $s++) {
			list($ApplicationID, $RequestID)	=	explode(":", $_SESSION['SmsApplicants'][$s]);

			echo '<tr>';
			echo '<td>';
			echo '<a href="' . IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '" target="_blank">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/user_go.png" border="0" title="Applicant Summary">&nbsp;';
			echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Applicant Summary</b>';
			echo '</a>';
			
			echo '&nbsp;&nbsp;';
			$multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
			echo $ApplicationID . ' - ' . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
			echo '&nbsp;&nbsp;';
			echo getEmailAddress ( $OrgID, $ApplicationID, $permit );
			echo '</td>';
			echo '</tr>';
		}
		?>
		</table>
		</div>
	</div>		
			
	<form name="frmTwilioConversation" id="frmTwilioConversation" method="post">
		<table width="100%" border="0" cellpadding="3" cellspacing="1" class="table table-bordered">
			<tr>
				<td colspan="2" align="left">
					<h4 style="margin:0px">Send text to all applicants.</h4>
				</td>
			</tr>
			
			<tr>
				<td width="10%" align="left"><strong>Templates: </strong></td>
				<td>
					<?php
					$templates_list_info	=	G::Obj('TwilioTextMessageTemplates')->getTwilioTextMessagingTemplatesByOrgID($OrgID);
					$templates_list_count	=	$templates_list_info['count'];
					$templates_list			=	$templates_list_info['results'];
					?>
					
					<select name="TwilioMessageTemplates" id="TwilioMessageTemplates" onchange="getTwilioMessageTemplateInfoByGuID(this.value, '<?php echo htmlspecialchars($_REQUEST['ApplicationID']);?>', '<?php echo htmlspecialchars($_REQUEST['RequestID']);?>', 'False')">
					<option value="">Select</option>
					<?php
					for($t = 0; $t < $templates_list_count; $t++)
					{
						$passed_value		=	$templates_list[$t]['PassedValue'];
						$display_text		=	$templates_list[$t]['DisplayText'];
						$template_guid		=	$templates_list[$t]['TemplateGuID'];
						
						?><option value="<?php echo $template_guid;?>"><?php echo $passed_value;?></option><?php
					}
					?>
					</select>
				</td>
			</tr>
			
			<?php
			if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
				?>
				<tr>
					<td colspan="2" align="left">
						<h6 style="margin:0px;color:blue">Successfully send the text to all the applicants.</h6>
					</td>
				</tr>
				<?php
			}
			else if(isset($_GET['msg']) && $_GET['msg'] == 'failed') {
				?>
				<tr>
					<td colspan="2" align="left">
						<h6 style="margin:0px;color:red">
							There is an issue in sending sms to these below applicants. <br>
							Please copy the list and try to resend. <br>
							Applications List:
							<?php
								foreach($_SESSION['FaildApplicants'] as $FailedApplicationID=>$FailedApplicantName) {
									echo $FailedApplicationID . " - " . $FailedApplicantName . "<br>";
								}
								
								unset($_SESSION['FaildApplicants']);
							?>
						</h6>
					</td>
				</tr>
				<?php
			}
			?>
			<tr>
				<td width="10%" align="left"><strong>Message: </strong></td>
				<td align="left">
					<textarea name="taSmsMessage" id="taSmsMessage" rows="8" cols="60" maxlength="600" onkeypress="setCharsLeft()"></textarea>
					<span id="spnSmsMessageCharsLeft">600 chars left</span>
				</td>
			</tr>		
			<tr>
				<td colspan="2" align="left">
					<input type="submit" name="btnSendSMS" id="btnSendSMS" class="btn btn-primary" value="Send Text">
				</td>
			</tr>
		</table>
	</form>
</div>