<div id="page-wrapper">
    <?php
    //Get Wotc Ids list
    $wotcid_info = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, "");
    ?>
	<div class="row" id="dashboard_page_title">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo $title;?></h1>
		</div>
	</div>
    <?php
    if(isset($_GET['msg']) && $_GET['msg'] != '') {
        if($_GET['msg'] == 'suc') $class_alert = ' alert alert-success';
        ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="<?php echo $class_alert;?>">
                <?php
                if($_GET['msg'] == 'suc') echo 'Your Configuration is successfully updated';
                ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
        
    <form name="frmComparativeAnalysisConfig"
    	id="frmComparativeAnalysisConfig" method="post">
    	
    	<h4>Applicants Columns:</h4>
    	<div id="job_application_columns" class="row">
    	       <?php 
    	       if(count($config_info['JobApplicationsColumnsList']) > 0) {
                    $k = 1;
                    foreach($config_info['JobApplicationsColumnsList'] as $column_value) {
                         ?>
        	             <div class="col-lg-10" id="list_item_<?php echo $k;?>" style="padding: 3px;margin-left: 20px;">
                			<div class="form-control">
                				<input type="checkbox" name="job_applications_columns[]" value="<?php echo $column_value;?>" checked="checked">
                    	        &nbsp;
                    	        <?php echo $config_columns_list[$column_value]; ?>
                	        </div>
                		 </div>
        	             <?php
        	       }
               } 
    
    	       foreach($config_columns_list as $column_value => $column_name ) {
                   if(!in_array($column_value, $config_info['JobApplicationsColumnsList'])) {
                       	?>
        	             <div class="col-lg-10" id="list_item_<?php echo $i;?>" style="padding: 3px;margin-left: 20px;">
                			<div class="form-control">
                				<input type="checkbox" name="job_applications_columns[]" value="<?php echo $column_value;?>">
                    	        &nbsp;
                    	        <?php echo $column_name;?>
                	        </div>
                		 </div>
        	             <?php
                   }
    	       }
    	       ?>
    	</div>
    	
    	
    	<h4>Comparative Analysis  Questions:</h4>
        <div id="applicant_summary_questions" class="row">
	        <?php
			if(!empty($comp_const_list)){
				foreach($comp_const_list as$k=> $comp_list) { ?> 
					<div class="col-lg-10" id="list_item_" style="padding: 3px;margin-left: 20px;">
				    	<div class="form-control">
				        	<?php if(in_array($comp_list['QuestionID'],$config_info['ComparativeAnalysisQuestionColumnsList'])){ ?>

		<input type="checkbox" name="comparative_analysis_columns[]" checked ="checked"   value="<?php echo $comp_list['QuestionID'];?>">
				            		          &nbsp;
				            	<?php echo $comp_list['Question'];
							}else{?>

				          		<input type="checkbox" name="comparative_analysis_columns[]"   value="<?php echo $comp_list['QuestionID'];?>">
				          		&nbsp;
				          		<?php echo $comp_list['Question']; 
							}?>
				      </div>
	        		</div> 
        		<?php }
        	} ?>
    	</div>
    	
    	<h4>Weighted Search Columns:</h4>
    	<div id="weighted_search_columns" class="row">
    	<?php 
    	if(count($config_info['WeightedSearchColumnsList']) > 0) {
            $wcli = 1;
            foreach($config_info['WeightedSearchColumnsList'] as $column_value) {
				if($column_value != "wotc_application_status") {
					?>
                    <div class="col-lg-10" id="list_item_<?php echo $wcli;?>" style="padding: 3px;margin-left: 20px;">
        			 <div class="form-control">
        				<input type="checkbox" name="weighted_search_columns[]" checked="checked" value="<?php echo $column_value;?>">
            	        &nbsp;
            	        <?php echo $weighted_searches_list[$column_value]; ?>
            	        <?php 
            		        if($ws_frm_types[$column_value] == "WebForms") {
            		        	echo "(".$ws_frm_types[$column_value].")";
            		        }
            		     ?>
        	          </div>
        		    </div>
                    <?php
                    $wcli++;
				}
            }
        }
        
        if(count($config_info['WeightedSearchColumnsList']) > 0) {
            $cumulative_check = '';
            if(isset($config_info['WeightedSearchColumnsListSum']) && $config_info['WeightedSearchColumnsListSum'] == 'cumulative') {
            	$cumulative_check = ' checked="checked"';
            }
            ?>
        	<!--<div class="col-lg-10" id="list_item_" style="padding: 3px;margin-left: 20px;">
                <div class="form-control">
        	       <input type="checkbox" name="weighted_search_columns_sum" id="weighted_search_columns_sum" value="cumulative" <?php echo $cumulative_check;?>>
        	       &nbsp;
        	       Total Score of Selected Weighted Search Columns (Ordering is not be applicable) 
                </div>
            </div>-->        	       
        	<?php
        }
        
        if(is_array($weighted_searches_list)) {
        	foreach ($weighted_searches_list as $weighted_search_value => $weighted_search_name) {
                if(!in_array($weighted_search_value, $config_info['WeightedSearchColumnsList'])
					&& $weighted_search_value != "wotc_application_status") {
                    ?>
                    <div class="col-lg-10" id="list_item_" style="padding: 3px;margin-left: 20px;">
            	         <div class="form-control">
            		        <input type="checkbox" name="weighted_search_columns[]" value="<?php echo $weighted_search_value;?>">
            		        &nbsp;
            		        <?php echo $weighted_search_name;?>
            		        <?php
            		        if($ws_frm_types[$weighted_search_value] == "WebForms") {
            		        	echo "(".$ws_frm_types[$weighted_search_value].")";
            		        }
            		        ?>
            		     </div>
                    </div>
                    <?php
                }
            }
        }

        if($wotcid_info['wotcID'] != "") {
        	?>
        	<div class="col-lg-10" id="list_item_" style="padding: 3px;margin-left: 20px;">
	        	<div class="form-control">
					<input type="checkbox" name="weighted_search_columns[]" <?php if(in_array("wotc_application_status", $config_info['WeightedSearchColumnsList'])) { echo ' checked="checked"'; } ?> value="wotc_application_status">
					&nbsp;
					CMS WOTC Application Status
	            </div>
	        </div>
        	<?php
        }
        ?>
        </div>

        <h4>Applicant Distance:</h4>
    	<div class="row">
        <?php
        $applicant_distance_check = '';
        if(isset($config_info['ApplicantDistance']) && $config_info['ApplicantDistance'] == 'yes') {
            $applicant_distance_check = ' checked="checked"';
        }
        ?>
    	<div class="col-lg-10" id="list_item_" style="padding: 3px;margin-left: 20px;">
            <div class="form-control">
    	       <input type="checkbox" name="applicant_distance" id="applicant_distance" value="yes" <?php echo $applicant_distance_check;?>>
    	       &nbsp;
    	       Applicant Distance (Ordering is not be applicable) 
            </div>
        </div>        	       
        
    	
    	<div class="row">
    	   <div class="col-lg-12">
    	       <input type="submit" name="btnUpdateConfig" id="btnUpdateConfig" class="btn btn-primary" value="Update">
    	   </div>
    	</div>
    </form>

</div>

<script>
$(document).ready(function() {
	$("#job_application_columns").sortable({
		 cursor: 'move',
		 update : function () {
			  $('#job_application_columns').sortable('serialize');
			  //No need of ajax call
	     }
	});

	$("#weighted_search_columns").sortable({
		 cursor: 'move',
		 update : function () {
			  $('#weighted_search_columns').sortable('serialize');
			//No need of ajax call
	     }
	});

	$("#applicant_summary_questions").sortable({
		 cursor : 'move',
		 update : function () {
			  $('#applicant_summary_columns').sortable('serialize');
			//No need of ajax call
	     }
	});
});                 
</script>
