<?php
if($action != 'weightedsearch') {
    echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered">';
    echo '<tr><td valign="top" width="80%"><b>';
    //echo $title; // redundant
    echo '</b></td><td valign="top" align="left">';
    
    $formaction = IRECRUIT_HOME . "applicants.php";
    echo '<form method="post" name="frmApplicantsNavigation" id="frmApplicantsNavigation" action="' . $formaction . '">';
    echo '<select name="action" onChange="applicantsSubmitAction(this.value)" class="form-control">';
    
    $iii = 0;
    
    echo '<option value="search"';
    if ($action == "search") {
        echo " selected='selected'";
        $iii ++;
    }
    echo '>Applicant Search</option>';
    
    echo '<option value="status"';
    if ($action == "status") {
        echo " selected='selected'";
        $iii ++;
    }
    echo '>Applicants by Status</option>';
    
    echo '<option value="maintainsearch"';
    if ($action == "maintainsearch") {
        echo " selected='selected'";
        $iii ++;
    }
    echo '>Maintain Saved Searches</option>';
    
    if ($BackgroundCheckLinksObj->displayNavigation == 'on') {
        echo '<option value="backgroundcheck"';
        if ($action == "backgroundcheck") {
            echo " selected='selected'";
            $iii ++;
        }
        echo '>Background Checks</option>';
    }
    
    if($action == "scheduleinterview") {
        echo '<option value="scheduleinterview" selected="selected">Schedule Interview</option>';
        $iii ++;
    }
    
    echo '<option value="calendartableview"';
    if ($action == "remindappointments" || $action == "manageappointments" || $action == "calendartableview" || $action == "calendarlistview") {
        echo ' selected="selected"';
        $iii++;
    }
    echo '>Appointment Calendar</option>';
    
    
    if ($iii == 0) {
        echo '<option value="" selected="selected">' . $subtitle [$action] . '</option>';
    }
    
    echo '</select>';
    
    if ($action == "manageappointments" || $action == "calendartableview" || $action == "calendarlistview" || $action == "remindappointments") {
        echo '<select name="appointmentaction" id="appointmentaction" onChange="submit()" class="form-control" style="margin-top:10px">';
        echo '<option value="calendartableview"';
        if($action == 'calendartableview') echo ' selected="selected"';
        echo '>Table View</option>';
    
        echo '<option value="calendarlistview"';
        if($action == 'calendarlistview') echo ' selected="selected"';
        echo '>List View</option>';
        echo '</select>';
    }
    
    echo '</td></tr>';
    echo '</form>';
    echo '</table>';
}
?>
