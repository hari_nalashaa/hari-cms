<script language="JavaScript" type="text/javascript">
function processForwardApplicantsInfo(callback_url) {
	
	var form = $('#forwardemail');

	$("#forward_app_message_bottom").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	var request = $.ajax({
		method: "POST",
  		url: callback_url, 
		type: "POST",
		data: form.serialize(),
		success: function(data) {
			$("#forward_app_message_bottom").html(data);
			$("#forward_app_message_top").html(data);
			$("#EmailComments").text('');

			var editor = tinyMCE.get('EmailComments');
			editor.setContent('');
			
			var checkname = document.forwardemail.em;
			for (i = 0; i < checkname.length; i++) {
			    checkname[i].checked = false;
			}
    	}
	});
}

function confirmForwardApplicants(checkname,action,callback_url)
{
    // validate checkboxes to determine how many are selected
    ii  =   0;
    for (var i = 0; i < checkname.length; i++) {
        if (checkname[i].checked == true) {
            ii++;
        }
    }
    
    // confirm the count and confirm if more than one
    if (ii > 0) {
        if(action.value == 'deleteapplicant') {
            var agree = confirm('Are you sure you want to process these applications?');
        } else {
            processForwardApplicantsInfo(callback_url);
        }
    } else {
        alert('Please select an email to process.');
    }
    
    // process confirm
    if (agree) {
        processForwardApplicantsInfo(callback_url);
    } else {
        return false;
    }
}
</script>
<?php
require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';
require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
require_once IRECRUIT_DIR . 'applicants/ApplicantsMultipleApps.inc';
?>
<form method="post" action="applicants.php" name="forwardemail" id="forwardemail">
	<div class="table-responsive">
		<table border="0" cellspacing="0" cellpadding="5" width="770"
			class="table table-bordered table-hover">
			<tr>
				<td colspan="2">
					<div id="forward_app_message_top"></div>
				</td>
			</tr>
			<tr>
				<td colspan="100%">
				<div id="forward_affected_applicants">
				<?php
				echo '<br>The following applications will be affected:<br>';
				displayList ( $multipleapps, $OrgID, $permit );
				?>
				</div>
				</td>
			</tr>

			<tr>
				<td colspan="100%"><strong>Who would you like to forward this applicant to (<a href="<?php echo IRECRUIT_HOME;?>configuration.php?tab_action=maintainemaillists">Add Contacts under Maintain Email Lists</a>):</strong></td>
			</tr>

			<?php
			//Set columns
			$columns             =   "FirstName, LastName, EmailAddress";
			//Set where condition
			$where_emaillist     =   array("OrgID = :OrgID", "UserID = :UserID");
			//Set parameters
			$params_emaillist    =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
			//Get UserEmail Lists
			$results             =   $IrecruitUsersObj->getUserEmailLists($columns, $where_emaillist, 'FirstName', array($params_emaillist));
			
			echo '<input type="hidden" id="em" name="to[]" value="">';
			
			$i = 0;
			$rowcolor = "#eeeeee";
			
			if(is_array($results['results'])) {
				foreach($results['results'] as $row) {
					
                    $Name   =   $row ['FirstName'] . ' ' . $row ['LastName'];
                    $Email  =   $row ['EmailAddress'];
				
					echo '<tr bgcolor="' . $rowcolor . '">';
					echo '<td>';
					echo '&nbsp;&nbsp;&nbsp;' . $Name . '&nbsp;&nbsp;&nbsp;<a href="mailto:' . $Email . '">' . $Email . '</a><br>';
					echo '</td>';
					echo '<td>';
					echo '<input type="checkbox" id="em" name="to[]" value="' . $Email . '">';
					echo '</td>';
					echo '</tr>';
					$i ++;
				
					if ($rowcolor == "#eeeeee") {
						$rowcolor = "#ffffff";
					} else {
						$rowcolor = "#eeeeee";
					}
				}
			}
			
			$rowcolor == "#eeeeee";
			?>
			
			<tr>
				<td colspan="100%"><strong>What information would you like to forward:</strong></td>
			</tr>

			<tr bgcolor="<?php echo $rowcolor?>">
				<td>&nbsp;&nbsp;&nbsp;Application</td>
				<td><input type="checkbox" name="ForwardAttachments[application]" value="Y" checked></td>
			</tr>

			<?php
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
			?>

			<tr bgcolor="<?php echo $rowcolor?>">
				<td>&nbsp;&nbsp;&nbsp;Applicant History</td>
				<td><input type="checkbox" name="ForwardAttachments[history]" value="Y"></td>
			</tr>
			
			<?php
				$ma     =   explode ( '|', $multipleapps );
				$macnt  =   count ( $ma ) - 1;
				
				if ($macnt < 1) {
					
					//Get JobApplicationsDetailInfo
					$results       =   $ApplicationsObj->getJobApplicationsDetailInfo('FormID', $OrgID, $ApplicationID, $RequestID);
					$FormID        =   $results['FormID'];
					
					$ap            =   explode ( ':', $ma [0] );
					$ApplicationID =   $ap [0];
					$RequestID     =   $ap [1];
					
					//Set where condition
					$where         =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID != 99");
					//Set parameters
					$params        =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
					//Get FormQuestions
					$results_formque = $FormQuestionsObj->getFormQuestionsInformation("FormID, QuestionID, value", $where, "QuestionOrder", array($params));
					
				} else { // end count
                    //Get JobApplicationsDetailInfo
                    $results       =   $ApplicationsObj->getJobApplicationsDetailInfo('FormID', $OrgID, $ApplicationID, $RequestID);
                    $FormID        =   $results['FormID'];
					
					//Set where condition
					$where         =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID != 99", "Active = 'Y'");
					//Set parameters
					$params        =   array(":OrgID"=>$OrgID, ":FormID"=>'STANDARD');
					//Get FormQuestions
					$results_formque = $FormQuestionsObj->getFormQuestionsInformation("FormID, QuestionID, value", $where, "QuestionOrder", array($params));
					
					$override      =   "Y";
				} // end count
				
				if(is_array($results_formque['results'])) {
					foreach($results_formque['results'] as $Attachments) {
					
						if ($override == "Y") {
							$hit = 1;
						} else {
				
							//Set where conditon
							$where_aa    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "TypeAttachment = :TypeAttachment");
							//Set parameters
							$params_aa   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":TypeAttachment"=>$Attachments ['QuestionID']);
							//Get Applicant Attachments
							$resultsIN   =   $AttachmentsObj->getApplicantAttachments('PurposeName', $where_aa, '', array($params_aa));
							$hit         =   $resultsIN['count'];
						}
					
						if ($hit > 0) {
							if ($rowcolor == "#eeeeee") {
								$rowcolor = "#ffffff";
							} else {
								$rowcolor = "#eeeeee";
							}
							
							echo '<tr bgcolor="' . $rowcolor . '"><td>';
							echo '&nbsp;&nbsp;&nbsp;' . $Attachments ['value'];
							echo '</td><td>';
							echo '<input type="checkbox" name="ForwardAttachments[' . $Attachments ['QuestionID'] . ']" value="Y"';
							if (($OrgID == "I20141105") && ($Attachments ['QuestionID'] == "resumeupload")) {
								echo ' checked';
							}
							echo '>';
							echo '</td></tr>';
						} // end if ok
					} // end foreach
				}
			?>
			
			<tr bgcolor="<?php echo $rowcolor?>">
				<td>&nbsp;&nbsp;&nbsp;Related Applications</td>
				<td><input type="checkbox" name="ForwardAttachments[related_applications]" value="Y"></td>
			</tr>
			

			<tr>
				<td colspan="100%"><br>
				&nbsp;&nbsp;
				<strong>Email From:</strong>&nbsp;
				<?php
				//Set where condition
				$where      =   array("OrgID = :OrgID", "UserID = :UserID");
				//Set parameters
				$params     =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
				//Get UserInformation
				$results    =   $IrecruitUsersObj->getUserInformation("UserID, EmailAddress, FirstName, LastName", $where, "", array($params));
				
				echo '<select name="From">';
				echo '<option value="NoReply@iRecruit-us.com">iRecruit NoReply - NoReply@iRecruit-us.com</option>';
				
				//Get Organization Email Information
				$OE         =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
				//Get OrganizationName
				$OrgName    =   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);
				
				if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
                    if($OE ['OrgName'] != "") {
                        echo '<option value="' . $OE ['Email'] . '">' . $OE ['OrgName'] . ' - ' . $OE ['Email'] . '</option>';
                    }
                    else {
                    	echo '<option value="' . $OE ['Email'] . '">' . $OrgName . ' - ' . $OE ['Email'] . '</option>';
                    }
				}
				
				if(is_array($results['results'])) {
					foreach($results['results'] as $USERS) {
						echo '<option value="' . $USERS ['EmailAddress'] . '"';
						if ($USERID == $USERS ['UserID']) {
							echo ' selected';
						}
						echo '>' . $USERS ['FirstName'] . ' ' . $USERS ['LastName'] . ' - ' . $USERS ['EmailAddress'] . '</option>';
					} // end foreach
				}
				
				echo '</select>';
				?>
				</td>
			</tr>

			<tr>
				<td colspan="100%">
					<table>
						<tr>
							<td align="right" valign="top">&nbsp;&nbsp;&nbsp;Comments:</td>
							<td><textarea name="EmailComments" rows="4" cols="75" wrap="virtual" class="mceEditor"></textarea></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="forward_app_message_bottom">
						
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="100%" height="60" valign="middle" align="center">
				<input type="hidden" name="process" value="Y"> 
				<input type="hidden" name="action" value="forwardapplicant">
				<?php
				if ($multipleapps) {
					?>
					<input type="hidden" name="multi" value="Y">
					<input type="hidden" name="multipleapps" value="<?php echo $multipleapps;?>">
					<?php
				} else {
					?>
					<input type="hidden" name="ApplicationID" value="<?php echo $ApplicationID;?>">
					<input type="hidden" name="RequestID" value="<?php echo $RequestID;?>">
					<?php
				}

				//Now both conditions that doesn't have any difference, 
				//have to test some related pages also
				if (preg_match ( '/applicantsSearch.php$/', $_SERVER ["SCRIPT_NAME"] )) {
					$forward_app_callback_url = "applicants/processForwardApplicant.php";
				}
				else {
					$forward_app_callback_url = "applicants/processForwardApplicant.php";
				}
				?>
				<input type="button" value="Forward Applicant" class="btn btn-primary" onClick="return confirmForwardApplicants(document.forwardemail.em, document.forwardemail.action,'<?php echo $forward_app_callback_url;?>')">
				</td>
			</tr>
			
		</table>
	</div>
</form>
