<?php
include COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

$columns        =   "FormID, VeteranEditStatus, DisabledEditStatus, AAEditStatus";
$job_app_info   =   G::Obj('Applications')->getJobApplicationsDetailInfo($columns, $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);

//Condition to check RequestID and ApplicationID exists or not
$app_req_exist_status = (isset($_REQUEST['ApplicationID']) 
						&& ($_REQUEST['ApplicationID'] != "") 
						&& isset($_REQUEST['RequestID'])
						&& $_REQUEST['RequestID'] != "");

if ($app_req_exist_status && $display_app_header != "no") {
	echo '<div class="row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';
	echo displayHeader ( $_REQUEST['ApplicationID'], $_REQUEST['RequestID'] );
	echo '<hr>';
	echo '</div>';
	echo '</div>';
}

$APPDATA = $ApplicantsObj->getAppData ( $_REQUEST['OrgID'], $_REQUEST['ApplicationID'] );	

if (isset($_REQUEST['form']) && $_REQUEST['form'] == "aa") {
    
	$aa_sec_info    =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $_REQUEST['OrgID'], $job_app_info['FormID'], "AA");
	$AATitle        =   $aa_sec_info['SectionTitle'];
	
	if($feature['UserPortal'] == 'Y') {
	    echo '<input type=\'checkbox\' name=\'chkAA\' id=\'chkAA\' value=\'Y\' onclick=\'updAAVetDisEditStatus(this.checked, "AA")\'';

        if($job_app_info['AAEditStatus'] == "Y") {
            echo ' checked="checked"';
        }
    
        echo '> Userportal User Edit Status<br><br>';
    }
    
	if ($AATitle) {
		echo '<b class="title">' . $AATitle . '</b><br><br>';
	}

	if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
	    echo getFormData($OrgID, $job_app_info['FormID'], $_REQUEST['ApplicationID'], "AA", $APPDATA);
	} else {
		include COMMON_DIR . 'application/AA.inc';
		$applicant_name   =   G::Obj('ApplicantDetails')->getApplicantName($OrgID, $_REQUEST['ApplicationID']);
		echo AffirmativeActionSection($OrgID, $job_app_info['FormID'], $_REQUEST['ApplicationID'], $APPDATA, $applicant_name);
	}
	
} // end aa

if (isset($_REQUEST['form']) && $_REQUEST['form'] == "veteran") {
	
	// Get Veteran section Title
	$vet_sec_info  =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $_REQUEST['OrgID'], $job_app_info['FormID'], "VET");
	$VTitle        =   $vet_sec_info['SectionTitle'];
	
	if($feature['UserPortal'] == 'Y') {
	    echo '<input type=\'checkbox\' name=\'chkVet\' id=\'chkVet\' value=\'Y\' onclick=\'updAAVetDisEditStatus(this.checked, "VET")\'';
	    if($job_app_info['VeteranEditStatus'] == "Y") {
	        echo ' checked="checked"';
	    }
	    echo '> Userportal User Edit Status<br><br>';
	}
	
	if ($VTitle) {
	    echo '<b class="title">' . $VTitle . '</b><br><br>';
	}
	
	if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
	    echo getFormData($OrgID, $job_app_info['FormID'], $_REQUEST['ApplicationID'], "VET", $APPDATA);
	} else {
		include COMMON_DIR . 'application/Veteran.inc';
		$applicant_name   =   G::Obj('ApplicantDetails')->getApplicantName($OrgID, $_REQUEST['ApplicationID']);
		echo VeteranSection($OrgID, $job_app_info['FormID'], $_REQUEST['ApplicationID'], $APPDATA, $applicant_name);
	}
} // end veteran

if (isset($_REQUEST['form']) && $_REQUEST['form'] == "disabled") {
	
	// Get Disabled section Title
	$dis_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $job_app_info['FormID'], "DIS");
    $DTitle         =   $dis_sec_info['DisplayTitle'];

	if($feature['UserPortal'] == 'Y') {
	    echo '<input type=\'checkbox\' name=\'chkDisabled\' id=\'chkDisabled\' value=\'Y\' onclick=\'updAAVetDisEditStatus(this.checked, "DIS")\'';
	    if($job_app_info['DisabledEditStatus'] == "Y") {
	        echo ' checked="checked"';
	    }
	    echo '> Userportal User Edit Status<br><br>';
	}
	
	if ($DTitle) {
		echo '<div style="float:left;">';
		echo '<b class="title">' . $DTitle . '</b>';
		echo '</div>';
	}

	if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
	    echo getFormData($OrgID, $job_app_info['FormID'], $_REQUEST['ApplicationID'], "DIS", $APPDATA);
	} else {
		include COMMON_DIR . 'application/Disabled.inc';
		$applicant_name   =   G::Obj('ApplicantDetails')->getApplicantName($OrgID, $_REQUEST['ApplicationID']);
		echo DisabledSection($OrgID, $job_app_info['FormID'], $_REQUEST['ApplicationID'], $APPDATA, $applicant_name);
	}
} // end disabled

if ($DTextPost) {
	echo "<br>";
	echo $DTextPost . "<br>";
	echo '<div style="clear:both;"></div>';
}

function getFormData($OrgID, $FormID, $ApplicationID, $SectionID, $APPDATA) {
    
    $rtn        =   "";
    
    //Set parameters
    $params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
    //Set where condition
    $where      =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
    //Get QuestionsInformation
    $results    =   G::Obj('FormQuestions')->getQuestionsInformation("FormQuestions", "*", $where, "QuestionOrder", array($params));
    
    foreach ($results['results'] as $FORM) {
        
        if ($FORM ['QuestionTypeID'] == "99") {
            
            $rtn .= '<div class="row">';
            $rtn .= '<div class="col-lg-12 col-md-12 col-sm-12">' . "\n";
            $rtn .= $FORM ['Question'];
            $rtn .= '</div>' . "\n";
            $rtn .= '</div>' . "\n";
            
        } else {
            
            if ($FORM ['QuestionTypeID'] == "10") {
                
                $name = G::Obj('ApplicantDetails')->getApplicantName($OrgID, $ApplicationID);
                
                $rtn .= '<div class="row">';
                
                $rtn .= '<div class="col-lg-3 col-md-3 col-sm-3">' . "\n";
                $rtn .= 'Name:';
                $rtn .= '</div>';
                
                $rtn .= '<div class="col-lg-9 col-md-9 col-sm-9">' . "\n";
                $rtn .= '<strong> ' . ($name ? $name : 'N/A') . '</strong>';
                $rtn .= '</div>';

                $rtn .= '</div>' . "\n\n";
                
            }
            
            if (($FORM ['QuestionTypeID'] != "10") && ($FORM ['value'] != "")) {
                $APPDATA [$FORM ['QuestionID']] = G::Obj('ApplicationFormQueAns')->getDisplayValue ( $APPDATA [$FORM ['QuestionID']], $FORM ['value'] );
            }
            
            $rtn .= '<div class="row">';

            $rtn .= '<div class="col-lg-3 col-md-3 col-sm-3">' . "\n";
            $rtn .= $FORM ['Question'];
            $rtn .= '</div>' . "\n";
            
            $rtn .= '<div class="col-lg-9 col-md-9 col-sm-9">' . "\n";
            $rtn .= '<strong>' . ($APPDATA [$FORM ['QuestionID']] ? $APPDATA [$FORM ['QuestionID']] : 'N/A') . '</strong>';
            $rtn .= '<strong>' . $ans . '</strong>';
            $rtn .= '</div>' . "\n";
            
            $rtn .= '</div>' . "\n";
        }
    } // end while
    
    return $rtn;
} // end function
?>