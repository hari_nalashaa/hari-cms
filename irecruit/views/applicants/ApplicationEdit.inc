<?php
include IRECRUIT_DIR . 'applicants/ProcessApplicationFormInfo.inc';

require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';
displayList ( $ApplicationID . ':' . $RequestID . '|', $OrgID, $permit );

// Query and Set Text Elements
$params 	=	array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
//Get text blocks information
$where		=	array("OrgID = :OrgID", "FormID = :FormID");
$results	=	G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
    }
}

$Reason    =  '<br>';
$Reason    .=  '<p align="center">Reason for updating information.<br />';
$Reason    .=  '<textarea name="Comment" class="mceEditor" rows="5" cols="50" wrap="yes"></textarea>';
$Reason    .=  '</p>';
$Reason    .=  '<p align="center">';
$Reason    .=  '<input type="hidden" name="action" value="applicationedit">';
$Reason    .=  '<input type="hidden" name="HoldID" value="' . $HoldID . '">';
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div style="float:right;">
			Sections List: <select name="SectionID" id="SectionID" onchange="getApplicantEditFormBySectionID(this.value)" class="form-control width-auto-inline">
    			<?php
    			foreach ($sections_list as $section_id=>$section_info) {
    			    ?>
    		    	<option value="<?php echo $section_id;?>" <?php if($section_id == $SectionID) echo ' selected="selected"';?>>
    		    	<?php echo $section_info['SectionTitle'];?>
    				</option>
    				<?php
    			}
    			?>
    		</select>
		</div>
	</div>
</div>

<br><br>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
        <div class="application_process_content" id="section_content<?php echo $SectionID;?>">
            <form name="<?php echo $section_forms[$SectionID];?>" id="<?php echo $section_forms[$SectionID];?>" method="post" enctype="mutipart/form-data">
                <?php
                    $section_id =   $SectionID;
                    $application_form = include COMMON_DIR . "application/ApplicationSectionInfo.inc";
                    echo $application_form;
                ?>
                <br>
                <input type="hidden" name="ApplicationID" id="ApplicationID" value="<?php echo $ApplicationID;?>">
                <input type="hidden" name="SectionID" id="SectionID" value="<?php echo $SectionID;?>">
                <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>">
                <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
                <input type="hidden" name="process" id="process" value="Y">
                
                <?php
                if ($View != "Y") {
                    echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
                    echo '<tr><td height="60" valign="middle">';
                    echo $Reason;
                    echo '</td></tr>' . "\n\n";
                    echo '</table>' . "\n";
                }
                ?>
                <input type="button" name="btnInfo" id="btnInfo" class="btn btn-primary" value="Save" onclick="processApplicationInfo(this)">
            </form>
        </div>
        
	</div>
</div>    
<br>
<span style="color:#428bca" id="edit_application_progress"></span>
<div id="process_message"></div>

<br>
<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info	=	JSON.parse('<?php echo json_encode($child_ques_info);?>');
</script>
<script src="<?php echo IRECRUIT_HOME;?>js/application-edit.js"></script>