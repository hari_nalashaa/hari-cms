<?php 
//Insert Default Twilio Messaging Templates
G::Obj('TwilioTextMessageTemplates')->insDefaultTwilioTextMessagingTemplates($OrgID);

if(isset($_REQUEST['btnSubmitTemplate']) && $_REQUEST['btnSubmitTemplate'] == "Submit") {

	//Delete all records by OrgID
	G::Obj('TwilioTextMessageTemplates')->delTextMessagingTemplatesByOrgID($OrgID);

	for($p = 0; $p < count($_REQUEST['passed_value']); $p++) {
		if($_REQUEST['passed_value'] != "" && $_REQUEST['display_text'][$p] != "") {
			G::Obj('TwilioTextMessageTemplates')->insTextMessagingTemplate($OrgID, $_REQUEST['passed_value'][$p], $_REQUEST['display_text'][$p], $p);
		}
	}
	
	echo "<script type='text/javascript'>location.href='administration.php?action=irecruittextmessagetemplates&menu=8&msg=suc';</script>";
	exit;
}
?>

<div class="table-responsive">
	<form name="frmTextMessageTemplate" id="frmTextMessageTemplate"	method="post">
	<table width="770" cellspacing="3" cellpadding="5" class="table table-bordered">
		<tr>
			<td colspan="2"><strong>Placeholders:</strong> {first}, {last}, {JobTitle}, {RecruiterName}, {OrganizationName} </td>
		</tr>
		
		<tr>
			<td colspan="2">
				<div id="update_msg" style="color:blue">
				<?php
				if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'suc') {
					echo "Successfully updated the templates information";
				}
				?>			
				</div>
			</td>
		</tr>
	</table>
	
	<table width="770" cellspacing="3" cellpadding="5" class="table table-bordered" id="sort_templates">
		<thead>		
			<tr>
				<td>Passed Value</td>
				<td>Display Text</td>
			</tr>
		</thead>
		
		<tbody>
		<?php
			$templates_list_count	=	0;
			$templates_list_info	=	G::Obj('TwilioTextMessageTemplates')->getTwilioTextMessagingTemplatesByOrgID($OrgID);
			$templates_list_count	=	$templates_list_info['count'];
			$templates_list			=	$templates_list_info['results'];
			
			for($t = 0; $t < $templates_list_count; $t++)
			{
				$input_passed_value_name	=	$templates_list[$t]['PassedValue'];
				$input_display_text_name	=	$templates_list[$t]['DisplayText'];
				
				$passed_value				=	$templates_list[$t]['PassedValue'];
				$display_text				=	$templates_list[$t]['DisplayText'];
				
				echo '<tr id="'.$templates_list[$t]['TemplateGuID'].'">';
					echo '<td>';
						echo '<input type="text" name="passed_value['.$t.']" value="'.$passed_value.'">';
					echo '</td>';
					echo '<td>';
						echo '<textarea type="text" name="display_text['.$t.']" rows="3" cols="100">'.$display_text.'</textarea>';
					echo '</td>';
				echo '</tr>';
			}			

			echo '<tr>';
				echo '<td>';
					echo '<input type="text" name="passed_value['.$t.']">';
				echo '</td>';
				echo '<td>';
					echo '<textarea type="text" name="display_text['.$t.']" rows="3" cols="100"></textarea>';
				echo '</td>';
			echo '</tr>';
						
			$templates_list_count++	//Increment one time to show the empty record option
		?>
		</tbody>	
	</table>
	
	<table>
		<tr>
			<td colspan="2"><input type="submit" name="btnSubmitTemplate" id="btnSubmitTemplate" class="btn btn-primary" value="Submit"></td>
		</tr>
	</table>

		<input type="hidden" name="action" value="irecruittextmessagetemplates">
	</form>
</div>

<script>
function updateTextMessageTemplatesSortOrder(forms_data) {
	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "applicants/updateTwilioTextMessageTemplatesSortOrder.php",
  		data: forms_data,
		type: "POST",
		success: function(data) {
			$("#update_msg").html("Successfully updated the sorting order.");
    	}
	});
}
			
var forms_data = {
	forms_info: []
};

$( "#sort_templates tbody" ).sortable({
	cursor: 'move',
	placeholder: 'ui-state-highlight',
    stop: function( event, ui ) {
        
    	forms_data = {
			forms_info: []
    	};

    	var SortOrder = 0;
    	$(this).find('tr').each(function(i) {

            SortOrder++;
            var tr_id = $(this).attr('id');
           
            var GuID = tr_id;
        	
        	forms_data.forms_info.push({
        		 "GuID" : GuID,
        		 "SortOrder" : SortOrder
        	});
        });
        
        updateTextMessageTemplatesSortOrder(forms_data);
    }
});
</script>