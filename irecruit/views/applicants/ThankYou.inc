<?php
$Email      =   $ApplicantDetailsObj->getAnswer($OrgID, $ApplicationID, 'email');

$columns    =   array('FormID');
$where      =	array("OrgID = :OrgID", "RequestID = :RequestID");
$params     =	array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);

//Get FormID from Requisitions
$results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", "", array($params));
$FormID = $results['results'][0]['FormID'];

//Get Text From TextBlocks
$Response =	$FormFeaturesObj->getTextFromTextBlocks($OrgID, $FormID, 'Response');

echo '<table border="0" cellspacing="0" cellpadding="10" width="100%" class="table table-striped table-bordered">';
echo '<tr><td align="center">';

$OTHERJOBS = array ();

$columns =  array('RequestID');
$where	 =	array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
$params	 =	array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);

//Get JobApplications Information
$results = $ApplicationsObj->getJobApplicationsInfo($columns, $where, '', '', array($params));

$i = 0;
if(is_array($results['results'])) {
	foreach ($results['results'] as $JA) {
	    		    
	    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $JA ['RequestID']);
		$OTHERJOBS [$i] = $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] );
		$i ++;
	}
}

if ($i > 1) {
	
	echo '<p>You have applied for the following positions:<br>';
} else {
	
	echo '<p>You have applied for the following position:';
}

$ii = 0;
while ( $ii < count ( $OTHERJOBS ) ) {
	echo '&nbsp;&nbsp;&nbsp;<b>' . $OTHERJOBS [$ii] . '</b><br>';
	$ii ++;
}

echo '</p>';

echo '<p>Your Application ID is: <b>' . $ApplicationID . '</b><br><br>';
if (! preg_match ( '/addApplicant.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	echo 'Confirmation email will be sent to: <b>' . $Email . '</b></p>';
	echo '<p>' . $Response . '</p>';
}
echo '</td></tr>';
echo '</table>';

if (! $USERID) {
	echo '<table border="1" cellspacing="1" cellpadding="10" bordercolor="darkblue" class="table table-striped table-bordered">';
	echo '<tr><td bgcolor=#FFFFFF>';
	echo '<p><b><a href="' . $_SERVER ['PHP_SELF'] . '?';
	echo 'OrgID=' . $OrgID;
	if ($MultiOrgID != "") {
		echo '&MultiOrgID=' . $MultiOrgID;
	} 
	echo '&pg=listings">Continue</a></b></p>';
	echo '</td></tr>';
	echo '</table>';
	echo '<br>';
}
?>
