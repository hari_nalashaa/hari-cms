<?php 
$ApplicationID  =   $_REQUEST['ApplicationID'];
$RequestID      =   $_REQUEST['RequestID'];
$get_checklist_header =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);

if(isset($displayFormHeader)) {
    echo $displayFormHeader;
    
    echo '<div class="row"><div class="col-lg-12 col-md-12 col-sm-12"><hr><br></div></div>';
}

echo G::Obj('ChecklistData')->getChecklistView($OrgID, $ApplicationID, $RequestID, $get_checklist_header['ChecklistID']);

?>
