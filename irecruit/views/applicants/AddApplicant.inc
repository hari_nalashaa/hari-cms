<div id="page-wrapper">
	<?php include_once IRECRUIT_VIEWS . 'responsive/DashboardPageTitle.inc';?>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					<?php
						if($this->applicant_inserted == '1') {
							include IRECRUIT_VIEWS . 'applicants/ThankYou.inc';	
						}
						else {
							if ($feature['ResumeParsing'] == "Y" 
							    && isset($_REQUEST['RequestID']) 
							    && $_REQUEST['RequestID'] != "") {
                                
								$resumeparse    =   G::Obj('Sovren')->uploadResumeForm($OrgID, $HoldID);
							
								if (is_array($resumeparse)) {
									$APPDATA = $resumeparse;
								} else {
									echo $resumeparse;
								}
							} // end feature
							
							include IRECRUIT_VIEWS . 'applicants/AddApplicantForm.inc';	
						}
					?>
					<!-- /.row (nested) -->
					</div>
				</div>
				<!-- /.panel-body -->
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>