<?php
if($ServerInformationObj->getRequestSource() != 'ajax') {
	?>
	<style>
    .row {
    	margin: 0px !important;
	    clear:both;
	    padding-top: 3px;
    }
    </style>
	<?php
}

if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];

if (($_REQUEST['ApplicationID']) && isset($OrgID) && ($_REQUEST['RequestID'])) {
	
    $ApplicationID  =   $_REQUEST['ApplicationID'];
    $RequestID      =   $_REQUEST['RequestID'];
    
	// get applicant data
    $APPDATA        =   G::Obj('Applicants')->getAppData ($OrgID, $_REQUEST['ApplicationID'] );
	
    $ApplicantInfo  =   G::Obj('Applications')->getJobApplicationsDetailInfo("FormID", $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
    
    //Get MultiOrgID based on OrgID, RequestID
    $MultiOrgID     =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
    
	$applicationsize = count ( $APPDATA );
	
	// validate the FormID
	$FormID        =   $ApplicantInfo['FormID'];
	//Bind parameters
	$params        =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where         =   array("OrgID = :OrgID", "FormID = :FormID");
	//Get Distinct FormID's count
	$results       =   $FormQuestionsObj->getFormQuestionsInformation(array("DISTINCT(FormID) AS DFormID"), $where, "", array($params));
	$hit           =   $results['count'];
	
	if ($hit < 1) {
		$FormID = 'STANDARD';
	}
	
    $form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
	
    foreach ( $form_sections_info as $form_section_id=>$form_section_info) {
        $TITLES [$form_section_id] = $form_section_info['SectionTitle'];
    } // end foreach

	//Titles for Affirmative Action, Veteran, Disabled Forms
	$TITLES ["AA"] = "Affirmative Action";
	$TITLES ["VET"] = "Veteran";
	$TITLES ["DIS"] = "Disabled";
	
	//Bind parameters
	$params = array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where = array("OrgID = :OrgID", "FormID = :FormID");
	// Query and Set Text Elements
	$results = $FormFeaturesObj->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $row) {
			$TextBlocks [$row ['TextBlockID']] = $row ['Text'];
		}	
	}
	
	
	if ($applicationsize < 30) {
		if ($OrgID == 'I20100603') {
			$rtn .= '<center><b>Applicant MUST complete an application at the time of interview</b></center><br>';
		}
	}
	
	// Read and Set Logos
	if (! $emailonly) {
		
		//Bind parameters
		$params = array(':OrgID'=>$OrgID, ':MultiOrgID'=>(string)$MultiOrgID);
		//Set condition
		$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
		//Get Organization Logos Information
		$resultsOL = $OrganizationsObj->getOrganizationLogosInformation(array("OrgID"), $where, "", array($params));
		$imgcnt = $resultsOL['count'];
		
		if ($imgcnt > 0) {
			$rtn .= '<img src="' . IRECRUIT_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br>';
		} // end imgcnt
	} // end email only

	//Bind parameters
	$params = array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
	//Set condition
	$where = array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = 'employment'");
	//Get FormQuestions Information
	$results = $FormQuestionsObj->getFormQuestionsInformation("Question", $where, "", array($params));

	$Employment = $results['results'][0]['Question'];
	
	if ($Employment == '') {
		$Employment = 'Employment Status Desired:';
	}
	
	//Bind parameters
	$params = array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':ApplicationID'=>$_REQUEST['ApplicationID']);
	//Set condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "ApplicationID = :ApplicationID");
	//Get JobApplications Information
	$results = $ApplicationsObj->getJobApplicationsInfo(array("RequestID", "Distinction", "EntryDate"), $where, '', 'Distinction', array($params));
		
	if(is_array($results['results'])) {
		foreach ($results['results'] as $JA) {
		    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $JA['RequestID']);
		    
			$ED = $JA ['EntryDate'];
			if ($JA ['Distinction'] == 'P') {
				$TT = 'Position Desired';
			} else {
				$TT = 'Other Position Desired';
			}

			$rtn .= "<div class='row' style='clear:both'>";
			$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12">'.$TT . ': ';
			$rtn .= '<b>(' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . ')</b>';
			$rtn .= '<b> ' . $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] ) . '</b>';
			$rtn .= '</div>';
			$rtn .= '</div>';
		}
	}
	
	$rtn .= '<div class="row" style="clear:both">';
	$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" id="question_space" style="width:100%"></div>';
	$rtn .= '</div>';
	
	
	$rtn .= '<div class="row" style="clear:both">';
	$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" style="width:100%">Application ID: ';
	$rtn .= '<b>' . $_REQUEST['ApplicationID'] . '</b>';
	$rtn .= '</div>';
	$rtn .= '</div>';

	$rtn .= '<div class="row" style="clear:both">';
	$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" id="question_space" style="width:100%"></div>';
	$rtn .= '</div>';
	
	$rtn .= '<div class="row" style="clear:both">';
	$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" style="width:100%">Application Date: ';
	$rtn .= '<b>' . $ED . '</b>';
	$rtn .= '</div>';
	$rtn .= '</div>';
	

	$rtn .= '<div class="row" style="clear:both">';
	$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" id="question_space" style="width:100%"></div>'; 
	$rtn .= '</div>';
	
	$rtn .= '<div class="row" style="clear:both">';
	$rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" style="width:100%">'.$Employment.' ';
	$rtn .= '<b>' . $APPDATA ['employment'] . '</b>';
	$rtn .= '</div>';
	$rtn .= '</div>';

	$rtn .= '<br>';
	
	if ($TITLES [1] != "") {
	    $rtn .= '<div class="row" style="clear:both">';
	    $rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" style="width:100%">';
	    $rtn .= '<strong>' . $TITLES [1] . '</strong>';
	    $rtn .= '</div>';
	    $rtn .= '</div>';
	}

	$AppQueAnswers =   $ApplicationFormQueAnsObj->getApplicationViewInfo($OrgID, $ApplicationID, $RequestID, $APPDATA);

	$personal_info_ques = array("name", "address", "address2", "faddress", "country", "email");
	$personal_info_excludes = array("first", "last", "middle", "county", "city", "state", "name", "address", "address2", "faddress", "country", "email", "zip");
	
	foreach($personal_info_ques as $personal_que) {
	    $personal_que_answer   =   $AppQueAnswers[1][0][$personal_que]['Answer'];
	    
	    if($personal_que_answer != "") {
	        $rtn .= '<div class="row" style="clear:both">';
	        $rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" style="width:100%">';
	        if($personal_que == "email") {
	            if($permit ['Applicants_Contact'] >= 1) {
	                $rtn .= 'Email Address: <a href="mailto:' . $personal_que_answer . '">' . $personal_que_answer . '</a>';
	            }
	            else {
	                $rtn .= 'Email Address: ' . $personal_que_answer;
	            }
	        }
	        else {
	            $rtn .= $personal_que_answer;
	        }
	        $rtn .= '</div>';
	        $rtn .= '</div>';
	    }
	}
	
	$rtn .= "<br>";

	foreach ($AppQueAnswers as $SectionID=>$QA)
	{
	    if($form_sections_info[$SectionID]['ViewPrintStatus'] == "Y") {

	        if($SectionID != "1") {
	            $rtn .= '<div class="row" style="clear:both">';
	            $rtn .= '<div class="col-lg-12 col-md-12 col-sm-12" style="width:100%">';
	            $rtn .= "<br><strong>".$TITLES[$SectionID]."</strong>";
	            $rtn .= '</div>';
	            $rtn .= '</div>';
	        }
	        
	        $SubSecCount = count($AppQueAnswers[$SectionID]);
	        
	        for($subseci = 0; $subseci < $SubSecCount; $subseci++) {
	            
	            $que_section = 0;
	            foreach ($QA[$subseci] as $QuestionID=>$QuestionAnswerInfo) {
	                
	                if(!in_array($QuestionID, $personal_info_excludes)) {
	                    if($que_section == 0) {
	                        $rtn .= "<div class='row' style='clear:both'>";
	                        
	                        if($subseci == 0) {
	                           $rtn .= "<div class='col-lg-12 col-md-12 col-sm-12' style='float:left;'>".$QuestionAnswerInfo['SubSecInstruction']."</div>";
	                        }
                            
	                        $rtn .= "<div class='col-lg-12 col-md-12 col-sm-12' style='float:left;'><br><u>".$QuestionAnswerInfo['SubSecTitle']."</u></div>";
	                        $rtn .= "</div>";
	                    }
	                     
	                    $que_section++;
	                     
	                    $rtn .= "<div class='row' style='clear:both'>";
	                    
	                    if($QuestionAnswerInfo['QuestionTypeID'] == 100) {
	                    
	                        $rtn .= "<div class='col-lg-3 col-md-3 col-sm-3' style='width:28% !important;border:1px solid #FFFFFF;float:left;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                         
	                        $canswer = $QuestionAnswerInfo['Answer'];
	                    
	                        $content_100 = '';
	                        if(is_array($canswer)) {
	                            $content_100 .= '<table border="0" cellspacing="3" cellpadding="3">';
	                    
	                            foreach ($canswer as $cakey=>$caval) {
	                                $content_100 .= '<tr>';
	                                $content_100 .= '<td style="padding-right:5px !important">'.$cakey.'</td>';
	                                foreach ($caval as $cavk=>$cavv) {
	                                    $content_100 .= '<td style="padding-right:5px !important">'.$cavv.'</td>';
	                                }
	                                $content_100 .= '</tr>';
	                            }
	                    
	                            $content_100 .= '</table>';
	                        }
	                         
	                        $rtn .= "<div class='col-lg-9 col-md-9 col-sm-9' style='width:70% !important;border:1px solid #FFFFFF;float:left;'><strong>".$content_100."</strong></div>";
	                    
	                    }
	                    else if($QuestionAnswerInfo['QuestionTypeID'] == 120) {
	                    
	                        $rtn .= "<div class='col-lg-3 col-md-3 col-sm-3' style='width:28% !important;border:1px solid #FFFFFF;float:left;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                        
	                        $canswer = $QuestionAnswerInfo['Answer'];
	                    
	                        $content_120 = '';
	                        if(is_array($canswer)) {
	                    
	                            $content_120 .= '<table border="0" cellspacing="3" cellpadding="0">';
	                             
	                            $days_count = count($canswer['days']);
	                             
	                            for($ci = 0; $ci < $days_count; $ci++)  {
	                                $content_120 .= '<tr>';
	                                $content_120 .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
	                                $content_120 .= '<td width="5%">from&nbsp;&nbsp;</td>';
	                                $content_120 .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
	                                $content_120 .= '<td width="5%">to </td>';
	                                $content_120 .= '<td>'.$canswer['to_time'][$ci].'</td>';
	                                $content_120 .= '</tr>';
	                            }
	                             
	                            $content_120 .= '</table>';
	                        }
	                    
	                        $rtn .= "<div class='col-lg-9 col-md-9 col-sm-9' style='width:70% !important;border:1px solid #FFFFFF;float:left;'><strong>".$content_120."</strong></div>";
	                    }
	                    else if($QuestionAnswerInfo['QuestionTypeID'] == 99) {
	                        $rtn .= "<div class='col-lg-12 col-md-12 col-sm-12' style='width:100% !important;float:left;'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
	                    }
	                    else {
	                        $rtn .= "<div class='col-lg-3 col-md-3 col-sm-3' style='width:28% !important;border:1px solid #FFFFFF;float:left;'>".$QuestionAnswerInfo['Question']."</div>";
	                        $rtn .= "<div class='col-lg-9 col-md-9 col-sm-9' style='width:70% !important;border:1px solid #FFFFFF;float:left;'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
	                    }

	                    $rtn .= "</div>";
	                    
	                    $rtn .= "<div style='clear:both;border:1px solid #FFFFFF;'></div>";
	                }
	            }
	        }
	    }
	}
	
	
	if ($TextBlocks ["ApplicantAuthorizations"]) {
	    $ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
	} else {
	    $ApplicantAuthorizations = "";
	}
	
	$Signature = '<br>' . $TextBlocks ["Signature"];
	
	if ($OrgID == "I20100301") { // Meritan
	    $Signature .= ' I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.';
	} else {
	    $Signature .= ' I agree with the above statement.';
	}
	$Signature .= '<br><br>';
	
	$Signature .= 'Electronic Signature: <font style="font-color:red">X</font>&nbsp;<b>' . $APPDATA ['signature'] . '</b>';
	
	if (($TextBlocks ["Signature"]) || ($ApplicantAuthorizations)) {
	    $rtn .='<br><br><table border="0" cellspacing="3" cellpadding="0" class="table">';
	    $rtn .= '<tr><td><b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '</td></tr>';
	    $rtn .= '<tr><td>';
	    $rtn .= $Signature;
	    $rtn .= '<td></tr>';
	    $rtn .= '</table>';
	    $rtn .= '<br>';
	}

	if (($OrgID == "B12345467xx") || ($OrgID == "I20130510")) {
	    $rtn .= '<img src="' . IRECRUIT_HOME . 'images/hiring_managers_only.jpg' . '" style="width:786px;">';
	}
	
} // end if ApplicationID, OrgID, RequestID

return $rtn;
?>
