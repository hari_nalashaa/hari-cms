<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title = $title =	"Custom Code";

if(isset($_REQUEST['taCssInfo']) 
	&& str_replace(" ", "", $_REQUEST['taCssInfo']) != ""
	&& isset($_REQUEST['btnCssSubmit'])
	&& $_REQUEST['btnCssSubmit'] != "") {
	
	$images_dir = PUBLIC_DIR . 'images/' . $OrgID;
	
	if (! file_exists($images_dir)) {
	    mkdir($images_dir, 0700);
	    chmod($images_dir, 0777);
	}
	
	$file	=	fopen($images_dir . '/style.css', "w");
	fwrite($file, $_REQUEST['taCssInfo']);
	fclose($file);
	
	header("Location:publicTheme.php?msg=csuc");
	exit;
}

if(isset($_REQUEST['taFooterCodeInfo'])
	&& isset($_REQUEST['btnFooterCodeSubmit'])
	&& $_REQUEST['btnFooterCodeSubmit'] != "") {

	$images_dir = PUBLIC_DIR . 'images/' . $OrgID;
	
	if (! file_exists($images_dir)) {
	    mkdir($images_dir, 0700);
	    chmod($images_dir, 0777);
	}

	$file	=	fopen($images_dir . '/FooterCode.inc', "w");
	fwrite($file, $_REQUEST['taFooterCodeInfo']);
	fclose($file);
	
	header("Location:publicTheme.php?msg=jsuc");
	exit;
}

$TemplateObj->css_content	=	$css_content	=	file_get_contents(PUBLIC_DIR . 'images/' . $OrgID . '/style.css');
$TemplateObj->footer_code	=	$footer_code	=	file_get_contents(PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc');

echo $TemplateObj->displayIrecruitTemplate('views/public/PublicTheme');
?>