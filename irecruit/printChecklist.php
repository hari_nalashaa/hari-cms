<?php
$PAGE_TYPE = "PopUp";
require_once 'Configuration.inc';
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

$TemplateObj->title = $title = 'iRecruit - Checklist View';
$TemplateObj->ApplicationID     =   $ApplicationID      =   $_REQUEST['ApplicationID'];
$TemplateObj->RequestID         =   $RequestID          =   $_REQUEST['RequestID'];
$TemplateObj->displayFormHeader =   $displayFormHeader  =   displayHeader ( $ApplicationID, $RequestID, "No" );
echo $TemplateObj->displayIrecruitTemplate('views/applicants/PrintChecklist');
?>
