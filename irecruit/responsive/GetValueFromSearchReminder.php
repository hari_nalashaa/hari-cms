<?php
require_once '../Configuration.inc';
if (isset ( $_POST ['searchdate'] ) && isset ( $_POST ['typeofreminder'] ) && $_POST ['searchdate'] != '' && $_POST ['typeofreminder'] != 'Select') {
	
	$selected_date = $_POST ['searchdate'];
	$formated_date = date ( "Y-m-d", strtotime ( $selected_date ) );
	$typeofreminder = $_POST ['typeofreminder'];
	if ($typeofreminder == 'Reminder') {
		
		//Set where condition
		$where = array("UserID = :UserID", "Date(DateTime) = :DateTime");
		//Set parameters
		$params = array(":UserID"=>$USERID, ":DateTime"=>$formated_date);
		//Get AppointmentReminders Information
		$result_row = $AppointmentsObj->getAppointmentReminderInfo("*", $where, "", array($params));
		
		$result_count = $result_row['count'];
		
		if ($result_count >= 1) {
			$i = 0;
			echo "<table><tr><th>SNo</th><th>Subject</th></tr>";
			
			foreach ($result_row['results'] as $row_value) {
				$i ++;
				echo "<tr><td>" . $i . "</td><td>" . $row_value ['Subject'] . "</td></tr>";
			}
			echo "</table>";
		} else {
			echo "<div style='color:red;text-align:center;'>No Records Found</div>";
		}
		
	}
	
	if ($typeofreminder == 'Appoinment') {
		
		//Set where condition
		$where = array("SIUserID = :SIUserID", "SIOrgID = :SIOrgID", "Date(SIDateTime) = :SIDateTime");
		//Set parameters
		$params = array(":SIUserID"=>$USERID, ":SIOrgID"=>$OrgID, ":SIDateTime"=>$formated_date);
		//Get ScheduledInterview Information
		$result_row = $AppointmentsObj->getScheduleInterviewInfo("*", $where, '', '', array($params));
		
		$result_count = $result_row['count'];
		
		if ($result_count >= 1) {
			$i = 0;
			echo "<table><tr><th>SNo</th><th>Subject</th></tr>";
			
			if(is_array($result_row['results'])) {
				foreach($result_row['results'] as $row_value) {
					$i ++;
					echo "<tr><td>" . $i . "</td><td>" . $row_value ['SISubject'] . "</td></tr>";
				}
			}
			
			echo "</table>";
		} else {
			echo "<div style='color:red;text-align:center;'>No Records Found</div>";
		}
	}
} else {
	echo "<div style='color:red;text-align:center;'>All Fields Are Madatory </div>";
}
?>