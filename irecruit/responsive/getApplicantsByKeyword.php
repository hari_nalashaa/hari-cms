<?php
require_once '../Configuration.inc';

$keyword 			= $_POST['applicants_keyword'];
$sort_field 		= $_POST['sort_field'];
$sort_order 		= $_POST['sort_order'];
$request_id 		= $_POST['ddlActiveRequisitions'];

$applicants_info    =   $ApplicationsObj->getApplicantsByKeyword($keyword, $request_id, $sort_field, $sort_order);

$applicants_list    =   $applicants_info["results"];
echo json_encode($applicants_list);
?>