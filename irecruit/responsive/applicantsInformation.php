<?php
require_once '../Configuration.inc';

//Pie Charts
$POST['BeginDate'] = $_POST['BeginDate'];
$POST['FinalDate'] = $_POST['FinalDate'];
$POST['QUERYTYPE'] = $_POST['QUERYTYPE'];

//Applicants by Distinction
if($POST['QUERYTYPE'] == 'AD') {
	$ADInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));
	echo $ADInfo;
}

//Applications by Process Order as Status
if($POST['QUERYTYPE'] == 'APO') {
	$APOInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));
	echo $APOInfo;
}

//Applications by Disposition
if($POST['QUERYTYPE'] == 'ADC') {
	$ADCInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));
	echo $ADCInfo;
}

//Applications Searchable
if($POST['QUERYTYPE'] == 'AS') {
	$ASInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));
	echo $ASInfo;
} ?>