<?php
require_once '../Configuration.inc';

$flag_to_close = "Pie Charts";

require_once 'SetDefaultBlockOrdersConfig.inc';

$POST['BeginDate'] = date('Y-m-d', strtotime("-3 Years"));
$POST['FinalDate'] = date('Y-m-d');

//Applications by Distinction
$POST['QUERYTYPE'] = 'AD';
$ADInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

//Applications by Process Order(Status)
$POST['QUERYTYPE'] = 'APO';
$APOInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

//Applications by Disposition Code
$POST['QUERYTYPE'] = 'ADC';
$ADCInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

//Applications by Searchable
$POST['QUERYTYPE'] = 'AS';
$ASInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

$title = "Applications Pie Charts";

include '../Header.inc';
?>
<div id="page-wrapper">
	<?php include_once '../DashboardPageTitle.inc';?>
    <div class="row">
    	<!-- /.col-lg-12 -->
        <div class="col-lg-12">
	    	<div class="alert alert-info alert-dismissable">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
	            You can drag and drop the blocks to set your own sorting order.
	        </div>
	        <div id="msgsortable" class="alert alert-success" style="display: none"></div>
        </div>
    </div>
    <!-- /.row -->
    <div class="row" id="piechartblocks">
            	<!-- /.col-lg-12 -->
                <?php 
                	foreach($userblockordersinfo as $blockid=>$blockinfo) {
						$listitem = "listitem_".$blockid;
						$blocktitle = $blockinfo['BlockTitle'];
						$piechartid = $piechartids[$blockid];
						
						if($piechartid == "applications-searchable") $ModuleId = "6";
						if($piechartid == "applications-by-distinction") $ModuleId = "5";
						if($piechartid == "applications-by-processorder") $ModuleId = "7";
						if($piechartid == "applications-by-dispositioncode") $ModuleId = "8";
						
						include 'DashboardPieChartApplications.inc';
                	}
                ?>
            </div>
            <!-- /.row -->
</div>
<!-- /#page-wrapper -->
<script type="text/javascript">
var app_by_distinction = <?php echo $ADInfo;?>;
var app_by_processorder  = <?php echo $APOInfo;?>;
var app_by_disposition_code = <?php echo $ADCInfo;?>;	
var app_by_searchable  = <?php echo $ASInfo;?>;
</script>
<?php 
$ISPIECHART = true;
include 'Footer.inc';
?>