<?php
require_once '../Configuration.inc';

$keyword 				=	$_POST['wotc_applicants_keyword'];
$sort_field 			=	$_POST['wotc_sort_field'];
$sort_order 			=	$_POST['wotc_sort_order'];
$qualifies				=	$_POST['ddlQualifies'];
$wotc_srch_from_date	=	$_POST['woct_quicksrch_from_date'];
$wotc_srch_to_date		=	$_POST['woct_quicksrch_to_date'];

$applicants_info    	=   G::Obj('WOTCApplicantData')->getWotcApplicantsByKeyword($keyword, $qualifies, $wotc_srch_from_date, $wotc_srch_to_date, $sort_field, $sort_order);
$applicants_list    	=   $applicants_info["results"];

for($a = 0; $a < count($applicants_list); $a++) {
	if($applicants_list[$a]['Qualifies'] == "Y")
	{
		$applicants_list[$a]['Qualifies'] = "Potentially Qualified";
	}
	if($applicants_list[$a]['Qualifies'] == "N")
	{
		$applicants_list[$a]['Qualifies'] = "Not Qualified";
	}
}

echo json_encode($applicants_list);
?>
