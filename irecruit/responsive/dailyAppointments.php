<?php 
require_once '../Configuration.inc';

$TemplateObj->title = $title = "Daily Appointments";

$POST['DataType'] = 'APPOINTMENTS';
$POST['AppointmentType'] = 'DAY';
$POST['SDay'] = date('d');
$POST['SMonth'] = date('m');
$POST['SYear'] = date('Y');
$POST['SDate'] = date('Y-m-d');
	
$resjson = $AppointmentsObj->getAppointments(json_encode($POST));
$day_appointments = json_decode($resjson, true);

$TemplateObj->appointments = $appointments = $day_appointments[date('j')];
$TemplateObj->count = $count = count($appointments);

echo $TemplateObj->displayIrecruitTemplate('views/responsive/DailyAppointments');
?>