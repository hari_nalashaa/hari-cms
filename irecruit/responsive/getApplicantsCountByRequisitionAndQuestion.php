<?php
require_once '../Configuration.inc';

$applicants_count_info = $ApplicationsObj->getApplicantsCountByQuestionIDAndRequestID($_REQUEST['Question'], $OrgID, $_REQUEST['RequestID'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status']);
$applicants_count_results = $applicants_count_info['results'];

$i = 0;
$row_applications   =   array();
foreach ( $applicants_count_results as $row ) {
    // Please don't assign array directly to $rowapplications,
    // because parsing the data string to integer in front end is complicated than this
    $row_applications [$i] ['label'] = $row ['Answer'];
    $row_applications [$i] ['data'] = ( int ) $row ['ApplicantsCount'];
    $i ++;
}

// If no records return empty string
if ($i == 0)
    echo '';
else
    echo json_encode ( $row_applications );