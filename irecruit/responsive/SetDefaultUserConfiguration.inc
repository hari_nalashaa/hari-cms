<?php 
######################################################################################################
######### Insert default modules and top blocks, update default applicants max date for front end
######################################################################################################
//Retrieve user top blocks information, Insert DefaultTopBlocks if top blocks of user doesn't exist
$dashusertopblocks = $DashboardConfigurationObj->getUserDashboardTopBlocks();

$dashboardusertopblocks = $dashusertopblocks[0];

if ($dashusertopblocks [1] == 0) {
	$DashboardConfigurationObj->insDefaultDashboardTopBlocks();

	$dashusertopblocks = $DashboardConfigurationObj->getUserDashboardTopBlocks();
	$dashboardusertopblocks = $dashusertopblocks[0];
}

$TemplateObj->dashboardusertopblocks = $dashboardusertopblocks;

// Insert the default user modules to DashboardUsersModule if not exist
$DashboardConfigurationObj->insUserDashboardModules ();

$moduleconfig = $DashboardConfigurationObj->selModuleUserConfig();
$TemplateObj->moduleconfig = $moduleconfig;
#######################################################################################################
?>