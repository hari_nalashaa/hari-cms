<div id="wrapper">

	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation"
		style="margin-bottom: 0">
		<?php include 'Logo.inc';?>
		<!-- /.navbar-header -->
			
		<?php include 'TopRightMenuLinks.inc';?>
        <!-- /.navbar-top-links -->
			
        <?php include 'LeftSideBar.inc';?>
        <!-- /.navbar-static-side -->
	</nav>