<?php
require_once '../Configuration.inc';

$TemplateObj->title = $title = "Daily Reminders";

//Reminders
$POST['DataType'] = 'REMINDERS';
$POST['ReminderType'] = 'DAY';
$POST['SDay'] = date('d');
$POST['RMonth'] = date('m');
$POST['RYear'] = date('Y');
$POST['SDate'] = date('Y-m-d');

$resjson = $RemindersObj->getReminders(json_encode($POST));
$day_reminders = json_decode($resjson,true);

$TemplateObj->reminders = $reminders = $day_reminders[date('j')];
$TemplateObj->count = $count = count($reminders);

echo $TemplateObj->displayIrecruitTemplate('views/responsive/DailyReminders');
?>