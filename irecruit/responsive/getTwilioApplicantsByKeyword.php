<?php
require_once '../Configuration.inc';

$keyword 			=	$_POST['twilio_applicants_keyword'];
$sort_field 		=	$_POST['twilio_sort_field'];
$sort_order 		=	$_POST['twilio_sort_order'];

//Set from, to dates
$from_date 			= 	G::Obj('DateHelper')->getYmdFromMdy($_POST['twilio_search_from_date']);
$to_date 			= 	G::Obj('DateHelper')->getYmdFromMdy($_POST['twilio_search_to_date']);

$applicants_info    =   G::Obj('Applications')->getTwilioApplicantsByKeyword('', $OrgID, $keyword, $sort_field, $sort_order, $from_date, $to_date);

$applicants_list    =   $applicants_info["results"];

//Add the conversation related information to applicants_list
for($al = 0; $al < count($applicants_list); $al++) {
	$ApplicationID		=	$applicants_list[$al]['ApplicationID'];
	$RequestID			=	$applicants_list[$al]['RequestID'];
	
	//Get applicant cellphone
	$cell_phone_info	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphone');
	$cell_phone			=	json_decode($cell_phone_info, true);
	$applicant_number	=	"+1".$cell_phone[0].$cell_phone[1].$cell_phone[2];

	//Get twilio number
	$user_details		=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
	$user_cell_number	=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);
	
	$conversation_info	=	G::Obj('TwilioConversationInfo')->getUserConversationResources($OrgID, $USERID, $applicant_number, $user_cell_number);
	$ResourceID			=	$conversation_info[0]['ResourceID'];

	//Get applicant permission
	$cell_phone_permission	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphonepermission');
	
	$twilio_agreement_info  =   G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($OrgID);
	
	if($twilio_agreement_info['Active'] != 'Y') {
	    $cell_phone_permission	=	"No";
	}

	$applicants_list[$al]['CellPhonePermission']    =	$cell_phone_permission;
    $applicants_list[$al]['ConversationInfo']       =	json_encode($conversation_info);
    $applicants_list[$al]['ResourceID']             =	$ResourceID;
}

echo json_encode($applicants_list);
?>