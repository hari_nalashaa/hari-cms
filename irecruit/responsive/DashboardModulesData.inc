<?php
// Retrieve all user modules with ModuleId as Key
$dashboardusermodules = $DashboardConfigurationObj->getUserDashboardModules();
$TemplateObj->dashboardmodules = $DashboardConfigurationObj->getDashboardModules();

$TemplateObj->usermodules       =   $usermodules		= $dashboardusermodules[0];
$TemplateObj->usermodulesinfo   =   $usermodulesinfo    = $dashboardusermodules[2];
$TemplateObj->usermoduleidinfo  =   $usermoduleidinfo   = $dashboardusermodules[3];

if($user_preferences['DashboardWidgetsDateRange'] == "") $user_preferences['DashboardWidgetsDateRange'] = "30";

if(isset($usermodules['1']) && $usermodules['1'] == "1") {
    //Requisitions
    $JSON['DataType'] = 'REQUISITIONS';
    $JSON['Active'] = 'Y';
    $JSON['LIMIT'] = " LIMIT 5";
    $resjson = $RequisitionsObj->getRequisitions(json_encode($JSON));
    $TemplateObj->requisitions = json_decode($resjson,true);
}

//Appointments
$JSON['DataType'] = 'APPOINTMENTS';
$JSON['SMonth'] = date('m');
$JSON['SYear'] = date('Y');
$JSON['SDate'] = date('Y-m-d');
$resjson = $AppointmentsObj->getAppointments(json_encode($JSON));
$TemplateObj->appointments = json_decode($resjson,true);
  

//Reminders
$JSON['DataType'] = 'REMINDERS';
$JSON['RMonth'] = date('m');
$JSON['RYear'] = date('Y');
$JSON['Sort'] = ' ORDER BY DateTime ASC';
$resjson = $RemindersObj->getReminders(json_encode($JSON));
$TemplateObj->reminders = json_decode($resjson,true);

//Pie Charts
$POST['BeginDate'] = date('m/d/Y', strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days"));
$POST['FinalDate'] = date('m/d/Y');

$POST['QUERYTYPE'] = 'AD';
$TemplateObj->POST = $POST;
$TemplateObj->ADInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

$POST['QUERYTYPE'] = 'APO';
$TemplateObj->POST = $POST;
$TemplateObj->APOInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

$POST['QUERYTYPE'] = 'ADC';
$TemplateObj->POST = $POST;
$TemplateObj->ADCInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

$POST['QUERYTYPE'] = 'AS';
$TemplateObj->POST = $POST;
$TemplateObj->ASInfo = $ApplicationsObj->getApplicationsInfo(json_encode($POST));

if(isset($usermodules['17']) && $usermodules['17'] == "1") {
    //Applicants By Source
    $apps_count_info = $ApplicationsObj->getApplicantsCountByQuestionIDAndRequestID('informed', $OrgID, '', date('m/d/Y', strtotime("-".$user_preferences['DashboardWidgetsDateRange']." Days")), date('m/d/Y'), '');
    $apps_count_results = $apps_count_info['results'];
    
    $app_ref_src = 0;
    $row_applications   =   array();
    foreach ( $apps_count_results as $app_ref_row ) {
        // Please don't assign array directly to $rowapplications,
        // because parsing the data string to integer in front end is complicated than this
        $row_applications [$app_ref_src] ['label'] = $app_ref_row ['Answer'];
        $row_applications [$app_ref_src] ['data'] = ( int ) $app_ref_row ['ApplicantsCount'];
        $app_ref_src ++;
    }
    
    // If no records return empty string
    if ($app_ref_src == 0) {
        $TemplateObj->applicants_by_source = $applicants_by_source = '';
    }
    else {
        $TemplateObj->applicants_by_source = $applicants_by_source = json_encode ( $row_applications );
    }
}

if(isset($usermodules['2']) && $usermodules['2'] == "1") {
    //Applicants
    $applicants = '';
    $JSON['DataType'] = 'APPLICANTS';
    $TemplateObj->JSON = $JSON;
    if(is_object($ApplicantsObj)) {
        $resjson = $ApplicantsObj->getApplicants(json_encode($JSON));
        $TemplateObj->applicants = json_decode($resjson,true);
    }
}

/**
 * Set the horizantal BarData
 */
if(count($_POST) > 0) {
	$POST['RequestID'] = implode("','", $_POST['vrequisitions']);
}
$appproccessbyreq = $ApplicationsObj->daysToCompletion(json_encode($POST));

$TemplateObj->barhgraphdata = $appproccessbyreq[0];
$TemplateObj->barhgraphyayis = $appproccessbyreq[1];


/**
 * Set the vertical BarData
 */
if(count($_POST) > 0) {
	$POST['RequestID'] = implode("','", $_POST['vrequisitions']);
}

$appbyreq = $ApplicationsObj->getApplicationsByRequisition(json_encode($POST));

$TemplateObj->barvgraphdata = $appbyreq[0];
$TemplateObj->barvgraphxaxis  = $appbyreq[1];

//Set the custom 
$TemplateObj->custom_links_info = $CustomLinksObj->getCustomLinks();

//Zip Recruiter
$Active =   "Y";
$TemplateObj->ZipRecruiterReqLimit = $ZipRecruiterReqLimit = $Limit  =   "10";
$IsRequisitionApproved = "Y";

$TemplateObj->FreeZipRecruiter      =   $FreeZipRecruiter       =   "Yes";
$TemplateObj->PaidZipRecruiter      =   $PaidZipRecruiter       =   "Yes";
$TemplateObj->SubscribeZipRecruiter =   $SubscribeZipRecruiter  =   "Yes";

//Get Requisitions Info
require_once IRECRUIT_DIR . 'services/GetRequisitions.inc';

$TemplateObj->zip_recruiter_req_info = $zip_recruiter_req_info = json_decode($requisitions_info, true);
?>