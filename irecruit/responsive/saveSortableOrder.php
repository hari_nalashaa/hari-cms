<?php 
require_once '../Configuration.inc';

global $USERID;
$Order = 1;

if(isset($_POST['type']) && $_POST['type'] == "pie") {
	$POST['USERID'] = $USERID;
	foreach($_POST['listitem'] as $BlockID) {
		$POST['BlockID'] = $BlockID;
		$POST['BlockOrder'] = $Order;
		$POST['BlockType']	= "PIECHARTS";
		$DashboardConfigurationObj->updBlockOrders($POST);
		$Order++;
	}
}
else if(isset($_POST['type']) && $_POST['type'] == "topblocks") {
	$POST['USERID'] = $USERID;
	foreach($_POST['listitem'] as $BlockID) {
		$POST['BlockId'] = substr ( $BlockID , 0, 1 );
		$POST['BlockOrder'] = $Order;
		$POST['BlockPanelColor'] = substr ( $BlockID , 1, 1 );
		
		$DashboardConfigurationObj->updTopBlocksOrder($POST);
		$Order++;
	}
}
else {
	$POST['USERID'] = $USERID;
	foreach($_POST['listitem'] as $ModuleId) {
		$POST['ModuleId'] = $ModuleId;
		$POST['ModuleOrder'] = $Order;
		$DashboardConfigurationObj->updDashboardUserModuleOrder($POST);
		$Order++;
	}
}