<?php
require_once 'Configuration.inc';

$inventory_details = array ();

include_once IRECRUIT_DIR . 'requisitions/JobBoardPostingPageTitles.inc';
include_once COMMON_DIR . 'listings/SocialMedia.inc';

$active_tab     = "purchase-zip-recruiter-feed";

if(isset($_SESSION['social_tab']) && $_SESSION['social_tab'] ==1){
$active_tab     = "socialmedia-requisition";  
unset($_SESSION['social_tab']);
}else{
if(isset($_REQUEST['active_tab']) && $_REQUEST['active_tab'] != "") {
    $active_tab = $_REQUEST['active_tab'];
}
}
$TemplateObj->active_tab = $active_tab;

//Set page title
$TemplateObj->title                         =   $title;
$TemplateObj->GuIDRequisition               =   $GuIDRequisition            =   $GenericLibraryObj->getGlobalUniqueID($OrgID);
$TemplateObj->ZipRecruiterJobCategories     =   $ZipRecruiterJobCategories  =   $ZipRecruiterObj->getZipRecruiterJobCategories();
//Get labels list
$TemplateObj->labels_list                   =   $labels_list                =   $LabelsObj->getAllLabels($OrgID);

if(isset($_GET['jobboard_apply']))
{
    if($_GET['jobboard_apply'] == "FreeIndeed") $JobBoardList = "Indeed";
    else if($_GET['jobboard_apply'] == "FreeMonster") $JobBoardList = "Monster";
    else if($_GET['jobboard_apply'] == "ClearFreeIndeed") $JobBoardList = "ClearIndeed";
    else if($_GET['jobboard_apply'] == "ClearFreeMonster") $JobBoardList = "ClearMonster";
    
    if(isset($_POST['chk_req'])) {
        $reqs_list = "";
    	foreach ($_POST['chk_req'] as $ChkReqID=>$ChkReqValue) {

    	    if($JobBoardList == "ClearIndeed" || $JobBoardList == "ClearMonster") {
    	        
    	        $req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $ChkReqID);
    	        $free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);

    	        $new_free_jobboard_list = array();
    	        if($JobBoardList == "ClearIndeed") {
    	           for($a = 0; $a < count($free_jobboard_list); $a++) {
    	               if($free_jobboard_list[$a] != "Indeed") {
    	               		$new_free_jobboard_list[] = $free_jobboard_list[$a];
    	               }
    	           }    
    	        }
    	        else if($JobBoardList == "ClearMonster") {
    	           for($a = 0; $a < count($free_jobboard_list); $a++) {
    	               if($free_jobboard_list[$a] != "Monster") {
    	                   $new_free_jobboard_list[] = $free_jobboard_list[$a];
    	               } 
    	           }
    	        }
    	        
    	        $new_free_jobboard_list = array_unique($new_free_jobboard_list);
    	        
    	        $where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
    	        $params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$ChkReqID, ":FreeJobBoardLists"=>json_encode($new_free_jobboard_list));
    	        $set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists");
    	        
    	        $result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
    	         
    	    }
    	    else {

    	        $columns   =   "FreeJobBoardLists";
    	        if($JobBoardList == "Monster") {
    	            $columns   =   "MonsterJobCategory, MonsterJobOccupation, MonsterJobPostedDate, MonsterJobPostType, FreeJobBoardLists";
    	        }
    	        
    	        $req_info           =   $RequisitionsObj->getRequisitionsDetailInfo($columns, $OrgID, $ChkReqID);
    	        $free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);
    	        	
    	        $free_jobboard_list[]  = $JobBoardList;
    	        $free_jobboard_list    = array_unique($free_jobboard_list);

    	        if($JobBoardList == "Monster") {
    	            if($req_info['MonsterJobCategory'] != ""
    	                && $req_info['MonsterJobOccupation'] != ""
	                    && $req_info['MonsterJobPostedDate'] != ""
                        && $req_info['MonsterJobPostType'] != "")   {
    	            
    	                $where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
    	                $params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$ChkReqID, ":FreeJobBoardLists"=>json_encode($free_jobboard_list));
    	                $set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists");
    	                 
    	                $result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
    	            
    	            }
    	            else {
    	            	$reqs_list .= "&rs[]=".$ChkReqID;
    	            } 
    	        }
    	        else {
    	            $where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
    	            $params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$ChkReqID, ":FreeJobBoardLists"=>json_encode($free_jobboard_list));
    	            $set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists");
    	             
    	            $result         =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
    	        }
    	         
    	    }
    	    
    	}
    }

    $suc_msg = "";
    if($JobBoardList == "Indeed") {
        $suc_msg = "appsucindeed";
    }
    else if($JobBoardList == "Monster") {
        $suc_msg = "appsucmonster".$reqs_list;
    }
    else if($JobBoardList == "ClearIndeed") {
        $suc_msg = "appsucindeedcleared";
    }
    else if($JobBoardList == "ClearMonster") {
        $suc_msg = "appsucmonstercleared";
    }

    header("Location:jobBoardPosting.php?menu=3&msg=".$suc_msg);
    exit;
}

$new_job_image = '';
$hot_job_image = '';
// update new job status
$highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);

if($highlight_req_settings['HighlightNewJob'] == "Yes") {
    $new_job_days = $highlight_req_settings['NewJobDays'];
    $new_job_icon = $HighlightRequisitionsObj->getJobImage($highlight_req_settings['NewJobIcon']);

    if($new_job_icon != "") {
        $new_job_image = '<img src="'.IRECRUIT_HOME.'vault/highlight/'.$new_job_icon.'">';
    }
}

if($highlight_req_settings['HighlightHotJob'] == "Yes") {
    $hot_job_icon = $HighlightRequisitionsObj->getJobImage($highlight_req_settings['HotJobIcon']);

    if($hot_job_icon != "") {
        $hot_job_image = '<img src="'.IRECRUIT_HOME.'vault/highlight/'.$hot_job_icon.'">';
    }
}

//Include configuration related javascript in header
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$scripts_header[] = "js/irec_Display.js";
$scripts_header[] = "js/loadAJAX.js";
$TemplateObj->page_scripts_header = $scripts_header;

//Set footer scripts
$scripts_footer[] = "js/job-board-requisition.js";
$scripts_footer[] = "js/job-board-requisitions-search.js";

$script_vars_footer [] = 'var highlight_hot_jobs = "'.$highlight_req_settings['HighlightHotJob'].'";';
$script_vars_footer [] = "var new_job_image = '".$new_job_image."';";
$script_vars_footer [] = "var hot_job_image = '".$hot_job_image."';";

$script_vars_header [] = "var labels_list = '".json_encode($labels_list)."';";
$script_vars_header [] = "var zip_recruiter_job_categories = '".json_encode($ZipRecruiterJobCategories)."';";


$TemplateObj->scripts_vars_footer = $script_vars_footer;
$TemplateObj->script_vars_header = $script_vars_header;

$TemplateObj->page_scripts_footer = $scripts_footer;

$page_styles["header"][] = 'css/requisitions-search-print.css';

//Set page styles information
$TemplateObj->page_styles =  $page_styles;

$Active = 'Y';
if(isset($_REQUEST['Active']) && $_REQUEST['Active'] != "") {
    $Active = $_REQUEST['Active'];
}
 
$TemplateObj->Active = $Active;

$TemplateObj->MonsterJobIndustries 	= $MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();
$TemplateObj->JobCategories 		= $JobCategories 		= $MonsterObj->monsterJobCategories("yes");
$TemplateObj->JobOccupations 		= $JobOccupations 		= $MonsterObj->getMonsterJobCategoriesOccupations();

// update new job status
$highlight_req_settings                 = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
$TemplateObj->highlight_req_settings    = $highlight_req_settings;

$TemplateObj->new_job_image = $new_job_image;
$TemplateObj->hot_job_image = $hot_job_image;

if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
    require_once IRECRUIT_DIR . 'requisitions/GetRequisitionInfo.inc';

} else {
    require_once IRECRUIT_DIR . 'requisitions/GetRequisitions.inc';
}

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/JobBoardPosting');
?>
