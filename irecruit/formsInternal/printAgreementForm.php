<?php 
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

$TemplateObj->ApplicationID     =   $ApplicationID      =   $_REQUEST['ApplicationID'];
$TemplateObj->RequestID         =   $RequestID          =   $_REQUEST['RequestID'];
$TemplateObj->AgreementFormID   =   $AgreementFormID    =   $_REQUEST['AgreementFormID'];
$TemplateObj->title             =   $title              =  "Agreement Form View";
$TemplateObj->displayFormHeader =   $displayFormHeader  =   displayHeader ( $ApplicationID, $RequestID, "No" );

if($ServerInformationObj->getRequestSource() == 'ajax') {
    //view for AddApplicant.inc
    require_once COMMON_DIR . 'formsInternal/AgreementFormView.inc';
}
else {
    //view for AddApplicant.inc
    $TemplateObj->default_view_dir = COMMON_DIR;
    echo $TemplateObj->displayIrecruitTemplate('formsInternal/AgreementFormView');
}
?>