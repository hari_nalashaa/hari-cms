<?php
//set where condition
$where = array("OrgID = :OrgID", "FormStatus = :FormStatus");
//set parameters
$params = array(":OrgID"=>$OrgID, ":FormStatus"=>'Active');

if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Active')
{
    //set where condition
    $where = array("OrgID = :OrgID", "FormStatus = :FormStatus");
    //set parameters
    $params = array(":OrgID"=>$OrgID, ":FormStatus"=>'Active');
}
else if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Inactive')
{
    //set where condition
    $where = array("OrgID = :OrgID", "FormStatus = :FormStatus");
    //set parameters
    $params = array(":OrgID"=>$OrgID, ":FormStatus"=>'Inactive');
}

$results = $FormsInternalObj->getAgreementFormsInfo("*", $where, "FormPart, FormName", array($params));
$cnt = $results['count'];

echo '<div id="process_agreement_form_status"></div>';
echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table table-striped table-bordered">';

echo '<tr>';

echo '<td colspan="3">Form Status:';
echo '<select name="ddlFormStatus" id="ddlFormStatus" onchange="getAgreementFormsByStatus(this.value)">';

echo '<option value="Active"';
if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Active')
{
    echo ' selected="selected"';
}
echo '>Active</option>';
echo '<option value="Inactive"';
if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Inactive')
{
    echo ' selected="selected"';
}
echo '>Inactive</option>';
echo '</select>';
echo '</td>';

echo '</tr>' . "\n";
echo '</table>';

echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table table-striped table-bordered">';
if ($cnt > 0) {
	echo '<tr>';
	echo '<td width="200"><b>Type of Form</b></td>';
	echo '<td width="330"><b>Form Name</b></td>';
	echo '<td width="330"><b>Status</b></td>';//Sort Order
	if(substr ( $USERROLE, 0, 21 ) == 'master_admin') {
	    echo '<td align="center" width="60"><b>Active</b></td>';
	}
	if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
		echo '<td align="center" width="60"><b>Copy</b></td>';
		echo '<td align="center" width="60"><b>Edit</b></td>';
		if($_REQUEST['ddlFormStatus'] != "Deleted") {
		    echo '<td align="center" width="60"><b>Delete</b></td>';
		}
	} // end permit Internal_Forms_Edit
	echo '<td align="center" width="60"><b>View<br>Form</b></td>';
	echo '</tr>' . "\n";
}

$rowcolor = "#eeeeee";
if(is_array($results['results'])) {
	foreach ($results['results'] as $AFN) {
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>';
		// echo 'Agreement Form';
		echo $AFN ['FormPart'];
		echo '</td>';
		echo '<td>';
		echo $AFN ['FormName'];
		echo '</td>';
	
		echo '<td>';
		echo $AFN ['FormStatus'];
		//echo '<input type="hidden" ';
		//echo 'size="2" maxlength="2" value="' . $AFN ['SortOrder'] . '"';
		//echo ' onchange=\'updateInterAssignedFormSortOrder("' . $OrgID . '", "' . $AFN ['AgreementFormID'] . '", "AgreementForm", this.value)\'>';
		echo '</td>';
		
		if(substr ( $USERROLE, 0, 21 ) == 'master_admin') {
            echo '<td align="center" width="60">';
            echo '<input type="checkbox" name="chkAgreementFormID'.$AFN ['AgreementFormID'].'" id="chkAgreementFormID'.$AFN ['AgreementFormID'].'" value="'.$AFN ['AgreementFormID'].'" onclick=\'updateAgreementFormStatus("'.$AFN ['AgreementFormID'].'", this.checked)\'';
            if(isset($AFN ['FormStatus']) && $AFN ['FormStatus'] == "Active") {
                echo ' checked="checked"';
            }
            echo  '>';
            echo '</td>'; 
		}
		
		
		if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
	
			echo '<td align="center">';
			echo '<a href="' . $formscript . '?typeform=agreementforms&action=copy&AgreementFormID=' . $AFN ['AgreementFormID'] . '" onclick="return confirm(\'Are you sure you want to copy the following form?\n\n' . $AFN ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a>';
			echo '</td>';
	
			echo '<td align="center">';
			echo '<a href="' . $formscript . '?typeform=agreementforms&action=edit&AgreementFormID=' . $AFN ['AgreementFormID'] . '">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit">';
			echo '</a>';
			echo '</td>';
	
			if($_REQUEST['ddlFormStatus'] != "Deleted") {
			    echo '<td align="center">';
			    echo '<a href="' . $formscript . '?typeform=agreementforms&action=delete&AgreementFormID=' . $AFN ['AgreementFormID'] . '" onclick="return confirm(\'Are you sure you want to delete the following form?\n\n' . $AFN ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
			    echo '</td>';
			}
			
		} // end permit Internal_Forms_Edit
	
		echo '<td align="center">';
	
		$link = IRECRUIT_HOME . "formsInternal/completeAgreementForm.php?AgreementFormID=" . $AFN ['AgreementFormID'];
		if ($typeform) {
			$link .= "&typeform=" . $typeform;
		}
		if ($AccessCode != "") {
			$link .= "&k=" . $AccessCode;
		}
	
		echo '<a href="#" onclick="javascript:window.open(\'' . $link . '\',\'_blank\',\'location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes\');"><img src="' . IRECRUIT_HOME . 'images/icons/application_form.png" border="0" title="View Form" style="margin:0px 3px -4px 0px;"></a>';
		echo '</td>';
	
		echo '</tr>';
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}

if ($cnt == 0) {
	
	echo '<tr>';
	echo '<td style="text-align:left;width:770px;height:60px;">';
	echo 'There are no forms assigned.';
	echo '</td>';
	echo '</tr>';
}

echo '</table>';
?>