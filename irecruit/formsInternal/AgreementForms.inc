<?php
if ($feature ['AgreementForms'] != "Y") {
	include IRECRUIT_DIR . 'irecruit_permissions.err';
} // end feature
else {
    
    $print = "Y";
    
    if ($action) {
        
        if ($action == "new") {
            
            if ($_POST ['formtitle'] == "") {
                $msg = "The following information is missing.\\n\\n";
                $msg .= " - Title is missing.\\n\\n";
                $displaybox = "Y";
            } else {
                //set where condition
                $where = array("OrgID = :OrgID", "FormName = :FormName");
                //set parameters
                $params = array(":OrgID"=>$OrgID, ":FormName"=>$_POST ['formtitle']);
                //get agreement forms information
                $results = G::Obj('FormsInternal')->getAgreementFormsInfo("*", $where, "", array($params));
                
                if ($results['count'] > 0) {
                    $msg = "The following information is missing.\\n\\n";
                    $msg .= " - Title is not unique.\\n\\n";
                    $displaybox = "Y";
                }
            }
            
            if (! $msg) {
                
                $AgreementFormID = uniqid ();
                
                if ($_POST['FormPart'] == "Primary") {
                    
                    //set where condition
                    $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
                    //set parameters
                    $params = array(":OrgID"=>'MASTER', ":AgreementFormID"=>'Primary');
                    //set columns
                    $columns = "'$OrgID', '$AgreementFormID', QuestionID, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
                    
                    $results_copy = G::Obj('FormQuestions')->getQuestionsInformation('AgreementFormQuestions', "Question", $where, "", array($params));
                    
                    if($results_copy['count'] == 0) {
                        //insert agreement form questions
                        G::Obj('FormQuestions')->insFormQuestionsMaster('AgreementFormQuestions', $columns, $where, array($params));
                    }
                    
                    //Insert Agreement Forms Information
                    $insert_info = array("OrgID"=>$OrgID, "AgreementFormID"=>$AgreementFormID, "FormName"=>$_POST ['formtitle'], "FormPart"=>'Primary');
                    G::Obj('FormsInternal')->insFormData('AgreementForms', $insert_info);
                    
                    $msg = "You have created a new primary agreement form.\\n\\n";
                } // end Primary
                
                if ($_POST['FormPart'] == "Secondary") {
                    
                    //set where condition
                    $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
                    //set parameters
                    $params = array(":OrgID"=>'MASTER', ":AgreementFormID"=>'Secondary');
                    //set columns
                    $columns = "'$OrgID', '$AgreementFormID', QuestionID, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
                    
                    $results_copy = G::Obj('FormQuestions')->getQuestionsInformation('AgreementFormQuestions', "Question", $where, "", array($params));
                    
                    if($results_copy['count'] == 0) {
                        //insert agreement form questions
                        G::Obj('FormQuestions')->insFormQuestionsMaster('AgreementFormQuestions', $columns, $where, array($params));
                    }
                    
                    //Insert Agreement Forms Information
                    $insert_info = array("OrgID"=>$OrgID, "AgreementFormID"=>$AgreementFormID, "FormName"=>$_POST ['formtitle'], "FormPart"=>'Secondary', "LinkedTo"=>$_POST ['LinkedTo']);
                    G::Obj('FormsInternal')->insFormData('AgreementForms', $insert_info);
                    
                    $msg = "You have created a new secondary agreement form.\\n\\n";
                } // end Secondary
            }
            
            if ($msg) {
                echo '<script language="JavaScript" type="text/javascript">';
                echo "alert('" . $msg . "')";
                echo '</script>';
            }
        } else if ($action == "edit") {
            
            $print = "N";
        } else if ($action == "copy") {
            
            $newAgreementFormID = uniqid ();
            
            //set where condition
            $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_GET ['AgreementFormID']);
            //set columns
            $columns = "'$OrgID', '$newAgreementFormID', QuestionID, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
            //insert agreement form questions
            G::Obj('FormQuestions')->insFormQuestionsMaster('AgreementFormQuestions', $columns, $where, array($params));
            
            //set where condition
            $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_GET ['AgreementFormID']);
            $results = G::Obj('FormsInternal')->getAgreementFormsInfo("*", $where, "FormName LIMIT 0,1", array($params));
            $AFN = $results['results'][0];
            
            $newformtitle = $AFN ['FormName'] . " Copy";
            
            //Insert Agreement Forms Information
            $insert_info = array("OrgID"=>$OrgID, "AgreementFormID"=>$newAgreementFormID, "FormName"=>$newformtitle, "FormPart"=>$AFN ['FormPart'], "LinkedTo"=>$AFN ['LinkedTo']);
            G::Obj('FormsInternal')->insFormData('AgreementForms', $insert_info);
            
            $msg = "You have created a new agreement form from " . $AFN ['FormName'] . ".\\n\\n";
            
            echo '<script language="JavaScript" type="text/javascript">';
            echo "alert('" . $msg . "')";
            echo '</script>';
            
            $print = "Y";
            
        } else if ($action == "delete") {
            
            $delcnt = 0;
            
            //set where condition
            $where = array("OrgID = :OrgID", "LinkedTo = :LinkedTo");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":LinkedTo"=>$_GET ['AgreementFormID']);
            //set agreement forms information
            $results = G::Obj('FormsInternal')->getAgreementFormsInfo("*", $where, "", array($params));
            
            $delcnt += $results['count'];
            
            
            //set where condition
            $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
            //set parameters
            $params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_GET ['AgreementFormID']);
            //set agreement forms information
            $results = G::Obj('FormsInternal')->getAgreementFormsInfo("FormName", $where, "FormName LIMIT 0, 1", array($params));
            $FormName = $results['results'][0]['FormName'];
            
            if ($delcnt == 0) {
                
                //set where condition
                $where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
                //set parameters
                $params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_GET ['AgreementFormID']);
                //set information
                $set_info = array("FormStatus = 'Deleted'");
                //update agreement forms
                G::Obj('FormsInternal')->updAgreementForms($set_info, $where, array($params));
                
                $msg = "You have deleted " . $FormName . ".\\n\\n";
            } else {
                
                $msg = "You cannot delete " . $FormName . " because a Secondary Form depends on it.\\n\\n";
            } // end delcnt
            
            echo '<script language="JavaScript" type="text/javascript">';
            echo "alert('" . $msg . "')";
            echo '</script>';
            
            $print = "Y";
        } // end delete
    } // end action
    
    
    echo '<br>';
    
    if ($print == "Y") {
        
       $dis = "none";
        if ($displaybox == "Y") {
            $dis = "block";
        } // end msg
        echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered"  style="">';
        
        if (substr ( $USERROLE, 0, 21 ) == 'master_admin') {
            echo '<tr>';
            echo '<td width="100%">';
            echo '<a href="javascript:ReverseDisplay(\'AddNew\')">';
            echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">';
            echo '<b style="FONT-SIZE:8pt;COLOR:#000000">';
            echo 'Add Agreement Form';
            echo '</b>';
            echo '</a>';
            echo '</td>';
            echo '</tr>';
        }
        
       
        
        echo '<tr><td>';
        
        //set where condition
        $where      =   array("OrgID = :OrgID", "FormPart = 'Primary'");
        //set parameters
        $params     =   array(":OrgID"=>$OrgID);
        //set agreement forms information
        $results    =   G::Obj('FormsInternal')->getAgreementFormsInfo("AgreementFormID, FormName", $where, "FormName", array($params));
        $PCnt       =   $results['count'];
        
        echo '<form method="post" action="' . $formscript . '">';
        echo '<div id="AddNew" style="display:' . $dis . ';">';
        
        echo '<div style="float:left;padding-left:20px;">';
        echo 'Title: <input type="text" class="form-control width-auto-inline" name="formtitle" size="30" maxlength="45" value="">';
        echo '&nbsp;&nbsp;';
        echo 'Form Part: ';
        echo '<select name="FormPart" onChange="DisplayPrimaryForms(this.value,\'PrimaryForms\')" class="form-control width-auto-inline">';
        echo '<option value="Primary">Primary</option>';
        if ($PCnt > 0) {
            echo '<option value="Secondary">Secondary</option>';
        }
        echo '</select>';
        echo '</div>';
        
        echo '<div id="PrimaryForms" style="display:none;float:left;">';
        echo '&nbsp;&nbsp;&nbsp;';
        echo 'Link to Form: ';
        echo '<select name="LinkedTo" class="form-control width-auto-inline">';
        
        if(is_array($results['results'])) {
            foreach ($results['results'] as $AFN) {
                echo '<option value="' . $AFN ['AgreementFormID'] . '">' . $AFN ['FormName'] . '</option>';
            } // end foreach
        }
        
        echo '</select>';
        echo '</div>';
        
        echo '<div style="float: left;margin-left:20px;">';
        echo '<input type="hidden" name="action" value="new">';
        echo '<input type="hidden" name="typeform" value="agreementforms">';
        echo '<input type="submit" value="Add New Form" class="btn btn-primary">';
        echo '</div>';
        
        echo '</div>';
        echo '</form>';
        echo '</td></tr>';
        echo '</table>';
        
        if (0) {
            echo '<tr><td><a href="javascript:ReverseDisplay(\'' . $IF ['AgreementFormID'] . '\')">';
            echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">';
            echo $IF ['FormType'];
            echo '</b></a></td></tr>';
            
            $dis = "none";
            if ($displaybox == "Y") {
                if ($_POST ['AgreementFormID'] == $IF ['AgreementFormID']) {
                    $dis = "block";
                }
            } // end msg
            
            echo '<tr><td><div id="' . $IF ['AgreementFormID'] . '" style="display:' . $dis . ';">';
            
            echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';
            echo '<form method="post" action="' . $formscript . '">';
            
            echo '<tr><td width="15"></td><td>Title: <input type="text" name="formtitle" size="30" maxlength="45" value="">' . "</td></tr>";
            echo '<tr><td></td><td><input type="submit" value="Add ' . $IF ['FormType'] . '"></td></tr>';
            
            echo '<input type="hidden" name="action" value="new">';
            echo '<input type="hidden" name="typeform" value="agreementforms">';
            echo '<input type="hidden" name="AgreementFormID" value="' . $IF ['AgreementFormID'] . '">';
            echo '</form>';
            echo '</table>';
            
            echo '</div></td></tr>';
        } // end if 0
        
        echo '<br>';
        include IRECRUIT_DIR . 'formsInternal/DisplayAgreementFormsList.inc';
    } else { // else print
        
        include IRECRUIT_DIR . 'formsInternal/EditAgreementForms.inc';
    } // end if print
}
?>