<?php
require_once '../irecruitdb.inc';


if (isset ( $_REQUEST ['OrgID'] ) && isset ( $_REQUEST ['PreFilledFormID'] )) {

	$OrgID = $_REQUEST ['OrgID']; // Assign OrgID
	
	require_once IRECRUIT_DIR . 'formsInternal/WritePDF.inc';
	
	$dfile = '';
	
	if (isset ( $_REQUEST ['ApplicationID'] ) && $_REQUEST ['ApplicationID'] != "" && isset ( $_REQUEST ['RequestID'] ) && $_REQUEST ['RequestID'] != "") {

	    // Create Directories
	    $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
	    if (! file_exists ( $dir )) {
	        mkdir ( $dir, 0700 );
	        chmod ( $dir, 0777 );
	    }
	    
	    $dir .= "/applicantvault";
	    if (! file_exists ( $dir )) {
	        mkdir ( $dir, 0700 );
	        chmod ( $dir, 0777 );
	    }
	    
	    $dir .= "/internalforms";
	    if (! file_exists ( $dir )) {
	        mkdir ( $dir, 0700 );
	        chmod ( $dir, 0777 );
	    }
	     
		$dfile = createPDFi9 ($dir, $_REQUEST ['PreFilledFormID'], $_REQUEST ['ApplicationID'], $_REQUEST ['RequestID'] );
	} else {
		
		// set where condition
		$where = array (
				"PreFilledFormID = :PreFilledFormID" 
		);
		// set parameters
		$params = array (
				":PreFilledFormID" => $_REQUEST ['PreFilledFormID'] 
		);
		// get prefilled forms information
		$results = $FormsInternalObj->getPrefilledFormsInfo ( "TypeForm, Form", $where, "", array (
				$params 
		) );
		// assign array information to TypeForm, Form
		list ( $TypeForm, $Form ) = @array_values ( $results ['results'] [0] );
		
		$dfile = IRECRUIT_DIR . 'formsInternal/PDFForms/' . preg_replace ( '/\s/', '_', $TypeForm ) . '/' . preg_replace ( '/\s/', '_', $Form ) . '.pdf';
	} // end ApplicationID

	if (file_exists ( $dfile )) {
		$browser = get_browser( null, true );
		
				
		if ($browser['browser'] == "IE") {
			
			header ( 'Content-Description: File Transfer' );
			header ( 'Content-Type: application/octet-stream' );
			header ( 'Content-Disposition: inline; filename="' . $dfile . '"' );
			header ( 'Content-Transfer-Encoding: binary' );
			header ( 'Expires: 0' );
			header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header ( 'Pragma: public' );
			header ( 'Content-Length: ' . filesize ( $dfile ) );
			ob_clean();
			flush();
			readfile ( $dfile );
			exit ();
		} else {
			header ( 'Content-Description: File Transfer' );
			header ( 'Content-Type: application/octet-stream' );
			header ( 'Content-Disposition: attachment; filename=' . basename ( $dfile ) );
			header ( 'Content-Transfer-Encoding: binary' );
			header ( 'Expires: 0' );
			header ( 'Cache-Control:' );
			header ( 'Pragma: cache' );
			header ( 'Content-Length: ' . filesize ( $dfile ) );
			readfile ( $dfile );
			exit ();
		}
	}
} // end if OrgID, TypeForm, Form
?>
