<?php
$params_info_afq = array ();
$set_info_afq = array ();

// handle Question Detail updates
if (isset ( $_REQUEST ['process'] ) && $_REQUEST ['process'] == 'individual') {
	
    if(isset($_REQUEST['ddlPrefilledQueData']) && $_REQUEST['ddlPrefilledQueData'] != "") {
    
        if($_REQUEST['ddlPrefilledQueData'] ==  "States")
        {
            $address_states_results =   $AddressObj->getAddressStateList();
            $address_states_count   =   $address_states_results['count'];
            $address_states         =   $address_states_results['results'];
    
            $def                    =   '';
    
            $address_states_list    =   array();
            $address_states_list[]  =   "Select";
            $answers_info[]         =   "" . ":" . "Select";
            for($asl = 0; $asl < $address_states_count; $asl++) {
                $address_states_list[$address_states[$asl]['Abbr']] = $address_states[$asl]['Description'];
                if($address_states[$asl]['Description'] != "") {
                    $answers_info[] =  $address_states[$asl]['Abbr'] . ':' . $address_states[$asl]['Description'];
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] ==  "Days")
        {
            $days_list              =   array(
                                            "SUN"   =>  "Sunday", 
                                            "MON"   =>  "Monday", 
                                            "TUE"   =>  "Tuesday", 
                                            "WED"   =>  "Wednesday", 
                                            "THU"   =>  "Thursday", 
                                            "FRI"   =>  "Friday", 
                                            "SAT"   =>  "Saturday"
                                        );
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($days_list as $day_key=>$day_value) {
                if($day_value != "") {
                    $answers_info[] =   $day_key . ':' . $day_value;
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] ==  "Months")
        {
            $months_list            =   array(
                                            "JAN"   =>  "January",
                                            "FEB"   =>  "February",
                                            "MAR"   =>  "March",
                                            "APR"   =>  "April",
                                            "MAY"   =>  "May",
                                            "JUN"   =>  "June",
                                            "JUL"   =>  "July",
                                            "AUG"   =>  "August",
                                            "SEP"   =>  "September",
                                            "OCT"   =>  "October",
                                            "NOV"   =>  "November",
                                            "DEC"   =>  "December"
                                        );
    
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($months_list as $month_key=>$month_value) {
                if($month_value != "") {
                    $answers_info[] =   $month_key . ':' . $month_value;
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] == 'SocialMedia') {
            $social_media   =   array(
                                        "Facebook"  => "Facebook",
                                        "Twitter"   => "Twitter",
                                        "Linkedin"  => "Linkedin",
                                        "Google+"   => "Google+",
                                        "YouTube"   => "YouTube",
                                        "Pintrest"  => "Pintrest",
                                        "Instagram" => "Instagram",
                                        "Digg"	    => "Digg"
                                    );
            
            
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($social_media as $socialmedia_key=>$socialmedia_value) {
                if($socialmedia_value != "") {
                    $answers_info[] =   $socialmedia_key . ':' . $socialmedia_value;
                }
            }
        }
    
        $answer = implode("::", $answers_info);
    }
    else {
    
    	$def   =   '';
    	$i     =   0;
    	$ii    =   0;
    	for($i; $i < $cnt; $cnt) {
    		$i ++;
    		$v = 'value-' . $i;
    		$d = 'display-' . $i;
    		$de = 'default-' . $i;
    		
    		if ($_POST [$d] != '') {
    			if ($default == $de) {
    				$def = $_POST [$v];
    			}
    			$answer .= $_POST [$v] . ':' . $_POST [$d] . '::';
    			$ii ++;
    			$data [] = array (
    					'value' => $_POST [$v],
    					'name' => $_POST [$d] 
    			);
    		}
    	}
    	
    	if ($sort == 'Y') {
    		
    		$answer = "";
    		
    		foreach ( $data as $key => $row ) {
    			$value [$key] = $row ['value'];
    			$name [$key] = $row ['name'];
    		}
    		array_multisort ( $name, SORT_ASC, $data );
    		
    		foreach ( $data as $key => $row ) {
    			$answer .= $row ['value'] . ':' . $row ['name'] . '::';
    			$ii ++;
    		}
    	}
    	
    	if ($ii > 0) {
    		$answer = substr ( "$answer", 0, - 2 );
    	}
    	
    	if ($Clear == 'Y') {
    		$def = '';
    	}
    	
    	if ($_POST ['mergecontent'] != "") {
    		$answer = $_POST ['mergecontent'];
    	}
    	
    	if ($_REQUEST ['QuestionTypeID'] == "100") {
    		
    		$question_answers ['RVal'] = $_REQUEST ['RVal'];
    		$question_answers ['LabelValRow'] = $_REQUEST ['LabelValRow'];
    		
    		if ($sort == 'Y') {
    			//Sort based on alphabetical order
    			asort($question_answers ['LabelValRow']);
    		
    			if(is_array($question_answers ['LabelValRow'])) {
    				$q100_i = 1;
    				foreach($question_answers ['LabelValRow'] as $lable_key_name=>$label_val_name) {
    					$question_answers ['LabelValRow']["LabelValRow".$q100_i] = $label_val_name;
    		
    					$q100_i++;
    				}
    			}
    		}
    		
    		if (is_array ( $question_answers )) {
    			$answer = serialize ( $question_answers );
    		}
    		
    		if ($_REQUEST ['rows'] != "" && $_REQUEST ['cols'] != "") {
    			$params_info_afq [':rows'] = $_REQUEST ['rows'];
    			$params_info_afq [':cols'] = $_REQUEST ['cols'];
    			$set_info_afq [] = "rows = :rows";
    			$set_info_afq [] = "cols = :cols";
    		}
    	}
    	
    	if ($_REQUEST ['QuestionTypeID'] == "120") {
    		$question_answers ['day_names'] = $_REQUEST ['day_names'];
    		if (is_array ( $question_answers )) {
    			$answer = serialize ( $question_answers );
    		}
    		
    		if ($_REQUEST ['number_of_days'] != "") {
    			$params_info_afq [':rows'] = $_REQUEST ['number_of_days'];
    			$set_info_afq [] = "rows = :rows";
    		}
    	}
    }
    
	//set update information
	$set_info_afq [] = "Question = :Question";
	$set_info_afq [] = "value = :value";
	$set_info_afq [] = "defaultValue = :defaultValue";
	$set_info_afq [] = "Active = :Active";
	$set_info_afq [] = "Required = :Required";
	
	//set parameters
	$params_info_afq [":Question"] = $Question;
	$params_info_afq [":value"] = $answer;
	$params_info_afq [":defaultValue"] = $def;
	$params_info_afq [":Active"] = $Active;
	$params_info_afq [":Required"] = $Required;
	
	//set where condition
	$where_info = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
	
	//set parameters
	$params_info_afq [":OrgID"] = $OrgID;
	$params_info_afq [":AgreementFormID"] = $_REQUEST ['AgreementFormID'];
	$params_info_afq [":QuestionID"] = $_REQUEST ['QuestionID'];
	
	// Update Agreement Forms Information
	$FormQuestionsObj->updQuestionsInfo ( 'AgreementFormQuestions', $set_info_afq, $where_info, array ($params_info_afq) );
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Update completed!')";
	echo '</script>';
} // end process
?>