<?php 
$next_run_date  =   $IconnectFormsReminderSettingsObj->getNextRunDate($OrgID, "", $RequestID);
$IFRS           =   $IconnectFormsReminderSettingsObj->getIconnectFormsReminderSettings($OrgID, "", $RequestID);

//Set where condition
$where          =   array("OrgID = :OrgID", "RequestID = :RequestID", "PresentedTo = 1", "Status < 3");
//Set parameters
$params         =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get InternalFormsAssigned Information Count
$results        =   $FormsInternalObj->getInternalFormsAssignedInfo("OrgID, ApplicationID", $where, "ApplicationID", "", array($params));
$apps_count     =   $results['count'];

if(($USERROLE == 'master_admin' || $USERROLE == 'master_recruiter_wOnboard') && preg_match ( '/requisitionsSearch.php/', $_SERVER ["HTTP_REFERER"] )) {
    ?>
    <div class="table-responsive">
        <form method="post" name="frmIconnectFormSettings" id="frmIconnectFormSettings">
    		    <table class="table table-bordered">
    		      <tr>
    	              <td style="font-weight:bold;">
    	                  iConnect Forms Reminder Settings
    	                  <span id="rem_not_msg" style="color: blue"></span>
    	              </td>
    		      </tr>
    		      <tr>
    		          <td>
                        <strong>Applicants Count: </strong><?php echo $apps_count;?>
                        &nbsp;&nbsp;
                        <input type="hidden" name="ReminderNotification" id="ReminderNotification" value="Off">
                        <strong>Days: </strong>
            		    <select name="NumberOfDays" id="NumberOfDays">
            		         <option value="0">Off</option>
            			     <?php
            			     for($k = 1; $k <= 7; $k++) {
                                if($IFRS['NumberOfDays'] == $k) $selected = ' selected="selected"';
            			     	echo '<option value="'.$k.'"'.$selected.'>'.$k.'</option>';
            			     	unset($selected);
            			     }
            			     ?>
            			</select>
                        &nbsp;&nbsp;
                        <?php 
                        if($IFRS['ReminderNotification'] == 'On') {
                        	if($IFRS['LastRunDateTime'] != '0000-00-00 00:00:00') {
                            ?>
                            <strong>Last Run Date: </strong>
                            <?php echo date('Y-m-d H:i:s', strtotime('+1 hour', strtotime($IFRS['LastRunDateTime'])));?>
                            <strong>Next Run Date: </strong>
                            <?php
                            echo date('Y-m-d H:i:s', strtotime('+1 hour', strtotime($next_run_date)));
                        	}
                        }
                        ?>
                        </td>
    		      </tr>
    		      <tr>
    		          <td>
                        <input type="button" class="btn btn-primary" name="btnSubmit" id="btnSubmit" value="Save Settings" onclick="saveSettings(this)">&nbsp;&nbsp;
                        <?php 
                        if($apps_count > 0) {
                        	?><input type="button" class="btn btn-primary" name="btnProcessEmailList" id="btnProcessEmailList" value="Process Email List" onclick="processEmailList(this)"><?php
                        }
                        ?>
    		          </td>
    		      </tr>
    		    </table>
    			<input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>"> 
    	</form>
    </div>
    <script>
    //Configure form to save settings
    function saveSettings(btn_obj) {
    	var save_iconn_form_rem = irecruit_home + 'formsInternal/saveIconnectFormsReminderSettings.php?process=Y';
    
    	var form_obj = $(btn_obj).parents('form:first');
    	var input_data = $(form_obj).serialize();
    
    	$.ajax({
    		method: "POST",
    	    data: input_data,
      		url: save_iconn_form_rem,
    		type: "POST",
    		beforeSend: function() {
    			$("#rem_not_msg").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
    		},
    		success: function(data) {
    			$("#rem_not_msg").html('<br>Settings saved successfully');
        	}
    	});
    }
    
    //Configure form to process email list
    function processEmailList(btn_obj) {
    	if(confirm("Are you sure do you want to process?")) {
    		var save_iconn_form_rem = irecruit_home + 'formsInternal/saveIconnectFormsReminderSettings.php?process=Process';
    
    		var form_obj = $(btn_obj).parents('form:first');
    		var input_data = $(form_obj).serialize();
    
    		$.ajax({
    			method: "POST",
    	  		url: save_iconn_form_rem,
    			type: "POST",
    		    data: input_data,
    			beforeSend: function() {
    				$("#rem_not_msg").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
    			},
    			success: function(data) {
    				$("#rem_not_msg").html('<br>Emails processed successfully');
    	    	}
    		});		
    	}
    }
    </script>
    <?php
}
?>
