<?php 
if ((($feature ['WebForms'] != "Y") 
    && ($feature ['AgreementForms'] != "Y") 
    && ($feature ['SpecificationForms'] != "Y") 
    && ($feature ['PreFilledForms'] != "Y"))
    || $permit ['Internal_Forms'] < 1) {
	include IRECRUIT_DIR . 'irecruit_permissions.err';
}
else {
    
    $all_forms_sort_order = array();
    
    //set where condition
    $where		=	array("OrgID = :OrgID", "FormStatus != 'Deleted'");
    //set parameters
    $params 	=	array(":OrgID"=>$OrgID);
    //Get WebForms Information
    $results	=	G::Obj('FormsInternal')->getWebFormsInfo("WebFormID, SortOrder", $where, "", array($params));
    
    $is_in_sort_order = true;
   
    
    if(is_array($results['results'])) {
    	foreach($results['results'] as $row) {
    		$sort_order_rows[$row["WebFormID"]] = $row['SortOrder'];
    		$table_names[$row["WebFormID"]] = "WebForms";
    		if($row['SortOrder'] == 0) $is_in_sort_order = false;
    	}
    }
    
    //set where condition
    $where  = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
    //set parameters
    $params = array(":OrgID"=>$OrgID);
    //get agreement forms information
    $results = G::Obj('FormsInternal')->getAgreementFormsInfo("AgreementFormID, SortOrder", $where, "", array($params));
    
    if(is_array($results['results'])) {
    	foreach($results['results'] as $row) {
    		$sort_order_rows[$row["AgreementFormID"]] = $row['SortOrder'];
    		$table_names[$row["AgreementFormID"]] = "AgreementForms";
    		if($row['SortOrder'] == 0) $is_in_sort_order = false;
    	}
    }
    
    //set columns
    $columns = "SpecificationFormID, SortOrder";
    //set where condition
    $where = array("OrgID = :OrgID", "FormStatus != 'Deleted'");
    //set params
    $params = array(":OrgID"=>$OrgID);
    //Get Specification Forms Information
    $results = $FormsInternalObj->getSpecificationForms($columns, $where, "", array($params));
    
    if(is_array($results['results'])) {
    	foreach($results['results'] as $row) {
    		$sort_order_rows[$row["SpecificationFormID"]] = $row['SortOrder'];
    		$table_names[$row["SpecificationFormID"]] = "SpecificationForms";
    		if($row['SortOrder'] == 0) $is_in_sort_order = false;
    	}
    }
    
    //set where condition
    $where = array("OrgID = :OrgID");
    //set parameters
    $params = array(":OrgID"=>$OrgID);
    //get prefilled forms
    $results = $FormsInternalObj->getPrefilledFormsInfo("PreFilledFormID, SortOrder", $where, "", array($params));
    
    if(is_array($results['results'])) {
    	foreach($results['results'] as $row) {
    		$sort_order_rows[$row["PreFilledFormID"]] = $row['SortOrder'];
    		$table_names[$row["PreFilledFormID"]] = "PreFilledForms";
    		if($row['SortOrder'] == 0) $is_in_sort_order = false;
    	}
    }
    
    $sort_order_rows_vals = array_values($sort_order_rows);
    
    $duplicate_sort_orders = array_count_values($sort_order_rows_vals);
    
    foreach($duplicate_sort_orders as $dup_sort_order_key=>$dup_sort_order_val) {
    	if($dup_sort_order_val > 1) {
    	    $is_in_sort_order = false;
    	}
    }
    
    $sort_orders_list = array();
    foreach ($sort_order_rows as $UniqueFormID => $SortOrder) {
        $sort_orders_list[] = $SortOrder;
        $duplicate_sorts_list = array_count_values($sort_orders_list);
    }
    
    $flag_count = 0;
    if($is_in_sort_order == false) {
    	
    	foreach ($sort_order_rows as $UniqueFormID => $SortOrder) {
    
    		$table_name = $table_names[$UniqueFormID];
    		
    		if($duplicate_sorts_list[$SortOrder] > 1) {
    	        $SortOrder = count($sort_order_rows) + 1;
    	        $flag_count++;
    	    
    	        //Overwrite sort order value if it has more than one zeros in array list
    	        if($flag_count > 1) {
    	            $SortOrder = count($sort_order_rows) + $flag_count;
    	        }
    		}
    		else if($SortOrder == 0) {
    		    $SortOrder = count($sort_order_rows) + 1;
    		    $flag_count++;
    		     
    		    //Overwrite sort order value if it has more than one zeros in array list
    		    if($flag_count > 1) {
    		        $SortOrder = count($sort_order_rows) + $flag_count;
    		    }
    		}
    		
    		if($table_name == 'WebForms')
    			$where = array("OrgID = :OrgID", "WebFormID = :FormID");
    		if($table_name == 'AgreementForms')
    			$where = array("OrgID = :OrgID", "AgreementFormID = :FormID");
    		if($table_name == 'PreFilledForms')
    			$where = array("OrgID = :OrgID", "PreFilledFormID = :FormID");
    		if($table_name == 'SpecificationForms')
    			$where = array("OrgID = :OrgID", "SpecificationFormID = :FormID");
    		
    		//set update information
    		$set_info =   array("SortOrder = :SortOrder");
    		//set parameters
    		$params   =   array(":SortOrder"=>$SortOrder, ":OrgID"=>$OrgID, ":FormID"=>$UniqueFormID);
    		//Update Forms Sorting Order
    		G::Obj('Forms')->updFormsInfo($table_name, $set_info, $where, array($params));
    	}
    	
    }
    
    $feature ['packages'] ='Y';

    echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered">';
    echo '<form method="post" action="' . $_SERVER ['SCRIPT_NAME'] . '">';
    echo '<tr><td valign="top"><b>';
    echo $title;
    echo '</b></td><td valign="top" align="right">';

    if (($typeform == "webforms") && ($action == "edit")) {
    	
    	if ($QuestionID) {
    		
    		echo '<a href="formsInternal.php?typeform=webforms&action=edit&WebFormID=' . $_REQUEST['WebFormID'] . '"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
    	} else {
    		
    		echo '<a href="formsInternal.php?typeform=webforms"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
    	}
    } else if (($typeform == "agreementforms") && ($action == "edit")) {
    	
    	if ($QuestionID) {
    		
    		echo '<a href="formsInternal.php?typeform=agreementforms&action=edit&AgreementFormID=' . $_REQUEST['AgreementFormID'] . '"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
    	} else {
    		
    		echo '<a href="formsInternal.php?typeform=agreementforms"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
    	}
    } else {
    	echo '<select name="typeform" onChange="submit()" class="form-control width-auto-inline">';
    	echo '<option value="x">Manage Internal Forms</option>';
    	
    	if ($permit ['Internal_Forms_Edit'] > 0) {
    		
			if ($feature ['packages'] == "Y") {
    			echo '<option value="packages"';
    			if ($typeform == "packages") {
    				echo " selected";
    				$iii ++;
    			}
    			echo '>Packages</option>';
    		}
    		
    		if ($feature ['WebForms'] == "Y") {
    			echo '<option value="webforms"';
    			if ($typeform == "webforms") {
    				echo " selected";
    				$iii ++;
    			}
    			echo '>Web Forms</option>';
    		}
    		
    		if ($feature ['AgreementForms'] == "Y") {
    			echo '<option value="agreementforms"';
    			if ($typeform == "agreementforms") {
    				echo " selected";
    				$iii ++;
    			}
    			echo '>Agreement Forms</option>';
    		}
    		
    		if ($feature ['SpecificationForms'] == "Y") {
    			echo '<option value="specforms"';
    			if ($typeform == "specforms") {
    				echo " selected";
    				$iii ++;
    			}
    			echo '>Specification Forms</option>';
    		}
    		
    		if ($feature ['PreFilledForms'] == "Y") {
    			echo '<option value="prefilledforms"';
    			if ($typeform == "prefilledforms") {
    				echo " selected";
    				$iii ++;
    			}
    			echo '>Federal & State Forms</option>';
    		}
		
    		echo '<option value="formstatus"';
    		if ($typeform == "formstatus") {
    			echo " selected";
    			$iii ++;
    		}
    		echo '>Form Status</option>';
    	} // end permit Internal_Forms_Edit
    	
    	echo '</select>';
    } // end if back
    
    echo '</td></tr>';
    echo '</table>';
    echo '</form>';
    echo '<br>';
    
    $used = 0;
    if ($typeform == "webforms") {
    	
    	include IRECRUIT_DIR . 'formsInternal/WebForms.inc';
    	$used ++;
    } else if ($typeform == "agreementforms") {
    	
    	include IRECRUIT_DIR . 'formsInternal/AgreementForms.inc';
    	$used ++;
    } else if ($typeform == "specforms") {
    	
    	include IRECRUIT_DIR . 'formsInternal/SpecificationForms.inc';
    	$used ++;
    } else if ($typeform == "prefilledforms") {
    	
    	include IRECRUIT_DIR . 'formsInternal/PreFilledForms.inc';
    	$used ++;
    } else if ($typeform == "packages") { 
    	include IRECRUIT_DIR . 'formsInternal/AddIconnectForm.inc';
    	$used ++;
    } else {
        if (substr ( $USERROLE, 0, 21 ) == 'master_admin') {
        	if ($feature ['WebForms'] == "Y") {
        	   if ($permit ['Internal_Forms_Edit'] > 0) {
        			echo '<a href="formsInternal.php?typeform=webforms&displaybox=Y">';
        			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add Web Form" style="margin:0px 3px -4px 0px;">';
        			echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Add Web Form</b>';
        			echo '</a><br>';
        		} // end permit Edit
        	}	
        
        	if ($feature ['AgreementForms'] == "Y") {
        		if ($permit ['Internal_Forms_Edit'] > 0) {
        			echo '<a href="formsInternal.php?typeform=agreementforms&displaybox=Y">';
        			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add Agreement Form" style="margin:0px 3px -4px 0px;">';
        			echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Add Agreement Form</b>';
        			echo '</a><br>';
        		} // end permit Edit
        	}
        
        	if ($feature ['SpecificationForms'] == "Y") {
        		if ($permit ['Internal_Forms_Edit'] > 0) {
        			echo '<a href="formsInternal.php?typeform=specforms">';
        			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add Specification Form" style="margin:0px 3px -4px 0px;">';
        			echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Add Specification Form</b>';
        			echo '</a><br>';
        		} // end permit Edit
        	}	
        
        	if ($feature ['PreFilledForms'] == "Y") {
        		if ($permit ['Internal_Forms_Edit'] > 0) {
        			echo '<a href="formsInternal.php?typeform=prefilledforms">';
        			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add Federal & State Form" style="margin:0px 3px -4px 0px;">';
        			echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Add Federal & State Form</b>';
        			echo '</a><br>';
        		} // end permit Edit
        	}

              if ($feature ['packages'] == "Y") {
        		if ($permit ['Internal_Forms_Edit'] > 0) { 
        			echo '<a href="formsInternal.php?typeform=packages">';
        			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="iConnect Packages" style="margin:0px 3px -4px 0px;">';
        			echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Packages</b>';
        			echo '</a><br>';
        		} // end permit Edit
        	}

        	
		

        	echo "<br>";
        }
    	
    	echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table">';
    	echo '<tr><td><span id="sort_internal_forms_msg" style="color:blue"></span></td></tr>';
    	echo '</table>';
    	
    	echo '<div class="table-responsive">';
    	echo '<table border="0" cellspacing="1" cellpadding="3" width="100%" class="table table-striped table-bordered" id="sort_internal_forms">';
    	
    	echo '<thead>';
    		echo '<tr>';
    		echo '<td width="20%"><b>Section</b></td>';
    		echo '<td width="15%"><b>Type of Display</b></td>';
    		echo '<td width="25%"><b>Display Name</b></td>';
    		
    		if ($permit ['Internal_Forms_Edit'] > 0 
    		    && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
    			echo '<td align="center" width="5%"><b>Copy</b></td>';
			    echo '<td align="center" width="5%"><b>Edit</b></td>';
    			echo '<td align="center" width="5%"><b>Delete</b></td>';
    		} // end permit Internal_Forms_Edit
    		
    		echo '<td align="center" width="5%"><b>View<br>Form</b></td>';
    		echo '</tr>' . "\n";
    	echo '</thead>';
    	
    	echo '<tbody>';
    	
    	$sort_forms = array();
    	
    	if ($feature ['WebForms'] == "Y") {
    		include IRECRUIT_DIR . 'formsInternal/ListWebForms.inc';
    		$used ++;
    	} // end WebForms
    	
    	if ($feature ['AgreementForms'] == "Y") {
    		include IRECRUIT_DIR . 'formsInternal/ListAgreementForms.inc';
    		$used ++;
    	} // end AgreementForms
    	
    	if ($feature ['SpecificationForms'] == "Y") {
    		include IRECRUIT_DIR . 'formsInternal/ListSpecificationForms.inc';
    	} // end SpecificationForms
    	
    	if ($feature ['PreFilledForms'] == "Y") {
    		include IRECRUIT_DIR . 'formsInternal/ListPreFilledForms.inc';
    	} // end PreFilledForms
    
    	ksort($sort_forms);
    	echo implode("", $sort_forms);
    	
    	echo '</tbody>';
    	
    	echo '</table>';
    	echo '</div>';
    } // end action new

}

?>
