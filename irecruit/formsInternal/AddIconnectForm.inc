<?php 
function addIconnectForm() { 
	global $feature, $AccessCode, $OrgID, $ApplicationID, $RequestID, $Active, $action, $UniqueID, $presentedTo, $AssignStatus, $CompleteByFactor, $CompleteByInterval, $FormsInternalObj, $ApplicantsObj;
	
	echo '<div>';

	?>
	<script type="text/javascript">
	var page_url = window.location.href;
	function addInternalForm(btn_obj) {  
		request_url = "formsInternal/";
		if(page_url.indexOf("formsInternal/") > 1) request_url = "";

		var form_id = $(btn_obj).parents('form').attr('id');
		var form = $('#'+form_id);
		var input_data = form.serialize();

		var dest_id = $(btn_obj).parents('.ra-tab-content').attr('id');

		$("#"+dest_id).html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');

		$.ajax({
			method: "POST",
	  		url: request_url+"getIconnectpackage.php?display_app_header=no",
			type: "POST",
			data: input_data,
			success: function(data) {
				$("#"+dest_id).html(data);

				$.ajax({
					method: "POST",
					url: "formsInternal/getIconnectpackageFormsCount.php?ApplicationID=<?php echo $ApplicationID?>&RequestID=<?php echo $RequestID;?>",
					type: "POST",
					success: function(data) {
						$("#"+dest_id+"-COUNT").html("( "+data+" )");

						if(document.URL.indexOf("requisitionsSearch.php") >= 0) {
							getAssignedFormsCount();
						}
                                             

					}
				});   self.location.reload();
	    	}
		});
	}
	</script>
	<?php
          $packagename ='';
         if(isset($_SESSION['PackageName'])){
  		$packagename = $_SESSION['PackageName'];
	  } 
	echo "<br>";
	echo "<form method='POST' id='ProcessAdditional$OrgID$ApplicationID$RequestID' name='ProcessAdditional$OrgID$ApplicationID$RequestID'>";
	 
	                                echo 'Package Name: <input type="text"   name="packagename" value="'.$packagename.'" required><br><br>';
                                                                                  
					

        echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
	
	echo '<tr>';
	echo '<td valign="bottom"><b>Presented To</b></td>';
	echo '<td valign="bottom"><b>iConnect Form</b></td>';
	echo '<td valign="bottom"><b>iConnect at Status</b></td>';
	echo '<td valign="bottom" colspan="2"><b>How Long to Complete</b></td>';
	echo '</tr>';
	
	echo '<tr>';
	
	echo '<td align="left">';
	echo '<select name=\'PresentedTo\' id=\'PresentedTo\' onchange=\'getInternalFormsPullDown("'.$ApplicationID.'", this.value, "internal_forms_list")\'>';
	
	$CustomPresentedTo = 2;
	if ($feature ['UserPortal'] == "Y") {
	    $CustomPresentedTo = 1;
		echo '<option value="1" selected="selected">Applicant</option>';
	} // end feature UserPortal
	
	echo '<option value="2">Internal Staff</option>';
	echo '</select>';
	echo '</td>';
	echo '<td align="left">';
	echo '<div id="internal_forms_list">';
	echo '<select name="InternalForms[]" id="InternalForms" multiple="multiple" style="max-width:300px;height:150px;">' . "\n";
	
	$internal_forms_list = array();
	if ($feature ['PreFilledForms'] == "Y") {
		if ($ApplicationID != "") {
			
			//set where condition
			$where = array("OrgID = :OrgID");
			//set parameters
			$params = array(":OrgID"=>$OrgID);
			//Get PrefilledForms Information
			$results = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, Form", array($params));
			
			if(is_array($results['results'])) {
				foreach ($results['results'] as $PFF) {
				
					$Restrict = "";
					
					if ($PFF ['PreFilledFormID'] == "FE-I9m") {
					    $Restrict = "Primary";
					}
					if ($PFF ['PreFilledFormID'] == "FE-I9m-2020") {
					    $Restrict = "Primary";
					}
					
					$display_option = "true";
					if($CustomPresentedTo == "1"
					    && $Restrict == "Primary") {
					    $display_option = "false";
					}
					
					if($display_option == "true") {
					    $internal_forms_list[$PFF ['SortOrder']] = '<option value="' . $PFF ['PreFilledFormID'] . '|PreFilled|' . $PFF ['TypeForm'] . ': ' . $PFF ['FormName'] . '|' . $Restrict . '"' . $sel . '>' . $PFF ['TypeForm'] . ': ' . $PFF ['FormName'] . '</option>\n';
					}
				} // end foreach
			}
			
		} // end no ApplicationID
	} // end feature
	
	if ($feature ['WebForms'] == "Y") {
		
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//Get WebForms Information
		$results = $FormsInternalObj->getWebFormsInfo("*", $where, "FormName", array($params));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $WFQNT) {
				$Restrict = "";
					
				$display_option = "true";
				if($CustomPresentedTo == "1"
				    && $Restrict == "Primary") {
				    $display_option = "false";
				}
				if($display_option == "true") {
				    $internal_forms_list[$WFQNT ['SortOrder']] = '<option value="' . $WFQNT ['WebFormID'] . '|WebForm|' . $WFQNT ['FormName'] . '|' . $Restrict . '"' . $sel . '>' . $WFQNT ['FormName'] . " (Web Form)</option>\n";
				}
			} // end foreach
		}
		
	} // end feature
	
	if ($feature ['AgreementForms'] == "Y") {
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		// Agreement Forms
		$results = $FormsInternalObj->getAgreementFormsInfo("*", $where, "FormName", array($params));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $AFN ) {
				$Restrict = $AFN ['FormPart'];
					
				$display_option = "true";
				if($CustomPresentedTo == "1" 
				    && $Restrict == "Primary") {
				    $display_option = "false";
				}
					
				if($display_option == "true") {
				    $internal_forms_list[$AFN ['SortOrder']] = '<option value="' . $AFN ['AgreementFormID'] . '|Agreement|' . $AFN ['FormName'] . '|' . $Restrict . '"' . $sel . '>' . $AFN ['FormName'] . " (Agreement Form)</option>\n";
				}
			} // end foreach
		}
		
	} // end feature
	
	if ($feature ['SpecificationForms'] == "Y") {
		
		//set where condition
		$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//get specification forms
		$results = $FormsInternalObj->getSpecificationForms("SpecificationFormID, DisplayTitle, FormType, SortOrder", $where, "FormType, DisplayTitle", array($params));
		
		if(is_array($results['results']))
		{
			foreach($results['results'] as $WFQNT) {
				$Restrict = "";
					
				$display_option = "true";
				if($CustomPresentedTo == "1"
				    && $Restrict == "Primary") {
				    $display_option = "false";
				}
					
				if($display_option == "true") {
				    $internal_forms_list[$WFQNT ['SortOrder']] = '<option value="' . $WFQNT ['SpecificationFormID'] . '|' . $WFQNT ['FormType'] . '|' . $WFQNT ['DisplayTitle'] . '|' . $Restrict . '"' . $sel . '>' . $WFQNT ['FormType'] . ": " . $WFQNT ['DisplayTitle'] . "</option>\n";
				}
			} // end foreach
		
		}	
		
	} // end feature
	
	ksort($internal_forms_list);
	echo implode("", $internal_forms_list);
	
	echo '</select>';
	echo '</div>';
	?>
</td>

<td align="left">
<?php
	//set where condition
	$where = array("OrgID = :OrgID", "Active = 'Y'");
	//set parameters
	$params = array(":OrgID"=>$OrgID);
	//get applicant process flow information
	$results = $ApplicantsObj->getApplicantProcessFlowInfo('ProcessOrder, Description', $where, 'ProcessOrder', array($params));
	
	
	if ($ApplicationID != "") {

        echo '<select name="AssignStatus">';
		
		if ($AssignStatus == 'immediately') {
			$sel = " selected";
		} else {
			$sel = "";
		}
		echo '<option value="immediately"' . $sel . '>Immediately';
		
		echo '</select>';
		
	} else {

        echo '<select name="AssignStatus[]" multiple="multiple" style="max-width:300px;height:150px;">';
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $APF) {
		
				if ($APF ['ProcessOrder'] == $AssignStatus) {
					$sel = " selected";
				} else {
					$sel = "";
				}
				echo '<option value="' . $APF ['ProcessOrder'] . '"' . $sel . '>' . $APF ['Description'];
			} // end foreach	
		}
		
		echo '</select>';
		
	} // end if ApplicationID
	
	
	?>
</td>

<td align="left" colspan="3">
<select name="CompleteByInterval">
<?php
	for($i = 1; $i <= 7; $i ++) {
		if ($CompleteByInterval == $i) {
			$sel = " selected";
		} else {
			$sel = "";
		}
		echo '<option value="' . $i . '"' . $sel . '>' . $i . "</option>";
	}
	?>
</select> 
<select name="CompleteByFactor">
<?php
	if ($CompleteByFactor == "day") {
		$sel = " selected";
	} else {
		$sel = "";
	}
	echo '<option value="day"' . $sel . '>Days</option>';
	if ($CompleteByFactor == "week") {
		$sel = " selected";
	} else {
		$sel = "";
	}
	echo '<option value="week"' . $sel . '>Weeks</option>';
	if ($CompleteByFactor == "month") {
		$sel = " selected";
	} else {
		$sel = "";
	}
	echo '<option value="month"' . $sel . '>Months</option>';
	?>
</select>

<?php
	echo '&nbsp;&nbsp;&nbsp;&nbsp;';
	echo '<input type="hidden" name="ApplicationID" value="' . htmlspecialchars($ApplicationID) . '">';
	echo '<input type="hidden" name="RequestID" value="' . htmlspecialchars($RequestID) . '">';
	echo '<input type="hidden" name="process" value="A">';
	echo '<input type="hidden" name="action" value="' . htmlspecialchars($action) . '">';
	echo '<input type="hidden" name="Active" value="' . htmlspecialchars($Active) . '">';
	echo '<input type="button" class="btn btn-primary" value="iConnect Form" name="btnAssignNewForm" id="btnAssignNewForm" onclick="addInternalForm(this);">';
	if($_REQUEST['refresh'] == 'formstatus') echo '<input type="hidden" name="refresh" id="refresh" value="formstatus">';
	echo '</td></tr>';
	echo "</table>";
	echo '</form>';
	echo '</div>';
} // end function
?>
