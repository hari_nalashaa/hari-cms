<?php
if (isset($_REQUEST['copy']) && $_REQUEST['copy'] != "") {
	
	$id = uniqid ();
	
	//set columns
	$columns = "OrgID, FormID, WebFormID, concat('CUST',QuestionID,'$id'), concat('Copy: ',Question), QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder+1, Required, Validate";
	//set where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$_REQUEST['copy']);
	
	$results_copy = $FormQuestionsObj->getQuestionsInformation("WebFormQuestions", "Question", $where, "", array($params));
	
	if($results_copy['count'] == 0) {
		//insert form questions master information
		$FormQuestionsObj->insFormQuestionsMaster("WebFormQuestions", $columns, $where, array($params));
	}
	

	//set where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID']);
	//Get WebFormQuestions
	$results = $FormQuestionsObj->getQuestionsInformation('WebFormQuestions', 'WebFormID, QuestionID', $where, "QuestionOrder", array($params));
	
	$cnt = 0;
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $WFQT) {
			$cnt ++;
			
			//set webformquestions update information
			$set_info = array("QuestionOrder = :QuestionOrder");
			//set parameters
			$params = array(":QuestionOrder"=>$cnt, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$WFQT ['QuestionID']);
			//set where condition
			$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			//update webformquestions
			$FormQuestionsObj->updQuestionsInfo('WebFormQuestions', $set_info, $where, array($params));
		} // end foreach
	}
	
} // end copy

if (isset($_REQUEST['delete']) && $_REQUEST['delete'] != "") {
	
	//set where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$_REQUEST['delete']);
	//set delete questions information
	$FormQuestionsObj->delQuestionsInfo('WebFormQuestions', $where, array($params));

	
	//set parameters
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
	//set where condition
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID']);
	//Get WebFormQuestions
	$results = $FormQuestionsObj->getQuestionsInformation('WebFormQuestions', 'WebFormID, QuestionID', $where, "QuestionOrder", array($params));

	$cnt = 0;
	if(is_array($results['results'])) {
		foreach($results['results'] as $WFQT) {
			$cnt ++;			
			//set webformquestions update information
			$set_info = array("QuestionOrder = :QuestionOrder");
			//set parameters
			$params = array(":QuestionOrder"=>$cnt, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$WFQT ['QuestionID']);
			//set where condition
			$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			//update webformquestions
			$FormQuestionsObj->updQuestionsInfo('WebFormQuestions', $set_info, $where, array($params));
		} // end foreach
	}
} // end delete

if (isset($_REQUEST['new']) && $_REQUEST['new'] == "add") {
	
	//set columns
	$columns = "FormID, MAX(QuestionOrder) MaxQuestionOrder";
	//set where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
	//set paramters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST['WebFormID']);
	//Get web form questions
	$results = $FormQuestionsObj->getQuestionsInformation('WebFormQuestions', $columns, $where, "", array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $WFQT) {
			$formid = $WFQT ['FormID'];
			$questionorder = $WFQT ['MaxQuestionOrder'] + 1;
		}	
	}
	
	if(is_null($formid)) $formid = "";
	$questionid = CUST . uniqid ();

	$web_form_que_info = array (
			"OrgID"          =>  $OrgID, 
			"FormID"         =>  $formid, 
			"WebFormID"      =>  $_REQUEST ['WebFormID'], 
			"QuestionID"     =>  $questionid, 
			"Question"       =>  'New question added.', 
			"QuestionTypeID" =>  '5', 
			"size"           =>  '0', 
			"maxlength"      =>  '0', 
			"rows"           =>  '0', 
			"cols"           =>  '0', 
			"wrap"           =>  'virtual', 
			"value"          =>  '', 
			"Active"         =>  'Y', 
			"QuestionOrder"  =>  $questionorder
	);
	
	//Insert Agreement Form Questions
	$FormQuestionsObj->insFormQuestions ( 'WebFormQuestions', $web_form_que_info );
	
	
} // end new question

if (($QuestionOrder != "") & ($direction != "")) {
	
	if ($direction == "up") {
		
		$minus = $QuestionOrder - 1;
		
		$info = array();
		
		//set parameters for first update
		$info[] = array(":SQuestionOrder"=>'999', ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionOrder"=>$minus);
		//set parameters for one more update
		$info[] = array(":SQuestionOrder"=>$minus, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionOrder"=>$QuestionOrder);
		//set parameters for one more update
		$info[] = array(":SQuestionOrder"=>$QuestionOrder, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionOrder"=>'999');
		
		//set webformquestions update information, this update will be executed 3 times for each set of parameter
		$set_info = array("QuestionOrder = :SQuestionOrder");
		//set where condition
		$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionOrder = :QuestionOrder");
		//update webformquestions
		$FormQuestionsObj->updQuestionsInfo('WebFormQuestions', $set_info, $where, $info);
		
		
	} else if ($direction == "down") {
		
		$plus = $QuestionOrder + 1;
		
		$info = array();
		
		//set parameters for first update
		$info[] = array(":SQuestionOrder"=>'999', ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionOrder"=>$plus);
		//set parameters for one more update
		$info[] = array(":SQuestionOrder"=>$plus, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionOrder"=>$QuestionOrder);
		//set parameters for one more update
		$info[] = array(":SQuestionOrder"=>$QuestionOrder, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionOrder"=>'999');
		
		//set webformquestions update information, this update will be executed 3 times for each set of parameter
		$set_info = array("QuestionOrder = :SQuestionOrder");
		//set where condition
		$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionOrder = :QuestionOrder");
		//update webformquestions
		$FormQuestionsObj->updQuestionsInfo('WebFormQuestions', $set_info, $where, $info);
		
	} // end direction
} // end sort feature

if ($process == "individual") {
	
	include IRECRUIT_DIR . 'formsInternal/ProcessWebFormQuestions.inc';
} // end process individual

if ($process == "questionlisting") {
	
	foreach ( $_POST as $key => $value ) {
		
		$pieces = explode ( "-", $key );
		
		if ($pieces [1] == 'questiontypeid') {
			
			//set where condition
			$where = array("Editable = 'Y'", "QuestionTypeID = :QuestionTypeID");
			//set parameters
			$params = array(":QuestionTypeID"=>$value);
			//get QuestionTypes Information
			$results = G::Obj('QuestionTypes')->getQuestionTypesInfo("*", $where, "", array($params));
			
			$QTs = $results['results'][0];
			
			
			//set webformquestions update information, this update will be executed 3 times for each set of parameter
			$set_info = array("QuestionTypeID = :QuestionTypeID", "size = :size", "maxlength = :maxlength", "wrap = :wrap");
			//set parameters
			$params = array(":QuestionTypeID"=>$value, ":size"=>$QTs ['size'], ":maxlength"=>$QTs ['maxlength'], ":wrap"=>$QTs ['wrap'], ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$pieces [0]);
				
			if($value != 100) {
			    $set_info[] = "rows = :rows";
			    $set_info[] = "cols = :cols";
			    
			    $params[":rows"] = $QTs ['rows'];
			    $params[":cols"] = $QTs ['cols'];
			}
			
			//set where condition
			$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			//update webformquestions
			$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
			
		} // end pieces questiontypeid
		
		if ($pieces [1] == 'question') {
			
			//set webformquestions update information, this update will be executed 3 times for each set of parameter
			$set_info = array("Question = :Question");
			//set where condition
			$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			//set parameters
			$params = array(":Question"=>$value, ":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$pieces [0]);
			//update webformquestions
			$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
			
		} // end pieces question
		
		if ($value == 'D') {
			
			//set webformquestions update information, this update will be executed 3 times for each set of parameter
			$set_info = array("Active = ''", "Required = ''");
			//set where condition
			$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
			//set parameters
			$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$key);
			//update webformquestions
			$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
			
		}
		
		if ($value == 'Y') {
			
			if ($pieces [1] == 'A') {
				
				//set webformquestions update information, this update will be executed 3 times for each set of parameter
				$set_info = array("Active = 'Y'");
				//set where condition
				$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
				//set parameters
				$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$pieces [0]);
				//update webformquestions
				$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
				
			} elseif ($pieces [1] == 'R') {
				
				//set webformquestions update information, this update will be executed 3 times for each set of parameter
				$set_info = array("Required = 'Y'");
				//set where condition
				$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
				//set parameters
				$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$pieces [0]);
				//update webformquestions
				$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));

			}
		}
	} // end POST
	
	//set webformquestions update information, this update will be executed 3 times for each set of parameter
	$set_info = array("Question = 'Electronic Signature:'");
	//set where condition
	$where = array("OrgID = :OrgID", "QuestionTypeID = 30");
	//set parameters
	$params = array(":OrgID"=>$OrgID);
	//update webformquestions
	$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
	
	
	//set webformquestions update information, this update will be executed 3 times for each set of parameter
	$set_info = array("Required = ''");
	//set where condition
	$where = array("OrgID = :OrgID", "QuestionTypeID = 99");
	//set parameters
	$params = array(":OrgID"=>$OrgID);
	//update webformquestions
	$FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
	
	if ($_REQUEST ['formname']) {
		//set parameters		
		$params   =   array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":FormName"=>$_REQUEST ['formname']);
		//set where condition
		$where    =   array("OrgID = :OrgID", "WebFormID = :WebFormID");
		//set information
		$set_info =   array("FormName = :FormName");
		//Update WebForms Information
		G::Obj('Forms')->updFormsInfo('WebForms', $set_info, $where, array($params));
		
	}
} // end process questionlisting


if(isset($_REQUEST['date_field']) && $_REQUEST['date_field'] != "") {
	//Set information
	$set_info		=	array("Validate = :Validate");
	//Where information
	$where_info		=	array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
	//Set parameters
	$params			=	array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":QuestionID"=>$QuestionID, ":Validate"=>json_encode($_POST['date_field']));
	//Update the table information based on bind and set values
	$res_que_info	=   G::Obj('FormQuestions')->updQuestionsInfo ( "WebFormQuestions", $set_info, $where_info, array ($params) );
}

if ($QuestionID) {
	
	?>
<form method="post" action="<?php echo $formscript?>">
	<table class="formtable" border="0" cellspacing="3" cellpadding="5"
		width="770" class="table table-striped table-bordered">
		<tr>
			<td><b>Edit Question</b></td>
		<tr>
		
		<?php
		  if($_REQUEST['QuestionTypeID'] == '3') {
		  	   ?>
        		<tr>
        			<td>
        			    <strong>Note*:</strong> If you want to prefill the data with the existing list of options available, please select any option in below pull down.
        			    <br>
        			    Prefill with existing data:
        			    <select name="ddlPrefilledQueData" id="ddlPrefilledQueData">
        			         <option value="">Select</option>
        			         <option value="States">States</option>
        			         <option value="Days">Days</option>
        			         <option value="Months">Months</option>
        			         <option value="SocialMedia">Social Media</option>
        			    </select>
        			    <br><br>
        			</td>
        		<tr>
		  	   <?php
		  }
		?>
		
		<tr>
			<td>
			<?php
				include IRECRUIT_VIEWS . 'forms/AdminQuestions.inc';
			?>
			</td>
		</tr>
		<tr>
			<td align="center" height="60" valign="middle">
			
			<input type="hidden" name="QuestionID" value="<?php echo htmlspecialchars($_REQUEST ['QuestionID']); ?>" /> 
			<input type="hidden" name="FormID" value="<?php echo htmlspecialchars($_REQUEST ['FormID']); ?>" /> 
			<input type="hidden" name="WebFormID" value="<?php echo htmlspecialchars($_REQUEST ['WebFormID']); ?>" /> 
			<input type="hidden" name="action" value="<?php echo $action; ?>" /> 
			<input type="hidden" name="typeform" value="webforms" /> 
			<input type="hidden" name="QuestionTypeID" id="QuestionTypeID" value="<?php echo htmlspecialchars($_REQUEST['QuestionTypeID']); ?>">
			<input type="hidden" name="process" value="individual" /> 
			<input type="submit" value="Update" class="btn btn-primary" />
			<?php
			if ($process) {
				echo '<input type="submit" name="finish" value="Finish" class="btn btn-primary"/>';
			} else {
				echo '<input type="submit" name="finish" value="Cancel" class="btn btn-primary"/>';
			}
			?>
			</td>
		</tr>
		</form>
	</table>

<?php
	
	// display preview
	if ($Active == 'Y') {
		
		echo '<hr size="1"/>';
		echo '<table class="table table-striped table-bordered">';
		echo '<tr><td colspan="100%">';
		echo '<p><b>Preview</b></p>';
		$colwidth = "220";
		echo include COMMON_DIR . 'application/DisplayQuestions.inc';
		echo '</td></tr>';
		echo '</table>';
	}
} else { // QuestionID
	
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID']);
	//set where condition
	$where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
	//get webforms information
	$resultsIN = $FormsInternalObj->getWebFormsInfo("*", $where, "WebFormID LIMIT 0,1", array($params));
	$WFN = $resultsIN['results'][0];

	echo "<p>Edit the <u>" . $WFN ['FormName'] . "</u> form.</p>";
	
	echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
	echo '<form method="post" action="' . $formscript . '">';
	
	echo "<tr><td colspan=\"7\" valign=\"middle\" height=\"40\">Form Name: ";
	
	echo '<input type="text" name="formname" value="' . $WFN ['FormName'] . '" size="30" maxlength="45">';
	
	echo "</td></tr>";
	
	echo '<tr>';
	echo '<td width="300"><b>Question</b></td>';
	echo '<td align="center" width="120"><b>Question Type</b></td>';
	echo '<td align="center" width="60"><b>Active</b></td>';
	echo '<td align="center" width="10"><b>Req</b></td>';
	echo '<td align="center" width="60"><b>Edit</b></td>';
	echo '<td align="center" width="60"><b>Order</b></td>';
	echo '<td align="center" width="40"><b>Copy<br>Delete</b></td>';
	echo '</tr>';
	
	//set where condition
	$where     =   array("Editable = 'Y'", "QuestionTypeID != '60'", "QuestionTypeID != '8'");
	$results   =   G::Obj('QuestionTypes')->getQuestionTypesInfo("*", $where, "SortOrder", array());
	$rowcolor  =   "#eeeeee";

	if(is_array($results['results'])) {
		foreach($results['results'] as $QT) {
			$QUES [$QT ['QuestionTypeID']] = $QT ['Description'];
		}
	}
	
	//set columns
	$columns   =   "substring(QuestionID,1,4) DEL, FormID, WebFormID, QuestionID, Question, QuestionTypeID, Active, QuestionOrder, Required";
	//set where condition
	$where     =   array("OrgID = :OrgID", "WebFormID = :WebFormID");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID']);
	//get webform questions information
	$results   =   G::Obj('FormQuestions')->getQuestionsInformation('WebFormQuestions', $columns, $where, "QuestionOrder", array($params));
	
	$rowcnt    =   $results['count'];
	$i         =   0;
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $WFQ) {

			$i ++;
		
			if ($WFQ ['Active'] == "Y") {
				$ckd1 = ' checked';
			} else {
				$ckd1 = '';
			}
			if ($WFQ ['Required'] == "Y") {
				$ckd2 = ' checked';
			} else {
				$ckd2 = '';
			}
		
			echo '<tr bgcolor="' . $rowcolor . '">';
		
			echo '<td>';
		
			if ($WFQ ['QuestionTypeID'] == '99') { // Instructions
				echo substr ( strip_tags ( $WFQ ['Question'] ), 0, 40 ) . "...";
			} else if ($WFQ ['QuestionTypeID'] == '30') { // Signature
				echo $WFQ ['Question'];
			} else {
				echo '<input type="text" name="' . $WFQ ['QuestionID'] . '-question" value="';
				echo $WFQ ['Question'];
				echo '" size="50">';
			}
		
			echo '</td>';
		
			echo '<td align=center valign="top"><select name="' . $WFQ ['QuestionID'] . '-questiontypeid" style="width:120px;">';
			foreach ( $QUES as $qtid => $des ) {
				echo '<option value="' . $qtid . '"';
				if ($WFQ ['QuestionTypeID'] == $qtid) {
					echo ' selected';
				}
				echo '>' . $des . "\n";
			}
			echo '</select></td>';
		
			echo '<td align=center valign="top"><input type="hidden" name="' . $WFQ ['QuestionID'] . '" value="D"><input type="checkbox" name="' . $WFQ ['QuestionID'] . '-A" value="Y"' . $ckd1 . '></td>';
		
			echo '<td align=center valign="top">';
		
			if (($WFQ ['QuestionTypeID'] != 99) && ($WFQ ['QuestionTypeID'] != 98) && ($WFQ ['QuestionTypeID'] != 30)) {
				echo '<input type="checkbox" name="' . $WFQ ['QuestionID'] . '-R" value="Y"' . $ckd2 . '>';
			}
		
			echo '</td>';
		
			echo '<td align=center valign="top"><a href="' . $formscript . '?typeform=webforms&action=' . $action . '&FormID=' . $WFQ ['FormID'] . '&QuestionID=' . $WFQ ['QuestionID'] . '&WebFormID=' . $_REQUEST ['WebFormID'] . '&QuestionTypeID='.$WFQ ['QuestionTypeID'].'"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a></td>';
		
			echo '<td align="center" valign="top">';
		
			if ($i > 1) {
			    echo '<a href="' . $formscript . '?typeform=webforms&action=' . $action . '&WebFormID=' . htmlspecialchars($_REQUEST ['WebFormID']) . '&QuestionOrder=' . htmlspecialchars($WFQ ['QuestionOrder']) . '&direction=up">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_up.png" width="20" height="16" title="Up" border="0" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
			}
		
			if ($i < $rowcnt) {
				echo '<a href="' . $formscript . '?typeform=webforms&action=' . $action . '&WebFormID=' . $_REQUEST ['WebFormID'] . '&QuestionOrder=' . $WFQ ['QuestionOrder'] . '&direction=down">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_down.png" width="20" height="16" title="Down" border="0" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
			}
		
			echo '</td>';
		
			echo '<td align="center">';
			if ($WFQ ['DEL'] == "CUST") {
				echo '<a href="' . $formscript . '?typeform=webforms&action=' . $action . '&WebFormID=' . $_REQUEST ['WebFormID'] . '&delete=' . $WFQ ['QuestionID'] . '" onclick="return confirm(\'Are you sure you want to delete the following question?\n\n' . $WFQ ['Question'] . '\n\n\')"><img src="' .  IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
			} else {
				echo '<a href="' . $formscript . '?typeform=webforms&action=' . $action . '&WebFormID=' . $_REQUEST ['WebFormID'] . '&copy=' . $WFQ ['QuestionID'] . '" onclick="return confirm(\'Are you sure you want to copy the following question?\n\n' . $WFQ ['Question'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a>';
			}
			echo '</td>';
			echo '</tr>';
		
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach
	}
	
	
	echo '<tr>';
	echo '<td valign="middle" align="left" colspan="100%" height="60">';
	echo '<a href="' . $formscript . '?typeform=webforms&action=' . $action . '&WebFormID=' . $_REQUEST ['WebFormID'] . '&new=add">';
	echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Custom Question</b>';
	echo '</a>';
	echo '</td></tr>';
	
	echo '<tr>';
	echo '<td valign="middle" align="center" colspan="100%" height="60">';
	echo '<input type="hidden" name="process" value="questionlisting">';
	echo '<input type="hidden" name="action" value="edit">';
	echo '<input type="hidden" name="typeform" value="webforms">';
	echo '<input type="hidden" name="WebFormID" value="' . $_REQUEST['WebFormID'] . '">';
	echo '<input type="submit" value="Change Status" class="btn btn-primary">';
	
	echo '</td></tr>';


echo '</form>';
echo '</table>';

} // end else QuestionID
?>