<?php
require_once '../Configuration.inc';

require_once COMMON_DIR 		. 'formsInternal/ApplicantFormStatus.inc';
require_once IRECRUIT_DIR 		. 'applicants/EmailApplicant.inc';
require_once IRECRUIT_DIR 		. 'formsInternal/AddInternalForm.inc';


if(isset($_REQUEST['process']) && $_REQUEST['process'] == 'Y') {

    if($_REQUEST['NumberOfDays'] == 0) {
    	if($ReminderNotification == 0) {
    	    $ReminderNotification = 'Off';
    	}
    } else {
        $ReminderNotification = 'On';
    }
    
    $info   =   array(
        ":OrgID"                    =>  $OrgID,
        ":MultiOrgID"               =>  '',
        ":RequestID"                =>  $_REQUEST['RequestID'],
        ":ReminderNotification"     =>  $ReminderNotification,
        ":NumberOfDays"             =>  $_REQUEST['NumberOfDays'],
        ":UReminderNotification"    =>  $ReminderNotification,
        ":UNumberOfDays"            =>  $_REQUEST['NumberOfDays']
    );
    
    //Insert Update Forms Reminder Settings
    $IconnectFormsReminderSettingsObj->insUpdIconnFormsReminderSettings($info);
}
else if(isset($_REQUEST['process']) && $_REQUEST['process'] == 'Process') {

    $IFRS           =   $IconnectFormsReminderSettingsObj->getIconnectFormsReminderSettings($OrgID, "", $_REQUEST['RequestID']);

    //Set where condition
    $where          =   array("OrgID = :OrgID", "RequestID = :RequestID", "PresentedTo = 1", "Status < 3");
    //Set parameters
    $params         =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
    //Get InternalFormsAssigned Information Count
    $results        =   $FormsInternalObj->getInternalFormsAssignedInfo("OrgID, ApplicationID, RequestID", $where, "OrgID, ApplicationID", "", array($params));
    $apps_list      =   $results['results'];
    $apps_count     =   $results['count'];

    for($a = 0; $a < $apps_count; $a++) {
        $ApplicationID      =   $apps_list[$a]['ApplicationID'];
        $RequestID          =   $apps_list[$a]['RequestID'];

        $em                 =   internalFormTickler ( $OrgID, $ApplicationID, $RequestID, "" );
        $alertmessage      .=   'An email has been sent to: ' . $OrgID . " - " . $ApplicationID . " - " . $RequestID . " - " . $em . "\n";

        // Insert into ApplicantHistory
        $UpdateID = $USERID;
        $Comments = 'iConnect forms reminder email sent to: ' . $em;

        $job_application_history = array (
            "OrgID"                 =>  $OrgID,
            "ApplicationID"         =>  $ApplicationID,
            "RequestID"             =>  $RequestID,
            "ProcessOrder"          =>  "-6",
            "Date"                  =>  "NOW()",
            "UserID"                =>  $UpdateID,
            "Comments"              =>  $Comments
        );
        
        //Insert Job Application History
        G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );
    }
    
    //Update the last run date per organization
    $IconnectFormsReminderSettingsObj->updLastRunDate($OrgID, "", $_REQUEST['RequestID']);
}
?>