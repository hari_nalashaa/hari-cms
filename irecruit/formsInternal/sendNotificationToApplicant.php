<?php
require_once '../Configuration.inc';

include COMMON_DIR . 'formsInternal/ApplicantFormStatus.inc';
include IRECRUIT_DIR . 'applicants/EmailApplicant.inc';

if ($_REQUEST ['resend'] == "Y") {
	if (isset($_REQUEST['ApplicationID']) && isset($_REQUEST['RequestID'])) {
			
		$em = internalFormTickler ( $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID'], "" );
			
		$alertmessage = 'An email has been sent to: ' . $em;

		if($alertmessage != 'An email has been sent to: no one') {
		    // Insert into ApplicantHistory
		    $UpdateID	=	$USERID;
		    $Comments	=	'iConnect forms reminder email sent to: ' . $em;
		    
		    $job_application_history	=	array(
										        "OrgID"                 =>  $OrgID,
										        "ApplicationID"         =>  $_REQUEST['ApplicationID'],
										        "RequestID"             =>  $_REQUEST['RequestID'],
										        "ProcessOrder"          =>  "-6",
										        "Date"                  =>  "NOW()",
										        "UserID"                =>  $UpdateID,
										        "Comments"              =>  $Comments
										    );
		    
		    G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );
		}
		
		echo $alertmessage;
	} // end if ApplicationID and RequestID
} // end resend
?>