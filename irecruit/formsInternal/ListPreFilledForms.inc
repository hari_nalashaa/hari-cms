<?php
//set where condition
$where = array("OrgID = :OrgID");
//set parameters
$params = array(":OrgID"=>$OrgID);
//get prefilled forms information
$results = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, Form", array($params));

$prefillcnt = $results['count'];

// Display
if(is_array($results['results'])) {
	foreach($results['results'] as $PFF) {
	
		if ($PFF ['SortOrder'] % 2 == 0) {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	
		$form_data = "";
	
		$form_data .= '<tr bgcolor="' . $rowcolor . '" id="'.$OrgID."*".$PFF ['PreFilledFormID']."*"."PreFilledForm".'">';
	
		$form_data .= '<td>';
		$form_data .= 'Federal & State Forms';
		$form_data .= '</td>';
	
		$form_data .= '<td>PDF & HTML</td>';
		$form_data .= '<td>' . $PFF ['TypeForm'] . ": " . $PFF ['FormName'] . '</td>';
	
		//For testing i keep this one as hidden variable
		/*
		$form_data .= '<td>';
		$form_data .= '<input type="hidden" ';
		$form_data .= 'size="2" maxlength="2" value="'.$PFF ['SortOrder'].'"';
		$form_data .= ' onchange=\'updateInterAssignedFormSortOrder("'.$OrgID.'", "'.$PFF ['PreFilledFormID'].'", "PreFilledForm", this.value)\'>';
		$form_data .= '</td>';
		*/
		if ($permit ['Internal_Forms_Edit'] > 0
		    && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
			$form_data .= '<td align="center">&nbsp;</td>';
			$form_data .= '<td align="center">&nbsp;</td>';
			$form_data .= '<td align="center">';
			$form_data .= '<a href="' . $formscript . '?typeform=prefilledforms&action=delete&PreFilledFormID=' . $PFF ['PreFilledFormID'] . '" onclick="return confirm(\'Are you sure you want to delete the following form?\n\n' . $PFF ['TypeForm'] . ': ' . $PFF ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
			$form_data .= '</td>';
		} // end permit Internal_Forms_Edit
	
		$form_data .= '<td>&nbsp;</td>';
	
		$form_data .= '</tr>' . "\n";
	
		$sort_forms[$PFF ['SortOrder']] = $form_data;
	} // end foreach
}
?>
