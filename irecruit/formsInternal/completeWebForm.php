<?php
$PAGE_TYPE  =   "PopUp";
require_once '../Configuration.inc';
$TemplateObj->title         = $title            = 'View Internal Form';

$TemplateObj->RequestID     = $RequestID        = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->ApplicationID	= $ApplicationID    = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->WebFormID     = $WebFormID        = isset($_REQUEST['WebFormID']) ? $_REQUEST['WebFormID'] : '';
$TemplateObj->HoldID        = $HoldID           = isset($_REQUEST['HoldID']) ? $_REQUEST['HoldID'] : '';
$TemplateObj->typeform      = $typeform         = isset($_REQUEST['typeform']) ? $_REQUEST['typeform'] : '';
$TemplateObj->process       = $process          = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->edit          = $edit             = isset($_REQUEST['edit']) ? $_REQUEST['edit'] : '';
$TemplateObj->k             = $k                = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/formsInternal/CompleteWebForm.inc';

	//Code to set date pickers year range
	$web_form_ques_res		=	G::Obj('WebFormQuestions')->getWebFormQuestionsListByQueTypeID($OrgID, $WebFormID, '17');
	$web_form_ques_list		=	$web_form_ques_res['results'];
	
	$validate_dates_info	=	array();
	for($o = 0; $o < count($web_form_ques_list); $o++) {
		$validate_dates_info[$web_form_ques_list[$o]['QuestionID']] 	=	json_decode($web_form_ques_list[$o]['Validate'], true);
	}
	
	$validate_dates_info	=	json_encode($validate_dates_info);
	
	if(isset($lst)) {
		?>
		<script>
		var lst					=	'<?php echo $lst?>';
		var validate_dates_info	=	'<?php echo $validate_dates_info;?>';
		validate_dates_info		=	JSON.parse(validate_dates_info);

		var date_split_ids		=	lst.split(",");
		var date_objs			=	new Array();

		var i = 0;
		var date_id = "";
		var year_range = "";
		for(date_id_key in date_split_ids) {
			date_id		=	date_split_ids[i];
			date_id		=	$.trim(date_id);
			year_range	=	getYearRange(validate_dates_info, date_id);
			date_picker(date_id, 'mm/dd/yy', year_range, '');
			i++;
		}
		</script>
		<?php
	}
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/formsInternal/CompleteWebForm');
}
?>