<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title	= $title = 'Change Status of Internal Form';

$TemplateObj->RequestID	= $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->ApplicationID	= $ApplicationID = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';

$TemplateObj->PreFilledFormID = $PreFilledFormID = isset($_REQUEST['PreFilledFormID']) ? $_REQUEST['PreFilledFormID'] : '';
$TemplateObj->AgreementFormID = $AgreementFormID = isset($_REQUEST['AgreementFormID']) ? $_REQUEST['AgreementFormID'] : '';
$TemplateObj->WebFormID = $WebFormID = isset($_REQUEST['WebFormID']) ? $_REQUEST['WebFormID'] : '';
$TemplateObj->SpecificationFormID = $SpecificationFormID = isset($_REQUEST['SpecificationFormID']) ? $_REQUEST['SpecificationFormID'] : '';

$TemplateObj->StatusCode = $StatusCode = isset($_REQUEST['StatusCode']) ? $_REQUEST['StatusCode'] : '';
$TemplateObj->UniqueID = $UniqueID = isset($_REQUEST['UniqueID']) ? $_REQUEST['UniqueID'] : '';
$TemplateObj->FormType = $FormType = isset($_REQUEST['FormType']) ? $_REQUEST['FormType'] : '';
$TemplateObj->process = $process = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';

include IRECRUIT_DIR . 'views/formsInternal/ChangeStatus.inc';
?>