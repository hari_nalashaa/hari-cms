<?php
require_once '../Configuration.inc';

//Template Variables
$ApplicationID      =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$CompleteByInterval =   isset($_REQUEST['CompleteByInterval']) ? $_REQUEST['CompleteByInterval'] : '';
$CompleteByFactor   =   isset($_REQUEST['CompleteByFactor']) ? $_REQUEST['CompleteByFactor'] : '';
$PresentedTo        =   isset($_REQUEST['PresentedTo']) ? $_REQUEST['PresentedTo'] : '';
$AssignStatus       =   isset($_REQUEST['AssignStatus']) ? $_REQUEST['AssignStatus'] : '';
$action             =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$Active             =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$process            =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$k                  =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$InternalFormInfo   =   isset($_REQUEST['InternalForms']) ? $_REQUEST['InternalForms'] : '';
$next_run_date      =   $IconnectFormsReminderSettingsObj->getNextRunDate($OrgID, "", $RequestID);
$IFRS               =   $IconnectFormsReminderSettingsObj->getIconnectFormsReminderSettings($OrgID, "", $RequestID);
$Packageid          =   isset($_REQUEST['Packageid']) ? $_REQUEST['Packageid'] : '';


if(isset($_POST ['InternalForms'])) { 
	foreach ($_POST ['InternalForms'] as $InternalForm) {
		include IRECRUIT_DIR . 'formsInternal/ProcessAssignedInternalForms.inc';
	}
}else if (isset($_POST['Packageid'])){ 
		include IRECRUIT_DIR . 'formsInternal/ProcessAssignedInternalForms.inc';

}
else if ($process == "F") { 
	include IRECRUIT_DIR . 'formsInternal/ProcessAssignedInternalForms.inc';
}
 
include_once IRECRUIT_VIEWS . 'formsInternal/AssignInternal.inc';
?>