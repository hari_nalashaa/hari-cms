<?php 
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

$TemplateObj->ApplicationID     =   $ApplicationID      =   $_REQUEST['ApplicationID'];
$TemplateObj->RequestID         =   $RequestID          =   $_REQUEST['RequestID'];
$TemplateObj->PreFilledFormID   =   $PreFilledFormID    =   $_REQUEST['PreFilledFormID'];
$TemplateObj->title             =   $title              =  "Prefilled Form View";
$TemplateObj->displayFormHeader =   $displayFormHeader  =   displayHeader ( $ApplicationID, $RequestID, "No" );

if($ServerInformationObj->getRequestSource() == 'ajax') {
    //view for AddApplicant.inc
    require_once COMMON_DIR . 'formsInternal/PreFilledFormView.inc';
}
else {
    //view for AddApplicant.inc
    $TemplateObj->default_view_dir = COMMON_DIR;
    echo $TemplateObj->displayIrecruitTemplate('formsInternal/PreFilledFormView');
}
?>