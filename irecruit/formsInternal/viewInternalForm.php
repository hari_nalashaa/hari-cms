<?php
$PAGE_TYPE  =   "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title             =   $title              =   'View Internal Form';
$TemplateObj->RequestID         =   $RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->ApplicationID     =   $ApplicationID      =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->typeform          =   $typeform           =   isset($_REQUEST['typeform']) ? $_REQUEST['typeform'] : '';
$TemplateObj->specformid        =   $specformid         =   isset($_REQUEST['specformid']) ? $_REQUEST['specformid'] : '';
$TemplateObj->k                 =   $k                  =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->PreFilledFormID   =   $PreFilledFormID    =   isset($_REQUEST['PreFilledFormID']) ? $_REQUEST['PreFilledFormID'] : '';
$TemplateObj->WebFormID         =   $WebFormID          =   isset($_REQUEST['WebFormID']) ? $_REQUEST['WebFormID'] : '';
$TemplateObj->AgreementFormID   =   $AgreementFormID    =   isset($_REQUEST['AgreementFormID']) ? $_REQUEST['AgreementFormID'] : '';

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
&& strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	include IRECRUIT_DIR . 'views/formsInternal/ViewInternalForm.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/formsInternal/ViewInternalForm');
}
?>