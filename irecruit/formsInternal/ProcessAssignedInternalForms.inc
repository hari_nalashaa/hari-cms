<?php 
 
// Finalize all completed forms
if ($process == "F") { 

	// set where condition
	$where = array (
			"OrgID			= :OrgID",
			"ApplicationID 	= :ApplicationID",
			"RequestID 		= :RequestID",
			"Status 		= '3'"
	);
	// set parameters
	$params = array (
			":OrgID" 			=> $OrgID,
			":ApplicationID" 	=> $ApplicationID,
			":RequestID" 		=> $RequestID
	);
	// get internal forms assigned information
	$results = $FormsInternalObj->getInternalFormsAssignedInfo ( "*", $where, "", "", array (
			$params
	) );

	if (is_array ( $results ['results'] )) {

		foreach ( $results ['results'] as $IFA ) {
				
			// Add History
			$Comment = "Form Finalized";
				
			// Insert Internal Form History
			$int_form_history = array (
					"OrgID" 			=> $OrgID,
					"ApplicationID" 	=> $ApplicationID,
					"RequestID" 		=> $RequestID,
					"InternalFormID" 	=> $IFA ['UniqueID'],
					"Date" 				=> "NOW()",
					"UserID" 			=> $USERID,
					"Comments" 			=> $Comment
			);
			// Set Internal Form History
			$FormsInternalObj->insInternalFormHistory ( $int_form_history );
				
			// set information
			$set_info = array (
					"Status = '4'",
					"LastUpdated = NOW()"
			);
			// set where condition
			$where = array (
					"OrgID 				= :OrgID",
					"ApplicationID 		= :ApplicationID",
					"RequestID 			= :RequestID",
					"UniqueID 			= :UniqueID"
			);
			// set parameters
			$params = array (
					":OrgID" 			=> $OrgID,
					":ApplicationID" 	=> $ApplicationID,
					":RequestID" 		=> $RequestID,
					":UniqueID" 		=> $IFA ['UniqueID']
			);
				
			// Update Internal Forms Assigned
			$FormsInternalObj->updInternalFormsAssigned ( $set_info, $where, array (
					$params
			) );
		} // end foreach
	}

	// end process F

	// Process Add new Form
} else if ($process == "A") { 


	list ( $UniqueID, $FormType, $FormName, $Restrict ) = explode ( '|', $InternalForm );

	if ($TypeForm != "") {
		$FormName = $TypeForm . ":" . $FormName;
	}

	if ($AssignStatus == "immediately") {
		$AssignStatus =   0;
		$Status       =   $PresentedTo;

		if (($FormType == "Agreement") && ($Restrict == "Secondary")) {
			$Status = 0;
		} // end FormType
		if (($FormType == "PreFilled") && ($Restrict == "Primary")) {
			$Status = 0;
		} // end FormType

		$PresentedTo 			= !isset($PresentedTo) ? '' : $PresentedTo;
		$AssignStatus 			= !isset($AssignStatus) ? '' : $AssignStatus;
		$CompleteByInterval 	= !isset($CompleteByInterval) ? '' : $CompleteByInterval;
		$CompleteByFactor 		= !isset($CompleteByFactor) ? '' : $CompleteByFactor;
			
		$ins_int_forms_assigned	=	array(
										"OrgID"			=> $OrgID,
										"ApplicationID"		=> $ApplicationID,
										"RequestID"		=> $RequestID,
										"UniqueID"		=> $UniqueID,
										"FormType"		=> $FormType,
										"FormName"		=> $FormName,
										"PresentedTo"		=> $PresentedTo,
										"Status"		=> $Status,
										"LastUpdated"		=> "NOW()",
										"AssignedDate"		=> "NOW()",
										"DueDate"		=> "DATE_ADD(NOW(), INTERVAL $CompleteByInterval $CompleteByFactor)"
									);

		// Set inbuilt functions those are going to insert
		$inbuilt_func =   array ("DATE_ADD(NOW(), INTERVAL $CompleteByInterval $CompleteByFactor)");
		// Insert Internal Forms Assigned
		$result       =   G::Obj('FormsInternal')->insInternalFormsAssigned ( $ins_int_forms_assigned, $inbuilt_func );

		if ($result ['affected_rows'] > 0) { // only insert history if new insert was successful
			// Add History
			$Comment = "Form Assigned";
				
			$int_form_his_info   =   array (
                        				"OrgID" 			=> $OrgID,
                        				"ApplicationID" 	=> $ApplicationID,
                        				"RequestID" 		=> $RequestID,
                        				"InternalFormID" 	=> $UniqueID,
                        				"Date" 				=> "NOW()",
                        				"UserID" 			=> $USERID,
                        				"Comments" 			=> $Comment
                        			 );
			G::Obj('FormsInternal')->insInternalFormHistory ( $int_form_his_info );
		} // end result
	}else { // else AssignStatus immediately

		if ($FormType != "PreFilled") { // PreFilledForms are auto assigned so they are not set by requisition
			//internal forms information
				
            $PresentedTo 		= !isset($PresentedTo) ? '' : $PresentedTo;
            $CompleteByInterval = !isset($CompleteByInterval) ? '' : $CompleteByInterval;
            $CompleteByFactor 	= !isset($CompleteByFactor) ? '' : $CompleteByFactor;

			foreach ($AssignStatus as $AssignStatusValue) {
                
			    // set parameters
			    $params              =   array (
                                            ":OrgID"        => $OrgID,
                                            ":RequestID"    => $RequestID,
                                            ":UniqueID"     => $UniqueID,
                                            ":AssignStatus" => $AssignStatusValue,
                        			     );
			    // Set where condition
			    $where_delete        =   array (
                        			        "OrgID 			= :OrgID",
                        			        "RequestID 		= :RequestID",
                        			        "UniqueID 		= :UniqueID",
                        			        "FormStatus 	= 'Active'",
                                            "AssignStatus 	= :AssignStatus",
                        			     );
			    //Delete Forms having status is 'Deleted'
			    //$del_int_form_res  =   G::Obj('Forms')->delFormsInfo('InternalForms', $where_delete, array($params));
			    
			    $int_forms_info      =   array(
                        			        "OrgID"					=>	$OrgID,
                        			        "RequestID"				=>	$RequestID,
                        			        "FormName"				=>	$FormName,
                        			        "FormType"				=>	$FormType,
                        			        "UniqueID"				=>	$UniqueID,
                        			        "PresentedTo"			=>	$PresentedTo,
                        			        "AssignStatus"			=>	$AssignStatusValue,
                        			        "CompleteByInterval"	=>	$CompleteByInterval,
                        			        "CompleteByFactor"		=>	$CompleteByFactor
                        			     );
			    G::Obj('Forms')->insFormsInfo('InternalForms', $int_forms_info);
			    
			}
			
				
		} // end FormType PreFilled
	} // end AssignStatus immediately
} // end process = A

 if($process == "Iconnect"){  
    
      //Set Parameters
	$params    =   array(':OrgID'=>$OrgID,'IconnectPackagesHeaderID'=>$Packageid);
	//Set condition
	$where     =   array("OrgID = :OrgID","IconnectPackagesHeaderID= :IconnectPackagesHeaderID", "FormStatus = 'Active'");
	//Get Internal Forms Information
         $results   =   G::Obj('IconnectPackages')->getIconnectFormsInfo("*", $where, "", array($params));
        
	 $resultsheader   =   G::Obj('IconnectPackagesHeader')->geticonnectPackagesHeader($OrgID,$Packageid);
           
           
	$MultiOrgID =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
        $REQS = G::Obj('Requisitions')->getReqDetailInfo('Title', $OrgID, $MultiOrgID, $RequestID);


	 
 

                foreach($results['results'] as $k=>$datum){ 
                          // set where condition
		            $where = array (
		                "OrgID         = :OrgID",
		                //"UniqueID      = :UniqueID",
                                "FormName      = :FormName",
 		            );
		            // set parameters
		            $params = array (
		                ":OrgID"           => $OrgID,
		                //":UniqueID"        => $UniqueID,
                                ":FormName"        => $datum['FormName'],
		            );
	
			$params    =   array(':OrgID'=>$OrgID, ':RequestID'=>$RequestID,':FormName' =>$datum['FormName']);
	        //Set condition
	          $where     =   array("OrgID = :OrgID", "RequestID = :RequestID", "FormStatus = 'Active'","FormName = :FormName");
	      //Get Internal Forms Information
	    $internalresults   =   G::Obj('FormsInternal')->getInternalFormsInfo("*", $where, "", array($params));

           
              if(empty($internalresults['results'])){ 
			$int_forms_info      =   array(
                        			        "OrgID"					=>	$OrgID,
                        			        "RequestID"				=>	$RequestID,
                        			        "FormName"				=>	$datum['FormName'],
                        			        "FormType"				=>	$datum['FormType'],
                        			        "UniqueID"				=>	$datum['UniqueID'],
                        			        "PresentedTo"			        =>	$datum['PresentedTo'],
                        			        "AssignStatus"			        =>	$datum['AssignStatus'],
                        			        "CompleteByInterval"	                =>	$datum['CompleteByInterval'],
                        			        "CompleteByFactor"		        =>	$datum['CompleteByFactor']
                        			     );

  
			   $resultinternal =  G::Obj('Forms')->insFormsInfo('InternalForms', $int_forms_info);


             }

		           
	}
           if (isset($resultinternal['affected_rows']) && !empty($resultinternal['affected_rows'])) {


		$req_history = array(
         "OrgID"             =>  $OrgID,
         "RequestID"         =>  $RequestID,
	 "RequestTitle"      =>  $REQS['Title'],
         "Comments"          =>  'Added New Package -'.$resultsheader['PackageName'],
	 "UpdatedFields"     =>  '',//json_encode(array("Updated Fields"=>array("LastModified"=>"NOW()"))),
         "CreatedDateTime"   =>  "NOW()",
         "UserID"            =>  $USERID
        ); 
        // Insert
        $insert_req_history = G::Obj('RequisitionHistory')->insRequisitionHistory($req_history);

       }    


}

 if($process == "Iconnectapplicanthistory") {

  		//Set Parameters
	$params    =   array(':OrgID'=>$OrgID,'IconnectPackagesHeaderID'=>$Packageid);
	//Set condition
	$where     =   array("OrgID = :OrgID","IconnectPackagesHeaderID= :IconnectPackagesHeaderID", "FormStatus = 'Active'");
	//Get Internal Forms Information
         $results   =   G::Obj('IconnectPackages')->getIconnectFormsInfo("*", $where, "", array($params));
                    
       	 foreach($results['results'] as $k=>$datum){ 	
		
				
			$params    =   array(':OrgID'=>$OrgID, ':RequestID'=>$RequestID,':FormName' =>$datum['FormName'],':ApplicationID'=>$ApplicationID);
	           //Set condition
	          $where     =   array("OrgID = :OrgID", "RequestID = :RequestID","FormName = :FormName","ApplicationID = :ApplicationID");
	          //Get Internal Forms Information
                   $assignedresults = G::Obj('FormsInternal')->getInternalFormsAssignedInfo ( "*", $where, "", "", array ($params) );
                  $AssignStatus =   0;
		 $Status       =   $PresentedTo;

		
		$PresentedTo 			= !isset($PresentedTo) ? '' : $PresentedTo;
		$AssignStatus 			= !isset($AssignStatus) ? '' : $AssignStatus;
		$CompleteByInterval 	= !isset($datum['CompleteByInterval']) ? '' : $datum['CompleteByInterval'];
		$CompleteByFactor 		= !isset($datum['CompleteByFactor']) ? '' : $datum['CompleteByFactor'];
			
		$ins_int_forms_assigned	=	array(
										"OrgID"			=> $OrgID,
										"ApplicationID"	=> $ApplicationID,
										"RequestID"		=> $RequestID,
										"UniqueID"		=> $datum['UniqueID'],
										"FormType"		=> $datum['FormType'],
										"FormName"		=> $datum['FormName'],
										"PresentedTo"	=>  $datum['PresentedTo'],
										"Status"		=>1,//$datum['AssignStatus'],
										"LastUpdated"	=> "NOW()",
								                 "AssignedDate"	=> "NOW()",
										 "DueDate"		=> "DATE_ADD(NOW(), INTERVAL $CompleteByInterval $CompleteByFactor)"
									);

		// Set inbuilt functions those are going to insert
		$inbuilt_func =   array ("DATE_ADD(NOW(), INTERVAL $CompleteByInterval $CompleteByFactor)");
             
              	 if(empty($assignedresults['results'])){           
		// Insert Internal Forms Assigned
		$result_assign       =   G::Obj('FormsInternal')->insInternalFormsAssigned ( $ins_int_forms_assigned, $inbuilt_func );
		
		}

		// form history here
		if ($result_assign ['affected_rows'] > 0) { // only insert history if new insert was successful
                        // Add History
                        $Comment = "Form Assigned";

                        $int_form_his_info   =   array (
                                                        "OrgID"                         => $OrgID,
                                                        "ApplicationID"         	=> $ApplicationID,
							"RequestID"              	=>   $RequestID,
                                                        "InternalFormID"        	=> $datum['UniqueID'],
                                                        "Date"                          => "NOW()",
                                                        "UserID"                        => $USERID,
                                                        "Comments"                      => $Comment
                                                 );
                        G::Obj('IconnectPackages')->insIconnectFormHistory ( $int_form_his_info );

		} // affected rows
                                    

         } 

	 $resultsheader   =   G::Obj('IconnectPackagesHeader')->geticonnectPackagesHeader($OrgID,$Packageid);

           if (isset($result_assign['affected_rows']) && !empty($result_assign['affected_rows'])) {

		// Job Application History
	         $job_app_history = array (
			"OrgID"                  =>  $OrgID,
			"ApplicationID"          =>  $ApplicationID,
			"RequestID"              =>   $RequestID,
			"ProcessOrder"           =>  "-3",
			"StatusEffectiveDate"    =>  "DATE(NOW())",
			"Date"                   =>  "NOW()",
			"UserID"                 =>  $USERID,
			"Comments"               =>  "iConnect-Package added: ".$resultsheader['PackageName']
	  );
	
	// Insert Job Application History
	$apphistory = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
       
          }
       

 }
?>
