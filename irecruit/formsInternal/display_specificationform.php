<?php
require_once '../irecruitdb.inc';

if (isset($_REQUEST['OrgID']) && isset($_REQUEST['specformid'])) {
	
	//set where condition
	$where = array("OrgID = :OrgID", "SpecificationFormID = :SpecificationFormID");
	//set parameters
	$params = array(":OrgID"=>$_REQUEST['OrgID'], ":SpecificationFormID"=>$_REQUEST['specformid']);
	//get specification forms information
	$results = $FormsInternalObj->getSpecificationForms("*", $where, "", array($params));
	
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $IMG) {
			$displaytitle = $IMG ['DisplayTitle'];
			$attfile = $IMG ['Attachment'];
			$atttype = $IMG ['AttachmentType'];
			$attext = $IMG ['AttachmentExt'];
		}	
	}
	
	$filename = $displaytitle . "." . $attext;
	
	$browser = get_browser( null, true );
	if ($browser['browser'] == "IE") {
			
		header ( 'Content-Description: File Transfer' );
		header ( 'Content-Type: application/octet-stream' );
		header ( 'Content-Disposition: inline; filename="' . $filename . '"' );
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		header ( 'Content-Length: ' . filesize ( $filename ) );
		echo $attfile;
		exit ();
	} else {
		header ( 'Content-Description: File Transfer' );
		header ( "Content-type: $atttype" );
		header ( 'Content-Disposition: attachment; filename=' . $filename );
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		echo $attfile;
		exit ();
	}
	
} // end if OrgID, UpdateID
?>