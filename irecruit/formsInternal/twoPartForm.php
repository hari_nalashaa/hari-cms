<?php
require_once '../Configuration.inc';

//set where condition
$where_info = array("AgreementFormID = :AgreementFormID");
//set parameters
$params = array(":AgreementFormID"=>$_REQUEST['AgreementFormID']);
//get agreement forms information
$results = $FormsInternalObj->getAgreementFormsInfo("LinkedTo", $where_info, "", array($params));

//get LinkedTo
$LinkedTo = $results['results'][0]['LinkedTo'];


//set where condition
$where_info = array("AgreementFormID = :AgreementFormID");
//set parameters
$params = array(":AgreementFormID"=>$LinkedTo);
//get agreement forms information
$results = $FormsInternalObj->getAgreementFormsInfo("FormName", $where_info, "", array($params));

//get FormName
$FormName = $results['results'][0]['FormName'];
?>
<div style="border: 1px solid #999999; background-color: #eeeeee; padding: 5px;">
	<div style="padding: 3px;">
		<strong>Insert Code Generator:</strong>
	</div>
	<div style="padding: 2px; margin-left: 5px;">
	<?php echo $FormName?>: 
	<select onchange="loadTwoPartFormQuestions(this.value,'','<?php echo htmlspecialchars($_REQUEST['AgreementFormID'])?>','<?php echo htmlspecialchars($_REQUEST['k'])?>');" style="width: 200px;">
		<option value="">Please Select</option>
		<?php
		//set parameters
		$params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$LinkedTo);
		//set where condition
		$where_info = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionTypeID NOT IN (99,98,30,60)");
		//get AgreementFormQuestions Information
		$results = $FormQuestionsObj->getQuestionsInformation('AgreementFormQuestions', "QuestionID, Question", $where_info, "", array($params));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $WEBQ) {
				echo '<option value="' . $WEBQ ['QuestionID'] . '"';
				if ($WEBQ ['QuestionID'] == $_REQUEST['IFQuestionID']) {
					echo ' selected';
					$Question = $WEBQ ['Question'];
				}
				echo '>' . $WEBQ ['Question'] . '</option>';
			} // end foreach
		}
		?>
	</select>
<?php
if ($_REQUEST['IFQuestionID']) {
	echo '{' . htmlspecialchars($_REQUEST['IFQuestionID']) . '}';
} // end IFQuestionID
echo "</div>";
?>
<div style="padding: 2px; margin-left: 5px;">
	Applicant Data: 
	<select onchange="loadTwoPartFormQuestions('',this.value,'<?php echo htmlspecialchars($_REQUEST['AgreementFormID'])?>','<?php echo htmlspecialchars($_REQUEST['k'])?>');" style="width: 200px;">
		<option value="">Please Select</option>
		<?php
		$PERSONAL = array (
				'first' => 'First Name',
				'last' => 'Last Name',
				'address' => 'Address Line One',
				'address2' => 'Address Line Two',
				'city' => 'City',
				'state' => 'State',
				'zip' => 'Zip Code',
				'email' => 'Email Address' 
		);
	
		foreach ( $PERSONAL as $QID => $Q ) {
			
			echo '<option value="' . $QID . '"';
			if ($QID == $_REQUEST['ADQuestionID']) {
				echo ' selected';
				$Question = $Q;
			}
			echo '>' . $Q . '</option>';
		} // end foreach
		?>
	</select>
	<?php
	if ($_REQUEST['ADQuestionID']) {
		echo '{' . htmlspecialchars($_REQUEST['ADQuestionID']) . '}';
	} // end ADQuestionID
	echo "</div>";
	
	echo "</div>";
	
	echo '<div style="height:10px;"></div>';
	?>