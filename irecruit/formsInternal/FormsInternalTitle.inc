<?php 
//set mostly using request variables
$TemplateObj->typeform = $typeform = isset($_REQUEST['typeform']) ? $_REQUEST['typeform'] : '';
$TemplateObj->action = $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->displaybox = $displaybox = isset($_REQUEST['displaybox']) ? $_REQUEST['displaybox'] : '';
$TemplateObj->process = $process = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->QuestionOrder = $QuestionOrder = isset($_REQUEST['QuestionOrder']) ? $_REQUEST['QuestionOrder'] : '';
$TemplateObj->direction = $direction = isset($_REQUEST['direction']) ? $_REQUEST['direction'] : '';
$TemplateObj->QuestionID = $QuestionID = isset($_REQUEST['QuestionID']) ? $_REQUEST['QuestionID'] : '';
$TemplateObj->Active = $Active = isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->Required = $Required = isset($_REQUEST['Required']) ? $_REQUEST['Required'] : '';
$TemplateObj->Question = $Question = isset($_REQUEST['Question']) ? $_REQUEST['Question'] : '';
$TemplateObj->formname = $formname = isset($_REQUEST['formname']) ? $_REQUEST['formname'] : '';
$TemplateObj->finish = $finish = isset($_REQUEST['finish']) ? $_REQUEST['finish'] : '';
$TemplateObj->cnt = $cnt = isset($_REQUEST['cnt']) ? $_REQUEST['cnt'] : '';
$TemplateObj->add = $add = isset($_REQUEST['add']) ? $_REQUEST['add'] : '';
$TemplateObj->default = $default = isset($_REQUEST['default']) ? $_REQUEST['default'] : '';

$TemplateObj->Clear = $Clear = isset($_REQUEST['Clear']) ? $_REQUEST['Clear'] : '';
$TemplateObj->AgreementFormID = $AgreementFormID = isset($_REQUEST['AgreementFormID']) ? $_REQUEST['AgreementFormID'] : '';
$TemplateObj->WebFormID = $WebFormID = isset($_REQUEST['WebFormID']) ? $_REQUEST['WebFormID'] : '';
$TemplateObj->FormID = $FormID = isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';

//Request Variables of FormStatus
$TemplateObj->statuslevel = $statuslevel = isset($_REQUEST['statuslevel']) ? $_REQUEST['statuslevel'] : '';
$TemplateObj->CompleteByInterval = $CompleteByInterval = isset($_REQUEST['CompleteByInterval']) ? $_REQUEST['CompleteByInterval'] : '';
$TemplateObj->CompleteByFactor = $CompleteByFactor = isset($_REQUEST['CompleteByFactor']) ? $_REQUEST['CompleteByFactor'] : '';

if(isset($_REQUEST['sort'])) $TemplateObj->sort = $sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : '';

$title = 'iConnect';

$subtitle = array("manage" => "Manage Internal Forms",
		  "packages" => "Packages",
		  "webforms" => "Web Forms",
                  "agreementforms" => "Agreement Forms",
                  "specforms" => "Specification Forms",
                  "prefilledforms" => "Federal & State Forms",
                  "formstatus" => "Form Status"
                  );

if ($typeform == "x") { $typeform = ""; }

if ($typeform != "") {
$title .= ' - ' . $subtitle[$typeform];
  if ($action) {
    $title .= " - " . $action;
  }
}

$TemplateObj->title = $title;
?>
