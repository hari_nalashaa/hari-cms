<?php
// prep limitations
$where_ifa = array();
if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$reqs = "";
	
	//set where condition
	$where = array("OrgID = :OrgID", "UserID = :UserID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
	//get requisition managers information
	$results = $RequisitionsObj->getRequisitionManagersInfo("RequestID", $where, "", "", array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $RM) {
			$reqs .= "'" . $RM ['RequestID'] . "',";
		}	
	}
	
	$reqs = substr ( $reqs, 0, - 1 );
	
	if ($reqs) {
		$where_ifa[] = "RequestID in ($reqs)";
	} // end if reqs
} // end hiring manager

echo '<form name="frmFormStatus" id="frmFormStatus" method="post">';
echo '<input type="hidden" name="AccessCode" id="AccessCode">';
echo '<input type="hidden" name="ApplicationID" id="ApplicationID">';
echo '<input type="hidden" name="RequestID" id="RequestID">';
echo '</form>';

echo 'The following applicants have forms assigned that are not complete.';
echo '<br><br>';

$from_date  =   date('m/d/Y', strtotime("-6 Months"));
$to_date    =   date('m/d/Y');
$sort_order =   "LastUpdated";

// Internal
$where_ifa[] = "OrgID = :OrgID";
$where_ifa[] = "Status IN ('1','2')";
$params = array(":OrgID"=>$OrgID);
if(isset($from_date) && $from_date != "" && isset($to_date) && $to_date) {
    if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] != "") {
        $from_date  =   $_REQUEST['from_date'];
    }
    if(isset($_REQUEST['to_date']) && $_REQUEST['to_date'] != "") {
        $to_date    =   $_REQUEST['to_date'];
    }

    // Internal
    $where_ifa[] = "DATE(LastUpdated) >= :FromLastUpdated";
    $where_ifa[] = "DATE(LastUpdated) <= :ToLastUpdated";
    $params[":FromLastUpdated"] = $DateHelperObj->getYmdFromMdy($from_date);
    $params[":ToLastUpdated"]   = $DateHelperObj->getYmdFromMdy($to_date);
}

if(isset($_REQUEST['requisitions_list']) && $_REQUEST['requisitions_list'] != "") {
    $where_ifa[] = "RequestID = :RequestID";
    $params[":RequestID"] = $_REQUEST['requisitions_list'];
}

//Columns List
$columns        =   "*";
//Get Internal Forms Assigned
$results        =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo($columns, $where_ifa, "ApplicationID, RequestID", $sort_order, array($params));

// Internal
$where_ifa      =   array("OrgID = :OrgID");
$where_ifa[]    =   "Status in ('1','2')";
$params         =   array(":OrgID"=>$OrgID);
$columns        =   "RequestID, (SELECT Title FROM Requisitions WHERE OrgID = InternalFormsAssigned.OrgID AND RequestID = InternalFormsAssigned.RequestID) AS Title";
$req_results    =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo($columns, $where_ifa, "ApplicationID, RequestID", $sort_order, array($params));

$i = 0;
if(is_array($results['results'])) {
	foreach($results['results'] as $IFA) {
	
		if (($IFA ['ApplicationID'] != "") && ($IFA ['RequestID'] != "")) {
			$i++;
	
			//set where condition
			$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "QuestionID IN ('first','last')");
			//set parameters
			$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$IFA ['ApplicationID']);
			//Get Applicant Data Information
			$resultsIN = $ApplicantsObj->getApplicantDataInfo("*", $where, '', '', array($params));
			
			foreach($resultsIN['results'] as $AD) {
				if ($AD ['QuestionID'] == "first") {
					$FirstName = $AD ['Answer'];
				}
				if ($AD ['QuestionID'] == "last") {
					$LastName = $AD ['Answer'];
				}
			}
	
			$LIST [$i] ['FirstName']     =   $FirstName;
			$LIST [$i] ['LastName']      =   $LastName;
			$LIST [$i] ['ApplicationID'] =   $IFA ['ApplicationID'];
			$LIST [$i] ['RequestID']     =   $IFA ['RequestID'];
			$LIST [$i] ['LastUpdated']   =   $IFA ['LastUpdated'];
			$LIST [$i] ['Title']         =   $IFA ['Title'];
		}
	} // end foreach	
}

echo '<form name="frmRefineFormStatus" id="frmRefineFormStatus" method="GET">';
echo '<table class="table table-bordered">';
echo '<tr>';
echo '<td colspan="10">';
echo '<select name="requisitions_list" id="requisitions_list" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';

$req_ids = array();
foreach ($req_results['results'] as $ListInfo) {
    if($ListInfo['Title'] != "" && !in_array($ListInfo['RequestID'], $req_ids)) {
        $req_selected = '';
        
        if(isset($_REQUEST['requisitions_list']) && $_REQUEST['requisitions_list'] != "") {
        	$req_selected = ' selected="selected"';
        }
        
        echo "<option value='".$ListInfo['RequestID']."' ".$req_selected.">".$ListInfo['Title']."</option>";
        $req_ids[] = $ListInfo['RequestID'];
    }
}

echo '</select>&nbsp;&nbsp;';
echo 'From: <input type="text" class="form-control width-auto-inline" name="from_date" id="from_date" value="'.htmlspecialchars($from_date).'">&nbsp;&nbsp;';
echo 'To: <input type="text" class="form-control width-auto-inline" name="to_date" id="to_date" value="'.htmlspecialchars($to_date).'">&nbsp;&nbsp;';
echo '<input type="submit" name="btnRefine" id="btnRefine" class="btn btn-primary" value="Refine">';
echo  '</td>';
echo '</tr>';
echo '</table>';
echo '</form>';

echo '<table class="table table-bordered">';
echo '<tr>';
echo '<td colspan="10">';
echo "<i>Note:*</i> To make the filter efficient please don't use it for large dates";
echo '</td>';
echo '</tr>';
echo '</table>';


echo '<div id="reminder_notification_msg" style="color:blue;padding-top:10px;padding-bottom:10px;">';
if(isset($_GET['msg']) && $_GET['msg'] == "notificationsucc") {
	echo 'Notification sent successfully.';
}
echo '</div>';

echo '<form name="frmReminderSendNotification" id="frmReminderSendNotification" method="POST">';
echo '<table class="table table-bordered" id="data-form-status">';
echo '<thead>';
echo '<tr>';
echo '<th><input type="checkbox" name="chkAllSendReminder" id="chkAllSendReminder" onclick="check_uncheck(this.checked, \'chkSendReminder\')"></th>';
echo '<th valign="top">Application ID</th>';
echo '<th valign="top">First Name</th>';
echo '<th valign="top">Last Name</th>';
echo '<th valign="top">RequisitionID/JobID</th>';
echo '<th valign="top">Pending Forms</th>';
echo '<th valign="top">Completed Forms</th>';
echo '<th valign="top">Finalized Forms</th>';
echo '<th valign="top">Last Updated Date</th>';
echo '<th valign="top">Send Reminder</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';
if(is_array($LIST)) {
	foreach ( $LIST as $key => $row ) {
	
	    $ApplicationID         =   $row ['ApplicationID'];
	    $RequestID             =   $row ['RequestID'];
	    
	    $ApplicantFormID       =   $ApplicantDetailsObj->getFormID($OrgID, $ApplicationID, $RequestID);
	    
	    if($ApplicantFormID != "") {
	        $assigned_forms_info   =   $FormsInternalObj->getApplicantAssignedFormsCountInfo($OrgID, $row ['ApplicationID'], $row ['RequestID']);
	        $assign_id             =   $OrgID."-".$ApplicationID."-".$RequestID;
	        $multiorgid_req        =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $row['RequestID']);
	        $reqid_jobid           =   $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $row['RequestID'] );
	        $mail_notification_id  =   "reminder_notification_msg";
	        $email_link = 'sendNotificationToApplicant.php?ApplicationID=' . $ApplicationID . "&RequestID=" . $RequestID . "&resend=Y";
	        if ($AccessCode != "") {
	            $email_link .= "&k=" . $AccessCode;
	        }
	        
	        echo '<tr>';
	        echo '<th><input type="checkbox" name="chkSendReminder[]" class="chkSendReminder" value="'.$ApplicationID."-".$RequestID.'"></th>';
	        echo '<td><a href="'.IRECRUIT_HOME.'applicantsSearch.php?ApplicationID='.$ApplicationID.'&RequestID='.$RequestID.'&tab_action=assign-iconnect-form" target="_blank">'.$row ['ApplicationID'].'</a></td>';
	        echo '<td>'.$row ['FirstName'].'</td>';
	        echo '<td>'.$row ['LastName'].'</td>';
	        echo '<td>'.$reqid_jobid.'</td>';
	        echo '<td>'.$assigned_forms_info['PendingFormsCount'].'</td>';
	        echo '<td>'.$assigned_forms_info['CompletedFormsCount'].'</td>';
	        echo '<td>'.$assigned_forms_info['FinalizedFormsCount'].'</td>';
	        echo '<td>'.$row['LastUpdated'].'</td>';
	        echo '<td style="min-width:140px;">';
	        echo '<a href="javascript:void(0);" onclick=\'sendEmailNotification("'.$email_link.'", "'.$mail_notification_id.'");\'>';
	        echo '<img src="' . IRECRUIT_HOME . 'images/icons/email.png" border="0" title="Tickler" style="margin:0px 8px -4px 8px;">';
	        echo '<b style="font-size:8pt;color:#000000;">Send Reminder</b></a>';
	        echo '&nbsp;&nbsp;&nbsp;&nbsp;<span id="send_mail_'.$OrgID.'-'.$ApplicationID.'-'.$RequestID.'" style="color:#428bca"></span>';
	        echo '</td>';
	        echo '</tr>';
	    }
    }
}
echo '</tbody>';

echo '<tr>';
echo '<td colspan="10" style="color:blue">';
echo '<input type="submit" name="btnSendReminder" id="btnSendReminder" class="btn btn-primary" value="Send Reminder">';
echo '</td>';
echo '</tr>';

echo '</table>';
echo '</form>';
?>
<script>
$(document).ready(function() {
    $('#data-form-status').DataTable({
        responsive: true,
        aLengthMenu: [
          [50, 100, -1],
          [50, 100, "All"]
        ]
    });
});

function getUrlParameter(urlString, sParam) {
	var vars = [], hash;
    var sPageURL = decodeURIComponent(urlString);
	
    var hashes = sPageURL.slice(sPageURL.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    
    return vars[sParam];
}
</script>
