<?php  
require_once '../Configuration.inc';

//Template Variables
$ApplicationID      =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$CompleteByInterval =   isset($_REQUEST['CompleteByInterval']) ? $_REQUEST['CompleteByInterval'] : '';
$CompleteByFactor   =   isset($_REQUEST['CompleteByFactor']) ? $_REQUEST['CompleteByFactor'] : '';
$PresentedTo        =   isset($_REQUEST['PresentedTo']) ? $_REQUEST['PresentedTo'] : '';
$AssignStatus       =   isset($_REQUEST['AssignStatus']) ? $_REQUEST['AssignStatus'] : '';
$action             =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$Active             =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$process            =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$k                  =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$InternalFormInfo   =   isset($_REQUEST['InternalForms']) ? $_REQUEST['InternalForms'] : '';
$next_run_date      =   $IconnectFormsReminderSettingsObj->getNextRunDate($OrgID, "", $RequestID);
$IFRS               =   $IconnectFormsReminderSettingsObj->getIconnectFormsReminderSettings($OrgID, "", $RequestID);
if(!empty($_REQUEST['packagename'])){
$packagename        =  $_REQUEST['packagename'];
}else{
 $packagename        = $_SESSION['PackageName'];
}

if(isset($_POST ['InternalForms'])) {    
	foreach ($_POST ['InternalForms'] as $InternalForm) { 
		include IRECRUIT_DIR . 'formsInternal/ProcessAssignedIconnectForms.inc';

	}
} 
else if ($process == "F") {
	include IRECRUIT_DIR . 'formsInternal/ProcessAssignedIconnectForms.inc';
}

include_once IRECRUIT_VIEWS . 'formsInternal/Iconnectpackage.inc';

?>