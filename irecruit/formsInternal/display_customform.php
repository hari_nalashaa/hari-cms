<?php
require_once '../irecruitdb.inc';

$columns    =   "OrgID, ApplicationID, RequestID";
$APPLICANT  =   $ApplicationsObj->getJobApplicationsDetailInfo($columns, $_GET['OrgID'], $_GET['ApplicationID'], $_GET['RequestID']);

if (($FillPDFObj->isCustomPDFForm($APPLICANT['OrgID'],$_REQUEST['WebFormID'])) && ($APPLICANT['OrgID'] != "")) {
    
    if ($_REQUEST['WebFormID'] == "54230bd0a5aa5") {
        $dfile = $FillPDFObj->createCRFPDF('CA-CRS', '54230bd0a5aa5', $APPLICANT['OrgID'], $APPLICANT['ApplicationID'], $APPLICANT['RequestID']);
    } else {
        $dfile = $FillPDFObj->createCustomPDF($_REQUEST['WebFormID'], $APPLICANT['OrgID'], $APPLICANT['ApplicationID'], $APPLICANT['RequestID']);
    }
    
    if (file_exists($dfile)) {
        
        $browser = get_browser(null, true);
        
        if ($browser['browser'] == "IE") {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: inline; filename="' . $dfile . '"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($dfile));
            ob_clean();
            flush();
            readfile($dfile);
            exit();
        } else {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($dfile));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control:');
            header('Pragma: cache');
            header('Content-Length: ' . filesize($dfile));
            readfile($dfile);
            exit();
        } // end browser
    } // end dfile
} // end APPLICANT
?>
