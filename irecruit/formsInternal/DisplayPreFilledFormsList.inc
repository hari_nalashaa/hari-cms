<?php
//set where condition
$where = array("OrgID = :OrgID");
//set parameters
$params = array(":OrgID"=>$OrgID);
//Get prefilled form information
$results = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, Form", array($params));
$prefillcnt = $results['count'];

// Display
echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table table-striped table-bordered">' . "\n";
if ($prefillcnt > 0) {
	echo '<tr>';
	echo '<td width="200"><b>Display</b></td>';
	echo '<td width="330"><b>Form Name</b></td>';
	echo '<td width="330"><b>&nbsp;</b></td>'; // Sort Order
	if ($permit ['Internal_Forms_Edit'] > 0) {
		echo '<td align="center" width="60"><b style="color:#ffffff;">Copy</b></td>';
		echo '<td align="center" width="60"><b style="color:#ffffff;">Edit</b></td>';
		echo '<td align="center" width="60"><b>Delete</b></td>';
	} // end permit Internal_Forms_Edit
} // end ptlcnt
echo '</tr>' . "\n";

$rowcolor = "#eeeeee";
if(is_array($results['results'])) {
	foreach($results['results'] as $PFF) {
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>PDF & HTML</td>';
		echo '<td>' . $PFF ['TypeForm'] . ": " . $PFF ['FormName'] . '</td>';
	
		echo '<td>';
		echo '<input type="hidden" ';
		echo 'size="2" maxlength="2" value="' . $PFF ['SortOrder'] . '"';
		echo ' onchange=\'updateInterAssignedFormSortOrder("' . $OrgID . '", "' . $PFF ['PreFilledFormID'] . '", "PreFilledForm", this.value)\'>';
		echo '</td>';
	
		if ($permit ['Internal_Forms_Edit'] > 0) {
	
			echo '<td align="center">&nbsp;</td>';
			echo '<td align="center">&nbsp;</td>';
	
			echo '<td align="center">';
	
			echo '<a href="' . $formscript . '?typeform=prefilledforms&action=delete&PreFilledFormID=' . $PFF ['PreFilledFormID'] . '" onclick="return confirm(\'Are you sure you want to delete the following form?\n\n' . $PFF ['TypeForm'] . ': ' . $PFF ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
	
			echo '</td>';
		} // end permit Internal_Forms_Edit
	
		echo '</tr>' . "\n";
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}

if ($prefillcnt == 0) {
	
	echo '<tr>';
	echo '<td style="text-align:left;width:770px;height:60px;">';
	echo 'There are no forms assigned.';
	echo '</td>';
	echo '</tr>';
}
echo '</table>' . "\n";
?>