<?php
global $RequisitionsObj, $FormsInternalObj;
// set where condition
$where = array (
		"OrgID            =   :OrgID",
		"RequestID        =   :RequestID",
		"AssignStatus     =   :AssignStatus",
		"FormStatus       =   'Active'" 
);
// set parameters
$params = array (
		":OrgID"          =>  $OrgID,
		":RequestID"      =>  $RequestID,
		":AssignStatus"   =>  $ProcessOrder 
);

// get internal forms information
$results = $FormsInternalObj->getInternalFormsInfo ( "*", $where, "", array ($params) );

if (is_array ( $results ['results'] )) {
	foreach ( $results ['results'] as $IF ) {
		
		$Status = $IF ['PresentedTo'];
		
		if ($IF ['FormType'] == "Agreement") {
			// set where condition
			$where = array ("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
			// set parameters
			$params = array (":OrgID" => $OrgID, ":AgreementFormID" => $IF ['UniqueID']);
			// get agreement form data
			$results = $FormsInternalObj->getAgreementFormsInfo ( "FormPart", $where, "", array ($params) );
			$FormPart = $results ['results'] [0] ['FormPart'];
			
			if ($FormPart == "Secondary") {
				$Status = 0;
			}
		}
		
		if($IF ['CompleteByInterval'] != "" && $IF ['CompleteByInterval'] != 0 && $IF ['CompleteByFactor'] != "") {
			$DueDate = "DATE_ADD(NOW(), INTERVAL " . $IF ['CompleteByInterval'] . " " . $IF ['CompleteByFactor']  . ")";
		}	
		else {
			$DueDate = "DATE_ADD(NOW(), INTERVAL 10 DAY)";
		}
		$inbuilt_func = array ($DueDate);
		
		$insert_inter_forms = array (
                "OrgID"         => $OrgID,
                "ApplicationID" => $ApplicationID,
                "RequestID"     => $RequestID,
                "UniqueID"      => $IF ['UniqueID'],
                "FormType"      => $IF ['FormType'],
                "FormName"      => $IF ['FormName'],
                "PresentedTo"   => $IF ['PresentedTo'],
                "Status"        => $Status,
                "LastUpdated"   => "NOW()",
                "AssignedDate"  => "NOW()",
                "DueDate"       => $DueDate
		);
		
		// Insert Internal Forms Assigned
		$result_int_forms_assigned = $FormsInternalObj->insInternalFormsAssigned ( $insert_inter_forms, $inbuilt_func );
		
		if (isset($result_int_forms_assigned['affected_rows']) && $result_int_forms_assigned['affected_rows'] > 0) {
			
			$Comment = 'Form Assigned';
			
			$internal_form_history = array (
                "OrgID"            => $OrgID,
                "ApplicationID"    => $ApplicationID,
                "RequestID"        => $RequestID,
                "InternalFormID"   => $IF ['UniqueID'],
                "Date"             => "NOW()",
                "UserID"           => $USERID,
                "Comments"         => $Comment 
			);
			// Internal Form History
			$FormsInternalObj->insInternalFormHistory ( $internal_form_history );
		} // end result
	} // end foreach
}

if ($feature ['PreFilledForms'] == 'Y') {
	
	// set where condition
	$where = array (
            "OrgID          =   :OrgID",
            "RequestID      =   :RequestID" 
	);
	// set paramaters
	$params = array (
            ":OrgID"        =>  $OrgID,
            ":RequestID"    =>  $RequestID 
	);
	// get requisition information
	$results_state = $RequisitionsObj->getRequisitionInformation ( "State", $where, "", "", array ($params) );
	$State = $results_state ['results'] [0] ['State'];
	
	// set where condition
	$where = array (
			"OrgID           =   :OrgID",
			"AssignStatus    =   :AssignStatus",
			"substring(PreFilledFormID,1,2) IN ('FE','$State')" 
	);
        if ($OrgID == 'I20140912') { // do not auto assign W-9 for CRF
          $where[] = "PreFilledFormID not in ('FE-w9')";
        }
	// set parameters
	$params = array (
			":OrgID"         => $OrgID,
			":AssignStatus"  => $ProcessOrder 
	);
	// get prefilled forms information
	$results = $FormsInternalObj->getPrefilledFormsInfo ( "*", $where, "", array ($params) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $PF ) {
			
			$Status = $PF ['PresentedTo'];
			
			if ($PF ['PreFilledFormID'] == "FE-I9m") {
				$Status = 0;
			}
			if ($PF ['PreFilledFormID'] == "FE-I9m-2020") {
				$Status = 0;
			}
			
			
			if($PF ['CompleteByInterval'] != "" && $PF ['CompleteByInterval'] != 0 && $PF ['CompleteByFactor'] != "") {
				$DueDate = "DATE_ADD(NOW(), INTERVAL " . $PF ['CompleteByInterval'] . " " . $PF ['CompleteByFactor'] . ")";
			}
			else {
				$DueDate = "DATE_ADD(NOW(), INTERVAL 10 DAY)";
			}
			//Inbuilt functions
			$inbuilt_func = array ($DueDate);
			
			$internal_forms_assigned =   array(
                                            "OrgID"             =>  $OrgID, 
                                            "ApplicationID"     =>  $ApplicationID, 
                                            "RequestID"         =>  $RequestID, 
                                            "UniqueID"          =>  $PF ['PreFilledFormID'], 
                                            "FormType"          =>  'PreFilled', 
                                            "FormName"          =>  $PF ['TypeForm'] . ': ' . $PF ['FormName'], 
                                            "PresentedTo"       =>  $PF ['PresentedTo'], 
                                            "Status"            =>  $Status, 
                                            "LastUpdated"       =>  "NOW()", 
                                            "AssignedDate"      =>  "NOW()", 
                                            "DueDate"           =>  $DueDate
			                             );
			
			//Insert Internal Forms Assigned Information
			$FormsInternalObj->insInternalFormsAssigned ( $internal_forms_assigned, $inbuilt_func );
			
			if (isset($result['affected_rows']) && $result['affected_rows'] > 0) {
				
				$Comment = 'Form Assigned';
				
				//insert internal form history
				$internal_form_history = array(
                                            "OrgID"             =>  $OrgID, 
                                            "ApplicationID"     =>  $ApplicationID, 
                                            "RequestID"         =>  $RequestID, 
                                            "InternalFormID"    =>  $PF ['PreFilledFormID'], 
                                            "Date"              =>  "NOW()", 
                                            "UserID"            =>  $USERID, 
                                            "Comments"          =>  $Comment
				                        );
				$FormsInternalObj->insInternalFormHistory($internal_form_history);
			} // end result
		} // end foreach
	}
} // end feature

internalFormTickler ( $OrgID, $ApplicationID, $RequestID, $ProcessOrder );
?>
