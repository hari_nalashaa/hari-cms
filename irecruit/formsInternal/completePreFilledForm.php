<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title	= $title = 'View Internal Form';


//Assign add applicant datepicker variables
$script_vars_footer[]	= 'var datepicker_ids = "#employmentdate, #listAexpdate1, #listAexpdate2, #listAexpdate3, #listBexpdate, #listCexpdate";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';

//Script vars footer
$TemplateObj->scripts_vars_footer = $script_vars_footer;

$TemplateObj->ApplicationID	= $ApplicationID = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID	= $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->PreFilledFormID = $PreFilledFormID = isset($_REQUEST['PreFilledFormID']) ? $_REQUEST['PreFilledFormID'] : '';
$TemplateObj->typeform	= $typeform = isset($_REQUEST['typeform']) ? $_REQUEST['typeform'] : '';
$TemplateObj->process = $process = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->k	= $k = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->HoldID = $HoldID = isset($_REQUEST['HoldID']) ? $_REQUEST['HoldID'] : '';
$TemplateObj->edit = $edit = isset($_REQUEST['edit']) ? $_REQUEST['edit'] : '';

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/formsInternal/CompletePreFilledForm.inc';
	
	if(isset($lst)) {
		?><script>date_picker('<?php echo $lst?>', 'mm/dd/yy');</script><?php
	}
}
else {
	echo $TemplateObj->displayIrecruitTemplate('views/formsInternal/CompletePreFilledForm');
}
?>