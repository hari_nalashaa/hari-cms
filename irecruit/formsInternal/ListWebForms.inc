<?php
//set where condition
$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
//set parameters
$params = array(":OrgID"=>$OrgID);
//get web forms information
$results = $FormsInternalObj->getWebFormsInfo("*", $where, "FormName", array($params));

$cnt = $results['count'];

if(is_array($results['results'])) {
	foreach($results['results'] as $WFQ) {
	
		if ($WFQ ['SortOrder'] % 2 == 0) {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	
		$form_data = "";
	
		$form_data .= '<tr bgcolor="' . $rowcolor . '" id="'.$OrgID."*".$WFQ ['WebFormID']."*"."WebForm".'">';
	
		$form_data .= '<td>';
		$form_data .= 'Web Forms';
		$form_data .= '</td>';
	
		$form_data .= '<td>';
		$form_data .= 'Web Form';
		$form_data .= '</td>';
	
		$form_data .= '<td>';
		$form_data .= $WFQ ['FormName'];
		$form_data .= '</td>';
	
		//For testing i keep this one as hidden variable
		/*
		$form_data .= '<td>';
		$form_data .= '<input type="hidden" ';
		$form_data .= 'size="2" maxlength="2" value="'.$WFQ ['SortOrder'].'"';
		$form_data .= ' onchange=\'updateInterAssignedFormSortOrder("'.$OrgID.'", "'.$WFQ ['WebFormID'].'", "WebForm", this.value)\'>';
		$form_data .= '</td>';
		*/
		if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
	
			$form_data .= '<td align="center">';
			$form_data .= '<a href="' . $formscript . '?typeform=webforms&action=copy&WebFormID=' . $WFQ ['WebFormID'] . '" onclick="return confirm(\'Are you sure you want to copy the following form?\n\n' . $WFQ ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a>';
			$form_data .= '</td>';
	
		    $form_data .= '<td align="center">';
		    $form_data .= '<a href="' . $formscript . '?typeform=webforms&action=edit&WebFormID=' . $WFQ ['WebFormID'] . '">';
		    $form_data .= '<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit">';
		    $form_data .= '</a>';
		    $form_data .= '</td>';
	
			$form_data .= '<td align="center">';
			$form_data .= '<a href="' . $formscript . '?typeform=webforms&action=delete&WebFormID=' . $WFQ ['WebFormID'] . '" onclick="return confirm(\'Are you sure you want to delete the following form?\n\n' . $WFQ ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
			$form_data .= '</td>';
		} // end permit Internal_Forms_Edit
	
		$form_data .= '<td align="center">';
	
		$link = IRECRUIT_HOME . "formsInternal/completeWebForm.php?WebFormID=" . $WFQ ['WebFormID'];
		if ($typeform) {
			$link .= "&typeform=" . $typeform;
		}
		if ($AccessCode != "") {
			$link .= "&k=" . $AccessCode;
		}
	
		$form_data .= '<a href="#" onclick="javascript:window.open(\'' . $link . '\',\'_blank\',\'location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes\');"><img src="' . IRECRUIT_HOME . 'images/icons/application_form.png" border="0" title="View Form" style="margin:0px 3px -4px 0px;"></a>';
		$form_data .= '</td>';
	
		$form_data .= '</tr>';
	
		$sort_forms[$WFQ ['SortOrder']] = $form_data;
	
	} // end foreach	
}
?>