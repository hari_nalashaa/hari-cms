<?php
//set where condition
$where = array("OrgID = :OrgID", "FormStatus = :FormStatus");
//set parameters
$params = array(":OrgID"=>$OrgID, ":FormStatus"=>'Active');

if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Active')
{
    //set where condition
    $where = array("OrgID = :OrgID", "FormStatus = :FormStatus");
    //set parameters
    $params = array(":OrgID"=>$OrgID, ":FormStatus"=>'Active');
}
else if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Inactive')
{
    //set where condition
    $where = array("OrgID = :OrgID", "FormStatus = :FormStatus");
    //set parameters
    $params = array(":OrgID"=>$OrgID, ":FormStatus"=>'Inactive');
}

//get specification forms information
$results = $FormsInternalObj->getSpecificationForms("*", $where, "FormType, DisplayTitle", array($params));
$ptlcnt = $results['count'];

echo '<div id="process_specification_form_status"></div>';
// Display
echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table table-striped table-bordered">' . "\n";
echo '<tr>';
echo '<td colspan="4">Form Status:';
echo '<select name="ddlFormStatus" id="ddlFormStatus" onchange="getSpecificationFormsByStatus(this.value)">';
echo '<option value="Active"';
if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Active')
{
    echo ' selected="selected"';
}
echo '>Active</option>';
echo '<option value="Inactive"';
if(isset($_REQUEST['ddlFormStatus']) && $_REQUEST['ddlFormStatus'] == 'Inactive')
{
    echo ' selected="selected"';
}
echo '>Inactive</option>';
echo '</select>';
echo '</td>';
echo '</tr>' . "\n";
echo '</table>' . "\n";

// Display
echo '<table border="0" cellspacing="0" cellpadding="3" width="100%" class="table table-striped table-bordered">' . "\n";

if ($ptlcnt > 0) {
	echo '<tr>';
	echo '<td width="200"><b>Type of Display</b></td>';
	echo '<td width="330"><b>Display Name</b></td>';
	echo '<td width="330"><b>Status</b></td>';//Sort Order
	if (substr ( $USERROLE, 0, 21 ) == 'master_admin') {
	    echo '<td align="center" width="60"><b>Active</b></td>';
	}
	if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
		echo '<td align="center" width="60"><b>Edit</b></td>';
		if($_REQUEST['ddlFormStatus'] != "Deleted") {
		    echo '<td align="center" width="60"><b>Delete</b></td>';
		}
	} // end permit Internal_Forms_Edit
	echo '<td align="center" width="60"><b>View</b></td>';
} // end ptlcnt
echo '</tr>' . "\n";

$cnt = 0;
$rowcolor = "#eeeeee";
if(is_array($results['results'])) {
	foreach($results['results'] as $PTLINFO) {
		$cnt ++;
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>' . $PTLINFO ['FormType'] . '</td>';
	
		echo '<td>';
		echo $PTLINFO ['DisplayTitle'];
		echo '</td>';
	
		echo '<td>';
		echo $PTLINFO ['FormStatus'];
		//echo '<input type="hidden" ';
		//echo 'size="2" maxlength="2" value="'.$PTLINFO ['SortOrder'].'"';
		//echo ' onchange=\'updateInterAssignedFormSortOrder("'.$OrgID.'", "'.$PTLINFO ['SpecificationFormID'].'", "SpecificationForm", this.value)\'>';
		echo '</td>';
		
		if(substr ( $USERROLE, 0, 21 ) == 'master_admin') {
		    echo '<td align="center" width="60">';
		    echo '<input type="checkbox" name="chkSpecificationFormID'.$PTLINFO ['SpecificationFormID'].'" id="chkSpecificationFormID'.$PTLINFO ['SpecificationFormID'].'" value="'.$PTLINFO ['SpecificationFormID'].'" onclick=\'updateSpecificationFormStatus("'.$PTLINFO ['SpecificationFormID'].'", this.checked)\'';
		    if(isset($PTLINFO ['FormStatus']) && $PTLINFO ['FormStatus'] == "Active") {
		        echo ' checked="checked"';
		    }
		    echo  '>';
		    echo '</td>';
		}
		
		if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
		    echo '<td align="center"><a href="formsInternal.php?typeform=specforms&edit=' . $PTLINFO ['SpecificationFormID'] . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a></td>';
	
		    if($_REQUEST['ddlFormStatus'] != "Deleted") {
			    echo '<td align="center"><a href="formsInternal.php?typeform=specforms&delete=' . $PTLINFO ['SpecificationFormID'] . '" onclick="return confirm(\'Are you sure you want to delete this item?\n\nDisplay Title: ' . $PTLINFO ['DisplayTitle'] . '\nType of Display: ' . $PTLINFO ['FormType'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a></td>';
			}
		} // end permit Internal_Forms_Edit
	
		echo '<td align="center">';
		echo '<a href="';
	
		if ($PTLINFO ['FormType'] == "External Link") {
			echo $PTLINFO ['URL'];
		}
		if ($PTLINFO ['FormType'] == "Attachment") {
			echo IRECRUIT_HOME . 'formsInternal/display_specificationform.php?OrgID=' . $OrgID . '&specformid=' . $PTLINFO ['SpecificationFormID'];
		}
		if ($PTLINFO ['FormType'] == "HTML Page") {
	
			$link = IRECRUIT_HOME . "formsInternal/viewInternalForm.php?typeform=specforms&specformid=" . $PTLINFO ['SpecificationFormID'];
			if ($AccessCode != "") {
				$link .= "&k=" . $AccessCode;
			}
	
			$openwindow = " onclick=\"javascript:window.open('" . $link . "','_blank','location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes');";
	
			echo '#"' . $openwindow;
		}
	
		echo '"';
	
		if ($PTLINFO ['FormType'] == "External Link") {
			echo ' target="_blank"';
		}
		echo '>';
	
		if ($PTLINFO ['FormType'] == "External Link") {
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_picture.png" border="0" title="View External Link">';
		} else {
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View">';
		}
		echo '</a>';
		echo '</td>';
	
		echo '</tr>' . "\n";
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}

if ($ptlcnt == 0) {	
	echo '<tr>';
	echo '<td style="text-align:left;width:770px;height:60px;">';
	echo 'There are no forms assigned.';
	echo '</td>';
	echo '</tr>';
}

echo '</table>' . "\n";
?>