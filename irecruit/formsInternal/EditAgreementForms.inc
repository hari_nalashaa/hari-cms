<?php
if (isset($_REQUEST['copy']) && $_REQUEST['copy'] != "") {
	$id = uniqid ();
	
	// set where condition
	$where = array (
			"OrgID               =   :OrgID",
			"AgreementFormID     =   :AgreementFormID",
			"QuestionID          =   :QuestionID" 
	);
	// set parameters
	$params = array (
			":OrgID"             =>  $OrgID,
			":AgreementFormID"   =>  $_REQUEST['AgreementFormID'],
			":QuestionID"        =>  $_REQUEST['copy'] 
	);
	// set columns
	$columns       =   "OrgID, AgreementFormID, concat('CUST',QuestionID,'$id'), concat('Copy: ', Question), QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder+1, Required, Validate";
	$results_copy  =   $FormQuestionsObj->getQuestionsInformation('AgreementFormQuestions', "Question", $where, "", array($params));
	
	if($results_copy['count'] == 0) {
		// Insert AgreementFormQuestions Master
		$FormQuestionsObj->insFormQuestionsMaster ( 'AgreementFormQuestions', $columns, $where, array ($params) );
	}
	
	// set columns
	$columns   =   "AgreementFormID, QuestionID";
	// set where condition
	$where     =   array ("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	// set parameters
	$params    =   array (":OrgID" => $OrgID, ":AgreementFormID" => $_REQUEST['AgreementFormID']);
	// Get agreement form questions
	$results   =   $FormsInternalObj->getAgreementFormQuestions ( $columns, $where, "QuestionOrder", array ($params) );
	$cnt       =   0;
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $WFQT ) {
			$cnt ++;
			
			// Set update query fields
			$set = array (
					"QuestionOrder     =   :QuestionOrder" 
			);
			// Bind the parameters
			$params = array (
					":QuestionOrder"   =>  $cnt,
					":OrgID"           =>  $OrgID,
					":AgreementFormID" =>  $_REQUEST['AgreementFormID'],
					":QuestionID"      =>  $WFQT ['QuestionID'] 
			);
			// Set Condition
			$where = array (
					"OrgID             =   :OrgID",
					"AgreementFormID   =   :AgreementFormID",
					"QuestionID        =   :QuestionID" 
			);
			// Update AgreementFormQuestions
			$FormQuestionsObj->updQuestionsInfo ( 'AgreementFormQuestions', $set, $where, array ($params) );
		} // end foreach
	}
} // end copy

if (isset($_REQUEST['delete']) && $_REQUEST['delete'] != "") {
	
	// Delete AgreementFormQuestions
	$where     =   array (
            			"OrgID               =   :OrgID",
            			"AgreementFormID     =   :AgreementFormID",
            			"QuestionID          =   :QuestionID" 
            	   );
	// set parameters
	$params    =   array (
            			":OrgID"             =>  $OrgID,
            			":AgreementFormID"   =>  $_REQUEST['AgreementFormID'],
            			":QuestionID"        =>  $_REQUEST['delete'] 
            	   );
	// Delete FormQuestions Information
	$res_agree_form_ques = $FormQuestionsObj->delQuestionsInfo ( 'AgreementFormQuestions', $where, array($params) );
	
	// set where condition
	$where     =   array (
            			"OrgID               =   :OrgID",
            			"AgreementFormID     =   :AgreementFormID" 
                	);
	// set parameters
	$params    =   array (
            			":OrgID"             =>  $OrgID,
            			":AgreementFormID"   =>  $_REQUEST['AgreementFormID'] 
                	);
	// Get agreementformquestions
	$FormsInternalObj->getAgreementFormQuestions ( "AgreementFormID, QuestionID", $where, "QuestionOrder", array ($params) );
	
	$cnt = 0;
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $WFQT ) {
			$cnt ++;
			
			// Set update query fields
			$set = array (
					"QuestionOrder     =   :QuestionOrder" 
			);
			// Bind the parameters
			$params = array (
					":QuestionOrder"   =>  $cnt,
					":OrgID"           =>  $OrgID,
					":AgreementFormID" =>  $_REQUEST['AgreementFormID'],
					":QuestionID"      =>  $WFQT ['QuestionID'] 
			);
			// Set Condition
			$where = array (
					"OrgID             =   :OrgID",
					"AgreementFormID   =   :AgreementFormID",
					"QuestionID        =   :QuestionID" 
			);
			// Update AgreementFormQuestions
			$FormQuestionsObj->updQuestionsInfo ( 'AgreementFormQuestions', $set, $where, array ($params) );
		} // end foreach
	}
} // end delete

if (isset($_REQUEST['new']) && $_REQUEST['new'] == "add") {
	
	// set where condition
	$where     =   array ("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	// set parameters
	$params    =   array (":OrgID" => $OrgID, ":AgreementFormID" => $_REQUEST['AgreementFormID']);
	// Get agreementformquestions
	$results   =   $FormsInternalObj->getAgreementFormQuestions ( "AgreementFormID, MAX(QuestionOrder) MaxQuestionOrder", $where, "", array ($params) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $WFQT ) {
			$formid          =   $WFQT ['AgreementFormID'];
			$questionorder   =   $WFQT ['MaxQuestionOrder'] + 1;
		}
	}
	
	if(($formid == '' || !isset($formid)) && isset($_REQUEST['AgreementFormID'])) $formid = $_REQUEST['AgreementFormID'];
	
	$questionid = CUST . uniqid ();
	
	$agree_form_que_info   =   array (
                                    "OrgID"             =>  $OrgID,
                                    "AgreementFormID"   =>  $formid,
                                    "QuestionID"        =>  $questionid,
                                    "Question"          =>  'New question added.',
                                    "QuestionTypeID"    =>  '5',
                                    "size"              =>  '0',
                                    "maxlength"         =>  '0',
                                    "rows"              =>  '0',
                                    "cols"              =>  '0',
                                    "wrap"              =>  'virtual',
                                    "value"             =>  '',
                                    "Active"            =>  'Y',
                                    "QuestionOrder"     =>  $questionorder
	                               );
	
	//Insert Agreement Form Questions
	$FormQuestionsObj->insFormQuestions ( 'AgreementFormQuestions', $agree_form_que_info );
	
} // end new question

if (($QuestionOrder != "") & ($direction != "")) {
	
	if ($direction == "up") {
		
		$minus = $QuestionOrder - 1;
		
		$params = array();
		//set parameters
		$params[] = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionOrder"=>$minus, ":SQuestionOrder"=>'999');
		//set parameters
		$params[] = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionOrder"=>$QuestionOrder, ":SQuestionOrder"=>$minus);
		//set parameters
		$params[] = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionOrder"=>'999', ":SQuestionOrder"=>$QuestionOrder);

		
		//set information
		$set_info = array("QuestionOrder = :SQuestionOrder");
		//set where condition
		$where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionOrder = :QuestionOrder");
		//Update Agreement Form Questions
		$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, $params);
		
	} else if ($direction == "down") {
		
		$plus = $QuestionOrder + 1;
		
		$params	= array();
		//set parameters
		$params[] = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionOrder"=>$plus, ":SQuestionOrder"=>"999");
		//set parameters
		$params[] = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionOrder"=>$QuestionOrder, ":SQuestionOrder"=>$plus);
		//set parameters
		$params[] = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AgreementFormID, ":QuestionOrder"=>"999", ":SQuestionOrder"=>$QuestionOrder);
		
		//set information
		$set_info = array("QuestionOrder = :SQuestionOrder");
		//set where condition
		$where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionOrder = :QuestionOrder");
		//Update Agreement Form Questions
		$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, $params);
		
	} // end direction
} // end sort feature

if ($process == "individual") {
	
	include IRECRUIT_DIR . 'formsInternal/ProcessAgreementFormQuestions.inc';
} // end process individual

if ($process == "questionlisting") {
	
	foreach ( $_POST as $key => $value ) {
		
		$pieces = explode ( "-", $key );
		
		if ($pieces [1] == 'questiontypeid') {
			
			//set parameters
			$params  =   array(":QuestionTypeID"=>$value);
			//set where condition
			$where   =   array("Editable = 'Y'", "QuestionTypeID = :QuestionTypeID");
			//get question types information
			$results =   G::Obj('QuestionTypes')->getQuestionTypesInfo("*", $where, "", array($params));
			$QTs     =   $results['results'][0];
			
			
			//set information
			$set_info    =   array("QuestionTypeID = :QuestionTypeID", "size = :size", "maxlength = :maxlength", "wrap = :wrap");
			//set where condition
			$where       =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
			//set parameters
			$params      =   array(":QuestionTypeID"=>$value, ":size"=>$QTs ['size'], ":maxlength"=>$QTs ['maxlength'], ":wrap"=>$QTs ['wrap'], ":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":QuestionID"=>$pieces [0]);
			
			if($value != 100) {
			    $set_info[]      =   "rows = :rows";
			    $set_info[]      =   "cols = :cols";
			    $params[":rows"] =   $QTs ['rows'];
			    $params[":cols"] =   $QTs ['cols'];
			}
			//Update Agreement Form Questions
			$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
			
		} // end pieces questiontypeid
		
		if ($pieces [1] == 'question') {
			
			//set information
			$set_info    =   array("Question = :Question");
			//set where condition
			$where       =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
			//set parameters
			$params      =   array(":Question"=>$value, ":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":QuestionID"=>$pieces [0]);
			//Update Agreement Form Questions
			$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
			
		} // end pieces question
		
		if ($value == 'D') {
			
			//set information
			$set_info    =   array("Active = ''", "Required = ''");
			//set where condition
			$where       =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
			//set parameters
			$params      =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":QuestionID"=>$key);
			//Update Agreement Form Questions
			$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
			
		}
		
		if ($value == 'Y') {
			
			if ($pieces [1] == 'A') {
				
				//set information
				$set_info   =   array("Active = 'Y'");
				//set where condition
				$where      =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
				//set parameters
				$params     =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":QuestionID"=>$pieces [0]);
				//Update Agreement Form Questions
				$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
				
			} else if ($pieces [1] == 'R') {
				
				//set information
				$set_info   =   array("Required = 'Y'");
				//set where condition
				$where      =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
				//set parameters
				$params     =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":QuestionID"=>$pieces [0]);
				//Update Agreement Form Questions
				$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
			}
		}
	} // end POST
	
	//set information
	$set_info  =   array("Question = 'Electronic Signature:'");
	//set where condition
	$where     =   array("OrgID = :OrgID", "QuestionTypeID = 30");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID);
	//Update Agreement Form Questions
	$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));

	
	//set information
	$set_info  =   array("Question = 'Merge Content:'");
	//set where condition
	$where     =   array("OrgID = :OrgID", "QuestionTypeID = 60");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID);
	//Update Agreement Form Questions
	$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
	
	
	//set information
	$set_info  =   array("Required = ''");
	//set where condition
	$where     =   array("OrgID = :OrgID", "QuestionTypeID = 99");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID);
	//Update Agreement Form Questions
	$FormQuestionsObj->updQuestionsInfo('AgreementFormQuestions', $set_info, $where, array($params));
	
	if ($_REQUEST ['formname']) {
		//set information
		$set_info =   array("FormName = :FormName");
		//set where condition
		$where    =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
		//set parameters
		$params   =   array(":FormName"=>$_REQUEST ['formname'], ":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID']);
		//Update AgreementForms
		$FormsInternalObj->updAgreementForms($set_info, $where, array($params));
	}
} // end process questionlisting

if(isset($_REQUEST['date_field']) && $_REQUEST['date_field'] != "") {
	//Set information
	$set_info		=	array("Validate = :Validate");
	//Where information
	$where_info		=	array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "QuestionID = :QuestionID");
	//Set parameters
	$params			=	array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":QuestionID"=>$QuestionID, ":Validate"=>json_encode($_POST['date_field']));
	//Update the table information based on bind and set values
	$res_que_info	=   G::Obj('FormQuestions')->updQuestionsInfo ( "AgreementFormQuestions", $set_info, $where_info, array ($params) );
}

if ($QuestionID) {
	
	?>

<form method="post" action="formsInternal.php">
	<table class="formtable" border="0" cellspacing="3" cellpadding="5"
		width="770" class="table table-striped table-bordered">
		<tr>
			<td><b>Edit Question</b></td>
		<tr>
		
		<?php
		  if($_REQUEST['QuestionTypeID'] == '3') {
		  	   ?>
        		<tr>
        			<td>
        			    <strong>Note*:</strong> If you want to prefill the data with the existing list of options available, please select any option in below pull down.
        			    <br>
        			    Prefill with existing data:
        			    <select name="ddlPrefilledQueData" id="ddlPrefilledQueData">
        			         <option value="">Select</option>
        			         <option value="States">States</option>
        			         <option value="Days">Days</option>
        			         <option value="Months">Months</option>
        			         <option value="SocialMedia">Social Media</option>
        			    </select>
        			    <br><br>
        			</td>
        		<tr>
		  	   <?php
		  }
		?>
		
		<tr>
			<td>
			<?php
			$formtable = "AgreementFormQuestions";
			include IRECRUIT_VIEWS . 'forms/AdminQuestions.inc';
			?>
			</td>
		</tr>
		<tr>
			<td align="center" height="60" valign="middle">
				<input type="hidden" name="QuestionID" value="<?php echo htmlspecialchars($QuestionID); ?>" /> 
				<input type="hidden" name="AgreementFormID" value="<?php echo htmlspecialchars($AgreementFormID); ?>" />
				<input type="hidden" name="action" value="<?php echo htmlspecialchars($action) ?>" /> 
				<input type="hidden" name="typeform" value="agreementforms" /> 
				<input type="hidden" name="QuestionTypeID" id="QuestionTypeID" value="<?php echo htmlspecialchars($_REQUEST['QuestionTypeID']); ?>">
				<input type="hidden" name="process" value="individual" /> 
				<input type="submit" value="Update" />
				<?php
				if ($process) {
					echo '<input type="submit" name="finish" value="Finish" />';
				} else {
					echo '<input type="submit" name="finish" value="Cancel" />';
				}
				?>
			</td>
		</tr>
		
		</form>
	</table>

<?php
	
	// display preview
	if ($Active == 'Y') {
		
		echo '<hr size="1"/>';
		echo '<div class="table-responsive"><table class="table table-striped table-bordered table-hover">';
		echo '<tr><td colspan="100%">';
		echo '<p><b>Preview</b></p>';
		$colwidth = "220";
		echo include COMMON_DIR . 'application/DisplayQuestions.inc';
		echo '</td></tr>';
		echo '</table></div>';
	}
} else { // QuestionID
	
	//set where condition
	$where     =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID']);
	//Get AgreementForms
	$resultsIN =   $FormsInternalObj->getAgreementFormsInfo("*", $where, "OrgID LIMIT 0,1", array($params));
	
	$AFN       =   $resultsIN['results'][0];
	
	echo "<p>Edit the <u>" . $AFN ['FormPart'] . "</u> form";
	
	if ($AFN ['FormPart'] == "Primary") {
		
		//set where condition
		$where    =   array("OrgID = :OrgID", "LinkedTo = :LinkedTo");
		//set parameters
		$params   =   array(":OrgID"=>$OrgID, ":LinkedTo"=>$AFN ['AgreementFormID']);
		//Get AgreementForms
		$results  =   $FormsInternalObj->getAgreementFormsInfo("*", $where, "", array($params));
		$afcnt    =   $results['count'];
		
		$i = 0;
		if ($afcnt > 0) {
			echo " for: ";
		} else {
			echo ".";
		}
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $AF) {
				$i ++;
				if ($i > 1) {
					echo ", ";
				}
				echo "<b>" . $AF ['FormName'] . "</b>";
			} // end foreach	
		}		
	} else {
		
		//set where condition
		$where    =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
		//set parameters
		$params   =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$AFN ['LinkedTo']);
		//Get AgreementForms
		$results  =   $FormsInternalObj->getAgreementFormsInfo("FormName", $where, "", array($params));
		$FormName =   $results['results'][0]['FormName'];
		
		if ($FormName) {
			echo " for: <b>" . htmlspecialchars($FormName) . "</b>";
		} else {
			echo ".";
		}
	}
	
	echo "</p>";
	
	echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="5" width="770" class="table table-striped table-bordered">';
	echo '<form method="post" action="formsInternal.php">';
	
	echo "<tr><td colspan=\"7\" valign=\"middle\" height=\"40\">Form Name: ";
	
	echo '<input type="text" name="formname" value="' . $AFN ['FormName'] . '" size="30" maxlength="45">';
	
	echo "</td></tr>";
	
	echo '<tr>';
	echo '<td width="300"><b>Question</b></td>';
	echo '<td align="center" width="120"><b>Question Type</b></td>';
	echo '<td align="center" width="60"><b>Active</b></td>';
	echo '<td align="center" width="10"><b>Req</b></td>';
	echo '<td align="center" width="60"><b>Edit</b></td>';
	echo '<td align="center" width="60"><b>Order</b></td>';
	echo '<td align="center" width="40"><b>Copy<br>Delete</b></td>';
	echo '</tr>';
	
	//set parameters
	$params    =   array(":QuestionTypeID"=>$value);
	//set where condition
	$where     =   array("Editable = 'Y'", "QuestionTypeID != '8'");
	
	if ($AFN ['FormPart'] == "Primary") {
		$where[]  =   "QuestionTypeID != '60'";
	}
	//get question types information
	$results   =   G::Obj('QuestionTypes')->getQuestionTypesInfo("*", $where, "SortOrder", array($params));
	
	$rowcolor  =   "#eeeeee";
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $QT) {
			$QUES [$QT ['QuestionTypeID']] = $QT ['Description'];
		}	
	}
	
	//set where condition
	$where     =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID']);
	//set columns
	$columns   =   "substring(QuestionID,1,4) DEL, AgreementFormID, QuestionID, Question, QuestionTypeID, Active, QuestionOrder, Required";
	// Get agreementformquestions
	$results   =   $FormsInternalObj->getAgreementFormQuestions ( $columns, $where, "QuestionOrder", array ($params) );
	$rowcnt    =   $results['count'];
	
	$i = 0;
	
	
	if(is_array($results['results'])) {

		foreach ($results['results'] as $WFQ) {
			$i ++;
		
			if ($WFQ ['Active'] == "Y") {
				$ckd1 = ' checked';
			} else {
				$ckd1 = '';
			}
			if ($WFQ ['Required'] == "Y") {
				$ckd2 = ' checked';
			} else {
				$ckd2 = '';
			}
		
			echo '<tr bgcolor="' . $rowcolor . '">';
		
			echo '<td>';
		
			if ($WFQ ['QuestionTypeID'] == '99') { // Instructions
				echo substr ( strip_tags ( $WFQ ['Question'] ), 0, 40 ) . "...";
			} else if ($WFQ ['QuestionTypeID'] == '30') { // Signature
				echo $WFQ ['Question'];
			} else if ($WFQ ['QuestionTypeID'] == '60') { // Form Merge Content
				echo $WFQ ['Question'];
			} else {
				echo '<input type="text" name="' . $WFQ ['QuestionID'] . '-question" value="';
				echo $WFQ ['Question'];
				echo '" size="50">';
			}
		
			echo '</td>';
		
			echo '<td align=center valign="top">';
			echo '<select name="' . $WFQ ['QuestionID'] . '-questiontypeid" style="width:120px;">';
			foreach ( $QUES as $qtid => $des ) {
				echo '<option value="' . $qtid . '"';
				if ($WFQ ['QuestionTypeID'] == $qtid) {
					echo ' selected';
				}
				echo '>' . $des . "</option>\n";
			}
			echo '</select></td>';
		
			echo '<td align=center valign="top"><input type="hidden" name="' . $WFQ ['QuestionID'] . '" value="D"><input type="checkbox" name="' . $WFQ ['QuestionID'] . '-A" value="Y"' . $ckd1 . '></td>';
		
			echo '<td align=center valign="top">';
		
			if (($WFQ ['QuestionTypeID'] != 99) && ($WFQ ['QuestionTypeID'] != 98) && ($WFQ ['QuestionTypeID'] != 30) && ($WFQ ['QuestionTypeID'] != 60)) {
				echo '<input type="checkbox" name="' . $WFQ ['QuestionID'] . '-R" value="Y"' . $ckd2 . '>';
			}
		
			echo '</td>';
		
			echo '<td align=center valign="top"><a href="formsInternal.php?typeform=agreementforms&action=' . $action . '&AgreementFormID=' . $WFQ ['AgreementFormID'] . '&QuestionID=' . $WFQ ['QuestionID'] . '&AgreementFormID=' . $_REQUEST ['AgreementFormID'] . '&QuestionTypeID=' . $WFQ ['QuestionTypeID'] . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a></td>';
		
			echo '<td align="center" valign="top">';
		
			if ($i > 1) {
				echo '<a href="formsInternal.php?typeform=agreementforms&action=' . $action . '&AgreementFormID=' . htmlspecialchars($_REQUEST ['AgreementFormID']) . '&QuestionOrder=' . $WFQ ['QuestionOrder'] . '&direction=up">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_up.png" width="20" height="16" title="Up" border="0" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
			}
		
			if ($i < $rowcnt) {
				echo '<a href="formsInternal.php?typeform=agreementforms&action=' . htmlspecialchars($action) . '&AgreementFormID=' . htmlspecialchars($_REQUEST ['AgreementFormID']) . '&QuestionOrder=' . htmlspecialchars($WFQ ['QuestionOrder']) . '&direction=down">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_down.png" width="20" height="16" title="Down" border="0" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
			}
		
			echo '</td>';
		
			echo '<td align="center">';
			if ($WFQ ['DEL'] == "CUST") {
				?>
				<a href="formsInternal.php?typeform=agreementforms&action=<?php echo htmlspecialchars($action);?>&AgreementFormID=<?php echo htmlspecialchars($_REQUEST ['AgreementFormID']);?>&delete=<?php echo htmlspecialchars($WFQ ['QuestionID']);?>" onclick="return confirm('Are you sure you want to delete the following question?\n\n<?php echo htmlspecialchars($WFQ ['Question']);?>')">
				<img src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png" border="0" title="Delete">
				</a>
				<?php
			} else {
				echo '<a href="formsInternal.php?typeform=agreementforms&action=' . $action . '&AgreementFormID=' . htmlspecialchars($_REQUEST ['AgreementFormID']) . '&copy=' . $WFQ ['QuestionID'] . '" onclick="return confirm(\'Are you sure you want to copy the following question?\n\n' . $WFQ ['Question'] . '\n\n\')">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy">';
				echo '</a>';
			}
			echo '</td>';
			echo '</tr>';
		
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach

	}
	
	
	echo '<tr>';
	echo '<td valign="middle" align="left" colspan="100%" height="60">';
	echo '<a href="formsInternal.php?typeform=agreementforms&action=' . $action . '&AgreementFormID=' . htmlspecialchars($_REQUEST ['AgreementFormID']) . '&new=add">';
	echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" title="Add" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Custom Question</b>';
	echo '</a>';
	echo '</td></tr>';
	
	echo '<tr>';
	echo '<td valign="middle" align="center" colspan="100%" height="60">';
	echo '<input type="hidden" name="process" value="questionlisting">';
	echo '<input type="hidden" name="action" value="edit">';
	echo '<input type="hidden" name="typeform" value="agreementforms">';
	echo '<input type="hidden" name="AgreementFormID" value="' . htmlspecialchars($AgreementFormID) . '">';
	echo '<input type="submit" value="Change Status">';
	
	echo '</td></tr>';
	
	echo '</form>';
	echo '</table></div>';
} // end else QuestionID
?>
