<?php 
// Finalize all completed forms
 $packageID = G::Obj('IconnectPackagesHeader')->getIconnectCategoryByPackageName($OrgID, $_SESSION['PackageName']);
  
if ($process == "F") {

	// set where condition
	$where = array (
			"OrgID			= :OrgID",
			"ApplicationID 	= :ApplicationID",
			"RequestID 		= :RequestID",
			"Status 		= '3'"
	);
	// set parameters
	$params = array (
			":OrgID" 			=> $OrgID,
			":ApplicationID" 	=> $ApplicationID,
			":RequestID" 		=> $RequestID
	);
	// get internal forms assigned information
	$results = $FormsInternalObj->getIconnectFormsAssignedInfo ( "*", $where, "", "", array (
			$params
	) );

	if (is_array ( $results ['results'] )) {

		foreach ( $results ['results'] as $IFA ) {
				
			// Add History
			$Comment = "Form Finalized";
				
			// Insert Internal Form History
			$int_form_history = array (
					"OrgID" 			=> $OrgID,
					"ApplicationID" 	=> $ApplicationID,
                                        "IconnectPackagesHeaderID" =>	$packageID['IconnectPackagesHeaderID'],
					"InternalFormID" 	=> $IFA ['UniqueID'],
					"Date" 				=> "NOW()",
					"UserID" 			=> $USERID,
					"Comments" 			=> $Comment
			);
			// Set Internal Form History
			$FormsInternalObj->insInternalFormHistory ( $int_form_history );
				
			// set information
			$set_info = array (
					"Status = '4'",
					"LastUpdated = NOW()"
			);
			// set where condition
			$where = array (
					"OrgID 				= :OrgID",
					"ApplicationID 		= :ApplicationID",
					"RequestID 			= :RequestID",
					"UniqueID 			= :UniqueID"
			);
			// set parameters
			$params = array (
					":OrgID" 			=> $OrgID,
					":ApplicationID" 	=> $ApplicationID,
					":RequestID" 		=> $RequestID,
					":UniqueID" 		=> $IFA ['UniqueID']
			);
				
			// Update Internal Forms Assigned
			$FormsInternalObj->updInternalFormsAssigned ( $set_info, $where, array (
					$params
			) );
		} // end foreach
	}
 
	// end process F

	// Process Add new Form
} else if ($process == "A") { 

	list ( $UniqueID, $FormType, $FormName, $Restrict ) = explode ( '|', $InternalForm );

	if ($TypeForm != "") {
		$FormName = $TypeForm . ":" . $FormName;
	}

	if ($AssignStatus == "immediately") {
		$AssignStatus =   0;
		$Status       =   $PresentedTo;

		if (($FormType == "Agreement") && ($Restrict == "Secondary")) {
			$Status = 0;
		} // end FormType
		if (($FormType == "PreFilled") && ($Restrict == "Primary")) {
			$Status = 0;
		} // end FormType

		$PresentedTo 			= !isset($PresentedTo) ? '' : $PresentedTo;
		$AssignStatus 			= !isset($AssignStatus) ? '' : $AssignStatus;
		$CompleteByInterval 	= !isset($CompleteByInterval) ? '' : $CompleteByInterval;
		$CompleteByFactor 		= !isset($CompleteByFactor) ? '' : $CompleteByFactor;
              
		$ins_int_forms_assigned	=	array(
										"OrgID"			=> $OrgID,
										"ApplicationID"	=> $ApplicationID,
                                                                                "IconnectPackagesHeaderID" =>	$packageID['IconnectPackagesHeaderID'],
										"UniqueID"		=> $UniqueID,
										"FormType"		=> $FormType,
										"FormName"		=> $FormName,
										"PresentedTo"	=> $PresentedTo,
										"Status"		=> $Status,
										"LastUpdated"	=> "NOW()",
										"AssignedDate"	=> "NOW()",
										"DueDate"		=> "DATE_ADD(NOW(), INTERVAL $CompleteByInterval $CompleteByFactor)"
									);

		// Set inbuilt functions those are going to insert
		$inbuilt_func =   array ("DATE_ADD(NOW(), INTERVAL $CompleteByInterval $CompleteByFactor)");
		// Insert Internal Forms Assigned
		$result       =   G::Obj('IconnectPackages')->insIconnectFormsAssigned ( $ins_int_forms_assigned, $inbuilt_func );

		if ($result ['affected_rows'] > 0) { // only insert history if new insert was successful
			// Add History
			$Comment = "Form Assigned";
				
			$int_form_his_info   =   array (
                        				"OrgID" 			=> $OrgID,
                        				"ApplicationID" 	=> $ApplicationID,
                                                        "IconnectPackagesHeaderID" =>	$packageID['IconnectPackagesHeaderID'],
                        				"InternalFormID" 	=> $UniqueID,
                        				"Date" 				=> "NOW()",
                        				"UserID" 			=> $USERID,
                        				"Comments" 			=> $Comment
                        			 );
			G::Obj('IconnectPackages')->insIconnectFormHistory ( $int_form_his_info );
		} // end result
	} else { // else AssignStatus immediately

		if ($FormType != "PreFilled") { // PreFilledForms are auto assigned so they are not set by requisition
			//internal forms information
				
            $PresentedTo 		= !isset($PresentedTo) ? '' : $PresentedTo;
            $CompleteByInterval = !isset($CompleteByInterval) ? '' : $CompleteByInterval;
            $CompleteByFactor 	= !isset($CompleteByFactor) ? '' : $CompleteByFactor;

			foreach ($AssignStatus as $AssignStatusValue) {
                
			    // set parameters
			    $params              =   array (
                                            ":OrgID"        => $OrgID,
                                            //":RequestID"    => $RequestID,
                                            ":UniqueID"     => $UniqueID,
                                            ":AssignStatus" => $AssignStatusValue,
                        			     );
			    // Set where condition
			    $where_delete        =   array (
                        			        "OrgID 			= :OrgID",
                        			       // "RequestID 		= :RequestID",
                        			        "UniqueID 		= :UniqueID",
                        			        "FormStatus 	= 'Active'",
                                            "AssignStatus 	= :AssignStatus",
                        			     );
			    //Delete Forms having status is 'Deleted'
			    //$del_int_form_res  =   G::Obj('Forms')->delFormsInfo('IconnectPackages', $where_delete, array($params));
			    
                   //set where condition
                       $where = array("OrgID = :OrgID","IconnectPackagesHeaderID = :IconnectPackagesHeaderID");
                      //set parameters
                      $params = array(":OrgID"=>$OrgID, ":IconnectPackagesHeaderID"=>$packageID['IconnectPackagesHeaderID'], ":PackageName"=>$packagename);
                       //set information
                      $set_info = array("PackageName = :PackageName");
                        //update agreement forms
                     G::Obj('IconnectPackagesHeader')->updiconnectPackagesHeader($set_info, $where, array($params));

                $_SESSION['PackageName'] =$packagename; 
        
      //Set Parameters
	$params    =   array(':OrgID'=>$OrgID,'FormName'=>$FormName);
	//Set condition
	$where     =   array("OrgID = :OrgID","FormName= :FormName");
	//Get Internal Forms Information
         $results   =   G::Obj('IconnectPackages')->getIconnectFormsInfo("*", $where, "", array($params));
         

                       
                           $int_forms_info      =   array(
                        			        "OrgID"					=>	$OrgID,
                                                        "IconnectPackagesHeaderID" =>	$packageID['IconnectPackagesHeaderID'],
                        			        "FormName"				=>	$FormName,
                        			        "FormType"				=>	$FormType,
                        			        "UniqueID"				=>	$UniqueID,
                        			        "PresentedTo"			=>	$PresentedTo,
                        			        "AssignStatus"			=>	$AssignStatusValue,
                        			        "CompleteByInterval"	=>	$CompleteByInterval,
                        			        "CompleteByFactor"		=>	$CompleteByFactor
                        			     );
                           if(empty($results['results'])){
			    G::Obj('Forms')->insFormsInfo('IconnectPackages', $int_forms_info);
                          }
   
			}
			
				
		} // end FormType PreFilled


	} // end AssignStatus immediately
} // end process = A
?>
