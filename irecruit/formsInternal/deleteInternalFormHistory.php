<?php 
require_once '../Configuration.inc';

if (isset($_REQUEST['delete']) && $_REQUEST['delete'] != "") {

	// insert data to form history archive
	$FormsInternalObj->insInternalFormHistoryArchive ( $_REQUEST['delete'] );

	// delete internal form history
	$internal_form_his_res = $FormsInternalObj->delInternalFormHistory ( $_REQUEST['delete'] );
	
	if($internal_form_his_res['affected_rows'] > 0) {
		echo "<span style='color:#428bca;font-weight:bold'>Deleted successfully</span><br>";
	}
	else {
		echo "<span style='color:red;font-weight:bold'>Sorry unable to delete, please try again</span><br>";
	}
}
?>