<?php
if ($feature ['PreFilledForms'] != "Y"    || $permit ['Internal_Forms_Edit'] < 1)  {

    include IRECRUIT_DIR . 'irecruit_permissions.err';
}
else {
    if ($action == "delete") {
    	//set where condition
    	$where = array("OrgID = :OrgID", "PreFilledFormID = :PreFilledFormID");
    	//set parameters
    	$params = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID);
    	//get prefilled forms information
    	$results = G::Obj('FormsInternal')->getPrefilledFormsInfo("TypeForm, FormName", $where, "TypeForm, FormName", array($params));
    	
    	$TypeForm = $results['results'][0]['TypeForm'];
    	$FormName = $results['results'][0]['FormName'];
    	
    	//set where condition
    	$where = array("OrgID = :OrgID", "PreFilledFormID = :PreFilledFormID");
    	//set parameters
    	$params = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$PreFilledFormID);
    	//Delete PrefilledForms Information
    	G::Obj('Forms')->delFormsInfo('PreFilledForms', $where, array($params));
    	
    	$msg = "You have removed form " . $TypeForm . ': ' . $FormName . ".\\n\\n";
    	
    	echo '<script language="JavaScript" type="text/javascript">';
    	echo "alert('" . $msg . "')";
    	echo '</script>';
    }
    ?>
    Select the forms you would like to activate.
    <br>
    <br>
    <?php 
    if(isset($msg) && $msg != "") {
    	echo "<span style='color:blue'>".$msg."</span>";
    }
    ?>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
    <?php
    $where = array("OrgID = 'MASTER'");
    $results = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, FormName", array());
    
    $i = 0;
    $ii = 0;
    
    if(is_array($results['results'])) {
    	foreach ($results['results'] as $MPFF) {
    	
    		//set where condition
    		$where = array("OrgID = :OrgID", "PreFilledFormID = :PreFilledFormID");
    		//set parameters
    		$params = array(":OrgID"=>$OrgID, ":PreFilledFormID"=>$MPFF ['PreFilledFormID']);
    		//get prefilled forms information
    		$resultsIN = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, Form", array($params));
    		
    		$active = "N";
    	
    		if ($title != $MPFF ['TypeForm']) {
    			$title = $MPFF ['TypeForm'];
    			$i = 0;
    			$ii ++;
    		}
    	
    		if ($i == 0) {
    			$FORMS [$ii] = "<b>" . $title . "</b><br>";
    		}
    	
    		$FORMS [$ii] .= '&nbsp;&nbsp;';
    		$FORMS [$ii] .= '<input type="checkbox" name="form[]" value="' . $MPFF ['PreFilledFormID'] . '"';
    		if ($resultsIN['count'] == 1) {
    			$FORMS [$ii] .= ' checked';
    			$active = "Y";
    		}
    		$FORMS [$ii] .= '> ';
    	
    		$FORMS [$ii] .= $MPFF ['FormName'];
    	
    		if ($active == "Y") {
    	
    			$FORMS [$ii] .= '<a href="#" onclick="javascript:window.open(\'' . IRECRUIT_HOME;
    			$FORMS [$ii] .= 'formsInternal/completePreFilledForm.php?PreFilledFormID=' . $MPFF ['PreFilledFormID'];
    			if ($AccessCode != "") {
    				$FORMS [$ii] .= "&k=" . $AccessCode;
    			}
    			$FORMS [$ii] .= "','_blank','location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes";
    			$FORMS [$ii] .= '\');"><img src="' . IRECRUIT_HOME . 'images/icons/application_form.png" border="0" title="View Form" style="margin:0px 3px -4px 6px;"></a>';
    	
    			$link = IRECRUIT_HOME . "formsInternal/display_prefilledform.php?OrgID=" . $OrgID . "&PreFilledFormID=" . $MPFF ['PreFilledFormID'];
    	
    			$FORMS [$ii] .= '<a href="' . $link . '" target="_blank">';
    			$FORMS [$ii] .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_acrobat.png" border="0" title="View PDF Form" style="margin:0px 8px -4px 3px;">';
    			$FORMS [$ii] .= '</a>';
    		} // end active
    	
    		$FORMS [$ii] .= '<br>';
    	
    		$i ++;
    	} // end foreach	
    }
    
    
    echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';
    echo '<tr><td colspan="4" valign="top" height="60">' . $FORMS [1] . '</td></tr>';
    unset ( $FORMS [1] );
    
    $i = 0;
    foreach ( $FORMS as $value ) {
    	
    	$i ++;
    	
    	if ($i == 1) {
    		echo '<tr>';
    	}
    	
    	echo '<td width="180" valign="top">';
    	echo $value;
    	echo '</td>';
    	
    	if ($i == sizeof ( $FORMS ) || ($i == 4)) {
    		echo '</tr>';
    		$i = 0;
    	}
    } // end foreach
    
    echo "</table>";
    
    ?>
    <br>
    	<br>
    	<table class="table table-striped table-bordered">
    		<tr>
    			<td align="right">
    				Choose the status level when these forms are applied:</td>
    				<td>
    					<select name="statuslevel">
    					<option value="0">Please Select</option>
    					<?php
    					//set where condition
    					$where = array("OrgID = :OrgID", "AssignStatus > 1");
    					//set parameters
    					$params = array(":OrgID"=>$OrgID);
    					//get prefilled forms
    					$results = $FormsInternalObj->getPrefilledFormsInfo("AssignStatus, CompleteByInterval, CompleteByFactor", $where, "SortOrder, TypeForm, Form LIMIT 1", array($params));
    					
    					list ( $AssignStatus, $CompleteByInterval, $CompleteByFactor ) = array_values ( $results['results'][0] );
    					
    					//set where condition
    					$where = array("OrgID = :OrgID", "ProcessOrder > 1", "Searchable = 'N'", "Active = 'Y'");
    					//set parameters
    					$params = array(":OrgID"=>$OrgID);
    					//get applicant process flow information
    					$results = $ApplicantsObj->getApplicantProcessFlowInfo("*", $where, 'ProcessOrder', array($params));
    					
    					if(is_array($results['results'])) {
    						foreach($results['results'] as $APF) {
    
    							echo '<option value="' . $APF ['ProcessOrder'] . '"';
    							if ($APF ['ProcessOrder'] == $AssignStatus) {
    								echo ' selected';
    							}
    							echo '>' . $APF ['Description'] . '</option>';
    						} // end foreach	
    					}
    					?>
    					</select>
    			</td>
    		</tr>
    		<tr>
    			<td align="right">Choose how long a person has to complete:</td>
    			<td>
    				<select name="CompleteByInterval">
    				<?php
    				for($i = 1; $i < 8; $i ++) {
    					
    					echo '<option value="' . $i . '"';
    					if ($i == $CompleteByInterval) {
    						echo ' selected';
    					}
    					echo '>' . $i . '</option>' . "\n";
    				} // end foreach
    				?>
    				</select> 
    				<select name="CompleteByFactor">
    				<?php
    				$CBI = array (
    						'day' => 'Days',
    						'week' => 'Weeks',
    						'month' => 'Months' 
    				);
    				
    				foreach ( $CBI as $key => $value ) {
    					echo '<option value="' . $key . '"';
    					if ($key == $CompleteByFactor) {
    						echo ' selected';
    					}
    					echo '>' . $value . '</option>' . "\n";
    				} // end foreach
    				?>
    				</select>
    			</td>
    		</tr>
    		<tr>
    			<td align="center" colspan="2">
    				<input type="hidden" name="process" value="Y"> 
    				<input type="hidden" name="typeform" value="prefilledforms"> 
    				<input type="submit" value="Update Forms" class="btn btn-primary">
    			</td>
    		</tr>
    	</table>
    </form>
    <br>
    <br>
    <?php
}?>
