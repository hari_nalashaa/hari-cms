<?php 
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';
require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

$TemplateObj->ApplicationID     =   $ApplicationID      =   $_REQUEST['ApplicationID'];
$TemplateObj->RequestID         =   $RequestID          =   $_REQUEST['RequestID'];
$TemplateObj->WebFormID         =   $WebFormID          =   $_REQUEST['WebFormID'];
$TemplateObj->displayFormHeader =   $displayFormHeader  =   displayHeader ( $ApplicationID, $RequestID, "No" );
$TemplateObj->title             =   $title              =  "Web Form View";

if($ServerInformationObj->getRequestSource() == 'ajax') {
    //view for AddApplicant.inc
    require_once COMMON_DIR . 'formsInternal/WebFormView.inc';
}
else {
    //view for AddApplicant.inc
    $TemplateObj->default_view_dir = COMMON_DIR;
    echo $TemplateObj->displayIrecruitTemplate('formsInternal/WebFormView');
}

?>