<?php 
require_once '../Configuration.inc';
$ApplicationID = $_REQUEST['ApplicationID'];

echo '<select name="InternalForms[]" id="InternalForms" multiple="multiple" style="max-width:300px;height:150px">';

$internal_forms_list = array();
if ($feature ['PreFilledForms'] == "Y") {
	if ($ApplicationID != "") {
			
		//set where condition
		$where = array("OrgID = :OrgID");
		//set parameters
		$params = array(":OrgID"=>$OrgID);
		//Get PrefilledForms Information
		$results = $FormsInternalObj->getPrefilledFormsInfo("*", $where, "SortOrder, TypeForm, Form", array($params));
			
		if(is_array($results['results'])) {
			foreach ($results['results'] as $PFF) {

				$Restrict = "";

				if ($PFF ['PreFilledFormID'] == "FE-I9m") {
					$Restrict = "Primary";
				}
				if ($PFF ['PreFilledFormID'] == "FE-I9m-2020") {
					$Restrict = "Primary";
				}
				
				$display_option = "true";
				if($_REQUEST['PresentedTo'] == "1"
					&& $Restrict == "Primary") {
					$display_option = "false";
				}
				
				if($display_option == "true") {
				    $internal_forms_list[$PFF ['SortOrder']] = '<option value="' . $PFF ['PreFilledFormID'] . '|PreFilled|' . $PFF ['TypeForm'] . ': ' . $PFF ['FormName'] . '|' . $Restrict . '"' . $sel . '>' . $PFF ['TypeForm'] . ': ' . $PFF ['FormName'] . '</option>';
				}
			} // end foreach
		}
			
	} // end no ApplicationID
} // end feature

if ($feature ['WebForms'] == "Y") {

	//set where condition
	$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
	//set parameters
	$params = array(":OrgID"=>$OrgID);
	//Get WebForms Information
	$results = $FormsInternalObj->getWebFormsInfo("*", $where, "FormName", array($params));

	if(is_array($results['results'])) {
		foreach($results['results'] as $WFQNT) {
			$Restrict = "";
			
			$display_option = "true";
			if($_REQUEST['PresentedTo'] == "1"
				&& $Restrict == "Primary") {
				$display_option = "false";
			}
			if($display_option == "true") {
				$internal_forms_list[$WFQNT ['SortOrder']] = '<option value="' . $WFQNT ['WebFormID'] . '|WebForm|' . $WFQNT ['FormName'] . '|' . $Restrict . '"' . $sel . '>' . $WFQNT ['FormName'] . " (Web Form)</option>\n";
			}
		} // end foreach
	}

} // end feature

if ($feature ['AgreementForms'] == "Y") {
	//set where condition
	$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
	//set parameters
	$params = array(":OrgID"=>$OrgID);
	// Agreement Forms
	$results = $FormsInternalObj->getAgreementFormsInfo("*", $where, "FormName", array($params));

	if(is_array($results['results'])) {
		foreach($results['results'] as $AFN ) {
			$Restrict = $AFN ['FormPart'];
			
			$display_option = "true";
			if($_REQUEST['PresentedTo'] == "1") {
				if($Restrict == "Primary") {
					$display_option = "false";
				}
			}
			
			if($display_option == "true") {
				$internal_forms_list[$AFN ['SortOrder']] = '<option value="' . $AFN ['AgreementFormID'] . '|Agreement|' . $AFN ['FormName'] . '|' . $Restrict . '"' . $sel . '>' . $AFN ['FormName'] . " (Agreement Form)</option>\n";
			}
		} // end foreach
	}

} // end feature

if ($feature ['SpecificationForms'] == "Y") {

	//set where condition
	$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
	//set parameters
	$params = array(":OrgID"=>$OrgID);
	//get specification forms
	$results = $FormsInternalObj->getSpecificationForms("SpecificationFormID, DisplayTitle, FormType, SortOrder", $where, "FormType, DisplayTitle", array($params));

	if(is_array($results['results']))
	{
		foreach($results['results'] as $WFQNT) {
			$Restrict = "";
			
			$display_option = "true";
			if($_REQUEST['PresentedTo'] == "1"
					&& $Restrict == "Primary") {
				$display_option = "false";
			}
			
			if($display_option == "true") {
				$internal_forms_list[$WFQNT ['SortOrder']] = '<option value="' . $WFQNT ['SpecificationFormID'] . '|' . $WFQNT ['FormType'] . '|' . $WFQNT ['DisplayTitle'] . '|' . $Restrict . '"' . $sel . '>' . $WFQNT ['FormType'] . ": " . $WFQNT ['DisplayTitle'] . "</option>\n";
			}
		} // end foreach

	}

} // end feature

ksort($internal_forms_list);
echo implode("", $internal_forms_list);
echo '</select>';
?>
