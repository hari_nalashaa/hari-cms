<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= $title = 'Assign Internal Form';

//Include configuration related javascript in header
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$scripts_header[] = "js/loadAJAX.js";

//Page Scripts Header
$TemplateObj->page_scripts_header = $scripts_header;

//Template Variables
$TemplateObj->ApplicationID			= $ApplicationID 		= isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID				= $RequestID 			= isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->CompleteByInterval 	= $CompleteByInterval 	= isset($_REQUEST['CompleteByInterval']) ? $_REQUEST['CompleteByInterval'] : '';
$TemplateObj->CompleteByFactor 		= $CompleteByFactor 	= isset($_REQUEST['CompleteByFactor']) ? $_REQUEST['CompleteByFactor'] : '';
$TemplateObj->PresentedTo 			= $PresentedTo 			= isset($_REQUEST['PresentedTo']) ? $_REQUEST['PresentedTo'] : '';
$TemplateObj->AssignStatus 			= $AssignStatus 		= isset($_REQUEST['AssignStatus']) ? $_REQUEST['AssignStatus'] : '';
$TemplateObj->action				= $action 				= isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->Active				= $Active 				= isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->process				= $process 				= isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->k						= $k 					= isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/formsInternal/AssignInternal');
?>