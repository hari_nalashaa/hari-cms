<?php
//set parameters
$params = array(":OrgID"=>$OrgID);
//set where condition
$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
//get agreement forms information
$results = $FormsInternalObj->getAgreementFormsInfo("*", $where, "FormPart, FormName", array($params));

$cnt = $results['count'];

if(is_array($results['results'])) {
	foreach($results['results'] as $AFN) {
	
		if ($AFN ['SortOrder'] % 2 == 0) {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	
		$form_data = "";
	
		$form_data .= '<tr bgcolor="' . $rowcolor . '" id="'.$OrgID."*".$AFN ['AgreementFormID']."*"."AgreementForm".'">';
		$form_data .= '<td>';
		$form_data .= 'Agreement Form';
		$form_data .= '</td>';
	
		$form_data .= '<td>';
		$form_data .= $AFN ['FormPart'];
		$form_data .= '</td>';
		$form_data .= '<td>';
		$form_data .= $AFN ['FormName'];
		$form_data .= '</td>';
	
		//For testing i keep this one as hidden variable
		/*
		$form_data .= '<td>';
		$form_data .= '<input type="hidden" ';
		$form_data .= 'size="2" maxlength="2" value="'.$AFN ['SortOrder'].'"';
		$form_data .= ' onchange=\'updateInterAssignedFormSortOrder("'.$OrgID.'", "'.$AFN ['AgreementFormID'].'", "AgreementForm", this.value)\'>';
		$form_data .= '</td>';
		*/
	
		if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
			$form_data .= '<td align="center">';
			$form_data .= '<a href="' . $formscript . '?typeform=agreementforms&action=copy&AgreementFormID=' . $AFN ['AgreementFormID'] . '" onclick="return confirm(\'Are you sure you want to copy the following form?\n\n' . $AFN ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/application_cascade.png" border="0" title="Copy"></a>';
			$form_data .= '</td>';
	
		    $form_data .= '<td align="center">';
		    $form_data .= '<a href="' . $formscript . '?typeform=agreementforms&action=edit&AgreementFormID=' . $AFN ['AgreementFormID'] . '">';
		    $form_data .= '<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit">';
		    $form_data .= '</a>';
		    $form_data .= '</td>';
	
			$form_data .= '<td align="center">';
			$form_data .= '<a href="' . $formscript . '?typeform=agreementforms&action=delete&AgreementFormID=' . $AFN ['AgreementFormID'] . '" onclick="return confirm(\'Are you sure you want to delete the following form?\n\n' . $AFN ['FormName'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
			$form_data .= '</td>';
		} // end permit Internal_Forms_Edit
	
		$form_data .= '<td align="center">';
		$link = IRECRUIT_HOME . "formsInternal/completeAgreementForm.php?AgreementFormID=" . $AFN ['AgreementFormID'];
		if ($typeform) {
			$link .= "&typeform=" . $typeform;
		}
		if ($AccessCode != "") {
			$link .= "&k=" . $AccessCode;
		}
		$form_data .= '<a href="#" onclick="javascript:window.open(\'' . $link . '\',\'_blank\',\'location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes\');"><img src="' . IRECRUIT_HOME . 'images/icons/application_form.png" border="0" title="View Form" style="margin:0px 3px -4px 0px;"></a>';
		$form_data .= '</td>';
	
		$form_data .= '</tr>';
	
		$sort_forms[$AFN ['SortOrder']] = $form_data;
	} // end foreach
}
?>