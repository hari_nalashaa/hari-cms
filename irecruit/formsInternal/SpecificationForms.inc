<?php
if ($feature ['SpecificationForms'] != "Y" || $permit ['Internal_Forms_Edit'] < 1) {

	include IRECRUIT_DIR . 'irecruit_permissions.err';
}
else {
    $mainfunction   =   "Add";
    $extfunction    =   "Add";
    $attfunction    =   "Add";
    $htmlfunction   =   "Add";
    
    $maintype       =   "none";
    $exttype        =   "none";
    $atttype        =   "none";
    $htmltype       =   "none";
    
    $change         =   "N";
    
    if ($_GET ['edit'] != "") {
        
        // set where condition
        $where     =   array ("OrgID = :OrgID", "SpecificationFormID = :SpecificationFormID");
        // set parameters
        $params    =   array (":OrgID" => $OrgID, ":SpecificationFormID" => $_GET ['edit']);
        // get specification forms information
        $results   =   G::Obj('FormsInternal')->getSpecificationForms ( "*", $where, "", array ($params) );
        
        if (is_array ( $results ['results'] )) {
            foreach ( $results ['results'] as $PTL ) {
                
                if ($PTL ['FormType'] == "External Link") {
                    $exttype = "block";
                    $extfunction = "Change";
                    $extdisplaytitle = $PTL ['DisplayTitle'];
                    $exturl = $PTL ['URL'];
                }
                if ($PTL ['FormType'] == "Attachment") {
                    $atttype = "block";
                    $attfunction = "Change";
                    $attdisplaytitle = $PTL ['DisplayTitle'];
                }
                if ($PTL ['FormType'] == "HTML Page") {
                    $htmltype = "block";
                    $htmlfunction = "Change";
                    $htmldisplaytitle = $PTL ['DisplayTitle'];
                    $htmltext = $PTL ['HTML'];
                }
            } // end foreach
        }
    } // end edit
    
    if ($_GET ['delete'] != "") {
        
        // set information
        $set_info = array (
            "FormStatus = 'Deleted'"
        );
        // set where condition
        $where     =   array (
            "OrgID                   =   :OrgID",
            "SpecificationFormID     =   :SpecificationFormID"
        );
        // set parameters
        $params    =   array (
            ":OrgID"                 =>  $OrgID,
            ":SpecificationFormID"   =>  $_GET ['delete']
        );
        // Update Specification Forms Information
        G::Obj('Forms')->updFormsInfo ( 'SpecificationForms', $set_info, $where, array ($params) );
    }
    
    if ($_POST ['submit'] == "Add External Link") {
        
        if (($_POST ['exttitle']) && ($_POST ['url'])) {
            // insert specification forms information
            $specification_form_info  =   array(
                "OrgID"                 =>  $OrgID,
                "SpecificationFormID"   =>  uniqid (),
                "DisplayTitle"          =>  $_POST ['exttitle'],
                "FormType"              =>  'External Link',
                "URL"                   =>  $_POST ['url']
            );
            G::Obj('Forms')->insFormsInfo ( 'SpecificationForms', $specification_form_info );
        } // end exttitle && url
    } // end Add External Link
    
    if ($_POST ['submit'] == "Add Attachment") {
        
        if (($_POST ['atttitle']) && ($_FILES ['attfile'] ['type'] != "")) {
            
            $fileType = $_FILES ['attfile'] ['type'];
            $data =   file_get_contents ( $_FILES ['attfile'] ['tmp_name'] );
            
            $e    =   explode ( '.', $_FILES ['attfile'] ['name'] );
            $ecnt =   count ( $e ) - 1;
            $ext  =   $e [$ecnt];
            // insert specification forms information
            $specification_form_info  =   array(
                "OrgID"                 =>  $OrgID,
                "SpecificationFormID"   =>  uniqid (),
                "DisplayTitle"          =>  $_POST ['atttitle'],
                "FormType"              =>  'Attachment',
                "AttachmentType"        =>  $fileType,
                "AttachmentExt"         =>  $ext,
                "Attachment"            =>  $data
            );
            G::Obj('Forms')->insFormsInfo ( 'SpecificationForms', $specification_form_info );
        } // end file att type
    } // end Add Attachment
    
    if ($_POST ['submit'] == "Add HTML") {
        
        if (($_POST ['htmltitle']) && ($_POST ['html'])) {
            // insert specification forms information
            $specification_form_info = array (
                "OrgID" => $OrgID,
                "SpecificationFormID" => uniqid (),
                "DisplayTitle" => $_POST ['htmltitle'],
                "FormType" => 'HTML Page',
                "HTML" => $_POST ['html']
            );
            G::Obj('Forms')->insFormsInfo ( 'SpecificationForms', $specification_form_info );
        } // end htmltitle and html
    } // end Add HTML
    
    if ($_POST ['submit'] == "Change Section Title") {
        
        if (($_POST ['subtitle']) && ($_POST ['updateid'])) {
            
            // set information
            $set_info = array (
                "DisplayTitle = :DisplayTitle",
                "HTML = :HTML"
            );
            // set where condition
            $where = array (
                "OrgID = :OrgID",
                "SpecificationFormID = :SpecificationFormID"
            );
            // set parameters
            $params = array (
                ":DisplayTitle" => $_POST ['subtitle'],
                ":OrgID" => $OrgID,
                ":SpecificationFormID" => $_POST ['updateid']
            );
            // Update Specification Forms Information
            G::Obj('Forms')->updFormsInfo ( 'SpecificationForms', $set_info, $where, array (
                $params
            ) );
        } // end POST
    } // end change Section Title
    
    if ($_POST ['submit'] == "Change External Link") {
        
        if (($_POST ['exttitle']) && ($_POST ['url']) && ($_POST ['updateid'])) {
            
            // set information
            $set_info = array (
                "DisplayTitle = :DisplayTitle",
                "URL = :URL"
            );
            // set where condition
            $where = array (
                "OrgID = :OrgID",
                "SpecificationFormID = :SpecificationFormID"
            );
            // set parameters
            $params = array (
                ":DisplayTitle" => $_POST ['exttitle'],
                ":URL" => $_POST ['url'],
                ":OrgID" => $OrgID,
                ":SpecificationFormID" => $_POST ['updateid']
            );
            // Update Specification Forms Information
            G::Obj('Forms')->updFormsInfo ( 'SpecificationForms', $set_info, $where, array (
                $params
            ) );
        } // end exttitle && url
    } // end change External Link
    
    if ($_POST ['submit'] == "Change Attachment") {
        
        if (($_POST ['atttitle']) && ($_FILES ['attfile'] ['type']) && ($_POST ['updateid'])) {
            
            $fileType = $_FILES ['attfile'] ['type'];
            $data = file_get_contents ( $_FILES ['attfile'] ['tmp_name'] );
            
            $e = explode ( '.', $_FILES ['attfile'] ['name'] );
            $ecnt = count ( $e ) - 1;
            $ext = $e [$ecnt];
            
            // set information
            $set_info = array (
                "DisplayTitle = :DisplayTitle",
                "AttachmentType = :AttachmentType",
                "AttachmentExt = :AttachmentExt",
                "Attachment = :Attachment"
            );
            // set where condition
            $where = array (
                "OrgID = :OrgID",
                "SpecificationFormID = :SpecificationFormID"
            );
            // set parameters
            $params = array (
                ":DisplayTitle" => $_POST ['atttitle'],
                ":AttachmentType" => $fileType,
                ":AttachmentExt" => $ext,
                ":Attachment" => $data,
                ":OrgID" => $OrgID,
                ":SpecificationFormID" => $_POST ['updateid']
            );
            // Update Specification Forms Information
            G::Obj('Forms')->updFormsInfo ( 'SpecificationForms', $set_info, $where, array (
                $params
            ) );
        } // end arrtitle && attfile
    } // end change Attachment
    
    if ($_POST ['submit'] == "Change HTML") {
        
        if (($_POST ['htmltitle']) && ($_POST ['html']) && ($_POST ['updateid'])) {
            
            // set information
            $set_info = array (
                "DisplayTitle = :DisplayTitle",
                "HTML = :HTML"
            );
            // set where condition
            $where = array (
                "OrgID = :OrgID",
                "SpecificationFormID = :SpecificationFormID"
            );
            // set parameters
            $params = array (
                ":DisplayTitle" => $_POST ['htmltitle'],
                ":HTML" => $_POST ['html'],
                ":OrgID" => $OrgID,
                ":SpecificationFormID" => $_POST ['updateid']
            );
            // Update Specification Forms Information
            G::Obj('Forms')->updFormsInfo ( 'SpecificationForms', $set_info, $where, array (
                $params
            ) );
        } // end POST
    } // end change HTML
    
    // end submits
    if (substr ( $USERROLE, 0, 21 ) == 'master_admin') {
        
        echo '<form method="post" action="' . $_SERVER ['PHP_SELF'] . '" enctype="multipart/form-data">' . "\n";
        
        echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">' . "\n";
        
        echo '<tr><td width="770"><a href="javascript:ReverseDisplay(\'addlink\')">';
        echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
        echo $extfunction . ' External Link</b></a></td></tr>';
        echo '<tr><td><div id="addlink" style="display:' . $exttype . ';">';
        
        echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
        
        echo '<tr>';
        echo '<td width="15"></td>';
        echo '<td>Display Title: </td>';
        echo '<td><input type="text" class="form-control width-auto-inline" name="exttitle" size="30" maxlength="30" value="' . $extdisplaytitle . '"></td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td></td>';
        echo '<td>URL: </td>';
        echo '<td><input type="text" class="form-control width-auto-inline" name="url" size="50" maxlength="100" value="' . $exturl . '"> <i>(be sure to include http:// or https://)</i></td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td></td>';
        echo '<td colspan="2"><input type="submit" class="btn btn-primary" name="submit" value="' . $extfunction . ' External Link"></td>';
        echo '</tr>';
        
        echo '</table>';
        
        if ($extfunction == "Change") {
            echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
        }
        
        echo '</div></td></tr>';
        
        echo '<tr><td><a href="javascript:ReverseDisplay(\'addattachment\')">';
        echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
        echo $attfunction . ' Attachment</b></a></td></tr>';
        echo '<tr><td><div id="addattachment" style="display:' . $atttype . ';">';
        
        echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
        
        echo '<tr>';
        echo '<td width="15"></td>';
        echo '<td>Display Title:</td>';
        echo '<td><input type="text" class="form-control width-auto-inline" name="atttitle" size="30" maxlength="30" value="' . $attdisplaytitle . '">' . "</td>";
        echo '</tr>';
        
        echo '<tr>';
        echo '<td></td>';
        echo '<td>File:</td>';
        echo '<td><input type="file" class="width-auto-inline" name="attfile" size="20"></td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td></td>';
        echo '<td colspan="2"><input type="submit" class="btn btn-primary" name="submit" value="' . $attfunction . ' Attachment"></td>';
        echo '</tr>';
        
        echo '</table>';
        
        
        if ($attfunction == "Change") {
            echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
        }
        
        echo '</div></td></tr>';
        
        echo '<tr><td><a href="javascript:ReverseDisplay(\'addhtml\')">';
        echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
        echo $htmlfunction . ' HTML Page</b></a></td></tr>';
        echo '<tr><td><div id="addhtml" style="display:' . $htmltype . ';">';
        
        echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
        
        echo '<tr>';
        echo '<td width="15"></td>';
        echo '<td>Display Title: </td>';
        echo '<td><input type="text" class="form-control width-auto-inline" name="htmltitle" size="30" maxlength="30" value="' . $htmldisplaytitle . '"></td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td></td>';
        echo '<td valign="top">Text:</td>';
        echo '<td><textarea cols="80" rows="15" name="html" class="mceEditor">' . $htmltext . '</textarea></td>';
        echo '</tr>';
        
        echo '<tr>';
        echo '<td></td>';
        echo '<td colspan="2"><input type="submit" class="btn btn-primary" name="submit" value="' . $htmlfunction . ' HTML"></td>';
        echo '</tr>';
        
        echo '</table>';
        if ($htmlfunction == "Change") {
            echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
        }
        
        echo '</div></td></tr>';
        
        echo '</table>' . "\n";
        echo '<br>' . "\n";
        
        echo '<input type="hidden" name="MAX_FILE_SIZE" value="10485760">';
        echo '<input type="hidden" name="typeform" value="specforms">';
        echo '</form>' . "<br>";
    }

    include IRECRUIT_DIR . 'formsInternal/DisplaySpecificationFormsList.inc';
}
?>