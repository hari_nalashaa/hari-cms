<?php
$formscript = "formsInternal.php";
$formtable = "WebFormQuestions";

if ($feature ['WebForms'] != "Y") {
	include IRECRUIT_DIR . 'irecruit_permissions.err';
} // end feature
else {
    $print = "Y";
    
    if ($action) {
        
        if ($action == "new") {
            
            //set where condition
            $where = array("FormID = :FormID");
            //set parameters
            $params = array(":FormID"=>$_POST ['FormID']);
            //get webformtemplates information
            $results = $FormsInternalObj->getFormTemplatesInfo('WebFormTemplates', "*", $where, "FormID LIMIT 0, 1", array($params));
            
            $WFT = $results['results'][0];
            
            if ($_POST ['formtitle'] == "") {
                $msg = "The following information is missing.\\n\\n";
                $msg .= "Title is missing for the " . $WFT ['TemplateDescription'] . ".\\n\\n";
                $displaybox = "Y";
            }
            
            if (! $msg) {
                
                $WebFormID   =   uniqid ();
                //set where condition
                $where_info  =   array("OrgID = 'MASTER'", "FormID = :FormID");
                //set parameters
                $params      =   array(":FormID"=>$_POST ['FormID']);
                
                if($_POST ['FormID'] != 'other') {
                    //set columns
                    $columns = "'$OrgID', FormID, '$WebFormID', QuestionID, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
                    //Insert WebFormQuestions Information
                    G::Obj('FormQuestions')->insFormQuestionsMaster('WebFormQuestions', $columns, $where_info, array($params));
                }
                
                //set web forms information
                $web_forms_info = array("OrgID"=>$OrgID, "WebFormID"=>$WebFormID, "FormName"=>$_POST ['formtitle']);
                //Insert WebForms Information
                G::Obj('Forms')->insFormsInfo('WebForms', $web_forms_info);
                
                $msg = "You have created a new web form with template " . $WFT ['TemplateDescription'] . ".\\n\\n";
            }
            
            if ($msg) {
                echo '<script language="JavaScript" type="text/javascript">';
                echo "alert('" . $msg . "')";
                echo '</script>';
            }
        } else if ($action == "edit") {
            
            $print = "N";
        } else if ($action == "copy") {
            
            $newWebFormID =   uniqid ();
            //set where condition
            $where_info   =   array("OrgID = :OrgID", "WebFormID = :WebFormID");
            //set parameters
            $params       =   array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID']);
            
            $results_copy =   G::Obj('FormQuestions')->getQuestionsInformation("WebFormQuestions", "Question", $where_info, "", array($params));
            
            //set columns
            $columns    =   "'$OrgID', '', '$newWebFormID', QuestionID, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate";
            //Insert WebFormQuestions Information
            G::Obj('FormQuestions')->insFormQuestionsMaster('WebFormQuestions', $columns, $where_info, array($params));
            
            //Get WebForms Information
            $results    =   G::Obj('FormsInternal')->getWebFormsInfo("FormName", $where_info, "OrgID LIMIT 0,1", array($params));
            $WFN        =   $results['results'][0];
            
            //Get FormName
            $newformtitle = $WFN ['FormName'] . " Copy";
            
            //insert web forms information
            $web_forms_info = array("OrgID"=>$OrgID, "WebFormID"=>$newWebFormID, "FormName"=>$newformtitle);
            G::Obj('Forms')->insFormsInfo('WebForms', $web_forms_info);
            
            //set tables
            $table_name = "WebFormTemplates WFT, WebFormQuestions WFQ";
            //set where condition
            $where    =   array("WFQ.FormID = WFT.FormID", "WFQ.OrgID = :OrgID", "WFQ.WebFormID = :WebFormID");
            //set parameters
            $params   =   array(":OrgID"=>$OrgID, ":WebFormID"=>$_GET ['WebFormID']);
            //Get Template Description
            $results  =   G::Obj('FormQuestions')->getFormQuestionsAndTemplatesInfo($table_name, "WFT.TemplateDescription", $where_info, "", "WFQ.OrgID LIMIT 0, 1", array($params));
            $WFT      =   $results['results'][0];
            
            $msg    =   "You have created a new web form from " . $WFN ['FormName'] . ".\\n\\n";
            
            echo '<script language="JavaScript" type="text/javascript">';
            echo "alert('" . $msg . "')";
            echo '</script>';
            
            $print = "Y";
        } else if ($action == "delete") {
            //set where condition
            $where_info =   array("OrgID = :OrgID", "WebFormID = :WebFormID");
            //set parameters
            $params     =   array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID']);
            
            //Update WebForms Information
            $set_info   =   array("FormStatus = 'Deleted'");
            $results    =   G::Obj('Forms')->updFormsInfo('WebForms', $set_info, $where_info, array($params));
            
            //Get WebForms Information
            $results 	=   G::Obj('FormsInternal')->getWebFormsInfo("FormName", $where_info, "OrgID LIMIT 0,1", array($params));
            $WFN        =   $results['results'][0];
            
            $msg = "You have deleted " . $WFN ['FormName'] . ".\\n\\n";
            
            echo '<script language="JavaScript" type="text/javascript">';
            echo "alert('" . $msg . "')";
            echo '</script>';
            
            $print = "Y";
        }
        
    } // end action
    
    if ($print == "Y") {
        
        $dis = "none";
        if ($displaybox == "Y") {
            $dis = "table";
        } // end msg

        echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered" style="">';
        
        if (substr ( $USERROLE, 0, 21 ) == 'master_admin') {
            echo '<tr>';
            echo '<td width="100%">';
            echo '<a href="javascript:ReverseDisplay(\'AddNew\')">';
            echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">';
            echo 'Add Web Form';
            echo '</b></a></td></tr>';
        }
        
       
        
        echo '<tr><td><div id="AddNew" style="display:' . $dis . ';">';
        
        echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';
        echo '<form method="post" action="' . $formscript . '">';
        echo '<tr><td>';
        
        echo '&nbsp;&nbsp;';
        echo 'Title: <input type="text" class="form-control width-auto-inline" name="formtitle" size="30" maxlength="45" value="">';
        echo '&nbsp;&nbsp;';
        echo 'Select a template ';
        
        $params_web_form_id    =   array(":OrgID"=>$OrgID);
        $sel_web_form_id       =   "SELECT WebFormID FROM WebForms WHERE WebFormID IN (
                                    SELECT DISTINCT(WebFormID) FROM WebFormQuestions WHERE OrgID = :OrgID AND FormID = 'emergencycontact') and FormStatus = 'Active' LIMIT 1";
        $res_web_form_id       =   G::Obj('GenericQueries')->getRowInfoByQuery($sel_web_form_id, array($params_web_form_id));
        
        //set tables
        $table_name =   "WebFormTemplates WFT, WebFormQuestions WFQ";
        //set where condition
        $where      =   array("WFQ.OrgID = 'MASTER'", "WFT.FormID = WFQ.FormID", "WFT.FormID != 'fixed'");
        //Get Template Description
        $results    =   G::Obj('FormQuestions')->getFormQuestionsAndTemplatesInfo($table_name, "WFT.FormID, WFT.TemplateDescription", $where, "WFT.FormID", "WFT.SortOrder", array());
        
        echo '<select name="FormID" class="form-control width-auto-inline">';
        
        if(is_array($results['results'])) {
            foreach($results['results'] as $IF) {
                if($IF ['FormID'] == "emergencycontact") {
                    if($res_web_form_id['WebFormID'] == "") {
                        echo '<option value="' . $IF ['FormID'] . '"';
                        if ($IF ['FormID'] == $FormID) {
                            echo ' selected';
                        }
                        echo '>' . $IF ['TemplateDescription'];
                    }
                }
                else {
                    echo '<option value="' . $IF ['FormID'] . '"';
                    if ($IF ['FormID'] == $FormID) {
                        echo ' selected';
                    }
                    echo '>' . $IF ['TemplateDescription'];
                }
            } // end foreach
        }
        
        echo '</select>';
        
        echo '&nbsp;&nbsp;&nbsp;';
        echo '<input type="hidden" name="action" value="new">';
        echo '<input type="hidden" name="typeform" value="webforms">';
        echo '<input type="submit" value="Add New Form" class="btn btn-primary">';
        
        echo '</td></tr>' . "\n";
        echo '</form>';
        echo '</table>';
        
        echo '</div></td></tr>';
        
        echo '</table>';
        
        if (0) {
            echo '<tr><td><a href="javascript:ReverseDisplay(\'' . $IF ['FormID'] . '\')">';
            echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">';
            echo $IF ['TemplateDescription'];
            echo '</b></a></td></tr>';
            
            $dis = "none";
            if ($displaybox == "Y") {
                if ($_POST ['FormID'] == $IF ['FormID']) {
                    $dis = "block";
                }
            } // end msg
            
            echo '<tr><td><div id="' . $IF ['FormID'] . '" style="display:' . $dis . ';">';
            
            echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';
            echo '<form method="post" action="' . $formscript . '">';
            
            echo '<tr><td width="15"></td><td>Title: <input type="text" name="formtitle" size="30" maxlength="45" value="">' . "</td></tr>";
            echo '<tr><td></td><td><input type="submit" value="Add ' . $IF ['TemplateDescription'] . '"></td></tr>';
            
            echo '<input type="hidden" name="action" value="new">';
            echo '<input type="hidden" name="typeform" value="webforms">';
            echo '<input type="hidden" name="FormID" value="' . $IF ['FormID'] . '">';
            echo '</form>';
            echo '</table>';
            
            echo '</div></td></tr>';
        } // end if 0
        
        echo '<br>';
        include IRECRUIT_DIR . 'formsInternal/DisplayWebFormsList.inc';
    } else { // else print
        
        include IRECRUIT_DIR . 'formsInternal/EditWebForms.inc';
    } // end if print
}
?>
