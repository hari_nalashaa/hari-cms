<?php
require_once '../Configuration.inc';

echo '<select name="PresentedTo">';

if ($feature ['UserPortal'] == "Y") {
	
	if ($_REQUEST['Restrict'] != "Primary") {
		if ($_REQUEST['PresentedTo'] == "1") {
			$sel = " selected";
		} else {
			$sel = "";
		}
		echo '<option value="1"' . $sel . '>Applicant</option>';
	}
} // end feature UserPortal

if ($_REQUEST['PresentedTo'] == "2") {
	$sel = " selected";
} else {
	$sel = "";
}
echo '<option value="2"' . $sel . '>Internal Staff</option>';

echo '</select>';