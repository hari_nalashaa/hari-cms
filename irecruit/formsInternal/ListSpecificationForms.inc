<?php
//set where condition
$where = array("OrgID = :OrgID", "FormStatus = 'Active'");
//set parameters
$params = array(":OrgID"=>$OrgID);
//Get specification forms
$results = $FormsInternalObj->getSpecificationForms("*", $where, "FormType, DisplayTitle", array($params));
//get ptlcnt
$ptlcnt = $results['count'];

if(is_array($results['results'])) {
	foreach($results['results'] as $PTLINFO) {
	
		if ($PTLINFO ['SortOrder'] % 2 == 0) {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	
		$form_data = "";
	
		$form_data .= '<tr bgcolor="' . $rowcolor . '" id="'.$OrgID."*".$PTLINFO ['SpecificationFormID']."*"."SpecificationForm".'">';
	
		$form_data .= '<td>';
		$form_data .= 'Specification Forms';
		$form_data .= '</td>';
	
		$form_data .= '<td>' . $PTLINFO ['FormType'] . '</td>';
	
		$form_data .= '<td>';
		$form_data .= $PTLINFO ['DisplayTitle'];
		$form_data .= '</td>';
	
		//For testing i keep this one as hidden variable
		/*
		$form_data .= '<td>';
		$form_data .= '<input type="hidden" ';
		$form_data .= 'size="2" maxlength="2" value="'.$PTLINFO ['SortOrder'].'"';
		$form_data .= ' onchange=\'updateInterAssignedFormSortOrder("'.$OrgID.'", "'.$PTLINFO ['SpecificationFormID'].'", "SpecificationForm", this.value)\'>';
		$form_data .= '</td>';
		*/

		if ($permit ['Internal_Forms_Edit'] > 0 && substr ( $USERROLE, 0, 21 ) == 'master_admin') {
			$form_data .= '<td align="center">&nbsp;</td>';
			$form_data .= '<td align="center"><a href="formsInternal.php?typeform=specforms&edit=' . $PTLINFO ['SpecificationFormID'] . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a></td>';
			$form_data .= '<td align="center"><a href="formsInternal.php?typeform=specforms&delete=' . $PTLINFO ['SpecificationFormID'] . '" onclick="return confirm(\'Are you sure you want to delete this item?\n\nDisplay Title: ' . $PTLINFO ['DisplayTitle'] . '\nType of Display: ' . $PTLINFO ['FormType'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a></td>';
		} // end permit Internal_Forms_Edit
	
		$form_data .= '<td align="center">';
		$form_data .= '<a href="';
	
		if ($PTLINFO ['FormType'] == "External Link") {
			$form_data .= $PTLINFO ['URL'];
		}
		if ($PTLINFO ['FormType'] == "Attachment") {
			$form_data .= IRECRUIT_HOME . 'formsInternal/display_specificationform.php?OrgID=' . $OrgID . '&specformid=' . $PTLINFO ['SpecificationFormID'];
		}
		if ($PTLINFO ['FormType'] == "HTML Page") {
	
			$link = IRECRUIT_HOME . "formsInternal/viewInternalForm.php?typeform=specforms&specformid=" . $PTLINFO ['SpecificationFormID'];
			if ($AccessCode != "") {
				$link .= "&k=" . $AccessCode;
			}
	
			$openwindow = " onclick=\"javascript:window.open('" . $link . "','_blank','location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes');";
	
			$form_data .= '#"' . $openwindow;
		}
	
		$form_data .= '"';
	
		if ($PTLINFO ['FormType'] == "External Link") {
			$form_data .= ' target="_blank"';
		}
		$form_data .= '>';
	
		if ($PTLINFO ['FormType'] == "External Link") {
			$form_data .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_picture.png" border="0" title="View External Link">';
		} else {
			$form_data .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View">';
		}
		$form_data .= '</a>';
		$form_data .= '</td>';
	
		$form_data .= '</tr>' . "\n";
	
	
		$sort_forms[$PTLINFO ['SortOrder']] = $form_data;
	
	} // end foreach
}
?>