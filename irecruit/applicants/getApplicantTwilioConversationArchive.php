<?php
require_once '../Configuration.inc';

//Get twilio number
$user_details			=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);

//Add plus one before number
if(substr($user_details['Cell'], 0, 2) != "+1") {
	$user_cell_number	=	"+1".$user_cell_number;
}

//Get applicant cellphone
$cell_phone_info		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'cellphone');
$cell_phone				=	json_decode($cell_phone_info, true);

$applicant_number       =   "";
if($cell_phone[0].$cell_phone[1].$cell_phone[2] != "") {
    $applicant_number   =	"+1".$cell_phone[0].$cell_phone[1].$cell_phone[2];
}

//Get user conversation resources
$usr_conv_resources		=	G::Obj('TwilioConversationsArchive')->getUserConversationsArchiveResources($OrgID, $USERID, $applicant_number, $user_cell_number);

if($applicant_number != "") {
	?>
	<br><br>	
	<?php
	foreach ($usr_conv_resources as $conversation) {

	$resource_id                    =       $conversation['ResourceID'];

	if($resource_id != "") {
	    $messages      =   G::Obj('TwilioConversationMessagesArchive')->getTwilioConversationMessagesArchiveByResourceID($resource_id);
	    ?>
		<table width="100%" border="0" cellpadding="3" cellspacing="1" class="table table-bordered">
		<tr>
			<td colspan="3" align="left">
				<h4 style="margin:0px">Messages List:</h4>
			</td>
		</tr>
		<tr>
			<td width="20%" align="left">
				<strong>Author</strong>
			</td>
			<td width="15%" align="left">
				<strong>Date</strong>
			</td>
			<td align="left">
				<strong>Message</strong>
			</td>
		</tr>
		<?php
		$messages_count	= count($messages) - 1;

		if($messages_count > 0) {
		    
		    for($j = $messages_count; $j >= 0; $j--) {
		        
		        $PortalUserID      		=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'PortalUserID');
		        $first					=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'first');
		        $last      				=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'last');
		        $email					=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'email');
		        
		        $userportal_userid_info	=	G::Obj('UserPortalUsers')->getUserDetailInfoByEmail("UserID", $email);
		        
		        if($PortalUserID == "") {
		            $PortalUserID       =	$userportal_userid_info['UserID'];
		        }
		        
		        $userportal_user_info	=	G::Obj('UserPortalUsers')->getUserDetailInfoByUserID("FirstName, LastName, ProfileAvatarPicture", $PortalUserID);
		        
		        echo "<tr>";
		        
		        echo "<td align=\"left\">";
    		        
		        if($messages[$j]['Author'] == $applicant_number) {
    		            $abs_path			=	USERPORTAL_DIR . "vault/".$PortalUserID."/profile_avatars/";
    		            if (file_exists ( $abs_path . $userportal_user_info['ProfileAvatarPicture'] )) {
    		                ?>
    					<img src="<?php echo USERPORTAL_HOME . "vault/".$PortalUserID."/profile_avatars/".$userportal_user_info['ProfileAvatarPicture'];?>" width="29" height="29"><br>
    					<?php
    				} else {
    					?>
    					<img src="<?php echo USERPORTAL_HOME . "images/no-intern.jpg";?>" width="50" height="50"><br>
    					<?php
    				}
    				
    				if($PortalUserID != "") {
    					if($userportal_user_info['LastName'] != "") {
    						echo $userportal_user_info['LastName'].", ";
    					}
    					echo $userportal_user_info['FirstName'];
    				}
    				else {
    					if($last != "") {
    						echo $last.", ";
    					}
    					echo $first;
    				}
    			}
    			else if($messages[$j]['Author'] != $user_cell_number) {
    				if($user_preferences['DashboardAvatar'] != "") {
    				    ?><img src="<?php echo IRECRUIT_HOME . "vault/".$OrgID."/".$USERID."/profile_avatars/".$user_preferences['DashboardAvatar'];?>" width="50" height="50"><br><?php
    				}
    				else {
    				    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50"><br><?php
    				}
    				
    				echo $user_info['FirstName']." ".$user_info['LastName'];
    			}
    			else {
    				?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50"><br><?php						
    				echo "Applicant ";
    			}
    
    			echo "</td>";
    			echo "<td align=\"left\">";
    
    			//Convert object to array, otherwise it is not working
    			$twilio_date_info	=	new DateTime($messages[$j]['DateCreatedDate'], new DateTimeZone('UTC'));
    			$twilio_date_info->setTimezone(new DateTimeZone('America/New_York'));
    			echo $twilio_date_info->format('m/d/Y H:i') . ' EST';
    
    			echo "</td>";
    			echo "<td align=\"left\">";
    			echo $messages[$j]['Body'];
    
    			$ResourceID  =   $messages[$j]['ConversationResourceID'];
    			$MessageSID  =   $messages[$j]['ConversationMessageID'];
    			
    			$media_info  =   G::Obj('TwilioMediaInfo')->getMediaInfoByConversationIDMessageID($ResourceID, $MessageSID);
    
    			for($mc = 0; $mc < count($media_info); $mc++) {
    			    $MediaSID  =	$media_info[$mc]['MediaSID'];
    			    
    			    $media_image_url = IRECRUIT_HOME . "vault/".$OrgID."/twilio/".$ResourceID."/".$MessageSID."/".$MediaSID."/".$media_info[$mc]['FileName'];
                    echo '<img src="'.$media_image_url.'" width="150" height="150">';
    				echo "&nbsp;&nbsp;<a href='".IRECRUIT_HOME."applicants/openTwilioImageByURL.php?url_unique_id=$unique_id' target='_blank'>Open</a>";
    			}
    
    			echo "</td>";
    			echo "</tr>";
		   } // end if resource_id
                 } // end foreach conversation
		}
		else {
            ?>
            <tr>
    			<td colspan="3">
    				<strong>No messages created for this conversation</strong>
    			</td>
    		</tr>
            <?php
		}
		?>
		</table>
		<?php
	}
}
?>
