<?php
if($flag == true) {
	?>
	<tr>
		<th colspan="2" height="40"></th>
	</tr>
	<tr>
		<th colspan="2" align="left">Selected Questions</th>
	</tr>
	<?php
}

for($k = 0; $k < count ( $QuestionIds ); $k ++) {
	$QuestionID = $QuestionIds [$k];
	
    if($FormID == "WebForms") {
        $where_info     =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID", "Active = 'Y'");
        $params_info    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$QuestionIdSection[$QuestionID], ":QuestionID"=>$QuestionID);
        $resquestions   =   G::Obj('FormQuestions')->getQuestionsInformation("WebFormQuestions", "*", $where_info, "Question DESC", array($params_info));
        
        $FQR                =   $resquestions['results'][0];
        $FQR ['SectionID']  =   $FQR ['Section'] = $QuestionIdSection[$QuestionID];
    }
    else {
        //Get Form Question Details
        $columns            =   "Question, QuestionID, QuestionTypeID, value";
        $FQR                =   G::Obj('FormQuestionsDetails')->getFormQuestionInfoBySecIDQuesID($columns, $OrgID, $FormID, $QuestionIdSection[$QuestionID], $QuestionID);
        
        $sec_info           =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $FormID, $QuestionIdSection[$QuestionID]);
        $FQR['Section']     =   $sec_info['SectionTitle'];
        $FQR ['SectionID']  =   $QuestionIdSection[$QuestionID];
    }

    if ($FQR ['QuestionTypeID'] == 2 
        || $FQR ['QuestionTypeID'] == 3 
        || $FQR ['QuestionTypeID'] == 9 
        || $FQR ['QuestionTypeID'] == 18
        || $FQR ['QuestionTypeID'] == 1818
        || $FQR ['QuestionTypeID'] == 100) {
        $name = preg_replace ( '/ /', '_', $FQR ['Section'] ) . ":" . $FQR ['QuestionID'] . ":QUS::";
         
        $QAID = "QA".$FQR ['QuestionID'];

        $answer_weight = "";
        
        if($FQR ['QuestionTypeID'] == 100) {
            $answer_weights_info = array();
            if(isset($QuestionWeightInfo[$FQR ['SectionID']][$FQR ['QuestionID']][$name])) {
                $answer_weights_info = unserialize($QuestionWeightInfo[$FQR ['SectionID']][$FQR ['QuestionID']][$name]);
            }
            
            $answer_weight = $answer_weights_info[$name];
        }
        else {
        	if(isset($QuestionWeightInfo[$FQR ['SectionID']][$FQR ['QuestionID']][$name])) {
                $answer_weight = $QuestionWeightInfo[$FQR ['SectionID']][$FQR ['QuestionID']][$name];
            }
        }
		?>
		<tr>
			<td align="left"><i><u>Question Weight</u></i>: <?php echo $FQR['Question'];?></td>
			<td align="left">
				<input type="text" name="QA[<?php echo $FQR ['SectionID'];?>][<?php echo $FQR ['QuestionID']?>][<?php echo $name;?>]" size="2" value="<?php echo $answer_weight;?>">
				<div class="delete_weighted_search_question" onclick="remove_question('<?php echo $FQR ['SectionID'];?>', '<?php echo $FQR ['QuestionID']?>')"></div>
				<div class="weighted_search_expand" onclick="show_answer('<?php echo $QAID?>', 'wsec<?php echo $QuestionID?>')" id="wsec<?php echo $QuestionID?>"></div>&nbsp;
			</td>
		</tr>
		<tr>
			<td align="left" colspan="2"><i><u>Answers Weight</u></i>: </td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="<?php echo $QAID;?>" style="display:none;">
					<table width="100%">
					<?php
					if($FQR ['QuestionTypeID'] == 100) {
						$rate_your_skills = unserialize($FQR ['value']);
						
						/**
						 * @tutorial Get weights for each combinations.
						 * Rval(Rating Values) treated as number of columns
						 * LabelValRow(Label Values) treated as number of rows 
						 */
						$tr_count = count($rate_your_skills['LabelValRow']);
						$td_count = count($rate_your_skills['RVal']);
						?>
						<tr>
							<td colspan="2">
								<table>
									<tr>
										<td>&nbsp;</td>
										<?php
										foreach ($rate_your_skills['RVal'] as $rkey=>$rvalue)
										{
											?><td align="center"><?php echo $rvalue;?></td><?php
										}
										?>
									</tr>
									<?php
										foreach ($rate_your_skills['LabelValRow'] as $label_key=>$label_value)
										{
											?>
											<tr>
												<td><?php echo $label_value;?></td>
												<?php 
												foreach ($rate_your_skills['RVal'] as $rkey=>$rvalue)
												{	
												    $answer_weight = "";
												    if(isset($answer_weights_info) && count($answer_weights_info) > 0) {
												        $answer_weight = $answer_weights_info[$label_key][$rkey];
												    }
													?>
													<td>
													   <input type="text" size="2" name="QA[<?php echo $FQR ['SectionID'];?>][<?php echo $FQR ['QuestionID']?>][<?php echo $label_key;?>][<?php echo $rkey;?>]" maxlength="2" value="<?php echo $answer_weight;?>">
													</td>
													<?php
												}
												?>
											</tr>
											<?php
										}
									?>
								</table>
							</td>
						</tr>
						<?php
					}
					else {
						$mans = explode ( '::', $FQR ['value'] );
						for($i = 0; $i < count ( $mans ); $i ++) {
							$ans = explode ( ':', $mans [$i] );
							$name = preg_replace ( '/ /', '_', $FQR ['Section'] ) . ":" . $FQR ['QuestionID'] . ":ANS:" . $ans [0] . ":";
							for($a = 0; $a < count ( $a ); $a ++) {
								$answer = $ans [0];
								if ($ans[0] != "") {
                                $answer_weight = "";
                                if(isset($QuestionWeightInfo[$FQR ['SectionID']][$FQR ['QuestionID']][$name])) {
                                	$answer_weight = $QuestionWeightInfo[$FQR ['SectionID']][$FQR ['QuestionID']][$name];
                                }
								?>
								<tr>
									<td width="88%" align="left">
									<?php
									echo $answer;
									?>
									</td>
									<td align="left"><input type="text" size="2" name="QA[<?php echo $FQR ['SectionID'];?>][<?php echo $FQR ['QuestionID']?>][<?php echo $name;?>]" value="<?php echo $answer_weight;?>" maxlength="2"></td>
								</tr>
								<?php
								}
							}
						}	
					}
					?>
				</table>
				</div>
			</td>
		</tr>
		<?php
		
	}
			
}