<?php
require_once '../Configuration.inc';

$LimitApplicants = $preferences['ApplicantsSearchResultsLimit'];

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto = $goto = $_SERVER ['QUERY_STRING'];
}

$TemplateObj->Sort                  = $Sort                 = $_REQUEST['Sort'];
$TemplateObj->search_keyword        = $search_keyword       = $_REQUEST['keyword'];
$TemplateObj->org_app_lbl_filter    = $org_app_lbl_filter   = $_REQUEST['ddlOrgAppLabelFilter'];
$TemplateObj->irecruit_filter       = $irecruit_filter      = $_REQUEST['ddlIrecruitAppFilter'];
$TemplateObj->filter_rating         = $filter_rating        = $_REQUEST['FilterRating'];
$TemplateObj->ApplicantsSearchGuID  = $ApplicantsSearchGuID = $_REQUEST['ApplicantsSearchGuID'];
$TemplateObj->LeadGenerator         = $LeadGenerator        =   $_REQUEST['LeadGenerator'];

if (isset ( $_GET ['sorttype'] ) && $_GET ['sorttype'] != "" && $_GET ['sorttype'] == "ASC") {
	$TemplateObj->to_sort = $to_sort = "ASC";
} else if ((isset ( $_GET ['sorttype'] ) && $_GET ['sorttype'] != "" && $_GET ['sorttype'] == "DESC")) {
	$TemplateObj->to_sort = $to_sort = "DESC";
} else {
	$TemplateObj->to_sort = $to_sort = "DESC";
}

//Set where condition
$where = array("OrgID = :OrgID", "UserID = :UserID", "GuID = :GuID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":GuID"=>$ApplicantsSearchGuID);
//Get SearchQuery Information
$results_saved_query = $OrganizationsObj->getSearchQuery("SearchParams", $where, '', array($params));
//Set Filter Parameters
$filter_params = json_decode($results_saved_query['results'][0]['SearchParams'], true);

unset($filter_params[":ApplicantConsiderationStatus1"]);
unset($filter_params[":ApplicantConsiderationStatus2"]);
unset($filter_params[":Rating"]);
unset($filter_params[":LeadGenerator"]);

$TemplateObj->filter_params = $filter_params;

include IRECRUIT_DIR . 'applicants/BuildSearchQuery.inc';

$twilio_user_info = G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "TwilioSms");

$aps_index = 0;
if (is_array ( $applicants_search_results )) {
	
	foreach ( $applicants_search_results as $row ) {

		include IRECRUIT_DIR . 'applicants/BuildWeightedSearchQuery.inc';
		
		$ApplicantEmail   =   $ApplicantDetailsObj->getAnswer ( $OrgID, $row ['ApplicationID'], 'email' );
		$user_info        =   $UserPortalUsersObj->getUserDetailInfoByEmail("UserID", $ApplicantEmail);
	    if($user_info['UserID'] > 0) {
		    $applicants_search_results[$aps_index]['UserPortalUserID']    =   $user_info['UserID'];
		}
		else {
		    $applicants_search_results[$aps_index]['UserPortalUserID']    =   '';
		}
		
		
		$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
		$applicants_search_results [$aps_index] ['WeightedSearchScore'] = $total;
		$applicants_search_results [$aps_index] ['ApplicantEmail'] = $ApplicantEmail;
		
		$applicants_search_results [$aps_index] ['OrganizationName'] = $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID );
		$applicants_search_results [$aps_index] ['RequisitionJobID'] = $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . "<br>" . G::Obj('RequisitionDetails')->getJobTitle($OrgID, $multiorgid_req, $RequestID);
		
		$applicants_search_results [$aps_index]['ApplicantSortName'] = str_replace("'", "&#8217;", $applicants_search_results[$aps_index]['ApplicantSortName']);
		
		$applicants_search_results [$aps_index]['StatusDescription'] = $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $row ['ProcessOrder'] );
		
		if ($ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] )) {
			$DispositionDesc = $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] );
			$applicants_search_results [$aps_index]['DispositionDescription'] = '<br>Disposition: '.$DispositionDesc;
		}
		
		$applicants_search_results [$aps_index]['SendBulkSms'] = "No";
		
		if($twilio_user_info['TwilioSms'] == "Y") {
			//Get applicant permission
			$cell_phone_permission		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $row ['ApplicationID'], 'cellphonepermission');
		
			//Get twilio number
			$current_user_details		=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell");
			$current_user_cell_number	=	str_replace(array("-", "(", ")", " "), "", $current_user_details['Cell']);
		
			//Add plus one before number
			if(substr($user_details['Cell'], 0, 2) != "+1") {
				$current_user_cell_number	=	"+1".$current_user_cell_number;
			}
		
			//Get applicant cellphone
			$applicant_cell_phone_info	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $row ['ApplicationID'], 'cellphone');
			$applicant_cell_phone		=	json_decode($applicant_cell_phone_info, true);
		
            $applicant_number           =   "";
            if($applicant_cell_phone[0].$applicant_cell_phone[1].$applicant_cell_phone[2] != "") {
                $applicant_number   =	"+1".$applicant_cell_phone[0].$applicant_cell_phone[1].$applicant_cell_phone[2];
			}
			
			
			if($cell_phone_permission == 'Yes') {
		
				if(strlen($current_user_cell_number) == "12"
					||	strlen($applicant_cell_number) == "12") {
					$applicants_search_results [$aps_index]['SendBulkSms'] = "Yes";
				}
		
			}
			
			$archive_con_info       =   G::Obj('TwilioConversationsArchive')->getTwilioConversationsArchive($OrgID, $USERID, $applicant_number, $current_user_cell_number);
			
			$applicants_search_results [$aps_index]["ArchiveTextMessages"]       =   "False";
			
			if($archive_con_info[0]["ResourceID"] != "") {
			    $applicants_search_results [$aps_index]["ArchiveTextMessages"]   =   "True";
			}

			$get_checklist_process_data      =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $applicants_search_results[$aps_index]['ApplicationID'], $applicants_search_results[$aps_index]['RequestID']);

                	$applicants_search_results [$aps_index]["Checklist"]       =   "False";

                	if ($get_checklist_process_data['StatusMatch'] == "Y") {
                	   $applicants_search_results [$aps_index]["Checklist"]       =   "True";
                	}

			/* edge applicant interview tab control */
			$InterviewFormID =   G::Obj('Interview')->getActiveInterviewForm($OrgID,$applicants_search_results[$aps_index]['RequestID'],$applicants_search_results[$aps_index]['ApplicationID'],$USERID,$USERROLE,$permit);
                        $applicants_search_results [$aps_index]["InterviewForms"]   =   "False";
                	if ($InterviewFormID != "") {
                              $applicants_search_results [$aps_index]["InterviewForms"]   =   "True";
                        }
		}
		
		$aps_index++;
		
	} // end foreach
}

$left_nav_info = $PaginationObj->getPageNavigationInfo($Start, $LimitApplicants, $_REQUEST['ApplicantsCount'], '', '');

echo json_encode(array("applicants_search_results"=>$applicants_search_results, "previous"=>$left_nav_info['previous'], "next"=>$left_nav_info['next'], "total_pages"=>$left_nav_info['total_pages'], "current_page"=>$left_nav_info['current_page'], "applicants_count"=>$_REQUEST['ApplicantsCount'], "UserRole"=>$USERROLE));
?>
