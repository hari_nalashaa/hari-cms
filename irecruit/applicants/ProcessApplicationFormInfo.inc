<?php
if ($_POST['process'] == "Y") {
    //Get Application Information
    $app_detail_info    =   $ApplicationsObj->getJobApplicationsDetailInfo("*", $OrgID, $ApplicationID, $RequestID);
    $app_data_bef_upd   =   $ApplicantsObj->getAppData($OrgID, $ApplicationID);

    //Set Request Data, Files Data
    $REQUEST_DATA       =   $_REQUEST;
    $FILES_DATA         =   $_FILES;
    
	// Set Company Variables
	// Validate input
	include COMMON_DIR . 'process/ValidateRequired.inc';

	if ($ERROR) {
		$ERROR = str_replace("\\n", "<br>", $ERROR);
		echo "<span style='color:red'>".$ERROR."</span>";
	}
	else {
        
	    /**
	     * This is a temporary code until the final upgrade.
	     * Upto that time, we have to keep on adding different conditions to it.
	     */
	    $FormID                        =   $_POST['FormID'];
	    $GetFormPostAnswerObj->POST    =   $_POST;    //Set Post Data
	    
	    $json_form_que_list    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID, $SectionID);
	    $form_que_list         =   $json_form_que_list['json_ques'];
	    
	    $app_data_aft_upd      =   array();
	    $app_data_aft_qi       =   array();
	    
	    foreach($form_que_list as $QuestionID=>$QI) {
	        G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
	        $ignore_ques           =   array('RandID', 'otherjobs', 'instructions');
	        
	        //After the complete upgrade have to remove this.
	        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	        $QI['ApplicationID']   =   $ApplicationID;
	        
	        if($QI['QuestionTypeID'] != 99
                && $QI['QuestionTypeID'] != 8
                && !in_array($QI['QuestionID'], $ignore_ques)) {
	                
                G::Obj('Applicants')->insUpdApplicantData($QI);
                
                $app_data_aft_upd[$QI['QuestionID']]           =   $QI['Answer'];
                $app_data_aft_upd_que_type[$QI['QuestionID']]  =   $QI['QuestionTypeID'];
                $app_data_aft_qi[$QI['QuestionID']]            =   $QI;
            }
	    }
		
		if ($_FILES ['applicant_profile_picture'] ['tmp_name'] != "") {
		
			// FILEHANDLING
			$dir = IRECRUIT_DIR . 'vault/' . $OrgID;
		
			if (! file_exists ( $dir )) {
				mkdir ( $dir, 0700 );
				chmod ( $dir, 0777 );
			}
		
			$app_picture_dir = $dir . '/applicant_picture';
		
			if (! file_exists ( $app_picture_dir )) {
				mkdir ( $app_picture_dir, 0700 );
				chmod ( $app_picture_dir, 0777 );
			}
		
			if ($_FILES ['applicant_profile_picture'] ['name'] != "") {
		
				$data   =   file_get_contents ( $_FILES ['applicant_profile_picture'] ['tmp_name'] );
				$e      =   explode ( '.', $_FILES ['applicant_profile_picture'] ['name'] );
				$ecnt   =   count ( $e ) - 1;
				$ext    =   $e [$ecnt];
				$ext    =   preg_replace ( "/\s/i", '', $ext );
				$ext    =   substr ( $ext, 0, 5 );
		
				$filename = $app_picture_dir . '/' . $ApplicationID . '-ApplicantPicture.' . $ext;
				$fh = fopen ( $filename, 'w' );
				fwrite ( $fh, $data );
				fclose ( $fh );
		
				chmod ( $filename, 0666 );
		
				// Insert Information Applicant Data
				$QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "QuestionID"=>"ApplicantPicture", "Answer"=>$ApplicationID."-ApplicantPicture.".$ext);
				$ApplicantsObj->insUpdApplicantData($QI);
				
                $app_data_aft_upd_que_type[$question]   =   0;
                $app_data_aft_upd[$question]            =   $answer;
                $app_data_aft_qi[$question]             =   $QI;
			}
		}
		
		
		// Insert into JobApplicationHistory
		if ($_POST ["Comment"]) {
			$Comments = $_POST ["Comment"];
		} else {
			$Comments = 'Application data updated.';
		}
		
		$updated_fields_info  =   array();
		$skip_questions       =   array("HTTP_REFERER", "HTTP_USER_AGENT", "REMOTE_ADDR", "REQUEST_URI");
		foreach($app_data_aft_upd as $bef_upd_que=>$bef_upd_ans) {
		    if(!in_array($bef_upd_que, $skip_questions)) {
		        if($app_data_aft_upd[$bef_upd_que] != $app_data_bef_upd[$bef_upd_que]) {
		            
		            if($app_data_aft_upd_que_type[$bef_upd_que] == 13) {
		                
		                $app_ans_bef  =   ($app_data_bef_upd[$bef_upd_que] != "") ? json_decode($app_data_bef_upd[$bef_upd_que], true) : "";
		                $app_data_bef_upd[$bef_upd_que] =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], '' );
		                
		                $app_ans_aft  =   ($app_data_aft_upd[$bef_upd_que] != "") ? json_decode($app_data_aft_upd[$bef_upd_que], true) : "";
		                $app_data_aft_upd[$bef_upd_que] =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], '' );
		            }
		            else if($app_data_aft_upd_que_type[$bef_upd_que] == 14) {
		                
		                $app_ans_bef  =   ($app_data_bef_upd[$bef_upd_que] != "") ? json_decode($app_data_bef_upd[$bef_upd_que], true) : "";
		                $app_data_bef_upd[$bef_upd_que] =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], $app_ans_bef[3] );
		                
		                $app_ans_aft  =   ($app_data_aft_upd[$bef_upd_que] != "") ? json_decode($app_data_aft_upd[$bef_upd_que], true) : "";
		                $app_data_aft_upd[$bef_upd_que] =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], $app_ans_aft[3] );
		            }
		            else if($app_data_aft_upd_que_type[$bef_upd_que] == 15) {
		                
		                $app_ans_bef  =   ($app_data_bef_upd[$bef_upd_que] != "") ? json_decode($app_data_bef_upd[$bef_upd_que], true) : "";
		                $app_data_bef_upd[$bef_upd_que] =   $app_ans_bef[0].'-'.$app_ans_bef[1].'-'.$app_ans_bef[2];
		                
		                $app_ans_aft  =   ($app_data_aft_upd[$bef_upd_que] != "") ? json_decode($app_data_aft_upd[$bef_upd_que], true) : "";
		                $app_data_aft_upd[$bef_upd_que] =   $app_ans_aft[0].'-'.$app_ans_aft[1].'-'.$app_ans_aft[2];
		            }
		            else if($app_data_aft_upd_que_type[$bef_upd_que] == 18) {
		                
		                $que_vals              =   explode ( '::', $app_data_aft_qi[$bef_upd_que]['value'] );
		                $que_vals_cnt          =   count($que_vals);
		                
		                $que_values_info       =   array();
		                for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
		                    $que_vals_info     =   explode(":", $que_vals[$qvc]);
		                    $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
		                }
		                
		                $app_ans_bef        =   $app_data_bef_upd[$bef_upd_que];
		                $app_ans_aft        =   $app_data_aft_upd[$bef_upd_que];
		                
		                $ans_bef_ans_jd     =   json_decode($app_ans_bef, true);
		                $ans_aft_ans_jd     =   json_decode($app_ans_aft, true);
		                
		                if(is_array($ans_bef_ans_jd)) {
		                    foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
		                        $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
		                    }
		                }
		            }
		            else if($app_data_aft_upd_que_type[$bef_upd_que] == 1818) {
		                
		                $que_vals              =   explode ( '::', $app_data_aft_qi[$bef_upd_que]['value'] );
		                $que_vals_cnt          =   count($que_vals);
		                
		                $que_values_info       =   array();
		                
		                for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
		                    $que_vals_info     =   explode(":", $que_vals[$qvc]);
		                    $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
		                }
		                
		                $app_ans_bef        =   $app_data_bef_upd[$bef_upd_que];
		                $app_ans_aft        =   $app_data_aft_upd[$bef_upd_que];
		                
		                $ans_bef_ans_jd     =   json_decode($app_ans_bef, true);
		                $ans_aft_ans_jd     =   json_decode($app_ans_aft, true);
		                
		                if(is_array($ans_bef_ans_jd)) {
		                    foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
		                        $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
		                    }
		                }
		                
		                foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
		                    $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
		                }
		                
		                $app_data_bef_upd[$bef_upd_que]   =   @implode(", ", array_values($ans_bef_ans_jd));
		                $app_data_aft_upd[$bef_upd_que]   =   @implode(", ", array_values($ans_aft_ans_jd));
		            }
		            else if($app_data_aft_upd_que_type[$bef_upd_que] == 100) {
		                
		                $canswer1   =   unserialize($app_data_bef_upd[$bef_upd_que]);
		                $rtn1       =   '';
		                
		                if(is_array($canswer1)) {
		                    $rtn1 .= '<br>';
		                    foreach ($canswer1 as $cakey=>$caval) {
		                        $rtn1 .= '&nbsp;&nbsp;'.$cakey;
		                        foreach ($caval as $cavk=>$cavv) {
		                            $rtn1 .= ' - &nbsp;'.$cavv;
		                        }
		                    }
		                    $rtn1 .= '<br>';
		                }
		                
		                $canswer2   =   unserialize($app_data_aft_upd[$bef_upd_que]);
		                $rtn2       =   '';
		                
		                if(is_array($canswer2)) {
		                    $rtn2 .= '<br>';
		                    foreach ($canswer2 as $cakey=>$caval) {
		                        $rtn2 .= '&nbsp;&nbsp;'.$cakey;
		                        foreach ($caval as $cavk=>$cavv) {
		                            $rtn2 .= ' - &nbsp;'.$cavv;
		                        }
		                        $rtn2 .= '<br>';
		                    }
		                }
		                
		                $app_data_bef_upd[$bef_upd_que]   =   $rtn1;
		                $app_data_aft_upd[$bef_upd_que]   =   $rtn2;
		            }
		            else if($app_data_aft_upd_que_type[$bef_upd_que] == 120) {
		                
		                $canswer1   =   unserialize($app_data_bef_upd[$bef_upd_que]);
		                $rtn1       =   '';
		                
		                if(is_array($canswer1)) {
		                    $days_count = count($canswer1['days']);
		                    for($ci = 0; $ci < $days_count; $ci++)  {
		                        $rtn1 .= '<br>';
		                        $rtn1 .= '&nbsp;'.$canswer1['days'][$ci].'&nbsp;';
		                        $rtn1 .= '&nbsp;from&nbsp;&nbsp;&nbsp;';
		                        $rtn1 .= '&nbsp;'.$canswer1['from_time'][$ci].'&nbsp;&nbsp;';
		                        $rtn1 .= '&nbsp;to &nbsp;&nbsp;';
		                        $rtn1 .= '&nbsp;'.$canswer1['to_time'][$ci].'&nbsp;&nbsp;';
		                    }
		                }
		                
		                $canswer2   =   unserialize($app_data_aft_upd[$bef_upd_que]);
		                $rtn2       =   '';
		                
		                if(is_array($canswer2)) {
		                    $days_count = count($canswer2['days']);
		                    for($ci = 0; $ci < $days_count; $ci++)  {
		                        $rtn2 .= '<br>';
		                        $rtn2 .= '&nbsp;'.$canswer2['days'][$ci].'&nbsp;';
		                        $rtn2 .= '&nbsp;from&nbsp;&nbsp;&nbsp;';
		                        $rtn2 .= '&nbsp;'.$canswer2['from_time'][$ci].'&nbsp;&nbsp;';
		                        $rtn2 .= '&nbsp;to &nbsp;&nbsp;';
		                        $rtn2 .= '&nbsp;'.$canswer2['to_time'][$ci].'&nbsp;&nbsp;';
		                    }
		                }
		                
		                $app_data_bef_upd[$bef_upd_que]   =   $rtn1;
		                $app_data_aft_upd[$bef_upd_que]   =   $rtn2;
		            }
		            
		            $updated_fields_info['Updated Fields'][$bef_upd_que] = array("Original"=>$app_data_bef_upd[$bef_upd_que], "Updated"=>$app_data_aft_upd[$bef_upd_que]);
		            unset($app_data_aft_upd[$bef_upd_que]);
		            unset($app_data_bef_upd[$bef_upd_que]);
		        }
		        else if($app_data_aft_upd[$bef_upd_que] == $app_data_bef_upd[$bef_upd_que]) {
		            unset($app_data_aft_upd[$bef_upd_que]);
		            unset($app_data_bef_upd[$bef_upd_que]);
		        }
		    }
		}
		
		$updated_fields_info['Additional Fields'] =   $app_data_aft_upd;
		$json_updated_fields_info                 =   json_encode($updated_fields_info);
		
		if ($app_detail_info['RequestID'] != "" 
		    && count($updated_fields_info['Updated Fields']) > 0) {
            // Job Application History Information
            $job_app_history_info = array (
                "OrgID"                 =>  $OrgID,
                "ApplicationID"         =>  $ApplicationID,
                "RequestID"             =>  $app_detail_info['RequestID'],
                "ProcessOrder"          =>  "-3",
                "Date"                  =>  "NOW()",
                "StatusEffectiveDate"   =>  "NOW()",
                "UserID"                =>  $USERID,
                "Comments"              =>  $Comments,
                "UpdatedFields"         =>  $json_updated_fields_info
            );
            //Insert Job Application History
            G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history_info );
		}
		
		//Set Update Information
		$jobapp_set_info      =   array("LastModified = NOW()");
		
		if($SectionID == "1") {
		    $jobapp_set_info[]    =   "ApplicantSortName = :ApplicantSortName";
		}
		
		//Set Update Where Condition
		$jobapp_upd_where     =   array(
                                    "OrgID                  =   :OrgID", 
                                    "ApplicationID          =   :ApplicationID", 
                                    "RequestID              =   :RequestID"
    	                          );
		//Set Update Parameters
		$jobapp_upd_params    =  array(	
                                    ":OrgID"                =>  $OrgID, 
                                    ":ApplicationID"        =>  $ApplicationID, 
                                    ":RequestID"            =>  $RequestID,
    		                      );
		if($SectionID == "1") {
		    $jobapp_upd_params[":ApplicantSortName"]  =   $_REQUEST ['last'] . ', ' . $_REQUEST ['first'];
		}
		
		G::Obj('Applications')->updApplicationsInfo('JobApplications', $jobapp_set_info, $jobapp_upd_where, array($jobapp_upd_params));
		
		$message = "You have sucessfully updated this applicant.";
	}
	
} // end process
?>
