<?php
$POST = array();
$GET = array();
foreach ($_POST as $postkey => $postvalue) {
	//Escape Post Values With mysql_real_escape_string to avoid sql injection
	$POST[$postkey] = strip_tags($postvalue);
}
foreach ($_GET as $getkey => $getvalue) {
	//Escape Get Values With mysql_real_escape_string to avoid sql injection
	$GET[$getkey] = strip_tags($getvalue);
}