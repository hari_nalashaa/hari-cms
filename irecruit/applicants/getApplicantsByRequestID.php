<?php
require_once '../Configuration.inc';

// Get Applicants based on RequestID
$columns        =   "ApplicationID, RequestID, ApplicantSortName, ProcessOrder, EntryDate, LastModified";
$where_info     =   array("OrgID  =   :OrgID");
$params_info    =   array(":OrgID"  =>  $OrgID);


if(isset($_REQUEST['ToDate']) && isset($_REQUEST['ToDate'])) {
    $where_info[] = "EntryDate      <=  :ToDate";
    $where_info[] = "EntryDate      >=  :FromDate";
    
    $params_info[":FromDate"]   =   $DateHelperObj->getYmdFromMdy($_REQUEST['FromDate'], "/", "-");
    $params_info[":ToDate"]     =   $DateHelperObj->getYmdFromMdy($_REQUEST['ToDate'], "/", "-");
}

if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
    $where_info[] = "RequestID = :RequestID";
    $params_info[":RequestID"] = $_REQUEST['RequestID'];
}

if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") {
    $where_info[] = "MultiOrgID = :MultiOrgID";
    $params_info[":MultiOrgID"] = $_REQUEST['MultiOrgID'];
}

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
        $RequestID = $RequisitionDetailsObj->getHiringManagerRequestID();
	$where_info[]         =   "RequestID IN ('" . implode("','",$RequestID) . "')";
}


$app_results = $ApplicationsObj->getJobApplicationsInfo($columns, $where_info, '', 'EntryDate DESC LIMIT 100', array(
    $params_info
));

$applicants_list = $app_results['results'];

for ($i = 0; $i < count($applicants_list); $i ++) {
    $ApplicationID  = $applicants_list[$i]['ApplicationID'];
    $APPDATA        = $ApplicantsObj->getAppData($OrgID, $ApplicationID);
    
    $applicants_list[$i]['ReferralSource']  = $APPDATA['informed'];
    $applicants_list[$i]['ProcessOrder']    = $ApplicantDetailsObj->getProcessOrderDescription($OrgID, $applicants_list[$i]['ProcessOrder']);
    $applicants_list[$i]['Email']           = $APPDATA['email'];
    $applicants_list[$i]['HomePhone']       = $APPDATA ['homephone1'] . "-" . $APPDATA ['homephone2'] . "-" . $APPDATA ['homephone3'];
    $applicants_list[$i]['EntryDateMDY']    = date('m/d/Y H:i:s', strtotime($applicants_list[$i]['EntryDate']));
    $applicants_list[$i]['LastModifiedMDY'] = date('m/d/Y H:i:s', strtotime($applicants_list[$i]['LastModified']));
    
}

for($c = 0; $c < count($applicants_list); $c++) {
    $application_info       =   $applicants_list[$c];
    
    foreach($application_info as $application_key=>$application_value) {
        $application_info[$application_key] = utf8_encode($application_value);
    }
    
    $applicants_list[$c]    =   $application_info;
}

// Applications list
if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "applicants_list"           =>  $applicants_list,
        "previous"                  =>  utf8_encode($left_nav_info['previous']),
        "next"                      =>  utf8_encode($left_nav_info['next']),
        "total_pages"               =>  utf8_encode($left_nav_info['total_pages']),
        "current_page"              =>  utf8_encode($left_nav_info['current_page']),
        "applicants_count"          =>  utf8_encode($applicants_count),
        "count_per_page"            =>  utf8_encode($applicants_list['count']),
    ));
}
?>
