<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title =   'iRecruit - Document View';
$display_app_header =   $_REQUEST['display_app_header'];

if ($permit ['Applicants'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

$TemplateObj->SectionID     =   $SectionID      =   "AA";

include IRECRUIT_DIR . 'views/applicants/DocumentView.inc';
?>