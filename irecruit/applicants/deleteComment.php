<?php
require_once '../Configuration.inc';

if (isset ( $OrgID ) && isset ( $_REQUEST ['updateid'] )) {
	/*
	$params_ins = array (
			":OrgID" => $OrgID,
			":ApplicationID" => $_REQUEST ['ApplicationID'],
			":RequestID" => $_REQUEST ['RequestID'],
			":UpdateID" => $_REQUEST ['updateid'] 
	);
	G::Obj('JobApplicationHistory')->insJobApplicationHistoryArchive ( array ($params_ins ) );
	*/
	
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"ApplicationID = :ApplicationID",
			"RequestID = :RequestID",
			"UpdateID = :UpdateID" 
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":ApplicationID" => $_REQUEST ['ApplicationID'],
			":RequestID" => $_REQUEST ['RequestID'],
			":UpdateID" => $_REQUEST ['updateid'] 
	);
	// Delete JobApplicationHistory Information
	$res_app_history = $ApplicationsObj->delApplicationsInfo ( 'JobApplicationHistory', $where, array (
			$params 
	) );
}

$link = IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID=' . $_REQUEST ['ApplicationID'] . '&RequestID=' . $_REQUEST ['RequestID'];
if ($AccessCode) {
	$link .= '&k=' . $AccessCode;
}

if($res_app_history['affected_rows'] > 0) {
	echo "Deleted successfully";
}
else {
	echo "Sorry there is a problem while deleting.";
}
?>