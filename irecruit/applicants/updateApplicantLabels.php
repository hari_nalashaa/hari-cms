<?php 
require_once '../Configuration.inc';

$set_info   = array("ApplicantConsiderationStatus2 = :ApplicantConsiderationStatus2");
$where_info = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
$params     = array(":ApplicantConsiderationStatus2"=>$_REQUEST['app_label'],
    				":OrgID"=>$OrgID, 
    				":ApplicationID"=>$_REQUEST['ApplicationID'], 
    				":RequestID"=>$_REQUEST['RequestID']
                    );
$res_app_lbl_info = $ApplicationsObj->updApplicationsInfo('JobApplications', $set_info, $where_info, array($params));

//Get organization information
$results_labels_json  = $OrganizationDetailsObj->getOrganizationInformation($OrgID, $MultiOrgID, "ApplicantConsiderationLabels");
$results_labels  = json_decode($results_labels_json["ApplicantConsiderationLabels"], true);

$label_key = $_REQUEST['app_label'];
$label_value = $results_labels[$_REQUEST['app_label']];

// Job Application History Information
$job_app_history = array (
		"OrgID"=>$OrgID,
		"ApplicationID"=>$_REQUEST['ApplicationID'],
		"RequestID"=>$_REQUEST['RequestID'],
		"StatusEffectiveDate"=>"CURDATE()",
		"Date"=>"NOW()",
		"UserID"=>$USERID,
		"Comments"=>"Label updated to ".$label_value
);
// Insert Job Application History Information
$res_job_app_history = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );

echo json_encode($res_app_lbl_info);
?>