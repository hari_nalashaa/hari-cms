<?php 
global $USERID;

//Set the FormID, SectionID from Post
if (count ( $_POST ) > 0) {
	$FormID = $_POST['FormID'];
	$SectionID = $_POST['SectionID'];
}

//Code To Delete Existing Items
if(isset($_POST['uwkey'])) {

	//Set where condition
	$where         =   array("UserID = :UserID", "WeightedSearchRandId = :WeightedSearchRandId");
	//Set parameters
	$params        =   array(":UserID"=>$USERID, ":WeightedSearchRandId"=>$_POST['uwkey']);
	$ressearchform =   G::Obj('WeightedSearch')->getWeightSaveSearchInfo("SaveSearchName", $where, "WeightedSearchRandId", "", array($params));
	$rowsearchform =   $ressearchform['results'][0];
	
	if(trim($_POST['SaveSearch']) == trim($rowsearchform['SaveSearchName'])) {
		G::Obj('WeightedSearch')->delWeightSaveSearch($_POST['uwkey']);
	}	
	
	if (!headers_sent()) {
		header("Location:applicants.php?action=savedweightedsearch&msg=upd");
		exit;
	}
	else {
		echo '<script>location.href="applicants.php?action=savedweightedsearch&msg=upd";</script>';
	}
}

//Code To Delete The Saved Search
if(isset($_GET['wkey'])) {
	
	G::Obj('WeightedSearch')->delWeightSaveSearch($_GET['wkey']);
	
	if (!headers_sent()) {
		header("Location:applicants.php?action=savedweightedsearch&msg=del");
		exit;
	}
	else {
		echo '<script>location.href="applicants.php?action=savedweightedsearch&msg=del";</script>';
	}
}

//Set where condition
$where              =   array("UserID = :UserID");
//Set parameters
$params             =   array(":UserID"=>$USERID);
$ressavedsearch     =   G::Obj('WeightedSearch')->getWeightSaveSearchInfo("*", $where, "SaveSearchName", "", array($params));
$rownumsavedsearch  =   $ressavedsearch['count'];
?>
<table width="100%" cellpadding="4" cellspacing="4" class="table table-striped table-bordered table-hover">
  <tr>
    <th align="left" height="20" colspan="2"></th>
  </tr>
  <tr>
    <th align="left" colspan="2">Saved Forms</th>
  </tr>
  <tr>
    <td align="left" height="10" colspan="2">
    	<?php 
    		if($_GET['msg'] == 'suc') echo "<span style='color:red'>Successfully Saved</span>";
    		if($_GET['msg'] == 'upd') echo "<span style='color:red'>Successfully Updated</span>";
    		if($_GET['msg'] == 'del') echo "<span style='color:red'>Successfully Deleted</span>";
    	?>
    </td>
  </tr>
  <tr>
    <th align="left">Form Name</th>
    <th align="left">Action</th>
  </tr>
  <?php
  	if($rownumsavedsearch > 0) {
		foreach($ressavedsearch['results'] as $row) {
		?><tr>
			<td><?php echo $row['SaveSearchName'];?></td>
		  	<td align="left">
		  		<a href="applicants.php?action=weightedsearch&subaction=edit&uwkey=<?php echo $row['WeightedSearchRandId']?>&menu=10">
		  			<img title="Edit" src="<?php echo IRECRUIT_HOME;?>images/icons/pencil.png">
		  		</a>
		  		&nbsp;
		  		<a href="applicants.php?action=savedweightedsearch&subaction=delete&wkey=<?php echo $row['WeightedSearchRandId']?>&menu=10">
		  			<img title="Delete" src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png">
		  		</a>
		  	</td>
		  </tr><?php
		}	
  	}
	else {
		?>
		<tr>
			<td colspan="2" align="center">
				<h3 style="font-weight: normal;">
					You don't have any saved forms.
				</h3>
			</td>
		</tr>
		<?php
	}
  ?>
</table>