<?php 
require_once '../Configuration.inc';
require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';

// Set page title
$TemplateObj->title	=	$title	=	"Send Text";

// Load Graphs Related Scripts, Only for this page
$scripts_header [] = "js/common-min.js";
$scripts_header [] = "js/twilio.js";

// Set these scripts to header scripts objects
$TemplateObj->page_scripts_header 	=	$scripts_header;

if(isset($_REQUEST['send_bulk_sms']) && $_REQUEST['send_bulk_sms'] != "") {
	$_SESSION['SmsApplicants']	=	$_REQUEST['send_bulk_sms'];	
}

if(count($_SESSION['SmsApplicants']) > 0 && isset($_REQUEST['taSmsMessage']) && $_REQUEST['taSmsMessage'] != "") {
	for($sc = 0; $sc < count($_SESSION['SmsApplicants']); $sc++) {
		
		list($ApplicationID, $RequestID)	=	explode(":", $_SESSION['SmsApplicants'][$sc]);
		
		/**
		 * Get informaton to swap the template
		 */
		
		//Get Requisition Information
		$multiorgid_req	=   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
		$job_title		=   G::Obj('RequisitionDetails')->getJobTitle($OrgID, $multiorgid_req, $RequestID );
		
		//Organization name
		$OrgName		=   G::Obj('OrganizationDetails')->getOrganizationName($OrgID, "");
		
		//Applicant Information
		$first			=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'first');
		$last			=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'last');

		//Get twilio number
		$user_details	=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "FirstName, LastName");
		
		//Set information for every applicant
		$message		=	$_REQUEST['taSmsMessage'];
		$message 		=	str_replace("{first}", $first, $message);
		$message 		=	str_replace("{last}", $last, $message);
		$message 		=	str_replace("{JobTitle}", $job_title, $message);
		$message 		=	str_replace("{OrganizationName}", $OrgName, $message);
		$message 		=	str_replace("{RecruiterName}", trim($user_details['FirstName'] . ' ' . $user_details['LastName']), $message);
		
		include IRECRUIT_DIR . 'applicants/ProcessSendBulkSms.inc';
	}
	
	//Unset Applicants
	unset($_SESSION['SmsApplicants']);

	if(count($_SESSION['FaildApplicants']) > 0) {
		header("Location:sendBulkSms.php?menu=2&msg=failed");
		exit();
	}
	else {
		header("Location:sendBulkSms.php?menu=2&msg=suc");
		exit();
	}
}

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/SendBulkSms' );
?>