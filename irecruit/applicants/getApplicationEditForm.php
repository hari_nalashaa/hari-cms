<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

if(isset($_REQUEST['OrgID'])) $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['RequestID'])) $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['ApplicationID'])) $ApplicationID = $_REQUEST['ApplicationID'];

if (! $HoldID) {
    $HoldID = G::Obj('MysqlHelper')->getDateTime ( '%Y%m%d%H%m%s' );
    $HoldID = uniqid ( $HoldID );
} // end HoldID

$SectionID          =   (isset($_REQUEST['SectionID']) && $_REQUEST['SectionID'] != "") ? $_REQUEST['SectionID'] : '1';
$formtable          =   "FormQuestions";

require_once IRECRUIT_DIR . 'FilterIrecruitSections.inc';

$skip_sections      =   array("6", "14", "AA", "VET", "DIS");
$all_sections_list  =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y", "Yes");
$sections_list      =   array();

foreach($all_sections_list as $section_id=>$current_section_info)
{
    if(!in_array($section_id, $skip_sections)) {
        $sections_list[$section_id] =   $current_section_info;
    }
}

//Get Requisition Detail Information
$requisition_info   =   G::Obj('Requisitions')->getReqDetailInfo("RequisitionID", $OrgID, "", $RequestID);
$RequisitionID      =   $requisition_info['RequisitionID'];

//Get FormID based on ApplicationID, RequestID
$app_detail_info    =   G::Obj('Applications')->getJobApplicationsDetailInfo("FormID", $OrgID, $ApplicationID, $RequestID);
$FormID             =   $app_detail_info['FormID'];

//Get Applicant Data
$APPDATA            =   G::Obj('Applicants')->getAppData ( $OrgID, $ApplicationID );

//Get Child Questions Information
$personal_ques_info =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, "1");
$child_ques_info    =   G::Obj('ApplicationFormQuestions')->getFormChildQuesForParentQues($OrgID, $FormID, "1");

//Get Question Types List
$que_types_list     =   array();
if(is_array($personal_ques_info)) {
    foreach($personal_ques_info as $personal_que_id=>$personal_que_info) {
        $que_types_list[$personal_que_id]   =   $personal_que_info['QuestionTypeID'];
    }
}

//Process Application Information
include_once IRECRUIT_DIR . 'views/applicants/ApplicationEdit.inc';

if(isset($lst)) {
	?>
	<script>
	var lst					=	'<?php echo $lst?>';
	var validate_dates_info	=	'<?php echo $validate_dates_info;?>';
	validate_dates_info		=	JSON.parse(validate_dates_info);

	var date_split_ids		=	lst.split(",");
	var date_objs			=	new Array();

	var i 			=	0;
	var date_id 	=	"";
	var year_range 	=	"";
	for(date_id_key in date_split_ids) {
		date_id		=	date_split_ids[i];
		date_id		=	$.trim(date_id);
		year_range	=	getYearRange(validate_dates_info, date_id);
		date_picker(date_id, 'mm/dd/yy', year_range, '');
		i++;
	}
	</script>
	<?php
}
?>