<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title = $title = 'View Correspondence';

$TemplateObj->ApplicationID = $ApplicationID = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/applicants/ViewCorrespondence');
?>