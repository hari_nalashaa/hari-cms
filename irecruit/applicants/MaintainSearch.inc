<?php
if ($process == 'Y') {
	$IrecruitUsersObj->delUserSavedSearches ( $OrgID, $USERID, $SearchName );
} // end process

echo '<br>';
echo '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-bordered table-hover">';

//Set where condition
$where = array("OrgID = :OrgID", "UserID = :UserID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
//Get UserSaved Searches Information
$results = $IrecruitUsersObj->getUserSavedSearches("SearchName", $where, "", array($params));
$i = 0;
$rowcolor = "#eeeeee";

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$SearchName = $row ['SearchName'];
		$i ++;
	
		if ($i == 1) {
			echo '<tr><td><b>Search Name</b></td><td width="20">&nbsp;</td><td align="center" width="60"><b>View/Use</b></td><td align="center" width="60"><b>Delete</b></td></tr>';
		}
	
		echo '<tr bgcolor="' . $rowcolor . '">';
		echo '<td>';
		echo $SearchName;
		echo '</td>';
		echo '<td>&nbsp;</td>';
		echo '<td align="center">';
		echo '<a href="applicantsSearch.php?action=search&SearchName=' . $SearchName . '">';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View">';
		echo '</a>';
		echo '</td>';
		echo '<td align="center">';
		echo '<a href="applicants.php?action=maintainsearch&process=Y&SearchName=' . $SearchName . '" onclick="return confirm(\'Are you sure you want to delete the following search setting?\n\n' . $SearchName . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>';
		echo '</td>';
		echo '</tr>';
	
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}

if ($i == 0) {
	echo '<tr><td colspan="100%" height="100" align="center"><p>Currently there are no saved queries.</p></td></tr>';
}
?>
</table>