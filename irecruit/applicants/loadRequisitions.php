<?php
require_once '../Configuration.inc';
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 600);

$TemplateObj->Active    = $Active   = isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->criteria  = $criteria = isset($_REQUEST['criteria']) ? $_REQUEST['criteria'] : '';
$TemplateObj->k         = $k        = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->MultiOrgID    = $MultiOrgID   = isset($_REQUEST['MultiOrgID']) ? $_REQUEST['MultiOrgID'] : '';
$TemplateObj->new       = $new      = isset($_REQUEST['new']) ? $_REQUEST['new'] : '';
$TemplateObj->refine    = $refine   = isset($_REQUEST['refine']) ? $_REQUEST['refine'] : '';

if ($criteria != "") {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "UserID = :UserID", "SearchName = :SearchName");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":SearchName"=>$criteria);
	
	//Get UserSaved Searches
	$results = $IrecruitUsersObj->getUserSavedSearches("RequestIDs", $where, "", array($params));
	
	if(is_array($results['results'][0])) {
		list ( $RequestIDs ) = array_values ( $results['results'][0] );
	}	
}

//Set where condition
$where_reqs = array("OrgID = :OrgID", "Approved = 'Y'");
//Set parameters
$params_reqs = array(":OrgID"=>$OrgID);

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
		
	$reqs = "";
	
	//Set where condition
	$where = array("OrgID = :OrgID", "UserID = :UserID");
	//Set the parameters
	$params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
	//Get Requisition Managers Information
	$resultsIN = $RequisitionsObj->getRequisitionManagersInfo("RequestID", $where, "", "", array($params));
	
	if(is_array($resultsIN['results'])) {
		foreach($resultsIN['results'] as $RM) {
			$reqs .= "'" . $RM ['RequestID'] . "',";
		}	
	}
	
	$reqs = substr ( $reqs, 0, - 1 );
		
	if ($reqs) {
	  $where_reqs[] = "RequestID IN ($reqs)";
	} // end if reqs

} // end hiring manager

if (($Active == "Y") || ($Active == "N")) {
	$where_reqs[] = "Active = :Active";
	$params_reqs[":Active"] = $Active;
}

if ($feature ['MultiOrg'] == "Y") {
	
	if ($MultiOrgID == "") {
	} else if ($MultiOrgID == "ORIG") {
		$where_reqs[] = "MultiOrgID = :MultiOrgID";
		$params_reqs[":MultiOrgID"] = '';
	} else {
		$where_reqs[] = "MultiOrgID = :MultiOrgID";
		$params_reqs[":MultiOrgID"] = $MultiOrgID;
	}

} // end feature


//Get requisition information
$results = $RequisitionsObj->getRequisitionInformation("RequestID, Title", $where_reqs, "", "Title limit 1000", array($params_reqs));

$rslts = "";
$rslts .= '<select name="RequestIDs[]" multiple size="15" class="form-control width-auto-inline">';

if(is_array($results['results'])) {
	foreach($results['results'] as $REQS) {
	
		$RequestID = $REQS ['RequestID'];
		$Title = $REQS ['Title'];
	
		if(isset($RequestIDs) && $RequestIDs != "") {
		    $requests = explode ( "|", $RequestIDs );
		}
	
		if (isset($requests) && is_array($requests) && in_array ( $RequestID, $requests )) {
			$selected = ' selected';
		} else {
			$selected = '';
		}
	
		$rslts .= '<option value="' . $RequestID . '"' . $selected . '>';
		if ($feature ['MultiOrg'] == "Y") {
			$rslts .= $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . " - ";
		} // end feature
		
		$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
		$rslts .= $Title . ': ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
	}	
}

$rslts .= '</select>';

header ( 'P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"' );

echo $rslts;
?>
