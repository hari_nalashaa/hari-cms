<?php 
require_once '../Configuration.inc';

$where_info     =   array("OrgID = :OrgID");
$params_info    =   array(":OrgID"=>$OrgID);
$app_sum_ques   =   $ApplicantsObj->getApplicantSummaryQuestionsInfo("*", $where_info, '', '', array($params_info));

$app_info       =   $app_sum_ques['results'];
$app_cnt        =   $app_sum_ques['count'];

// Get applicant data
$APPDATA        =   $ApplicantsObj->getAppData ($OrgID, $_REQUEST['ApplicationID'] );

for($i = 0; $i < $app_cnt; $i++) {
	$QuestionTypeID    =   $app_info[$i]['QuestionTypeID'];
	$QuestionID        =   $app_info[$i]['QuestionID'];
	$Question          =   $app_info[$i]['Question'];
	$SectionID         =   $app_info[$i]['SectionID'];
	
	if($QuestionTypeID == 8) {
		
		// Set where condition
		$formque_where    =   array(
                                    "OrgID          =   :OrgID",
                                    "FormID         =   :FormID",
                                    "SectionID      =   6",
                                    "QuestionTypeID =   8",
                                    "Active         =   'Y'"
                                );
		// Set parameters
		$formque_params   =   array (
                                    ":OrgID"        =>  $OrgID,
                                    ":FormID"       =>  "STANDARD"
                                );
		$resultsA         =   $FormQuestionsObj->getFormQuestionsInformation ( "QuestionID, value, Required", $formque_where, "QuestionOrder", array($formque_params) );
		
		
		$attachments = array();
		if (is_array ( $resultsA ['results'] )) {
			foreach ( $resultsA ['results'] as $AA ) {
		
				// Set attachment parameters
				$attach_params  =   array(
                                        ":OrgID"          =>  $OrgID,
                                        ":ApplicationID"  =>  $_REQUEST['ApplicationID'],
                                        ":TypeAttachment" =>  $app_info[$i]['QuestionID']
                                    );
				// Set attachment where condition
				$attach_where   =   array(
                                        "OrgID            =   :OrgID",
                                        "ApplicationID    =   :ApplicationID",
                                        "TypeAttachment   =   :TypeAttachment"
                                    );
				$resultsB       =   $AttachmentsObj->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );
		
				$hit            =   $resultsB ['count'];
		
				if ($hit > 0) {
					$attachments[$AA ['QuestionID']]['AttachmentType'] = $AA ['value'];
					$attachments[$AA ['QuestionID']]['Link'] = 'display_attachment.php?OrgID=' . $OrgID . '&ApplicationID=' . $_REQUEST['ApplicationID'] . '&Type=' . $app_info[$i]['QuestionID'];
				} else {
					$attachments[$AA ['QuestionID']]['AttachmentType'] = $AA ['value'];
					$attachments[$AA ['QuestionID']]['Link'] = "Not Submitted";
				}
		
			} // end foreach
		}
		
		if($attachments[$QuestionID]['Link'] != "Not Submitted") {
			echo $Question . ": " . "<a href='".$attachments[$QuestionID]['Link']."'>Download</a>";
		}
		else {
			echo $Question . ": " . "Not Submitted";
		}
	}	
	else {
		echo $ApplicantsDataObj->printSection($OrgID, "STANDARD", $Question, $QuestionID, $QuestionTypeID, $SectionID, $APPDATA);
	}
}
?>