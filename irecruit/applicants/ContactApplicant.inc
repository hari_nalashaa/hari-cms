<?php
require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';
require_once IRECRUIT_DIR . 'applicants/ApplicantsMultipleApps.inc';

//Process Contact Applicant Information
include IRECRUIT_DIR . 'applicants/ProcessContactApplicant.inc';

//Contact Affected Applicants
echo '<div id="contact_affected_applicants">';
echo '<br>The following applications will be affected:<br>';
displayList ( $multipleapps, $OrgID, $permit );
echo '</div>';

//Correspondence Package Information
include IRECRUIT_DIR . 'applicants/CorrespondencePackageListForm.inc';
?>
<script src="js/contact-applicant.js" type="text/javascript"></script>
<?php
echo '<form method="post" action="applicants.php" name="frmContactApplicant" id="frmContactApplicant">';

echo '<div id="contact_applicant_msg_top" style="color:#398ab9"></div>';

echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';

echo '<input type="hidden" name="process_contact" id="process_contact" value="Y">';

//Correspondence Package Information
include IRECRUIT_DIR . 'applicants/GetCorrespondencePackageInfo.inc';

if ($multipleapps) {
	echo '<input type="hidden" name="multi" value="Y">';
	echo '<input type="hidden" name="multipleapps" value="'.$multipleapps.'">';
} else {
	echo '<input type="hidden" name="ApplicationID" value="'.$ApplicationID.'">';
	echo '<input type="hidden" name="RequestID" value="'.$RequestID.'">';
}

echo '<tr><td valign="top" width="70" align="right">Subject:</td><td width="670">';
echo '<input type="text" name="subject" id="subject" value="'.$subject.'" size="40" class="form-control width-auto-inline">';
echo '</td>';
echo '</tr>';

//Set where users email
$where_users_email  =   array("OrgID = :OrgID", "UserID = :UserID");
//Set parameters
$params_users_email =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
//Get UsersInformation
$results            =   G::Obj('IrecruitUsers')->getUserInformation("UserID, EmailAddress, FirstName, LastName", $where_users_email, "", array($params_users_email));
$USERS              =   $results['results'][0]; 

echo '<tr><td valign="top" align="right">From:</td><td>';

$options_list["NoReply@iRecruit-us.com"] = 'iRecruit NoReply - NoReply@iRecruit-us.com';

//Get Email and Email Verified
$OE         =   G::Obj('OrganizationDetails')->getEmailAndEmailVerified($OrgID, $MultiOrgID);
//Get OrganizationName
$OrgName    =   G::Obj('OrganizationDetails')->getOrganizationName($OrgID, $MultiOrgID);

echo '<select name="From" id="From" class="form-control width-auto-inline">';

if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
	$options_list[$OE ['Email']] = $OrgName." - ".$OE ['Email'];
}

$options_list[$USERS ['EmailAddress']] = $USERS ['FirstName'] . ' ' . $USERS ['LastName'] . ' - ' . $USERS ['EmailAddress'];

if($pkg_info['FromEmail'] == "") $pkg_info['FromEmail'] = $USERS ['EmailAddress'];
foreach ($options_list as $option_key=>$option_value) {
    $opt_selected = ($option_key == $pkg_info['FromEmail']) ? ' selected="selected"' : '';
    echo '<option value="'.$option_key.'" '.$opt_selected.'>'.$option_value.'</option>';
}

echo '</select>';
echo '</td></tr>';

//Get UserInformation
$user_where     =   array("OrgID = :OrgID");
//Get UserParameters
$user_params    =   array(":OrgID"=>$OrgID);
//Get EmailAddress Information
$results        =   G::Obj('IrecruitUsers')->getUserInformation("LastName, FirstName, EmailAddress", $user_where, "EmailAddress", array($user_params));

if ($results['count'] > 0) {
	echo '<tr><td valign="top" align="right">BCC:</td><td>';
	echo '<select name="BCC[]" id="BCC" class="form-control width-auto-inline" multiple="multiple">';
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $USERS) {
			echo '<option value="'. $USERS ['EmailAddress'] . '"';
			if ($USERID == $USERS ['UserID']) {
				echo ' selected';
			}
			echo '>'. $USERS ['FirstName'] . ' ' . $USERS ['LastName'] . ' - ' . $USERS ['EmailAddress'] . '</option>';
		} // end foreach
	}
	
	echo '</select>';
	echo '</td></tr>';
} // end rows > 0

echo '<tr>';
echo '<td valign="top" align="right">Body:</td>';
echo '<td>';
echo '<textarea name="body" id="body" cols="75" rows="15" wrap="virtual" class="mceEditor">';
echo $body;
echo '</textarea>';
echo '</td>';
echo '</tr>';
echo '</td></tr>';

include IRECRUIT_VIEWS . 'applicants/StatusFormNoComment.inc';

echo '</table>';
echo '</div>';

$action     =   "contactapplicant";
$contact    =   "Y";
include IRECRUIT_VIEWS . 'correspondence/SelectPackage.inc';

echo '</table>';
echo '</div>';

echo '</form>';
?>
<script>
$('#frmContactApplicant').bind('form-pre-serialize', function(e) {
    tinyMCE.triggerSave();
});
</script>
