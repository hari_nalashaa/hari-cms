<?php
//Get Twilio Account Information
$account_info 			=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

//Get applicant permission
$cell_phone_permission	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphonepermission');

//Get twilio numbers list
$account_phone_numbers	=	G::Obj('TwilioPhoneNumbersApi')->getAccountIncomingNumbers($OrgID);
$twilio_numbers_list	=	$account_phone_numbers['Response'];

//Get twilio number
$user_details			=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);

//Add plus one before number
if(substr($user_details['Cell'], 0, 2) != "+1") {
	$user_cell_number	=	"+1".$user_cell_number;
}

//Get applicant cellphone
$cell_phone_info		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphone');
$cell_phone				=	json_decode($cell_phone_info, true);
$applicant_number		=	"+1".$cell_phone[0].$cell_phone[1].$cell_phone[2];

//Get user conversation resources
$usr_conv_resources		=	G::Obj('TwilioConversationInfo')->getUserConversationResources($OrgID, $USERID, $applicant_number, $user_cell_number);
$usr_conv_count			=	count($usr_conv_resources);

$resource_id			=	'';

//Get ResourceID based on applicant phone number
if($usr_conv_count > 0) {
	$resource_id			=	$usr_conv_resources[0]['ResourceID'];

	//Overwrite the twilio numbers based on conversation
	$usr_conv_mobile_nums	=	json_decode($usr_conv_resources[0]['MobileNumbers'], true);
	$twilio_number_a		=	$usr_conv_mobile_nums[0][0];
	$twilio_number_u		=	$usr_conv_mobile_nums[1][0];
}
else {
	//Get Available Numbers
	$available_numbers		=	G::Obj('TwilioConversationInfo')->getAvailableProxyNumberPair($OrgID, $twilio_numbers_list, $applicant_number, $user_cell_number);
	$twilio_number_a		=	$available_numbers['ApplicantProxy'];
	$twilio_number_u		=	$available_numbers['UserProxy'];
}

###################################
//Check weather the phone number is sms capable or not
$applicant_number_sms	=	"false";
$user_cell_number_sms	=	"false";

$lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $user_cell_number);

//Get the Protected properties from SMS object, by extending the object through ReflectionClass
$reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
$property       		=   $reflection->getProperty("properties");
$property->setAccessible(true);
$properties				=   $property->getValue($lookup_phone_num_info['Response']);

if($properties['carrier']['type'] == 'mobile'
	|| $properties['carrier']['type'] == 'voip') {
	$user_cell_number_sms	=	"true";
}


$lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $applicant_number);

//Get the Protected properties from SMS object, by extending the object through ReflectionClass
$reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
$property       		=   $reflection->getProperty("properties");
$property->setAccessible(true);
$properties				=   $property->getValue($lookup_phone_num_info['Response']);

if($properties['carrier']['type'] == 'mobile'
	|| $properties['carrier']['type'] == 'voip') {
	$applicant_number_sms	=	"true";
}
####################################

if($applicant_number_sms == "false"
	|| $user_cell_number_sms == "false") {

	$new_conversation		=	false;
	
	if($usr_conv_count > 0) {
		$resource_id		=	$usr_conv_resources[0]['ResourceID'];
	}
	else {
		//Create new conversation
	    $new_resource		=	G::Obj('TwilioConversationApi')->createConversationResource($OrgID);
		$resource_res		=	$new_resource['Response'];
		$resource_id		=	$resource_res->sid;
	
		if($new_resource['Code'] == 'Success' && $resource_id != "") {
			$new_conversation	=	true;
		}
	
		$numbers_info[NID1]		=	$twilio_number_u;
		$numbers_info[NID2]		=	$user_cell_number;

		//Insert conversation information
		G::Obj('TwilioConversationInfo')->insConversationResourceInfo($OrgID, $USERID, $resource_id, $numbers_info);
		
		$user_info				=	array("First"=>$user_details["FirstName"], "Last"=>$user_details["LastName"]);
		
		//Add user phone number to this conversation
		$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $user_info);
		
		if($participant_response['Code'] == "Failed") {
		
			$used_proxys			=	G::Obj('TwilioConversationInfo')->getProxyNumbersOfPersonalNumber($applicant_number);
			$applicant_proxy_nums	=	array_diff($twilio_numbers_list, $used_proxys);
			$applicant_proxy_nums	=	array_values($applicant_proxy_nums);
			$twilio_number_u		=	$applicant_proxy_nums[0];
		
			//Available Phone Numbers
			$phone_numbers_response		=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
			$available_phone_numbers	=	$phone_numbers_response['Response'];
		
			//Purchase number
			if(is_array($available_phone_numbers) && count($available_phone_numbers) > 0) {
		
				$msg_service_id			=	$account_info['MessageServiceID'];
				$phone_numbers_result	=	G::Obj('TwilioPhoneNumbersApi')->getPhoneNumbersCountByMsgServiceID($OrgID, $msg_service_id);
				$phone_numbers_count	=	$phone_numbers_result['PhoneNumbersCount'];
		
				if($phone_numbers_count >= 400) {
					$message_service_info	=	G::Obj('TwilioMessagingServiceApi')->createMessageService($OrgID, G::Obj('GenericLibrary')->getGuIDBySalt($OrgID));
				}
		
				$available_phone_number	=	$available_phone_numbers[0];
				$new_num_response		=	G::Obj('TwilioPhoneNumbersApi')->createIncomingPhoneNumber($OrgID, $available_phone_number);
				$number_response		=	$new_num_response['Response'];
		
				if($number_response->sid != "") {
					$phone_number_res_info	=	G::Obj('TwilioMessagingServiceApi')->createPhoneNumberResource($OrgID, $account_info['MessageServiceID'], $number_response->sid);
					$twilio_number_u		=	$available_phone_number;
				}
		
			}
		
			$numbers_info[NID1]		=	$twilio_number_u;
			$numbers_info[NID2]		=	$applicant_number;
		
			$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $app_info);
		}
		
	}
	
	//Send sms
	if($new_conversation == true
		&& $resource_id != "") {
		
		$numbers_info[NID1]	=	$twilio_number_a;
		$numbers_info[NID2]	=	$applicant_number;
		
		//Get Applicant First Name, Last Name Information
		$app_info				=	array();
		$app_info["First"]		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'first');
		$app_info["Last"]		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'last');
		
		//Add applicant phone number to this conversation
		$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $app_info);
		
		if($participant_response['Code'] == "Failed") {
		
			$used_proxys			=	G::Obj('TwilioConversationInfo')->getProxyNumbersOfPersonalNumber($applicant_number);
			$applicant_proxy_nums	=	array_diff($twilio_numbers_list, $used_proxys);
			$applicant_proxy_nums	=	array_values($applicant_proxy_nums);
			$twilio_number_a		=	$applicant_proxy_nums[0];
		
			//Available Phone Numbers
			$phone_numbers_response		=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
			$available_phone_numbers	=	$phone_numbers_response['Response'];
		
			//Purchase number
			if(is_array($available_phone_numbers) && count($available_phone_numbers) > 0) {
		
				$msg_service_id			=	$account_info['MessageServiceID'];
				$phone_numbers_result	=	G::Obj('TwilioPhoneNumbersApi')->getPhoneNumbersCountByMsgServiceID($OrgID, $msg_service_id);
				$phone_numbers_count	=	$phone_numbers_result['PhoneNumbersCount'];
		
				if($phone_numbers_count >= 400) {
					$message_service_info	=	G::Obj('TwilioMessagingServiceApi')->createMessageService($OrgID, G::Obj('GenericLibrary')->getGuIDBySalt($OrgID));
				}
		
				$available_phone_number	=	$available_phone_numbers[0];
				$new_num_response		=	G::Obj('TwilioPhoneNumbersApi')->createIncomingPhoneNumber($OrgID, $available_phone_number);
				$number_response		=	$new_num_response['Response'];
		
				if($number_response->sid != "") {
					$phone_number_res_info	=	G::Obj('TwilioMessagingServiceApi')->createPhoneNumberResource($OrgID, $account_info['MessageServiceID'], $number_response->sid);
					$twilio_number_a		=	$available_phone_number;
				}
			}
		
			$numbers_info[NID1]		=	$twilio_number_a;
			$numbers_info[NID2]		=	$applicant_number;
		
			$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $app_info);
				
		}
		

		if($participant_response['Code'] ==	'Success') {
			//Update Conversation Resource Info
			G::Obj('TwilioConversationInfo')->updConversationResourceInfo($OrgID, $USERID, $resource_id, $numbers_info);
		}
		
	}

	//Send conversation message
	if($resource_id != "") {
		$conversation_msg_res	=	G::Obj('TwilioConversationApi')->createConversationMessage($OrgID, $resource_id, $twilio_number_u, $message);
	}
	else {
		$_SESSION['FaildApplicants'][$ApplicationID]	=	$app_info["Last"]. ' ' .$app_info["First"];
	}
}
else {
	//Get Applicant First Name, Last Name Information
	$app_info				=	array();
	$app_info["First"]		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'first');
	$app_info["Last"]		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'last');
	
	$_SESSION['FaildApplicants'][$ApplicationID]	=	$app_info["Last"]. ' ' .$app_info["First"];
}

?>