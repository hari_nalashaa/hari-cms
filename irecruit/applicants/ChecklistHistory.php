<?php
require_once '../Configuration.inc';
?>
<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover">
<tr><td colspan="5" bgcolor="#eeeeee"><b>Checklist History</b></td></tr>
<tr>
<td colspan="5">
	<div id="del_comment_status" class="alert alert-success" style="margin-bottom:0px;display:none;"></div>
</td>
</tr>
<tr>
<td width="100"><b>Updated By</b></td>
<td width="80"><b>Date</b></td>
<td><b>Comments</b></td>
</tr>
<?php 
//echo "<pre>";print_r($_GET);print_r($_REQUEST);echo "</pre>";
$OrgID = $_GET['OrgID'];
$ApplicationID = $_GET['ApplicationID'];
$RequestID = $_GET['RequestID'];
$ChecklistID = $_GET['ChecklistID'];

$get_checklist_data =   G::Obj('ChecklistData')->getCheckListHistory($OrgID, $ApplicationID, $RequestID);
$get_checklist_info = $get_checklist_data['results'];

$checklist_items     =   array();
$checklist_settings =   G::Obj('Checklist')->getCheckList($ChecklistID, $OrgID);
$checklist_items     =   json_decode($checklist_settings['Checklist'], true);

foreach($get_checklist_info AS $checklist_data){  ?>
    <tr>
    	<td>
    		<?php 
    		$updated_by_user_avatar_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($checklist_data[UserID], 'DashboardAvatar');
    		$user_avatar = IRECRUIT_HOME . "vault/".$OrgID."/".$checklist_data[UserID]."/profile_avatars/".$updated_by_user_avatar_info['DashboardAvatar'];
    		if($ServerInformationObj->validateUrlContent($user_avatar)) {
    		    ?><img src="<?php echo $user_avatar;?>" width="50" height="50">&nbsp;<?php echo G::Obj('IrecruitUsers')->getUsersName($OrgID, $checklist_data[UserID]);?><?php
    		}
    		else {
    		    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50">&nbsp;<?php echo G::Obj('IrecruitUsers')->getUsersName($OrgID, $checklist_data[UserID]);?><?php
    		}
		?>
    	</td>
    	<td>
    		<?php echo date("m/d/Y g:i A",strtotime($checklist_data[Date]));?>
    	</td>
    	<td>
    	<?php 
    	foreach($checklist_items AS $checklist_item){
    	    if($checklist_item['passed_value'] == $checklist_data[Checklist]){
    	        echo "<b>".$checklist_item[ChecklistItem]."</b><br />";
    	    }
    	}
    	echo $checklist_data[Comments];
    	?>
    	</td>
    </tr>
<?php }
?>
</table>


