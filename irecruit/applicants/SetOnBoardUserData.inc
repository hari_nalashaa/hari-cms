<?php 
//Set Update Information For JobApplications Information
$set_info   =   array("LastModified = NOW()");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);

if(isset($EmployeeDataHRP['employee_id']) && $EmployeeDataHRP['employee_id'] != "") {
	$set_info[] = "CloudEmployeeID = :CloudEmployeeID";
	$set_info[] = "CloudCompanyID = :CloudCompanyID";
	$set_info[] = "CloudStatus = '1'";
	$params[":CloudEmployeeID"] = $EmployeeDataHRP['employee_id'];
	$params[":CloudCompanyID"] = $_POST['ddlCloudCompaniesList'];
}
else if(isset($matrixcaregiver_info['id']) && $matrixcaregiver_info['id'] != "") {
    $set_info[] = "MatrixCaregiverID = :MatrixCaregiverID";
    $set_info[] = "MatrixCareCompanyID = :MatrixCareCompanyID";
    $set_info[] = "MatrixCaregiverStatus = '1'";
    $params[":MatrixCaregiverID"] = $matrixcaregiver_info['id'];
    $params[":MatrixCareCompanyID"] = $_POST['ddlMatrixCareCompaniesList'];
}

//Set where condition
$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
//Update JobApplications Information
$ApplicationsObj->updApplicationsInfo('JobApplications', $set_info, $where, array($params));

//Insert JobApplication History
$job_app_history = array (
		"OrgID"               =>  $OrgID,
		"ApplicationID"       =>  $ApplicationID,
		"RequestID"           =>  $RequestID,
		"ProcessOrder"        =>  "-2",
		"Date"                =>  "NOW()",
		"UserID"              =>  $USERID,
		"StatusEffectiveDate" =>  "NOW()",
		"Comments"            =>  $Comment
);

G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
?>
