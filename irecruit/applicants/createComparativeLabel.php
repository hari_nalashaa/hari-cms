<?php 
require_once '../Configuration.inc';

if($_REQUEST['LabelName'] != "") {
    //Random Label
    $LabelID    =   uniqid($OrgID."-".$USERID."-".time(), true);
    $LabelID    =   str_replace(".", "", $LabelID);
    $ComparativeAnalysisObj->insComparativeLabels($OrgID, $USERID, $LabelID, $_REQUEST['LabelName']);
}

$comparative_label_results  =   $ComparativeAnalysisObj->getComparativeLabels($OrgID, $USERID);
$comparative_labels_list    =   $comparative_label_results['results'];

echo json_encode($comparative_labels_list);
?>