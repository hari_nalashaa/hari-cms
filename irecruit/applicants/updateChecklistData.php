<?php 
require_once '../Configuration.inc';

$Checklist  =   $_REQUEST['Checklist'];
$StatusCategory      =   $_REQUEST['StatusCategory'];
$Comments    =   $_REQUEST['Comments'];
$login_user    =   $USERID;
$ApplicationID  =   $_REQUEST['ApplicationID'];
$RequestID      =   $_REQUEST['RequestID'];
$ChecklistID    =   $_REQUEST['ChecklistID'];
$ChecklistName   =   $_REQUEST['ChecklistName'];
$OrgID    =   $_REQUEST['OrgID'];
$get_checklist_data =   G::Obj('ChecklistData')->getCheckListDataByChecklistID($OrgID, $ApplicationID, $RequestID, $ChecklistID,$Checklist);

if($StatusCategory != $get_checklist_data[StatusCategory] && $Comments != $get_checklist_data[Comments]){
    if($get_checklist_data){
        $updated_comments = "Status was updated from ".$get_checklist_data[StatusCategory]." to ".$StatusCategory.". Comments were updated from ".$get_checklist_data[Comments]." to ".$Comments;
    }else{
        $updated_comments = "Status was updated to ".$StatusCategory.". Comments were updated to ".$Comments;
    }   
}else if($StatusCategory != $get_checklist_data[StatusCategory]){
    if($get_checklist_data){
        $updated_comments = "Status was updated from ".$get_checklist_data[StatusCategory]." to ".$StatusCategory;
    }else{
        $updated_comments = "Status was updated to ".$StatusCategory;
    }
}else if($Comments != $get_checklist_data[Comments]){
    if($get_checklist_data){
        $updated_comments = "Comments were updated from ".$get_checklist_data[Comments]." to ".$Comments;
    }else{
        $updated_comments = "Comments were updated to ".$Comments;
    }   
}

if($get_checklist_data['Checklist']){
    $now            = date('Y-m-d H:i:s');
    $set_info       =   array("StatusCategory = :StatusCategory","Comments = :Comments","CreatedBy = :CreatedBy","LastUpdated = :LastUpdated");
    $where_info     =   array("ChecklistID = :ChecklistID","OrgID = :OrgID","ApplicationID = :ApplicationID","RequestID = :RequestID","Checklist = :Checklist",);
    $params         =   array(":StatusCategory"=>$StatusCategory, ":Comments"=>$Comments, ":LastUpdated"=>$now, ":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":Checklist"=>$Checklist,":CreatedBy"=>$login_user);
    
    //Update Checklist Data
    G::Obj('ChecklistData')->updChecklistData($set_info, $where_info, array($params));
    
    //Insert Checklist Data History
    $insert_checklist_data_history = $new_checklist_history_info =   G::Obj('ChecklistData')->insChecklistDataHistory($ChecklistID, $OrgID, $ApplicationID, $RequestID, $updated_comments, $login_user,$Checklist);
    //print_r($insert_checklist_data_history);
}else{
    //Insert Checklist Data
    $insert_checklist_data = G::Obj('ChecklistData')->insChecklistData($ChecklistID, $OrgID, $ApplicationID, $RequestID, $Checklist, $StatusCategory, $Comments,$login_user);
    
    //Insert Checklist Data History
    $insert_checklist_data_history = $new_checklist_history_info =   G::Obj('ChecklistData')->insChecklistDataHistory($ChecklistID, $OrgID, $ApplicationID, $RequestID, $updated_comments, $login_user,$Checklist);
}

$get_new_checklist_data =   G::Obj('ChecklistData')->getCheckListDataByChecklistID($OrgID, $ApplicationID, $RequestID, $ChecklistID,$Checklist);
$checklist_data_arr['LastUpdated'] = date("m/d/Y",strtotime($get_new_checklist_data[LastUpdated]));
$checklist_data_arr['CreatedBy'] = G::Obj('IrecruitUsers')->getUsersName($OrgID, $get_checklist_data[CreatedBy]);
$get_new_checklist_json = json_encode($checklist_data_arr);

echo $get_new_checklist_json;

?>
