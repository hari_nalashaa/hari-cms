<?php 
require_once '../Configuration.inc';

$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$_REQUEST['ApplicationID'], ":RequestID"=>$_REQUEST['RequestID'], ":Notes"=>$_REQUEST['Notes']);
$where_info = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
$set_info = array("Notes = :Notes");
$results = $ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));

echo $results['affected_rows'];
?>