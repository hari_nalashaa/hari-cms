<?php 
require_once '../Configuration.inc';

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
    $TemplateObj->goto = $_SERVER ['QUERY_STRING'];
}

// Set page title
$TemplateObj->title		=	"Comparative Analysis Configuation";

$config_columns_list	=	array(
							    "DispositionCode"                   => "Disposition Code",  
							    "ProcessOrder"                      => "Status",
							    "EntryDate"                         => "Entry Date",
							    "RequestID"                         => "Requisition Title",
							    "Rating"                            => "Rating",
							    "ApplicantConsiderationStatus1"     => "Thumbs Up / Down",
							    "ApplicantConsiderationStatus2"     => "Label"
							);

//Get Wotc Ids list
$TemplateObj->wotcid_info =	$wotcid_info = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, "");

//Get weighted searches list information
$weight_search_names    =	array();
$weighted_searches_keys =	array();
$weighted_searches_list =	G::Obj('WeightedSearch')->getWeightedSearchNameValues($OrgID, $USERID);
if(is_array($weighted_searches_list)) $weighted_searches_keys = array_keys($weighted_searches_list);

if($wotcid_info['wotcID'] != "") {
	$weighted_searches_keys[]	=	'wotc_application_status';	//first value of wotc application status
}

for($apq = 0; $apq < count($app_sum_que); $apq++) {
    $app_sum_que_list[$app_sum_que[$apq]['QuestionID']] = $app_sum_que[$apq]['Question'];
}
 
if(is_array($app_sum_que_list)) $app_sum_que_keys_list = array_keys($app_sum_que_list);

//Get comparative analysis configuration information
$comp_analysis_config_info = $ComparativeAnalysisObj->getComparativeAnalysisConfig($OrgID, $USERID);
$config_info = json_decode($comp_analysis_config_info['ColumnsList'], true);

$columns_list['JobApplicationsColumnsList']             = array();
$columns_list['WeightedSearchColumnsList']              = array();
$columns_list['ComparativeAnalysisQuestionColumnsList'] = array();

if(count($config_info['JobApplicationsColumnsList']) == 0) {
    $columns_list['JobApplicationsColumnsList'] = array("ProcessOrder", "DispositionCode", "EntryDate", "RequestID", "Rating", "ApplicantConsiderationStatus1", "ApplicantConsiderationStatus2");
    $columns_list['WeightedSearchColumnsList'] = array();
    $columns_list['ComparativeAnalysisQuestionColumnsList'] = array();

    
    //Insert or update comparative analysis configuration
    $ComparativeAnalysisObj->insUpdComparativeAnalysisConfig($OrgID, $USERID, $columns_list);
}
else if(count($config_info['JobApplicationsColumnsList']) > 0 && count($_POST) == 0) {
    //Code to update the weighted search columns, if the weighted search is deleted by the user
    $columns_list['JobApplicationsColumnsList'] = $config_info['JobApplicationsColumnsList'];

    $weighted_search_columns_filtered_list = array();

    if(is_array($config_info['WeightedSearchColumnsList'])) {
        foreach($config_info['WeightedSearchColumnsList'] as $column_value) {
            if(in_array($column_value, $weighted_searches_keys)) {
                $weighted_search_columns_filtered_list[] = $column_value;
            }
        }
        
        $columns_list['WeightedSearchColumnsList'] = $weighted_search_columns_filtered_list;
        
        if(isset($config_info['WeightedSearchColumnsListSum'])) {
            $columns_list['WeightedSearchColumnsListSum'] = $config_info['WeightedSearchColumnsListSum'];
        }
    }
    
    //if(is_array($config_info['ApplicantSummaryQuestionColumnsList'])) {
    if(is_array($config_info['ComparativeAnalysisQuestionColumnsList'])) {

        $app_summary_que_columns_filtered_list = array();
        
        foreach($config_info['ComparativeAnalysisQuestionColumnsList'] as $column_value) {

            //if(!in_array($column_value, $app_sum_que_keys_list)) {
                $app_summary_que_columns_filtered_list[] = $column_value;
            //}
        }
    
        $columns_list['ComparativeAnalysisQuestionColumnsList'] = $app_summary_que_columns_filtered_list;
    }
    
    if(isset($config_info['ApplicantDistance'])) {
        $columns_list['ApplicantDistance'] = $config_info['ApplicantDistance'];
    }
   
    //Insert or update comparative analysis configuration
    G::Obj('ComparativeAnalysis')->insUpdComparativeAnalysisConfig($OrgID, $USERID, $columns_list);
}

$where_info = array("OrgID = :OrgID", "UserID = :UserID");
$params_info = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
$weighted_searches_results = G::Obj('WeightedSearch')->getWeightSaveSearchInfo("FormID, SaveSearchName, WeightedSearchRandId", $where_info, "WeightedSearchRandId", "", array($params_info));
$weighted_searches_info = $weighted_searches_results['results'];

$ws_frm_types = array();
for($wsi = 0; $wsi < count($weighted_searches_info); $wsi++) {
    $ws_frm_types[$weighted_searches_info[$wsi]['WeightedSearchRandId']] = $weighted_searches_info[$wsi]['FormID'];
}

if(isset($_POST['btnUpdateConfig']) && $_POST['btnUpdateConfig'] == "Update") { 
      $total_columns_count = 0;
    if(count($_POST['job_applications_columns']) > 0) {
        $ins_upd_columns_list['JobApplicationsColumnsList'] = $_POST['job_applications_columns'];
    }
    if(count($_POST['weighted_search_columns']) > 0) {
        $ins_upd_columns_list['WeightedSearchColumnsList'] = $_POST['weighted_search_columns'];
    }
    if(isset($_POST['weighted_search_columns_sum'])) {
        $ins_upd_columns_list['WeightedSearchColumnsListSum'] = $_POST['weighted_search_columns_sum'];
        $total_columns_count += 1;
    }
    if(isset($_POST['applicant_distance'])) {
        $ins_upd_columns_list['ApplicantDistance'] = $_POST['applicant_distance'];
        $total_columns_count += 1;
    }

     if(isset($_POST['comparative_analysis_columns'])) {
        $ins_upd_columns_list['ComparativeAnalysisQuestionColumnsList'] = $_POST['comparative_analysis_columns'];
     }
         
    //Insert or update comparative analysis configuration
    G::Obj('ComparativeAnalysis')->insUpdComparativeAnalysisConfig($OrgID, $USERID, $ins_upd_columns_list);

    //header("Location:comparativeAnalysisConfig.php?menu=10&msg=colsmax");
    header("Location:comparativeAnalysisConfig.php?menu=10&msg=suc");
    exit();
}

//Get comparative analysis configuration information
$comp_analysis_config_info = G::Obj('ComparativeAnalysis')->getComparativeAnalysisConfig($OrgID, $USERID);
$config_info = json_decode($comp_analysis_config_info['ColumnsList'], true);

//Get comparative analysis configuration list
$where_info  = array("OrgID = :OrgID");
$params_info = array(":OrgID"=>$OrgID);
$comp_analysis_comp_const_list = G::Obj('Applicants')->getComparativeAnalysisQuestionsInfo("*", $where_info, '', '', array($params_info));  
$comp_const_list = $comp_analysis_comp_const_list['results'];

$TemplateObj->weighted_searches_list    =   $weighted_searches_list;
$TemplateObj->config_columns_list       =   $config_columns_list;
$TemplateObj->config_info               =   $config_info;
$TemplateObj->columns_list              =   $columns_list;
$TemplateObj->weighted_searches_info    =   $weighted_searches_info;
$TemplateObj->ws_frm_types              =   $ws_frm_types;
$TemplateObj->comp_const_list           =   $comp_const_list;

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/ComparativeAnalysisConfig' );
?>