<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title = $title = 'iRecruit - Application View';

if ($permit ['Applicants'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/ApplicationView' );
?>