<?php 
require_once '../Configuration.inc';

if($_REQUEST['InternalApplicationStatus'] == "No") {
    $IsInternalApplication  =   "Yes";
}
else if($_REQUEST['InternalApplicationStatus'] == "Yes")
{
    $IsInternalApplication  =   "No";
}

$set_info       =   array("IsInternalApplication = :IsInternalApplication");
$where_info     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
$params         =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$_REQUEST['ApplicationID'], ":RequestID"=>$_REQUEST['RequestID'], ":IsInternalApplication"=>$IsInternalApplication);
G::Obj('Applications')->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));

echo $IsInternalApplication;
?>