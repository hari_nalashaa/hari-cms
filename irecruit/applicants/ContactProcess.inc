<?php
// *** FUNCTIONS *** //
function deliverEmail($OrgID, $ApplicationID, $RequestID, $subject, $body, $attachments, $USERID) {
	global $feature;
	
	$original_body = $body;
	$companyattachments = '';
	if (is_array ( $attachments )) {
	    while ( list ( $key, $val ) = each ( $attachments ) ) {
	        $companyattachments .= $val . ':';
		}
	}
	
	$companyattachmentslist = '';
	$cp    =   explode ( ':', $companyattachments );
	$cpcnt =   count ( $cp ) - 1;
	
	for($i = 0; $i < $cpcnt; $i ++) {
	    $companyattachmentslist .= '<li>' . getAttachmentName ( $OrgID, $cp [$i] );
	}
	
	$short_code_ques = array('address','address2','city','state','country','zip','first','middle','last');
	
	// swap out data in brackets //Set where condition
	$where_info = array (
			"OrgID = :OrgID",
			"ApplicationID = :ApplicationID",
			"QuestionID IN ('address','address2','city','state','country','zip','first','middle','last')" 
	);
	// Set parameters information
	$params_info = array (
			":OrgID"         =>  $OrgID,
			":ApplicationID" =>  $ApplicationID 
	);
	// Get Applicant Data Information
	$results = G::Obj('Applicants')->getApplicantDataInfo ( "*", $where_info, '', '', array (
			$params_info 
	) );
	
	$app_data_ques = array();
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $ADR ) {
			$app_data_ques[] = $ADR ['QuestionID'];
			$body = preg_replace ( "/\{" . $ADR ['QuestionID'] . "\}/i", $ADR ['Answer'], $body );
		}
	}
	
	
	$final_app_data = array_diff($short_code_ques, $app_data_ques);
	
	if(is_array($final_app_data)) {
		foreach($final_app_data as $que_short_code) {
			$body = preg_replace ( "/{".$que_short_code."}/i", '', $body );
		}	
	}
	
	$body              =   preg_replace ( "/\{ApplicationID\}/i", $ApplicationID, $body );
	
	$multiorgid_req    =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
	$JobTitle          =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
	$ApplicationDate   =   G::Obj('Applications')->getApplicationDate ( $OrgID, $ApplicationID );
	$body              =   preg_replace ( "/\{JobTitle\}/i", $JobTitle, $body );
	$body              =   preg_replace ( "/\{ApplicationDate\}/i", $ApplicationDate, $body );
	
	$applicantname     =   G::Obj('ApplicantDetails')->getApplicantName ( $OrgID, $ApplicationID );
	$applicantemail    =   G::Obj('ApplicantDetails')->getAnswer ( $OrgID, $ApplicationID, 'email' );

	$sent  = '<table>';
    $sent .= '<tr><td>';
    $sent .= 'Email:<br>';
    $sent .= '<pre>';
    $sent .= 'Subject: '.$subject.'<br>';
    $sent .=  $body.'<br>';
    $sent .= '</pre>';
    $sent .= 'Attachments:<br><ul>'.$companyattachmentslist.'</ul>';
    $sent .= '</td></tr>';
    $sent .= '</table>';
	
	if ($applicantemail) {
		include IRECRUIT_DIR . 'applicants/ContactEmail.inc';
		
		// Insert into ApplicantHistory
		$Comments = $sent;
		
		// Job Application History
		$job_app_history = array (
				"OrgID"                 =>  $OrgID,
				"ApplicationID"         =>  $ApplicationID,
				"RequestID"             =>  $RequestID,
				"ProcessOrder"          =>  "-4",
				"StatusEffectiveDate"   =>  "NOW()",
				"Date"                  =>  "NOW()",
				"UserID"                =>  $USERID,
				"Comments"              =>  $Comments,
                "OrginalMessage"        =>  $original_body
		);
		// Insert JobApplication History
		G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
		
		// Set Update JobApplications
		$set_upd_jobapp = array (
				"Correspondence         =   Correspondence+1" 
		);
		// Set Where JobApplications
		$where_upd_jobapp = array (
				"OrgID                  =   :OrgID",
				"ApplicationID          =   :ApplicationID",
				"RequestID              =   :RequestID" 
		);
		// Set Params
		$params_upd_jobapp = array (
				":OrgID"                =>  $OrgID,
				":ApplicationID"        =>  $ApplicationID,
				":RequestID"            =>  $RequestID 
		);
		// Update JobApplications Information
		G::Obj('Applications')->updApplicationsInfo ( 'JobApplications', $set_upd_jobapp, $where_upd_jobapp, array ($params_upd_jobapp) );
		
		echo '<li>' . $applicantname . ' - ' . $applicantemail . '<br>';
	} // end applicantemail
} // end function

function updateStatus($OrgID, $ApplicationID, $RequestID, $ProcessOrder, $DispositionCode, $USERID) {
	global $feature;
	
	if ($ProcessOrder) {
		$Comment      =   "Status updated with correspondence.";
		
		$StatusDate   =   G::Obj('MysqlHelper')->getCurDate ();
		
		// Job Application History
		$job_app_history = array (
				"OrgID"                 =>  $OrgID,
				"ApplicationID"         =>  $ApplicationID,
				"RequestID"             =>  $RequestID,
				"ProcessOrder"          =>  $ProcessOrder,
				"DispositionCode"       =>  $DispositionCode,
				"StatusEffectiveDate"   =>  $StatusDate,
				"Date"                  =>  "NOW()",
				"UserID"                =>  $USERID,
				"Comments"              =>  $Comment 
		);
		// Insert JobApplication History
		G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
		
		// Applicant Status Logs
		G::Obj('ApplicantStatusLogs')->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, $ProcessOrder, $DispositionCode);
		
		// Set where condition
		$where = array (
				"OrgID                  =   :OrgID",
				"ProcessOrder           =   :ProcessOrder"
		);
		// Set parameters
		$params = array (
				":OrgID"                =>  $OrgID,
				":ProcessOrder"         =>  $ProcessOrder 
		);
		// Get Applicant Process FlowInformation
		$results = G::Obj('Applicants')->getApplicantProcessFlowInfo ( "Searchable", $where, '', array ($params ) );
		$APF = $results ['results'] [0];
		
		
		//Set Information
		$set_info = array (
				"Searchable             =   :Searchable",
				"DispositionCode        =   :DispositionCode",
				"ProcessOrder           =   :ProcessOrder",
				"StatusEffectiveDate    =   :StatusEffectiveDate",
				"LastModified           =   NOW()" 
		);
		//Set where condition
		$where_info = array (
				"OrgID                  =   :OrgID",
				"RequestID              =   :RequestID",
				"ApplicationID          =   :ApplicationID" 
		);
		//Set parameters
		$params_info = array (
				":OrgID"                =>  $OrgID,
				":RequestID"            =>  $RequestID,
				":ApplicationID"        =>  $ApplicationID,
				":Searchable"           =>  $APF ['Searchable'],
				":DispositionCode"      =>  $DispositionCode,
				":ProcessOrder"         =>  $ProcessOrder,
				":StatusEffectiveDate"  =>  $StatusDate 
		);
		//Update Applications Information
		G::Obj('Applications')->updApplicationsInfo ( 'JobApplications', $set_info, $where_info, array ($params_info) );		
		
		$paperless_info = G::Obj('Applications')->getJobApplicationsDetailInfo("Paperless", $OrgID, $ApplicationID, $RequestID);
		
		if($paperless_info['Paperless'] == "No") {
			include IRECRUIT_DIR . 'formsInternal/AssignForm.inc';
		}
	} // end ProcessOrder
} // end function

function getAttachmentName($OrgID, $UpdateID) {

	//Set where condition
	$where     =   array("OrgID = :OrgID", "UpdateID = :UpdateID");
	//Set parameters
	$params    =   array(":OrgID"=>$OrgID, ":UpdateID"=>$UpdateID);
	//Get Correspondence Attachments
	
	$results   =   G::Obj('Correspondence')->getCorrespondenceAttachments('Filename', $where, '', array($params));

	$CA        =   $results['results'][0]['Filename'];

	return $CA;
} // end function
?>