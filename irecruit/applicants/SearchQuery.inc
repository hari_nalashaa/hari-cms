<?php
include IRECRUIT_DIR . 'applicants/ProcessApplicantsDropDown.inc';

include IRECRUIT_DIR . 'applicants/BuildSearchQuery.inc';

if(is_array($applicants_search_results)) {
	
	$aps_index = 0;
	foreach ($applicants_search_results as $row) {
		
		include IRECRUIT_DIR . 'applicants/BuildWeightedSearchQuery.inc';
		$applicants_search_results[$aps_index]['WeightedSearchScore'] = $total;
		$row['WeightedSearchScore'] = $total;
		####################################################################
		########## Modified Code To Sort The Weighted Search Score
		include IRECRUIT_DIR . 'applicants/ViewApplicantSearchResults.inc';
	
		########## End Of Modified Code To Sort The Weighted Search Score
		####################################################################

		
		$SEARCHhit++;
		if ($rowcolor == "#eeeeee") {
			$TemplateObj->rowcolor = $rowcolor = "#ffffff";
		} else {
			$TemplateObj->rowcolor = $rowcolor = "#eeeeee";
		}
		$aps_index++;
	} // end foreach
	
}

$TemplateObj->applicants_search_results = $applicants_search_results;
$TemplateObj->SEARCHhit = $SEARCHhit;

$SEARCHRESULTS .= $SEARCHRESULT;

if ($cnt == 500) {
	
	$SEARCHRESULTS .= '<tr>';
	$SEARCHRESULTS .= '<td colspan="100%" align="center">';
	$SEARCHRESULTS .= '<p style="color:red">Search results have been limited.</p>';
	$SEARCHRESULTS .= '</td>';
	$SEARCHRESULTS .= '</tr>';
}
$TemplateObj->SEARCHRESULTS = $SEARCHRESULTS;
?>