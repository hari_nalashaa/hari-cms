<?php
require_once '../Configuration.inc';

//Request Parameters
$RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$ApplicationID      =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$to                 =   isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
$ForwardAttachments =   isset($_REQUEST['ForwardAttachments']) ? $_REQUEST['ForwardAttachments'] : '';
$From               =   isset($_REQUEST['From']) ? $_REQUEST['From'] : '';
$EmailComments      =   isset($_REQUEST['EmailComments']) ? $_REQUEST['EmailComments'] : '';
$multipleapps       =   isset($_REQUEST['multipleapps']) ? $_REQUEST['multipleapps'] : '';
$multi              =   isset($_REQUEST['multi']) ? $_REQUEST['multi'] : '';

require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';
require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
require_once IRECRUIT_DIR . 'applicants/ApplicantsMultipleApps.inc';


if ($_REQUEST['process'] == 'Y') {

	$EmailList = '';

	foreach ( $_POST ['to'] as $key => $value ) {
		if ($value) {
			$EmailList .= $value . ', ';
		}
	}

    $cnt                =   strlen ( $EmailList );
    $EmailListOriginal  =   $EmailList = substr ( $EmailList, 0, $cnt - 2 );
    $EmailsList         =   explode(",", $EmailList);
    
	if (($multi == "Y") && ($multipleapps)) {

		$ma       =   explode ( '|', $multipleapps );
		$macnt    =   count ( $ma ) - 1;

		for($i = 0; $i < $macnt; $i ++) {
				
			$jarj            =   explode ( ':', $ma [$i] );
			$ApplicationID   =   $jarj [0];
			$RequestID       =   $jarj [1];
				
			if (($ApplicationID) && ($RequestID)) {

			    $status_index = 0;
			    for($el = 0; $el < count($EmailsList); $el++) {
			        $EmailList   =   trim($EmailsList[$el]);
			        //Have to update the $_POST
			        $Attachments =   forwardApplicant ( $OrgID, $ApplicationID, $RequestID, $EmailList, $ForwardAttachments, $EmailComments, $USERID, 'applicantforward', $status_index);
			        $status_index++;
			        $message .=  'Applicant ' . $ApplicationID . ' has been sent to ' . $EmailList."<br>";
			    }
			    
			    updateApplicantStatus ( $OrgID, $ApplicationID, $RequestID, $USERID, $EmailListOriginal, $Attachments, 'Application Forwarded' );
			} // end if
		} // end for
	} // end multiapps

	if($message != "") {
		echo "<span style='color:#398ab9'>".$message."</span>";
	}
	else {
		echo "<span style='color:red'>There is problem while forwarding these applicants. Please try again later.</span>";
	}
}
?>