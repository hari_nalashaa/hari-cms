<?php
if(!is_dir(IRECRUIT_DIR . "zips")) {
	mkdir(IRECRUIT_DIR . "zips");
	chmod(IRECRUIT_DIR . "zips", 0777);
}

//Get predefined options list
$zip_options    =   array("resumeupload", "coverletterupload", "otherupload", "custom", "appview", "apphistory", "iconnectforms");

$get_params     =   array("OrgID"=>$OrgID, "UserID"=>$USERID);
$where_params   =   array("OrgID = :OrgID", "UserID = :UserID");
$doc_option_res =   $IrecruitApplicationFeaturesObj->getDocumentsZipDownloadOptions("DocumentsList", $where_params, "", array($get_params));

if(isset($doc_option_res['results'][0]['DocumentsList'])
    && $doc_option_res['results'][0]['DocumentsList'] != "") {
	//Update with user settings
	$zip_options = json_decode($doc_option_res['results'][0]['DocumentsList'], true);
}

// Create Directories
$dir = IRECRUIT_DIR . 'zips';
if (! file_exists ( $dir )) {
    mkdir ( $dir, 0700 );
    chmod ( $dir, 0777 );
}

function downloadFile($file)
{
	if(file_exists($file)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
		@chmod($file, 0777);
		@unlink($file);
		exit();
	}

}

if(isset($_REQUEST['appid']) && is_array($_REQUEST['appid']))
{
	$zip = new ZipArchive();
	$available_to_download = false;
	
	foreach($_REQUEST['appid'] as $appid) {
		
		$appid_req_info   =   explode(":", $appid);

		$ApplicationID    =   $appid_req_info[0];
		$RequestID        =   $appid_req_info[1];
		
		$FormID           =   $ApplicantDetailsObj->getAnswer($OrgID, $ApplicationID, 'FormID');
		
		// set where condition
		$formque_where    =   array (
                                    "OrgID          =   :OrgID",
                                    "FormID         =   :FormID",
                                    "SectionID      =   6",
                                    "QuestionTypeID =   8",
                                    "Active         =   'Y'"
                                    );
        // set attachments condition
		if(!in_array("custom", $zip_options)) {
		    $formque_where[]  =  "QuestionID IN ('resumeupload', 'coverletterupload', 'otherupload')";
		}
		
		// Set parameters
		$formque_params   =   array (
                    				":OrgID"        =>  $OrgID,
                    				":FormID"       =>  $FormID
                                    );
		$resultsA         =   $FormQuestionsObj->getFormQuestionsInformation ( "QuestionID, value, Required", $formque_where, "QuestionOrder", array ($formque_params) );
		
		$type_attachments =   array();
		if (is_array ( $resultsA ['results'] )) {
			foreach ( $resultsA ['results'] as $AA ) {
			    
			    //Push the QuestionID if the custom option is checked.
			    if(in_array("custom", $zip_options)
                    &&  $AA ['QuestionID']   !=  "resumeupload"
                    &&  $AA ['QuestionID']   !=  "coverletterupload"
                    &&  $AA ['QuestionID']   !=  "otherupload") {
			        $zip_options[]   =   $AA ['QuestionID'];
			    }	    

				if(in_array($AA ['QuestionID'], $zip_options)) {

                    // Set attachment parameters
                    $attach_params          =   array (
                                                	":OrgID"             =>  $OrgID,
                                                	":ApplicationID"     =>  $ApplicationID,
                                                	":TypeAttachment"    =>  $AA ['QuestionID']
                                                );
                    // Set attachment where condition
                    $attach_where           =   array (
                                                	"OrgID               =   :OrgID",
                                                	"ApplicationID       =   :ApplicationID",
                                                	"TypeAttachment      =   :TypeAttachment"
                                                );
                    
                    $results_attachments    =   $AttachmentsObj->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );
                    $type_attachments[]     =   $results_attachments['results'][0];
					
				}
				
			}
		}

		for($t = 0; $t < count($type_attachments); $t++) {
		
			if (isset($type_attachments[$t]['OrgID']) && isset($type_attachments[$t]['ApplicationID']) && isset($type_attachments[$t]['TypeAttachment'])) {
				//Set where condition
				$where      =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "TypeAttachment = :TypeAttachment");
				//Set parameters
				$params     =   array(":OrgID"=>$type_attachments[$t]['OrgID'], ":ApplicationID"=>$type_attachments[$t]['ApplicationID'], ":TypeAttachment"=>$type_attachments[$t]['TypeAttachment']);
		        //Get Applicant Attachments
				$results    =   $AttachmentsObj->getApplicantAttachments ( 'PurposeName, FileType', $where, "", array($params) );
		
				if(is_array($results ['results'])) {
					foreach ( $results ['results'] as $img ) {
                        $purposename  =   $img ['PurposeName'];
                        $filetype     =   $img ['FileType'];
					}
				}
		
				$dfile = '';
		
				if (($purposename != "") && ($filetype != "")) {
					$dfile = IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments/' . $ApplicationID . '-' . $purposename . '.' . $filetype;
				}
		
				if (file_exists ( $dfile )) {
					$file_names[] = array($ApplicationID, $dfile);
				}
			}
		}
		
		//Create Temporary Files For Application View and History
		global $AccessCode, $USERID;
		
		$return_value = "true";
		
		include COMMON_DIR . 'application/PrintApplicationView.inc';
		$applicationdata  = "<style>";
		$applicationdata .= ".application_detail_view {
							    padding:0px 0px 0px 12px;
							}
							.application_detail_view p {
								margin:0px;
								padding:0px;
							}
							.application_detail_view hr {
								border:0px;
								margin-bottom:15px;
								margin-top:15px;
							}
							.application_detail_view .title {
								text-decoration:underline !important;
								font-weight:normal !important;
								font-size:18px;
							}
                            .application_detail_view .col-sm-3 {
                                width: 34%;
                                padding: 0px;
		                        float: left;
                            }
                            .application_detail_view .col-sm-9 {
                                width: 64%;
		                        float: left;
                            }
                            .application_detail_view .col-sm-12 {
                                width: 69%;
                                padding: 0px;
		                        float: left;
                            }
                            .application_detail_view #question_space {
                                padding: 0px;
		                        clear: both;
                            }";
		$applicationdata .= "</style>";
		$applicationdata .= "<div class='application_detail_view'>";
		$applicationdata .= $download_info;
		$applicationdata .= "</div>";
		
		$historydata = "
						<style type=\"text/css\">
						<!--
						body {
						  font-family: Arial, Helvitica;
						  font-size: 10pt;
						  background: #ffffff;
						}
								
						table {
						  padding: 5px;
						  border-width: 0px;
						}
								
						b.red {
						font-size: 10pt;
						font-style: bold;
						color: red;
						}
						-->
						</style>
						";
		
		$historydata .= '<table border="1" cellspacing="l" cellpadding="0">';
		$historydata .= '<tr><td colspan="4">Applicant History for ' . $ApplicationID . '</td></tr>';
		$historydata .= '<tr><td width="150"><b>Date</b></td><td><b>Status</b></td><td><b>Updated By</b></td><td><b>Comments</b></td></tr>';
		
		//Set columns
		$columns              =   "ProcessOrder, date_format(Date,'%Y-%m-%d %H:%i EST') as Date1, UserID, Comments, DispositionCode";
		//Set where condition
		$where_job_app_his    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
		//Set parameters
		$params_job_app_his   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
		//Get JobApplication History Information
		$results              =   G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo($columns, $where_job_app_his, '', 'Date DESC', array($params_job_app_his));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $row) {
		
				$ProcessOrder   =   $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $row ['ProcessOrder'] );
				$Date           =   $row ['Date1'];
				$UserID         =   $row ['UserID'];
				$Comments       =   $row ['Comments'];
				$disposition    =   $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] );
		
				$historydata .= '<tr><td valign=top>' . $Date . '</td><td valign=top>' . $ProcessOrder;
				if ($disposition) {
					$historydata .= '<br>' . $disposition;
				}
				$historydata .= '</td>';
				$historydata .= '<td valign=top>' . $UserID . '</td><td valign=top width=250>' . $Comments . '</td></tr>';
			}
		}
		
		$historydata .= '</table>';
		
		@chmod("zips", 0777);
		
		if(in_array("appview", $zip_options)) {
			$appdata_file_name = "ApplicantData-".$OrgID."-".$ApplicationID."-".$RequestID.rand().time()."."."html";
			$appdata_file = fopen(IRECRUIT_DIR . "zips/".$appdata_file_name, "w");
			fwrite($appdata_file, $applicationdata);
			fclose($appdata_file);
			@chmod(IRECRUIT_DIR . "zips/".$appdata_file_name, 0777);
			$html_file_names[] = array($ApplicationID, "zips/".$appdata_file_name);
		}
		
		if(in_array("apphistory", $zip_options)) {
			$history_file_name = "ApplicantHistory-".$OrgID."-".$ApplicationID."-".$RequestID.rand().time()."."."html";
			$history_file = fopen(IRECRUIT_DIR . "zips/".$history_file_name, "w");
			fwrite($history_file, $historydata);
			fclose($history_file);
			@chmod(IRECRUIT_DIR . "zips/".$history_file_name, 0777);
			$html_file_names[] = array($ApplicationID, "zips/".$history_file_name);
		}
		
		if(in_array("iconnectforms", $zip_options)) {
		    $columns      =   "UniqueID, FormName, FormType";
		    $where_info   =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = '1'", "Status >= '3'", "FormType IN ('Agreement', 'PreFilled', 'WebForm')");
		    $params_info  =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
		    
		    $assigned_completed_forms_res =   $FormsInternalObj->getInternalFormsAssignedInfo($columns, $where_info, "", "", array($params_info));
		    $assigned_completed_forms     =   $assigned_completed_forms_res['results'];
		    $assigned_completed_forms_cnt =   $assigned_completed_forms_res['count'];
		      
		    for($acf = 0; $acf < $assigned_completed_forms_cnt; $acf++) {
		
		        $iConnectUniqueID = $assigned_completed_forms[$acf]['UniqueID'];
		        $iConnectFormName = preg_replace("/[^a-z0-9.]+/i", "", $assigned_completed_forms[$acf]['FormName']);

		        if($assigned_completed_forms[$acf]['FormType'] == "WebForm") {
		            require_once COMMON_DIR . 'formsInternal/PrintWebFormView.inc';
		            $form_data = printWebForm ( $OrgID, $ApplicationID, $RequestID, $iConnectUniqueID );
		        }
		        else if($assigned_completed_forms[$acf]['FormType'] == "Agreement") {
		            require_once COMMON_DIR . 'formsInternal/PrintAgreementFormView.inc';
		            $form_data = printAgreementForm ( $OrgID, $ApplicationID, $RequestID, $iConnectUniqueID );
		        }
		        else if($assigned_completed_forms[$acf]['FormType'] == "PreFilled") {
            	    require_once IRECRUIT_DIR . 'formsInternal/WritePDF.inc';
            	    $form_data = createPDFi9($dir, $iConnectUniqueID, $ApplicationID, $RequestID);
		        }
		         
		        if($form_data != "") {
		            if($assigned_completed_forms[$acf]['FormType'] == "PreFilled") {
    	                $html_file_names[] = array($ApplicationID, $form_data);
	                }
	                else {    
		                $iconnect_file_name   =   $iConnectFormName."-".$OrgID."-".$ApplicationID."-".$RequestID."-".$iConnectUniqueID."."."html";
		                $iconnect_file        =   fopen("zips/".$iconnect_file_name, "w");
		                fwrite($iconnect_file, $form_data);
		                fclose($iconnect_file);
		                @chmod("zips/".$iconnect_file_name, 0777);
		                $html_file_names[] = array($ApplicationID, "zips/".$iconnect_file_name);
		            }
		        }
		    }
		
		}
		
		
	}

	$zip_name = "zips/".$OrgID.time()."-applicant_documents.zip";
	
	@touch($zip_name, 0777);
	@chmod(0777);
	
	if ($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
	
		if(is_array($file_names)) {
			foreach($file_names as $file_information)
			{
                $ApplicationID          =   $file_information[0];
                $file_path_name         =   $file_information[1];
                $ext                    =   pathinfo($file_path_name, PATHINFO_EXTENSION); // to get extension
                $fname                  =   pathinfo($file_path_name, PATHINFO_FILENAME); //file name without extension
                $file_name              =   $fname.".".$ext;
			
				$zip->addFile('vault/'.$OrgID.'/applicantattachments/'.$file_name, $ApplicationID."/".$file_name);
				$available_to_download = true;
			}
		}
		
		if(is_array($html_file_names)) {
			foreach($html_file_names as $file_information)
			{
                $ApplicationID          =   $file_information[0];
                $html_file_path_name    =   $file_information[1];
			    
				$html_ext               =   pathinfo($html_file_path_name, PATHINFO_EXTENSION); // to get extension
				$html_fname             =   pathinfo($html_file_path_name, PATHINFO_FILENAME); //file name without extension
				$html_file_name         =   $html_fname.".".$html_ext;
			
				$zip->addFile('zips/'.$html_file_name, $ApplicationID."/".$html_file_name);
				$available_to_download = true;
			}
		}
		
	}
	
	$zip->close ();
}

//Unlink Files
foreach($html_file_names as $file_information)
{
    $ApplicationID          =   $file_information[0];
    $html_file_path_name    =   $file_information[1];
    $html_ext               =   pathinfo($html_file_path_name, PATHINFO_EXTENSION); // to get extension
    $html_fname             =   pathinfo($html_file_path_name, PATHINFO_FILENAME); //file name without extension
    $html_file_name         =   $html_fname.".".$html_ext;
	
	@unlink('zips/'.$ApplicationID.'/'.$html_file_name);
}


//Unlink Directories
foreach($html_file_names as $file_information)
{
    $ApplicationID          =   $file_information[0];
    $html_file_path_name    =   $file_information[1];

    @rmdir('zips/'.$ApplicationID);
}

if($available_to_download == true) downloadFile($zip_name);
?>