<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title             =   $title              =   'Applicant Vault';
$TemplateObj->PreFilledFormID   =   $PreFilledFormID    =   isset($_REQUEST['PreFilledFormID']) ? $_REQUEST['PreFilledFormID'] : '';
$TemplateObj->action            =   $action             =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->process           =   $process            =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->ApplicationID     =   $ApplicationID      =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID         =   $RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->UpdateID          =   $UpdateID           =   isset($_REQUEST['UpdateID']) ? $_REQUEST['UpdateID'] : '';
$TemplateObj->k                 =   $k                  =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

if (($PreFilledFormID != "") && ($action == "")) {
  $close_link = IRECRUIT_HOME . 'formsInternal/completePreFilledForm.php';
  $close_link.= '?PreFilledFormID=' . $PreFilledFormID;
} else {
  $close_link = IRECRUIT_HOME . 'applicants.php';
  $close_link.= '?action=' . $action;
}
$close_link.= '&ApplicationID=' . $ApplicationID;
$close_link.= '&RequestID=' . $RequestID;

if ($AccessCode != "") { $close_link .= '&k=' . $AccessCode; }

$TemplateObj->close_link = $close_link;

include COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';


if ($feature['ApplicantVault'] != 'Y') { die (include IRECRUIT_DIR . 'irecruit_permissions.err'); }

if($ServerInformationObj->getRequestSource() == 'ajax') {
    //view for AddApplicant.inc
    require_once COMMON_DIR . 'formsInternal/ApplicantVault.inc';
}
else {
    $TemplateObj->default_view_dir = COMMON_DIR;
    //view for AddApplicant.inc
    echo $TemplateObj->displayIrecruitTemplate('formsInternal/ApplicantVault');
}
?>