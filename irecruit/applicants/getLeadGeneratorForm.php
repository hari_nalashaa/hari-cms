<?php 
require_once '../Configuration.inc';

$ApplicantFormID        =   $ApplicantDetailsObj->getFormID($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
$LeadStatus             =   $ApplicantDetailsObj->getLeadStatus($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
$lead_processed_date    =   $ApplicantDetailsObj->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'LeadProcessedDate');
$ApplicantEmail         =   $ApplicantDetailsObj->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'email');

//Get User Portal User Information
$USER                   =   $UserPortalUsersObj->getUserDetailInfoByEmail ( "*", $ApplicantEmail );


if($LeadStatus  ==  'N') {
    ?>
    <br><br>
    <strong>Note:</strong> After clicking on Process application, this application will no longer treated as lead. 
    If you want to update the FormID please update it before processing. 
    An email will be sent to applicant and new account will be created with this email in case account doesn't exist.
    <br><br>
    <form id="frmLeadGenerator" name="frmLeadGenerator" method="post">
        <?php
        // set condition
        $where  = array ("OrgID = :OrgID");
        // set parameters
        $params = array (":OrgID"=>$OrgID);
        // set columns
        $columns = "DISTINCT(FormID) as DistinctFormID";
        // Get formquestions information
        $results = $FormQuestionsObj->getFormQuestionsInformation ( $columns, $where, 'FormID', array ($params) );
    
        echo '<select name="FormID" id="FormID" class="form-control width-auto-inline">';
        if (is_array ( $results ['results'] )) {
            foreach ( $results ['results'] as $row ) {
                $sel = ($ApplicantFormID == $row ['DistinctFormID']) ? " selected='selected'" : "";
                echo '<option value="' . $row ['DistinctFormID'] . '"' . $sel . '>' . $row ['DistinctFormID'] . '</option>';
            }
        }
        echo '</select>';
        ?>
        <input type="button" name="btnProcessLeadApplication" id="btnProcessLeadApplication" class="btn btn-primary" value="Process Application" onclick="processLeadGeneratorApplication(this.form)">
        <span id="process_lead_message" style="border:1px solid #f5f5f5;text-align:center;color:#428bca;"></span>
        <br><br><br><br>
    </form>
    <?php
}
else if($LeadStatus  ==  'Y') {
	?>
	<br>
    	<h4>
    	This lead is already processed on <?php echo $lead_processed_date;?>.
    	</h4>
    	<form id="frmLeadGenerator" name="frmLeadGenerator" method="post">
        <input type="hidden" name="FormID" id="FormID" value="<?php echo $ApplicantFormID;?>">
        <span id="process_lead_message" style="border:1px solid #f5f5f5;text-align:center;color:#428bca;"></span>
    	<?php 
        if($USER['UserID'] != "") {
        	?>
            <input type="button" name="btnSendReminder" id="btnSendReminder" class="btn btn-primary" value="Send Reminder" onclick="sendCompleteApplicationReminder(this.form)">
        	<?php
        }
        ?>
        </form>
	<br>
	<?php
}
else {
    ?>
	<h3>This is not a lead to process.</h3>
	<?php
}
?>