<?php
require_once '../Configuration.inc';

$multiorgid_req	=   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
$job_title		=   G::Obj('RequisitionDetails')->getJobTitle($OrgID, $multiorgid_req, $_REQUEST['RequestID'] );

$OrgName		=   G::Obj('OrganizationDetails')->getOrganizationName($OrgID, "");

$first			=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'first');
$last			=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'last');

//Get twilio number
$user_details	=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "FirstName, LastName");


$template_info	=	G::Obj('TwilioTextMessageTemplates')->getTwilioTextMessagingTemplatesByTemplateGuID($_REQUEST['TemplateGuID']);

$message		=	$template_info['DisplayText'];

if(isset($_REQUEST['SwapPlaceHolders']) && $_REQUEST['SwapPlaceHolders'] == "True") {
	$message 		=	str_replace("{first}", $first, $message);
	$message 		=	str_replace("{last}", $last, $message);
	$message 		=	str_replace("{JobTitle}", $job_title, $message);
	$message 		=	str_replace("{OrganizationName}", $OrgName, $message);
	$message 		=	str_replace("{RecruiterName}", trim($user_details['FirstName'] . ' ' . $user_details['LastName']), $message);
}

echo $message;
?>