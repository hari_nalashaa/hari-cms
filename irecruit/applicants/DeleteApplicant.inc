<?php
if (($ApplicationID) && ($RequestID)) {
	archiveDelete ( $OrgID, $ApplicationID, $RequestID );
} // end check

if (($multi == "Y") && (! $multipleapps)) {
	$count = count ( $appid );
	for($i = 0; $i < $count; $i ++) {
		
		$jarj = explode ( ':', $appid [$i] );
		
		$ApplicationID = $jarj [0];
		$RequestID = $jarj [1];
		
		if (($ApplicationID) && ($RequestID)) {
			archiveDelete ( $OrgID, $ApplicationID, $RequestID );
		} // end check
	} // end for loop
} // end multi check

// *** FUNCTIONS *** //
function archiveDelete($OrgID, $ApplicationID, $RequestID) {
	global $ApplicationsObj, $RequisitionsObj, $RequisitionDetailsObj;
	
	$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
	// Delete JobApplications
	$ApplicationsObj->delJobApplications ( $OrgID, $ApplicationID, $RequestID );
	
	$message = 'ApplicationID: ' . $ApplicationID . ' - ';
	$message .= 'Requisition/JobID: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
	$message .= ' has been deleted.';
	
	if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
		
		echo '<meta http-equiv="refresh" content="0;url=applicants.php">';
	}
	if (preg_match ( '/deleteApplicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		if($message != "") echo $message;
	}
	
} // end function
?>