<?php
global $USERID;

if(isset($_POST['selectquestions']) && $_POST['selectquestions'] == "yes") {

    foreach($_POST['Questions'] as $SelQuestionID=>$SelQuestionIDVal) {

        //FormID
        if($FormID == "WebForms") {

            $where_info         =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID", "Active = 'Y'");
            $params_info        =   array(":OrgID"=>$OrgID, ":WebFormID"=>$SectionID, ":QuestionID"=>$SelQuestionID);
            $resquestions       =   G::Obj('FormQuestions')->getQuestionsInformation("WebFormQuestions", "*", $where_info, "Question DESC", array($params_info));
             
            $FQR                =   $resquestions['results'][0];
            $FQR ['SectionID']  =   $FQR ['Section'] = $SectionID;
        }
        else {
            //Get Form Question Details
            $columns            =   "Question, QuestionID, QuestionTypeID, value";
            $FQR                =   G::Obj('FormQuestionsDetails')->getFormQuestionInfoBySecIDQuesID($columns, $OrgID, $FormID, $SectionID, $SelQuestionID);
            $sec_info           =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $FormID, $SectionID);
            $FQR['Section']     =   $sec_info['SectionTitle'];
            $FQR['SectionID']   =   $SectionID;
        }

        if ($FQR ['QuestionTypeID'] == 2 
            || $FQR ['QuestionTypeID'] == 3 
            || $FQR ['QuestionTypeID'] == 9 
            || $FQR ['QuestionTypeID'] == 1818 
            || $FQR ['QuestionTypeID'] == 18 
            || $FQR ['QuestionTypeID'] == 100) {

            $name = preg_replace ( '/ /', '_', $FQR ['Section'] ) . ":" . $FQR ['QuestionID'] . ":QUS::";

            // weighted search information to insert, common for question and answer
            $weig_search_ins_info = array (
                "UserID"                =>  $USERID,
                "OrgID"                 =>  $OrgID,
                "FormID"                =>  $FormID,
                "SaveSearchName"        =>  $SaveSearchName,
                "WeightedSearchRandId"  =>  $_GET['uwkey'],
                "SectionID"             =>  $SectionID,
                "QuestionID"            =>  $SelQuestionID,
                "QuestionTypeID"        =>  $FQR ['QuestionTypeID'],
                "QuestionAnswerKey"     =>  $name,
                "CreatedDateTime"       =>  "NOW()"
            );

            //Insert Question Inforamtion
            $on_update = " ON DUPLICATE KEY UPDATE CreatedDateTime = NOW()";
            //Insert and update the weighted search questions and answers
            G::Obj('WeightedSearch')->insUpdWeightSaveSearch($weig_search_ins_info, $on_update, array());

            if($FQR ['QuestionTypeID'] == 100) {

                // weighted search information to insert
                $weig_search_ins_info["QuestionAnswerWeight"]       = 0;
                $weig_search_ins_info["CustomQuestion"]             = $FQR ['value'];
                $weig_search_ins_info["CustomQuestionAnswerWeight"] = 0;

                //Insert Question Inforamtion
                $on_update = " ON DUPLICATE KEY UPDATE CreatedDateTime = NOW()";
                //Insert and update the weighted search questions and answers
                G::Obj('WeightedSearch')->insUpdWeightSaveSearch($weig_search_ins_info, $on_update, array());
            }
            else {
                 
                $mans = explode ( '::', $FQR ['value'] );

                for ($i = 0; $i < count($mans); $i ++) {
                    $ans = explode(':', $mans[$i]);
                    $name = preg_replace('/ /', '_', $FQR['Section']) . ":" . $FQR['QuestionID'] . ":ANS:" . $ans[0] . ":";
                    for ($a = 0; $a < count($a); $a ++) {
                        $answer = $ans[0];
                        if ($ans[0] != "") {

                            $weig_search_ins_info["QuestionAnswerKey"] = $name;
                            $weig_search_ins_info["QuestionAnswerWeight"] = 0;

                            //Update CreatedDateTime
                            $on_update = " ON DUPLICATE KEY UPDATE CreatedDateTime = NOW()";
                            //Insert and update the weighted search questions and answers
                            G::Obj('WeightedSearch')->insUpdWeightSaveSearch($weig_search_ins_info, $on_update, array());
                        }
                    }
                }

            }
        }

    }

}

$QuestionIdsList = array ();
//Assign all the post values to database

if(is_array($_POST['QA'])) {
    foreach ( $_POST ['QA'] as $InSecID => $InQue ) {
        foreach ( $InQue as $InQueID => $InQueInfo ) {
            foreach ( $InQueInfo as $AKey => $AWeight ) {
                if($InSecID == "" || empty($InSecID)) $InSecID = "0";
                if($FormID == "WebForms") {
                    // Set where condition
                    $where_info     =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
                    $params_info    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$InSecID, ":QuestionID"=>$InQueID);
                    $res_que_info   =   G::Obj('FormQuestions')->getQuestionsInformation("WebFormQuestions", "*", $where_info, "Question DESC", array($params_info));
                    $row_que_info   =   $res_que_info ['results'] [0];
                }
                else {
                    $columns        =   "Question, QuestionID, QuestionTypeID, value";
                    $row_que_info   =   G::Obj('FormQuestionsDetails')->getFormQuestionDetailsInfo($columns, $OrgID, $FormID, $InQueID);
                }
                 
                $QuestionTypeID     =   $row_que_info ['QuestionTypeID'];
                
                if ($QuestionTypeID == 100) {
                    if (! in_array ( $InQueID, $QuestionIdsList )) {
    
                        // weighted search information to insert
                        $weig_search_ins_info = array (
                            "UserID"                        =>  $USERID,
                            "OrgID"                         =>  $OrgID,
                            "FormID"                        =>  $FormID,
                            "SaveSearchName"                =>  $SaveSearchName,
                            "WeightedSearchRandId"          =>  $_GET['uwkey'],
                            "SectionID"                     =>  $InSecID,
                            "QuestionID"                    =>  $InQueID,
                            "QuestionTypeID"                =>  $QuestionTypeID,
                            "QuestionAnswerKey"             =>  $AKey,
                            "QuestionAnswerWeight"          =>  $AWeight,
                            "CustomQuestion"                =>  $row_que_info ['value'],
                            "CustomQuestionAnswerWeight"    =>  serialize ( $_POST ['QA'] [$InSecID] [$InQueID] )
                        );
                        
                        //Insert Question Inforamtion
                        $on_update      =   " ON DUPLICATE KEY UPDATE CreatedDateTime = NOW(), SaveSearchName = :USaveSearchName, QuestionAnswerWeight = :UQuestionAnswerWeight, CustomQuestionAnswerWeight = :UCustomQuestionAnswerWeight, CustomQuestion = :UCustomQuestion";
                        //Update Information
                        $update_info    =   array(":UQuestionAnswerWeight" => $AWeight, ":USaveSearchName"=>$SaveSearchName, ":UCustomQuestionAnswerWeight" => serialize ( $_POST ['QA'] [$InSecID] [$InQueID] ), ":UCustomQuestion"=>$row_que_info ['value']);
                        //Insert and update the weighted search questions and answers
                        G::Obj('WeightedSearch')->insUpdWeightSaveSearch($weig_search_ins_info, $on_update, $update_info);
                    
                    }
                    
                    $QuestionIdsList [] = $InQueID;
                } else {
                    // weighted search information to insert
                    $weig_search_ins_info = array (
                        "UserID"                        =>  $USERID,
                        "OrgID"                         =>  $OrgID,
                        "FormID"                        =>  $FormID,
                        "SaveSearchName"                =>  $SaveSearchName,
                        "WeightedSearchRandId"          =>  $_GET['uwkey'],
                        "SectionID"                     =>  $InSecID,
                        "QuestionID"                    =>  $InQueID,
                        "QuestionTypeID"                =>  $QuestionTypeID,
                        "QuestionAnswerKey"             =>  $AKey,
                        "QuestionAnswerWeight"          =>  $AWeight,
                        "CustomQuestion"                =>  $row_que_info ['value'],
                        "CustomQuestionAnswerWeight"    =>  serialize ( $_POST ['QA'] [$InSecID] [$InQueID] )
                    );

                    //Insert Question Inforamtion
                    $on_update      =   " ON DUPLICATE KEY UPDATE CreatedDateTime = NOW(), SaveSearchName = :USaveSearchName, QuestionAnswerWeight = :UQuestionAnswerWeight, CustomQuestionAnswerWeight = :UCustomQuestionAnswerWeight, CustomQuestion = :UCustomQuestion";
                    //Update Information
                    $update_info    =   array(":USaveSearchName"=>$SaveSearchName, ":UQuestionAnswerWeight" => $AWeight, ":UCustomQuestionAnswerWeight" => serialize ( $_POST ['QA'] [$InSecID] [$InQueID] ), ":UCustomQuestion"=>$row_que_info ['value']);
                    //Insert and update the weighted search questions and answers
                    G::Obj('WeightedSearch')->insUpdWeightSaveSearch($weig_search_ins_info, $on_update, $update_info);
                }
            }
        }
    }
}

//if ($_POST ['process'] == "Y") {    
   // echo '<script>location.href="applicants.php?action=savedweightedsearch&msg=suc";</script>';
//}