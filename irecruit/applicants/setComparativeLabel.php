<?php 
require_once '../Configuration.inc';

$comparative_label_applicants   =   G::Obj('ComparativeAnalysis')->insComparativeLabelApplicants($OrgID, $_REQUEST['LabelID'], $USERID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);

if($comparative_label_applicants == "Maximum Limit Reached") {
    echo "Maximum limit reached. User can't set more than 100 applicants per label";
}
else {
    echo "Successfully added the comparative tag: ";
    echo "<a href='".IRECRUIT_HOME."applicants/comparativeAnalysis.php?label_id=".$_REQUEST['LabelID']."&menu=10' style='text-decoration:underline' target='_blank'>";
    echo $_REQUEST['LabelName'];
    echo "</a>";
}
?>
