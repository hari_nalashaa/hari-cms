<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title = $title = 'iRecruit - Application View';

if ($permit ['Applicants'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

if($ServerInformationObj->getRequestSource() == 'ajax') {
    //view for AddApplicant.inc
    require_once IRECRUIT_DIR . 'views/applicants/ApplicationView.inc';
}
else {
    //view for AddApplicant.inc
    echo $TemplateObj->displayIrecruitTemplate('views/applicants/ApplicationView');
}
?>