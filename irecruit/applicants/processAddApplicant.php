<?php
if (isset($_POST ['AddApplicant']) && $_POST ['AddApplicant'] == "AddApplicant") {
	  
	//Set Post Data
	G::Obj('GetFormPostAnswer')->POST =   $_POST;
	
	//Get all the form questions with MultiSection Prefix
	$json_form_que_list    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID, "1");
	$form_que_list         =   $json_form_que_list['json_ques'];
	
	//Insert temporary data
	foreach($form_que_list as $QuestionID=>$QI) {
        $ignore_ques        =   array('RandID', 'otherjobs', 'instructions');
	    
	    if($QI['QuestionTypeID'] != 99
	        && !in_array($QI['QuestionID'], $ignore_ques)) {

            $QI['Answer']   =   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
            
            // Bind Parameters
            $params         =   array(
                                    'OrgID'             =>  $OrgID,
                                    'HoldID'            =>  $HoldID,
                                    'QuestionID'        =>  $QuestionID,
                                    'SectionID'         =>  $QI['SectionID'],
                                    'Answer'            =>  $QI['Answer'],
                                    'Required'          =>  $QI['Required'],
                                );
            // Insert applicant data temp
            G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params);
            
        }
	}

	// Upload Attachments
	require_once IRECRUIT_DIR . 'applicants/ProcessApplicationAttachmentsTemp.inc';
	
	// Validate input
	require_once IRECRUIT_DIR . 'applicants/ValidateAddApplicant.inc';
	
	if ($ERROR) {
		$MESSAGE  =   "The following entries are missing information.\\n\\n";
		$MESSAGE .=   $ERROR;
		$MESSAGE .=   " - Please validate that your attachments are selected.\\n";
		
		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('$MESSAGE')" . "\n";
		echo "location.href = '" . IRECRUIT_HOME . "applicants/addApplicant.php" . "?HoldID=" . $HoldID . "&RequestID=" . $RequestID . "'" . "\n";
		echo '</script>' . "\n\n";
	} else {
		
		// Data to insert, here keys are table column names
		$ins_app_lock_info    =   array(
                                        "EntryDate"     =>  "NOW()",
                                        "FirstName"     =>  $_REQUEST ['first'],
                                        "LastName"      =>  $_REQUEST ['last'],
                                        "Address"       =>  $_REQUEST ['address'],
                                        "City"          =>  $_REQUEST ['city'],
                                        "Zip"           =>  $_REQUEST ['zip'],
                                        "Email"         =>  $email 
                                    );
		// lock for a duplicate submission
		G::Obj('Applicants')->insApplicantLock ( $ins_app_lock_info );
		// Generate ApplicationID
		G::Obj('Template')->ApplicationID   =   $ApplicationID  =   G::Obj('Applications')->getApplicationID ($OrgID);
		
		// process attachments
		if(isset($_REQUEST['resumeparsing']) 
		    && $_REQUEST['resumeparsing'] == 'resumeparsing') {

			// Set Requisions for DB
			G::Obj('Template')->RequestID = $RequestID;
				
			//Get Purpose names list
			$purpose         =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $_REQUEST ['FormID']);
			//Rename the file with cookie appid to application id
			$file_path_info  =   G::Obj('Sovren')->copyPublicPortalParsingTempToParsing($OrgID, $HoldID, $ApplicationID, $RequestID, $_REQUEST ['FormID']);
			
			//Applicant Attachments Information
			$applicant_attach_info   =   array(
                                            "OrgID"            =>  $OrgID,
                                            "ApplicationID"    =>  $ApplicationID,
                                            "TypeAttachment"   =>  'resumeupload',
                                            "PurposeName"      =>  $purpose['resumeupload'],
                                            "FileType"         =>  $file_path_info['extension']
                                        );
			//Insert applicant attachments information
			G::Obj('Attachments')->insApplicantAttachments ( $applicant_attach_info );
		}
		
		// process attachments
		include COMMON_DIR . 'process/FileHandling.inc';
		
		$ReceivedDate =   '0000-00-00';
		
		if ($_REQUEST ['receiveddate'] != "") {
			$ReceivedDate    =   G::Obj('MysqlHelper')->getDateTimeFromString ( $_REQUEST ['receiveddate'], '%m/%d/%Y' );
		}
		
		$req_info     =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Active", $OrgID, $RequestID);

		$LeadID       =   G::Obj('ApplicationLeads')->getLeadIDFromFormID($OrgID, $_REQUEST ['FormID']);
		$LeadStatus   =   ($LeadID == "") ? "" : "N";
		
		//Insert data into DB (JobApplications)
		$job_application_info =   array(
                                        "OrgID"                 =>  $OrgID,
                                        "MultiOrgID"            =>  $MultiOrgID,
                                        "ApplicationID"         =>  $ApplicationID,
                                        "RequestID"             =>  $RequestID,
                                        "ApplicantSortName"     =>  $_REQUEST ['last'] . ', ' . $_REQUEST ['first'],
                                        "Distinction"           =>  "P",
                                        "EntryDate"             =>  "NOW()",
                                        "ReceivedDate"          =>  $ReceivedDate,
                                        "LastModified"          =>  "NOW()",
                                        "ProcessOrder"          =>  "1",
                                        "StatusEffectiveDate"   =>  "DATE(NOW())",
                                        "FormID"                =>  $_REQUEST ['FormID'],
                                        "Informed"              =>  $_POST['informed'],
                                        "RequisitionStatus"     =>  $req_info['Active'],
                                        "LeadGenerator"         =>  $LeadID,
                                        "LeadStatus"            =>  $LeadStatus
                                    );
		//Insert JobApplication Information
		G::Obj('Applications')->insJobApplication ( $job_application_info );
		
		//Insert Applicant Status Logs
		G::Obj('ApplicantStatusLogs')->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, "1", "");
		
    	//Log Record Details if FormID is empty
        if(!isset($_REQUEST ['FormID']) || $_REQUEST ['FormID'] == "") {
            $app_info_log_data  = FROM_SRC."\n";
            $app_info_log_data .= "\nApplication Info: " . json_encode($job_application_info);
        
            Logger::writeMessage(ROOT."logs/appinfo/". $OrgID . "-" . date('Y-m-d-H') . "-emptyformid.txt", $app_info_log_data, "a+", true, "FormID is empty - processAddApplicant.php");
        }
		
		//Update requisitions information
		$set_info         =   array("ApplicantsCount = ApplicantsCount + 1");
		$where_info       =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID");
		$params_info      =   array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "RequestID"=>$RequestID);
		G::Obj('Requisitions')->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
		
		// Insert into ApplicantHistory
		$UpdateID         =   $USERID;//'Applicant';

		$multiorgid_req   =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
		$Comments         =   'Application submitted for: ' . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
		if($ReceivedDate != '0000-00-00') {
			$Comments .= "<br>ReceivedDate: ".$ReceivedDate;
		}		
		
		// Insert JobApplication History Data
		$job_application_history = array (
				"OrgID"                 =>  $OrgID,
				"ApplicationID"         =>  $ApplicationID,
                "RequestID"             =>  $RequestID,
				"ProcessOrder"          =>  "1",
				"StatusEffectiveDate"   =>  "DATE(NOW())",
				"Date"                  =>  "NOW()",
				"UserID"                =>  $UpdateID,
				"Comments"              =>  $Comments 
		);
		// Insert JobApplicationsHistory
		G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );
		
		/**
		 * This is a temporary code until the final upgrade.
		 * Upto that time, we have to keep on adding different conditions to it.
		 */
		G::Obj('GetFormPostAnswer')->POST =   $_POST;    //Set Post Data
		
		$json_form_que_list    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID, "1");
		$form_que_list         =   $json_form_que_list['json_ques'];
		
		foreach($form_que_list as $QuestionID=>$QI) {
		    
            	    $ignore_ques      =   array('RandID', 'otherjobs', 'instructions');
		    
		    G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
		    
		    if($QI['QuestionTypeID'] != 99
		        && $QI['QuestionTypeID'] != 8
		        && !in_array($QI['QuestionID'], $ignore_ques)) {
		            
	            $QI['Answer']           =   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	            $QI['ApplicationID']    =   $ApplicationID;
	            
	            G::Obj('Applicants')->insUpdApplicantData($QI);
	        }
		        
		}
		
		//Other Questions
		$other_ques_list    =   array(
                                    'FormID'    =>  array("Question"=>"FormID", "Answer"=>$FormID),
                        		);
		//Insert Other Questions
		foreach ($other_ques_list as $other_que_id=>$other_que_answer) {
		    $QI     =   array(
		                      "OrgID"             =>  $OrgID,
		                      "ApplicationID"     =>  $ApplicationID,
		                      "QuestionID"        =>  $other_que_id,
		                      "Answer"            =>  $other_que_answer["Answer"]
		                  );
		    G::Obj('Applicants')->insUpdApplicantData($QI);
		}
		
		// Set status if applicant inserted
		$TemplateObj->applicant_inserted = '1';

	}
}
?>
