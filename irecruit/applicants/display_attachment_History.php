<?php
require_once '../irecruitdb.inc';

if (($_REQUEST['OrgID']) && ($_REQUEST['File'])) {
	
	$dfile = '';
	
	$dfile = IRECRUIT_DIR . 'vault/' . $_REQUEST['OrgID'] . '/applicantattachments/' . $_REQUEST['File'];
	
	if (file_exists ( $dfile )) {
		
		header ( 'Content-Description: File Transfer' );
		header ( 'Content-Type: application/octet-stream' );
		header ( 'Content-Disposition: attachment; filename=' . basename ( $dfile ) );
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		header ( 'Content-Length: ' . filesize ( $dfile ) );
		readfile ( $dfile );
		exit ();
	}
} // end if OrgID and File
?>