<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title             =   $title              =   'Modify Attachment';

$TemplateObj->ApplicationID     =   $ApplicationID      =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID         =   $RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->Type              =   $Type               =   isset($_REQUEST['Type']) ? $_REQUEST['Type'] : '';
$TemplateObj->edit              =   $edit               =   isset($_REQUEST['edit']) ? $_REQUEST['edit'] : '';
$TemplateObj->action            =   $action             =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->k                 =   $k                  =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->process           =   $process            =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->HoldID            =   $HoldID             =   G::Obj('MysqlHelper')->getDateTime ( '%Y%m%d%H%m%s' );

$close_link                     =   IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID;

if ($AccessCode != "") {
	$close_link .= '&k=' . $AccessCode;
}

$TemplateObj->close_link = $close_link;

include IRECRUIT_DIR . 'views/applicants/EditAttachments.inc';
?>