<?php
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto = $_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title = "Comparative Tags";

// Scripts header
$scripts_header [] = "js/comparative-search.js";

//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

if(isset($_GET['action']) 
    && $_GET['action'] == 'delete'
    && isset($_GET['ApplicationID'])
    && $_GET['ApplicationID'] != ""
    && isset($_REQUEST['label_id'])
    && $_REQUEST['label_id'] != "") {
    $ComparativeAnalysisObj->delComparativeLabelApplicants($_REQUEST['label_id'], $USERID, $_GET['ApplicationID']);
    
    header("Location:".IRECRUIT_HOME."applicants/comparativeLabelApplicants.php?label_id=".$_REQUEST['label_id']);
    exit;
}

//Comparative Tag Results
$comp_lbl_results   =   $ComparativeAnalysisObj->getComparativeLabels($OrgID, $USERID);
$comp_lbls_list     =   $comp_lbl_results['results'];

if(isset($_REQUEST['label_id']) && $_REQUEST['label_id'] != "") {
    $LabelID            =   $_REQUEST['label_id'];
} else {
    $LabelID            =   $comp_lbls_list[0]['LabelID'];
}

if($LabelID != "") {
    $columns            =   "ApplicationID, RequestID";
    $app_results        =   $ComparativeAnalysisObj->getComparativeLabelApplicants($LabelID, $USERID);
}


//Applications list
$TemplateObj->applications_list     =   $applications_list     =   $app_results['results'];
$TemplateObj->comp_lbls_list        =   $comp_lbls_list;
$TemplateObj->LabelID               =   $LabelID;

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/ComparativeLabelApplicants' );
?>