<?php
$results = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
$DR = $results['DefaultSearchDateRange'];
if ($DR == "") {
	$DR = "90";
}

//Get Dates List
$columns = "DATE_FORMAT(NOW(),'%m/%d/%Y'),
    		DATE_FORMAT(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'),
  			DATE_FORMAT(NOW(),'%Y-%m-%d 23:59:59'),
  			DATE_FORMAT(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%Y-%m-%d 00:00:00')";
list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($MysqlHelperObj->getDatesList($columns));

if ($process == 'Y') {
	$StartDate =   $_REQUEST['ProcessDate_From'];
	$EndDate   =   $_REQUEST['ProcessDate_To'];
	$BeginDate =   substr ( $_REQUEST['ProcessDate_From'], - 4 ) . '-' . substr ( $_REQUEST['ProcessDate_From'], 0, 2 ) . '-' . substr ( $_REQUEST['ProcessDate_From'], 3, 2 ) . ' 00:00:00';
	$FinalDate =   substr ( $_REQUEST['ProcessDate_To'], - 4 ) . '-' . substr ( $_REQUEST['ProcessDate_To'], 0, 2 ) . '-' . substr ( $_REQUEST['ProcessDate_To'], 3, 2 ) . ' 23:59:59';
}


//Set where condition
$where_job_apps     =   array("OrgID = :OrgID");
//Set parameters
$params_job_apps    =   array(":OrgID"=>$OrgID);
//Set entry date where
$where_job_apps[]   =   $datestring = "(EntryDate < '$FinalDate' and EntryDate > '$BeginDate')";

//Set where condition
$where      =   array("OrgID = :OrgID", "UserID = :UserID");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
//Get ApplicationView
$results    =   G::Obj('IrecruitUsers')->getUserInformation("Applicationview", $where, "", array($params));
$row        =   $results['results'][0];

if ($row ['Applicationview'] == "S") {
	$where_job_apps[] = $distinctionstring = "Distinction = 'P'";
}

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$where_job_apps[] = $rolerestrict = $RequisitionsObj->restrictRequestID ( $OrgID, $USERID );
}

// loop through application status and set array of groups
$PROCESSORDER = array ();

//Set columns
$columns    =   "ProcessOrder, Description";
//Set where condition
$where      =   array("OrgID = :OrgID","Active = 'Y'");
//Set parameters
$params     =   array(":OrgID"=>$OrgID);
$results    =   $ApplicantsObj->getApplicantProcessFlowInfo($columns, $where, 'ProcessOrder', array($params));

$cnt = 0;

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		
		$po = $PROCESSORDER [$cnt] ['processorder'] = $row ['ProcessOrder'];
		$PROCESSORDER [$cnt] ['description'] = $row ['Description'];
		
		//Set columns information
		$columns      =   "COUNT(*) as Count1, count(distinct ApplicationID) as Count2";
		
		$params_job_apps[':ProcessOrder'] = $po;
		if($cnt == 0) {
			$where_job_apps[] = "ProcessOrder = :ProcessOrder";
		}
		
		$resultsIN    =   $ApplicationsObj->getJobApplicationsInfo($columns, $where_job_apps, '', '', array($params_job_apps));
		$pocnt        =   $resultsIN['results'][0];
	
		$PROCESSORDER [$cnt] ['count']            =   $pocnt ['Count1'];
		$PROCESSORDER [$cnt] ['applicantcount']   =   $pocnt ['Count2'];
		
		$cnt ++;
	} // end foreach create PROCESSORDER array
}


echo '<div class="col-md-8 table-responsive" style="padding:0px;">';
echo '<form method="post" action="applicants.php">';

echo '<table border="0" class="table table-striped table-bordered" cellspacing="3" cellpadding="5">';
echo '<tr><td align="left" class="col-md-1 vertical-align-middle" nowrap="nowrap">';
echo 'Process Date From:</td><td valign="middle" class="vertical-align-middle">';
echo '<input type="text" id="ProcessDate_From" name="ProcessDate_From" value="' . htmlspecialchars($StartDate) . '" size="10" class="input-text-box" onBlur="validate_date(this.value,\' From Date\');"/>';
echo '</td>';
echo '<td align="right" class="vertical-align-middle">To:</td><td class="vertical-align-middle">';
echo '<input type="text" id="ProcessDate_To" name="ProcessDate_To" value="' . htmlspecialchars($EndDate) . '" size="10" class="input-text-box" onBlur="validate_date(this.value,\' To Date\');" />';
echo '</td>';
echo '<td valign="middle" class="vertical-align-middle" align="center">';
echo '<input type="hidden" name="action" value="status">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update List" class="btn btn-primary btn-sm">';
echo '</td></tr>';
echo '</table>';
echo '</form>';
echo '</div>';

echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">';

// loop through process order and display the list of applications
$i = 0;
while ( $i < count ( $PROCESSORDER ) ) {
	
	echo '<tr>';
	echo '<td>';
	
	echo '<b><a href="applicantsSearch.php?action=search&process=Y&ProcessOrder=' . htmlspecialchars($PROCESSORDER [$i] ['processorder']) . '&LimitProcessed=N';
	echo '&ApplicationDate_From=' . htmlspecialchars($StartDate);
	echo '&ApplicationDate_To=' . htmlspecialchars($EndDate);
	echo '"';
	echo '>' . $PROCESSORDER [$i] ['description'] . '</a></b> (' . $PROCESSORDER [$i] ['count'] . ' total) (' . $PROCESSORDER [$i] ['applicantcount'] . ' individual applicants)';
	
	if ($PROCESSORDER [$i] ['count'] > 0) {
		echo '&nbsp;&nbsp;&nbsp;<a href="javascript:ReverseDisplay(\'' . $PROCESSORDER [$i] ['processorder'] . '\')">show/hide details</a>' . "\n";
	}
	echo '</td></tr>';
	
	echo '<tr><td>';
	
	if ($PROCESSORDER [$i] ['count'] > 0) {
		echo listofApplications ( $OrgID, $PROCESSORDER [$i] ['processorder'], $rolerestrict, $datestring, $permit, $distinctionstring );
	}
	
	echo '</td></tr>';
	
	$i ++;
} // end while PROCESSORDER display

echo '</table>';

// *** FUNCTIONS *** //
function listofApplications($OrgID, $ProcessOrder, $rolerestrict, $datestring, $permit, $distinctionstring) {
    global $ApplicationsObj, $feature, $ApplicantsObj, $RequisitionsObj, $RequisitionDetailsObj, $OnboardApplicationsObj, $ApplicantDetailsObj, $ApplicationLeadsObj;
	
	$reports_settings = G::Obj('Reports')->getReportsSettings($OrgID, "");
	
	$columns = "ApplicationID, RequestID, EntryDate, LastModified, LeadGenerator, LeadStatus, DATEDIFF(NOW(), JobApplications.EntryDate) AS DaysSinceApplied, IsInternalApplication";
    $columns .= ", (SELECT DATEDIFF(NOW(), R.PostDate) FROM Requisitions R WHERE R.OrgID = JobApplications.OrgID AND R.RequestID = JobApplications.RequestID) as Open";
	
	//Set where condition
	$where_job_apps    =   array("OrgID = :OrgID", "ProcessOrder = :ProcessOrder");
	//Set parameters
	$params_job_apps   =   array(":OrgID"=>$OrgID, ":ProcessOrder"=>$ProcessOrder);
	
	if($rolerestrict != "") $where_job_apps[] = $rolerestrict;
	if($datestring != "") $where_job_apps[] = $datestring;
	if($distinctionstring != "") $where_job_apps[] = $distinctionstring;
	
	$results           =   G::Obj('Applications')->getJobApplicationsInfo($columns, $where_job_apps, '', 'Distinction, ApplicationID, RequestID', array($params_job_apps));
	$HiredProcessOrder =   $results['results'][0]['HiredProcessOrder'];
	
	$retlist .= '<div id="' . $ProcessOrder . '" style="display:none;">' . "\n";
	$retlist .= '<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered">';
	$retlist .= '<tr>';
	$retlist .= '<td width="180"><b>Name</b></td>';
	$retlist .= '<td><b>App ID</b></td>';
	$retlist .= '<td><b>Req/Job ID</b></td>';
	if ($feature ['InternalRequisitions'] == "Y") {
	    $retlist .= '<td><b>Internal Application</b></td>';
	}
    $retlist .= '<td align="center"><b>Days Open</b></td>';
    $retlist .= '<td align="center"><b>Days Since Applied</b></td>';
	$retlist .= '<td><b>Date Entered</b></td>';
	$retlist .= '<td><b>Last Modified</b></td>';
	$retlist .= '<td align="center"><b>Process Lead</b></td>';
	$retlist .= '<td align="center"><b>Onboard Status</b></td>';
	$retlist .= '</tr>';
	
	
	$rowcolor = "#eeeeee";
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
		
			$appid               =   $row ['ApplicationID'];
			$requestid           =   $row ['RequestID'];
			$entrydate           =   $row ['EntryDate'];
			$lastmodified        =   $row ['LastModified'];
			$days_open           =   $row ['Open'];
			$dayssinceapplied    =   $row ['DaysSinceApplied'];
			$leadstatus          =   $row ['LeadStatus'];
			$isinternalapp       =   ($row ['IsInternalApplication'] != "") ? $row ['IsInternalApplication'] : "No";
			
			//Get all onboard applications
			$where_info          =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
			$params              =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$appid);
			$onb_apps_info       =   G::Obj('OnboardApplications')->getOnboardApplications("*", $where_info, "", "", array($params));
			$onb_apps_res        =   $onb_apps_info['results'];
			
			$onb_status_list     =   array();
			
			for($oar = 0; $oar < $onb_apps_info['count']; $oar++) {
			    $onb_status_list[$onb_apps_res[$oar]['OnboardFormID']]   =   $onb_apps_res[$oar]['Status'];
			}
			
			// get applicant data
			$PersonalInformation =   '';
			$Email               =   '';
			
			//Set where condition
			$where       =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "QuestionID in ('First', 'Last','email')");
			//Set parameters
			$params      =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$appid);
			//Get ApplicantData Information
			$Presults    =   G::Obj('Applicants')->getApplicantDataInfo("Answer, QuestionID", $where, '', 'QuestionID', array($params));
			
			if(is_array($Presults['results'])) {
				foreach($Presults['results'] as $Prow) {
						
					if ($Prow ['QuestionID'] == "email") {
						$Email .= $Prow ['Answer'];
					} else {
						$PersonalInformation .= $Prow ['Answer'] . ' ';
					}
				}
			}
			
		
			$retlist .= '<tr bgcolor="' . $rowcolor . '">';
			if ($permit ['Applicants_Contact'] == 1) {
				$retlist .= '<td><a href="mailto:' . $Email . '">' . $PersonalInformation . '</a></td>';
			} else {
				$retlist .= '<td>' . $PersonalInformation . '</td>';
			}
		
			$retlist .= '<td><a href="applicantsSearch.php?ApplicationID=' . $appid . '&RequestID=' . $requestid . '">' . $appid . '</a></td>';
			
			$multiorgid_req = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $requestid);
			$retlist .= '<td>' . G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $requestid ) . '</td>';
			if ($feature ['InternalRequisitions'] == "Y") {
			    $retlist .= '<td align="center">' . $isinternalapp . '</td>';
			}
			$retlist .= '<td align="center">' . $days_open . '</td>';
			$retlist .= '<td align="center">' . $dayssinceapplied . '</td>';
			$retlist .= '<td>' . $entrydate . '</td>';
			$retlist .= '<td>' . $lastmodified . '</td>';
			
			if($leadstatus != "") {
			    //Get Lead Generator Information
			    $lead_info  =   G::Obj('ApplicantDetails')->getLeadGeneratorInfo($OrgID, $appid, $requestid);
			    if($lead_info != "" && ($lead_info == "Monster" || $lead_info == "Indeed" || $lead_info == "ZipRecruiter")) {
			        $lead_name  =   $lead_info;
			    }
			    else if($lead_info != "") {
			        $lead_name  =   G::Obj('ApplicationLeads')->getLeadName($OrgID, $lead_info);
			    }
			    
			    $retlist .= '<td align="center">';
			    if($leadstatus == "Y") {
			        $retlist .= "<a href='applicantsSearch.php?ApplicationID=" . $appid . "&RequestID=" . $requestid . "&tab_action=lead-generator' data-toggle='tooltip' data-placement='right'";
			        $retlist .= " data-html='true' title='".$lead_name."'>";
			        $retlist .= "Yes";
			        $retlist .= '</a>';
			    }
			    else if($leadstatus == "N") {
			        $retlist .= "<a href='applicantsSearch.php?ApplicationID=" . $appid . "&RequestID=" . $requestid . "&tab_action=lead-generator' data-toggle='tooltip' data-placement='right'";
			        $retlist .= " data-html='true' title='".$lead_name."'>";
			        $retlist .= "No";
			        $retlist .= '</a>';
			    }    
			    else {
			        $retlist .= "";
			    }
			    $retlist .= '</td>';
			}
			else {
			    $retlist .= '<td>&nbsp;</td>';
			}
			
			$retlist .= '<td>';
			foreach ($onb_status_list as $onboard_form_id=>$onboard_status) {
			    $retlist .= $onboard_form_id . ' - ' . $onboard_status . '<br>';
			}
			$retlist .= '</td>';
			
			$retlist .= '</tr>';
		
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end while
	}
	
	
	$retlist .= '</table>';
	$retlist .= '</div>' . "\n";
	
	return $retlist;
} // end function
?>
