<?php 
require_once '../Configuration.inc';

$status     =   $_REQUEST['status'];
$type       =   $_REQUEST['type'];

if($type == "VET") {
    $set_info   =   array("VeteranEditStatus    =   :Status");
}
else if($type == "DIS") {
    $set_info   =   array("DisabledEditStatus   =   :Status");
}
else if($type == "AA") {
    $set_info   =   array("AAEditStatus         =   :Status");
}

$params     =   array(":Status"=>$status, ":OrgID"=>$OrgID, ":ApplicationID"=>$_REQUEST['ApplicationID'], ":RequestID"=>$_REQUEST['RequestID']);
$where_info =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");

$ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));
?>