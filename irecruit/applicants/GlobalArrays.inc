<?php
/**
 * Match Fields Of Two Tables
 */
$falias["first"] = "FirstName";
$falias["last"] = "LastName";
$falias["state"] = "State";
$falias["address"] = "Address1";
$falias["city"] = "City";
$falias["zip"] = "ZipCode";
$falias["email"] = "EmailAddress";
$falias["signature"] = "signature";
 
 
/*
 * Citizen Ship Status
*/
$citizenshipstatus = array("citizen"=>"4", "noncitizen"=>"5", "resident"=>"6", "alien"=>"7");

/*
 * List B Documents
*/
$listbdocuments['1'] = "Driver's license or ID card issued by a U.S. state or outlying possession";
$listbdocuments['2'] = "ID card issued by a U.S. federal, state or local government agency";
$listbdocuments['3'] = "School ID card";
$listbdocuments['4'] = "Voter registration card";
$listbdocuments['5'] = "U.S. military card or draft record";
$listbdocuments['6'] = "Military dependent's ID card";
$listbdocuments['7'] = "U.S. Coast Guard Merchant Mariner Card";
$listbdocuments['8'] = "Native American tribal document";
$listbdocuments['9'] = "Driver's license issued by a Canadian government authority";
$listbdocuments['10'] = "School record or report card (under age 18)";
$listbdocuments['11'] = "Clinic, doctor or hospital record (under age 18)";
$listbdocuments['12'] = "Day-care or nursery school record (under age 18)";
$listbdocuments['21'] = "Minor under age 18 without a List B document";
$listbdocuments['22'] = "Special Placement";



/*
 * List C Documents
*/
$listcdocuments['13'] = "Social Security Card";
$listcdocuments['14'] = "Certification of Birth Abroad (Form FS-545)";
$listcdocuments['15'] = "Certification of Report of Birth (Form DS-1350)";
$listcdocuments['16'] = "U.S. birth certificate (original or certified copy)";
$listcdocuments['17'] = "Native American tribal document";
$listcdocuments['18'] = "U.S. Citizen ID Card (Form I-197)";
$listcdocuments['19'] = "ID Card for Use of Resident Citizen in the United States (Form I-179)";
$listcdocuments['20'] = "Employment authorization document issued by the U.S. Department of Homeland Security";
$listcdocuments['21'] = "Minor under age 18 without a List B document";
$listcdocuments['22'] = "Special Placement";

$maindocumenttype["11"] = "Arrival/Departure Record (Form I-94) with temporary I-551 stamp or refugee admission stamp(receipt)";
$maindocumenttype["13"] = "Permanent Resident Card or Alien Registration Receipt Card (Form I-551)";
$maindocumenttype["17"] = "Employment Authorization Document (Form I-776)";
$maindocumenttype["24"] = "Foreign Passport with Arrival/Departure Record (Form I-94)";
$maindocumenttype["25"] = "Foreign Passport with temporary I-551 stamp or printed notation on a MRIV";
$maindocumenttype["28"] = "List B and C Documents";
$maindocumenttype["29"] = "U.S. Passport or Passport Card";

/*
 * ListA Document Types
 */
$listadocumenttypes["1"] = "U.S. Passport or U.S. Passport Card";
$listadocumenttypes["2"] = "Permanent Resident Card or Alien Registration Receipt Card (Form I-551)";
$listadocumenttypes["3"] = "Foreign passport that contains a temporary I-551 stamp";
$listadocumenttypes["4"] = "Employment Authorization Document that contains a photograph (Form I-766)";
$listadocumenttypes["5"] = "Foreign passport and Form I-94 or Form I-94A";
$listadocumenttypes["6"] = "Passport from the Federated States of Micronesia (FSM)or the Republic of the Marshall Islands (RMI)";

/*
 * ListB Document Types
 */
$listbdocumenttypes["1"] = "Driver's license or ID card issued by a State or outlying possession of the United States";
$listbdocumenttypes["2"] = "ID card issued by federal, state or local government agencies or entities";
$listbdocumenttypes["3"] = "School ID card with a photograph";
$listbdocumenttypes["4"] = "Voter's registration card";
$listbdocumenttypes["5"] = "U.S. Military card or draft record";
$listbdocumenttypes["6"] = "Military dependent's ID card";
$listbdocumenttypes["7"] = "U.S. Coast Guard Merchant Mariner Card";
$listbdocumenttypes["8"] = "Native American tribal document";
$listbdocumenttypes["9"] = "Driver's license issued by a Canadian government authority";
$listbdocumenttypes["10"] = "School record or report card";
$listbdocumenttypes["11"] = "Clinic, doctor, or hospital record";
$listbdocumenttypes["12"] = "Day-care or nursery school record";

/*
 * ListC Document Types
 */
$listcdocumenttypes["1"] = "A Social Security Account Number card";
$listcdocumenttypes["2"] = "Certification of Birth Abroad issued by the Department of State (Form FS-545)";
$listcdocumenttypes["3"] = "Certification of Report of Birth issued by the Department of State (Form DS-1350)";
$listcdocumenttypes["4"] = "Original or certified copy of birth certificate issued by a State bearing an official seal";
$listcdocumenttypes["5"] = "Native American tribal document";
$listcdocumenttypes["6"] = "U.S. Citizen ID Card (Form I-197)";
$listcdocumenttypes["7"] = "Identification Card for Use of Resident Citizen in the United States (Form I-179)";
$listcdocumenttypes["8"] = "Employment authorization document issued by the Department of Homeland Security";

/*
 * USA States List
*/
$usastates	=	array('AL'=>'Alabama', 'AK'=>'Alaska','AZ'=>'Arizona','AR'=>'Arkansas',
				'CA'=>'California', 'CO'=>'Colorado', 'CT'=>'Connecticut', 'DE'=>'Delaware',
				'DC'=>'District Of Columbia', 'FL'=>'Florida', 'GA'=>'Georgia',
				'HI'=>'Hawaii', 'ID'=>'Idaho', 'IL'=>'Illinois', 'IN'=>'Indiana',
				'IA'=>'Iowa', 'KS'=>'Kansas', 'KY'=>'Kentucky', 'LA'=>'Louisiana',
				'ME'=>'Maine', 'MD'=>'Maryland', 'MA'=>'Massachusetts', 'MI'=>'Michigan',
				'MN'=>'Minnesota', 'MS'=>'Mississippi', 'MO'=>'Missouri', 'MT'=>'Montana',
				'NE'=>'Nebraska', 'NV'=>'Nevada', 'NH'=>'New Hampshire', 'NJ'=>'New Jersey',
				'NM'=>'New Mexico', 'NY'=>'New York', 'NC'=>'North Carolina', 'ND'=>'North Dakota',
				'OH'=>'Ohio', 'OK'=>'Oklahoma', 'OR'=>'Oregon', 'PA'=>'Pennsylvania', 'RI'=>'Rhode Island',
				'SC'=>'South Carolina', 'SD'=>'South Dakota', 'TN'=>'Tennessee', 'TX'=>'Texas', 'UT'=>'Utah',
				'VT'=>'Vermont', 'VA'=>'Virginia', 'WA'=>'Washington', 'WV'=>'West Virginia', 
				'WI'=>'Wisconsin', 'WY'=>'Wyoming');

$usastates_json = json_encode($usastates);

/*
 * Country List
 */
$countries = array("AF" => "Afghanistan",
		"AX" => "�land Islands",
		"AL" => "Albania",
		"DZ" => "Algeria",
		"AS" => "American Samoa",
		"AD" => "Andorra",
		"AO" => "Angola",
		"AI" => "Anguilla",
		"AQ" => "Antarctica",
		"AG" => "Antigua and Barbuda",
		"AR" => "Argentina",
		"AM" => "Armenia",
		"AW" => "Aruba",
		"AU" => "Australia",
		"AT" => "Austria",
		"AZ" => "Azerbaijan",
		"BS" => "Bahamas",
		"BH" => "Bahrain",
		"BD" => "Bangladesh",
		"BB" => "Barbados",
		"BY" => "Belarus",
		"BE" => "Belgium",
		"BZ" => "Belize",
		"BJ" => "Benin",
		"BM" => "Bermuda",
		"BT" => "Bhutan",
		"BO" => "Bolivia",
		"BA" => "Bosnia and Herzegovina",
		"BW" => "Botswana",
		"BV" => "Bouvet Island",
		"BR" => "Brazil",
		"IO" => "British Indian Ocean Territory",
		"BN" => "Brunei Darussalam",
		"BG" => "Bulgaria",
		"BF" => "Burkina Faso",
		"BI" => "Burundi",
		"KH" => "Cambodia",
		"CM" => "Cameroon",
		"CA" => "Canada",
		"CV" => "Cape Verde",
		"KY" => "Cayman Islands",
		"CF" => "Central African Republic",
		"TD" => "Chad",
		"CL" => "Chile",
		"CN" => "China",
		"CX" => "Christmas Island",
		"CC" => "Cocos (Keeling) Islands",
		"CO" => "Colombia",
		"KM" => "Comoros",
		"CG" => "Congo",
		"CD" => "Congo, The Democratic Republic of The",
		"CK" => "Cook Islands",
		"CR" => "Costa Rica",
		"CI" => "Cote D'ivoire",
		"HR" => "Croatia",
		"CU" => "Cuba",
		"CY" => "Cyprus",
		"CZ" => "Czech Republic",
		"DK" => "Denmark",
		"DJ" => "Djibouti",
		"DM" => "Dominica",
		"DO" => "Dominican Republic",
		"EC" => "Ecuador",
		"EG" => "Egypt",
		"SV" => "El Salvador",
		"GQ" => "Equatorial Guinea",
		"ER" => "Eritrea",
		"EE" => "Estonia",
		"ET" => "Ethiopia",
		"FK" => "Falkland Islands (Malvinas)",
		"FO" => "Faroe Islands",
		"FJ" => "Fiji",
		"FI" => "Finland",
		"FR" => "France",
		"GF" => "French Guiana",
		"PF" => "French Polynesia",
		"TF" => "French Southern Territories",
		"GA" => "Gabon",
		"GM" => "Gambia",
		"GE" => "Georgia",
		"DE" => "Germany",
		"GH" => "Ghana",
		"GI" => "Gibraltar",
		"GR" => "Greece",
		"GL" => "Greenland",
		"GD" => "Grenada",
		"GP" => "Guadeloupe",
		"GU" => "Guam",
		"GT" => "Guatemala",
		"GG" => "Guernsey",
		"GN" => "Guinea",
		"GW" => "Guinea-bissau",
		"GY" => "Guyana",
		"HT" => "Haiti",
		"HM" => "Heard Island and Mcdonald Islands",
		"VA" => "Holy See (Vatican City State)",
		"HN" => "Honduras",
		"HK" => "Hong Kong",
		"HU" => "Hungary",
		"IS" => "Iceland",
		"IN" => "India",
		"ID" => "Indonesia",
		"IR" => "Iran, Islamic Republic of",
		"IQ" => "Iraq",
		"IE" => "Ireland",
		"IM" => "Isle of Man",
		"IL" => "Israel",
		"IT" => "Italy",
		"JM" => "Jamaica",
		"JP" => "Japan",
		"JE" => "Jersey",
		"JO" => "Jordan",
		"KZ" => "Kazakhstan",
		"KE" => "Kenya",
		"KI" => "Kiribati",
		"KP" => "Korea, Democratic People's Republic of",
		"KR" => "Korea, Republic of",
		"KW" => "Kuwait",
		"KG" => "Kyrgyzstan",
		"LA" => "Lao People's Democratic Republic",
		"LV" => "Latvia",
		"LB" => "Lebanon",
		"LS" => "Lesotho",
		"LR" => "Liberia",
		"LY" => "Libyan Arab Jamahiriya",
		"LI" => "Liechtenstein",
		"LT" => "Lithuania",
		"LU" => "Luxembourg",
		"MO" => "Macao",
		"MK" => "Macedonia, The Former Yugoslav Republic of",
		"MG" => "Madagascar",
		"MW" => "Malawi",
		"MY" => "Malaysia",
		"MV" => "Maldives",
		"ML" => "Mali",
		"MT" => "Malta",
		"MH" => "Marshall Islands",
		"MQ" => "Martinique",
		"MR" => "Mauritania",
		"MU" => "Mauritius",
		"YT" => "Mayotte",
		"MX" => "Mexico",
		"FM" => "Micronesia, Federated States of",
		"MD" => "Moldova, Republic of",
		"MC" => "Monaco",
		"MN" => "Mongolia",
		"ME" => "Montenegro",
		"MS" => "Montserrat",
		"MA" => "Morocco",
		"MZ" => "Mozambique",
		"MM" => "Myanmar",
		"NA" => "Namibia",
		"NR" => "Nauru",
		"NP" => "Nepal",
		"NL" => "Netherlands",
		"AN" => "Netherlands Antilles",
		"NC" => "New Caledonia",
		"NZ" => "New Zealand",
		"NI" => "Nicaragua",
		"NE" => "Niger",
		"NG" => "Nigeria",
		"NU" => "Niue",
		"NF" => "Norfolk Island",
		"MP" => "Northern Mariana Islands",
		"NO" => "Norway",
		"OM" => "Oman",
		"PK" => "Pakistan",
		"PW" => "Palau",
		"PS" => "Palestinian Territory, Occupied",
		"PA" => "Panama",
		"PG" => "Papua New Guinea",
		"PY" => "Paraguay",
		"PE" => "Peru",
		"PH" => "Philippines",
		"PN" => "Pitcairn",
		"PL" => "Poland",
		"PT" => "Portugal",
		"PR" => "Puerto Rico",
		"QA" => "Qatar",
		"RE" => "Reunion",
		"RO" => "Romania",
		"RU" => "Russian Federation",
		"RW" => "Rwanda",
		"SH" => "Saint Helena",
		"KN" => "Saint Kitts and Nevis",
		"LC" => "Saint Lucia",
		"PM" => "Saint Pierre and Miquelon",
		"VC" => "Saint Vincent and The Grenadines",
		"WS" => "Samoa",
		"SM" => "San Marino",
		"ST" => "Sao Tome and Principe",
		"SA" => "Saudi Arabia",
		"SN" => "Senegal",
		"RS" => "Serbia",
		"SC" => "Seychelles",
		"SL" => "Sierra Leone",
		"SG" => "Singapore",
		"SK" => "Slovakia",
		"SI" => "Slovenia",
		"SB" => "Solomon Islands",
		"SO" => "Somalia",
		"ZA" => "South Africa",
		"GS" => "South Georgia and The South Sandwich Islands",
		"ES" => "Spain",
		"LK" => "Sri Lanka",
		"SD" => "Sudan",
		"SR" => "Suriname",
		"SJ" => "Svalbard and Jan Mayen",
		"SZ" => "Swaziland",
		"SE" => "Sweden",
		"CH" => "Switzerland",
		"SY" => "Syrian Arab Republic",
		"TW" => "Taiwan, Province of China",
		"TJ" => "Tajikistan",
		"TZ" => "Tanzania, United Republic of",
		"TH" => "Thailand",
		"TL" => "Timor-leste",
		"TG" => "Togo",
		"TK" => "Tokelau",
		"TO" => "Tonga",
		"TT" => "Trinidad and Tobago",
		"TN" => "Tunisia",
		"TR" => "Turkey",
		"TM" => "Turkmenistan",
		"TC" => "Turks and Caicos Islands",
		"TV" => "Tuvalu",
		"UG" => "Uganda",
		"UA" => "Ukraine",
		"AE" => "United Arab Emirates",
		"GB" => "United Kingdom",
		"US" => "United States",
		"UM" => "United States Minor Outlying Islands",
		"UY" => "Uruguay",
		"UZ" => "Uzbekistan",
		"VU" => "Vanuatu",
		"VE" => "Venezuela",
		"VN" => "Viet Nam",
		"VG" => "Virgin Islands, British",
		"VI" => "Virgin Islands, U.S.",
		"WF" => "Wallis and Futuna",
		"EH" => "Western Sahara",
		"YE" => "Yemen",
		"ZM" => "Zambia",
		"ZW" => "Zimbabwe");

$countries_json = json_encode($countries);

/*
 * Issuing Authorities
 */
$issuingauthorities = array("AL"=>"Alabama",
		"AK"=>"Alaska",
		"AS"=>"American Samoa",
		"AZ"=>"Arizona",
		"AR"=>"Arkansas",
		"CA"=>"California",
		"CO"=>"Colorado",
		"CT"=>"Connecticut",
		"DE"=>"Delaware",
		"DC"=>"District of Columbia",
		"FL"=>"Florida",
		"GA"=>"Georgia",
		"GU"=>"Guam",
		"HI"=>"Hawaii",
		"ID"=>"Idaho",
		"IL"=>"Illinois",
		"IN"=>"Indiana",
		"IA"=>"Iowa",
		"KS"=>"Kansas",
		"KY"=>"Kentucky",
		"LA"=>"Louisiana",
		"ME"=>"Maine",
		"MD"=>"Maryland",
		"MA"=>"Massachusetts",
		"MI"=>"Michigan",
		"MN"=>"Minnesota",
		"MS"=>"Mississippi",
		"MO"=>"Missouri",
		"MT"=>"Montana",
		"NE"=>"Nebraska",
		"NV"=>"Nevada",
		"NH"=>"New Hampshire",
		"NJ"=>"New Jersey",
		"NM"=>"New Mexico",
		"NY"=>"New York",
		"NC"=>"North Carolina",
		"ND"=>"North Dakota",
		"MP"=>"Northern Mariana Islands",
		"OH"=>"Ohio",
		"OK"=>"Oklahoma",
		"OR"=>"Oregon",
		"PA"=>"Pennsylvania",
		"PR"=>"Puerto Rico",
		"RI"=>"Rhode Island",
		"SC"=>"South Carolina",
		"SD"=>"South Dakota",
		"TN"=>"Tennessee",
		"TX"=>"Texas",
		"UT"=>"Utah",
		"VT"=>"Vermont",
		"VI"=>"Virgin Islands",
		"VA"=>"Virginia",
		"WA"=>"Washington",
		"WV"=>"West Virginia",
		"WI"=>"Wisconsin",
		"WY"=>"Wyoming");