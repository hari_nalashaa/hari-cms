<?php
require_once '../Configuration.inc';

$caregiver_id           =   $_GET['caregiver_id'];
$ApplicationID          =   $_GET['app_id'];
$RequestID              =   $_GET['req_id'];
$MatrixCareCompanyID    =   $_GET['matrixcare_companyid'];


require_once IRECRUIT_DIR . 'onboard/MatrixCareLogin.inc';

$web_form_data_contact_map["ecaddress1"]        =   "address1";
$web_form_data_contact_map["ecaddress21"]       =   "address2";
$web_form_data_contact_map["ecaddress2"]        =   "address1";
$web_form_data_contact_map["ecaddress22"]       =   "address2";
$web_form_data_contact_map["ecaddress3"]        =   "address1";
$web_form_data_contact_map["ecaddress23"]       =   "address2";

$web_form_data_contact_map["eccity1"]           =   "city";
$web_form_data_contact_map["eccity2"]           =   "city";
$web_form_data_contact_map["eccity3"]           =   "city";

$web_form_data_contact_map["eccountry1"]        =   "country";
$web_form_data_contact_map["eccountry2"]        =   "country";
$web_form_data_contact_map["eccountry3"]        =   "country";

$web_form_data_contact_map["eccounty1"]         =   "county";
$web_form_data_contact_map["eccounty2"]         =   "county";
$web_form_data_contact_map["eccounty3"]         =   "county";

$web_form_data_contact_map["ecemailaddress1"]   =   "email";
$web_form_data_contact_map["ecemailaddress2"]   =   "email";
$web_form_data_contact_map["ecemailaddress3"]   =   "email";

$web_form_data_contact_map["eclname1"]          =   "lastName";
$web_form_data_contact_map["eclname2"]          =   "lastName";
$web_form_data_contact_map["eclname3"]          =   "lastName";

$web_form_data_contact_map["ecname1"]           =   "firstName";
$web_form_data_contact_map["ecname2"]           =   "firstName";
$web_form_data_contact_map["ecname3"]           =   "firstName";

$web_form_data_contact_map["ecphone1a"]         =   "phone1";
$web_form_data_contact_map["ecphone1atype"]     =   "phone1Type";
$web_form_data_contact_map["ecphone1b"]         =   "phone1";
$web_form_data_contact_map["ecphone1btype"]     =   "phone1Type";
$web_form_data_contact_map["ecphone1c"]         =   "phone1";
$web_form_data_contact_map["ecphone1ctype"]     =   "phone1Type";

$web_form_data_contact_map["ecphone2a"]         =   "phone2";
$web_form_data_contact_map["ecphone2atype"]     =   "phone2Type";
$web_form_data_contact_map["ecphone2b"]         =   "phone2";
$web_form_data_contact_map["ecphone2btype"]     =   "phone2Type";
$web_form_data_contact_map["ecphone2c"]         =   "phone2";
$web_form_data_contact_map["ecphone2ctype"]     =   "phone2Type";

$web_form_data_contact_map["ecrelationship1"]   =   "relation";
$web_form_data_contact_map["ecrelationship2"]   =   "relation";
$web_form_data_contact_map["ecrelationship3"]   =   "relation";

$web_form_data_contact_map["ecstate1"]          =   "state";
$web_form_data_contact_map["ecstate2"]          =   "state";
$web_form_data_contact_map["ecstate3"]          =   "state";

$web_form_data_contact_map["eczipcode1"]        =   "postalCode";
$web_form_data_contact_map["eczipcode2"]        =   "postalCode";
$web_form_data_contact_map["eczipcode3"]        =   "postalCode";

//Get JobApplicationsDetail Information
$rowjobapp = $ApplicationsObj->getJobApplicationsDetailInfo("MatrixCaregiverID, MatrixCaregiverStatus, MatrixCaregiverContactStatus, MatrixCaregiverContacts", $OrgID, $ApplicationID, $RequestID);

//Get matrix care contact information
$matrixcare_contactinfo     =   $MatrixCareObj->getMatrixCareContactInfo($OrgID, $RequestID, $ApplicationID);
$matrixcare_contact_results =   $matrixcare_contactinfo['results'];
$matrixcare_contact_count   =   count($matrixcare_contactinfo['results']);

$mc_que_ans = array();
for($mc = 0; $mc < $matrixcare_contact_count; $mc++) {
    $mc_que_ans[$matrixcare_contact_results[$mc]['QuestionID']] = $matrixcare_contact_results[$mc]['Answer'];
}

$mc_contact_info            =   array();

$mci = 0;
for($mc = 0; $mc < $matrixcare_contact_count; $mc++) {

    $mc_question = $web_form_data_contact_map[$matrixcare_contact_results[$mc]['QuestionID']];
    
    if($mc % 19 == 0 && $mc > 18) {
        $mci++;
    }
    
    if($mc % 19 == 0) {
        $mc_contact_info[$mci]["typeId"] = 1;    	
    }
    
    if(substr($matrixcare_contact_results[$mc]['QuestionID'], 0, -1) == "ecphone1a") {
        $mc_contact_info[$mci]["phone1"] = "(".$mc_que_ans["ecphone1a1"].")".$mc_que_ans["ecphone1a2"]."-".$mc_que_ans["ecphone1a3"];
    }
    else if(substr($matrixcare_contact_results[$mc]['QuestionID'], 0, -1) == "ecphone1b") {
        $mc_contact_info[$mci]["phone1"] = "(".$mc_que_ans["ecphone1b1"].")".$mc_que_ans["ecphone1b2"]."-".$mc_que_ans["ecphone1b3"];
    }
    else if(substr($matrixcare_contact_results[$mc]['QuestionID'], 0, -1) == "ecphone1c") {
        $mc_contact_info[$mci]["phone1"] = "(".$mc_que_ans["ecphone1c1"].")".$mc_que_ans["ecphone1c2"]."-".$mc_que_ans["ecphone1c3"];
    }
    else if(substr($matrixcare_contact_results[$mc]['QuestionID'], 0, -1) == "ecphone2a") {
        $mc_contact_info[$mci]["phone2"] = $mc_que_ans["ecphone2a1"].$mc_que_ans["ecphone2a2"].$mc_que_ans["ecphone2a3"];
    }
    else if(substr($matrixcare_contact_results[$mc]['QuestionID'], 0, -1) == "ecphone2b") {
        $mc_contact_info[$mci]["phone2"] = $mc_que_ans["ecphone2b1"].$mc_que_ans["ecphone2b2"].$mc_que_ans["ecphone2b3"];
    }
    else if(substr($matrixcare_contact_results[$mc]['QuestionID'], 0, -1) == "ecphone2c") {
        $mc_contact_info[$mci]["phone2"] = $mc_que_ans["ecphone2c1"].$mc_que_ans["ecphone2c2"].$mc_que_ans["ecphone2c3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1atype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1btype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1ctype"                
            ) {
        $mc_contact_info[$mci]["phone1Type"] = $mc_que_ans[$matrixcare_contact_results[$mc]['QuestionID']];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2atype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2btype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2ctype"
            ) {
        $mc_contact_info[$mci]["phone2Type"] = $mc_que_ans[$matrixcare_contact_results[$mc]['QuestionID']];
    }
    else {
        if($mc_question != "") {
            $mc_contact_info[$mci][$mc_question] = $matrixcare_contact_results[$mc]['Answer'];
        }
    }
}

$mcc_list = json_decode($rowjobapp['MatrixCaregiverContacts'], true);

if($mcc_list['C1'] == ""
    || $mcc_list['C2'] == ""
    || $mcc_list['C3'] == "") {
    
    $ckj = 0;
    if($caregiver_id != "") {
        foreach($mcc_list as $cc=>$cv) {
            if($cc == "C1") {
                $ckj = 0;
            }
            else if($cc == "C2") {
                $ckj = 1;
            }
            else if($cc == "C3") {
                $ckj = 2;
            }
            
            //Add if value doesn't exist
            if($cv == "" || $cv == NULL) {
                $mc_contact_info[$ckj]['country'] = 240;
                $contact_data = $mc_contact_info[$ckj];
                
                $headers_list               =   array("access_token"=>$matrixcare_info['access_token'], "token_type"=>$matrixcare_info['token_type']);
                $matrixcaregiver_url        =   $MatrixCareObj->api_router_url.$matrixcare_info['tenant']."/"."caregivers/".$caregiver_id."/contacts";
                $matrixcaregiver_contacts   =   $MatrixCareObj->setMatrixCareContactInfo($matrixcaregiver_url, $contact_data, $headers_list);
                $matrixcaregiver_contacts   =   json_decode($matrixcaregiver_contacts, true);
                
                $mcc_list[$cc]              =   ($matrixcaregiver_contacts['id'] == NULL || $matrixcaregiver_contacts['id'] == "") ? "" : $matrixcaregiver_contacts['id'];
    
                $params_info                =   array(":MatrixCaregiverContacts"=>json_encode($mcc_list), ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
                $set_info                   =   array("MatrixCaregiverContacts = :MatrixCaregiverContacts");
                $where_info                 =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
                $ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params_info));
            }
            else {  //Edit if value exists
                $mc_contact_info[$ckj]['country'] = 240;
                $contact_data = $mc_contact_info[$ckj];

                $headers_list               =   array("access_token"=>$matrixcare_info['access_token'], "token_type"=>$matrixcare_info['token_type']);
                $matrixcaregiver_url        =   $MatrixCareObj->api_router_url.$matrixcare_info['tenant']."/"."caregivers/".$caregiver_id."/contacts/".$cv;
                $matrixcaregiver_contacts   =   $MatrixCareObj->editMatrixCareContactInfo($matrixcaregiver_url, $contact_data, $headers_list);
                $matrixcaregiver_contacts   =   json_decode($matrixcaregiver_contacts, true);
                
            }
        }
    }
    
}

//Get JobApplicationsDetail Information
$rowjobapp  = $ApplicationsObj->getJobApplicationsDetailInfo("MatrixCaregiverID, MatrixCaregiverStatus, MatrixCaregiverContactStatus, MatrixCaregiverContacts", $OrgID, $ApplicationID, $RequestID);
$mcc_list   = json_decode($rowjobapp['MatrixCaregiverContacts'], true);

if($mcc_list['C1'] != "" && $mcc_list['C2'] != "" && $mcc_list['C3'] != "") {
    echo "SUCCESS";
}
else {
	echo "FAILED";
}
?>