<?php 
$processapp = <<<END
<tr><td colspan="100%" height="60" valign="middle">
<table border="0" cellspacing="3" cellpadding="5">
<tr><td width="150" align="right">
<select name="action" class="form-control">
END;

$processapp .= '<option value="">Please Select</option>';

$TemplateObj->SEARCHiii = $SEARCHiii = 0;
if ($permit ['Applicants_Update_Status'] == 1) {
	$processapp .= '<option value="updatestatus">Update Status</option>';
	$SEARCHiii ++;
}

if ($permit ['Applicants_Contact'] == 1) {
	$processapp .= '<option value="contactapplicant">Contact Applicants</option>';
	$SEARCHiii ++;
}

if ($permit ['Applicants_Forward'] == 1) {
	$processapp .= '<option value="forwardapplicant">Forward Applicants</option>';
	$SEARCHiii ++;
}

if (($permit ['Applicants_Delete'] == 1) && ($USERROLE == 'master_admin')) {
	$processapp .= '<option value="deleteapplicant">Delete Applicants</option>';
	$SEARCHiii ++;
}

if ($permit ['Applicants_Assign'] == 1) {
	$processapp .= '<option value="managerequisition">Assign Requisitions</option>';
	$SEARCHiii ++;
}

$processapp .= '<option value="send_bulk_sms">Send Text</option>';
$SEARCHiii ++;

$processapp .= '<option value="comparative_analysis">Comparative Analysis</option>';
$SEARCHiii ++;

$processapp .= '<option value="downloaddocumentszip">Download Documents Zip</option>';
$SEARCHiii ++;

$processapp .= <<<END
</select>
</td><td width="300">
<input type="hidden" name="multi" value="Y">&nbsp;
<input type="submit" value="Process Applicants" class="btn btn-primary" onClick="return listConfirm(document.multipleapps.appid,document.multipleapps.action)">
</td></tr>
</table>
</td></tr>
END;

$TemplateObj->SEARCHiii = $SEARCHiii;
$TemplateObj->processapp = $processapp;
?>
