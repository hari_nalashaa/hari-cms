<?php
$to = $applicantemail;
$subject = $subject;

global $OrganizationsObj, $OrganizationDetailsObj, $IrecruitUsersObj, $PHPMailerObj;

//Clear custom properties
G::Obj('PHPMailer')->clearCustomProperties();
G::Obj('PHPMailer')->clearCustomHeaders();

// Set who the message is to be sent to
G::Obj('PHPMailer')->addAddress ( $to );
// Set the subject line
G::Obj('PHPMailer')->Subject = $subject;
// Content Type Is HTML
G::Obj('PHPMailer')->ContentType = 'text/html';

//Get Email and Email Verified
$OE = G::Obj('OrganizationDetails')->getEmailAndEmailVerified($OrgID, $MultiOrgID);
//Get Org Name
$OrgName = G::Obj('OrganizationDetails')->getOrganizationName($OrgID, $MultiOrgID);

if (isset($_POST ['From'])) {

    $opt_list	=	array();
    
    //Set where users email
    $where_users_email = array("OrgID = :OrgID", "UserID = :UserID");
    //Set parameters
    $params_users_email = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
    //Get UsersInformation
    $results = G::Obj('IrecruitUsers')->getUserInformation("UserID, EmailAddress, FirstName, LastName", $where_users_email, "", array($params_users_email));
    $USERS = $results['results'][0];
    
    if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
        $opt_list[$OE ['Email']] = $OrgName;
    }

    if(is_array($results['results'])) {
        foreach($results['results'] as $USERS) {
            $opt_list[$USERS ['EmailAddress']] = $USERS ['FirstName'] . ' ' . $USERS ['LastName'];
        } // end foreach
    }

    if($_POST ['From'] == "") {
        $_POST ['From'] = $USERS ['EmailAddress'];
        $opt_list[""] = $USERS ['FirstName'] . ' ' . $USERS ['LastName'];
    }
    
	// Set who the message is to be sent from
	G::Obj('PHPMailer')->setFrom ( $_POST ['From'], $opt_list[$_POST ['From']] );
	// Set an alternative reply-to address
	G::Obj('PHPMailer')->addReplyTo ( $_POST ['From'], $opt_list[$_POST ['From']] );

} else {

   if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
	// Set who the message is to be sent from
	G::Obj('PHPMailer')->setFrom ( $OE ['Email'], $OrgName );
	// Set an alternative reply-to address
	G::Obj('PHPMailer')->addReplyTo ( $OE ['Email'], $OrgName );
   }
   
}

if (count($_POST ['BCC'])) {
    foreach($_POST ['BCC'] as $BCCEmailKey=>$BCCEmailValue) {
        G::Obj('PHPMailer')->addBCC($BCCEmailValue, "");
    }
}

// convert HTML into a basic plain-text alternative body
G::Obj('PHPMailer')->msgHTML ( $body );

$cp = explode ( ':', $companyattachments );
$cpcnt = count ( $cp ) - 1;

for($i = 0; $i < $cpcnt; $i ++) {
	
    $filename = getAttachmentName ( $OrgID, $cp [$i] );
	
	$file = IRECRUIT_DIR . 'vault/' . $OrgID . '/employmentdocuments/attachments/' . $filename;
	
	// Attach policies
	G::Obj('PHPMailer')->addAttachment ( $file );
}

if (DEVELOPMENT != "Y") {
	// send the message, check for errors, anyway we will not execute this file in browser
	if (! G::Obj('PHPMailer')->send ()) {
		Logger::writeMessage(ROOT_DIR."logs/mail-errors/".date('Y-m-d')."_errors.log", "Unable to send mail to user Email:$to. FileName: ContactEmail.inc", "a+");
	}
}
?>