<?php
require_once '../Configuration.inc';

$multi = isset($_REQUEST['multi']) ? $_REQUEST['multi'] : '';
$multipleapps = isset($_REQUEST['multipleapps']) ? $_REQUEST['multipleapps'] : '';

require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';
require_once IRECRUIT_DIR . 'applicants/ApplicantsMultipleApps.inc';

//Process manage requisition
include IRECRUIT_DIR . 'applicants/ProcessManageRequisition.inc';
?>