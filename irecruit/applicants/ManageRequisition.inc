<?php
require_once IRECRUIT_DIR . 'applicants/ApplicantsMultipleApps.inc';

//Process manage requisition
include IRECRUIT_DIR . 'applicants/ProcessManageRequisition.inc';
?>
<script language="JavaScript" type="text/javascript">
function processManageRequisition()
{
	var form = $('#frmManageRequisitions');

    $("#manage_req_msg_top").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
    //$("#manage_req_msg_bottom").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	var request = $.ajax({
		method: "POST",
 		url: 'applicants/processManageRequisition.php', 
		type: "POST",
		data: form.serialize(),
		success: function(data) {
			$("#manage_req_msg_top").html(data);
			//$("#manage_req_msg_bottom").html(data);
   		}
	});
}
</script>
<script language="JavaScript" type="text/javascript" src="js/selectbox.js"></script>
<?php
require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';

echo '<div id="manage_req_msg_top" style="color:#398ab9"></div>';

//Contact Affected Applicants
echo '<div id="manage_req_affected_applicants">';
echo '<br>The following applications will be affected:<br>';
displayList ( $multipleapps, $OrgID, $permit );
echo '</div>';

echo <<<END
<form method="post" action="applicants.php" name="frmManageRequisitions" id="frmManageRequisitions">
<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">
<tr><td valign="top" colspan="100%"><strong>Add the following Requisitions:</strong></td></tr>
<tr><td>
<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered"><tr><td>
<strong>Available:</strong>
<br>
<select name="requisitionlist[]" multiple size="10">
END;

$reqids = "";

//Set where condition
$where      =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
//Get Job Applications Information
$resultsIN  =   G::Obj('Applications')->getJobApplicationsInfo("RequestID", $where, '', '', array($params));

if(is_array($resultsIN['results'])) {
	foreach($resultsIN['results'] as $JA) {
		$reqids .= "'" . $JA ['RequestID'] . "',";
	}	
}

$reqids = substr ( $reqids, 0, - 1 );

//Set Requisitions Where Condition
$req_where  =   array("OrgID = :OrgID", "Active = 'Y'");
//Set Requisitions Parameters
$req_params =   array(":OrgID"=>$OrgID);

if ($reqids) {
	$req_where[] = "RequestID NOT IN ($reqids)";
} // end if reqids
//Get Requisitions Information
$results = $RequisitionsObj->getRequisitionInformation("RequestID, Title", $req_where, "", "", array($req_params));

if(is_array($results['results'])) {
	foreach($results['results'] as $CL) {
	
		$requestid        = $CL['RequestID'];
		$filename         = $CL['Title'];
		$multiorgid_req   = $RequisitionDetailsObj->getMultiOrgID($OrgID, $requestid);
		
		echo '<option value="' . $requestid . '">';
		if ($feature ['MultiOrg'] == "Y") {
			echo $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $requestid ) . ' - ';
		} // end feature
		echo $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $requestid ) . ' - ' . $filename . '</option>';
	} // end foreach
}

echo <<<END
</select></td>
</tr>
END;

echo <<<END
<tr><td colspan="100%" height="60" valign="middle" align="center">
END;

if ($multipleapps) {
	echo <<<END
<input type="hidden" name="multi" value="Y">
<input type="hidden" name="multipleapps" value="$multipleapps">
END;
} else {
	echo <<<END
<input type="hidden" name="ApplicationID" value="$ApplicationID">
<input type="hidden" name="RequestID" value="$RequestID">
END;
}

echo <<<END
Make these requisitions processable for single mode users: <select name="Distinction">
END;

echo '<option value="S">No</option>';
echo '<option value="P">Yes</option>';

echo <<<END
</select>
END;

echo <<<END
<input type="hidden" name="movepattern" value="">
<input type="hidden" name="action" value="managerequisition">
<input type="hidden" name="process" value="Y">
<input type="hidden" name="process_manage_requisition" value="YES">
<input type="button" class="btn btn-primary" value="Add Requisitions" onclick="processManageRequisition()">
<br><br><div id="manage_req_msg_bottom" style="color:#398ab9"></div>
</td></tr>
</table>
</td></tr>
</table>
</form>
END;
?>