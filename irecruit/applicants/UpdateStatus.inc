<?php
require_once IRECRUIT_DIR . 'applicants/ApplicantsMultipleApps.inc';
require_once IRECRUIT_DIR . 'applicants/ShowAffected.inc';

echo '<br><span id="upd_app_status_msg_top"><strong>The following applications will be updated:</strong></span><br>';

echo "<div id='update_status__applicants'>";
displayList ( $multipleapps, $OrgID, $permit );
echo "</div>";

include IRECRUIT_DIR . 'views/applicants/StatusForm.inc';
?>