<?php 
require_once '../Configuration.inc';

$OrgID = $_REQUEST['OrgID'];
$RequestID = $_REQUEST['RequestID'];
$ApplicationID = $_REQUEST['ApplicationID'];
$UpdateID = $_REQUEST['item'];

// Get JobApplication History

$CheckListDataHistoryinfo   =   G::Obj('ChecklistData')->getCheckListDataHistoryByUpdateID($OrgID, $ApplicationID, $RequestID, $UpdateID);

//Updated Fields Information
echo "<strong>Updated Fields:</strong> <br>";
echo "Comments: ".$CheckListDataHistoryinfo['Comments']."<br />";
echo "Date: ".date("m/d/Y H:i T",strtotime($CheckListDataHistoryinfo[Date]))."<br />";
?>