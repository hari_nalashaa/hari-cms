<?php
$colwidth="220";
$PAGE_TYPE = "PopUp";

require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= 'iRecruit - Document View';

if ($permit ['Applicants'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}

echo $TemplateObj->displayIrecruitTemplate('views/applicants/DocumentView');
?>