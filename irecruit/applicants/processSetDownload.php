<?php 
include '../Configuration.inc';

if(isset($_REQUEST['OrgID'])) $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['RequestID'])) $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['ApplicationID'])) $ApplicationID = $_REQUEST['ApplicationID'];

// Get Requisition Detail Information
$requisition_info = $RequisitionsObj->getReqDetailInfo("RequisitionID", $OrgID, "", $RequestID);
$RequisitionID = $requisition_info['RequisitionID'];

// Get FormID based on ApplicationID, RequestID
$app_detail_info = $ApplicationsObj->getJobApplicationsDetailInfo("FormID", $OrgID, $ApplicationID, $RequestID);
$FormID = $app_detail_info['FormID'];

if($OrgID != "" && $ApplicationID != "" && $RequestID != "") {
	include IRECRUIT_DIR . 'applicants/SetDownload.inc';
}
?>