<?php
require_once '../Configuration.inc';

$WList = $_GET['WSList'];
$AppId = $_GET['AppId'];

$WSFList = $WeightedSearchObj->WeightedSearchInfo($WList);
########################################################
####### Calculate Total Weight Of That Application
########################################################
$FormID = $WSFList[0]['FormID'];
$WebFormID = $WSFList[0]['SectionID']; //Here WebFormID = SectionID only (for only FormID = WebForms)

$where_info = array("OrgID = :OrgID", "WebFormID = :WebFormID");
$parms_info = array(":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID);
$web_form_info = $FormsInternalObj->getWebFormsInfo("FormName", $where_info, "", array($parms_info));
$web_form_name = $web_form_info['results'][0]['FormName'];

//Set where condition
$where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$AppId, ":WebFormID"=>$WebFormID);
//Get ApplicantData Information
$resultsQ = $FormDataObj->getWebFormData("*", $where, '', 'ApplicationID', array($params));
$resultsQ_results = $resultsQ['results'];

for($jw = 0; $jw < count($resultsQ_results); $jw++) {
    $WEBFORMDATA[$resultsQ_results[$jw]['QuestionID']] = $resultsQ_results[$jw]['Answer'];
}

for($we = 0; $we < count($WSFList); $we++) {
	
	$answer = 0;
	$input = $WSFList[$we]['QuestionAnswerKey'];
	$rank = $WSFList[$we]['QuestionAnswerWeight'];
	
	if ($rank != "") {
			$val = explode ( ':', $input );
			
			//Set where condition
			$where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID", "(QuestionID LIKE :QuestionID OR SUBSTRING(QuestionID FROM 2) = :SUBQuestionID)", "Answer != ''", "Answer != :Answer");
			//Set parameters
			$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$AppId, ":WebFormID"=>$WebFormID, ":QuestionID"=>$val [1]."%", ":SUBQuestionID"=>$val [1], ":Answer"=>$val [1]);
			//Get ApplicantData Information
			$resultsQ = $FormDataObj->getWebFormData("*", $where, '', 'ApplicationID', array($params));
			
			if($resultsQ['count'] > 0) {
			    if($WSFList[$we]['QuestionTypeID'] != "100") {
			        if($val[2] == 'QUS') {
			            $WEIGHT[$AppId][$val [0]][$val[1]]["QUS"] = $rank;
			        }
			        else {
			            $WEIGHT[$AppId][$val [0]][$val[1]]["ALL"][] = $rank;
			            $WEIGHT[$AppId][$val [0]][$val[1]]["ANSMULWEIGHT"][$val[3]] = $rank;
			        }
			    }
			    else if($WSFList[$we]['QuestionTypeID'] == "100") {
			        if($val[2] == 'QUS') {
			            $WEIGHT[$AppId][$val [0]][$val[1]]["QUS"] = $rank;
			        }
			    }
			}
			else {
			    $WEIGHT[$AppId][$val [0]][$val[1]]["QUS"] = 0;
			    $WEIGHT[$AppId][$val[0]][$val[1]]["ANS"] = 0;
			}
			if(is_array($resultsQ['results'])) {
				foreach($resultsQ['results'] as $AD) {
				    if($WSFList[$we]['QuestionTypeID'] == "100") {
				    	$cust_que_answer_weight = unserialize($WSFList[$we]['CustomQuestionAnswerWeight']);
				    	$cust_que_information = unserialize($WSFList[$we]['CustomQuestion']);
				    	$cust_que_answer_info = unserialize($AD["Answer"]);
				    	
				    	$flip_cust_que_label_info = array_flip($cust_que_information['LabelValRow']);
				    	$flip_cust_que_rval_info = array_flip($cust_que_information['RVal']);
				    	
				    	$cust_que_col_info = $cust_que_information['RVal'];
				    	$cust_que_row_info = $cust_que_information['LabelValRow'];
				    	$answer_weight_info = array();
				    	$answer_weight = 0;
				    	foreach ($cust_que_answer_info as $cust_que_label_name=>$cust_que_sel_info) {
				    	    $cust_que_sel_label_info = array_keys($cust_que_sel_info);
				    	    $cust_que_sel_col_value = $cust_que_sel_info[$cust_que_sel_label_info[0]];
				    	    $cust_que_sel_col_key = $flip_cust_que_rval_info[$cust_que_sel_col_value];
				    	    $label_row_id = $flip_cust_que_label_info[$cust_que_label_name];
				    	    $answer_weight += $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
				    	    $answer_weight_info[$label_row_id."-".$cust_que_sel_col_key] = $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
				    	}
				    	
				    	$WEIGHT[$AppId][$val [0]][$val[1]]["ALL"][] = $answer_weight;
				    	$WEIGHT[$AppId][$val[0]][$val[1]]["ANS"] = $answer_weight;
				    	$WEIGHT[$AppId][$val[0]][$val[1]]["ANSWER"] = $answer_weight_info;
				    }
				    else if($WSFList[$we]['QuestionTypeID'] == "1818" || $WSFList[$we]['QuestionTypeID'] == "18") {
				        $answer = 0;
				    
				        $ans_cnt = $WEBFORMDATA[$val[1].'cnt'];
				        
				        if($ans_cnt > 0) {
				            for($ac = 1; $ac <= $ans_cnt; $ac++) {
				    
				                $mul_ans = $WEBFORMDATA[$val[1].'-'.$ac];
				    
				                if($mul_ans != "") {
				                    $answer += $WEIGHT[$AppId][$val[0]][$val[1]]["ANSMULWEIGHT"][$mul_ans];
				                }
				    
				            }
				        }
				    
				        $WEIGHT[$AppId][$val[0]][$val[1]]["ANS"]    = $answer;
				        $WEIGHT[$AppId][$val[0]][$val[1]]["ANSWER"] = $val[3];
				    }
				    else if ($AD["Answer"] == "$val[3]") {
			            $answer = $rank;
			            $WEIGHT[$AppId][$val[0]][$val[1]]["ANS"] = $answer;
			            $WEIGHT[$AppId][$val[0]][$val[1]]["ANSWER"] = $val[3];
			        }
				} // end foreach
			}

	} // end rank

}

if($ServerInformationObj->getRequestSource() != 'ajax') {
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME?>css/customstyles.css">
	<script src="<?php echo IRECRUIT_HOME?>js/jquery-1.11.1.min.js"></script>
	<?php
}
?>
<table border="0" cellpadding="3" cellspacing="3" class="table table-bordered">
<?php 
if($ServerInformationObj->getRequestSource() != 'ajax') {
	?>
	<tr><td colspan="3" align="left"><h3 style="background-color:<?php echo $user_preferences['TableRowHeader'];?>;color:white;padding:5px;text-align:center;margin:0 auto;">Score Breakups List</h3></td></tr>
	<?php
}

$total = 0;

foreach($WEIGHT[$AppId] as $WS=>$WQ) {
	?>
	<tr>
	    <th align="left" colspan="3" style="background-color:<?php echo $user_preferences['TableRowHeader'];?>;color:#FFFFFF"><?php echo $web_form_name;?></th>
	</tr>
	<?php
	foreach($WQ as $QuestionID=>$WQINFO) {
		
		// Set where condition
        $where_info     =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
        $params_info    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$WebFormID, ":QuestionID"=>$QuestionID);
        $res_que_info   =   $FormQuestionsObj->getQuestionsInformation("WebFormQuestions", "Question, QuestionID, QuestionTypeID, value", $where_info, "Question DESC", array($params_info));
        $rowquestion    =   $res_que_info['results'][0];

		$weighted_question_types[$QuestionID] = $rowquestion['QuestionTypeID'];
		
		$total += $WQINFO['QUS'];
		$total += $WQINFO['ANS'];
		$WEIGHT[$AppId][$WS][$QuestionID]["QUESTION"] = $rowquestion['Question'];
		
		$wvalue = explode("::", $rowquestion['value']);
		$wvalues = array();
		for($wv = 0; $wv < count($wvalue); $wv++) {
		    $wv_split = explode(":", $wvalue[$wv]);
		    $wvalues[$QuestionID][$wv_split[0]] = $wv_split[1];
		}		
		?>
		<tr>
		    <td align="left" width="10%" valign="top"><strong>Question: </strong></td>
		    <td valign="top"><?php echo $rowquestion['Question'];?></td>
		    <td align="right" nowrap="nowrap" valign="top"><i>Question Weight: </i><?php echo $WQINFO['QUS'];?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Answer: </strong></td>
			<td valign="top">
			<?php
			 if($rowquestion['QuestionTypeID'] == "100") {
                    if(is_array($WQINFO['ANSWER'])) {
                        foreach ($WQINFO['ANSWER'] as $label_col_name=>$lc_weight) {
                             $label_col_name_info = explode("-", $label_col_name);
                             $label_row_id = $label_col_name_info[0];
                             $label_col_id = $label_col_name_info[1];
                            
                             echo $cust_que_row_info[$label_row_id] ." - " . $cust_que_col_info[$label_col_id] . " = " . $cust_que_answer_weight[$label_row_id][$label_col_id] ."<br>";
                        }
                    }
			 }
			 else if($rowquestion['QuestionTypeID'] == "18"
			     || $rowquestion['QuestionTypeID'] == "1818") {
			 
			     $ans_cnt = $WEBFORMDATA[$rowquestion['QuestionID'].'cnt'];
			     
			     $ans_data = "";
			     if($ans_cnt > 0) {
			         for($ac = 1; $ac <= $ans_cnt; $ac++) {
			 
			             $mul_ans = $WEBFORMDATA[$rowquestion['QuestionID'].'-'.$ac];
			 
			             if($mul_ans != "") {
			                 echo $wvalues[$rowquestion['QuestionID']][$mul_ans].'-'.$WEIGHT[$AppId][$WS][$QuestionID]["ANSMULWEIGHT"][$mul_ans].'<br>';
			             }
			 
			         }
			     }
			 }
			 else if($rowquestion['QuestionTypeID'] == "3") {
			     $question_value = $rowquestion['value'];
			     $split_key_value_pairs = explode("::", $question_value);
			     $question_key_val_info = array();
			      
			     for($sk = 0; $sk < count($split_key_value_pairs); $sk++) {
			         $que_key_val_info = explode(":", $split_key_value_pairs[$sk]);
			         $question_key_val_info[$que_key_val_info[0]] = $que_key_val_info[1];
			     }
			 
			     echo $question_key_val_info[$WQINFO['ANSWER']];
			 }
			 else {
			     echo $WQINFO['ANSWER'];
			 }
			?></td>
			<td align="right" nowrap="nowrap" valign="top"><i>Answer Weight: </i><?php echo $WQINFO['ANS'];?></td>
		</tr>
		<?php
	}
}?>
<td colspan="3" align="right" style="background-color: #F5F5F5"><strong>Total Score:</strong> <?php echo $total;?></td>
</table>

<br><br>

<div id="chart">
  <ul id="numbers">
    <li><span>100%</span></li>
    <li><span>90%</span></li>
    <li><span>80%</span></li>
    <li><span>70%</span></li>
    <li><span>60%</span></li>
    <li><span>50%</span></li>
    <li><span>40%</span></li>
    <li><span>30%</span></li>
    <li><span>20%</span></li>
    <li><span>10%</span></li>
    <li><span>0%</span></li>
  </ul>

  <ul id="bars">
  	<?php
  	foreach($WEIGHT[$AppId] as $GWS=>$GWQ) {
		
		foreach($GWQ as $GQuestionID=>$GWQINFO) {
			$sd_id = 'sd'.rand().uniqid();
			
			$MaxAnswerWeight = 0;
			if(is_array($WEIGHT[$AppId][$GWS][$GQuestionID]['ALL']) && count($WEIGHT[$AppId][$GWS][$GQuestionID]['ALL']) > 0) {
                if($weighted_question_types[$GQuestionID] == "18" || $weighted_question_types[$GQuestionID] == "1818") {
                    $MaxAnswerWeight = array_sum($WEIGHT[$AppId][$GWS][$GQuestionID]['ALL']);
                }
                else {
                    $MaxAnswerWeight = max($WEIGHT[$AppId][$GWS][$GQuestionID]['ALL']);
                }
            }
			$QuestionWeight = $WEIGHT[$AppId][$GWS][$GQuestionID]['QUS'];
			
			$MaxWeight = $MaxAnswerWeight + $QuestionWeight;
			$AppWeight = $WEIGHT[$AppId][$GWS][$GQuestionID]['ANS'] + $QuestionWeight;
			
			if($MaxWeight == 0) $Percentage = 0;
			else $Percentage = ($AppWeight / $MaxWeight) * 100;
			?>
			<li>
				<div style="height:<?php echo $Percentage;?>%;" class="bar" onmouseover="$('<?php echo "#".$sd_id;?>').show();" onmouseout="$('<?php echo "#".$sd_id;?>').hide();"></div>
				<span>&nbsp;&nbsp;Q<?php echo ++$i;?></span>
				<div class="tooltip" style="display:none" id="<?php echo $sd_id;?>">
					<?php echo $WEIGHT[$AppId][$GWS][$GQuestionID]['QUESTION'];?>
				</div>
			</li>
			<?php
		}
	}
  	?>
  </ul>
</div>
<br><br>