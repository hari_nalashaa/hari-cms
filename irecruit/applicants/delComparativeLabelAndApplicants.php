<?php 
require_once '../Configuration.inc';

//Delete the label and applicants related to it.
if(isset($_GET['label_id'])
    && $_GET['label_id'] != ""
    && isset($_GET['action'])
    && $_GET['action'] == "delete") {

    $delete_result = $ComparativeAnalysisObj->delComparativeLabels($OrgID, $USERID, $_REQUEST['label_id']);
}

if(isset($delete_result['error_info']) && $delete_result['error_info'] != "") {
    echo "Sorry there is a problem while deleting the label. Please do a page refresh and try again.";
}
else {
    echo "Successfully Deleted";
}
?>