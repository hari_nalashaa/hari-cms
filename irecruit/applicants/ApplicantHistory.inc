<div class="table-responsive">
<table border="0" cellspacing="0" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover">
<tr><td colspan="5" bgcolor="#eeeeee"><b>Applicant History</b></td></tr>
<tr>
<td colspan="5">
	<div id="del_comment_status" class="alert alert-success" style="margin-bottom:0px;display:none;"></div>
</td>
</tr>
<tr>
<td width="100"><b>Updated By</b></td>
<td width="80"><b>Date</b></td>
<td><b>Status</b></td>
<td><b>Comments</b></td>
<?php 
echo '</tr>';

$columns    =   "ProcessOrder, DATE_FORMAT(Date,'%m/%d/%Y %H:%i EST') as JAHDate, UserID,
			     Comments, UpdateID, DispositionCode, DATE_FORMAT(StatusEffectiveDate,'%m/%d/%Y') as StatusEffectiveDate, UpdatedFields";
$where      =   array (
            		"OrgID            = :OrgID",
            		"ApplicationID    = :ApplicationID",
            		"RequestID        = :RequestID"
                    );
$params     =   array (
            		":OrgID"          => $OrgID,
            		":ApplicationID"  => $ApplicationID,
            		":RequestID"      => $RequestID
                    );

// Get JobApplication History
$results = G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo ( $columns, $where, '', 'Date desc', array ($params) );

$rowcolor = "#eeeeee";
$tr_index = 0;
if (is_array ( $results ['results'] )) {
	foreach ( $results ['results'] as $row ) {
		$tr_index++;
        $ProcessOrderKey    =   $row ['ProcessOrder'];
        $ProcessOrder       =   $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $row ['ProcessOrder'] );
        $Date               =   $row ['JAHDate'];
        $Date               =   preg_replace ( "/ /", "<br>", $Date, 1 );
        $UpdatedFields      =   $row ['UpdatedFields'];
        $UpdatedFields      =   json_decode($UpdatedFields, true);
        
        if($row ['ProcessOrder'] == -17) {
            $ProcessOrder   =   'Process Lead';
        }
        
		$UP_avatar="";
		if (is_numeric ( $row ['UserID'] )) {

			$UserID = "PORTAL:<br>";
				
			//Get User, Email From Users
			$resultsUP = $UserPortalUsersObj->getUserDetailInfoByUserID("FirstName, LastName, Email, UserID, ProfileAvatarPicture", $row ['UserID']);
			if(is_array($resultsUP)) {
			    $User    =   $resultsUP['LastName'] . ", " . $resultsUP['FirstName'];
			    $Em      =   $resultsUP['Email'];
			}

		        $UP_avatar = USERPORTAL_HOME . "vault/".$resultsUP['UserID']."/profile_avatars/".$resultsUP['ProfileAvatarPicture'];
				
			$UserID .= $User . "<br>";
				
			if ($permit ['Applicants_Contact'] == 1) { // if access to view email
				$UserID .= '<a href="mailto:' . $Em . '">' . $Em . '</a>';
			} else {
				$UserID .= $Em;
			}
		} else {
			$UserID = $row ['UserID'];
		} // end is_numeric

		$updateid     = $row ['UpdateID'];
		$disposition  = $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] );
		$effdate      = $row ['StatusEffectiveDate'];

		if (($row ['ProcessOrder'] == -4) || ($row ['ProcessOrder'] == -1)) {
			$Comments = ' <a href="#" onclick="javascript:window.open(\'' . IRECRUIT_HOME . 'applicants/viewHistory.php?OrgID=' . $OrgID . '&item=' . $updateid;
			if ($AccessCode != "") {
				$Comments .= "&k=" . $AccessCode;
			}
			$Comments .= '\',\'_blank\',\'location=yes,toolbar=no,height=400,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes\');"><img src="images/icons/page_white_magnify.png" border="0" title="View">&nbsp;&nbsp;View details here</a>';
		}
		else if (($row ['ProcessOrder'] == -17)) {
		    $Comments = ' <a href="#" onclick="javascript:window.open(\'' . IRECRUIT_HOME . 'applicants/viewHistory.php?OrgID=' . $OrgID . '&item=' . $updateid;
		    if ($AccessCode != "") {
		        $Comments .= "&k=" . $AccessCode;
		    }
		    $Comments .= '\',\'_blank\',\'location=yes,toolbar=no,height=400,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes\');"><img src="images/icons/page_white_magnify.png" border="0" title="View">&nbsp;&nbsp;View details here</a>';
		}
		else {
			$Comments = $row ['Comments'];
		}

		if(count($UpdatedFields['Updated Fields']) > 0) {
            $Comments  .=   ' <a href="#" onclick="javascript:window.open(\'' . IRECRUIT_HOME . 'applicants/getUpdatedFieldsInfo.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&item=' . $updateid;
            $Comments  .=   '\',\'_blank\',\'location=yes,toolbar=no,height=400,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes\');"><br>';
            $Comments  .=   '<img src="images/icons/page_white_magnify.png" border="0" title="View">&nbsp;&nbsp;View Updated Fields Information</a>';
		}
		
		echo '<tr bgcolor="' . $rowcolor . '" class="'.$tr_index.'">';
		
		echo '<td valign=top>';
		
		$updated_by_user_avatar_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($UserID, 'DashboardAvatar');

		if ($UP_avatar != "") {
		  $user_avatar = $UP_avatar;
	  	} elseif ($row ['ProcessOrder'] == 1) {
		  $application_image=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, "ApplicantPicture");
		  $user_avatar = IRECRUIT_HOME . "vault/".$OrgID."/applicant_picture/".$application_image;
		} else {
		  $user_avatar = IRECRUIT_HOME . "vault/".$OrgID."/".$UserID."/profile_avatars/".$updated_by_user_avatar_info['DashboardAvatar'];
		}

		if($ServerInformationObj->validateUrlContent($user_avatar)) {
		    ?><img src="<?php echo $user_avatar;?>" width="50" height="50">&nbsp;<?php echo $UserID;?><?php
		}
		else {
		    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50">&nbsp;<?php echo $UserID;?><?php
		}
		echo '</td>';
				
		echo '<td valign=top style="width:70px;">' . $Date . '</td>';
		echo '<td valign=top style="width:180px;">' . $ProcessOrder;

		if ($disposition) {
			echo '<br>Disposition: ' . $disposition;
		}
			
		if ($effdate != "00/00/0000") {
			echo '<br>Effective: ' . $effdate;
		}

		echo '</td>';
		
		echo '<td valign=top width=250 style="width:50%;">';
		echo $Comments;
		echo '</td>';
		
		echo '</tr>';
		
		if ($rowcolor == "#eeeeee") {
			$rowcolor = "#ffffff";
		} else {
			$rowcolor = "#eeeeee";
		}
	} // end foreach
}
?>
</table>
<br>
</div>
