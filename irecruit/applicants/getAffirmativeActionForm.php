<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->action        =   $action         =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->process       =   $process        =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->processtype   =   $processtype    =   isset($_REQUEST['processtype']) ? $_REQUEST['processtype'] : '';

$TemplateObj->ApplicationID =   $ApplicationID  =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID     =   $RequestID      =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

$job_app_info               =   G::Obj('Applications')->getJobApplicationsDetailInfo("FormID", $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
$TemplateObj->FormID        =   $FormID         =   $job_app_info['FormID'];
$TemplateObj->SectionID     =   $SectionID      =   "AA";

require_once COMMON_DIR . 'formsInternal/EditAA.inc';
?>