<?php 
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title             =   $title              =   'Add Applicant';
$TemplateObj->email             =   $email              =   isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
$TemplateObj->RequestID         =   $RequestID          =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->HoldID            =   $HoldID             =   isset($_REQUEST['HoldID']) ? $_REQUEST['HoldID'] : '';
$TemplateObj->FormID            =   $FormID             =   isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';
$TemplateObj->ddlPaperless      =   $ddlPaperless       =   isset($_REQUEST['ddlPaperless']) ? $_REQUEST['ddlPaperless'] : '';

if($HoldID == '') {
    $TemplateObj->HoldID        =   $HoldID             =   G::Obj('ApplicantProcess')->setHoldID($OrgID, $MultiOrgID);
}

if(isset($_REQUEST['MultiOrgID'])) $TemplateObj->MultiOrgID = $MultiOrgID = $_REQUEST['MultiOrgID'];
if(is_null($MultiOrgID)) $MultiOrgID = '';

//Include configuration related javascript in footer
$scripts_footer[] = "js/applicants.js";

//Assign add applicant datepicker variables
$script_vars_footer[]	= 'var datepicker_ids = "#receiveddate";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';
$TemplateObj->scripts_vars_footer = $script_vars_footer;

//Assign all scripts to page_scripts_footer object
$TemplateObj->page_scripts_footer = $scripts_footer;

//Set color variable
$TemplateObj->COLOR = $COLOR; 

//Code to validate and insert the applicant
include IRECRUIT_DIR . 'applicants/processAddApplicant.php';

//view for AddApplicant.inc 
echo $TemplateObj->displayIrecruitTemplate('views/applicants/AddApplicant');