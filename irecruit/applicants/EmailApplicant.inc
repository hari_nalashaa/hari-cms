<?php
function forwardApplicant($OrgID, $ApplicationID, $RequestID, $EmailList, $ATTS, $EmailComments, $USERID, $processtype, $status_index = 0) {
	global $feature, $USERROLE, $RequisitionsObj, $OrganizationsObj, $FormsInternalObj, $AttachmentsObj, $FormQuestionsObj, $RequisitionDetailsObj, $ApplicantDetailsObj, $OrganizationDetailsObj, $IrecruitUsersObj, $PHPMailerObj;
	
    // check ForwardSpecs and set access accordingly //Set where condition
    $where              =   array("OrgID = :OrgID", "RequestID = :RequestID");
    // Set parameters
    $params             =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
    //Get Requisition Forward Specs Information
    $results            =   G::Obj('RequisitionForwardSpecs')->getRequisitionForwardSpecsInfo("*", $where, "", array($params));
    $RFShit             =   $results['count'];
    $RFS                =   $results['results'][0];
    $ForwardSpecsList   =   json_decode($RFS ['ForwardSpecsList'], true);
    $login_user_email   =   base64_encode($EmailList);
	
	if (($processtype == "processforward") && ($RFShit > 0)) {

        $ProcessOrder       =   $RFS ['ProcessOrder'];
        $DispositionCode    =   $RFS ['DispositionCode'];

		if ($ProcessOrder != "") {
		    
		    if($status_index == 0) {
		        
		        // Job Application History Information
		        $job_app_history = array (
		            "OrgID"					=>	$OrgID,
		            "ApplicationID"			=>	$ApplicationID,
		            "RequestID"				=>	$RequestID,
		            "ProcessOrder"			=>	$ProcessOrder,
		            "DispositionCode"		=>	$DispositionCode,
		            "StatusEffectiveDate"	=>	"CURDATE()",
		            "Date"					=>	"NOW()",
		            "UserID"				=>	'Applicant',
		            "Comments"				=>	'Status updated.'
		        );
		        	
		        // Insert Job Application History Information
		        $res_job_app_history  =   G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
		        	
		        //Set information
		        $set_info             =   array("ProcessOrder = :ProcessOrder", "DispositionCode = :DispositionCode");
		        //Set where condition
		        $where                =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
		        //Set parameters
		        $params               =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":ProcessOrder"=>$ProcessOrder, ":DispositionCode"=>$DispositionCode);
		        //Update JobApplications Information
		        $res_job_app_history  =   G::Obj('Applications')->updApplicationsInfo('JobApplications', $set_info, $where, array($params));
		    }
			
		} // end ProcessOrder
		
	} // end RFShit
	
	$Attachments   =   '';
	$to            =   $EmailList;

	// get applicant data
	$PI            =   '';
	
	//Set parameters
	$where         =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "QuestionID IN ('First', 'Last')");
	//Set where condition
	$params        =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
	//Get Applicant Data Information
	$Presults      =   G::Obj('Applicants')->getApplicantDataInfo('Answer', $where, '', 'QuestionID DESC', array($params));
	
	if(is_array($Presults['results'])) {
		foreach($Presults['results'] as $Prow) {
			if ($Prow ['Answer']) {
				$PI .= $Prow ['Answer'] . ' - ';
			}
		}
	}
	
    $cnt            =   strlen ( $PI );
    $PI             =   substr ( $PI, 0, $cnt - 3 );
    $multiorgid_req =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);	
    $subject        =   "Applicant " . $PI . " - " . $ApplicationID . "-" . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID ) . " forwarded for your review";
	
	if ($OrgID == "I20090304") {
		$subject = $PI . " - " . $ApplicationID . "-" . $RequisitionDetailsObj->getReqJobIds ( $OrgID, $multiorgid_req, $RequestID );
	}
	
	$subject       =   substr ( $subject, 0, 78 );
	
	//Get Requisitions Detail Information
	$MultiOrgID    =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
    //Get Email and EmailVerified
	$OE            =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
	//Get Org Name
	$OrgName       =   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);

	$PHPMailerObj = new PHPMailer();	
	$PHPMailerObj->clearCustomHeaders();
	$PHPMailerObj->clearCustomProperties();
	
	if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
	    $OrgNameTrimmed =  preg_replace("/[^a-z0-9\\s]+/i", "", $OrgName);
		// Set who the message is to be sent from
		$PHPMailerObj->setFrom ( $OE ['Email'], $OrgNameTrimmed );
		// Set an alternative reply-to address
		$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrgNameTrimmed );
	} 
	
	if ($_POST ['From']) {
		
	    $opt_list["NoReply@iRecruit-us.com"] = 'iRecruit NoReply ';
	    
	    //Set where users email
        $where_users_email      =   array("OrgID = :OrgID", "UserID = :UserID");
        //Set parameters
        $params_users_email     =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
        //Get UsersInformation
        $results                =   $IrecruitUsersObj->getUserInformation("UserID, EmailAddress, FirstName, LastName", $where_users_email, "", array($params_users_email));
	    
	    if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
            $OrgNameTrimmed            =   preg_replace("/[^a-z0-9\\s]+/i", "", $OrgName);
            $opt_list[$OE ['Email']]   =   $OrgNameTrimmed;
	        
	        if($OE ['OrgName'] != "") {
	            $opt_list[$OE ['Email']] = preg_replace("/[^a-z0-9\\s]+/i", "", $OE ['OrgName']);
	        }
	    }
	    
	    if(is_array($results['results'])) {
	        foreach($results['results'] as $USERS) {
	            $opt_list[$USERS ['EmailAddress']] = $USERS ['FirstName'] . ' ' . $USERS ['LastName'];
	        } // end foreach
	    }
	    
		// Set who the message is to be sent from
		$PHPMailerObj->setFrom ( $_POST ['From'], $opt_list[$_POST ['From']] );
		// Set an alternative reply-to address
		$PHPMailerObj->addReplyTo ( $_POST ['From'], $opt_list[$_POST ['From']] );
	}
	
	if ($feature ['EmailRespond'] == "Y") {
		
		$EmailRespond         =   "";
		$params_email_resp    =   array(":OrgID"=>$OrgID);
		$resultsIN            =   G::Obj('Email')->getEmailResponsesInfo("*", array("OrgID = :OrgID"), "", array($params_email_resp));
		$ER                   =   $resultsIN['results'][0];

		if ($ER ['FormComments'] == "Y") {
			
			$EmailRespond .= '<a href="' . IRECRUIT_HOME . 'emailrespond/updateApplicant.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&type=formcomment&fes='  . $login_user_email . '">Approve or Reject</a> this applicant.';

		} else { // else FormComments
			
			if ($ER ['ProcessOrder']) {
				$EmailRespond .= 'Accept: ' . IRECRUIT_HOME . 'emailrespond/updateApplicant.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&type=accept&fes='  . $login_user_email . "\n\n";
			}
			
			if ($ER ['RejectProcessOrder']) {
				$EmailRespond .= 'Reject: ' . IRECRUIT_HOME . 'emailrespond/updateApplicant.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&type=reject&fes=' . $login_user_email . "\n\n";
			}
		} // end FormComments
	} // end feature
	
	$message = '';
	
	$EmailComments = wordwrap ( $EmailComments, 70 );
	if ($EmailComments) {
		$message .= str_replace ( "\n.", "\n..", $EmailComments ) . "\n\n";
	}
	if ($EmailRespond) {
		$message .= $EmailRespond . "\n";
	}
	$message .= "\n\n";
	
	
	//Set where condition
	$where_job_app     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
	//Set parameters
	$params_job_app    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
	//Get JobApplications Information
	$results           =   G::Obj('Applications')->getJobApplicationsInfo('FormID', $where_job_app, '', '', array($params_job_app));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $JA) {
			$FormID = $JA ['FormID'];
		}
	}
	
	//Set where condition
	$where_form_que    =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID != 99");
	//Set parameters
	$params_form_que   =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
	//Get FormQuestions Information
	$results           =   $FormQuestionsObj->getFormQuestionsInformation("QuestionID, value", $where_form_que, "", array($params_form_que));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $FQ) {
			$QuestionType [$FQ ['QuestionID']] = $FQ ['value'];
		}
	}
	
	//Set where condition applicant attachments
	$where_aa          =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
	//Set parameters
	$params_aa         =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
	//Get Applicant Attachments
	$results           =   $AttachmentsObj->getApplicantAttachments("TypeAttachment, PurposeName, FileType", $where_aa, '', array($params_aa));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $Attach) {
			$Attachment [$Attach ['TypeAttachment']] = $Attach ['PurposeName'] . "." . $Attach ['FileType'];
		}
	}	
	
    //List of Attachments information loop	
	if(is_array($ATTS)) {
	    foreach ($ATTS as $AttachmentType=>$CheckedStatus) {
	        if($CheckedStatus == "Y") {
	            $resfile   =   IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments/' . $ApplicationID . "-" . $Attachment[$AttachmentType];
	            if(file_exists($resfile)) {
	                $attachment_data[$AttachmentType]  =   @file_get_contents ( $resfile );
	            }
	        }
	    }
	}
	
	global $AccessCode;
	$URL = IRECRUIT_HOME . 'applicants/application_view.php?User=' . $USERID . '&OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&emailonly=Y&k='.$AccessCode;

	$applicationdata = '';
	
	$curl_handle = curl_init ();
	curl_setopt ( $curl_handle, CURLOPT_URL, $URL );
	curl_setopt ( $curl_handle, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $curl_handle, CURLOPT_SSL_VERIFYHOST, true );
	curl_setopt ( $curl_handle, CURLOPT_SSL_VERIFYPEER, true );
	$applicationdata .= curl_exec ( $curl_handle );
	curl_close ( $curl_handle );
	
	$historydata = "<style type=\"text/css\">
                    <!--
                    body {
                      font-family: Arial, Helvitica;
                      font-size: 10pt;
                      background: #ffffff;
                    }
                    
                    table {
                      padding: 5px;
                      border-width: 0px;
                    }
                    
                    b.red {
                    font-size: 10pt;
                    font-style: bold;
                    color: red;
                    }
                    -->
                    </style>";
	
	$historydata .= '<table border="1" cellspacing="l" cellpadding="0">';
	$historydata .= '<tr><td colspan="4">Applicant History for ' . $ApplicationID . '</td></tr>';
	$historydata .= '<tr><td width="150"><b>Date</b></td><td><b>Status</b></td><td><b>Updated By</b></td><td><b>Comments</b></td></tr>';
	
	//Set columns
	$columns               =   "ProcessOrder, date_format(Date,'%Y-%m-%d %H:%i EST') as Date1, UserID, Comments, DispositionCode";
	//Set where condition
	$where_job_app_his     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
	//Set parameters
	$params_job_app_his    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
	//Get JobApplication History Information
	$results               =   G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo($columns, $where_job_app_his, '', 'Date DESC', array($params_job_app_his));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
		
			$ProcessOrder    =   $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $row ['ProcessOrder'] );
			$Date            =   $row ['Date1'];
			$UserID          =   $row ['UserID'];
			$Comments        =   $row ['Comments'];
			$disposition     =   $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] );
		
			$historydata .= '<tr><td valign=top>' . $Date . '</td><td valign=top>' . $ProcessOrder;
			if ($disposition) {
				$historydata .= '<br>' . $disposition;
			}
			$historydata .= '</td>';
			$historydata .= '<td valign=top>' . $UserID . '</td><td valign=top width=250>' . $Comments . '</td></tr>';
		}
	}
	
	
	$historydata .= '</table>';
	
	##############################
	
	if (in_array('application', array_keys($ATTS))) {
		$applicationdata  =   wordwrap ( $applicationdata, 70 );
		$applicationdata  =   str_replace ( "\n.", "\n..", $applicationdata );
		
		$msg_application  =   "<style>";
		$msg_application .=   "body {
                                    padding:0px 0px 0px 12px;
                               }
                               .table-bordered {
                                    border: 0px solid #ddd !important;
                               }
                               .table {
                                    margin-bottom: 0px !important;
                                    max-width: 100%;
                                    width: 100%;
                               }
                               .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                                    border: 0px solid #ddd !important;
                                	border-top: 0px solid #ddd !important;
                               }
                               .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                                    border-top: 0px solid #ddd !important;
                                    line-height: 1.42857;
                                    padding: 0px !important;
                                    vertical-align: top;
                               }";
		$msg_application .=   "</style>";
		$msg_application .=   $applicationdata; // The base64 encoded message
		
		$Attachments .= 'Application, ';
		
		$filename_application = $ApplicationID . "-Application.html";
		$PHPMailerObj->addStringAttachment($msg_application, $filename_application, 'base64', 'text/html');
	}
	
	##############################

	if (is_array($ATTS) && in_array('history', array_keys($ATTS))) {
		$historydata  =   wordwrap ( $historydata, 70 );
		$historydata  =   str_replace ( "\n.", "\n..", $historydata );
		
		$msg_application_history      =   $historydata;
		$filename_application_history =   $ApplicationID . "-History.html";

		$PHPMailerObj->addStringAttachment($msg_application_history, $filename_application_history, 'base64', 'text/html');
		
		$Attachments .= 'Applicant History, ';
	}

	##############################

	if (in_array('related_applications', array_keys($ATTS))) {
		//Get Applicant Email
		$ApplicantEmail	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'email');
		
		//Set columns
		$columns 		=	"JA.ApplicationID, JA.RequestID, JA.ProcessOrder";
		//Set where condition
		$where			=	array("JA.OrgID = :OrgID", "JA.ApplicationID = AD.ApplicationID", "JA.OrgID = AD.OrgID", "AD.QuestionID = :QuestionID", "AD.Answer = :Email", "JA.ApplicationID != :ApplicationID");
		//Set parameters
		$params			=	array(":OrgID"=>$OrgID, ":QuestionID"=>'email', ":Email"=>$ApplicantEmail, ":ApplicationID"=>$ApplicationID);
		$related_res	=	G::Obj('Applications')->getJobApplicationsAndApplicantData($columns, $where, "ApplicationID DESC", array($params));
		$related_apps	=	$related_res['results'];
		
		$i = 0;
		for($r = 0; $r < count($related_apps); $r++) {
			
			$multiorgid_req		=	G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $related_apps[$r]['RequestID']);
			$req_detail_info	=	G::Obj('Requisitions')->getRequisitionsDetailInfo("Active", $OrgID, $related_apps[$r]['RequestID']);
			
			if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
				$print = 0;
				if(in_array($related_apps[$r]['RequestID'], $hr_request_ids_list)) {
					$print	= 1;
				}
			} else { // else USERROLE			
				$print = 1;
			} // end USERROLE
			
			if ($print > 0) {
				$i ++;
				$div_color = 'background-color:#fffff';
				if($req_detail_info['Active'] != "Y") $div_color = 'background-color:#c5d8f7';
					
				$RelatedApplications .= '<div style="float:left;border:1px solid #ddd;margin-right:5px;margin-top:5px;padding:10px;min-height:135px;'.$div_color.'">'.$i.'<br>';
			
				if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
					$RelatedApplications .= $related_apps[$r]['ApplicationID'];
				}
				else {
					$RelatedApplications .= "<a href='".IRECRUIT_HOME."applicantsSearch.php?ApplicationID=".$related_apps[$r]['ApplicationID']."&RequestID=".$related_apps[$r]['RequestID']."' target='_blank'>".$related_apps[$r]['ApplicationID']."</a>";
				}
			
				if ($feature ['MultiOrg'] == "Y") {
					$RelatedApplications .= '<br><span style="font-size:8pt">' . G::Obj('OrganizationDetails')->getOrganizationNameByRequestID ($OrgID, $related_apps[$r]['RequestID'] ) . '</span>';
				}
				
				$RelatedApplications .= '<br><span style="font-size:8pt">' . G::Obj('RequisitionDetails')->getJobTitle ($OrgID, $multiorgid_req, $related_apps[$r]['RequestID'] ) . '</span>';
				$RelatedApplications .= '<br><span style="font-size:8pt;">Status: ' . G::Obj('ApplicantDetails')->getProcessOrderDescription ( $OrgID, $related_apps[$r]['ProcessOrder'] ) . '</span>';
					
				$RelatedApplications .= '<br><span style="font-size:8pt">Owner: ' . G::Obj('RequisitionDetails')->getRequisitionOwnersName($OrgID, $ALT ['RequestID']) . '</span>';
				$RelatedApplications .= '<br><span style="font-size:8pt">Managers: '. G::Obj('Requisitions')->getRequisitionManagersNames($OrgID, $ALT ['RequestID']) . '</span>';
					
				$RelatedApplications .= '<br><br>';
				
				$RelatedApplications .= '</div>';
			} // end > 0
			
		}
		
		if($i > 0) {
			$filename_application	=	$ApplicationID . "-Related-Applications.html";
			$PHPMailerObj->addStringAttachment($RelatedApplications, $filename_application, 'base64', 'text/html');
			$Attachments .= 'Related Applications, ';
		}
	}

	##############################
	
	//List of Attachments information loop
	foreach ($ATTS as $AttachmentType=>$CheckedStatus) {
	    if($AttachmentType != 'history' && $AttachmentType != 'application') {
    		$filename     =   $ApplicationID . "-" . $Attachment [$AttachmentType];
    		if(is_array($attachment_data) && in_array($AttachmentType, array_keys($attachment_data))) {
    		    $PHPMailerObj->addStringAttachment($attachment_data[$AttachmentType], $filename, 'base64', 'application/octet-stream');
    		    $Attachments .=   $QuestionType [$AttachmentType] . ', ';
    		}
	    }
	}

	##############################
	
	if (DEVELOPMENT != "Y") {
	    // Set who the message is to be sent to
        $PHPMailerObj->addAddress ( $to );
        // Set the subject line
        $PHPMailerObj->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        $PHPMailerObj->msgHTML ( $message );
        // Content Type Is HTML
        $PHPMailerObj->ContentType = 'text/html';
        //Send mail
        $PHPMailerObj->send ();
	}
	
	$cnt           =   strlen ( $Attachments );
	$Attachments   =   substr ( $Attachments, 0, $cnt - 2 );
	
	return $Attachments;
} // end function

function updateApplicantStatus($OrgID, $ApplicationID, $RequestID, $USERID, $EmailList, $Attachments, $EmailComments) {
	
	$StatusUpdate  =   'Applicant forwarded to ' . $EmailList . ';' . ' Attachments: ' . $Attachments . ';' . ' Comments: ' . $EmailComments;
	
	// Job Application History Information
	$job_app_history   =   array (
                            	"OrgID" 				=> 	$OrgID,
                            	"ApplicationID" 		=> 	$ApplicationID,
                            	"RequestID" 			=> 	$RequestID,
                            	"ProcessOrder" 			=> 	"-1",
                            	"Date" 					=> 	"NOW()",
                            	"StatusEffectiveDate"	=>	"NOW()",
                            	"UserID" 				=> 	$USERID,
                            	"Comments" 				=> 	$StatusUpdate 
                            );
	
	// Insert Job Application History Information
	$res_job_app_history = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
	return $res_job_app_history;
} // end function

function internalFormTickler($OrgID, $ApplicationID, $RequestID, $ProcessOrder) {
	global $feature, $OrganizationsObj, $RequisitionsObj, $FormsInternalObj, $IconnectEmailTemplateObj, $RequisitionDetailsObj, $OrganizationDetailsObj, $USERID, $IrecruitUsersObj, $PHPMailerObj;

		
	//Get application information
    $APPDATA    =   G::Obj('Applicants')->getAppData ($OrgID, $ApplicationID );
	
	//Get JobApplications Information
    $where      =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
    $params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
    $results    =   G::Obj('Applications')->getJobApplicationsInfo('MultiOrgID', $where, '', '', array($params));
    $MultiOrgID =   $results['results'][0]['MultiOrgID'];
    if(is_null($MultiOrgID)) $MultiOrgID = '';

	//Get job title
	$job_title     =   $RequisitionDetailsObj->getJobTitle($OrgID, $MultiOrgID, $RequestID);
	//Get Email and EmailVerified
	$OE            =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
	//Get Org Name
	$OrgName       =   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);

	//Get email message
	$email_template_info   	=   $IconnectEmailTemplateObj->getIconnectEmailTemplate($OrgID);
	$message               	=   $email_template_info['EmailMessage'];
	
	$subject				=	"Employment Action Needed";
	
	//Set where condition
	$where         =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = 1", "Status = 1");
	//Set parameters
	$params        =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
	//Get Internal Forms Assigned Information
	$results       =   $FormsInternalObj->getInternalFormsAssignedInfo("*", $where, "", "", array($params));
	$internalcnt   =   $results['count'];
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $IFA) {
				
			if ($IFA ['FormType'] != "PreFilled") {
				$forms_list_message .= $IFA ['FormType'] . ": ";
			}
				
			$forms_list_message .= $IFA ['FormName'] . "<br>";
	
		} // end foreach
	}
	
	//Get Wotc Ids list
	$wotcid_info   	=   G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $MultiOrgID);
	
	if($wotcid_info['wotcID'] != "") {
		$forms_list_message .= "WOTC Form<br>";
	}

	$userportal_link = '<a href="'. G::Obj('Organizations')->createRequestHoldLink($OrgID,$MultiOrgID,$ApplicationID,$RequestID,"AssignedForms") .'">Click here to complete forms</a>';

	if($email_template_info['Subject'] != "") $subject = $email_template_info['Subject'];
	
	//Apply default template if the template is empty
	if($email_template_info['EmailMessage'] == "") {
		
		$message = "Dear {last} {first},"."<br><br>";
		$message .= "The following forms are available and need to be completed for application {ApplicationID}."."<br><br>";
		$message .= "Please login in to our portal with the below link to complete."."<br><br>";
		$message .= "{UserPortalLink}"."<br><br>";
		$message .= "-----------"."<br><br>";
		$message .= "{FormsList}";
		
	}
	
	$message = str_replace("{ApplicationID}", $ApplicationID, $message);
	$message = str_replace("{first}", $APPDATA['first'], $message);
	$message = str_replace("{last}", $APPDATA['last'], $message);
	$message = str_replace("{UserPortalLink}", $userportal_link, $message);
	$message = str_replace("{FormsList}", $forms_list_message, $message);
	$message = str_replace("{JobTitle}", $job_title, $message);
	
	$to = "no one";
	
	if (($APPDATA['email']) && ($internalcnt > 0)) {

	        $PHPMailerObj = new PHPMailer();	
    		$PHPMailerObj->clearCustomProperties();
    		$PHPMailerObj->clearCustomHeaders();
		
		if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
			
			// Set who the message is to be sent from
			$PHPMailerObj->setFrom ( $OE ['Email'], $OrgName );
			// Set an alternative reply-to address
			$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrgName );
				
		} 
		
		if ($_POST ['From']) {
			
		    $opt_list["NoReply@iRecruit-us.com"] = 'iRecruit NoReply ';
		     
		    //Set where users email
		    $where    =   array("OrgID = :OrgID", "UserID = :UserID");
		    //Set parameters
		    $params   =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
		    //Get UsersInformation
		    $results  =   $IrecruitUsersObj->getUserInformation("UserID, EmailAddress, FirstName, LastName", $where, "", array($params));
		     
		    if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
		        $opt_list[$OE ['Email']] = $OrgName;
		    }
		     
		    if(is_array($results['results'])) {
		        foreach($results['results'] as $USERS) {
		            $opt_list[$USERS ['EmailAddress']] = $USERS ['FirstName'] . ' ' . $USERS ['LastName'];
		        } // end foreach
		    }
		    
		    // Set who the message is to be sent from
		    $PHPMailerObj->setFrom ( $_POST ['From'], $opt_list[$_POST ['From']] );
		    // Set an alternative reply-to address
		    $PHPMailerObj->addReplyTo ( $_POST ['From'], $opt_list[$_POST ['From']] );

		}
		
		$to = $APPDATA['email'];

		// Set who the message is to be sent to
		$PHPMailerObj->addAddress ( $to );
		// Set the subject line
		$PHPMailerObj->Subject = $subject;
		// convert HTML into a basic plain-text alternative body
		$PHPMailerObj->msgHTML ( $message );
		// Content Type Is HTML
		$PHPMailerObj->ContentType = 'text/html';
		
		if (DEVELOPMENT != "Y") {
		    if($ProcessOrder != "") {
		        $trigger_status = $FormsInternalObj->getAssignedFormsTriggerStatus($OrgID, $ApplicationID, $RequestID, $ProcessOrder);
		        if($trigger_status == "Yes") {
		            $PHPMailerObj->send ();
		        }
		    }
		    else {
		        $PHPMailerObj->send ();
		    }
		}
	} // end Email and Forms
	
	return $to;
} // end function

function internalFormManagerTickler($OrgID, $ApplicationID, $RequestID, $ProcessOrder) {
	global $feature, $OrganizationsObj, $RequisitionsObj, $FormsInternalObj, $IconnectEmailTemplateObj, $RequisitionDetailsObj, $OrganizationDetailsObj, $USERID, $IrecruitUsersObj, $PHPMailerObj;

		
	//Get application information
    $APPDATA    	=   G::Obj('Applicants')->getAppData ($OrgID, $ApplicationID );
	
	//Get JobApplications Information
    $where      	=   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
    $params     	=   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
    $results    	=   G::Obj('Applications')->getJobApplicationsInfo('MultiOrgID', $where, '', '', array($params));
    $MultiOrgID 	=   $results['results'][0]['MultiOrgID'];
    if(is_null($MultiOrgID)) $MultiOrgID = '';
    
    //Get Wotc Ids list
	$wotcid_info   	=   G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $MultiOrgID);
    
	//Get job title
	$job_title     	=   $RequisitionDetailsObj->getJobTitle($OrgID, $MultiOrgID, $RequestID);
	//Get Email and EmailVerified
	$OE            	=   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
	//Get Org Name
	$OrgName       	=   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);
	
	//Set where condition
	$where         	=   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = 2", "Status = 2");
	//Set parameters
	$params        	=   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
	//Get Internal Forms Assigned Information
	$results       	=   $FormsInternalObj->getInternalFormsAssignedInfo("*", $where, "", "", array($params));
	$internalcnt   	=   $results['count'];
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $IFA) {
				
			if ($IFA ['FormType'] != "PreFilled") {
				$forms_list_message .= $IFA ['FormType'] . ": ";
			}
				
			$forms_list_message .= $IFA ['FormName'] . "<br>";
	
		} // end foreach
	}

	if($wotcid_info['wotcID'] != "") {
		$forms_list_message .= "WOTC Form" . "<br>";
	}
	
	$subject = "iConnect Form Action Needed";

	$Link = IRECRUIT_HOME . "applicantsSearch.php?ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID . "&tab_action=assign-iconnect-form";

	$iRecruit_Link = "<a href=\"";
	$iRecruit_Link .= $Link;
	$iRecruit_Link .= "\">" . $Link . "</a>";
	
	//Apply default template if the template is empty
	$message .= "The following forms need to be completed for application {ApplicationID} - {last}, {first}."."<br><br>";
	$message .= "Please login to iRecruit with the below link to complete."."<br><br>";
	$message .= "{iRecruitLink}"."<br><br>";
	$message .= "-----------"."<br><br>";
	$message .= "{FormsList}";
	
	$message = str_replace("{ApplicationID}", $ApplicationID, $message);
	$message = str_replace("{first}", $APPDATA['first'], $message);
	$message = str_replace("{last}", $APPDATA['last'], $message);
	$message = str_replace("{iRecruitLink}", $iRecruit_Link, $message);
	$message = str_replace("{FormsList}", $forms_list_message, $message);
	$message = str_replace("{JobTitle}", $job_title, $message);

	$to = "no one";

	// Set where condition
        $where = array ("OrgID = :OrgID", "RequestID = :RequestID");
        // set parameters
        $params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
        // get requisition managers information
        $MANAGERS = $RequisitionsObj->getRequisitionManagersInfo ( "UserID", $where, "", "", array ($params) );

	if (($MANAGERS['count'] > 0) && ($internalcnt > 0)) {

	        $PHPMailerObj = new PHPMailer();	
    		$PHPMailerObj->clearCustomProperties();
    		$PHPMailerObj->clearCustomHeaders();

		if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
			// Set who the message is to be sent from
			$PHPMailerObj->setFrom ( $OE ['Email'], $OrgName );
			// Set an alternative reply-to address
			$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrgName );
		}

		foreach ($MANAGERS['results'] AS $M) {

		  	$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $M['UserID'], "EmailAddress" );
			
			$to = $userinfo['EmailAddress'];
			
			// Set who the message is to be sent to
			$PHPMailerObj->addAddress ( $to );
			// Set the subject line
			$PHPMailerObj->Subject = $subject;
			// convert HTML into a basic plain-text alternative body
			$PHPMailerObj->msgHTML ( $message );
			// Content Type Is HTML
			$PHPMailerObj->ContentType = 'text/html';
			
			if (DEVELOPMENT != "Y") {
			    if($ProcessOrder != "") {
			        $trigger_status = $FormsInternalObj->getAssignedFormsTriggerStatus($OrgID, $ApplicationID, $RequestID, $ProcessOrder);
			        if($trigger_status == "Yes") {
			            $PHPMailerObj->send ();
			        }
			    }
			    else {
			        $PHPMailerObj->send ();
			    }
			}

		} // end foreach
	} // end Email and Forms
	
	return $to;

} // end function
?>
