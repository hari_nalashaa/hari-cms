<?php
$actioncondition = ($action == "remindappointments" || $action == "manageappointments" || $action == "calendartableview" || $action == "calendarlistview");
if(isset($_REQUEST['appointmentaction']) && $_REQUEST['appointmentaction'] != "" && $actioncondition) {
	$TemplateObj->action = $action = $_REQUEST['appointmentaction'];	
}


if($action == 'scheduleinterview') {
	//Only for schedule appointment page
	header("Location:appointments/scheduleAppointment.php?action=scheduleinterview&ApplicationID=".$_REQUEST['ApplicationID']."&RequestID=".$_REQUEST['RequestID']."&RequisitionID=".$_REQUEST['RequisitionID']."&FormID=".$_REQUEST['FormID']."&OrgID=".$_REQUEST['OrgID']);
	exit;
}

if($action == "remindappointments") {
	$title = 'Appointment Management';
}
if($action == "weightedsearch") {
    $title = 'Weighted Search';
}
else {
	$title = 'Applicant Management';
}

$onboard_list_title = "Onboard List";

$subtitle = array (
	""                     =>  "Applicant Search",
	"search"               =>  "Applicant Search",
	"savedweightedsearch"  =>  "Saved Weighted Search",
	"backgroundcheck"      =>  "Background Checks",
	"status"               =>  "Applicants by Status",
	"calendartableview"    =>  "Appointment Calendar",
	"calendarlistview"     =>  "Appointment Calendar",
	"scheduleinterview"    =>  "Schedule Interview",
	"maintainsearch"       =>  "Maintain Saved Searches",
	"updateapplicant"      =>  "Applicant Summary",
	"updatestatus"         =>  "Update Status",
	"affirmativeaction"    =>  "Affirmative Action",
	"contactapplicant"     =>  "Contact Applicant",
	"forwardapplicant"     =>  "Forward Applicant",
	"applicationedit"      =>  "Edit Application Data",
	"managerequisition"    =>  "Assign Requisition",
	"onboardapplicant"     =>  "Onboard Applicant",
	"deleteapplicant"      =>  "Delete Applicant",
    "MatrixCare"           =>  "MatrixCare Onboard List",
    "WebABA"               =>  "WebABA Onboard List",
    "Lightwork"            =>  "Lightwork Onboard List",
    "Abila"                =>  "Abila Onboard List",
    "Mercer"               =>  "Mercer Onboard List",
    "XML"                  =>  "XML List",
    "HRMS"                 =>  "Data Manager Onboard List",
    "Exporter"             =>  "Data Manager Onboard List"
);

if ($BackgroundCheckLinksObj->displayNavigation == 'off') {
	unset($subtitle['backgroundcheck']);
}

if ($subtitle [$action] && $action != 'weightedsearch') {
	$title .= ' - ' . $subtitle [$action];
}

$TemplateObj->subtitle = $subtitle;
?>
