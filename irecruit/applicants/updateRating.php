<?php
require_once '../Configuration.inc';

$applicant_rating   =   $_REQUEST['Rating'];

$set_info           =   array("Rating = :Rating");
$params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$_REQUEST['ApplicationID'], ":RequestID"=>$_REQUEST['RequestID'], ":Rating"=>$applicant_rating);
$where_info         =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
$update_info        =   $ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));

// Job Application History Information
$job_app_history    =   array (
                    		"OrgID"               =>  $OrgID,
                    		"ApplicationID"       =>  $_REQUEST['ApplicationID'],
                    		"RequestID"           =>  $_REQUEST['RequestID'],
                    		"StatusEffectiveDate" =>  "CURDATE()",
                    		"Date"                =>  "NOW()",
                    		"UserID"              =>  $USERID,
                    		"Comments"            =>  "Rated as ".$applicant_rating." Star"
                        );
// Insert Job Application History Information
$res_job_app_history =  G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );

echo $update_info['affected_rows'];
?>