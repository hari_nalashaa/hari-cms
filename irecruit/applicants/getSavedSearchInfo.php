<?php 
require_once '../Configuration.inc';

$where_info = array("OrgID = :OrgID", "UserID = :UserID", "SearchName = :SearchName");
$params_info = array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":SearchName"=>$_REQUEST['SearchName']);

$user_saved_search_info = $IrecruitUsersObj->getUserSavedSearches("*", $where_info, "", array($params_info));

$ApplicationDate_From = $user_saved_search_info['results'][0]['ApplicationDate_From'];
$ApplicationDate_To = $user_saved_search_info['results'][0]['ApplicationDate_To'];

$FromDate = explode(" ", $ApplicationDate_From);
$ToDate = explode(" ", $ApplicationDate_To);

$FromDate = $DateHelperObj->getMdyFromYmd($FromDate[0]);
$ToDate = $DateHelperObj->getMdyFromYmd($ToDate[0]);

$user_saved_search_info['results'][0]['ApplicationDate_From'] = $FromDate;
$user_saved_search_info['results'][0]['ApplicationDate_To'] = $ToDate;

if($_REQUEST['SearchName'] != "Reset"
	&& $_REQUEST['SearchName'] != "") {
	echo json_encode($user_saved_search_info['results'][0]);
} else {
	$FromDate = date('m/d/Y', strtotime("-3 months", strtotime(date('m/d/Y'))));
	$ToDate = date('m/d/Y');
	$default_info = array("ApplicationDate_From"=>$FromDate, "ApplicationDate_To"=>$ToDate);
	echo json_encode($default_info);
}
?>