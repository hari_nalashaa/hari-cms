<?php
//Login UserID
global $USERID;

// Select Active Forms
$resultforms            =   G::Obj('WeightedSearch')->FormQuestions($OrgID);
if(is_array($resultforms['results'])) {
    foreach ($resultforms['results'] as $FQ) {
        $FormNames[$FQ['FormID']] = $FQ['FormID'];
    }
}

//Set web forms option also
$FormNames["WebForms"]  =   "Web Forms";
$formname_keys          =   array_keys($FormNames);

//Default FormID
if(!isset($_GET['FormID']) && !isset($_POST['FormID'])) {
    $FormID = $formname_keys[0];
}

if(isset($_GET['FormID'])) {
    $FormID = $_GET['FormID'];
}

//Set the FormID, SectionID from Post
if (count ( $_POST ) > 0) {
    $FormID     =   $_POST['FormID'];
    $SectionID  =   $_POST['SectionID'];
}

$QuestionIds        =   array ();
//Select FormSections
$resformsections    =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

//Form Sections Keys
$formsections_keys  =   array_keys($resformsections);
//Default SectionID
if($SectionID == "") {
    $SectionID      =   $formsections_keys[0];
}
$SaveSearchName     =   "";

if(isset($_POST['SaveSearch'])) {
    $SaveSearchName =   $_POST['SaveSearch'];
}

if($_POST['UnsetSession'] != "Yes") {
    //Insert weighted search information to database
    include_once IRECRUIT_DIR . 'applicants/ProcessWeightedSearch.inc';
}

// Select FormQuestions By Section
$formques               =   G::Obj('WeightedSearch')->FormQuestionsBySection($FormID, $SectionID, $OrgID);
$resultformquestions    =   $formques['results'];
$numberofrows           =   $formques['count'];

if(isset($_GET['uwkey']) && $_GET['uwkey'] != "") {
    $SavedFormData      =   G::Obj('WeightedSearch')->WeightedSearchInfo($_GET['uwkey']);
	for($k = 0; $k < count($SavedFormData); $k++) {
	    if(!in_array($SavedFormData[$k]['QuestionID'], $QuestionIds)) {
	        $QuestionIds[] = $SavedFormData[$k]['QuestionID'];
	        $QuestionIdSection[$SavedFormData[$k]['QuestionID']] = $SavedFormData[$k]['SectionID'];
	    }
	    
	    if($SavedFormData[$k]['QuestionTypeID'] == "100") {
	        $QuestionWeightInfo[$SavedFormData[$k]['SectionID']][$SavedFormData[$k]['QuestionID']][$SavedFormData[$k]['QuestionAnswerKey']] = $SavedFormData[$k]['CustomQuestionAnswerWeight'];
	    }
	    else {
	        $QuestionWeightInfo[$SavedFormData[$k]['SectionID']][$SavedFormData[$k]['QuestionID']][$SavedFormData[$k]['QuestionAnswerKey']] = $SavedFormData[$k]['QuestionAnswerWeight'];
	    }
	}
	
	if(count($QuestionIds) > 0 
	   && isset($SavedFormData[0]['SaveSearchName'])
	   && $SavedFormData[0]['SaveSearchName'] != "") {
		$SaveSearchName   =   $SavedFormData[0]['SaveSearchName'];
		$FormID           =   $SavedFormData[0]['FormID'];
		
		if($FormID == "WebForms") {
			$SectionID = $SavedFormData[0]['SectionID'];
		}
	}
}


if($FormID == "WebForms") {
    $where_info     =   array("OrgID = :OrgID");
    $params_info    =   array(":OrgID"=>$OrgID);
    $web_forms_info =   G::Obj('FormsInternal')->getWebFormsInfo("OrgID, WebFormID, FormName", $where_info, "FormName ASC", array($params_info));
    $web_forms_list =   $web_forms_info['results'];

    $web_forms      =   array();

    for($j = 0; $j < count($web_forms_info['results']); $j++) {
        $web_forms[$web_forms_list[$j]['WebFormID']] = $web_forms_list[$j]['FormName'];
    }

    $where_info     =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "Active = 'Y'");
    $params_info    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$SectionID);
    $formques       =   G::Obj('FormQuestions')->getQuestionsInformation("WebFormQuestions", "*", $where_info, "QuestionOrder ASC", array($params_info));
    
    $resultformquestions    =   $formques['results'];
    $numberofrows           =   $formques['count'];
}
?>
<form name="frmWeightedSearch" id="frmWeightSearch" method="post"
	action="applicants.php?<?php echo $_SERVER['QUERY_STRING'];?>">
	<input type="hidden" name="UnsetSession" id="UnsetSession"> 
	<input type="hidden" name="selectquestions" id="selectquestions" value=""> 
	<input type="hidden" name="uwkey" id="uwkey" value="<?php if(isset($_REQUEST['uwkey']) || $_REQUEST['uwkey'] != "") echo $_REQUEST['uwkey']; else echo "";?>">
	<?php $name = preg_replace ( '/ /', '_', $FQ ['Section'] ) . "::SEC::";?>
	<input type="hidden" name="process" id="process">

	<table width="100%" cellpadding="5" cellspacing="0"
		class="table table-striped table-bordered table-hover">
		<tr>
			<td colspan="2" align="right">
			 <a href="<?php echo IRECRUIT_HOME;?>applicants.php?action=savedweightedsearch">Saved Weighted Searches</a>
			</td>
		</tr>
		<?php 
		if(isset($_GET['subaction']) && $_GET['subaction'] == "edit") {
            ?>
            <tr>
			<td width="10%">Forms</td>
			<td>
    			<?php echo $FormNames[$FormID];?>
    			<input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
			</td>
		</tr>
        <?php
        }
        else {
        	?>
    		<tr>
    			<td width="10%">Forms</td>
    			<td>
        			<select name="FormID" id="FormID" onchange="clearSessionOnFormChange();" style="width: 190px;" class="form-control">
        				<?php
        					foreach($FormNames as $fkey=>$fval) {
        						?><option value="<?php echo $fkey?>" <?php if ($fkey == $FormID) echo 'selected="selected"'?>><?php echo $fval?></option><?php
        					}
        				?>
                    </select>
        		 </td>
		    </tr>
            <?php 
        }
        ?>
		<tr>
			<td>
			 <?php 
			     if($FormID == 'WebForms') {
			     	echo "Forms List";
			     }
			     else {
			         echo "Sections";
			     }
			 ?>
			</td>
			<td>
			    <?php
			    if(isset($_GET['subaction']) && $_GET['subaction'] == "edit" && $FormID == "WebForms") {
                    echo $web_forms[$SectionID];
                } 
                else {
                    ?>
				    <select name="SectionID" id="SectionID" onchange="clearSessionOnSectionChange();" style="width: 190px;" class="form-control">
					<option value="">Select</option>
    					<?php
        					if($FormID == 'WebForms') {
                                foreach($web_forms as $web_form_id => $web_form_name) {
                                    ?><option value="<?php echo $web_form_id;?>" <?php if ($SectionID == $web_form_id) echo 'selected="selected"'?>>
                                        <?php echo $web_form_name;?>
                                      </option>
                                    <?php
                                }
        					}
                            else {
                                if(is_array($resformsections)) {
                                    foreach($resformsections as $WSectionID=>$WSectionInfo) {
                                        ?>
                                        <option value="<?php echo $WSectionID;?>"
                                        <?php if ($WSectionID == $SectionID) echo 'selected="selected"'?>>
                        				    <?php echo $WSectionInfo['SectionTitle'];?>
                        				</option>
                        				<?php
                        			}
                        		}
                            }
    					?>
					</select>
					<?php
                }
                ?>
			</td>
		</tr>
		<tr>
			<td height="10" colspan="2"></td>
		</tr>
	</table>

	<table width="100%" cellpadding="5" cellspacing="0"
		class="table table-striped table-bordered table-hover">
		<tr>
			<td colspan="2" align="left"><b>Questions List:</b></td>
		</tr>
		<tr>
			<th align="left">Question</th>
			<th align="left">Select</th>
		</tr>
		<?php
		if ($numberofrows > 0) {
			if(is_array($resultformquestions)) {
				foreach ($resultformquestions as $FQUE) {
					if ($FQUE ['QuestionTypeID'] == 2 
                        || $FQUE ['QuestionTypeID'] == 3 
                        || $FQUE ['QuestionTypeID'] == 18 
                        || $FQUE ['QuestionTypeID'] == 1818
                        || $FQUE ['QuestionTypeID'] == 100) {
						?>
						<tr>
                			<td align="left"><?php echo $FQUE['Question'];?></td>
                			<td align="left">&nbsp; 
                			 <input type="checkbox" name="Questions[<?php echo $FQUE['QuestionID']?>]">
                			</td>
                		</tr>
						<?php
					}
				}
			}
		} else {
			?><tr>
			     <td colspan="2">No Questions For This Section</td>
		      </tr><?php
		}
		?>
		<tr>
			<td colspan="2">
			 <input type="submit" name="btnSelect" id="btnSelect" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton';?>" value="Select" onclick="set_value('selectquestions', 'yes');">
			</td>
		</tr>
	</table>
	<table class="table table-striped table-bordered table-hover">
		<?php
		if ((count ( $QuestionIds ) > 0)) {
			include 'WeightedSearchSelectedQuestions.inc';
		}
		
		if(isset($_REQUEST['uwkey']) && $_REQUEST['uwkey'] != "") {
			$display = 'display:block';
		}
		else {
			$display = 'display:none';
		}
		?>
		<tr>
			<td colspan="2" height="40"><strong>Note:*</strong> Please save your form after you have done with your changes.</td>
		</tr>
		<tr>
			<td colspan="2">
				<div id="savesearchblock" style="<?php echo $display;?>">
					Search Name:<input type="text" name="SaveSearch" id="SaveSearch" value="<?php echo $SaveSearchName;?>">
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
    			<input type="submit" name="btnSave" id="btnSave" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton';?>" value="Save Form" onclick="return save_form();"> 
    			<input type="hidden" name="QAList" id="QAList" value="yes">
			</td>
		</tr>
	</table>
	<input type="hidden" name="SE" id="SE" value=""> 
	<input type="hidden" name="QE" id="QE" value="">
</form>
<script>
function clearSessionOnFormChange() {
	document.forms['frmWeightedSearch'].UnsetSession.value = 'Yes';
	
	var FormID = document.getElementById('FormID').value;
	var uwkey = document.getElementById('uwkey').value;
	
	location.href = 'applicants.php?action=weightedsearch&FormID='+FormID+'&menu=10&uwkey='+uwkey+'&subaction=add';
}
function clearSessionOnSectionChange() {
	if(document.getElementById('FormID').value == 'WebForms') {
		document.forms['frmWeightedSearch'].UnsetSession.value = 'Yes';
	}
	document.forms['frmWeightedSearch'].submit();
}
</script>