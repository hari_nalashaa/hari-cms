<?php
##### Calculate Total Weight Of That Application
$weighted_form_info = $WeightedSearchObj->getWeightSaveSearchRowInfo("FormID, SectionID", $_REQUEST['WSList']);

if($weighted_form_info['FormID'] == "WebForms") {
    $total  =  $WeightedSearchObj->getScoreOfApplicantForWebForm($OrgID, $row ['ApplicationID'], $weighted_form_info['SectionID'], $_REQUEST['WSList']);
}
else {
    $total  =  $WeightedSearchObj->getWeightedScoreOfApplicant($OrgID, $row ['ApplicationID'], $_REQUEST['WSList']);
}
##### Calculate Total Weight Of That Application

$ApplicationID      =   $row ['ApplicationID'];
$RequestID          =   $row ['RequestID'];
$EntryDate          =   $row ['EntryDate'];
$ProcessOrder       =   $row ['ProcessOrder'];
$DispositionCode    =   $row ['DispositionCode'];
$EffDate            =   $row ['StatusEffectiveDate'];
$CorrespondenceCnt  =   $row ['Correspondence'];
$ReceivedDate       =   $row ['ReceivedDate'];
$MultiOrgID         =   $row ['MultiOrgID'];

$APPDATA            =   array ();

if(isset($row ['ApplicationID']) && $row ['ApplicationID'] != "") {
    $APPDATA            =   G::Obj('Applicants')->getAppData ( $OrgID, $row ['ApplicationID'] );
    
    $Applicant          =   $row['ApplicantSortName'];
    $Email              =   isset($APPDATA ['email']) ? $APPDATA ['email'] : '';
    
    if(!isset($APPDATA ['homephone1']))  $APPDATA ['homephone1'] = '';
    if(!isset($APPDATA ['homephone2']))  $APPDATA ['homephone2'] = '';
    if(!isset($APPDATA ['homephone3']))  $APPDATA ['homephone3'] = '';
    
    $Phone              =   '(' . $APPDATA ['homephone1'] . ') ' . $APPDATA ['homephone2'] . '-' . $APPDATA ['homephone3'];
}
?>