<?php 
require_once '../Configuration.inc';

$ConsiderationStatus = $_REQUEST['ConsiderationStatus'];
$ApplicationID = $_REQUEST['ApplicationID'];
$RequestID = $_REQUEST['RequestID'];
$action = $_REQUEST['action'];

if(isset($_REQUEST['action']) && $action == 'updappconsiderationstatus') {
	$set_info = array("ApplicantConsiderationStatus1 = :ConsiderationStatus");
	$where_info = array("ApplicationID = :ApplicationID",
						"RequestID = :RequestID",
						"OrgID = :OrgID");
	$params = array(
					":ConsiderationStatus"=>$ConsiderationStatus,
					":ApplicationID"=>$ApplicationID,
					":RequestID"=>$RequestID,
					":OrgID"=>$OrgID
					); 
	$res_app_consideration_info = $ApplicationsObj->updApplicationsInfo('JobApplications', $set_info, $where_info, array($params));
	
	$consider_status = $ConsiderationStatus;
	if($ConsiderationStatus == "ThumbsUp") $consider_status = "Applicant given a ThumbsUp";
	else if($ConsiderationStatus == "ThumbsDown") $consider_status = "Applicant given a ThumbsDown";
	else if($ConsiderationStatus == "") $consider_status = "ThumbsUp/Thumbs down status removed";
	
	// Job Application History Information
	$job_app_history = array (
			"OrgID"=>$OrgID,
			"ApplicationID"=>$ApplicationID,
			"RequestID"=>$RequestID,
			"StatusEffectiveDate"=>"CURDATE()",
			"Date"=>"NOW()",
			"UserID"=>$USERID,
			"Comments"=>$consider_status
	);
		
	// Insert Job Application History Information
	$res_job_app_history = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
}

$ApplicationConsiderationStatus = '';

$ApplicationDetails = $ApplicationsObj->getJobApplicationsDetailInfo('ApplicantConsiderationStatus1', $OrgID, $ApplicationID, $RequestID);
$ApplicationConsiderationStatus = $ApplicationDetails['ApplicantConsiderationStatus1'];

echo $ApplicationConsiderationStatus;
?>