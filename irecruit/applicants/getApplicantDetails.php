<?php 
require_once '../Configuration.inc';

$ApplicationID  =   $_REQUEST['ApplicationID'];
$RequestID      =   $_REQUEST['RequestID'];

$WSFList        =   G::Obj('WeightedSearch')->WeightedSearchInfo($_REQUEST['WSList']);

$weighted_question_types = array();

$SelectedFormID =   "";
for($wqt = 0; $wqt < count($WSFList); $wqt++) {
    $weighted_question_types[$WSFList[$wqt]['QuestionID']] = $WSFList[$wqt]['QuestionTypeID'];
    $SelectedFormID     =   $WSFList[$wqt]['FormID'];
    $SelectedSectionID  =   $WSFList[$wqt]['SectionID'];
}

if($SelectedFormID == "WebForms") {
    //Set where condition
    $where  	=	array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID");
    //Set parameters
    $params		=	array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":WebFormID"=>$SelectedSectionID);
    //Get ApplicantData Information
    $resultsQ	=	$FormDataObj->getWebFormData("*", $where, '', 'ApplicationID', array($params));
    $resultsQ_results = $resultsQ['results'];
    
    for($jw = 0; $jw < count($resultsQ_results); $jw++) {
        $WEBFORMDATA[$resultsQ_results[$jw]['QuestionID']] = $resultsQ_results[$jw]['Answer'];
    }
}

//Get twilio number
$user_details			=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);

//Add plus one before number
if(substr($user_details['Cell'], 0, 2) != "+1") {
    $user_cell_number	=	"+1".$user_cell_number;
}

//Get applicant cellphone
$cell_phone_info		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphone');
$cell_phone				=	json_decode($cell_phone_info, true);

$applicant_number       =   "";
if($cell_phone[0].$cell_phone[1].$cell_phone[2] != "") {
    $applicant_number   =	"+1".$cell_phone[0].$cell_phone[1].$cell_phone[2];
}

$archive_con_info       =   G::Obj('TwilioConversationsArchive')->getTwilioConversationsArchive($OrgID, $USERID, $applicant_number, $user_cell_number);

//Get Applicant Data
$APPDATA                =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);

$APPDATA["ArchiveTextMessages"]     =   "False";

if($archive_con_info[0]["ResourceID"] != "") {
    $APPDATA["ArchiveTextMessages"] =   "True";
}

//Get predefined options list
$zip_options    =   array("resumeupload", "coverletterupload", "otherupload", "custom", "appview", "apphistory", "iconnectforms");

$get_params     =   array("OrgID"=>$OrgID, "UserID"=>$USERID);
$where_params   =   array("OrgID = :OrgID", "UserID = :UserID");
$doc_option_res =   G::Obj('IrecruitApplicationFeatures')->getDocumentsZipDownloadOptions("DocumentsList", $where_params, "", array($get_params));

if(isset($doc_option_res['results'][0]['DocumentsList'])
    && $doc_option_res['results'][0]['DocumentsList'] != "") {
	//Update with user settings
	$zip_options   =   json_decode($doc_option_res['results'][0]['DocumentsList'], true);
}


$total_weight       =   0;
$applicant_score    =   0;
$WEIGHT             =   array();

for($we = 0; $we < count ( $WSFList ); $we ++) {

	$input =   $WSFList [$we] ['QuestionAnswerKey'];
	$rank  =   $WSFList [$we] ['QuestionAnswerWeight'];
	
	if ($rank != "") {
		$val = explode ( ':', $input );
		
		$weighted_form_info = G::Obj('WeightedSearch')->getWeightSaveSearchRowInfo("FormID, SectionID", $_REQUEST['WSList']);
		
		if($weighted_form_info['FormID'] == "WebForms") {
            //Set where condition
            $where      =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "WebFormID = :WebFormID", "(QuestionID LIKE :QuestionID OR SUBSTRING(QuestionID FROM 2) = :SUBQuestionID)", "Answer != ''", "Answer != :Answer");
            //Set parameters
            $params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":WebFormID"=>$weighted_form_info['SectionID'], ":QuestionID"=>$val [1]."%", ":SUBQuestionID"=>$val [1], ":Answer"=>$val [1]);
            //Get ApplicantData Information
            $resultsQ   =   $FormDataObj->getWebFormData("*", $where, '', 'ApplicationID', array($params));
		}
		else {
		    //Set where condition
            $where      =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "(QuestionID LIKE :QuestionID OR SUBSTRING(QuestionID FROM 2) = :SUBQuestionID)", "Answer != :Answer");
		    //Set parameters
            $params     =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":QuestionID"=>$val [1]."%", ":SUBQuestionID"=>$val [1], ":Answer"=>$val [1]);
		    //Get ApplicantData Information
            $resultsQ   =   $ApplicantsObj->getApplicantDataInfo("*", $where, '', 'ApplicationID', array($params));
		}
		
		if($WSFList[$we]['QuestionTypeID'] == "100") {
		    if ($val [2] == 'QUS') {
		        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["MAXQUS"] = $rank;
		    }
		    
		    $cust_que_ans_weight_info_tomax = unserialize($WSFList[$we]['CustomQuestionAnswerWeight']);
		    $cust_que_ans_max_weight = 0;
		    if(is_array($cust_que_ans_weight_info_tomax)) {
		        foreach($cust_que_ans_weight_info_tomax as $LabelValRow=>$RValInfo) {
		            if(substr($LabelValRow, 0, 11) == "LabelValRow") {
		                $rval_weights_list = array_values($RValInfo);
		                if(is_array($rval_weights_list)) {
		                    $cust_que_ans_max_weight += max($rval_weights_list);
		                }
		            }
		        }
		    }
		    
		    $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["MAXANS"][] = $cust_que_ans_max_weight;
		}
		else {
		    if ($val [2] == 'QUS') {
		        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["MAXQUS"] = $rank;
		    }
		    else if ($val [2] == 'ANS') {
		        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["MAXANS"][] = $rank;
		    }
		}
		
		if($resultsQ['count'] > 0) {
		    if ($val [2] == 'QUS') {
			    $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["QUS"] = $rank;
		    }
		    else if ($val [2] == 'ANS') {
		        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["ANS"][] = $rank;
		        $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ANSMULWEIGHT"][$val[3]] = $rank;
		    }
		}
		else {
		    $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["QUS"] = 0;
		    $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["ANS"][] = 0;
		}
		
		if(is_array($resultsQ['results'])) {
			foreach($resultsQ['results'] as $AD) {
			    if($WSFList[$we]['QuestionTypeID'] == "100") {
                    $cust_que_answer_weight     =   unserialize($WSFList[$we]['CustomQuestionAnswerWeight']);
                    $cust_que_information       =   unserialize($WSFList[$we]['CustomQuestion']);
                    $cust_que_answer_info       =   unserialize($AD["Answer"]);
                    
                    $flip_cust_que_label_info   =   array_flip($cust_que_information['LabelValRow']);
                    $flip_cust_que_rval_info    =   array_flip($cust_que_information['RVal']);
                    
                    $cust_que_col_info          =   $cust_que_information['RVal'];
                    $cust_que_row_info          =   $cust_que_information['LabelValRow'];
                    $answer_weight_info         =   array();
                    $answer_weight              =   0;
			    	
			    	foreach ($cust_que_answer_info as $cust_que_label_name=>$cust_que_sel_info) {
                        $cust_que_sel_label_info    =   array_keys($cust_que_sel_info);
                        $cust_que_sel_col_value     =   $cust_que_sel_info[$cust_que_sel_label_info[0]];
                        $cust_que_sel_col_key       =   $flip_cust_que_rval_info[$cust_que_sel_col_value];
                        $label_row_id               =   $flip_cust_que_label_info[$cust_que_label_name];
			    	    $answer_weight += $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
			    	    $answer_weight_info[$label_row_id."-".$cust_que_sel_col_key] = $cust_que_answer_weight[$label_row_id][$cust_que_sel_col_key];
			    	}
			    
			        $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["APPANS"] = $answer_weight;
			    }
			    else if($WSFList[$we]['QuestionTypeID'] == "1818" || $WSFList[$we]['QuestionTypeID'] == "18") {
			        
			        if($SelectedFormID == "WebForms") {
			            $answer = 0;
			            $ans_cnt = $WEBFORMDATA[$val[1].'cnt'];
			            if($ans_cnt > 0) {
			                for($ac = 1; $ac <= $ans_cnt; $ac++) {
			            
			                    $mul_ans = $WEBFORMDATA[$val[1].'-'.$ac];
			            
			                    if($mul_ans != "") {
			                        $answer += $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ANSMULWEIGHT"][$mul_ans];
			                    }
			            
			                }
			            }
			            
			            $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["APPANS"] = $answer;
			        }
			        else {
			            $answer = 0;
			            $ans_cnt = $APPDATA[$val[1].'cnt'];
			            if($ans_cnt > 0) {
			                for($ac = 1; $ac <= $ans_cnt; $ac++) {
			                     
			                    $mul_ans = $APPDATA[$val[1].'-'.$ac];
			                     
			                    if($mul_ans != "") {
			                        $answer += $WEIGHT[$ApplicationID][$val [0]][$val[1]]["ANSMULWEIGHT"][$mul_ans];
			                    }
			                     
			                }
			            }
			             
			            $WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["APPANS"] = $answer;
			        }
			         
			    }
				else if ($AD ["Answer"] == "$val[3]") {
					$WEIGHT [$ApplicationID] [$val [0]] [$val [1]] ["APPANS"] = $rank;
				}
			} // end foreach
		}

	} // end rank
}

if(is_array($WEIGHT[$ApplicationID])) {
	foreach($WEIGHT [$ApplicationID] as $que_label_key) {
	    
		foreach($que_label_key as $weight_info_key=>$weight_info_value) {
		    
			$total_weight += $weight_info_value['MAXQUS'];
			
			if(is_array($weight_info_value['MAXANS']) && count($weight_info_value['MAXANS']) > 0) {
			    if($weighted_question_types[$weight_info_key] == '18' || $weighted_question_types[$weight_info_key] == '1818') {
                    $total_weight += array_sum($weight_info_value['MAXANS']);
			    }
			    else {
			        $total_weight += max($weight_info_value['MAXANS']);
			    }
			}
	
			$applicant_score += $weight_info_value['QUS'];
			$applicant_score += $weight_info_value['APPANS'];
		}
	}
}

// Set required columns
$columns                =   "RequestID, FormID, date_format(EntryDate,'%m/%d/%Y %H:%i EST') as EntryDate, date_format(ReceivedDate,'%m/%d/%Y') ReceivedDate, ProcessOrder, DispositionCode, IFNULL(StatusEffectiveDate,'') StatusEffectiveDate, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2, Rating, Paperless, LeadGenerator, LeadStatus, IsInternalApplication";
$Applicants             =   G::Obj('Applications')->getJobApplicationsDetailInfo($columns, $OrgID, $ApplicationID, $RequestID);

$RequestID              =   $Applicants ['RequestID'];
$FormID                 =   $Applicants ['FormID'];
$ApplicationDate        =   $Applicants ['EntryDate'];
$ReceivedDate           =   $Applicants ['ReceivedDate'];
$LastProcessOrder       =   $Applicants ['ProcessOrder'];
$LastDispositionCode    =   $Applicants ['DispositionCode'];
$LastEffDate            =   $Applicants ['StatusEffectiveDate'];
$multiorgid_req         =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
$status                 =   G::Obj('ApplicantDetails')->getProcessOrderDescription ( $OrgID, $LastProcessOrder );
$disposition            =   G::Obj('ApplicantDetails')->getDispositionCodeDescription ( $OrgID, $LastDispositionCode );
$jobtitle               =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
$reqownersname          =   G::Obj('RequisitionDetails')->getRequisitionOwnersName ( $OrgID, $RequestID );
$requestids             =   G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
$RequisitionInfo        =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequestID, Active", $OrgID, $RequestID);

$linkedin               =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'soc_linkedin');
$informed               =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'informed');

//Get Wotc Ids list
$wotcid_info			=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $multiorgid_req);
//Get Wotc Application Header Information
$wotc_app_header_info	=	G::Obj('WOTCIrecruitApplications')->getWotcApplicationInfo("*", $wotcid_info['wotcID'], $ApplicationID, $RequestID);
//Get Wotc ApplicationID
$WotcApplicationID		=	$wotc_app_header_info['ApplicationID'];
$WotcFormID				=	$wotc_app_header_info['WotcFormID'];

$wotc_app_data_info		=	G::Obj('WOTCApplicantData')->getApplicantDataInfo("Qualifies", $wotcid_info['wotcID'], $WotcFormID, $WotcApplicationID);

if(isset($wotcid_info['wotcID']) && $wotcid_info['wotcID']) {
	$APPDATA['WotcApplicationQualify'] = "Application Pending";
}

if($wotc_app_data_info['Qualifies'] == "Y")
{
	$APPDATA['WotcApplicationQualify'] = "<font color=\"green\">Potentially Qualified</font>";
}
if($wotc_app_data_info['Qualifies'] == "N")
{
	$APPDATA['WotcApplicationQualify'] = "<font color=\"red\">Not Qualified</font>";
}

$APPDATA['RequisitionExist'] = "True";
if($RequisitionInfo['RequestID'] == "") {
	$APPDATA['RequisitionExist'] = "False";
}

if($Applicants['Rating'] == "" || !is_float($Applicants['Rating'])) $Applicants['Rating'] = 0;
$APPDATA['ApplicantConsiderationStatus1']   =   $Applicants ['ApplicantConsiderationStatus1'];
$APPDATA['ApplicantConsiderationStatus2']   =   $Applicants ['ApplicantConsiderationStatus2'];
$APPDATA['IsInternalApplication']           =   ($Applicants ['IsInternalApplication'] == "Yes") ? "Yes" : "No";

if ($feature ['InternalRequisitions'] == "Y") {
    $APPDATA['FeatureInternalRequisitions'] =   'Y';
}
else {
    $APPDATA['FeatureInternalRequisitions'] =   'N';
}

$APPDATA['FormID']              =   $FormID;
$APPDATA['LeadGenerator']       =   @implode(", ", $Applicants ['LeadGenerator']);
$APPDATA['RequisitionStatus']   =   $RequisitionInfo['Active'];
$APPDATA['Paperless']           =   $Applicants ['Paperless'];
$APPDATA['Rating']              =   $Applicants ['Rating'];
$APPDATA['ApplicantScore']      =   $applicant_score;
$APPDATA['TotalScore']          =   $total_weight;
$APPDATA['Rating']              =   $Applicants['Rating'];

$APPDATA['ApplicationID']       =   $_REQUEST['ApplicationID'];
$APPDATA['OrgID']               =   $OrgID;
$APPDATA['RequestID']           =   $RequestID;

$APPDATA['ApplicationDate']     =   $ApplicationDate;
$APPDATA['ReceivedDate']        =   $ReceivedDate;
$APPDATA['LastProcessOrder']    =   $LastProcessOrder;
$APPDATA['LastDispositionCode'] =   $LastDispositionCode;
$APPDATA['LastEffDate']         =   $LastEffDate;
$APPDATA['status']              =   $status;
$APPDATA['disposition']         =   $disposition;
$APPDATA['UserRole']            =   $USERROLE;

$APPDATA['jobtitle']            =   $jobtitle;
$APPDATA['reqownersname']       =   $reqownersname;
$APPDATA['requestids']          =   $requestids;
$APPDATA['linkedin']            =   (is_null($linkedin) || !isset($linkedin)) ? '' : $linkedin;
$APPDATA['informed']            =   (is_null($informed) || !isset($informed)) ? '' : $informed;

$AHomePhone                     =   ($APPDATA ['homephone'] != "") ? json_decode($APPDATA ['homephone'], true) : "";
$APPDATA['homephone']           =   $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $AHomePhone[0], $AHomePhone[1], $AHomePhone[2], $AHomePhone[3] );

$ACellPhone                     =   ($APPDATA ['cellphone'] != "") ? json_decode($APPDATA ['cellphone'], true) : "";
$APPDATA['cellphone']           =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $ACellPhone[0], $ACellPhone[1], $ACellPhone[2], $ACellPhone[3] );

$APPDATA['FormattedAddress']    =   G::Obj('Address')->formatAddress ( $OrgID, $APPDATA ["country"], $APPDATA ["city"], $APPDATA ["state"], $APPDATA['province'], $APPDATA ["zip"], $APPDATA ["county"] );

$source                         =   $APPDATA ['source'];

$forms_count_info               =   G::Obj('FormsInternal')->getApplicantAssignedFormsCountInfo($OrgID, $ApplicationID, $RequestID);
$APPDATA ['AssignedFormsCount'] =   "Pending Forms: ".$forms_count_info['PendingFormsCount']."&nbsp; Completed Forms: ".$forms_count_info['CompletedFormsCount']."&nbsp; Finalized Forms: ".$forms_count_info['FinalizedFormsCount'];
if(isset($_REQUEST['ApplicationID'])) $ApplicationID = $_REQUEST['ApplicationID'];

//Get Affirmative Action, Veteran, Disabled Forms
$aa_sec_info    =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $FormID, "AA");
$vet_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $FormID, "VET");
$dis_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $FormID, "DIS");

$APPDATA['AAActive']    =   $aa_sec_info['Active'];
$APPDATA['VETActive']   =   $vet_sec_info['Active'];
$APPDATA['DISActive']   =   $dis_sec_info['Active'];

// Set where condition
$formque_where  =   array (
                		"OrgID            =   :OrgID",
                		"FormID           =   :FormID",
                		"SectionID        =   6",
                		"QuestionTypeID   =   8",
                		"Active           =   'Y'"
                    );
// Set parameters
$formque_params =   array (
                		":OrgID"          =>  $OrgID,
                		":FormID"         =>  $FormID
                    );
$resultsA       =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "QuestionID, value, Required", $formque_where, "QuestionOrder", array ($formque_params) );

$attachments    =   array();

$APPDATA['display_download_link'] = "False";

if (is_array ( $resultsA ['results'] )) {
	foreach ( $resultsA ['results'] as $AA ) {

		// Set attachment parameters
		$attach_params    =   array (
                    				":OrgID"            => $OrgID,
                    				":ApplicationID"    => $ApplicationID,
                    				":TypeAttachment"   => $AA ['QuestionID']
                        		);
		// Set attachment where condition
		$attach_where     =   array (
                    				"OrgID              = :OrgID",
                    				"ApplicationID      = :ApplicationID",
                    				"TypeAttachment     = :TypeAttachment"
                        		);
		$resultsB         =   G::Obj('Attachments')->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );
		
		$hit              =   $resultsB ['count'];

		if ($hit > 0) {
			//If ZipOptions exist
			if(in_array($AA ['QuestionID'], $zip_options)) {
				$APPDATA['display_download_link'] = "True";
			}
			$attachments[$AA ['QuestionID']]['AttachmentType'] = $AA ['value'];
			$attachments[$AA ['QuestionID']]['Link'] = 'display_attachment.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&Type=' . $AA ['QuestionID'];
		} else {
			$attachments[$AA ['QuestionID']]['AttachmentType'] = $AA ['value'];
			$attachments[$AA ['QuestionID']]['Link'] = "Not Submitted";
		}

	} // end foreach
}

$APPDATA['attachments'] = $attachments;

if(in_array("appview", $zip_options)) {
	$APPDATA['display_download_link'] = "True";
}
if(in_array("apphistory", $zip_options)) {
	$APPDATA['display_download_link'] = "True";
}

$columns        =   "UniqueID, FormName, FormType";
$where_info     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = '1'", "Status >= 3", "FormType IN ('Agreement', 'PreFilled', 'WebForm')");
$params_info    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);

$assigned_completed_forms_res   =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo($columns, $where_info, "", "", array($params_info));
$assigned_completed_forms       =   $assigned_completed_forms_res['results'];
$assigned_completed_forms_cnt   =   $assigned_completed_forms_res['count'];

if(in_array("iconnectforms", $zip_options) && $assigned_completed_forms_cnt > 0) {
    $APPDATA['display_download_link'] = "True";
}

// Set where condition
$where      =   array ("OrgID = :OrgID", "RequestID = :RequestID");
// Set parameters
$params     =   array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Get Job Applications Information
$resultsCNT =   G::Obj('Applications')->getJobApplicationsInfo ( "count(*) as cnt", $where, '', '', array ($params) );

$APPDATA['applicants_count'] = $resultsCNT ['results'] [0] ['cnt'];

foreach ($APPDATA as $a_key=>$a_value) {
	if(is_string($a_value) && $a_key == 'otherskills') $APPDATA[$a_key] = str_replace("?", "&#183;", utf8_decode($a_value));
}

foreach ($APPDATA as $app_data_key=>$app_data_value) {
    if(is_string($app_data_value)) {
        $APPDATA[$app_data_key] = utf8_decode($app_data_value);
    }
}

//Get applicant permission
$cell_phone_permission	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphonepermission');

$APPDATA["CellPhonePermission"]	=	$cell_phone_permission;

$twilio_agreement_info  =   G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($OrgID);

if($twilio_agreement_info['Active'] != 'Y') {
    $APPDATA["CellPhonePermission"]	=	"No";
}

$APPDATA	=	array_map('encode_all_strings', $APPDATA);

//To encode the first level array values in APPDATA
function encode_all_strings($app_info) {
	if(!is_array($app_info)) {
		return utf8_encode($app_info);
	}
	else {
		return $app_info;
	}
}

$APPDATA['TagsSet']=G::Obj('ComparativeAnalysis')->getApplicantComparativeLabels($OrgID, $USERID, $RequestID, $ApplicationID);

header('Content-Type: application/json; charset=utf-8');
echo json_encode($APPDATA);
exit;
?>
