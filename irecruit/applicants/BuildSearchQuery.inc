<?php
if(isset($Sort)) {
    if($Sort == '') {
        $Sort = 'EntryDate';
    }
    
	$SortValue = $Sort;
	$SortType = $to_sort;
	if($Sort == "RequisitionTitle") $Sort = "(SELECT R.Title FROM Requisitions R WHERE R.OrgID = JobApplications.OrgID AND R.RequestID = JobApplications.RequestID) $to_sort, EntryDate";
	if($Sort == "ApplicantSortName") $Sort = 'ApplicantSortName';
	if($Sort == "AppID") $Sort = 'ApplicationID';
	if($Sort == "ApplicantStatus") $Sort = "ProcessOrder $to_sort, EntryDate";
	if($Sort == "EntryDate") $Sort = "JobApplications.EntryDate";
	if($Sort == "ProcessStatus") $Sort = "JobApplications.ProcessOrder";
}

$search_where = array("JobApplications.OrgID = :OrgID");
$filter_params[":OrgID"] = $OrgID;

$search_where[] = "(EntryDate <= :FinalDate1 AND EntryDate >= :BeginDate1)";

if($ApplicationDate_From != "00/00/0000" 
	&& $ApplicationDate_From != ""
	&& $ApplicationDate_To != "00/00/0000"
	&& $ApplicationDate_To != "") {
	
	$BeginDate = substr ( $ApplicationDate_From, - 4 ) . '-' . substr ( $ApplicationDate_From, 0, 2 ) . '-' . substr ( $ApplicationDate_From, 3, 2 ) . ' 00:00:00';
	$FinalDate = substr ( $ApplicationDate_To, - 4 ) . '-' . substr ( $ApplicationDate_To, 0, 2 ) . '-' . substr ( $ApplicationDate_To, 3, 2 ) . ' 23:59:59';
	
	$filter_params[":FinalDate1"] = $FinalDate;
	$filter_params[":BeginDate1"] = $BeginDate;
}

if(isset($ProcessOrder) && $ProcessOrder != "") {
	$filter_params[":ProcessOrder"] = $ProcessOrder;
}
if(isset($Code) && $Code != "") {
	$filter_params[":DispositionCode"] = $Code;
}

if (isset($filter_params[":ProcessOrder"]) && $filter_params[":ProcessOrder"] != "") {
	$search_where[] = "ProcessOrder = :ProcessOrder";
}

if (isset($filter_params[":DispositionCode"]) && $filter_params[":DispositionCode"] != "") {
	$search_where[] = "DispositionCode = :DispositionCode";
}

$is_search_words = false;
if(isset($search_keyword) && $search_keyword != '') {
	$search_words_list = explode(" ", $search_keyword);
	$words_count = count($search_words_list);
	if($words_count > 0) {
		for($swi = 0; $swi < $words_count; $swi++) {
			$search_word_value = trim($search_words_list[$swi]);
			//Have to put flag if resume parsing is turned off
			if($search_word_value != "") {
				if ($feature['ResumeParsing'] == "Y") {
					$is_search_words = true;
					$swi = $words_count;
				}
			}
		}
	}				
	
	$order = 'JobApplications.EntryDate '.$to_sort;
} else {
	$match = '';
	$order = 'JobApplications.EntryDate '.$to_sort;
}

if(isset($RequestIDs)) {
	if(is_array($RequestIDs) && count($RequestIDs) > 0) {
		foreach ( $RequestIDs as $t ) {
			if($t != "") $requestid .= "'" . $t . "',";
		}
		$requestid = substr ( $requestid, 0, - 1 );
		
		if($requestid != "") $search_where[] = "RequestID IN ($requestid)";
	}
}
else {
	
	if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager'
		&& !isset($RequestIDs)) {
		$search_where[] = $rolerestrict = $RequisitionsObj->restrictRequestID ( $OrgID, $USERID );
	}
	else if ((! isset($RequestIDs)) && ($refinereq != "no") && ($refinereq != "")) {
	
		$refine = "";
		$reqs = "";
	
		//Set parameters
		$where = array("OrgID = :OrgID");
		//Set where condition
		$params = array(":OrgID"=>$OrgID);
		//Get RequestIDs
		$results = G::Obj('Requisitions')->getRequisitionInformation("RequestID", $where, "", "", array($params));
	
		if(is_array($results['results'])) {
			foreach ($results['results'] as $REQS) {
				$reqs .= "'" . $REQS ['RequestID'] . "',";
			}
		}
	
		$reqs = substr ( $reqs, 0, - 1 );
		if ($reqs) {
			$search_where[] = "RequestID in ($reqs)";
		} // end if reqs
	} // end refinereq
	else if (($Active != "") && ($Active != "B")) {
		$filter_params[":RequisitionStatus"] = $Active;
		$search_where[] = "RequisitionStatus = :RequisitionStatus";
	} // end if active Y or N
}


if ($feature ['MultiOrg'] == "Y" 
		&& !isset($_REQUEST['IndexStart']) && $MultiOrgID != "") {
	if ($MultiOrgID == "ORIG") {
		$filter_params[":MultiOrgID"] = '';
		$search_where[] = "MultiOrgID = :MultiOrgID";
	} else {
		$filter_params[":MultiOrgID"] = $MultiOrgID;
		$search_where[] = "MultiOrgID = :MultiOrgID";
	}
} // end feature

$row = G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Applicationview");

if ($row ['Applicationview'] == "S") {
	unset($filter_params[":Distinction"]);
	$filter_params[":Distinction"] = 'P';
	$search_where[] = "Distinction = :Distinction";
}

if(isset($_REQUEST['InternalApplications']) && $_REQUEST['InternalApplications'] == "Yes") {
    unset($filter_params[":IsInternalApplication"]);
    $filter_params[":IsInternalApplication"] = 'Yes';
    $search_where[] = "IsInternalApplication = :IsInternalApplication";
}

//Set columns
$columns = "JobApplications.ApplicationID, ApplicantSortName, RequestID, date_format(EntryDate,'%m/%d/%Y %H:%i EST') as EntryDate, ProcessOrder, DispositionCode, StatusEffectiveDate, Correspondence, ReceivedDate, MultiOrgID, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2, Rating, LeadGenerator, LeadStatus, IsInternalApplication";
$count_column = "COUNT(JobApplications.ApplicationID) AS ApplicantsCount";

if($is_search_words == true) {
	 $query_from = " FROM `JobApplications` LEFT JOIN `Parsing` 
					 ON JobApplications.`OrgID` = Parsing.`OrgID` 
					 AND JobApplications.`ApplicationID` = Parsing.`ApplicationID`";

	 $main_search_query = "SELECT $columns".$query_from;
	 $main_count_query 	= "SELECT $count_column".$query_from;
}
else {
	$query_from = " FROM JobApplications";
	
	$main_search_query = "SELECT $columns".$query_from;
	$main_count_query = "SELECT $count_column".$query_from;
}

if ($_REQUEST ['process'] == 'Y') {
	
	if(!isset($SortValue) || $SortValue == "") $SortValue = "EntryDate";
	if(!isset($SortType) || $SortType == "") $SortType = "DESC";
	
	//SearchQuery Information to insert
	$info          =   array("OrgID"=>$OrgID, "UserID"=>$USERID, "GuID"=>$ApplicantsSearchGuID, "Query"=>$main_search_query, "MainSearchWhere"=>implode(" AND ", $search_where), "SearchParams"=>json_encode($filter_params), "SortValue"=>$SortValue, "SortType"=>$SortType, "LastUpdated"=>"NOW()");
	//Set SearchQuery Information
	$on_update     =   " ON DUPLICATE KEY UPDATE Query = :UQuery, FilterSearchWhere = '', MainSearchWhere = :UMainSearchWhere, SearchParams = :USearchParams, SortValue = :USortValue, SortType = :USortType, LastUpdated = NOW()";
	//Update Information
	$update_info   =   array(":UQuery"=>$main_search_query, ":UMainSearchWhere"=>implode(" AND ", $search_where), ":USortValue"=>$SortValue, ":USearchParams"=>json_encode($filter_params), ":USortType"=>$SortType);
	//Insert Search Query
	G::Obj('Organizations')->insSearchQuery($info, $on_update, $update_info);
}

/*
if (isset($query) && preg_match ( '/SCORE/', $query )) {
	$weighted = "Y";
}

if(count($search_where) > 0) {
	$search_query .= " WHERE " . implode(" AND ", $search_where);
	$count_query  .= " WHERE " . implode(" AND ", $search_where);
}
*/

if (isset($Sort)) {
	$order = $Sort .' '. $to_sort;
}

$SEARCHRESULTS = "";

$Start = 0;
if(isset($_REQUEST['IndexStart'])) $Start = $_REQUEST['IndexStart'];
$order .= " LIMIT $Start, $LimitApplicants";

if (isset($_REQUEST['IndexStart'])) {
    
	//Set where condition
	$where     =   array("OrgID = :OrgID", "UserID = :UserID", "GuID = :GuID");
	//Set parameters
	$params    =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":GuID"=>$ApplicantsSearchGuID);
	//Get SearchQuery Information
	$resultsIN =   G::Obj('Organizations')->getSearchQuery("Query, MainSearchWhere", $where, '', array($params));
	$TemplateObj->where_params         =   $where_params       =   $resultsIN['results'][0]['MainSearchWhere'];
	$TemplateObj->main_search_query    =   $main_search_query;
	
	$search_query_filter = $main_search_query . " WHERE " . $where_params;
	$query = $main_count_query . " WHERE " . $where_params;
	
	$keyword_filter_conditions = array();
	$filter_conditions = array();
	
	if($search_keyword != '') {
		$search_words_list = explode(" ", $search_keyword);
		$words_count = count($search_words_list);
		$search_word_conditions = array();
		if($words_count > 0) {
			for($swi = 0; $swi < $words_count; $swi++) {
				$search_word_value = trim($search_words_list[$swi]);
				//Have to put flag if resume parsing is turned off
				if($search_word_value != "") {
					if ($feature['ResumeParsing'] == "Y") {
						$is_search_words = true;
						$keyword_filter_conditions[] = "(JobApplications.ApplicationID = '".Database::escape($search_word_value)."' OR ApplicationAnswers LIKE '%".Database::escape($search_word_value)."%' OR Comments LIKE '%".Database::escape($search_word_value)."%' OR TEXT LIKE '%".Database::escape($search_word_value)."%' OR ApplicantSortName LIKE '%".Database::escape($search_word_value)."%' OR REPLACE(ApplicantSortName, ',', '') LIKE '%".Database::escape($search_word_value)."%')";
					}
					else {
						$keyword_filter_conditions[] = "(JobApplications.ApplicationID = '".Database::escape($search_word_value)."' OR ApplicationAnswers LIKE '%".Database::escape($search_word_value)."%' OR Comments LIKE '%".Database::escape($search_word_value)."%' OR ApplicantSortName LIKE '%".Database::escape($search_word_value)."%' OR REPLACE(ApplicantSortName, ',', '') LIKE '%".Database::escape($search_word_value)."%')";
					}
				}
			}
		
			if(count($keyword_filter_conditions) > 0) $search_word_condition = implode(" OR ", $keyword_filter_conditions);
		
			if($search_word_condition != "") $filter_conditions[] = "(".$search_word_condition.")";
		}
	
	}
	
	if(isset($irecruit_filter) && $irecruit_filter != "") {
		unset($filter_params[":ApplicantConsiderationStatus1"]);
		$filter_conditions[] = "ApplicantConsiderationStatus1 = :ApplicantConsiderationStatus1";
		$filter_params[":ApplicantConsiderationStatus1"] = $irecruit_filter;
	}
	
	if(isset($org_app_lbl_filter) && $org_app_lbl_filter != "") {
		unset($filter_params[":ApplicantConsiderationStatus2"]);
		$filter_conditions[] = "ApplicantConsiderationStatus2 = :ApplicantConsiderationStatus2";
		$filter_params[":ApplicantConsiderationStatus2"] = $org_app_lbl_filter;
	}
	
	if(isset($filter_rating) && $filter_rating != "" &&  $filter_rating != "0") {
		unset($filter_params[":Rating"]);
		$filter_conditions[] = "Rating = :Rating";
		$filter_params[":Rating"] = $filter_rating;
	}
	
	if(isset($LeadGenerator) && $LeadGenerator != "") {
	    unset($filter_params[":LeadGenerator"]);
	    $filter_conditions[] = "LeadGenerator LIKE :LeadGenerator";
	    $filter_params[":LeadGenerator"] = "%".$LeadGenerator."%";
	}
	
	if(isset($_REQUEST['InternalApplications']) && $_REQUEST['InternalApplications'] == "Yes") {
	    unset($filter_params[":IsInternalApplication"]);
	    $filter_conditions[] = "IsInternalApplication = :IsInternalApplication";
	    $filter_params[":IsInternalApplication"] = "Yes";
	}
	
	if(count($filter_conditions) > 0) {
	    $query .= " AND " . implode(" AND ", $filter_conditions);
	    $search_query_filter .= " AND " . implode(" AND ", $filter_conditions);
	}
	
	if($query != "") {
		$query = str_replace("WHERE AND ", "WHERE ", $query);
		$applicants_count_info = $ApplicationsObj->getInfo($query, array($filter_params));
	}

	if($search_query_filter != "") {
		$search_query_filter  .=  " ORDER BY ".$order;
		$search_query_filter  =   str_replace("WHERE AND ", "WHERE ", $search_query_filter);
		$search_results       =   $ApplicationsObj->getInfo($search_query_filter, array($filter_params));
	}
	
	$TemplateObj->total_applicants_count = $total_applicants_count = $applicants_count_info['results'][0]['ApplicantsCount'];
	
	if($SortValue == "") $SortValue = "EntryDate";
	if($SortType == "") $SortType = "DESC";
	
	//SearchQuery Information to insert
	$info          =   array("OrgID"=>$OrgID, "UserID"=>$USERID, "GuID"=>$ApplicantsSearchGuID, "Query"=>$main_search_query, "FilterSearchWhere"=>implode(" AND ", $filter_conditions), "SearchParams"=>json_encode($filter_params), "SortValue"=>$SortValue, "SortType"=>$SortType, "LastUpdated"=>"NOW()");
	//Set SearchQuery Information
	$on_update     =   " ON DUPLICATE KEY UPDATE Query = :UQuery, FilterSearchWhere = :UFilterSearchWhere, SearchParams = :USearchParams, SortValue = :USortValue, SortType = :USortType, LastUpdated = NOW()";
	//Update Information
	$update_info   =   array(":UQuery"=>$main_search_query, ":UFilterSearchWhere"=>implode(" AND ", $filter_conditions), ":USortValue"=>$SortValue, ":USortType"=>$SortType, ":USearchParams"=>json_encode($filter_params));
	//Insert Search Query
	$OrganizationsObj->insSearchQuery($info, $on_update, $update_info);
	
} // end QID
else {
	
	if($is_search_words == true) {
		$applicants_count_info = $ApplicationsObj->getJobApplicationsSearchInfo("COUNT(JobApplications.ApplicationID) AS ApplicantsCount", $search_where, '', '', array($filter_params));
	}
	else {
		$applicants_count_info = $ApplicationsObj->getJobApplicationsInfo("COUNT(JobApplications.ApplicationID) AS ApplicantsCount", $search_where, '', '', array($filter_params));
	}
	
	$TemplateObj->total_applicants_count = $total_applicants_count = $applicants_count_info['results'][0]['ApplicantsCount'];
	
	if($is_search_words == true) {
		$search_results = $ApplicationsObj->getJobApplicationsSearchInfo($columns, $search_where, '', $order, array($filter_params));
	}
	else {
		$search_results = $ApplicationsObj->getJobApplicationsInfo($columns, $search_where, '', $order, array($filter_params));
	}
}

//Get Job Applications Information
$applicants_search_results = $search_results['results'];

//Set count
$cnt = $search_results['count'];

$TemplateObj->SEARCHhit     =   $SEARCHhit = 0;
$TemplateObj->filter_params =   $filter_params;
$rowcolor = "#eeeeee";
?>