<?php 
require_once '../Configuration.inc';

//Get User Detail Information
$USER                   =   $UserPortalUsersObj->getUserDetailInfoByEmail ( "*", $_REQUEST['Email'] );
$MultiOrgID             =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $_REQUEST['RequestID']);
$APPDATA                =   $ApplicantsObj->getAppData($OrgID, $_REQUEST['ApplicationID']);
$LeadApplicationFormID  =   $ApplicantDetailsObj->getFormID($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
$RequisitionTitle       =   $RequisitionDetailsObj->getJobTitle($OrgID, $MultiOrgID, $_REQUEST['RequestID']);

//Process Lead Template Information
$lead_template_master   =   $LeadGeneratorEmailTemplateObj->getProcessLeadTemplateInfo('MASTER');

//Process Lead Template Information
$lead_template_info     =   $LeadGeneratorEmailTemplateObj->getProcessLeadTemplateInfo($OrgID);

if($lead_template_info['RegisteredUserSubject'] == "") 
{
    $lead_template_info['RegisteredUserSubject']    =   $lead_template_master['RegisteredUserSubject'];
}
if($lead_template_info['RegisteredUserMessage'] == "")
{
    $lead_template_info['RegisteredUserMessage']    =   $lead_template_master['RegisteredUserMessage'];
}
if($lead_template_info['NonRegisteredUserSubject'] == "")
{
    $lead_template_info['NonRegisteredUserSubject']    =   $lead_template_master['NonRegisteredUserSubject'];
}
if($lead_template_info['NonRegisteredUserMessage'] == "")
{
    $lead_template_info['NonRegisteredUserMessage']    =   $lead_template_master['NonRegisteredUserMessage'];
}

if(isset($USER['UserID']) && $USER['UserID'] != "") {
    
    if(isset($lead_template_info['RegisteredUserSubject'])
        && $lead_template_info['RegisteredUserSubject'] != ""
        && isset($lead_template_info['RegisteredUserMessage'])
        && $lead_template_info['RegisteredUserMessage'] != "") {
        //Send verification mail to registered user
        $subject = $lead_template_info['RegisteredUserSubject'];

	$edit_app_link = '<a href="'. G::Obj('Organizations')->createRequestHoldLink($OrgID,$MultiOrgID,$_REQUEST['ApplicationID'],$_REQUEST['RequestID'],"EditApplication") .'">Click here to complete application</a>';
        
        $message  = $lead_template_info['RegisteredUserMessage'];

	$ApplicationDate = date_format(date_create(G::Obj('Applications')->getApplicationDate($OrgID, $_REQUEST ['ApplicationID'])),"m/d/Y");
        
        $message  = str_replace("{first}", $USER['FirstName'], $message);
        $message  = str_replace("{last}", $USER['LastName'], $message);
        $message  = str_replace("{RequisitionTitle}", $RequisitionTitle, $message);
        $message  = str_replace("{Email}", $_REQUEST ['Email'], $message);
        $message  = str_replace("{ApplicationDate}", $ApplicationDate, $message);
        $message  = str_replace("{ApplicationID}", $_REQUEST['ApplicationID'], $message);
        $message  = str_replace("{EditApplicationLink}", $edit_app_link, $message);
        
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        
        //Get Email and EmailVerified
        $OE = $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
        //Get OrganizationName
        $OrgName = $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);
        
        //Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
        
        if (($OE['Email']) && ($OE['EmailVerified'] == 1)) {
            // Set who the message is to be sent from
            $PHPMailerObj->setFrom ( $OE['Email'], $OrgName );
            // Set an alternative reply-to address
            $PHPMailerObj->addReplyTo ( $OE['Email'], $OrgName );
        } else {
        	//Get support information
        	$support	=	G::Obj('IrecruitSettings')->getValue("Support");
        	 
            // Set who the message is to be sent from
            $PHPMailerObj->setFrom ( $support['Email'], $support['Name'] );
            // Set an alternative reply-to address
            $PHPMailerObj->addReplyTo ( $support['Email'], $support['Name'] );
        }
        
        // Set who the message is to be sent to
        $PHPMailerObj->addAddress ( $_REQUEST ['Email'] );
        // Set the subject line
        $PHPMailerObj->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        $PHPMailerObj->msgHTML ( $message );
        // Content Type Is HTML
        $PHPMailerObj->ContentType = 'text/html';
        //Send email
        $PHPMailerObj->send ();
    }
    
    $now    =   $MysqlHelperObj->getNow();
    //Update PortalUserID
    $ApplicantDetailsObj->updApplicantAnswer($OrgID, $_REQUEST['ApplicationID'], 'PortalUserID', $USER['UserID']);
    //Update AssignedManually
    $ApplicantDetailsObj->updApplicantAnswer($OrgID, $_REQUEST['ApplicationID'], 'AssignedManually', $USER['UserID']);
    //Update LeadApplicationFormID
    $ApplicantDetailsObj->updApplicantAnswer($OrgID, $_REQUEST['ApplicationID'], 'LeadApplicationFormID', $LeadApplicationFormID);
    //Update LeadProcessedDate
    $ApplicantDetailsObj->updApplicantAnswer($OrgID, $_REQUEST['ApplicationID'], 'LeadProcessedDate', $now);
    //Update FormID
    $ApplicantDetailsObj->updApplicantAnswer($OrgID, $_REQUEST['ApplicationID'], 'FormID', $_REQUEST['FormID']);
    
    //Get Lead Generator Information
    $lead_info  =   $ApplicantDetailsObj->getLeadGeneratorInfo($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
    if($lead_info != "" && ($lead_info == "Monster" || $lead_info == "Indeed" || $lead_info == "ZipRecruiter")) {
        $lead_name  =   $lead_info;
    }
    else if($lead_info != "") {
        $lead_name  =   $ApplicationLeadsObj->getLeadName($OrgID, $lead_info);
    }
    
    if($lead_name != "") {
        $Comments = "Email sent to applicant to complete application. (" . $lead_name . ") <br><br>";
        $Comments .= "Subject: ".$subject."<br><br>";
        $Comments .= $message;
    }
    else {
        $Comments = "Email sent to applicant to complete application. <br><br>";
        $Comments .= "Subject: ".$subject."<br><br>";
        $Comments .= $message;
    }
    
    $job_application_history = array (
        "OrgID"                 =>  $OrgID,
        "ApplicationID"         =>  $_REQUEST['ApplicationID'],
        "RequestID"             =>  $_REQUEST['RequestID'],
        "ProcessOrder"          =>  "-17",
        "StatusEffectiveDate"   =>  "DATE(NOW())",
        "Date"                  =>  "NOW()",
        "UserID"                =>  $USERID,
        "Comments"              =>  $Comments
    );
    G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );
    
   
    echo json_encode(array('message'=>'Reminder sent successfully to user', 'status'=>'success'));
}
?>
