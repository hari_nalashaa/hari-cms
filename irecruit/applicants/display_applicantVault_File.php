<?php
require_once '../irecruitdb.inc';

if ($_REQUEST ['ApplicationID'] != "") {
	
	// Set where condition
	$where     =   array(
                        "OrgID          =   :OrgID",
                        "ApplicationID  =   :ApplicationID",
                        "UpdateID       =   :UpdateID" 
                    );
	// Set parameters
	$params    =   array (
                        ":OrgID"            =>  $_REQUEST ['OrgID'],
                        ":ApplicationID"    =>  $_REQUEST ['ApplicationID'],
                        ":UpdateID"         =>  $_REQUEST ['UpdateID'] 
                    );
	// Get Applicant Vault Information
    $results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
	
    $AV         =   $results ['results'] [0];
	
    $filename   =   $AV ['ApplicationID'] . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '.' . $AV ['FileType'];

} else {
    
    $filename   =   $File;
    
} // end else ApplicationID

if ($filename) {
	
	$dfile = '';
	
	$dfile = IRECRUIT_DIR . 'vault/' . $_REQUEST ['OrgID'] . '/applicantvault/' . $filename;
	
	if (file_exists ( $dfile )) {
		header ( 'Content-Description: File Transfer' );
		header ( 'Content-Type: application/octet-stream' );
		header ( 'Content-Disposition: attachment; filename=' . basename ( $dfile ) );
		header ( 'Content-Transfer-Encoding: binary' );
		header ( 'Expires: 0' );
		header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header ( 'Pragma: public' );
		header ( 'Content-Length: ' . filesize ( $dfile ) );
		readfile ( $dfile );
		exit ();
	}
} // end if FileName
?>
