<?php
require_once '../Configuration.inc';

$FILES  =   array();

if ($_FILES['resumefile']['type'] != "") {
	$MultiOrgID =   G::Obj('RequisitionDetails')->getMultiOrgID($_REQUEST['OrgID'], $_REQUEST['RequestID']);
	$FormID     =   G::Obj('RequisitionDetails')->getFormID($_REQUEST['OrgID'], $MultiOrgID, $_REQUEST['RequestID']);
	G::Obj('Sovren')->processPublicPortalResumeForm($_REQUEST['OrgID'], $FormID, $_REQUEST['RequestID'], $_REQUEST['HoldID'], $_FILES ['resumefile'] ['tmp_name']);
	
	$FILES['resumeupload']  =   $_FILES['resumefile'];
	// Bind Parameters
	$params     =   array('OrgID'=>$_REQUEST['OrgID'], 'HoldID'=>$_REQUEST['HoldID'], 'QuestionID'=>"FILES_DATA", 'Answer'=>serialize($FILES));
	// Insert applicant data temp
	G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params);
	echo "RELOAD";
}
?>