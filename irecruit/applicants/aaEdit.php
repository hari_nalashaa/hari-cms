<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title = $title = 'Edit Affirmitive Action';

//Add Table Css
$page_styles["header"][] = 'css/table-non-responsive.css';
$TemplateObj->page_styles = $page_styles;

//Set page title
$TemplateObj->action = $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->process = $process = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->processtype = $processtype = isset($_REQUEST['processtype']) ? $_REQUEST['processtype'] : '';

$TemplateObj->ApplicationID = $ApplicationID = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/applicants/AAEdit');
?>