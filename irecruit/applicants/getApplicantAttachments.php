<?php
require_once '../Configuration.inc';

$RequestID = $_REQUEST['RequestID'];

if(isset($_REQUEST['ApplicationID'])) $ApplicationID = $_REQUEST['ApplicationID'];

$application_details = $ApplicationsObj->getJobApplicationsDetailInfo("FormID", $OrgID, $ApplicationID, $RequestID);

$FormID = $application_details['FormID'];

// Set where condition
$formque_where  =   array (
                        "OrgID          =   :OrgID",
                        "FormID         =   :FormID",
                        "SectionID      =   6",
                        "QuestionTypeID =   8",
                        "Active         =   'Y'"
                    );
// Set parameters
$formque_params =   array (
                		":OrgID"      =>  $OrgID,
                		":FormID"     =>  $FormID
                    );
$resultsA = $FormQuestionsObj->getFormQuestionsInformation ( "QuestionID, Question, value, Required", $formque_where, "QuestionOrder", array (
		$formque_params
) );

if (is_array ( $resultsA ['results'] )) {
	foreach ( $resultsA ['results'] as $AA ) {

		// Set attachment parameters
        $attach_params  =   array (
                        		":OrgID"          =>  $OrgID,
                        		":ApplicationID"  =>  $ApplicationID,
                        		":TypeAttachment" =>  $AA ['QuestionID']
                            );
        // Set attachment where condition
        $attach_where   =   array (
                        		"OrgID            =   :OrgID",
                        		"ApplicationID    =   :ApplicationID",
                        		"TypeAttachment   =   :TypeAttachment"
                            );
		$resultsB =   $AttachmentsObj->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );
		$hit      =   $resultsB ['count'];

		if ($hit > 0) {
			$link = '<a href="' . IRECRUIT_HOME . 'display_attachment.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&Type=' . $AA ['QuestionID'] . '">';
			$link .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
			$link .= 'Download</a>';
				
			$edit = ' <a href="javascript:void(0);" onclick="add_edit_attachments(\'applicants/editAttachments.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&Type=' . $AA ['QuestionID'] . '&edit=Y&action=updateapplicant';
			if ($AccessCode != "") {
				$edit .= "&k=" . $AccessCode;
			}
			$edit .= '\');"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;">Replace</a>';
		} else {
			$link = "Not Submitted";
			$edit = ' <a href="javascript:void(0);" onclick="add_edit_attachments(\'applicants/editAttachments.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&Type=' . $AA ['QuestionID'] . '&edit=New&action=updateapplicant';
			if ($AccessCode != "") {
				$edit .= "&k=" . $AccessCode;
			}
			$edit .= '\');"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">Add</a>';
		}

		echo '<tr><td>' . $AA ['Question'] . '</td><td>' . $link . '&nbsp;&nbsp;&nbsp;' . $edit . '</td></tr>';
	} // end foreach
}
?>