<?php
// *** FUNCTIONS *** //
function displayList($multipleapps, $OrgID, $permit) {
	global $RequisitionsObj, $ApplicationsObj, $DisplayApplicantSummary, $RequisitionDetailsObj;
	
	echo '<table border="0" cellspacing="3" cellpadding="5" width="770" class="table table-striped table-bordered">';
	echo '<tr><td>';
	
	$ma = explode ( '|', $multipleapps );
	$macnt = count ( $ma ) - 1;
	
	for($i = 0; $i < $macnt; $i ++) {
		
		$jarj             =   explode ( ':', $ma [$i] );
		$ApplicationID    =   $jarj [0];
		$RequestID        =   $jarj [1];
		
		if (($ApplicationID) && ($RequestID)) {
			
			if($DisplayApplicantSummary != 'no') {
				echo '<a href="' . IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '"><img src="' . IRECRUIT_HOME . 'images/icons/user_go.png" border="0" title="Applicant Summary">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">Applicant Summary</b></a>';
			}
			echo '&nbsp;&nbsp;';
			$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
			echo $ApplicationID . ' - ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
			echo '&nbsp;&nbsp;';
			echo getEmailAddress ( $OrgID, $ApplicationID, $permit );
			echo '<br>';
			
		}
	} // end for
	
	echo '</td></tr></table>';
} // end function

function getEmailAddress($OrgID, $ApplicationID, $permit) {
	global $ApplicantsObj;
	
	$rtn = '';
	
	//Set where condition
	$where_info    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "QuestionID IN ('email','first','last')");
	//Set parameters
	$params        =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
	//Get ApplicantData Information
	$results       =   $ApplicantsObj->getApplicantDataInfo("QuestionID, Answer", $where_info, '', '', array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $AD) {
		
			if ($AD ['QuestionID'] == 'first') {
				$first = $AD ['Answer'];
			}
			if ($AD ['QuestionID'] == 'last') {
				$last = $AD ['Answer'];
			}
			if ($AD ['QuestionID'] == 'email') {
				$email = $AD ['Answer'];
			}
		}
	}
	
	
	$rtn = $first . ' ' . $last;
	
	if ($permit ['Applicants_Contact'] == 1) {
		$rtn .= ',&nbsp;<span><a href="mailto:' . $email . '">(' . $email . ')</a></span>';
	} else {
		$rtn .= ',&nbsp;(' . $email . ')';
	}
	
	return $rtn;
} // end function
?>
