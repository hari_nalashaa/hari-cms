<?php
require_once '../Configuration.inc';

$RelatedApplications = "";

//Set where condition
$where		=	array("JA.OrgID = :OrgID", "JA.ApplicationID = AD.ApplicationID", "JA.OrgID = AD.OrgID", "AD.QuestionID = :QuestionID", "AD.Answer = :Email", "JA.ApplicationID != :ApplicationID");
//Set parameters
$params		=	array(":OrgID"=>$_REQUEST['OrgID'], ":QuestionID"=>'email', ":Email"=>$_REQUEST['Email'], ":ApplicationID"=>$_REQUEST['ApplicationID']);
//Set columns
$columns 	=	"JA.ApplicationID, JA.RequestID, JA.ProcessOrder, DATE_FORMAT(JA.EntryDate,'%m/%e/%Y') AS EntryDate, JA.DispositionCode";
$results 	=	$ApplicationsObj->getJobApplicationsAndApplicantData($columns, $where, "", array($params));

if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	
	//Set where condition
	$where		=	array("OrgID = :OrgID", "RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :RMOrgID AND UserID = :UserID)");
	//Set parameters
	$params 	=	array(":OrgID"=>$_REQUEST['OrgID'], ":RMOrgID"=>$_REQUEST['OrgID'], ":UserID"=>$_REQUEST['USERID']);
	//Set Requisition Information
	$resultsIN	=	$RequisitionsObj->getRequisitionInformation("RequestID, Title", $where, "", "", array($params));
	
	$hr_request_ids_list = array();
	if(is_array($resultsIN['results'])) {
		foreach ($resultsIN['results'] as $rowIN) {
			$hr_request_ids_list[] = $rowIN['RequestID'];
		}
	}
	
}

$i = 0;

echo "<br>";
if(is_array($results['results'])) {
	foreach($results['results'] as $ALT) {
	    $multiorgid_req		=	$RequisitionDetailsObj->getMultiOrgID($OrgID, $ALT ['RequestID']);
	    $req_detail_info	=	$RequisitionsObj->getRequisitionsDetailInfo("Active", $OrgID, $ALT ['RequestID']);
	     
		if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	
			$print = 0;
	
			if(in_array($ALT ['RequestID'], $hr_request_ids_list)) {
				$print	= 1;
			}
			
		} else { // else USERROLE
	
			$print = 1;
		} // end USERROLE
	
		if ($print > 0) {
			$i ++;
			$div_color = 'background-color:#fffff';
			if($req_detail_info['Active'] != "Y") $div_color = 'background-color:#c5d8f7';
			
			if(isset($_REQUEST['app_display']) && $_REQUEST['app_display'] == 'inline') {
				$RelatedApplications .= '<div style="float:left;border:1px solid #ddd;margin-right:5px;margin-top:5px;padding:10px;'.$div_color.'">'.$i.'<br>';
			}

			if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
			    $RelatedApplications .= $ALT ['ApplicationID'];
			}
			else {
			    $RelatedApplications .= "<a href='".IRECRUIT_HOME."applicantsSearch.php?ApplicationID=".$ALT ['ApplicationID']."&RequestID=".$ALT ['RequestID']."' target='_blank'>".$ALT ['ApplicationID']."</a>";
			}
	
			if ($feature ['MultiOrg'] == "Y") {
				$RelatedApplications .= '<br><span style="font-size:8pt">' . $OrganizationDetailsObj->getOrganizationNameByRequestID ( $_REQUEST['OrgID'], $ALT ['RequestID'] ) . '</span>';
			}
			$RelatedApplications .= '<br><span style="font-size:8pt">' . $RequisitionDetailsObj->getJobTitle ( $_REQUEST['OrgID'], $multiorgid_req, $ALT ['RequestID'] ) . '</span>';

			$RelatedApplications .= '<br><span style="font-size:8pt;">Date Applied: ' . $ALT ['EntryDate'] . '</span>';
			$RelatedApplications .= '<br><span style="font-size:8pt;">Status: ' . $ApplicantDetailsObj->getProcessOrderDescription ( $_REQUEST['OrgID'], $ALT ['ProcessOrder'] ) . '</span>';

			$disposition = $ApplicantDetailsObj->getDispositionCodeDescription ( $_REQUEST['OrgID'], $ALT ['DispositionCode'] );
			if ($disposition != "") {
			$RelatedApplications .= '<br><span style="font-size:8pt;">Disposition: ' . $disposition . '</span>';
			}
			
			$RelatedApplications .= '<br><span style="font-size:8pt">Owner: '.$RequisitionDetailsObj->getRequisitionOwnersName($OrgID, $ALT ['RequestID']) . '</span>';
			$RelatedApplications .= '<br><span style="font-size:8pt">Managers: '.$RequisitionsObj->getRequisitionManagersNames($OrgID, $ALT ['RequestID']) . '</span>';
			
			if(isset($_REQUEST['app_display']) && $_REQUEST['app_display'] == 'inline') {
				$RelatedApplications .= '</div>';
			}
			else {
				$RelatedApplications .= '<br><br>';
			}
		} // end > 0
	} // end foreach
}

echo "<h4>Related Applications:</h4>";

if ($i == 0) {
	echo 'No matches';
}
else if($i > 0)
{
	echo $RelatedApplications;
}	


echo "<div style='clear:both'></div>";

$ApplicationID  = $_REQUEST['ApplicationID'];
$RequestID      = $_REQUEST['RequestID'];

// additional jobs applied for //Set where condition
$where = array (
		"OrgID				=	:OrgID",
		"ApplicationID		=	:ApplicationID",
		"RequestID			!=	:RequestID"
);
// Set parameters
$params = array (
		":OrgID"			=>	$OrgID,
		":ApplicationID"	=>	$ApplicationID,
		":RequestID"		=>	$RequestID
);
// Get CloudEmployeeID
$results = $ApplicationsObj->getJobApplicationsInfo ( "RequestID, ProcessOrder, Distinction, CloudEmployeeID", $where, '', '', array (
		$params
) );

$AdditionalInterest = "";
if (is_array ( $results ['results'] )) {
	foreach ( $results ['results'] as $JA ) {
	    $multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $JA ['RequestID']);
		$requisition = $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $JA ['RequestID'] );

		if (($JA ['ProcessOrder'] == "P") || ($Applicationview == "M")) {
			$requisition = '<a href="' . IRECRUIT_HOME . 'applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $JA ['RequestID'] . '">' . $requisition . '</a>';
		}

		if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	
			$print = 0;
	
			if(in_array($JA ['RequestID'], $hr_request_ids_list)) {
				$print	= 1;
			}
			
		} else { // else USERROLE
	
			$print = 1;
		} // end USERROLE

		if($print > 0) {
			$AdditionalInterest .= "<div style=\"border:1px solid #F5F5F5;\">";
			if ($feature ['MultiOrg'] == "Y") {
				$AdditionalInterest .= $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $JA ['RequestID'] ) . " -<br>";
			}
			$AdditionalInterest .= $requisition . '<br>' . $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $JA ['RequestID'] );
			$AdditionalInterest .= '<br>Req Owner: ' . $RequisitionDetailsObj->getRequisitionOwnersName ( $OrgID, $JA ['RequestID'] );
			$AdditionalInterest .= '<br>Status: ' . $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $JA ['ProcessOrder'] ) . '<br><br>';
			$AdditionalInterest .= "</div>";
		}
	} // end foreach
}

echo "<h4>Additional Interests:</h4>";
if($AdditionalInterest != "") echo "<br>". $AdditionalInterest;
else echo "No matches"
?>
