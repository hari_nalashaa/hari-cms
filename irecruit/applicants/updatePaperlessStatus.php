<?php 
require_once '../Configuration.inc';

$set_info = array("Paperless = :Paperless");
$where_info = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$_REQUEST['ApplicationID'], ":RequestID"=>$_REQUEST['RequestID'], ":Paperless"=>$_REQUEST['Paperless']);
$ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));

echo "Updated successfully";
?>