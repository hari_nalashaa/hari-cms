<?php 
if ($_REQUEST['process'] == 'Y') {
	
	if ($ProcessOrder == "CO") {
		$ProcessOrder = 0;
	}
	if ($Comment == "") {
		$Comment = "no comment";
	}
	
	if ($ProcessOrder > 1) {
		$Comment = "Status updated. " . $_POST ['Comment'];
	}
	if ($ProcessOrder == 9) {
		$Comment = $_POST ['Comment'];
	}
	
	$StatusDate = "NOW()";
	
	if ($StatusEffectiveDate) {
		$StatusDate = substr ( $StatusEffectiveDate, - 4 ) . '-' . substr ( $StatusEffectiveDate, 0, 2 ) . '-' . substr ( $StatusEffectiveDate, 3, 2 );
	} else {
		$DT = $MysqlHelperObj->getCurDate ();
		$StatusDate = $DT;
	}
	
	if ($ProcessOrder == 0) {
		$DispositionCode = '';
	}
	
	// Set JobApplication History Information
	$job_app_history = array (
			"OrgID"                  => $OrgID,
			"ApplicationID"          => $ApplicationID,
			"RequestID"              => $RequestID,
			"ProcessOrder"           => $ProcessOrder,
			"DispositionCode"        => $DispositionCode,
			"StatusEffectiveDate"    => $StatusDate,
			"Date"                   => "NOW()",
			"UserID"                 => $USERID,
			"Comments"               => $Comment 
	);
	
	// Insert JobApplication History
	$ins_res_job_app_his = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
    	
	$affected_rows = $ins_res_job_app_his ['affected_rows'];

	// Applicant Status Log
	$ApplicantStatusLogsObj->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, $ProcessOrder, $DispositionCode);
	
	if ($ProcessOrder != 0) {
		
		// Set where condition
		$where = array ("OrgID = :OrgID", "ProcessOrder = :ProcessOrder");
		// Set parameters
		$params = array (":OrgID" => $OrgID, ":ProcessOrder" => $ProcessOrder);
		
		$results = $ApplicantsObj->getApplicantProcessFlowInfo ( "Searchable", $where, '', array ($params) );
		$APF = $results ['results'] [0] ['Searchable'];
		
		if (isset ( $APF )) {
			// Set Update Information
			$upd_set_info = array (
					"Searchable            = :Searchable",
					"DispositionCode       = :DispositionCode",
					"ProcessOrder          = :ProcessOrder",
					"StatusEffectiveDate   = :StatusEffectiveDate",
					"LastModified          = NOW()" 
			);
			// Set where condition
			$upd_where_info = array (
					"OrgID                 = :OrgID",
					"RequestID             = :RequestID",
					"ApplicationID         = :ApplicationID" 
			);
			// Set parameters
			$upd_params = array (
					":Searchable"          => $APF,
					":DispositionCode"     => $DispositionCode,
					":ProcessOrder"        => $ProcessOrder,
					":StatusEffectiveDate" => $StatusDate,
					":OrgID"               => $OrgID,
					":RequestID"           => $RequestID,
					":ApplicationID"       => $ApplicationID 
			);

			//Update Applications Information
			$ApplicationsObj->updApplicationsInfo ( 'JobApplications', $upd_set_info, $upd_where_info, array ($upd_params) );
			
		}
		
	} // if ProcessOrder <> 0

	$paperless_info = $ApplicationsObj->getJobApplicationsDetailInfo("Paperless", $OrgID, $ApplicationID, $RequestID);
	
	if($paperless_info['Paperless'] == "No" && $ProcessOrder != "CO") {
		include IRECRUIT_DIR . 'formsInternal/AssignForm.inc';
	}

} // /end process
?>
