<?php 
require_once '../Configuration.inc';

$ApplicationID  =   $_POST['ApplicationID'];
$RequestID      =   $_POST['RequestID'];
$ChecklistID    =   $_POST['ChecklistID'];

if (($ChecklistID != "") && ($ApplicationID != "") && ($RequestID != "")) {

$checklist_info =   G::Obj('Checklist')->getCheckList($ChecklistID,$OrgID);
$login_user    =   $USERID;

$set_info       =   array("ChecklistID = :ChecklistID","LastUpdated = NOW()");
$where_info     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
$params         =   array(":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);

//Update Checklist Data Header
G::Obj('ChecklistData')->updChecklistDataHeader($set_info, $where_info, array($params));

//Insert Checklist Data History
$new_checklist_history_info =   G::Obj('ChecklistData')->insChecklistDataHistory($ChecklistID, $OrgID, $ApplicationID, $RequestID, 'Application checklist set to '.$checklist_info[ChecklistName], $login_user,'');

}

?>
