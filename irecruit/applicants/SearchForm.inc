<?php
// Create object for WeightedSearch Class
$WSList = $WeightedSearchObj->WeightedSearchList ($USERID);

// if search or save and search buttons submitted
if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	echo '<form method="post" action="applicants.php" name="Search">';
}
if (preg_match ( '/applicantsSearch.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	echo '<form method="post" action="applicantsSearch.php?menu=2" name="Search">';
}

echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-bordered table-hover">';

if (! $Active) {
	$Active = "Y";
}

if ($saved_searches_count > 0) {
	echo '<tr><td align="right">';

	echo 'Use Saved Search:</td>';
	echo '<td><select name="selectedPage" onChange="getSavedSearchInfo(this.value)" class="form-control width-auto-inline">';
	echo '<option value="">Select</option>';
	foreach ( $saved_searches_list as $key => $value ) {
		
		$search_record_info = $saved_searches_list[$key];
		
		echo '<option value="'.$search_record_info['SearchName'].'">' . $search_record_info['SearchName'] . '</option>';
	}
	echo '<option value="Reset">Reset</option>';
	echo '</select>';
	echo '<span id="msg_saved_search_info" style="color:grey"></span>';
	echo '</td>';
	echo '</tr>';
}

echo '<input type="hidden" name="criteria" value="' . $SearchName . '">';
if ($feature ['MultiOrg'] == "Y") {
	?>
	<tr>
		<td align="right">Organization:</td>
		<td>
			<select name="MultiOrgID" id="MultiOrgID" class="form-control width-auto-inline" onchange="loadRequisitions('<?php echo IRECRUIT_HOME?>','<?php echo $AccessCode?>')">
				<option value="">All</option>
				<?php
					//Set where condition
					$where = array("OrgID = :OrgID");
					//Set parameters
					$params = array(":OrgID"=>$OrgID);
					//Get OrgData Information
					$orginfo = $OrganizationsObj->getOrgDataInfo("*", $where, '', array($params));
	
					if(is_array($orginfo['results'])) {
						foreach ( $orginfo['results'] as $OD ) {
							if ($OD ['MultiOrgID'] == "") {
								$OD ['MultiOrgID'] = "ORIG";
							}
							echo '<option value="' . $OD ['MultiOrgID'] . '"';
							if ($MultiOrgID == $OD ['MultiOrgID']) {
								echo " selected";
							}
							echo '>' . $OD ['OrganizationName'] . '</option>';
						}
					}
				?>
			</select>
		</td>
	</tr>
	<?php
} else { // feature MultiOrg
	echo '<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="">';
} // end feature MultiOrg

//Get UserPreferences
$user_preferences_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
$DR = $user_preferences_info['DefaultSearchDateRange'];
if ($DR == "") {
	$DR = "90";
}

//Set columns
$columns = 'DATE_FORMAT(DATE_SUB(CURDATE(),INTERVAL ' . $DR . '  DAY),"%m/%d/%Y"), DATE_FORMAT(DATE_ADD(CURDATE(),INTERVAL 0 DAY),"%m/%d/%Y")';
$results_date = $MysqlHelperObj->getDatesList($columns);

if(is_array($results_date)) {
	list($StartDate, $EndDate) = array_values($results_date);
}
?>

<tr>
	<td align="right">Application Date From:</td>
	<td>
	<input type="text" id="ApplicationDate_From" 
			name="ApplicationDate_From" value="<?php echo $StartDate;?>" size="10"
			onBlur="validate_date(this.value,' From Date');" class="form-control width-auto-inline" /> &nbsp;&nbsp;thru:&nbsp; 
	<input type="text" id="ApplicationDate_To" name="ApplicationDate_To"
			value="<?php echo $EndDate;?>" size="10" onBlur="validate_date(this.value,' To Date');"
			class="form-control width-auto-inline" />
	</td>
</tr>



<input type="hidden" name="refinereq" value="no">
<tr>
	<td align="right" valign="top">Requisition / Job Posting:<br> <br>
		Limit: 
		<select name="Active" 
				id="Active"
				onChange="loadRequisitions('<?php echo IRECRUIT_HOME?>','<?php echo $AccessCode?>')"
				class="form-control width-auto-inline">
				<?php
				$A = array (
						'B' => 'All',
						'Y' => 'Active',
						'N' => 'Not Active' 
				);
			
				foreach ( $A as $key => $value ) {
					echo '<option value = "' . $key . '"';
					if ($Active == $key) {
						echo ' selected';
					}
					echo '>' . $value . '</option>';
				}
				?>
		</select>
	</td>
	<td>
		<div id="requests"></div>
	</td>
</tr>

<tr>
	<td align="right">Status:</td>
	<td>
		<select name="ProcessOrder" id="ProcessOrder" class="form-control width-auto-inline">
			<option value="" <?php if($ProcessOrder == "") echo ' selected="selected"'?>>All</option>
			<?php
			//Set where condition
			$where           =   array("OrgID = :OrgID","Active = 'Y'");
			//Set parameters
			$params          =   array(":OrgID"=>$OrgID);
			$org_status_res  =   $ApplicantsObj->getApplicantProcessFlowInfo("*", $where, 'ProcessOrder', array($params));
			$orgstatuslist   =   $org_status_res['results'];
			
			foreach ( $orgstatuslist as $APF ) {
                $APFPO      =   $APF ['ProcessOrder'];
                $APFDESC    =   $APF ['Description'];

				if ($ProcessOrder === $APFPO) {
					$selected = ' selected';
				} else {
					$selected = '';
				}
				echo '<option value="' . $APFPO . '"' . $selected . '>' . $APFDESC;
			}
			?>
		</select>
	</td>
</tr>

<?php
	//Get Applicant Dispositition Codes	
	$results_disp_codes = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'Y');
	
	if ($results_disp_codes['count'] > 0) {
		?>
		<tr>
			<td align="right">Disposition:</td>
			<td>
				<select name="Code" id="Code" class="form-control width-auto-inline">
					<option value="">All</option>
					<?php
					if(is_array($results_disp_codes['results'])) {
						foreach($results_disp_codes['results'] as $ADC) {
							$Code        =   $ADC ['Code'];
							$Description =   $ADC ['Description'];
								
							if (isset($CD) && $Code == $CD) {
								$selected = ' selected';
							} else {
								$selected = '';
							}
							echo '<option value="' . $Code . '"' . $selected . '>' . $Description;
						}
					}
					?>
				</select>
			</td>
		</tr>
		<?php 
	}	

	if (count ( $WSList ) > 0) {
		?>
		<tr>
			<td align="right">Weighted Search:</td>
			<td>
				<select name="WSList" id="WSList" class="form-control width-auto-inline">
					<option value="">Select</option>
					<?php
					for($w = 0; $w < count ( $WSList ); $w ++) {
						?><option value="<?php echo $WSList[$w]['WeightedSearchRandId']?>"><?php echo $WSList[$w]['SaveSearchName']?></option><?php
					}
					?>
				</select>
			</td>
		</tr>
		<?php
	}
	?>
<tr>
	<td align="right" height="60" valine="middle">Name This Search:</td>
	<td>
		<input type="hidden" name="action" value="search"> 
		<input type="hidden" name="process" value="Y"> 
		<input type="text" name="SearchName" size="15" maxlength="30" class="form-control width-auto-inline"> &nbsp;&nbsp;&nbsp;
		<button id="show" onclick="ShowProgressAnimation();submit();" class="btn btn-primary">Save and Search</button> &nbsp;&nbsp;&nbsp;
		<button id="show" onclick="ShowProgressAnimation();submit();" class="btn btn-primary">Search Applicants</button>
	</td>
</tr>

</form>
</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
	loadRequisitions('<?php echo IRECRUIT_HOME?>','<?php echo $AccessCode?>');
});	
</script>

<div id="loading-div-background">
	<div id="loading-div">
		<img style="height: 30px; margin: 30px 0px 10px 0px;" src="images/wait.gif" alt="Loading.." /><br>
	</div>
</div>
