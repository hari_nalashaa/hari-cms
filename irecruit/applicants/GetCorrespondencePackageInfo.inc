<?php
$selected_pkg_info = array();
$pkg_info = array();

//Set where users email
$where_users_email  =   array("OrgID = :OrgID", "UserID = :UserID");
//Set parameters
$params_users_email =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
//Get UsersInformation
$results_email      =   G::Obj('IrecruitUsers')->getUserInformation("EmailAddress", $where_users_email, "", array($params_users_email));

$selected_pkg_info['user_email']       =   $results_email['results'][0]['EmailAddress'];

if (isset($_REQUEST['package']) && $_REQUEST['package']) {
	$selected_pkg_info['request'] = $_REQUEST;
	// select data if editing or copy //Get Correspondence Packages
	$params_corresp    =   array (":OrgID"=>$OrgID, ":UpdateID"=>$_REQUEST['package']);
	//Set Correspondence Packages
	$where_corresp     =   array ("OrgID = :OrgID", "UpdateID = :UpdateID");
	//Set Correspondence columns
	$columns_corres    =   "*";
	$results           =   G::Obj('Correspondence')->getCorrespondencePackages ( $columns_corres, $where_corresp, '', array ($params_corresp) );
    
	
	$selected_pkg_info['pkg_info']         =   $pkg_info           = $results['results'][0];
	$selected_pkg_info['ProcessOrder']     =   $ProcessOrder       = $pkg_info ['ProcessOrder'];
	$selected_pkg_info['DispositionCode']  =   $DispositionCode    = $pkg_info ['DispositionCode'];
	
	
	
	// select data if editing or copy //Get Correspondence Packages
	$params_corresp    =   array (":OrgID"=>$OrgID, ":UpdateID"=>$pkg_info['Letters']);
	//Set Correspondence Packages
	$where_corresp     =   array ("OrgID = :OrgID", "UpdateID = :UpdateID");
	//Get Correspondence Letters
	$results           =   G::Obj('Correspondence')->getCorrespondenceLetters("UpdateID, Classification, Subject, Body", $where_corresp, '', array($params_corresp));

	if(is_array($results['results'])) {
		foreach ($results['results'] as $CL) {
			$selected_pkg_info['updateid']       = $updateid         = $CL ['UpdateID'];
			$selected_pkg_info['classification'] = $classification   = $CL ['Classification'];
			$selected_pkg_info['subject']        = $subject          = $CL ['Subject'];
			$selected_pkg_info['body']           = $body             = $CL ['Body'];
		}
	}
} // end if package

########################
//Set Condition
$where		=	array("OrgID = :OrgID");
//Bind the parameters
$params		=	array(':OrgID'=>$OrgID);

$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments(array('UpdateID', 'Filename'), $where, 'Filename', array($params));

$attachments_list = array();
if(is_array($results['results'])) {
	foreach ($results['results'] as $CL) {

		$updateid = $CL ['UpdateID'];
		$filename = $CL ['Filename'];

		$attachments_list[$updateid] = $filename;
	}
}

$attachments_selected = array();
if(is_array($results['results'])) {
	foreach ($results['results'] as $CL) {

		$updateid = $CL ['UpdateID'];
		$filename = $CL ['Filename'];

		if ($CorrespondenceObj->checkIfSelected ( $pkg_info ['Attachments'], $updateid )) {
		    $attachments_selected[$updateid] = $filename;
		}
	}
}

$selected_pkg_info['attachments_list']      =   $attachments_list;
$selected_pkg_info['attachments_selected']  =   $attachments_selected;
?>