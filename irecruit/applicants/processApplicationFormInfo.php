<?php
require_once '../Configuration.inc';

if(isset($_REQUEST['OrgID'])) $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['RequestID'])) $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['ApplicationID'])) $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['HoldID'])) $HoldID = $_REQUEST['HoldID'];

$SectionID  =   (isset($_REQUEST['SectionID']) && $_REQUEST['SectionID'] != "") ? $_REQUEST['SectionID'] : '1';

if ($HoldID) {
    $APPDATAREQ    =   array ();
    
    $results       =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);
    
    if(is_array($results['results'])) {
        foreach($results['results'] as $row) {
            $APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
        } // end foreach
    }
} // end HoldID

include IRECRUIT_DIR . 'applicants/ProcessApplicationFormInfo.inc';

echo $message;
?>