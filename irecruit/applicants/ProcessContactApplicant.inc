<?php
if ($_REQUEST['process'] == "Y" && isset($_REQUEST['process_contact']) && $_REQUEST['process_contact'] == "Y") {
    
    if (isset($_REQUEST['subject']) && $_REQUEST['subject'] != "" && isset($_REQUEST['body']) && $_REQUEST['body'] != "") {
        
        include IRECRUIT_DIR . 'applicants/ContactProcess.inc';
        
        echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';
        echo '<tr><td>';
        echo 'Correspondence sent to:';
        echo '<br><ul>';
        
        if (($multi == "Y") && ($multipleapps)) {
            
            $ma = explode('|', $multipleapps);
            $macnt = count($ma) - 1;
            
            for ($i = 0; $i < $macnt; $i ++) {
                
                $jarj = explode(':', $ma[$i]);
                
                $ApplicationID  =   $jarj[0];
                $RequestID      =   $jarj[1];
                
                if (($ApplicationID) && ($RequestID)) {
                    deliverEmail($OrgID, $ApplicationID, $RequestID, $_REQUEST['subject'], $_REQUEST['body'], $_REQUEST['attachments'], $USERID);
                    updateStatus($OrgID, $ApplicationID, $RequestID, $_REQUEST['ProcessOrder'], $_REQUEST['DispositionCode'], $USERID);
                }
            }
        } else {
            
            if (($ApplicationID) && ($RequestID)) {
                deliverEmail($OrgID, $ApplicationID, $RequestID, $_REQUEST['subject'], $_REQUEST['body'], $_REQUEST['attachments'], $USERID);
                updateStatus($OrgID, $ApplicationID, $RequestID, $_REQUEST['ProcessOrder'], $_REQUEST['DispositionCode'], $USERID);
            }
        }
        
        echo '</ul>';
        echo '</td>';
        echo '</tr>';
        echo '</table>';
    } else { // end subject and body
        
        $ERROR = 'Please indicate a Subject and Body message';
        
        echo $ERROR;
    }
} // end process
?>