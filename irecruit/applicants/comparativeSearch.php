<?php
require_once '../Configuration.inc';

if(count($_POST) > 0) {
    //Insert ApplicationID's
    foreach($_POST['appids_list'] as $ApplicationRequestID) {
        $ApplicationRequestIDInfo   = explode(":", $ApplicationRequestID); 
        $CompareApplicationID       = $ApplicationRequestIDInfo[0];
        $CompareRequestID           = $ApplicationRequestIDInfo[1];
        $comparative_label_applicants   =   $ComparativeAnalysisObj->insComparativeLabelApplicants($OrgID, $_REQUEST['LabelID'], $USERID, $CompareApplicationID, $CompareRequestID);
        
        if($comparative_label_applicants == "Maximum Limit Reached") {
            header("Location:comparativeSearch.php?msg=maxlimitreached&menu=10");
            exit();
        }
    }
    
    header("Location:comparativeSearch.php?msg=succ&menu=10");
    exit();
}

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto = $_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title = "Comparative Search";

// Scripts header
$scripts_header [] = "js/comparative-search.js";

//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

// Assign add applicant datepicker variables
$script_vars_footer [] = 'var datepicker_ids = "#application_from_date, #application_to_date";';
$script_vars_footer [] = 'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer = $script_vars_footer;


//Get Applicants based on RequestID
if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$RequestID = $RequisitionDetailsObj->getHiringManagerRequestID();
	$where_info         =   array("OrgID = :OrgID", "Approved = :Approved", "Active = :Active", "RequestID IN ('" . implode("','",$RequestID) . "')");
	$params_info        =   array(":OrgID"=>$OrgID, ":Approved"=>'Y', ":Active"=>"Y");
}else{
	$where_info         =   array("OrgID = :OrgID", "Approved = :Approved", "Active = :Active");
	$params_info        =   array(":OrgID"=>$OrgID, ":Approved"=>'Y', ":Active"=>"Y");
}

// Get Requisition Information
$TemplateObj->req_results =  $req_results = $RequisitionsObj->getRequisitionInformation ( "Title, RequestID, RequisitionID, JobID", $where_info, "", "Title, RequisitionID, JobID DESC", array (
    $params_info
) );

//Get Applicants based on RequestID
if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
	$RequestID = $RequisitionDetailsObj->getHiringManagerRequestID();
	$where_info         =   array("OrgID = :OrgID", "EntryDate <= :ToDate", "EntryDate >= :FromDate", "RequestID IN ('" . implode("','",$RequestID) . "')");
	$params_info        =   array(":OrgID"=>$OrgID);
}else{
	$where_info         =   array("OrgID = :OrgID", "EntryDate <= :ToDate", "EntryDate >= :FromDate");
	$params_info        =   array(":OrgID"=>$OrgID);
}

if(isset($_REQUEST['application_from_date'])) {
    $params_info[":FromDate"]   = $DateHelperObj->getYmdFromMdy($_REQUEST['application_from_date'], "/", "-");
}
else {
    $params_info[":FromDate"]   = date('Y-m-d', strtotime("-3 months", strtotime(date('Y-m-d'))));
}

if(isset($_REQUEST['application_to_date'])) {
    $params_info[":ToDate"]     = $DateHelperObj->getYmdFromMdy($_REQUEST['application_to_date'], "/", "-");
}
else {
    $params_info[":ToDate"]     = date('Y-m-d');
}

$columns            =   "ApplicationID, RequestID, ApplicantSortName, ProcessOrder, EntryDate";
$app_results        =   $ApplicationsObj->getJobApplicationsInfo("*", $where_info, '', 'EntryDate DESC LIMIT 100', array($params_info));

//Comparative Label Results
$comp_lbl_results  =   $ComparativeAnalysisObj->getComparativeLabels($OrgID, $USERID);
$comp_lbls_list    =   $comp_lbl_results['results'];


//Applications list
$TemplateObj->applications_list  =   $applications_list     =   $app_results['results'];
$TemplateObj->requisitions_list  =   $requisitions_list     =   $req_results['results'];
$TemplateObj->comp_lbls_list     =   $comp_lbls_list;

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/ComparativeSearch' );
?>