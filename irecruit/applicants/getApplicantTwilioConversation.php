<?php
require_once '../Configuration.inc';

if(isset($_REQUEST['DivID']) && $_REQUEST['DivID'] != "") {
	$div_id	= $_REQUEST['DivID'];
}
else {
	$div_id	= "divConversationInfo";
}

//Get Twilio Account Information
$account_info 			=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

//Insert Default Twilio Messaging Templates
G::Obj('TwilioTextMessageTemplates')->insDefaultTwilioTextMessagingTemplates($OrgID);

//Get applicant permission
$cell_phone_permission	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'cellphonepermission');

//Get twilio numbers list
$account_phone_numbers	=	G::Obj('TwilioPhoneNumbersApi')->getAccountIncomingNumbers($OrgID);
$twilio_numbers_list	=	$account_phone_numbers['Response'];

//Get twilio number
$user_details			=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);

//Add plus one before number
if(substr($user_details['Cell'], 0, 2) != "+1") {
	$user_cell_number	=	"+1".$user_cell_number;
}

//Get applicant cellphone
$cell_phone_info		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'cellphone');
$cell_phone				=	json_decode($cell_phone_info, true);
$applicant_number		=	"+1".$cell_phone[0].$cell_phone[1].$cell_phone[2];

#################################################################
//Check Start weather the phone number is sms capable or not
#################################################################
/*
$applicant_number_sms	=	"false";
$user_cell_number_sms	=	"false";

$lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $user_cell_number);

//Get the Protected properties from SMS object, by extending the object through ReflectionClass
$reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
$property       		=   $reflection->getProperty("properties");
$property->setAccessible(true);
$properties				=   $property->getValue($lookup_phone_num_info['Response']);

if($properties['carrier']['type'] == 'mobile'
	|| $properties['carrier']['type'] == 'voip') {
	$user_cell_number_sms	=	"true";
}

$lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $applicant_number);

//Get the Protected properties from SMS object, by extending the object through ReflectionClass
$reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
$property       		=   $reflection->getProperty("properties");
$property->setAccessible(true);
$properties				=   $property->getValue($lookup_phone_num_info['Response']);

if($properties['carrier']['type'] == 'mobile'
	|| $properties['carrier']['type'] == 'voip') {
	$applicant_number_sms	=	"true";
}
*/
#################################################################
//Check end weather the phone number is sms capable or not
#################################################################

//Get user conversation resources
$usr_conv_resources		=	G::Obj('TwilioConversationInfo')->getUserConversationResources($OrgID, $USERID, $applicant_number, $user_cell_number);
$usr_conv_count			=	count($usr_conv_resources);

$resource_id			=	'';

//Get ResourceID based on applicant phone number
if($usr_conv_count > 0) {
	$resource_id			=	$usr_conv_resources[0]['ResourceID'];

	//Overwrite the twilio numbers based on conversation
	$usr_conv_mobile_nums	=	json_decode($usr_conv_resources[0]['MobileNumbers'], true);
	$twilio_number_a		=	$usr_conv_mobile_nums[0][0];
	$twilio_number_u		=	$usr_conv_mobile_nums[1][0];
}
else {
	//Get Available Numbers
	$available_numbers		=	G::Obj('TwilioConversationInfo')->getAvailableProxyNumberPair($OrgID, $twilio_numbers_list, $applicant_number, $user_cell_number);
	$twilio_number_a		=	$available_numbers['ApplicantProxy'];
	$twilio_number_u		=	$available_numbers['UserProxy'];
}

$new_conversation			=	false;

//Send text Message
if(isset($_POST['taSmsMessage']) && $_POST['taSmsMessage'] != "") {

	if($usr_conv_count > 0) {
		$resource_id		=	$usr_conv_resources[0]['ResourceID'];
	}
	else {
		//Create new conversation
		$new_resource		=	G::Obj('TwilioConversationApi')->createConversationResource($OrgID);
		$resource_res		=	$new_resource['Response'];
		$resource_id		=	$resource_res->sid;

		if($new_resource['Code'] == 'Success' && $resource_id != "") {
			$new_conversation	=	true;
		}

		$numbers_info[NID1]	=	$twilio_number_a;
		$numbers_info[NID2]	=	$applicant_number;
		
		//Get Applicant First Name, Last Name Information
		$app_info				=	array();
		$app_info["First"]		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'first');
		$app_info["Last"]		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'last');

		//Insert conversation information
		G::Obj('TwilioConversationInfo')->insConversationResourceInfo($OrgID, $USERID, $resource_id, $numbers_info);
		
		//Add applicant phone number to this conversation
		$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $app_info);
		
		if($participant_response['Code'] == "Failed") {
		
			$used_proxys			=	G::Obj('TwilioConversationInfo')->getProxyNumbersOfPersonalNumber($applicant_number);
			$applicant_proxy_nums	=	array_diff($twilio_numbers_list, $used_proxys);
			$applicant_proxy_nums	=	array_values($applicant_proxy_nums);
			$twilio_number_a		=	$applicant_proxy_nums[0];
		
			//Available Phone Numbers
			$phone_numbers_response		=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
			$available_phone_numbers	=	$phone_numbers_response['Response'];
		
			//Purchase number
			if(is_array($available_phone_numbers) && count($available_phone_numbers) > 0) {
		
				$msg_service_id			=	$account_info['MessageServiceID'];
				$phone_numbers_result	=	G::Obj('TwilioPhoneNumbersApi')->getPhoneNumbersCountByMsgServiceID($OrgID, $msg_service_id);
				$phone_numbers_count	=	$phone_numbers_result['PhoneNumbersCount'];
		
				if($phone_numbers_count >= 400) {
					$message_service_info	=	G::Obj('TwilioMessagingServiceApi')->createMessageService($OrgID, G::Obj('GenericLibrary')->getGuIDBySalt($OrgID));
				}
		
				$available_phone_number	=	$available_phone_numbers[0];
				$new_num_response		=	G::Obj('TwilioPhoneNumbersApi')->createIncomingPhoneNumber($OrgID, $available_phone_number);
				$number_response		=	$new_num_response['Response'];
		
				if($number_response->sid != "") {
					$phone_number_res_info	=	G::Obj('TwilioMessagingServiceApi')->createPhoneNumberResource($OrgID, $account_info['MessageServiceID'], $number_response->sid);
					$twilio_number_a		=	$available_phone_number;
				}
			}
		
			$numbers_info[NID1]		=	$twilio_number_a;
			$numbers_info[NID2]		=	$applicant_number;
		
			$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $app_info);
				
		}
		
	}

	//Send text
	if($new_conversation == true && $resource_id != "") {
		
		$numbers_info[NID1]		=	$twilio_number_u;
		$numbers_info[NID2]		=	$user_cell_number;
	
		$user_info				=	array("First"=>$user_details["FirstName"], "Last"=>$user_details["LastName"]);
		
		//Add user phone number to this conversation
		$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $user_info);
		
		if($participant_response['Code'] == "Failed") {
		
			$used_proxys			=	G::Obj('TwilioConversationInfo')->getProxyNumbersOfPersonalNumber($applicant_number);
			$applicant_proxy_nums	=	array_diff($twilio_numbers_list, $used_proxys);
			$applicant_proxy_nums	=	array_values($applicant_proxy_nums);
			$twilio_number_u		=	$applicant_proxy_nums[0];
		
			//Available Phone Numbers
			$phone_numbers_response		=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
			$available_phone_numbers	=	$phone_numbers_response['Response'];
				
			//Purchase number
			if(is_array($available_phone_numbers) && count($available_phone_numbers) > 0) {
		
				$msg_service_id			=	$account_info['MessageServiceID'];
				$phone_numbers_result	=	G::Obj('TwilioPhoneNumbersApi')->getPhoneNumbersCountByMsgServiceID($OrgID, $msg_service_id);
				$phone_numbers_count	=	$phone_numbers_result['PhoneNumbersCount'];
		
				if($phone_numbers_count >= 400) {
					$message_service_info	=	G::Obj('TwilioMessagingServiceApi')->createMessageService($OrgID, G::Obj('GenericLibrary')->getGuIDBySalt($OrgID));
				}
		
				$available_phone_number	=	$available_phone_numbers[0];
				$new_num_response		=	G::Obj('TwilioPhoneNumbersApi')->createIncomingPhoneNumber($OrgID, $available_phone_number);
				$number_response		=	$new_num_response['Response'];
		
				if($number_response->sid != "") {
					$phone_number_res_info	=	G::Obj('TwilioMessagingServiceApi')->createPhoneNumberResource($OrgID, $account_info['MessageServiceID'], $number_response->sid);
					$twilio_number_u		=	$available_phone_number;
				}
		
			}
		
			$numbers_info[NID1]		=	$twilio_number_u;
			$numbers_info[NID2]		=	$applicant_number;
		
			$participant_response	=	G::Obj('TwilioConversationApi')->addConversationSMSParticipant($OrgID, $resource_id, $numbers_info, $app_info);
		}
		
		if($participant_response['Code'] ==	'Success') {
			//Update Conversation Resource Info
			G::Obj('TwilioConversationInfo')->updConversationResourceInfo($OrgID, $USERID, $resource_id, $numbers_info);
		}
		
	}

	//Send conversation message
	if($resource_id != "") {
		$conversation_msg_res	=	G::Obj('TwilioConversationApi')->createConversationMessage($OrgID, $resource_id, $twilio_number_u, $_REQUEST['taSmsMessage']);
	}
}

if($user_cell_number == ""
	|| $applicant_number == "") {
	echo '<h3>';
	echo 'Sorry one of these numbers are not filled<br>
			1) Applicant Cell Phone Number<br>
			2) User Cell Phone Number';
	echo '</h3>';
}
else if(strlen($user_cell_number) != "12"
	||	strlen($applicant_number) != "12") {
	echo '<h3>';
	echo 'Sorry one of these numbers are not valid<br>
			1) Applicant Cell Phone Number <br>
			2) User Cell Phone Number';
	echo '</h3>';
}
else if(substr($user_cell_number, 0, 2) != "+1"
		||	substr($applicant_number, 0, 2) != "+1") {
	echo '<h3>';
	echo 'Sorry one of these numbers are not valid<br>
			1) Applicant Cell Phone Number <br>
			2) User Cell Phone Number<br>';
	echo 'Number should start with +1';
	echo '</h3>';
}
else if($twilio_number_a == ""
		|| $twilio_number_u == "") {
	echo '<h3>';
	echo 'Sorry there is an issue with proxy numbers. Please contact support team.<br>';
	echo '</h3>';
}
else {
	if($user_cell_number != ""
		&& $applicant_number != ""
		&& $twilio_number_a != ""
		&& $twilio_number_u != "") {
		if($cell_phone_permission == 'Yes') {
			?>
			<br><br>
			<form name="frmTwilioConversation" id="frmTwilioConversation" method="post">
				<table width="100%" border="0" cellpadding="3" cellspacing="1" class="table table-bordered">
					<!--  <tr>
						<td colspan="2" align="left">  -->
							<?php
							/*
							echo "Twilio 1: ". $twilio_number_a."<br>";
							echo "Applicant: ". $applicant_number . "<br>";
							echo "Twilio 2: ". $twilio_number_u."<br>";
							echo "Personal: ". $user_cell_number ."<br>";
							*/
							?>
						<!--  </td>
					</tr> -->
					<tr>
						<td colspan="2" align="left">
							<h4 style="margin:0px">Send text to applicant.</h4>
						</td>
					</tr>

					<?php
					if(isset($conversation_msg_res) && $conversation_msg_res['Code'] == 'Success') {
						echo "<tr>";
						echo "<td style='color:blue' colspan='2' align='left'>Message sent successfully.</td>";
						echo "</tr>";
					}
					else if(isset($conversation_msg_res) && $conversation_msg_res['Code'] == 'Failed') {
						echo "<tr>";
						echo "<td style='color:red' colspan='2' align='left'>Failed to send message.</td>";
						echo "</tr>";
					}
					?>
					
					<tr>
						<td align="left">
							<strong>Templates:</strong>
						</td>
						<td align="left">
							<?php
							$templates_list_info	=	G::Obj('TwilioTextMessageTemplates')->getTwilioTextMessagingTemplatesByOrgID($OrgID);
							$templates_list_count	=	$templates_list_info['count'];
							$templates_list			=	$templates_list_info['results'];
							?>
							<select name="TwilioMessageTemplates" id="TwilioMessageTemplates" onchange="getTwilioMessageTemplateInfoByGuID(this.value, '<?php echo htmlspecialchars($_REQUEST['ApplicationID']);?>', '<?php echo htmlspecialchars($_REQUEST['RequestID']);?>', 'True')">
							<option value="">Select</option>
							<?php
							for($t = 0; $t < $templates_list_count; $t++)
							{
								$passed_value		=	$templates_list[$t]['PassedValue'];
								$display_text		=	$templates_list[$t]['DisplayText'];
								$template_guid		=	$templates_list[$t]['TemplateGuID'];
								
								?><option value="<?php echo $template_guid;?>"><?php echo $passed_value;?></option><?php
							}
							?>
							</select>
						</td>
					</tr>
					
					<tr>
						<td width="10%" align="left"><strong>Message: </strong></td>
						<td align="left">
							<textarea name="taSmsMessage" id="taSmsMessage" rows="8" cols="60" maxlength="600" onkeypress="setCharsLeft()"></textarea>
							<span id="spnSmsMessageCharsLeft">600 chars left</span>
							<span id="spnSmsMessageLoading"></span>
						</td>
					</tr>		
					<tr>
						<td colspan="2" align="left">
<?php

// Get Active Conversation Count				
$where                  =   array("OrgID = :OrgID");
$params			=   array(":OrgID"=>$OrgID);
$conversations_results  =   G::Obj('TwilioConversationInfo')->getAllConversations("count(*) as cnt", $where, "OrgID", "OrgID", array($params));
$conversation_cnt     =   $conversations_results['results'][0]['cnt'];

if ($account_info['Licenses'] > 0) { $account_info['Credits'] = 999; }

if ($conversation_cnt >= $account_info['Credits']) { // credits exceeded
echo "<div style=\"margin:20px;color:red;\">The licensing credit limit has been reached to start additional conversations. Please contact your iRecruit administrator to evaluate if more credits are needed. Credits become available each day as active conversations expire.</div>";
} else { // else credits exceeded
?>
<input type="button" name="btnSendSMS" id="btnSendSMS" class="btn btn-primary" onclick="sendTwilioConversationMessage(this, '<?php echo htmlspecialchars($div_id);?>');" value="Send Text">
<?php
}	// end if credits exceeded
?>
						</td>
					</tr>
				</table>
				<input type="hidden" name="ApplicationID" id="ApplicationID" value="<?php echo htmlspecialchars($_REQUEST['ApplicationID']);?>">
				<input type="hidden" name="RequestID" id="RequestID" value="<?php echo htmlspecialchars($_REQUEST['RequestID']);?>">
			</form>
			<br><br>	
			<?php
			if($resource_id != "") {
				$conversations		=	G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $resource_id);
				$conversation_res	=	$conversations['Response'];
				
				$conversation_res_cnt   =   0;
				if(is_array($conversation_res)) {
				    $conversation_res_cnt = count($conversation_res);
				}
				
				$messages			=	array();
				
				for($c = 0; $c < $conversation_res_cnt; $c++) {
				
					$message_obj	=	$conversation_res[$c];
				
					if(is_object($message_obj)) {
					    //Get the Protected properties from SMS object, by extending the object through ReflectionClass
					    $reflection     =   new ReflectionClass($message_obj);
					    $property       =   $reflection->getProperty("properties");
					    $property->setAccessible(true);
					    $messages[]		=   $property->getValue($message_obj);
					}
				}
				?>
				<table width="100%" border="0" cellpadding="3" cellspacing="1" class="table table-bordered">
				<tr>
					<td colspan="3" align="left">
						<h4 style="margin:0px">Messages List:</h4>
					</td>
				</tr>
				<tr>
					<td width="20%" align="left">
						<strong>Author</strong>
					</td>
					<td width="15%" align="left">
						<strong>Date</strong>
					</td>
					<td align="left">
						<strong>Message</strong>
					</td>
				</tr>
				<?php
				$messages_count	= count($messages) - 1;
				for($j = $messages_count; $j >= 0; $j--) {
					$PortalUserID      		=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'PortalUserID');
					$first					=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'first');
					$last      				=   G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'last');
					$email					=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], 'email');

					$userportal_userid_info	=	G::Obj('UserPortalUsers')->getUserDetailInfoByEmail("UserID", $email);

					if($PortalUserID == "") {
						$PortalUserID			=	$userportal_userid_info['UserID'];
					}

					$userportal_user_info	=	G::Obj('UserPortalUsers')->getUserDetailInfoByUserID("FirstName, LastName, ProfileAvatarPicture", $PortalUserID);
						
					echo "<tr>";
					
					echo "<td align=\"left\">";
					
					if($messages[$j]['author'] == $applicant_number) {
						$abs_path			=	USERPORTAL_DIR . "vault/".$PortalUserID."/profile_avatars/";
						if (file_exists ( $abs_path . $userportal_user_info['ProfileAvatarPicture'] )) {
							?>
							<img src="<?php echo USERPORTAL_HOME . "vault/".$PortalUserID."/profile_avatars/".$userportal_user_info['ProfileAvatarPicture'];?>" width="29" height="29"><br>
							<?php
						} else {
							?>
							<img src="<?php echo USERPORTAL_HOME . "images/no-intern.jpg";?>" width="50" height="50"><br>
							<?php
						}
						
						if($PortalUserID != "") {
							if($userportal_user_info['LastName'] != "") {
								echo $userportal_user_info['LastName'].", ";
							}
							echo $userportal_user_info['FirstName'];
						}
						else {
							if($last != "") {
								echo $last.", ";
							}
							echo $first;
						}
					}
					else if($messages[$j]['author'] != $user_cell_number) {
						if($user_preferences['DashboardAvatar'] != "") {
						    ?><img src="<?php echo IRECRUIT_HOME . "vault/".$OrgID."/".$USERID."/profile_avatars/".$user_preferences['DashboardAvatar'];?>" width="50" height="50"><br><?php
						}
						else {
						    ?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50"><br><?php
						}
						
						echo $user_info['FirstName']." ".$user_info['LastName'];
					}
					else {
						?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50"><br><?php						
						echo "Applicant ";
					}

					echo "</td>";
					echo "<td align=\"left\">";

					//Convert object to array, otherwise it is not working
					$twilio_created_date_info = (array) $messages[$j]['dateCreated'];

					$message_date_info	=	$twilio_created_date_info['date']; 	
						
					$date_info			=	explode(".", $message_date_info);
					$date_time_info		=	explode(" ", $date_info[0]);
				
					$twilio_date_info	=	new DateTime($date_info[0], new DateTimeZone('UTC'));
					$twilio_date_info->setTimezone(new DateTimeZone('America/New_York'));
					echo $twilio_date_info->format('m/d/Y H:i') . ' EST';
					
					echo "</td>";
					echo "<td align=\"left\">";
					echo $messages[$j]['body'];
					
					$media_count	=	count($messages[$j]['media']);
					
					$request_info['account_sid']	=	$messages[$m]['accountSid'];
					$message_id    =   $messages[$j]['sid'];
					
					for($mc = 0; $mc < $media_count; $mc++) {
                        $media_info             =	$messages[$j]['media'][$mc];
                        $media_sid              =	$media_info['sid'];
						
                        /*
						$media_resource_json	=	G::Obj('TwilioConversationMediaApi')->getConversationMediaResource($OrgID, $media_sid);
						$media_resource_info	=	json_decode($media_resource_json, true);
						
						$media_image_url		=	$media_resource_info['links']['content_direct_temporary'];
						$unique_id              =   uniqid(true);
						
						$_SESSION[$unique_id]['media_image_url']  =   $media_image_url;
                        */
						//echo $media_image_url."<br><br>";
                        $media_image_url = IRECRUIT_HOME . "vault/".$OrgID."/twilio/".$resource_id."/".$message_id."/".$media_info['sid']."/".$media_info['filename'];
                        
                        echo '<img src="'.$media_image_url.'" width="150" height="150">';
						
						//echo "&nbsp;&nbsp;<a href='".IRECRUIT_HOME."applicants/openTwilioImageByURL.php?url_unique_id=$unique_id' target='_blank'>Open</a>";
                        echo "&nbsp;&nbsp;<a href='".$media_image_url."' target='_blank'>Open</a>";
					}

					echo "</td>";
					echo "</tr>";


				}
				?>
				</table>
				<?php
			}
		}
		else {
			echo '<h3>This candidate haven\'t set the cellphone permissions to send text</h3>';
		}
	}		
}
?>
