<?php
if($flag == false) {
	?>
	<tr>
		<th colspan="2" height="40"></th>
	</tr>
	<tr>
		<th colspan="2" align="left">Selected Questions</th>
	</tr>
	<?php
}

foreach ( $_SESSION['I'][$FormID]['QA'] as $Skey => $SInfo ) {
	
	$SectionIdValue = $Skey;
	$Q = $_SESSION['I'][$FormID]['QA'][$SectionIdValue];
	
	$SWI = 0;
	
	foreach ( $Q as $S ) {
		
		foreach ( $S as $SK => $SV ) {
			
				$splitq         =   explode ( ":", $SK );
				$QuestionID     =   $splitq [1];
				$AnswerValue    =   $splitq [3];
				$QAID           =   "QA" . $QuestionID;
				
				if($FormID == "WebForms") {
				    $where_info     =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID", "Active = 'Y'");
				    $params_info    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$SectionIdValue, ":QuestionID"=>$QuestionID);
				    $resqueinfo     =   $FormQuestionsObj->getQuestionsInformation("WebFormQuestions", "*", $where_info, "Question DESC", array($params_info));
                    $rowqueinfo     =   $resqueinfo['results'][0];
                    $rowqueinfo['SectionID']    =   $SectionIdValue;
				}
				else {
                    //Get Form Question Details
                    $columns        =   "Question, QuestionID, QuestionTypeID, value";
                    $rowqueinfo     =   G::Obj('FormQuestionsDetails')->getFormQuestionInfoBySecIDQuesID($columns, $OrgID, $FormID, $SectionIdValue, $QuestionID);
                    
                    $sec_info       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $FormID, $SectionID);
                    $rowqueinfo['Section']      =   $sec_info['SectionTitle'];
                    $rowqueinfo['SectionID']    =   $SectionIdValue;
				}
				
				if($rowqueinfo['QuestionTypeID'] == 100) {
					?>
					<tr>
						<td align="left"><i><u>Question Weight</u></i>: <?php echo $rowqueinfo['Question']?></td>
						<td align="left">
							<input type="text" name="QA[<?php echo $SectionIdValue;?>][<?php echo $QuestionID?>][<?php echo $SK;?>]" value="<?php echo $SV;?>" size="2"> 
							<div onclick="remove_question('<?php echo $SectionIdValue;?>', '<?php echo $QuestionID?>')" class="delete_weighted_search_question"></div>
							<div onclick="show_answerc('<?php echo $QAID?>', 'wsec<?php echo $QuestionID?>')" id="wsec<?php echo $QuestionID?>" class="weighted_search_expand"></div>&nbsp;
						</td>
					</tr>
					<tr>
						<td align="left" colspan="2"><i><u>Answers Weight</u></i>: </td>
					</tr>
					<?php
					$answer_weights_info = array();
					if (array_key_exists('ANSWERWEIGHT', $_SESSION['I'][$FormID] ['QA'][$SectionIdValue][$QuestionID])) {
						$answer_weights_info = unserialize($_SESSION['I'][$FormID] ['QA'][$SectionIdValue][$QuestionID]['ANSWERWEIGHT']);
					}
					
					$rate_your_skills = unserialize($rowqueinfo ['value']);
					
					/**
					 * @tutorial Get weights for each combinations.
					 * Rval(Rating Values) treated as number of columns
					 * LabelValRow(Label Values) treated as number of rows
					*/
					$tr_count = count($rate_your_skills['LabelValRow']);
					$td_count = count($rate_your_skills['RVal']);
					
					?>
					<tr>
						<td colspan="2" class="<?php echo $QAID;?>" style="display: none">
							<div>
							<table>
								<tr>
									<td>&nbsp;</td>
									<?php
									foreach ($rate_your_skills['RVal'] as $rkey=>$rvalue)
									{
										?><td align="center"><?php echo $rvalue;?></td><?php
									}
									?>
								</tr>
								<?php 
									foreach ($rate_your_skills['LabelValRow'] as $label_key=>$label_value)
									{
										
											?>
											<tr>
												<td><?php echo $label_value;?></td>
												<?php 
												foreach ($rate_your_skills['RVal'] as $rkey=>$rvalue)
												{	
													if(count($answer_weights_info) > 0)
													{
														$answer_weight = $answer_weights_info[$label_key][$rkey];
													}
													else {
														$answer_weight = $_SESSION['I'][$FormID] ['QA'][$SectionIdValue][$QuestionID][$label_key][$rkey];
													}		
													
													
													?>
													<td>
													<input type="text" 
															size="2" 
															name="QA[<?php echo $SectionIdValue;?>][<?php echo $QuestionID?>][<?php echo $label_key;?>][<?php echo $rkey;?>]" 
															maxlength="2" 
															value="<?php echo $answer_weight?>">
													</td>
													<?php
												}
												?>
											 </tr>
											 <?php
										
										
									}
								?>
							</table>
							</div>
						</td>
					</tr>
					<?php
				}
				else {
					if (strstr ( $SK, "QUS" )) {
						?>
						<tr>
							<td align="left"><i><u>Question Weight</u></i>: <?php echo $rowqueinfo['Question']?></td>
							<td align="left">
								<input type="text" name="QA[<?php echo $SectionIdValue;?>][<?php echo $QuestionID?>][<?php echo $SK;?>]" value="<?php echo $SV;?>" size="2"> 
								<div onclick="remove_question('<?php echo $SectionIdValue;?>', '<?php echo $QuestionID?>')" class="delete_weighted_search_question"></div>
								<div onclick="show_answerc('<?php echo $QAID?>', 'wsec<?php echo $QuestionID?>')" id="wsec<?php echo $QuestionID?>" class="weighted_search_expand"></div>&nbsp;
							</td>
						</tr>
						<tr>
							<td align="left" colspan="2"><i><u>Answers Weight</u></i>: </td>
						</tr>
						<tr>
							<td colspan="2" class="<?php echo $QAID;?>" style="display: none">
								<?php
									if($AnswerValue != "") {
										?>
										<div>
											<table width="100%">
												<tr>
													<td width="88%" align="left"><?php echo $AnswerValue;?></td>
													<td align="left">
														<input type="text" size="2" name="QA[<?php echo $SectionIdValue;?>][<?php echo $QuestionID?>][<?php echo $SK;?>]" value="<?php echo $SV;?>" maxlength="2">
													</td>
												</tr>
											</table>
										</div>
										<?php
									}
								?>
							</td>
						</tr>
						<?php
					}
					if (strstr ( $SK, "ANS" )) {
						?>
						<tr>
							<td colspan="2" class="<?php echo $QAID;?>" style="display: none">
								<div>
									<table width="100%">
										<tr>
											<td width="88%" align="left"><?php echo $AnswerValue;?></td>
											<td align="left">
												<input type="text" size="2" name="QA[<?php echo $SectionIdValue;?>][<?php echo $QuestionID?>][<?php echo $SK;?>]" value="<?php echo $SV;?>" maxlength="2">
											</td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
						<?php
					}
				}
				
			
		}
	}
}
?>