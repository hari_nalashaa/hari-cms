<?php
$ERROR  =   "";

//Get Request and Files Data
$SERIALIZE_REQUEST_DATA =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "REQUEST_DATA");
$SERIALIZE_FILES_DATA   =   G::Obj('ApplicantDataTemp')->getApplicantDataTempQueInfo($OrgID, $HoldID, "FILES_DATA");

//Set Request Data, Files Data
$REQUEST                =   unserialize($SERIALIZE_REQUEST_DATA);
$FILES                  =   unserialize($SERIALIZE_FILES_DATA);

if($REQUEST == ""
    || !isset($REQUEST)
    || empty($REQUEST)
    || $REQUEST == NULL) {
    $REQUEST = array();
}

if($FILES == ""
    || !isset($FILES)
    || empty($FILES)
    || $FILES == NULL) {
    $FILES = array();
}
        
$REQUEST                =   array_merge($REQUEST, $_REQUEST);
$FILES                  =   array_merge($FILES, $_FILES);

//Insert entire request data to database to reproduce validation.
// Bind Parameters
$params =   array('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'QuestionID'=>"REQUEST_DATA", 'Answer'=>serialize($REQUEST));
// Insert applicant data temp
G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params);

// Bind Parameters
$params =   array('OrgID'=>$OrgID, 'HoldID'=>$HoldID, 'QuestionID'=>"FILES_DATA", 'Answer'=>serialize($FILES));
// Insert applicant data temp
G::Obj('ApplicantDataTemp')->insApplicantDataTemp ($params);

//Validate the information
G::Obj('GetFormPostAnswer')->POST   =   $REQUEST;
G::Obj('GetFormPostAnswer')->FILES  =   $FILES;

//Get Modified POST Answers
$SEC1_POSTANSWERS   =   G::Obj('GetFormPostAnswer')->getPostDataAnswersOfFormQuestions($OrgID, $FormID, "1");
//Get Modified POST Answers
$SEC6_POSTANSWERS   =   G::Obj('GetFormPostAnswer')->getPostDataAnswersOfFormQuestions($OrgID, $FormID, "6");

$POSTANSWERS        =   array_merge($SEC1_POSTANSWERS, $SEC6_POSTANSWERS);

//Validate the information
G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']   =   $POSTANSWERS;
G::Obj('ValidateApplicationForm')->FORMDATA['FILES']     =   $FILES;
        
//Validate Required Fields By SectionID
$errors_info    =   G::Obj('ValidateApplicationForm')->validateApplicationForm($OrgID, $FormID, "1", $HoldID);
$errors_list    =   $errors_info['ERRORS'];

if(isset($errors_list) && is_array($errors_list)) {
    foreach ($errors_list as $error_que_id=>$error_que_msg) {
        $ERROR  .=  $error_que_msg;
    }
}

//Validate Required Fields By SectionID
$errors_info    =   G::Obj('ValidateApplicationForm')->validateApplicationForm($OrgID, $FormID, "6", $HoldID);
$errors_list    =   $errors_info['ERRORS'];

if(isset($errors_list) && is_array($errors_list)) {
    foreach ($errors_list as $error_que_id=>$error_que_msg) {
        $ERROR  .=  $error_que_msg;
    }
}
?>