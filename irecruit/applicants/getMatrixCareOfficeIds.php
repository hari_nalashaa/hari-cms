<?php 
require_once '../Configuration.inc';

$matrix_care_ques   =   ["matrixcare_OfficeID"];

$office_ids_info    =   $MatrixCareObj->getMatrixCareOfficeIds($OrgID, $_REQUEST['CompanyID']);
$branches_info      =   $office_ids_info['BranchesInfo'];
$branches_info      =   json_decode($branches_info);

$matrix_care_info   =   $MatrixCareObj->getMatrixCareLoginSessionID($OrgID, $_REQUEST['CompanyID']);

$APPDATA            =   $ApplicantsObj->getAppData ( $OrgID, $_REQUEST['ApplicationID'] );


$matrix_care_ans    =   array();

foreach($APPDATA as $APPDATA_QUE_ID => $APPDATA_ANS) {
    if(in_array($APPDATA_QUE_ID, $matrix_care_ques)) {
        $matrix_care_ans[$APPDATA_QUE_ID]   =   $APPDATA_ANS;
    }
}

if($branches_info != "") {
    echo json_encode(array($branches_info, $matrix_care_info['access_token'], $matrix_care_ans));
}
else {
	echo json_encode(array());
}
?>