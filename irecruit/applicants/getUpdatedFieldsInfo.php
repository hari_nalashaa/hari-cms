<?php 
require_once '../Configuration.inc';

// Get JobApplication History
$where          =   array ("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UpdateID = :UpdateID");
$params         =   array (":OrgID" => $OrgID, ":ApplicationID" => $_REQUEST['ApplicationID'], ":RequestID" => $_REQUEST['RequestID'], ":UpdateID"=>$_REQUEST['item']);
$results        =   G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo ( "*", $where, '', 'Date desc', array($params) );
$UpdatedFields  =   $results['results'][0]['UpdatedFields'];
$UpdatedFields  =   json_decode($UpdatedFields, true);

if ($UpdatedFields['Section'] != "") {

	echo "<div style=\"margin-bottom:20px;font-size:14pt;\">";
	echo "<strong>" . $UpdatedFields['Section'] . " Section Changes</strong><br>";
	echo "</div>";

	foreach ($UpdatedFields['Updated Fields'] AS $U) {

		echo "<div style=\"margin-left:10px;\">";
		echo "<b>Question:</b> " . $U['Question'] . "<br>"; 
		echo "<div style=\"margin:10px 0 20px 10px;\">";

		echo "<b>From:</b>";

		echo "<div style=\"margin-left:10px;margin-bottom:5px;\">";
		echo G::Obj('FormatAnswers')->getFormattedAnswer($U['QuestionID'],$U['QuestionTypeID'],$U['values'],$U['APPDATA']);
		echo "</div>";

		echo "<b>To:</b>";

		echo "<div style=\"margin-left:10px;\">";
		echo G::Obj('FormatAnswers')->getFormattedAnswer($U['QuestionID'],$U['QuestionTypeID'],$U['values'],$U['REQUEST']);
		echo "</div>";

		echo "</div>";
		echo "</div>";

	} // end foreach

} else { // else Section != ""

$APPDATA        =   $ApplicantsObj->getAppData($OrgID, $ApplicationID);

$FormID         =   $ApplicantDetailsObj->getFormID($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
$JsonQuesInfo   =   $ApplicationFormQuestionsObj->getFormQuestionsList($OrgID, $FormID);
$QuesInfo       =   $JsonQuesInfo['json_ques'];

$UpdatedFieldsInfo      =   $UpdatedFields['Updated Fields'];
$AdditionalFieldsInfo   =   $UpdatedFields['Additional Fields'];

$APPDATA_ORI            =   $APPDATA;

foreach($APPDATA_ORI as $UpdQueID=>$UpdAnswer) {
    if($UpdatedFieldsInfo[$UpdQueID]['Original'] != "") {
        $APPDATA_ORI[$UpdQueID] = $UpdatedFieldsInfo[$UpdQueID]['Original'];
    }
}

//Updated Fields Information
echo "<strong>Updated Fields:</strong> <br>";
foreach($UpdatedFieldsInfo as $UpdatedFieldsInfoQue=>$UpdatedFieldsInfoAns) {
    
    $sub_que_1   =   substr($UpdatedFieldsInfoQue, 0, -1);
    $sub_que_2   =   substr($UpdatedFieldsInfoQue, 0, -2);
    
    if($QuesInfo[$UpdatedFieldsInfoQue]['Question'] != "") {
        echo "Question: "   .   $QuesInfo[$UpdatedFieldsInfoQue]['Question'] . "<br>";
    }
    else if($QuesInfo[$sub_que_1]['Question'] != "") {
        echo "Question: "   .   $QuesInfo[$sub_que_1]['Question'] . "<br>";
    }
    else if($QuesInfo[$sub_que_2]['Question'] != "") {
        echo "Question: "   .   $QuesInfo[$sub_que_2]['Question'] . "<br>";
    }

    echo "Original: "   .   $UpdatedFieldsInfoAns["Original"]."<br>";
    echo "Updated: "    .   $UpdatedFieldsInfoAns["Updated"]."<br><br>";
}

} // end else Section

/*
if(count($AdditionalFieldsInfo) > 0) {
echo "<strong>Additional Fields: </strong><br>";
}
//Additional Fields Information
foreach($AdditionalFieldsInfo as $AdditionalFieldsInfoQue=>$AdditionalFieldsInfoAns) {
    
}
*/

// **** FUNCTIONS **** //
function printSection($OrgID, $FormID, $APPDATA, $SectionID) {

    global $permit, $feature, $FormQuestionsObj, $AddressObj;

    //Set columns
    $columns    =   array("Question", "QuestionID", "QuestionTypeID", "value", "SectionID");
    //Set the parameters
    $params =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
    //Set condition
    $where  =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
    //Get Questions information
    $rslt   =   $FormQuestionsObj->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));

    $rtn    =   '';

    $rowcnt =   $rslt['count'];

    foreach ($rslt['results'] as $FQ) {

        // format answers
        if ($FQ ['QuestionID'] == 'sin') {
            $sin_info   =   json_decode($APPDATA['sin'], true);
            if ($APPDATA['sin'] != "") {
                if ($_GET ['emailonly'] == 'Y') {
                    $APPDATA [$FQ ['QuestionID']] = 'XXX-XX-XXXX';
                } else {
                    $APPDATA [$FQ ['QuestionID']] = $sin_info[0] . '-' . $sin_info[1] . '-' . $sin_info[2];
                }
            } // end if not blank
        } // end sin

        if ($FQ ['QuestionID'] == 'social') {
            $social_info = json_decode($APPDATA['social'], true);
            if ($APPDATA ['social'] != "") {
                if ($_GET ['emailonly'] == 'Y') {
                    $APPDATA [$FQ ['QuestionID']] = 'XXX-XX-XXXX';
                } else {
                    $APPDATA [$FQ ['QuestionID']] = $social_info[0] . '-' . $social_info[1] . '-' . $social_info[2];
                }
            } // end if not blank
        } // end social

        if ($FQ ['QuestionID'] == 'cellphone') {
            $cellphone_info = json_decode($APPDATA['cellphone'], true);
            $APPDATA [$FQ ['QuestionID']] = $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $cellphone_info[0], $cellphone_info[1], $cellphone_info[2], '' );
        } // end cellphone

        if ($FQ ['QuestionID'] == 'homephone') {
            $homephone_info = json_decode($APPDATA['homephone'], true);
            $APPDATA [$FQ ['QuestionID']] = $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $homephone_info[0], $homephone_info[1], $homephone_info[2], '' );
        } // end homephone

        if ($FQ ['QuestionID'] == 'busphone') {
            $busphone_info = json_decode($APPDATA['busphone'], true);
            $APPDATA [$FQ ['QuestionID']] = $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $busphone_info[0], $busphone_info[1], $busphone_info[2], $busphone_info[3] );
        } // end busphone

        if ($FQ ['value'] && $FQ ['QuestionTypeID'] != 100 && $FQ ['QuestionTypeID'] != 120) {
            $APPDATA [$FQ ['QuestionID']] = getDisplayValue ( $APPDATA [$FQ ['QuestionID']], $FQ ['value'] );
        }

        if ($FQ ['QuestionID'] == 'cur_phone') {
            $cur_phone_info = json_decode($APPDATA['cur_phone'], true);
            $APPDATA [$FQ ['QuestionID']] = $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $cur_phone_info[0], $cur_phone_info[1], $cur_phone_info[2], '' );
        } // end cur_phone

        if (($FQ ['QuestionTypeID'] == 18) || ($FQ ['QuestionTypeID'] == 1818) || ($FQ ['QuestionTypeID'] == 9)) {
            $APPDATA [$FQ ['QuestionID']] = getMultiAnswer ( $APPDATA, $FQ ['QuestionID'], $FQ ['value'], $FQ ['QuestionTypeID'] );
        }

        if (($FQ ['QuestionTypeID'] == 1) || ($FQ ['QuestionTypeID'] == 24)) {
            $APPDATA [$FQ ['QuestionID']] = "from " . $APPDATA [$FQ ['QuestionID'] . from] . " to " . $APPDATA [$FQ ['QuestionID'] . to];
        }

        $print = "Y";

        if ($feature ['LimitApplicationToAnswersOnly'] == "Y") {
            	
            if (($APPDATA [$FQ ['QuestionID']] == "") || ($APPDATA [$FQ ['QuestionID']] == "from  to ")) {
                $print = "N";
            }
            if (($FQ ['QuestionTypeID'] == 99) || ($FQ ['QuestionTypeID'] == 98)) {
                $print = "N";
            }
        } // end feature

        if ($print == "Y") {
            	
            if ($FQ ['QuestionTypeID'] == 100) {

                if($APPDATA [$FQ ['QuestionID']] != "") {

                    $canswer = @unserialize($APPDATA [$FQ ['QuestionID']]);
                    	
                    if(is_array($canswer)) {
                        $rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
                        $rtn .= '<tr>';
                        $rtn .= '<td width="20%">'.$FQ ['Question'].'</td>';
                        $rtn .= '</tr>';

                        foreach ($canswer as $cakey=>$caval) {
                            $rtn .= '<tr><td>'.$cakey.'</td>';
                            	
                            foreach ($caval as $cavk=>$cavv) {
                                $rtn .= '<td>'.$cavv.'</td>';
                            }
                            	
                            $rtn .= '</tr>';
                        }
                        	
                        $rtn .= '</table>';
                    }
                    	
                }

            }
            else if($FQ ['QuestionTypeID'] == 120) {
                if($APPDATA [$FQ ['QuestionID']] != "") {
                    $canswer = unserialize($APPDATA [$FQ ['QuestionID']]);
                    	
                    if(is_array($canswer)) {
                        $rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
                        $rtn .= '<tr>';
                        $rtn .= '<td colspan="5">'.$FQ ['Question'].'</td>';
                        $rtn .= '</tr>';

                        $days_count = count($canswer['days']);


                        for($ci = 0; $ci < $days_count; $ci++)  {
                            $rtn .= '<tr>';
                            $rtn .= '<td width="20%">'.$canswer['days'][$ci].'</td>';
                            $rtn .= '<td width="5%">from</td>';
                            $rtn .= '<td width="10%">'.$canswer['from_time'][$ci].'</td>';
                            $rtn .= '<td width="5%">to </td>';
                            $rtn .= '<td>'.$canswer['to_time'][$ci].'</td>';
                            $rtn .= '</tr>';
                        }

                        $rtn .= '</table>';
                    }
                }

            }
            else if (($FQ ['QuestionTypeID'] == 99) || ($FQ ['QuestionTypeID'] == 98)) {
                $rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
                $rtn .= '<tr><td colspan="100%">';
                $rtn .= $FQ ['Question'];
            } elseif (($FQ ['QuestionID'] != 'salarydesireddefinition') && ($FQ ['QuestionID'] != 'cur_salaryper')) {
                $rtn .= '<table border="0" cellspacing="3" cellpadding="0" class="table">' . "\n";
                $rtn .= '<tr><td width="20%" valign="top">';
                if($FQ ['QuestionID'] != 'middle') $rtn .= $FQ ['Question'];
                $rtn .= '</td><td valign="top">';
            } else {
                $rtn .= '&nbsp;&nbsp;' . $FQ ['Question'] . '&nbsp;&nbsp;';
            }
            	
            if (($FQ ['QuestionTypeID'] != 99) && ($FQ ['QuestionTypeID'] != 98) && ($FQ ['QuestionTypeID'] != 100) && ($FQ ['QuestionTypeID'] != 120)) {
                if($FQ ['QuestionID'] != 'middle') $rtn .= '<b>' . $APPDATA [$FQ ['QuestionID']] . '</b>';
            }
            	
            //if (($FQ ['QuestionID'] != 'salarydesired') && ($FQ ['QuestionID'] != 'cur_salary')) {
            $rtn .= '</td></tr>';
            $rtn .= '</table>';
            //}
            	
        } // end if print
    } // end while

    if ($rowcnt == 0) {
        $rtn = '';
    }

    return $rtn;
} // end of function

function getDisplayValue($ans, $displaychoices) {
    $rtn = '';

    if ($ans != '') {
        $Values = explode ( '::', $displaychoices );
        foreach ( $Values as $v => $q ) {
            list ( $vv, $qq ) = explode ( ":", $q, 2 );
            if ($vv == $ans) {
                $rtn .= $qq;
            }
        }
    }
    return $rtn;
} // end of function

function getMultiAnswer($APPDATA, $id, $displaychoices, $questiontype) {
    $rtn = '';
    $val = '';
    $lex = '';

    if (($id == 'daysavailable') || ($id == 'typeavailable')) {

        $le = ', ';
        $li = '';
        $lecnt = - 2;
    } else {

        $le = '<br>';
        $li = '&#8226;&nbsp;';
        $lecnt = - 4;
    }

    $cnt = $APPDATA [$id . 'cnt'];

    for($i = 1; $i <= $cnt; $i ++) {

        $val = getDisplayValue ( $APPDATA [$id . '-' . $i], $displaychoices );
        if ($val) {
            $rtn .= $li . $val;
            $lex = 'on';
            $val = '';
        }

        if ($questiontype == 9) {
            	
            $val = $APPDATA [$id . '-' . $i . '-yr'];
            if ($val) {
                $rtn .= ',&nbsp;' . $val . ' yrs. ';
                $lex = 'on';
                $val = '';
            }
            	
            $val = $APPDATA [$id . '-' . $i . '-comments'];
            if ($val) {
                $rtn .= '&nbsp;(' . $val . ') ';
                $lex = 'on';
                $val = '';
            }
        } // end questiontype

        if ($lex == 'on') {
            $rtn .= $le;
            $lex = '';
        }
    }

    $rtn = substr ( $rtn, 0, $lecnt );

    return $rtn;
} // end of function
