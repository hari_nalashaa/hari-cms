<?php
if (isset($_REQUEST['process']) 
	&& ($_REQUEST['process'] == "Y") 
	&& ($_REQUEST['requisitionlist']) 
	&& $_REQUEST['process_manage_requisition'] == "YES") {
	
	if (($multi == "Y") && ($multipleapps)) {
		
		$ma = explode ( '|', $multipleapps );
		$macnt = count ( $ma ) - 1;
		
		for($i = 0; $i < $macnt; $i ++) {
			
			$jarj = explode ( ':', $ma [$i] );
			
			$ApplicationID = $jarj [0];
			$RequestID = $jarj [1];
			
			if (($ApplicationID) && ($RequestID)) {
				G::Obj('Applications')->addApplication ( $OrgID, $ApplicationID, $RequestID, $_REQUEST['requisitionlist'], $USERID, $_REQUEST['Distinction'] );
			}
		} // end for
	} else { // end multi
		
		if (($_REQUEST['ApplicationID']) && ($_REQUEST['RequestID'])) {
		    G::Obj('Applications')->addApplication ( $OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID'], $_REQUEST['requisitionlist'], $USERID, $_REQUEST['Distinction'] );
		}
	} // end else multi
	
} // end process
?>