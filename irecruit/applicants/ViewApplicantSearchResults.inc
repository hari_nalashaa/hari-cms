<?php
$SEARCHRESULT .= '<tr>';

$SEARCHRESULT .= '<td valign="top">';

if ($feature ['MultiOrg'] == "Y") {
	$SEARCHRESULT .= $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID ) . "<br>";
} // end feature

$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
$FullViewReqJobID = $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $multiorgid_req, $RequestID );
$FullViewReqJobTitle = G::Obj('RequisitionDetails')->getJobTitle($OrgID, $multiorgid_req, $RequestID);
if($FullViewReqJobID == "") $FullViewReqJobID = '<strong style="color:red">Requisition has been deleted</strong>';
$SEARCHRESULT .= $FullViewReqJobID .  "<br>" . $FullViewReqJobTitle . '</td>';

if ($permit ['Applicants_Contact'] == 1) {
	$SEARCHRESULT .= '<td valign="top"><a href="mailto:' . $Email . '">' . $Applicant . '</a></td>';
} else {
	$SEARCHRESULT .= '<td valign="top">' . $Applicant . '</td>';
}

if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	$SEARCHRESULT .= '<td valign="top"><a href="applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '">' . $ApplicationID . '</a></td>';
}
if (preg_match ( '/applicantsSearch.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	
	if($OrgID == "I20110816") {
		$SEARCHRESULT .= '<td valign="top"><a href="applicantsSearch.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '" target="blank">' . $ApplicationID . '</a></td>';
	}
	else {
		$SEARCHRESULT .= '<td valign="top"><a href="javascript:void(0);" onclick=\'getApplicantDetailsFromSearch("'.$ApplicationID.'", "'.$RequestID.'", "'.$applicants_search_results[$aps_index]['WeightedSearchScore'].'", "'.$row['ApplicantSortName'].'", "'.$applicants_search_results[$aps_index]['ApplicantEmail'].'");\'>' . $ApplicationID . '</a></td>';
	}
}

$TemplateObj->ApplicationIDhold = $ApplicationIDhold = $ApplicationID;
$TemplateObj->RequestIDhold = $RequestIDhold = $RequestID;

$SEARCHRESULT .= '<td valign="top">';
$SEARCHRESULT .= $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $ProcessOrder );

if ($ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $DispositionCode )) {
	$SEARCHRESULT .= '<br>Disposition: ';
	$SEARCHRESULT .= $ApplicantDetailsObj->getDispositionCodeDescription ( $OrgID, $DispositionCode );
}

if ($EffDate) {
	$SEARCHRESULT .= '<br>Effective: ' . $EffDate;
}
if ($ReceivedDate != '0000-00-00') {
	$SEARCHRESULT .= '<br>Received: ' . $ReceivedDate;
}

if ($CorrespondenceCnt > 0) {
	$linkcor = "applicants/viewCorrespondence.php?ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID;
	if ($AccessCode != "") {
		$linkcor .= "&k=" . $AccessCode;
	}
	$onclickcor = "onclick=\"javascript:window.open('" . $linkcor . "','_blank','location=yes,toolbar=no,height=400,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";
	$SEARCHRESULT .= '<br>Correspondence: <a href="#" ' . $onclickcor . '>' . $CorrespondenceCnt . '</a>';
} // end CorrespondenceCnt

$def = "N";
if (isset($APPDATA ['source']) 
    && isset($SOURCE [$APPDATA ['source']]) 
    && $SOURCE [$APPDATA ['source']] != "") {
	$SEARCHRESULT .= '<br>Source: ' . $SOURCE [$APPDATA ['source']];
	$def = "Y";
}

if (isset($APPDATA ['source']) && ($APPDATA ['source'] != "") && ($def == "N")) {
	$SEARCHRESULT .= '<br>Source: ' . $APPDATA ['source'];
}

$SEARCHRESULT .= '</td>';

$itm = $ApplicationID . ":" . $RequestID;

if($twilio_user_info['TwilioSms'] == "Y") {
	//Get applicant permission
	$cell_phone_permission	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphonepermission');
	
	//Get twilio number
	$current_user_details		=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell");
	$current_user_cell_number	=	str_replace(array("-", "(", ")", " "), "", $current_user_details['Cell']);
	
	//Add plus one before number
	if(substr($current_user_cell_number, 0, 2) != "+1") {
		$current_user_cell_number	=	"+1".$current_user_cell_number;
	}
	
	//Get applicant cellphone
	$applicant_cell_phone_info	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, 'cellphone');
	$applicant_cell_phone		=	json_decode($applicant_cell_phone_info, true);
	$applicant_cell_number		=	"+1".$applicant_cell_phone[0].$applicant_cell_phone[1].$applicant_cell_phone[2];
	
	//Send sms option
	$SEARCHRESULT .= '<td>';
	if($cell_phone_permission == 'Yes') {

		if(strlen($current_user_cell_number) == "12"
			||	strlen($applicant_cell_number) == "12") {
			$SEARCHRESULT .= '<input type="checkbox" id="send_bulk_sms" name="send_bulk_sms[]" value="'.$itm.'">';
		}
		
	}
	$SEARCHRESULT .= '</td>';
}

$weighted_form_info = $WeightedSearchObj->getWeightSaveSearchRowInfo("FormID, SectionID", $_REQUEST['WSList']);

if(isset($_REQUEST['WSList']) && $_REQUEST['WSList'] != "" && $weighted_form_info["FormID"] == "WebForms") {
    $SEARCHRESULT .= '<td valign="top" align="left"><a href=\'javascript:void(0)\' onclick=\'javascript:window.open("'.IRECRUIT_HOME.'applicants/weightedSearchWebFormScoreBreakUp.php?WSList='.$_REQUEST['WSList'].'&AppId='.$ApplicationID.'&OrgID='.$OrgID.'","_blank","location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes")\'>'.$total.'</a></td>';
}
else if(isset($_REQUEST['WSList']) && $_REQUEST['WSList'] != "") {
    $SEARCHRESULT .= '<td valign="top" align="left"><a href=\'javascript:void(0)\' onclick=\'javascript:window.open("'.IRECRUIT_HOME.'applicants/weightedSearchScoreBreakUp.php?WSList='.$_REQUEST['WSList'].'&AppId='.$ApplicationID.'&OrgID='.$OrgID.'","_blank","location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes")\'>'.$total.'</a></td>';
}

if ($SEARCHiii > 0) {
	$SEARCHRESULT .= '<td align="left" valign="top"><input type="checkbox" id="appid" name="appid[]" value="' . $itm . '"></td>';
}

$SEARCHRESULT .= '</tr>';
?>