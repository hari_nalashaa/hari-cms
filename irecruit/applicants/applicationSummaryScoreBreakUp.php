<?php
require_once '../Configuration.inc';

$AppId = $_GET ['AppId'];
$RESULTS = array ();

global $USERID;

//Set columns
$columns = "WeightedSearchRandId, SaveSearchName";
//Set where condition
$where_info = array("UserID = :UserID");
//Set parameters
$params = array(":UserID"=>$USERID);
//Get Weighted Search Information
$ressavedsearch = $WeightedSearchObj->getWeightSaveSearchInfo($columns, $where_info, "SaveSearchName", "", array($params));

$numrowssearch = $ressavedsearch['count'];

echo '<link rel="stylesheet" type="text/css" href="'.IRECRUIT_HOME.'css/customstyles.css">';
echo '<script src="'.IRECRUIT_HOME.'js/jquery-1.11.1.min.js"></script>';
echo '<h3 style="background-color:#5A78A1;color:white;width:79%;padding:5px;text-align:center;margin:0 auto;">Score Breakups List</h3>';

if(is_array($ressavedsearch['results'])) {
	foreach($ressavedsearch['results'] as $rowsavedsearch) {
	
		$WList = $rowsavedsearch ['WeightedSearchRandId'];
		$SName = $rowsavedsearch ['SaveSearchName'];
	
		$WSFList = array ();
		$WEIGHT = array ();
		$WSFList = $WeightedSearchObj->WeightedSearchInfo ( $WList );
	
		// ######################################################
		// ##### Calculate Total Weight Of That Application
		// ######################################################
		$FormID = $WSFList [0] ['FormID'];
		
		for($we = 0; $we < count ( $WSFList ); $we ++) {
	
			if ($WSFList [$we] ['QuestionTypeID'] == "100") {
					
				$answer = 0;
				$input = $WSFList [$we] ['QuestionAnswerKey'];
				$rank = $WSFList [$we] ['QuestionAnswerWeight'];
					
				if ($rank != "") {
	
					$val = explode ( ':', $input );
	
					if ($val [2] == 'QUS') $WEIGHT [$AppId] [$val [0]] [$val [1]] ["QUS"] = $rank;

					//Set data parameters
					$app_data_params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$AppId, ":QuestionID"=>$WSFList [$we] ['QuestionID']);
					//Set app data where condition
					$app_data_where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "(QuestionID = :QuestionID");
					//Get Applicant Data Information
					$resultsQ = $ApplicantsObj->getApplicantDataInfo("*", $app_data_where, '', 'ApplicationID', array($app_data_params));
					
					$question_info = $resultsQ['results'][0];
					$answer_info = @unserialize ( $question_info ['Answer'] );
					$customquestion = @unserialize ( $WSFList [$we] ['CustomQuestion'] );
					$customquestionweights = @unserialize ( $WSFList [$we] ['CustomQuestionAnswerWeight'] );
	
					$weights_info = array ();
					if(is_array($customquestion ['LabelValRow'])) {
						foreach ( $customquestion ['LabelValRow'] as $label => $label_name ) {
							if(is_array($customquestion ['RVal'])) {
								foreach ( $customquestion ['RVal'] as $ratekey => $ratevalue ) {
									$weights_info [$label_name] [$ratevalue] = $customquestionweights [$label] [$ratekey];
								}
							}
						}	
					}
					
					if(is_array($answer_info)) {
						foreach ( $answer_info as $alabel => $alabelkey ) {
							$alabelkeyname = key ( $alabelkey );
							$alabelrating = $answer_info [$alabel] [$alabelkeyname];
							$applicant_answer_info [$alabel] = $alabelrating;
							$answer += ( int ) $weights_info [$alabel] [$alabelrating];
						}
					}
	
					$applicant_answer = "";
	
					if(is_array($applicant_answer_info)) {
						foreach ( $applicant_answer_info as $appkey => $appval ) {
							$get_weight = $weights_info [$appkey] [$appval];
							$applicant_answer .= $appkey . " - (A:" . $appval . " - W:" . $get_weight . "), ";
						}
					}
	
					$applicant_answer = trim ( $applicant_answer, "," );
					$WEIGHT [$AppId] [$val [0]] [$val [1]] ["ANSWER"] = $applicant_answer;
				}
				$WEIGHT [$AppId] [$val [0]] [$val [1]] ["ANS"] = $answer;
			} else {
				$answer = 0;
				$input = $WSFList [$we] ['QuestionAnswerKey'];
				$rank = $WSFList [$we] ['QuestionAnswerWeight'];
					
				if ($rank != "") {
					$val = explode ( ':', $input );
					
					if ($val [2] == 'QUS') $WEIGHT [$AppId] [$val [0]] [$val [1]] ["QUS"] = $rank;
	
					//, ":QuestionID1"=>$val [1], ":QuestionID2"=>$val [1]
					//Set data parameters
					$appdata_params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$AppId);
					//Set app data where condition
					$appdata_where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "(QuestionID = '" . $val [1] . "' OR SUBSTRING(QuestionID FROM 2) = '" . $val [1] . "')");
					//Get Applicant Data Information
					$resultsQ = $ApplicantsObj->getApplicantDataInfo("*", $appdata_where, '', 'ApplicationID', array($appdata_params));
					
					$applicant_data_num_rows = $resultsQ['count'];
					
					if(is_array($resultsQ['results'])) {
						foreach($resultsQ['results'] as $AD) {
							if ($AD ["Answer"] == "$val[3]") {
								$answer = $rank;
								$WEIGHT [$AppId] [$val [0]] [$val [1]] ["ANS"] = ( int ) $answer;
								$WEIGHT [$AppId] [$val [0]] [$val [1]] ["ANSWER"] = $val [3];
							}
						} // end foreach
					}
					
				} // end rank
			}
		}
	
		// calculated the score sepeartely for each saved search
		// if the answer is empty, make the question weight also zero(0)
		$total = 0;
		if(is_array($WEIGHT [$AppId])) {
			foreach ( $WEIGHT [$AppId] as $WS => $WQ ) {
				if(is_array($WQ)) {
					foreach ( $WQ as $QuestionID => $WQINFO ) {
					
						if (! array_key_exists ( "ANS", $WQINFO )) {
							$WEIGHT [$AppId] [$WS] [$QuestionID] ['ANS'] = 0;
							$WEIGHT [$AppId] [$WS] [$QuestionID] ['QUS'] = 0;
							$WQINFO ['QUS'] = $WQINFO ['ANS'] = 0;
						}
					
						$total += ( int ) $WQINFO ['QUS'];
						$total += ( int ) $WQINFO ['ANS'];
					}
				}
			}
		}
		
	
		$class = 'sd' . rand () . uniqid ();
	
		$RESULTS [$total] [] = '<table border="0" width="80%" cellpadding="3" cellspacing="3" align="center">';
		$RESULTS [$total] [] = '<tr>
							<th align="left" colspan="2" width="70%" style="background-color:#8A9DBF;color:#FFFFFF">' . $SName . '</th>
							<th align="right" style="background-color:#8A9DBF;color:#FFFFFF">Total:' . $total . '&nbsp;</th>
							<th style="background-color:#8A9DBF;color:#FFFFFF"><span onclick=\'$(".' . $class . '").toggle();$(this).toggleClass("weighted_search_collapse");\' class="weighted_search_expand"></span></th>
						  </tr></table><table border="0" width="80%" cellpadding="3" cellspacing="3" align="center">';
		
		if(is_array($WEIGHT [$AppId])) {
			foreach ( $WEIGHT [$AppId] as $WS => $WQ ) {
				$RESULTS [$total] [] = '<tr class="' . $class . '" style="display:none">
								<td colspan="4" style="background-color: #f5f5f5;text-align:left"><i>Section</i>: <strong><i>' . $WS . '</i></strong></td>
							  </tr>';
				if(is_array($WQ)) {
					foreach ( $WQ as $QuestionID => $WQINFO ) {
							
						//Set where condition
						$where_que_info = array("FormID = :FormID", "QuestionID = :QuestionID");
						//Set parameters
						$params_que_info = array(":FormID"=>$FormID, ":QuestionID"=>$QuestionID);
						$resquestion = $FormQuestionsObj->getFormQuestionsInformation("Question", $where_que_info, "", array($params_que_info));
						//Get Row Question
						$rowquestion = $resquestion['results'][0];
							
						$RESULTS [$total] [] = '<tr class="' . $class . '" style="display:none">
								    <td align="left" width="10%"><strong>Question</strong></td>
									<td width="1%">:</td>
								    <td>' . $rowquestion ['Question'] . '</td>
								    <td align="right"><i>Question Weight: </i>' . $WQINFO ['QUS'] . '</td>
								  </tr>';
						$RESULTS [$total] [] = '<tr class="' . $class . '" style="display:none">
									<td><strong>Answer: </strong></td>
									<td width="1%">:</td>
									<td>' . $WQINFO ['ANSWER'] . '</td>
									<td align="right"><i>Answer Weight: </i>' . $WQINFO ['ANS'] . '</td>
								  </tr>';
					}
				}
				
			}
		}
		
	
		$RESULTS [$total] [] = '<td colspan="4" align="right" class="' . $class . '" style="display:none">
							<strong style="background-color: #F5F5F5">Total Score: ' . $total . '</strong>
						  </td>
						</table>';
	}
}


$FINALRESULT = "";

// Sort The Array Based On Weighted Search Score
krsort ( $RESULTS );
if(is_array($RESULTS)) {
	foreach ( $RESULTS as $SK => $SV ) {
		$FINALRESULT .= implode ( "", $SV );
	}	
}

echo $FINALRESULT;