<?php
echo '<form method="post" action="applicants.php" name="frmContactCorrespondencePkg" id="frmContactCorrespondencePkg">
<div class="table-responsive">
<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">
<tr><td valign="top" colspan="2">
<strong>Select Email Package:</strong>&nbsp;&nbsp;';
echo '<select name="package" id="package" onchange="getCorrespondencePackageInfo(this.value);" class="form-control width-auto-inline">
<option value="">Choose a Package</option>';

//Get Correspondence Packages
$params_corresp = array (":OrgID" => $OrgID);
//Set Correspondence Packages
$where_corresp = array ("OrgID = :OrgID");
$results = $CorrespondenceObj->getCorrespondencePackages ( "UpdateID, PackageName", $where_corresp, 'PackageName', array ($params_corresp) );

if(is_array($results['results'])) {
	foreach($results['results'] as $CP) {

		$print = 'Y';

		if (($OrgID == "I20140912") && (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager')) { // Community Research Foundation
			if (substr ( $CP ['PackageName'], 0, 2 ) == 'HR') {
				$print = 'N';
			} // end packagename
		} // end CRF

		if ($print == "Y") {

			if ($package == $CP ['UpdateID']) {
				$selected = ' selected';
			} else {
				$selected = '';
			}
			echo '<option value="' . $CP ['UpdateID'] . '"' . $selected . '>' . $CP ['PackageName'] . '</option>';
		} // end print
	} // end foreach
}


echo '</select><input type="hidden" name="action" value="'.$action.'">';

if ($multipleapps) {
	echo '<input type="hidden" name="multi" value="Y">
		  <input type="hidden" name="multipleapps" value="'.$multipleapps.'">';
} else {
	echo '<input type="hidden" name="ApplicationID" value="'.$ApplicationID.'">
		  <input type="hidden" name="RequestID" value="'.$RequestID.'">';
}

echo '</td></tr>
</table></div>
</form>';
?>