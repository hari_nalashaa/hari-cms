<?php  
require_once '../Configuration.inc';

//Comment Request parameters
$RequestID              =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$ApplicationID          =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$ProcessOrder           =   isset($_REQUEST['ProcessOrder']) ? $_REQUEST['ProcessOrder'] : '';
$DispositionCode        =   isset($_REQUEST['DispositionCode']) ? $_REQUEST['DispositionCode'] : '';
$StatusEffectiveDate    =   isset($_REQUEST['StatusEffectiveDate']) ? $_REQUEST['StatusEffectiveDate'] : '';
$Comment                =   isset($_REQUEST['Comment']) ? $_REQUEST['Comment'] : '';
$multipleapps           =   isset($_REQUEST['multipleapps']) ? $_REQUEST['multipleapps'] : '';
$multi                  =   isset($_REQUEST['multi']) ? $_REQUEST['multi'] : '';

$message                =   "";
if ($_REQUEST['process'] == "Y") {
	
	if(isset($_REQUEST['process_type']) && $_REQUEST['process_type'] == "single") {
		require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
		require_once IRECRUIT_DIR . 'applicants/StatusProcess.inc';
	}
	if(isset($_REQUEST['process_type']) && $_REQUEST['process_type'] == "multiple") {
		require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
		if (($multi == "Y") && ($multipleapps)) {
		
			$ma = explode ( '|', $multipleapps );
			$macnt = count ( $ma ) - 1;
		
			for($i = 0; $i < $macnt; $i ++) {
		
				$jarj = explode ( ':', $ma [$i] );
				$ApplicationID = $jarj [0];
				$RequestID = $jarj [1];
		
				if (($ApplicationID) && ($RequestID)) {
		
					include IRECRUIT_DIR . 'applicants/StatusProcess.inc';
		
				} // end if
			} // end for
		} // end multiapps
		
	}

	//Get Checklist Process Order data
        $get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);

        $StatusMatch = "N";
        if($ProcessOrder == $get_checklist_process_data['ProcessOrder']){
            $StatusMatch = "Y";
            $get_checklist_process_data      =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);
            if($get_checklist_process_data){
                $now            = date('Y-m-d H:i:s');
                $set_info       =   array("StatusMatch = :StatusMatch","LastUpdated = :LastUpdated");
                $where_info     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
                $params         =   array(":StatusMatch"=>$StatusMatch, ":LastUpdated"=>$now, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
                G::Obj('ChecklistData')->updChecklistDataHeader($set_info, $where_info, array($params));
            }else{
                G::Obj('ChecklistData')->insChecklistHeaderData($OrgID, $ApplicationID, $RequestID, "", $StatusMatch);
            }


            $login_user    =   $USERID;
            //Insert Checklist Data History
            $new_checklist_history_info =   G::Obj('ChecklistData')->insChecklistDataHistory('', $OrgID, $ApplicationID, $RequestID, 'Application Checklist Activated for this Applicant.', $login_user,'');

	} else { // reset tab if applicant qualifies but status update is not a match
            $get_checklist_process_data      =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);
            if($get_checklist_process_data){
              $StatusMatch = "Y";
	    }
	}
	
	echo json_encode(array("ProcessOrder"=>$ProcessOrder,"StatusMatch"=>$StatusMatch));

}
?>
