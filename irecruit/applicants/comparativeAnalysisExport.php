<?php
require_once '../Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Config Columns List
$config_columns_list = array(
    "DispositionCode"                   => "Disposition Code",
    "ProcessOrder"                      => "Status",
    "EntryDate"                         => "Entry Date",
    "RequestID"                         => "Requisition Title",
    "Rating"                            => "Rating",
    "ApplicantConsiderationStatus1"     => "Thumbs Up / Down",
    "ApplicantConsiderationStatus2"     => "Label"
);
                 
   
//Get ApplicantDispositionCodes
$disposition_results = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'');
$disposition_rows = $results['count'];
$disposition_codes = array();

if ($disposition_rows > 0) {
    if(is_array($disposition_results['results'])) {
        foreach ($disposition_results['results'] as $row) {
            $disposition_codes[$row ['Code']] = $row ['Description'];
        }
    }
} // end if disposition set up

//Applicant Summary Questions List
$where_info  = array("OrgID = :OrgID");
$params_info = array(":OrgID"=>$OrgID);
$app_sum_que_results = $ApplicantsObj->getComparativeAnalysisQuestionsInfo("*", $where_info, '', '', array($params_info));
$app_sum_que = $app_sum_que_results['results'];
$app_sum_cnt = $app_sum_que_results['count'];

for($apq = 0; $apq < count($app_sum_que); $apq++) {
    $app_sum_que_list[$app_sum_que[$apq]['QuestionID']] = $app_sum_que[$apq]['Question'];

    $app_sum_que_info[$app_sum_que[$apq]['QuestionID']]['QuestionTypeID'] = $app_sum_que[$apq]['QuestionTypeID'];
    $app_sum_que_info[$app_sum_que[$apq]['QuestionID']]['SectionID'] = $app_sum_que[$apq]['SectionID'];
}

if(is_array($app_sum_que_list)) $app_sum_que_keys_list = array_keys($app_sum_que_list);

//Get weighted searches list information
$weight_search_names = array();
$weighted_searches_keys = array();
$weight_search_names = $WeightedSearchObj->getWeightedSearchNameValues($OrgID, $USERID);
if(is_array($weight_search_names)) $weighted_searches_keys = array_keys($weight_search_names);

//Get comparative analysis configuration information
$comp_analysis_config_info  =    $ComparativeAnalysisObj->getComparativeAnalysisConfig($OrgID, $USERID);
$config_info    =   json_decode($comp_analysis_config_info['ColumnsList'], true);

if(isset($_POST['export_appids_list'])) {
    $app_list_post = $_POST['export_appids_list'];
}

$app_results['results'] = array();
//Insert ApplicationID's
foreach($app_list_post as $ApplicationRequestID) {
    $ApplicationRequestIDInfo   =   explode(":", $ApplicationRequestID);
    $CompareApplicationID       =   $ApplicationRequestIDInfo[0];
    $CompareRequestID           =   $ApplicationRequestIDInfo[1];

    $columns = "ApplicationID, ApplicantSortName, ProcessOrder, DispositionCode, EntryDate, RequestID, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2, Rating";
    $app_results['results'][] = $ApplicationsObj->getJobApplicationsDetailInfo($columns, $OrgID, $CompareApplicationID, $CompareRequestID);
}

$applicants_list  =   $app_results['results'];


$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Comparative Analysis")
->setSubject("Excel")
->setDescription("To view the applicants information those are selected in comparative analysis")
->setKeywords("phpExcel")
->setCategory("Comparative Analysis");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();


$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application ID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Applicant Name');

if(is_array($config_info['JobApplicationsColumnsList'])) {
    foreach ($config_info['JobApplicationsColumnsList'] as $column_id) {
        $al++;
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue( $config_columns_list[$column_id]);

    }
}

if(is_array($config_info['ComparativeAnalysisQuestionColumnsList'])) {
    foreach ($config_info['ComparativeAnalysisQuestionColumnsList'] as $app_summary_que_id) {
        $al++;
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue(  $app_sum_que_list[$app_summary_que_id]);

    }
}



if(is_array($config_info['WeightedSearchColumnsList'])) {
    foreach ($config_info['WeightedSearchColumnsList'] as $weighted_search_id) { 
	if($weighted_search_id == 'wotc_application_status') {
        $weighted_search_name ="CMS WOTC Application Status";
        }else{
        $weighted_search_name = $weight_search_names[$weighted_search_id];
        } 
        $al++;
             $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue($weighted_search_name);

    }
}

if(isset($config_info['WeightedSearchColumnsListSum'])
&& $config_info['WeightedSearchColumnsListSum'] == "cumulative") {
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue("Total");
}



if(isset($config_info['ApplicantDistance'])
&& $config_info['ApplicantDistance'] == "yes") {
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue("Applicant Distance");

}



###  Append Applicants Data To Spread Sheet ###

$where_info = array("OrgID = :OrgID", "UserID = :UserID");
$params_info = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
$weighted_searches_results = $WeightedSearchObj->getWeightSaveSearchInfo("FormID, SaveSearchName, WeightedSearchRandId, SectionID", $where_info, "WeightedSearchRandId", "", array($params_info));
$weighted_searches_info = $weighted_searches_results['results'];

$ws_frm_types = array();
$ws_sections = array();
for($wsi = 0; $wsi < count($weighted_searches_info); $wsi++) {
    $ws_frm_types[$weighted_searches_info[$wsi]['WeightedSearchRandId']] = $weighted_searches_info[$wsi]['FormID'];
    $ws_sections[$weighted_searches_info[$wsi]['WeightedSearchRandId']] = $weighted_searches_info[$wsi]['SectionID'];
}

for($i = 0, $l = 2; $i < count($applicants_list); $i++, $l++) {
    $alp = 0;
    $ApplicationID                  =   $applicants_list[$i]['ApplicationID'];
    $ApplicantName                  =   $applicants_list[$i]['ApplicantSortName'];
    $RequestID                      =   $applicants_list[$i]['RequestID'];
    $ProcessOrder                   =   $applicants_list[$i]['ProcessOrder'];
    $ApplicantConsiderationStatus1  =   $applicants_list[$i]['ApplicantConsiderationStatus1'];
    $ApplicantConsiderationStatus2  =   $applicants_list[$i]['ApplicantConsiderationStatus2'];
    $ApplicantConsiderationLabels   =   json_decode($organization_info['ApplicantConsiderationLabels'], true);
    $DispositionCode                =   $disposition_codes[$applicants_list[$i]['DispositionCode']];
    
    $app_req_id                     =   $ApplicationID . ':' . $RequestID;
    $multiorgid_req                 =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
    

    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($ApplicationID);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($ApplicantName);


    if(is_array($config_info['JobApplicationsColumnsList'])) {
        foreach ($config_info['JobApplicationsColumnsList'] as $column_id) {
            if($column_id == "ProcessOrder") {
                $status     = $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, $ProcessOrder );
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($status);

            }
            else if($column_id == "DispositionCode") {
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($DispositionCode);
            }
            else if($column_id == "RequestID") {
                $job_title = $RequisitionDetailsObj->getJobTitle($OrgID, $multiorgid_req, $RequestID);
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($job_title);

            }
            else if($column_id == "ApplicantConsiderationStatus1") {
                if($ApplicantConsiderationStatus1 == "ThumbsUp") {
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue("ThumbsUp");

                }
                else if($ApplicantConsiderationStatus1 == "ThumbsDown") {
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue("ThumbsDown");

                }
                else {
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue(" ");

                }
            }
            else if($column_id == "ApplicantConsiderationStatus2") {
                $applicant_label = $ApplicantConsiderationLabels[$ApplicantConsiderationStatus2];
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($applicant_label);

            }
            else if($column_id == "Rating") {
                $rating_value = $applicants_list[$i][$column_id];

                $star_span = '';
                for($star_index = 1; $star_index <= $rating_value; $star_index++) {
                    $star_span .= '*';
                }
                
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($star_span);

            }
            else {
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($applicants_list[$i][$column_id]);
            }
        }
    }
  if(is_array($config_info['ComparativeAnalysisQuestionColumnsList'])) {
        foreach ($config_info['ComparativeAnalysisQuestionColumnsList'] as $app_summary_que_id) {
            $app_sum_que_ans = $ComparativeAnalysisObj->getApplicantQuestionsSummary($OrgID, $ApplicationID, $app_summary_que_id, $app_sum_que_info[$app_summary_que_id]['QuestionTypeID'], $app_sum_que_list[$app_summary_que_id], $app_sum_que_info[$app_summary_que_id]['SectionID']);
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue(strip_tags($app_sum_que_ans));

        }
    }


    $total_weighted_search_score = 0;
    if(is_array($config_info['WeightedSearchColumnsList'])) {
        foreach ($config_info['WeightedSearchColumnsList'] as $weighted_search_id) {
            $weighted_search_name = $weight_search_names[$weighted_search_id];

            if($ws_frm_types[$weighted_search_id] == "WebForms") {
                $applicant_score = $WeightedSearchObj->getScoreOfApplicantForWebForm($OrgID, $ApplicationID, $ws_sections[$weighted_search_id], $weighted_search_id);
                $total_weighted_search_score += $applicant_score;
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($applicant_score);

            }
            else {
                $applicant_score = $WeightedSearchObj->getWeightedScoreOfApplicant($OrgID, $ApplicationID, $weighted_search_id);
                $total_weighted_search_score += $applicant_score;
                $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($applicant_score);
            }
        }
    }

    if(isset($config_info['WeightedSearchColumnsListSum'])
    && $config_info['WeightedSearchColumnsListSum'] == "cumulative") {
         $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($total_weighted_search_score);
    }


    if(isset($config_info['ApplicantDistance'])
    && $config_info['ApplicantDistance'] == "yes") {

        $applicant_distance = $ComparativeAnalysisObj->getApplicantDistance($OrgID, $multiorgid_req, $RequestID, $ApplicationID);
        $applicant_distance = sprintf("%01.1f", str_replace(",", "", $applicant_distance));

         $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue( $applicant_distance);

    }

    

}


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="comparative_analysis.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
