<?php
require_once '../Configuration.inc';

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto = $_SERVER ['QUERY_STRING'];
}

// Set page title
$TemplateObj->title     =   $title  =   "Comparative Analysis";

//Config Columns List
$config_columns_list    =   array(
                                "DispositionCode"                   => "Disposition Code",
                                "ProcessOrder"                      => "Status",
                                "EntryDate"                         => "Entry Date",
                                "RequestID"                         => "Requisition Title",
                                "Rating"                            => "Rating",
                                "ApplicantConsiderationStatus1"     => "Thumbs Up / Down",
                                "ApplicantConsiderationStatus2"     => "Label"
                            );

//Get Wotc Ids list
$TemplateObj->wotcid_info =	$wotcid_info = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, "");

//Applicant Summary Questions List
$where_info     =   array("OrgID = :OrgID");
$params_info    =   array(":OrgID"=>$OrgID);
$app_sum_ques   =   $ApplicantsObj->getComparativeAnalysisQuestionsInfo("*", $where_info, '', '', array($params_info));
$app_sum_que    =   $app_sum_ques['results'];
$app_sum_cnt    =   $app_sum_ques['count'];

for($apq = 0; $apq < count($app_sum_que); $apq++) {
    $app_sum_que_list[$app_sum_que[$apq]['QuestionID']] = $app_sum_que[$apq]['Question'];
    $app_sum_que_info[$app_sum_que[$apq]['QuestionID']]['QuestionTypeID'] = $app_sum_que[$apq]['QuestionTypeID'];
    $app_sum_que_info[$app_sum_que[$apq]['QuestionID']]['SectionID'] = $app_sum_que[$apq]['SectionID'];
}

if(is_array($app_sum_que_list)) $app_sum_que_keys_list = array_keys($app_sum_que_list);

//Get weighted searches list information
$weight_search_names    =   array();
$weighted_searches_keys =   array();
$weight_search_names    =   $WeightedSearchObj->getWeightedSearchNameValues($OrgID, $USERID);
if(is_array($weight_search_names)) $weighted_searches_keys = array_keys($weight_search_names);

if($wotcid_info['wotcID'] != "") {
	$weighted_searches_keys[]	=	'wotc_application_status';	//first value of wotc application status
}

//Get comparative analysis configuration information
$comp_analysis_config_info  =   $ComparativeAnalysisObj->getComparativeAnalysisConfig($OrgID, $USERID);
$config_info                =   json_decode($comp_analysis_config_info['ColumnsList'], true);

### Code to update the weighted search columns, if the weighted search is deleted by the user ###

$columns_list['JobApplicationsColumnsList'] = $config_info['JobApplicationsColumnsList'];

$weighted_search_columns_filtered_list = array();

if(is_array($config_info['WeightedSearchColumnsList'])) {
    
    foreach($config_info['WeightedSearchColumnsList'] as $column_value) {
    
        if(in_array($column_value, $weighted_searches_keys)) {
            $weighted_search_columns_filtered_list[] = $column_value;
        }
    }
    
    $columns_list['WeightedSearchColumnsList'] = $weighted_search_columns_filtered_list;
    
    if(isset($config_info['WeightedSearchColumnsListSum'])) {
        $columns_list['WeightedSearchColumnsListSum'] = $config_info['WeightedSearchColumnsListSum'];
    }
}

/*$app_summary_que_columns_filtered_list = array();

if(is_array($config_info['ApplicantSummaryQuestionColumnsList'])) {
    
    foreach($config_info['ApplicantSummaryQuestionColumnsList'] as $column_value) {

        if(in_array($column_value, $app_sum_que_keys_list)) {
            $app_summary_que_columns_filtered_list[] = $column_value;
        }
    }

    $columns_list['ApplicantSummaryQuestionColumnsList'] = $app_summary_que_columns_filtered_list;
}*/

$comp_ana_columns_filtered_list = array();

if(is_array($config_info['ComparativeAnalysisQuestionColumnsList'])) {
    
    foreach($config_info['ComparativeAnalysisQuestionColumnsList'] as $column_value) {

        if(in_array($column_value, $app_sum_que_keys_list)) {
            $comp_ana_columns_filtered_list[] = $column_value;
        }
    }

    $columns_list['ComparativeAnalysisQuestionColumnsList'] = $comp_ana_columns_filtered_list;
}

if(isset($config_info['ApplicantDistance'])) {
    $columns_list['ApplicantDistance'] = $config_info['ApplicantDistance'];
}

//Insert or update comparative analysis configuration
$ComparativeAnalysisObj->insUpdComparativeAnalysisConfig($OrgID, $USERID, $columns_list);

### ------------------------------------------------------------------------------------------ ###

//Get ApplicantDispositionCodes
$disposition_results = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'');
$disposition_rows = $results['count'];
$disposition_codes = array();

if ($disposition_rows > 0) {
    if(is_array($disposition_results['results'])) {
        foreach ($disposition_results['results'] as $row) {
            $disposition_codes[$row ['Code']] = $row ['Description'];
        }
    }
} // end if disposition set up


//Comparative Label Results
$comp_lbl_results  =   $ComparativeAnalysisObj->getComparativeLabels($OrgID, $USERID);
$comp_lbls_list    =   $comp_lbl_results['results'];

//Get comparative analysis configuration information
$comp_analysis_config_info  =	$ComparativeAnalysisObj->getComparativeAnalysisConfig($OrgID, $USERID);
$config_info   =   json_decode($comp_analysis_config_info['ColumnsList'], true);

if((count($_POST) > 0 && count($_POST['appids_list']) > 0)
    || (count($_POST) > 0 && count($_POST['appid']) > 0)) {
    
    if(isset($_POST['appid'])) {
        $app_list_post = $_POST['appid'];
    }
    if(isset($_POST['appids_list'])) {
        $app_list_post = $_POST['appids_list'];
    }
    
    $app_results['results'] = array();
    //Insert ApplicationID's
    foreach($app_list_post as $ApplicationRequestID) {
        $ApplicationRequestIDInfo   =   explode(":", $ApplicationRequestID);
        $CompareApplicationID       =   $ApplicationRequestIDInfo[0];
        $CompareRequestID           =   $ApplicationRequestIDInfo[1];
        
        $columns = "ApplicationID, ApplicantSortName, ProcessOrder, DispositionCode, EntryDate, RequestID, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2";
        $app_results['results'][] = $ApplicationsObj->getJobApplicationsDetailInfo($columns, $OrgID, $CompareApplicationID, $CompareRequestID);
    }

}
else if(isset($_REQUEST['label_id']) && $_REQUEST['label_id'] != "") {
    $app_results        =   $ComparativeAnalysisObj->getJobApplicationsInfoByLabelID($OrgID, $USERID, $_REQUEST['label_id']);
}
else {
	if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
		$RequestID = $RequisitionDetailsObj->getHiringManagerRequestID();
		$where_info         =   array("OrgID = :OrgID", "RequestID IN ('" . implode("','",$RequestID) . "')");
		$params_info        =   array(":OrgID"=>$OrgID);
	}else{
	    $where_info         =   array("OrgID = :OrgID");
	    $params_info        =   array(":OrgID"=>$OrgID);
	}
	$order		        =   "EntryDate DESC LIMIT 100";

    $app_results        =   $ApplicationsObj->getJobApplicationsInfo("*", $where_info, '', $order, array($params_info));
}

$applicants_total_count =   count($app_results['results']);
$applicants_results     =   $app_results['results'];

for($a = 0; $a < $applicants_total_count; $a++) {
    $app_results['results'][$a]['Email'] = $ApplicantDetailsObj->getAnswer($applicants_results[$a]['OrgID'], $applicants_results[$a]['ApplicationID'], 'email');
}

//Applicants list
$TemplateObj->applicants_list       =   $applicants_list    =   $app_results['results'];

// Get Requisition Information
$TemplateObj->req_results           =   $req_results        =   $RequisitionsObj->getRequisitionInformation ( "Title, RequestID, RequisitionID, JobID", $where_info, "", "", array ($params_info));

$TemplateObj->app_sum_que           =   $app_sum_que;
$TemplateObj->app_sum_cnt           =   $app_sum_cnt;
$TemplateObj->app_sum_que_list      =   $app_sum_que_list;
$TemplateObj->app_sum_que_info      =   $app_sum_que_info;

$TemplateObj->req_list              =   $req_list = $req_results['results'];
$TemplateObj->config_info           =   $config_info;
$TemplateObj->config_columns_list   =   $config_columns_list;
$TemplateObj->weight_search_names   =   $weight_search_names;
$TemplateObj->comp_lbls_list        =   $comp_lbls_list;
$TemplateObj->disposition_codes     =   $disposition_codes;

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/ComparativeAnalysis' );
?>
