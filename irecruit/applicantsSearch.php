<?php
require_once 'Configuration.inc';
ini_set("allow_url_fopen", 1);

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto = $_SERVER ['QUERY_STRING'];
}

//Insert Default Address
G::Obj('OrgAddressList')->insDefaultAddress($OrgID);

//Insert Default Twilio Messaging Templates
G::Obj('TwilioTextMessageTemplates')->insDefaultTwilioTextMessagingTemplates($OrgID);

//Get onboard settings by orgid
$onboard_settings					=	G::Obj('OnboardSettings')->getOnboardSettingsInfoByOrgID($OrgID);

// Set page title
$TemplateObj->title                 =   "Applicants Search";
$TemplateObj->ApplicantsSearchGuID  =   $ApplicantsSearchGuID   =   $GenericLibraryObj->getGlobalUniqueID($OrgID);


$single_onboard_process             =   '';

if($feature['MatrixCare'] == 'Y') {
    $single_onboard_process         =   'MatrixCare';
}
if($feature['WebABA'] == 'Y') {
    $single_onboard_process         =   'WebABA';
}
if($feature['Lightwork'] == 'Y') {
    $single_onboard_process         =   'Lightwork';
}
if($feature['Abila'] == 'Y') {
    $single_onboard_process         =   'Abila';
}
if($feature['Mercer'] == 'Y') {
    $single_onboard_process         =   'Mercer';
}
if($feature['DataManagerType'] != 'N') {
    $single_onboard_process         =   $feature['DataManagerType'];
}

// Search Request Parameters
$TemplateObj->single_onboard_process    =   $single_onboard_process =   $single_onboard_process;
$TemplateObj->criteria                  =   $criteria               =   isset ( $_REQUEST ['criteria'] ) ? $_REQUEST ['criteria'] : '';
$TemplateObj->MultiOrgID				=   $MultiOrgID				=   isset ( $_REQUEST ['MultiOrgID'] ) ? $_REQUEST ['MultiOrgID'] : '';
$TemplateObj->SearchWords               =   $SearchWords            =   isset ( $_REQUEST ['SearchWords'] ) ? $_REQUEST ['SearchWords'] : '';
$TemplateObj->refinereq                 =   $refinereq              =   isset ( $_REQUEST ['refinereq'] ) ? $_REQUEST ['refinereq'] : '';
$TemplateObj->Active                    =   $Active                 =   isset ( $_REQUEST ['Active'] ) ? $_REQUEST ['Active'] : '';
$TemplateObj->ProcessOrder              =   $ProcessOrder           =   isset ( $_REQUEST ['ProcessOrder'] ) ? $_REQUEST ['ProcessOrder'] : '';
$TemplateObj->Code                      =   $Code                   =   isset ( $_REQUEST ['Code'] ) ? $_REQUEST ['Code'] : '';
$TemplateObj->ApplicationDate_From      =   $ApplicationDate_From   =   isset ( $_REQUEST ['ApplicationDate_From'] ) ? $_REQUEST ['ApplicationDate_From'] : '';
$TemplateObj->ApplicationDate_To        =   $ApplicationDate_To     =   isset ( $_REQUEST ['ApplicationDate_To'] ) ? $_REQUEST ['ApplicationDate_To'] : '';
$TemplateObj->Code                      =   $Code                   =   isset ( $_REQUEST['Code'] ) ? $_REQUEST['Code'] : '';

if(isset($_REQUEST['RequestIDs'])) $TemplateObj->RequestIDs = $RequestIDs = $_REQUEST['RequestIDs'];

//Comparative Label Results
$comp_lbl_results  =   $ComparativeAnalysisObj->getComparativeLabels($OrgID, $USERID);
$TemplateObj->comp_lbls_list = $comp_lbls_list = $comp_lbl_results['results'];

if (isset($_REQUEST['SearchName']) && $_REQUEST['SearchName'] != "") {
	$SearchName = $_REQUEST['SearchName'];

	if (isset($RequestIDs)) {
		if(is_array($RequestIDs) && count($RequestIDs) > 0) {
			foreach ( $RequestIDs as $t ) {
				$rjid .= $t . "|";
			}
		}
	}

	if(is_null($rjid)) $rjid = "";
	if(is_null($SearchName)) $SearchName = "";
	if(is_null($SearchWords)) $SearchWords = "";
	if(is_null($Code)) $Code = "";
	if(is_null($LimitProcessed)) $LimitProcessed = "";
	if(is_null($LimitReceivedDate)) $LimitReceivedDate = "";
	if(is_null($MultiOrgID)) $MultiOrgID = "";

	// UserSavedSearches Information
	$info  =   array(
        			"OrgID"                  => $OrgID,
        			"UserID"                 => $USERID,
        			"SearchName"             => $SearchName,
        			"Active"                 => $Active,
        			"SearchWords"            => $SearchWords,
        			"RequestIDs"             => $rjid,
        			"ProcessOrder"           => $ProcessOrder,
        			"Code"                   => $Code,
        			"ApplicationDate_From"   => G::Obj('DateHelper')->getYmdFromMdy($ApplicationDate_From),
                    "ApplicationDate_To"     => G::Obj('DateHelper')->getYmdFromMdy($ApplicationDate_To),
        			"DataManager"            => $LimitProcessed,
        			"LimitReceivedDate"      => $LimitReceivedDate,
        			"MultiOrgID"             => $MultiOrgID
        	   );

	//Insert UserSavedSearches
	G::Obj('IrecruitUsers')->insUserSavedSearches ( $info );
} // end if name

//Get Twilio Account Information
$TemplateObj->account_info      =   $account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
if($account_info['AccountSid'] != "") {
    $TemplateObj->twilio_user_info	=	$twilio_user_info	=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "TwilioSms");
}

//Set where condition
$where		=	array("OrgID = :OrgID", "UserID = :UserID");
//Set parameters
$params 	=	array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
//Set columns
$columns	=	'SearchName, SearchWords, RequestIDs, ProcessOrder, Code, date_format(ApplicationDate_From,"%m/%d/%Y") ApplicationDate_From, date_format(ApplicationDate_To,"%m/%d/%Y") ApplicationDate_To, DataManager, Active, LimitReceivedDate, MultiOrgID';
//Get saved searches
$results_usr_saved_searches = G::Obj('IrecruitUsers')->getUserSavedSearches($columns, $where, "", array($params));

$TemplateObj->saved_searches_list   =   $saved_searches_list    =   $results_usr_saved_searches['results'];
$TemplateObj->saved_searches_count  =   $saved_searches_count   =   $results_usr_saved_searches['count'];

if(is_array($saved_searches_list)) {
	foreach($saved_searches_list as $row) {
	    if (isset($_REQUEST['SearchName']) && ($row ['SearchName'] == $_REQUEST['SearchName'])) {
			
			$RequestIDSList = explode("|", $row ['RequestIDs']);
			if(count($RequestIDSList) > 0) {
				$TemplateObj->RequestIDs = $RequestIDs = $RequestIDSList;
			}
			
			$TemplateObj->ProcessOrder			=	$ProcessOrder			= $row ['ProcessOrder'];
			$TemplateObj->Code					=	$Code 					= $row ['Code'];
			$TemplateObj->ApplicationDate_From	=	$ApplicationDate_From 	= $row ['ApplicationDate_From'];
			$TemplateObj->ApplicationDate_To	=	$ApplicationDate_To 	= $row ['ApplicationDate_To'];
			$TemplateObj->LimitProcessed		=	$LimitProcessed 		= $row ['DataManager'];
			$TemplateObj->LimitReceivedDate		=	$LimitReceivedDate 		= $row ['LimitReceivedDate'];
			$TemplateObj->MultiOrgID			=	$MultiOrgID 				= $row ['MultiOrgID'];
			$TemplateObj->Active				=	$Active 				= $row ['Active'];

			if($ProcessOrder == "0") $TemplateObj->ProcessOrder = $ProcessOrder = "";
			if($Code == "0") $TemplateObj->Code = $Code = "";
			
			//Set where condition
			$where_search_query      =   array("OrgID = :OrgID", "UserID = :UserID", "GuID = :GuID");
			//Set parameters
			$params_search_query     =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":GuID"=>$ApplicantsSearchGuID);
			//Get SearchQuery Information
			$results_saved_query     =   G::Obj('Organizations')->getSearchQuery("SearchParams", $where_search_query, '', array($params_search_query));
			$search_params_list      =   $results_saved_query['results'][0]['SearchParams'];
			$search_params_list_info =   json_decode($search_params_list, true);
			
			$search_params_list_info[':BeginDate1'] = $DateHelperObj->getYmdFromMdy($ApplicationDate_From);
			$search_params_list_info[':FinalDate1'] = $DateHelperObj->getYmdFromMdy($ApplicationDate_To);
				
			$upd_params_search_query =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":GuID"=>$ApplicantsSearchGuID, ":SearchParams"=>json_encode($search_params_list_info));
			$search_set_info         =   "SearchParams = :SearchParams";
			G::Obj('Organizations')->updSearchQuery($search_set_info, $where_search_query, array($upd_params_search_query));
				
		}
	}  //end foreach
}

if($ApplicationDate_From == "00/00/0000" 
	|| $ApplicationDate_From == ""
	|| $ApplicationDate_To == "00/00/0000"
	|| $ApplicationDate_To == "")
{
	//Get UserPreferences
	$user_preferences_info = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
	$DR = $user_preferences_info['DefaultSearchDateRange'];
	if ($DR == "") $DR = "90";

	//Set columns
	$columns = 'date_format(DATE_SUB(CURDATE(),INTERVAL ' . $DR . '  DAY), "%Y-%m-%d %H:%i:%s") as BeginDate, date_format(DATE_ADD(CURDATE(),INTERVAL 0 DAY), "%Y-%m-%d %H:%i:%s") as FinalDate';
	$results_date = $MysqlHelperObj->getDatesList($columns);
	
	if(is_array($results_date)) {
		list($ApplicationDate_From, $ApplicationDate_To) = array_values($results_date);
	}
	
	$TemplateObj->ApplicationDate_From = $ApplicationDate_From;
	$TemplateObj->ApplicationDate_To   = $ApplicationDate_To;
}

$TemplateObj->WSList                    =   $WSList                     =   isset ( $_REQUEST ['WSList'] ) ? $_REQUEST ['WSList'] : '';
$TemplateObj->LimitProcessed            =   $LimitProcessed             =   isset ( $_REQUEST ['LimitProcessed'] ) ? $_REQUEST ['LimitProcessed'] : '';
$TemplateObj->LimitReceivedDate         =   $LimitReceivedDate          =   isset ( $_REQUEST ['LimitReceivedDate'] ) ? $_REQUEST ['LimitReceivedDate'] : '';
$TemplateObj->SearchName                =   $SearchName                 =   isset ( $_REQUEST ['SearchName'] ) ? $_REQUEST ['SearchName'] : '';
$TemplateObj->k                         =   $k                          =   isset ( $_REQUEST ['k'] ) ? $_REQUEST ['k'] : '';
$TemplateObj->DisplayApplicantSummary   =   $DisplayApplicantSummary    =   "no";
$TemplateObj->LimitApplicants           =   $LimitApplicants            =   $preferences['ApplicantsSearchResultsLimit'];
$TemplateObj->ApplicationID             =   $ApplicationID              =   isset ( $_REQUEST ['ApplicationID'] ) ? $_REQUEST ['ApplicationID'] : '';
$TemplateObj->RequestID                 =   $RequestID                  =   isset ( $_REQUEST ['RequestID'] ) ? $_REQUEST ['RequestID'] : '';

if (isset ( $_GET ['sorttype'] ) && $_GET ['sorttype'] != "" && $_GET ['sorttype'] == "ASC") {
	$TemplateObj->to_sort = $to_sort = "ASC";
} else if ((isset ( $_GET ['sorttype'] ) && $_GET ['sorttype'] != "" && $_GET ['sorttype'] == "DESC")) {
	$TemplateObj->to_sort = $to_sort = "DESC";
} else {
	$TemplateObj->to_sort = $to_sort = "DESC";
}

//Update organization applicant Labels with default values if empty
if($organization_info['ApplicantConsiderationLabels'] == ''
    && isset($OrgID) 
    && $OrgID != "") {
	$app_cons_labels_default = array("Shortlist"=>"Shortlisted","TechRound"=>"Cleared technical round","Hr"=>"Cleared hr round","Hired"=>"Hired", "Rejected"=>"Rejected");
	$set_info = array("ApplicantConsiderationLabels = :ApplicantConsiderationLabels");
	$where_info = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":ApplicantConsiderationLabels"=>json_encode($app_cons_labels_default));
	$OrganizationsObj->updOrganizationInfo("OrgData", $set_info, $where_info, array($params));
}

// Load Graphs Related Scripts, Only for this page
$scripts_header [] = "js/loadAJAX.js";
$scripts_header [] = "js/irec_Display.js";
$scripts_header [] = "tiny_mce/tinymce.min.js";
$scripts_header [] = "js/irec_Textareas.js";
$scripts_header [] = "js/selectbox.min.js";
$scripts_header [] = "js/weightedsearch.js";
$scripts_header [] = "js/applicants-search.js";
$scripts_header [] = "js/rating.min.js";
$scripts_header [] = "js/jquery.form.js";
$scripts_header [] = "js/twilio.js";
$scripts_header [] = "js/common.js";

// Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

// Include configuration related javascript in footer
$scripts_footer [] = "js/applicants.js";

// Assign add applicant datepicker variables
$script_vars_footer [] = 'var datepicker_ids = "#StatusEffectiveDate, #DownloadProcessDate_From, #DownloadProcessDate_To, #ApplicationDate_From, #ApplicationDate_To, #ProcessDate_From, #ProcessDate_To, #ProcessDate_From, #ProcessDate_To, #Sage_HireDate";';
$script_vars_footer [] = 'var date_format = "mm/dd/yy";';
$script_vars_header [] = 'var single_onboard_process = "'.$single_onboard_process.'";';

if(isset($onboard_settings['InitialCaps']) && $onboard_settings['InitialCaps'] == "Yes") {
	$script_vars_footer [] = 'var initial_caps = "Yes";';
}
else {
	$script_vars_footer [] = 'var initial_caps = "No";';
}

$TemplateObj->script_vars_header    =   $script_vars_header;
$TemplateObj->scripts_vars_footer   =   $script_vars_footer;

// Assign all scripts to page_scripts_footer object
$TemplateObj->page_scripts_footer = $scripts_footer;

//Set non responsive css for
$page_styles["header"][] = 'css/non-responsive.css';
$page_styles["header"][] = 'css/applicants-search-print.css';

//Set page styles information
$TemplateObj->page_styles =  $page_styles;

if(!isset($_REQUEST['ApplicationID']) 
	&& !isset($_REQUEST['RequestID'])
	&& !isset($_REQUEST['process'])) {

	//Set where condition
	$where_search_query    =   array("OrgID = :OrgID", "UserID = :UserID", "GuID = :GuID");
	//Set parameters
	$params_search_query   =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":GuID"=>$ApplicantsSearchGuID);
	//Get SearchQuery Information
	$results_saved_query   =   $OrganizationsObj->getSearchQuery("Query, MainSearchWhere, FilterSearchWhere, SearchParams, SortValue, SortType", $where_search_query, '', array($params_search_query));
	
	if(!isset($results_saved_query['results'][0])) {
	    $results_saved_query['results'][0]['MainSearchWhere']      =   '';
	    $results_saved_query['results'][0]['Query']                =   '';
	    $results_saved_query['results'][0]['FilterSearchWhere']    =   '';
	    $results_saved_query['results'][0]['SortValue']            =   '';
	    $results_saved_query['results'][0]['SortType']             =   '';
	    $results_saved_query['results'][0]['SearchParams']         =   json_encode(array());
	}
	
	$TemplateObj->where_params         =   $where_params       =   $results_saved_query['results'][0]['MainSearchWhere'];
	$TemplateObj->main_search_query    =   $main_search_query  =   $results_saved_query['results'][0]['Query'];

	$filter_search_where               =   $results_saved_query['results'][0]['FilterSearchWhere'];
	$sort_value                        =   $results_saved_query['results'][0]['SortValue'];
	$sort_type                         =   $results_saved_query['results'][0]['SortType'];
	
	$TemplateObj->sort_value           =   $sort_value;
	$TemplateObj->to_sort              =   $to_sort            =   $TemplateObj->sort_type = $sort_type;
	$TemplateObj->filter_params        =   $filter_params      =   json_decode($results_saved_query['results'][0]['SearchParams'], true);
	$TemplateObj->filter_search_where  =   $filter_search_where;
	
	if($main_search_query != "" && $where_params != "") {
		$saved_search_query = $main_search_query . " WHERE " . $where_params;
		if($filter_search_where != "") {
			$saved_search_query .= " AND ".$filter_search_where;
		}
	
		if($saved_search_query != "") {
			
			if($sort_value == "RequisitionTitle") $saved_search_query .= " ORDER BY RequestID $to_sort, EntryDate";
			if($sort_value == "ApplicantSortName") $saved_search_query .= " ORDER BY ApplicantSortName";
			if($sort_value == "AppID") $saved_search_query .= " ORDER BY ApplicationID";
			if($sort_value == "ApplicantStatus") $saved_search_query .= " ORDER BY ProcessOrder $to_sort, EntryDate";
			if($sort_value == "EntryDate") $saved_search_query .= " ORDER BY JobApplications.EntryDate $to_sort";
			
			if($sort_value == "") {
				$TemplateObj->sort_value = "EntryDate";
				$TemplateObj->to_sort = $to_sort = $TemplateObj->sort_type = $sort_type = "DESC";
				$saved_search_query .= " ORDER BY JobApplications.EntryDate $to_sort";
			}
			
			$saved_search_results_cnt = $ApplicationsObj->getInfo($saved_search_query, array($filter_params));
			$TemplateObj->total_applicants_count = $total_applicants_count = count($saved_search_results_cnt['results']);
		}
		
		if($saved_search_query != "") {
			$saved_search_query .= " LIMIT 0, $LimitApplicants";
			$saved_search_results = $ApplicationsObj->getInfo($saved_search_query, array($filter_params));
	
			//Get ProcessApplicants DropDown Information
			include IRECRUIT_DIR . 'applicants/ProcessApplicantsDropDown.inc';
			
			// print the search rows //Get Job Applications Information
			$TemplateObj->applicants_search_results = $applicants_search_results = $saved_search_results['results'];
			//Set count
			$TemplateObj->cnt = $cnt = count($saved_search_results['results']);
		}
		
	}
}
else { 
	if(isset($_REQUEST['ApplicationID']) && isset($_REQUEST['RequestID'])) {
		
		/**
		 * @tutorial	Build Default Query
		 */
		if(!isset($SortValue) || $SortValue == "") $SortValue = "EntryDate";
		if(!isset($SortType) || $SortType == "") $SortType = "DESC";
		
		$columns              =   "JobApplications.ApplicationID, ApplicantSortName, RequestID, date_format(EntryDate,'%M %d, %Y; %H:%i EST') as EntryDate, ProcessOrder, DispositionCode, StatusEffectiveDate, Correspondence, ReceivedDate, MultiOrgID, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2, Rating, LeadGenerator, LeadStatus";
		$main_search_query    =   "SELECT $columns FROM JobApplications";
		
		$search_where         =   array("JobApplications.OrgID = :OrgID");
		$filter_params[":OrgID"] = $OrgID;
		
		$search_where[]       =   "(EntryDate <= :FinalDate1 AND EntryDate >= :BeginDate1)";
		
		//Get UserPreferences
		$user_preferences_info    =   G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
		$DR = $user_preferences_info['DefaultSearchDateRange'];
		if ($DR == "") {
			$DR = "90";
		}
		
		//Set columns
		$columns = 'date_format(DATE_SUB(CURDATE(),INTERVAL ' . $DR . '  DAY), "%Y-%m-%d %H:%i:%s") as BeginDate, date_format(DATE_ADD(CURDATE(),INTERVAL 0 DAY), "%Y-%m-%d %H:%i:%s") as FinalDate';
		$results_date = $MysqlHelperObj->getDatesList($columns);
		
		if(is_array($results_date)) {
			list($BeginDate, $FinalDate) = array_values($results_date);
		}
		
		$filter_params[":FinalDate1"] = $FinalDate;
		//$filter_params[":FinalDate2"] = $FinalDate;
		$filter_params[":BeginDate1"] = $BeginDate;
		//$filter_params[":BeginDate2"] = $BeginDate;
				
		
		//Set parameters
		$params   =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
		//Set where condition
		$where    =   array("OrgID = :OrgID", "UserID = :UserID");
		//Get Application View
		$results  =   $IrecruitUsersObj->getUserInformation("Applicationview", $where, "", array($params));
		$row      =   $results['results'][0];
		
		if ($row ['Applicationview'] == "S") {
			$filter_params[":Distinction"] = 'P';
			$search_where[] = "Distinction = :Distinction";
		}
		
		//SearchQuery Information to insert
		$info         =   array("OrgID"=>$OrgID, "UserID"=>$USERID, "GuID"=>$ApplicantsSearchGuID, "Query"=>$main_search_query, "MainSearchWhere"=>implode(" AND ", $search_where), "SearchParams"=>json_encode($filter_params), "SortValue"=>$SortValue, "SortType"=>$SortType, "LastUpdated"=>"NOW()");
		//Set SearchQuery Information
		$on_update    =   " ON DUPLICATE KEY UPDATE Query = :UQuery, FilterSearchWhere = '', MainSearchWhere = :UMainSearchWhere, SearchParams = :USearchParams, SortValue = :USortValue, SortType = :USortType, LastUpdated = NOW()";
		//Update Information
		$update_info  =   array(":UQuery"=>$main_search_query, ":UMainSearchWhere"=>implode(" AND ", $search_where), ":USortValue"=>$SortValue, ":USearchParams"=>json_encode($filter_params), ":USortType"=>$SortType);
		//Insert Search Query
		$OrganizationsObj->insSearchQuery($info, $on_update, $update_info);
		
		
		//Applicant detail information
		$applicants_search_results[0]['ApplicationID']    =   $ApplicationID  =   $_REQUEST['ApplicationID'];
		$applicants_search_results[0]['RequestID']        =   $RequestID      =   $_REQUEST['RequestID'];
		// Set required columns
		$columns      =   "ApplicantSortName, MultiOrgID, RequestID, date_format(EntryDate,'%Y-%m-%d %H:%i EST') EntryDate, date_format(ReceivedDate,'%Y-%m-%d') ReceivedDate, ProcessOrder, DispositionCode, IFNULL(StatusEffectiveDate,'') StatusEffectiveDate, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2, Rating, LeadGenerator, LeadStatus";
		// Get JobApplications Detail Information based on ApplicationID, RequestID
		$Applicants   =   $ApplicationsObj->getJobApplicationsDetailInfo($columns, $OrgID, $ApplicationID, $RequestID);
	
		foreach($Applicants as $ApplicantKey=>$ApplicantValue) {
			$TemplateObj->{$ApplicantKey} = ${$ApplicantKey} = $ApplicantValue;
			$applicants_search_results[0][$ApplicantKey] = $ApplicantValue;
		}
		
		$TemplateObj->applicants_search_results = $applicants_search_results;
	
		//Get ProcessApplicants DropDown Information
		include IRECRUIT_DIR . 'applicants/ProcessApplicantsDropDown.inc';
			

	}
	else if ($_REQUEST ['process'] == 'Y') {
	
		//Set where condition
		$where_search_query   =   array("OrgID = :OrgID", "UserID = :UserID", "GuID = :GuID");
		//Set parameters
		$params_search_query  =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID, ":GuID"=>$ApplicantsSearchGuID);
		//Get SearchQuery Information
		$results_saved_query  =   $OrganizationsObj->getSearchQuery("Query, MainSearchWhere, FilterSearchWhere, SearchParams, SortValue, SortType", $where_search_query, '', array($params_search_query));

		if(!isset($results_saved_query['results'][0])) {
		    $results_saved_query['results'][0]['MainSearchWhere'] =   '';
		    $results_saved_query['results'][0]['Query']           =   '';
		}

		$TemplateObj->where_params        =   $where_params       =   $results_saved_query['results'][0]['MainSearchWhere'];
		$TemplateObj->main_search_query   =   $main_search_query  =   $results_saved_query['results'][0]['Query'];
		
		include IRECRUIT_DIR . 'applicants/ProcessApplicantsDropDown.inc';
	
		include IRECRUIT_DIR . 'applicants/BuildSearchQuery.inc';
	
		$TemplateObj->applicants_search_results = $applicants_search_results;
	}
}

$aps_index      =   0;
$SEARCHRESULT   =   '';
if (isset($applicants_search_results) 
    && is_array ( $applicants_search_results )) { 	
	foreach ( $applicants_search_results as $row ) {
		
		include IRECRUIT_DIR . 'applicants/BuildWeightedSearchQuery.inc';
		
		$ApplicantEmail   =   $ApplicantDetailsObj->getAnswer($OrgID, $row['ApplicationID'], 'email');
		$user_info        =   $UserPortalUsersObj->getUserDetailInfoByEmail("UserID", $ApplicantEmail);
		if($user_info['UserID'] > 0) {
		    $applicants_search_results[$aps_index]['UserPortalUserID']    =   $user_info['UserID'];
		}
		else {
		    $applicants_search_results[$aps_index]['UserPortalUserID']    =   '';
		}
		
		$applicants_search_results[$aps_index]['WeightedSearchScore'] =   $total;
		$applicants_search_results[$aps_index]['ApplicantEmail']      =   $ApplicantEmail;
		$applicants_search_results[$aps_index]['ApplicantSortName']   =   str_replace("'", "&#8217;", $applicants_search_results[$aps_index]['ApplicantSortName']);

		if($applicants_search_results[$aps_index]['LeadGenerator'] != "") {
		    $applicants_search_results[$aps_index]['LeadGenerator']   =   $applicants_search_results[$aps_index]['LeadGenerator'];
		}
		else {
		    $applicants_search_results[$aps_index]['LeadGenerator']   =   '';
		}

		######## Modified Code To Sort The Weighted Search Score
		include IRECRUIT_DIR . 'applicants/ViewApplicantSearchResults.inc';
		######## End Of Modified Code To Sort The Weighted Search Score
		
		
		if($twilio_user_info['TwilioSms'] == "Y") {
		    //Get applicant permission
		    $cell_phone_permission		=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $row ['ApplicationID'], 'cellphonepermission');
		    
		    //Get twilio number
		    $current_user_details		=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell");
		    $current_user_cell_number	=	str_replace(array("-", "(", ")", " "), "", $current_user_details['Cell']);
		    
		    //Add plus one before number
		    if(substr($user_details['Cell'], 0, 2) != "+1") {
		        $current_user_cell_number	=	"+1".$current_user_cell_number;
		    }
		    
		    //Get applicant cellphone
		    $applicant_cell_phone_info	=	G::Obj('ApplicantDetails')->getAnswer($OrgID, $row ['ApplicationID'], 'cellphone');
		    $applicant_cell_phone		=	json_decode($applicant_cell_phone_info, true);
		    
            $applicant_cell_number      =   "";
            if($applicant_cell_phone[0].$applicant_cell_phone[1].$applicant_cell_phone[2] != "") {
                $applicant_cell_number  =	"+1".$applicant_cell_phone[0].$applicant_cell_phone[1].$applicant_cell_phone[2];
		    }
		    
		    //Get twilio number
		    $user_details			=	G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Cell, FirstName, LastName");
		    $user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_details['Cell']);
		    
		    //Add plus one before number
		    if(substr($user_details['Cell'], 0, 2) != "+1") {
		        $user_cell_number	=	"+1".$user_cell_number;
		    }
		    
		    $archive_con_info       =   G::Obj('TwilioConversationsArchive')->getTwilioConversationsArchive($OrgID, $USERID, $applicant_cell_number, $user_cell_number);
		    
		    $applicants_search_results [$aps_index]["ArchiveTextMessages"]       =   "False";
		    
		    if($archive_con_info[0]["ResourceID"] != "") {
		        $applicants_search_results [$aps_index]["ArchiveTextMessages"]   =   "True";
		    }
		    
		}

		$get_checklist_process_data      =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $applicants_search_results[$aps_index]['ApplicationID'], $applicants_search_results[$aps_index]['RequestID']);

                $applicants_search_results [$aps_index]["Checklist"]       =   "False";

                if ($get_checklist_process_data['StatusMatch'] == "Y") {
                   $applicants_search_results [$aps_index]["Checklist"]       =   "True";
		}

		/* edge applicant interview tab control */
		$InterviewFormID =   G::Obj('Interview')->getActiveInterviewForm($OrgID,$applicants_search_results[$aps_index]['RequestID'],$applicants_search_results[$aps_index]['ApplicationID'],$USERID,$USERROLE,$permit);
		$applicants_search_results [$aps_index]["InterviewForms"]   =   "False";
		if ($InterviewFormID != "") {
		  $applicants_search_results [$aps_index]["InterviewForms"]   =   "True";
		}
		
		$SEARCHhit ++;
		if ($rowcolor == "#eeeeee") {
			$TemplateObj->rowcolor = $rowcolor = "#ffffff";
		} else {
			$TemplateObj->rowcolor = $rowcolor = "#eeeeee";
		}
		$aps_index++;
	} // end foreach
	
	$TemplateObj->applicants_search_results = $applicants_search_results;
	$TemplateObj->SEARCHiii = $SEARCHiii;
	$TemplateObj->SEARCHhit = $SEARCHhit;
	
	$SEARCHRESULTS .= $SEARCHRESULT;
	
	
	if ($cnt == 500) {
	
		$SEARCHRESULTS .= '<tr>';
		$SEARCHRESULTS .= '<td colspan="100%" align="center">';
		$SEARCHRESULTS .= '<p style="color:red">Search results have been limited.</p>';
		$SEARCHRESULTS .= '</td>';
		$SEARCHRESULTS .= '</tr>';
	}
	
	$TemplateObj->SEARCHRESULTS = $SEARCHRESULTS;	
	
	if(isset($applicants_search_results[0])) {
		//Applicant detail information
		$TemplateObj->ApplicationID   =   $ApplicationID  =   $applicants_search_results[0]['ApplicationID'];
		$TemplateObj->RequestID       =   $RequestID      =   $applicants_search_results[0]['RequestID'];
	}
}

//applicant and organization information
include_once IRECRUIT_DIR . 'organizations/OrganizationAndApplicantDetails.inc';
//schedule iterview template information
include_once IRECRUIT_DIR . 'appointments/ScheduleInterviewEmailTemplate.inc';

// Set required columns
$columns    =   "ApplicantSortName, RequestID, date_format(EntryDate,'%Y-%m-%d %H:%i EST') EntryDate, date_format(ReceivedDate,'%Y-%m-%d') ReceivedDate, ProcessOrder, DispositionCode, IFNULL(StatusEffectiveDate,'') StatusEffectiveDate, ApplicantConsiderationStatus1, ApplicantConsiderationStatus2, Rating, LeadGenerator, LeadStatus";
// Get JobApplications Detail Information based on ApplicationID, RequestID
$Applicants =   G::Obj('Applications')->getJobApplicationsDetailInfo($columns, $OrgID, $ApplicationID, $RequestID);

$TemplateObj->RequestID             =   $RequestID                = $Applicants ['RequestID'];
$TemplateObj->ApplicationDate       =   $ApplicationDate          = $Applicants ['EntryDate'];
$TemplateObj->ReceivedDate          =   $ReceivedDate             = $Applicants ['ReceivedDate'];
$TemplateObj->LastProcessOrder      =   $LastProcessOrder         = $Applicants ['ProcessOrder'];
$TemplateObj->LastDispositionCode   =   $LastDispositionCode      = $Applicants ['DispositionCode'];
$TemplateObj->LastEffDate           =   $LastEffDate              = $Applicants ['StatusEffectiveDate'];
$TemplateObj->Rating                =   $Rating                   = $Applicants ['Rating'];

$multiorgid_req                     =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

$TemplateObj->status                =   $status                   = G::Obj('ApplicantDetails')->getProcessOrderDescription ( $OrgID, $LastProcessOrder );
$TemplateObj->disposition           =   $disposition              = G::Obj('ApplicantDetails')->getDispositionCodeDescription ( $OrgID, $LastDispositionCode );
$TemplateObj->jobtitle              =   $jobtitle                 = $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
$TemplateObj->reqownersname         =   $reqownersname            = $RequisitionDetailsObj->getRequisitionOwnersName ( $OrgID, $RequestID );
$TemplateObj->linkedin              =   $linkedin                 = $ApplicantDetailsObj->getAnswer($OrgID, $ApplicationID, 'soc_linkedin');

//Get Applicant Data
if(isset($ApplicationID) && $ApplicationID != "") { 
    $APPDATA                            =   $ApplicantsObj->getAppData($OrgID, $ApplicationID);
    $format_address                     =   $AddressObj->formatAddress ( $OrgID, $APPDATA ["country"], $APPDATA ["city"], $APPDATA ["state"], $APPDATA['province'], $APPDATA ["zip"], $APPDATA ["county"] );
    
    $TemplateObj->ApplicantEmail        =   $ApplicantEmail           = $TemplateObj->Email = $Email = $APPDATA ['email'];
    $TemplateObj->ApplicantSortName     =   $Applicants['ApplicantSortName'];
    $TemplateObj->First                 =   $First                    = $APPDATA ['first'];
    $TemplateObj->Last                  =   $Last                     = $APPDATA ['last'];
    $TemplateObj->AppDetailAddress      =   $AppDetailAddress         = $APPDATA ['address'];
    $TemplateObj->AppDetailAddress2     =   $AppDetailAddress2        = $APPDATA ['address2'];
    $TemplateObj->AppDetailCounty       =   $AppDetailCounty          = $APPDATA ['county'];
    $TemplateObj->AppDetailCity         =   $AppDetailCity            = $APPDATA ['city'];
    $TemplateObj->AppDetailState        =   $AppDetailState           = $APPDATA ['state'];
    $TemplateObj->AppDetailZip          =   $AppDetailZip             = $APPDATA ['zip'];
    $TemplateObj->AppDetailCountry      =   $AppDetailCountry         = $APPDATA ['country'];
    $TemplateObj->FormattedAddress      =   $FormattedAddress         = $format_address;
    $TemplateObj->HomePhone1            =   $HomePhone1               = isset($APPDATA ['homephone1']) ? $APPDATA ['homephone1'] : '';
    $TemplateObj->HomePhone2            =   $HomePhone2               = isset($APPDATA ['homephone2']) ? $APPDATA ['homephone2'] : '';
    $TemplateObj->HomePhone3            =   $HomePhone3               = isset($APPDATA ['homephone3']) ? $APPDATA ['homephone3'] : '';
    $TemplateObj->CellPhone1            =   $CellPhone1               = isset($APPDATA ['cellphone1']) ? $APPDATA ['cellphone1'] : '';
    $TemplateObj->CellPhone2            =   $CellPhone2               = isset($APPDATA ['cellphone2']) ? $APPDATA ['cellphone2'] : '';
    $TemplateObj->CellPhone3            =   $CellPhone3               = isset($APPDATA ['cellphone3']) ? $APPDATA ['cellphone3'] : '';
    $TemplateObj->BusPhone1             =   $BusPhone1                = isset($APPDATA ['busphone1']) ? $APPDATA ['busphone1'] : '';
    $TemplateObj->BusPhone2             =   $BusPhone2                = isset($APPDATA ['busphone2']) ? $APPDATA ['busphone2'] : '';
    $TemplateObj->BusPhone3             =   $BusPhone3                = isset($APPDATA ['busphone3']) ? $APPDATA ['busphone3'] : '';
    $TemplateObj->BusPhoneExt           =   $BusPhoneExt              = isset($APPDATA ['busphoneext']) ? $APPDATA ['busphoneext'] : '';
    $TemplateObj->FormID                =   $FormID                   = $APPDATA ['FormID'];
    $TemplateObj->source                =   $source                   = $APPDATA ['source'];
    $TemplateObj->informed              =   $informed                 = $APPDATA ['informed'];
    $TemplateObj->tab_action            =   $tab_action               = isset($_REQUEST['tab_action']) ? $_REQUEST['tab_action'] : 'applicant-history';
}

if ($permit ['Applicants_Affirmative_Action'] == 1) {

    if (isset($source) && $source != $InternalCode) {

		// Set where condition
		$where_info   =   array ("OrgID = :OrgID", "FormID = :FormID", "Active = 'Y'");
		// Set parameters information
		$params_info  =   array (":OrgID" => $OrgID, ":FormID"=>$FormID);
		// Get FormQuestions Information
		$resultsIN    =   G::Obj('FormQuestions')->getFormQuestionsInformation( "QuestionID", $where_info, "", array ($params_info) );

		if ($resultsIN ['count'] > 0) {
		    
			// Set parameters
			$params_app  =   array (
                					":OrgID"           =>  $OrgID,
                					":ApplicationID"   =>  $ApplicationID,
                					":OrgIDAA"         =>  $OrgID
                                );
			// Set where condition
			$where_app   =   array (
                                    "OrgID             =   :OrgID",
                                    "ApplicationID     =   :ApplicationID",
                                    "QuestionID IN (SELECT QuestionID FROM FormQuestions WHERE OrgID = :OrgIDAA and FormID = 'STANDARD' and Active = 'Y')"
                                );
			// Get ApplicantData Information
			$resultsIN   =   G::Obj('Applicants')->getApplicantDataInfo ( "*", $where_app, '', '', array ($params_app) );
			$hitIN       =   $resultsIN ['count'];
				
			if ($hitIN == 0) {
				$typeIN = "Enter";
			} else {
				$typeIN = "Edit";
			}
				
			$link = IRECRUIT_HOME . "applicants/aaEdit.php";
			$link .= '?ApplicationID=' . $ApplicationID;
			$link .= '&RequestID=' . $RequestID;
				
			$affirmative_action_link = "javascript:void(0);";
			if ($typeIN == "Edit") {
				$affirmative_action_link = "javascript:window.open('" . IRECRUIT_HOME . "applicants/document_view.php?OrgID=" . $OrgID . "&ApplicationID=" . $ApplicationID . "&RequestID=" . $RequestID;
				if ($AccessCode != "") {
					$affirmative_action_link .= "&k=" . $AccessCode;
				}
				$affirmative_action_link .= "&form=aa";
				$affirmative_action_link .= "','_blank','location=yes,toolbar=no,height=600,width=820,status=no,menubar=yes,resizable=yes,scrollbars=yes');";
			}
				
		} // end if affirmative action questions
	} // end if FormID not internal
	 
} // end permit affirmative action

echo $TemplateObj->displayIrecruitTemplate ( 'views/applicants/ApplicantsSearch' );
?>
