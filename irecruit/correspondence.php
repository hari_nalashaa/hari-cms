<?php
require_once 'Configuration.inc';

include IRECRUIT_DIR . 'correspondence/CorrespondencePageTitle.inc';

//Set page title
$TemplateObj->title	= $title;

$scripts_header[] = "js/irec_Display.js";
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";

//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

//Page scripts
$corres_scripts_footer[]            = "js/selectbox.js";
$TemplateObj->page_scripts_footer   = $corres_scripts_footer;

$TemplateObj->UpdateID      = $UpdateID     = isset($_REQUEST['UpdateID']) ? $_REQUEST['UpdateID'] : '';
$TemplateObj->item          = $item         = isset($_REQUEST['item']) ? $_REQUEST['item'] : '';

$TemplateObj->movepattern1  = $movepattern1 = isset($_REQUEST['movepattern1']) ? $_REQUEST['movepattern1'] : '';
$TemplateObj->movepattern2  = $movepattern2 = isset($_REQUEST['movepattern2']) ? $_REQUEST['movepattern2'] : '';

$TemplateObj->policies      = $policies     = isset($_REQUEST['policies']) ? $_REQUEST['policies'] : '';
$TemplateObj->forms         = $forms        = isset($_REQUEST['forms']) ? $_REQUEST['forms'] : '';
$TemplateObj->attachments   = $attachments  = isset($_REQUEST['attachments']) ? $_REQUEST['attachments'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/correspondence/Correspondence');
?>