<?php
require_once 'Configuration.inc';

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
if ($action == 'formquestions') $action = "onboardquestions";

include_once 'administration/AdministrationActionTitles.inc'; 

if (isset($_REQUEST ['form']) && $_REQUEST ['form'] != '' && $action == 'onboardquestions' && $_REQUEST ['new'] == 'add') {

	if($action == 'formquestions') {
	    // Set Columns
	    $columns       =   array ('MAX(QuestionOrder) as QuestionOrder');
	    // set condition
	    $where         =   array ("OrgID = :OrgID", "FormID = :FormID");
	    // set parameters
	    $params        =   array (":OrgID" => $OrgID, ":FormID" => $_REQUEST ['form']);
	    // Get FormQuestionsInformation
	    $results       =   $FormQuestionsObj->getFormQuestionsInformation ( $columns, $where, '', array ($params) );
	}
	else if($action == 'onboardquestions') {
	    // Set Columns
	    $columns       =   array ('MAX(QuestionOrder) as QuestionOrder');
	    // set condition
	    $where         =   array ("OrgID = :OrgID", "OnboardFormID = :OnboardFormID");
	    // set parameters
	    $params        =   array (":OrgID" => $OrgID, ":OnboardFormID" => $_REQUEST ['form']);
	    // Get FormQuestionsInformation
	    $results       =   $OnboardQuestionsObj->getOnboardQuestionsInformation ( $columns, $where, '', array ($params) );
	}
	
	$row_que_order =   $results ['results'] [0];

	$QuestionOrder =   $row_que_order ['QuestionOrder'] + 1;

	$QuestionID    =   "CUST" . uniqid ();

	// Set Insert Information
	$onboard_que_info = array (
							"OrgID"          =>  $OrgID,
							"OnboardFormID"  =>  $_REQUEST ['form'],
							"QuestionID"     =>  $QuestionID,
							"Question"       =>  'New Question Added.',
							"QuestionTypeID" =>  '6',
							"size"           =>  '0',
							"maxlength"      =>  '0',
							"rows"           =>  '2',
							"cols"           =>  '2',
							"wrap"           =>  '',
							"value"          =>  '',
							"defaultValue"   =>  '',
							"Active"         =>  'Y',
							"QuestionOrder"  =>  $QuestionOrder,
							"Required"       =>  '',
							"Validate"       =>  ''
						);

	// Insert Form Questions
	$result_custom_question = $FormQuestionsObj->insFormQuestions ( 'OnboardQuestions', $onboard_que_info );

	$redirect_url  =   "Location:administration.php?form=" . $_REQUEST ['form'] . "&action=".$action;   
	
	if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] != "") {
	    $redirect_url  .=  "&OnboardSource=".$_REQUEST['OnboardSource'];
	}
	
	header ( $redirect_url );
	exit ();
}

// EDIT A FORM
if (isset ( $_REQUEST ['form'] ) && $_REQUEST ['form'] != "" && isset ( $_REQUEST ['delete'] ) && $_REQUEST ['delete'] != "" && $_REQUEST['subactiondelete'] == 'yes') {
	
	// Delete Form Questions
	$where         =   array ("OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
	// set parameters
	$params        =   array (":OnboardFormID" => $_REQUEST ['form'], ":QuestionID" => $_REQUEST ['delete']);
	// Delete FormQuestions Information
	$res_form_ques =   $FormQuestionsObj->delQuestionsInfo ( 'OnboardQuestions', $where, array ($params) );
	$redirect_url  =   "Location:administration.php?action=".$action;
	
	if(isset($_REQUEST['OnboardSource']) && $_REQUEST['OnboardSource'] != "") {
	    $redirect_url  .=  "&OnboardSource=".$_REQUEST['OnboardSource'];
	}
	
	header ( $redirect_url );
	exit ();
}

//Include configuration related javascript in header
if($action != 'applicantlabels') {
	$scripts_header[] = "tiny_mce/tinymce.min.js";
	$scripts_header[] = "js/irec_Textareas.js";
}
$scripts_header[] = "js/loadAJAX.js";

//Load Js Colors
if($action == 'orgcolors') $scripts_header[] = "jscolor/jscolor.js";
//Load Purchases
if($action == 'purchases') {
    $scripts_header[] = "js/purchases.js";
    $scripts_header[] = "js/requisition.js";
    $scripts_header[] = "js/requisitions-search.js";
    $scripts_header[] = "js/job-board-requisitions-search.js";
}

if($modal == "Y")
{
	//Assign add applicant datepicker variables
	$script_vars_header[]	= 'var datepicker_ids = "'.$lst.'";';
	$script_vars_header[]	= 'var date_format = "mm/dd/yy";';
	
	$TemplateObj->script_vars_header = $script_vars_header;
	
	if(isset($lst)) {
		//Declare datepicker function
		$scripts_header_inline[]	= 'function again_cal() { date_picker(datepicker_ids, date_format); }';
		$TemplateObj->scripts_header_inline = $scripts_header_inline;
	}
}
else {
    //Assign add applicant datepicker variables
    $script_vars_footer[]	= 'var datepicker_ids = "#TimeToFillFromDate, #TimeToFillToDate, #txtFromDate, #txtToDate, #txtPurchaseFromDate, #txtPurchaseToDate";';
    $script_vars_footer[]	= 'var date_format = "mm/dd/yy";';

    $TemplateObj->scripts_vars_footer = $script_vars_footer;
}

//Load Graphs Related Scripts, Only for this page
$scripts_footer[] = "js/irec_Display.js";

if($action == 'onboardquestions') {
	$scripts_footer[] = "js/forms.js";
}
//Set these scripts to footer scripts objects
$TemplateObj->page_scripts_footer = $scripts_footer;
//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

echo $TemplateObj->displayIrecruitTemplate('views/administration/Administration');
?>