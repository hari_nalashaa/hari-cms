<?php 
//Turn off errror reporting in production
error_reporting(0);
require_once '../irecruitdb.inc';
require_once IRECRUIT_DIR.'linkedin/ClassLinkedIn.inc';
$LinkedInIrecruitObj = new LinkedInIrecruit();

include IRECRUIT_DIR.'linkedin/LinkedInCheckState.inc';

if($LState == false) {
	
	$siteurl  = "";
	$siteurl  = ($_SERVER['HTTPS'] == "on") ? "https://" : "http://";
	$siteurl .= $_SERVER['HTTP_HOST'];
	
	if(preg_match ( '/surya/', $_SERVER ["SCRIPT_NAME"] ) && $_SERVER ['HTTP_HOST'] == 'dev.irecruit-us.com') {
	    $linkedinaccess = "/surya/irecruit/linkedin/LinkedInProfile.php";
	}
	else if($_SERVER['HTTP_HOST'] == "dev.irecruit-us.com" || $_SERVER['HTTP_HOST'] == "quality.irecruit-us.com") {
		$linkedinaccess = "/irecruit/linkedin/LinkedInProfile.php";
	}
	else {
		$linkedinaccess = "/linkedin/LinkedInProfile.php";
	}
	
	/*
	if (FROM_SRC == "PUBLIC") {
		$linkedinaccess = PUBLIC_HOME . "linkedin/LinkedInProfile.php";
	} else if (FROM_SRC == "USERPORTAL") {
		$linkedinaccess = USERPORTAL_HOME . "linkedin/LinkedInProfile.php";
	} else {
		$linkedinaccess = IRECRUIT_HOME . "linkedin/LinkedInProfile.php";
	}
	*/
	header("Location:".$linkedinaccess);
	exit;
}
