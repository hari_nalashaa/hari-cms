<?php
$LState = false;
// OAuth 2 Control Flow
if (isset ( $_GET ['error'] )) {
	$LState = true;
	// LinkedIn returned an error
	echo $_GET ['error'] . ': ' . $_GET ['error_description'];
	exit ();
} else if(time () < $_SESSION['I']['expires_at'] && $_SESSION['I']['access_token'] != "") {
    $LState = true;
} else if (isset ( $_GET ['code'] )) {
	$LState = true;
	// User authorized your application
	if ($_SESSION['I']['state'] == $_GET ['state']) {
		// Get token so you can make API calls
		$LinkedInIrecruitObj->getAccessToken ();
	} else {
		// CSRF attack? Or did you mix up your states? //Below echo is a custom message
		echo "Sorry your token doesn't matched.";
		exit ();
	}
} else {
	if ((empty ( $_SESSION['I']['expires_at'] )) || (time () > $_SESSION['I']['expires_at'])) {
		$LState = true;
		// Token has expired, clear the state,
		// Please don't unset global session array
		// Unset only individual sessions
		$_SESSION['I']['expires_at'] = "";
		$_SESSION['I']['access_token'] = "";
		$_SESSION['I']['state'] = "";
	}
	if (empty ( $_SESSION['I']['access_token'] )) {
		$LState = true;
		// Start authorization process
		$LinkedInIrecruitObj->getAuthorizationCode ();
	}
}