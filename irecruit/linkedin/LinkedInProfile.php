<?php
// Turn off errror reporting in production
require_once '../irecruitdb.inc';
require_once IRECRUIT_DIR . 'linkedin/ClassLinkedIn.inc';
$LinkedInIrecruitObj = new LinkedInIrecruit ();

include IRECRUIT_DIR . 'linkedin/LinkedInCheckState.inc';

//Get User Profile
$user_profile       =   $LinkedInIrecruitObj->fetch ( 'GET', 'me' );

//Get EmailAddress
$required_params    =   array('q'=>'members', 'projection'=>'(elements*(handle~))');
$email_information  =   $LinkedInIrecruitObj->fetch ( 'GET', 'emailAddress', $required_params );

//Set Profile Information
$profile_info['firstName']      =   $user_profile['localizedFirstName'];
$profile_info['lastName']       =   $user_profile['localizedLastName'];
$profile_info['emailAddress']   =   $email_information['elements'][0]["handle~"]['emailAddress'];

$_SESSION['I']['LIEDUCATION']   =   $educationdetails;
$_SESSION['I']['LIUSER']        =   $profile_info;
$_SESSION['I']['LIUSERPROFILE'] =   $user_profile;

header ( "Location:" . $_SESSION['I']['LIURL'] . "&secheck=no" );
exit ();
?>