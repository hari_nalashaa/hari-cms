<?php
class LinkedInIrecruit {
	
	// Change these with the registered application details
	var $API_KEY = '';
	var $API_SECRET = '';
	
	// You must pre-register your redirect_uri at https://www.linkedin.com/secure/developer
	var $REDIRECT_URI = '/linkedin/LinkedInProfile.php';
	
	// Have to request the user for contact info, email address and full profile
	var $SCOPE = 'r_liteprofile r_emailaddress';//r_liteprofile
	
	function __construct() {
		/*
		if (FROM_SRC == "PUBLIC") {
			$linkedinaccess = PUBLIC_HOME . "linkedin/LinkedInProfile.php";
		} else if (FROM_SRC == "USERPORTAL") {
			$linkedinaccess = USERPORTAL_HOME . "linkedin/LinkedInProfile.php";
		} else {
			$linkedinaccess = IRECRUIT_HOME . "linkedin/LinkedInProfile.php";
		}
		
		$this->REDIRECT_URI = $linkedinaccess;
		*/
		$siteurl  = "";
		$siteurl  = ($_SERVER['HTTPS'] == "on") ? "https://" : "http://";
		$siteurl .= $_SERVER['HTTP_HOST'];
		
		if(preg_match ( '/surya/', $_SERVER ["SCRIPT_NAME"] ) && $_SERVER ['HTTP_HOST'] == 'dev.irecruit-us.com') {
		    $linkedinaccess = "/surya/irecruit/linkedin/LinkedInProfile.php";
		}
		else if($_SERVER['HTTP_HOST'] == "dev.irecruit-us.com" || $_SERVER['HTTP_HOST'] == "quality.irecruit-us.com") {
			$linkedinaccess = "/irecruit/linkedin/LinkedInProfile.php";
		}
		else {
			$linkedinaccess = "/linkedin/LinkedInProfile.php";
		}
		$this->REDIRECT_URI = $siteurl.$linkedinaccess;
		
		// $this->REDIRECT_URI = "https://www.irecruit-us.com/linkedin/LinkedInProfile.php";
		
		if (preg_match ( '/surya/', $_SERVER ["SCRIPT_NAME"] ) && $_SERVER ['HTTP_HOST'] == 'dev.irecruit-us.com') {
		    // Change these with the registered application details
		    $this->API_KEY = '86jrb668w5ln7f';
		    $this->API_SECRET = '9wc0uaUsnlnFwf2n';
		} else if ($_SERVER ['HTTP_HOST'] == 'dev.irecruit-us.com') {
			// Change these with the registered application details
			$this->API_KEY = '75ic0kqu3rqjbw';
			$this->API_SECRET = 'mxgzJoU8y692yYBp';
		} else if ($_SERVER ['HTTP_HOST'] == 'apps.irecruit-us.com') {
			// Change these with the registered application details
			$this->API_KEY = '78rpvumsfilg54';
			$this->API_SECRET = 'PW2Fd4FA28vYY5sR';
		} else if ($_SERVER ['HTTP_HOST'] == 'quality.irecruit-us.com') {
			// Change these with the registered application details
			$this->API_KEY = '78anly3bqw3yb6';
			$this->API_SECRET = '7fbYdhZMWLeWuZYp';
		} else if ($_SERVER ['HTTP_HOST'] == 'www.irecruit-us.com') {
			// Change these with the registered application details
			$this->API_KEY = '75zwc8pfxrv86y';
			$this->API_SECRET = 'lPeFg5jHHSWWJXWI';
		} else if ($_SERVER ['HTTP_HOST'] == 'www.myirecruit.com' || $_SERVER ['HTTP_HOST'] == 'myirecruit.com') {
			// Change these with the registered application details
			$this->API_KEY = '75myhuzg9w00np';
			$this->API_SECRET = 'qiQdOLQO41aLPfDf';
		} else {
			// Change these with the registered application details
			$this->API_KEY = '75x2z7ykanfdyn';
			$this->API_SECRET = 'Hua77IcTb9KvFaL8';
		}
		
		// Have to request the user for contact info, email address and full profile
		$this->SCOPE = 'r_liteprofile r_emailaddress';//
	}
	
	/**
	 * @method     getAuthorizationCode
	 * @tutorial   LinkedIn Methods To Authenticate User, UserToken, Retrieve User Profile
	 */
	function getAuthorizationCode() {
		$state = rand () . rand () . crypt ( time () );
		$params = array (
				'response_type' =>  'code',
				'client_id'     =>  $this->API_KEY,
				'scope'         =>  $this->SCOPE,
				'state'         =>  $state, // unique long string
				'redirect_uri'  =>  $this->REDIRECT_URI 
		);
		
		// Authentication request
		$url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query ( $params );
		
		// Needed to identify request when it returns to us
		$_SESSION['I']['state'] = $state;
		
		// Redirect user to authenticate
		header ( "Location: $url" );
		exit ();
	}
	
	/**
	 * @method getAccessToken
	 * @return boolean
	 */
	function getAccessToken() {
		$params = array (
				'grant_type'    =>  'authorization_code',
				'client_id'     =>  $this->API_KEY,
				'client_secret' =>  $this->API_SECRET,
				'code'          =>  $_GET ['code'],
				'redirect_uri'  =>  $this->REDIRECT_URI 
		);
		
		// Access Token request
		$url = 'https://www.linkedin.com/oauth/v2/accessToken?' . http_build_query ( $params );
		// Retrieve access token information
		$response = self::get_data ( $url );
		
		// Native PHP object, please
		$token = json_decode ( $response );
		
		// Store access token and expiration time
		$_SESSION['I']['access_token'] = $token->access_token; // guard this!
		$_SESSION['I']['expires_in'] = $token->expires_in; // relative time (in seconds)
		$_SESSION['I']['expires_at'] = time () + $_SESSION['I']['expires_in']; // absolute time
		
		return true;
	}
	
	/**
	 * @method fetch
	 * @param  string $method
	 * @param  string $resource
	 * @param  string $body
	 */
    function fetch($method, $resource = '', $resource_params = array()) {
     
        $headers    =   array (
                            'x-li-format'   =>  'json',
                            'X-RestLi-Protocol-Version' =>  '2.0.0'
                        );
    	
        $params     =   array (
                			'oauth2_access_token'   =>  $_SESSION['I']['access_token']
                        );
         
        $params     =   array_merge($resource_params, $params);
     	
        // Need to use HTTPS
        $url = 'https://api.linkedin.com/v2/'.$resource;
     
        // Append query parameters (if there are any)
        if (count($params)) { $url .= '?' . http_build_query($params); } 
     
        // Get Data
        $response = self::get_data ( $url, $headers );
     
        // Native PHP object, please
        return json_decode($response, true);
    }
		
	/**
	 * @method get_data
	 * @param  string $url
	 * @param  string $headers
	 * @return string
	 */
	function get_data($url, $headers = array()) {
		$ch       =   curl_init ();
		$timeout  =   5;
		curl_setopt ( $ch, CURLOPT_URL, $url );
		
		if (count ( $headers ) > 0) {
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		}
		
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, $timeout );
		
		$data = curl_exec ( $ch );
		
		if (curl_errno ( $ch )) {
			echo 'Curl error: ' . curl_error ( $ch );
		}
		
		curl_close ( $ch );
		
		return $data;
	}
}
?>