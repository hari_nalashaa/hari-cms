<?php
// You'll probably use a database
require_once '../irecruitdb.inc';

// Change these
define('API_KEY',      '86jrb668w5ln7f');
define('API_SECRET',   '9wc0uaUsnlnFwf2n');
// You must pre-register your redirect_uri at https://www.linkedin.com/secure/developer
define('REDIRECT_URI', 'https://dev.irecruit-us.com/surya/irecruit/linkedin/linkedin.php');
define('SCOPE',        'r_liteprofile r_emailaddress');
 
 
// OAuth 2 Control Flow
if (isset($_GET['error'])) {
    // LinkedIn returned an error
    print $_GET['error'] . ': ' . $_GET['error_description'];
    exit;
} else if (isset($_GET['code'])) {
    // User authorized your application
    if ($_SESSION['I']['state'] == $_GET['state']) {
        // Get token so you can make API calls
        //getAccessToken();
    } else {
        // CSRF attack? Or did you mix up your states?
        //exit;
    }
} else { 
    if ((empty($_SESSION['I']['expires_at'])) || (time() > $_SESSION['I']['expires_at'])) {
        // Token has expired, clear the state
        $_SESSION['I'] = array();
    }
    if (empty($_SESSION['I']['access_token'])) {
        // Start authorization process
        getAuthorizationCode();
    }
}
 
echo "Session: <pre>".print_r($_SESSION, true)."</pre><br>";
// Congratulations! You have a valid token. Now fetch your profile 
$user = fetch('GET', 'me');
echo "User: <pre>".print_r($user, true)."</pre>";
exit;
// Retrieve access token information
function get_data($url, $headers = array()) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	if(count($headers) > 0) {
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	
	if(curl_errno($ch))
	{
		echo 'Curl error: ' . curl_error($ch);
	}
	curl_close($ch);
	return $data;
}

// Native PHP object, please
function getAuthorizationCode() {
    $params = array(
        'response_type' =>  'code',
        'client_id'     =>  API_KEY,
        'scope'         =>  SCOPE,
        'state'         =>  uniqid('', true), // unique long string
        'redirect_uri'  =>  REDIRECT_URI,
    );
 
    // Authentication request
    $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);
   
    // Needed to identify request when it returns to us
    $_SESSION['I']['state'] = $params['state'];
 
    // Redirect user to authenticate
    header("Location: $url");
    exit;
}
     
function getAccessToken() {
    
    $params = array(
        'grant_type'    =>  'authorization_code',
        'client_id'     =>  API_KEY,
        'client_secret' =>  API_SECRET,
        'code'          =>  $_GET['code'],
        'redirect_uri'  =>  REDIRECT_URI,
    );
    
    // Access Token request
    $url = 'https://www.linkedin.com/oauth/v2/accessToken?' . http_build_query($params);
    // Retrieve access token information    
    $response = get_data($url);
    
    // Native PHP object, please
 	$token    = json_decode($response);
    
 	echo "Token: <pre>";
 	print_r($token);
 	echo "</pre>";
 	
    // Store access token and expiration time
    $_SESSION['I']['access_token'] = $token->access_token; // guard this! 
    $_SESSION['I']['expires_in']   = $token->expires_in; // relative time (in seconds)
    $_SESSION['I']['expires_at']   = time() + $_SESSION['I']['expires_in']; // absolute time
     
    return true;
}
 
function fetch($method, $resource = '', $resource_params = array()) {
 
    $headers    =   array (
                        'x-li-format'   =>  'json',
                        'X-RestLi-Protocol-Version' =>  '2.0.0'
                    );
	
    $params     =   array (
            			'oauth2_access_token'   =>  $_SESSION['I']['access_token']
                    );
     
    $params     =   array_merge($resource_params, $params);
	
    echo "Headers: <pre>";
 	print_r($headers);
 	echo "</pre>";
 	
    // Need to use HTTPS
    $url = 'https://api.linkedin.com/v2/'.$resource;

    // Append query parameters (if there are any)
    if (count($params)) { $url .= '?' . http_build_query($params); } 
 
    //Get Data
    $response = get_data ( $url, $headers );
 
    // Native PHP object, please
    return json_decode($response);
}
