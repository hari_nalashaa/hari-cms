<?php
require_once 'Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Deleted the temporary data based on this id on section change of web forms
if(isset($_POST['UnsetSession']) && $_POST['UnsetSession'] == 'Yes') {
    G::Obj('WeightedSearch')->delWeightSaveSearch($_GET['uwkey']);
}

if(isset($_GET['subaction']) 
    && $_GET['subaction'] == 'delete'
    && isset($_GET['uwkey'])
    && $_GET['uwkey'] != "") {
    //Delete question related records
    G::Obj('WeightedSearch')->delWeightSaveSearchQuestion($_GET['uwkey'], $_GET['QuestionID']);
    header("Location:" . IRECRUIT_HOME . "applicants.php?action=weightedsearch&menu=10&uwkey=".$_GET['uwkey']."&subaction=edit");
    exit;
}

//Redirect to weighted search onload to pass the randid
if(isset($_REQUEST['wlink_from']) && $_REQUEST['wlink_from'] == 'leftnav') {
    $wrand_one_var = $GenericLibraryObj->getGuIDV4();
    $wrand_complete_var = strtoupper ( uniqid () . rand ( 5, 15 ) . rand ( 5, 15 ) . time () . $wrand_one_var );
    $wrand_complete_var = str_replace("-", "", $wrand_complete_var);
    header("Location:" . IRECRUIT_HOME . "applicants.php?action=weightedsearch&menu=10&uwkey=".$wrand_complete_var."&subaction=add");
    exit;
}

//Set permit data
$TemplateObj->permit = $permit;
//Set action variable
$TemplateObj->action = $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
//Set Appointment Action
$TemplateObj->appointmentaction = $appointmentaction = isset($_REQUEST['appointmentaction']) ? $_REQUEST['appointmentaction'] : '';

require_once IRECRUIT_DIR . 'applicants/ApplicantsAboveHeader.inc';

//Set page title
$TemplateObj->title                     = $title;
$TemplateObj->process                   = $process                  = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';

//Weighted Search
$TemplateObj->subaction                 = $subaction                = isset($_REQUEST['subaction']) ? $_REQUEST['subaction'] : '';
$TemplateObj->uwkey                     = $uwkey                    = isset($_REQUEST['uwkey']) ? $_REQUEST['uwkey'] : '';
$TemplateObj->wkey                      = $wkey                     = isset($_REQUEST['wkey']) ? $_REQUEST['wkey'] : '';

//Download List
/*
$TemplateObj->DownloadProcessDate_From  = $DownloadProcessDate_From = isset($_REQUEST['DownloadProcessDate_From']) ? $_REQUEST['DownloadProcessDate_From'] : '';
$TemplateObj->DownloadProcessDate_To    = $DownloadProcessDate_To   = isset($_REQUEST['DownloadProcessDate_To']) ? $_REQUEST['DownloadProcessDate_To'] : '';
 */
$TemplateObj->appid                     = $appid                    = isset($_REQUEST['appid']) ? $_REQUEST['appid'] : '';
$TemplateObj->all                       = $all                      = isset($_REQUEST['all']) ? $_REQUEST['all'] : '';

//Contact Applicant Parameters
$TemplateObj->RequisitionID             = $RequisitionID            = isset($_REQUEST['RequisitionID']) ? $_REQUEST['RequisitionID'] : '';
$TemplateObj->DevApps                   = $DevApps                  = isset($_REQUEST['DevApps']) ? $_REQUEST['DevApps'] : '';
$TemplateObj->FormID                    = $FormID                   = isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';
$TemplateObj->policies                  = $policies                 = isset($_REQUEST['policies']) ? $_REQUEST['policies'] : '';
$TemplateObj->forms                     = $forms                    = isset($_REQUEST['forms']) ? $_REQUEST['forms'] : '';

//Set contact parameters information
$TemplateObj->package                   = $package                  = isset($_REQUEST['package']) ? $_REQUEST['package'] : '';
$TemplateObj->multi                     = $multi                    = isset($_REQUEST['multi']) ? $_REQUEST['multi'] : '';
$TemplateObj->multipleapps              = $multipleapps             = isset($_REQUEST['multipleapps']) ? $_REQUEST['multipleapps'] : '';
$TemplateObj->subject                   = $subject                  = isset($_REQUEST['subject']) ? $_REQUEST['subject'] : '';
$TemplateObj->From                      = $From                     = isset($_REQUEST['From']) ? $_REQUEST['From'] : '';
$TemplateObj->BCC                       = $BCC                      = isset($_REQUEST['BCC']) ? $_REQUEST['BCC'] : '';
$TemplateObj->body                      = $body                     = isset($_REQUEST['body']) ? $_REQUEST['body'] : '';
$TemplateObj->movepattern1              = $movepattern1             = isset($_REQUEST['movepattern1']) ? $_REQUEST['movepattern1'] : '';
$TemplateObj->movepattern2              = $movepattern2             = isset($_REQUEST['movepattern2']) ? $_REQUEST['movepattern2'] : '';
$TemplateObj->type                      = $type                     = isset($_REQUEST['type']) ? $_REQUEST['type'] : '';
$TemplateObj->UpdateID                  = $UpdateID                 = isset($_REQUEST['UpdateID']) ? $_REQUEST['UpdateID'] : '';

if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "")
	$TemplateObj->OrgID = $OrgID = isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';

//Assign Requisition
$TemplateObj->multipleapps              = $multipleapps             = isset($_REQUEST['multipleapps']) ? $_REQUEST['multipleapps'] : '';
$TemplateObj->multi                     = $multi                    = isset($_REQUEST['multi']) ? $_REQUEST['multi'] : '';
$TemplateObj->Distinction               = $Distinction              = isset($_REQUEST['Distinction']) ? $_REQUEST['Distinction'] : '';
$TemplateObj->movepattern               = $movepattern              = isset($_REQUEST['movepattern']) ? $_REQUEST['movepattern'] : '';

//Common parameters
$TemplateObj->ApplicationID             = $ApplicationID            = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID                 = $RequestID                = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

//Comment Request parameters
$TemplateObj->ProcessOrder              = $ProcessOrder             = isset($_REQUEST['ProcessOrder']) ? $_REQUEST['ProcessOrder'] : '';
$TemplateObj->DispositionCode           = $DispositionCode          = isset($_REQUEST['DispositionCode']) ? $_REQUEST['DispositionCode'] : '';
$TemplateObj->StatusEffectiveDate       = $StatusEffectiveDate      = isset($_REQUEST['StatusEffectiveDate']) ? $_REQUEST['StatusEffectiveDate'] : '';
$TemplateObj->Comment                   = $Comment                  = isset($_REQUEST['Comment']) ? $_REQUEST['Comment'] : '';

//Search Request Parameters
$TemplateObj->criteria                  = $criteria                 = isset($_REQUEST['criteria']) ? $_REQUEST['criteria'] : '';
$TemplateObj->MultiOrgID				= $MultiOrgID				= isset($_REQUEST['MultiOrgID']) ? $_REQUEST['MultiOrgID'] : '';
$TemplateObj->SearchWords               = $SearchWords              = isset($_REQUEST['SearchWords']) ? $_REQUEST['SearchWords'] : '';
$TemplateObj->refinereq                 = $refinereq                = isset($_REQUEST['refinereq']) ? $_REQUEST['refinereq'] : '';
$TemplateObj->Active                    = $Active                   = isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->ProcessOrder              = $ProcessOrder             = isset($_REQUEST['ProcessOrder']) ? $_REQUEST['ProcessOrder'] : '';
$TemplateObj->Code                      = $Code                     = isset($_REQUEST['Code']) ? $_REQUEST['Code'] : '';
$TemplateObj->ApplicationDate_From      = $ApplicationDate_From     = isset($_REQUEST['ApplicationDate_From']) ? $_REQUEST['ApplicationDate_From'] : '';
$TemplateObj->ApplicationDate_To        = $ApplicationDate_To       = isset($_REQUEST['ApplicationDate_To']) ? $_REQUEST['ApplicationDate_To'] : '';
$TemplateObj->WSList                    = $WSList                   = isset($_REQUEST['WSList']) ? $_REQUEST['WSList'] : '';
$TemplateObj->LimitProcessed            = $LimitProcessed           = isset($_REQUEST['LimitProcessed']) ? $_REQUEST['LimitProcessed'] : '';
$TemplateObj->LimitReceivedDate         = $LimitReceivedDate        = isset($_REQUEST['LimitReceivedDate']) ? $_REQUEST['LimitReceivedDate'] : '';
$TemplateObj->SearchName                = $SearchName               = isset($_REQUEST['SearchName']) ? $_REQUEST['SearchName'] : '';
$TemplateObj->k                         = $k                        = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->LimitApplicants           = $LimitApplicants          = $preferences['ApplicantsSearchResultsLimit'];

//Applicants search sorting points
$TemplateObj->Sort = $Sort = isset($_REQUEST['Sort']) ? $_REQUEST['Sort'] : '';

//Request Id's
$TemplateObj->RequestIDs = $RequestIDs = isset($_REQUEST['RequestIDs']) ? $_REQUEST['RequestIDs'] : '';

//Forward Applicant
if($action == "forwardapplicant") {
	$TemplateObj->to                   = $to                   = isset($_REQUEST['to']) ? $_REQUEST['to'] : '';
	$TemplateObj->application          = $application          = isset($_REQUEST['application']) ? $_REQUEST['application'] : '';
	$TemplateObj->history              = $history              = isset($_REQUEST['history']) ? $_REQUEST['history'] : '';
	$TemplateObj->resumeupload         = $resumeupload         = isset($_REQUEST['resumeupload']) ? $_REQUEST['resumeupload'] : '';
	$TemplateObj->coverletterupload    = $coverletterupload    = isset($_REQUEST['coverletterupload']) ? $_REQUEST['coverletterupload'] : '';
	$TemplateObj->From                 = $From                 = isset($_REQUEST['From']) ? $_REQUEST['From'] : '';
	$TemplateObj->EmailComments        = $EmailComments        = isset($_REQUEST['EmailComments']) ? $_REQUEST['EmailComments'] : '';
}

$SEARCHRESULT = '';
if (($action == "search") && ($process == 'Y')) {
	//Include WeightedSearch Class
	require_once IRECRUIT_DIR . 'applicants/SearchQuery.inc';
}

//Load Graphs Related Scripts, Only for this page
$scripts_header[] = "js/loadAJAX.js";
$scripts_header[] = "js/appointmentscheduling.js";
$scripts_header[] = "js/irec_Display.js";
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$scripts_header[] = "js/selectbox.js";
$scripts_header[] = "js/weightedsearch.js";

//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

//Include configuration related javascript in footer
$scripts_footer[] = "js/applicants.js";

//Assign add applicant datepicker variables
if($action == "updateapplicant") {
	$script_vars_footer[]	= 'var datepicker_ids = "#StatusEffectiveDate";';
}
if($action == "search") {
	$script_vars_footer[]	= 'var datepicker_ids = "#ApplicationDate_From, #ApplicationDate_To";';
}
if($action == "status") {
	$script_vars_footer[]	= 'var datepicker_ids = "#ProcessDate_From, #ProcessDate_To";';
}
if($action == "onboardapplicant") {
	$script_vars_footer[]	= 'var datepicker_ids = "#Sage_HireDate";';
}
if($action == "updatestatus") {
	$script_vars_footer[]	= 'var datepicker_ids = "#StatusEffectiveDate";';
}

$script_vars_footer[]	= 'var datepicker_ids = "#ProcessDate_From, #ProcessDate_To";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';
$TemplateObj->scripts_vars_footer = $script_vars_footer;

//Assign all scripts to page_scripts_footer object
$TemplateObj->page_scripts_footer = $scripts_footer;

require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';

if ($permit ['Applicants'] < 1) {
	die ( require_once IRECRUIT_DIR . 'irecruit_permissions.err' );
}

$TemplateObj->DM = $DM = G::Obj('Organizations')->getExportType($OrgID);

//If action is empty redirect to applicantsSearch.php
if (!$action || $action == "search") {
	header("Location:applicantsSearch.php");
	exit();
}

echo $TemplateObj->displayIrecruitTemplate('views/applicants/Applicants');
?>
