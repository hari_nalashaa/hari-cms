<?php
require_once 'irecruitdb.inc';

$OrgID          =   isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$ApplicationID  =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$Type           =   isset($_REQUEST['Type']) ? $_REQUEST['Type'] : '';

if (isset($OrgID) && isset($ApplicationID) && isset($Type)) {
	//Set where condition
	$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "TypeAttachment = :TypeAttachment");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":TypeAttachment"=>$Type);
	//Get attachments information
	$results = $AttachmentsObj->getApplicantAttachments ( 'PurposeName, FileType', $where, "", array($params) );

	if(is_array($results ['results'])) {
		foreach ( $results ['results'] as $img ) {
			$purposename =   $img ['PurposeName'];
			$filetype    =   $img ['FileType'];
		}
	}
	
	$dfile = '';
	
	if (($purposename != "") && ($filetype != "")) {
		$dfile = IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments/' . $ApplicationID . '-' . $purposename . '.' . $filetype;
	}
	
	if (file_exists ( $dfile )) {
		
		header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename='.basename($dfile));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($dfile));
	    readfile($dfile);
	    exit;
	}
} // end if OrgID, AppID, and Type
?>