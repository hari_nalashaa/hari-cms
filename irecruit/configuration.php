<?php
require_once 'Configuration.inc';

//Set page title
$TemplateObj->title             =   $title              =   "Dashboard Configuration";

$TemplateObj->themesList        =   $themesList         =   G::Obj('IrecruitUserPreferences')->getThemes();
$TemplateObj->userPreferences   =   $userPreferences    =   G::Obj('IrecruitUserPreferences')->getUserPreferencesByUserID($USERID);

//Include configuration related javascript in footer
$scripts_footer[] = "js/configuration.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

global $USERID;

$TemplateObj->submit            =   $submit             =   'New Contact';

####################
// Insert the default user modules to DashboardUsersModule if not exist
G::Obj('DashboardConfiguration')->insUserDashboardModules ();

$moduleconfig   =   G::Obj('DashboardConfiguration')->selModuleUserConfig();

if(isset($moduleconfig[2]) && $moduleconfig[2]['ApplicationMaxDate'] != date('m/d/Y')) {
    $MC['ModuleId']         =   2;
    $MC['ModuleUpdType']    =   'Regular';
    G::Obj('DashboardConfiguration')->updModuleUserConfigByModuleId($MC);
}

// Retrieve all dashboard modules with ModuleId as Key
$TemplateObj->dashboardmodules  =   G::Obj('DashboardConfiguration')->getDashboardModules ();

// Retrieve all user modules with ModuleId as Key
$dashboardusermodules           =   G::Obj('DashboardConfiguration')->getUserDashboardModules ();

$TemplateObj->usermodules       =   $dashboardusermodules[0];
$TemplateObj->usermoduleidinfo  =   $dashboardusermodules[3];

// Retrieve user top blocks information
$dashusertopblocks              =   G::Obj('DashboardConfiguration')->getUserDashboardTopBlocks ();
$dashboardusertopblocks         =   $dashusertopblocks[0];

if ($dashusertopblocks [1] == 0) {
    G::Obj('DashboardConfiguration')->insDefaultDashboardTopBlocks ();
    
    $dashusertopblocks = G::Obj('DashboardConfiguration')->getUserDashboardTopBlocks ();
    $dashboardusertopblocks = $dashusertopblocks[0];
}

$TemplateObj->dashboardusertopblocks    =   $dashboardusertopblocks;

//Get all available top blocks information
$TemplateObj->dashboardtopblocks        =   G::Obj('DashboardConfiguration')->getDashboardTopBlocks ();

####################

if(isset($_POST['Save']) && $_POST['Save'] == "Save") {
    $RQ = $_POST;
    G::Obj('IrecruitUserPreferences')->updUserPreferences($USERID, $RQ);
    echo '<script>location.href="configuration.php?msg=usuc";</script>';
    exit ();
}
else if(isset($_POST['UserIDedit']))
{
	G::Obj('IrecruitUsers')->updateUserInformation(json_encode($_POST));
	echo '<script>location.href="configuration.php?msg=usuc";</script>';
	exit ();
}
else if($_REQUEST['tab_action'] == 'maintainemaillists') {
    
    if ($_REQUEST['process'] == 'Y') {
        
        $user_email_info    =   array(
            "OrgID"         =>  $OrgID,
            "UserID"        =>  $USERID,
            "EmailAddress"  =>  $_REQUEST['emailaddress'],
            "FirstName"     =>  $_REQUEST['firstname'],
            "LastName"      =>  $_REQUEST['lastname']
        );
        
        if (($_REQUEST['emailaddress'] != "") && ($_REQUEST['firstname'] != "") && ($_REQUEST['input'] == 'New Contact')) {
            //Insert Email Lists
            G::Obj('IrecruitUsers')->insUserEmailLists($user_email_info);
        } else if (isset($_REQUEST['emailaddress']) && isset($_REQUEST['firstname']) && ($_REQUEST['input'] = 'Edit Contact')) {
            $user_email_info['OrgEmailAddress'] = $_REQUEST['origemail'];
            //Update UserEmail Lists
            G::Obj('IrecruitUsers')->updUserEmailLists($user_email_info);
        }
        
        //Delete User Email Lists
        if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'delete') {
            G::Obj('IrecruitUsers')->delUserEmailLists($OrgID, $USERID, $_REQUEST['emailaddress']);
        } // end delete
        
    } // end process
    
    if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'edit') {
        //Set Email Lists Condition
        $where     =   array("OrgID = :OrgID", "UserID = :UserID", "EmailAddress = :EmailAddress");
        //Bind Parameters
        $params    =   array(':OrgID'=>$OrgID, ':UserID'=>$USERID, ':EmailAddress'=>$_REQUEST['emailaddress']);
        $results   =   G::Obj('IrecruitUsers')->getUserEmailLists("*", $where, '', array($params));
        $TemplateObj->EDITREC   =   $EDITREC    =   $results['results'][0];

        $TemplateObj->fn    =   $fn     =   $EDITREC ['FirstName'];
        $TemplateObj->ln    =   $ln     =   $EDITREC ['LastName'];
        $TemplateObj->em    =   $em     =   $_REQUEST['emailaddress'];
    
        $TemplateObj->submit    =   $submit =   'Edit Contact';
    }
}
else if(isset($_POST['photo_upload']) && $_POST['photo_upload'] == "Upload Photo") {
    if(isset($_FILES['profile_photo']['name']) && $_FILES['profile_photo']['name'] != "") {
        if($_FILES['profile_photo']['size'] > 60000) {
            header ( "Location:configuration.php?msg=maxsize" );
            exit ();
        }
        else {
            $path_info = pathinfo($_FILES['profile_photo']['name']);
            $extension = $path_info['extension'];
            
            ##########################################################################
            //Create vault directory if not exists
            if (! is_dir ( IRECRUIT_DIR . "vault/" )) {
                @mkdir(IRECRUIT_DIR . "vault/", 0777);
                @chmod(IRECRUIT_DIR . "vault/", 0777);
            }
            
            //Create profile avatars directory if not exist
            if (! is_dir ( IRECRUIT_DIR . "vault/".$OrgID."/" )) {
                @mkdir(IRECRUIT_DIR . "vault/".$OrgID."/", 0777);
                @chmod(IRECRUIT_DIR . "vault/".$OrgID."/", 0777);
            }
            
            //Create profile avatars directory if not exist
            if (! is_dir ( IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/" )) {
                @mkdir(IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/", 0777);
                @chmod(IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/", 0777);
            }
            
            //Create profile avatars directory if not exist
            if (! is_dir ( IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/profile_avatars/" )) {
                @mkdir(IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/profile_avatars/", 0777);
                @chmod(IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/profile_avatars/", 0777);
            }
            
            //Set the Profile Directory
            $PROFILE_DIR = IRECRUIT_DIR . "vault/".$OrgID."/".$USERID."/profile_avatars/";
            
            if (! is_dir ( $PROFILE_DIR )) {
                @mkdir($PROFILE_DIR, 0777);
                @chmod($PROFILE_DIR, 0777);
            }
            
            if($user_preferences['DashboardAvatar'] != "")
            {
                @unlink($PROFILE_DIR.$user_preferences['DashboardAvatar']);
            }
            
            ##########################################################################
            
            
            $file_name = "profile_photo.".$extension;
            if(@move_uploaded_file ( $_FILES['profile_photo']['tmp_name'], $PROFILE_DIR.$file_name)) {
                G::Obj('DashboardConfiguration')->updateDashboardAvatarImage($file_name);
            }
            echo '<script>location.href="configuration.php?msg=suc";</script>';
            exit ();
        }
    }
}
else {
	//Update user topblock information
	if(isset($_POST['topblock'])) {
		G::Obj('DashboardConfiguration')->updDashboardUserTopBlockInfo();
		echo '<script>location.href="configuration.php?msg=tsuc";</script>';
		exit;
	}
	
	//Update modules configuration
	if(isset($_POST['moduleconfiguration'])) {
		G::Obj('DashboardConfiguration')->updDashboardUserModuleConfig($_POST);
		$MI = $_POST['moduleconfiguration'];
		echo '<script>location.href="configuration.php?msg=msuc&module='+$MI+'";</script>';
		exit;
	}

	// Update User Selected Modules
	if (isset ( $_POST ) && isset($_POST ['hidemodules']) && $_POST ['hidemodules'] == '1') {
		G::Obj('DashboardConfiguration')->updDashboardUserModules ( $_POST );
		echo '<script>location.href="configuration.php?msg=suc";</script>';
		exit ();
	}
}

echo $TemplateObj->displayIrecruitTemplate('views/responsive/ResponsiveConfiguration');
?>