<?php
require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';
require_once IRECRUIT_DIR . 'SessionConfig.inc';

require_once VENDOR_DIR . 'autoload.php';

ini_set ('display_errors', 0);
date_default_timezone_set('America/New_York');
ini_set("allow_url_fopen", 1);

define('FROM_SRC', 'IRECRUIT');

error_reporting(E_ALL);

$error_log_file_path = ROOT . "logs/php-errors/php-error-log-".date('Y-m-d').".log";

if(!is_dir(ROOT . "logs/php-errors")) {
	mkdir(ROOT . "logs/php-errors");
	chmod(ROOT . "logs/php-errors", 0777);
}

ini_set("log_errors", 1);
ini_set("error_log", $error_log_file_path);

//Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';
require_once COMMON_DIR . 'color.inc';
?>
