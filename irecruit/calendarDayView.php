<?php 
require_once 'Configuration.inc';

if($_POST['view'] == "today") {
	$today = explode("-", $_POST['today']);
	
	$SYear  = $today[0];
	$SMonth = (int)$today[1];
	$SDay   = (int)$today[2];
}
if($_POST['view'] == "day") {
	$SYear  = $_POST['year'];
	$SMonth = (int)$_POST['month'];
	$SDay   = (int)$_POST['day'];
}

if($SMonth < 9) $SMonth = "0".$SMonth;
if($SDay < 9) $SDay = "0".$SDay;

$cdate = "$SYear-$SMonth-$SDay";

//Calculate the next and previous days
if($_POST['navigation'] == 'next') {
	$date = date("Y-m-d", strtotime('+1 day', strtotime($cdate)));

	$SYear  = date('Y', strtotime($date));
	$SMonth = date('m', strtotime($date));
	$SDay   = date('d', strtotime($date));
}
if($_POST['navigation'] == 'previous') {
	$date = date("Y-m-d", strtotime('-1 day', strtotime($cdate)));

	$SYear  = date('Y', strtotime($date));
	$SMonth = date('m', strtotime($date));
	$SDay   = date('d', strtotime($date));
}

$calendar_day_view = $AppointmentsObj->calendarDayView($SYear, $SMonth, $SDay);

$json['calendar'] = $calendar_day_view;
$json['year'] = $SYear;
$json['month'] = $SMonth;
$json['day'] = $SDay;
$json['calendar_label'] = date("M Y", strtotime(date("$SYear-$SMonth-$SDay")));

echo json_encode($json);
?>