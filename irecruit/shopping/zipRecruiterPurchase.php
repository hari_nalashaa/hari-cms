<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title                         =   $title                      =   'Purchase ZipRecruiter';

$TemplateObj->OrgID                         =   $OrgID                      =   isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$TemplateObj->RequestID                     =   $RequestID                  =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->k                             =   $k                          =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

//Update Zip Recruiter Job Category
if(isset($_REQUEST['ZipRecruiterJobCategory']) && $_REQUEST['ZipRecruiterJobCategory'] != "") {
    $set_info   = array("ZipRecruiterJobCategory = :ZipRecruiterJobCategory");
    $where_info = array("OrgID = :OrgID", "RequestID = :RequestID");
    $params     = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":ZipRecruiterJobCategory"=>$_REQUEST['ZipRecruiterJobCategory']);
    $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params));
}

$TemplateObj->req_info_zip_rec              =   $req_info_zip_rec           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists, ZipRecruiterJobCategory, ZipRecruiterLabelID", $OrgID, $RequestID);

//Get labels list
$TemplateObj->labels_list                   =   $labels_list                =   $LabelsObj->getAllLabels($OrgID);

$TemplateObj->ZipRecruiterJobCategories     =   $ZipRecruiterJobCategories  =   $ZipRecruiterObj->getZipRecruiterJobCategories();
$TemplateObj->ZipRecruiterJobCategory       =   $ZipRecruiterJobCategory    =   $req_info_zip_rec['ZipRecruiterJobCategory'];

$TemplateObj->free_jobboard_list            =   $free_jobboard_list         =   json_decode($req_info_zip_rec['FreeJobBoardLists'], true);

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/shopping/ZipRecruiterPurchase.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate ( 'views/shopping/ZipRecruiterPurchase' );
}
?>