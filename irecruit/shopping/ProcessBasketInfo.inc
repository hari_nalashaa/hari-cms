<?php
// set where condition
$where      =   array ("substring(PurchaseNumber, 1, 4) = :PurchaseNumber");
// set parameters
$params     =   array (":PurchaseNumber" => $year);
// Get purchases information
$results    =   G::Obj('Purchases')->getPurchasesInfo ( "*", $where, "", array ($params) );
$invcnt     =   $results ['count'];
 
$invcnt ++;
$invcnt     =   "000" . $invcnt;
$invcnt     =   substr ( $invcnt, - 4, 4 );
 
$PurchaseNumber = $year . $invcnt;
 
$digitccnumber = preg_replace ( "/[- ]/", '', $_REQUEST['CCnumber'] );
for($i = 1; $i <= strlen ( substr ( $digitccnumber, 0, - 4 ) ); $i ++) {
    $hide .= "X";
}
$dbccnum = $hide . substr ( $digitccnumber, - 4, 4 );
$dbccexp = $_REQUEST['CCexpyear'] . "-" . $_REQUEST['CCexpmo'] . "-01";
 
//Change your Access Token Here
$access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
//Add your request ID here
$intuit_request_id    =   uniqid(str_replace(".", "", microtime(true)));
 
$CurlClientObj = new CurlClient();
 
$body = [
        "amount"    =>  $_REQUEST['amount'],
        "currency"  =>  "USD",
        "context"   =>  [
            "mobile"        =>  "false",
            "isEcommerce"   =>  "true"
        ]
    ];

if(isset($_REQUEST['CCSavedCards']) && $_REQUEST['CCSavedCards'] != "") {
    $body["cardOnFile"]   =   $_REQUEST['CCSavedCards'];
}
else {
    $body["card"]   =   [
        "expYear"   =>  $_REQUEST['CCexpyear'],
        "expMonth"  =>  $_REQUEST['CCexpmo'],
        "address"   =>  [
            "region"           => $_REQUEST['Country'],
            "postalCode"       => $_REQUEST['ZipCode'],
            "streetAddress"    => $_REQUEST['Address1'],
            "country"          => $_REQUEST['Country'],
            "city"             => $_REQUEST['City']
        ],
        "name"      =>  $_REQUEST['FullName'],
        "cvc"       =>  $_REQUEST['CCidentifier1'],
        "number"    =>  $_REQUEST['CCnumber']
    ];
}
 
$http_header = array(
    'Accept'          =>  'application/json',
    'Request-Id'      =>  $intuit_request_id,
    'Authorization'   =>  $access_token,
    'Content-Type'    =>  'application/json;charset=UTF-8'
);
 
$intuit_response = $CurlClientObj->makeAPICall($IntuitObj->create_charge_url, "POST", $http_header, json_encode($body), null, false);
 
$req_data  = "\nRequest:".json_encode($http_header)."<br>";
$req_data .= json_encode($body)."<br>";
$req_data .= "\nResponse:".json_encode($intuit_response);
 
Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-payment-response.txt", $req_data, "a+", false);
Logger::writeMessageToDb($OrgID, "Intuit", $req_data);

//$intuit_response        =   "{\"errors\":[{\"code\":\"PMT-4000\",\"type\":\"invalid_request\",\"message\":\"card.number is invalid.\",\"detail\":\"card.number\",\"infoLink\":\"https:\/\/developer.intuit.com\/v2\/docs?redirectID=PayErrors\"}]}";
$intuit_payment_info    =   json_decode($intuit_response, true);

if($intuit_payment_info['id'] != "") {
    $intuit_info = array(
        "OrgID"             =>  $OrgID,
        "PurchaseNumber"    =>  $PurchaseNumber,
        "Created"           =>  $intuit_payment_info['created'],
        "Status"            =>  $intuit_payment_info['status'],
        "Amount"            =>  $intuit_payment_info['amount'],
        "Currency"          =>  $intuit_payment_info['currency'],
        "CardInfo"          =>  json_encode($intuit_payment_info['card']),
        "AvsStreet"         =>  $intuit_payment_info['avsStreet'],
        "AvsZip"            =>  $intuit_payment_info['avsZip'],
        "CSCMatch"          =>  $intuit_payment_info['cardSecurityCodeMatch'],
        "Id"                =>  $intuit_payment_info['id'],
        "Context"           =>  json_encode($intuit_payment_info['context']),
        "AuthCode"          =>  $intuit_payment_info['authCode'],
        "PaymentCategory"   =>  "OTHER",
        "CreatedDateTime"   =>  "NOW()"
    );

    if(isset($_REQUEST['CCSavedCards']) && $_REQUEST['CCSavedCards'] != "") {
        $intuit_info["CardOnFileID"]    =   $_REQUEST['CCSavedCards'];
    }
    
    G::Obj('IntuitPaymentInformation')->insIntuitPaymentInformation($intuit_info);
}
 
if ($intuit_payment_info['status'] == "CAPTURED") {

    //Code to save the card
    if(isset($_REQUEST['chkDoYouWantToSave']) 
        && $_REQUEST['chkDoYouWantToSave'] == "Y") {

        //Get Intuit Access Information
        $intuit_access_info   =   G::Obj('Intuit')->getUpdatedAccessTokenInfo();
        //Change your Access Token Here
        $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
        //Add your request ID here
        $intuit_request_id    =   uniqid(str_replace(".", "", microtime(true)));
        
        $card_info = array(
            "number"    =>  preg_replace('[\D]', '', $_REQUEST['CCnumber']),
            "expMonth"  =>  $_REQUEST['CCexpmo'],
            "expYear"   =>  $_REQUEST['CCexpyear'],
            "name"      =>  $_REQUEST['FullName'],
            "cvc"       =>  $_REQUEST['CCidentifier1'],
            "address"   =>  array(
                "streetAddress"  => $_REQUEST['Address1'],
                "city"           => $_REQUEST['City'],
                "region"         => $_REQUEST['Country'],
                "country"        => $_REQUEST['Country'],
                "postalCode"     => $_REQUEST['ZipCode']
            )
        );
        
        $http_header = array(
            'Accept'          =>  'application/json',
            'Request-Id'      =>  $intuit_request_id,
            'Authorization'   =>  $access_token,
            'Content-Type'    =>  'application/json;charset=UTF-8'
        );
        
        $card_on_file_url       =   $IntuitManageCardsObj->customers_url.$OrgID.'/cards';
        $intuit_save_card_resp  =   $CurlClientObj->makeAPICall($card_on_file_url, "POST", $http_header, json_encode($card_info), null, false);
        
        $req_data  = "\nRequest:".json_encode($http_header)."<br>";
        $req_data .= "\nResponse:".json_encode($intuit_save_card_resp);
        
        Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-card-on-file.txt", $req_data, "a+", false);
        Logger::writeMessageToDb($OrgID, "Intuit", $req_data);
        
    }
    
    //Unset Payment Information Session
    unset($_SESSION['I']['payment_information']);
    
    $purchases_info = array (
        "OrgID"             =>  $OrgID,
        "PurchaseNumber"    =>  $PurchaseNumber,
        "UserID"            =>  $USERID,
        "PurchaseDate"      =>  "NOW()",
        "TransactionID"     =>  $intuit_payment_info['id'],
        "ApprovalCode"      =>  $intuit_payment_info['status'],
        "AuthAmount"        =>  $intuit_payment_info["amount"],
        "Processed"         =>  "Y",
        "PaymentCategory"   =>  "OTHER",
        "CreatedDateTime"   =>  "NOW()"
    );
    
    if(isset($_REQUEST['CCSavedCards']) && $_REQUEST['CCSavedCards'] != "") {
        $purchases_info["FullName"]     =   $intuit_payment_info['card']['name'];
        $purchases_info["Address1"]     =   $intuit_payment_info['card']['address']['streetAddress'];
        $purchases_info["Address2"]     =   '';
        $purchases_info["City"]         =   $intuit_payment_info['card']['address']['city'];
        $purchases_info["State"]        =   '';
        $purchases_info["ZipCode"]      =   $intuit_payment_info['card']['address']['postalCode'];
        $purchases_info["Country"]      =   $intuit_payment_info['card']['address']['country'];
        $purchases_info["Phone"]        =   $_REQUEST['Phone'];
        $purchases_info["Email"]        =   $_REQUEST['Email'];
        $purchases_info["CCtype"]       =   $intuit_payment_info['card']['cardType'];
        $purchases_info["CCnumber"]     =   $intuit_payment_info['card']['number'];
        $purchases_info["CCexp"]        =   $intuit_payment_info['card']['expYear'] . "-" . $intuit_payment_info['card']['expMonth'] . "-01";;
    }
    else {
        $purchases_info["FullName"]     =   $_REQUEST['FullName'];
        $purchases_info["Address1"]     =   $_REQUEST['Address1'];
        $purchases_info["Address2"]     =   $_REQUEST['Address2'];
        $purchases_info["City"]         =   $_REQUEST['City'];
        $purchases_info["State"]        =   $_REQUEST['State'];
        $purchases_info["ZipCode"]      =   $_REQUEST['ZipCode'];
        $purchases_info["Country"]      =   $_REQUEST['Country'];
        $purchases_info["Phone"]        =   $_REQUEST['Phone'];
        $purchases_info["Email"]        =   $_REQUEST['Email'];
        $purchases_info["CCtype"]       =   $_REQUEST['CCtype'];
        $purchases_info["CCnumber"]     =   $dbccnum;
        $purchases_info["CCexp"]        =   $dbccexp;
    }
    
    //Insert purchase information
    G::Obj('Purchases')->insPurchaseInfo ( 'Purchases', $purchases_info );
     
    $i = 0;
     
    //Set where condition
    $where = array("OrgID = :OrgID", "UserID = :UserID");
    //set parameters
    $params = array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
    //get basket information
    $results = G::Obj('Purchases')->getBasketInfo("*", $where, "", array($params));
     
    if(is_array($results['results'])) {
         
        foreach($results['results'] as $BASKET) {

            $i ++;

            //Purchase items information
            $purchase_items_info = array (
                    "OrgID"             =>  $OrgID,
                    "PurchaseNumber"    =>  $PurchaseNumber,
                    "LineNo"            =>  $i,
                    "ServiceType"       =>  $BASKET ['ServiceType'],
                    "RequestID"         =>  $BASKET ['RequestID'],
                    "InvoiceNo"         =>  $BASKET ['InvoiceNo'],
                    "BudgetTotal"       =>  $BASKET ['BudgetTotal'],
                    "Comment"           =>  $BASKET ['Comment']
            );
            //Insert purchase information
            G::Obj('Purchases')->insPurchaseInfo ( 'PurchaseItems', $purchase_items_info );

            if ($BASKET ['ServiceType'] == "ZipRecruiter") {

                //ziprecruiter feed information
                $ziprecruiter_feed_info = array (
                    "OrgID"             =>  $OrgID,
                    "PurchaseNumber"    =>  $PurchaseNumber,
                    "RequestID"         =>  $BASKET ['RequestID'],
                    "BudgetTotal"       =>  $BASKET ['BudgetTotal'],
                    "FullName"          =>  $_REQUEST['FullName'],
                    "Email"             =>  $_REQUEST['Email']
                );
                //Insert purchase information
                G::Obj('Purchases')->insPurchaseInfo ( 'ZipRecruiterFeed', $ziprecruiter_feed_info );
                
                $set_info               =   array("ZipRecruiterTrafficBoost = :ZipRecruiterTrafficBoost");
                $where_info             =   array("OrgID = :OrgID", "RequestID = :RequestID");
                $params_info            =   array(":OrgID"=>$OrgID, ":RequestID"=>$BASKET ['RequestID'], ":ZipRecruiterTrafficBoost"=>$BASKET['ZipRecruiterTrafficBoost']);
            
                $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
                
            } // end if indeed
            else if ($BASKET ['ServiceType'] == "Indeed") {

                //Indeed feed information
                $indeed_feed_info = array (
                    "OrgID"             =>  $OrgID,
                    "PurchaseNumber"    =>  $PurchaseNumber,
                    "RequestID"         =>  $BASKET ['RequestID'],
                    "BudgetTotal"       =>  $BASKET ['BudgetTotal'],
                    "FullName"          =>  $_REQUEST['FullName'],
                    "Email"             =>  $_REQUEST['Email']
                );
                //Insert purchase information
                G::Obj('Purchases')->insPurchaseInfo ( 'IndeedFeed', $indeed_feed_info );

            } // end if indeed

        } // end foreach

    }
     
    // set where condition
    $where = array ("OrgID = :OrgID", "UserID = :UserID");
    // set parameters
    $params = array (":OrgID" => $OrgID, ":UserID" => $USERID);
    // delete Baseket information
    G::Obj('Purchases')->delBasketAndPurchaseInfo ( 'Basket', $where, array ($params) );
     
    if (isset($_REQUEST['Email']) && $_REQUEST['Email'] != "") {
        
        $to         =   $_REQUEST['Email'];
        $subject    =   "Your iRecruit Order # " . $PurchaseNumber;

        $message .= '<img src="' . PUBLIC_HOME . 'images/iRecruit.png"><br><br>';
        
        $message .= "This email is to confirm your recent purchase with iRecruit." . "<br><br>";

        $message .= "Purchase Date: " . $MysqlHelperObj->getDateTime('%m/%d/%Y') . "<br>";
        $message .= "Purchase Number: " . $PurchaseNumber . "<br>";
        $message .= "Purchaser's Name: " . $intuit_payment_info['card']['name'] . "<br>";
        $message .= "Payment Details: $" . $intuit_payment_info ["amount"] . " was charged to card ending in " . substr($intuit_payment_info['card']['number'],-4) . "<br>";
        $message .= "Merchant Name: COST MANAGEMENT SERVICE LLC" . "<br><br>";
         
        $message .= "Information regarding your order can also be found under iRecruit Administration on the ";
        $message .= "<a href=\"" . IRECRUIT_HOME . "administration.php?action=purchases\">Purchases</a> page." . "<br><br>";
        $message .= "Thank you," . "<br>";
        $message .= "iRecruit" . "<br>";
        $message .= "<a href=\"https://www.irecruit-software.com\">www.irecruit-software.com</a>" . "<br>";
        
        $message  = nl2br($message);
        
        //Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
         
        // Set who the message is to be sent to
        $PHPMailerObj->addAddress ( $to );
        // Set the subject line
        $PHPMailerObj->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        $PHPMailerObj->msgHTML ( $message );
        // Content Type Is HTML
        $PHPMailerObj->ContentType = 'text/html';
        // send mail
        $PHPMailerObj->send ();
    } // end email

    echo '<br>';
    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    echo '<tr><td width="50"></td><td height="150">';
    echo 'Thank you for your order. The order number for your records is ' . $PurchaseNumber . '. An email confirmation of your order has been sent to your account email address. ';
    
    if($rem_div_id == "purchase-premium-feed") {
        echo 'Purchases can also be viewed within the Administration section of this site under ';
        echo "<a href=\"" . IRECRUIT_HOME . "administration.php?action=purchases\">Purchases</a>.";
    }
     
    echo '</td><td width="50"></td></tr>';
    echo '</table>';

} else { // else approved

    $_SESSION['I']['payment_information'][$USERID]['CCSavedCards']   =   $_REQUEST['CCSavedCards'];
    $_SESSION['I']['payment_information'][$USERID]['CCtype']         =   $_REQUEST['CCtype'];
    $_SESSION['I']['payment_information'][$USERID]['CCnumber']       =   $_REQUEST['CCnumber'];
    $_SESSION['I']['payment_information'][$USERID]['CCexpmo']        =   $_REQUEST['CCexpmo'];
    $_SESSION['I']['payment_information'][$USERID]['CCexpyear']      =   $_REQUEST['CCexpyear'];
    $_SESSION['I']['payment_information'][$USERID]['CCidentifier1']  =   $_REQUEST['CCidentifier1'];
    $_SESSION['I']['payment_information'][$USERID]['FullName']       =   $_REQUEST['FullName'];
    $_SESSION['I']['payment_information'][$USERID]['Address1']       =   $_REQUEST['Address1'];
    $_SESSION['I']['payment_information'][$USERID]['Address2']       =   $_REQUEST['Address2'];
    $_SESSION['I']['payment_information'][$USERID]['City']           =   $_REQUEST['City'];
    $_SESSION['I']['payment_information'][$USERID]['State']          =   $_REQUEST['State'];
    $_SESSION['I']['payment_information'][$USERID]['ZipCode']        =   $_REQUEST['ZipCode'];
    $_SESSION['I']['payment_information'][$USERID]['Country']        =   $_REQUEST['Country'];
    $_SESSION['I']['payment_information'][$USERID]['Email']          =   $_REQUEST['Email'];  
    $_SESSION['I']['payment_information'][$USERID]['Phone']          =   $_REQUEST['Phone'];

    $message  = $OrgID . "\n";
    $message .= $USERID . "\n";

    if(count($intuit_payment_info) > 0) {
        $message .= print_r($intuit_payment_info, true)."\n";
    }
    
    $err_message = "";
    if(isset($intuit_payment_info['errors']) && count($intuit_payment_info['errors']) > 0) {
        for($e = 0; $e < count($intuit_payment_info['errors']); $e++) {
            $err_message .= $intuit_payment_info['errors'][$e]['message']."\n";
        }
    }

    $message .= $err_message;
    $message  = nl2br($message);
    
    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
        
    // Set who the message is to be sent to
    $PHPMailerObj->addAddress ( 'dedgecomb@irecruit-software.com' );
    // Set the subject line
    $PHPMailerObj->Subject = 'Response from Payment Gateway';
    // convert HTML into a basic plain-text alternative body
    $PHPMailerObj->msgHTML ( $message );
    // Content Type Is HTML
    $PHPMailerObj->ContentType = 'text/html';
    // send mail
    $PHPMailerObj->send ();
    
    echo '<br>';

    echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';
    echo '<tr><td width="50"></td><td height="150" style="color: red;">';
    echo 'There has been an error with your credit card information. Please ';
    echo '<a href="javascript:void(0)" onclick=\'getBasketInfo("'.$AccessCode.'")\'>click here</a>';
    echo ' to return to the shopping basket and verify that your information is accurate.';
     
    if($err_message != "") {
        echo '<div style="padding:10px;background-color:#eeeeee;margin-top:20px;">';
        echo nl2br($err_message);
        echo '</div>';
    }
     
    echo '</td><td width="50"></td></tr>';
    echo '</table>';

} // end else approved
 
$display = "N";
?>
