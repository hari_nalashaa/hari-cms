<?php 
// qualify fields
$ERROR = "";
$error = "";

if(isset($_REQUEST['CCSavedCards']) && $_REQUEST['CCSavedCards'] == "") {
	
    if ($_REQUEST['CCtype'] == "") {
        $ERROR .= " - Credit Card Type is not selected." . "\\n";
        $CCtype_error = "Y";
    }
    
    if ($_REQUEST['CCnumber'] == "") {
        if ($error != "Y") {
            $ERROR .= " - Credit Card Number is missing." . "\\n";
            $CCnumber_error = "Y";
            $error = "Y";
        }
    }
    
    if (! is_numeric ( preg_replace ( "/[- ]/", '', $_REQUEST['CCnumber'] ) )) {
        if ($error != "Y") {
            $ERROR .= " - Credit Card Number needs to contain only digits separated by\\n    spaces or hyphens." . "\\n";
            $CCnumber_error = "Y";
            $error = "Y";
        }
    }
    
    $length = 13;
    if (strlen ( preg_replace ( "/[- ]/", '', $_REQUEST['CCnumber'] ) ) < $length) {
        if ($error != "Y") {
            $ERROR .= " - Credit Card Number does not contain enough digits." . "\\n";
            $CCnumber_error = "Y";
        }
    }
    $error = "";
    
    if ($_REQUEST['CCexpmo'] == "") {
        $ERROR .= " - Credit Card Expiration Month is not selected." . "\\n";
        $CCexpmo_error = "Y";
    }
    
    if ($_REQUEST['CCexpyear'] == "") {
        $ERROR .= " - Credit Card Expiration Year is not selected." . "\\n";
        $CCexpyear_error = "Y";
    }
    
    if ($_REQUEST['CCidentifier1'] == "") {
        $ERROR .= " - Credit Card Security Code (CSC) is missing." . "\\n";
        $CCidentifier1_error = "Y";
        $error = "Y";
    }
    
    if (! is_numeric ( $_REQUEST['CCidentifier1'] )) {
        if ($error != "Y") {
            $ERROR .= " - Credit Card Security Code (CSC) needs to contain only digits." . "\\n";
            $CCidentifier1_error = "Y";
            $error = "Y";
        }
    }
    
    $length = 3;
    if (strlen ( $_REQUEST['CCidentifier1'] ) < $length) {
        if ($error != "Y") {
            $ERROR .= " - Credit Card Security Code (CSC) does not contain enough digits." . "\\n";
            $CCidentifier1_error = "Y";
        }
    }
    
    $error = "";
    
    $length = 5;
    if (strlen ( $_REQUEST['FullName'] ) < $length) {
        $ERROR .= " - Name does not contain enough characters." . "\\n";
        $FullName_error = "Y";
    }
    
    $length = 10;
    if (strlen ( $_REQUEST['Address1'] ) < $length) {
        $ERROR .= " - Address does not contain enough characters." . "\\n";
        $Address1_error = "Y";
    }
    
    $length = 4;
    if (strlen ( $_REQUEST['City'] ) < $length) {
        $ERROR .= " - City does not contain enough characters." . "\\n";
        $City_error = "Y";
    }
    
    if ($_REQUEST['State'] == "") {
        $ERROR .= " - State is not selected." . "\\n";
        $State_error = "Y";
    }
    
    if ($_REQUEST['ZipCode'] == "") {
        $ERROR .= " - ZIP Code is missing." . "\\n";
        $ZipCode_error = "Y";
        $error = "Y";
    }
    
    if (! is_numeric ( preg_replace ( "/[- ]/", '', $_REQUEST['ZipCode'] ) )) {
        if ($error != "Y") {
            $ERROR .= " - ZIP Code needs to contain only digits and\\n    a hyphen or space when using +4." . "\\n";
            $ZipCode_error = "Y";
            $error = "Y";
        }
    }
    
    $length = 5;
    if (strlen ( $_REQUEST['ZipCode'] ) < $length) {
        if ($error != "Y") {
            $ERROR .= " - ZIP Code does not contain enough digits." . "\\n";
            $ZipCode_error = "Y";
        }
    }
    $error = "Y";
    
    if ($_REQUEST['Country'] == "") {
        $ERROR .= " - Country Code is not selected." . "\\n";
        $Country_error = "Y";
    }
    
}

if ($_REQUEST['agree'] == "") {
    $ERROR .= " - You have not agreed to our policies." . "\\n";
    $agree_error = "Y";
}