<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title	    =   $title      =   'Shopping Cart';

$TemplateObj->k         =   $k          =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->file_src  =   $file_src   =   "ZipRecruiterBasket";

if(isset($_REQUEST['cpage']) && $_REQUEST['cpage'] == 'purchases') {
    $TemplateObj->rem_div_id = $rem_div_id = "basket_payment_info";
}
else {
    $TemplateObj->rem_div_id = $rem_div_id = "purchase-premium-feed";
}

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/shopping/Basket.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate ( 'views/shopping/Basket' );
}
?>