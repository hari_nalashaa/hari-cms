<?php
$navbar_color = "";

if(isset($user_preferences)) {
    $navbar_color = $user_preferences['TableRowHeader'];
}

// basket here
$basket = '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';

$basket .= '<tr bgcolor="'.$navbar_color.'">';

$basket .= '<td width="100"><b style="color: #ffffff;">&nbsp;Item</b></td>';
$basket .= '<td><b style="color: #ffffff;">&nbsp;Description</b></td>';
$basket .= '<td width="60" align="center"><b style="color: #ffffff;">Item Total</b></td>';
if ($PURCHASES ['PurchaseNumber'] == "") {
	$basket .= '<td width="60" align="center"><b style="color: #ffffff;">Remove</b></td>';
} // end PURCHASES

$basket .= '</tr>';

$amount = 0;

if ($PURCHASES ['PurchaseNumber'] != "") {
	//set where condition
	$where     =   array("PurchaseNumber = :PurchaseNumber");
	//set parameters
	$params    =   array(":PurchaseNumber"=>$PURCHASES ['PurchaseNumber']);
	//get purchase items
	$resultsIN =   G::Obj('Purchases')->getPurchaseItems("*", $where, "", array($params));
} else {
	//set where condition
	$where     =   array("OrgID = :OrgID", "UserID = :UserID");
	//set parametes
	$params    =   array(":OrgID"=>$OrgID, ":UserID"=>$USERID);
	//get basket information
	$resultsIN =   G::Obj('Purchases')->getBasketInfo("*", $where, "", array($params));
}

$itmcnt = $resultsIN['count'];

if(is_array($resultsIN['results'])) {
	foreach($resultsIN['results'] as $BASKET) {
        $multiorgid_req =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $BASKET ['RequestID']);
        $description    =   "";
	
        $item           =   $BASKET ['ServiceType'];
	
		if ($BASKET ['RequestID'] != "") {
			$itemline = "Sponsor Link: " . $RequisitionDetailsObj->getReqJobIDs ( $BASKET ['OrgID'], $multiorgid_req, $BASKET ['RequestID'] ) . " - " . $RequisitionDetailsObj->getJobTitle ( $BASKET ['OrgID'], $multiorgid_req, $BASKET ['RequestID'] ) . "<br>";
	
			if (isset($BASKET ['Canceled']) && $BASKET ['Canceled'] != "0000-00-00 00:00:00") {
				$canceled = '<font style="color:red;">Canceled:</font> ';
				$BASKET ['BudgetTotal'] = 0;
				$description .= $canceled . "<strike>" . $itemline . "</strike>";
			} else {
				$description .= $itemline;
			}
		}
		if ($BASKET ['InvoiceNo'] != "") {
			$description .= "Invoice Number: " . $BASKET ['InvoiceNo'] . "<br>";
		}
	
		if ($BASKET ['Comment'] != "") {
			$description .= "" . $BASKET ['Comment'] . "<br>";
		}
	
		$itemamount = money_format ( '%i', $BASKET ['BudgetTotal'] );
		$amount += $itemamount;
	
		$basket .= '<tr>';
		$basket .= '<td valign="top">' . $item . '</td>';
		$basket .= '<td>' . $description . '</td>';
		$basket .= '<td align="right" valign="bottom">' . $itemamount . '</td>';
		if ($PURCHASES ['PurchaseNumber'] == "") {
			$basket .= '<td align="center">';

			if($file_src == "ZipRecruiterBasket") {
			    $basket .= '<a href=\'javascript:void(0);\'  onclick=\'removeItemFromZipRecruiterBasket("'.$BASKET ['LineNo'].'", "'.$RequisitionDetailsObj->getJobTitle ( $BASKET ['OrgID'], $multiorgid_req, $BASKET ['RequestID']).'", "'. $rem_div_id .'")\'>';
			}
			else {
			    $basket .= '<a href=\'javascript:void(0);\'  onclick=\'removeItemFromBasket("'.$BASKET ['LineNo'].'", "'.$RequisitionDetailsObj->getJobTitle ( $BASKET ['OrgID'], $multiorgid_req, $BASKET ['RequestID']).'", "'. $rem_div_id .'")\'>';
			}

			$basket .= '<img src="' . IRECRUIT_HOME . 'images/icons/basket_remove.png" border="0" title="Remove Item" style="margin:0px 3px -4px 0px;"></a>';
			$basket .= '</td>';
		} // end PURCHASES
		$basket .= '</tr>';
	
		$RequestID = $BASKET ['RequestID'];
	} // end foreach	
}

$basket .= '<tr>';
$basket .= '<td colspan="2" align="right">Total&nbsp;</td><td align="right"><b>$' . money_format ( '%i', $amount ) . '</b></td>';
if ($PURCHASES ['PurchaseNumber'] == "") {
	$basket .= '<td>&nbsp;</td>';
} // end PURCHASES

$basket .= '</tr>';

$basket .= '</table>';

// basket here

// Can we cancel this order

//set where condition
$where  =   array(
                "P.OrgID            =   PI.OrgID",
                "P.PurchaseNumber   =   PI.PurchaseNumber", 
                "P.OrgID            =   :OrgID", 
                "P.PurchaseNumber   =   :PurchaseNumber", 
                "P.Processed        =   '0000-00-00 00:00:00'", 
                "PI.Canceled        =   '0000-00-00 00:00:00'"
                );
//set parameters
$params = array(":OrgID"=>$OrgID, ":PurchaseNumber"=>$PURCHASES ['PurchaseNumber']);

//get purchases and purchaseitems
$resultsIN = G::Obj('Purchases')->getPurchasesAndPurchaseItems("Purchases P, PurchaseItems PI", "*", $where, "", array($params));

$Processedcnt = $resultsIN['count'];

if ($Processedcnt > 0) {
	$basket .= '<form method="POST" action="">';
	$basket .= '<center><button type="submit" name="cancelORDER" value="' . $PURCHASES ['PurchaseNumber'] . '" onclick="return confirm(\'Are you sure you want to cancel this order?\n\n\Invoice Number: ' . $PURCHASES ['PurchaseNumber'] . '\')">Cancel this order.</button></center>';
	$basket .= '<input type="hidden" name="process" value="Y">';
	$basket .= '</form>';
}
?>