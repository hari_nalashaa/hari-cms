<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title     =   $title      =   'Purchase Sponsored Links';

$TemplateObj->OrgID     =   $OrgID      =   isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$TemplateObj->RequestID =   $RequestID  =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->k         =   $k          =   isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

//echo print_r($_REQUEST,true);
//exit;

//Update Indeed Label
if(isset($_REQUEST['IndeedLabel']) && $_REQUEST['IndeedLabel'] != "") {
    $set_info   =   array("IndeedLabelID = :IndeedLabel","IndeedWFH= :IndeedWFH");
    $where_info =   array("OrgID = :OrgID", "RequestID = :RequestID");
    $params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":IndeedLabel"=>$_REQUEST['IndeedLabel'], ":IndeedWFH"=>$_REQUEST['IndeedWFH']);
    $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params));
}

$req_info       =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists, IndeedLabelID, IndeedWFH", $OrgID, $RequestID);

//Free Job Board List
$TemplateObj->free_jobboard_list    =   $free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);
//Get labels list
$TemplateObj->labels_list           =   $labels_list        =   $LabelsObj->getAllLabels($OrgID);

if($ServerInformationObj->getRequestSource() == 'ajax') {
	require_once IRECRUIT_DIR . 'views/shopping/LinkPurchase.inc';
}
else {
	echo $TemplateObj->displayIrecruitTemplate ( 'views/shopping/LinkPurchase' );
}
?>
