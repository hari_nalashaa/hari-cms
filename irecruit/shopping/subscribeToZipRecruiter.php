<?php
require_once '../Configuration.inc';

if(isset($_POST['subscribe']) && $_POST['subscribe'] == "plus") {
    $status = "Inserted";
}
else if(isset($_POST['subscribe']) && $_POST['subscribe'] == "minus") {
    $status = "Deleted";
}

//Update Zip Recruiter Job Category
if(isset($_REQUEST['ZipRecruiterJobCategory']) && $_REQUEST['ZipRecruiterJobCategory'] != "") {
    $set_info   = array("ZipRecruiterJobCategory = :ZipRecruiterJobCategory");
    $where_info = array("OrgID = :OrgID", "RequestID = :RequestID");
    $params     = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":ZipRecruiterJobCategory"=>$_REQUEST['ZipRecruiterJobCategory']);
    $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params));
}

$subscription_settings = $ZipRecruiterObj->getZipRecruiterSubscriptionSettingsInfo($OrgID);
$subscriptions_count = $ZipRecruiterObj->getZipRecruiterSubscriptionsCount($OrgID);

if($subscription_settings['TotalSubscriptions'] > 0) {

    if($status == "Deleted") {
        $ZipRecruiterObj->delZipRecruiterSubscriptions($OrgID, $_POST['RequestID']);
        $ZipRecruiterObj->insZipRecruiterSubscriptionHistory($OrgID, $_POST['RequestID'], $status);
        $message = 'Deleted';
    }
    else if($status == "Inserted") {
        //Subscription allowed only if they have the
        if($subscriptions_count < $subscription_settings['TotalSubscriptions']) {
            if($status == "Inserted") {
                $ZipRecruiterObj->insZipRecruiterSubscriptions($OrgID, $_POST['RequestID']);
                $ZipRecruiterObj->insZipRecruiterSubscriptionHistory($OrgID, $_POST['RequestID'], $status);
                
                $req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $_POST['RequestID']);
                $free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);
                $free_count         =   count($free_jobboard_list);
                
                for($j = 0; $j < $free_count; $j++) {
                    if($free_jobboard_list[$j] == "ZipRecruiter") unset($free_jobboard_list[$j]);
                }
                
                $free_jobboard_list = @array_unique($free_jobboard_list);
                
                $free_jobboard_list = @array_values($free_jobboard_list);
                
                $where_info     =   array("OrgID = :OrgID", "RequestID = :RequestID");
                $params_info    =   array(":OrgID"=>$OrgID, ":RequestID"=>$_POST['RequestID'], ":FreeJobBoardLists"=>json_encode($free_jobboard_list));
                $set_info       =   array("FreeJobBoardLists = :FreeJobBoardLists");
                
                $result_info    =   $RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
                
                $message = 'Success';
            }
        }
        else {
            $message = 'AlreadyUsed';
        }
    }
}
else {
    $message = 'NoSubscriptions';
}

echo json_encode(array("message"=>$message, "total_subscriptions"=>$subscription_settings['TotalSubscriptions'], "requisition_subscriptions"=>$subscriptions_count));
?>