<?php
require_once '../Configuration.inc';

$user_preferences = G::Obj('IrecruitUserPreferences')->getUserTheme();

$navbar_color = "";

if(isset($user_preferences)) {
    $navbar_color = $user_preferences['TableRowHeader'];
}

$title      = 'General Purchase';
$process    = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';

include_once IRECRUIT_VIEWS . 'shopping/GeneralPurchase.inc';
?>