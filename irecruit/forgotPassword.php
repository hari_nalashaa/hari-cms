<?php
$PAGE_TYPE	= "Default";
$IRECRUIT_USER_AUTH	=	FALSE;
require_once 'Configuration.inc';

//Set page title
$TemplateObj->title = $title =	"Forgot Password";

if(isset($_GET['ctact']) && $_GET['ctact'] != "")
{
	$where     =   array("ForgotPasswordAccessKey = :ForgotPasswordAccessKey");
	$params    =   array(":ForgotPasswordAccessKey"=>$_GET['ctact']);
	$TemplateObj->user_info_by_session = $user_info_by_session = $IrecruitUsersObj->getUserInformation("*", $where, "", array($params));
}

if(isset($_POST['update_password']) && $_POST['update_password'] == "Y" && isset($_POST['ProcessForgotPassword']) && $_POST['ProcessForgotPassword'] == 'Submit') {

	$TemplateObj->error_message = $error_message = "";
	
	if($_POST['confirmpassword'] == "" || $_POST['password'] == "") {
		$TemplateObj->error_message = $error_message = "Please fill the passwords";
	}
	else if($_POST['confirmpassword'] != $_POST['password']) {
		$TemplateObj->error_message = $error_message = "Sorry your passwords are not matched";
	}
	else if($_POST['confirmpassword'] == $_POST['password']) {
		$encrypt_password = password_hash($_POST['password'], PASSWORD_DEFAULT);
		//Update password based on session id
		$IrecruitUsersObj->updPassword($_GET['ctact'], $encrypt_password);
		//Update session id to blank by user name
		$IrecruitUsersObj->updUserSession('', $user_info_by_session['results'][0]['UserID']);
		
		header("Location:forgotPassword.php?msg=suc");
		exit();
	}
}

if($_POST['process_forgot_password'] == 'Y') {
	$user_info =   $IrecruitUsersObj->getUserInfoByUserID($_REQUEST['user'], 'EmailAddress, FirstName, LastName, OrgID');
	$to        =   $user_info['EmailAddress'];
	$first     =   $user_info['FirstName'];
	$last      =   $user_info['LastName'];
	$OrgID     =   $user_info['OrgID'];
	
	$TemplateObj->username_exists = $username_exists = "false";
	
	if(isset($to) && $to != "")
	{
	    $OE = $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, "");
	    $OrgName = $OrganizationDetailsObj->getOrganizationName($OrgID, "");
	    	    
		$TemplateObj->username_exists = $username_exists = "true";
		
		$random_session = crypt(time().rand(), rand().time().crypt(time(), rand(1, 20)));
		$random_session = preg_replace("/[^a-zA-Z0-9]/", "", $random_session);
		
		$message = "Dear ".$first. " " . $last  . ",<br><br>";
		$message .= "We received a request to reset your iRecruit password.<br><br>";
		$message .= "<a href='".IRECRUIT_HOME."forgotPassword.php?ctact=$random_session'>Click here to reset password</a><br><br>";
		$message .= "<strong>Password Tips: </strong>Passwords should have at least eight characters and include at least one uppercase character and at least one number.<br>";
		$message .= "If you didn't request a password reset. Please ignore this message.<br><br>";
		$message .= "Thank you,<br>";
		$message .= "iRecruit Support<br>";
		$message .= "<a href='https://www.iRecruit-Software.com'>www.iRecruit-Software.com</a><br>";
		
		$IrecruitUsersObj->updUserSession($random_session, $_REQUEST['user']);
	
        //Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
	
		// Set who the message is to be sent to
		$PHPMailerObj->addAddress ( $to );
		// Set the subject line
		$PHPMailerObj->Subject = "iRecruit forgot password request";
		// Content Type Is HTML
		$PHPMailerObj->ContentType = 'text/html';
		
		if (($OE['Email']) && ($OE['EmailVerified'] == 1)) {
    		// Set who the message is to be sent from
    		$PHPMailerObj->setFrom ( $OE['Email'], $OrgName );
    		// Set an alternative reply-to address
    		$PHPMailerObj->addReplyTo ( $OE['Email'], $OrgName );
        }
		
		// convert HTML into a basic plain-text alternative body
		$PHPMailerObj->msgHTML ( $message );
		
		if (DEVELOPMENT != "Y") {
			
			// send the message, check for errors, anyway we will not execute this file in browser
			if (! $PHPMailerObj->send ()) {
				Logger::writeMessage(ROOT_DIR."logs/mail-errors/".date('Y-m-d')."_errors.log", "Unable to send mail to user Email:$to. FileName: forgotPassword.php", "a+");
			}
		}
	}
}


echo $TemplateObj->displayIrecruitTemplate('views/ForgotPassword');
?>
