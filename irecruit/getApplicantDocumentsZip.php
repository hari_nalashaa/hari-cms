<?php
require_once 'Configuration.inc';

//reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

//instantiate and use the dompdf class
$options    =   new Options();
$options->setIsRemoteEnabled(true);
$options->set('enable_html5_parser', true);

require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

if(!is_dir("zips")) {
    mkdir("zips");
    chmod("zips", 0777);
}

if(isset($_REQUEST['ApplicationID'])) $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID'])) $RequestID = $_REQUEST['RequestID'];
$FormID = G::Obj('ApplicantDetails')->getFormID($OrgID, $ApplicationID, $RequestID);

//Get predefined options list
$zip_options    =   array("resumeupload", "coverletterupload", "otherupload", "custom", "appview", "apphistory", "iconnectforms","checklist");
$get_params     =   array("OrgID"=>$OrgID, "UserID"=>$USERID);
$where_params   =   array("OrgID = :OrgID", "UserID = :UserID");
$doc_option_res =   G::Obj('IrecruitApplicationFeatures')->getDocumentsZipDownloadOptions("DocumentsList", $where_params, "", array($get_params));

if(isset($doc_option_res['results'][0]['DocumentsList'])
    && $doc_option_res['results'][0]['DocumentsList'] != "") {
	//Update with user settings
	$zip_options = json_decode($doc_option_res['results'][0]['DocumentsList'], true);
}

// Set where condition
$formque_where  =   array (
                    	"OrgID             =   :OrgID",
                    	"FormID            =   :FormID",
                    	"SectionID         =   6",
                    	"QuestionTypeID    =   8",
                    	"Active            =   'Y'"
                    );
// set attachments condition
if(!in_array("custom", $zip_options)) {
    $formque_where[]  =  "QuestionID IN ('resumeupload', 'coverletterupload', 'otherupload')";
}
// Set parameters
$formque_params =   array (
                    	":OrgID"           =>  $OrgID,
                    	":FormID"          =>  $FormID
                    );
$resultsA       =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "QuestionID, value, Required", $formque_where, "QuestionOrder", array ($formque_params) );

$type_attachments = array();
if (is_array ( $resultsA ['results'] )) {
	foreach ( $resultsA ['results'] as $AA ) {
		
	    //Push the QuestionID if the custom option is checked.
	    if(in_array("custom", $zip_options)
            &&  $AA ['QuestionID']   !=  "resumeupload"
            &&  $AA ['QuestionID']   !=  "coverletterupload"
            &&  $AA ['QuestionID']   !=  "otherupload") {
	        $zip_options[]   =   $AA ['QuestionID'];
	    }	    
	    	     
		if(in_array($AA ['QuestionID'], $zip_options)) {
			// Set attachment parameters
			$attach_params       =   array (
                                        ":OrgID"           =>   $OrgID,
                                        ":ApplicationID"   =>   $ApplicationID,
                                        ":TypeAttachment"  =>   $AA ['QuestionID']
                                        );
			// Set attachment where condition
			$attach_where        =   array (
                                        "OrgID             =    :OrgID",
                                        "ApplicationID     =    :ApplicationID",
                                        "TypeAttachment    =    :TypeAttachment"
                                        );
			$results_attachments =   G::Obj('Attachments')->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );
			$type_attachments[]  =   $results_attachments['results'][0];
		}
		
	}
}	

for($t = 0; $t < count($type_attachments); $t++) {
	
	if (isset($type_attachments[$t]['OrgID']) 
	    && isset($type_attachments[$t]['ApplicationID']) 
	    && isset($type_attachments[$t]['TypeAttachment'])) {
		//Set where condition
		$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "TypeAttachment = :TypeAttachment");
		//Set parameters
		$params   =   array(":OrgID"=>$type_attachments[$t]['OrgID'], ":ApplicationID"=>$type_attachments[$t]['ApplicationID'], ":TypeAttachment"=>$type_attachments[$t]['TypeAttachment']);
		//Get Attachments 
		$results  =   G::Obj('Attachments')->getApplicantAttachments ( array ('PurposeName', 'FileType' ), $where, "", array($params) );
		
		if(is_array($results ['results'])) {
			foreach ( $results ['results'] as $img ) {
				$purposename    =   $img ['PurposeName'];
				$filetype       =   $img ['FileType'];
			}
		}
	
		$dfile = '';
	
		if (($purposename != "") && ($filetype != "")) {
			$dfile = IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments/' . $ApplicationID . '-' . $purposename . '.' . $filetype;
		}
	
		if (file_exists ( $dfile )) {
			$file_names[] = $dfile;
		}
	}
}

$zip_name = "zips/".$OrgID.time()."-applicant_documents.zip";

@touch($zip_name);
@chmod($zip_name, 0777);

$zip = new ZipArchive();
$available_to_download = false;
if ($zip->open($zip_name, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
	
	$i = 0;
	if(is_array($file_names)) {
		foreach($file_names as $file_path_name)
		{
			$ext         =   pathinfo($file_path_name, PATHINFO_EXTENSION); // to get extension
			$fname       =   pathinfo($file_path_name, PATHINFO_FILENAME); //file name without extension
			$file_name   =   $fname.".".$ext;
		
			$zip->addFile('vault/'.$OrgID.'/applicantattachments/'.$file_name, $file_name);
			$available_to_download = true;
		}
	}
}

//Create Temporary Files For Application View and History
global $AccessCode, $USERID;

$return_value = "true";

$applicationdata  = '';
$applicationdata .= "<style>";
$applicationdata .= "
* {
	font-family: 'Open Sans', Arial;
	font-size: 13px;
}
.application_detail_view {
	padding: 0px 0px 0px 12px;
}
.application_detail_view p {
	margin: 0px;
	padding: 0px;
}
.application_detail_view hr {
	border: 0px;
	margin-bottom: 15px;
	margin-top: 15px;
}
.application_detail_view .title {
	text-decoration: underline !important;
	font-weight: normal !important;
	font-size: 18px !important;
}
";

$applicationdata .= "</style>";
$applicationdata .= "<div class='application_detail_view'>";
$applicationdata .= include IRECRUIT_VIEWS . '/applicants/PrintApplicationView.inc';
$applicationdata .= "</div>";

$historydata = "
<style type=\"text/css\">
<!--
* 
{
    font-family: \"Open Sans\", Arial;
    font-size:13px;
}   
body {
  font-size: 10pt;
  background: #ffffff;
}

table {
  padding: 5px;
  border-width: 0px;
}

b.red {
font-size: 10pt;
font-style: bold;
color: red;
}
-->
</style>
";

$historydata .= '<table border="1" cellspacing="l" cellpadding="0" width="100%">';
$historydata .= '<tr>';
$historydata .= '<td colspan="4">Applicant History for ' . $ApplicationID . '</td>';
$historydata .= '</tr>';
$historydata .= '<tr>';
$historydata .= '<td width="5%">';
$historydata .= '<b>Date</b>';
$historydata .= '</td>';
$historydata .= '<td width="5%">';
$historydata .= '<b>Status</b>';
$historydata .= '</td>';
$historydata .= '<td width="5%">';
$historydata .= '<b>Updated By</b>';
$historydata .= '</td>';
$historydata .= '<td width="5%">';
$historydata .= '<b>Comments</b>';
$historydata .= '</td>';
$historydata .= '</tr>';

//Set columns
$columns            =   "ProcessOrder, date_format(Date,'%Y-%m-%d %H:%i EST') as Date1, UserID, Comments, DispositionCode";
//Set where condition
$where_job_app_his  =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
//Set parameters
$params_job_app_his =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
//Get JobApplication History Information
$results            =   G::Obj('JobApplicationHistory')->getJobApplicationHistoryInfo($columns, $where_job_app_his, '', 'Date DESC', array($params_job_app_his));

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {

		$ProcessOrder     =   G::Obj('ApplicantDetails')->getProcessOrderDescription ( $OrgID, $row ['ProcessOrder'] );
		$Date             =   $row ['Date1'];
		$UserID           =   $row ['UserID'];
		$Comments         =   $row ['Comments'];
		$disposition      =   G::Obj('ApplicantDetails')->getDispositionCodeDescription ( $OrgID, $row ['DispositionCode'] );

		$historydata     .=   '<tr><td valign="top">' . $Date . '</td><td valign="top">' . $ProcessOrder;
		if ($disposition) {
			$historydata .= '<br>' . $disposition;
		}
		$historydata     .=   '</td>';
		$historydata     .=   '<td valign="top">' . $UserID . '</td><td valign="top">' . $Comments . '</td></tr>';
	}
}

$historydata .= '</table>';

//checklist
$checklistdata = "
<style type=\"text/css\">
<!--
*
{
    font-family: \"Open Sans\", Arial;
    font-size:13px;
}   
body {
  font-size: 10pt;
  background: #ffffff;
}

table {
  border-collapse: collapse;
}
th {
  text-align: left;
}
-->
</style>

";

$get_checklist_header =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);

$application_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("ApplicantSortName", $OrgID, $ApplicationID, $RequestID);

$checklistdata .= '<h3 class="page-header">';
$checklistdata .= displayHeader ( $ApplicationID, $RequestID, "No" );
$checklistdata .= '</h3>';

$checklistdata .= G::Obj('ChecklistData')->getChecklistView($OrgID, $ApplicationID, $RequestID, $get_checklist_header['ChecklistID']);


//		
$can_del_app_his = false;
$can_del_app_view = false;

if(in_array("appview", $zip_options)) {
    
//    if(($OrgID == 'I20200305') || ($OrgID == 'I20190101')) { // Piper Aircraft, Farmington Country Club
        $appdata_pdf_file  =   "ApplicantData-".$OrgID."-".$ApplicationID."-".$RequestID.rand().time()."."."pdf";
        $dompdf = new Dompdf($options);
        //Create PDF
        $dompdf->loadHtml($applicationdata);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents("zips/".$appdata_pdf_file, $output);
        @chmod("zips/".$appdata_pdf_file, 0777);
        $zip->addFile("zips/".$appdata_pdf_file, $appdata_pdf_file);
        $can_del_app_view = true;
        $available_to_download = true;
	/*
    }
    else {
        $appdata_file_name = "ApplicantData-".$OrgID."-".$ApplicationID."-".$RequestID.rand().time()."."."html";
        $appdata_file = fopen("zips/".$appdata_file_name, "w");
        fwrite($appdata_file, $applicationdata);
        fclose($appdata_file);
        @chmod("zips/".$appdata_file_name, 0777);
        $zip->addFile("zips/".$appdata_file_name, $appdata_file_name);
        $can_del_app_view = true;
        $available_to_download = true;
    }
	 */
}

if(in_array("apphistory", $zip_options)) {
	$history_file_name = "ApplicantHistory-".$OrgID."-".$ApplicationID."-".$RequestID.rand().time()."."."html";
	$history_file = fopen("zips/".$history_file_name, "w");
	fwrite($history_file, $historydata);
	fclose($history_file);
	@chmod("zips/".$history_file_name, 0777);
	$zip->addFile("zips/".$history_file_name, $history_file_name);
	$can_del_app_his = true;
	$available_to_download = true;
}

if(in_array("checklist", $zip_options) && !empty($get_checklist_header['ChecklistID'])) {
	$checklist_data_file = "Checklist-".$OrgID."-".$ApplicationID."-".$RequestID.rand().time()."."."html";
	$checklist_file = fopen("zips/".$checklist_data_file, "w");
	fwrite($checklist_file, $checklistdata);
	fclose($checklist_file);
	@chmod("zips/".$checklist_data_file, 0777);
	$zip->addFile("zips/".$checklist_data_file, $checklist_data_file);
	$can_del_app_his = true;
	$available_to_download = true;
}

if(in_array("applicant_vault", $zip_options)) {
	
	// Bind the parameters
	$params = array (
			":OrgID"          =>  $OrgID,
			":ApplicationID"  =>  $ApplicationID,
			":RequestID"      =>  $RequestID
	);
	// Set the condition
	$where = array (
			"OrgID            =   :OrgID",
			"ApplicationID    =   :ApplicationID",
			"RequestID        =   :RequestID"
	);
	
	// Set Columns
	$columns    =   "ApplicationID, FileName, FileType, date_format(Date,'%Y-%m-%d') Date, UpdateID, UserID";
	// Get Applicant Vault Information
	$results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( $columns, $where, 'FileName', array ($params) );
	// Get Count Applicant Vault Information Records Count
	$cnt        =   $results ['count'];
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $AV ) {

			// get contents of a file into a string
			$file_name	=	$AV ['ApplicationID'] . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '.' . $AV ['FileType'];
			$filename	=	IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantvault/' . $file_name;
			
			$handle		=	fopen($filename, "r");
			$contents	=	fread($handle, filesize($filename));
			fclose($handle);
			
			$vault_file_name	=	"ApplicantVault-".$OrgID."-".$ApplicationID."-".$RequestID."-".$AV ['FileName'].".". $AV ['FileType'];
			
			$vault_file			=	fopen("zips/".$vault_file_name, "w");
			fwrite($vault_file, $contents);
			fclose($vault_file);
			@chmod("zips/".$vault_file_name, 0777);
			$zip->addFile("zips/".$vault_file_name, $vault_file_name);

			$available_to_download	=	true;

		} // end foreach
	}
}

if(in_array("iconnectforms", $zip_options)) {
    $columns        =   "UniqueID, FormName, FormType";
    $where_info     =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = '1'", "Status >= 3", "FormType IN ('Agreement', 'PreFilled', 'WebForm')");
    $params_info    =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);

    $assigned_completed_forms_res   =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo($columns, $where_info, "", "", array($params_info));
    $assigned_completed_forms       =   $assigned_completed_forms_res['results'];
    $assigned_completed_forms_cnt   =   count($assigned_completed_forms_res['results']);
    
    foreach($assigned_completed_forms as $assigned_completed_forms_list) {
        $form_data  =   '';
        
        $iconnect_form_css  = '<style>
        * 
        {
            font-family: "Open Sans", Arial;
            font-size:13px;
        }';
        $iconnect_form_css  .= '</style>';
        
        $iConnectUniqueID   =   $assigned_completed_forms_list['UniqueID'];
        $iConnectFormName   =   preg_replace("/[^a-z0-9.]+/i", "", $assigned_completed_forms_list['FormName']);

        if($assigned_completed_forms_list['FormType'] == "WebForm") {
    	    require_once COMMON_DIR . 'formsInternal/PrintWebFormView.inc';
    	    
    	    $form_data .= displayHeader($ApplicationID, $RequestID, "No");
    	    $form_data .= printWebForm ( $OrgID, $ApplicationID, $RequestID, $iConnectUniqueID );
    	}
    	else if($assigned_completed_forms_list['FormType'] == "Agreement") {
    	    
    	    require_once COMMON_DIR . 'formsInternal/PrintAgreementFormView.inc';
    	    
    	    $form_data .= displayHeader($ApplicationID, $RequestID, "No");
    	    $form_data .= printAgreementForm ( $OrgID, $ApplicationID, $RequestID, $iConnectUniqueID );
    	}
    	else if($assigned_completed_forms_list['FormType'] == "PreFilled") {
    	    require_once IRECRUIT_DIR . 'formsInternal/WritePDF.inc';

    	    // Create Directories
    	    $dir = IRECRUIT_DIR . 'zips';
    	    if (! file_exists ( $dir )) {
    	        mkdir ( $dir, 0700 );
    	        chmod ( $dir, 0777 );
    	    }
    	    
    	    $form_data .= displayHeader($ApplicationID, $RequestID, "No");
    	    $form_data .= createPDFi9($dir, $iConnectUniqueID, $ApplicationID, $RequestID);
    	}
        
    	if($form_data != "") {
    	    
    	    if($assigned_completed_forms_list['FormType'] == "PreFilled") {
    	        
    	       $path_info = pathinfo($form_data);
    	       $zip->addFile("zips/".$path_info['basename'], $path_info['basename']);
    	       
    	       $available_to_download = true;
    	    }
    	    else {
    	        if(($OrgID == 'I20200305') || ($OrgID == 'I20190101')) {  // Piper Aircraft, Farmington Country Club
    	            $iconnect_pdf_file     =   $iConnectFormName."-".$OrgID."-".$ApplicationID."-".$RequestID."-".$iConnectUniqueID."."."pdf";
    	            
    	            $dompdf = new Dompdf($options);
    	            //Create PDF
    	            $dompdf->loadHtml($form_data);
    	            $dompdf->setPaper('A4', 'portrait');
    	            $dompdf->render();
    	            $output = $dompdf->output();
    	            file_put_contents("zips/".$iconnect_pdf_file, $output);
    	            @chmod("zips/".$iconnect_pdf_file, 0777);
    	            
    	            $zip->addFile("zips/".$iconnect_pdf_file, $iconnect_pdf_file);
    	            
    	            $available_to_download = true;
    	        }
    	        else {
    	            $iconnect_file_name = $iConnectFormName."-".$OrgID."-".$ApplicationID."-".$RequestID."-".$iConnectUniqueID."."."html";
    	            $iconnect_file = fopen("zips/".$iconnect_file_name, "w");
    	            fwrite($iconnect_file, $form_data);
    	            fclose($iconnect_file);
    	            @chmod("zips/".$iconnect_file_name, 0777);
    	            $zip->addFile("zips/".$iconnect_file_name, $iconnect_file_name);
    	            
    	            $available_to_download = true;
    	        }
    	    }
    	    
    	}
    	
    }
}

$zip->close ();

function downloadFile($file) 
{
	if(file_exists($file)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
		@chmod($file, 0777);
		@unlink($file);
		exit;
	}
}

if($can_del_app_view == true) @unlink("zips/".$appdata_file_name);
if($can_del_app_his == true) @unlink("zips/".$history_file_name);

if($available_to_download == true) downloadFile($zip_name);
?>
