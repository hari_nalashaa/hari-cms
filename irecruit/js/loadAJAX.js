function loadLocation(MultiOrgID, RequestID, AccessCode, iRecruitHome) {

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			if(typeof(document.getElementById("data")) != 'undefined' 
				&& document.getElementById("data") != null) {
				var validation_star = 'Please select at least one: <span style="color:red">*</span>';
				document.getElementById("data").innerHTML = validation_star + xmlhttp.responseText;
			}
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("GET", irecruit_home 
			+ "requisitions/loadLocation.php?MultiOrgID=" + MultiOrgID
			+ "&RequestID=" + RequestID + "&new=" + d + "&k=" + AccessCode,
			true);
	xmlhttp.send();

} // end function

function loadRequisitionLocation(MultiOrgID, RequestID, AccessCode, iRecruitHome, OrganizationLocation) {

	var d = new Date().getTime();
	input_data = {"MultiOrgID":MultiOrgID, "RequestID":RequestID, "new":d, "k":AccessCode, "OrganizationLocation":OrganizationLocation};
	
	$.ajax({
		async: true,   // this will solve the problem
		method: "POST",
  		url: iRecruitHome + "requisitions/loadLocation.php",
		type: "POST",
		data: input_data,
		success: function(data) {
			if(typeof(document.getElementById("data")) != 'undefined' 
				&& document.getElementById("data") != null) {
				document.getElementById("data").innerHTML = data;
			}
    	}
	});

} // end function


function showRequisitions(json) {
	document.getElementById("requests").innerHTML = json.rslts;
}

function loadRequisitions(iRecruitHome, AccessCode) {
	var d = new Date().getTime();
	var a = document.forms['Search'].Active.value;
	var b = document.forms['Search'].MultiOrgID.value;
	var c = document.forms['Search'].criteria.value;
	var r = document.forms['Search'].refinereq.value;

	/*
	 * var url = iRecruitHome + "applicants/loadRequisitions.php?callback=?";
	 * //var url = "https://dev.irecruit-us.com/irecruit/edge/edge.php"; // The
	 * server URL $.getJSON( url, {MultiOrgID: b, Active: a, criteria: c, k:
	 * AccessCode}, showRequisitions);
	 */

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("requests").innerHTML = xmlhttp.responseText;
		}
	}

	xmlhttp.open("GET", iRecruitHome
			+ "applicants/loadRequisitions.php?MultiOrgID=" + b + "&Active=" + a
			+ "&criteria=" + c + "&refine=" + r + "&new=" + d + "&k="
			+ AccessCode, true);

xmlhttp.timeout = 600000; // Set timeout to 4 seconds (4000 milliseconds)
xmlhttp.ontimeout = function () { alert("Timed out!!!"); }

	xmlhttp.send();

} // end function

function loadTwoPartFormQuestions(IFQuestionID, ADQuestionID, AgreementFormID,
		AccessCode) {

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("twopartform").innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", "formsInternal/twoPartForm.php", true);
	xmlhttp.setRequestHeader("Content-type",
			"application/x-www-form-urlencoded");
	xmlhttp.send("AgreementFormID=" + AgreementFormID + "&ADQuestionID="
			+ ADQuestionID + "&IFQuestionID=" + IFQuestionID + "&k="
			+ AccessCode + "&d=" + d);

} // end function

function DisplayPrimaryForms(formpart, d) {

	if (formpart == 'Secondary') {
		document.getElementById(d).style.display = "block";
	} else {
		document.getElementById(d).style.display = "none";
	}

} // end function


function getInternalFormsPullDown(ApplicationID, PresentedTo, DivID) {
	//Set InnerHtml
	$("#"+DivID).html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
	
	var xmlhttp;
	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			$("#"+DivID).html(xmlhttp.responseText);
		}
	}

	var page_url = window.location.href;
	
	if(page_url.indexOf("formsInternal/") > 1) {
		request_url = "getInternalFormsPullDown.php?PresentedTo="+PresentedTo+"&ApplicationID="+ApplicationID;
	}
	else {
		request_url = "formsInternal/getInternalFormsPullDown.php?PresentedTo="+PresentedTo+"&ApplicationID="+ApplicationID;
	}
		
	xmlhttp.open("GET", request_url, true);
	xmlhttp.send();
}

function loadPresentedTo(PresentedTo, AccessCode, FormNameID, PresentedToDiv) {

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById(PresentedToDiv).innerHTML = xmlhttp.responseText;
		}
	}

	var a = document.forms[FormNameID].InternalForm.value;
	var Form = a.split("|");
	var d = new Date().getTime();

	var page_url = window.location.href;
	
	if(page_url.indexOf("formsInternal/") > 1)
		request_url = "presentedTo.php?PresentedTo=" + PresentedTo + "&Restrict=" + Form[3] + "&new=" + d + "&k=" + AccessCode;
	else 
		request_url = "formsInternal/presentedTo.php?PresentedTo=" + PresentedTo + "&Restrict=" + Form[3] + "&new=" + d + "&k=" + AccessCode;
		
	xmlhttp.open("GET", request_url, true);
	xmlhttp.send();

} // end function

function loadExporterForm(app, action) {

	var expform = "";
	var exporterid = "";

	document.getElementById("ExporterItems").innerHTML = "";
	if (action == "ExporterName") {
		expform = $('#exportername').serialize();

	} else if (action == "ExporterID") {
		expform = $('#exporterid').serialize();
		// exporterid = document.forms['exporterid'].ExporterID.value;

	} else if (action == "ExporterSchedulerName") {
		expform = $('#exporterschedulername').serialize();

	} else if (action == "ExporterSchedulerID") {
		expform = $('#exporterschedulerid').serialize();

	} else if (action == "UpdateExporterSchedule") {
		expform = $('#exporterschedulerid').serialize();

	} else {
		expform = $('#exporterform').serialize();

	}

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("ExporterForm").innerHTML = xmlhttp.responseText;
			if (app == "exporterForm.php") {
				//To handle the ExportID value if the form doesn't exist
				if(typeof(document.forms['exporterid']) != "undefined") {
					loadExporterItems(document.forms['exporterid'].ExporterID.value);
				}				
			} else {
			  again_cal();
			}
		}
	}


	var d = new Date().getTime();
	xmlhttp.open("POST", irecruit_home+"reports/exporter/" + app, true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(expform + "&action=" + action + "&d=" + d);

} // end function

function loadExporterItems(ExporterID) {

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("ExporterItems").innerHTML = xmlhttp.responseText;
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("POST", irecruit_home+"reports/exporter/exporterItems.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("ExporterID=" + ExporterID + "&d=" + d);

} // end function

function LoadChecklistHistory(OrgID, GetApplicationID, GetRequestID, GetChecklistID, irecruit_home) {


	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			if(typeof(document.getElementById("ChecklistHistoryData")) != 'undefined' 
				&& document.getElementById("ChecklistHistoryData") != null) {
				
				document.getElementById("ChecklistHistoryData").innerHTML = xmlhttp.responseText;
			}
		}
	}

	var d = new Date().getTime();

	xmlhttp.open("GET", irecruit_home 
			+ "applicants/ChecklistHistory.php?OrgID=" + OrgID + "&ApplicationID=" + GetApplicationID +
			 "&RequestID=" + GetRequestID + "&ChecklistID=" + GetChecklistID + "&new=" + d ,
			true);
	xmlhttp.send();

} // end function

function exporterAction(orgid, action, item, message) {

	var doaction = "N";

	var fn = "";

	if (action == 'fnupdate') {
		fn = document.getElementById(item).value;
	}

	if (message != '') {
		var r = confirm(message);
		if (r == true) {
			doaction = "Y";
		}
	} else {
		doaction = "Y";
	}

	if (doaction == "Y") {

		var xmlhttp;
		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera,
									// Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
				if (action == "deleteExport") {
					loadExporterItems(document.forms['exporterid'].ExporterID.value);
					loadExporterForm('exporterForm.php', '');
				} else if (action == "deleteSchedule") {
					loadExporterForm('exporterScheduler.php', '');
				} else {
					loadExporterItems(document.forms['exporterid'].ExporterID.value);
				}

			}
		}

		var d = new Date().getTime();

		xmlhttp.open("POST", irecruit_home+"reports/exporter/exporterAction.php", true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send("?OrgID=" + orgid + "&action=" + action + "&item=" + item + "&fn=" + fn + "&d=" + d);
	} // end doaction

} // end function

function exportData() {

	
	var ExportTo = $('input[name="ExportTo"]:checked').val();
	var ExporterFileID = document.forms['exporter'].ExporterID.value;
	var ExporterType = $('input[name="TypeExport"]:checked').val();
	var url = 'reports/exporter/exportData.php?ExporterFileID=' + ExporterFileID + '&ExporterType=' + ExporterType;
	var expform = "";
	expform = $('#exporter').serialize();

	var xmlhttp;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("ExporterItems").innerHTML = xmlhttp.responseText;
			$("#export_progress_msg").html("");
		}
	}

	var d = new Date().getTime();

	if (ExportTo == "FILE") {
		$("#export_progress_msg").html("&nbsp;Email will be sent to you with download link after the file has exported.");
	} else {
		$("#export_progress_msg").html("&nbsp;Please wait ...")
	}
	xmlhttp.open("POST", irecruit_home+"reports/exporter/exportData.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send(expform + "&d=" + d);

} // end function

function updateInterAssignedFormSortOrder(forms_data, OrgID, FormID, FormType, SortOrder) {
	
	$("#sort_internal_forms_msg").html("Please wait.. ");
	$.ajax({
		 method: "POST",
		 url: "updateInterAssignedFormSortOrder.php",
		 data: forms_data,
		 success : function () {
			 $("#sort_internal_forms_msg").html("Successfully updated the forms order");
		 }
	});
}
