function confirmDeleteFormQuestion(page_source, form, action, section, question_id, question, onboard_source) {
	var confirm_msg = 'Are you sure you want to delete the following question?\n\n' + question + '\n\n';
	if (confirm(confirm_msg)) {
		if(page_source == 'administration')
			location.href = 'administration.php?form=' + form + '&action=' + action + '&section=' + section + '&delete=' + question_id + '&subactiondelete=yes&OnboardSource='+onboard_source;
		else
			location.href = 'forms.php?form=' + form + '&action=' + action + '&section=' + section + '&delete=' + question_id;
	} else {
		return false;
	}
}


var forms_data = {
		forms_info: []
};

$( "#sort_onboard_questions tbody" ).sortable({
	cursor: 'move',
	placeholder: 'ui-state-highlight',
    stop: function( event, ui ) {
        
    	forms_data = {
    			forms_info: []
    	};
    	
    	$(this).find('tr').each(function(i) {

        	var SortOrder = i;
            SortOrder++;
            var tr_id = $(this).attr('id');
			var tr_info = tr_id.split("*");
           
            var FormID = tr_info[1];
            var QuestionID = tr_info[2];
        	
            if(FormID != "NoSort") {
            	forms_data.forms_info.push({
	           		 "FormID"    	: FormID,
	           		 "QuestionID"  	: QuestionID,
	           		 "SortOrder" 	: SortOrder
            	});
            }
        });
        
    	updateOnboardQuestionsSortOrder(forms_data);
    }
});


function updateOnboardQuestionsSortOrder(forms_data) {
	
	$("#sort_onboard_questions_msg").html("Please wait.. ");
	$.ajax({
		 method: "POST",
		 url: "onboard/updateOnboardQuestionsSortOrder.php",
		 data: forms_data,
		 success : function () {
			 $("#sort_onboard_questions_msg").html("Successfully updated the forms order");
		 }
	});
}