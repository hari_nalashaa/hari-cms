function setSelectedValue(selectObj, valueToSet, objName) {
	
    for (var i = 0; i < selectObj.options.length; i++) {
        if(objName == "ProcessOrder") {
        	if(valueToSet == "0") valueToSet = "";
        }
        if (selectObj.options[i].value == valueToSet) {
            selectObj.options[i].selected = true;
        }
    }
}

function processContactApplicant(btnObj) {
	tinyMCE.triggerSave();
	
	var form = $('#frmContactApplicant');
	$(btnObj).prop('disabled', true);
	
	$("#contact_applicant_msg_top").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	$("#contact_applicant_msg_bottom").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');

	var request = $.ajax({
		method: "POST",
  		url: 'applicants/processContactApplicant.php', 
		type: "POST",
		data: form.serialize(),
		success: function(data) {
			$("#contact_applicant_msg_top").html(data);
			$("#contact_applicant_msg_bottom").html(data);
			$(btnObj).prop('disabled', false);
    	}
	});
}

function getCorrespondencePackageInfo(package_name) {
	var form = $('#frmContactCorrespondencePkg');

	var request = $.ajax({
		method: "POST",
  		url: 'applicants/getCorrespondencePackageInfo.php', 
		type: "POST",
		data: form.serialize(),
		dataType: 'json',
		success: function(corres_pkg_info) {
			
			var attachments_obj	=   document.frmContactApplicant.attachments;
			//from email object
			var from_email_obj	=   document.frmContactApplicant.From;

			var from_email		=   '';
			if(typeof(corres_pkg_info.pkg_info) != 'undefined') {
				from_email		=	corres_pkg_info.pkg_info["FromEmail"];
			}

			if(typeof(from_email) == 'undefined' || from_email == '' || typeof(corres_pkg_info.pkg_info) == 'undefined') {
				from_email = corres_pkg_info['user_email'];
			}

			setSelectedValue(from_email_obj, from_email, "From");

			if(package_name != "")
			{
				var disp_code   =	corres_pkg_info.DispositionCode;
				var process_ord =	corres_pkg_info.ProcessOrder;
			    
				disp_code       =	disp_code.toString();
				process_ord     =	process_ord.toString();

				//ProcessOrder Object
				var ProcessOrderObj = document.frmContactApplicant.ProcessOrder;
				//DispositionCode Object
				var DispositionCodeObj = document.frmContactApplicant.DispositionCode;
				
				//Set Values based on package
				setSelectedValue(ProcessOrderObj, process_ord, "ProcessOrder");
				if(typeof(DispositionCodeObj) != 'undefined') {
					setSelectedValue(DispositionCodeObj, disp_code, "DispositionCode");
				}

				$('#subject').val(corres_pkg_info.subject);
				var editor = tinyMCE.get('body');
				editor.setContent(corres_pkg_info.body);
				$("#body").text(corres_pkg_info.body);
			}
			if(package_name == "") {
				$('#subject').val('');
				var editor = tinyMCE.get('body');
				editor.setContent('');
				$("#body").text('');
			}

			//Remove select dropdowns
			$("#attachments option").remove();

			var selected_attachments = corres_pkg_info.attachments_selected;
			
			if(typeof(corres_pkg_info.attachments_list) != "undefined") {
				for (var aselected in corres_pkg_info.attachments_list) {
					var option_attachment_slected = (selected_attachments.hasOwnProperty(aselected)) ? true : false;
					attachments_obj.options[attachments_obj.options.length] = new Option(corres_pkg_info.attachments_list[aselected], aselected, false, option_attachment_slected);
				}
			}
			
    	}
	}).done(function( data ) {
		tinyMCE.triggerSave();
	});
}