var radio_button_name = '';
var can_val_license_id = '';
var job_single_refresh_license_id = '';
var invetory_license_id = '';
var can_val_duration = '';
var can_val_location = '';
var job_single_refresh_duration = '';
var job_single_refresh_location = '';

function post_job(btn_obj) {
	
	var MonsterJobIndustry = document.getElementById('MonsterJobIndustry').value;
	var MonsterJobCategory = document.getElementById('MonsterJobCategory').value;
	var MonsterJobOccupation = document.getElementById('MonsterJobOccupation').value;
	var MonsterJobPostType = document.getElementById('MonsterJobPostType').value;
	var MonsterJobPostDuration = document.getElementById('MonsterJobPostDuration').value;
	
	if(MonsterJobIndustry != "" 
		&& MonsterJobCategory != "" 
		&& MonsterJobOccupation != "" 
		&& MonsterJobPostType != ""
		&& MonsterJobPostDuration != "") {
		
		var form_obj = $(btn_obj).parents('form:first');
		var job_posting_location = $(form_obj).find('input[name="job_location"]').val();
		
		if ( $("input[name='inventory_info']").is(':checked')) {
			
			$(':radio').each(function() {
					
			   var radio_button_name = $(this).attr("name");
			   var radio_button_value = $(this).val();
				   
	 		   if(radio_button_name == "can_info") {
	 			  if($(this).is(":checked")) {
	 				  can_val = radio_button_value.split(":");
	 				  can_val_license_id = can_val[0];
		 			  can_val_duration = can_val[1];
		 			  can_val_location = can_val[2];
	 	 		  }  				  
			   }
			   if(radio_button_name == "has_job_single_refresh") {
				   if($(this).is(":checked")) {
		 			  job_single_refresh = radio_button_value.split(":");
		 			  job_single_refresh_license_id = job_single_refresh[0];
		 			  job_single_refresh_duration = job_single_refresh[1];
		 			  job_single_refresh_location = job_single_refresh[2];
		 	 	   }
			   }
			   if(radio_button_name == "inventory_info") {
				   if($(this).is(":checked")) {
					   invetory_license_id = radio_button_value;
				   }	   
				   
			   }

			});

			if(can_val_location != "" && job_single_refresh_location != "") {
				if(can_val_location != job_single_refresh_location || job_posting_location != job_single_refresh_location || job_posting_location != can_val_location) {
					alert("Your CAN Duration, Has Job Single Refresh loacations and Inventory type location should be same");
					return false;
				}
			}
			if(can_val_location != "") {
				if(can_val_location != job_posting_location) {
					alert("Your CAN location, Inventory type location should be same");
					return false;
				}
			}
			if(job_single_refresh_location != "") {
				if(job_posting_location != job_single_refresh_location) {
					alert("Has Job Single Refresh loacation and Inventory type location should be same");
					return false;
				}
			}
			if(can_val_license_id != "" && invetory_license_id != can_val_license_id) {
				alert("Selected CAN Duration is not related to same License Type\nPlease select CAN Duration with same license type");
				return false;
	  	  	}
			if(job_single_refresh_license_id != "" && invetory_license_id != job_single_refresh_license_id) {
				alert("Selected 'Has Job Single Refresh' is not related to same License Type\nPlease select Has Job Single Refresh with same license type");
				return false;
		  	}

			var form_obj = $(btn_obj).parents('form:first');
			var input_data = $(form_obj).serialize();
			
			var request = $.ajax({
				method: "POST",
		  		url: "monster/postJobToMonster.php",
				type: "POST",
				data: input_data,
				beforeSend: function() {
					$("#post_job_status_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
				},
				success: function(data) {
					$("#post-job-to-monster").html(data);
		    	}
			});
			
		}
		else {
			alert("Please select type of inventory");
			return false;
		}
	}
	else {
		alert("One of these fields are missing (Post Type, Duration, Job Industry, Category, Occupation), please update these fileds");
		return false;
	}
	
}

function post_job_to_monster() {
	var job_posting_location = document.getElementById('job_location').value;
	
	if ( $("input[name='inventory_info']").is(':checked')) {
			
		$(':radio').each(function() {
			   var radio_button_name = $(this).attr("name");
			   var radio_button_value = $(this).val();
			   
	 		   if(radio_button_name == "can_info") {
	 			  if($(this).is(":checked")) {
	 				  can_val = radio_button_value.split(":");
	 				  can_val_license_id = can_val[0];
		 			  can_val_duration = can_val[1];
		 			  can_val_location = can_val[2];
	 	 		  }  				  
			   }
			   if(radio_button_name == "has_job_single_refresh") {
				   if($(this).is(":checked")) {
		 			  job_single_refresh = radio_button_value.split(":");
		 			  job_single_refresh_license_id = job_single_refresh[0];
		 			  job_single_refresh_duration = job_single_refresh[1];
		 			  job_single_refresh_location = job_single_refresh[2];
		 	 	   }
			   }
			   if(radio_button_name == "inventory_info") {
				   if($(this).is(":checked")) {
					   invetory_license_id = radio_button_value;
				   }	   
				   
			   }
		});

		if(can_val_location != "" && job_single_refresh_location != "") {
			if(can_val_location != job_single_refresh_location || job_posting_location != job_single_refresh_location || job_posting_location != can_val_location) {
				alert("Your CAN Duration, Has Job Single Refresh loacations and Inventory type location should be same");
				return false;
			}
		}
		if(can_val_location != "") {
			if(can_val_location != job_posting_location) {
				alert("Your CAN location, Inventory type location should be same");
				return false;
			}
		}
		if(job_single_refresh_location != "") {
			if(job_posting_location != job_single_refresh_location) {
				alert("Has Job Single Refresh loacation and Inventory type location should be same");
				return false;
			}
		}
		if(can_val_license_id != "" && invetory_license_id != can_val_license_id) {
			alert("Selected CAN Duration is not related to same License Type\nPlease select CAN Duration with same license type");
			return false;
  	  	}
		if(job_single_refresh_license_id != "" && invetory_license_id != job_single_refresh_license_id) {
			alert("Selected 'Has Job Single Refresh' is not related to same License Type\nPlease select Has Job Single Refresh with same license type");
			return false;
	  	}
		document.forms['frmJobPostDetails'].submit();
	}
	else {
		alert("Please select type of inventory");
		return false;
	}
}
function set_inventory_details(has_bolding_job, has_autorefresh_status_job, has_autorefresh_duration, posting_duration, inventory_license_id, location, inventory_job_board_id, monster_video_id, inventory_license_name) {

	if(inventory_license_name == "Local_Posting") {
		job_inventory_type_post = "transactional";
	}
	if(inventory_license_name == "Standard Reserved Jobs") {
		job_inventory_type_post = "slotted";
	}
	
	document.frmJobPostDetails.job_has_bold.value = has_bolding_job;
	document.frmJobPostDetails.job_auto_refresh_status.value = has_autorefresh_status_job;
	document.frmJobPostDetails.job_auto_refresh_duration.value = has_autorefresh_duration;
	document.frmJobPostDetails.job_posting_duration.value = posting_duration;

	document.frmJobPostDetails.job_inventory_license_id.value = inventory_license_id;
	document.frmJobPostDetails.job_location.value = location;
	document.frmJobPostDetails.job_board_id.value = inventory_job_board_id;
	
	document.frmJobPostDetails.job_video_monster_id.value = monster_video_id;
	document.frmJobPostDetails.job_inventory_type.value = job_inventory_type_post;
}