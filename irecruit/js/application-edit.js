function processApplicationInfo(btn_obj) {
	
	$("#edit_application_progress").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...');
	
	var form_id = $(btn_obj).parents("form").attr('id');
	var input_data = new FormData($('#'+form_id)[0]);

	$.ajax({
		method: "POST",
  		url: 'applicants/processApplicationFormInfo.php', 
		data: input_data,
		type: "POST",
		processData: false,
		contentType: false,
		success: function(data) {
			$("#edit_application_progress").html("<br>"+data);
    	}
	});
}

function getFormIDChildQuestionsInfo() {
	for (question_id in child_ques_info) {
		  var child_que_info =  child_ques_info[question_id];
		  for (question_id_option in child_que_info) {
			  if(que_types_list[question_id] == 2
				|| que_types_list[question_id] == 22
				|| que_types_list[question_id] == 23) {
					
                    var checked_radio = $("input[name='"+question_id+"']:checked").val();
                    
                    if(checked_radio == question_id_option)
                    {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                        	if(child_ques_list[child_ques_ids] == "show") {
                        		$("#divQue-"+child_ques_ids).show();
                        	}
                        	else if(child_ques_list[child_ques_ids] == "hidden") {
                        		$("#divQue-"+child_ques_ids).hide();
                        	}							
                        }
                    }
                    else if(typeof(checked_radio) == 'undefined') {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                            $("#divQue-"+child_ques_ids).hide();
                        }
                   }
			 }
			 else if(que_types_list[question_id] == 3) {
				  var checked_radio = $("#"+question_id).val();
				  //Exception for country question, have to handle it in simple way
				  if(question_id == 'country' && typeof(checked_radio) != 'undefined') {
					  if(checked_radio != 'US' && checked_radio != 'CA') {
						  	child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
					  }
					  else if(checked_radio == question_id_option)
	                  {
		                    child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
	                  }
				  }
				  else if(checked_radio == question_id_option)
                  {
	                    child_ques_list = child_que_info[question_id_option];
	                    for(child_ques_ids in child_ques_list) {
	                    	if(child_ques_list[child_ques_ids] == "show") {
	                    		$("#divQue-"+child_ques_ids).show();
	                    	}
	                    	else if(child_ques_list[child_ques_ids] == "hidden") {
	                    		$("#divQue-"+child_ques_ids).hide();
	                    	}							
	                    }
                  }
                  else if(typeof(checked_radio) == 'undefined') {
                      child_ques_list = child_que_info[question_id_option];
                      
                      for(child_ques_ids in child_ques_list) {
                      		$("#divQue-"+child_ques_ids).hide();
                      }
                 }
			 }
		  }
	}
}

$(document).ready(function() {
	getFormIDChildQuestionsInfo();
});