function getRequisitionsByStatus(RequisitionStatus) {
    $('#ddlRequisitions').children('option:not(:first)').remove();
	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;
    var option_text			=	'';
    var option_value		=	'';
    var options_list    	=   '';

	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);
	console.log(sel_requisitions);

    $.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?FilterOrganization="+FilterOrganization+"&RequisitionStatus="+RequisitionStatus,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#status_message").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		   += " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#status_message").html("");
			$("#ddlRequisitions").append(options_list);
    	}
	});
}

function sortApplicants(to_sort) {
	var IndexStart     		=   document.frmApplicantsParams.IndexStart.value;
	var FromDate       		=   document.frmApplicantsParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantsParams.applicants_to_date.value;
	var keyword        		=   document.frmApplicantsParams.search_word.value;
	var status         		=   document.frmApplicantsParams.ProcessOrder.value;
	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;
	
    document.frmSortOptions.ddlToSort.value = to_sort;

	var sort_type  			=   document.frmSortOptions.ddlSortType.value;

	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);
	
	if(sort_type == "DESC") {
		document.frmSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var RequestID   		=	document.getElementById('ddlRequisitions').value;
	var REQUEST_URL			=	irecruit_home + "services/getApplicants.php?FilterOrganization="+FilterOrganization+"&RequestID="+sel_requisitions+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&Status="+status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#applicants_results").find("tr:gt(0)").remove();
			applicants_status    =   "<tr>";
			applicants_status	+=	"<td colspan='9' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			applicants_status	+=	"</tr>";
			
			$("#applicants_results").append(applicants_status);
			
		},
		success: function(data) {
			setApplicantsList(data);
    	}
	});
}

$("#ddlRequisitions").change(function(){
    if ($(this).val() == "") {
    	$("#requisition_detail_information").find("tr:gt(0)").remove();
    }
});
    	           
function getRequisitionDetailsInfoByRequestID(RequestID) {

	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);

	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;
	var REQUEST_URL			=	irecruit_home + "reports/getRequisitionDetails.php?FilterOrganization="+FilterOrganization+"&RequestID="+sel_requisitions;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#requisition_detail_information").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='7' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#requisition_detail_information").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			
			$("#requisition_detail_information").find("tr:gt(0)").remove();
			
			var req_detail_info	= "";
			for(i in data) {
				
				var Title 				=	data[i].Title;
				var RequisitionID 		=	data[i].RequisitionID;
				var JobID 				=	data[i].JobID;
				var DaysOpen			=	data[i].Open;
				var PostDate 			=	data[i].PostDate;
				var ExpireDate 			=	data[i].ExpireDate;
				var ApplicantsCount		=	data[i].ApplicantsCount;
				var OrganizationName	=	data[i].OrganizationName;
				
				if(DaysOpen == null) DaysOpen = '0';
				
				req_detail_info += "<tr>";
				req_detail_info += "<td valign='top'>"+Title+"</td>";
				req_detail_info += "<td valign='top'>"+RequisitionID+" / "+JobID+"</td>";
				req_detail_info += "<td valign='top'>"+PostDate+"</td>";
				req_detail_info += "<td valign='top'>"+ExpireDate+"</td>";
				req_detail_info += "<td valign='top'>"+DaysOpen+"</td>";
				req_detail_info += "<td valign='top'>"+ApplicantsCount+"</td>";
				req_detail_info += "<td valign='top'>"+OrganizationName+"</td>";
				req_detail_info += "</tr>";
			}
				
			$("#requisition_detail_information").append(req_detail_info);
    	}
	});
}

function getRecordsByPage(IndexStart) {
	document.frmApplicantsParams.IndexStart.value = IndexStart;
	var to_sort        		=   document.frmSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmSortOptions.ddlSortType.value;
	var FromDate       		=   document.frmApplicantsParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantsParams.applicants_to_date.value;
	var keyword        		=   document.frmApplicantsParams.search_word.value;
	var status         		=   document.frmApplicantsParams.ProcessOrder.value;
	var RecordsLimit		=	document.frmApplicantsParams.RecordsLimit.value;
	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;
	
	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);
	
	var RequestID      		=   document.getElementById('ddlRequisitions').value;
	var REQUEST_URL    		=   irecruit_home + "services/getApplicants.php?FilterOrganization="+FilterOrganization+"&RequestID="+sel_requisitions+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&Status="+status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#applicants_results").find("tr:gt(0)").remove();
			applicants_list	 =	"<tr>";
			applicants_list	+=	"<td colspan='9' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			applicants_list	+=	"</tr>";
			
			$("#applicants_results").append(applicants_list);
			
		},
		success: function(data) {
			setApplicantsList(data);
    	}
	});
	
}

function getApplicantsByPageNumber(page_number) {
	
	var RecordsLimit = document.frmApplicantsParams.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}

function setApplicantsList(data) {
	var RecordsLimit		=	document.frmApplicantsParams.RecordsLimit.value;
	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;

	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);
	
	var app_prev         	=	data.previous;
	var app_next         	=	data.next;
	var total_pages      	=	data.total_pages;
	var current_page     	=	data.current_page;
	var applicants_count 	=	data.applicants_count;
	
	$("#applicants_results").find("tr:gt(0)").remove();
	$("#applicants-pagination").html("");
	
	var app_full_view_results	=	'';
	var data_length				=	data['applicants_list'].length;
	var applicants_list			=	data['applicants_list'];
	var applications_pagination =	"";
	
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {

			var Title               =	applicants_list[i].Title;
			var ReqIDJobID 		    =	applicants_list[i].RequisitionID + '/' + applicants_list[i].JobID;
			var ApplicationID 		=	applicants_list[i].ApplicationID;
			var ApplicantSortName 	=	applicants_list[i].ApplicantSortName;
			var ApplicantEmail 		=	applicants_list[i].Email;
			var EntryDate 			=	applicants_list[i].EntryDateMDY;
			var LastModified 		=	applicants_list[i].LastModifiedMDY;
			var ProcessOrder 		=	applicants_list[i].ProcessOrder;
			var DispositionCode     =	applicants_list[i].DispositionCode;
			var RequestID 			=	applicants_list[i].RequestID;
			var HomePhone			=	applicants_list[i].HomePhone;
			var IsInternalApplication	=	(typeof(applicants_list[i].IsInternalApplication) == 'undefined' || applicants_list[i].IsInternalApplication == '') ? 'No' : applicants_list[i].IsInternalApplication;

			
		    if(DispositionCode == null) DispositionCode = '';
			if(ApplicantEmail == null) ApplicantEmail = '';
			
			var app_request_id		=	ApplicationID+":"+RequestID;
			app_full_view_results += "<tr>";
			app_full_view_results += "<td><input type='checkbox' name='appids_list[]' id='app_req_id_"+app_request_id+"' value='"+app_request_id+"'></td>";
			app_full_view_results += "<td valign='top'>"+Title+"</td>";
			app_full_view_results += "<td valign='top'>"+ReqIDJobID+"</td>";
			app_full_view_results += "<td valign='top'><a href='"+irecruit_home+"applicantsSearch.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID+"' target='_blank'>"+ApplicationID+"</a></td>";
			app_full_view_results += "<td valign='top'>"+ApplicantSortName+"</td>";
			app_full_view_results += "<td valign='top'>"+EntryDate+"</td>";
			app_full_view_results += "<td valign='top'>"+ProcessOrder+"</td>";
			if(FeatureInternalRequisitions == 'Y') {
				app_full_view_results += "<td valign='top'>"+IsInternalApplication+"</td>";
			}
			app_full_view_results += "<td valign='top'>"+DispositionCode+"</td>";
			app_full_view_results += "<td valign='top'>"+LastModified+"</td>";
			app_full_view_results += "<td valign='top'>"+ApplicantEmail+"</td>";
			app_full_view_results += "<td valign='top'>"+HomePhone+"</td>";
			app_full_view_results += "</tr>";
			
		}

	    if(parseInt(applicants_count) > parseInt(RecordsLimit)) {
			
			applications_pagination += "<div class='row'>";
			
			applications_pagination += "<div class='col-lg-6'>";
			applications_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			applications_pagination += "</div>";

			applications_pagination += "<div class='col-lg-6' style='text-align:right'>";
			applications_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			applications_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getApplicantsByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			applications_pagination += "</div>";
			
			applications_pagination += "</div>";
			
			applications_pagination += "<div class='row'>";
			
			applications_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			applications_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			applications_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			applications_pagination += "</div>";
			
			applications_pagination += "</div>";

		}	

		$("#applicants-pagination").html(applications_pagination);
	}
	else {

		app_full_view_results += "<tr>";
		app_full_view_results += "<td colspan='8' align='center'>No records found</td>";
		app_full_view_results += "</tr>";

	}
	
	$("#applicants_results").append(app_full_view_results);
}

function getApplicantsByRequestID() {
	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);
	
	var IndexStart     		=   document.frmApplicantsParams.IndexStart.value = 0;
	var to_sort        		=   document.frmSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmSortOptions.ddlSortType.value;
	var FromDate       		=   document.frmApplicantsParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantsParams.applicants_to_date.value;
	var keyword        		=   document.frmApplicantsParams.search_word.value;
	var status         		=   document.frmApplicantsParams.ProcessOrder.value;
	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;
	
	document.frmApplicantsParams.CurrentPage.value = 1;	
	
	var RequestID 			=	document.getElementById('ddlRequisitions').value;
	
	if(RequestID != "") getRequisitionDetailsInfoByRequestID(RequestID);
	
	var REQUEST_URL = irecruit_home + "services/getApplicants.php?FilterOrganization="+FilterOrganization+"&RequestID="+sel_requisitions+"&IndexStart="+IndexStart+"&to_sort="+to_sort+"&sort_type="+sort_type+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&Status="+status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#applicants_results").find("tr:gt(0)").remove();
			app_full_view_results	 = "<tr>";
			app_full_view_results	+= "<td colspan='6' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			app_full_view_results	+= "</tr>";
			
			$("#applicants_results").append(app_full_view_results);
		},
		success: function(data) {
			setApplicantsList(data);
    	}
	});
}

function exportApplicants() {
	var RequestID      		=   document.getElementById('ddlRequisitions').value;
	var IndexStart     		=   document.frmApplicantsParams.IndexStart.value = 0;
	var FromDate       		=   document.frmApplicantsParams.applicants_from_date.value;
	var ToDate         		=   document.frmApplicantsParams.applicants_to_date.value;
	var keyword        		=   document.frmApplicantsParams.search_word.value;
	var to_sort        		=   document.frmSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmSortOptions.ddlSortType.value;
	var status         		=   document.frmApplicantsParams.ProcessOrder.value;
	var FilterOrganization	=	document.frmApplicantsParams.ddlOrganizationsList.value;
	
	var sel_requisitions	=	$('#ddlRequisitions option:selected').toArray().map(item => item.value);
	
	document.frmApplicantsParams.CurrentPage.value = 1;	
			
	var REQUEST_URL = irecruit_home + "services/getApplicants.php?FilterOrganization="+FilterOrganization+"&RequestID="+sel_requisitions+"&IndexStart="+IndexStart+"&Export=YES&FromDate="+FromDate+"&ToDate="+ToDate+"&Keyword="+keyword+"&to_sort="+to_sort+"&sort_type="+sort_type+"&Status="+status+"&PageSource=ApplicantsStatusByRequisition";
	
	document.frmApplicantsParams.action = REQUEST_URL;
	document.frmApplicantsParams.submit();
}

$(document).ready(function() {
    getApplicantsByRequestID();
});