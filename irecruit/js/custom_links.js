function validate_custom_links() {
	var link_title = document.getElementById('LinkTitle').value;
	var link_href  = document.getElementById('LinkTarget').value;
	
	var message = "";
	if(link_title == "") {
		message += "Link Title should not be empty<br>";
	}
	if(link_href == "") {
		message += "Link Target should not be empty<br>";
	}
	
	if(message != "") {
		document.getElementById('custom_links_error').setAttribute('class', 'alert alert-danger alert-dismissable');
		document.getElementById('custom_links_error').innerHTML = message;	
	}
	
	
	if(message != "") return false;
	return true;
}

function updateCustomLinksSortOrder(forms_data) {
	$("#custom_links_sort_message").html("Please wait.. ");
	$.ajax({
		 method: "POST",
		 url: "updateCustomLinksSortOrder.php",
		 data: forms_data,
		 success : function () {
			 $("#custom_links_sort_message").addClass('alert alert-success alert-dismissable');
			 $("#custom_links_sort_message").html("Successfully updated the links order");
		 }
	});
}

$(document).ready(function() {

	$( "#my_custom_links tbody" ).sortable({
		cursor: 'move',
		placeholder: 'ui-state-highlight',
	    stop: function( event, ui ) {
	    	var forms_data = {
	    			forms_info: []
	    	};
	        $(this).find('tr').each(function(i) {
				
	        	var SortOrder = i;
	            SortOrder++;
	            
	            var LinkID = $(this).attr('id');
	        	
	        	forms_data.forms_info.push({
	        		 "LinkID"    : LinkID,
	        		 "SortOrder" : SortOrder
	        	});
	        	
	        });
	        
	        updateCustomLinksSortOrder(forms_data);
	    }
	});

});