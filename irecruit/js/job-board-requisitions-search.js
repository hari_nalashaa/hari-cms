/*var address_info_error_msg = 'Your job requisition requires the City, State, and Zip Code fields to proceed.'; */
var address_info_error_msg = 'Your job requisition requires a State selection to proceed.';

function toggleVerticalResultsText() { 
	if(document.getElementById('search-results-detail').style.display == "block")
	{
		document.getElementById('search-results-detail').style.display = 'none';
		$('#applicant-detail-view').show();
		$('#requisitions-minified-search-results').show();
	}
	else if(document.getElementById('search-results-detail').style.display == "none")
	{
		document.getElementById('search-results-detail').style.display = 'block';
		$('#applicant-detail-view').hide();
		$('#requisitions-minified-search-results').hide();
	}
	
}

//Make the first letter capital in string
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//Set error message based on response
function setError(error_message, display_id) {
	error_message = capitalizeFirstLetter(error_message);
	$(display_id).html("<br>"+"<h3>"+error_message+"</h3>");
}

//Set ajax error information
function setAjaxErrorInfo(request, display_id) {
	request.fail(function(jqXHR, textStatus, errorThrown) {
		if(textStatus === 'timeout')
	    {
	        setError('Sorry, unable to complete your request. Please try again.', display_id)
	    }
		else {
			setError(textStatus+": "+errorThrown, display_id);
		}
	});
	request.error(function( jqXHR, textStatus, errorThrown ) {
		if(textStatus === 'timeout')
	    {     
	        setError('Sorry, unable to complete your request. Please try again.', display_id)
	    }
		else {
			setError(textStatus+": "+errorThrown, display_id);
		}
	});
}

function getExportRequisitionInformation() {

	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request_listing_url = "requisitions/exportRequisition.php?RequestID="+req_id;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		beforeSend: function(){
			$("#export-requisition").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#export-requisition").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#export-requisition");
}

function getSocialMediaRequisitionInformation() {

	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request_listing_url = "requisitions/socialMediaRequisition.php?RequestID="+req_id;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}

	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		beforeSend: function(){
			$("#socialmedia-requisition-body").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#socialmedia-requisition-body").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#socialmedia-requisition'");
}

function getRepostRequisitionInformation() {

	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request_listing_url = "requisitions/repostRequisition.php?OriginalRequestID="+req_id;
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		beforeSend: function() {
			$("#repost-requisition").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#repost-requisition").html("<h4>Repost:</h4>"+data);
    	}
	});
	
	setAjaxErrorInfo(request, "#repost-requisition");
}

function getRequisitionTabInfo(req_id, multi_org_id) {  
	document.frmRequisitionDetailInfo.RequestID.value = req_id;
	document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;
	
	getRequisitionDetailInfo(req_id, multi_org_id);
}

function getRequisitionDetailInfo(req_id, multi_org_id) {

	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var validation_error_msg = '<br>&nbsp;&nbsp;'+address_info_error_msg;
	validation_error_msg += '<br><br>&nbsp;&nbsp;<a href="'+irecruit_home+'requisitionsSearch.php?menu=3&RequestID='+req_id+'&active_tab=edit-requisition&validate_errors_list=csz">Click here to view requisitions</a>';

	$("#job_boards_processing_msg").removeAttr("class");
	
	if(typeof(MonsterJobIndustries) == 'string') MonsterJobIndustries = JSON.parse(MonsterJobIndustries); 
	if(typeof(JobCategories) == 'string') JobCategories = JSON.parse(JobCategories);
	if(typeof(JobOccupations) == 'string') JobOccupations = JSON.parse(JobOccupations);

	$("#process_message_req_details").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	$("#process_message_req_details").attr("class", "alert alert-info");
	
	document.frmRequisitionDetailInfo.RequestID.value = req_id;
	document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;
	var active = document.frmRequisitionDetailInfo.Active.value;
	var GuIDRequisition = document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;
	
	var req_search_res_len = 0;
	if(typeof(requisitions_list) == 'string') {
		var req_search_res = JSON.parse(requisitions_list);
		req_search_res_len = req_search_res.length;
	}
	else if(typeof(requisitions_list) == 'object') {
		var req_search_res = requisitions_list;
		req_search_res_len = requisitions_list.length;
	}
	
	if(req_search_res_len > 0) {
		if(req_id == req_search_res[0].RequestID) {
			$("#req_detail_prev").hide();
		}
		if(req_id != req_search_res[0].RequestID) {
			$("#req_detail_prev").show();
		}
		if(req_id == req_search_res[req_search_res_len - 1].RequestID) {
			$("#req_detail_next").hide();
		}
		if(req_id != req_search_res[req_search_res_len - 1].RequestID) {
			$("#req_detail_next").show();
		}
		
		var request_listing_url = "requisitions/getRequisitionDetailInfo.php?RequestID="+req_id+"&MultiOrgID="+multi_org_id+"&Active="+active+"&GuIDRequisition="+GuIDRequisition;
		       var request = $.ajax({
			method: "POST",
	  		url: request_listing_url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				
				var Address1	= '';
				var Address2	= '';
				var City 		= '';
				var State 		= '';
				var ZipCode 	= '';
				
				if(typeof(data.Address1) != 'undefined' || data.Address1 != null) {
					Address1 = data.Address1;
				}
				if(typeof(data.Address2) != 'undefined' || data.Address2 != null) {
					Address2 = data.Address2;
				}
				if(typeof(data.City) != 'undefined' || data.City != null) {
					City = data.City;
				}
				if(typeof(data.State) != 'undefined' || data.State != null) {
					State = data.State;
				}
				if(typeof(data.ZipCode) != 'undefined' || data.ZipCode != null) {
					ZipCode = data.ZipCode;
				}
				
				document.frmRequisitionDetailInfo.Address1.value	= Address1;
				document.frmRequisitionDetailInfo.Address2.value	= Address2;
				document.frmRequisitionDetailInfo.City.value		= City;
				document.frmRequisitionDetailInfo.State.value		= State;
				document.frmRequisitionDetailInfo.ZipCode.value		= ZipCode;

				if(data.IsPaid == "Yes") {
					var free_ziprecruiter_msg = "";
					
					if(data.ZipRecruiterTrafficBoost == "boost") {
						
						free_ziprecruiter_msg += '<br><img src="images/ziprecruiter_jobboard.png">';
						free_ziprecruiter_msg += '&nbsp;&nbsp;&nbsp;';
				    
						free_ziprecruiter_msg += 'This item is already sponsored on ZipRecruiter '+data.ZipRecruiterTrafficBoost+'.<br><br><br>';
					}
					else {
						free_ziprecruiter_msg += '<br><img src="images/ziprecruiter_jobboard.png">';
						free_ziprecruiter_msg += '&nbsp;&nbsp;&nbsp;';
				    
						free_ziprecruiter_msg += 'This item is already sponsored on ZipRecruiter subscription.<br><br><br>';

					}
					
					$('#free-ziprecruiter-feed').html(free_ziprecruiter_msg);
				}
				
				/*if(City == '' || State == '' || ZipCode == '') { */
				if(State == '') {
					$('#free-posting-indeed').html(validation_error_msg);
					$('#free-ziprecruiter-feed').html(validation_error_msg);
				}
				
				if(data.FreeJobBoardLists != "") {
					var FreeJobListings = JSON.parse(data.FreeJobBoardLists);
					var chk_val = '';
					
					$('.free_job_board_lists').each( function () {
						chk_val = $(this).val();
						
						if(typeof(FreeJobListings) !== 'undefined' && FreeJobListings != null) {
							if(FreeJobListings.indexOf(chk_val) >= 0) {
								$(this).prop('checked', true);
							}
							else {
								$(this).prop('checked', false);
							}
						}
					});

				}
				else {
					$('.free_job_board_lists').each(function () {
						$(this).prop('checked', false);
					});
				}
				
				$("#header_requisition_title").html('<a href="'+irecruit_home+'requisitionsSearch.php?menu=3&RequestID='+req_id+'">'+data.Title+'</a>');
				$("#header_requisition_post_date").html(data.PostDate);
				$("#header_requisition_expire_date").html(data.ExpireDate);
				$("#header_requisition_requisition_id").html(data.RequisitionID);
				$("#header_requisition_job_id").html(data.JobID);
				$("#header_number_of_applicants").html(data.ApplicantsCount);
				
				if(data.MonsterJobIndustry == 0) {
					$("#header_monster_job_post_industry").html("All");
					document.frmRequisitionDetailInfo.MonsterJobIndustry.value = data.MonsterJobIndustry;
				}
				if(data.MonsterJobIndustry != "") {
					$("#header_monster_job_post_industry").html(MonsterJobIndustries[data.MonsterJobIndustry]);
					document.frmRequisitionDetailInfo.MonsterJobIndustry.value = data.MonsterJobIndustry;
				}
				
				if(data.MonsterJobIndustry === "") {
					$("#header_monster_job_post_industry").html("");
					document.frmRequisitionDetailInfo.MonsterJobIndustry.value = "";
				}
				
				if(data.feature.MonsterAccount != "Y") {
					$("#requisition_monster_information").hide();
					$("#header_monster_job_post_type, #header_monster_job_post_duration, #header_monster_job_post_occupation, #header_monster_job_post_category").html();
				}
				else {
					$("#requisition_monster_information").show();
					$("#header_monster_job_post_type").html(data.MonsterJobPostType);
					$("#header_monster_job_post_duration").html(data.Duration);
					
					document.frmRequisitionDetailInfo.MonsterJobPostDuration.value = data.Duration;
					document.frmRequisitionDetailInfo.MonsterJobPostType.value = data.MonsterJobPostType;
					
					if(typeof(data.MonsterJobCategory) != 'undefined'
						&& data.MonsterJobCategory != '') {
						joboccupations(data.MonsterJobCategory, req_id, data.MonsterJobOccupation);
						$("#header_monster_job_post_category").html(JobCategories[data.MonsterJobCategory]);
						document.frmRequisitionDetailInfo.MonsterJobCategory.value = data.MonsterJobCategory;
					}
					else {
						$("#header_monster_job_post_category").html("");
						document.frmRequisitionDetailInfo.MonsterJobCategory.value = "";
					}

					//MonsterJobOccupation
					if(typeof(data.MonsterJobOccupation) != 'undefined'
						&& data.MonsterJobOccupation != '') {
						$("#header_monster_job_post_occupation").html(JobOccupations[data.MonsterJobOccupation]);
						document.frmRequisitionDetailInfo.MonsterJobOccupation.value = data.MonsterJobOccupation;
					}
					else {
						$("#header_monster_job_post_occupation").html("");
						document.frmRequisitionDetailInfo.MonsterJobOccupation.value = "";
					}
				}
				
				if(data.Facebook != "" || data.Twitter != "" || data.Linkedin != "") { 
					$("#share_facebook").html("&nbsp;"+data.Facebook+"<br><br>");
					$("#share_twitter").html("&nbsp;"+data.Twitter+"<br><br>");
					$("#share_linkedin").html("&nbsp;"+data.Linkedin+"<br><br>");
				}
				
				if(data.Facebook == "" && data.Twitter == "" && data.Linkedin == "") {
					$("#share_facebook").html("There is no data available");
					$("#share_twitter").html("");
					$("#share_linkedin").html("");
				}
				
				$("#process_message_req_details").html('');
				$("#process_message_req_details").removeAttr('class');

				$('#FreeZipRecruiterJobCategory option[value="'+data.ZipRecruiterJobCategory+'"]').prop('selected', true);

				getSelectedTabInfo(selected_tab, req_id);

	    	}
		});
		
		setAjaxErrorInfo(request, "#process_message_req_details");
	}
	else {
		$("#requisitions-minified-results").html("<div class='col-lg-12'><strong>No results found. Please update your search filters and try again.</strong></div>");
		$("#process_message_req_details").html("");
		$("#process_message_req_details").removeAttr('class');
	}
	
	getAssignedFormsCount();
}

function getAssignedFormsCount() {
	
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request = $.ajax({
		method: "POST",
  		url: "requisitions/getAssignedFormsCount.php?RequestID="+req_id,
		type: "POST",
		success: function(data) {
			$("#header_number_of_assign_forms").html(data);
    	}
	});

	setAjaxErrorInfo(request, "#header_number_of_assign_forms");
}

function delRequisition(req_id, req_title, clicked_object) {

	if(confirm('Are you sure you want to delete the following requisition?'+"\n"+req_title)) {
		$(clicked_object).closest('tr').remove();
		
		var active = document.frmRequisitionDetailInfo.Active.value;
		
		var request = $.ajax({
			method: "POST",
	  		url: "requisitions/delRequisition.php?RequestID="+req_id+"&action=delete&process=Y&Active="+active,
			type: "POST",
			dataType: "json",
			beforeSend: function() {
				$("#del_process_message").attr('class', '');
				$("#del_process_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');	
			},
			success: function(data) {
				if(data.status == "failed") {
					$("#del_process_message").html("Failed to delete this requisition");
					$("#del_process_message").css({
				      "color": data.color,
				    });
				}
				if(data.status == "success") {
					
					getRequisitionsListByFilters('yes');
					
					$("#del_process_message").html(data.message);
					$("#del_process_message").css({
				      "color": data.color,
				    });
				}
	    	}
		});
		setAjaxErrorInfo(request, "#del_process_message");
	}
	
}

function getPurchaseZipRecruiterFeedInfo(show_msg) {

	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var org_id = document.frmRequisitionDetailInfo.OrgID.value;
	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;

	var request_url = irecruit_home + "shopping/zipRecruiterPurchase.php?OrgID="+org_id+"&RequestID="+req_id;
	if(access_code != "") request_url += "&k="+access_code;

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		beforeSend: function() {
			$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-zip-recruiter-feed").html(data);
			if(show_msg == 'yes') {
				$("#job_boards_processing_msg").attr("class", "alert alert-info");
				$("#job_boards_processing_msg").html("Successfully Updated");
			}
    	}
	});
	setAjaxErrorInfo(request, "#purchase-zip-recruiter-feed");
}

function addToBasketReqZipRecruiterFeed(btn_obj) {
	
	var form_obj	=	$(btn_obj).parents('form:first');
	var input_data	=	$(form_obj).serialize();

	var zip_rec_job_cat = document.forms['frmZipRecruiterPurchase'].ZipRecruiterJobCategory.value;

	if(zip_rec_job_cat == "") {
		alert("Please select ziprecruiter job category.");
		return false;
	}
	
	var request_url =	"shopping/zipRecruiterPurchase.php";
	
	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function() {
			$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-zip-recruiter-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-zip-recruiter-feed");
}

function viewZipRecruiterBasket(request_url) {
	
	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		beforeSend: function(){
			$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-zip-recruiter-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-zip-recruiter-feed");
}

function removeItemFromZipRecruiterBasket(delete_id, requisition_title) {

	if(confirm('Are you sure you want to delete the following item?\n\n' + requisition_title)) {
		
		var request = $.ajax({
			method: "POST",
	  		url: "shopping/zipRecruiterBasket.php?delete="+delete_id,
			type: "POST",
			beforeSend: function(){
				$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
			},
			success: function(data) {
				$("#purchase-zip-recruiter-feed").html(data);
	    	}
		});
		setAjaxErrorInfo(request, "#purchase-zip-recruiter-feed");
	}
}

function getPurchasePremiumFeedInfo(show_msg, display) {

	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var org_id = document.frmRequisitionDetailInfo.OrgID.value;
	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;

	var request_url = irecruit_home + "shopping/linkPurchase.php?OrgID="+org_id+"&RequestID="+req_id+"&display="+display;
	if(access_code != "") request_url += "&k="+access_code;

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		beforeSend: function(){
			$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		    $("#job_boards_processing_msg").removeAttr("class");
		    $("#job_boards_processing_msg").html("");
		},
		success: function(data) {
			$("#purchase-premium-feed").html(data);
			if(show_msg == 'yes') {
			    $("#job_boards_processing_msg").attr("class", "alert alert-info");
			    $("#job_boards_processing_msg").html("Successfully Updated");
			}
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}

function removeItemFromBasket(delete_id, requisition_title, div_id) {

	var request_url = "shopping/basket.php?delete="+delete_id;
	
	if(div_id == "basket_payment_info")	{
		request_url += "&cpage=purchases";
	}
	
	if(confirm('Are you sure you want to delete the following item?\n\n' + requisition_title)) {
		
		var request = $.ajax({
			method: "POST",
	  		url: request_url,
			type: "POST",
			beforeSend: function(){
				$("#"+div_id).html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
			},
			success: function(data) {
				$("#"+div_id).html(data);
	    	}
		});
		
		setAjaxErrorInfo(request, "#purchase-premium-feed");
	}
}

function viewBasket(request_url) {
	
	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		beforeSend: function(){
			$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-premium-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}

function purchaseFeed(btn_obj, div_id) {
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var request_url = "shopping/basket.php";
	if(div_id == "basket_payment_info")	{
		request_url += "?cpage=purchases";
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#"+div_id).html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
		},
		success: function(data) {
			$("#"+div_id).html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}

function purchaseZipRecruiterFeed(btn_obj) {
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var request_url = "shopping/zipRecruiterBasket.php";

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-zip-recruiter-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-zip-recruiter-feed");
}

function addToBasketReqPremFeed(btn_obj) {
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var request_url = "shopping/linkPurchase.php";
	
	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-premium-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}

function endIndeedCampaign(btn_obj) {

	var form_obj	=	$(btn_obj).parents('form:first');
	var input_data	=	$(form_obj).serialize();

	var request_url =	"shopping/linkPurchase.php";
	
	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function() {
			$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-premium-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}

function sponsorIndeedRequisition(btn_obj) {
	var form_obj	=	$(btn_obj).parents('form:first');
	var input_data	=	$(form_obj).serialize();
	
	var request_url =	"shopping/linkPurchase.php";

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function() {
			$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-premium-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}


function getPaidMonster(div_id) {
	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var request_monster_url = "";
	
	request_monster_url = "requisitions/validatePaidMonsterJobPost.php?RequestID="+req_id+"&Active="+active+"&MultiOrgID="+multi_org_id;

	var request = $.ajax({
		method: "POST",
  		url: request_monster_url,
		type: "POST",
		beforeSend: function() {
			$("#"+div_id).html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(mon_data) {
			if(mon_data == "Failed") {
				$("#"+div_id).html("<h4>Sorry you don't have permission to post this job<h4>");
			}
			else {
				var monster_data = '<img src="'+irecruit_home+'images/monster_jobboard.jpg"><br>';
				monster_data += mon_data;
				$("#"+div_id).html(monster_data);
			}
    	}
	});
}

function getFreeMonster(div_id) {
	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var request_monster_url = "";

	request_monster_url = "requisitions/validateFreeMonsterJobPost.php?RequestID="+req_id+"&Active="+active+"&MultiOrgID="+multi_org_id;
	
	var request = $.ajax({
		method: "POST",
  		url: request_monster_url,
		type: "POST",
		beforeSend: function() {
			$("#"+div_id).html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(mon_data) {
			if(mon_data == "Failed") {
				$("#"+div_id).html("<h4>Sorry you don't have permission to post this job<h4>");
			}
			else {
				$("#"+div_id).html(mon_data);
			}
    	}
	});
}


function postJobToMonster(div_id) {
	
	var active	= document.frmRequisitionDetailInfo.Active.value;
	var req_id	= document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var request_monster_url = "";
	
	if(div_id == 'post-job-to-monster')
	{
		request_monster_url = "requisitions/validateMonsterJobPost.php?RequestID="+req_id+"&Active="+active+"&MultiOrgID="+multi_org_id;
	}
	else if(div_id == 'free-post-job-to-monster')
	{
		request_monster_url = "requisitions/validateFreeMonsterJobPost.php?RequestID="+req_id+"&Active="+active+"&MultiOrgID="+multi_org_id;
	}
	

	var request = $.ajax({
		method: "POST",
  		url: request_monster_url,
		type: "POST",
		beforeSend: function() {
			$("#"+div_id).html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(mon_data) {
			if(mon_data == "Failed") {
				$("#"+div_id).html("<h4>Sorry you don't have permission to post this job<h4>");
			}
			else {
				monster_data = mon_data;
				$("#"+div_id).html(monster_data);
			}
    	}
	});
	setAjaxErrorInfo(request, "#post-job-to-monster");
}

function processRequisitions(process_action, btn_obj) {

	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();
	
	var chkLength = $('input.check_requisitions:checked').length; 
	
	if(chkLength > 0) {

		var d = new Date();
		var milli_secs = d.getTime();
		var min = 10000;
		var max = 100000000;
		var rand_int = Math.floor(Math.random() * (max - min)) + min;
		
		if(process_action == 'Inactive') {
			
			var request = $.ajax({
				method: "POST",
		  		url: "requisitions/deActivateRequisitions.php",
				type: "POST",
				data: input_data,
				beforeSend: function() {
					$("#process_requisitions").html('<br><br>loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
					$("#process_requisitions").css({
				      "color": nav_color,
				      "text-align": "center",
				    });
				},
				success: function(data) {
					
					getRequisitionsListByFilters('yes');

					$("#process_requisitions").css({
				      "color": nav_color,
				      "text-align": "center",
				    });
					$("#process_requisitions").html(data);
		    	}
			});
			
		}
		else if(process_action == 'Repost') {
			$(form_obj).attr('action', 'repostedRequisitions.php?req_guid='+milli_secs+rand_int);
			$(form_obj).submit();
		}
	}
	else
	{
		alert("Please select the requisition to process");
	}	
}

function setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart) {
	var json_data			= 	JSON.parse(data);
	requisitions_list		= 	json_data.requisitions_search_list;
	var req_search_res_len	=	json_data.results.length;
	var req_data			= 	json_data.results;
	var app_prev			= 	json_data.previous;
	var app_next			= 	json_data.next;
	var total_pages			= 	json_data.total_pages;
	var current_page		= 	json_data.current_page;
	var req_min_results		=	"";
	var req_full_results	=	"";
	
	var feature_monster_account = document.frmRequisitionDetailInfo.FeatureMonsterAccount.value;
	document.frmRequisitionDetailInfo.RequisitionsCount.value = json_data.requisitions_count;
	document.frmRequisitionDetailInfo.CurrentPage.value = current_page;
	
	$("#requisitions_full_view_results").find("tr:gt(0)").remove();
	var feature_monster_account = document.frmRequisitionDetailInfo.FeatureMonsterAccount.value;
	
	for (var i = 0; i < req_search_res_len; i++) {
		var req_id = req_data[i].RequestID;
		var mul_id = req_data[i].MultiOrgID;
		var req_title = req_data[i].Title;
		
		var display_copy_option = "true";
		
		if(req_data[i].Active == "R" || req_data[i].Approved == "N")
		{
			display_copy_option = "false";
		}

		req_min_results += "<div class='col-lg-12'><a href='javascript:void(0);' onclick=\"getRequisitionTabInfo('"+req_id+"', '"+mul_id+"')\">"+req_title+"</a></div>";
		
		req_full_results += "<tr>";

		req_full_results += "<td valign='top'>";
		req_full_results += req_data[i].OrganizationName+"<br>";
		req_full_results += req_data[i].RequisitionID+"/"+req_data[i].JobID;
		
		req_full_results += "<br>";
		req_full_results += req_data[i].Title;
		req_full_results += "</td>";

		var FreeJobBoardLists = req_data[i].FreeJobBoardLists;
		if(req_data[i].FreeJobBoardLists == null) FreeJobBoardLists = '';

		req_full_results += '<td align="left">'+FreeJobBoardLists+'</td>';
		
		/*if(req_data[i].City == "" || req_data[i].State == "" || req_data[i].ZipCode == "") { */
		if(req_data[i].State == "") {
			req_full_results += "<td valign='bottom' align='left'>";
			/*req_full_results += "Please complete city, state, zipcode"; */
			req_full_results += "Please complete state";
			req_full_results += '<br><a href="'+irecruit_home+'requisitionsSearch.php?menu=3&RequestID='+req_id+'&active_tab=edit-requisition&validate_errors_list=csz">Click here to update requisition information</a>';
			req_full_results += "</td>";
			req_full_results += "<td>&nbsp;</td>";
		}
		else {
			
			if(feature_monster_account == 'Y') {
				req_full_results += "<td valign=\"bottom\" align=\"left\">";

            	if(req_data[i].MonsterJobPostType === ''
                   || req_data[i].MonsterJobIndustry === ''
                   || req_data[i].MonsterJobCategory === ''
                   || req_data[i].MonsterJobCategory === 0
                   || req_data[i].MonsterJobOccupation === 0
                   || req_data[i].MonsterJobOccupation === '') {
            		req_full_results += "Please fill monster information"; 
                }
                else {
                	req_full_results += 'N/A';
                }
            	
            	req_full_results += "</td>";
            }
            else {
            	req_full_results += "<td valign=\"bottom\" align=\"left\">N/A</td>";
            }
			req_full_results += "<td><input type='checkbox' name='chk_req["+req_data[i].RequestID+"]' id='chk_req_"+req_data[i].RequestID+"' class='check_requisitions'></td>";
		}
		
		req_full_results += "</tr>";		

	}
	
	
	if(parseInt(json_data.requisitions_count) > parseInt(RequisitionsLimit)) {
		$("#requisitions_full_view_results").find("tr:gt(0)").remove();
		
		req_min_results += '<div class="col-lg-12">';
	  	
		req_min_results += '<div class="row">';
		req_min_results += '<div class="col-lg-6"><strong>Per page:</strong> '+RequisitionsLimit+'</div>';
		req_min_results += '<div class="col-lg-6" style="text-align:right">';
		req_min_results += '<input type="text" name="current_page_number" id="current_page_number" value="'+current_page+'" style="width:50px;text-align:center" maxlength="4">';
		req_min_results += ' <input type="button" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" class="btn-small" onclick="getRequisitionsByPageNumber(document.getElementById(\'current_page_number\').value, \'\', \'\', \'no\')">';
		req_min_results += '</div>';
		req_min_results += '</div>';
		
		
		req_min_results += '<div class="row">';
		req_min_results += '<div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">';
		req_min_results += app_prev;
		req_min_results += '</div>';
		req_min_results += '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
		req_min_results += current_page + " - " + total_pages;
	  	req_min_results += '</div>';
	  	req_min_results += '<div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">';
	  	req_min_results += app_next;
	  	req_min_results += '</div>';
	  	req_min_results += '</div>';		
		
		req_min_results += '</div>';
		
		req_full_results += '<tr>';
		req_full_results += '<td colspan="100%">';
		req_full_results += '<div style="padding:0px;margin-top:0px;overflow:hidden;">';
		req_full_results += '<div style="text-align:center;width:100%;float: left;border:0px solid #f5f5f5;padding-top:5px;padding-bottom:5px;height:40px">';
		req_full_results += '<div style="float:left">&nbsp;<strong>Per page:</strong> '+RequisitionsLimit+'</div>';
		req_full_results += '<div style="float:right">';
		req_full_results += '<input type="text" name="current_page_fullview_number" id="current_page_fullview_number" value="'+current_page+'" style="width:50px;text-align:center" maxlength="4">';
		req_full_results += ' <input type="button" name="btnFullViewPageNumberInfo" id="btnFullViewPageNumberInfo" value="Go" class="btn-small" onclick="getRequisitionsByPageNumber(document.getElementById(\'current_page_fullview_number\').value, \'\', \'\', \'no\')">';
		req_full_results += '</div>';
		req_full_results += '</div>';
		req_full_results += '<div id="span_fullview_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left;padding-left:0px;">'+app_prev+'</div>';
		req_full_results += '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
		req_full_results += current_page+" - "+total_pages;
		req_full_results += '</div>';
		req_full_results += '<div id="span_fullview_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right;padding-right:0px;">'+app_next+'</div>';
		req_full_results += '</td>';
		req_full_results += '</tr>';
		
	}
	else {
		req_full_results += '<tr><td colspan="100%"><input type="hidden" name="current_page_fullview_number" id="current_page_fullview_number" value="1"></td></tr>';
	}

	if(display_copy_option == "true") {
		req_full_results += '<tr>';
		req_full_results += '<td colspan="100%">';
		req_full_results += '<select name="ddlProcessRequisitions" id="ddlProcessRequisitions" class="form-control width-auto-inline">';
		req_full_results += '<option value="">Select Action</option>';
		req_full_results += '<option value="Repost">Repost</option>';
		req_full_results += '</select>';
		req_full_results += '&nbsp;';
		req_full_results += '<input type="button" name="btnProcessRequisitions" id="btnProcessRequisitions" value="Process" class="btn btn-primary" class="btn btn-primary" onclick="processRequisitions(document.getElementById(\'ddlProcessRequisitions\').value, this);">';
		req_full_results += '</td>';
		req_full_results += '</tr>';
	}

	$("#requisitions-minified-results").html(req_min_results);
	$("#requisitions_full_view_results").append(req_full_results);
	
	if(typeof(requisitions_list) == 'string') {
		var req_search_res = JSON.parse(requisitions_list);
		req_search_res_len = req_search_res.length;
	}
	else if(typeof(requisitions_list) == 'object') {
		var req_search_res = requisitions_list;
		req_search_res_len = requisitions_list.length;
	}
	

	if(req_search_res_len > 0) {
		var req_id = req_search_res[0].RequestID;
		var multi_org_id = req_search_res[0].MultiOrgID;
		var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;	
		
		document.frmRequisitionDetailInfo.RequestID.value = req_id;
		document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;

		getRequisitionDetailInfo(req_id, multi_org_id);
		getSelectedTabInfo(selected_tab, req_id);
	}
	else {
		$("#requisitions-minified-results").html("<div class='col-lg-12'><strong>No results found. Please update your search filters and try again.</strong></div>");
		$("#process_message_req_details").html("");
		$("#process_message_req_details").removeAttr('class');
	}
}

function getRequisitionsListByFilters(reset_index) {

	var active_status = document.frmFilters.ddlFilterActiveStatus.value;
	document.frmRequisitionDetailInfo.Active.value = active_status;

	var RequisitionsCount = document.frmRequisitionDetailInfo.RequisitionsCount.value;
	var RequisitionsLimit = document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var GuIDRequisition = document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var SearchKeyword = document.frmSortOptions.txtKeyword.value;
	var KeywordType = document.frmSortOptions.rdKeywordType.value;	
	
	var IndexStart;

	if(reset_index == 'yes') 
	{
		IndexStart = 0;
		document.frmRequisitionDetailInfo.IndexStart.value = 0;
	}
	else {
		IndexStart = document.frmRequisitionDetailInfo.IndexStart.value;
	}
	
	var req_page_url = "requisitions/getRequisitionsListByFilters.php?Active="+active_status+"&IndexStart="+IndexStart+"&RequisitionsCount="+RequisitionsCount+"&GuIDRequisition="+GuIDRequisition+'&PageSource=JobBoard';
	if(SearchKeyword != "") {
		if(typeof(KeywordType) == 'undefined') KeywordType = 'W';
		req_page_url += "&keyword="+SearchKeyword+"&keyword_match="+KeywordType;
	}
	
	var to_sort = document.frmSortOptions.ddlReqDetailSort.value;
	var sort_type = document.frmSortOptions.ddlReqDetailSortType.value;
	if(to_sort == '') to_sort = 'date_opened';
	if(to_sort != "" && sort_type != "") {
		req_page_url += "&to_sort="+to_sort+"&sort_type="+sort_type;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: req_page_url,
		type: "POST",
		beforeSend: function() {
			$("#req_list_process_msg").html('loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart);
			$("#req_list_process_msg").html('');
    	}
	});
	setAjaxErrorInfo(request, "#requisitions_full_view_results");
}

function getRecordsByPage(IndexStart) {

	var active_status = document.frmFilters.ddlFilterActiveStatus.value;

	document.frmRequisitionDetailInfo.IndexStart.value = IndexStart;
	document.frmRequisitionDetailInfo.Active.value = active_status;
	
	var RequisitionsCount = document.frmRequisitionDetailInfo.RequisitionsCount.value;
	var RequisitionsLimit = document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var GuIDRequisition = document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var SearchKeyword = document.frmSortOptions.txtKeyword.value;
	var KeywordType = document.frmSortOptions.rdKeywordType.value; 

	var req_page_url = "requisitions/getRequisitionsListByFilters.php?Active="+active_status+"&IndexStart="+IndexStart+"&RequisitionsCount="+RequisitionsCount+"&GuIDRequisition="+GuIDRequisition+'&PageSource=JobBoard';
	
	if(SearchKeyword != "") {
		if(typeof(KeywordType) == 'undefined') KeywordType = 'W';
		req_page_url += "&keyword="+SearchKeyword+"&keyword_match="+KeywordType;
	}
	
	var to_sort = document.frmSortOptions.ddlReqDetailSort.value;
	var sort_type = document.frmSortOptions.ddlReqDetailSortType.value;
	if(to_sort == '') to_sort = 'date_opened';
	if(to_sort != "" && sort_type != "") {
		req_page_url += "&to_sort="+to_sort+"&sort_type="+sort_type;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: req_page_url,
		type: "POST",
		success: function(data) {
			setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart);
    	}
	});
	setAjaxErrorInfo(request, "#requisitions_full_view_results");
}

function sortRequisitionsFullView(to_sort) {
	var active_status = document.frmFilters.ddlFilterActiveStatus.value;

	var IndexStart = document.frmRequisitionDetailInfo.IndexStart.value;
	document.frmRequisitionDetailInfo.Active.value = active_status;
	
	var RequisitionsCount = document.frmRequisitionDetailInfo.RequisitionsCount.value;
	var RequisitionsLimit = document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var GuIDRequisition = document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var SearchKeyword = document.frmSortOptions.txtKeyword.value;
	var KeywordType = document.frmSortOptions.rdKeywordType.value; 

	$("#results_loading").html('&nbsp;&nbsp;&nbsp;Please wait.. loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	$("#results_loading").attr('class', 'alert alert-info');
	
	var x = document.getElementById("ddlReqDetailSortType").selectedIndex;
	var y = document.getElementById("ddlReqDetailSortType").options;
		
	if(y[x].text == 'Ascending') document.getElementById("ddlReqDetailSortType").selectedIndex = 1;
	else if(y[x].text == 'Descending') document.getElementById("ddlReqDetailSortType").selectedIndex = 0;
	
	var req_page_url = "requisitions/getRequisitionsListByFilters.php?Active="+active_status+"&IndexStart="+IndexStart+"&RequisitionsCount="+RequisitionsCount+"&GuIDRequisition="+GuIDRequisition+'PageSource=JobBoard';
	
	if(SearchKeyword != "") {
		if(typeof(KeywordType) == 'undefined') KeywordType = 'W';
		req_page_url += "&keyword="+SearchKeyword+"&keyword_match="+KeywordType;
	}
	
	var sort_type = document.frmSortOptions.ddlReqDetailSortType.value;
	if(to_sort == '') to_sort = 'date_opened';
	if(to_sort != "" && sort_type != "") {
		req_page_url += "&to_sort="+to_sort+"&sort_type="+sort_type;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: req_page_url,
		type: "POST",
		success: function(data) {
			$("#results_loading").html('');
			$("#results_loading").removeAttr('class');
			setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart);
    	}
	});
	setAjaxErrorInfo(request, "#requisitions_full_view_results");
}

function updateFreeJobBoardListings(job_board_value) {

	var RequestID = document.frmRequisitionDetailInfo.RequestID.value;
	var job_board_name = job_board_value;
	
	if(job_board_value == 'ZipRecruiter') {
		job_board_type	=	'ZipRecruiter';
		job_board_value =	'ZipRecruiter';
		var zip_rec_job_cat = document.forms['frmZipRecruiterPurchase'].ZipRecruiterJobCategory.value;
		var zip_rec_label = document.forms['frmZipRecruiterPurchase'].ZipRecruiterLabel.value;
		var zip_rec_checked = $(".zip_rec_paid_tf").prop('checked');

		if(zip_rec_checked == false) {
			job_board_value = "";
		}

		if(zip_rec_job_cat == "" || zip_rec_label == "") {
			request_url = "";
			alert("Form Choice and Zip Recruiter Category are required");
		}
		else {
			request_url		=	"requisitions/updFreeJobBoardLists.php?RequestID="+RequestID+"&job_boards_list="+job_board_value+"&job_board_type="+job_board_type+"&ZipRecruiterFreeLabel="+zip_rec_label+"&ZipRecruiterJobCategory="+zip_rec_job_cat;
		}
	}
	else if(job_board_value == 'Indeed') {
		job_board_type	=	'Indeed';
		job_board_value =	'Indeed';
		var indeed_label		=	document.forms['frmLinkPurchase'].IndeedLabel.value;
		var indeed_wfh			=	document.forms['frmLinkPurchase'].IndeedWFH.value;
		var indeed_config_apply 	=	document.forms['frmLinkPurchase'].IndeedConfigApply.value;
		var indeed_checked		=	$(".indeed_fp_sel").prop('checked');
		var message 			=	"";

		if(indeed_checked == false) {
			job_board_value = "";
		}

		if(indeed_config_apply == "Y") {
			if((indeed_label == "") && (indeed_checked == true)) {
			   request_url	=	"";
			   message += " - Form Choice must be selected\n";
			}
			else {
			   request_url	=	encodeURI("requisitions/updFreeJobBoardLists.php?RequestID="+RequestID+"&job_boards_list="+job_board_value+"&job_board_type="+job_board_type+"&IndeedLabel="+indeed_label+"&IndeedWFH="+indeed_wfh);
			}
		   if (message != "") {
			alert("The following entries are missing information:\n\n" + message);
		   }
		}
		else {
			request_url	=	encodeURI("requisitions/updFreeJobBoardLists.php?RequestID="+RequestID+"&job_boards_list="+job_board_value+"&job_board_type="+job_board_type+"&IndeedLabel="+indeed_label+"&IndeedWFH="+indeed_wfh);
		}

	}
	else if(job_board_value == 'Monster') {
		job_board_type	=	'Monster';
		job_board_value =	'Monster';
		var monster_checked = $(".monster_fp_sel").prop('checked');

		if(monster_checked == false) {
			job_board_value = "";
		}
		
		request_url		=	"requisitions/updFreeJobBoardLists.php?RequestID="+RequestID+"&job_boards_list="+job_board_value+"&job_board_type="+job_board_type;
	}
	
	if(request_url != "") {
		$.ajax({
			method: "POST",
	  		url: request_url,
			type: "POST",
			beforeSend: function() {
				if(job_board_name == 'ZipRecruiter') {
					$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
				}
				else if(job_board_name == 'Indeed') {
					$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
				}
				else if(job_board_name == 'Monster') {
					$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
				}
			},
			success: function(data) {
				if(job_board_name == 'ZipRecruiter') {
					getPurchaseZipRecruiterFeedInfo('yes');
				}
				else if(job_board_name == 'Indeed') {
					getPurchasePremiumFeedInfo('yes', '');
				}
				else if(job_board_name == 'Monster') {
					postJobToMonster('post-job-to-monster');
				}
	    	}
		});
	}
}

function getRequisitionsByPageNumber(page_number) {
	var RequisitionsLimit = document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var RequisitionsCount = document.frmRequisitionDetailInfo.RequisitionsCount.value;
	
	var TotalPages = Math.ceil(RequisitionsCount/RequisitionsLimit);
	var IndexStart = (page_number - 1) * RequisitionsLimit;
	
	if(page_number <= TotalPages) getRecordsByPage(IndexStart);
}

function getSelectedTabInfo(selected_tab, req_id) {

	$('.ra-tab-content').hide();
	$(selected_tab).show();

	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var validation_error_msg = '<br>&nbsp;&nbsp;'+address_info_error_msg;
	validation_error_msg += '<br><br>&nbsp;&nbsp;<a href="'+irecruit_home+'requisitionsSearch.php?menu=3&RequestID='+req_id+'&active_tab=edit-requisition&validate_errors_list=csz">Click here to view requisitions</a>';
	
	$("#job_boards_processing_msg").html("");
	
	$(".side-tabs > li").removeAttr('class');
	$('#tab_free_ziprecruiter, #tab_paid_ziprecruiter').attr('style', 'background-color:#C3EA33;border-color:#C3EA33;');
	$('#tab_free_indeed, #tab_paid_indeed').attr('style', 'background-color:#3363F1;border-color:#3363F1;');
	$('#tab_free_monster, #tab_paid_monster').attr('style', 'background-color:#8F2763;border-color:#8F2763;');
	$('a[href="'+selected_tab+'"]').closest('li').addClass('active');
	
	var City 	= document.frmRequisitionDetailInfo.City.value;
	var State 	= document.frmRequisitionDetailInfo.State.value;
	var ZipCode = document.frmRequisitionDetailInfo.ZipCode.value;
	

	/*if(City == '' || State == '' || ZipCode == '') { */
	if(State == '') {
		
		$('#post-job-to-monster').html(validation_error_msg);
		$('#free-post-job-to-monster').html(validation_error_msg);
		$('#free-posting-indeed').html(validation_error_msg);
		$('#purchase-premium-feed').html(validation_error_msg);
		$('#export-requisition').html(validation_error_msg);
		$('#repost-requisition').html(validation_error_msg);
		$('#free-ziprecruiter-feed').html(validation_error_msg);
		$('#purchase-zip-recruiter-feed').html(validation_error_msg);
	}
	else {

		if(selected_tab == '#post-job-to-monster') {
			postJobToMonster('post-job-to-monster');
		}
		if(selected_tab == '#free-post-job-to-monster') {
			postJobToMonster('free-post-job-to-monster');
		}
		if(selected_tab == '#purchase-premium-feed') {
			getPurchasePremiumFeedInfo('no', '');
		}
		if(selected_tab == '#export-requisition') {
			getExportRequisitionInformation();
		}
		if(selected_tab == '#repost-requisition') {
			getRepostRequisitionInformation();
		}
		if(selected_tab == '#purchase-zip-recruiter-feed') {
			getPurchaseZipRecruiterFeedInfo('no');
		}
		if(selected_tab == '#socialmedia-requisition') {
			getSocialMediaRequisitionInformation();
		}
		
	}
}

function getPrevNextApplicant(nav_type) {

	var curr_multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var curr_req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	if(typeof(requisitions_list) == 'string') {
		var req_search_res = JSON.parse(requisitions_list);
		req_search_res_len = req_search_res.length;
	}
	else if(typeof(requisitions_list) == 'object') {
		var req_search_res = requisitions_list;
		req_search_res_len = requisitions_list.length;
	}
	
	var j = 0;
	
	if(nav_type == 'next') {
		$("#req_detail_prev").show();
	}
	
	for (var i = 0; i < req_search_res_len; i++) {
		
		if(curr_req_id == req_search_res[i].RequestID) {
			
			if(nav_type == 'previous') {
				j = i - 1;
			}
			if(nav_type == 'next') {
				j = i + 1;
			}

			var req_id = req_search_res[j].RequestID;
			var multi_org_id = req_search_res[j].MultiOrgID;
			var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;	
			
			document.frmRequisitionDetailInfo.RequestID.value = req_id;
			document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;

			getRequisitionDetailInfo(req_id, multi_org_id);
			getSelectedTabInfo(selected_tab, req_id);

			i = req_search_res_len;
		}

	}	
}

function freeMonsterTabInfo(req_id, multi_org_id) {
	document.frmRequisitionDetailInfo.current_sel_tab.value = '#free-post-job-to-monster';
	document.frmRequisitionDetailInfo.RequestID.value = req_id;
	document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;

	$("#full-view-navigation-bar").hide();
	
	document.getElementById('search-results-detail').style.display = 'none';
	$('#applicant-detail-view').show();
	$('#requisitions-minified-search-results').show();
	$('#applicant-detail-info').show();
    $('.ra-tab-content').hide();
	$('#free-post-job-to-monster').show();
	
	getRequisitionTabInfo(req_id, multi_org_id);
}

$(document).ready(function () {
    
	$('body').on({   
		click : function(){

	    	var traffic_boost = $(this).val();
	    	
	    	if(traffic_boost == 'boost') {
	    		$("body #BudgetTotal").val('boost');
	    		$("body #btnAddToBasket").val('add to basket');
	    		$("body #btnAddToBasket").attr("onclick", "addToBasketReqZipRecruiterFeed(this);");
	    	}
	    	else if(traffic_boost == 'subscribe') {
	    		$("body #btnAddToBasket").val('Subscribe');
	    		$("body #btnAddToBasket").attr("onclick", "subscribeToZipRecruiter();");
	    	}
	    	else if(traffic_boost == 'free') {
	    		if(typeof($(this).attr('checked')) == 'string') {
	    			$( ".zip_rec_paid_tf" ).each(function() {
						$(this).removeAttr('checked');
						$(this).prop('checked',false);
    				});
	    		}
	    		else if(typeof($(this).attr('checked')) == 'undefined') {
	    			$(this).attr('checked', 'checked');
					$(this).prop('checked',true);
	    		}
	    		$("body #btnAddToBasket").val('Update');
	    		$("body #btnAddToBasket").attr("onclick", "updateFreeJobBoardListings('ZipRecruiter');");
	    	}
		}
	
	  },".zip_rec_paid_tf");
 
	$('body').on({   
		click : function(){

	    	var traffic_boost = $(this).val();
	    	
	    	if(traffic_boost == 'Sponsored') {
	    		if(typeof($(this).attr('checked')) == 'string') {
	    			$(".indeed_fp_sel").each(function() {
						$(this).removeAttr('checked');
						$(this).prop('checked',false);
						
						$("body #SponsoredInfo").val('UnSponsored');
			    		$("body #btnAddToBasket").val('Remove from Sponsored List');
			    		$("body #btnAddToBasket").attr("onclick", "sponsorIndeedRequisition(this);");
    				});
	    		}
	    		else if(typeof($(this).attr('checked')) == 'undefined') {
	    			$(this).attr('checked', 'checked');
					$(this).prop('checked',true);
					
					$("body #SponsoredInfo").val('Sponsored');
		    		$("body #btnAddToBasket").val('Sponsor Job on Indeed now');
		    		$("body #btnAddToBasket").attr("onclick", "sponsorIndeedRequisition(this);");
	    		}

	    	}
	    	else if(traffic_boost == 'Free') {
	    		if(typeof($(this).attr('checked')) == 'string') {
	    			$( ".indeed_fp_sel" ).each(function() {
						$(this).removeAttr('checked');
						$(this).prop('checked',false);
    				});
	    		}
	    		else if(typeof($(this).attr('checked')) == 'undefined') {
	    			$(this).attr('checked', 'checked');
					$(this).prop('checked',true);
	    		}
	    		$("body #btnAddToBasket").val('Update');
	    		$("body #btnAddToBasket").attr("onclick", "updateFreeJobBoardListings('Indeed');");
	    	}
		}
	
	  },".indeed_fp_sel");

	$('body').on({   
		click : function(){

	    	var traffic_boost = $(this).val();
	    	
	    	if(traffic_boost == 'Free') {
	    		if(typeof($(this).attr('checked')) == 'string') {
	    			$( ".monster_fp_sel" ).each(function() {
						$(this).removeAttr('checked');
						$(this).prop('checked',false);
    				});
	    		}
	    		else if(typeof($(this).attr('checked')) == 'undefined') {
	    			$(this).attr('checked', 'checked');
					$(this).prop('checked',true);
	    		}
	    	}
		}
	
	  },".monster_fp_sel");

	
});

function subscribeToZipRecruiter() {
	
	var subscribe = document.getElementById('btnAddToBasket').value;
	var input_data = "";

	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var zip_rec_job_cat = document.forms['frmZipRecruiterPurchase'].ZipRecruiterJobCategory.value;

	if(zip_rec_job_cat == "") {
		alert("Please select ziprecruiter job category.");
		return false;
	}
	
	if(subscribe == "Subscribe") {
		input_data = {"subscribe":"plus","RequestID":req_id, "ZipRecruiterJobCategory":zip_rec_job_cat};
	}
	else if(subscribe == "Unsubscribe") {
		input_data = {"subscribe":"minus","RequestID":req_id, "ZipRecruiterJobCategory":zip_rec_job_cat};
	}
	
	$.ajax({
		method: "POST",
  		url: "shopping/subscribeToZipRecruiter.php",
		type: "POST",
		data: input_data,
		dataType: "json",
		beforeSend: function() {
			//$("#req_list_process_msg").html('loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			
			if(data.message == "Deleted" || data.message == "Success") {
				var req_id = document.frmRequisitionDetailInfo.RequestID.value;
				var org_id = document.frmRequisitionDetailInfo.OrgID.value;
				var access_code = document.frmRequisitionDetailInfo.AccessCode.value;

				var request_url = irecruit_home + "shopping/zipRecruiterPurchase.php?OrgID="+org_id+"&RequestID="+req_id;
				if(access_code != "") request_url += "&k="+access_code;

				var request = $.ajax({
					method: "POST",
			  		url: request_url,
					type: "POST",
					beforeSend: function() {
						$("#purchase-zip-recruiter-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
					},
					success: function(data) {
						var msg_form = "";

						if(subscribe == "Subscribe") {
							msg_form = "<br><span style='color:blue;weight:bold'>This requisition has been subscribed to TrafficBoost.</span>";
						}
						else if(subscribe == "Unsubscribe") {
							msg_form = "<br><span style='color:blue;weight:bold'>This requisition has been unsubscribed from TrafficBoost.</span><br><br>";
						}
						
						msg_form += data;
						$("#purchase-zip-recruiter-feed").html(msg_form);
			    	}
				});
			}
			else if(data.message == 'AlreadyUsed') {
				alert("You have used all your subscriptions. Please try traffic boost.")
			}
			else if(data.message == 'NoSubscriptions') {
				alert("You don't have any subscriptions.")
			}
    	}
	});
}


function validateNumeric(budget_total) {
	
	if(isNaN(budget_total)) {
		$("#txtBudgetTotal").blur();
		$('#txtBudgetTotal').val("");
		alert("Enter valid number.");
		return false;
	}
	
	$('#BudgetTotal4').val(budget_total);  

}  

$(document).ready(function() {
	
	//Have to add substr to fix it
	if(r_action != 'purchases') {
		
		$(".vertical-text").click(function() {
			var button_id = $(this).attr('id');

			if(button_id == 'vertical-text-full-view-results') {
				$("#full-view-navigation-bar").show();
				
				document.getElementById('search-results-detail').style.display = 'block';
				$('#applicant-detail-view').hide();
				$('#requisitions-minified-search-results').hide();
			}
			
			if(button_id == 'vertical-text-detail-view-results') {
				$("#full-view-navigation-bar").hide();
				
				document.getElementById('search-results-detail').style.display = 'none';
				$('#applicant-detail-view').show();
				$('#requisitions-minified-search-results').show();
				$('#applicant-detail-info').show();
			}
		});
		
		$('.requisition-tabs').click(function(event) {
			$("#job_boards_processing_msg").removeAttr("class");
			
			event.preventDefault();
			var RequestID = document.frmRequisitionDetailInfo.RequestID.value;
			
	        var src_href = this.href;
	        var href_tab_info = src_href.split("#");
	        var content_tab = href_tab_info[1];
			var selected_tab = "#"+content_tab;
			
	        $('.ra-tab-content').hide();
	    	$('#'+content_tab).show();

	    	document.frmRequisitionDetailInfo.current_sel_tab.value = selected_tab;
		    	
	    	getSelectedTabInfo(selected_tab, RequestID);
		});

		var req_id = document.frmRequisitionDetailInfo.RequestID.value;
		var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;

		getRequisitionDetailInfo(req_id, multi_org_id);
		
	}
});
