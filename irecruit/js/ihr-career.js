function saveSkillsInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmSkills").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'saveCareerSkillsInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveEducationInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmEducation").serialize();
	var ajax_params = {"file_upload":"false"};

	//Have to do the validation before saving the information
	var request_url = 'saveEducationInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveEmployerInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmPreviousEmployer").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'saveEmployerInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function setSkillsInfo(json_data) {
	
	var skills_info_txt_elem = ["taSkills"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in skills_info_txt_elem) {
		object_key = skills_info_txt_elem[txtkey];
		object_value = data[object_key];
		$("#frmSkills #"+object_key).val(object_value);
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setEducationInfo(json_data) {
	
	var education_info_txt_elem = ["Aattended", "Ayearscompleted", "Ayear", "Agpa", "Aotherdegree", "Aeduphone", "Aeduaddress",
	                               "Battended", "Byearscompleted", "Byear", "Bgpa", "Botherdegree", "Beduphone", "Beduaddress",
	                               "Cattended", "Cyearscompleted", "Cyear", "Cgpa", "Cotherdegree", "Ceduphone", "Ceduaddress"];
	var education_info_ddl_elem = ["Cdegree", "Cmajor", "Cminor"];
	var education_info_radio_elem = ["Agraduated", "Bgraduated", "Cgraduated"];
	
	var data = getJsonObject(json_data);
	
	var object_key, object_value;
	for (var txtkey in education_info_txt_elem) {
		object_key = education_info_txt_elem[txtkey];
		object_value = data[object_key];
		
		if(object_key == "Aeduphone" || object_key == "Beduphone" || object_key == "Ceduphone") 
		{
			if(object_value != null) {
				var phone_info = object_value.split("-");
				$("#frmEducation #"+object_key+"1").val(phone_info[0]);
				$("#frmEducation #"+object_key+"2").val(phone_info[1]);
				$("#frmEducation #"+object_key+"3").val(phone_info[2]);
			}
		}
		else {
			$("#frmEducation #"+object_key).val(object_value);
		}
	}
	for (var ddlkey in education_info_ddl_elem) {
		object_key = education_info_ddl_elem[ddlkey];
		object_value = data[object_key];
		$("#frmEducation #"+object_key+">option[value='" + object_value + "']").prop('selected', true);
	}
	for (var ddlkey in education_info_radio_elem) {
		object_key = education_info_radio_elem[ddlkey];
		object_value = data[object_key];
		radio_button_id = object_key+object_value;
		if(object_value == null 
			|| typeof(object_value) == 'undefined'
			|| object_value == "") {
			//document.getElementById(radio_button_id).checked = false;
			$('input[name='+object_key+']').attr('checked',false);
		}
		else {
			document.getElementById(radio_button_id).checked = true;
		}
	}
	
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');
}

function setPreviousEmployerInfo(json_data) {
	var employer_info_txt_elem = ["Amrpcompanyname", "Amrpaddress", "Amrpcity", "Amrpstate", "Amrppostalcode", "Amrpdatesofemploymentfrom", "Amrpdatesofemploymentto", "Amrpposition", "Amrpstartingsalary", "Amrpsalary", "Amrplastsupervisor", "Amrpphone", "Amrpdescribeposition", "Amrpreasonforleaving", "Adischargedreason"];
	var employer_info_ddl_elem = ["Amrpsalarydefinition", "Amrpsalarydefinition", "Amrpoktocontact", "Amrpcurrentposition", "Adischarged"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in employer_info_txt_elem) {
		object_key = employer_info_txt_elem[txtkey];
		object_value = data[object_key];
		if(object_key == "Amrpdatesofemploymentfrom" || object_key == "Amrpdatesofemploymentto") object_value = getMdyFromYmd(object_value, "-", "/");
		if(object_key == "Amrpphone") 
		{
			if(object_value != null) {
				var phone_info = object_value.split("-");
				$("#frmPreviousEmployer #Amrpphone1").val(phone_info[0]);
				$("#frmPreviousEmployer #Amrpphone2").val(phone_info[1]);
				$("#frmPreviousEmployer #Amrpphone3").val(phone_info[2]);
			}
		}
		else {
			$("#frmPreviousEmployer #"+object_key).val(object_value);
		}
	}
	for (var ddlkey in employer_info_ddl_elem) {
		object_key = employer_info_ddl_elem[ddlkey];
		object_value = data[object_key];
		if(object_value == null 
			|| object_value == '') {
			$("#frmPersonalInfo #"+object_key+">option[value='']").prop('selected', true);
		}
		else {
			$("#frmPreviousEmployer #"+object_key+">option[value='" + object_value + "']").prop('selected', true);
		}
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');
}