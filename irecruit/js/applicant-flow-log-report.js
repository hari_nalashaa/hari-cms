function getRequisitionsByStatus(RequisitionStatus) {
	var FilterOrganization	=	document.forms['frmApplicantFlowLog'].ddlOrganizationsList.value;
	var ProcessOrder		=	document.forms['frmApplicantFlowLog'].ddlApplicationProcessOrder.value;
	
	$('#ddlRequisitionsList').children('option:not(:first)').remove();

    var option_text		=	'';
    var option_value	=	'';
    var options_list    =   '';
    
	$.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?FilterOrganization="+FilterOrganization+"&RequisitionStatus="+RequisitionStatus+"&ProcessOrder="+ProcessOrder,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#message_status").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		+= " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#message_status").html("");
			$("#ddlRequisitionsList").append(options_list);
    	}
	});
}
        
function sortApplicantFlowLog(to_sort) {
	var IndexStart     		=   document.frmApplicantFlowLog.IndexStart.value;
	var FilterOrganization	=	document.forms['frmApplicantFlowLog'].ddlOrganizationsList.value;
	var ProcessOrder		=	document.forms['frmApplicantFlowLog'].ddlApplicationProcessOrder.value;
	
    document.frmApplicantFlowSortOptions.ddlToSort.value = to_sort;
    
	var sort_type  =   document.frmApplicantFlowSortOptions.ddlSortType.value;
	if(sort_type == "DESC") {
		document.frmApplicantFlowSortOptions.ddlSortType.value = "ASC";
		sort_type = "ASC";
	}
	else if(sort_type == "ASC") {
		document.frmApplicantFlowSortOptions.ddlSortType.value = "DESC";
		sort_type = "DESC";
	}
	
	var from_date      	=   document.frmApplicantFlowLog.applicants_from_date.value;
	var to_date        	=   document.frmApplicantFlowLog.applicants_to_date.value;
    var RequestID      	=   document.getElementById('ddlRequisitionsList').value;
    var EEOCode        	=   document.getElementById('EEOCode').value;
	
	var REQUEST_URL		=	irecruit_home + "reports/getApplicantFlowLog.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&to_sort="+to_sort+"&sort_type="+sort_type+"&RequestID="+RequestID+"&EEOCode="+EEOCode+"&ProcessOrder="+ProcessOrder;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-applicant-flow-log").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='19' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-applicant-flow-log").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setApplicantFlowLog(data);
    	}
	});
}
        		      
function getRecordsByPage(IndexStart) {
	document.frmApplicantFlowLog.IndexStart.value = IndexStart;
	
	var from_date      		=   document.frmApplicantFlowLog.applicants_from_date.value;
	var to_date        		=   document.frmApplicantFlowLog.applicants_to_date.value;
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var EEOCode        		=   document.getElementById('EEOCode').value;
	var Status         		=   document.getElementById('ddlActive').value;
	var FilterOrganization	=	document.forms['frmApplicantFlowLog'].ddlOrganizationsList.value;
	var ProcessOrder		=	document.forms['frmApplicantFlowLog'].ddlApplicationProcessOrder.value;
	
	var REQUEST_URL    		=   irecruit_home + "reports/getApplicantFlowLog.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&RequestID="+RequestID+"&EEOCode="+EEOCode+"&Status="+Status+"&ProcessOrder="+ProcessOrder;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			
			$("#data-applicant-flow-log").find("tr:gt(0)").remove();
			requisition_details_loading_status	 =	"<tr>";
			requisition_details_loading_status	+=	"<td colspan='19' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			requisition_details_loading_status	+=	"</tr>";
			
			$("#data-applicant-flow-log").append(requisition_details_loading_status);
			
		},
		success: function(data) {
			setApplicantFlowLog(data);
    	}
	});
	
}

function getApplicantFlowLogByPageNumber(page_number) {
	
	var RecordsLimit = document.frmApplicantFlowLog.RecordsLimit.value;
	var IndexStart = (page_number - 1) * RecordsLimit;
	
	getRecordsByPage(IndexStart);
}


function setApplicantFlowLog(data) {
    
	var RecordsLimit 		=	document.frmApplicantFlowLog.RecordsLimit.value;
	var FilterOrganization	=	document.forms['frmApplicantFlowLog'].ddlOrganizationsList.value;
	var ProcessOrder		=	document.forms['frmApplicantFlowLog'].ddlApplicationProcessOrder.value;
	
	var app_prev         	=	data.previous;
	var app_next         	=	data.next;
	var total_pages      	=	data.total_pages;
	var current_page     	=	data.current_page;
	var total_count      	=	data.total_count;
	
	$("#data-applicant-flow-log").find("tr:gt(0)").remove();
	$("#applicant-flow-log-pagination").html("");

	var data_length;
	if(data['applicants_list'] == null) {
		data_length = 0;
	}
	else {
		data_length = data['applicants_list'].length;
	}
	
    var applicants_list			=	data['applicants_list'];
    var req_results				=	"";
    var requisitions_pagination =	"";
    
	$("#data-applicant-flow-log").find("tr:gt(0)").remove();
	
	if(data_length > 0) {
		
		for (var i = 0; i < data_length; i++) {

			var ApplicationID				=	applicants_list[i].ApplicationID;
			var ApplicantSortName			=	applicants_list[i].ApplicantSortName;
			var JobTitle					=	applicants_list[i].JobTitle;
			var EEO							=	applicants_list[i].EEO;
			var EntryDate					=	applicants_list[i].EntryDate;
			var RaceBlack					=	applicants_list[i].RaceBlack;
			var Hispanic					=	applicants_list[i].Hispanic;
			var RaceAsian					=	applicants_list[i].RaceAsian;
			var RaceIndian					=	applicants_list[i].RaceIndian;
			var RaceWhite					=	applicants_list[i].RaceWhite;
			var RaceTwo						=	applicants_list[i].RaceTwo;
			var Male						=	applicants_list[i].Male;
			var Female						=	applicants_list[i].Female;
			var DisabilityYes				=	applicants_list[i].DisabilityYes;
			var DisabilityNo				=	applicants_list[i].DisabilityNo;
			var DisabilityNonDisclose		=	applicants_list[i].DisabilityNonDisclose;
			var VeteranIntentified			=	applicants_list[i].VeteranIntentified;
			var VeteranNotProtected			=	applicants_list[i].VeteranNotProtected;
			var VeteranNonDisclose			=	applicants_list[i].VeteranNonDisclose;
			var ProcessOrder				=	applicants_list[i].ProcessOrder;
			var DispositionCode				=	applicants_list[i].DispositionCode;
			var StartDate					=	applicants_list[i].StartDate;

			if(ApplicationID ==	null) ApplicationID = '';
			if(ApplicantSortName ==	null) ApplicantSortName = '';
			if(JobTitle ==	null) JobTitle = '';
			if(EEO == null) EEO = '';
			if(EntryDate == null) EntryDate = '';
			if(RaceBlack == null) RaceBlack = '';
			if(Hispanic == null) Hispanic = '';
			if(RaceAsian == null) RaceAsian = '';
			if(RaceIndian == null) RaceIndian = '';
			if(RaceWhite == null) RaceWhite = '';
			if(RaceTwo == null) RaceWhite = '';
			if(Male == null) Male = '';
			if(Female == null) Female = '';
			if(DisabilityYes == null) DisabilityYes = '';
			if(DisabilityNo == null) DisabilityNo = '';
			if(DisabilityNonDisclose == null) DisabilityNonDisclose = '';
			if(VeteranIntentified == null) VeteranIntentified = '';
			if(VeteranNotProtected == null) VeteranNotProtected = '';
			if(VeteranNonDisclose == null) VeteranNonDisclose = '';
			if(ProcessOrder == null) ProcessOrder = '';
			if(DispositionCode == null) DispositionCode = '';
			if(StartDate == null) StartDate = '';
			
			
			req_results  = "<tr>";
			req_results += "<td align='left' valign='top'>"+ApplicationID+"</td>";
			req_results += "<td align='left' valign='top'>"+ApplicantSortName+"</td>";
			req_results += "<td align='left' valign='top'>"+JobTitle+"</td>";
			req_results += "<td align='left' valign='top'>"+EEO+"</td>";
			req_results += "<td align='left' valign='top'>"+EntryDate+"</td>";
			req_results += "<td align='center' valign='top'>"+RaceBlack+"</td>";
			req_results += "<td align='center' valign='top'>"+Hispanic+"</td>";
			req_results += "<td align='center' valign='top'>"+RaceAsian+"</td>";
			req_results += "<td align='center' valign='top'>"+RaceIndian+"</td>";
			req_results += "<td align='center' valign='top'>"+RaceWhite+"</td>";
			req_results += "<td align='center' valign='top'>"+RaceTwo+"</td>";
			req_results += "<td align='center' valign='top'>"+Male+"</td>";
			req_results += "<td align='center' valign='top'>"+Female+"</td>";
			req_results += "<td align='center' valign='top'>"+DisabilityYes+"</td>";
			req_results += "<td align='center' valign='top'>"+DisabilityNo+"</td>";
			req_results += "<td align='center' valign='top'>"+DisabilityNonDisclose+"</td>";
			req_results += "<td align='center' valign='top'>"+VeteranIntentified+"</td>";
			req_results += "<td align='center' valign='top'>"+VeteranNotProtected+"</td>";
			req_results += "<td align='center' valign='top'>"+VeteranNonDisclose+"</td>";
			req_results += "<td align='center' valign='top'>"+ProcessOrder+"</td>";
			req_results += "<td align='center' valign='top'>"+DispositionCode+"</td>";
			req_results += "<td align='center' valign='top'>"+StartDate+"</td>";
			req_results += "</tr>";

			$("#data-applicant-flow-log").append(req_results);

		}

		req_results  = "<tr>";
		req_results += "<td valign='top' colspan='19' align='left'>Total Applicants Count: "+data.count_list.ApplicantsCount+"</td>";
		req_results += "</tr>";

		req_results += "<tr>";
		req_results += "<td align='left' valign='top' colspan='4'>Total of all pages: </td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.RaceBlackCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.HispanicCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.RaceAsianCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.RaceIndianCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.RaceWhiteCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.RaceTwoCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.MaleCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.FemaleCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.DisabilityYesCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.DisabilityNoCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.DisabilityNonDiscloseCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.VeteranIntentifiedCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.VeteranNotProtectedCount+"</td>";
		req_results += "<td align='center' valign='top'>"+data.count_list.VeteranNonDiscloseCount+"</td>";
		req_results += "<td align='center' valign='top'>&nbsp;</td>";
		req_results += "<td align='center' valign='top'>&nbsp;</td>";
		req_results += "<td align='center' valign='top'>&nbsp;</td>";
		req_results += "</tr>";

		$("#data-applicant-flow-log").append(req_results);
		
	    if(parseInt(total_count) > parseInt(RecordsLimit)) {
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div class='col-lg-6'>";
			requisitions_pagination += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+RecordsLimit+"</div>";
			requisitions_pagination += "</div>";

			requisitions_pagination += "<div class='col-lg-6' style='text-align:right'>";
			requisitions_pagination += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
			requisitions_pagination += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getApplicantFlowLogByPageNumber(document.getElementById(\"current_page_number\").value)'>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "<div class='row'>";
			
			requisitions_pagination += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
			requisitions_pagination += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
			requisitions_pagination += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
			requisitions_pagination += "</div>";
			
			requisitions_pagination += "</div>";

		}

		$("#applicant-flow-log-pagination").html(requisitions_pagination);
		
	}
	else {

		req_results = "<tr>";
		req_results += "<td colspan='19' align='center'>No records found</td>";
		req_results += "</tr>";

		$("#data-applicant-flow-log").append(req_results);
	}
}

function exportRequisitions() {
	var from_date      		=   document.frmApplicantFlowLog.applicants_from_date.value;
	var to_date        		=   document.frmApplicantFlowLog.applicants_to_date.value;
	var to_sort        		=   document.frmApplicantFlowSortOptions.ddlToSort.value;
	var sort_type      		=   document.frmApplicantFlowSortOptions.ddlSortType.value;
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var EEOCode        		=   document.getElementById('EEOCode').value;
	var Status         		=   document.getElementById('ddlActive').value;
	var FilterOrganization	=	document.forms['frmApplicantFlowLog'].ddlOrganizationsList.value;
	var ProcessOrder		=	document.forms['frmApplicantFlowLog'].ddlApplicationProcessOrder.value;
	
	var REQUEST_URL    		=   irecruit_home + "reports/getApplicantFlowLog.php?FilterOrganization="+FilterOrganization+"&Export=YES"+"&FromDate="+from_date+"&ToDate="+to_date+"&RequestID="+RequestID+"&to_sort="+to_sort+"&sort_type="+sort_type+"&EEOCode="+EEOCode+"&Status="+Status+"&ProcessOrder="+ProcessOrder;

	document.frmApplicantFlowLog.action = REQUEST_URL;
	document.frmApplicantFlowLog.submit();
}
		      
function getApplicantFlowLog() {
	
	var from_date      		=   document.frmApplicantFlowLog.applicants_from_date.value;
	var to_date        		=   document.frmApplicantFlowLog.applicants_to_date.value;
	var RequestID      		=   document.getElementById('ddlRequisitionsList').value;
	var IndexStart     		=   document.frmApplicantFlowLog.IndexStart.value;
	var EEOCode        		=   document.getElementById('EEOCode').value;
	var Status         		=   document.getElementById('ddlActive').value;
	var FilterOrganization	=	document.forms['frmApplicantFlowLog'].ddlOrganizationsList.value;
	var ProcessOrder		=	document.forms['frmApplicantFlowLog'].ddlApplicationProcessOrder.value;

	var REQUEST_URL			=	irecruit_home + "reports/getApplicantFlowLog.php?FilterOrganization="+FilterOrganization+"&IndexStart="+IndexStart+"&FromDate="+from_date+"&ToDate="+to_date+"&RequestID="+RequestID+"&EEOCode="+EEOCode+"&Status="+Status+"&ProcessOrder="+ProcessOrder;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#data-applicant-flow-log").find("tr:gt(0)").remove();
			req_results	 = "<tr>";
			req_results	+= "<td colspan='19' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			req_results	+= "</tr>";
			
			$("#data-applicant-flow-log").append(req_results);
		},
		success: function(data) {
			setApplicantFlowLog(data);
    	}
	});
}