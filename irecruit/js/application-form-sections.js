$("#sort_form_section_titles tbody").sortable({
	cursor: 'move',
	placeholder: 'ui-state-highlight',
    	stop: function( event, ui ) {

    	var forms_data = {
    		forms_info: []
    	};
    		      		   
        $(this).find('tr').each(function(i) {

        	var SortOrder = i;
            SortOrder++;
           
            var FormIDSectionID = $(this).attr('id');
			var FormIDSectionIDInfo	= FormIDSectionID.split("*");
			var FormID = FormIDSectionIDInfo[0];
			var SectionID = FormIDSectionIDInfo[1];
            
            if(typeof(SectionID) !== 'undefined') {
            	forms_data.forms_info.push({
					 "FormID" : FormID,
              		 "SectionID"  : SectionID,
               		 "SortOrder"  : SortOrder
               	});
            }
        	
        });

        updateFormSectionTitlesSortOrder(forms_data);
    }
});

function updateFormSectionTitlesSortOrder(forms_data) {

	var REQUEST_URL = irecruit_home + "forms/updateFormSectionTitlesSortOrder.php";
	
	$.ajax({
		method: "POST",
  		url: REQUEST_URL,
		type: "POST",
		data: forms_data,
		beforeSend: function() {
			$("#form_section_titles_status_message").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			$("#form_section_titles_status_message").html("<span style='color:blue'>"+data+"</span>");
		}
	});
}

function changeActiveStatus(form_id, section_id, is_checked) {

	var REQUEST_URL 	=	irecruit_home + "forms/updateApplicationFormSectionActiveStatus.php";
	var active			=	(is_checked) ? "Y" : "N";
	var input_data		=	{"FormID":form_id, "SectionID":section_id, "Active":active};

	$('#actionsection').find('option').remove();
	if(is_checked == false) {
	    $("#view_print_"+section_id).removeAttr("checked");
	}
	
	$.ajax({
		method: "POST",
  		url: REQUEST_URL,
		type: "POST",
		data: input_data,
		dataType: 'json',
		beforeSend: function() {
			$("#form_section_titles_status_message").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			let opt_list = '';

			$("#form_section_titles_status_message").html("<span style='color:blue'>Updated successfully.</span>");
	
			$.each( data, function( key, value ) {
				opt_list	+=	'<option';
				if(key == 'formquestions:AFS') opt_list	+=	' selected="selected"';
				opt_list	+=	' value="'+key+'">'+value+'</option>';
			});

			$('#actionsection').append(opt_list);

		}
	});
}

function changeViewPrintStatus(form_id, section_id, is_checked) {
	
	var sec_active	=	document.getElementById("section_"+section_id).checked;
	
	if(sec_active == true) {
		var REQUEST_URL =	irecruit_home + "forms/updateApplicationFormSectionViewPrintStatus.php";
		var active		=	(is_checked) ? "Y" : "N";
		var input_data	=	{"FormID":form_id, "SectionID":section_id, "ViewPrintStatus":active};

		$.ajax({
			method: "POST",
	  		url: REQUEST_URL,
			type: "POST",
			data: input_data,
			beforeSend: function() {
				$("#form_section_titles_status_message").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
			},
			success: function(data) {
				$("#form_section_titles_status_message").html("<span style='color:blue'>"+data+"</span>");
				if(data != "Updated successfully") {
				    $("#view_print_"+section_id).removeAttr("checked");
				}
			}
		});
	}
	else {
		$("#form_section_titles_status_message").html("<span style='color:blue'>Please make the section active before updating this print status.</span>");
		$("#view_print_"+section_id).removeAttr("checked");
	}

}

$(".toggle").on('click', function (event){
	  event.preventDefault();
	  $(".toggle-div").slideToggle("fast");
	    $(this).html(function(i,html) {
        if (html.indexOf('Show') != -1 ){
           html = html.replace('Show','Hide');
        } else {
           html = html.replace('Hide','Show');
        }
        return html;
    });
});

function showHideAddSection() {
	$("#tbl_add_section").toggle();
	$("#btnAddSection").hide();
}