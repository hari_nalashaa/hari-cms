function set_value(input_id, input_value) {
	document.getElementById(input_id).value = input_value;
}
function show_answer(qa_id, id) {
	$("#" + id).toggleClass('weighted_search_collapse');
	$("#" + qa_id).toggle();
}
function show_answerc(qa_c, id) {
	$("#" + id).toggleClass('weighted_search_collapse');
	$("." + qa_c).toggle();
}
function remove_question(section_id, question_id) {
	var uwkey = document.getElementById('uwkey').value;
	
	location.href = irecruit_home + 'applicants.php?action=weightedsearch&menu=10&subaction=delete&uwkey='+uwkey+'&QuestionID='+question_id;
}
function save_form() {
	document.getElementById("savesearchblock").style.display = 'block';
	document.getElementById('process').value = 'Y';
	if (document.getElementById('SaveSearch').value == '') {
		$("#SaveSearch").addClass("error_text");
		return false;
	} else {
		$("#SaveSearch").removeClass("error_text");
	}

	return true;
}
$("input[type=text]").not('#SaveSearch').keypress(function(event) {
	if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
		event.preventDefault(); // stop character from entering input
	}
});