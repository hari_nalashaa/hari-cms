function saveCurrentPayInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmCurrentPay").serialize();
	var ajax_params = {"file_upload":"false"};

	//Have to do the validation before saving the information
	var request_url = 'saveCurrentPayInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);

}

function saveCurrentJobInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmCurrentJob").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'saveCurrentJobInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function getEmployeeHistoryInfo(TabInfo) {
	
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var SelectedTab = document.frmEmployeeInformation.SelectedTab.value;
	
	var request_url = "getEmployeeInformation.php?IHrGuID="+IHrGuID+"&EmployeeID="+EmployeeID+"&InfoType="+SelectedTab;
	
	if(SelectedTab == "JobHistory") {
		request_url += '&HistoryType='+TabInfo;
	}
	
	getInfo(request_url, '', '');
}

function setCurrentPayInfo(json_data) {
	
	var current_pay_info_txt_elem = ["txtUnitPayRate", "txtAnnualPay", "txtPayEffective"];
	var current_pay_info_ddl_elem = ["ddlPayrollStatus", "ddlShift", "ddlPayPeriodSalary", "ddlShiftPremium", "ddlHoursUnits", "ddlComparatio", "ddlPayFrequency", "ddlBonousAmount"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in current_pay_info_txt_elem) {
		object_key = current_pay_info_txt_elem[txtkey];
		object_value = data[object_key];
		if(object_key == "txtPayEffective") object_value = getMdyFromYmd(object_value, "-", "/");
		$("#frmCurrentPay #"+object_key).val(object_value);
	}
	for (var ddlkey in current_pay_info_ddl_elem) {
		object_key = current_pay_info_ddl_elem[ddlkey];
		object_value = data[object_key];
		$("#frmCurrentPay #"+object_key+">option[value='" + object_value + "']").prop('selected', true);
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setCurrentJobInfo(json_data) {

	var current_job_info_txt_elem = ["txtJobTitle", "txtJobStartDate", "txtChangeReason", "txtChangeEffective"];
	var current_job_info_ddl_elem = ["ddlEmployeeStatus", "ddlJobGroupCode", "ddlEmployeeType", "ddlEEOJobGroup", "ddlSalariedOrHourly", "ddlPayEquityJobGroup", "ddlSalaryGrade", "ddlDirectOrIndirect", "ddlExemptStatus", "ddlEEOClassification", "ddlOvertimeEligible"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in current_job_info_txt_elem) {
		object_key = current_job_info_txt_elem[txtkey];
		object_value = data[object_key];
		if(object_key == "txtJobStartDate" || object_key == "txtChangeEffective") object_value = getMdyFromYmd(object_value, "-", "/");
		$("#frmCurrentJob #"+object_key).val(object_value);
	}
	for (var ddlkey in current_job_info_ddl_elem) {
		object_key = current_job_info_ddl_elem[ddlkey];
		object_value = data[object_key];
		$("#frmCurrentJob #"+object_key+">option[value='" + object_value + "']").prop('selected', true);
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');
}

function getEmployeeHistoryDetailInfo(EmpHistoryGuID, EmployeeGuID, EmployeeID, TabInfo) {
	
	var request_url = "getEmployeeInformation.php?EmpHistoryGuID="+EmpHistoryGuID+"&IHrGuID="+EmployeeGuID+"&EmployeeID="+EmployeeID+"&InfoType=EmployeeHistoryDetail";
	
	if(SelectedTab == "JobHistory") {
		request_url += '&HistoryType='+TabInfo;
	}
	
	$.ajax({
	    method: "POST",
	    url: request_url,
	    type: "POST",
	    beforeSend: function() {
	    	$("#employee_operations_history_details").html('<img src="' + irecruit_home + 'images/wait.gif"/> Please wait..loading...');
	    },
	    success: function(data) {
	    	if(typeof(data) == 'string') {
	    		data = JSON.parse(data);	    	
	    	}
	    	
	    	var data_updated = data['DataUpdated'];

	    	var data_html = '<table class="table table-bordered">';
	    	for (data_info in data_updated) {
	    		if(data_info != "IHrGuID") {
	    			if(data_updated[data_info] == null) data_updated[data_info] = '';
		    		data_html += "<tr><td width=\"25%\"><strong>" + data_info + "</strong></td><td>" + data_updated[data_info] + "</td></tr>";
	    		}
    		}
	    	data_html += '</table><br><br>';
	    	
	    	$("#employee_operations_history_details").html(data_html);
	    }
	});
}

function setJobHistoryInfo(json_data) {
	var data = getJsonObject(json_data);
	var data_length = data.length;
	
	var employee_history = '<table class="table table-bordered">';
	employee_history += '<tr><th>Operation</th><th>View Data</th><th>Date and Time</th></tr>';
	for(var d = 0; d < data_length; d++) {
		
		var EmpHistoryGuID 	= data[d].EmpHistoryGuID;
		var EmployeeGuID 	= data[d].EmployeeGuID;
		var EmployeeID 		= data[d].EmployeeID;
		var TabDetails		= data[d].TabInfo;
		
		employee_history += '<tr>';
		employee_history += '<td>';
		employee_history += data[d].OperationType;
		employee_history += '</td>';
		employee_history += '<td><a href="javascript:void(0);" onclick=\'getEmployeeHistoryDetailInfo("'+EmpHistoryGuID+'", "'+EmployeeGuID+'", "'+EmployeeID+'", "'+TabDetails+'")\'>View Data</a></td>';
		employee_history += '<td>';
		employee_history += data[d].ModifiedDateTime;
		employee_history += '</td>';
		employee_history += '</tr>';
	}
	
	$("#employee_operations_history").html(employee_history); 
	$("#form_status_msg").html('');
}
