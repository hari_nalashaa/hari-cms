function getTabInfo() {
	$("#form_status_msg").html('<br><img src="' + irecruit_home + 'images/wait.gif"/> Please wait..loading...');
	
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var SelectedTab = document.frmEmployeeInformation.SelectedTab.value;
	
	var request_url = 'getEmployeeInformation.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID+'&InfoType='+SelectedTab;
	
	if(SelectedTab == "JobHistory") {
		request_url += '&HistoryType=Address';
	}
	
	getInfo(request_url, '', "form_status_msg");
}

function getJsonObject(json_data) {
	var data;
	if(typeof(json_data) == 'string') {
		data = JSON.parse(json_data);
	}
	else if(typeof(data) == 'object') {
		data = json_data;
	}
	return data;
}

function iHRAjaxSuccessCallBack( data, textStatus, jqXHR ) {

	var active_tab = document.forms['frmEmployeeInformation'].SelectedTab.value;

    if(active_tab == 'Address') {
    	setAddressAndPhoneInfo(data);
	}
    if(active_tab == 'PersonalInfo') {
    	setPersonalInfo(data);
	}
    if(active_tab == 'Photo') {
    	setPhotoInfo(data);
	}
    if(active_tab == "Seniority") {
    	setSeniorityInfo(data);
    }
    if(active_tab == "EmploymentEligibility") {
    	setEmploymentEligibilityInfo(data);
    }
    if(active_tab == "Military") {
    	setMilitaryInfo(data);
    }
    if(active_tab == "EmployeeAttachments") {
    	setEmployeeAttachmentsInfo(data);
    }
    if(active_tab == "FirstContact") {
    	setFirstContactInfo(data);
    }
    if(active_tab == "SecondContact") {
    	setSecondContactInfo(data);
    }
    if(active_tab == "CurrentPay") {
    	setCurrentPayInfo(data);
    }
    if(active_tab == "CurrentJob") {
    	setCurrentJobInfo(data);
    }
    if(active_tab == "JobHistory") {
    	setJobHistoryInfo(data);
    }
    if(active_tab == "EmployeeSkills") {
    	setSkillsInfo(data);
    }
    if(active_tab == "Education") {
    	setEducationInfo(data);
    }
    if(active_tab == "PreviousEmployer") {
    	setPreviousEmployerInfo(data);
    }
}

function getInfo(request_url, input_data, display_id) {
	var request = $.ajax({
					    method: "POST",
					    url: request_url,
					    type: "POST",
					    data: input_data,
					    success: iHRAjaxSuccessCallBack
				  });
	setAjaxErrorInfo(request, display_id);
}

function saveiHRInfoByAjaxCall(request_url, input_data, ajax_params) {
	$("#form_status_msg").html('<img src="' + irecruit_home + 'images/wait.gif"/> Please wait..loading...');
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');

	if(ajax_params.file_upload == 'false') {
		$.ajax({
			method : "POST",
			url : request_url,
			data : input_data,
			type : "POST",
			success : function(data) {
				if(data == 'success') {
					$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12 alert alert-success');
					$("#form_status_msg").html('Successfully updated.');
				}
				if(data == 'failed') {
					$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12 alert alert-danger');
					$("#form_status_msg").html('Unable to update the information.');
				}
			}
		});
	}
	if(ajax_params.file_upload == 'true') {
		$.ajax({
			method : "POST",
			url : request_url,
			data : input_data,
			type : "POST",
			processData : false,
			contentType : false,
			success : function(data) {
				if(data == 'success') {
					$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12 alert alert-success');
					$("#form_status_msg").html('Successfully updated.');
				}
				if(data == 'failed') {
					$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12 alert alert-danger');
					$("#form_status_msg").html('Unable to update the information.');
				}
			}
	});
	}
}

function setEmployeeInfo(employee_name, employee_id, employee_status, hired_date, ihr_guid) {
	
	$("#nav_employee_name").html(employee_name);
	$("#nav_employee_id").html(employee_id);
	$("#nav_employee_status").html(employee_status+" "+hired_date);
	
	document.forms['frmEmployeeInformation'].EmployeeID.value = employee_id;
	document.forms['frmEmployeeInformation'].IHrGuID.value = ihr_guid;
	
	var curr_ihrguid = document.frmEmployeeInformation.IHrGuID.value;
	
	var employee_results;
	
	if(typeof(employees_list) == 'string') {
		employee_results = JSON.parse(employees_list);
		employee_results_len = employee_results.length;
	}
	else if(typeof(employees_list) == 'object') {
		employee_results = employees_list;
		employee_results_len = employee_results.length;
	}
	
	var first_record_ihrguid = employee_results[0].IHrGuID;
	var last_record_ihrguid = employee_results[employee_results_len - 1].IHrGuID;
	
	if(curr_ihrguid == first_record_ihrguid) {
		$("#emp_detail_prev").hide();
	}
	if(curr_ihrguid == last_record_ihrguid) {
		$("#emp_detail_next").hide();
	}
	if(curr_ihrguid != last_record_ihrguid) {
		$("#emp_detail_next").show();
	}
	
	var active_tab = document.forms['frmEmployeeInformation'].SelectedTab.value;
	
	if(active_tab != '') {
		getTabInfo();
	}
	
}

function setEmployeesList(data) {
	
	var json_data = JSON.parse(data);
	var emp_results_list_len = json_data.employees_list.length;
	var emp_results_list = employees_list = json_data.employees_list;
	var employees_count = json_data.employees_count;
	var emp_prev = json_data.previous;
	var emp_next = json_data.next;
	var total_pages = json_data.total_pages;
	var current_page = json_data.current_page;
	var EmployeesLimit = document.frmEmployeeInformation.EmployeesLimit.value;
	document.frmEmployeeInformation.EmployeesCount.value = json_data.employees_count;
	
	if(total_pages == null) total_pages = '';
	if(current_page == null) current_page = '';
	
	var emp_min_results = '';
	emp_min_results += '<br>';
	
	for (var i = 0; i < emp_results_list_len; i++) {
		emp_min_results += '<div class="col-lg-12 col-md-12 col-sm-12">';
		emp_min_results += '<a href=\'javascript:void(0)\' onclick=\'setEmployeeInfo("'+emp_results_list[i]['EmployeeName']+'", "'+emp_results_list[i]['EmployeeID']+'", "'+emp_results_list[i]['Status']+'", "'+emp_results_list[i]['HiredDate']+'", "'+emp_results_list[i]['IHrGuID']+'")\'>'+emp_results_list[i]['EmployeeName']+'</a>';
		emp_min_results += '</div>';
	}
	
	if(emp_results_list_len == 0) {
		emp_min_results += '<div style="padding:5px;margin-top:5px;">No records found based on your filter</div>';
	}
	
	if(parseInt(employees_count) > parseInt(EmployeesLimit)) {
		emp_min_results += '<div class="col-lg-12">';
		
		emp_min_results += '<div class="row">';
		
		emp_min_results += '<div class="col-lg-6">';
		emp_min_results += '<div style="float:left">&nbsp;<strong>Per page:</strong> '+EmployeesLimit+'</div>';
		emp_min_results += '</div>';

		emp_min_results += '<div class="col-lg-6" style="text-align:right">';
		emp_min_results += '<input type="text" name="current_page_number" id="current_page_number" value="'+current_page+'" style="width:50px;text-align:center" maxlength="4">';
		emp_min_results += ' <input type="button" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" class="btn-small" onclick="getRecordsByPageNumber(document.getElementById(\'current_page_number\').value, \'\', \'\', \'no\')">';
		emp_min_results += '</div>';
		
		emp_min_results += '</div>';
		
		emp_min_results += '<div class="row">';
		
		emp_min_results += '<div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left;padding-left:15px;padding-right:0px;">'+emp_prev+'</div>';
		emp_min_results += '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">'+current_page+" - "+total_pages+'</div>';
		emp_min_results += '<div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right;padding-left:10px;padding-right:10px;">'+emp_next+'</div>';
		emp_min_results += '</div>';
		
		emp_min_results += '</div>';
		
		emp_min_results += '</div>';
	}
	else {
		emp_min_results += '<tr><td colspan="100%"><input type="hidden" name="current_page_fullview_number" id="current_page_fullview_number" value="1"></td></tr>';
	}
	
	
	var employee_id 	= emp_results_list[0].EmployeeID;
	var ihr_guid 		= emp_results_list[0].IHrGuID;
	var employee_name 	= emp_results_list[0].EmployeeName;
	var employee_status = emp_results_list[0].Status;
	var hired_date 		= emp_results_list[0].HiredDate;

	setEmployeeInfo(employee_name, employee_id, employee_status, hired_date, ihr_guid);
	
	$("#employees-search-results-min").html(emp_min_results);
}

function getRecordsByPageNumber(page_number) {
	
	var EmployeesLimit = document.frmEmployeeInformation.EmployeesLimit.value;
	var keyword = document.frmFilterEmployees.txtKeyword.value;
	//var EmployeesCount = document.frmEmployeeInformation.EmployeesCount.value;
	
	//var TotalPages = Math.ceil(EmployeesCount/EmployeesLimit);
	var IndexStart = (page_number - 1) * EmployeesLimit;
	document.frmEmployeeInformation.IndexStart.value = IndexStart;
	
	var app_page_url = "getEmployeesList.php?IndexStart="+IndexStart+"&Keyword="+keyword;

	var request = $.ajax({
						method: "POST",
				  		url: app_page_url,
						type: "POST",
						success: function(data) {
							setEmployeesList(data);
				    	}
					});
	
	setAjaxErrorInfo(request, "#employees-search-results-min");
}

function getRecordsByPage(IndexStart) {
	document.frmEmployeeInformation.IndexStart.value = IndexStart;
	var keyword = document.frmFilterEmployees.txtKeyword.value;
	var emp_page_url = "getEmployeesList.php?IndexStart="+IndexStart+"&Keyword="+keyword;

	var request = $.ajax({
					method: "POST",
			  		url: emp_page_url,
					type: "POST",
					success: function(data) {
						setEmployeesList(data);
			    	}
				});
	
	setAjaxErrorInfo(request, "#employees-search-results-min");
}

function getEmployeesFilterdList() {
	var keyword = document.frmFilterEmployees.txtKeyword.value;
	document.frmEmployeeInformation.IndexStart.value = 0;
	var emp_page_url = "getEmployeesList.php?IndexStart=0&Keyword="+keyword;
	
	var request = $.ajax({
					method: "POST",
			  		url: emp_page_url,
					type: "POST",
					success: function(data) {
						setEmployeesList(data);
			    	}
				});
	
	setAjaxErrorInfo(request, "#employees-search-results-min");
}

function getPrevNextApplicant(nav_type) {

	var curr_ihrguid = document.frmEmployeeInformation.IHrGuID.value;
	
	if(typeof(employees_list) == 'string') {
		var employee_results = JSON.parse(employees_list);
		employee_results_len = employee_results.length;
	}
	else if(typeof(employees_list) == 'object') {
		var employee_results = employees_list;
		employee_results_len = employee_results.length;
	}
	var j = 0;
	
	if(nav_type == 'next') {
		$("#emp_detail_prev").show();
	}
	
	for (var i = 0; i < employee_results_len; i++) {

		if(curr_ihrguid == employee_results[i].IHrGuID) {
			
			if(nav_type == 'previous') {
				j = i - 1;
			}
			if(nav_type == 'next') {
				j = i + 1;
			}

			var employee_id 	= employee_results[j].EmployeeID;
			var employee_name 	= employee_results[j].EmployeeName;
			var employee_status = employee_results[j].Status;
			var hired_date 		= employee_results[j].HiredDate;
			var ihr_guid	 	= employee_results[j].IHrGuID;
			
			setEmployeeInfo(employee_name, employee_id, employee_status, hired_date, ihr_guid);
			
			i = employee_results_len;
		}

	}	
}

function resetEmployeesList() {
	document.frmEmployeeInformation.IndexStart.value = 0;
	document.getElementById('txtKeyword').value = '';
	var emp_page_url = "getEmployeesList.php?IndexStart=0";
	
	var request = $.ajax({
					method: "POST",
			  		url: emp_page_url,
					type: "POST",
					success: function(data) {
						setEmployeesList(data);
			    	}
				});
	
	setAjaxErrorInfo(request, "#employees-search-results-min");
}

/**
 * @method		setTabInfo
 * @tutorial	
 * 
 */
function setTabInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var SelectedTab = document.frmEmployeeInformation.SelectedTab.value;
	
	var request_url = "getEmployeeInformation.php?IHrGuID="+IHrGuID+"&EmployeeID="+EmployeeID+"&InfoType="+SelectedTab;
	
	if(SelectedTab == "JobHistory") {
		request_url += '&HistoryType=Address';
	}
	
	getInfo(request_url, '', '');
}

/**
 * @method		setSelectedTab
 * @param 		selected_tab
 * @tutorial	Set selected tab string to hidden variable when user clicks the tab
 */
function setSelectedTab(selected_tab) {
	$("#form_status_msg").html('<img src="' + irecruit_home + 'images/wait.gif"/> Please wait..loading...');
	
	if(selected_tab == "ihr-addressandphone") {
		document.frmEmployeeInformation.SelectedTab.value = 'Address';
	}
	if(selected_tab == "ihr-personalinfo") {
		document.frmEmployeeInformation.SelectedTab.value = 'PersonalInfo';
	}
	if(selected_tab == "ihr-photo") {
		document.frmEmployeeInformation.SelectedTab.value = 'Photo';
	}
	if(selected_tab == "ihr-seniority") {
		document.frmEmployeeInformation.SelectedTab.value = 'Seniority';
	}
	if(selected_tab == "ihr-employment-eligibility") {
		document.frmEmployeeInformation.SelectedTab.value = 'EmploymentEligibility';
	}
	if(selected_tab == "ihr-military") {
		document.frmEmployeeInformation.SelectedTab.value = 'Military';
	}
	if(selected_tab == "ihr-firstcontact") {
		document.frmEmployeeInformation.SelectedTab.value = 'FirstContact';
	}
	if(selected_tab == "ihr-secondcontact") {
		document.frmEmployeeInformation.SelectedTab.value = 'SecondContact';
	}

	setTabInfo();
}

/**
 * @tutorial	Code for ihr tabs show and hide functionality
 */
$(document).ready(function() {
	$('.applicant-tabs').click(function(event) {
		event.preventDefault();
		$(".applicant-tabs").parent().removeAttr('style');
	    var src_href = this.href;
	    var href_tab_info = src_href.split("#");
	    var content_tab = href_tab_info[1];
	    setSelectedTab(content_tab);
	    
	    $('.ra-tab-content').hide();
		$('#'+content_tab).show();
		$( "a[href='#"+content_tab+"']" ).parents( "li" ).css({"background-color":"grey"});
		
	});
});

/**
 * @method		setCurrentTab
 * @tutorial	SetCurrentTab string to hidden variable when the page is loaded
 */
function setCurrentTab() {
	if(document.URL.indexOf("personalDemographics.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'Address';
	}
	if(document.URL.indexOf("personalHRStatus.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'Seniority';
	}
	if(document.URL.indexOf("personalEmployeeAttachments.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'EmployeeAttachments';
	}
	if(document.URL.indexOf("personalEmergencyContacts.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'FirstContact';
	}
	if(document.URL.indexOf("jobandpayCurrentPay.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'CurrentPay';
	}
	if(document.URL.indexOf("jobandpayCurrentJob.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'CurrentJob';
	}
	if(document.URL.indexOf("jobandpayJobHistory.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'JobHistory';
	}
	if(document.URL.indexOf("careerSkills.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'EmployeeSkills';
	}
	if(document.URL.indexOf("careerEducation.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'Education';
	}
	if(document.URL.indexOf("careerPreviousEmployer.php") >= 0) {
		document.frmEmployeeInformation.SelectedTab.value = 'PreviousEmployer';
	}
}