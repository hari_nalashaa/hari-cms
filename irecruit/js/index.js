/**
 * Common for all ajax calls
 */
function getWidgetRequisitions(callback_url, widget_id) {
	
	$("#"+widget_id).find("tr:gt(0)").remove();

	var sort_value = document.getElementById('sort_dashboard_req').value;
	var sort_var = "&sort=A";
	if(sort_value == "A") {
		sort_var = "&sort="+sort_value;
		document.getElementById('sort_dashboard_req').value = "D";
	}
	if(sort_value == "D") {
		sort_var = "&sort="+sort_value;
		document.getElementById('sort_dashboard_req').value = "A";
	}

	$.ajax({
  		url: callback_url+sort_var,
		type: "GET",
		success: function(result) {
			$("#"+widget_id+" tbody").append(result);
    	}
	 });
}

function ajax_call(callback_url, content_block_id, raw_data) {
	$("#"+content_block_id).css("text-align","center");
	$("#"+content_block_id).html('<br><br><br><img src="'+irecruit_home+'/images/wait.gif" alt="Please Wait.."> &nbsp;Please Wait..<br><br><br><br><br><br>');
	
	if(typeof(header_navigation) == 'undefined') header_navigation = '';

	$.ajax({
  		url: callback_url, 
		type: "POST",
		data: raw_data,
		dataType: 'json',
		success: function(result) {
			$("#"+content_block_id).html(result.calendar);
			$("#selected_year").val(result.year);
			$("#selected_month").val(result.month);
			$("#selected_day").val(result.day);
			$("#calendar_date_label").html(result.calendar_label);
			ChangeViewColor(0);
    	}
	 });
}

function getMonthName(month) {
	var months = new Array();
	months[1] = "January";
	months[2] = "February";
	months[3] = "March";
	months[4] = "April";
	months[5] = "May";
	months[6] = "June";
	months[7] = "July";
	months[8] = "August";
	months[9] = "September";
	months[10] = "October";
	months[11] = "November";
	months[12] = "December";
	return months[month]; 
}

function setDashboardDate() {
	 var year  = $("#selected_year").val();
	 var month = $("#selected_month").val();
	 var day   = $("#selected_day").val();
	 
	 var month_name = getMonthName(parseInt(month));
	 if(month <= 9) {
		 var today_month = "0"+month;
	 }
	 
	 $("#calendar_date_label").html(month_name+" "+year);
	 $("#today_date").val(year+"-"+today_month+"-"+day);	 
}

function dashboardCalendarDataView(id) {
	var res = id.substr(6);
	$("#selected_calendar_view").val(res);
	$("#tcviewmonth, #tcviewweek, #tcviewday").removeAttr('class');
	$("#"+id).attr('class', 'dashbaord_cal_active_view');

	$(".calendardataview").hide();

	var year = $("#selected_year").val();
	var month = $("#selected_month").val();
	var day = $("#selected_day").val();
	var raw_data = "year="+year+"&month="+month+"&day="+day;
	
	if(res == 'month') {
		$("#cdatamonth").show();

		ajax_call(irecruit_home+"calendarMonthView.php", "cdatamonth", raw_data);
	}
	if(res == 'week') {
		$("#cdataweek").show();

		ajax_call(irecruit_home+"calendarWeekView.php", "cdataweek", raw_data);
	}
	if(res == 'day') {
		$("#cdataday").show();

		raw_data += "&view=day";
		ajax_call(irecruit_home+"calendarDayView.php", "cdataday", raw_data);
	}
}

function loadTodayCalendar() {
	$(".calendardataview").hide();
	$("#cdataday").show();

	$("#selected_calendar_view").val('day');
	$("#tcviewmonth, #tcviewweek, #tcviewday").removeAttr('class');
	$("#tcviewday").attr('class', 'dashbaord_cal_active_view');
	
	var today_date = $("#today_date").val();
	ajax_call(irecruit_home+"calendarDayView.php", "cdataday", "today="+today_date+"&view=today");
}

function loadCalendarView(navigation) {
	var calendar_view = $("#selected_calendar_view").val();

	var year 	= $("#selected_year").val();
	var month 	= $("#selected_month").val();
	var day 	= $("#selected_day").val();

	if(calendar_view == 'month') {
		var raw_data = "year="+year+"&month="+month+"&day="+day+"&view=day&navigation="+navigation;
		ajax_call(irecruit_home+"calendarMonthView.php", "cdatamonth", raw_data);
	}
	if(calendar_view == 'week') {
		var raw_data = "year="+year+"&month="+month+"&day="+day+"&view=day&navigation="+navigation;
		ajax_call(irecruit_home+"calendarWeekView.php", "cdataweek", raw_data);
	}
	if(calendar_view == 'day') {
		var raw_data = "year="+year+"&month="+month+"&day="+day+"&view=day&navigation="+navigation;
		ajax_call(irecruit_home+"calendarDayView.php", "cdataday", raw_data);
	}
}

function sortWotcApplicantsField(sort_field_key) {
	var frmobj = document.frmWotcApplicantsSearch;
	frmobj.wotc_sort_field.value = sort_field_key;

	if(frmobj.wotc_sort_order.value == 'desc') {
		frmobj.wotc_sort_order.value = 'asc';
	}
	else if(frmobj.wotc_sort_order.value == 'asc') {
		frmobj.wotc_sort_order.value = 'desc';
	}
	
	getWotcApplicantsByKeyword();
}

function sortApplicantsField(sort_field_key) {
	var frmobj = document.frmQuickNameSearch;
	frmobj.sort_field.value = sort_field_key;

	if(frmobj.sort_order.value == 'desc') {
		frmobj.sort_order.value = 'asc';
	}
	else if(frmobj.sort_order.value == 'asc') {
		frmobj.sort_order.value = 'desc';
	}
	
	getApplicantsByKeyword();
}

function sortRequisitionsField(sort_field_key) {
	var frmobj = document.frmRequisitionsSearch;
	frmobj.sort_field.value = sort_field_key;

	if(frmobj.sort_order.value == 'desc') {
		frmobj.sort_order.value = 'asc';
	}
	else if(frmobj.sort_order.value == 'asc') {
		frmobj.sort_order.value = 'desc';
	}
	
	getRequisitionsListByParams();
}

function getRequisitionsListByParams() {
	var keyword 		= $("#requisitions_keyword").val();
	var form 			= $('#frmRequisitionsSearch');
	var input_data 		= form.serialize();

	$.ajax({
	  	beforeSend: function () {
	  			$("#requisitions_list").html('<br><h4>Please wait..</h4> <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br>');
	  	},
	  	url: irecruit_home + "requisitions/getRequisitionsListByParams.php", 
		type: "POST",
		data : input_data,
	  	success: function(result) {
   		  	var requisitions_list = 	'';
   		  	var table_data	=	'<table width="100%" class="table table-striped">';
   		 	table_data	+=	'<tr>';
   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortRequisitionsField(\'job_title\');">Job Title</a></strong></td>';
   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortRequisitionsField(\'requisition_id\');">RequisitionID</a>';
   		 	table_data	+=	' / ';
   		 	table_data	+=	'<a href="javascript:void(0);" onclick="sortRequisitionsField(\'job_id\');">JobID</a></strong></td>';
   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortRequisitionsField(\'posted_date\');">PostDate</a></strong></td>';
   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);">Expiry Date</a></strong></td>';
   		 	table_data	+=	'<td align="center"><strong><a href="javascript:void(0);">Expires In</a></strong></td>';
   		 	table_data	+=	'<td align="center"><strong><a href="javascript:void(0);">Applications Count</a></strong></td>';
   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);">History</a></strong></td>';
   		 	table_data	+=	'</tr>';
			var has_data = false;
   		  	jQuery.each(JSON.parse(result), function(item, val) {
				var RequestID 			= val.RequestID;
				var RequisitionID 		= val.RequisitionID;
				var JobID 				= val.JobID;
				var PostDate 			= val.PostDate;
				var RequisitionTitle 	= val.Title;
				var Active				= val.Active;
				var ExpireDate			= val.ExpireDate;
				var ExpireDays			= val.ExpireDays;
				var ApplicationsCount	= val.ApplicationsCount;
				
				
				requisitions_list 	+=	'<tr>';
				requisitions_list 	+=	'<td align="left">';
				requisitions_list 	+=	'<a href="requisitionsSearch.php?menu=3&RequestID='+RequestID+'&Active='+Active+'" target="_blank">'+RequisitionTitle+'</a>';
				requisitions_list 	+=	'</td>';
				requisitions_list 	+=	'<td align="left">'+RequisitionID+'/'+JobID+'</td>';
				requisitions_list 	+=	'<td align="left">'+PostDate+'</td>';
				
				requisitions_list 	+=	'<td align="left">'+ExpireDate+'</td>';
				requisitions_list 	+=	'<td align="center">'+ExpireDays+'</td>';
				requisitions_list 	+=	'<td align="center">'+ApplicationsCount+'</td>';
				requisitions_list 	+=	'<td align="left"><a href="'+irecruit_home+'requisitionsSearch.php?menu=3&RequestID='+RequestID+'&active_tab=requisition-history">History</a></td>';
				requisitions_list 	+=	'</tr>';
	   		 	has_data	= true;
		    });
		    
		    if(has_data == true) {
		    	table_data	+=	requisitions_list;
			}
		    else {
		    	table_data	+=	'<tr><td colspan="4" align="center">No records found, based on your keyword</td></tr>';
			}
		    table_data	+=	'<table>';
   		    $("#requisitions_list").html(table_data);
   		 	
    	}
	});
		
}

function getApplicantsByKeyword() {
	var keyword 			= 	$("#applicants_keyword").val();

	if(typeof(keyword) != 'undefined') {
		var keyword_length 	= 	keyword.length;
		var form 			= 	$('#frmQuickNameSearch');
		var input_data 		= 	form.serialize();

		$.ajax({
			beforeSend: function () {
				$("#applicants_list").html('<br><h4>Please wait..</h4> <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br>');
		  	},
		  	url: irecruit_home + "responsive/getApplicantsByKeyword.php", 
			type: "POST",
			data : input_data,
		  	success: function(result) {
	   		  	var applicants_list = 	'';
	   		  	var table_data	=	'<table width="100%" class="table table-striped">';
	   		 	table_data	+=	'<tr>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortApplicantsField(\'app_id\');">ApplicationID</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortApplicantsField(\'app_name\');">Applicant Name</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong>Applicant Email</strong></td>';
	   		 	table_data	+=	'<td align="left"><strong>Status</strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortApplicantsField(\'req_title\');">Requisition Title</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortApplicantsField(\'entry_date\');">Application Date</a></strong></td>';
	   		 	table_data	+=	'</tr>';
				var has_data = false;
	   		  	jQuery.each(JSON.parse(result), function(item, val) {
					var AppID				=	val.ApplicationID;
					var AppName				=	val.ApplicantSortName;
					var ApplicantEmail		=	val.ApplicantEmail;
					var ProcessOrderDesc	=	val.ProcessOrderDescription;
					var AppEntryDate		=	val.EntryDate;
					var AppRequestID		=	val.RequestID;
					var RequisitionTitle	=	val.Title;
					
					var application_link	=	irecruit_home + 'applicantsSearch.php?ApplicationID='+AppID+'&RequestID='+AppRequestID
						
		   		  	applicants_list +=	'<tr>';
					applicants_list +=	'<td align="left"><a href="'+application_link+'" target="_blank">'+AppID+'</a></td>';
					applicants_list +=	'<td align="left">'+AppName+'</td>';
					applicants_list +=	'<td align="left">'+ApplicantEmail+'</td>';
					applicants_list +=	'<td align="left">'+ProcessOrderDesc+'</td>';
					applicants_list +=	'<td align="left">'+RequisitionTitle+'</td>';
					applicants_list +=	'<td align="left">'+AppEntryDate+'</td>';
					applicants_list +=	'</tr>';
					
		   		 	has_data				=	true;
			    });
			    
			    if(has_data == true) {
			    	table_data	+=	applicants_list;
				}
			    else {
			    	table_data	+=	'<tr><td colspan="6" align="center">No records found, based on your keyword</td></tr>';
				}
			    table_data	+=	'<table>';
	   		    $("#applicants_list").html(table_data);
	   		 	
	    	}
		 	
		});
	}
}

function sendTwilioConversationMessage(btn_obj, div_id) {
	var ApplicationID	=	document.frmTwilioConversation.ApplicationID.value;
	var RequestID		=	document.frmTwilioConversation.RequestID.value;

	var input_data		=	{"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	$("#"+div_id).html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	var form_obj 	=	$(btn_obj).parents('form:first');
	var input_data 	=	$(form_obj).serialize();
	
	var request		=	$.ajax({
							method: "POST",
					  		url: irecruit_home + "applicants/getApplicantTwilioConversation.php",
					  		data: input_data,
							type: "POST",
							success: function(data) {
								$("#"+div_id).html(data);
					    	}
						});
}

function sortTwilioApplicantsField(sort_field_key) {
	var frmobj = document.frmTwilioQuickNameSearch;
	frmobj.twilio_sort_field.value = sort_field_key;

	if(frmobj.twilio_sort_order.value == 'desc') {
		frmobj.twilio_sort_order.value = 'asc';
	}
	else if(frmobj.twilio_sort_order.value == 'asc') {
		frmobj.twilio_sort_order.value = 'desc';
	}
	
	getTwilioApplicants();
}

function getConversationInfo(ResourceID, ApplicationID, RequestID, DivID) {
	
	$("#"+DivID).css("text-align","center");
	$("#"+DivID).html('<br><br><br><img src="'+irecruit_home+'/images/wait.gif" alt="Please Wait.."> &nbsp;Please Wait..<br><br><br><br><br><br>');
	
	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	//divConversationInfo
	$("#"+DivID).html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	var request	=	$.ajax({
							method: "POST",
					  		url: irecruit_home + "applicants/getApplicantTwilioConversation.php?DivID="+DivID,
					  		data: input_data,
							type: "POST",
							success: function(data) {
								$("#"+DivID).html(data);
					    	}
						});
	
}

function getTwilioApplicants() {
	
	var keyword	= $("#twilio_applicants_keyword").val();
	
	if(typeof(keyword) != "undefined") {
		var keyword_length 	= keyword.length;
		var form 			= $('#frmTwilioQuickNameSearch');
		var input_data 		= form.serialize();
	
		$.ajax({
	  		beforeSend: function () {
		  		$("#twilio_applicants_list").html('<br><h4>Please wait..</h4> <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br>');
		  	},
		  	url: irecruit_home + "responsive/getTwilioApplicantsByKeyword.php", 
			type: "POST",
			data : input_data,
		  	success: function(result) {
	   		  	var twilio_applicants_list = 	'';
	   		  	var table_data	=	'<table width="100%" class="table table-striped">';
	   		 	table_data	+=	'<tr>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortTwilioApplicantsField(\'app_id\');">ApplicationID</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortTwilioApplicantsField(\'app_name\');">Applicant Name</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong>Applicant Email</strong></td>';
	   		 	table_data	+=	'<td align="left"><strong>Status</strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortTwilioApplicantsField(\'req_title\');">Requisition Title</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortTwilioApplicantsField(\'entry_date\');">Application Date</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong>iRecruit Text</strong></td>';
	   		 	table_data	+=	'</tr>';
				
	   		  	jQuery.each(JSON.parse(result), function(item, val) {
					var AppID				=	val.ApplicationID;
					var AppName				=	val.ApplicantSortName;
					var ApplicantEmail		=	val.ApplicantEmail;
					var ProcessOrderDesc	=	val.ProcessOrderDescription;
					var AppEntryDate		=	val.EntryDate;
					var AppRequestID		=	val.RequestID;
					var RequisitionTitle	=	val.Title;
					var CellPhonePermission	=	val.CellPhonePermission;
					var ResourceID			=	(val.ResourceID == null) ? "" : val.ResourceID;
					
					var application_link	=	irecruit_home + 'applicantsSearch.php?ApplicationID='+AppID+'&RequestID='+AppRequestID+'&tab_action=twilio-sms'
						
					twilio_applicants_list +=	'<tr>';
					twilio_applicants_list +=	'<td align="left"><a href="'+application_link+'" target="_blank">'+AppID+'</a></td>';
					twilio_applicants_list +=	'<td align="left">'+AppName+'</td>';
					twilio_applicants_list +=	'<td align="left">'+ApplicantEmail+'</td>';
					twilio_applicants_list +=	'<td align="left">'+ProcessOrderDesc+'</td>';
					twilio_applicants_list +=	'<td align="left">'+RequisitionTitle+'</td>';
					twilio_applicants_list +=	'<td align="left">'+AppEntryDate+'</td>';
					
					twilio_applicants_list +=	'<td align="left">';
					
					if(CellPhonePermission == "Yes") {
						if(ResourceID != "") {
							twilio_applicants_list +=	'<a href=\'#openFormWindow\' onclick=\'getConversationInfo("'+ResourceID+'", "'+AppID+'", "'+AppRequestID+'", "widget_popup_info")\'>View Conversation</a>';
						}
						else {
							twilio_applicants_list +=	'<a href=\'#openFormWindow\' onclick=\'getConversationInfo("'+ResourceID+'", "'+AppID+'", "'+AppRequestID+'", "widget_popup_info")\'>Create Conversation</a>';
						}
					}
					else {
						twilio_applicants_list +=	' - ';
					}
					
					twilio_applicants_list +=	'</td>';
					
					twilio_applicants_list +=	'</tr>';
					
		   		 	has_data				=	true;
			    });
			    
			    if(has_data == true) {
			    	table_data	+=	twilio_applicants_list;
				}
			    else {
			    	table_data	+=	'<tr><td colspan="4" align="center">No records found, based on your keyword</td></tr>';
				}
			    
			    table_data	+=	'<table>';
	   		    $("#twilio_applicants_list").html(table_data);
	   		 	
	    	}
		 	
		});
	}	
}

function getWotcApplicantsByKeyword() {
	var keyword 		= $("#wotc_applicants_keyword").val();
	
	if(typeof(keyword) != "undefined") {
	 	var keyword_length 	= keyword.length;
		var form 			= $('#frmWotcApplicantsSearch');
		var input_data 		= form.serialize();

		$.ajax({
			beforeSend: function () {
				$("#wotc_applicants_list").html('<br><h4>Please wait..</h4> <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br>');
		  	},
		  	url: irecruit_home + "responsive/getWotcApplicantsByKeyword.php", 
			type: "POST",
			data : input_data,
		  	success: function(result) {

	   		  	var applicants_list = 	'';
	   		  	var table_data	=	'<table width="100%" class="table table-striped">';
	   		 	table_data	+=	'<tr>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortWotcApplicantsField(\'app_id\');">ApplicationID</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortWotcApplicantsField(\'app_name\');">Applicant Name</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortWotcApplicantsField(\'req_title\');">Requisition Title</a></strong></td>';
	   		 	table_data	+=	'<td align="left"><strong>CMS WOTC Application Status</strong></td>';
	   		 	table_data	+=	'<td align="left"><strong><a href="javascript:void(0);" onclick="sortWotcApplicantsField(\'entry_date\');">Application Date</a></strong></td>';
	   		 	table_data	+=	'</tr>';

				var has_data = false;
	   		  	jQuery.each(JSON.parse(result), function(item, val) {
					var AppID					=	val.IrecruitApplicationID;
					var AppRequestID			=	val.IrecruitRequestID;
					var AppName					=	val.ApplicantSortName;
					var RequisitionTitle		=	val.RequisitionTitle;
					var WotcApplicationStatus	=	val.Qualifies;
					var AppEntryDate			=	val.CreatedDateTime;

					var application_link		=	irecruit_home + 'applicantsSearch.php?ApplicationID='+AppID+'&RequestID='+AppRequestID+'&tab_action=onboard-applicant&OnboardSource=WOTC';
					
		   		  	applicants_list +=	'<tr>';
					applicants_list +=	'<td align="left"><a href="'+application_link+'" target="_blank">'+AppID+'</a></td>';
					applicants_list +=	'<td align="left">'+AppName+'</td>';
					applicants_list +=	'<td align="left">'+RequisitionTitle+'</td>';
					applicants_list +=	'<td align="left">'+WotcApplicationStatus+'</td>';
					applicants_list +=	'<td align="left">'+AppEntryDate+'</td>';
					applicants_list +=	'</tr>';
					
		   		 	has_data				=	true;
			    });
			    
			    if(has_data == true) {
			    	table_data	+=	applicants_list;
				}
			    else {
			    	table_data	+=	'<tr><td colspan="6" align="center">No records found, based on your keyword</td></tr>';
				}
			    
			    table_data	+=	'<table>';
			    
	   		    $("#wotc_applicants_list").html(table_data);
	   		 	
	    	}
		 	
		});
	}
}

$('#theme-change').click(function() {
	if($('#theme-change').width() > 28) 
		$('#theme-change').css({"width": "30px", "height": "28px"});
	else 	
		$('#theme-change').css({"width": "212px", "height": "70px", "transition": "1s", "-webkit-transition": "1s"});
});

// When the document is ready set up our sortable with it's inherant function(s)
$(document).ready(function() {

	if($("#blocktype").val() == "2") {
		$("#div_new_applicants_interval").show();
	}
	else {
		$("#div_new_applicants_interval").hide();
	}
	
	$( "#blocktype" ).bind("change", function() {
		if($("#blocktype").val() == "2") {
			$("#div_new_applicants_interval").show();
		}
		else {
			$("#div_new_applicants_interval").hide();
		}
	});
	
    $("#moduleslist").sortable({
    	 cursor: 'move',
    	 handle: '.panel',
    	 dropOnEmpty: false,
    	 placeholder: "highlight",
    	 item: '.panel',
    	 update : function () {
   		  	var order = $('#moduleslist').sortable('serialize');
   		  	 $.ajax({
   		  		beforeSend: function () {
   		  			$("#msgsortable").show();
   		  			$("#msgsortable").html("Please wait, your settings are going to save.");
	   		  	},
	   		  	url: irecruit_home + "responsive/saveSortableOrder.php", 
				type: "POST",
				data : order+"&userid="+user_id,
	   		  	success: function(result){
		     	   $("#msgsortable").html("Settings successfully updated.");
		    	}
			 });
         }
	});


    $("#topblocks").sortable({
    	cursorAt: { top: 0, left: 0 },
    	 cursor: 'move',
    	 update : function () {
   		  	var order = $('#topblocks').sortable('serialize');
   		  	 $.ajax({
   		  		beforeSend: function () {
   		  			$("#msgtopsortable").show();
   		  			$("#msgtopsortable").html("Please wait, your settings are going to save.");
	   		  	},
	   		  	url: irecruit_home + "responsive/saveSortableOrder.php", 
				type: "POST",
				data : order+"&userid="+user_id+"&type=topblocks",
	   		  	success: function(result) {
		     	   $("#msgtopsortable").html("Settings successfully updated.");
		    	}
			 });
         }
	});

	
    $("#piechartblocks").sortable({
    	 cursor: 'move',
    	 update : function () {
   		  	var order = $('#piechartblocks').sortable('serialize');
   		  	 $.ajax({
   		  		beforeSend: function () {
   		  			$("#msgsortable").show();
   		  			$("#msgsortable").html("<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>x</button>Please wait, your settings are going to save.");
	   		  	},
	   		  	url: irecruit_home + "responsive/saveSortableOrder.php", 
				type: "POST",
				data : order+"&userid="+user_id+"&type=pie",
	   		  	success: function(result){
		     	   $("#msgsortable").html("<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>x</button>Settings successfully updated.");
		    	}
			 });
         }
	});
    
});

$('#appointment_reminder_widget').datepicker({
    changeMonth: true,
    changeYear: true,
    showButtonPanel: true,
    showOn: 'button',
    buttonImage: irecruit_home + 'images/calendar.gif',
    yearRange: '-80:+5',
    buttonImageOnly: true,
    buttonText: 'Select Date'
}); 

         
$("#ui-datepicker-div").hide();

var forms_data = {
	forms_info: []
};
$( "#sort_internal_forms tbody" ).sortable({
	cursor: 'move',
	placeholder: 'ui-state-highlight',
    stop: function( event, ui ) {
        
    	forms_data = {
    			forms_info: []
    	};
    	
    	$(this).find('tr').each(function(i) {

        	var SortOrder = i;
            SortOrder++;
            var tr_id = $(this).attr('id');
			var tr_info = tr_id.split("*");
           
            var FormID = tr_info[1];
            var FormType = tr_info[2];
        	
        	forms_data.forms_info.push({
        		 "FormID"    : FormID,
        		 "FormType"  : FormType,
        		 "SortOrder" : SortOrder
        	});
        });
        
        updateInterAssignedFormSortOrder(forms_data);
    }
});

function getApplicantsSourceCountByRequestID(RequestID, FromDate, ToDate, Status) {

	var REQUEST_URL = irecruit_home + "responsive/getApplicantsCountByRequisitionAndQuestion.php?RequestID="+RequestID+"&FromDate="+FromDate+"&ToDate="+ToDate+"&Question=informed&Status="+Status;
	
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		beforeSend: function() {
			
			$("#applicants_count_by_req_and_source").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");

		},
		success: function(data) {
			if(data != "") {
				var json_data_obj = JSON.parse(data);
				fun_app_by_source(json_data_obj);
			}
			else {
				$("#applicants_count_by_req_and_source").css("text-align","center");
				$("#applicants_count_by_req_and_source").html("<br><br><br>Sorry no records");
			}
		}
	});
}

/**
 * @params query_type(related to pie charts), from_date, to_date
 * @function filter_applicants
 */
function filter_applicants(query_type, from_date, to_date, module_id) {
	$("#module_status_message"+module_id).html("Please wait...");
	$.ajax({
		url: irecruit_home + "responsive/applicantsInformation.php", 
		type: "POST",
		data : "QUERYTYPE="+query_type+"&BeginDate="+from_date+"&FinalDate="+to_date,
		success: function(result) {
			if(result != "") {
				var json_data = jQuery.parseJSON(result);
				
		   	   	if(query_type == "APO") {
					fun_app_by_processorder(json_data);
				}
				else if(query_type == "ADC") {
	   		  		fun_app_by_disposition_code(json_data);
				}
				else if(query_type == "AD") {
					fun_app_by_distinction(json_data);
				}
				else if(query_type == "AS") {
					fun_app_by_searchable(json_data);
				}
		   	 	$("#module_status_message"+module_id).html("");
			}
	   	   	else {
				$("#module_status_message"+module_id).html("Sorry unable to fetch the records between these dates");
			}
    	}
	});
}

function getDashboardModuleReqByStatus(RequisitionStatus) {
    $('#ddlRequisitionsList').children('option:not(:first)').remove();

    var option_text		=	'';
    var option_value	=	'';
    var options_list    =   '';
    
	$.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?RequisitionStatus="+RequisitionStatus,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#message_status").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		+= " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#message_status").html("");
			$("#ddlRequisitionsList").append(options_list);
    	}
	});
}

//Script to load charts in home page on all templates except responsive 
if(typeof(str) != 'undefined' && str != "") {
	// <![CDATA[
	var chart = new AnyChart('anychart/swf/AnyChart.swf');
	chart.width = 700;
	chart.height = 200;
	chart.setXMLFile('anychart/newApplicants.php'+str);
	chart.wMode = "opaque";
	chart.write('chartDiv-1');
	// ]]>
}
