$(function() {
	//Set fromdate datepicker
	date_picker('#fromdate, #todate', 'mm/dd/yy');
	
	date_picker('#PostDate, #ExpireDate', 'mm/dd/yy');		
	
	$('.postexpiredate').each(function() {
		var current_id = this.id;
		var datepicker_id = "#"+current_id;
		date_picker(datepicker_id, 'mm/dd/yy');
    });
});

function delRequest(req_id, clicked_object) {
	
	req_title = $("#request-title-"+req_id).html();

	if(confirm('Are you sure you want to delete the following requisition?'+"\n"+req_title)) {
		$(clicked_object).closest('tr').remove();
		
		var request = $.ajax({
			method: "POST",
	  		url: irecruit_home + "requisitions/delRequisition.php?RequestID="+req_id+"&action=delete&process=Y",
			type: "POST",
			dataType: "json",
			beforeSend: function() {
				$("#del_process_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');	
			},
			success: function(data) {
				if(data.status == "failed") {
					$("#del_process_message").html("Failed to delete this requisition");
					$("#del_process_message").css({
				      "color": data.color,
				    });
				}
				if(data.status == "success") {
					
					$("#del_process_message").html(data.message);
					$("#del_process_message").css({
				      "color": data.color,
				    });
				}
	    	}
		});
		
		setAjaxErrorInfo(request, "#del_process_message");
	}
	
}


function getAssignInternalInfo(RequestID) {

	if(typeof(RequestID) == 'undefined') {
		if(typeof(document.frmRequisitionDetailInfo) != 'undefined') {
			var RequestID = document.frmRequisitionDetailInfo.RequestID.value;
		}
	}
	$("#assign-iconnect-form").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	var request = $.ajax({
		method: "POST",
  		url: "formsInternal/getAssignInternal.php?RequestID="+RequestID+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#assign-iconnect-form").html(data);
    	}
	});
}

function deleteIconnectInternalInfo(callback_url, confirm_msg) {
	
	$("#assign-iconnect-form").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	if(confirm(confirm_msg)) {
		var request = $.ajax({
			method: "POST",
	  		url: callback_url+'&display_app_header=no',
			type: "POST",
			success: function(data) {
				$("#assign-iconnect-form").html(data);

	    	    }               

		}); self.location.reload();
	}
}

function deleteAssignInternalInfo(callback_url, confirm_msg) {
	
	$("#assign-iconnect-form").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	if(confirm(confirm_msg)) {
		var request = $.ajax({
			method: "POST",
	  		url: callback_url+'&display_app_header=no',
			type: "POST",
			success: function(data) {
				$("#assign-iconnect-form").html(data);

	    	    }               

		}); 	}
}


function validateCreateNewRequisitions() {
	var error = false;
	if($(".jobid").val() == "") {
		$(".jobid").css("border", "1px solid red");
		error = true;
	}
	$( ".postexpiredate" ).each(function( index ) {
		if($(this).val() == "") {
			$(this).css("border", "1px solid red");
			error = true;
		}
	});
	if(error == true) {
		return false;
	}
	return true;
}

function loadRequisitionSearchLocation(MultiOrgID, AccessCode, iRecruitHome) {

    var xmlhttp;
    if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else
    {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function()
    {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            document.getElementById("data").innerHTML = xmlhttp.responseText;
        }
    }

    var d = new Date().getTime();

    xmlhttp.open("GET", iRecruitHome + "requisitions/loadRequisitionSearchLocations.php?MultiOrgID=" + MultiOrgID + "&new=" + d + "&k=" + AccessCode, true);
    xmlhttp.send();

} // end function


function setorder(skey) {
	var sorder = document.getElementById('sord').value;
	if(sorder == 'asc') document.getElementById('sord').value = 'desc';
	else if(sorder == 'desc') document.getElementById('sord').value = 'asc';
	document.getElementById('skey').value = skey;
	document.frmjobboard.submit();
}

function refreshResults() {
	document.getElementById("frmjobboard").method = "post";
	document.getElementById('refreshresults').value = 'Yes';
}

function loadRequisitionsJobTitles(iRecruitHome, AccessCode) {
	var d = new Date().getTime(); 
	
	var a = document.getElementById('Active').value;
	var b = document.getElementById('MultiOrgID').value;
	var c = document.getElementById('criteria').value;
	var r = document.getElementById('refinereq').value;

	/*
	var url = iRecruitHome + "applicants/loadRequisitions.php?callback=?";
	//var url = "https://dev.irecruit-us.com/irecruit/edge/edge.php"; // The server URL
	$.getJSON( url, {MultiOrgID: b, Active: a, criteria: c, k: AccessCode}, showRequisitions);
	*/

	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("requests").innerHTML=xmlhttp.responseText;
	    }
	  }

	xmlhttp.open("GET", iRecruitHome + "applicants/loadRequisitions.php?MultiOrgID=" + b + "&Active=" + a + "&criteria=" + c + "&refine=" + r + "&new=" + d + "&k=" + AccessCode,true);
	xmlhttp.send();

} // end function

