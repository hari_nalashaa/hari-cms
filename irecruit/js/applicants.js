var StatesList = new Array();
StatesList['AL'] = 'Alabama';
StatesList['AK'] = 'Alaska';
StatesList['AZ'] = 'Arizona';
StatesList['AR'] = 'Arkansas';
StatesList['CA'] = 'California';
StatesList['CO'] = 'Colorado';
StatesList['CT'] = 'Connecticut';
StatesList['DE'] = 'Delaware';
StatesList['DC'] = 'District Of Columbia';
StatesList['FL'] = 'Florida';
StatesList['GA'] = 'Georgia';
StatesList['HI'] = 'Hawaii';
StatesList['ID'] = 'Idaho';
StatesList['IL'] = 'Illinois';
StatesList['IN'] = 'Indiana';
StatesList['IA'] = 'Iowa';
StatesList['KS'] = 'Kansas';
StatesList['KY'] = 'Kentucky';
StatesList['LA'] = 'Louisiana';
StatesList['ME'] = 'Maine';
StatesList['MD'] = 'Maryland';
StatesList['MA'] = 'Massachusetts';
StatesList['MI'] = 'Michigan';
StatesList['MN'] = 'Minnesota';
StatesList['MS'] = 'Mississippi';
StatesList['MO'] = 'Missouri';
StatesList['MT'] = 'Montana';
StatesList['NE'] = 'Nebraska';
StatesList['NV'] = 'Nevada';
StatesList['NH'] = 'New Hampshire';
StatesList['NJ'] = 'New Jersey';
StatesList['NM'] = 'New Mexico';
StatesList['NY'] = 'New York';
StatesList['NC'] = 'North Carolina';
StatesList['ND'] = 'North Dakota';
StatesList['OH'] = 'Ohio';
StatesList['OK'] = 'Oklahoma';
StatesList['OR'] = 'Oregon';
StatesList['PA'] = 'Pennsylvania';
StatesList['RI'] = 'Rhode Island';
StatesList['SC'] = 'South Carolina';
StatesList['SD'] = 'South Dakota';
StatesList['TN'] = 'Tennessee';
StatesList['TX'] = 'Texas';
StatesList['UT'] = 'Utah';
StatesList['VT'] = 'Vermont';
StatesList['VA'] = 'Virginia';
StatesList['WA'] = 'Washington';
StatesList['WV'] = 'West Virginia';
StatesList['WI'] = 'Wisconsin';
StatesList['WY'] = 'Wyoming';

function changePage(newLoc) {
	nextPage = newLoc.options[newLoc.selectedIndex].value;

	if (nextPage != "") {
		document.location.href = nextPage;
	}
}

function checkAll(checkname, exby) {
	for (var i = 0; i < checkname.length; i++)
		checkname[i].checked = exby.checked ? true : false;
}

function listConfirm(checkname, ddl_action) {
	
	var form = document.getElementById('multipleapps');
	
	if(ddl_action.value == "comparative_analysis") {
		
	    var ii = 0;
	    for(var n=0;n < form.length;n++){
	      if(form[n].name == 'appid[]' && form[n].checked){
	    	  ii++;
	      }
	    }
	    
		// confirm the count and confirm if more than one
		if (ii <= 0) {
			alert('Please select an application to process.');
			return false;
		}
		
		form.action = 'applicants/comparativeAnalysis.php?menu=10';
		document.forms['multipleapps'].submit();
	} 
	else if(ddl_action.value == "send_bulk_sms") {
		
	    var ii = 0;
	    for(var n=0;n < form.length;n++){
	      if(form[n].name == 'send_bulk_sms[]' && form[n].checked){
	    	  ii++;
	      }
	    }
	    
		// confirm the count and confirm if more than one
		if (ii <= 0) {
			alert('Please select atleast one send sms option.');
			return false;
		}
		
		form.action = 'applicants/sendBulkSms.php?menu=2';
		document.forms['multipleapps'].submit();
	}
	else {
	    var ii = 0;
	    for(var n=0;n < form.length;n++){
	      if(form[n].name == 'appid[]' && form[n].checked){
	    	  ii++;
	      }
	    }
	    
		// confirm the count and confirm if more than one
		if (ii > 0) {
			if (ddl_action.value == 'deleteapplicant') {
				var agree = confirm('Are you sure you want to process these applications?');
			} else {
				return true;
			}
		} else {
			alert('Please select an application to process.');
		}
		// process confirm
		if (agree) {
			return true;
		} else {
			return false;
		}
	}
}

function validate_date(formdate,formmessage) {

	var err = 0;
	var errtxt = '';
	
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/ //Basic check for format validity
	
	if (formdate != "") {
		  if (!validformat.test(formdate)){
			   errtxt += "Invalid '" + formmessage + "' Format.\n\nEnter as mm/dd/yyyy\n";
			   err++;
		  }
		  else 
		  {
			   var monthfield=formdate.split("/")[0]
			   var dayfield=formdate.split("/")[1]
			   var yearfield=formdate.split("/")[2]
			   var dayobj = new Date(yearfield, monthfield-1, dayfield)
		
			   if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield)) {
				   errtxt += "Invalid '" + formmessage + "' Day, Month, or Year range detected.\n\nEnter as mm/dd/yyyy\n\n";
				   err++;
			   }
		  }
	}
	
	// general error
	if (err > 0) {
	    alert(errtxt);
	    return false;
	} 
	return true;

}


function ValidateZipCode(zip_input_obj, form_name) {
	var msg = "";
	if (IsNumeric(zip_input_obj.value) == false) {
		msg = 'Zip Code must only be numbers.';
	}
	
	if(msg != "") {
		$(zip_input_obj).next(".zipmessage").html('<span style="color:red">&nbsp;Zip code must only be numbers.</span>');
		return false;
	}
	
	$(zip_input_obj).next(".zipmessage").html('<img src="'+irecruit_home+'images/loading-small.jpg" alt="Validating Zipcode..Please wait.." />');
}

function applicantsSubmitAction(action_item) {
	frm_action = irecruit_home+"applicants.php?action="+action_item;
	document.frmApplicantsNavigation.action = frm_action; 
	document.forms['frmApplicantsNavigation'].submit();
}

$(document).ready(function() {
	$('.zip_input').each(function(){
		var form_name = $(this).parents("form").attr('name');
		if(this.value != "") ValidateZipCode(this, form_name);
	});
	
	$('.zip_input').change(function() {
		var form_name = $(this).parents("form").attr('name');
		if(this.value != "") ValidateZipCode(this, form_name);
	});
	
	if(typeof(datepicker_ids) != "undefined" && datepicker_ids != "") {
		date_picker(datepicker_ids, date_format);
	}
	
	$('body').tooltip({
	    selector: '[data-toggle="tooltip"]'
	});

});
