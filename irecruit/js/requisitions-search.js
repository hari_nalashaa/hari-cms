function toggleVerticalResultsText() {
	
	if(document.getElementById('search-results-detail').style.display == "block")
	{
		document.getElementById('search-results-detail').style.display = 'none';
		$('#applicant-detail-view').show();
		$('#requisitions-minified-search-results').show();
	}
	else if(document.getElementById('search-results-detail').style.display == "none")
	{
		document.getElementById('search-results-detail').style.display = 'block';
		$('#applicant-detail-view').hide();
		$('#requisitions-minified-search-results').hide();
	}
	
}

//Make the first letter capital in string
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//Set error message based on response
function setError(error_message, display_id) {
	error_message = capitalizeFirstLetter(error_message);
	$(display_id).html("<br>"+"<h3>"+error_message+"</h3>");
}

//Set ajax error information
function setAjaxErrorInfo(request, display_id) {
	request.done(function() {
	    //console.log('success');
	});
	request.fail(function(jqXHR, textStatus, errorThrown) {
		if(textStatus === 'timeout')
	    {
	        setError('Sorry, unable to complete your request. Please try again.', display_id)
	    }
		else {
			setError(textStatus+": "+errorThrown, display_id);
		}
	});
	request.error(function( jqXHR, textStatus, errorThrown ) {
		if(textStatus === 'timeout')
	    {     
	        setError('Sorry, unable to complete your request. Please try again.', display_id)
	    }
		else {
			setError(textStatus+": "+errorThrown, display_id);
		}
	});
}

function getRequisitionNotes() {

	var access_code =	document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id		=	document.frmRequisitionDetailInfo.RequestID.value;
	var active		=	document.frmRequisitionDetailInfo.Active.value;
	var request_listing_url = "requisitions/requisitionNotes.php?RequestID="+req_id+"&Active="+active;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
							method: "POST",
					  		url: request_listing_url,
							type: "POST",
							beforeSend: function(){
								$("#requisition-notes").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
							},
							success: function(data) {
								$("#requisition-notes").html(data);
					    	}
						});
	setAjaxErrorInfo(request, "#requisition-notes");
}

function addRequisitionNotes(btn_obj) {

	var req_stage 			= document.getElementById('ddlRequisitionStage').value;
	var req_stage_reason  	= document.getElementById('ddlRequisitionStageReason').value;
	var req_stage_date  	= document.getElementById('txtRequisitionStageDate').value;
	var req_stage_comments  = document.getElementById('RequisitionStageComments').value;
	
	
	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();
	
	var request_listing_url = "requisitions/requisitionNotes.php?RequestID="+req_id+"&RequisitionStage="+req_stage+"&RequisitionStageReason="+req_stage_reason+"&RequisitionStageDate="+req_stage_date;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	if(req_stage == "" && req_stage_reason != "") {
		alert("Please select a Requisition Stage");
	}
	else if(req_stage == "" && req_stage_reason == "" && req_stage_comments == "") {
		alert("Please add note");
	}
	else {
		var request = $.ajax({
			method: "POST",
	  		url: request_listing_url,
			type: "POST",
			data: input_data,
			beforeSend: function() {
				$("#requisition-notes").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
			},
			success: function(data) {
				$("#requisition-notes").html(data);
	    	}
		});
		setAjaxErrorInfo(request, "#requisition-notes");
		
	}
}

function deleteRequisitionNotes(updateid) {
	
	if(confirm('Are you sure you want to delete this comment?')) {

		var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
		var req_id = document.frmRequisitionDetailInfo.RequestID.value;
		
		var request_listing_url = "requisitions/requisitionNotes.php?RequestID="+req_id+"&updateid="+updateid;
		if(access_code != "") {
			request_listing_url += "&k="+access_code;
		}
		
		var request = $.ajax({
			method: "POST",
	  		url: request_listing_url,
			type: "POST",
			beforeSend: function(){
				$("#requisition-notes").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
			},
			success: function(data) {
				$("#requisition-notes").html(data);
	    	}
		});
		setAjaxErrorInfo(request, "#requisition-notes");
	}
	
}

function getAutoForwardListForm() {

	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var active = document.frmRequisitionDetailInfo.Active.value;
	
	var request_listing_url = "requisitions/autoForwardList.php?RequestID="+req_id+"&Active="+active;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		data: {"RequestID":req_id},
		beforeSend: function(){
			$("#auto-forward-list").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#auto-forward-list").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#auto-forward-list");
}

function updateAutoForwardListForm(btn_obj) {
	
	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var active = document.frmRequisitionDetailInfo.Active.value;
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();
	
	var request_listing_url = "requisitions/autoForwardList.php?RequestID="+req_id+"&Active="+active;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#auto-forward-list").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#auto-forward-list").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#auto-forward-list");
}

function getPrescreenQuestionsForm() {

	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var active = document.frmRequisitionDetailInfo.Active.value;
	
	var request_listing_url = "requisitions/prescreen.php?RequestID="+req_id+"&Active="+active;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		data: {"RequestID":req_id},
		beforeSend: function(){
			$("#prescreen-questions").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#prescreen-questions").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#prescreen-questions");
}


function getRequisitionTabInfo(req_id, multi_org_id) { 
	document.frmRequisitionDetailInfo.RequestID.value = req_id;
	document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;
	var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;
	
	getRequisitionDetailInfo(req_id, multi_org_id);
	getSelectedTabInfo(selected_tab, req_id);
}

function getRequisitionDetailInfo(req_id, multi_org_id) {

	if(typeof(MonsterJobIndustries) == 'string') MonsterJobIndustries = JSON.parse(MonsterJobIndustries); 
	if(typeof(JobCategories) == 'string') JobCategories = JSON.parse(JobCategories);
	if(typeof(JobOccupations) == 'string') JobOccupations = JSON.parse(JobOccupations);

	$("#process_message_req_details").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	document.frmRequisitionDetailInfo.RequestID.value = req_id;
	document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;
	var active = document.frmRequisitionDetailInfo.Active.value;
	var GuIDRequisition = document.frmRequisitionDetailInfo.GuIDRequisition.value;
	
	var req_search_res_len = 0;
	if(typeof(requisitions_list) == 'string') {
		var req_search_res = JSON.parse(requisitions_list);
		req_search_res_len = req_search_res.length;
	}
	else if(typeof(requisitions_list) == 'object') {
		var req_search_res = requisitions_list;
		req_search_res_len = requisitions_list.length;
	}
	
	if(req_search_res_len > 0) {
		if(req_id == req_search_res[0].RequestID) {
			$("#req_detail_prev").hide();
		}
		if(req_id != req_search_res[0].RequestID) {
			$("#req_detail_prev").show();
		}
		if(req_id == req_search_res[req_search_res_len - 1].RequestID) {
			$("#req_detail_next").hide();
		}
		if(req_id != req_search_res[req_search_res_len - 1].RequestID) {
			$("#req_detail_next").show();
		}
		
		var request_listing_url = "requisitions/getRequisitionDetailInfo.php?RequestID="+req_id+"&MultiOrgID="+multi_org_id+"&Active="+active+"&GuIDRequisition="+GuIDRequisition;
		
		var request = $.ajax({
			method: "POST",
	  		url: request_listing_url,
			type: "POST",
			dataType: "json",
			success: function(data) {
				
				document.frmRequisitionDetailInfo.Active.value = data.Active;

				var active = document.frmRequisitionDetailInfo.Active.value;
				var jobboards_display_status = "True";
				var RequisitionJobBoards = data.RequisitionJobBoards;
				
				if(RequisitionJobBoards.PaidZipRecruiter == "Yes" 
					|| RequisitionJobBoards.SubscribeZipRecruiter == "Yes")
				{
					$("#advert_header_trafficboost").hide();
					$("#advert_editreq_popup_ziprecruiter").hide();
				}
				if(RequisitionJobBoards.PaidZipRecruiter == "No" 
						&& RequisitionJobBoards.SubscribeZipRecruiter == "No")
				{
					$("#advert_header_trafficboost").show();
					$("#advert_editreq_popup_ziprecruiter").show();
				}
				
				if(RequisitionJobBoards.PaidIndeed == "Yes") {
					$("#advert_header_indeed").hide();
					$("#advert_editreq_popup_indeed").hide();
				}
				if(RequisitionJobBoards.PaidIndeed == "No") {
					$("#advert_header_indeed").show();
					$("#advert_editreq_popup_indeed").show();
				}
				
				if(RequisitionJobBoards.PaidMonster == "Yes") {
					$("#advert_header_monster").hide();
					$("#advert_editreq_popup_monster").hide();
				}
				if(RequisitionJobBoards.PaidMonster == "No") {
					$("#advert_header_monster").show();
					$("#advert_editreq_popup_monster").show();
				}
				
				if((RequisitionJobBoards.PaidZipRecruiter == "Yes" 
					|| RequisitionJobBoards.SubscribeZipRecruiter == "Yes")
					&& RequisitionJobBoards.PaidIndeed == "Yes"
					&& RequisitionJobBoards.PaidMonster == "Yes")
				{
					jobboards_display_status = "False";
				}
				
				if(active == "Y") {
					jobboards_display_status = "True";
					$("#advert_header_trafficboost").show();
					$("#advert_editreq_popup_ziprecruiter").show();
					$("#advert_header_indeed").show();
					$("#advert_editreq_popup_indeed").show();
					$("#advert_header_monster").show();
					$("#advert_editreq_popup_monster").show();
					$("#header_job_board_posting").parent().show();
				}
				else {
					jobboards_display_status = "False";
					$("#advert_header_trafficboost").hide();
					$("#advert_editreq_popup_ziprecruiter").hide();
					$("#advert_header_indeed").hide();
					$("#advert_editreq_popup_indeed").hide();
					$("#advert_header_monster").hide();
					$("#advert_editreq_popup_monster").hide();
					$("#header_job_board_posting").parent().hide();
				}
				
				$("#JobBoardsStatus").val(jobboards_display_status);
				
				$("#header_requisition_title").html(data.Title);
				$("#header_requisition_post_date").html(data.PostDate);
				$("#header_requisition_expire_date").html(data.ExpireDate);
				$("#header_requisition_requisition_id").html(data.RequisitionID);
				$("#header_requisition_job_id").html(data.JobID);
				$("#header_number_of_applicants").html(data.ApplicantsCount);
				
				if(data.Highlight != "" && data.Highlight != null) {
					if(data.Highlight == 'Hot') {
						$("#header_highlight_requisition").html("<strong>Highlight Status:</strong> "+hot_job_image);
					}
					else if(data.Highlight == 'New') {
						$("#header_highlight_requisition").html("<strong>Highlight Status:</strong> "+new_job_image);
					}
				}
				else {
					$("#header_highlight_requisition").html("");
				}
				
				if(data.MonsterJobIndustry == 0) {
					$("#header_monster_job_post_industry").html("All");
					document.frmRequisitionDetailInfo.MonsterJobIndustry.value = data.MonsterJobIndustry;
				}
				if(data.MonsterJobIndustry != "") {
					$("#header_monster_job_post_industry").html(MonsterJobIndustries[data.MonsterJobIndustry]);
					document.frmRequisitionDetailInfo.MonsterJobIndustry.value = data.MonsterJobIndustry;
				}
				
				if(data.MonsterJobIndustry === "") {
					$("#header_monster_job_post_industry").html("");
					document.frmRequisitionDetailInfo.MonsterJobIndustry.value = "";
				}
				
				if(data.feature.MonsterAccount != "Y") {
					$("#requisition_monster_information").hide();
					$("#header_monster_job_post_type, #header_monster_job_post_duration, #header_monster_job_post_occupation, #header_monster_job_post_category").html();
				}
				else {
					$("#requisition_monster_information").show();
					$("#header_monster_job_post_type").html(data.MonsterJobPostType);
					$("#header_monster_job_post_duration").html(data.Duration);
					
					document.frmRequisitionDetailInfo.MonsterJobPostDuration.value = data.Duration;
					document.frmRequisitionDetailInfo.MonsterJobPostType.value = data.MonsterJobPostType;
					
					if(typeof(data.MonsterJobCategory) != 'undefined'
						&& data.MonsterJobCategory != '') {
						$("#header_monster_job_post_category").html(JobCategories[data.MonsterJobCategory]);
						document.frmRequisitionDetailInfo.MonsterJobCategory.value = data.MonsterJobCategory;
					}
					else {
						$("#header_monster_job_post_category").html("");
						document.frmRequisitionDetailInfo.MonsterJobCategory.value = "";
					}

					//MonsterJobOccupation
					if(typeof(data.MonsterJobOccupation) != 'undefined'
						&& data.MonsterJobOccupation != '') {
						$("#header_monster_job_post_occupation").html(JobOccupations[data.MonsterJobOccupation]);
						document.frmRequisitionDetailInfo.MonsterJobOccupation.value = data.MonsterJobOccupation;
					}
					else {
						$("#header_monster_job_post_occupation").html("");
						document.frmRequisitionDetailInfo.MonsterJobOccupation.value = "";
					}
				}
				
				if(data.RequisitionStage != null && data.RequisitionStage != "") {
					$("#requisition_stage").html("<strong>Requisition Stage: </strong>"+data.RequisitionStage+", " + data.RequisitionStageDate + '<br>');
				}
				else {
					$("#requisition_stage").html("");
				}
				
				if(data.Active == "Y" && data.Approved == "Y") {
					$("#header_job_board_posting").prop('href', irecruit_home+'jobBoardPosting.php?menu=3&RequestID='+req_id);
				}
				else {
					$("#header_job_board_posting").prop('href', 'javascript:void(0);');
				}
				$("#process_message_req_details").html('');
				if(data.InterviewAdmin == "True") {
                		  $("#tab-interview-admin").show();
				}
				else {
                		  $("#tab-interview-admin").hide();
				}

				if(data.InterviewResults == "True") {
                		  $("#tab-interview-results").show();
				}
				else {
                		  $("#tab-interview-results").hide();
				}
	    	}
		});
		
		setAjaxErrorInfo(request, "#process_message_req_details");
	}
	else {
		$("#requisitions-minified-results").html("<div class='col-lg-12'><strong>No results found. Please update your search filters and try again.</strong></div>");
		$("#process_message_req_details").html("");
	}
	
	getAssignedFormsCount();
}

function updateRequisitionStage() {
	
	var req_id 			=	document.frmRequisitionDetailInfo.RequestID.value;
	var req_stage 		=	document.getElementById('ddlRequisitionStage').value;
	var req_stage_date	=	document.getElementById('txtRequisitionStageDate').value;
	
	var request = $.ajax({
		method: "POST",
  		url: 	"requisitions/updateRequisitionStatus.php?RequestID="+req_id+"&RequisitionStage="+req_stage+"&RequisitionStageDate="+req_stage_date,
		type:	"POST",
		success: function(data) {
			$("#process_message_req_details").css({
			      "color": nav_color,
			});
			
			$("#process_message_req_details").html("<div class='col-lg-12'><br><strong>Updated successfully.</strong><br></div>");
    	}
	});

	setAjaxErrorInfo(request, "#header_number_of_assign_forms");
}

function getAssignedFormsCount() {
	
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request = $.ajax({
		method: "POST",
  		url: "requisitions/getAssignedFormsCount.php?RequestID="+req_id,
		type: "POST",
		success: function(data) {
			$("#header_number_of_assign_forms").html(data);
    	}
	});

	setAjaxErrorInfo(request, "#header_number_of_assign_forms");
}

function delRequisition(req_id, req_title, clicked_object) {

	if(confirm('Are you sure you want to delete the following requisition?'+"\n"+req_title)) {
		$(clicked_object).closest('tr').remove();
		
		var active = document.frmRequisitionDetailInfo.Active.value;
		
		var request = $.ajax({
			method: "POST",
	  		url: "requisitions/delRequisition.php?RequestID="+req_id+"&action=delete&process=Y&Active="+active,
			type: "POST",
			dataType: "json",
			beforeSend: function() {
				$("#del_process_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');	
			},
			success: function(data) {
				if(data.status == "failed") {
					$("#del_process_message").html("Failed to delete this requisition");
					$("#del_process_message").css({
				      "color": data.color,
				    });
				}
				if(data.status == "success") {
					
					getRequisitionsListByFilters('yes');
					
					$("#del_process_message").html(data.message);
					$("#del_process_message").css({
				      "color": data.color,
				    });
				}
	    	}
		});
		setAjaxErrorInfo(request, "#del_process_message");
	}
	
}

function getEditRequisitionForm() {
	$("#full-view-navigation-bar").hide();
	
	document.getElementById('search-results-detail').style.display = 'none';
	$('#applicant-detail-view').show();
	$('#requisitions-minified-search-results').show();
	$('#applicant-detail-info').show();
	
	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;

	var edit_req_url = "requisitions/getEditRequisitionForm.php?RequestID="+req_id+"&action=edit&Active="+active;

	var url_req_id = urlParam('RequestID');
	var url_errors_list = urlParam('validate_errors_list');
	
	if(url_errors_list == 'csz' && req_id == url_req_id) {
		edit_req_url += "&ValidateRequestID="+url_req_id+"&validate_errors_list=csz";
	}
	
	var request = $.ajax({
		method: "POST",
  		url: edit_req_url,
		type: "POST",
		beforeSend: function(){
			$("#edit-requisition").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#edit-requisition").html(data);
			tinymce_editor();
			tinymce_editor_advertise();
			var multi_org_id = '';
			if(typeof(document.forms['frmCreateEditRequisition'].MultiOrgID) != 'undefined') {
				multi_org_id = document.forms['frmCreateEditRequisition'].MultiOrgID.value;
			}
			
			var jobboards_display_status = $("#JobBoardsStatus").val();
			
			loadLocation(multi_org_id, req_id, access_code, irecruit_home);
    	}
	});
	setAjaxErrorInfo(request, "#edit-requisition");
}

function getPurchasePremiumFeedInfo() {

	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var org_id = document.frmRequisitionDetailInfo.OrgID.value;
	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;

	var request_url = irecruit_home + "shopping/linkPurchase.php?OrgID="+org_id+"&RequestID="+req_id;
	if(access_code != "") request_url += "&k="+access_code;

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		beforeSend: function(){
			$("#purchase-premium-feed").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#purchase-premium-feed").html(data);
    	}
	});
	setAjaxErrorInfo(request, "#purchase-premium-feed");
}

function postJobToMonster() {

	var active = document.frmRequisitionDetailInfo.Active.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var request_monster_url = "requisitions/validateMonsterJobPost.php?RequestID="+req_id+"&Active="+active+"&MultiOrgID="+multi_org_id;

	var request = $.ajax({
		method: "POST",
  		url: request_monster_url,
		type: "POST",
		beforeSend: function(){
			$("#post-job-to-monster").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(mon_data) {
			if(mon_data == "Failed") {
				$("#post-job-to-monster").html("<h4>Sorry you don't have permission to post this job<h4>");
			}
			else {
				$("#post-job-to-monster").html(mon_data);
			}
    	}
	});
	setAjaxErrorInfo(request, "#post-job-to-monster");
}

function getRequisitionApplicants() {

	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id		= document.frmRequisitionDetailInfo.RequestID.value;
	var request_listing_url = "requisitions/getRequisitionApplicants.php?RequestID="+req_id;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		dataType: "json",
		beforeSend: function() {
			$("#applicants").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {

			var view_applicants  =	'<table class="table table-bordered" id="applicants-list-by-req">';
			view_applicants 	+=	'<thead>';
			view_applicants 	+=	'<tr>';
			view_applicants 	+=	'<th>Applicant Name</th>';
			view_applicants 	+=	'<th>Application ID</th>';
			view_applicants 	+=	'<th>Email</th>';
			view_applicants 	+=	'<th>Entry Date</th>';
			view_applicants 	+=	'<th>Status</th>';
			view_applicants 	+=	'</tr>';
			view_applicants 	+=	'</thead>';
			
			view_applicants 	+=	'<tbody>';
			if(data.length == 0) {
				view_applicants 	+=	'<tr>';
				view_applicants 	+=	'<td colspan="5">No applications found.</td>';
				view_applicants 	+=	'</tr>';
			}
			else {
				
				for(var i = 0; i < data.length; i++) {
					view_applicants 	+=	'<tr>';
					view_applicants 	+=	'<td><a href="mailto:'+data[i].Email+'">'+data[i].ApplicantSortName+'</a></td>';
					view_applicants 	+=	'<td><a href="'+irecruit_home+'applicantsSearch.php?ApplicationID='+data[i].ApplicationID+'&RequestID='+req_id+'" target="_blank">'+data[i].ApplicationID+'</a></td>';
					view_applicants 	+=	'<td>'+data[i].Email+'</td>';
					view_applicants 	+=	'<td data-sort="'+data[i].ToTime+'">'+data[i].EntryDate+'</td>';
					view_applicants 	+=	'<td>'+data[i].ProcessOrder+'</td>';
					view_applicants 	+=	'</tr>';
				}
			}
			view_applicants 	+=	'</tbody>';
			
			view_applicants 	+=	'</table>';
			
			$("#applicants").html('<br>'+view_applicants);
			
			if(data.length > 0) {
				$('#applicants-list-by-req').DataTable({
			        responsive: true,
			        aLengthMenu: [
			          [50, 100, -1],
			          [50, 100, "All"]
			        ]
			    });
			}
    	}
	});
	setAjaxErrorInfo(request, "#view-requisition");
}

function viewListing() {

	var access_code = document.frmRequisitionDetailInfo.AccessCode.value;
	var req_id = document.frmRequisitionDetailInfo.RequestID.value;
	var request_listing_url = "requisitions/getListingPreview.php?RequestID="+req_id;
	if(access_code != "") {
		request_listing_url += "&k="+access_code;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: request_listing_url,
		type: "POST",
		dataType: "json",
		beforeSend: function(){
			$("#view-requisition").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			if(data.requisition_info.RequestID == ''
				|| typeof(data.requisition_info.RequestID == 'undefined')) {
				$("#view-requisition").html('Sorry unable to fetch requisitions based on your filters. Please update your filters');
			} 
			$("#view-requisition").html('<br>'+data.view_listing);
    	}
	});
	setAjaxErrorInfo(request, "#view-requisition");
}

function processRequisitions(process_action, btn_obj) {

	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();
	
	var chkLength = $('input.check_requisitions:checked').length; 
	
	if(chkLength > 0) {
		if(process_action == 'Inactive') {
			
			var request = $.ajax({
				method: "POST",
		  		url: "requisitions/deActivateRequisitions.php",
				type: "POST",
				data: input_data,
				beforeSend: function() {
					$("#process_requisitions").html('<br><br>loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
					$("#process_requisitions").css({
				      "color": nav_color,
				      "text-align": "center",
				    });
				},
				success: function(data) {
					
					getRequisitionsListByFilters('yes');

					$("#process_requisitions").css({
				      "color": nav_color,
				      "text-align": "center",
				    });
					$("#process_requisitions").html(data);
		    	}
			});
			
		}
		else if(process_action == 'ApplyHotJob') {
			
			$(form_obj).attr('action', 'applyHotJob.php');
			$(form_obj).submit();
		}
		else if(process_action == 'ClearHotJob') {
			
			$(form_obj).attr('action', 'clearHotJob.php');
			$(form_obj).submit();
		}
	}
	else
	{
		alert("Please select the requisition to process");
	}	
}

function setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart) { 

	var json_data			=	JSON.parse(data);
	requisitions_list		=	json_data.requisitions_search_list;
	var req_search_res_len	=	json_data.results.length
	var req_min_results		=	"";
	var req_full_results	=	"";
	var req_data			=	json_data.results;
	var app_prev			=	json_data.previous;
	var app_next			=	json_data.next;
	var total_pages			=	json_data.total_pages;
	var current_page		=	json_data.current_page;
	
	document.frmRequisitionDetailInfo.RequisitionsCount.value = json_data.requisitions_count;
	document.frmRequisitionDetailInfo.CurrentPage.value = current_page;
	
	req_full_results = "";
	req_min_results  = "";
	$("#requisitions_full_view_results").find("tr:gt(0)").remove();

	for (var i = 0; i < req_search_res_len; i++) { 
		var req_id = req_data[i].RequestID;
		var mul_id = req_data[i].MultiOrgID;
		var req_title = req_data[i].Title;
		
		var display_copy_option = "true";
		
		if(req_data[i].Active == "R" || req_data[i].Approved == "N")
		{
			display_copy_option = "false";
		}
  //console.log(json_data);
		req_min_results += "<div class='col-lg-12'><a href='javascript:void(0);'  data-toggle='tooltip' data-placement='right' data-html='true' title='RequisitionID: "+req_title+" <br> JobID:"+req_title+" <br>  "+json_data['tooltip'][req_id]+"  '  onclick=\"getRequisitionTabInfo('"+req_id+"', '"+mul_id+"')\">"+req_title+"</a></div>";
		
		req_full_results += "<tr>";
		
		req_full_results += "<td valign='top'>"+req_data[i].PostDate+"</td>";

		req_full_results += "<td valign='top'>"+req_data[i].ExpireDate+"</td>";

		req_full_results += "<td valign='top'>";
		req_full_results += req_data[i].OrganizationName+"<br>";
		req_full_results += req_data[i].RequisitionID+"/"+req_data[i].JobID;
		
		req_full_results += "<br>";
		req_full_results += req_data[i].Title;
		req_full_results += "</td>";
		
		var days_open_full_view = '';
		if(req_data[i].Open == null) days_open_full_view = '';
		else if(req_data[i].Open < 0) days_open_full_view = '0';
		else days_open_full_view = req_data[i].Open;
		req_full_results += "<td align='center' valign='middle'>"+days_open_full_view+"</td>";
		req_full_results += "<td align='center' valign='middle'>"+req_data[i].ExpireDate+"</td>";
		req_full_results += "<td align='center' valign='middle'>";
			
		if (json_data.permit_Reports == 1) {

			req_full_results += req_data[i].ApplicantsCount+'&nbsp;';
			req_full_results += '<a href="'+req_data[i].ReportLink+'">';
			req_full_results += '<img src="'+irecruit_home+'images/icons/page_white_text.png" border="0" title="Applicants Report" style="margin:0px 0px 0px 0px;">';
			req_full_results += '</a>';
		} else { // end permit

			req_full_results += req_data[i].ApplicantsCount;
		}
			
		req_full_results += '</td>';
		
		if(req_data[i].Highlight == 'Hot') {
			req_full_results += "<td align='center' valign='middle'>"+hot_job_image+"</td>";
		}
		else if(req_data[i].Highlight == 'New') {
			req_full_results += "<td align='center' valign='middle'>"+new_job_image+"</td>";
		}
		else {
			req_full_results += "<td align='center' valign='middle'>&nbsp;</td>";
		}
		
		if (json_data.permit_Requisitions_Edit == 1) {

			req_full_results += '<td align="center">';
			
			if(display_copy_option == "true") {
				req_full_results += '<a href="'+req_data[i].Linkc+'">';
				req_full_results += '<img src="'+irecruit_home+'images/icons/application_cascade.png" border="0" title="Copy" style="margin:0px 3px -4px 0px;"></a>';
			}
			else {
				req_full_results += '-';
			}
			req_full_results += '</td>';

			req_full_results += '<td align="center"><a href="javascript:void(0);" onclick=\'getEditRequisition("'+req_id+'", "'+mul_id+'")\'><img src="'+irecruit_home+'images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;"></a></td>';
				
			if (json_data.permit_Admin == 1) {
				req_full_results += '<td align="center"><a href=\'javascript:void(0);\' onclick=\'delRequisition("'+req_data[i].ReportLink+'", "'+req_data[i].Title+'", this)\'><img src="'+irecruit_home+'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px;"></a></td>';
			} // end permit

		} else { // else action != Requisition_Edit
			req_full_results +=  '<td align="center"><a href="'+req_data[i].ViewListing+'" target="_blank"><img src="'+irecruit_home+'images/icons/page_white_magnify.png" border="0" title="View Requisition" style="margin:0px 3px -4px 0px;"></a></td>';
		} // end else Requisition_Edit
		
		req_full_results += "<td><input type='checkbox' name='chk_req["+req_data[i].RequestID+"]' id='chk_req_"+req_data[i].RequestID+"' class='check_requisitions'></td>";
		
		req_full_results += "</tr>";		
	}
	
	
	if(parseInt(json_data.requisitions_count) > parseInt(RequisitionsLimit)) {
		$("#requisitions_full_view_results").find("tr:gt(0)").remove();
		
		req_min_results += '<div class="col-lg-12">';
	  	
		req_min_results += '<div class="row">';
		req_min_results += '<div class="col-lg-6"><strong>Per page:</strong> '+RequisitionsLimit+'</div>';
		req_min_results += '<div class="col-lg-6" style="text-align:right">';
		req_min_results += '<input type="text" name="current_page_number" id="current_page_number" value="'+current_page+'" style="width:50px;text-align:center" maxlength="4">';
		req_min_results += ' <input type="button" name="btnPageNumberInfo" id="btnPageNumberInfo" value="Go" class="btn-small" onclick="getRequisitionsByPageNumber(document.getElementById(\'current_page_number\').value, \'\', \'\', \'no\')">';
		req_min_results += '</div>';
		req_min_results += '</div>';
		
		
		req_min_results += '<div class="row">';
		req_min_results += '<div id="span_left_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left">';
		req_min_results += app_prev;
		req_min_results += '</div>';
		req_min_results += '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
		req_min_results += current_page + " - " + total_pages;
	  	req_min_results += '</div>';
	  	req_min_results += '<div id="span_left_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right">';
	  	req_min_results += app_next;
	  	req_min_results += '</div>';
	  	req_min_results += '</div>';		
		
		req_min_results += '</div>';
	  	
	  	
		req_full_results += '<tr>';
		req_full_results += '<td colspan="100%">';
		req_full_results += '<div style="padding:0px;margin-top:0px;overflow:hidden;">';
		req_full_results += '<div style="text-align:center;width:100%;float: left;border:0px solid #f5f5f5;padding-top:5px;padding-bottom:5px;height:40px">';
		req_full_results += '<div style="float:left">&nbsp;<strong>Per page:</strong> '+RequisitionsLimit+'</div>';
		req_full_results += '<div style="float:right">';
		req_full_results += '<input type="text" name="current_page_fullview_number" id="current_page_fullview_number" value="'+current_page+'" style="width:50px;text-align:center" maxlength="4">';
		req_full_results += ' <input type="button" name="btnFullViewPageNumberInfo" id="btnFullViewPageNumberInfo" value="Go" class="btn-small" onclick="getRequisitionsByPageNumber(document.getElementById(\'current_page_fullview_number\').value, \'\', \'\', \'no\')">';
		req_full_results += '</div>';
		req_full_results += '</div>';
		req_full_results += '<div id="span_fullview_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left;padding-left:0px;">'+app_prev+'</div>';
		req_full_results += '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
		req_full_results += current_page+" - "+total_pages;
		req_full_results += '</div>';
		req_full_results += '<div id="span_fullview_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right;padding-right:0px;">'+app_next+'</div>';
		req_full_results += '</td>';
		req_full_results += '</tr>';
		
	}
	else {
		req_full_results += '<tr><td colspan="100%"><input type="hidden" name="current_page_fullview_number" id="current_page_fullview_number" value="1"></td></tr>';
	}

	if(display_copy_option == "true") {
		req_full_results += '<tr>';
		req_full_results += '<td colspan="100%">';
		req_full_results += '<select name="ddlProcessRequisitions" id="ddlProcessRequisitions" class="form-control width-auto-inline">';
		req_full_results += '<option value="">Select Action</option>';
		req_full_results += '<option value="Inactive">Inactive</option>';
		if(highlight_hot_jobs == 'Yes') {
			req_full_results += '<option value="ApplyHotJob">Apply Hot Job</option>';
			req_full_results += '<option value="ClearHotJob">Clear Hot Job</option>';
		}

		req_full_results += '</select>';
		req_full_results += '&nbsp;';
		req_full_results += '<input type="button" name="btnProcessRequisitions" id="btnProcessRequisitions" value="Process" class="btn btn-primary" class="btn btn-primary" onclick="processRequisitions(document.getElementById(\'ddlProcessRequisitions\').value, this);">';
		req_full_results += '</td>';
		req_full_results += '</tr>';
	}
	
	$("#requisitions-minified-results").html(req_min_results);
	$("#requisitions_full_view_results").append(req_full_results);
	
	if(typeof(requisitions_list) == 'string') {
		var req_search_res = JSON.parse(requisitions_list);
		req_search_res_len = req_search_res.length;
	}
	else if(typeof(requisitions_list) == 'object') {
		var req_search_res = requisitions_list;
		req_search_res_len = requisitions_list.length;
	}
	
	if(req_search_res_len > 0) {
		var req_id = req_search_res[0].RequestID;
		var multi_org_id = req_search_res[0].MultiOrgID;
		var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;	
		
		document.frmRequisitionDetailInfo.RequestID.value = req_id;
		document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;

		getRequisitionDetailInfo(req_id, multi_org_id);
		getSelectedTabInfo(selected_tab, req_id);
	}
	else {
		$("#requisitions-minified-results").html("<div class='col-lg-12'><strong>No results found. Please update your search filters and try again.</strong></div>");
		$("#process_message_req_details").html("");
	}
}

function getRequisitionsListByFilters(reset_index) {

	var active_status		=	document.frmFilters.ddlFilterActiveStatus.value;
	document.frmRequisitionDetailInfo.Active.value = active_status;

	var RequisitionsCount	=	document.frmRequisitionDetailInfo.RequisitionsCount.value;
	var RequisitionsLimit	=	document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var GuIDRequisition		=	document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var SearchKeyword		=	document.frmSortOptions.txtKeyword.value;
	var KeywordType			=	document.frmSortOptions.rdKeywordType.value;	
	var FilterOrganization	=	document.frmFilters.ddlOrganizationsList.value;
	var PresentOn			=	document.frmFilters.PresentOn.value;
	
	var IndexStart;

	if(reset_index == 'yes') 
	{
		IndexStart = 0;
		document.frmRequisitionDetailInfo.IndexStart.value = 0;
	}
	else {
		IndexStart = document.frmRequisitionDetailInfo.IndexStart.value;
	}
	
	var req_page_url = "requisitions/getRequisitionsListByFilters.php?Active="+active_status+"&IndexStart="+IndexStart+"&RequisitionsCount="+RequisitionsCount+"&GuIDRequisition="+GuIDRequisition+"&PresentOn="+PresentOn;
	if(SearchKeyword != "") {
		if(typeof(KeywordType) == 'undefined') KeywordType = 'W';
		req_page_url += "&keyword="+SearchKeyword+"&keyword_match="+KeywordType;
	}
	req_page_url += '&FilterOrganization='+FilterOrganization;
	req_page_url += '&PresentOn='+PresentOn;
	
	var to_sort = document.frmSortOptions.ddlReqDetailSort.value;
	var sort_type = document.frmSortOptions.ddlReqDetailSortType.value;
	if(to_sort == '') to_sort = 'date_opened';
	if(to_sort != "" && sort_type != "") {
		req_page_url += "&to_sort="+to_sort+"&sort_type="+sort_type;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: req_page_url,
		type: "POST",
		beforeSend: function() {
			$("#req_list_process_msg").html('loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			//requisition_details = data.results;
			setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart);
			$("#req_list_process_msg").html('');
    	}
	});
	setAjaxErrorInfo(request, "#requisitions_full_view_results");
}

function getRecordsByPage(IndexStart) {

	var active_status = document.frmFilters.ddlFilterActiveStatus.value;

	document.frmRequisitionDetailInfo.IndexStart.value = IndexStart;
	document.frmRequisitionDetailInfo.Active.value = active_status;
	
	var RequisitionsCount	=	document.frmRequisitionDetailInfo.RequisitionsCount.value;
	var RequisitionsLimit	=	document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var GuIDRequisition		=	document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var SearchKeyword		=	document.frmSortOptions.txtKeyword.value;
	var KeywordType			=	document.frmSortOptions.rdKeywordType.value; 
	var PresentOn			=	document.frmFilters.PresentOn.value;
	console.log(PresentOn);
	var req_page_url		=	"requisitions/getRequisitionsListByFilters.php?Active="+active_status+"&IndexStart="+IndexStart+"&RequisitionsCount="+RequisitionsCount+"&GuIDRequisition="+GuIDRequisition+"&PresentOn="+PresentOn;
	
	if(SearchKeyword != "") {
		if(typeof(KeywordType) == 'undefined') KeywordType = 'W';
		req_page_url += "&keyword="+SearchKeyword+"&keyword_match="+KeywordType;
	}
	
	var to_sort				=	document.frmSortOptions.ddlReqDetailSort.value;
	var sort_type			=	document.frmSortOptions.ddlReqDetailSortType.value;

	if(to_sort == '') to_sort = 'date_opened';
	if(to_sort != "" && sort_type != "") {
		req_page_url += "&to_sort="+to_sort+"&sort_type="+sort_type;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: req_page_url,
		type: "POST",
		success: function(data) {
			setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart);
    	}
	});
	setAjaxErrorInfo(request, "#requisitions_full_view_results");
}

function sortRequisitionsFullView(to_sort) {
	var active_status		=	document.frmFilters.ddlFilterActiveStatus.value;

	var IndexStart			=	document.frmRequisitionDetailInfo.IndexStart.value;
	document.frmRequisitionDetailInfo.Active.value = active_status;
	
	var RequisitionsCount	=	document.frmRequisitionDetailInfo.RequisitionsCount.value;
	var RequisitionsLimit	=	document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var GuIDRequisition		=	document.frmRequisitionDetailInfo.GuIDRequisition.value;
	var SearchKeyword		=	document.frmSortOptions.txtKeyword.value;
	var KeywordType			=	document.frmSortOptions.rdKeywordType.value; 
	var PresentOn			=	document.frmFilters.PresentOn.value;
	console.log(PresentOn);
	$("#results_loading").html('&nbsp;&nbsp;&nbsp;Please wait.. loading.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	var x = document.getElementById("ddlReqDetailSortType").selectedIndex;
	var y = document.getElementById("ddlReqDetailSortType").options;
		
	if(y[x].text == 'Ascending') document.getElementById("ddlReqDetailSortType").selectedIndex = 1;
	else if(y[x].text == 'Descending') document.getElementById("ddlReqDetailSortType").selectedIndex = 0;
	
	var req_page_url = "requisitions/getRequisitionsListByFilters.php?Active="+active_status+"&IndexStart="+IndexStart+"&RequisitionsCount="+RequisitionsCount+"&GuIDRequisition="+GuIDRequisition+"&PresentOn="+PresentOn;
	
	if(SearchKeyword != "") {
		if(typeof(KeywordType) == 'undefined') KeywordType = 'W';
		req_page_url += "&keyword="+SearchKeyword+"&keyword_match="+KeywordType;
	}
	
	var sort_type = document.frmSortOptions.ddlReqDetailSortType.value;
	if(to_sort == '') to_sort = 'date_opened';
	if(to_sort != "" && sort_type != "") {
		req_page_url += "&to_sort="+to_sort+"&sort_type="+sort_type;
	}
	
	var request = $.ajax({
		method: "POST",
  		url: req_page_url,
		type: "POST",
		success: function(data) {
			$("#results_loading").html('');
			setRequisitionsList(data, active_status, RequisitionsCount, RequisitionsLimit, IndexStart);
    	}
	});
	setAjaxErrorInfo(request, "#requisitions_full_view_results");
}

function getRequisitionsByPageNumber(page_number) {
	var RequisitionsLimit = document.frmRequisitionDetailInfo.RequisitionsLimit.value;
	var RequisitionsCount = document.frmRequisitionDetailInfo.RequisitionsCount.value;
	
	var TotalPages = Math.ceil(RequisitionsCount/RequisitionsLimit);
	var IndexStart = (page_number - 1) * RequisitionsLimit;
	
	if(page_number <= TotalPages) getRecordsByPage(IndexStart);
}

function getSelectedTabInfo(selected_tab, req_id) {
	
	$(".tab-content-tabs > li").removeAttr('class');
	$('a[href="'+selected_tab+'"]').closest('li').addClass('active');
	
	if(selected_tab == '#view-requisition') {
		viewListing();
	}
	if(selected_tab == '#edit-requisition') {
		getEditRequisitionForm();
	}
	if(selected_tab == '#assign-iconnect-form') {
		getAssignInternalInfo(req_id);
	}
	if(selected_tab == '#prescreen-questions') {
		getPrescreenQuestionsForm();
	}
	if(selected_tab == '#auto-forward-list') {
		getAutoForwardListForm();
	}
	if(selected_tab == '#requisition-notes') {
		getRequisitionNotes();
	}
	if(selected_tab == '#export-requisition') {
		getExportRequisitionInformation();
	}
	if(selected_tab == '#post-job-to-monster') {
		postJobToMonster();
	}
	if(selected_tab == '#purchase-premium-feed') {
		getPurchasePremiumFeedInfo();
	}
	if(selected_tab == '#applicants') {
		getRequisitionApplicants();
	}
	if(selected_tab == '#requisition-history') {
		getRequisitionHistory();
	}
       if(selected_tab == '#requisition-attachments') {
		getRequisitionAttachments();
	}
       if(selected_tab == '#interview-admin') {
		getInterviewAdmin();
	}
       if(selected_tab == '#interview-results') {
		getInterviewResults();
	}


}

function getEditRequisition(req_id, multi_org_id) {
	document.frmRequisitionDetailInfo.current_sel_tab.value = "#edit-requisition";
	toggleVerticalResultsText();
	
	$('.ra-tab-content').hide();
	$('#edit-requisition').show();

	getRequisitionTabInfo(req_id, multi_org_id);
}

function getPrevNextApplicant(nav_type) {

	//var curr_multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;
	var curr_req_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	if(typeof(requisitions_list) == 'string') {
		var req_search_res = JSON.parse(requisitions_list);
		req_search_res_len = req_search_res.length;
	}
	else if(typeof(requisitions_list) == 'object') {
		var req_search_res = requisitions_list;
		req_search_res_len = requisitions_list.length;
	}
	
	var j = 0;
	
	if(nav_type == 'next') {
		$("#req_detail_prev").show();
	}
	
	for (var i = 0; i < req_search_res_len; i++) {
		
		if(curr_req_id == req_search_res[i].RequestID) {
			
			if(nav_type == 'previous') {
				j = i - 1;
			}
			if(nav_type == 'next') {
				j = i+1;
			}

			var req_id = req_search_res[j].RequestID;
			var multi_org_id = req_search_res[j].MultiOrgID;
			var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;	
			
			document.frmRequisitionDetailInfo.RequestID.value = req_id;
			document.frmRequisitionDetailInfo.MultiOrgID.value = multi_org_id;

			getRequisitionDetailInfo(req_id, multi_org_id);
			getSelectedTabInfo(selected_tab, req_id);

			i = req_search_res_len;
		}

	}	
}

function getRequisitionHistory() {

	var request_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "requisitions/getRequisitionHistory.php?RequestID="+request_id,
		type: "POST",
		beforeSend: function(){
			$("#requisition-history").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#requisition-history").html(data);
    	}
	});
	
}

function getRequisitionAttachments() {

	var request_id = document.frmRequisitionDetailInfo.RequestID.value;
	
	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "requisitions/getRequisitionAttachments.php?RequestID="+request_id,
		type: "POST",
		beforeSend: function(){
			$("#requisition-attachments").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#requisition-attachments").html(data);
    	}
	});
	
}

function getInterviewQuestions(interviewformid) {

	var input_data = $('form').serialize();
	input_data += '&InterviewFormID=' + interviewformid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/adminQuestions.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-admin").html(data);
    		}
	});

}

function getInterviewFormResults(requestid,interviewformid,userid,applicationid) {

	input_data = 'RequestID=' + requestid;
	input_data += '&InterviewFormID=' + interviewformid;
	input_data += '&UserID=' + userid;
	input_data += '&ApplicationID=' + applicationid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/viewInterviewFormResults.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-results").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-results").html(data);
    		}
	});

}

function editInterviewForm(requestid,interviewformid,userid,applicationid) {

	input_data = 'RequestID=' + requestid;
	input_data += '&InterviewFormID=' + interviewformid;
	input_data += '&UserID=' + userid;
	input_data += '&ApplicationID=' + applicationid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/editInterviewForm.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-results").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-results").html(data);
    		}
	});

}

function processEditInterviewForm(process_status,uid) {

        var input_data = $('form[name="InterviewForm"]').serialize();
        input_data += '&process_status=' + process_status + '&uid=' + uid;

        const required = JSON.parse(document.getElementById('Required').value);

        var err = '';

        required.forEach(function(questionid) {
          var ques = document.getElementById(Object.keys(questionid));
          var q = ques.value;

                if(Object.prototype.toString.call(ques) === '[object HTMLInputElement]') {
                        var value = $("input[type='radio'][name='"+Object.keys(questionid)+"']:checked").val();
                        if (value != '') {
                                q=value;
                        }
                        if (typeof value === 'undefined') {
                          err += ' - ' + Object.values(questionid) + '\n';
                        }
                }

          if (q == '') {
                  err += ' - ' + Object.values(questionid) + '\n';
          }
        });

        if (err != '') { err = 'The following questions are required:\n\n' + err; alert(err); return false; }

        var request = $.ajax({
                method: "POST",
                url: irecruit_home + "interview/processInterviewFormData.php",
                type: "POST",
                data: input_data,
                beforeSend: function(){
                        $("#interview-results").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
                },
                success: function(data) {
                        $("#interview-results").html(data);
                }
        });

}

function viewInterviewFormHistory(requestid,interviewformid,userid,applicationid) {

	input_data = 'RequestID=' + requestid;
	input_data += '&InterviewFormID=' + interviewformid;
	input_data += '&UserID=' + userid;
	input_data += '&ApplicationID=' + applicationid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/viewInterviewFormHistory.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-results").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-results").html(data);
    		}
	});

}

function determineInterviewTotal(requestid,interviewformid,questionid) {

	var input_data = 'RequestID=' + requestid + '&InterviewFormID=' + interviewformid + '&QuestionID=' + questionid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/adminQuestionTotal.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-admin").html(data);
    	}
	});

}

function getInterviewValues(requestid,interviewformid,questionid) {

	var input_data = 'RequestID=' + requestid + '&InterviewFormID=' + interviewformid + '&QuestionID=' + questionid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/adminQuestionValues.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-admin").html(data);
    	}
	});

}
function processInterviewTotalValue(value,checkbox,question) {

        var input_data = $('form[name="TotalQuestions"]').serialize();
	input_data += '&ChangeValue='+value;

	var requestid = document.getElementById('RequestID').value;
	var interviewformid = document.getElementById('InterviewFormID').value;
	var questionid = document.getElementById('QuestionID').value;

    		if(checkbox.checked){
		   input_data += '&change=add';
    		}
    		else{
		   input_data += '&change=clear';
    		}

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/updateInterviewTotal.php",
		type: "POST",
                data: input_data,
		success: function(data) {
			//$("#interview-admin").html(data);
			//determineInterviewTotal(requestid,interviewformid,questionid);
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">' + question + ' has been updated for totaling.</span>'); }, 500);
    		}
	});

}

function processInterviewValues(action,row,newvalue) {

        var input_data = $('form[name="QuestionValues"]').serialize();
	input_data += '&action=' + action + '&row=' + row;
	if (newvalue != "") {
	        input_data += '&newvalue=' + newvalue;
	}

	var requestid = document.getElementById('RequestID').value;
	var interviewformid = document.getElementById('InterviewFormID').value;
	var questionid = document.getElementById('QuestionID').value;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/updateInterviewValue.php",
		type: "POST",
                data: input_data,
		success: function(data) {
			//$("#interview-admin").html(data);
			if((action == 'Display') || (action == 'Value')) {
			  $(message).html('<span style="color:red;font-style:italic;">Value has been updated.</span>');
			} else {
			  getInterviewValues(requestid,interviewformid,questionid);
			  setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Value has been updated.</span>'); }, 100);
			}
    		}
	});

}

function addInterviewQuestion(requestid,interviewformid) {

	var input_data = 'RequestID=' + requestid + '&InterviewFormID=' + interviewformid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/addQuestion.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			getInterviewQuestions(interviewformid);
    		}
	});

}

function getInterviewAdmin() {

	var input_data = $('form').serialize();

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/adminForms.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-admin").html(data);
    	}
	});

}

function getInterviewFormView(requestid,interviewformid) {

	var input_data = 'RequestID=' + requestid + '&InterviewFormID=' + interviewformid;

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/viewInterviewForm.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
			$("#interview-admin").html(data);
    	}
	});

}

function processNewInterviewForm() {

	var input_data = $('form').serialize();

	input_data += '&FormName=' + document.getElementById('FormName').value;
	input_data += '&CopyRequestID=' + document.getElementById('CopyRequestID').value;
	input_data += '&Action=newinterviewform';

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/adminForms.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-admin").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-admin").html(data);
    	}
	});

}

function updateInterviewQuestion(changetype,requestid,interviewformid,questionid,value,checkbox) {

	if ((changetype == "Active") || (changetype == "Required")) {
    		if(checkbox.checked){
	    	   value=value;
    		}
    		else{
	    	    value='';
    		}
	}

	const input_data = {ChangeType:changetype, RequestID:requestid, InterviewFormID:interviewformid, QuestionID:questionid,Value:value};

	const CT = {Question:"Question", QuestionTypeID:"Question Type", Required:"Required",Active:"Active"};
	const QT = JSON.parse(document.getElementById('QuestionTypeIDs').value);

	if (changetype == 'QuestionTypeID') {
                if (value != "") {
                  value = QT[value]['Description'];
                }
        }

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/updateInterviewQuestion.php",
		type: "POST",
                data: input_data,
		success: function(data) {
		if ((changetype == "Active") || (changetype == "Required")) {
    		  if(checkbox.checked){
	    	   value='selected';
    	 	  }
    		  else{
	    	    value='not selected';
    		  }
		}

		if (changetype == "QuestionTypeID") {
                    getInterviewQuestions(interviewformid);
		}

                    	setTimeout(function() { $(message).html('<span style="color:red;font-style:italic;">' + CT[changetype] + ' changed to ' + value + '.</span>'); }, 500);

    		}
	});

}

function updateInterviewApplicants(requestid,applicationid,value,checkbox) {

    	if(checkbox.checked){
	    value=value;
    	}
    	else{
	    value='';
    	}

	const input_data = {RequestID:requestid, ApplicationID:applicationid,Value:value};

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/updateInterviewApplicants.php",
		type: "POST",
                data: input_data,
		success: function(data) {

    		if(checkbox.checked){
	    	  value='selected';
    	 	}
    		else{
	    	  value='not selected';
    		}

		//$("#interview-results").html(data);
                 //getInterviewResults();
                 setTimeout(function() { $(message).html('<span style="color:red;font-style:italic;">' + applicationid + ' changed to ' + value + '.</span>'); }, 500);

    		}
	});

}

function updateInterviewForm(changetype,requestid ,interviewformid,value) {


	const input_data = {ChangeType:changetype, RequestID:requestid, InterviewFormID:interviewformid, Value:value};
	const CT = {FormName:"Form Name", Target:"Target Group", WhenAvailable:"When Available"};

	const T = {Admin:"Interview Admin", Owner:"Requisition Owner", Manager:"Requisition Hiring Manager"};

	if (changetype == 'Target') {
		if (value != "") {
		  value = T[value];
		}
	}

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/updateInterviewForm.php",
		type: "POST",
                data: input_data,
		success: function(data) {
                    $(message).html('<span style="color:red;font-style:italic;">' + CT[changetype] + ' changed to ' + value + '.</span>');
    		}
	});

}

function getInterviewResults() {

	var requestid = document.frmRequisitionDetailInfo.RequestID.value;
	const input_data = {RequestID:requestid};

	var request = $.ajax({
		method: "POST",
  		url: irecruit_home + "interview/results.php",
		type: "POST",
                data: input_data,
		beforeSend: function(){
			$("#interview-results").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
		},
		success: function(data) {
  
			$("#interview-results").html(data);
    	}
	});

}

function updateInterviewFormQuestionSortOrder(forms_data,interviewformid) {

        var REQUEST_URL = irecruit_home + "interview/updateInterviewFormQuestionOrder.php";

        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: forms_data,
                success: function(data) {
                        getInterviewQuestions(interviewformid);
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Question order has been updated.</span>'); }, 500);
                }
        });
}

function updateInterviewFormValuesSortOrder(forms_data,requestid,interviewformid,questionid) {

        var REQUEST_URL = irecruit_home + "interview/updateInterviewQuestionValueOrder.php";

        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: forms_data,
                success: function(data) {
			//$("#interview-admin").html(data);
			getInterviewValues(requestid,interviewformid,questionid);
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Value order has been updated.</span>'); }, 500);
                }
        });
}

function updateInterviewFormSortOrder(forms_data) {

	var REQUEST_URL = irecruit_home + "interview/updateInterviewFormOrder.php";

        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: forms_data,
                success: function(data) {
                        getInterviewAdmin();
			setTimeout(function () {
                        $(message).html('<span style="color:red;font-style:italic;">Form order has been updated.</span>');
			}, 500);
                }
        });
}

function copyInterviewForm(requestid,interviewformid,form) {

	if(confirm('Are you sure you want to copy the following form?\n\n'+form)) {

        var REQUEST_URL = irecruit_home + "interview/copyInterviewForm.php";

	const input_data = {RequestID:requestid, InterviewFormID:interviewformid};


        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: input_data,
                success: function(data) {
			//$("#interview-admin").html(data);
                        getInterviewAdmin();
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Interview form has been copied.</span>');}, 500);
                }
        });

	}
}

function deleteInterviewForm(requestid,interviewformid,form) {

	if(confirm('Are you sure you want to delete the following form?\n\n'+form)) {

        var REQUEST_URL = irecruit_home + "interview/deleteInterviewForm.php";

	const input_data = {RequestID:requestid, InterviewFormID:interviewformid};

        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: input_data,
                success: function(data) {
                        getInterviewAdmin();
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Interview form has been deleted.</span>');}, 500);
                }
        });

	}
}

function deleteInterviewQuestion(requestid,interviewformid,questionid,question) {

	if(confirm('Are you sure you want to delete the following question?\n\n'+question)) {

        var REQUEST_URL = irecruit_home + "interview/deleteInterviewQuestion.php";

	const input_data = {RequestID:requestid, InterviewFormID:interviewformid, QuestionID:questionid};

        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: input_data,
                success: function(data) {
                        getInterviewQuestions(interviewformid);
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Interview question has been deleted.</span>');}, 500);
                }
        });

	}
}

function deleteInterviewFormData(requestid,interviewformid,userid,user,applicationid) {

	if(confirm('Are you sure you want to delete the following users data?\n\n'+user+' for application '+applicationid)) {

        var REQUEST_URL = irecruit_home + "interview/deleteInterviewFormData.php";

	const input_data = {RequestID:requestid, InterviewFormID:interviewformid, UserID:userid, ApplicationID:applicationid};

        $.ajax({
                method: "POST",
                url: REQUEST_URL,
                type: "POST",
                data: input_data,
                success: function(data) {
			getInterviewResults();
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Interview form data for the user has been deleted.</span>');}, 500);
                }
        });

	}
}



//Child questions array should be empty, in future we can change it.
var child_ques_info = {};
function getFormIDChildQuestionsInfo() {
	if(typeof(child_ques_info) != 'undefined') {
		return false;
	}
	for (question_id in child_ques_info) {
		  var child_que_info =  child_ques_info[question_id];
		  for (question_id_option in child_que_info) {
			  if(que_types_list[question_id] == 2
				|| que_types_list[question_id] == 22
				|| que_types_list[question_id] == 23) {
					
                    var checked_radio = $("input[name='"+question_id+"']:checked").val();
                    
                    if(checked_radio == question_id_option)
                    {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                        	if(child_ques_list[child_ques_ids] == "show") {
                        		$("#divQue-"+child_ques_ids).show();
                        	}
                        	else if(child_ques_list[child_ques_ids] == "hidden") {
                        		$("#divQue-"+child_ques_ids).hide();
                        	}							
                        }
                    }
                    else if(typeof(checked_radio) == 'undefined') {
                        child_ques_list = child_que_info[question_id_option];
                        
                        for(child_ques_ids in child_ques_list) {
                            $("#divQue-"+child_ques_ids).hide();
                        }
                   }
			 }
			 else if(que_types_list[question_id] == 3) {
				  var checked_radio = $("#"+question_id).val();
				  //Exception for country question, have to handle it in simple way
				  if(question_id == 'country' && typeof(checked_radio) != 'undefined') {
					  if(checked_radio != 'US' && checked_radio != 'CA') {
						  	child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
					  }
					  else if(checked_radio == question_id_option)
	                  {
		                    child_ques_list = child_que_info[question_id_option];
		                    for(child_ques_ids in child_ques_list) {
		                    	if(child_ques_list[child_ques_ids] == "show") {
		                    		$("#divQue-"+child_ques_ids).show();
		                    	}
		                    	else if(child_ques_list[child_ques_ids] == "hidden") {
		                    		$("#divQue-"+child_ques_ids).hide();
		                    	}							
		                    }
	                  }
				  }
				  else if(checked_radio == question_id_option)
                  {
	                    child_ques_list = child_que_info[question_id_option];
	                    for(child_ques_ids in child_ques_list) {
	                    	if(child_ques_list[child_ques_ids] == "show") {
	                    		$("#divQue-"+child_ques_ids).show();
	                    	}
	                    	else if(child_ques_list[child_ques_ids] == "hidden") {
	                    		$("#divQue-"+child_ques_ids).hide();
	                    	}							
	                    }
                  }
                  else if(typeof(checked_radio) == 'undefined') {
                      child_ques_list = child_que_info[question_id_option];
                      
                      for(child_ques_ids in child_ques_list) {
                      		$("#divQue-"+child_ques_ids).hide();
                      }
                 }
			 }
		  }
	}
}

$(document).ready(function() {

	//Have to add substr to fix it
	if(r_action != 'purchases') {

		var def_selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;
		
		$('.ra-tab-content').hide();
		$(def_selected_tab).show();
		
		$(".vertical-text").click(function() {
			
			var button_id = $(this).attr('id');
			
			if(button_id == 'vertical-text-full-view-results') {
				$("#full-view-navigation-bar").show();
				
				document.getElementById('search-results-detail').style.display = 'block';
				$('#applicant-detail-view').hide();
				$('#requisitions-minified-search-results').hide();
			}
			
			if(button_id == 'vertical-text-detail-view-results') {
				$("#full-view-navigation-bar").hide();
				
				document.getElementById('search-results-detail').style.display = 'none';
				$('#applicant-detail-view').show();
				$('#requisitions-minified-search-results').show();
				$('#applicant-detail-info').show();
			}

		});
		
		$('.requisition-tabs').click(function(event) {
				event.preventDefault();
				var RequestID = document.frmRequisitionDetailInfo.RequestID.value;
				
		        var src_href = this.href;
		        var href_tab_info = src_href.split("#");
		        var content_tab = href_tab_info[1];
				var selected_tab = "#"+content_tab;

		        $('.ra-tab-content').hide();
		    	$('#'+content_tab).show();

		    	document.frmRequisitionDetailInfo.current_sel_tab.value = selected_tab;
			    	
		    	getSelectedTabInfo(selected_tab, RequestID);
		});

		var selected_tab = document.frmRequisitionDetailInfo.current_sel_tab.value;
		var req_id = document.frmRequisitionDetailInfo.RequestID.value;
		var multi_org_id = document.frmRequisitionDetailInfo.MultiOrgID.value;

		getRequisitionDetailInfo(req_id, multi_org_id);
		getSelectedTabInfo(selected_tab, req_id);
		
		$("#full-view-navigation-bar").hide();
		
	}
	
});


