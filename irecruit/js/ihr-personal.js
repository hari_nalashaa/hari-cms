function saveAddressAndPhone() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmAddressAndPhone").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'saveAddressAndPhone.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function savePersonalInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmPersonalInfo").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'savePersonalInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveEmployeePhoto() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = new FormData($('#frmPersonalPhoto')[0]);
	var ajax_params = {"file_upload":"true"};
	var SelectedTab = document.frmEmployeeInformation.SelectedTab.value;
	
	//Have to do the validation before saving the information
	var request_url = 'uploadEmployeePhoto.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
	
	var request_url = "getEmployeeInformation.php?IHrGuID="+IHrGuID+"&EmployeeID="+EmployeeID+"&InfoType="+SelectedTab;
	getInfo(request_url, '', '');
}

function saveSeniorityInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmSeniority").serialize();
	var ajax_params = {"file_upload":"false"};

	//Have to do the validation before saving the information
	var request_url = 'saveSeniorityInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveEmploymentEligibilityInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmEmploymentEligibility").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'saveEmploymentEligibilityInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveMilitaryInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmMilitary").serialize();
	var ajax_params = {"file_upload":"false"};
	
	//Have to do the validation before saving the information
	var request_url = 'saveMilitaryInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveFirstContactInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmFirstContact").serialize();
	var ajax_params = {"file_upload":"false"};

	//Have to do the validation before saving the information
	var request_url = 'saveFirstContactInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveSecondContactInfo() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = $("#frmSecondContact").serialize();
	var ajax_params = {"file_upload":"false"};

	//Have to do the validation before saving the information
	var request_url = 'saveSecondContactInfo.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function saveEmployeeAttachments() {
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	var IHrGuID = document.frmEmployeeInformation.IHrGuID.value;
	var input_data = new FormData($('#frmAttachments')[0]);
	var ajax_params = {"file_upload":"true"};

	//Have to do the validation before saving the information
	var request_url = 'uploadEmployeeAttachments.php?EmployeeID='+EmployeeID+'&IHrGuID='+IHrGuID;
	saveiHRInfoByAjaxCall(request_url, input_data, ajax_params);
}

function setAddressAndPhoneInfo(json_data) {

	var data = getJsonObject(json_data);
	console.log(json_data);
	
	for (var key in data) {
		if(key == 'rdUseInterPhone') {
	    	$(".rdUseInterPhone").removeAttr('checked');
	    	
	    	if(data[key] != "" 
	    		&& typeof(data[key]) != "undefined" 
	    		&& data[key] != null) {
	    		document.getElementById("rdUseInterPhone"+data[key]).checked = true;	
	    	} else {
	    		$('input[name=rdUseInterPhone]').attr('checked',false);
	    	}
		}
		else {
			$("#frmAddressAndPhone #"+key).val(data[key]);
		}
	}
	
	$("#form_status_msg").html('');
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
}

function setPersonalInfo(json_data) {

	var pers_info_txt_elem = ["txtFirst", "txtMiddle", "txtLast", "txtNickname", "txtSalutation", "txtSSN", "txtLanguage", "txtBirthDate"];
	var pers_info_ddl_elem = ["ddlMaritalStatus", "ddlDisability", "ddlEthinicOrigin"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in pers_info_txt_elem) {
		object_key = pers_info_txt_elem[txtkey];
		object_value = data[object_key];
		if(object_key == "txtBirthDate") object_value = getMdyFromYmd(object_value, "-", "/");
		$("#frmPersonalInfo #"+object_key).val(object_value);
	}
	for (var ddlkey in pers_info_ddl_elem) {
		object_key = pers_info_ddl_elem[ddlkey];
		object_value = data[object_key];
		
		if(object_value == null) {
			$("#frmPersonalInfo #"+object_key+">option[value='']").prop('selected', true);
		}
		else {
			$("#frmPersonalInfo #"+object_key+">option[value='" + object_value + "']").prop('selected', true);
		}
	}
	
	$(".rdGender").removeAttr('checked');
	var gender_value = data['rdGender'];
	var gender_id = "rd"+gender_value;

	if(gender_value != null) {
		document.getElementById(gender_id).checked = true;
	}	
	else {
		$('input[name=rdGender]').attr('checked',false);
	}
	
	$("#form_status_msg").html('');
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
}

function setPhotoInfo(json_data) {

	var data = getJsonObject(json_data);
	var employee_photo = data.EmployeePhoto;
	var OrgID = document.frmEmployeeInformation.OrgID.value;
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	
	var current_date = new Date();
	var current_time = current_date.getTime();
	
	if(employee_photo != "" && typeof(employee_photo) !== "undefined") {
		$("#img_employee_photo").html('<img src="'+irecruit_home+'vault/ihr/'+OrgID+'/'+EmployeeID+'/'+employee_photo+'?'+current_time+'" alt="Employee Photo">');
	}
	else {
		$("#img_employee_photo").html('');
	}
	
	$("#form_status_msg").html('');
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
}

function setSeniorityInfo(json_data) {

	var seniority_info_txt_elem = ["txtRecruiter", "taSourceOfHire", "txtSeniorityYearsMonths", "txtCitizen", "txtTimeOffYearsMonths"];
	var seniority_info_date_elem = ["txtOriginalHireDate", "txtLastHireDate", "txtAdjustedSeniorityDate", "txtTimeOffServiceDate"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in seniority_info_txt_elem) {
		object_key = seniority_info_txt_elem[txtkey];
		object_value = data[object_key];
		$("#frmSeniority #"+object_key).val(object_value);
	}
	for (var txtkey in seniority_info_date_elem) {
		object_key = seniority_info_date_elem[txtkey];
		object_value = data[object_key];
		object_value = getMdyFromYmd(object_value, "-", "/");
		$("#frmSeniority #"+object_key).val(object_value);
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setEmploymentEligibilityInfo(json_data) {

	var emp_eligibility_info_txt_elem = ["txtVisaType", "txtDocumentA", "txtDocumentB", "txtDocumentC"];
	var emp_eligibility_info_date_elem = ["txtVisaExpirationDate", "txtI9ReverificationDate", "txtDocumentAExpirationDate", "txtDocumentBExpirationDate", "txtDocumentCExpirationDate"];
	var emp_eligibility_info_ddl_elem = ["ddlI9VerificationStatus", "ddlWorkAuthorization"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in emp_eligibility_info_txt_elem) {
		object_key = emp_eligibility_info_txt_elem[txtkey];
		object_value = data[object_key];
		$("#frmEmploymentEligibility #"+object_key).val(object_value);
	}
	for (var txtkey in emp_eligibility_info_date_elem) {
		object_key = emp_eligibility_info_date_elem[txtkey];
		object_value = data[object_key];
		object_value = getMdyFromYmd(object_value, "-", "/");
		$("#frmEmploymentEligibility #"+object_key).val(object_value);
	}
	for (var ddlkey in emp_eligibility_info_ddl_elem) {
		object_key = emp_eligibility_info_ddl_elem[ddlkey];
		object_value = data[object_key];
		
		if(object_value == null) {
			$("#frmEmploymentEligibility #"+object_key+">option[value='']").prop('selected', true);
		}
		else {
			$("#frmEmploymentEligibility #"+object_key+">option[value='" + object_value + "']").prop('selected', true);
		}
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setMilitaryInfo(json_data) {
	var military_info_txt_elem = ["txtMilitaryExperience", "txtActiveDutyDischarge"];
	var military_info_chk_elem = ["chkDisabledVeteran", "chkArmedForcesServiceMedalVeteran", "chkADWorCBV"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in military_info_txt_elem) {
		object_key = military_info_txt_elem[txtkey];
		object_value = data[object_key];
		$("#frmMilitary #"+object_key).val(object_value);
	}

	$("#frmMilitary .chkMilitary").each(function() {
		var check_box_id = this.id;
		
		if(data[check_box_id] == null || data[check_box_id] == "") {
			document.getElementById(check_box_id).checked = false;
		}
		else {
			document.getElementById(check_box_id).checked = true;
		}
		
	});
	
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setEmployeeAttachmentsInfo(json_data) {

	var data = getJsonObject(json_data);
	var EmployeeID = document.frmEmployeeInformation.EmployeeID.value;
	
	var i = 0;
	var attachments_list = '<div class="col-lg-6"><strong>Attachment Name</strong></div>';
	attachments_list += '<div class="col-lg-6"><strong>Download</strong></div>';
	for (var property in data) {
		i++;
		var emp_att_name = data[property].Name;
		var emp_att_file_name = data[property].FileName;

		attachments_list += '<div class="col-lg-6">'+emp_att_name+'</div>';
		attachments_list += '<div class="col-lg-6"><a href="employeeAttachments.php?f='+emp_att_file_name+'&e='+EmployeeID+'">Download</a></div>';
	}
	
	if(i == 0) attachments_list = '<div class="col-lg-12" style="text-align:center">There are no attachments</div>';
	
	$("#emp_attachments_info").html(attachments_list);
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setFirstContactInfo(json_data) {
	
	var first_contact_info_txt_elem = ["txtName", "txtRelation", "txtAddressLine1", "txtAddressLine2", "txtAddressLine3", "txtHome", "txtBusiness", "txtCellular"];
	var first_contact_info_radio_elem = ["rdInternationalPhoneNumbers"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in first_contact_info_txt_elem) {
		object_key = first_contact_info_txt_elem[txtkey];
		object_value = data[object_key];
		$("#frmFirstContact #"+object_key).val(object_value);
	}
	for (var txtkey in first_contact_info_radio_elem) {
		object_key = first_contact_info_radio_elem[txtkey];
		object_value = data[object_key];

		if(data[object_key] != "" 
    		&& typeof(data[object_key]) != "undefined" 
    		&& data[object_key] != null) {
			document.getElementById("rdInterPhoneNumbersFirstContact"+data[object_key]).checked = true;	
		}
		else {
			$('input[name=rdInternationalPhoneNumbers]').attr('checked',false);
		}
	}
	
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	
}

function setSecondContactInfo(json_data) {

	var second_contact_info_txt_elem = ["txtName", "txtRelation", "txtAddressLine1", "txtAddressLine2", "txtAddressLine3", "txtHome", "txtBusiness", "txtCellular"];
	var second_contact_info_radio_elem = ["rdInternationalPhoneNumbers"];
	
	var data = getJsonObject(json_data);
	var object_key, object_value;
	for (var txtkey in second_contact_info_txt_elem) {
		object_key = second_contact_info_txt_elem[txtkey];
		object_value = data[object_key];
		$("#frmSecondContact #"+object_key).val(object_value);
	}

	var object_rd_value = data.rdInternationalPhoneNumbers;
	if(object_rd_value != "" 
		&& typeof(object_rd_value) != "undefined" 
		&& object_rd_value != null) {
		document.getElementById("rdInterPhoneNumbersSecondContact"+object_rd_value).checked = true;
	}
	else {
		$('input[name=rdInternationalPhoneNumbers]').attr('checked',false);
	}
	$("#form_status_msg").attr('class', 'col-lg-12 col-md-12 col-sm-12');
	$("#form_status_msg").html('');	

}