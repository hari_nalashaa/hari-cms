/**
 * To toggle the sidebar and highlight the left side menu color 
 */
$(document).ready(function() {

    $('.sidebar-toggle-box-menu').click(function() {
    	
		$(".sidebar").toggle();
    	
		if( $(".sidebar").css("display") == 'none' ) {
			$("#page-wrapper").removeAttr('style');
			$('#page-wrapper').css({"min-height": "324px", "margin-left": "0px"});
		}
		if( $(".sidebar").css("display") == 'block' ) {
			$("#page-wrapper").removeAttr('style');
			$('#page-wrapper').css({"min-height": "324px"});
		}
		
	});

    $('#side-menu li').click(function() {
		$("#side-menu li a span").attr('class', 'fa fa-caret-right');

		$('#side-menu li').each(function(i, li) {
			if($(this).attr('class') == 'active')
				 $(this).find("a span").attr('class', 'fa fa-caret-down');
			
		});
			
	});

    if(document.URL.indexOf("applicantsSearch.php") >= 0 
    		|| document.URL.indexOf("requisitionsSearch.php") >= 0
    		|| document.URL.indexOf("personalDemographics.php") >= 0
    		|| document.URL.indexOf("personalHRStatus.php") >= 0
    		|| document.URL.indexOf("personalEmployeeAttachments.php") >= 0
    		|| document.URL.indexOf("personalEmergencyContacts.php") >= 0
    		|| document.URL.indexOf("jobandpayCurrentPay.php") >= 0
    		|| document.URL.indexOf("jobandpayCurrentJob.php") >= 0
    		|| document.URL.indexOf("careerSkills.php") >= 0
    		|| document.URL.indexOf("careerEducation.php") >= 0
    		|| document.URL.indexOf("careerPreviousEmployer.php") >= 0
    		|| document.URL.indexOf("jobBoardPosting.php") >= 0) {
    		//Remove it after completed that applicant search
    		$('.sidebar-toggle-box-menu').trigger("click");
    		
    		$('.navbar-toggle').click(function() {
    	    	$(".sidebar").toggle();
    	    	
    	    	if( $(".sidebar").css("display") == 'none' ) {
    				$("#page-wrapper").removeAttr('style');
    				$('#page-wrapper').css({"min-height": "324px", "margin-left": "0px"});
    			}
    			if( $(".sidebar").css("display") == 'block' ) {
    				$("#page-wrapper").removeAttr('style');
    				$('#page-wrapper').css({"min-height": "324px"});
    			}
    		});
	}
    

    

});

var request_que_info = {
	request_que_data: []
};

$("#tblRequestQuestions tbody").sortable({
	cursor: 'move',
    stop: function( event, ui ) {
        $(this).find('tr').each(function(i) {
        	
        	var SortOrder = i;
            SortOrder++;
            var tr_id = $(this).attr('id');
			var tr_info = tr_id.split("-");
           
            var QuestionID = tr_info[2];

            request_que_info.request_que_data.push({
        		 "QuestionID" : QuestionID
        	});
        });

        $.ajax({
			method: "POST",
	  		url: 'administration/updateRequestQuestionsOrder.php',
			type: "POST",
			data: request_que_info,
			beforeSend: function() {
	            // setting a timeout
				$("#req_update_status").html("Please wait ..");
	        },
			success: function(data) {
				$("#req_update_status").html(data);
	    	}
		});
    }
});

//iConnect Forms 
function show_confirm(form, url)  {  

	var ck=confirm("Are you sure you want to override form: \\n -" + form);  
		if (ck==true) {  
		window.open(url,'_blank','location=yes,toolbar=no,height=400,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes');
		} 
}

var page_url = window.location.href;

function sendEmailNotification(callback_url, send_mail_notfication_id) {
	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = ""
	
	if(confirm("Are you sure you want to send a reminder?")) {
		var request = $.ajax({
			method: "POST",
	  		url: request_url+callback_url+'&display_app_header=no',
			type: "POST",
			success: function(data) {
				$("#"+send_mail_notfication_id).html(data);
	    	}
		});
	}
}

function getChangeStatus(callback_url) {
	$(".iconnect_forms_data").hide();
	$("#iconnect_change_status").show();
	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#iconnect_change_status").html(data);
    	}
	});
}

function getAgreementForm(callback_url) {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_agree_form").show();

	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") > 1) request_url = "";

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#iconnect_complete_agree_form").html(data);
    	}
	});
}

function processAgreementForm() {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_agree_form").show();

	var callback_url = $("#frmCompleteAgreementForm").attr('action');
	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var form = $("#frmCompleteAgreementForm");
	var input_data = form.serialize();
	
	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		data: input_data,
		success: function(data) {
			$("#iconnect_complete_agree_form").html(data);
    	}
	});
	
}

function finalizeAllCompletedForms(finalize_btn_obj) {
	var form_id = $(finalize_btn_obj).parents('form').attr('id');

	var form = $("#"+form_id);
	var input_data = form.serialize();

	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var assign_internal_flag = "single"; 
	if(typeof(document.frmFormStatus) != "undefined") {
		assign_internal_flag = "multiple"
		var AccessCode = document.frmFormStatus.AccessCode.value;
		var ApplicationID = document.frmFormStatus.ApplicationID.value;
		var RequestID = document.frmFormStatus.RequestID.value;
		var div_to_load = document.frmFormStatus.DivToLoad.value;
	}

	
	if(confirm('Are you sure you want to Finalize All Completed Forms?')) {
		var request = $.ajax({
			method: "POST",
	  		url: request_url + 'getAssignInternal.php?display_app_header=no',
			type: "POST",
			data: input_data,
			success: function(data) {
				if(assign_internal_flag == "single") {
					getAssignInternalInfo();	
				}
				if(assign_internal_flag == "multiple") {
					getApplicantAssignInternalInfo(div_to_load, ApplicationID, RequestID, AccessCode);
				}
	    	}
		});
	}
}


function processPreFilledForm() {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_pref_form").show();

	var callback_url = $("#frmCompletePreFilledForm").attr('action');
	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var form = $("#frmCompletePreFilledForm");
	var input_data = form.serialize();

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		data: input_data,
		success: function(data) {
			$("#iconnect_complete_pref_form").html(data);
    	}
	});
	
	request.fail(function(jqXHR, textStatus, errorThrown) {
		if(textStatus === 'timeout')
	    {     
	        console.log('Sorry, unable to complete your request. Please try again.');
	    }
		else {
			console.log(textStatus+": "+errorThrown);
		}
	});
	request.error(function( jqXHR, textStatus, errorThrown ) {
		if(textStatus === 'timeout')
	    {     
			console.log('Sorry, unable to complete your request. Please try again.');
	    }
		else {
			console.log(textStatus+": "+errorThrown);
		}
	});

}

function processWebForm() {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_web_form").show();

	var callback_url = $("#frmCompleteWebForm").attr('action');
	var request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var form = $("#frmCompleteWebForm");
	var input_data = form.serialize();
	
	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		data: input_data,
		success: function(data) {
			$("#iconnect_complete_web_form").html(data);
    	}
	});
	
	request.fail(function(jqXHR, textStatus, errorThrown) {
		if(textStatus === 'timeout')
	    {     
	        console.log('Sorry, unable to complete your request. Please try again.');
	    }
		else {
			console.log(textStatus+": "+errorThrown);
		}
	});
	request.error(function( jqXHR, textStatus, errorThrown ) {
		if(textStatus === 'timeout')
	    {     
			console.log('Sorry, unable to complete your request. Please try again.');
	    }
		else {
			console.log(textStatus+": "+errorThrown);
		}
	});
}


function getPrefilledForm(callback_url) {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_pref_form").show();
	$("#iconnect_complete_pref_form").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#iconnect_complete_pref_form").html(data);
    	}
	});
}

function getWebForm(callback_url) {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_web_form").show();
	$("#iconnect_complete_web_form").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#iconnect_complete_web_form").html(data);
    	}
	});
}

function getSpecificationForm(callback_url) {
	$(".iconnect_forms_data").hide();
	$("#iconnect_complete_spec_form").show();
	$("#iconnect_complete_spec_form").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#iconnect_complete_spec_form").html(data);
    	}
	});
}

function getViewHistory(callback_url) {
	$(".iconnect_forms_data").hide();
	$("#iconnect_view_history").show();
	
	request_url = "formsInternal/";
	if(page_url.indexOf("formsInternal/") >= 0) request_url = "";

	var request = $.ajax({
		method: "POST",
  		url: request_url+callback_url+'&display_app_header=no',
		type: "POST",
		success: function(data) {
			$("#iconnect_view_history").html(data);
    	}
	});
}