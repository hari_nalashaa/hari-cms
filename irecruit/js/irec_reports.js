function showFilters(theForm) {
      var reportindex = theForm.selectedReport.selectedIndex;
      var report = theForm.selectedReport.options[reportindex].value;
	  if(report == 'applicantsbyrequisition') {
		applicantsbyrequisition();
	  } else if(report == 'applicantstatusbyrequisition') {
		applicantstatusbyrequisition();
	  } else if(report == 'applicantsbyowner') {
		applicantsbyowner();
	  } else if(report == 'applicantsbymanager') {
		applicantsbymanager();
	  } else if(report == 'applicantsbydistance') {
		applicantsbydistance();
	  } else if(report == 'applicantsbyreferral') {
		applicantsbyreferral();
	  } else if(report == 'requisitionstatus') {
		requisitionstatus();
	  } else if(report == 'requisitionsbyowner') {
		requisitionsbyowner();
	  } else if(report == 'requisitionsbymanager') {
		requisitionsbymanager();
	  } else if(report == 'requisitioncosts') {
		requisitioncosts();
	  } else if(report == 'eeobyjobclass') {
		eeobyjobclass();
	  } else if(report == 'eeoappsummary') {
		eeoappsummary();
	  } else if(report == 'eeoposdesired') {
		eeoposdesired();
	  } else if(report == 'eeostatus') {
		eeostatus();
	  } else if(report == 'excelexport') {
		excelexport();
	  } 
}

function setInitialReportForm() {

document.getElementById('DateFilter').style.display = "none";
document.getElementById('ReqJobDivFilter').style.display = "none";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function applicantsbyrequisition() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function applicantstatusbyrequisition() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function applicantsbyowner() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "block";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";


}

function applicantsbymanager() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "block";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";


}

function applicantsbydistance() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "block";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function applicantsbyreferral() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "block";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function requisitionstatus() {

document.getElementById('DateFilter').style.display = "none";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function requisitionsbyowner() {

document.getElementById('DateFilter').style.display = "none";
document.getElementById('ReqJobDivFilter').style.display = "none";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "block";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function requisitionsbymanager() {

document.getElementById('DateFilter').style.display = "none";
document.getElementById('ReqJobDivFilter').style.display = "none";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "block";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}


function requisitioncosts() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function eeobyjobclass() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "none";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function eeoappsummary() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "none";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function eeoposdesired() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function eeostatus() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "none";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "none";

}

function excelexport() {

document.getElementById('DateFilter').style.display = "block";
document.getElementById('ReqJobDivFilter').style.display = "block";
document.getElementById('ReportClassDivFilter').style.display = "none";
document.getElementById('ProcessOrderDivFilter').style.display = "block";
document.getElementById('DistanceDivFilter').style.display = "none";
document.getElementById('OwnerManagerDivFilter').style.display = "none";
document.getElementById('ReferralDivFilter').style.display = "none";
document.getElementById('ExcelDivFilter').style.display = "block";

}
