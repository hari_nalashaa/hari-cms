function getApplicantAssignInternalInfo(div_to_load, ApplicationID, RequestID, AccessCode) {

	document.frmFormStatus.AccessCode.value = AccessCode;
	document.frmFormStatus.ApplicationID.value = ApplicationID;
	document.frmFormStatus.RequestID.value = RequestID;
	document.frmFormStatus.DivToLoad.value = div_to_load;
	
	$("#"+div_to_load).html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	var callback_url = "formsInternal/getAssignInternal.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&display_app_header=no";
	if(typeof(AccessCode) != 'undefined' && AccessCode != "") callback_url += "&k="+AccessCode;
	
	$.ajax({
		method: "POST",
		url: callback_url,
		type: "POST",
		success: function(data) {
			$("#"+div_to_load).html(data);
			
			$.ajax({
				method: "POST",
				url: "formsInternal/getApplicantAssignedFormsCount.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID,
				type: "POST",
				success: function(data) {
					$("#"+div_to_load+"-COUNT").html("&nbsp;( "+data+" )");
				}
			});
		}
	});
}

function getWebFormsByStatus(form_status) {
	location.href = 'formsInternal.php?typeform=webforms&menu=5&ddlFormStatus='+form_status;
}

function getAgreementFormsByStatus(form_status) {
	location.href = 'formsInternal.php?typeform=agreementforms&menu=5&ddlFormStatus='+form_status;
}

function getSpecificationFormsByStatus(form_status) {
	location.href = 'formsInternal.php?typeform=specforms&menu=5&ddlFormStatus='+form_status;
}

function updateWebFormStatus(WebFormID, IsChecked) {
	
	var FormStatus = 'Active';
	if(IsChecked == true) FormStatus = 'Active';
	else if(IsChecked == false) FormStatus = 'Inactive';
	
	$("#process_web_form_status").html("<span style='color:blue'>Please wait....<br></span>");
	$.ajax({
		method: "POST",
		url: "forms/updateWebFormStatus.php?WebFormID="+WebFormID+"&FormStatus="+FormStatus,
		type: "POST",
		success: function(data) {
			$("#process_web_form_status").html("<span style='color:blue'>Status updated<br></span>");
		}
	});
}

function updateAgreementFormStatus(AgreementFormID, IsChecked) {
	
	var FormStatus = 'Active';
	if(IsChecked == true) FormStatus = 'Active';
	else if(IsChecked == false) FormStatus = 'Inactive';
	
	$("#process_agreement_form_status").html("<span style='color:blue'>Please wait....<br></span>");
	$.ajax({
		method: "POST",
		url: "forms/updateAgreementFormStatus.php?AgreementFormID="+AgreementFormID+"&FormStatus="+FormStatus,
		type: "POST",
		success: function(data) {
			$("#process_agreement_form_status").html("<span style='color:blue'>Status updated<br></span>");
		}
	});
}

function updateSpecificationFormStatus(SpecificationFormID, IsChecked) {
	
	var FormStatus = 'Active';
	if(IsChecked == true) FormStatus = 'Active';
	else if(IsChecked == false) FormStatus = 'Inactive';
	
	$("#process_specification_form_status").html("<span style='color:blue'>Please wait....<br></span>");
	$.ajax({
		method: "POST",
		url: "forms/updateSpecificationFormStatus.php?SpecificationFormID="+SpecificationFormID+"&FormStatus="+FormStatus,
		type: "POST",
		success: function(data) {
			$("#process_specification_form_status").html("<span style='color:blue'>Status updated<br></span>");
		}
	});
}

$(document).ready(function() {

	$('#close_form_status_ic_forms').click(function() {
		
		var AccessCode = document.frmFormStatus.AccessCode.value;
		var ApplicationID = document.frmFormStatus.ApplicationID.value;
		var RequestID = document.frmFormStatus.RequestID.value;
		var div_to_load = document.frmFormStatus.DivToLoad.value;
		
		getApplicantAssignInternalInfo(div_to_load, ApplicationID, RequestID, AccessCode);
		
	});

});
