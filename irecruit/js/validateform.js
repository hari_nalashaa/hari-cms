function overdueother(othervalue) {
	if (othervalue == 'LATE_HIRE_DT_RSN_OTHER')
		document.getElementById('otherdescriptionblock').style.display = 'block';
	else
		document.getElementById('otherdescriptionblock').style.display = 'none';
}

$(function() {
	$('#DOB').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date'
	});

	$('#AlienExpDate').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date'
	});

	$('#estatusfrom').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'yy-mm-dd'
	});

	$('#estatusto').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'yy-mm-dd'
	});

	$('#ListAdocexpiration1').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'mm/dd/yy'
	});

	$('#ListAdocexpiration2').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'mm/dd/yy'
	});

	$('#ListAdocexpiration3').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'mm/dd/yy'
	});

	$('#ListBdocexpiration').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'mm/dd/yy'
	});

	$('#ListCdocexpiration').datepicker({
		changeMonth : true,
		changeYear : true,
		showButtonPanel : true,
		showOn : 'button',
		buttonImage : irecruit_home + 'images/calendar.gif',
		yearRange : '-80:+5',
		buttonImageOnly : true,
		buttonText : 'Select Date',
		dateFormat : 'mm/dd/yy'
	});
});

function validate(typ, obj, msg) {
	obj.style.color = '#000000';
	switch (typ) {
	case "numeric":
		if (IsNumeric(obj.value) == false) {
			msg = msg + ' must only be numbers.';
			obj.style.color = '#EE0000';
			// obj.value='';
			alert(msg);
		}
		break;
	case 'decimal':
		if (IsDecimal(obj.value) == false) {
			msg = msg + ' must be numbers or a decimal value.';
			obj.style.color = '#EE0000';
			// obj.value='';
			alert(msg);
		}
		break;
	}
}

function IsNumeric(strString)
// check for valid numeric strings
{
	var strValidChars = "0123456789";
	var strChar;
	var blnResult = true;
	if (strString.length == 0)
		return false;

	// test strString consists of valid characters listed above
	for (var i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}

function IsDecimal(strString)
// check for valid numeric strings
{
	var strValidChars = "0123456789.";
	var strChar;
	var blnResult = true;

	if (strString.length == 0)
		return false;

	// test strString consists of valid characters listed above
	for (var i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}

function StatesByCountry(countryName) {
	if (countryName == 'US') {
		document.getElementById('State').style.display = 'block';
		document.getElementById('OtherState').style.display = 'none';
	} else {
		document.getElementById('State').style.display = 'none';
		document.getElementById('OtherState').style.display = 'block';
	}

}

function ManageCardNumber(citizenship) {

	if (citizenship == 'resident' || citizenship == 'alien')
		document.getElementById('CardNumberBlock').style.display = 'block';
	else
		document.getElementById('CardNumberBlock').style.display = 'none';
}

function DisplayEverifyStatusMessage(message) {
	document.getElementById('StatusMessage').innerHTML = message;
}