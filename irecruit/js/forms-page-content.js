function handleSectionsChange(ddl_obj, ddl_obj_value, form_id) {
	if(ddl_obj_value == 'formquestions:AFS') {
		location.href = irecruit_home + 'forms.php?form='+form_id+'&action=formquestions&section=0&form_home=yes';
	}
	else {
		document.forms['frmSectionsList'].submit();
	}
}

var forms_data = {
	forms_info: []
};

$( "#sort_form_questions tbody" ).sortable({
	cursor: 'move',
	placeholder: 'ui-state-highlight',
    stop: function( event, ui ) {
        
    	forms_data = {
    			forms_info: []
    	};
    	
    	$(this).find('tr').each(function(i) {

        	var SortOrder = i;
            SortOrder++;
            var tr_id = $(this).attr('id');
			var tr_info = tr_id.split("*");
           
            var FormID 		=	tr_info[0];
            var SectionID 	=	tr_info[1];
            var QuestionID	=	tr_info[2];
        	
        	forms_data.forms_info.push({
        		 "FormID"    : FormID,
        		 "SectionID"  : SectionID,
        		 "QuestionID"  : QuestionID,
        		 "SortOrder" : SortOrder
        	});
        });

        updateFormQuestionsSortOrder(forms_data);
    }
});

function updateFormQuestionsSortOrder(forms_data) {
	
	$("#sort_form_ques_msg").html("Please wait.. ");
	$.ajax({
		 method: "POST",
		 url: "updateFormQuestionsSortOrder.php",
		 data: forms_data,
		 success : function () {
			 $("#sort_form_ques_msg").html("<br>Successfully updated the questions order");
		 }
	});
}