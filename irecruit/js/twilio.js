function getTwilioMessageTemplateInfoByGuID(TemplateGuID, ApplicationID, RequestID, SwapPlaceHolders) {
	
	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID, "TemplateGuID":TemplateGuID, "SwapPlaceHolders":SwapPlaceHolders};
	
	$("#spnSmsMessageLoading").html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	var request	=	$.ajax({
							method: "POST",
					  		url: irecruit_home + "applicants/getTwilioMessageTemplateInfoByGuID.php",
					  		data: input_data,
							type: "POST",
							success: function(data) {
								$("#spnSmsMessageLoading").html('');
								$("#taSmsMessage").val(data);
					    	}
						});
}