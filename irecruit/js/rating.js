function fillStarsByRating(div_id, rating_hidden_input) {
	var rating = document.getElementById(rating_hidden_input).value;

	$("#"+div_id+" span").attr('class', 'fa fa-star fa-2x empty_stars');

	$( "#"+div_id+" span" ).each(function() {
		
		var span_id_info = this.id.split("-");
		var span_id_identifier = span_id_info[0];
		var star_id = span_id_info[1];
		if(star_id <= rating) {
			$("#"+span_id_identifier+"-"+star_id).attr('class', 'fa fa-star fa-2x filled_stars');
		}
	});
}

function clearStars(div_id) {
	$("#"+div_id+" span").attr('class', 'fa fa-star fa-2x empty_stars');
}

function fillStarsOnMouseOver(div_id, span_id) {
	
	var span_id_info = span_id.split("-");
	var span_id_identifier = span_id_info[0];
	var star_id = span_id_info[1];

	$("#"+div_id+" span").attr('class', 'fa fa-star fa-2x empty_stars');
	for(var i = 1; i <= star_id; i++) {
		$("#"+span_id_identifier+"-"+i).attr('class', 'fa fa-star fa-2x filled_stars');
	}
}

function fillStarsOnMouseOut(div_id, rating_hidden_input) {
	clearStars(div_id);
	if(rating_hidden_input != "" && rating_hidden_input != "0") {
		fillStarsByRating(div_id, rating_hidden_input);
	}
}