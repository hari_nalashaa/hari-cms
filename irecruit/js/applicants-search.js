function getSavedSearchInfo(saved_search_name) {
	
	$("#msg_saved_search_info").html('&nbsp;Please wait .... ');

	$.ajax({
		method: "POST",
  		url: "applicants/getSavedSearchInfo.php",
		type: "POST",
		data: {"SearchName":saved_search_name},
		dataType: 'json',
		success: function(data) {

			var AccessCode		= document.frmApplicationDetailInfo.AccessCode.value;
			var MultiOrgIDObj	= document.getElementById('MultiOrgID');
			var ProcessOrderObj = document.getElementById('ProcessOrder');
			var CodeObj			= document.getElementById('Code');
			var ActiveObj		= document.getElementById('Active');
			
			if(saved_search_name == ""
				|| saved_search_name == "Reset") {
				document.getElementById('ApplicationDate_From').value = data.ApplicationDate_From;
				document.getElementById('ApplicationDate_To').value = data.ApplicationDate_To;
				
				setOptionByElementObject(MultiOrgIDObj, "");
				setOptionByElementObject(ActiveObj, "B");
				setOptionByElementObject(ProcessOrderObj, "");
				setOptionByElementObject(CodeObj, "");
				document.forms['Search'].SearchName.value = saved_search_name;
				
				loadRequisitions(irecruit_home, AccessCode);
			}
			else if(saved_search_name != "") {
				document.getElementById('ApplicationDate_From').value = data.ApplicationDate_From;
				document.getElementById('ApplicationDate_To').value = data.ApplicationDate_To;
				
				setOptionByElementObject(MultiOrgIDObj, data.MultiOrgID);
				setOptionByElementObject(ActiveObj, data.Active);
				setOptionByElementObject(ProcessOrderObj, data.ProcessOrder);
				setOptionByElementObject(CodeObj, data.Code);
				document.forms['Search'].SearchName.value = saved_search_name;
				
				loadRequisitions(irecruit_home, AccessCode);
			}
			
			$("#msg_saved_search_info").html('&nbsp;Please submit you form after requisitions load..');
			
    	}
	});
}

function verifyOnboard(OrgID, RequestID, ApplicationID) {

	var request = $.ajax({
		async: true,   // this will solve the problem
		method: "POST",
  		url: "onboard/validateDataManagerForm.php?OrgID="+OrgID+"&RequestID="+RequestID+"&ApplicationID="+ApplicationID,
		type: "POST",
		beforeSend: function() {
			$("#verify_onboard_info").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#verify_onboard_info").html(data);
    	}
	});

	setAjaxErrorInfo(request, 'verify_onboard_info');
}

function updateApplicantNotes() {

	$("#notes_process_status").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	var notes = document.frmUpdateNotes.applicant_notes_info.value;
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	var request = $.ajax({
					method: "POST",
			  		url: "applicants/updateApplicantNotes.php",
					type: "POST",
					data: {"ApplicationID":app_id, "RequestID":req_id, "Notes":notes},
					success: function(data) {
						$("#notes_process_status").css({'color':nav_color}).html('Updated successfully');
			    	}
				});
	setAjaxErrorInfo(request, active_tab);
}

function getApplicantNotes() {
	$("#notes_process_status").css({'color':nav_color}).html('');
	$("#request_process_status").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...loading...');
	
	var current_app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var current_req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	if(typeof(applicants_list_info) == 'string') {
		var app_search_res = JSON.parse(applicants_list_info);
		var app_search_res_len = app_search_res.length;
	}
	else if(typeof(applicants_list_info) == 'object') {
		var app_search_res = applicants_list_info;
		var app_search_res_len = app_search_res.length;
	}

	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	var request = $.ajax({
						method: "POST",
				  		url: "applicants/getApplicantNotes.php",
						type: "POST",
						data: {"ApplicationID":current_app_id, "RequestID":current_req_id},
						success: function(data) {
							var editor_app_notes_info = tinyMCE.get('applicant_notes_info');
							editor_app_notes_info.setContent(data);
							$("#applicant_notes_info").text(data);
							$("#request_process_status").html('');
				    	}
					});
	setAjaxErrorInfo(request, active_tab);
}

function getApplicantInformation() {
	var current_app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var current_req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	var app_search_res_len = 0;
	var app_search_res;
	if(typeof(applicants_list_info) == 'string') {
		app_search_res = JSON.parse(applicants_list_info);
		app_search_res_len = app_search_res.length;
	}
	else if(typeof(applicants_list_info) == 'object') {
		app_search_res = applicants_list_info;
		app_search_res_len = app_search_res.length;
	}
	
	for (var i = 0; i < app_search_res_len; i++) {
		var ApplicationID = app_search_res[i].ApplicationID;
		var RequestID = app_search_res[i].RequestID;
		
		if(current_app_id == ApplicationID && RequestID == current_req_id) {
			return app_search_res[i];
		}
	}
}

function processApplicantDownload(clear_status) {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	var OrgID = document.frmApplicationDetailInfo.OrgID.value;

	var input_data;
	if(clear_status == 'clear') {
		input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID, "OrgID":OrgID, "clear_download" : "yes"};
	}
	else {
		input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID, "OrgID":OrgID};
	}
	send_request('applicants/processSetDownload.php', input_data);
}

function setApplicantsList(data) {
	
	var json_data			=	JSON.parse(data);
	var app_search_res_len	=	json_data.applicants_search_results.length;
	var app_search_res		=	json_data.applicants_search_results;
	
	var app_prev			=	json_data.previous;
	var app_next			=	json_data.next;
	var total_pages			=	json_data.total_pages;
	var current_page		=	json_data.current_page;
	var applicants_count	=	json_data.applicants_count;
	var ApplicantsLimit		=	document.frmApplicationDetailInfo.ApplicantsLimit.value;
	var OrgID				=	document.frmApplicationDetailInfo.OrgID.value;
	
	var WSList = $("#WSStatus").val();
	
	document.frmApplicationDetailInfo.ApplicantsCount.value = applicants_count;
	
	applicants_list_info	=	app_search_res;
	
	if(total_pages == null) total_pages = '';
	if(current_page == null) current_page = '';
	
	var app_min_results			=	'';
	var app_full_view_results	=	'';
	
	$("#applicants_full_view_results").find("tr:gt(0)").remove();
	
	
	for (var i = 0; i < app_search_res_len; i++) {
		
		var ApplicationID		=	app_search_res[i].ApplicationID;
		var ApplicantSortName	=	app_search_res[i].ApplicantSortName;
		var ApplicantEmail		=	app_search_res[i].ApplicantEmail;
		var DispositionCode		=	app_search_res[i].DispositionCode;
		var EntryDate			=	app_search_res[i].EntryDate;
		var MultiOrgID			=	app_search_res[i].MultiOrgID;
		var ProcessOrder		=	app_search_res[i].ProcessOrder;
		var ReceivedDate		=	app_search_res[i].ReceivedDate;
		var RequestID			=	app_search_res[i].RequestID;
		var StatusEffectiveDate =	app_search_res[i].StatusEffectiveDate;
		var WeightedSearchScore =	app_search_res[i].WeightedSearchScore;
		var SendBulkSms			=	app_search_res[i].SendBulkSms;
		
		var OrganizationName		=	app_search_res[i].OrganizationName;
		var RequisitionJobID		=	app_search_res[i].RequisitionJobID;
		var StatusDescription		=	app_search_res[i].StatusDescription;
		var DispositionDescription	=	app_search_res[i].DispositionDescription;
		if(typeof(DispositionDescription) == 'undefined') DispositionDescription = '';
		if(typeof(WeightedSearchScore) == 'undefined') WeightedSearchScore = '';

		if(i == 0) {
			getApplicantDetails(ApplicationID, RequestID, WeightedSearchScore, ApplicantSortName, ApplicantEmail);
		}
		
		app_min_results += "<div class='col-lg-12 col-md-12 col-sm-12' style='padding-bottom:5px;'>";
		app_min_results += "<a href='javascript:void(0);' data-toggle='tooltip' data-placement='right'";
		app_min_results += " data-html='true' title='ApplicationID: "+ApplicationID;
		app_min_results += " <br>EntryDate: "+EntryDate+"<br>StatusEffectiveDate: "+StatusEffectiveDate+"'";
		app_min_results += " onclick=\"getApplicantDetails('"+ApplicationID+"', '"+RequestID+"', '"+WeightedSearchScore+"', '"+ApplicantSortName+"', '"+ApplicantEmail+"')\"";
		app_min_results += " >";
		app_min_results += "<i class='fa fa-file-text-o fa-fw' aria-hidden='true'></i>&nbsp;"+ApplicantSortName;
		app_min_results += "</a>";		
		app_min_results += "</div>";				
		
		if(OrganizationName == 'null' || OrganizationName == null) OrganizationName = '';
		else OrganizationName = OrganizationName + "<br>";
		if(RequisitionJobID == 'null' || RequisitionJobID == null || RequisitionJobID == '') RequisitionJobID = '<strong style="color:red">Requisition has been deleted</strong>';
		app_full_view_results += "<tr>";
		app_full_view_results += "<td valign='top'>"+OrganizationName+RequisitionJobID+"</td>";
		app_full_view_results += "<td valign='top'><a href='mailto:"+ApplicantEmail+"'>"+ApplicantSortName+"</a></td>";
		app_full_view_results += "<td valign='top'>";

		if(OrgID == "I20110816") {
			app_full_view_results += "<a href='applicantsSearch.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID+"' target='_blank'>";
			app_full_view_results += ApplicationID+"</a>";
			app_full_view_results += "</td>";
		}
		else {
			app_full_view_results += "<a href='javascript:void(0);'";
			app_full_view_results += " onclick=\"getApplicantDetailsFromSearch('"+ApplicationID+"', '"+RequestID+"', '"+WeightedSearchScore+"', '"+ApplicantSortName+"', '"+ApplicantEmail+"');\">";
			app_full_view_results += ApplicationID+"</a>";
			app_full_view_results += "</td>";
		}
		
		app_full_view_results += "<td valign='top'>"+StatusDescription;
		if(typeof(DispositionDescription) != 'undefined') {
			app_full_view_results += "<br>"+DispositionDescription;
		}
		app_full_view_results += "<br>Effective: "+StatusEffectiveDate+"<br>Received: "+EntryDate+"</td>";

		if(SendBulkSms == "Yes") {
			app_full_view_results += "<td>";
			app_full_view_results += "<input type='checkbox' id='send_bulk_sms' name='send_bulk_sms[]' value='"+ApplicationID+":"+RequestID+"'>";
			app_full_view_results += "</td>";
		}
		else {
			app_full_view_results += "<td>&nbsp;</td>";
		}
		
		if(typeof(WSList) != "undefined" && WSList != "") {
			app_full_view_results += "<td valign='top' align='left'><a href=\"javascript:void(0)\" onclick=\"javascript:window.open('"+irecruit_home+"applicants/weightedSearchScoreBreakUp.php?WSList="+WSList+"&AppId="+ApplicationID+"&OrgID="+OrgID+"','_blank','location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes')\">"+WeightedSearchScore+"</a></td>";
		}
		
		app_full_view_results += "<td valign='top' align='left'><input id='appid' name='appid[]' value='"+ApplicationID+":"+RequestID+"' type='checkbox'></td>";
		
		app_full_view_results += "</tr>";

	}
	
	if(app_search_res_len == 0) {
		app_min_results += "<div style='padding:5px;margin-top:5px;'>No records found based on your filter</div>";
		
		app_full_view_results += "<tr><td valign='top' colspan='100%' align='center'>No records found based on your filter</td></tr>";
	}
	
	if(parseInt(applicants_count) > parseInt(ApplicantsLimit)) {
		app_min_results += "<div class='col-lg-12'>";
		
		app_min_results += "<div class='row'>";
		
		app_min_results += "<div class='col-lg-6'>";
		app_min_results += "<div style='float:left'>&nbsp;<strong>Per page:</strong> "+ApplicantsLimit+"</div>";
		app_min_results += "</div>";

		app_min_results += "<div class='col-lg-6' style='text-align:right'>";
		app_min_results += "<input type='text' name='current_page_number' id='current_page_number' value='"+current_page+"' style='width:50px;text-align:center' maxlength='4'>";
		app_min_results += " <input type='button' name='btnPageNumberInfo' id='btnPageNumberInfo' value='Go' class='btn-small' onclick='getApplicantsByPageNumber(document.getElementById(\"current_page_number\").value, \"\", \"\", \"no\")'>";
		app_min_results += "</div>";
		
		app_min_results += "</div>";
		
		app_min_results += "<div class='row' id='min_app_search_results_process_msg' style='text-align: center'></div>";
		
		app_min_results += "<div class='row'>";
		
		app_min_results += "<div id='span_left_page_nav_previous' class='col-lg-4 col-md-4 col-sm-4' style='text-align:left;padding-left:15px;padding-right:0px;'>"+app_prev+"</div>";
		app_min_results += "<div class='current_page_and_total_pages col-lg-4 col-md-4 col-sm-4'>"+current_page+' - '+total_pages+"</div>";
		app_min_results += "<div id='span_left_page_nav_next' class='col-lg-4 col-md-4 col-sm-4' style='text-align:right;padding-left:10px;padding-right:10px;'>"+app_next+"</div>";
		app_min_results += "</div>";
		
		app_min_results += "</div>";
		
		app_min_results += "</div>";

		app_full_view_results += '<tr>';
		app_full_view_results += '<td colspan="100%">';
		app_full_view_results += '<div style="padding:0px;margin-top:0px;overflow:hidden;">';
		app_full_view_results += '<div style="text-align:center;width:100%;float: left;border:0px solid #f5f5f5;padding-top:5px;padding-bottom:5px;height:40px">';
		app_full_view_results += '<div style="float:left">&nbsp;<strong>Per page:</strong> '+ApplicantsLimit+'</div>';
		app_full_view_results += '<div style="float:right">';
		app_full_view_results += '<input type="text" name="current_page_fullview_number" id="current_page_fullview_number" value="'+current_page+'" style="width:50px;text-align:center" maxlength="4">';
		app_full_view_results += ' <input type="button" name="btnFullViewPageNumberInfo" id="btnFullViewPageNumberInfo" value="Go" style="background-color:'+nav_color+';color:white;border:2px solid '+nav_color+'" onclick="getApplicantsByPageNumber(document.getElementById(\'current_page_fullview_number\').value, \'\', \'\', \'no\')">';
		app_full_view_results += '</div>';
		app_full_view_results += '</div>';
		app_full_view_results += '<div id="span_fullview_page_nav_previous" class="col-lg-4 col-md-4 col-sm-4" style="text-align:left;padding-left:0px;">'+app_prev+'</div>';
		app_full_view_results += '<div class="current_page_and_total_pages col-lg-4 col-md-4 col-sm-4">';
		app_full_view_results += current_page+" - "+total_pages;
		app_full_view_results += '</div>';
		app_full_view_results += '<div id="span_fullview_page_nav_next" class="col-lg-4 col-md-4 col-sm-4" style="text-align:right;padding-right:0px;">'+app_next+'</div>';
		app_full_view_results += '</td>';
		app_full_view_results += '</tr>';
	}
	else {
 		app_full_view_results += '<tr><td colspan="100%"><input type="hidden" name="current_page_fullview_number" id="current_page_fullview_number" value="1"></td></tr>';
	}
	
	if(applicants_count > 0) {
		app_full_view_results += '<tr><td width="150" align="right">';
		app_full_view_results += '<select name="action" class="form-control">';
		app_full_view_results += '<option value="">Please Select</option>';
		app_full_view_results += '<option value="updatestatus">Update Status</option>';
		app_full_view_results += '<option value="contactapplicant">Contact Applicants</option>';
		app_full_view_results += '<option value="forwardapplicant">Forward Applicants</option>';
		
		if(json_data.UserRole == 'master_admin') {
			app_full_view_results += '<option value="deleteapplicant">Delete Applicants</option>';
		}
		
		app_full_view_results += '<option value="managerequisition">Assign Requisitions</option>';
		app_full_view_results += '<option value="comparative_analysis">Comparative Analysis</option>';
		app_full_view_results += '<option value="downloaddocumentszip">Download Documents Zip</option>';
		app_full_view_results += '</select>';
		app_full_view_results += '</td><td colspan="100%">';
		app_full_view_results += '<input name="multi" value="Y" type="hidden">&nbsp;';
		app_full_view_results += '<input value="Process Applicants" class="btn btn-primary" onclick="return listConfirm(document.multipleapps.appid,document.multipleapps.action)" type="submit">';
		app_full_view_results += '</td></tr>';
	}
	
	$("#applicants-search-results-min").html(app_min_results);
    $('#applicants_full_view_results').append(app_full_view_results);
}

function setThumbsColor(AppConsiderStatus) {
	if(AppConsiderStatus == '') {
		$("#AppConsiderStatusThumbsUp").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsDown").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsClear").css({'color':'#babfc6'});
	}
	if(AppConsiderStatus == 'ThumbsUp') {
		$("#AppConsiderStatusThumbsUp").css({'color':nav_color});
		$("#AppConsiderStatusThumbsDown").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsClear").css({'color':nav_color});
	}
	if(AppConsiderStatus == 'ThumbsDown') {
		$("#AppConsiderStatusThumbsUp").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsDown").css({'color':nav_color});
		$("#AppConsiderStatusThumbsClear").css({'color':nav_color});
	}
}

function setThumbsFilterColor(AppConsiderStatus) {
	if(AppConsiderStatus == '') {
		$("#AppConsiderStatusThumbsUpFilter").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsDownFilter").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsClearFilter").css({'color':'#babfc6'});
	}
	if(AppConsiderStatus == 'ThumbsUp') {
		$("#AppConsiderStatusThumbsUpFilter").css({'color':nav_color});
		$("#AppConsiderStatusThumbsDownFilter").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsClearFilter").css({'color':nav_color});
	}
	if(AppConsiderStatus == 'ThumbsDown') {
		$("#AppConsiderStatusThumbsUpFilter").css({'color':'#babfc6'});
		$("#AppConsiderStatusThumbsDownFilter").css({'color':nav_color});
		$("#AppConsiderStatusThumbsClearFilter").css({'color':nav_color});
	}
}

function getApplicantConsiderationStatus() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	var request = $.ajax({
		method: "POST",
  		url: 'applicants/getApplicantConsiderationStatus.php?ApplicationID='+app_id+'&RequestID='+req_id,
		type: "POST",
		success: function(data) {
			setThumbsColor(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#app_header_process_msg, #applicants-search-results-min");
}

function getApplicantSummaryQuestionsInfo() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	
	var request = $.ajax({
						method: "POST",
				  		url: 'applicants/getApplicantSummaryQuestionsInfo.php?ApplicationID='+app_id,
						type: "POST",
						success: function(data) {
							var applicant_summary = '';
							if(data != '') applicant_summary = "<strong>Applicant Summary</strong><br>"+data; 
							$("#applicant_summary_info").html(applicant_summary);
				    	}
					});
	
	setAjaxErrorInfo(request, "#app_header_process_msg, #applicants-search-results-min");
}


function getApplicantVaultInfo() {
	
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	var k = document.frmApplicationDetailInfo.AccessCode.value;
	
	var request = $.ajax({
		method: "POST",
  		url: 'applicants/applicantVault.php?ApplicationID='+ApplicationID+'&RequestID='+RequestID+'&action=updateapplicant&k='+k+'&display_app_header=no',
		type: "POST",
		beforeSend: function() {
			$("#add-to-vault").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
		},
		success: function(data) {
			$("#add-to-vault").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#app_header_process_msg, #applicants-search-results-min");
}

function getAssignInternalInfo() {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	var k = document.frmApplicationDetailInfo.AccessCode.value;
	
	var request = $.ajax({
		method: "POST",
  		url: "formsInternal/getAssignInternal.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID+'&display_app_header=no',
		type: "POST",
		beforeSend: function() {
			$("#assign-iconnect-form").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
		},
		success: function(data) {
			$("#assign-iconnect-form").html(data);
    	}
	});
		
	setAjaxErrorInfo(request, "#app_header_process_msg, #applicants-search-results-min");
}

function affectedApplicants() {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;

	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	send_request('applicants/affectedApplicants.php', input_data);
}

function getRecordsByPage(IndexStart, to_sort, sort_type) {
	document.frmApplicationDetailInfo.IndexStart.value = IndexStart;
	
	var ApplicantsCount			=	document.frmApplicationDetailInfo.ApplicantsCount.value;
	var keyword					=	document.getElementById('txtApplicantKeyword').value;
	var org_app_label			=	document.getElementById('ddlOrgAppLabelFilter').value;
	var ire_app_filter			=	document.getElementById('ddlIrecruitAppFilter').value;
	var filter_rating			=	document.getElementById('RatingFilter').value;
	var app_filter_sort 		=	document.getElementById('ddlAppDetailSort').value;
	var app_filter_sort_type 	=	document.getElementById('ddlAppDetailSortType').value;
	var ApplicantsSearchGuID 	=	document.frmApplicationDetailInfo.ApplicantsSearchGuID.value;
	var LeadGenerator			=	document.frmLeadGenerator.ddlLeadGenerator.value;
	var InternalApplications	=	(document.frmLeadGenerator.InternalApplications.checked == true) ? "Yes" : "No";
	
	var WSList = $("#WSStatus").val();
	
	if((typeof(sort_type) == 'undefined' || sort_type == 'undefined') && app_filter_sort != "") {
		sort_type = app_filter_sort_type;
	}
	if((typeof(to_sort) == 'undefined' || to_sort == 'undefined') && app_filter_sort_type != "") {
		to_sort = app_filter_sort;
	}
	
	var app_page_url			=	"applicants/getApplicantsByPage.php?IndexStart="+IndexStart+"&ApplicantsCount="+ApplicantsCount+"&Sort="+to_sort+"&sorttype="+sort_type+"&ddlOrgAppLabelFilter="+org_app_label+"&ddlIrecruitAppFilter="+ire_app_filter+"&ApplicantsSearchGuID="+ApplicantsSearchGuID;

	if(keyword != "") app_page_url += "&keyword="+keyword;
	if(filter_rating != "") app_page_url += "&FilterRating="+filter_rating;
	app_page_url += "&LeadGenerator="+LeadGenerator;
	app_page_url += "&InternalApplications="+InternalApplications;
	
	if(WSList != "" && WSList != "undefined") app_page_url += "&WSList="+WSList;

	var request = $.ajax({
					method: "POST",
			  		url: app_page_url,
					type: "POST",
					beforeSend: function() {
						$("#min_app_search_results_process_msg").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
					},
					success: function(data) {
						setApplicantsList(data);
						$("#min_app_search_results_process_msg").html('');
			    	}
				});
	
	setAjaxErrorInfo(request, "#applicants-search-results-min");
}

function clearFilterRating() {
	document.getElementById('RatingFilter').value = "";
	fillStarsByRating("div_filter_rating", "RatingFilter");
	getApplicantsByFilter();
}

function getApplicantsByFilter() {
	var org_app_label			=	document.getElementById('ddlOrgAppLabelFilter').value;
	var ire_app_filter			=	document.getElementById('ddlIrecruitAppFilter').value;
	var filter_rating			=	document.getElementById('RatingFilter').value;
	var app_filter_sort 		=	document.getElementById('ddlAppDetailSort').value;
	var app_filter_sort_type 	=	document.getElementById('ddlAppDetailSortType').value;
	var LeadGenerator			=	document.frmLeadGenerator.ddlLeadGenerator.value;
	var ApplicantsSearchGuID	= 	document.frmApplicationDetailInfo.ApplicantsSearchGuID.value;
	var InternalApplications	=	(document.frmLeadGenerator.InternalApplications.checked == true) ? "Yes" : "No";
	
	var keyword					=	$("#txtApplicantKeyword").val();
	var WSList					=	$("#WSStatus").val();
	
	var filter_request_url = "keyword="+keyword+"&IndexStart=0&ddlOrgAppLabelFilter="+org_app_label+"&ApplicantsSearchGuID="+ApplicantsSearchGuID;
	filter_request_url += "&ddlIrecruitAppFilter="+ire_app_filter;
	filter_request_url += "&LeadGenerator="+LeadGenerator;
	filter_request_url += "&InternalApplications="+InternalApplications;
	
	if(WSList != "") filter_request_url += "&WSList="+WSList;
	if(filter_rating != "") {
		filter_request_url += "&FilterRating="+filter_rating;
	}
	if(app_filter_sort != "" && app_filter_sort_type != "") filter_request_url += "&Sort="+app_filter_sort+"&sorttype="+app_filter_sort_type;
	
	var request = $.ajax({
					  	beforeSend: function () {
					  		$("#applicants-search-results-min").html('<br>&nbsp;&nbsp;&nbsp;&nbsp; Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
					  	},
					  	url: "applicants/getApplicantsByFilter.php", 
						type: "POST",
						data : filter_request_url,
					  	success: function(data) {
					  		setApplicantsList(data);
				    	}
					});
	
	setAjaxErrorInfo(request, "#applicants-search-results-min");
}

function resetApplicantsByFilter() {
	
	document.getElementById('ddlIrecruitAppFilter').selectedIndex 	=	0;
	document.getElementById('ddlOrgAppLabelFilter').selectedIndex 	=	0;
	document.getElementById('ddlAppDetailSort').selectedIndex 		=	0;
	document.getElementById('ddlAppDetailSortType').selectedIndex 	=	1;
	document.getElementById('txtApplicantKeyword').value 			=	'';
	document.getElementById('RatingFilter').value 					=	0;
	document.getElementById('ddlLeadGenerator').selectedIndex 		=	0;
	document.frmLeadGenerator.InternalApplications.checked			=	false;

	var ApplicantsSearchGuID = document.frmApplicationDetailInfo.ApplicantsSearchGuID.value;
	setThumbsFilterColor('');
	
	clearStars('div_filter_rating');
	
	var request = $.ajax({
					  	beforeSend: function () {
					  		$("#applicants-search-results-min").html('<br> Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
					  	},
					  	url: "applicants/getApplicantsByFilter.php?clear=y&ApplicantsSearchGuID="+ApplicantsSearchGuID, 
						type: "POST",
						data : "IndexStart=0",
					  	success: function(data) {
					  		setApplicantsList(data);
				    	}
					});
	
	setAjaxErrorInfo(request, "#applicants-search-results-min");
}

function getApplicantsByPageNumber(page_number, to_sort, sort_type, sort_type_swap) {
	
	if(sort_type_swap == 'yes') {
		var sort_type = document.frmSortOptions.ddlAppDetailSortType.value;

		var x = document.getElementById("ddlAppDetailSortType").selectedIndex;
		var y = document.getElementById("ddlAppDetailSortType").options;
		
		if(y[x].text == 'Ascending') document.getElementById("ddlAppDetailSortType").selectedIndex = 1;
		else if(y[x].text == 'Descending') document.getElementById("ddlAppDetailSortType").selectedIndex = 0;
	}
	
	var ApplicantsLimit = document.frmApplicationDetailInfo.ApplicantsLimit.value;
	var ApplicantsCount = document.frmApplicationDetailInfo.ApplicantsCount.value;
	
	var TotalPages = Math.ceil(ApplicantsCount/ApplicantsLimit);
	var IndexStart = (page_number - 1) * ApplicantsLimit;
	
	getRecordsByPage(IndexStart, to_sort, sort_type);
}

function sortApplicants() {
	
	var current_page_num = 1;
	if(document.getElementById('current_page_fullview_number') != null) {
		current_page_num = document.getElementById('current_page_fullview_number').value;
	}
	var to_sort = document.getElementById('ddlAppDetailSort').value;
	var sort_type = document.getElementById('ddlAppDetailSortType').value;

	if(to_sort != "") getApplicantsByPageNumber(current_page_num, to_sort, sort_type, 'no');
}

function getOnboardApplicant(data_type, onboard_source) {
	
	if(data_type == 'process') {
		var form = $('#appedit_onboard');
		var input_data = form.serialize();
	}	
	else if(data_type == '') {
		var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
		var RequestID = document.frmApplicationDetailInfo.RequestID.value;

		if(typeof(document.forms['appedit']) != 'undefined') {
			document.forms['appedit'].ApplicationID.value = ApplicationID;
			document.forms['appedit'].RequestID.value = RequestID;
		}
		
		var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID, "DisplayApplicantSummary":"no", "action":"onboardapplicant"};
	}
	
	$("#onboard-applicant").html('<br><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
	
	var request = $.ajax({
		method: "POST",
  		url: 'onboard/getOnboardApplicant.php?OnboardSource='+onboard_source, 
		type: "POST",
		data: input_data,
		success: function(data) {
			
			$("#onboard-applicant").html(data);
			
			$('.zip_input').each(function(){
				var form_name = $(this).parents("form").attr('name');
				if(this.value != "") ValidateZipCode(this, form_name);
			});
			
			$('.zip_input').change(function() {
				var form_name = $(this).parents("form").attr('name');
				if(this.value != "") ValidateZipCode(this, form_name);
			});

    	}
	});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function delApplicants(ApplicationID, RequestID) {
	
	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	if (confirm("Are you sure? Do you want to delete this applicant!") == true) {
		$.ajax({
			method: "POST",
	  		url: 'applicants/deleteApplicants.php', 
			type: "POST",
			data: input_data,
			success: function(data) {
				if(data != '') {
					alert(data);
					location.href = irecruit_home + 'applicantsSearch.php';
				}
	    	}
		});
	}
	
}

//Common request function based the selected tab
function send_request(callback_url, input_data) {
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;

	var data_type = 'json';
	if(active_tab == '#applicant-history' 
		|| active_tab == '#view-application'
		|| active_tab == '#applicant-attachments'
		|| active_tab == '#forward-applicant-email'
		|| active_tab == '#edit-application'
		|| active_tab == '#contact-applicant'
		|| active_tab == '#assign-requisition'
		|| active_tab == '#checklist') {
		data_type = '';
		
		if(active_tab != '#forward-applicant-email' 
			&& active_tab != '#contact-applicant'
			&& active_tab != '#assign-requisition'
			&& active_tab != '#applicant-attachments')
		{
			$(active_tab).html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..loading...');
		}
	}

	$("#request_process_status").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...loading...');
	var request = $.ajax({
						method: "POST",
						//timeout: 100000, //100 second timeout
				  		url: callback_url, 
						type: "POST",
						data: input_data,
						dataType: data_type,
						success: function(app_details) {
							$("#request_process_status").html("");
							if(active_tab == '#applicant-details') {
								setApplicantHeaderInfo(app_details);
							}
							if(active_tab == '#view-application') {
								setApplicationView('<br>'+app_details);
							}
							if(active_tab == '#applicant-attachments') {
								setAttachmentDetails('<br><table class="table table-striped table-bordered table-hover">'+app_details+'</table>');
							}
							if(active_tab == '#applicant-history') {
								setApplicantHistory('<br>'+app_details);
							}
							if(active_tab == '#checklist') {
								setChecklist('<br>'+app_details);
							}
							if(active_tab == '#forward-applicant-email') {
								setForwardApplicantsFormInfo('The following applications will be affected:<br>'+app_details)
							}
							if(active_tab == '#edit-application') {
								setApplicantEditForm('<br>'+app_details);
							}
							if(active_tab == '#contact-applicant') {
								setContactApplicantFormInfo(app_details);
							}
							if(active_tab == '#assign-requisition') {
								setManageRequisitionFormInfo(app_details);
							}
				    	}
					});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function setApplicantEditForm(ad) {
	$('#edit-application').html(ad);
	
	$('.zip_input').each(function(){
		var form_name = $(this).parents("form").attr('name');
		if(this.value != "") ValidateZipCode(this, form_name);
	});
	$('.zip_input').change(function() {
		
		var form_name = $(this).parents("form").attr('name');
		if(this.value != "") ValidateZipCode(this, form_name);
	});

	tinymce_editor();	
}

function setContactApplicantFormInfo(ad) {
	$('#contact_affected_applicants').html('<br>The following applications will be affected:<br>' + ad);
}

//Get Contact Applicants Prefilled Form Information
function getContactApplicantsFormInfo() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	if(typeof(document.frmContactApplicant.multi) != "undefined" 
		&& document.frmContactApplicant.multi.value == "Y") {
		document.frmContactApplicant.multipleapps.value = app_id + ':' + req_id + '|';
	}
	else {
		document.frmContactApplicant.ApplicationID.value = app_id;
		document.frmContactApplicant.RequestID.value = req_id;
	}
	
	if(typeof(document.frmContactCorrespondencePkg.multi) != "undefined" 
		&& document.frmContactCorrespondencePkg.multi.value == "Y") {
		document.frmContactCorrespondencePkg.multipleapps.value = app_id + ':' + req_id + '|';
	}
	else {
		document.frmContactCorrespondencePkg.ApplicationID.value = app_id;
		document.frmContactCorrespondencePkg.RequestID.value = req_id;
	}

	$("select#ProcessOrder").prop('selectedIndex', 0);
	$("select#package").prop('selectedIndex', 0);
	$("select#DispositionCode").prop('selectedIndex', 0);
	$("#subject").val('');
	var editor = tinyMCE.get('body');
	editor.setContent('');
	$("#body").text('');
	$("#contact_applicant_msg_top").html('');
	$("#contact_applicant_msg_bottom").html('');
	
	data = {"ApplicationID":app_id, "RequestID":req_id, "DownloadStatus":"no", "DisplayApplicantSummary":"no"};
	send_request('applicants/affectedApplicants.php', data);
}


//Get Contact Applicants Prefilled Form Information
function getManageRequisitionFormInfo() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	if(typeof(document.frmManageRequisitions.multi) != "undefined" 
		&& document.frmManageRequisitions.multi.value == "Y") {
		document.frmManageRequisitions.multipleapps.value = app_id + ':' + req_id + '|';
	}
	else {
		document.frmManageRequisitions.ApplicationID.value = app_id;
		document.frmManageRequisitions.RequestID.value = req_id;
	}

	data = {"ApplicationID":app_id, "RequestID":req_id, "DownloadStatus":"no"};
	send_request('applicants/affectedApplicants.php', data);
}

function getApplicantEditForm() {
	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	var OrgID			=	document.frmApplicationDetailInfo.OrgID.value;

	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID, "OrgID":OrgID, "action":"applicationedit"};
	send_request('applicants/getApplicationEditForm.php', input_data);
}

function getApplicantEditFormBySectionID(SectionID) {
	var OrgID			=	document.frmApplicationDetailInfo.OrgID.value;
	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	
	var input_data = {"OrgID":OrgID, "ApplicationID":ApplicationID, "RequestID":RequestID, "SectionID":SectionID, "action":"applicationedit"};
	send_request('applicants/getApplicationEditForm.php', input_data);
}


//Get Application view
function getApplicationView(org_id, app_id, req_id) {
	data = {"OrgID": org_id, "ApplicationID":app_id, "RequestID":req_id};
	send_request('applicants/getApplicationView.php', data);
}

//Set Application view data 
function setApplicationView(ad) {
	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;

	$('#view-application').html('<div style="float:right"><a href="'+irecruit_home+'printApplication.php?ApplicationID='+ApplicationID+'&RequestID='+RequestID+'" target="_blank"><img src="'+irecruit_home+'/images/icons/printer.png"> Print Application</a></div>'+ad);
}

//Get Update Status Form
function getUpdateStatusFormInfo() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	var request = $.ajax({
		method: "POST",
  		url:	'applicants/getUpdateStatusForm.php?ApplicationID='+app_id+'&RequestID='+req_id,
		type:	"POST",
		beforeSend: function () {
			$("#update-applicant-status").html('<h5> Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h5>');
	  	},
		success: function(data) {
			$("#update-applicant-status").html(data);
			tinymce_editor();
    	}
	});

}

//Get Forward Applicants Prefilled Form Information
function getForwardApplicantsFormInfo() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	$("#forward_app_message_top").html('');
    $("#forward_app_message_bottom").html('');

    if (typeof(document.forwardemail) != 'undefined' 
    	&& typeof(document.forwardemail.multi) != "undefined" 
    	&& document.forwardemail.multi.value == "Y") {
        document.forwardemail.multipleapps.value = app_id + ':' + req_id + '|';
    } else {
        document.forwardemail.ApplicationID.value = app_id;
        document.forwardemail.RequestID.value = req_id;
    }
	
	var request = $.ajax({
		method: "POST",
  		url: 'applicants/getForwardApplicantForm.php?ApplicationID='+app_id+'&RequestID='+req_id,
		type: "POST",
		beforeSend: function () {
			$("#forward-applicant-email").html('<h5> Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h5>');
	  	},
		success: function(data) {
			$("#forward-applicant-email").html(data);
			tinymce_editor();
    	}
	});

	setAjaxErrorInfo(request, "#forward-applicant-email");
}

//Set Forward Applicants Form Information
function setForwardApplicantsFormInfo(ad) {
	$("#forward_affected_applicants").html(ad);
}

//Set Forward Applicants Form Information
function setManageRequisitionFormInfo(ad) {
	$("#manage_req_msg_bottom").html('');
	$("#manage_req_msg_top").html('');
	$("#manage_req_affected_applicants").html('<br>The following applications will be affected:<br>'+ad);
}

function setApplicantHeaderInfo(ad) {
	if(ad.CellPhonePermission == "Yes") {
		$("#tab_twilio_sms").show();
	} 
	else {
		$("#tab_twilio_sms").hide();
	}

	setApplicantDetails(ad);
	$('#nav_applicant_name').html(ad.first + ' ' + ad.last);
	document.frmApplicationDetailInfo.FirstName.value = ad.first;
	document.frmApplicationDetailInfo.LastName.value = ad.last;
	
	$('#nav_applicant_email').html(ad.email);
	$('#nav_applicant_email').attr('href', 'mailto:'+ad.email);
	document.frmApplicationDetailInfo.Email.value = ad.email;
	
	if(ad.RequisitionExist == "False") $("#app_header_process_msg").html("<h4 style='color:red'>Requisition related to this application has been deleted</h4>");
	else $("#app_header_process_msg").html("");
	if(typeof(ad.address2) != 'undefined' && typeof(ad.address2) != null) 
	{
		$('#nav_app_address2').html(ad.address2);
	}
	else {
		$('#nav_app_address2').html('');
	}
	
	if(typeof(ad.address) != 'undefined' && typeof(ad.address) != null) 
	{
		$('#nav_app_address').html(ad.address);
	}
	else {
		$('#nav_app_address').html('');
	}
	
	$("#nav_app_formatted_address").html(ad.FormattedAddress);
	
	//Set ThumbsUp status of applicant
	setThumbsColor(ad.ApplicantConsiderationStatus1);
	
	//Set Filter label status of applicant
	var org_app_lbls = document.getElementById("ddlOrgAppLabelsList");
	
	for(var j=0; j < org_app_lbls.options.length; j++){
		if(org_app_lbls.options[j].value == ad.ApplicantConsiderationStatus2)
		{
			document.getElementById('ddlOrgAppLabelsList').selectedIndex = j;
		}
	}
	
	var WSStatus = '';
	if(typeof(document.frmApplicationDetailInfo.WSStatus) != 'undefined') {
		WSStatus = document.frmApplicationDetailInfo.WSStatus.value;
	}
	

	if(typeof(WSStatus) != 'undefined' && WSStatus != "") {
		$('#nav_applicant_score').text(ad.ApplicantScore+" out of "+ad.TotalScore);
		$('#nav_applicant_score').attr('href', '#applicant-score-info');
	}
	else {
		$('#nav_applicant_score').attr('href', '#applicant-score-info');
		$('#nav_applicant_score').text('');
	}

	if(typeof(ad.WotcApplicationQualify) != 'undefined' 
		&& ad.WotcApplicationQualify != "") {
		$('#wotc_status').html("<br><strong>CMS WOTC Status:</strong> " + ad.WotcApplicationQualify);
	}
	else {
		$('#wotc_status').html('');
	}

	if(typeof(ad.homephone) != "undefined") {
		$('#nav_app_homephone').html(ad.homephone + ' (Home Phone)');
	}
	else {
		$('#nav_app_homephone').html(' (Home Phone)');
	}

	if(typeof(ad.cellphone) != "undefined") {
		$('#nav_app_cellphone').html(ad.cellphone + ' (Cell Phone)');
	}
	else {
		$('#nav_app_cellphone').html(' (Cell Phone)');
	}
	
	if(ad.informed == "" || typeof(ad.informed) == 'undefined') {
		$('#nav_referral_source').html('');
	}
	else {
		$('#nav_referral_source').html('<strong>Referral Source:</strong> '+ad.informed);
	}
	
	if(ad.IsInternalApplication == "" 
		|| typeof(ad.IsInternalApplication) == 'undefined'
		|| ad.FeatureInternalRequisitions != 'Y'
		) {
		$('#nav_is_internal_application').html('');
	}
	else {
		$('#nav_is_internal_application').html('<strong>Internal Application:</strong> <a href="javascript:void(0);" id="link_internal_application_status" onclick="updateInternalApplicationStatus();">'+ad.IsInternalApplication+'</a>');
	}
	
	$('#nav_applicant_status').html(ad.status);
	$('#nav_date_applied').html(ad.ApplicationDate);
	$('#nav_tags_set').html(ad.TagsSet);
	
	$('#nav_reqid_jobid').html("<a href='"+irecruit_home + "requisitionsSearch.php?menu=3&RequestID="+ad.RequestID+"&Active="+ad.RequisitionStatus+"' target='_blank'>"+ad.requestids+"</a>");
	$('#nav_req_title').html($.trim(ad.jobtitle));
	$('#nav_assigned_forms_count').html("<strong>Assigned Forms:</strong> "+ad.AssignedFormsCount);
	$("#nav_applicants_count").html("("+ad.applicants_count+" Applicants)");
	$('#nav_req_owner').html(ad.reqownersname);
	$('#nav_application_id').html(ad.ApplicationID);
	var Paperless = ad.Paperless;
	
	//Set Filter label status of applicant
	var paperless_options = document.getElementById("Paperless");
	
	if(typeof(paperless_options) != 'undefined' && paperless_options != null) {
		for(var j=0; j < paperless_options.options.length; j++) {
			if(paperless_options.options[j].value == ad.Paperless)
			{
				document.getElementById('Paperless').selectedIndex = j;
			}
		}
	}
	
	
	if(ad.Rating != "" && ad.Rating != '0') {
		document.getElementById('ApplicantRating').value = ad.Rating;
	}
	else {
		document.getElementById('ApplicantRating').value = 0;
	}
	
	fillStarsByRating("applicant_rating", "ApplicantRating");	
	
}

function updateInternalApplicationStatus() {
	var app_id						=	document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id						=	document.frmApplicationDetailInfo.RequestID.value;
	var InternalApplicationStatus	=	$("#link_internal_application_status").text(); 

	var request	=	$.ajax({
						method: "POST",
				  		url: 'applicants/updateInternalApplicationStatus.php?ApplicationID='+app_id+'&RequestID='+req_id+'&InternalApplicationStatus='+InternalApplicationStatus,
						type: "POST",
						beforeSend: function () {
							$("#app_header_process_msg").html("Please wait...");
					  	},
						success: function(data) {
							$("#app_header_process_msg").html("Updated successfully");
							$('#nav_is_internal_application').html('<strong>Internal Application:</strong> <a href="javascript:void(0);" id="link_internal_application_status" onclick="updateInternalApplicationStatus()">'+data+'</a>');
				    	}
					});
}

function updatePaperlessStatus() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	var paperless_status = document.frmPaperlessStatus.Paperless.value;
	var request = $.ajax({
						method: "POST",
				  		url: 'applicants/updatePaperlessStatus.php?ApplicationID='+app_id+'&RequestID='+req_id+'&Paperless='+paperless_status,
						type: "POST",
						success: function(data) {
							$("#app_header_process_msg").html(data);
				    	}
					});

	setAjaxErrorInfo(request, "#app_header_process_msg");
}

function setApplicantRating(div_id, span_id, rating_hidden_input) {
	
	var span_id_info		=	span_id.split("-");
	var span_id_identifier	=	span_id_info[0];
	var star_id				=	span_id_info[1];
	
	document.getElementById(rating_hidden_input).value = star_id;
	
	$("#"+div_id+" span").attr('class', 'fa fa-star fa-2x empty_stars');
	for(var j = 1; j <= star_id; j++) {
		$("#"+span_id_identifier+"-"+j).attr('class', 'fa fa-star fa-2x filled_stars');
	}

	fillStarsByRating(div_id, rating_hidden_input);

	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	var rating = document.getElementById(rating_hidden_input).value;
	
	var request	= $.ajax({
						method: "POST",
				  		url: 'applicants/updateRating.php?ApplicationID='+app_id+'&RequestID='+req_id+"&Rating="+rating,
						type: "POST",
						success: function(data) {
							//Nothing to do
				    	}
					});
	
	setAjaxErrorInfo(request, "#request_process_status");
}

function filterApplicnatsByRating(div_id, span_id, rating_hidden_input) {
	
	var span_id_info = span_id.split("-");
	var span_id_identifier = span_id_info[0];
	var star_id	= span_id_info[1];
	document.getElementById(rating_hidden_input).value = star_id;
	
	$("#"+div_id+" span").attr('class', 'fa fa-star fa-2x empty_stars');
	for(var j = 1; j <= star_id; j++) {
		$("#"+span_id_identifier+"-"+j).attr('class', 'fa fa-star fa-2x filled_stars');
	}

	fillStarsByRating(div_id, rating_hidden_input);

	getApplicantsByFilter();
}

function getApplicantHeaderInfo() {
	
	var app_id		=	document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id		=	document.frmApplicationDetailInfo.RequestID.value;
	var WSStatus	=	document.frmApplicationDetailInfo.WSStatus.value;
	
	var input_data	=	{"ApplicationID":app_id, "RequestID":req_id, "WSList":WSStatus};
	
	var request = $.ajax({
		method: "POST",
  		url: 'applicants/getApplicantDetails.php', 
		type: "POST",
		data: input_data,
		dataType: 'json',
		success: function(app_details) {
			setApplicantHeaderInfo(app_details);
    	}
	});

	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function setApplicantScoreInfo() {
	
	var ApplicationID		=	document.frmApplicationDetailInfo.ApplicationID.value;
	var OrgID				=	document.frmApplicationDetailInfo.OrgID.value;
	var WSStatus			=	document.frmApplicationDetailInfo.WSStatus.value;
	var WeightedSearchForm	=	document.frmApplicationDetailInfo.WeightedSearchFormID.value;
	
	var ws_score_breakup_url = '';
	if(WeightedSearchForm == "WebForms") {
		ws_score_breakup_url = 'applicants/weightedSearchWebFormScoreBreakUp.php?WSList='+WSStatus+'&AppId='+ApplicationID+'&OrgID='+OrgID;
	}
	else {
		ws_score_breakup_url = 'applicants/weightedSearchScoreBreakUp.php?WSList='+WSStatus+'&AppId='+ApplicationID+'&OrgID='+OrgID;
	}
	
	var request = $.ajax({
					method: "POST",
			  		url: ws_score_breakup_url, 
					type: "POST",
					success: function(data) {
						$("#applicant-score-info").html('<br>'+data);
			    	}
				});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

//Get applicant details information
function getApplicantDetails(app_id, req_id, weighted_score, applicant_sort_name, applicant_email) {
	
	document.frmApplicationDetailInfo.ApplicationID.value		=	app_id;
	document.frmApplicationDetailInfo.RequestID.value			=	req_id;
	document.frmApplicationDetailInfo.WeightedSearchScore.value =	weighted_score;
	document.frmApplicationDetailInfo.ApplicantSortName.value	=	applicant_sort_name;
	document.frmApplicationDetailInfo.Email.value				=	applicant_email;
	
	document.frmScheduleInterview.ApplicationID.value			=	app_id;
	document.frmScheduleInterview.RequestID.value				=	req_id;
	
	document.frmStatusForm.ApplicationID.value					=	app_id;
	document.frmStatusForm.RequestID.value						=	req_id;
	
	getApplicantSummaryQuestionsInfo();
	
	if(typeof(applicant_sort_name) != "undefined") {
		var fname_lname = applicant_sort_name.split(", ");
		var fname = fname_lname[1];
		var lname = fname_lname[0];
		
		document.frmApplicationDetailInfo.FirstName.value = fname;
		document.frmApplicationDetailInfo.LastName.value = lname;
	}
	
	var org_id		=	document.frmApplicationDetailInfo.OrgID.value;
	var email		=	document.frmApplicationDetailInfo.Email.value;
	var WSStatus	=	document.frmApplicationDetailInfo.WSStatus.value;
	
	data = {"ApplicationID":app_id, "RequestID":req_id, "WSList":WSStatus};
	var selected_tab = document.frmApplicationDetailInfo.current_sel_tab.value;

	if(selected_tab != '#applicant-details') {
		getApplicantHeaderInfo();
	}
	if(selected_tab == '#affirmative-action') {
		viewAffirmativeActionInfo("aa");
	}
	if(selected_tab == '#veteran-status') {
		viewAffirmativeActionInfo("veteran");
	}
	if(selected_tab == '#disability-status') {
		viewAffirmativeActionInfo("disabled");
	}	
	if(selected_tab == '#applicant-details') {
		send_request('applicants/getApplicantDetails.php', data);
	}
	if(selected_tab == '#applicant-score-info') {
		setApplicantScoreInfo();
	}
	if(selected_tab == '#view-application') {
		getApplicationView(org_id, app_id, req_id);
	}
	if(selected_tab == '#applicant-attachments') {
		getAttachmentDetails();
		$("#attachments_info").html("");
	}
	if(selected_tab == '#applicant-history') { 
		getApplicantHistory(app_id, req_id);
	}
	if(selected_tab == '#checklist') {
		getChecklist(app_id, req_id);
	}
	if(selected_tab == '#related-applications') {
		if(email != "") loadXMLDoc(app_id, req_id, email);
	}
	if(selected_tab == '#schedule-interview') {
		setScheduleInterviewFormInfo();
	}
	if(selected_tab == '#forward-applicant-email') {
		getForwardApplicantsFormInfo();
	}
	if(selected_tab == '#edit-application') {
		getApplicantEditForm();
	}
	if(selected_tab == '#contact-applicant') {
		getContactApplicantsFormInfo();
	}
	if(selected_tab == '#assign-requisition') {
		getManageRequisitionFormInfo();
	}
	if(selected_tab == '#onboard-applicant') {
		var onboard_src = urlParam('OnboardSource');
		if(typeof(onboard_src) == 'undefined' || onboard_src == null) onboard_src = '';
		if(onboard_src == '') onboard_src = single_onboard_process;
		
		getOnboardApplicant('', onboard_src);
	}
	if(selected_tab == '#add-to-vault') {
		getApplicantVaultInfo();
	}
	if(selected_tab == '#assign-iconnect-form') {
		getAssignInternalInfo();
	}
	if(selected_tab == '#applicant-resume-html-view') {
		getResumeHtmlView();
	}
	if(selected_tab == '#applicant-notes') {
		getApplicantNotes();
	}
	if(selected_tab == '#update-applicant-status') {
		getUpdateStatusFormInfo();
	}
	if(selected_tab == '#lead-generator') {
		getLeadGeneratorForm();
	}
	if(selected_tab == '#portal-user') {
		getUserPortalUserInfo();
	}
	if(selected_tab == '#twilio-sms') {
		getApplicantTwilioConversation();
	}
	if(selected_tab == '#twilio-sms-archive') {
		getTwilioConversationsArchive();
	}
	if(selected_tab == '#interview-analysis') {
		getInterviewForms(req_id, app_id);
	}

	var curr_app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var curr_req_id = document.frmApplicationDetailInfo.RequestID.value;
	
	if(typeof(applicants_list_info) == 'string') {
		var app_search_res		=	JSON.parse(applicants_list_info);
		var app_search_res_len	=	app_search_res.length;
	}
	else if(typeof(applicants_list_info) == 'object') {
		var app_search_res		=	applicants_list_info;
		var app_search_res_len	=	app_search_res.length;
	}
	
	var first_index_rec = '';
	var last_index_rec = '';
	if(typeof(app_search_res) != 'undefined') {
		first_index_rec = app_search_res[0].ApplicationID + "-" + app_search_res[0].RequestID;
		last_index_rec = app_search_res[app_search_res_len - 1].ApplicationID + "-" + app_search_res[app_search_res_len - 1].RequestID;
	}
	
	var cur_rec = curr_app_id+"-"+curr_req_id;
	
	var current_app_info = getApplicantInformation();

	if(current_app_info.Checklist == "False") {
		$("#checklist-tab").hide();
	}
	else if(current_app_info.Checklist == "True") {
		$("#checklist-tab").show();
	}

	if(current_app_info.ArchiveTextMessages == "False") {
		$("#tab_twilio_sms_archive").hide();
	}
	else if(current_app_info.ArchiveTextMessages == "True") {
		$("#tab_twilio_sms_archive").show();
	}

	if(current_app_info.LeadStatus == "") {
		$("#tab_process_lead").hide();
	}
	else {
		$("#tab_process_lead").show();
	}
	
	if(current_app_info.UserPortalUserID != '') {
		$("#tab_portal_user").show();
	}
	else {
		$("#tab_portal_user").hide();
	}
	
	if(cur_rec == first_index_rec) {
		$("#app_detail_prev").hide();
	}
	if(cur_rec == last_index_rec) {
		$("#app_detail_next").hide();
	}
	if(cur_rec != last_index_rec) {
		$("#app_detail_next").show();
	}
	if(cur_rec != first_index_rec) {
		$("#app_detail_prev").show();
	}
	if(current_app_info.InterviewForms == "False") {
		$("#tab-interview-analysis").hide();
	}
	else if(current_app_info.InterviewForms == "True") {
		$("#tab-interview-analysis").show();
	}
}

function getApplicantTwilioConversation() {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;

	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	
	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	$(active_tab).html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	var request	=	$.ajax({
							method: "POST",
					  		url: "applicants/getApplicantTwilioConversation.php",
					  		data: input_data,
							type: "POST",
							success: function(data) {
								$(active_tab).html(data);
					    	}
						});
}

function getTwilioConversationsArchive() {
	var ApplicationID 	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	
	var active_tab		=	document.frmApplicationDetailInfo.current_sel_tab.value;
	
	var input_data		=	{"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	$(active_tab).html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	var request			=	$.ajax({
								method: "POST",
						  		url: "applicants/getApplicantTwilioConversationArchive.php",
						  		data: input_data,
								type: "POST",
								success: function(data) {
									$(active_tab).html(data);
						    	}
							});
}

function sendTwilioConversationMessage(btn_obj, div_id) {
	//Here div_id not useful 
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;

	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	
	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID};
	
	$(active_tab).html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');

	var form_obj 	=	$(btn_obj).parents('form:first');
	var input_data 	=	$(form_obj).serialize();
	
	var request		=	$.ajax({
							method: "POST",
					  		url: "applicants/getApplicantTwilioConversation.php",
					  		data: input_data,
							type: "POST",
							success: function(data) {
								$(active_tab).html(data);
					    	}
						});
}

function getInterviewForms(reqid, appid) {

        var request = $.ajax({
                method: "POST",
                url: irecruit_home + "interview/presentForms.php?RequestID="+reqid+"&ApplicationID="+appid,
                type: "POST",
                beforeSend: function(){
                        $("#interview-analysis").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
                },
                success: function(data) {

                        $("#interview-analysis").html(data);
        }
        });

}

function getApplicantDetailsFromSearch(app_id, req_id, weighted_score, applicant_sort_name, applicant_email) {
	$("#search-results-detail").hide();
	$("#applicant-detail-view").show();
	$("#applicants-minified-search-results").show();
	$("#applicant-detail-info").show();
	$("#search-form").hide();
	$("#full-view-navigation-bar").hide();
	
	getApplicantDetails(app_id, req_id, weighted_score, applicant_sort_name, applicant_email);
}

function setScheduleInterviewFormInfo() {
	var first = document.frmApplicationDetailInfo.FirstName.value;
	var last = document.frmApplicationDetailInfo.LastName.value;
	var email = document.frmApplicationDetailInfo.Email.value;
	
	$("#sche_intw_first_last").html(first + ' ' + last);
	
	$("#sche_intw_email").text(email);
	$('#sche_intw_email').attr('href', 'mailto:'+email)
}

function add_edit_attachments(edit_link, data_process) {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
		
	var request = $.ajax({
		method: "POST",
  		url: edit_link, 
		type: "POST",
		success: function(data) {
			$('#attachments_info').html(data);
    	}
	});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function processAttachments() {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	
	$("#msg_process_attachments_info").html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');
	var input_data = new FormData($('#frmEditAttachments')[0]);

	var request = $.ajax({
					method: "POST",
			  		url: "applicants/editAttachments.php",
			  		data: input_data,
					type: "POST",
					processData: false,
					contentType: false,
					dataType: 'json',
					success: function(data) {
						$('#attachments_info').html(data.applicant_info + data.message);
						if(data.edit_type == "New" && data.status == "success") {
							getAttachmentDetails();
						}
			    	}
				});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function updAAVetDisEditStatus(is_checked, form_type) {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	var OrgID = document.frmApplicationDetailInfo.OrgID.value;
	
	if(is_checked == true) status = 'Y';
	else if(is_checked == false) status = 'N';
	
	var request = $.ajax({
						method: "POST",
				  		url: "applicants/updAAVetDisEditStatus.php?OrgID="+OrgID+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&status="+status+"&type="+form_type,
						type: "POST",
						beforeSend: function () {
							//For now nothing is there
					  	},
						success: function(data) {
							//For now nothing is there
				    	}
					});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}


function viewAffirmativeActionInfo(form_type) {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	var OrgID = document.frmApplicationDetailInfo.OrgID.value;
	var k = document.frmApplicationDetailInfo.AccessCode.value;
	
	$('.ra-tab-content').hide();
	$('#affirmative-action').show();
	
	var request = $.ajax({
					method: "POST",
			  		url: "applicants/getAffirmativeActionInfo.php?OrgID="+OrgID+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&k="+k+"&form="+form_type+'&display_app_header=no',
					type: "POST",
					beforeSend: function () {
						$("#affirmative-action").html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');
				  	},
					success: function(data) {
						var action_data;
						if(form_type == 'aa') {
							action_data = '<br><span id="ad_edit_affirmative_action" style="padding: 0px 10px 10px 10px;cursor:pointer;" onclick="getAffirmativeActionForm()">Edit Details<img src="'+irecruit_home+'images/icons/pencil.png" title="Edit" border="0"></span><br>';
							action_data += '<br/>'+data;
						}
						else {
							action_data = '<br/>'+data;
						}
						$("#affirmative-action").html(action_data);
			    	}
				});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function getAffirmativeActionForm() {
	var ApplicationID = document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID = document.frmApplicationDetailInfo.RequestID.value;
	var OrgID = document.frmApplicationDetailInfo.OrgID.value;
	var k = document.frmApplicationDetailInfo.AccessCode.value;
	
	$('.ra-tab-content').hide();
	$('#affirmative-action').show();
	
	var input_data = {"ApplicationID":ApplicationID, "RequestID":RequestID, "OrgID":OrgID};

	var request = $.ajax({
						method: "POST",
				  		url: "applicants/getAffirmativeActionForm.php?OrgID="+OrgID+"&ApplicationID="+ApplicationID+"&RequestID="+RequestID+"&k="+k,
						type: "POST",
						data: input_data, 
						beforeSend: function () {
							$("#affirmative-action").html('<br/><img src="'+irecruit_home+'images/wait.gif"/> Please wait..data is processing...');
					  	},
						success: function(data) {
							$("#affirmative-action").html('<br>'+data);
				    	}
					});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

var numbs = new Array(1, 2, 3, 4, 5);
var jj = 0;
//Set applicant details to template 
function setApplicantDetails(ad) {

	var OrgID = document.frmApplicationDetailInfo.OrgID.value;
	var ApplicationID = ad.ApplicationID;
	var RequestID = ad.RequestID;
	
	$('#ad_application_id').html(ad.ApplicationID);

	document.frmApplicationDetailInfo.FirstName.value = ad.first;
	document.frmApplicationDetailInfo.LastName.value = ad.last;
	
	$('#ad_email').html(ad.email);
	$('#ad_app_email').attr('href', 'mailto:'+ad.email)
	document.frmApplicationDetailInfo.Email.value = ad.email;
	
	if(ad.ApplicantPicture != "" && typeof(ad.ApplicantPicture) != "undefined") {
		$("#applicant_picture").attr('src', irecruit_home+"vault/"+OrgID+"/applicant_picture/"+ad.ApplicantPicture);
	}
	else {
		$("#applicant_picture").attr('src', irecruit_home+"images/no-intern.jpg");
	}
	
	$('#ad_city').html(ad.city);
	$('#ad_address').html(ad.address);
	
	if(ad.country == 'CA') {
		if(typeof(ad.province) != 'undefined' && typeof(ad.province) != null) 
		{
			$('#ad_state').html(ad.province);
		}
		else {
			$('#ad_state').html('');
		}
	}
	else {
		if((typeof(ad.state) != 'undefined' 
			&& typeof(ad.state) != null)) 
		{
			$('#ad_state').html(ad.state);
		}
		else {
			$('#ad_state').html('');
		}
	}

	$('#ad_country').html(ad.country);
	$('#ad_zip').html(ad.zip);

	if(ad.display_download_link == "True") {
		$("#download_applicant_documents").show();
		$("#download_applicant_documents").attr('href', 'getApplicantDocumentsZip.php?ApplicationID='+ApplicationID+'&RequestID='+RequestID);
	}
	else {
		$("#download_applicant_documents").hide();
		$("#download_applicant_documents").attr('href', 'javascript:void(0);');
	}
	
	var score = document.frmApplicationDetailInfo.WeightedSearchScore.value;
	var WSStatus = '';
	if(typeof(document.frmApplicationDetailInfo.WSStatus) != 'undefined') {
		WSStatus = document.frmApplicationDetailInfo.WSStatus.value;
	}
	
	if(typeof(WSStatus) != 'undefined' && WSStatus != "") {
		$('#ad_app_score').text('Applicant Score: '+score);
		$('#ad_app_score').attr('onclick', 'javascript:window.open("'+irecruit_home+'applicants/weightedSearchScoreBreakUp.php?WSList='+WSStatus+'&AppId='+ad.ApplicationID+'&OrgID='+ad.OrgID+'","_blank","location=yes,toolbar=no,height=600,width=800,status=no,menubar=yes,resizable=yes,scrollbars=yes")');
	}
	else {
		$('#ad_app_score').text('');
	}

	$('#ad_homephone').html(ad.homephone + ' (Home Phone)');
	$('#ad_cellphone').html(ad.cellphone + ' (Cell Phone)');

	
	if(ad.source == "" || typeof(ad.source) == 'undefined') {
		$('#ad_source').html('');
	}
	else {
		$('#ad_source').html('Source: '+ad.source);
	}
	
	if(ad.informed == "" || typeof(ad.informed) == 'undefined') {
		$('#ad_ref_source').html('');
	}
	else {
		$('#ad_ref_source').html('Referral Source: '+ad.informed);
	}
	
	$('#ad_status').html(ad.status);
	$('#ad_disposition').html(ad.disposition);
	$('#ad_lasteffdate').html(ad.LastEffDate);
	
	$('#ad_application_date').html(ad.ApplicationDate);
	
	if(ad.ReceivedDate == '0000-00-00') {
		$('#ad_received_date').html('');
	}
	else {
		$('#ad_received_date').html('Received Date: '+ad.ReceivedDate);
	}
	
	$('#ad_requestids').html(ad.requestids);
	$('#ad_jobtitle').html(ad.jobtitle);
	$('#ad_req_owner_name').html(ad.reqownersname);
	
	if(ad.linkedin == "") {
		$('#ad_linkedin_profile').text('');
		$('#ad_linkedin_profile').attr('href', ad.linkedin);
	}
	else {
		$('#ad_linkedin_profile').text('Linkedin Profile');
		$('#ad_linkedin_profile').attr('href', ad.linkedin);
	}

	/**
	 * @returns
	 */
	if(ad.AAActive == "N") {
		$("#tab_affirmative_action").hide();
	}
	else {
		$("#tab_affirmative_action").show();
	}
	
	if(ad.VETActive == "N") {
		$("#tab_veteran_status").hide();
	}
	else {
		$("#tab_veteran_status").show();
	}
	
	if(ad.DISActive == "N") {
		$("#tab_disability_status").hide();
	}
	else {
		$("#tab_disability_status").show();
	}

	$(".app_details_attachments").each(function() {
		
		if(this.id == 'ad_view_coverletter') {
			if(typeof(ad.attachments.coverletterupload) != 'undefined' && ad.attachments.coverletterupload['Link'] == "Not Submitted") {
				$(this).html(ad.attachments.coverletterupload['AttachmentType'] + ": Not Submitted");
				$(this).removeAttr('onclick');
				$(this).css({'color':'black', 'cursor':'default'});
			}
			if(typeof(ad.attachments.coverletterupload) != 'undefined' && ad.attachments.coverletterupload['Link'] != "Not Submitted") {
				$(this).html(ad.attachments.coverletterupload['AttachmentType'] + ' <img border="0" src="'+irecruit_home+'images/icons/page_white_magnify.png" style="margin-left: 0px; margin-right: 3px; margin-top: -4px" title="View"/>');
				$(this).attr('onclick', 'location.href="'+irecruit_home + ad.attachments.coverletterupload['Link']+'"');
				$(this).css({'color':'#428bca', 'cursor':'pointer'});
			}
		}
		
		if(this.id == 'ad_view_resume') {
			if(typeof(ad.attachments.resumeupload) != 'undefined' && ad.attachments.resumeupload['Link'] == "Not Submitted") {
				$(this).html(ad.attachments.resumeupload['AttachmentType'] + ": Not Submitted");
				$(this).removeAttr('onclick');
				$(this).css({'color':'black', 'cursor':'default'});
			}
			if(typeof(ad.attachments.resumeupload) != 'undefined' && ad.attachments.resumeupload['Link'] != "Not Submitted") {
				$(this).html(ad.attachments.resumeupload['AttachmentType'] + ' <img border="0" src="'+irecruit_home+'images/icons/page_white_magnify.png" style="margin-left: 0px; margin-right: 3px; margin-top: -4px" title="View"/>');
				$(this).attr('onclick', 'location.href="'+irecruit_home + ad.attachments.resumeupload['Link']+'"');
				$(this).css({'color':'#428bca', 'cursor':'pointer'});
			}
		}
		
		if(this.id == 'ad_view_otherattachments') {
			if(typeof(ad.attachments.otherupload) == "undefined") {
				$(this).html("");
				$(this).removeAttr('onclick');
				$(this).css({'color':'black', 'cursor':'default'});
			}
			else {
				if(typeof(ad.attachments.otherupload) != "undefined" && ad.attachments.otherupload['Link'] == "Not Submitted") {
					$(this).html(ad.attachments.otherupload['AttachmentType'] + ": Not Submitted");
					$(this).removeAttr('onclick');
					$(this).css({'color':'black', 'cursor':'default'});
				}
				if(typeof(ad.attachments.otherupload) != "undefined" && ad.attachments.otherupload['Link'] != "Not Submitted") {
					$(this).html(ad.attachments.otherupload['AttachmentType'] + ' <img border="0" src="'+irecruit_home+'images/icons/page_white_magnify.png" style="margin-left: 0px; margin-right: 3px; margin-top: -4px" title="View"/>');
					$(this).attr('onclick', 'location.href="'+irecruit_home + ad.attachments.otherupload['Link']+'"');
					$(this).css({'color':'#428bca', 'cursor':'pointer'});
				}
			}
		}
	});
	
	//Set social media icons profile links
	$(".social_media").each(function() {
		if(this.id == 'soc_linkedin' || this.id == 'nav_soc_linkedin') {
			if(typeof(ad.soc_linkedin) == 'undefined') {
				$(this).attr('href', 'javascript:void(0);');
				$(this).attr('style', 'color:#bdc1c9;cursor: default;');
				$(this).removeAttr('target');
			}
			if(typeof(ad.soc_linkedin) != 'undefined' && ad.soc_linkedin != "") {
				var soc_link = ad.soc_linkedin;
				if (soc_link.indexOf('http://') === -1 && soc_link.indexOf('https://') === -1) {
					soc_link = 'https://' + soc_link;
				}	
				$(this).attr('href', soc_link);
				$(this).attr('style', 'color:'+nav_color);
				$(this).attr('target', '_blank');
			}
		}
		
		if(this.id == 'soc_facebook' || this.id == 'nav_soc_facebook') {
			if(typeof(ad.soc_facebook) == 'undefined') {
				$(this).attr('href', 'javascript:void(0);');
				$(this).attr('style', 'color:#bdc1c9;cursor: default;');
				$(this).removeAttr('target');
			}
			if(typeof(ad.soc_facebook) != 'undefined' && ad.soc_facebook != "") {
				var soc_link = ad.soc_facebook;
				if (soc_link.indexOf('http://') === -1 && soc_link.indexOf('https://') === -1) {
					soc_link = 'https://' + soc_link;
				}	
				$(this).attr('href', soc_link);
				$(this).attr('style', 'color:'+nav_color);
				$(this).attr('target', '_blank');
			} 
		}
		
		if(this.id == 'soc_twitter' || this.id == 'nav_soc_twitter') {
			if(typeof(ad.soc_twitter) == 'undefined') {
				$(this).attr('href', 'javascript:void(0);');
				$(this).attr('style', 'color:#bdc1c9;cursor: default;');
				$(this).removeAttr('target');
			}
			if(typeof(ad.soc_twitter) != 'undefined' && ad.soc_twitter != "") {
				var soc_link = ad.soc_twitter;
				if (soc_link.indexOf('http://') === -1 && soc_link.indexOf('https://') === -1) {
					soc_link = 'https://' + soc_link;
				}	
				$(this).attr('href', soc_link);
				$(this).attr('style', 'color:'+nav_color);
				$(this).attr('target', '_blank');
			} 
		}
		
		if(this.id == 'soc_googleplus' || this.id == 'nav_soc_googleplus') {
			if(typeof(ad.soc_google) == 'undefined') {
				$(this).attr('href', 'javascript:void(0);');
				$(this).attr('style', 'color:#bdc1c9;cursor: default;');
				$(this).removeAttr('target');
			}
			if(typeof(ad.soc_google) != 'undefined' && ad.soc_google != "") {
				var soc_link = ad.soc_google;
				if (soc_link.indexOf('http://') === -1 && soc_link.indexOf('https://') === -1) {
					soc_link = 'https://' + soc_link;
				}
				$(this).attr('href', soc_link);
				$(this).attr('style', 'color:'+nav_color);
				$(this).attr('target', '_blank');
			}
		}
		
		if(this.id == 'soc_instagram' || this.id == 'nav_soc_instagram') {
			if(typeof(ad.soc_instagram) == 'undefined') {
				$(this).attr('href', 'javascript:void(0);');
				$(this).attr('style', 'color:#bdc1c9;cursor: default;');
				$(this).removeAttr('target');
			}
			if(typeof(ad.soc_instagram) != 'undefined' && ad.soc_instagram != "") {
				var soc_link = ad.soc_instagram;
				if (soc_link.indexOf('http://') === -1 && soc_link.indexOf('https://') === -1) {
					soc_link = 'https://' + soc_link;
				}	
				$(this).attr('href', soc_link);
				$(this).attr('style', 'color:'+nav_color);
				$(this).attr('target', '_blank');
			} 
		}
		
	});
	
}

//Set attachment data to content area
function setAttachmentDetails(ad) {
	$('#applicants-attachments-info').html(ad);
}

//Get attachment details
function getAttachmentDetails() {
	var org_id = document.frmApplicationDetailInfo.OrgID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	
	var input_data = {"ApplicationID":app_id, "RequestID":req_id, "OrgID":org_id};
	send_request('applicants/getApplicantAttachments.php', input_data);
}

//Set applicant history to content area
function setApplicantHistory(ad) {
	$('#applicant-history').html(ad);
}

//Set applicant history to content area
function setChecklist(ad) {
	$('#checklist').html(ad);
}

//Get applicant history
function getApplicantHistory(app_id, req_id) {
	data = {"ApplicationID":app_id, "RequestID":req_id};
	send_request('applicants/getApplicantHistory.php', data);
}

//Get Checklist
function getChecklist(app_id, req_id) {
	data = {"ApplicationID":app_id, "RequestID":req_id};
	send_request('applicants/getApplicantChecklist.php', data);
}

function getSelectedTabInfo(selected_tab, app_id, org_id, req_id, email, first, last, weighted_score, applicant_sort_name) {
	
	$(".tab-content-tabs > li").removeAttr('class');
	$('a[href="'+selected_tab+'"]').closest('li').addClass('active');
	
	if(selected_tab == '#applicant-details') {
		getApplicantDetails(app_id, req_id, weighted_score, applicant_sort_name, email);
	}
	if(selected_tab == '#applicant-score-info') {
		setApplicantScoreInfo();
	}
	if(selected_tab == '#affirmative-action') {
		viewAffirmativeActionInfo("aa");
	}
	if(selected_tab == '#veteran-status') {
		viewAffirmativeActionInfo("veteran");
	}
	if(selected_tab == '#disability-status') {
		viewAffirmativeActionInfo("disabled");
	}	
	if(selected_tab == '#view-application') {
		getApplicationView(org_id, app_id, req_id);
	}
	if(selected_tab == '#applicant-attachments') {
		getAttachmentDetails();
		$("#attachments_info").html("");
	}
	if(selected_tab == '#applicant-history') {
		getApplicantHistory(app_id, req_id);
	}
	if(selected_tab == '#checklist') {
		getChecklist(app_id, req_id);
	}
	if(selected_tab == '#update-applicant-status') {
		getUpdateStatusFormInfo();
	}	
	if(selected_tab == '#related-applications') {
		if(email != "") loadXMLDoc(app_id, req_id, email);
	}
	if(selected_tab == '#schedule-interview') {
		setScheduleInterviewFormInfo();
	}
	if(selected_tab == '#forward-applicant-email') {
		getForwardApplicantsFormInfo();
	}
	if(selected_tab == '#edit-application') {
		getApplicantEditForm();
	}
	if(selected_tab == '#contact-applicant') {
		getContactApplicantsFormInfo();
	}
	if(selected_tab == '#assign-requisition') {
		getManageRequisitionFormInfo();
	}
	if(selected_tab == '#onboard-applicant') {
		var onboard_src = urlParam('OnboardSource');
		if(typeof(onboard_src) == 'undefined' || onboard_src == null) onboard_src = '';
		if(onboard_src == '') onboard_src = single_onboard_process;
		
		getOnboardApplicant('', onboard_src);
	}
	if(selected_tab == '#add-to-vault') {
		getApplicantVaultInfo();
	}
	if(selected_tab == '#assign-iconnect-form') {
		getAssignInternalInfo();
	}
	if(selected_tab == '#applicant-resume-html-view') {
		getResumeHtmlView();
	}
	if(selected_tab == '#applicant-notes') {
		getApplicantNotes();
	}
	if(selected_tab == '#lead-generator') {
		getLeadGeneratorForm();
	}
	if(selected_tab == '#portal-user') {
		getUserPortalUserInfo();
	}
	if(selected_tab == '#twilio-sms') {
		getApplicantTwilioConversation();
	}
	if(selected_tab == '#twilio-sms-archive') {
		getTwilioConversationsArchive();
	}
	if(selected_tab == '#interview-analysis') {
		getInterviewForms(req_id, app_id);
	}
	
}

function getResumeHtmlView() {
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var org_id = document.frmApplicationDetailInfo.OrgID.value;
	$("#applicant-resume-html-view").html('<h5> Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h5>');
	
	var request = $.ajax({
						method: "POST",
				  		url: 'applicants/getResumeHtmlView.php?ApplicationID='+app_id,
						type: "POST",
						success: function(data) {
							var resume_data = '<a href="display_attachment.php?OrgID='+org_id+'&ApplicationID='+app_id+'&Type=resumeupload" style="float:right">Print Resume</a><br>';
							
							if(data == '') {
								resume_data += '<h3 style="text-align:center">Resume Preview Not Available. Go to Applicant Attachments.</h3>';
								$("#applicant-resume-html-view").html(resume_data);
							}
							else {
								resume_data += data;
								$("#applicant-resume-html-view").html(resume_data);
							}
				    	}
					});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function updateAppLabelsList() {
	$("#request_process_status").html("Please wait....");
	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	var app_label = document.frmAppLblList.ddlOrgAppLabelsList.value;
	var request = $.ajax({
		method: "POST",
  		url: 'applicants/updateApplicantLabels.php?ApplicationID='+app_id+'&RequestID='+req_id+"&app_label="+app_label,
		type: "POST",
		success: function(data) {
			$("#request_process_status").html("Filter updated successfully");	
    	}
	});
	
	var active_tab = document.frmApplicationDetailInfo.current_sel_tab.value;
	setAjaxErrorInfo(request, active_tab);
}

function getPrevNextApplicant(nav_type) {

	var curr_app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var curr_req_id = document.frmApplicationDetailInfo.RequestID.value;
	var cur_rec = curr_app_id+"-"+curr_req_id;
	
	if(typeof(applicants_list_info) == 'string') {
		var app_search_res = JSON.parse(applicants_list_info);
		app_search_res_len = app_search_res.length;
	}
	else if(typeof(applicants_list_info) == 'object') {
		var app_search_res = applicants_list_info;
		app_search_res_len = app_search_res.length;
	}
	var j = 0;
	
	if(nav_type == 'next') {
		$("#app_detail_prev").show();
	}
	
	
	for (var i = 0; i < app_search_res_len; i++) {

		if(cur_rec == app_search_res[i].ApplicationID + "-" + app_search_res[i].RequestID) {
			
			if(nav_type == 'previous') {
				j = i - 1;
			}
			if(nav_type == 'next') {
				j = i+1;
			}

			var ApplicationID = app_search_res[j].ApplicationID;
			var ApplicantSortName = app_search_res[j].ApplicantSortName;
			var ApplicantEmail = app_search_res[j].ApplicantEmail;
			var DispositionCode = app_search_res[j].DispositionCode;
			var EntryDate = app_search_res[j].EntryDate;
			var MultiOrgID = app_search_res[j].MultiOrgID;
			var ProcessOrder = app_search_res[j].ProcessOrder;
			var ReceivedDate = app_search_res[j].ReceivedDate;
			var RequestID = app_search_res[j].RequestID;
			var StatusEffectiveDate = app_search_res[j].StatusEffectiveDate;
			var WeightedSearchScore = app_search_res[j].WeightedSearchScore;

			var OrganizationName = app_search_res[j].OrganizationName;
			var RequisitionJobID = app_search_res[j].RequisitionJobID;
			var StatusDescription = app_search_res[j].StatusDescription;
			var DispositionDescription = app_search_res[j].DispositionDescription;
			if(typeof(DispositionDescription) == 'undefined') DispositionDescription = '';
			if(typeof(WeightedSearchScore) == 'undefined') WeightedSearchScore = '';
			
			getApplicantDetails(ApplicationID, RequestID, WeightedSearchScore, ApplicantSortName, ApplicantEmail);

			i = app_search_res_len;
		}

	}	
}

function setTab(tab_name) {
	var src_href		=	tab_name;
    var href_tab_info	=	src_href.split("#");
    var content_tab		=	href_tab_info[0];
	var selected_tab	=	"#"+content_tab;

	var app_id			=	document.frmApplicationDetailInfo.ApplicationID.value;
	var org_id			=	document.frmApplicationDetailInfo.OrgID.value;
	var req_id			=	document.frmApplicationDetailInfo.RequestID.value;
	var email			=	document.frmApplicationDetailInfo.Email.value;
	var first			=	document.frmApplicationDetailInfo.FirstName.value;
	var last			=	document.frmApplicationDetailInfo.LastName.value;
	var weighted_score	=	document.frmApplicationDetailInfo.WeightedSearchScore.value;
	var app_sort_name	=	document.frmApplicationDetailInfo.ApplicantSortName.value; 
		
    $('.ra-tab-content').hide();
 	$('#'+content_tab).show();

 	document.frmApplicationDetailInfo.current_sel_tab.value = selected_tab;
 	
 	getSelectedTabInfo(selected_tab, app_id, org_id, req_id, email, first, last, weighted_score, app_sort_name);
}

$(document).ready(function() {
	$('body').tooltip({
	    selector: '[data-toggle="tooltip"]'
	});

	var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
	var org_id = document.frmApplicationDetailInfo.OrgID.value;
	var req_id = document.frmApplicationDetailInfo.RequestID.value;
	var email = document.frmApplicationDetailInfo.Email.value;
	var first = document.frmApplicationDetailInfo.FirstName.value;
	var last = document.frmApplicationDetailInfo.LastName.value;
	var weighted_score = document.frmApplicationDetailInfo.WeightedSearchScore.value;
	var applicant_sort_name = document.frmApplicationDetailInfo.ApplicantSortName.value; 

	
	$(".vertical-text").click(function() {
		
		var button_id = $(this).attr('id');
		
		if(button_id == 'vertical-text-full-view-results') {
			$("#full-view-navigation-bar").show();
			
			document.getElementById('search-results-detail').style.display = 'block';
			document.getElementById('applicant-detail-view').style.display = 'none';
			document.getElementById('applicants-minified-search-results').style.display = 'none';
			document.getElementById('applicant-detail-info').style.display = 'none';
			document.getElementById('search-form').style.display = 'none';
		}
		
		if(button_id == 'vertical-text-detail-view-results') {
			$("#full-view-navigation-bar").hide();
			
			document.getElementById('search-results-detail').style.display = 'none';
			document.getElementById('applicant-detail-view').style.display = 'block';
			document.getElementById('applicants-minified-search-results').style.display = 'block';
			document.getElementById('applicant-detail-info').style.display = 'block';
			document.getElementById('search-form').style.display = 'none';
		}
		
		if(button_id == 'vertical-text-detail-view-modify-search'
			|| button_id == 'vertical-text-full-view-modify-search') {
			$("#full-view-navigation-bar").hide();
			document.getElementById('search-results-detail').style.display = 'none';
			document.getElementById('applicant-detail-view').style.display = 'none';
			document.getElementById('applicants-minified-search-results').style.display = 'none';
			document.getElementById('applicant-detail-info').style.display = 'none';
			document.getElementById('search-form').style.display = 'block';
		}
	});
	
	$(".ThumbsFilter").click(function() {

		var ThumbsId = $(this).attr('id');
		var AppConsiderStatus = '';
		var ThumbsColor = $("#"+ThumbsId).css('color');
		
		if(ThumbsId == "AppConsiderStatusThumbsUpFilter") {
			if(ThumbsColor == "rgb(186, 191, 198)" || ThumbsColor == nav_color) {
				AppConsiderStatus = 'ThumbsUp';
			}
		}
		if(ThumbsId == "AppConsiderStatusThumbsDownFilter") {
			if(ThumbsColor == "rgb(186, 191, 198)" || ThumbsColor == nav_color) {
				AppConsiderStatus = 'ThumbsDown';
			}
		}

		if(AppConsiderStatus == '') {
			$("#AppConsiderStatusThumbsUpFilter").css({'color':'#babfc6'});
			$("#AppConsiderStatusThumbsDownFilter").css({'color':'#babfc6'});
			$("#AppConsiderStatusThumbsClearFilter").css({'color':'#babfc6'});
		}
		if(AppConsiderStatus == 'ThumbsUp') {
			$("#AppConsiderStatusThumbsUpFilter").css({'color':nav_color});
			$("#AppConsiderStatusThumbsDownFilter").css({'color':'#babfc6'});
			$("#AppConsiderStatusThumbsClearFilter").css({'color':nav_color});
		}
		if(AppConsiderStatus == 'ThumbsDown') {
			$("#AppConsiderStatusThumbsUpFilter").css({'color':'#babfc6'});
			$("#AppConsiderStatusThumbsDownFilter").css({'color':nav_color});
			$("#AppConsiderStatusThumbsClearFilter").css({'color':nav_color});
		}
		
		document.getElementById('ddlIrecruitAppFilter').value = AppConsiderStatus;
		getApplicantsByFilter();
	});

	$(".ThumbsConsiderationStatus").on('click', function() {
		
		var ThumbsId = $(this).attr('id');
		var ConsiderationStatus = '';
		var ThumbsColor = $("#"+ThumbsId).css('color');
		
		if(ThumbsId == "AppConsiderStatusThumbsUp") {
			if(ThumbsColor == "rgb(186, 191, 198)" || ThumbsColor == nav_color) {
				ConsiderationStatus = 'ThumbsUp';
			}
		}
		if(ThumbsId == "AppConsiderStatusThumbsDown") {
			if(ThumbsColor == "rgb(186, 191, 198)" || ThumbsColor == nav_color) {
				ConsiderationStatus = 'ThumbsDown';
			}
		}
		
		var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
		var req_id = document.frmApplicationDetailInfo.RequestID.value;

		
		setThumbsColor(ConsiderationStatus);
		
		var request = $.ajax({
						method: "POST",
				  		url: 'applicants/getApplicantConsiderationStatus.php?ApplicationID='+app_id+'&RequestID='+req_id+'&action=updappconsiderationstatus&ConsiderationStatus='+ConsiderationStatus,
						type: "POST",
						success: function(data) {
							setThumbsColor(data);
				    	}
					});
		
		setAjaxErrorInfo(request, "#app_header_process_msg");
		
	});
	
    $('.applicant-tabs').click(function(event) {
    	
		if($(this).prop('tagName') == "A")
        {
	    	document.getElementById('ddlApplicantTabs').selectedIndex = 0;

			event.preventDefault();
	        var src_href = this.href;
	        var href_tab_info = src_href.split("#");
	        var content_tab = href_tab_info[1];
			var selected_tab = "#"+content_tab;
			
			var app_id				=	document.frmApplicationDetailInfo.ApplicationID.value;
			var org_id				=	document.frmApplicationDetailInfo.OrgID.value;
			var req_id				=	document.frmApplicationDetailInfo.RequestID.value;
			var email				=	document.frmApplicationDetailInfo.Email.value;
			var first				=	document.frmApplicationDetailInfo.FirstName.value;
			var last				=	document.frmApplicationDetailInfo.LastName.value;
			var weighted_score		=	document.frmApplicationDetailInfo.WeightedSearchScore.value;
			var applicant_sort_name =	document.frmApplicationDetailInfo.ApplicantSortName.value; 
			
	        $('.ra-tab-content').hide();
	    	$('#'+content_tab).show();

	    	document.frmApplicationDetailInfo.current_sel_tab.value = selected_tab;
		    	
	    	getSelectedTabInfo(selected_tab, app_id, org_id, req_id, email, first, last, weighted_score, applicant_sort_name);
		}
    });


    $('.applicant-tabs').change(function() {
		
        var src_href = this.value;
        var href_tab_info = src_href.split("#");
        var content_tab = href_tab_info[1];
		var selected_tab = "#"+content_tab;
		
		var app_id = document.frmApplicationDetailInfo.ApplicationID.value;
		var org_id = document.frmApplicationDetailInfo.OrgID.value;
		var req_id = document.frmApplicationDetailInfo.RequestID.value;
		var email = document.frmApplicationDetailInfo.Email.value;
		var first = document.frmApplicationDetailInfo.FirstName.value;
		var last = document.frmApplicationDetailInfo.LastName.value;
		var weighted_score = document.frmApplicationDetailInfo.WeightedSearchScore.value;
		var applicant_sort_name = document.frmApplicationDetailInfo.ApplicantSortName.value; 
		
        $('.ra-tab-content').hide();
    	$('#'+content_tab).show();

    	document.frmApplicationDetailInfo.current_sel_tab.value = selected_tab;
	    	
    	getSelectedTabInfo(selected_tab, app_id, org_id, req_id, email, first, last, weighted_score, applicant_sort_name);

    });

    if((app_id != '' && req_id != '')) {
    	getApplicantDetails(app_id, req_id, weighted_score, applicant_sort_name, email);
	}
    
    fillStarsByRating("applicant_rating", "ApplicantRating");
});


function formatAddress(OrgID, Country, City, State, Province, Zip, County) {
	address = '';

	if (Country == "") {
		Country = "US";
	}

	if (OrgID !=  "" && City != "") {

		if (Country == "CA") {

			address = City+", "+Province+"&nbsp;&nbsp;"+Zip;
			
			if (County != "") {
				address += "&nbsp;&nbsp;"+County;
			}
		}
		else if (Country == "US") {

			address = City+", "+State+"&nbsp;&nbsp;"+Zip;
			
			if (County != "") {
				address += "&nbsp;&nbsp;"+County;
			}
		}
		else {

			address = City+"&nbsp;&nbsp;"+Zip;
		}
	} // end items

	return address;
} // end function

function processLeadGeneratorApplication(frmObj) {

	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	var Email			=	document.frmApplicationDetailInfo.Email.value;
	var FormID			=	frmObj.FormID.value;
	
	$.ajax({
		method: "POST",
  		url: "applicants/processLeadGeneratorApplication.php?RequestID="+RequestID+"&ApplicationID="+ApplicationID+"&Email="+Email+"&FormID="+FormID,
		type: "POST",
		beforeSend: function() {
			$("#process_lead_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
			$("#btnProcessLeadApplication").attr('disabled', 'disabled');
		},
		success: function(data) {
			$("#process_lead_message").html('&nbsp;Successfully processed');
			$("#btnProcessLeadApplication").attr('disabled', 'disabled');
    	}
	});

}

function sendCompleteApplicationReminder(frmObj) {

	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	var Email			=	document.frmApplicationDetailInfo.Email.value;
	var FormID			=	frmObj.FormID.value;
	
	$.ajax({
		method: "POST",
  		url:	"applicants/sendCompleteApplicationReminder.php?RequestID="+RequestID+"&ApplicationID="+ApplicationID+"&Email="+Email+"&FormID="+FormID,
		type:	"POST",
		beforeSend: function() {
			$("#process_lead_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
			$("#btnSendReminder").attr('disabled', 'disabled');
		},
		success: function(data) {
			$("#process_lead_message").html('&nbsp;Reminder successfully sent');
			$("#btnSendReminder").attr('disabled', 'disabled');
    	}
	});

}

function getLeadGeneratorForm() {
	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	var Email			=	document.frmApplicationDetailInfo.Email.value;
	
	$.ajax({
		method: "POST",
  		url: "applicants/getLeadGeneratorForm.php?RequestID="+RequestID+"&ApplicationID="+ApplicationID+"&Email="+Email,
		type: "POST",
		beforeSend: function() {
			$("#request_process_status").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#request_process_status").html('');
			$("#lead-generator").html(data);
    	}
	});

}


function getUserPortalUserInfo() {
	var ApplicationID	=	document.frmApplicationDetailInfo.ApplicationID.value;
	var RequestID		=	document.frmApplicationDetailInfo.RequestID.value;
	var Email			=	document.frmApplicationDetailInfo.Email.value;
	
	$.ajax({
		method: "POST",
  		url: "applicants/getUserPortalUserInfo.php?RequestID="+RequestID+"&ApplicationID="+ApplicationID+"&Email="+Email,
		type: "POST",
		dataType: 'json',
		beforeSend: function() {
			$("#request_process_status").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#request_process_status").html('');
			
			var userportal_info	= '<br><br>';
			
			if(typeof(data.UserID) == 'undefined') {
				userportal_info = '<br><h4>There is no account associated with this email.</h4><br>';
			}
			else {
				userportal_info += "<strong>User Portal ID:</strong> "+data.UserID+'<br>';
				userportal_info += "<strong>Activation Date:</strong> "+data.ActivationDate+'<br>';
				userportal_info += "<strong>Last Access:</strong> "+data.LastAccess+'<br>';
			}
			
			$("#portal-user").html(userportal_info);
    	}
	});
}


function setComparativeLabel() {
	
	var app_id		=	document.frmApplicationDetailInfo.ApplicationID.value;
	var req_id 		=	document.frmApplicationDetailInfo.RequestID.value;
	var label_id	=	document.frmComparativeLabelsList.ddlComparativeLabels.value;
	var label_name 	=	$("#ddlComparativeLabels option[value='"+label_id+"']").text();
	
	var request = $.ajax({
		method: "POST",
  		url: "applicants/setComparativeLabel.php?RequestID="+req_id+"&ApplicationID="+app_id+"&LabelID="+label_id+"&LabelName="+label_name,
		type: "POST",
		beforeSend: function() {
			$("#request_process_status").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#request_process_status").html(data);
			getApplicantHeaderInfo();
    	}
	});

	setAjaxErrorInfo(request, 'request_process_status');
}

function processInterviewForm(process_status) {

	var input_data = $('form[name="InterviewForm"]').serialize();
	input_data += '&process_status=' + process_status;

	const required = JSON.parse(document.getElementById('Required').value);

	var requestid = document.getElementById('RequestID').value;
        var appid = document.getElementById('ApplicationID').value;

	var err = '';

	if (process_status == 'final') {
	required.forEach(function(questionid) {
	  var ques = document.getElementById(Object.keys(questionid));
	  var q = ques.value;

		if(Object.prototype.toString.call(ques) === '[object HTMLInputElement]') {
			var value = $("input[type='radio'][name='"+Object.keys(questionid)+"']:checked").val();
			if (value != '') {
				q=value;
			}
			if (typeof value === 'undefined') {
		          err += ' - ' + Object.values(questionid) + '\n';
			}
		}

	  if (q == '') {
		  err += ' - ' + Object.values(questionid) + '\n';
	  }
	});
	}

	var process = 'N';

	if (err != '') { err = 'The following questions are required:\n\n' + err; alert(err); return false; }

	if ((err == '') && (process_status == 'final')) { 
	 if (confirm('Are you sure that you want to finalize this form:\n\n') == true) { 
		 process = 'Y';
	 }
	}
	if ((err == '') && (process_status == 'temp')) { 
		 process = 'Y';
	}

	if (process == 'Y') {
        var request = $.ajax({
                method: "POST",
                url: irecruit_home + "interview/processInterviewFormData.php",
                type: "POST",
                data: input_data,
                beforeSend: function(){
                        $("#interview-analysis").html('<br>Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
                },
                success: function(data) {
                        //$("#interview-analysis").html(data);
			getInterviewForms(requestid, appid);
			setTimeout(function () { $(message).html('<span style="color:red;font-style:italic;">Successfully Submitted.</span>'); }, 500);

                }
        });
	}

}
