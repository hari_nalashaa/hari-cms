//Flot Pie Chart
//Applications information by distinction
function fun_app_by_distinction(app_by_distinction) {
		var data = app_by_distinction;
	    var plotObj = $.plot($("#applications-by-distinction"), data, {
	        series: {
	            pie: {
	                show: true
	            }
	        },
	        grid: {
	            hoverable: true
	        },
	        tooltip: true,
	        tooltipOpts: {
	            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
	            shifts: {
	                x: 20,
	                y: 0
	            },
	            defaultTheme: false
	        }
	    });
}
	
if(typeof(app_by_distinction) != 'undefined') {
	if (CheckDocumentObject('applications-by-distinction')) fun_app_by_distinction(app_by_distinction);
	/*$(window).resize(function() {
		var app_by_distinction_obj = document.getElementById('applications-by-distinction');
		if (CheckDocumentObject('applications-by-distinction')) fun_app_by_distinction(app_by_distinction);
	});*/
}

//Applications searchable
function fun_app_by_searchable(app_by_searchable) {
	
    var data = app_by_searchable;
    
    var plotObj = $.plot($("#applications-searchable"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });
    
}


if(typeof(app_by_searchable) != 'undefined') {	
	if (CheckDocumentObject('applications-searchable')) fun_app_by_searchable(app_by_searchable);
	/*$(window).resize(function() {
		if (CheckDocumentObject('applications-searchable')) fun_app_by_searchable(app_by_searchable);
	});*/
}

//Applications by processorder and the label is displaying as status in dashboard home page
function fun_app_by_processorder(app_by_processorder) {
	
    var data = app_by_processorder;
    
    var plotObj = $.plot($("#applications-by-processorder"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });
}

if(typeof(app_by_processorder) != 'undefined') {
	if (CheckDocumentObject('applications-by-processorder')) fun_app_by_processorder(app_by_processorder);
	/*$(window).resize(function(){
		if (CheckDocumentObject('applications-by-processorder')) fun_app_by_processorder(app_by_processorder);
	});*/
}

//Applications by disposition code
function fun_app_by_disposition_code(app_by_disposition_code) {
	
	var data = app_by_disposition_code;
	
    var plotObj = $.plot($("#applications-by-dispositioncode"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });

}

if(typeof(app_by_disposition_code) != 'undefined') {	
	if (CheckDocumentObject('applications-by-dispositioncode')) fun_app_by_disposition_code(app_by_disposition_code);
	/*$(window).resize(function(){
		if (CheckDocumentObject('applications-by-dispositioncode')) fun_app_by_disposition_code(app_by_disposition_code);
	});*/
}

//Applications by disposition code
function fun_app_by_source(app_by_source) {
	
	var data = app_by_source;
	
    var plotObj = $.plot($("#applicants_count_by_req_and_source"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
            hoverable: true
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        }
    });

}

if(typeof(app_by_source) != 'undefined') {
	
	if (CheckDocumentObject('applicants_count_by_req_and_source')) {
		fun_app_by_source(app_by_source);
	}
	/*$(window).resize(function(){
		if (CheckDocumentObject('applications-by-dispositioncode')) fun_app_by_disposition_code(app_by_disposition_code);
	});*/
}