if(typeof(hbargraphdata) != "undefined" && typeof(hbargraphyaxis) != "undefined") {
	$(function () {
	    var css_id = "#horibarchartplaceholder";
	    var data = hbargraphdata;
	    var options = {
	        series: {stack: 0,
	        lines: {show: false, steps: false },
	        bars: {show: true, barWidth: 0.3, align: 'center',  horizontal:true},},
	        yaxis: hbargraphyaxis
	    };

	    $.plot($(css_id), data, options);
	});
}
