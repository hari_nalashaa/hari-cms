if(typeof(vbargraphdata) != "undefined" && typeof(vbargraphxaxis) != "undefined" && dashboard_vbargraph == true) {
	$(function () {
	    var data = vbargraphdata;
	    var options = {
	        series: {stack: 0,
	        lines: {show: false, steps: false },
	        bars: {show: true, barWidth: 0.4, align: 'center',}},
	        xaxis: vbargraphxaxis,
	    };

	    $.plot($("#vertbarchartplaceholder"), data, options);
	});
}