function loadCategories(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("cats").innerHTML=xmlhttp.responseText;
    }
  }

var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var k = document.forms['Search'].AccessCode.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadCategories.php?report=" + report + "&criteria=" + criteria + "&new=" + d + "&k=" + k,true);
xmlhttp.send();


} // end function

function loadGroupBy(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("gb").innerHTML=xmlhttp.responseText;
    }
  }

var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var k = document.forms['Search'].AccessCode.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadGroupBy.php?report=" + report + "&criteria=" + criteria + "&new=" + d + "&k=" + k,true);
xmlhttp.send();


} // end function

function loadOrderBy(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("ob").innerHTML=xmlhttp.responseText;
    }
  }

var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var k = document.forms['Search'].AccessCode.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadOrderBy.php?report=" + report + "&criteria=" + criteria + "&new=" + d + "&k=" + k,true);
xmlhttp.send();


} // end function


function loadLimit(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("Active").value=xmlhttp.responseText;
    }
  }

var a = document.forms['Search'].Active.value;
var k = document.forms['Search'].AccessCode.value;
var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET",irecruit_home + "reports_extended/app/loadLimit.php?report=" + report + "&criteria=" + criteria + "&act=" + a + "&new=" + d + "&k=" + k,true);
xmlhttp.send();

} // end function

function loadRefine(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	 
    document.getElementById("RefineReq").value=xmlhttp.responseText;
    }
  }

var a = document.forms['Search'].RefineReq.value;
var k = document.forms['Search'].AccessCode.value;
var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadRefine.php?report=" + report + "&criteria=" + criteria + "&refine=" + a + "&new=" + d + "&k=" + k,true);
xmlhttp.send();

} // end function

function loadRequisitions(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("reqs").innerHTML=xmlhttp.responseText;
    }
  }

var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var k = document.forms['Search'].AccessCode.value;
var n=report.split(":"); 
report=n[0];
var a = document.forms['Search'].Active.value;
var b = document.forms['Search'].MultiOrgID.value;
var c = document.forms['Search'].RefineReq.value;
c=c.replace(" ","%20");

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadRequisitions.php?report=" + report + "&criteria=" + criteria + "&MultiOrgID=" + b + "&Active=" + a + "&RefineReq=" + c + "&new=" + d + "&k=" + k,true);
xmlhttp.send();

} // end function

function loadSavedReports(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("SavedReports").innerHTML=xmlhttp.responseText;
    }
  }

var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var k = document.forms['Search'].AccessCode.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadSavedReports.php?report=" + report + "&criteria=" + criteria + "&new=" + d + "&k=" + k,true);
xmlhttp.send();

} // end function

function loadMultiOrg(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("MultiOrgID").value=xmlhttp.responseText;
    }
  }

var b = document.forms['Search'].MultiOrgID.value;
var k = document.forms['Search'].AccessCode.value;
var d = new Date().getTime(); 
report = document.forms['Search'].report.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadMultiOrg.php?report=" + report + "&criteria=" + criteria + "&MultiOrgID=" + b + "&new=" + d + "&k=" + k,true);
xmlhttp.send();

} // end function

function loadDateRange(criteria) {

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
	  var response = xmlhttp.responseText;
	  if(response != "") {
		  var data = response.split(":");
		  document.getElementById('ReportDate_From').value=data[0];
		  document.getElementById('ReportDate_To').value=data[1];
	  }
    }
  }

var d = new Date().getTime(); 
var report = document.forms['Search'].report.value;
var k = document.forms['Search'].AccessCode.value;
var n=report.split(":"); 
report=n[0];

xmlhttp.open("GET", irecruit_home + "reports_extended/app/loadDateRange.php?report=" + report + "&criteria=" + criteria + "&new=" + d + "&k=" + k,true);
xmlhttp.send();

} // end function

function loadCriteria(c) {

var criteria = document.forms['Search'].criteria.value;

if (c != "") {
criteria=c;
}

loadSavedReports(criteria);
loadMultiOrg(criteria);
loadLimit(criteria);
loadRefine(criteria);
loadRequisitions(criteria);
loadDateRange(criteria);
loadCategories(criteria);
loadGroupBy(criteria);
loadOrderBy(criteria);

} // end function
