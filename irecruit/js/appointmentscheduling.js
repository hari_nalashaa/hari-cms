$(document).ready(function() {
		$('#sidate').datepicker(
		{
				changeMonth: true,
				changeYear: true,
				showButtonPanel: true,
				showOn: 'button',
				buttonImage: irecruit_home+'images/calendar.gif',
				yearRange: '0:+5',
				buttonImageOnly: true,
				buttonText: 'Select Date',
				minDate: 0
		});
});

function validateAppointmentScheduling() {
	var error = false;
	if($("#sidate").val() == "") {
		$("#sidate").css("border", "1px solid red");
		error = true;
	}
	if($("#sisubject").val() == "") {
		$("#sisubject").css("border", "1px solid red");
		error = true;
	}
	if($("#silocation").val() == "") {
		$("#silocation").css("border", "1px solid red");
		error = true;
	}
	if($("#sidescription").val() == "") {
		$("#sidescription").css("border", "1px solid red");
		error = true;
	}
	if(error == true) {
		var sche_error_message = '<span style="color:red">Please fill the mandatory fields to schedule an appointment.</span>';
		$("#si_loading_top").html(sche_error_message);
		$("#si_loading_bottom").html(sche_error_message);
		return false;
	}
	
	return true;
}

function modifyEmailBody(callback_url, email_template) {
	var req_id = document.frmScheduleInterview.RequestID.value;	
	
	var request = $.ajax({
		method: "POST",
  		url: callback_url, 
		type: "POST",
		data: {"RequestID":req_id, "emailbodytemplate":email_template},
		dataType: 'json',
		success: function(app_email_template) {
			var editor = tinyMCE.get('sidescription');
			editor.setContent(app_email_template.message);
			$("#sidescription").text(app_email_template.message);
			$("#viewmessage").html(app_email_template.viewmessage);
    	}
	});

}

function scheduleAppointment(callback_url) {
	
	if(validateAppointmentScheduling()) {
		$("#si_loading_top").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...');
		$("#si_loading_bottom").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...');
		
		var form = $('#frmScheduleInterview');
		
		var request = $.ajax({
			method: "POST",
	  		url: callback_url, 
			type: "POST",
			data: form.serialize(),
			dataType: 'json',
			success: function(data) {
				
				$("#sidate").val('');
				$("#sisubject").val('');
				$("#silocation").val('');
				
				$("#sidate").removeAttr('style');
				$("#sidate").css({"width":"100px", "float":"left"});
				$("#sisubject").removeAttr('style');
				$("#silocation").removeAttr('style');

				
				$('#ddlLocation option[value=""]').attr("selected", "selected");
				$('#sistatus option[value=""]').attr("selected", "selected");
				$('#siispositioncode option[value=""]').attr("selected", "selected");
				$('#sihrs option[value="01"]').attr("selected", "selected");
				$('#simins option[value="00"]').attr("selected", "selected");
				$('#siampm option[value="am"]').attr("selected", "selected");
				
				console.log(data.appid);
				if(typeof(data.appid) != "undefined" && data.appid != "") {
					var sche_message = '<span style="color:#428bca">&nbsp;Appointment scheduled successfully.';
					if(data.defcal != "") {
						sche_message += '&nbsp;<a href="'+data.event_url+'" style="text-decoration:underline">Add this appointment to your calendar</a>';
					}
					sche_message += '</span>';
					$("#si_loading_top").html(sche_message);
					$("#si_loading_bottom").html(sche_message);
				}
				else {
					$("#si_loading_top").html('<span style="color:red">Sorry there is an issue while scheduling the appointment</span>');
					$("#si_loading_bottom").html('<span style="color:red">Sorry there is an issue while scheduling the appointment</span>');
				}
	    	}
		});
	}
}

function set_month_year(selected_date) {
	var sd = selected_date.split("-");
	var Year = sd[0];
	var Month = sd[1];
	
	document.getElementById('Year').value = Year;
	document.getElementById('Month').value = Month;
}

function loadAppointmentsCalendar(View, SIDate) {
	$("#CalendarDayView").html('<br><br><br><br><br><br><img src="'+irecruit_home+'images/loading.gif"><br><br>Loading...Please wait..');
	$.ajax({
		type: "GET",
		url: irecruit_home+"appointments/calendar.php?View="+View+"&SIDate="+SIDate,
		success: function(data) {
			$("#CalendarDayView").html(data);
			ChangeViewColor(0);
		}
	});
}

function preloader() 
{
     // counter
     var i = 0;
     // create object
     imageObj = new Image();

     // set image list
     images = new Array();
     images[0] = irecruit_home+'/images/loading.gif';

     imageObj.src=images[i];
} 
preloader();