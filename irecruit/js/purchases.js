function getBasketInfo(k) {
	
	var request_url = "shopping/basket.php?k="+k+'&cpage=purchases';

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "GET",
		beforeSend: function(){
			$("#basket_payment_info").html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
		},
		success: function(data) {
			$("#basket_payment_info").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#basket_payment_info");
}

function getInvoiceForm(k) {
	
	var request_url = "shopping/generalPurchase.php?k="+k;

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		beforeSend: function(){
			$("#basket_payment_info").html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
		},
		success: function(data) {
			$("#basket_payment_info").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#basket_payment_info");
}

function createInvoice(btn_obj, k) {
	
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var request_url = "shopping/generalPurchase.php?k="+k;

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#basket_payment_info").html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
		},
		success: function(data) {
			getBasketInfo(k);
    	}
	});
	
	setAjaxErrorInfo(request, "#basket_payment_info");
}


function getPurchases(btn_obj) {
	var form_obj = $(btn_obj).parents('form:first');
	var input_data = $(form_obj).serialize();

	var request_url = "administration/getPurchases.php";

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: input_data,
		beforeSend: function(){
			$("#basket_payment_info").html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
		},
		success: function(data) {
			$("#basket_payment_info").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#basket_payment_info");
}


function viewPurchasesInfo() {

	var txtPurchaseFromDate = document.getElementById('txtPurchaseFromDate').value;
	var txtPurchaseToDate = document.getElementById('txtPurchaseToDate').value;
	
	var request_url = "administration/getPurchases.php?";

	var request = $.ajax({
		method: "POST",
  		url: request_url,
		type: "POST",
		data: {'txtPurchaseFromDate':txtPurchaseFromDate, 'txtPurchaseToDate':txtPurchaseToDate},
		beforeSend: function(){
			$("#basket_payment_info").html('<h4 style="text-align:center">Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/></h4>');
		},
		success: function(data) {
			$("#basket_payment_info").html(data);
    	}
	});
	
	setAjaxErrorInfo(request, "#basket_payment_info");
}

function displayPurchaseInfo(id) {
	var content_id = 'content-' + id;
	
	$(".purchase_content").hide();
	
	$("#"+content_id).show('slow');
}