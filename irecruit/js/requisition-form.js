function copyRequisitionForm() {
	var confirm_copy = confirm("Are you sure you want to make a copy of this form and continue? You cannot edit the old form.");
	if (confirm_copy == true) {
		document.getElementById('rdCopyForm').value = 'Yes';
		document.forms['frmRequisitionForm'].submit();
	}
}