function getApplicantsByRequestID() {
	var from_date 	= document.getElementById('application_from_date').value;
	var to_date 	= document.getElementById('application_to_date').value;
	var RequestID 	= document.getElementById('ddlRequisitions').value;
	
	var REQUEST_URL = "getApplicantsByRequestID.php?FromDate="+from_date+"&ToDate="+to_date;
	
	if(RequestID != "") REQUEST_URL += "&RequestID="+RequestID;
	$.ajax({
		method: "POST",
  		url:	REQUEST_URL,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#applicants_results").find("tr:gt(0)").remove();
			app_full_view_results	 = "<tr>";
			app_full_view_results	+= "<td colspan='6' align='center'>Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/><br><br></td>";
			app_full_view_results	+= "</tr>";
			
			$("#applicants_results").append(app_full_view_results);
		},
		success: function(data) {
			
			$("#applicants_results").find("tr:gt(0)").remove();
			
			var app_full_view_results = '';
			var data_length = data['applicants_list'].length;
			var applicants_list = data['applicants_list'];
			
			if(data_length > 0) {
				
				for (var i = 0; i < data_length; i++) {
					
					var ApplicationID 		=	applicants_list[i].ApplicationID;
					var ApplicantSortName 	=	applicants_list[i].ApplicantSortName;
					var ApplicantEmail 		=	applicants_list[i].Email;
					var EntryDate 			=	applicants_list[i].EntryDate;
					var ProcessOrder 		=	applicants_list[i].ProcessOrder;
					var RequestID 			=	applicants_list[i].RequestID;

					var app_request_id		=	ApplicationID+":"+RequestID;
					app_full_view_results += "<tr>";
					app_full_view_results += "<td><input type='checkbox' name='appids_list[]' id='app_req_id_"+app_request_id+"' class='applicants_list' value='"+app_request_id+"'></td>";
					app_full_view_results += "<td valign='top'><a href='"+irecruit_home+"'applicantsSearch.php?ApplicationID="+ApplicationID+"&RequestID="+RequestID+"' target='_blank'>"+ApplicationID+"</a></td>";
					app_full_view_results += "<td valign='top'><a href='mailto:"+ApplicantEmail+"' target='_blank'>"+ApplicantSortName+"</a></td>";
					app_full_view_results += "<td valign='top'>"+ProcessOrder+"</td>";
					app_full_view_results += "<td valign='top'>"+ApplicantEmail+"</td>";
					app_full_view_results += "<td valign='top'>"+EntryDate+"</td>";
					app_full_view_results += "</tr>";
				}
				
			}
			else {

				app_full_view_results += "<tr>";
				app_full_view_results += "<td colspan='6' align='center'>No records found</td>";
				app_full_view_results += "</tr>";

			}
			
			$("#applicants_results").append(app_full_view_results);
    	}
	});
}

function getRequisitionsByStatus(RequisitionStatus) {
	
	$('#ddlRequisitions option').each(function() {
		$(this).remove();
	});
	
	$.ajax({
		method: "POST",
  		url: 	irecruit_home + "requisitions/getRequisitions.php?RequisitionStatus="+RequisitionStatus,
		type: 	"POST",
		dataType: 'json',
		beforeSend: function() {
			$("#refresh_requisitions_list").html("Please wait.. <img src='"+irecruit_home+"images/icons/loading-small.gif'/>");
		},
		success: function(data) {
			var options_list = '<option value="">Select</option>';
			
			for(var j = 0; j < data.length; j++) {
				var option_text		=	data[j].Title;
				option_text		+= " - " + data[j].RequisitionID + " / " + data[j].JobID;	
				var option_value	=	data[j].RequestID;
				
				options_list	+=	'<option value="'+option_value+'">'+option_text+'</option>';
			}
			
			$("#refresh_requisitions_list").html("");
			$("#ddlRequisitions").append(options_list);
    	}
	});
}


function createComparativeLabels(LabelName) {
	
	if(LabelName != "") {
		$("#lbl_creation_deletion_status_msg").css({'color':nav_color}).html("Please wait..");
		$('#label_id option').each(function() {
			$(this).remove();
		});
		
		$('#ddlDelLabels option').each(function() {
			$(this).remove();
		});
		
		$.ajax({
			method: "POST",
	  		url: 	irecruit_home + "applicants/createComparativeLabel.php?LabelName="+LabelName,
			type: 	"POST",
			dataType: 'json',
			success: function(data) {
				var options_list = '<option value="">Select</option>';
				
				for(var j = 0; j < data.length; j++) {
					var OptionValue	=	data[j]['LabelID'];
					var OptionText	=	data[j]['LabelName'];
					options_list	+=	'<option value="'+OptionValue+'">'+OptionText+'</option>';
				}

				$("#lbl_creation_deletion_status_msg").css({'color':nav_color}).html("Label created successfully");
				$("#label_id").append(options_list);
				$('#ddlDelLabels').append(options_list);
				document.getElementById('txtCreateLabel').value = '';
	    	}
		});
	}

}

function delComparativeLabelAndApplicants(label_id) {
	
	if(label_id != "") {
		$("#lbl_creation_deletion_status_msg").css({'color':nav_color}).html("Please wait..");
		
		$.ajax({
			method: "POST",
	  		url: 	irecruit_home + "applicants/delComparativeLabelAndApplicants.php?label_id="+label_id+"&action=delete",
			type: 	"POST",
			success: function(data) {
				var options_list = '<option value="">Select</option>';
				
				for(var j = 0; j < data.length; j++) {
					var OptionValue	=	data[j]['LabelID'];
					var OptionText	=	data[j]['LabelName'];
					options_list	+=	'<option value="'+OptionValue+'">'+OptionText+'</option>';
				}
				
				if(data == "Successfully Deleted") {
					$("#label_id option[value='"+label_id+"']").remove();
					$("#ddlDelLabels option[value='"+label_id+"']").remove();
				}
				
 				$("#lbl_creation_deletion_status_msg").css({'color':nav_color}).html(data);
 				
 				location.href = irecruit_home + 'applicants/comparativeLabelApplicants.php';
	    	}
		});
	}

}


function processCompSearchApplicants(label_id) {
	
	var frmobj 		= document.frmComparativeSearchApplicants;
	frmobj.LabelID.value	=	label_id;
	
	document.forms['frmComparativeSearchApplicants'].submit();
}