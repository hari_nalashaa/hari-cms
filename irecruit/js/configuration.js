$(document).ready(function() {

	if($("#blocktype").val() == "2") {
		$("#div_new_applicants_interval").show();
	}
	else {
		$("#div_new_applicants_interval").hide();
	}

	$( "#blocktype" ).bind("change", function() {
		
		if($("#blocktype").val() == "2") {
			$("#div_new_applicants_interval").show();
		}
		else {
			$("#div_new_applicants_interval").hide();
		}
	});	
	
	$("#moduleslistconfiguration").sortable({
		 cursor: 'move',
		 update : function () {
		  	var order = $('#moduleslistconfiguration').sortable('serialize');
		  	 $.ajax({
		  		beforeSend: function () {
		  			$("#msgsortable").show();
		  			$("#msgsortable").html("Please wait, your settings are going to save.");
	   		  	},
	   		  	url: irecruit_home + "responsive/saveSortableOrder.php", 
				type: "POST",
				data : order+"&userid="+user_id,
	   		  	success: function(result){
		     	   $("#msgsortable").html("Settings successfully updated.");
		    	}
			 });
	     }
	});

});