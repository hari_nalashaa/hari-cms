<?php
require_once 'Configuration.inc';
    
$subtitle	=	array(
					"orgcenter"    =>  "Organization Center",
					"usefullinks"  =>  "Links"
				);

$title		=	'Dashboard ';

if(isset($_REQUEST['pg']) && isset($subtitle [$_REQUEST['pg']])) {
	$title .= ' - ' . $subtitle [$_REQUEST['pg']];
}

//Get Wotc Ids list
$wotcid_info = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, "");

if($wotcid_info['wotcID'] == ""
	|| is_null($wotcid_info['wotcID']))
{
	$WOTCModuleId = 21;
	G::Obj('DashboardConfiguration')->updDashboardUserModuleStatus($USERID, $WOTCModuleId, 0);
}

//Set page title
$TemplateObj->title	=	$title;

//Load Graphs Related Scripts, Only for this page
$scripts_footer[] = "js/plugins/flot/applications-data.js";
$scripts_footer[] = "js/plugins/flot/applications-vbar-data.js";
$scripts_footer[] = "js/index.js";
$scripts_footer[] = "js/twilio.js";

$script_vars_header [] = 'var wotcID = "'.$wotcid_info['wotcID'].'";';

$TemplateObj->script_vars_header    =	$script_vars_header;

//$TemplateObj->scripts_vars_footer = $script_vars_footer;
$TemplateObj->page_scripts_footer   =   $scripts_footer;
$TemplateObj->wotcid_info           =	$wotcid_info;

//Modules List With Filenames
$TemplateObj->DahboardMainModules 	=	G::Obj('DashboardConfiguration')->getDashboardModuleFilesById();
require_once IRECRUIT_DIR . 'responsive/SetDefaultUserConfiguration.inc';
require_once IRECRUIT_DIR . 'responsive/DashboardModulesData.inc';

echo $TemplateObj->displayIrecruitTemplate('views/ResponsiveIndex');
?>