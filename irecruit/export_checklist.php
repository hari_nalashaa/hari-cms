<?php 
require_once 'Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
$get_checklist_process_data      =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);

$ChecklistID = $get_checklist_process_data['ChecklistID'];

if ($ChecklistID != "") { 

    $APPDATA =   $ApplicantsObj->getAppData($OrgID, $_REQUEST['ApplicationID']);
    $filename = "Checklist_".$_REQUEST['ApplicationID']."_". $APPDATA['first'] . " " .  $APPDATA['last'];

    $checklist_info =   G::Obj('Checklist')->getCheckList($ChecklistID,$OrgID); 
    $checklist_items     =   json_decode($checklist_info['Checklist'], true);
    $checklist_Status_Lists     =   json_decode($checklist_info['StatusCategory'], true);
    $ChecklistName  =   $checklist_info['ChecklistName'];

    $STATUSLIST=array();
    foreach ($checklist_Status_Lists AS $SL) {
            $STATUSLIST[$SL['passed_value']]=$SL['field_name'];
    }

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("iRecruit")
       ->setLastModifiedBy("iRecruit")
       ->setTitle("Checklist Export")
       ->setSubject("Excel")
       ->setDescription("Checklist")
       ->setKeywords("phpExcel")
       ->setCategory("Output");


// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->mergeCells('A1:D1');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setVertical('top');
$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setWrapText(true);

//$objPHPExcel->getActiveSheet()->mergeCells('E1:F1');
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setVertical('top');
$objPHPExcel->getActiveSheet()->getStyle('E1')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(80);

$where_info[] = "OrgID = :OrgID and RequestID = :RequestID";
$params_info[":OrgID"] = $OrgID;
$params_info[":RequestID"] = $_REQUEST['RequestID'];
$JAresults =   G::Obj('Applications')->getJobApplicationsInfo('MultiOrgID, RequestID, ApplicationID, DATE_FORMAT(EntryDate, "%m/%d/%Y") as AppDate', $where_info, '', $order_by, array($params_info));
$JA = $JAresults['results'][0];


$position = "(" . G::Obj('RequisitionDetails')->getJobID($OrgID, $JA['MultiOrgID'], $_REQUEST['RequestID']);
$position .= "/" . G::Obj('RequisitionDetails')->getRequisitionID($OrgID, $JA['MultiOrgID'], $_REQUEST['RequestID']);
$position .= ") ";
$position .= G::Obj('RequisitionDetails')->getJobTitle($OrgID, $JA['MultiOrgID'], $_REQUEST['RequestID']);

$FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );
$OnboardFormID  =   $FEATURES['DataManagerType'];

G::Obj('GenericQueries')->conn_string =   "IRECRUIT";

$query = "SELECT Answer FROM OnboardData WHERE OrgID = :OrgID AND OnboardFormID = :OnboardFormID AND ApplicationID = :ApplicationID AND QuestionID = 'Sage_HireDate'"; 

$params     =   array(':OrgID'    =>  $OrgID, ':OnboardFormID'    =>  $OnboardFormID, ':ApplicationID'    =>  $_REQUEST['ApplicationID']);
$OD = G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$headertextone = "Position: " . $position . "\r";
$headertextone .= "ApplicationID: " . $_REQUEST['ApplicationID'] . "\r";
$headertextone .= "Application Date: " . $JA['AppDate'] . "\r";
if ($OD[0]['Answer'] != "") {
$headertextone .= "Application Target Start Date: " . $OD[0]['Answer'] . "\r";
}

$headertexttwo = $APPDATA['first'];
if ($APPDATA['middle'] != "") {
$headertexttwo .= ' ' . $APPDATA['middle'];
}
$headertexttwo .= ' ' . $APPDATA['last'] . "\r";

$headertexttwo .= $APPDATA['address'] . "\r";
$headertexttwo .= $APPDATA['city'] . ', ' . $APPDATA['state'] . ' ' . $APPDATA['zip'] . ' ' . $APPDATA['country'] . "\r";

$objPHPExcel->getActiveSheet()->getCell('A1')->setValue($headertextone);
$objPHPExcel->getActiveSheet()->getCell('E1')->setValue($headertexttwo);


$objPHPExcel->getActiveSheet()->getCell('A2')->setValue($ChecklistName);
$objPHPExcel->getActiveSheet()->getCell('B2')->setValue('Date Updated');
$objPHPExcel->getActiveSheet()->getCell('C2')->setValue('Updated By');
$objPHPExcel->getActiveSheet()->getCell('D2')->setValue('Status');
$objPHPExcel->getActiveSheet()->getCell('E2')->setValue('Comment');


for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getAlignment()->setVertical('center');


$n  =   3;

foreach($checklist_items AS $checklist_item){
        $get_checklist_data =   G::Obj('ChecklistData')->getCheckListDataByChecklistID($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID'], $ChecklistID,$checklist_item['passed_value']);
        $userName = G::Obj('IrecruitUsers')->getUsersName($OrgID, $get_checklist_data[CreatedBy]);

    // Rename sheet
    $title      =   "Export Checklist";
    $objPHPExcel->getActiveSheet()->setTitle($title);

    if ($get_checklist_data[LastUpdated] != "") {
	    $updated = date("m/d/Y",strtotime($get_checklist_data[LastUpdated]));
    } else {
	    $updated = "";
    }
   
    
$objPHPExcel->getActiveSheet()->getCell('A'.$n)->setValue($checklist_item[ChecklistItem]);
$objPHPExcel->getActiveSheet()->getCell('B'.$n)->setValue($updated);
$objPHPExcel->getActiveSheet()->getCell('C'.$n)->setValue( $userName);
$objPHPExcel->getActiveSheet()->getCell('D'.$n)->setValue($STATUSLIST[$get_checklist_data['StatusCategory']]);
$objPHPExcel->getActiveSheet()->getCell('E'.$n)->setValue($get_checklist_data['Comments']);

    $n++;

} // end foreach


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.".xlsx");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

} // end get ChecklistID
} // end ApplicationID and RequestID

?>
