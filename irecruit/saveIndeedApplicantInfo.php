<?php
require_once 'irecruitdb.inc';

// Posted Data
$data               =   file_get_contents('php://input');

// Static Posted Data
//$data               =   file_get_contents(ROOT . 'logs/indeed_applicants_info/test.json');

// Decode json data
$data               =   str_replace("\'", "'", $data);
$POST               =   json_decode($data, true);

$log_file_name      =   date('Y-m-d-H-i-s');
$date               =   date("Y-m-d");


//Skip page open, close questions
$page_open_close    =   array();
for($poc = 1; $poc < 40; $poc++) {
    $page_open_close[]  =   "page".$poc."_open";
    $page_open_close[]  =   "page".$poc."_close";
}

if(isset($POST['job']['jobId']) && $POST['job']['jobId'] != "") {
    
    $que_answers        =   $POST['questionsAndAnswers']['questionsAndAnswers'];
    $que_ans            =   $POST['questions']['answers'];
    
    $APPDATA            =   array();
    
    $que_info           =   array();
    $que_answers_list   =   array();
    
    foreach ($que_answers as $question_answer) {
        $que_id             =   $question_answer['question']['id'];
        $que_info[$que_id]  =   $question_answer['question'];
        $answer             =   $question_answer['answer'];
        
        $que_answers_list[$que_id]              =   $question_answer['question'];
        $que_answers_list[$que_id]['answer']    =   $question_answer['answer'];
        
        if(is_array($answer)) {
            $APPDATA[$que_id]   =   $answer;
        }
        else if(is_string($answer)) {
            $APPDATA[$que_id]   =   $answer;
        }
    }

    $job_meta           =   $POST['job']['jobMeta'];
    $web_url            =   IRECRUIT_HOME . "saveIndeedApplicantInfo.php?".$job_meta;
    
    $job_meta_query     =   parse_url($web_url, PHP_URL_QUERY);
    parse_str($job_meta_query, $job_meta_info);
    
    $OrgID              =   $job_meta_info['OrgID'];
    $MultiOrgID         =   $job_meta_info['MultiOrgID'];
    $FormID             =   $job_meta_info['FormID'];

    $RequestID          =   $POST['job']['jobId'];

    //Get Form Questions
    $where_info         =   array("OrgID = :OrgID", "FormID = :FormID", "Active = 'Y'");
    $params_info        =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
    $form_ques_results  =   G::Obj('FormQuestions')->getFormQuestionsInformation("*", $where_info, "QuestionOrder ASC", array($params_info));
    $form_ques_info     =   $form_ques_results['results'];
    $form_ques_list     =   array();
    
    for($fqi = 0; $fqi < count($form_ques_info); $fqi++) {
        $form_ques_list[$form_ques_info[$fqi]['QuestionID']]   =   $form_ques_info[$fqi];
    }
    
    // Generate ApplicationID
    $ApplicationID      =   G::Obj('Applications')->getApplicationID($OrgID);
            
    if(!is_dir(dirname(__FILE__) . '/indeed_applicants_info')) {
        //Directory does not exist, so lets create it.
        mkdir(dirname(__FILE__) . '/indeed_applicants_info', 0777);
        chmod(dirname(__FILE__) . '/indeed_applicants_info', 0777);
    }
    else {
        chmod(dirname(__FILE__) . '/indeed_applicants_info', 0777);
    }
    
    if(!is_dir(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID) && $OrgID != "") {
        //Directory does not exist, so lets create it.
        mkdir(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID, 0777);
        chmod(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID, 0777);
    }
    else {
        chmod(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID, 0777);
    }
    
    if(!file_exists(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID.'/'.$log_file_name.'-'.$POST['id'].'.json')) {
        @touch(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID.'/'.$log_file_name.'-'.$POST['id'].'.json');
        @chmod(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID.'/'.$log_file_name.'-'.$POST['id'].'.json', 0777);
    }
    else {
        @chmod(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID.'/'.$log_file_name.'-'.$POST['id'].'.json', 0777);
    }
    
    //Write log based on OrgID    
    $file = fopen(dirname(__FILE__) . '/indeed_applicants_info/'.$OrgID.'/'.$log_file_name.'-'.$POST['id'].'.json',"w+");
    fwrite($file, $data);
    fclose($file);
    
    
    $first_name     =   $POST['applicant']['resume']['json']['firstName'];
    $last_name      =   $POST['applicant']['resume']['json']['lastName'];

    if($first_name == "") {
        $first_name =   $APPDATA['first'];
    }
    if($last_name == "") {
        $last_name  =   $APPDATA['last'];
    }
    if($first_name == "") {
        $first_name =   $POST['applicant']['fullName'];
    }
    
    
    // Set CoverLetterData
    if (isset($POST['applicant']['coverletter']) && $POST['applicant']['coverletter'] != "")
        $message    =   $POST['applicant']['coverletter'];
    else
        $message    =   "";
    
    $path_info      =   pathinfo($POST['applicant']['resume']['file']['fileName']);
    $fileContent    =   $POST['applicant']['resume']['file']['data'];
    $ext            =   $path_info['extension'];

    $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
    if (! file_exists($dir)) {
        mkdir($dir, 0700);
        chmod($dir, 0777);
    }
    
    $apatdir = $dir . '/applicantattachments';
    
    if (! file_exists($apatdir)) {
        mkdir($apatdir, 0700);
        chmod($apatdir, 0777);
    }
    
    //Get Purpose names list
    $purpose                    =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);
    // Converting byte code data to text and writing it into file
    $resumedata                 =   base64_decode($fileContent);
    
    // Insert Indeed Applicant Information
    $info = array(
        "ID"                    =>  $POST['id'],
        "OrgID"                 =>  $OrgID,
        "MultiOrgID"            =>  $MultiOrgID,
        "RequestID"             =>  $RequestID,
        "FormID"                =>  $FormID,
        "ApplicationID"         =>  $ApplicationID,
        "FirstName"             =>  $first_name,
        "LastName"              =>  $last_name,
        "FullName"              =>  $first_name. ' ' . $last_name,
        "Email"                 =>  $POST['applicant']['email'],
        "PhoneNumber"           =>  $POST['applicant']['phoneNumber'],
        "CreatedDateTime"       =>  "NOW()"
    );
    
    // Insert Indeed Applicant Information
    $indeed_app_info            =   G::Obj('Indeed')->insIndeedPostData($info);
    
    //Update requisitions information
    $set_info                   =   array("ApplicantsCount = ApplicantsCount + 1");
    $where_info                 =   array("OrgID = :OrgID", "RequestID = :RequestID");
    $params_info                =   array("OrgID"=>$OrgID, "RequestID"=>$RequestID);
    G::Obj('Requisitions')->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params_info));
    
    // Get Requisitions Information
    $req_info                   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("IndeedLabelID, Active", $OrgID, $RequestID);

    // Get FormID
    if($FormID == "") {
        $FormID                 =   $req_info['IndeedLabelID'];
    }
    
    // Insert JobApplication Information
    $job_app_info               =   array(
                                        "OrgID"                 =>  $OrgID,
                                        "MultiOrgID"            =>  $MultiOrgID,
                                        "ApplicationID"         =>  $ApplicationID,
                                        "RequestID"             =>  $RequestID,
                                        "ApplicantSortName"     =>  $last_name. ', ' . $first_name,
                                        "Distinction"           =>  'P',
                                        "EntryDate"             =>  "NOW()",
                                        "LastModified"          =>  "NOW()",
                                        "ProcessOrder"          =>  "1",
                                        "StatusEffectiveDate"   =>  "DATE(NOW())",
                                        "FormID"                =>  $FormID,
                                        "Informed"              =>  '',
                                        "LeadStatus"            =>  "N",
                                        "RequisitionStatus"     =>  $req_info['Active']
                                    );
    G::Obj('Applications')->insJobApplication($job_app_info);

    //Insert Applicant Status Logs
    G::Obj('ApplicantStatusLogs')->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, "1", "");
    
    $lead_name                  =   'Indeed';
    //Update Lead Generator
    G::Obj('Applications')->updLeadGenerator($lead_name, $OrgID, $ApplicationID, $RequestID);
        
    // Insert into ApplicantHistory
    $UpdateID                   =   'Applicant';
    $Comments                   =   'Application submitted for: ' . G::Obj('RequisitionDetails')->getReqJobIDs($OrgID, $MultiOrgID, $RequestID);
    
    // Insert JobApplication History Data
    $job_application_history    =   array(
                                        "OrgID"                 =>  $OrgID,
                                        "ApplicationID"         =>  $ApplicationID,
                                        "RequestID"             =>  $RequestID,
                                        "ProcessOrder"          =>  "1",
                                        "StatusEffectiveDate"   =>  "DATE(NOW())",
                                        "Date"                  =>  "NOW()",
                                        "UserID"                =>  $UpdateID,
                                        "Comments"              =>  $Comments
                                    );
    // Insert JobApplicationsHistory
    G::Obj('JobApplicationHistory')->insJobApplicationHistory($job_application_history);
    
    // Get Organization Name
    $org_info                   =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "OrganizationName");
    $OrganizationName           =   $org_info['OrganizationName'];
    
    $location_city_info         =   explode(",", $POST['applicant']['resume']['json']['location']['city']);
    $city                       =   trim($location_city_info[0]);
    $state                      =   trim($location_city_info[1]);
    $country                    =   $POST['applicant']['resume']['json']['location']['country'];
    
    //Get Form Questions
    $where_info                 =   array("OrgID = :OrgID", "FormID = :FormID", "Active = 'Y'");
    $params_info                =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
    $form_ques_results          =   G::Obj('FormQuestions')->getFormQuestionsInformation("*", $where_info, "QuestionOrder ASC", array($params_info));
    $form_ques_info             =   $form_ques_results['results'];
    $form_ques_list             =   array();

    for($fqi = 0; $fqi < count($form_ques_info); $fqi++) {
        $form_ques_list[$form_ques_info[$fqi]['QuestionID']]   =   $form_ques_info[$fqi];
    }
    
    if($city == "") {
        $city       =   $APPDATA['city'];
    }
    if($state == "") {
        $state      =   $APPDATA['state'];
    }
    if($country == "") {
        $country    =   $APPDATA['country'];
    }
    
    
    $que_ids        =   array(
                            'first',
                            'last',
                            'email',
                            'city',
                            'country',
                            'state',
                            'source',
                            'FormID'
                        );
    
    // Insert Applicant Data
    $info           =   array();

    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'first',
        ":Answer"           =>  $first_name,
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'last',
        ":Answer"           =>  $last_name,
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'email',
        ":Answer"           =>  $POST['applicant']['email'],
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'city',
        ":Answer"           =>  $city,
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'country',
        ":Answer"           =>  $country,
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'state',
        ":Answer"           =>  $state,
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'source',
        ":Answer"           =>  'INDEED',
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'FormID',
        ":Answer"           =>  $FormID,
    );

    //Add Custom Questions and Answers to array
    foreach ($que_answers_list as $question_id=>$que_ans_info) {
        
        if(!in_array($question_id, $que_ids)) {
            
            if($question_id ==  'applicant_profile_picture') {
                 
                $applicant_picture_name = $que_ans_info['answer']['fileName'];
                $applicant_picture_data = $que_ans_info['answer']['data'];
            
                if ($applicant_picture_data != "") {
            
                    // FILEHANDLING
                    $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
            
                    if (! file_exists ( $dir )) {
                        mkdir ( $dir, 0700 );
                        chmod ( $dir, 0777 );
                    }
            
                    $app_picture_dir = $dir . '/applicant_picture';
            
                    if (! file_exists ( $app_picture_dir )) {
                        mkdir ( $app_picture_dir, 0700 );
                        chmod ( $app_picture_dir, 0777 );
                    }
            
                    if ($applicant_picture_name != "") {
                         
                        $data       =   base64_decode($applicant_picture_data);
                        $e          =   explode ( '.', $applicant_picture_name );
                        $ecnt       =   count ( $e ) - 1;
                        $ext        =   $e [$ecnt];
                        $ext        =   preg_replace ( "/\s/i", '', $ext );
                        $ext        =   substr ( $ext, 0, 5 );
            
                        $filename   =   $app_picture_dir . '/' . $ApplicationID . '-ApplicantPicture.' . $ext;
                        $fh         =   fopen ( $filename, 'w' );
                        fwrite ( $fh, $data );
                        fclose ( $fh );
            
                        chmod ( $filename, 0666 );
            
                        $ProfileAnswer = $ApplicationID . '-ApplicantPicture.'.$ext;
            
                        $info[] = array(
                            ":OrgID"            =>  $OrgID,
                            ":ApplicationID"    =>  $ApplicationID,
                            ":QuestionID"       =>  "ApplicantPicture",
                            ":Answer"           =>  $ProfileAnswer,
                        );
                        
                    }
                }
            
            }
            else {
                
                $answer             =   $que_ans_info['answer'];
                $question_type_id   =   0;
                $question           =   "";
                $answer_status      =   0;
                
                if(!in_array($question_id, $page_open_close)) {

                    if(isset($form_ques_list[$question_id]['QuestionTypeID'])) {
                        
                        if($form_ques_list[$question_id]['QuestionTypeID'] == 13
                            || $form_ques_list[$question_id]['QuestionTypeID'] == 14) {
                                
                            $phone_info             =   G::Obj('Address')->getFormattedLeadPhoneNumber($answer);
                            
                            $question_type_id       =   $form_ques_list[$question_id]['QuestionTypeID'];
                            $question               =   $form_ques_list[$question_id]['Question'];
                            $answer_status          =   1;
                            $answer                 =   $phone_info['Phone'];
                            
                            if($que_info[$question_id]['type'] != 'information') {
                                if($question_id != $answer) {
                                    $info[] = array(
                                        ":OrgID"            =>  $OrgID,
                                        ":ApplicationID"    =>  $ApplicationID,
                                        ":QuestionID"       =>  $question_id,
                                        ":Answer"           =>  $answer,
                                    );
                                }
                            }
                        }
                        else if(isset($que_ans_info['answer']['value'])) {
                            $info[] = array(
                                ":OrgID"            =>  $OrgID,
                                ":ApplicationID"    =>  $ApplicationID,
                                ":QuestionID"       =>  $question_id,
                                ":Answer"           =>  $que_ans_info['answer']['value'],
                            );
                        }
                        else if(is_array($que_ans_info['answer'])) {
                            for($ca = 0, $qai = 1; $ca < count($que_ans_info['answer']); $ca++, $qai++) {
                                $info[] = array(
                                    ":OrgID"            =>  $OrgID,
                                    ":ApplicationID"    =>  $ApplicationID,
                                    ":QuestionID"       =>  $question_id."-".$qai,
                                    ":Answer"           =>  $que_ans_info['answer'][$ca]['value'],
                                );
                            }
                            
                            if(count($que_ans_info['answer']) > 0) {
                                $info[] = array(
                                    ":OrgID"                =>  $OrgID,
                                    ":ApplicationID"        =>  $ApplicationID,
                                    ":QuestionID"           =>  $question_id."cnt",
                                    ":Answer"               =>  count($que_ans_info['answer']),
                                );
                            }
                        }
                        else if(is_string($que_ans_info['answer']['value'])) {
                            $info[] = array(
                                ":OrgID"            =>  $OrgID,
                                ":ApplicationID"    =>  $ApplicationID,
                                ":QuestionID"       =>  $question_id,
                                ":Answer"           =>  $answer,
                            );
                        }
                        
                    }
                    
                }
            
            }
            
        }
        
        //Insert $question_id to $que_ids
        $que_ids[]  =  $question_id;
    }

    $params_list    =   array();
    $question_ids   =   array();
    $pi             =   0;

    $informed       =   '';
    for($ini = 0; $ini < count($info); $ini++) {
        
        if(!in_array($info[$ini][":QuestionID"], $question_ids)) {
            
            if($que_info[$info[$ini][":QuestionID"]]['type'] != "pagebreak")
            {
                $params_list[$pi][":OrgID"]             =   $info[$ini][":OrgID"];
                $params_list[$pi][":ApplicationID"]     =   $info[$ini][":ApplicationID"];
                $params_list[$pi][":QuestionID"]        =   $info[$ini][":QuestionID"];
                $params_list[$pi][":Answer"]            =   $info[$ini][":Answer"];
 
                $question_ids[]                         =   $info[$ini][":QuestionID"];
                
                if($info[$ini][":QuestionID"] == 'informed') {
                    $informed   =   $info[$ini][":Answer"];
                }
                
                $pi++;
            }
            
        }
    }
    
    // Insert Applicant Data
    G::Obj('Applicants')->insApplicantData($params_list);
    
    $set_info               =   array("Informed = :Informed");
    $where_info             =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
    $params_info            =   array(":Informed"=>$informed, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
    $update_result          =   G::Obj('Applications')->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params_info));

    // Create resume based on binary data
    if ($resumedata != "" && $ext != "") {
        $fileName = $apatdir . '/' . $ApplicationID . '-resume' . '.' . $ext;
        touch($fileName);
        $fp = fopen($fileName, 'w');
        fwrite($fp, $resumedata);
        fclose($fp);
    
        chmod($fileName, 0644);
    
        // Insert Applicant Attachments Information
        $attachment_info = array(
            "OrgID"             =>  $OrgID,
            "ApplicationID"     =>  $ApplicationID,
            "TypeAttachment"    =>  'resumeupload',
            "PurposeName"       =>  'resume',
            "FileType"          =>  $ext
        );
        
        G::Obj('Attachments')->insApplicantAttachments($attachment_info);
    }
    
    
    // Create coverletter in text file
    if ($POST['applicant']['coverletter'] != "") {
        $fileName = $apatdir . '/' . $ApplicationID . '-coverletter.txt';
        touch($fileName);
        $fp = fopen($fileName, 'w');
        fwrite($fp, str_replace(array("<br>", "<br/>", "<br / >"), "\n", $POST['applicant']['coverletter']));
        fclose($fp);
    
        chmod($fileName, 0644);
    
        // Insert Applicant Attachments Information
        $attachment_info = array(
            "OrgID"             =>  $OrgID,
            "ApplicationID"     =>  $ApplicationID,
            "TypeAttachment"    =>  'coverletterupload',
            "PurposeName"       =>  'coverletter',
            "FileType"          =>  'txt'
        );
        G::Obj('Attachments')->insApplicantAttachments($attachment_info);
    }
    
    // Set OnboardStatus to ready for this Application
    if ($OrgID == "I20091201") {
        //Get Applicant MultiOrgID
        $AppMultiOrgID  =   G::Obj('ApplicantDetails')->getMultiOrgID($OrgID, $ApplicationID, $RequestID);
        //Set Onboard Header Information
        $onboard_info   =   array(
                                "OrgID"         =>  $OrgID,
                                "MultiOrgID"    =>  $AppMultiOrgID,
                                "ApplicationID" =>  $ApplicationID,
                                "RequestID"     =>  $RequestID,
                                "Status"        =>  'ready',
                                "ProcessDate"   =>  "NOW()",
                                "OnboardFormID" =>  'XML',
                            );
        $on_update      =   " ON DUPLICATE KEY UPDATE Status = :UStatus, ProcessDate = NOW()";
        $update_info    =   array(":UStatus" =>  'ready');
        G::Obj('OnboardApplications')->insOnboardApplication($onboard_info, $on_update, $update_info);
    }
    
    // Bind parameters
    $params     =   array (':OrgID' => $OrgID);
    // Set condition
    $where      =   array ("OrgID = :OrgID", "Role IN ('','master_admin')");
    // Auto Forward //Get Userinformation based on role
    $results    =   G::Obj('IrecruitUsers')->getUserInformation ( "UserID", $where, "Role LIMIT 1", array ($params) );
    $admin      =   $results ['results'] [0] ['UserID'];
    
    //Assign Status, Presented To
    $ProcessOrder   =   $NPresentedTo   =   $NAssignStatus  =   '1';
    //Assign Internal Forms
    include COMMON_DIR . 'formsInternal/AssignInternalForms.inc';
    
    // Bind parameters
    $params     =   array (':OrgID' => $OrgID, ':RequestID' => $RequestID);
    // Set condition
    $where      =   array ("OrgID    = :OrgID", "RequestID = :RequestID");
    // Get Requisition Forward Information
    $results    =   G::Obj('Requisitions')->getRequisitionForwardInfo ( "EmailAddress", $where, "", array ($params) );
    
    if ($results ['count'] > 0) {
    
        require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
    
        $ATTS               =   array();
        //Have to update it with forward applicant specs information
        $req_forward_specs  =   G::Obj('RequisitionForwardSpecs')->getRequisitionForwardSpecsDetailInfo($OrgID, $RequestID);
        $forward_specs_list =   json_decode($req_forward_specs ['ForwardSpecsList'], true);
        //Forward Specs List
        foreach($forward_specs_list as $spec_que_id) {
            $ATTS[$spec_que_id] =   'Y';
        }
    
        $EmailList     =   array();
        $status_index  =   0;
        if (is_array ( $results ['results'] )) {
            foreach ( $results ['results'] as $EM ) {
                $Attachments = forwardApplicant ( $OrgID, $ApplicationID, $RequestID, $EM ['EmailAddress'], $ATTS, 'Application Auto Forwarded', $admin, 'processforward', $status_index);
                $EmailList[] = $EM ['EmailAddress'];
                $status_index++;
            } // end foreach
        }
    
        updateApplicantStatus ( $OrgID, $ApplicationID, $RequestID, 'Application', implode(", ", $EmailList), $Attachments, 'Application Auto Forwarded' );
    } // end result > 0
    
}
else {
    //Log the ziprecruiter response
    Logger::writeMessage ( ROOT . "logs/indeed_applicants_info/applicant-data" . date ( 'Y-m-d-H:i:s' ) . '-' . uniqid() . ".log", $data, "a+", true );
}