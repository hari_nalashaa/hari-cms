<?php

// An Export Builder that looks up and configures Questions and their custom export key name to be used upon export
// An Export feature that will look up the data for the form and deliver it via
// webservices, excel, csv, tab deliniated

if ($feature['Exporter'] != "Y") { die (include IRECRUIT_DIR . 'irecruit_permissions.err'); } // end feature

$rtn.='<div><strong style="font-size:14pt;">Exporter</strong></div>';
$rtn.='<div class="table-responsive"><div style="margin: 10px 0 20px 20px;line-height:190%;">';
$rtn.='<a href="#openFormWindow" onClick="loadExporterForm(\'exporterForm.php\',\'\');">';
$rtn .= '<img src="' . IRECRUIT_HOME . 'images/icons/chart_curve_add.png" width="16" height="16" title="Select Data" border="0" style="margin:0px 3px -4px 0px;">';
$rtn.='Select Data';
$rtn.='</a><br>';

$rtn.='<a href="#openFormWindow" class="button" onClick="loadExporterForm(\'exporterExport.php\',\'\');">';
$rtn .= '<img src="' . IRECRUIT_HOME . 'images/icons/sitemap_color.png" width="16" height="16" title="Export Data" border="0" style="margin:0px 3px -4px 0px;">';
$rtn .= 'Export Data';
$rtn.='</a><br>';

/*
$rtn.='<a href="#openFormWindow" class="button" onClick="loadExporterForm(\'exporterScheduler.php\',\'\');">';
$rtn .= '<img src="' . IRECRUIT_HOME . 'images/icons/clock.png" width="16" height="16" title="Schedule" border="0" style="margin:0px 3px -4px 0px;">';
$rtn .= 'Schedule';
$rtn.='</a>';
$rtn.='</div>';
*/

$rtn.='<div id="openFormWindow" class="openForm">';
$rtn.='<div>';
$rtn.='<p style="text-align:right;"><a href="#closeForm" title="Close Window">';
$rtn.='<img src="' . IRECRUIT_HOME . 'images/icons/photo_delete.png" title="Close Window" style="margin:0px 3px -4px 0px;" border="0">';
$rtn.='<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b></a></p>';
$rtn.='<div id="ExporterForm"></div>';
$rtn.='<div id="ExporterItems"></div>';
$rtn.='</div>';
$rtn.='</div></div>' . "\n";

echo $rtn;
?>
<script>
loadExporterForm('exporterForm.php','');
</script>
