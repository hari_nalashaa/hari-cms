<?php
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Applicants Referral Source")
->setSubject("Excel")
->setDescription("To view the applicants by referral source")
->setKeywords("phpExcel")
->setCategory("Applicants By Referral Source");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application ID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Requisition Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('RequisitionID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('JobID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Name');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Entry Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Status');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Last Updated');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Email');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Phone Number');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Source');


for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha = 0;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicationID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Title']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['JobID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicantSortName']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['EntryDateMDY']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ProcessOrder']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['LastModifiedMDY']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Email']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['HomePhone']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ReferralSource']);


}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="applicants_status_by_referral_source.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
