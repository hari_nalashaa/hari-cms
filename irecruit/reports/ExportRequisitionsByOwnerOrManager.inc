<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Requisitions Owner Or Manager Reports")
->setSubject("Excel")
->setDescription("To view the requisitions information those are filtered in Requisitions By Owner Or Manager page in Reports section")
->setKeywords("phpExcel")
->setCategory("Requisitions Reports");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('ReqID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('JobID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Status');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Requisition Stage');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Post Options');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Owner');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Managers');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Date Opened');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Expiration Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Days Open');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Applicants Count');
$al++;


for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    
    $StatusActive   =  '';
    if($list[$ah]['Active'] == "Y") $StatusActive = "Active";
    else if($list[$ah]['Active'] == "N") $StatusActive = "In-Active";
    else if($list[$ah]['Active'] == "R") $StatusActive = "Pending";
    //string validate
    $strcnt = strlen($list[$ah]['PresentOn']);
    if($strcnt >=15){
                    $splarray =  explode("AND",$list[$ah]['PresentOn']);
                    $PresentOn = ucfirst(strtolower($splarray[0]))." and ".ucfirst(strtolower($splarray[1]));

               }

         if($strcnt < 14){
                    $splarray =  explode("ONLY",$list[$ah]['PresentOn']);
                    $PresentOn = ucfirst(strtolower($splarray[0]))." Only";

               }

        //
   
    $alpha = 0;
   


   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Title']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionID']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['JobID']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($StatusActive);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionStage']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($PresentOn);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionOwnersName']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionManagersNames']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['PostDate']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ExpireDate']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Open']);
   $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['ApplicantsCount']));

}

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="requisitions_by_owner_manager.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
