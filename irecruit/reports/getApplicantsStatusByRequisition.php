<?php
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
    $TemplateObj->goto  =   $_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title     =   "Applicants Status By Requisition";

// Assign add applicant datepicker variables
$script_vars_footer[]   =   'var datepicker_ids = "#applicants_from_date, #applicants_to_date";';
$script_vars_footer[]   =   'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer = $script_vars_footer;

$FilterOrganization     =   $_REQUEST['FilterOrganization'];


$hrmanager ='';
 if (substr ($USERROLE, 0, 21 ) == 'master_hiring_manager') { 
    $queryString ="select RequestID from RequisitionManagers where OrgID ='".$OrgID."' and UserID ='".$USERID."'";
    //Set Database
    $HRmagaerdata =   G::Obj('GenericQueries')->getInfoByQuery($queryString);
    $hrmanager_req_id = $HRmagaerdata[0]['RequestID'];

// Get Applicants based on RequestID
$where_info             =   array("OrgID = :OrgID", "Approved = :Approved","RequestID = :RequestID");
$params_info            =   array(":OrgID"=>$OrgID, ":Approved"=>'Y',"RequestID"=>$hrmanager_req_id);

// end hiring manager
 }else{

// Get Applicants based on RequestID
$where_info             =   array("OrgID = :OrgID", "Approved = :Approved");
$params_info            =   array(":OrgID"=>$OrgID, ":Approved"=>'Y');

} 
   
$columns                =   "Title, RequestID, RequisitionID, JobID";
$order_by               =   "Title, RequisitionID, JobID DESC";
  
 



// Get Requisition Information
$req_results            =   G::Obj('Requisitions')->getRequisitionInformation ( $columns, $where_info, "", $order_by, array($params_info) );
$requisitions_list      =   $req_results['results'];

$TemplateObj->req_results           =   $req_results;
$TemplateObj->requisitions_list     =   $requisitions_list;

$from_date  =   date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d'))));
$to_date    =   date('m/d/Y');

if(isset($_REQUEST['FromDate']) && $_REQUEST['FromDate'] != "") {
    $from_date  =   $_REQUEST['FromDate'];
}
if(isset($_REQUEST['ToDate']) && $_REQUEST['ToDate'] != "") {
    $to_date    =   $_REQUEST['ToDate'];
}

$TemplateObj->from_date     =   $from_date;
$TemplateObj->to_date       =   $to_date;

echo $TemplateObj->displayIrecruitTemplate ( 'views/reports/ApplicantsStatusByRequisition' );
?>