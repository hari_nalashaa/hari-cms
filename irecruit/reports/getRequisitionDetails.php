<?php 
require_once '../Configuration.inc';

require_once COMMON_DIR . 'listings/SocialMedia.inc';

if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
    $RequestIDsList =   explode(",", $_REQUEST['RequestID']);
    
    $RequestIDs =   "'" . implode ( "', '", $RequestIDsList ) . "'";
    
    // Set columns
    $columns    =   "NOW() AS CurrentDateTime, DATE_FORMAT(PostDate,'%m/%d/%Y') PostDate, DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate,
			         DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,";
    $columns    .=  "IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate)) Open, DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate) Closed,";
    $columns    .=  "date_format(date_add(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current,
			         RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID";
    
    $where_info     =   array("OrgID = :OrgID");
    $params_info    =   array(":OrgID"=>$OrgID);
    
    if(isset($_REQUEST['RequestID']) && count($_REQUEST['RequestID']) > 0) {
        $where_info =   array("RequestID IN (".$RequestIDs.")");
    }
    
    $req_results    =   G::Obj('Requisitions')->getRequisitionInformation($columns, $where_info, "", "", array($params_info));
    $requisitions   =   $req_results['results'];
    
    for($r = 0; $r < count($requisitions); $r++)
    {
        $RequestID =   $requisitions[$r]['RequestID'];

        $requisitions[$r]['OrganizationName']   =   '';
        
        if ($feature ['MultiOrg'] == "Y") {
            $requisitions[$r]['OrganizationName']   =   G::Obj('OrganizationDetails')->getOrganizationNameByRequestID ( $OrgID, $RequestID );
        } // end feature
        
        
        $requisitions[$r]['feature'] = $feature;
        
        // Set where condition
        $where      =   array ("OrgID = :OrgID", "RequestID = :RequestID");
        // Set parameters
        $params     =   array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
        // Get Job Applications Information
        $resultsCNT =   G::Obj('Applications')->getJobApplicationsInfo ( "count(*) as cnt", $where, '', '', array ($params) );
        $ttcnt      =   $resultsCNT ['results'] [0] ['cnt'];
        
        $requisitions[$r]['ApplicantsCount'] = $ttcnt;
    }
}

echo json_encode($requisitions);
?>