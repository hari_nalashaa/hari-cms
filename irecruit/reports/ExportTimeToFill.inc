<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit AAP Reports")
->setSubject("Excel")
->setDescription("To view the applicants information those are filtered in reports")
->setKeywords("phpExcel")
->setCategory("AAP");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Open Date');

$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Expire Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Final Stage');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Days Open');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('RequisitionID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('JobID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('EEO');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('First Name');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Last Name');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application ID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Entry Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Date Hired');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Time To Hire');

for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha = 0;

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['PostDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ExpireDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['FinalStageDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Open']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['JobTitle']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['JobID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['EEO']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['FirstName']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['LastName']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicationID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['EntryDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['DateHired']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['TimeToHire']);
}



header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="time_to_fill.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
