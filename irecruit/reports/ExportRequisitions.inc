<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Requisitions Reports")
->setSubject("Excel")
->setDescription("To view the requisitions information those are filtered in reports requisitions page")
->setKeywords("phpExcel")
->setCategory("Requisitions Reports");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('ReqID/JobID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Status');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Date Opened');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Expiration Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Days Open');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Applicants Count');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Company');


for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha = 0;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Title']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionID'] . "/" . $list[$ah]['JobID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ActiveStatus']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['PostDateByFeature']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ExpireDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Open']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['ApplicantsCount']));
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['OrganizationName']);
}



header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="requisitions.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
