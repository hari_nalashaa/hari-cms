<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title     =   $title  =   'Portal Application Status';

//Pagination parameters
$TemplateObj->Start     =   $Start  =   0;
$TemplateObj->Limit     =   $Limit  =   $user_preferences['ReportsSearchResultsLimit'];

// Assign datepicker variables
$script_vars_footer[]   =   'var datepicker_ids = "#applicants_from_date, #applicants_to_date";';
$script_vars_footer[]   =   'var date_format = "mm/dd/yy";';

$TemplateObj->FilterOrganization    =   $FilterOrganization = $_REQUEST['ddlOrganizationsList'];

if($FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);

    $where  =   array("UA.OrgID = :OrgID", "UA.MultiOrgID = :MultiOrgID");
    $params =   array(":OrgID" => $FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
}
else {
    $where  =   array("UA.OrgID = :OrgID");
    $params =   array(":OrgID"=>$OrgID);
}

if(isset($_REQUEST['FromDate'])
    && $_REQUEST['FromDate'] != ""
    && isset($_REQUEST['ToDate'])
    && $_REQUEST['ToDate'] != "") {
    $where[] = "DATE(LastUpdatedDate) >= :FromDate";
    $where[] = "DATE(LastUpdatedDate) <= :ToDate";

    $params[":FromDate"] = $_REQUEST['FromDate'];
    $params[":ToDate"] = $_REQUEST['ToDate'];
}
else {
    $where[] = "DATE(LastUpdatedDate) >= :FromDate";
    $where[] = "DATE(LastUpdatedDate) <= :ToDate";

    $params[":FromDate"] = date('Y-m-d', strtotime("-6 months", strtotime(date('Y-m-d'))));
    $params[":ToDate"] = date('Y-m-d');
}

//if(isset($_REQUEST['Status']) && $_REQUEST['Status'] != "") {
    $where[] = "Status = :Status AND Deleted = 'No'";
    $params[":Status"] = 'Pending';//$_REQUEST['Status'];
//}

    //$where[] = "Deleted = :Deleted";
    //$params[":Deleted"] = 'Pending';//$_REQUEST['Status'];
        
$order_by  = "UA.Status DESC";
$order_by .= " LIMIT 0, $Limit";

//Reports 
$ReportsApplicationsObj->conn_string    =   "USERPORTAL";
$user_applications_results              =   G::Obj('ReportsApplications')->getUserApplications($where, "", $order_by, array($params));

//Total Count
$ReportsApplicationsObj->conn_string    =   "USERPORTAL";
$TemplateObj->total_count               =   G::Obj('ReportsApplications')->getUserApplicationsCount($where, $order_by, array($params));

$TemplateObj->user_applications         =   $user_applications      = $user_applications_results['results'];
$TemplateObj->user_applications_cnt     =   $user_applications_cnt  = $user_applications_results['count'];
$TemplateObj->scripts_vars_footer       =   $script_vars_footer;

echo $TemplateObj->displayIrecruitTemplate('views/reports/CompleteIncompleteApplications');
?>