<?php
require_once '../../Configuration.inc';

//Get UserPreferences based on UserID
$results    =   G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
$DR         =   $results['DefaultSearchDateRange'];

if ($DR == "") {
	$DR = "90";
}

$columns    =   'DATE_FORMAT(DATE_SUB(CURDATE(), INTERVAL ' . $DR . ' DAY),"%m/%d/%Y"), DATE_FORMAT(DATE_ADD(CURDATE(),INTERVAL 1 DAY),"%m/%d/%Y")';
$results    =   $MysqlHelperObj->getDatesList($columns);
if(is_array($results)) list ( $StartDate, $EndDate ) = array_values($results);

//Set where condition
$where      =   array ("OrgID = :OrgID", "ExporterID != 'DataManager'");
//Set parameters
$params     =   array(":OrgID"=>$OrgID);
//Get ExporterNames Information
$results    =   $ExporterDataManagerObj->getExporterInfo('ExporterNames', "*", $where, "ExportName", array($params));
//NameCount
$namecnt    =   $results['count'];

echo '<div style="float:left;">';
echo '<strong style="font-size:14pt;">Export Data</strong>';
echo '</div>';

echo '<div style="float:right;margin-top:10px;">';
echo '<a href="#openFormWindow" onclick="loadExporterForm(\'exporterForm.php\',\'\');">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/chart_curve_add.png" width="16" height="16" title="Select Data" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Select Data</b>';
echo '</a>';
echo '</div>';

echo '<div style="clear:both;margin-bottom:15px;">';
echo '</div>';

if ($namecnt > 0) {
	
	echo '<form id="exporter">';
	echo '<div style="margin-bottom:3px;padding:3px;float:left;text-align:right;width:120px;margin-right:10px;">';
	echo 'Available Exports:';
	echo '</div>';
	
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo '<select name="ExporterID">' . "\n";
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $EXPORTNAMES) {
		
			echo '<option value="' . $EXPORTNAMES ['ExporterID'] . '"';
			if ($EXPORTNAMES ['ExporterID'] == $_POST ['ExporterID']) {
				echo ' selected';
			}
			echo '>';
			echo $EXPORTNAMES ['ExportName'];
			echo '</option>' . "\n";
		} // end foreach
	}
	
	
	echo '</select>' . "\n";
	echo '</div>';
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo 'Data Upper Case:';
	echo '</div>';
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo '<input type="radio" name="UpperCase" value="Upper"> Yes';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="UpperCase" value="" checked> No';
	echo '&nbsp;&nbsp;';
	echo '</div>';
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo 'Names or Values:';
	echo '</div>';
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo '<input type="radio" name="NameValue" value="N"> Names';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="NameValue" value="" checked> Values';
	echo '&nbsp;&nbsp;';
	echo '</div>';
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo 'Applicant Dates:';
	echo '</div>';
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo 'From: <input type="text" id="ReportDate_From" name="ReportDate_From" value="' . $StartDate . '" size="10" onBlur="validate_date(this.value,\'Date From\');" />&nbsp;&nbsp;&nbsp;To: <input type="text" id="ReportDate_To" name="ReportDate_To" value="' . $EndDate . '" size="10" onBlur="validate_date(this.value,\'Date To\');" />';
	echo '</div>';
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo 'ApplicationID:';
	echo '</div>';
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo '<input type="text" name="ApplicationID" size="20" maxlength="30" value="">';
	echo '&nbsp;&nbsp;&nbsp;RequestID:&nbsp;&nbsp;';
	echo '<select name="RequestID">';
	echo '<option value="">Select All</option>';
	
	//Set where condition
	$where = array("OrgID = :OrgID1", "RequestID IN (SELECT DISTINCT(RequestID) FROM JobApplications WHERE OrgID = :OrgID2)");
	//Set parameters
	$params = array(":OrgID1"=>$OrgID, ":OrgID2"=>$OrgID);
	//Get Requisitions
	$resultsREQS = $RequisitionsObj->getRequisitionInformation("RequestID, Title", $where, "", "Title", array($params));
	
	if(is_array($resultsREQS)) {
		foreach($resultsREQS['results'] as $REQS) {
			echo '<option value="' . $REQS ['RequestID'] . '">' . $REQS ['Title'] . '</option>';
		} // end foreach
	}	
	
	echo '</select>';
	echo '</div>';
	
        $OnboardFormIDs = "";

        if($feature['MatrixCare'] == 'Y') {
          $OnboardFormIDs .= "'MatrixCare',";
        }
        if($feature['WebABA'] == 'Y') {
          $OnboardFormIDs .= "'WebABA',";
        }
        if($feature['Lightwork'] == 'Y') {
          $OnboardFormIDs .= "'Lightwork',";
        }
        if($feature['Abila'] == 'Y') {
          $OnboardFormIDs .= "'Abila',";
        }
        if($feature['Mercer'] == 'Y') {
          $OnboardFormIDs .= "'Mercer',";
        }
        if($feature['DataManagerType'] != 'N') {
          $OnboardFormIDs .= "'" . $feature['DataManagerType'] . "',";
        }

        $OnboardFormIDs = substr($OnboardFormIDs,0,-1);

        if ($OnboardFormIDs != "") {
            
            //Get Job Applications based on Onboard Data
            $params       =     array(":OrgID"=>$OrgID);
            $sel_info     =     "SELECT OA.OnboardFormID, OA.Status AS DMS";
            $sel_info    .=     " FROM OnboardApplications OA";
            $sel_info    .=     " WHERE OA.OrgID = :OrgID";
            $sel_info    .=     " AND OA.OnboardFormID IN (" . $OnboardFormIDs . ")";
            $sel_info    .=     " GROUP BY OA.OrgID, OA.OnboardFormID, OA.Status";
            $sel_info    .=     " ORDER BY OA.Status";
            $resultsREQS  =     $ApplicationsObj->getInfo($sel_info, array($params));
            
            echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
            echo 'Onboard Status:';
            echo '</div>';
            echo '<div style="margin-bottom:3px;padding:3px;">';

            echo '<select name="OnboardStatus">';
            echo '<option value="">Select All</option>';

            if(is_array($resultsREQS['results'])) {
                foreach($resultsREQS['results'] as $JA) {
                	if ($JA ['DMS'] != "") {
                		echo '<option value="' . $JA ['OnboardFormID'].":".$JA ['DMS']. '">' . $JA ['OnboardFormID']." - ".$JA ['DMS'] . '</option>';
                	}
                } //end foreach
            }

            echo '</select>';
            echo '</div>';

	} // end onboard
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo 'Type of Export:';
	echo '</div>';
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo '<input type="radio" name="TypeExport" value="TXT" checked> Array';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="TypeExport" value="CSV"> CSV';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="TypeExport" value="XML"> XML';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="TypeExport" value="JSON"> JSON';
	echo '</div>';
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo 'Export To:';
	echo '</div>';
	echo '<div style="margin-bottom:3px;padding:3px;">';
	echo '<input type="radio" name="ExportTo" value="SCREEN" checked> Screen';
	echo '&nbsp;&nbsp;';
	echo '<input type="radio" name="ExportTo" value="FILE"> File';
	echo '&nbsp;&nbsp;';
	echo '</div>';
	
	echo '<div style="clear:both;padding:5px 3px 0px 3px;text-align:right;float:left;width:120px;margin-right:10px;">';
	echo '&nbsp;';
	echo '</div>';
	echo '<div style="margin-bottom:3px;margin-top:10px;padding:3px;">';
	echo '<button onclick="exportData();return false;">Export</button>';
	echo '<span id="export_progress_msg" style="color:blue"></span>';
	echo '</form>';
	echo '</div>';
} else { // else if 0 cnt
	
	echo '<div style="margin-bottom:30px;padding:3px;">';
	echo 'Available Exports:&nbsp;&nbsp;';
	echo 'There are no export configurations.';
	echo '&nbsp;&nbsp;&nbsp;';
	echo '<a onclick="loadExporterForm(\'exporterForm.php\',\'\');">Please select data fields</a>.';
	echo '</div>';
} // end if 0 cnt
?>
