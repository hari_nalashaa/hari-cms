<?php
require_once '../../Configuration.inc';

list ( $ExporterID, $item, $FormID ) = explode ( ':', $_REQUEST ['item'] );

if ($_REQUEST ['action'] == "fnupdate") {
	
	$fn = $_REQUEST ['fn'];
	$fn = preg_replace ( '/\s/', '_', $fn );
	$fn = preg_replace ( "/[^A-Za-z0-9\/|_]/", "", $fn );
	
	if (is_numeric ( substr ( $fn, 0, 1 ) )) {
		$fn = "";
	}
	
	$ALPHA = array (
			'XX',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
			'I',
			'J',
			'K',
			'L',
			'M',
			'N',
			'O',
			'P',
			'Q',
			'R',
			'S',
			'T',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z' 
	);
	
	if ($fn != "") {
		for($i = 1; $i < 10; $i ++) {
			
			//Set where condition
			$where = array("OrgID = :OrgID", "ExporterID = :ExporterID", "FormID = :FormID", "FriendlyName = :FriendlyName");
			//Set parameters
			$params = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":FormID"=>$FormID, ":FriendlyName"=>$fn);
			//Get Exporter Information
			$results = $ExporterDataManagerObj->getExporterInfo('Exporter', "*", $where, "", array($params));
			$cnt = $results['count'];
			
			if ($cnt > 0) {
				$fn .= "_" . $ALPHA [$i];
			} else {
				break;
			}
		} // end for
	} // end fn
	
	//Set Information
	$set_info = array("FriendlyName = :FriendlyName");
	//Set where condition
	$where = array("OrgID = :OrgID", "ExporterID = :ExporterID", "FormID = :FormID", "QuestionID = :QuestionID");
	//Set parameters
	$params = array(":FriendlyName"=>$fn, ":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":FormID"=>$FormID, ":QuestionID"=>$item);
	//Update Exporter Information	
	$ExporterDataManagerObj->updExporterInfo('Exporter', $set_info, $where, array($params));
	
} else if ($_REQUEST ['action'] == "deleteExport") {
	//Set where condition
	$where = array("OrgID = :OrgID", "ExporterID = :ExporterID");
	//Set parameters
	$params = array(":ExporterID"=>$ExporterID, ":OrgID"=>$OrgID);
	
	//Update Exporter Information
	$ExporterDataManagerObj->delExporterInfo("Exporter", $where, array($params));
	//Update Exporter Information
	$ExporterDataManagerObj->delExporterInfo("ExporterNames", $where, array($params));
	
} else if ($_REQUEST ['action'] == "delete") {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "ExporterID = :ExporterID", "SortOrder = :SortOrder");
	//Set parameters
	$params = array(":ExporterID"=>$ExporterID, ":OrgID"=>$OrgID, ":SortOrder"=>$item);
	
	//Update Exporter Information
	$ExporterDataManagerObj->delExporterInfo("Exporter", $where, array($params));
	
} else if ($_REQUEST ['action'] == "up") {
	
	$minus = $item - 1;
	
	$params = array();
	
	//Set parameters
	$params[] = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":USortOrder"=>'999', ":SortOrder"=>$minus);
	//Set parameters
	$params[] = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":USortOrder"=>$minus, ":SortOrder"=>$item);
	//Set parameters
	$params[] = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":USortOrder"=>$item, ":SortOrder"=>'999');
	
	//Set Update Information
	$set_info = array("SortOrder = :USortOrder");
	//Set where condition
	$where = array("OrgID = :OrgID", "ExporterID = :ExporterID", "SortOrder = :SortOrder");
	//Update Information
	$ExporterDataManagerObj->updExporterInfo('Exporter', $set_info, $where, $params);
	
	
} else if ($_REQUEST ['action'] == "down") {
	
	$plus = $item + 1;
	
	$params = array();
	
	//Set parameters
	$params[] = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":USortOrder"=>'999', ":SortOrder"=>$plus);
	//Set parameters
	$params[] = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":USortOrder"=>$plus, ":SortOrder"=>$item);	
	//Set parameters
	$params[] = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":USortOrder"=>$item, ":SortOrder"=>'999');
	
	//Set Update Information
	$set_info = array("SortOrder = :USortOrder");
	//Set where condition
	$where = array("OrgID = :OrgID", "ExporterID = :ExporterID", "SortOrder = :SortOrder");
	//Update Information
	$ExporterDataManagerObj->updExporterInfo('Exporter', $set_info, $where, $params);
}
?>
