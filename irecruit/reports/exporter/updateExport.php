#!/usr/local/bin/php -q
<?php
ini_set ('display_errors', 0);
require_once realpath(__DIR__. '/../..') . '/irecruitdb.inc';
require_once realpath(__DIR__) . '/class.exporter.php';

$ARGV=array();

foreach ($argv as $val) {
  list($key,$value,$value2) = explode(":",$val);
  if ($value != "") {
    if ($value2 != "") {
      $ARGV['OnboardFormID']=$value;
      $ARGV['Status']=$value2;
    } else {
      $ARGV[$key]=$value;
    }
  }
}

$StartDate     =   $ARGV['ReportDate_From'];
$EndDate       =   $ARGV['ReportDate_To'];
$BeginDate     =   substr ( $StartDate, - 4 ) . '-' . substr ( $StartDate, 0, 2 ) . '-' . substr ( $StartDate, 3, 2 );
$FinalDate     =   substr ( $EndDate, - 4 ) . '-' . substr ( $EndDate, 0, 2 ) . '-' . substr ( $EndDate, 3, 2 );


if ($ARGV['OnboardFormID'] != "") {

    //Get Job Applications based on Onboard Data
    $sel_info     =   "SELECT OA.ApplicationID, OA.RequestID";
    $sel_info    .=   " FROM OnboardApplications OA";
    $sel_info    .=   " WHERE OA.OrgID = :OrgID
                        AND OA.ProcessDate <= :EndDate
                        AND OA.ProcessDate >= :StartDate";
    $params       =   array(":OrgID"=>$ARGV['OrgID'], ":StartDate"=>$BeginDate, ":EndDate"=>$FinalDate);

    if ($ARGV['ApplicationID'] != "") {
        $sel_info    .=  " AND OA.ApplicationID = :ApplicationID";
        $params[":ApplicationID"] = $ARGV['ApplicationID'];
    }

    if ($ARGV['RequestID'] != "") {
        $sel_info    .=  " AND OA.RequestID = :RequestID";
        $params[":RequestID"] = $ARGV['RequestID'];
    }

    //Set OnboardFormID, Status
    $sel_info    .=  " AND OA.OnboardFormID = :OnboardFormID";
    $params[':OnboardFormID']   =   $ARGV['OnboardFormID'];
    $sel_info    .=  " AND OA.Status = :Status";
    $params[':Status']   =   $ARGV['Status'];

    $sel_info    .=  " ORDER BY OA.ApplicationID";

} else {

    //Get Job Applications based on Onboard Data
    $sel_info     =   "SELECT JA.ApplicationID, JA.RequestID FROM JobApplications JA";
    $sel_info    .=  " WHERE JA.OrgID = :OrgID";
	$sel_info    .=  " AND JA.EntryDate <= :EndDate";
	$sel_info    .=  " AND JA.EntryDate >= :StartDate";

    $params       =   array(":OrgID"=>$ARGV['OrgID'], ":StartDate"=>$BeginDate, ":EndDate"=>$FinalDate);

    if ($ARGV['ApplicationID'] != "") {
        $sel_info    .=  " AND JA.ApplicationID = :ApplicationID";
        $params[":ApplicationID"] = $ARGV['ApplicationID'];
    }

    if ($ARGV['RequestID'] != "") {
        $sel_info    .=  " AND JA.RequestID = :RequestID";
        $params[":RequestID"] = $ARGV['RequestID'];
    }

    $sel_info    .=  " ORDER BY JA.ApplicationID";

} // end else DataManager

$EXPRESULT     =   array ();
$EXPRESULT     =   $ApplicationsObj->getInfo($sel_info, array($params));

// parse data
$RES    =   array ();
$i      =   0;

foreach($EXPRESULT['results'] as $EXPR) {
    $EXPORTDATA   =   array();
    $EXPORTDATA   =   G::Obj('ExporterDataManager')->getExportData ( $ARGV['OrgID'], $EXPR['ApplicationID'], $EXPR['RequestID'], $ARGV['ExporterID'], $ARGV['UpperCase'], $ARGV['NameValue'], $ARGV['OnboardFormID']);
    $RES[$i]      =   $EXPORTDATA[0];
    $i++;
}

// format data
$ex     =   new EXPORTER ();
$rtn    .=  $ex->formatResults ( $RES, $ARGV['TypeExport'] );

//Set where condition
$where    =   array("OrgID = :OrgID", "ExporterID = :ExporterID");
//Set Inforamtion
$set_info =   array("ExportData = :ExportData", "LastAccessed = NOW()");
//Set parameters
$params   =   array(":ExportData"=>$rtn, ":OrgID"=>$ARGV['OrgID'], ":ExporterID"=>$ARGV['ExporterID']);
//Update ExporterNames Information
$ExporterDataManagerObj->updExporterInfo('ExporterNames', $set_info, $where, array($params));

if ($ARGV['ExportTo'] == "FILE") {

    //Set where condition
    $where      =   array("OrgID = :OrgID", "ExporterID = :ExporterID");
    //Set parameters
    $params     =   array(":OrgID"=>$ARGV['OrgID'], ":ExporterID"=>$ARGV['ExporterID']);
    //Set columns
    $columns    =   "ExportName, ExportData";
    //Get ExporterNames Information
    $results    =   $ExporterDataManagerObj->getExporterInfo('ExporterNames', $columns, $where, "", array($params));

    if(is_array($results['results'][0])) {
        list ( $ExportName, $file ) = array_values($results['results'][0]);
    }


    $dfile      =   preg_replace ( '/[^a-zA-Z0-9]/', '', $ExportName ) . "." . strtolower ( $ARGV['TypeExport'] );

    $file_name  =       $ARGV['USERID']."-".date('Y-m-d-H-i-s')."-".$dfile;
    $file_path  =       IRECRUIT_DIR."export-temp/".$file_name;

    $dir                =       IRECRUIT_DIR."export-temp";
    if (! file_exists($dir)) {
        mkdir($dir, 0777);
        chmod($dir, 0777);
    }

    $fp = fopen($file_path, 'w+');
    fwrite($fp, $file);
    fclose($fp);

    $user_info  =       G::Obj('IrecruitUsers')->getUserInfoByUserID($ARGV['USERID'], "EmailAddress, FirstName, LastName");
    $to_mail    =       $user_info['EmailAddress'];

    unset($user_info['EmailAddress']);

    $message    =       "Dear ".implode(" ", array_values($user_info)) . ", <br>";
    $message    .=      "<br>";
    $message    .=      "Please <a href='".IRECRUIT_HOME."downloadTempData.php?f=".$file_name."'>click here</a> to download the exported data<br>";
    $message    .=      "<br>";
    $message    .=      "Thanks<br>";
    $message    .=      "iRecruit Support Team<br>";

    $subject    =       "Please find the exported data file link";
    $to         =       $to_mail;

    G::Obj('PHPMailer')->clearCustomProperties();
    G::Obj('PHPMailer')->clearCustomHeaders();

    // Set who the message is to be sent to
    G::Obj('PHPMailer')->addAddress($to);
    // Set the subject line
    G::Obj('PHPMailer')->Subject = $subject;
    // convert HTML into a basic plain-text alternative body
    G::Obj('PHPMailer')->msgHTML($message);
    // Attach an coverletter file
    G::Obj('PHPMailer')->addAttachment ( $dfile );
    // Content Type Is HTML
    G::Obj('PHPMailer')->ContentType = 'text/plain';
    // Send email
    G::Obj('PHPMailer')->send();

}
?>
