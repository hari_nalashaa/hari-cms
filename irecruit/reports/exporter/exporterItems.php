<?php
require_once '../../Configuration.inc';

$ExporterID = $_REQUEST ['ExporterID'];
$d = $_REQUEST['d'];

if (($ExporterID != "") && ($ExporterID != "New")) {
	
	$FORMS         =   array (
                            'ApplicationForm'       =>  'Application Form',
                            'AffirmativeActionForm' =>  'Affirmative Action Form',
                            'DisabledForm'          =>  'Disabled Form',
                            'VeteranForm'           =>  'Veteran Form',
                            'WebForm'               =>  'Web Form',
                            'PreFilledForm'         =>  'Federal & State Form',
                            'AgreementForm'         =>  'Agreement Form',
                            'OnboardQuestions'      =>  'Onboard Questions',
                            'OnboardApplications'   =>  'Onboard Applications',
                            'OrgData'               =>  'Organizational Data',
                            'Requisition'           =>  'Requisition',
                            'RequisitionOrgLevels'  =>  'Requisition Org Levels',
                            'JobApplication'        =>  'Job Application' 
                        );
	
	$QUESTIONTYPES =   array ();

	// Get QuestionTypes Information
	$resultsIN =   G::Obj('QuestionTypes')->getQuestionTypesInfo ( "*" );
	
	if (is_array ( $resultsIN ['results'] )) {
		foreach ( $resultsIN ['results'] as $QT ) {
			$QUESTIONTYPES [$QT ['QuestionTypeID']] = $QT ['Description'];
		} // end foreach
	}
	
	$MULTI     =   array ();
	
	// Set parameters
	$params    =   array (":OrgID" => $OrgID);
	// Set where condition
	$where     =   array ("OrgID = :OrgID", "SectionID = 0");
	// Get FormQuestions Information
	$resultsIN =   $FormQuestionsObj->getFormQuestionsInformation ( "QuestionID, value", $where, "", array ($params) );
	
	if (is_array ( $resultsIN ['results'] )) {
		foreach ( $resultsIN ['results'] as $FQ ) {
			$MULTI [$FQ ['QuestionID']] = $FQ ['value'];
		}
	}
	
	// List table of entries
	// Set where condition
	$where     =   array ("OrgID = :OrgID", "ExporterID = :ExporterID");
	// Set paramters
	$params    =   array (":OrgID" => $OrgID, ":ExporterID" => $ExporterID);
	// Get Exporter Information
	$results   =   $ExporterDataManagerObj->getExporterInfo ( 'Exporter', "*", $where, "SortOrder", array ($params) );
	
	$cnt       =   $results ['count'];

	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $EXPORT ) {
			
			// up and down and delete navigation
			echo '<div style="float:left;valign:top;width:50px;height:30px;text-align:center;padding-top:3px;">';
			
			if ($EXPORT ['SortOrder'] > 1) {
				echo '<a href="#" onclick="exporterAction(\'' . $OrgID . '\',\'up\',\'' . $EXPORT ['ExporterID'] . ':' . $EXPORT ['SortOrder'] . '\',\'\');return false;">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_up.png" width="20" height="16" title="Up" border="0" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
			}
			
			if ($EXPORT ['SortOrder'] < $cnt) {
				echo '<a href="#" onclick="exporterAction(\'' . $OrgID . '\',\'down\',\'' . $EXPORT ['ExporterID'] . ':' . $EXPORT ['SortOrder'] . '\',\'\');return false;">';
				echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_down.png" width="20" height="16" title="Down" border="0" style="margin:0px 3px -4px 0px;">';
				echo '</a>';
			}
			
			echo '</div>';
			echo '<div style="float:left;valign:top;width:35px;min-height:30px;text-align:left;padding-top:2px;">';
			
			echo '<a href="#" onclick="exporterAction(\'' . $OrgID . '\',\'delete\',\'' . $EXPORT ['ExporterID'] . ':' . $EXPORT ['SortOrder'] . '\',\'Are you sure you want to delete:\n' . $EXPORT ['Question'] . '?\n\n\');return false;">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" height="16" title="Remove" border="0" style="margin:0px 3px -4px 0px;">';
			echo '</a>';
			
			echo '</div>';
			
			$PERSONAL    =   array(
            					'first'    =>  'First Name',
            					'middle'   =>  'Middle Name',
            					'last'     =>  'Last Name',
            					'address'  =>  'Address',
            					'address2' =>  'Second Address',
            					'city'     =>  'City',
            					'state'    =>  'State',
            					'zip'      =>  'Zip',
            					'email'    =>  'Email Address' 
                             );
			
			// Initial Data displayed for easy update to friendly name
			echo '<div style="margin-bottom:10px;margin-left:90px;position: relative;">';
			
			if($EXPORT['FormID'] != "") {
				echo "<strong>FormID:</strong> ". $EXPORT['FormID'] .  '<br>';
			}
			
			echo '<b>Question</b>: ';
			if ($EXPORT ['SectionID'] == "1") {
				if ($PERSONAL [$EXPORT ['QuestionID']]) {
					echo $PERSONAL [$EXPORT ['QuestionID']] . '<br>';
				} else {
					echo $EXPORT ['Question'] . '<br>';
				}
			} else {
				echo $EXPORT ['Question'] . '<br>';
			}
			
			echo '<b>iRecruit Code</b>: ';
			echo ' {' . $EXPORT ['QuestionID'] . "}";
			
			echo "&nbsp;&nbsp;&nbsp;";
			echo '<b>Friendly Name</b>: ' . '<input type="text" id="' . $EXPORT ['ExporterID'] . ':' . $EXPORT ['QuestionID'] . ':' . $EXPORT ['FormID'] . '" size="10" maxlength = "45" value="' . $EXPORT ['FriendlyName'] . '" onchange="exporterAction(\'' . $OrgID . '\',\'fnupdate\',\'' . $EXPORT ['ExporterID'] . ':' . $EXPORT ['QuestionID'] . ':' . $EXPORT ['FormID'] . '\',\'\')">';
			
			echo "&nbsp;&nbsp;&nbsp;";
			echo '<a href="javascript:ReverseDisplay(\'' . $EXPORT ['FormType'] . $EXPORT ['QuestionID'] . '\')">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<i style="FONT-SIZE:8pt;COLOR:#000000">';
			echo 'Details';
			echo '</i></a>';
			echo '<br>';
			
			// Additional Data
			echo '<div id="' . $EXPORT ['FormType'] . $EXPORT ['QuestionID'] . '" style="display:none;">';
			
			// Form Type and Form Name
			echo '<b>Form Type</b>: ';
			echo $FORMS [$EXPORT ['FormType']];
			
			if ($EXPORT ['FormID']) {
				echo "&nbsp;&nbsp;&nbsp;";
				echo '<b>Form Name</b>: ';
				
				if ($EXPORT ['FormType'] == "WebForm") {
					
					// Set where condition
					$where     =   array (
                                        "OrgID       =   :OrgID",
                                        "WebFormID   =   :WebFormID",
                                        "FormStatus !=   'Deleted'"
                					);
					// Set parameters
					$params    =   array (
            							":OrgID"     =>  $OrgID,
            							":WebFormID" =>  $EXPORT ['FormID']
                					);
					// Get FormName from WebForms Information
					$results   =   $FormsInternalObj->getWebFormsInfo ( "FormName", $where, "", array ($params) );
					// Get FormName
					$FormName  =   $results ['results'] [0] ["FormName"];
					
				} else if ($EXPORT ['FormType'] == "AgreementForm") {
					
					// Set where condition
					$where     =   array (
                                        "OrgID              =   :OrgID",
                                        "AgreementFormID    =   :AgreementFormID",
                                        "FormStatus         !=  'Deleted'"
                                    );
					// Set paramters
					$params    =   array (
            							":OrgID"             =>  $OrgID,
            							":AgreementFormID"   =>  $EXPORT ['FormID']
                					);
					// Get AgreementForms Information
					$results = $FormsInternalObj->getAgreementFormsInfo ( "FormName", $where, "", array (
							$params 
					) );
					// Get FormName
					$FormName = $results ['results'] [0] ["FormName"];
				} else if ($EXPORT ['FormType'] == "PreFilledForm") {
					
					// Set where condition
					$where = array (
							"OrgID = :OrgID",
							"PreFilledFormID = :PreFilledFormID" 
					);
					// Set paramters
					$params = array (
							":OrgID" => $OrgID,
							":PreFilledFormID" => $EXPORT ['FormID'] 
					);
					// Get PrefilledForms Information
					$results = $FormsInternalObj->getPrefilledFormsInfo ( "concat(TypeForm,': ',FormName) AS TypeFormFormName", $where, "", array (
							$params 
					) );
					// Get FormName
					$FormName = $results ['results'] [0] ["TypeFormFormName"];
				} else {
					$FormName = $EXPORT ['FormID'];
				}
				
				echo $FormName;
			}
			
			echo '<br>';
			
			// Question Type and Expected Result
			$value = "";
			
			if ($EXPORT ['FormType'] == "ApplicationForm") {
				
				$QuestionID = $EXPORT ['QuestionID'];
				
				if ($EXPORT ['Multi'] != "") {
					$skip = "/^" . $EXPORT ['Multi'] . "/";
					$QuestionID = preg_replace ( $skip, '', $EXPORT ['QuestionID'] );
				}
				
				// COLLATE latin1_general_cs //Set where condition
				$where = array (
						"OrgID        =   :OrgID",
						"FormID       =   :FormID",
						"SectionID    =   :SectionID",
						"QuestionID   =   :QuestionID" 
				);
				// Set parameters
				$params = array (
						":OrgID"      =>  $OrgID,
						":FormID"     =>  $EXPORT ['FormID'],
						":SectionID"  =>  $EXPORT ['SectionID'],
						":QuestionID" =>  $QuestionID 
				);
				// Get FormQuestions Information
				$results = $FormQuestionsObj->getFormQuestionsInformation ( "value", $where, "", array ($params) );
				
				if (is_array ( $results ['results'] [0] )) {
					list ( $value ) = array_values ( $results ['results'] [0] );
				}
			} else if ($EXPORT ['FormType'] == "AffirmativeActionForm") {

			    //Get Affirmative Action question information value
			    $aa_que_info    =   $FormQuestionsObj->getQuestionDetails("value", "FormQuestions", $OrgID, $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $aa_que_info['value'];
				
			} else if ($EXPORT ['FormType'] == "DisabledForm") {
				
				//Get Affirmative Action question information value
			    $dis_que_info   =   $FormQuestionsObj->getQuestionDetails("value", "FormQuestions", $OrgID, $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $dis_que_info['value'];
				
			} else if ($EXPORT ['FormType'] == "VeteranForm") {
				
				//Get Affirmative Action question information value
			    $vet_que_info   =   $FormQuestionsObj->getQuestionDetails("value", "FormQuestions", $OrgID, $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $vet_que_info['value'];
				
			} else if ($EXPORT ['FormType'] == "WebForm") {

			    //Get Web Form Question Info Value
				$web_que_info   =   $WebFormQuestionsObj->getQuestionDetails("value", $OrgID, $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $web_que_info['value'];
				
				
			} else if ($EXPORT ['FormType'] == "PreFilledForm") {
				
			    //Get PreFilled Form Question info value				
				$pre_que_info   =   $PreFilledFormQuestionsObj->getQuestionDetails("value", 'MASTER', $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $pre_que_info['value'];
				
			} else if ($EXPORT ['FormType'] == "AgreementForm") {
				
				//Get Agreement Form Question info value
				$agr_que_info   =   $AgreementFormQuestionsObj->getQuestionDetails("value", $OrgID, $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $agr_que_info['value'];
				
			} else if ($EXPORT ['FormType'] == "OnboardQuestions") {
				
				// Get value OnboardQuestions Information
				$onb_que_info   =   $OnboardQuestionsObj->getOnboardQuestionInfo($OrgID, $EXPORT ['FormID'], $EXPORT ['QuestionID']);
				$value          =   $onb_que_info['value'];
				
			} // end
			
			echo '<b>Question Type</b>: ';
			echo $QUESTIONTYPES [$EXPORT ['QuestionTypeID']];
			echo "<br>";
			
			echo '<b>Expected Result</b>: ';
			
			if ($EXPORT ['QuestionTypeID'] == "1") { // Date From and Two - 2 part
				
				echo '(Two Dates) MM/DD/YYYY|MM/DD/YYYY';
			} else if ($EXPORT ['QuestionTypeID'] == "8") { // File Upload
				
				echo 'Location URL of file';
			} else if ($EXPORT ['QuestionTypeID'] == "9") { // Checkbox with Year and Comment - 3 part multi
				
				echo '(3 Parts repeated for number of selections) Yr|Selection|Comment!!';
				echo '<br>Selection Choices: ' . displayOptions ( $value );
			} else if ($EXPORT ['QuestionTypeID'] == "13") { // Phone - 3 part
				
				echo '(3 Parts) Text-Text-Text';
			} else if ($EXPORT ['QuestionTypeID'] == "14") { // Phone with Ext - 4 part
				
				echo '(4 Parts) Text-Text-Text-Text';
			} else if ($EXPORT ['QuestionTypeID'] == "15") { // SSN - 3 part
				
				echo '(3 Parts) ddd-dd-dddd';
			} else if ($EXPORT ['QuestionTypeID'] == "24") { // Date From to Present - 2 part
				
				echo '(Two Dates) MM/DD/YYYY|MM/DD/YYYY';
			} else if ($EXPORT ['QuestionTypeID'] == "17") { // Date
				
				if (($EXPORT['FormType'] == "OnboardApplications") || ($EXPORT ['FormType'] == "OrgData") || ($EXPORT ['FormType'] == "Requisition") || ($EXPORT ['FormType'] == "JobApplication")) {
					echo 'YYYY-MM-DD hh:mm:ss';
				} else {
					echo 'MM/DD/YYYY';
				}
			} else if ($EXPORT ['QuestionTypeID'] == "30") { // Signature
				
				echo 'Symbol|MM/DD/YYYY';
			} else if (($EXPORT ['FormType'] == "JobApplication") && ($EXPORT ['QuestionID'] == "ProcessOrder")) {
				
				// Set where condition
				$where      =   array ("OrgID = :OrgID","Active = 'Y'");
				// Set parameters
				$params     =   array (":OrgID" => $OrgID);
				// Get Applicant Process Flow Information
				$resultsIN  =   $ApplicantsObj->getApplicantProcessFlowInfo ( "*", $where, 'ProcessOrder', array ($params) );
				
				if (is_array ( $resultsIN ['results'] )) {
					foreach ( $resultsIN ['results'] as $APF ) {
						echo '<b>' . $APF ['Description'] . '</b>: {' . $APF ['ProcessOrder'] . '}&nbsp;&nbsp;';
					} // end foreach
				}
			} else if (($EXPORT ['FormType'] == "JobApplication") && ($EXPORT ['QuestionID'] == "DispositionCode")) {
				
				$resultsIN  =   $ApplicantsObj->getApplicantDispositionCodes ( $OrgID, 'Y' );
				
				if (is_array ( $resultsIN ['results'] )) {
					foreach ( $resultsIN ['results'] as $ADC ) {
						echo '<b>' . $ADC ['Description'] . '</b>: {' . $ADC ['Code'] . '}&nbsp;&nbsp;';
					} // end foreach
				}
			} else if ($value) {
				
				echo displayOptions ( $value );
			} else {
				
				echo 'Text';
			} // end
			
			echo "<br>";
			
			echo '</div>';
			
			echo '<div style="clear:both;"></div>';
			
			echo '</div>';
		} // end foreach
	}
	
	if ($cnt > 0) {
		echo '<div style="width:800px;margin:10px auto 0 auto;text-align:center;padding:5px;">Friendly Names must be unique identifiers for the data within this export and not contain special characters or begin with a number.</div>';
	}
} // end ExporterID

function displayOptions($val) {
	$rtn = '';
	
	$V = explode ( '::', $val );
	
	foreach ( $V as $namepair ) {
		
		list ( $value, $displayname ) = explode ( ':', $namepair );
		
		$rtn .= '<b>' . $displayname . '</b>: {' . $value . '}&nbsp;&nbsp;';
	}
	
	return $rtn;
} // end function
?>
