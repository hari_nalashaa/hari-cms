<?php
class EXPORTER {

	public function formatResults($result, $TypeExport) {
		$rtn = "";
		
		// Comma seperated values
		if ($TypeExport == "CSV") {
			
			if(is_array($result [0])) {
				foreach ( $result [0] as $index => $post ) {
					$rtn .= "\"" . $index . "\",";
				}
				$rtn .= "\n";
			}
			
			if(is_array($result)) {
				foreach ( $result as $index => $post ) {
					if(is_array($post)) {
						foreach ( $post as $key => $value ) {
								
							if(substr($key, 0, 4) == "CUST")
							{
								$u_value = @unserialize($value);

								if($u_value !== false) {
						
									$assoc_array_keys = $this->multiarray_keys($u_value);
						
									if(@in_array("days", $assoc_array_keys)) {
										$s = "";
										for($a = 0; $a < count($u_value['days']); $a++) {
											$s .= $u_value['days'][$a]." ".$u_value['from_time'][$a]."-".$u_value['to_time'][$a].",";
										}
										$s = trim($s, ",");
										$rtn .= "\"" . preg_replace ( "/\"/", "\\\"", $s ) . "\",";
									}
									else if (@in_array("LabelSelect1", $assoc_array_keys)) {
										$r = "";
										foreach($u_value as $lkey => $lvalue) {
											if(is_array($lvalue)) {
												foreach($lvalue as $lrating) {
													$r .= $lkey . " - " . $lrating.",";
												}
											}
						
										}
										$r = trim($r, ",");
										$rtn .= "\"" . preg_replace ( "/\"/", "\\\"", $r ) . "\",";
									}
									else {
										$rtn .= "\"" . preg_replace ( "/\"/", "\\\"", $value ) . "\",";
									}
						
								}
								else {
								    $rtn .= "\"" . preg_replace ( "/\"/", "\\\"", $value ) . "\",";
								}
							}
							else {
								$rtn .= "\"" . preg_replace ( "/\"/", "\\\"", $value ) . "\",";
							}
								
						}
					}
					
					$rtn .= "\n";
				}
			}
			
			
			// Extensible Markup Language
		} else if ($TypeExport == "XML") {
			
			$rtn .= "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n";
			$rtn .= "<Exporter>\n";
			
			foreach ( $result as $index => $post ) {
				
				$rtn .= '<Applicant' . $index . '>' . "\n";
				
				foreach ( $post as $key => $value ) {
					if(substr($key, 0, 4) == "CUST")
					{
						$rtn .= '<' . $key . '><![CDATA[';
						
						$u_value = @unserialize($value);
						
						if($u_value) {
							
							$assoc_array_keys = $this->multiarray_keys($u_value);
							
							if(@in_array("days", $assoc_array_keys)) {
								for($a = 0; $a < count($u_value['days']); $a++) {
									$rtn .= "\n" . $u_value['days'][$a]." ".$u_value['from_time'][$a]."-".$u_value['to_time'][$a];
								}
							}
							else if (@in_array("LabelSelect1", $assoc_array_keys)) {
									
								foreach($u_value as $lkey => $lvalue) {
									if(is_array($lvalue)) {
										foreach($lvalue as $lrating) {
											$rtn .= "\n" . $lkey . " - " . $lrating;
										}
									}
									
								}
							}
							else {
								$rtn .= $value;
							}
							
						}
						$rtn .= ']]></' . $key . '>' . "\n";
					}
					else {
						$rtn .= '<' . $key . '>';
						$rtn .= "<![CDATA[";
						$rtn .= $value;
						$rtn .= "]]>";
						$rtn .= '</' . $key . '>' . "\n";
					}
					
				}
				
				$rtn .= '  </Applicant' . $index . '>' . "\n";
			}
			
			$rtn .= "</Exporter>\n";
			
			// JavaScript Object Notation
		} else if ($TypeExport == "JSON") {
			
			$result = $this->unserialize_custom_que_value($result);
			
			$json = json_encode ( $result );
			$rtn .= $json;
			
		} else {
			
			$result = $this->unserialize_custom_que_value($result);
			$rtn .= print_r ( $result, true );
		}
		
		return $rtn;
	} // end function
	
	function unserialize_custom_que_value($result) {
		/**
		 * Added on August 29th to
		 * unserialize the custom questions
		 * values
		 */
		if(is_array($result)) {
			foreach($result as $key=>$value) {
					
				if(is_array($value)) {
					foreach ($value as $QuestionName=>$Answer) {
						if(substr($QuestionName, 0, 4) == "CUST")
						{
							if($Answer != "") {
			
								$unserialized_array = @unserialize($Answer);
								@ksort($unserialized_array);
			
			
								$assoc_array_keys = $this->multiarray_keys($unserialized_array);
			
			
								if(@in_array("days", $assoc_array_keys)) {
									$result[$key][$QuestionName] = $unserialized_array;
								}
								else if (@in_array("LabelSelect1", $assoc_array_keys)) {
									$rating = array();
									foreach($unserialized_array as $lkey => $lvalue) {
										if(is_array($lvalue)) {
											foreach($lvalue as $lrating) {
												$rating[$lkey] = $lrating;
											}
										}
									}
									$result[$key][$QuestionName] = $rating;
										
								}
			
			
							}
						}
					}
				}
					
			}
		}
		
		return $result;
	}
	
	function multiarray_keys($ar) {

        foreach($ar as $k => $v) {
            $keys[] = $k;
            if (is_array($ar[$k]))
                $keys = array_merge($keys, $this->multiarray_keys($ar[$k]));
        }
        return $keys;
    }
	
	
} // end class

?>
