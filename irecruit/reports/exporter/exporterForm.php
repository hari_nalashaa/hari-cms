<?php
require_once '../../Configuration.inc';

$FormType       =   isset($_REQUEST['FormType']) ? $_REQUEST['FormType'] : '';
$ExporterID     =   isset($_REQUEST['ExporterID']) ? $_REQUEST['ExporterID'] : '';
$action         =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$FormID         =   isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';
$SectionID      =   isset($_REQUEST['SectionID']) ? $_REQUEST['SectionID'] : '';
$QuestionID     =   isset($_REQUEST['QuestionID']) ? $_REQUEST['QuestionID'] : '';
$d              =   isset($_REQUEST['d']) ? $_REQUEST['d'] : '';

if ($_POST ['action'] == "ExporterName") {
	
	if ($_POST ['Name'] != "") {
		
		$ExporterID   =   uniqid ();
		
		// Insert ExporterNames Information
		$info         =   array (
                                "OrgID"         =>  $OrgID,
                                "ExporterID"    =>  $ExporterID,
                                "ExportName"    =>  $_POST ['Name'],
                                "LastAccessed"  =>  "NOW()" 
                            );
		$ExporterDataManagerObj->insExporterInfo ( 'ExporterNames', $info );
	}
} // end action ExporterName
  

// Set where condition
$where          =   array ("OrgID = :OrgID", "ExporterID != 'DataManager'");
// Set params
$params         =   array (":OrgID" => $OrgID);
// Get Exporter Information
$resultsNames   =   $ExporterDataManagerObj->getExporterInfo ( 'ExporterNames', "*", $where, "ExportName", array ($params) );
// Get ExporterNames Count
$namecnt        =   $resultsNames ['count'];


$SCHEDULED      =   array ();

echo '<div style="float:left">';
echo '<strong style="font-size:14pt;">Select Data Fields</strong>';
echo '</div>';

if ($namecnt > 0) {
	echo '<div style="float:right;margin-top:10px;">';
	echo '<a href="#openFormWindow" class="button" onclick="loadExporterForm(\'exporterExport.php\',\'\');">';
	echo '<img src="' . IRECRUIT_HOME . 'images/icons/sitemap_color.png" width="16" height="16" title="Export Data" border="0" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Export Data</b>';
	echo '</a>';
	echo '</div>';
} // end namecnt

echo '<div style="clear:both;margin-bottom:10px;">';
echo '</div>';

if (($namecnt > 0) && ($_POST ['ExporterID'] != "New")) {
	
	echo '<form id="exporterid" name="exporterid">';
	echo 'Editing: ';
	echo '<select name="ExporterID" id="ExporterID" onchange="loadExporterForm(\'exporterForm.php\',\'ExporterID\');">' . "\n";
	$i = 0;
	
	if (is_array ( $resultsNames ['results'] )) {
		foreach ( $resultsNames ['results'] as $EXPORTNAMES ) {
			$i ++;
			
			if ($i == 1) {
				$ExporterID = $EXPORTNAMES ['ExporterID'];
				$ExportName = $EXPORTNAMES ['ExportName'];
			}
			echo '<option value="' . $EXPORTNAMES ['ExporterID'] . '"';
			if ($EXPORTNAMES ['ExporterID'] == $_POST ['ExporterID']) {
				$ExporterID = $EXPORTNAMES ['ExporterID'];
				$ExportName = $EXPORTNAMES ['ExportName'];
				echo ' selected';
			}
			echo '>';
			echo $EXPORTNAMES ['ExportName'];
			echo '</option>' . "\n";
		} // end foreach
	}
	
	echo '<option value="New"';
	if ($_POST ['ExporterID'] == "New") {
		echo ' selected';
	}
	echo '>Create Export List</option>' . "\n";
	echo '</select>' . "\n";
	
	if (! in_array ( $ExporterID, $SCHEDULED )) {
	    echo '<a href="#" onclick="exporterAction(\'' . htmlspecialchars($OrgID) . '\',\'deleteExport\',\'' . htmlspecialchars($ExporterID) . ':xx' . '\',\'Are you sure you want to delete export:\n' . $ExportName . '?\n\n\');return false;">';
		echo '<img src="' . IRECRUIT_HOME . 'images/icons/cross.png" height="16" title="Remove" border="0" style="margin:0px 3px -4px 0px;">';
		echo '</a>';
	} // end if NOT SCHEDULED
	echo '</form>';
	
	$added = "";
	
	if ($action == "") {
		$action       =   "Initialize";
	}
	
	if ($action == "Initialize") {
		$FormType     =   "";
		$FormID       =   "";
		$SectionID    =   "";
		$QuestionID   =   "";
	}
	if ($action == "FormType") {
		$FormID       =   "";
	}
	if ($action == "FormID") {
		$SectionID    =   "";
		$QuestionID   =   "";
	}
	if ($action == "SectionID") {
		$QuestionID   =   "";
	}
	if ($action == 'QuestionID') {
		
		$Multi = 0;
		
		list ( $QID, $QuesType, $Ques, $Type, $Multi ) = explode ( '|', $QuestionID );
		
		$FormIDins = $FormID;
		if ($Type) {
			$FormIDins = $Type;
		}
		if (! $SectionID) {
			$SectionID = 0;
		}
		
		// Set where condition
		$where        =   array ("OrgID = :OrgID", "ExporterID = :ExporterID");
		// Set parameters
		$params       =   array (":OrgID" => $OrgID, ":ExporterID" => $ExporterID);
		// Get Exporter Information
		$results      =   $ExporterDataManagerObj->getExporterInfo ( 'Exporter', 'MAX(SortOrder) as MAXSortOrder', $where, "", array ($params) );
		// Get SortOrder Information
		$SortOrder    =   $results ['results'] [0] ['MAXSortOrder'];
		
		$SortOrder ++;
		
		// //****** Need to handle multi part questions ******////
		if ($Multi > 0) {
			
		    $ALPHA1 = array ('XX');
		    $ALPHA2 = $GenericLibraryObj->getAlphabets();
		    $ALPHA  = array_merge($ALPHA1, $ALPHA2);
		    
			for($mcnt = 1; $mcnt <= $Multi; $mcnt ++) {
				
				// Set Exporter information to insert
				$info = array (
						"OrgID"           =>  $OrgID,
						"ExporterID"      =>  $ExporterID,
						"FormType"        =>  $FormType,
						"FormID"          =>  $FormIDins,
						"SectionID"       =>  $SectionID,
						"QuestionID"      =>  $ALPHA [$mcnt] . $QID,
						"QuestionTypeID"  =>  $QuesType,
						"Question"        =>  $Ques,
						"SortOrder"       =>  $SortOrder,
						"Multi"           =>  $ALPHA [$mcnt] 
				);
				// On Update
				$on_update = " ON DUPLICATE KEY UPDATE QuestionTypeID = :UQuestionTypeID, Question = :UQuestion, SortOrder = :USortOrder, Multi = :UMulti";
				// Update Information
				$update_info = array (
						":UQuestionTypeID"    =>  $QuesType,
						":UQuestion"          =>  $Ques,
						":USortOrder"         =>  $SortOrder,
						":UMulti"             =>  $ALPHA [$mcnt] 
				);
				// Insert Exporter Information
				$ExporterDataManagerObj->insExporterInfo ( 'Exporter', $info, $on_update, $update_info );
			}
		} else { // else Multi
		         
			// Set Exporter information to insert
			$info = array (
					"OrgID"            =>  $OrgID,
					"ExporterID"       =>  $ExporterID,
					"FormType"         =>  $FormType,
					"FormID"           =>  $FormIDins,
					"SectionID"        =>  $SectionID,
					"QuestionID"       =>  $QID,
					"QuestionTypeID"   =>  $QuesType,
					"Question"         =>  $Ques,
					"SortOrder"        =>  $SortOrder 
			);
			// On Update
			$on_update = " ON DUPLICATE KEY UPDATE QuestionTypeID = :UQuestionTypeID, Question = :UQuestion, SortOrder = :USortOrder";
			// Update Information
			$update_info = array (
					":UQuestionTypeID" =>  $QuesType,
					":UQuestion"       =>  $Ques,
					":USortOrder"      =>  $SortOrder 
			);
			// Insert Exporter Information
			$ExporterDataManagerObj->insExporterInfo ( 'Exporter', $info, $on_update, $update_info );
		} // end else Multi
		  
		// assure the order even afer and update for duplicate
		$ExporterDataManagerObj->setAndUpdateExporterInfo($OrgID, $ExporterID);
		
		$QuestionID = "";
		$added = "Success. You have added question: " . $Ques;
	} // end QuestionID
	else if($action == 'OrgLevelIDs') {
		
	    $columns   =   'MAX(SortOrder) as MAXSortOrder';
	    // Set where condition
	    $where     =   array ("OrgID = :OrgID", "ExporterID = :ExporterID");
	    // Set parameters
	    $params    =   array (":OrgID" => $OrgID, ":ExporterID" => $ExporterID);
	    // Get Exporter Information
	    $results   =   $ExporterDataManagerObj->getExporterInfo ( 'Exporter', $columns, $where, "", array ($params) );
	    // Get SortOrder Information
	    $SortOrder =   $results ['results'] [0] ['MAXSortOrder'];
	    
	    $SortOrder ++;
	    
	    list($ReqOrgLevelOrgID, $ReqOrgLevelMultiOrgID) = explode("-", $_REQUEST['OrgsList']);
	    
	    $OrgLevelInfo  =    $OrganizationLevelsObj->getOrgLevelInfo($OrgID, $ReqOrgLevelMultiOrgID, $_REQUEST['OrgLevelIDs']);
	    
	    // Set Exporter information to insert
		$info = array (
                "OrgID"             =>  $OrgID,
                "ExporterID"        =>  $ExporterID,
                "FormType"          =>  'RequisitionOrgLevels',
                "FormID"            =>  $ReqOrgLevelMultiOrgID,
                "SectionID"         =>  0,
                "QuestionID"        =>  $_REQUEST['OrgLevelIDs'],
                "FriendlyName"      =>  $OrgLevelInfo['OrganizationLevel'],
                "QuestionTypeID"    =>  3,
                "Question"          =>  $OrgLevelInfo['OrganizationLevel'],
                "SortOrder"         =>  $SortOrder 
		);
		// On Update
		$on_update = " ON DUPLICATE KEY UPDATE QuestionTypeID = :UQuestionTypeID, Question = :UQuestion, SortOrder = :USortOrder, FriendlyName = :UFriendlyName";
		// Update Information
		$update_info = array (
                ":UQuestionTypeID"  => 3,
                ":UQuestion"        => $OrgLevelInfo['OrganizationLevel'],
                ":UFriendlyName"    => $OrgLevelInfo['OrganizationLevel'],
                ":USortOrder"       => $SortOrder 
		);
		// Insert Exporter Information
		$ExporterDataManagerObj->insExporterInfo ( 'Exporter', $info, $on_update, $update_info );
			
	    $QuestionID = "";
	    $added = "Success. You have added question: " . $OrgLevelInfo['OrganizationLevel'];
	} //end process
	  
	// FormType
	echo '<div style="border:1px solid #000000;background-color:#eeeeee;margin-top:10px;margin-bottom:20px;padding:10px;border-radius:10px;box-shadow: 2px 2px 2px #888888;">';
	
	if ($added) {
		echo '<div style="padding:3px;">';
		echo '<strong style="color:red;">' . $added . '</strong><br>';
		echo '</div>';
	}
	
	echo '<form id="exporterform" name="exporterform">';
	echo '<input type="hidden" name="ExporterID" value="' . $ExporterID . '">';
	echo '<div style="padding:3px;float:left;">';
	
	echo 'Form Type: <select name="FormType" onchange="loadExporterForm(\'exporterForm.php\',\'FormType\');" style="width:140px;">';
	echo '<option value="">Select Here</option>';
	
	$FORMS = array (
        'ApplicationForm'           =>  'Application Form',
        'AffirmativeActionForm'     =>  'Affirmative Action Form',
        'DisabledForm'              =>  'Disabled Form',
        'VeteranForm'               =>  'Veteran Form',
        'WebForm'                   =>  'Web Form',
        'PreFilledForm'             =>  'Federal & State Form',
        'AgreementForm'             =>  'Agreement Form',
        'OnboardForm'               =>  'Onboard Form',
        'OnboardApplications'       =>  'Onboard Applications',
        'OrgData'                   =>  'Organizational Data',
        'Requisition'               =>  'Requisition',
        'RequisitionOrgLevels'      =>  'Requisition Org Levels',
        'JobApplication'            =>  'Job Application',
        'DataManager'               =>  'DataManager' 
	);
	
	
	//Set parameters
	$params    =   array(":OrgID"=>$OrgID);
	//Set where condition
	$where     =   array("OrgID = :OrgID");
	//Get FormQuestions Information
	$results   =   $FormQuestionsObj->getFormQuestionsInformation("DISTINCT(FormID) AS DISTINCTFormID", $where, "FormID", array($params));
	
	if ($results['count'] > 0) {
		echo '<option value="ApplicationForm"';
		if ($FormType == "ApplicationForm") {
			echo ' selected';
		}
		echo '>' . $FORMS ['ApplicationForm'] . '</option>';
	} // end cnt
	
	if ($feature ['WebForms'] == "Y") {
		//Set parameters
		$params   =   array(":OrgID"=>$OrgID);
		//Set where condition
		$where    =   array("OrgID = :OrgID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
		//Get QuestionsInformation
		$results  =   $FormQuestionsObj->getQuestionsInformation('WebFormQuestions', "DISTINCT(WebFormID) AS DISTINCTWebFormID", $where, "", array($params));
		
		if($results['count'] > 0) {
			echo '<option value="WebForm"';
			if ($FormType == "WebForm") {
				echo ' selected';
			}
			echo '>' . $FORMS ['WebForm'] . '</option>';
		} // end cnt
	} // end feature
	
	if ($feature ['PreFilledForms'] == "Y") {
		//Set parameters		
		$params   =   array(":OrgID"=>$OrgID);
		//Get Prefilled Forms Information
		$results  =   $FormsInternalObj->getPrefilledFormsInfo("PreFilledFormID", array("OrgID = :OrgID"), "", array($params));
		
		if ($results['count'] > 0) {
			echo '<option value="PreFilledForm"';
			if ($FormType == "PreFilledForm") {
				echo ' selected';
			}
			echo '>' . $FORMS ['PreFilledForm'] . '</option>';
		} // end cnt
	} // end feature
	
	if ($feature ['AgreementForms'] == "Y") {
		//Set parameters
		$params   =   array(":OrgID"=>$OrgID);
		//Set where condition
		$where    =   array("OrgID = :OrgID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
		//Get AgreementFormQuestions
		$results  =   $FormQuestionsObj->getQuestionsInformation('AgreementFormQuestions', "distinct(AgreementFormID)", $where, "", array($params));
		
		if ($results['count'] > 0) {
			echo '<option value="AgreementForm"';
			if ($FormType == "AgreementForm") {
				echo ' selected';
			}
			echo '>' . $FORMS ['AgreementForm'] . '</option>';
		} // end cnt
	} // end feature
	

	//Set parameters
	$params    =   array(":OrgID"=>$OrgID);
	//Set where condition
	$where     =   array("OrgID = :OrgID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
	//Get Onboard Questions
	$results   =   $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', "QuestionID", $where, "", array($params));
	
	
	if ($results['count'] > 0) {
		echo '<option value="OnboardForm"';
		if ($FormType == "OnboardForm") {
			echo ' selected';
		}
		echo '>' . $FORMS ['OnboardForm'] . '</option>';
	}

	echo '<option value="OnboardApplications"';
	if ($FormType == "OnboardApplications") {
		echo ' selected';
	}
	echo '>' . $FORMS ['OnboardApplications'] . '</option>';
	
	echo '<option value="OrgData"';
	if ($FormType == "OrgData") {
		echo ' selected';
	}
	echo '>' . $FORMS ['OrgData'] . '</option>';
	
	echo '<option value="Requisition"';
	if ($FormType == "Requisition") {
		echo ' selected';
	}
	echo '>' . $FORMS ['Requisition'] . '</option>';

	echo '<option value="RequisitionOrgLevels"';
	if ($FormType == "RequisitionOrgLevels") {
	    echo ' selected';
	}
	echo '>' . $FORMS ['RequisitionOrgLevels'] . '</option>';
	
	echo '<option value="JobApplication"';
	if ($FormType == "JobApplication") {
		echo ' selected';
	}
	echo '>' . $FORMS ['JobApplication'] . '</option>';
	
	if ($feature ['DataManagerType'] == "Exporter") {
		echo '<option value="DataManager"';
		if ($FormType == "DataManager") {
			echo ' selected';
		}
		echo '>' . $FORMS ['DataManager'] . '</option>';
	}
	
	echo '</select>';
	echo '</div>';
	
	// FormID
	if (($FormType == "ApplicationForm") || ($FormType == "WebForm") || ($FormType == "AgreementForm") || ($FormType == "PreFilledForm") || ($FormType == "OnboardForm")) {
		
		
		if ($FormType == "ApplicationForm") {
            //Set where condition
            $where      =   array("OrgID = :OrgID");
            //Set params
            $params     =   array(":OrgID"=>$OrgID);
            //Set columns
            $columns    =   "DISTINCT(FormID) AS FormID, FormID AS FormName";
            //Get FormQuestions Information
            $resultsIN  =   $FormQuestionsObj->getFormQuestionsInformation($columns, $where, "FormID", array($params));
		}
		
		if ($FormType == "WebForm") {
            //Set where condition
            $where      =   array("OrgID = :OrgID", "FormStatus != 'Deleted'");
            //Set parameters
            $params     =   array(":OrgID"=>$OrgID);
            //Get WebFormsInformation
            $resultsIN  =   $FormsInternalObj->getWebFormsInfo("WebFormID as FormID, FormName", $where, "FormName", array($params));
		}
		
		if ($FormType == "AgreementForm") {
            //Set columns
            $columns    =   "AgreementFormID as FormID, concat(FormPart,' - ',FormName) as FormName";
            //Set where condition
            $where      =   array("OrgID = :OrgID", "FormStatus != 'Deleted'");
            //Set parameters
            $params     =   array(":OrgID"=>$OrgID);
            //Get Agreement Forms Information
            $resultsIN  =   $FormsInternalObj->getAgreementFormsInfo($columns, $where, "FormPart, FormName", array($params));
		}
		
		if ($FormType == "PreFilledForm") {
            //Set parameters
            $params     =   array(":OrgID"=>$OrgID);
            //Get Prefilled forms information
            $resultsIN  =   $FormsInternalObj->getPrefilledFormsInfo("PreFilledFormID as FormID, concat(TypeForm,': ',FormName) as FormName", array("OrgID = :OrgID"), "TypeForm, FormName", array($params));
		}
		
		
		if ($FormType == "OnboardForm") {
            $where      =   array("OrgID = :OrgID");
            //Set parameters
            $params     =   array(":OrgID"=>$OrgID);
            //Get Prefilled forms information
            $resultsIN  =   $FormsInternalObj->getOnboardFormsInfo("OnboardFormID as FormID", $where, 'OnboardFormID', "", array($params));
		}
		
		
		echo '<div style="padding:3px;float:left;">';
		echo 'Form Name: <select name="FormID" onchange="loadExporterForm(\'exporterForm.php\',\'FormID\');" style="max-width:150px;">';
		echo '<option value="">Select Here</option>';
		
		if ($FormType == "OnboardForm") {
		    if(is_array($resultsIN['results'])) {
		        foreach($resultsIN['results'] as $FORMS) {
		            if(in_array($FORMS ['FormID'], $onboard_features_list)) {
		                echo '<option value="' . $FORMS ['FormID'] . '"';
		                if ($FORMS ['FormID'] == $FormID) {
		                    echo ' selected';
		                }
		                echo '>' . $FORMS ['FormID'] . '</option>';
		            }
		        } // end foreach
		    }
		}   
		else {
		    if(is_array($resultsIN['results'])) {
		        foreach($resultsIN['results'] as $FORMS) {
		            echo '<option value="' . $FORMS ['FormID'] . '"';
		            if ($FORMS ['FormID'] == $FormID) {
		                echo ' selected';
		            }
		            echo '>' . $FORMS ['FormName'] . '</option>';
		        } // end foreach
		    }
		} 
		
		
		echo '</select>';
		echo '</div>';
	}
	
	if ($FormType == "RequisitionOrgLevels") {
            echo '<div style="padding:3px;float:left;">';
            echo 'Organizations: ';
            echo '<select name="OrgsList" name="OrgsList" onchange="loadExporterForm(\'exporterForm.php\',\'OrgsList\');" style="max-width:150px;">';
            echo '<option value="">Select Here</option>';
            $where_info     =   array("OrgID = :OrgID");
            $params_info    =   array(":OrgID"=>$OrgID);
            
            $orgs_results   =   $OrganizationsObj->getOrgDataInfo("OrgID, MultiOrgID, OrganizationName", $where_info, '', array($params_info));
            $orgs_list      =   $orgs_results['results'];
            
            for($ol = 0; $ol < count($orgs_list); $ol++) {
                $org_list_option_val = $OrgID.'-'.$orgs_list[$ol]['MultiOrgID'];
                if($org_list_option_val == $_REQUEST['OrgsList']) $org_list_opt_sel = ' selected="selected"';
                echo '<option value="'.$org_list_option_val.'"'.$org_list_opt_sel.'>'.$orgs_list[$ol]['OrganizationName'].'</option>';
                unset($org_list_opt_sel);
            }
            echo '</select>';
            echo '</div>';
	}
	
	if (isset($_REQUEST['OrgsList']) && $_REQUEST['OrgsList'] != "") {
            
            list($ReqOrgLevelOrgID, $ReqOrgLevelMultiOrgID) = explode("-", $_REQUEST['OrgsList']);
            $OrgLevelsList = $OrganizationLevelsObj->getOrgLevels($ReqOrgLevelOrgID, $ReqOrgLevelMultiOrgID);
            
            echo '<div style="padding:3px;float:left;">';
            echo 'Organization Levels: ';
            echo '<select name="OrgLevelIDs" name="OrgLevelIDs" onchange="loadExporterForm(\'exporterForm.php\',\'OrgLevelIDs\');" style="max-width:150px;">';
            echo '<option value="">Select Here</option>';
            
            for($ol = 0; $ol < count($OrgLevelsList); $ol++) {
                if($OrgLevelsList[$ol]['OrgLevelID'] == $_REQUEST['OrgLevelIDs']) $org_level_opt_sel = ' selected="selected"';
                echo '<option value="'.$OrgLevelsList[$ol]['OrgLevelID'].'"'.$org_level_opt_sel.'>'.$OrgLevelsList[$ol]['OrganizationLevel'].'</option>';
                unset($org_level_opt_sel);
            }
             
            echo '</select>';	     
            echo '</div>';
            
	}

	// SectionID
	if (($FormType == "ApplicationForm") && ($FormID != "")) {
		echo '<div style="padding:3px;float:left;">';
		echo 'Section: <select name="SectionID" onchange="loadExporterForm(\'exporterForm.php\',\'SectionID\');" style="max-width:100px;">';
		echo '<option value="">Select Here</option>';

		$sections_info    =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
		
		foreach($sections_info as $section_id=>$section_info) {
			echo '<option value="' . $section_id . '"';
			if ($section_info ['SectionID'] == $SectionID) {
				echo ' selected';
			}
			echo '>' . $section_info ['SectionTitle'] . '</option>';
		} // end foreach
		
		echo '</select>';
		echo '</div>';
	}
	
	if ($FormType == "ApplicationForm") {
		echo '<div style="clear:both;"></div>';
		
		$MULTI        =   array ();
		
		//Set where condition
		$where        =   array("OrgID = :OrgID", "SectionID = 0");
		//Set parameters
		$params       =   array(":OrgID"=>$OrgID);
		//Get FormQuestions Information
		$resultsIN    =   G::Obj('FormQuestions')->getFormQuestionsInformation("QuestionID, value", $where, "", array($params));
		
		if(is_array($resultsIN['results'])) {
			foreach($resultsIN['results'] as $FQ) {
				$MULTI [$FQ ['QuestionID']] = $FQ ['value'];
			}
		}
		
	}
	
	// Question
	$presentquestion = "N";
	
	if (($FormType == "OnboardApplications") || ($FormType == "OrgData") || ($FormType == "Requisition") || ($FormType == "JobApplication") || ($FormType == "DataManager")) {
		$presentquestion  =   "Y";
	} else if (($FormType == "AffirmativeActionForm") || ($FormType == "DisabledForm") || ($FormType == "VeteranForm")) {
		$presentquestion  =   "Y";
	} else if (($FormType == "OnboardForm") || ($FormType == "WebForm") || ($FormType == "AgreementForm") || ($FormType == "PreFilledForm")) {
		if ($FormID) {
			$presentquestion = "Y";
		}
	} else if (($FormType == "ApplicationForm") && ($SectionID != "")) {
		$presentquestion  =   "Y";
	}
	
	if ($presentquestion == "Y") {
		
		if ($FormType == "ApplicationForm") {
			
			//Set where condition
			$where       =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
			//Set parameters
			$params      =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$SectionID);
			//Set columns
			$columns     =   "QuestionTypeID, QuestionID, Question";
			//Get FormQuestions Information
			$resultsIN   =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));
			
		} else if ($FormType == "WebForm") {
			
			//Set where condition
			$where       =   array("OrgID = :OrgID", "WebFormID = :WebFormID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
			//Set parameters
			$params      =   array(":OrgID"=>$OrgID, ":WebFormID"=>$FormID);
			//WebFormQuestions
			$resultsIN   =   G::Obj('FormQuestions')->getQuestionsInformation('WebFormQuestions', "QuestionTypeID, QuestionID, Question", $where, "QuestionOrder", array($params));
			
		} else if ($FormType == "PreFilledForm") {
			
			//Set where condition
			$where       =   array("OrgID = 'MASTER'", "PreFilledFormID = :PreFilledFormID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
			//Set parameters
			$params      =   array(":PreFilledFormID"=>$FormID);
			//PreFilledFormQuestions
			$resultsIN   =   G::Obj('FormQuestions')->getQuestionsInformation('PreFilledFormQuestions', "QuestionTypeID, QuestionID, Question", $where, "QuestionOrder", array($params));
			
		} else if ($FormType == "AgreementForm") {
			
			//Set where condition
			$where       =   array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID", "Active = 'Y'", "QuestionTypeID NOT IN (SELECT QuestionTypeID FROM QuestionTypes WHERE Exportable != 'Y')");
			//Set parameters
			$params      =   array(":OrgID"=>$OrgID, ":AgreementFormID"=>$FormID);
			//PreFilledFormQuestions
			$resultsIN   =   G::Obj('FormQuestions')->getQuestionsInformation('AgreementFormQuestions', "QuestionTypeID, QuestionID, Question", $where, "QuestionOrder", array($params));
			
		} else if ($FormType == "OnboardForm") {
			
			//Set where condition
			$where       =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "Active = 'Y'");
			//Set parameters
			$params      =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$FormID);
			//OnboardQuestions
			$resultsIN   =   G::Obj('FormQuestions')->getQuestionsInformation('OnboardQuestions', "QuestionTypeID, QuestionID, Question", $where, "QuestionOrder", array($params));
			
		} else if ($FormType == "OrgData") {
			
			//Set where condition
			$where       =   array("FormType = :FormType");
			//Set parameters
			$params      =   array(":FormType"=>$FormType);
			//Set columns
			$columns     =   "QuestionTypeID, DataField as QuestionID, Description as Question";
			//Get ExporterDataMap Information
			$resultsIN   =   G::Obj('ExporterDataManager')->getExporterInfo('ExporterDataMap', $columns, $where, "SortOrder", array($params));
			
		} else if ($FormType == "Requisition") {
			
			//Set where condition
			$where       =   array("FormType = :FormType");
			//Set parameters
			$params      =   array(":FormType"=>$FormType);
			//Set columns
			$columns     =   "QuestionTypeID, DataField as QuestionID, Description as Question";
			//Get ExporterDataMap Information
			$resultsIN   =   G::Obj('ExporterDataManager')->getExporterInfo('ExporterDataMap', $columns, $where, "SortOrder", array($params));

		} else if ($FormType == "OnboardApplications") {
			
			//Set where condition
			$where       =   array("FormType = :FormType");
			//Set parameters
			$params      =   array(":FormType"=>$FormType);
			//Set columns
			$columns     =   "QuestionTypeID, DataField as QuestionID, Description as Question";
			//Get ExporterDataMap Information
			$resultsIN   =   G::Obj('ExporterDataManager')->getExporterInfo('ExporterDataMap', $columns, $where, "SortOrder", array($params));
			
		} else if ($FormType == "JobApplication") {
			
			//Set where condition
			$where       =   array("FormType = :FormType");
			if($feature ['InternalRequisitions'] != "Y") {
				$where[] = "DataField != 'IsInternalApplication'";
			}
			//Set parameters
			$params      =   array(":FormType"=>$FormType);
			//Set columns
			$columns     =   "QuestionTypeID, DataField as QuestionID, Description as Question";
			//Get ExporterDataMap Information
			$resultsIN   =   G::Obj('ExporterDataManager')->getExporterInfo('ExporterDataMap', $columns, $where, "SortOrder", array($params));

		} else if ($FormType == "DataManager") {
			
			//Set where condition
			$where       =   array("FormType = :FormType");
			if ($feature ['PreFilledForms'] != "Y") {
				$where[] = "DataField != 'FedStateTaxes'";
			}
			//Set parameters
			$params      =   array(":FormType"=>$FormType);
			//Set columns
			$columns     =   "QuestionTypeID, DataField as QuestionID, Description as Question";
			//Get ExporterDataMap Information
			$resultsIN   =   G::Obj('ExporterDataManager')->getExporterInfo('ExporterDataMap', $columns, $where, "SortOrder", array($params));

		}
		
		$qcnt = $resultsIN['count'];
		
		if ($qcnt > 0) {
			echo '<div style="padding:3px;float:left;">';
			echo 'Question: <select name="QuestionID" onchange="loadExporterForm(\'exporterForm.php\',\'QuestionID\');" style="max-width:180px;">';
			
			echo '<option value="">Select and Add Question</option>';
			
			if ($SectionID == "1") {
				
				$PERSONAL = array (
						'first'       =>  'First Name',
						'middle'      =>  'Middle Name',
						'last'        =>  'Last Name',
						'address'     =>  'Address',
						'address2'    =>  'Second Address',
						'city'        =>  'City',
						'state'       =>  'State',
						'zip'         =>  'Zip',
						'email'       =>  'Email Address' 
				);
				
				foreach ( $PERSONAL as $q => $t ) {
				    echo '<option value="' . $q . '|6|' . $t . '|' . '">' . $t;
					
					// check if duplicate key
					//Set where condition
					$where     =   array("OrgID = :OrgID", "ExporterID = :ExporterID", "FormType = :FormType", "QuestionID = :QuestionID");
					//Set parameters
					$params    =   array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":FormType"=>$FormType, ":QuestionID"=>$q);
					//Get Exporter Information
					$results   =   G::Obj('ExporterDataManager')->getExporterInfo('Exporter', "*", $where, "", array($params));
					
					if ($results['count'] > 0) {
						echo '&nbsp;&nbsp;(This question is already in the list)';
					}
					echo '</option>';
				} // end foreach
			} // end SectionID
			
			if(is_array($resultsIN['results'])) {
				foreach ($resultsIN['results'] as $QUESTIONS) {
				
					echo '<option value="' . $QUESTIONS ['QuestionID'] . '|' . $QUESTIONS ['QuestionTypeID'] . '|' . $QUESTIONS ['Question'] . '||' . '"';
					if ($QUESTIONS ['QuestionID'] . '|' . $QUESTIONS ['QuestionTypeID'] . '|' . $QUESTIONS ['Question'] . '||' == $QuestionID) {
						echo ' selected';
					}
					echo '>' . $QUESTIONS ['Question'];
				
					//Set where condition
					$where     =   array("OrgID = :OrgID", "ExporterID = :ExporterID", "FormType = :FormType", "QuestionID = :QuestionID");
					//Set parameters
					$params    =   array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID, ":FormType"=>$FormType, ":QuestionID"=>$QUESTIONS ['QuestionID']);
					
					if($FormID != "") {
					    $where[]           =   "FormID = :FormID";
					    $params[':FormID'] =   $FormID;
					}
					
					//Get Exporter Information
					$results   =   G::Obj('ExporterDataManager')->getExporterInfo('Exporter', "*", $where, "", array($params));

					// check if duplicate key
					if ($results['count'] > 0) {
						echo '&nbsp;&nbsp;(This question is already in the list)';
					}
					echo '</option>';
				} // end foreach
			}
			
			echo '</select>';
			echo '</div>';
		} else { // else if > 0
			
			echo '<div style="padding:3px;float:left;">';
			echo 'There are no available questions for this selection';
			echo '</div>';
		} // end if > 0
	} // end presentquestion
	  
	// all button
	echo '<div style="padding:3px;float:left;margin-left:5px;">';
	if ($FormType) {
		// echo '<button onclick="loadExporterForm(\'exporterForm.php\',\'Process\');return false;">Add Entire Category</button>';
	}
	
	echo '</div>';
	echo '</form>';
	
	echo '<div style="clear:both;"></div>';
	
	echo '</div>';
} else { // else namecnt and new
	
	echo '<form id="exportername" name="exportername">';
	echo 'Please select a name for your data collection.';
	echo '&nbsp;&nbsp;';
	echo '<input type="text" name="Name" size="15" maxlength="45">';
	echo '&nbsp;&nbsp;';
	echo '<button onClick="loadExporterForm(\'exporterForm.php\',\'ExporterName\');return false;">Create Export List</button>';
	echo '&nbsp;&nbsp;';
	echo '&nbsp;&nbsp;';
	echo '<button onClick="loadExporterForm(\'exporterForm.php\',\'\');return false;">Reset</button>';
	echo '</form>';
	echo '<br><br>';
} // end namecnt
?>
