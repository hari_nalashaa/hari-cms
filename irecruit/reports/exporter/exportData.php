<?php
require_once '../../Configuration.inc';

ini_set('max_execution_time', 1200); //300 seconds = 5 minutes, 0 infinate

$args = " USERID:" . $USERID . " OrgID:" . $OrgID;
foreach ($_REQUEST as $key=>$value) {
    $args .= " " . $key . ":" . $value;
}

$cmd = IRECRUIT_DIR . "reports/exporter/updateExport.php " . $args;

if ($_REQUEST['ExportTo'] == "SCREEN") {
  exec($cmd);
} else {
  exec($cmd . " > /dev/null &");
}

//Set where condition
$where      =   array("OrgID = :OrgID", "ExporterID = :ExporterID");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":ExporterID"=>$_REQUEST['ExporterID']);
//Set columns
$columns    =   "ExportName, ExportData";
//Get ExporterNames Information
$results    =   G::Obj('ExporterDataManager')->getExporterInfo('ExporterNames', $columns, $where, "", array($params));
//echo "<pre>";print_r($results);echo "</pre>";
if(is_array($results['results'][0])) {
	list ( $ExportName, $file ) = array_values($results['results'][0]);
}

if ($_REQUEST['ExportTo'] == "SCREEN") {
    echo '<textarea cols="80" rows="20">';
    echo $file;
    echo '</textarea>';
}
?>
