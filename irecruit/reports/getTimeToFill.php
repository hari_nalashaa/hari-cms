<?php
require_once '../Configuration.inc';

$FilterOrganization         =   $_REQUEST['FilterOrganization'];

$limit = $user_preferences['ReportsSearchResultsLimit'];
if(isset($_REQUEST['RecordsLimit']) && $_REQUEST['RecordsLimit'] != "") $limit = $_REQUEST['RecordsLimit'];

$start = 0;
if(isset($_REQUEST['IndexStart'])) $start = $_REQUEST['IndexStart'];

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

$order_by                       =   '';

//Get total records count
$total_count                    =   G::Obj('Reports')->getTimeToFillCount($FilterOrganization, $OrgID, $_REQUEST['RequestID'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status']);

$applicants_flow_log_results    =   G::Obj('Reports')->getTimeToFill($FilterOrganization, $OrgID, $_REQUEST['RequestID'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status'], $order_by, $start, $limit);
$applicants_flow_log            =   $applicants_flow_log_results['results'];

$export_app_flow_log_results    =   G::Obj('Reports')->getTimeToFill($FilterOrganization, $OrgID, $_REQUEST['RequestID'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status'], $order_by, '', '');
$export_app_flow_log            =   $export_app_flow_log_results['results'];

$left_nav_info                  =   G::Obj('Pagination')->getPageNavigationInfo($start, $limit, $total_count, '', '');

if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "applicants_list"                   =>  $applicants_flow_log,
        "previous"                          =>  $left_nav_info['previous'],
        "next"                              =>  $left_nav_info['next'],
        "total_pages"                       =>  $left_nav_info['total_pages'],
        "current_page"                      =>  $left_nav_info['current_page'],
        "total_count"                       =>  $total_count,
        "export_app_flow_log"               =>  $export_app_flow_log
    ));
}

############        Export Code     ############
if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
    $list = $export_app_flow_log;
    require_once IRECRUIT_DIR . 'reports/ExportTimeToFill.inc';
}
############        Export Code     #############
?>
