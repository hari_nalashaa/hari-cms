<?php
require_once '../Configuration.inc';

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER['QUERY_STRING']) {
    $TemplateObj->goto = $_SERVER['QUERY_STRING'];
}

// Set page title
$TemplateObj->title     =   $title  =   "Requisitions";

// Assign add applicant datepicker variables
$script_vars_footer []  =   'var datepicker_ids = "#requisitions_from_date, #requisitions_to_date";';
$script_vars_footer []  =   'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer = $script_vars_footer;

// set where it is common for below results
$where      =   array("OrgID = :OrgID");
// set params it is common for below results
$params     =   array(":OrgID" => $OrgID);
// set columns 
$columns    =   "UserID, FirstName, LastName, EmailAddress";

// Get Users List
$users_results_info = $IrecruitUsersObj->getUserInformation($columns, $where, "", array($params));

$users_results = $users_results_info['results'];
for ($j = 0; $j < $users_results_info['count']; $j ++) {
    $users_list[$users_results[$j]['UserID']] = $users_results[$j];
}

// Get Managers List
$manager_results_info       =   $RequisitionsObj->getRequisitionManagersInfo("UserID", $where, "UserID", "", array($params));
$requisition_managers_list  =   $manager_results_info['results'];

// Get Requisitions List
$where[] = 'Owner != ""';
$requisition_owner_results  =   $RequisitionsObj->getRequisitionInformation("Owner", $where, "Owner", "", array($params));
$requisition_owners_list    =   $requisition_owner_results['results'];

// Managers List
$TemplateObj->requisition_managers_list =   $requisition_managers_list;
$TemplateObj->requisition_owners_list   =   $requisition_owners_list;
$TemplateObj->users_list                =   $users_list;

echo $TemplateObj->displayIrecruitTemplate('views/reports/RequisitionsByManagerOrOwner');
?>