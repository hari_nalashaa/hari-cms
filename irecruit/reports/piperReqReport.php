#!/usr/local/bin/php -q
<?php
require_once realpath(__DIR__.'/..') . '/irecruitdb.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

if ($argv[1] != "") {
$where          =       array ("UserID = :UserID");
$params         =       array (":UserID" => $argv[1]);
$results        =       G::Obj('IrecruitUsers')->getUserInformation ( "OrgID, EmailAddress, concat(FirstName,' ',LastName) AS Name", $where, "", array ($params) );
$OrgID = $results['results'][0]['OrgID'];
$EmailAddress = $results['results'][0]['EmailAddress'];
$Name = $results['results'][0]['Name'];
}

if (($EmailAddress != "") && ($OrgID == "I20200305")) { // Piper

$ALPHA = range('A', 'Z');
$ALPHA[]="AA";
$ALPHA[]="AB";
$ALPHA[]="AC";
$ALPHA[]="AD";
$ALPHA[]="AE";
$ALPHA[]="AF";
$ALPHA[]="AG";
$ALPHA[]="AH";
$ALPHA[]="AI";
$ALPHA[]="AJ";
$ALPHA[]="AK";
$ALPHA[]="AL";
$ALPHA[]="AM";
$ALPHA[]="AN";
$ALPHA[]="AO";
$ALPHA[]="AP";
$ALPHA[]="AQ";
$ALPHA[]="AR";
$ALPHA[]="AS";
$ALPHA[]="AT";
$ALPHA[]="AU";
$ALPHA[]="AV";
$ALPHA[]="AW";
$ALPHA[]="AX";
$ALPHA[]="AY";
$ALPHA[]="AZ";
$ALPHA[]="BA";
$ALPHA[]="BB";
$ALPHA[]="BC";
$ALPHA[]="BD";
$ALPHA[]="BE";
$ALPHA[]="BF";
$ALPHA[]="BG";
$ALPHA[]="BH";
$ALPHA[]="BI";
$ALPHA[]="BJ";
$ALPHA[]="BK";

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "IRECRUIT";

//Get RequisitionFormID
$RequisitionFormID     =   G::Obj('RequisitionForms')->getDefaultRequisitionFormID($OrgID);

$query = "SELECT QuestionID, Question";
$query .= " FROM RequisitionQuestions";
$query .= " WHERE OrgID = :OrgID";
$query .= " AND QuestionID in ('CUST16674883936363da89cbb','CUST1630425511612e51a7c83','CUST1629479928611fe3f8656','CUST1629480016611fe450465','CUST1630329121612cd921636','CUST1630426135612e5417c3f')";
$query .= " AND RequisitionFormID = :RequisitionFormID";
$query .= " ORDER BY QuestionOrder";
$params     =   array(':OrgID' =>  $OrgID, ':RequisitionFormID' => $RequisitionFormID );
$CUSREQ =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$query = "SELECT";
$query .= " Active AS 'Active',";
$query .= " OrgID,";
$query .= " RequestID,";
$query .= " RequisitionFormID,";
$query .= " RequisitionID AS 'Requistion ID', JobID AS 'Job ID',";
$query .= " Title AS 'Job Title',";
$query .= " DATE_FORMAT(PostDate,'%m/%d/%Y %h:%i %p') AS 'Post Date',";
$query .= " DATE_FORMAT(ExpireDate,'%m/%d/%Y %h:%i %p') AS 'Expire Date',";
$query .= " 'Status',";
$query .= " 'Supervisor',";
$query .= " HowMany AS '# Vacancies',";
$query .= " getRequisitionStageDescription(OrgID, RequisitionStage) AS 'Requisition Stage',";
$query .= " CASE WHEN Exempt_Status = 'Y' THEN 'Yes' ELSE 'No' END AS 'Exempt Status'";
$query .= " FROM Requisitions";
$query .= " WHERE OrgID = :OrgID";
$query .= " ORDER BY Active DESC, Title";
//$query .= " LIMIT 3,2";

$params     =   array(':OrgID' =>  $OrgID);
$RESULTS    =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

$RES=array();

$i=0;
foreach ($RESULTS AS $REQ) {

   $RES[$i]=$REQ;

   $RN = G::Obj('Requisitions')->getRequisitionManagersNames($REQ['OrgID'],$REQ['RequestID']);
   $RES[$i]['Supervisor']=preg_replace('/\s+/', ' ', $RN);

   foreach ($CUSREQ AS $CR) {
      $CUS = G::Obj('RequisitionsData')->getRequisitionsDataByQuestionID($REQ['OrgID'],$REQ['RequestID'],$CR['QuestionID']); 
      $ans=$CUS['Answer'];
      if ($CUS['QuestionTypeID'] == '3') {

	$query = "SELECT value";
	$query .= " FROM RequisitionQuestions";
	$query .= " WHERE OrgID = :OrgID";
	$query .= " AND RequisitionFormID = :RequisitionFormID";
	$query .= " AND QuestionID = :QuestionID";
	$params     =   array(':OrgID' =>  $OrgID, ':RequisitionFormID' => $REQ['RequisitionFormID'], ':QuestionID' => $CR['QuestionID'] );
	$VALUE  =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
	$V = getValueArray($VALUE[0]['value']);
        $ans=$V[$ans];
      }
      $RES[$i][$CR['Question']]=$ans;
   }


   $RES[$i]['Status']=isActive($REQ['Post Date'],$REQ['Expire Date']);

   $query = "SELECT count(*) AS cnt";
   $query .= " FROM JobApplications";
   $query .= " WHERE OrgID = :OrgID";
   $query .= " AND RequestID = :RequestID";
   $params =   array(':OrgID' =>  $REQ['OrgID'], ':RequestID' => $REQ['RequestID']);
   $TOTALCNT  =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

   $RES[$i]['Total Applicants'] = $TOTALCNT[0]['cnt'];

   $query = "select ProcessOrder, Description from ApplicantProcessFlow where OrgID = :OrgID";
   $params     =   array(':OrgID' =>  $OrgID);
   $PROCESSFLOW =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

   foreach ($PROCESSFLOW as $PF) {

      $query = "SELECT count(*) AS cnt";
      $query .= " FROM JobApplications";
      $query .= " WHERE OrgID = :OrgID";
      $query .= " AND RequestID = :RequestID";
      $query .= " AND ProcessOrder = :ProcessOrder";
      $params =   array(':OrgID' =>  $REQ['OrgID'], ':RequestID' => $REQ['RequestID'], ':ProcessOrder' => $PF['ProcessOrder']);
      $PROCESSCNT =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

      $RES[$i][$PF['Description']]= $PROCESSCNT[0]['cnt'];

   }

   $RES[$i]['# Vacancies']=$RES[$i]['How Many are you hiring?'];

unset($RES[$i]['How Many are you hiring?']);
unset($RES[$i]['OrgID']);
unset($RES[$i]['RequestID']);
unset($RES[$i]['RequisitionFormID']);

$i++;
} // end foreach

//echo print_r($RES,true);
//exit;

$objPHPExcel = new Spreadsheet();

$objPHPExcel->getProperties()->setCreator("iRecruit")
       ->setLastModifiedBy("System")
       ->setTitle("iRecruit Export")
       ->setSubject("Excel")
       ->setDescription("Not Active")
       ->setKeywords("phpExcel")
       ->setCategory("Output");

$totalrows=sizeof($RES[0]);
$lastkey=$ALPHA[--$totalrows];

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(100);
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('P:'.$lastkey)->getAlignment()->setHorizontal('center');

$i=0;
foreach (array_keys($RES[0]) AS $key) {
$objPHPExcel->getActiveSheet()->getCell($ALPHA[$i].'1')->setValue($key);

$i++;
}

for ($i = 'A'; $i != 'P'; $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);

$n=1;
foreach ($RES AS $row=>$R) {
 if ($R['Labor Status'] == "DL") {
  $n++;
  $iii=0;
  foreach ($R AS $value) {
$objPHPExcel->getActiveSheet()->getCell($ALPHA[$iii].$n)->setValue($value);

  $iii++;
  }
 }
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Direct Requisitions");

// Create a new worksheet, after the default sheet
$objPHPExcel->createSheet();

// Create a first sheet, representing sales data
$objPHPExcel->setActiveSheetIndex(1);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(100);
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getStyle('A1:'.$lastkey.'1')->getAlignment()->setWrapText(true);

$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal('center');
$objPHPExcel->getActiveSheet()->getStyle('P:'.$lastkey)->getAlignment()->setHorizontal('center');

$i=0;
foreach (array_keys($RES[0]) AS $key) {
$objPHPExcel->getActiveSheet()->getCell($ALPHA[$i].'1')->setValue($key);

$i++;
}

//for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
for ($i = 'A'; $i != 'O'; $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);

$n=1;
foreach ($RES AS $row=>$R) {
 if ($R['Labor Status'] == "IL") {
  $n++;
  $iii=0;
  foreach ($R AS $value) {
$objPHPExcel->getActiveSheet()->getCell($ALPHA[$iii].$n)->setValue($value);

  $iii++;
  }
 }
}

// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle("Indirect Requisitions");

$objPHPExcel->setActiveSheetIndex(0);

$PHPMailerObj->clearCustomProperties();
$PHPMailerObj->clearCustomHeaders();

$subject = "iRecruit Requisition Report " . date("m/d/Y");

$message = "Here is your requisition report.<br>\nData is as of: " . date("m/d/Y") . "<br><br>\n\n";

$message .= "Thank you!<br>\n";
$message .= "<strong>iRecruit</strong><br>\n";

// Set who the message is to be sent to
$PHPMailerObj->addAddress($EmailAddress, $Name);

// Set the subject line
$PHPMailerObj->Subject = $subject;
// convert HTML into a basic plain-text alternative body
$PHPMailerObj->msgHTML($message);
// Content Type Is HTML
$PHPMailerObj->ContentType = 'text/html';

$tempfile=ROOT.'cron/temp/tmpreport.xlsx';
$objWriter = IOFactory::createWriter($objPHPExcel , 'Xlsx');

$objWriter->save($tempfile);

$data = file_get_contents($tempfile);

$PHPMailerObj->addStringAttachment($data, 'Requisition Report.xlsx', 'base64', 'text/html');

// Send email
$PHPMailerObj->send();

unlink($tempfile);

} // end email OrgID check

exit;

function getValueArray($value) {

   $values_list    =   array();
   $que_values     =   explode ( '::', $value );

   foreach ( $que_values as $v => $q ) {
        list ( $vv, $qq )   =   explode ( ":", $q);
        $values_list[$vv]   =   $qq;
   }

   return $values_list;
}


function isActive($post,$expire) {

   $rtn="";
   $currentdate=date('Y-m-d');
   $postdate=date('Y-m-d', strtotime($post));
   $expiredate=date('Y-m-d', strtotime($expire));
    
   if (($currentdate >= $postdate) && ($currentdate <= $expiredate)){
     $rtn .= "Active";
   }

   return $rtn;

} // end function

?>
