<?php
$title = 'Report';

//Declare mostly using super globals over here
$TemplateObj->action = $action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

$subtitle = array (
		"ap" => "Applications",
		"ra" => "Requisitions",
		"eeo" => "EEO",
		"aap" => "AAP",
		"if" => "iRecruit Form Status",
		"ia" => "iRecruit Activity",
		"ic" => "iRecruit Configuration",
		"exporter" => "Exporter"
);

if ($feature ['Exporter'] != "Y") {
        unset ( $subtitle ['exporter'] );
}

if ($subtitle [$action]) {
	$title .= ' - ' . $subtitle [$action];
}

?>
