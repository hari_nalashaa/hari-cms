<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit AAP Reports")
->setSubject("Excel")
->setDescription("To view the applicants information those are filtered in reports")
->setKeywords("phpExcel")
->setCategory("AAP");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application ID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Applicant Name');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Job Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('EEO');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Black/African American');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Hispanic');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Asian/Pacific Islander');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('American Indians/Alaskan Natives');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('White(not Hispanic)');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Two or More Races');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Two or More Races');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Female');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Disabled  Yes');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Disabled  No');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Disabled Not Disclosed');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Veteran Identified');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Veteran Not Protected');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Veteran Not Disclosed');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Status');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Disposition Code');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Start Date');

for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha = 0;

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicationID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicantSortName']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['JobTitle']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['EEO']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['EntryDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RaceBlack']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Hispanic']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RaceAsian']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RaceIndian']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RaceWhite']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RaceTwo']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Male']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Female']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['DisabilityYes']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['DisabilityNo']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['DisabilityNonDisclose']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['VeteranIntentified']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['VeteranNotProtected']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['VeteranNonDisclose']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ProcessOrder']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['DispositionCode']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['StartDate']);

}

$e++;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$e.':S'.$e);
$objPHPExcel->getActiveSheet()->getCell('A'.$e)->setValue("Total Applicants Count: ".count($list));
$e++;
$objPHPExcel->getActiveSheet()->mergeCells('A'.$e.':D'.$e);
$objPHPExcel->getActiveSheet()->getCell('A'.$e)->setValue("Total: ");


$objPHPExcel->getActiveSheet()->getCell('F'.$e)->setValue($count_list['RaceBlackCount']);
$objPHPExcel->getActiveSheet()->getCell('G'.$e)->setValue($count_list['HispanicCount']);
$objPHPExcel->getActiveSheet()->getCell('H'.$e)->setValue($count_list['RaceAsianCount']);
$objPHPExcel->getActiveSheet()->getCell('I'.$e)->setValue($count_list['RaceIndianCount']);
$objPHPExcel->getActiveSheet()->getCell('J'.$e)->setValue($count_list['RaceWhiteCount']);
$objPHPExcel->getActiveSheet()->getCell('K'.$e)->setValue($count_list['RaceTwoCount']);
$objPHPExcel->getActiveSheet()->getCell('L'.$e)->setValue($count_list['MaleCount']);
$objPHPExcel->getActiveSheet()->getCell('M'.$e)->setValue($count_list['FemaleCount']);
$objPHPExcel->getActiveSheet()->getCell('N'.$e)->setValue($count_list['DisabilityYesCount']);
$objPHPExcel->getActiveSheet()->getCell('O'.$e)->setValue($count_list['DisabilityNoCount']);
$objPHPExcel->getActiveSheet()->getCell('P'.$e)->setValue($count_list['DisabilityNonDiscloseCount']);
$objPHPExcel->getActiveSheet()->getCell('Q'.$e)->setValue($count_list['VeteranIntentifiedCount']);
$objPHPExcel->getActiveSheet()->getCell('R'.$e)->setValue($count_list['VeteranNotProtectedCount']);
$objPHPExcel->getActiveSheet()->getCell('S'.$e)->setValue($count_list['VeteranNonDiscloseCount']);
$objPHPExcel->getActiveSheet()->getCell('T'.$e)->setValue("");
$objPHPExcel->getActiveSheet()->getCell('U'.$e)->setValue("");
$objPHPExcel->getActiveSheet()->getCell('V'.$e)->setValue("");


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="aap.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
