<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Requisitions Reports")
->setSubject("Excel")
->setDescription("To view the requisitions information those are filtered in reports requisitions page")
->setKeywords("phpExcel")
->setCategory("Requisitions Reports");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('ReqID/JobID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Date Opened');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Expiration Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Address Information');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Free ZipRecruiter');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Paid ZipRecruiter');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Free Indeed');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Paid Indeed');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Free Monster');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Paid Monster');

for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha = 0;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Title']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['RequisitionID'] . "/" . $list[$ah]['JobID']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['PostDateByFeature']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ExpireDate']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['Title']);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(str_replace("&nbsp;", " ", $list[$ah]['AddressInformation']));

   
    
    if($list[$ah]['FreeZipRecruiter'] == "Yes") {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("Yes");
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("");

    }
    
    if($list[$ah]['PaidZipRecruiter'] == "Yes") {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("Yes");
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("");

    }
    
    if($list[$ah]['FreeIndeed'] == "Yes") {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("Yes");
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("");

    }
    
    if($list[$ah]['PaidIndeed'] == "Yes") {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("Yes");
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("");

    }
    
    if($list[$ah]['FreeMonster'] == "Yes") {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("Yes");
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("");
    }
    
    if($list[$ah]['PaidMonster'] == "Yes") {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("Yes");
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue("");
    }
    
}




header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="requisitions_by_job_board_status.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
