<?php
require_once '../Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Get Requisition Approval Records
$req_appr_fields    =   G::Obj('RequisitionsReports')->getRequisitionsApprovalFieldsInfo($OrgID, $_REQUEST['from_date'], $_REQUEST['to_date']);

$req_approval_list  =   $req_appr_fields["req_approval_list"];
$map_columns        =   $req_appr_fields["map_columns"];

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Requisition Approval Reports")
            ->setLastModifiedBy("Requisition Approval Reports")
            ->setTitle("iRecruit Requisition Approval Reports")
            ->setSubject("Excel")
            ->setDescription("Requisition Approval Reports List")
            ->setKeywords("phpExcel")
            ->setCategory("Requisition Approvals");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

foreach ($map_columns as $map_column_id=>$map_column_heading) {
    if($map_column_id == "MaxApprovers") {
        for($maxa = 0; $maxa < $map_column_heading; $maxa++) {
            $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Approver');
            $al++;
            $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Authority Level');
            $al++;
            $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Approved On');
            $al++;
        }
    }
    else {
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue($map_column_heading);
        $al++;
    }
}

for ($i = 'A'; $i !=  $objPHPExcel->getActiveSheet()->getHighestColumn(); $i++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);
}
    $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(TRUE);


$l = 2;
if (is_array ( $req_approval_list )) {
    foreach ( $req_approval_list as $RR ) {

        $alp = 0;
       


        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['DateEntered']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['RequesterName']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['Title']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['RequisitionID']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['JobID']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['RequesterReason']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['PostDate']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['ExpireDate']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['ApprovalLevelRequired']);


        
        for($maxa = 0; $maxa < $map_columns["MaxApprovers"]; $maxa++) {
        
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['RequestApprovers'][$maxa]['EmailAddress']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['RequestApprovers'][$maxa]['AuthorityLevel']);
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['RequestApprovers'][$maxa]['DateApproval']);


	}

        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['ApprovalStatus']." ");
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['DaysOpen']." ");
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($RR['ApplicantsCount']." ");        
        $l++;
    }
}

###  Append Applicants Data To Spread Sheet ###

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="requisition_approvals.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

