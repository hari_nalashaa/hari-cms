<?php
require_once '../Configuration.inc';

//Added Export Condition
/*if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
}*/

$FilterOrganization =   $_REQUEST['FilterOrganization'];

$limit_app_history  =   $user_preferences['ReportsSearchResultsLimit'];

if(isset($_REQUEST['RecordsLimit']) && $_REQUEST['RecordsLimit'] != "") $limit_app_history = $_REQUEST['RecordsLimit'];

$start = 0;
if(isset($_REQUEST['IndexStart'])) $start = $_REQUEST['IndexStart'];

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

if (isset ( $_REQUEST ['to_sort'] )) {

    if ($_REQUEST ['to_sort'] == "applicant_name") {
        $history_order           =   " JA.ApplicantSortName $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "application_id") {
        $history_order           =   " JA.ApplicationID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "status") {
        $history_order           =   " JH.ProcessOrder $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "entry_date") {
        $history_order           =   " JA.EntryDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "last_updated") {
        $history_order           =   " JH.StatusEffectiveDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "updated_by") {
        $history_order           =   " JH.UserID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "disposition_code") {
        $history_order           =   " getDispositionCodeByCodeID(JH.OrgID, JH.DispositionCode) $sort_type";
    }
    else {
        $history_order           =   " JA.EntryDate DESC";
    }

} else {
    $history_order  =   " JA.EntryDate DESC";
}

if(!isset($_REQUEST['Export'])) {
    $history_order .= " LIMIT $start, $limit_app_history";
}    

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER['QUERY_STRING']) {
    $TemplateObj->goto = $_SERVER['QUERY_STRING'];
}

// Set page title
$TemplateObj->title = $title = "Requisitions By Manager / Owner";

if($FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);

    $where  =   array("JA.OrgID = :OrgID", "JA.MultiOrgID = :MultiOrgID");
    $params =   array(":OrgID" => $FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
}
else {
    $where  =   array("JA.OrgID = :OrgID");
    $params =   array(":OrgID" => $OrgID);
}

if (isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") {
    $where[]                    =   "JH.ApplicationID = :ApplicationID";
    $params[":ApplicationID"]   =   $_REQUEST['ApplicationID'];
}

if (isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
    $where[]                    =   "JH.RequestID = :RequestID";
    $params[":RequestID"]       =   $_REQUEST['RequestID'];
}

if($_REQUEST['ProcessOrder'] != "") {
    $where[]                    =   "JH.ProcessOrder = :ProcessOrder";
    $params[":ProcessOrder"]    =   $_REQUEST['ProcessOrder'];
}

if($_REQUEST['DispositionCode'] != "") {
    $where[]                    =   "JH.DispositionCode = :DispositionCode";
    $params[":DispositionCode"] =   $_REQUEST['DispositionCode'];
}

if (isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])) {
    $where[]                    =   "JA.EntryDate >= :FromDate AND JA.EntryDate <= :ToDate";
    $params[":FromDate"]        =   G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['FromDate']);
    $params[":ToDate"]          =   G::Obj('DateHelper')->getYmdFromMdy($_REQUEST['ToDate']);
}

if(isset($_REQUEST['Keyword']) && $_REQUEST['Keyword'] != "") {
    $where[]                    =   "(JA.ApplicantSortName LIKE :Keyword1)";
    $params[":Keyword1"]        =   "%".$_REQUEST['Keyword']."%";
}

// Get Applications History Count
$columns                =   "JA.ApplicantSortName, JA.EntryDate, JH.*, ";
$columns                .=  "(SELECT R.EEOCode FROM Requisitions R WHERE R.OrgID = JH.OrgID AND R.RequestID = JH.RequestID) as EEOCode,";
$columns                .=  "(SELECT AD.Answer FROM ApplicantData AD WHERE AD.OrgID = JH.OrgID AND AD.ApplicationID = JH.ApplicationID AND AD.QuestionID = 'aa_ethnicity') as AAEthnicity,";
$columns                .=  "(SELECT AD.Answer FROM ApplicantData AD WHERE AD.OrgID = JH.OrgID AND AD.ApplicationID = JH.ApplicationID AND AD.QuestionID = 'aa_veteran') as AAVeteran,";
$columns                .=  "(SELECT AD.Answer FROM ApplicantData AD WHERE AD.OrgID = JH.OrgID AND AD.ApplicationID = JH.ApplicationID AND AD.QuestionID = 'aa_veteranstatus') as AAVeteranStatus,";
$columns                .=  "(SELECT AD.Answer FROM ApplicantData AD WHERE AD.OrgID = JH.OrgID AND AD.ApplicationID = JH.ApplicationID AND AD.QuestionID = 'Sage_HireDate') as SageHireDate ";
$app_history_results    =   G::Obj('Applications')->getJobApplicationsAndHistory($columns, $where, $history_order, array($params));

$app_his_res            =   $app_history_results['results'];
$app_his_res_count      =   $app_history_results['count'];

// Get Applications History Count
$columns                =   "COUNT(JA.ApplicationID) AS ApplicantsHistoryCount";
$app_his_total_cnt      =   G::Obj('Applications')->getJobApplicationsAndHistory($columns, $where, "", array($params));
$apps_his_res_count     =   $app_his_total_cnt['results'][0]['ApplicantsHistoryCount'];


$list = array();
for ($i = 0; $i < $app_his_res_count; $i ++) {

    $app_his_res[$i]['EEOCode'] =   G::Obj('Organizations')->displayEEO($app_his_res[$i]['EEOCode']);
    
    if (is_numeric($app_his_res[$i]['UserID'])) {
        
        $UserID     =   "PORTAL:<br>";
        
        // Get User, Email From Users
        $resultsUP  =   G::Obj('UserPortalUsers')->getUserDetailInfoByUserID("CONCAT(FirstName,' ',LastName) AS Name, Email", $app_his_res[$i]['UserID']);

        if (is_array($resultsUP)) list ($User, $Em) = array_values($resultsUP);
        
        $UserID     .=  $User . "<br>";
        
        if ($permit['Applicants_Contact'] == 1) { // if access to view email
            $UserID .= '<a href="mailto:' . $Em . '">' . $Em . '</a>';
        } else {
            $UserID .= $Em;
        }
    } else {
        $UserID = $app_his_res[$i]['UserID'];
    } // end is_numeric
    
    $DispositionCode            =   G::Obj('ApplicantDetails')->getDispositionCodeDescription($OrgID, $app_his_res[$i]['DispositionCode']);
    $ProcessOrder               =   G::Obj('ApplicantDetails')->getProcessOrderDescription($OrgID, $app_his_res[$i]['ProcessOrder']);
    $ApplicantName              =   $app_his_res[$i]['ApplicantSortName'];
    $multiorgid_req             =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $app_his_res[$i]['RequestID']);
    $JobTitle                   =   G::Obj('RequisitionDetails')->getJobTitle($OrgID, $multiorgid_req, $app_his_res[$i]['RequestID']);
    $StatusEffectiveDate        =   date('m/d/Y', strtotime($app_his_res[$i]['StatusEffectiveDate']));
    $EntryDate                  =   date('m/d/Y', strtotime($app_his_res[$i]['EntryDate']));
    
    $DOS                        =   G::Obj('RequisitionsData')->getRequisitionsDataByQuestionID($OrgID, $app_his_res[$i]['RequestID'], 'Sage_OrgLevel1');

    $RequisitionFormID          =   G::Obj('RequisitionDetails')->getRequisitionFormID($OrgID, $app_his_res[$i]['RequestID']);
    $req_que_info               =   G::Obj('RequisitionQuestions')->getRequisitionQuestionInfo("value", $OrgID, 'Sage_OrgLevel1', $RequisitionFormID);
    $dos_vals                   =   G::Obj('GetFormPostAnswer')->getDisplayValue($req_que_info['value']);

    //Set condition
    $where                      =   array("OrgID = :OrgID", "RequestID = :RequestID", "OrgLevelID = 1");
    //Set parameters
    $params                     =   array(":OrgID"=>$OrgID, ":RequestID"=>$app_his_res[$i]['RequestID']);
    //Get Requisition Org Levels Information
    $resultsIN                  =   $RequisitionsObj->getRequisitionOrgLevelsInfo("*", $where, "OrgLevelID, SelectionOrder", array($params));
    $req_org_levels             =   $resultsIN['results'];
    
    $org_level_data             =   G::Obj('OrganizationLevels')->getOrgLevelCategories($OrgID, $multiorgid_req, "1");

    $selected_org_level1        =   array();
    for($r = 0; $r < count($req_org_levels); $r++) {
        $selected_org_level1[$r]    =   $org_level_data[$req_org_levels[$r]['SelectionOrder']];
    }
    
    $list[$i] = array(
        "OrgID"                 =>  utf8_encode($OrgID),
        "ApplicantName"         =>  utf8_encode($ApplicantName),
        "ApplicationID"         =>  utf8_encode($app_his_res[$i]['ApplicationID']),
        "JobTitle"              =>  utf8_encode($JobTitle),
        "ProcessOrder"          =>  utf8_encode($ProcessOrder),
        "DispositionCode"       =>  utf8_encode($DispositionCode),
        "EntryDate"             =>  $EntryDate,
        "StatusEffectiveDate"   =>  $StatusEffectiveDate,
        "UserID"                =>  $UserID,
        "Comments"              =>  utf8_encode($app_his_res[$i]['Comments']),
        "EEOCode"               =>  $app_his_res[$i]['EEOCode'],
        "AAEthnicity"           =>  $app_his_res[$i]['AAEthnicity'],
        "AAVeteran"             =>  $app_his_res[$i]['AAVeteran'],
        "AAVeteranStatus"       =>  $app_his_res[$i]['AAVeteranStatus'],
        "SageHireDate"          =>  $app_his_res[$i]['SageHireDate'],
        "DOS"                   =>  implode(", ", $selected_org_level1),
    );
}

$left_nav_info = G::Obj('Pagination')->getPageNavigationInfo($start, $limit_app_history, $apps_his_res_count, '', '');

if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "applicants_list"           =>  $list,
        "previous"                  =>  $left_nav_info['previous'],
        "next"                      =>  $left_nav_info['next'],
        "total_pages"               =>  $left_nav_info['total_pages'],
        "current_page"              =>  $left_nav_info['current_page'],
        "applicants_count"          =>  $apps_his_res_count
    ));
}


############        Export Code     ############
if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
    require_once IRECRUIT_DIR . 'reports/ExportApplicantsHistory.inc';
}
############        Export Code     #############
?>