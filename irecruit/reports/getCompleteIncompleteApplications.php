<?php
require_once '../Configuration.inc';

$TemplateObj->FilterOrganization    =   $FilterOrganization =   $_REQUEST['FilterOrganization'];

$Limit = $user_preferences['ReportsSearchResultsLimit'];
if(isset($_REQUEST['RecordsLimit']) && $_REQUEST['RecordsLimit'] != "") $Limit = $_REQUEST['RecordsLimit'];

//Set active
$Active =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : 'Y';
$IsRequisitionApproved = "Y";
if($Active == "NA") {
    $Active = "N";
    $IsRequisitionApproved = "N";
}

$Start = 0;
if(isset($_REQUEST['IndexStart'])) $Start = $_REQUEST['IndexStart'];

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

$order_by           =   " UA.Status $sort_type";
if (isset ( $_REQUEST ['to_sort'] )) {

    if ($_REQUEST ['to_sort'] == "status") {
        $order_by           =   " UA.Status $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_title") {
        $order_by           =   " UA.RequisitionTitle $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "first_name") {
        $order_by           =   " (SELECT `FirstName` FROM Users WHERE UserID = UA.UserID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "last_name") {
        $order_by           =   " (SELECT `LastName` FROM Users WHERE UserID = UA.UserID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "email") {
        $order_by           =   " (SELECT `Email` FROM Users WHERE UserID = UA.UserID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "lastupdateddate") {
        $order_by           =   " UA.LastUpdatedDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "deleted") {
        $order_by           =   " UA.Deleted $sort_type";
    }
    else {
        $order_by           =   " UA.Status $sort_type";
    }

    $sort_by_key            =   $_REQUEST ['to_sort'];
}

if(!isset($_REQUEST['Export'])) {
    $order_by .= " LIMIT $Start, $Limit";
}

if($FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
    
    $where  =   array("UA.OrgID = :OrgID", "UA.MultiOrgID = :MultiOrgID");
    $params =   array(":OrgID" => $FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
}
else {
    $where  =   array("UA.OrgID = :OrgID");
    $params =   array(":OrgID"=>$OrgID);
}

if(isset($_REQUEST['FromDate'])
    && $_REQUEST['FromDate'] != ""
    && isset($_REQUEST['ToDate'])
    && $_REQUEST['ToDate'] != "") {
    $where[] = "DATE(LastUpdatedDate) >= :FromDate";
    $where[] = "DATE(LastUpdatedDate) <= :ToDate";

    $params[":FromDate"] = $DateHelperObj->getYmdFromMdy($_REQUEST['FromDate']);
    $params[":ToDate"] = $DateHelperObj->getYmdFromMdy($_REQUEST['ToDate']);
}
else {
    $where[] = "DATE(LastUpdatedDate) >= :FromDate";
    $where[] = "DATE(LastUpdatedDate) <= :ToDate";

    $params[":FromDate"] = date('Y-m-d', strtotime("-6 months", strtotime(date('Y-m-d'))));
    $params[":ToDate"] = date('Y-m-d');
}

if(isset($_REQUEST['Status']) && $_REQUEST['Status'] == "Pending") {
    $where[] = "(Status = :Status AND Deleted = 'No')";
    $params[":Status"] = 'Pending';
}
else if(isset($_REQUEST['Status']) && $_REQUEST['Status'] == "Finished") {
    $where[] = "(Status = :Status AND Deleted = 'No')";
    $params[":Status"] = 'Finished';
}
else if(isset($_REQUEST['Status']) && $_REQUEST['Status'] == "Deleted") {
    $where[] = "Deleted = 'Yes'";
}
    
G::Obj('ReportsApplications')->conn_string      =   "USERPORTAL";

//Get user application results
$user_applications_results                      =   G::Obj('ReportsApplications')->getUserApplications($where, "", $order_by, array($params));

//Total Count
G::Obj('ReportsApplications')->conn_string      =   "USERPORTAL";

$total_count                            =   G::Obj('ReportsApplications')->getUserApplicationsCount($where, "", array($params));

$user_applications                      =   $user_applications_results['results'];
$user_applications_cnt                  =   $user_applications_results['count'];

$left_nav_info                          =   G::Obj('Pagination')->getPageNavigationInfo($Start, $Limit, $total_count, '', '');

//Loop through the user applications
foreach ($user_applications as $user_key=>$user_app_info) {
    
    $ApplicationOrgID      =   $user_app_info['OrgID'];
    $ApplicationMultiOrgID =   $user_app_info['MultiOrgID'];
    $ApplicationUserID     =   $user_app_info['UserID'];
    $ApplicationRequestID  =   $user_app_info['RequestID'];
    
    if($user_app_info['Status'] == "Finished") {
        $cell_phone_json    =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $user_app_info['ApplicationID'], 'cellphone');
        $cell_phone_info    =   json_decode($cell_phone_json, true);
        $cell_phone         =   (count($cell_phone_info) > 0) ? implode("-", json_decode($cell_phone_json, true)) : '';
    }
    else {
        $cell_phone_json    =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($ApplicationOrgID, $ApplicationMultiOrgID, $ApplicationUserID, $ApplicationRequestID, 'cellphone');
        $cell_phone         =   implode("-",json_decode($cell_phone_json, true));
    }
    
    $user_applications[$user_key]['CellPhone'] =    $cell_phone;
}

if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "user_applications_list"        =>  $user_applications,
        "previous"                      =>  $left_nav_info['previous'],
        "next"                          =>  $left_nav_info['next'],
        "total_pages"                   =>  $left_nav_info['total_pages'],
        "current_page"                  =>  $left_nav_info['current_page'],
        "total_count"                   =>  $total_count,
        "count_per_page"                =>  $user_applications_cnt,
    ));
}

############        Export Code     ############
if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
    $list = $user_applications;
    require_once IRECRUIT_DIR . 'reports/ExportCompleteIncompleteApplications.inc';
}
############        Export Code     #############
?>
