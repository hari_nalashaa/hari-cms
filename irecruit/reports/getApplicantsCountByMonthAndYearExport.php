<?php
require_once '../Configuration.inc';
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

//Set page title
$title  =   'Applicants Count By Year and Month';

//Get Applicants Count By Month And Year
$apps_list  =   G::Obj('ApplicantsReports')->getApplicantsCountByMonthAndYear($OrgID);
$apps_years =   array_keys($apps_list);

$months_list    =   G::Obj('GenericLibrary')->getMonths();

$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Requisition Approval Reports")
->setLastModifiedBy("Requisition Approval Reports")
->setTitle("iRecruit Requisition Approval Reports")
->setSubject("Excel")
->setDescription("Requisition Approval Reports List")
->setKeywords("phpExcel")
->setCategory("WOTC Applicant Data");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al++]."1")->setValue('');
for($m = 1; $m <= count($months_list); $m++) {
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al++]."1")->setValue($months_list[$m]);

}

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al++]."1")->setValue("Annual Total");

for($y = 0, $l = 2; $y < count($apps_years); $y++, $l++) {
    $alp = 0;
     $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($apps_years[$y]);

    $year_count = 0;
    for($m = 1; $m <= count($months_list); $m++) {
        $year_count += $apps_list[$apps_years[$y]][$months_list[$m]];
        $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($apps_list[$apps_years[$y]][$months_list[$m]]);

    }

    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alp++].$l)->setValue($year_count);

}

###  Append Applicants Data To Spread Sheet ###

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="applicants_by_month_year.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();
