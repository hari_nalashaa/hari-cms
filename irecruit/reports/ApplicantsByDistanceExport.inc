<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

// Create new PHPExcel object
$objPHPExcel = new Spreadsheet();

// Set document properties
$objPHPExcel->getProperties()->setCreator("CostManagementSystems")
->setLastModifiedBy("CostManagementSystems")
->setTitle("iRecruit Applicants Status By Requisition Reports")
->setSubject("Excel")
->setDescription("To view the applicants information those are filtered in reports applicants by status page")
->setKeywords("phpExcel")
->setCategory("Applicants By Status Reports");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();


$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(45);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(false);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);

$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(30);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFill()->getStartColor()->setRGB('0c00ac');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true)->setSize(12)->getColor()->setRGB('ffffff');
$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getAlignment()->setVertical('center');

$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Requisition Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('RequisitionID / JobID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application ID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Name');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Status');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Address');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Distance');


for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha              =   0;
    $CRD                =   $list[$ah];
    
    ###################################################
    
    $req_details        =   G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionID, JobID", $OrgID, $CRD['RequestID']);
    
    $ApplicationID      =   $CRD['ApplicationID'];
    $RequestID          =   $CRD['RequestID'];
    
    $QuestionIDs        =   array('first', 'last', 'email', 'cellphone', 'city', 'state', 'province', 'zip', 'country', 'county');
    $app_data           =   G::Obj('ApplicantsData')->getApplicantData($OrgID, $ApplicationID, $QuestionIDs);
    $phone_ans          =   ($app_data['cellphone'] != "") ? json_decode($app_data['cellphone'], true) : "";
    $ReqMultiOrgID      =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);
    
    $RequisitionID      =   $req_details['RequisitionID'];
    $JobID              =   $req_details['JobID'];
    $Date               =   $CRD['EntryDate'];
    $Status             =   G::Obj('ApplicantDetails')->getProcessOrderDescription($OrgID, $CRD['ProcessOrder']);
    $Name               =   $app_data['first'] . ' ' . $app_data['last'];
    $Email              =   $app_data['email'];
    $Phone              =   G::Obj('Address')->formatPhone($OrgID, $CRD['country'], $phone_ans[0], $phone_ans[1], $phone_ans[2], '');
    
    $city               =   $app_data['city'];
    $state              =   $app_data['state'];
    $province           =   $app_data['province'];
    
    $zipcode            =   $app_data['zip'];
    $country            =   $app_data['country'];
    $county             =   $app_data['county'];
    
    //Swap state variable with province, if country is Canada
    $locaddr            =   G::Obj('Address')->formatAddress($OrgID, $country, $city, $state, $province, $zipcode, $county);
    
    $app_distance       =   $CRD['Distance'];
    $DispositionCode    =   G::Obj('ApplicantDetails')->getDispositionCodeDescription($OrgID, $CRD['DispositionCode']);
    $EffDate            =   $CRD['StatusEffectiveDate'];
    $RequisitionTitle   =   G::Obj('RequisitionDetails')->getJobTitle($OrgID, $ReqMultiOrgID, $CRD['RequestID']);

    $StatusInfo         =   $Status;
    
    if ($DispositionCode) {
        $StatusInfo     .=  ", ";
        $StatusInfo     .=  'Disposition: ' . $DispositionCode;
    }
    if ($EffDate) {
        $StatusInfo     .=  ", ";
        $StatusInfo     .=  'Effective: ' . $EffDate;
    }
    
    ###################################################
    
    if ($Email != "") { $Name .= ' - ' . $Email; } 
    if ($Phone != "") { $Name .= ' - ' . $Phone; } 


$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($RequisitionTitle);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($RequisitionID . "/" . $JobID);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue( $ApplicationID);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($Name);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue( $StatusInfo);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue( $Date);
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(str_replace("&nbsp;", " ", $locaddr));
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue( sprintf("%01.1f", str_replace(",", "", $app_distance)));

  }

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="applicants_by_distance.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();
?>
