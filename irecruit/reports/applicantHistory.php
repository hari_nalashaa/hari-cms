<?php
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
    $TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title =   $title  =   "Applicant History";

// Assign add applicant datepicker variables
$script_vars_footer[] = 'var datepicker_ids = "#applicants_from_date, #applicants_to_date";';
$script_vars_footer[] = 'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer   =   $script_vars_footer;

$TemplateObj->FilterOrganization    =   $FilterOrganization =   $_REQUEST['ddlOrganizationsList'];

// Get Requisition Information
$where   =  array("OrgID = :OrgID");
$params  =  array(":OrgID"=>$OrgID);

// Get Requisitions List
$requsition_results_info        =   $RequisitionsObj->getRequisitionInformation ( "RequestID, Title, RequisitionID, JobID", $where, "", "Title ASC", array ($params) );
$requisitions_results           =   $requsition_results_info['results'];
$requisitions_results_count     =   $requsition_results_info['count'];

//Requisitions List
$TemplateObj->requisitions_results          = $requisitions_results;
$TemplateObj->requisitions_results_count    = $requisitions_results_count;

echo $TemplateObj->displayIrecruitTemplate('views/reports/ApplicantHistory');
?>