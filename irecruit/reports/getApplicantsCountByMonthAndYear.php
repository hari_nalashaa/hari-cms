<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title =   $title  =   'Applicants Count By Year and Month';

//Get Applicants Count By Month And Year
$TemplateObj->apps_list     =   $apps_list  =   G::Obj('ApplicantsReports')->getApplicantsCountByMonthAndYear($OrgID);
$TemplateObj->apps_years    =   array_keys($apps_list);

echo $TemplateObj->displayIrecruitTemplate('views/reports/GetApplicantsCountByMonthAndYear');
?>