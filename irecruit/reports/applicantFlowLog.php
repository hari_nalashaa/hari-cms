<?php
require_once '../Configuration.inc';

// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER['QUERY_STRING']) {
    $TemplateObj->goto = $_SERVER['QUERY_STRING'];
}

// Set page title
$title = "AAP";

$FilterOrganization                         =   $_REQUEST['ddlOrganizationsList'];

//Pagination parameters
$TemplateObj->Start                         =   $Start = 0;
$TemplateObj->Limit                         =   $Limit = $user_preferences['ReportsSearchResultsLimit'];

// Assign add applicant datepicker variables
$script_vars_footer[]                       =   'var datepicker_ids = "#applicants_from_date, #applicants_to_date";';
$script_vars_footer[]                       =   'var date_format = "mm/dd/yy";';

//Get total applicants count
$total_count                                =   $ReportsObj->getApplicantFlowLogCount($FilterOrganization, $OrgID, '', '', date('m/d/Y', strtotime("-12 months",strtotime(date("Y-m-d")))), date('m/d/Y'), '', '');

//Get default applicants list
$applicants_flow_log_results                =   $ReportsObj->getApplicantFlowLog($FilterOrganization, $OrgID, '', '', date('m/d/Y', strtotime("-12 months",strtotime(date("Y-m-d")))), date('m/d/Y'), '', '', '', $Start, $Limit);
$applicants_flow_log                        =   $applicants_flow_log_results['results'];

$export_app_flow_log_results                =   $ReportsObj->getApplicantFlowLog($FilterOrganization, $OrgID, '', '', date('m/d/Y', strtotime("-12 months",strtotime(date("Y-m-d")))), date('m/d/Y'), '', '', '', '', '');
$export_app_flow_log                        =   $export_app_flow_log_results['results'];

$count_list                                 =   array();
$count_list["ApplicantsCount"]              =   count($export_app_flow_log);
$count_list["RaceBlackCount"]               =   0;
$count_list["HispanicCount"]                =   0;
$count_list["RaceAsianCount"]               =   0;
$count_list['RaceIndianCount']              =   0;
$count_list['RaceWhiteCount']               =   0;
$count_list['RaceTwoCount']                 =   0;
$count_list['MaleCount']                    =   0;
$count_list['FemaleCount']                  =   0;
$count_list['DisabilityYesCount']           =   0;
$count_list['DisabilityNoCount']            =   0;
$count_list['DisabilityNonDiscloseCount']   =   0;
$count_list['VeteranIntentifiedCount']      =   0;
$count_list['VeteranNotProtectedCount']     =   0;
$count_list['VeteranNonDiscloseCount']      =   0;

for($k = 0; $k < count($export_app_flow_log); $k++) {

    if($export_app_flow_log[$k]['RaceBlack'] == 1) $count_list["RaceBlackCount"] += 1;
    if($export_app_flow_log[$k]['Hispanic'] == 1) $count_list["HispanicCount"] += 1;
    if($export_app_flow_log[$k]['RaceAsian'] == 1) $count_list["RaceAsianCount"] += 1;
    if($export_app_flow_log[$k]['RaceIndian'] == 1) $count_list['RaceIndianCount'] += 1;
    if($export_app_flow_log[$k]['RaceWhite'] == 1) $count_list['RaceWhiteCount'] += 1;
    if($export_app_flow_log[$k]['RaceTwo'] == 1) $count_list['RaceTwoCount'] += 1;
    if($export_app_flow_log[$k]['Male'] == 1) $count_list['MaleCount'] += 1;
    if($export_app_flow_log[$k]['Female'] == 1) $count_list['FemaleCount'] += 1;
    if($export_app_flow_log[$k]['DisabilityYes'] == 1) $count_list['DisabilityYesCount'] += 1;
    if($export_app_flow_log[$k]['DisabilityNo'] == 1) $count_list['DisabilityNoCount'] += 1;
    if($export_app_flow_log[$k]['DisabilityNonDisclose'] == 1) $count_list['DisabilityNonDiscloseCount'] += 1;
    if($export_app_flow_log[$k]['VeteranIntentified'] == 1) $count_list['VeteranIntentifiedCount'] += 1;
    if($export_app_flow_log[$k]['VeteranNotProtected'] == 1) $count_list['VeteranNotProtectedCount'] += 1;
    if($export_app_flow_log[$k]['VeteranNonDisclose'] == 1) $count_list['VeteranNonDiscloseCount'] += 1;
}

// Get Requisition Information
$where                                      =   array("OrgID = :OrgID");
$params                                     =   array(":OrgID"=>$OrgID);

// Get Requisitions List
$requsition_results_info                    =   $RequisitionsObj->getRequisitionInformation ( "RequestID, Title, RequisitionID, JobID", $where, "", "Title ASC", array ($params) );
$requisitions_results                       =   $requsition_results_info['results'];
$requisitions_results_count                 =   $requsition_results_info['count'];

//Assign to template object
$TemplateObj->requisitions_results          =   $requisitions_results;
$TemplateObj->requisitions_results_count    =   $requisitions_results_count;
$TemplateObj->applicants_flow_log           =   $applicants_flow_log;
$TemplateObj->total_count                   =   $total_count; 
$TemplateObj->scripts_vars_footer           =   $script_vars_footer;
$TemplateObj->title                         =   $title;
$TemplateObj->count_list                    =   $count_list;

echo $TemplateObj->displayIrecruitTemplate('views/reports/ApplicantFlowLog');
?>