<?php
require_once '../Configuration.inc';

//Added Export Condition
/*if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
}*/

$FilterOrganization =   $_REQUEST['FilterOrganization'];

$limit = $user_preferences['ReportsSearchResultsLimit'];
if(isset($_REQUEST['RecordsLimit']) && $_REQUEST['RecordsLimit'] != "") $limit = $_REQUEST['RecordsLimit'];

$start = 0;
if(isset($_REQUEST['IndexStart'])) $start = $_REQUEST['IndexStart'];

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

$order_by   = '';
if (isset ( $_REQUEST ['to_sort'] )) {

    if ($_REQUEST ['to_sort'] == "applicant_name") {
        $order_by           =   " JA.ApplicantSortName $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "application_date") {
        $order_by           =   " JA.EntryDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_title") {
        $order_by           =   " (SELECT R.Title FROM Requisitions R WHERE R.OrgID = JA.OrgID AND R.RequestID = JA.RequestID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "eeo_code") {
        $order_by           =   " (SELECT R.EEOCode FROM Requisitions R WHERE R.OrgID = JA.OrgID AND R.RequestID = JA.RequestID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "application_id") {
        $order_by           =   " JA.ApplicationID $sort_type";
    }
}

//Get total records count
$total_count                                =   G::Obj('Reports')->getApplicantFlowLogCount($FilterOrganization, $OrgID, $_REQUEST['RequestID'], $_REQUEST['EEOCode'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status'], $_REQUEST['ProcessOrder']);

$applicants_flow_log_results                =   G::Obj('Reports')->getApplicantFlowLog($FilterOrganization, $OrgID, $_REQUEST['RequestID'], $_REQUEST['EEOCode'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status'], $_REQUEST['ProcessOrder'], $order_by, $start, $limit);
$applicants_flow_log                        =   $applicants_flow_log_results['results'];

$export_app_flow_log_results                =   G::Obj('Reports')->getApplicantFlowLog($FilterOrganization, $OrgID, $_REQUEST['RequestID'], $_REQUEST['EEOCode'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['Status'], $_REQUEST['ProcessOrder'], $order_by, '', '');
$export_app_flow_log                        =   $export_app_flow_log_results['results'];

$count_list                                 =   array();
$count_list["ApplicantsCount"]              =   count($export_app_flow_log);
$count_list["RaceBlackCount"]               =   0;
$count_list["HispanicCount"]                =   0;
$count_list["RaceAsianCount"]               =   0;
$count_list['RaceIndianCount']              =   0;
$count_list['RaceWhiteCount']               =   0;
$count_list['RaceTwoCount']                 =   0;
$count_list['MaleCount']                    =   0;
$count_list['FemaleCount']                  =   0;
$count_list['DisabilityYesCount']           =   0;
$count_list['DisabilityNoCount']            =   0;
$count_list['DisabilityNonDiscloseCount']   =   0;
$count_list['VeteranIntentifiedCount']      =   0;
$count_list['VeteranNotProtectedCount']     =   0;
$count_list['VeteranNonDiscloseCount']      =   0;

for($k = 0; $k < count($export_app_flow_log); $k++) {
	
	if($export_app_flow_log[$k]['RaceBlack'] == 1) $count_list["RaceBlackCount"] += 1;
	if($export_app_flow_log[$k]['Hispanic'] == 1) $count_list["HispanicCount"] += 1;
	if($export_app_flow_log[$k]['RaceAsian'] == 1) $count_list["RaceAsianCount"] += 1;
	if($export_app_flow_log[$k]['RaceIndian'] == 1) $count_list['RaceIndianCount'] += 1;
	if($export_app_flow_log[$k]['RaceWhite'] == 1) $count_list['RaceWhiteCount'] += 1;
	if($export_app_flow_log[$k]['RaceTwo'] == 1) $count_list['RaceTwoCount'] += 1;
	if($export_app_flow_log[$k]['Male'] == 1) $count_list['MaleCount'] += 1;
	if($export_app_flow_log[$k]['Female'] == 1) $count_list['FemaleCount'] += 1;
	if($export_app_flow_log[$k]['DisabilityYes'] == 1) $count_list['DisabilityYesCount'] += 1;
	if($export_app_flow_log[$k]['DisabilityNo'] == 1) $count_list['DisabilityNoCount'] += 1;
	if($export_app_flow_log[$k]['DisabilityNonDisclose'] == 1) $count_list['DisabilityNonDiscloseCount'] += 1;
	if($export_app_flow_log[$k]['VeteranIntentified'] == 1) $count_list['VeteranIntentifiedCount'] += 1;
	if($export_app_flow_log[$k]['VeteranNotProtected'] == 1) $count_list['VeteranNotProtectedCount'] += 1;
	if($export_app_flow_log[$k]['VeteranNonDisclose'] == 1) $count_list['VeteranNonDiscloseCount'] += 1;
}

$left_nav_info                      =   G::Obj('Pagination')->getPageNavigationInfo($start, $limit, $total_count, '', '');

if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "applicants_list"           =>  $applicants_flow_log,
        "previous"                  =>  $left_nav_info['previous'],
        "next"                      =>  $left_nav_info['next'],
        "total_pages"               =>  $left_nav_info['total_pages'],
        "current_page"              =>  $left_nav_info['current_page'],
        "total_count"               =>  $total_count,
        "count_list"                =>  $count_list,
        "export_app_flow_log"       =>  $export_app_flow_log
    ));
}

############        Export Code     ############
if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
    $list = $export_app_flow_log;
    require_once IRECRUIT_DIR . 'reports/ExportApplicantFlowLog.inc';
}
############        Export Code     #############
?>