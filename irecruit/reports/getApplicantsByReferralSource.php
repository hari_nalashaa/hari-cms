<?php
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto = $_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title = "Applicants By Referral Source";

// Assign add applicant datepicker variables
$script_vars_footer [] = 'var datepicker_ids = "#application_from_date, #application_to_date";';
$script_vars_footer [] = 'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer = $script_vars_footer;

//Get Applicants based on RequestID
$where_info     =   array("OrgID = :OrgID", "Approved = :Approved", "Active = :Active");
//Set parameters
$params_info    =   array(":OrgID"=>$OrgID, ":Approved"=>'Y', ":Active"=>'Y');
//Set columns
$columns        =   "Title, RequestID, RequisitionID, JobID";
//Order By
$order_by       =   "Title, RequisitionID, JobID DESC";
// Get Requisition Information
$TemplateObj->req_results =  $req_results = $RequisitionsObj->getRequisitionInformation ( $columns, $where_info, "", $order_by, array ($params_info) );

$TemplateObj->requisitions_list  =   $requisitions_list     =   $req_results['results'];

echo $TemplateObj->displayIrecruitTemplate ( 'views/reports/ApplicantsByReferralSource' );
?>