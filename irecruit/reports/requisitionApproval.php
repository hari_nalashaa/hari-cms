<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title =   $title  =   'Requisition Approvals';

//Set footer scripts
$scripts_footer[]   =   "js/requisition.js";


// Assign add applicant datepicker variables
$script_vars_footer [] = 'var datepicker_ids = "#from_date, #to_date";';
$script_vars_footer [] = 'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer = $script_vars_footer;

if(isset($_REQUEST['from_date'])) {
    $from_date  =   $_REQUEST['from_date'];
}
else {
    $from_date  =   date('m/d/Y', strtotime("-36 months", strtotime(date('Y-m-d'))));
}

if(isset($_REQUEST['to_date'])) {
    $to_date  =   $_REQUEST['to_date'];
}
else {
    $to_date  =   date('m/d/Y');
}

//Get Requisition Approval Records
$req_appr_fields    =   G::Obj('RequisitionsReports')->getRequisitionsApprovalFieldsInfo($OrgID, $from_date, $to_date);

$TemplateObj->req_approval_list     =   $req_approval_list  =   $req_appr_fields["req_approval_list"];
$TemplateObj->map_columns           =   $map_columns        =   $req_appr_fields["map_columns"];

$TemplateObj->from_date             =   $from_date;
$TemplateObj->to_date               =   $to_date;

//Page scripts footer
$TemplateObj->page_scripts_footer   =   $scripts_footer;

echo $TemplateObj->displayIrecruitTemplate('views/reports/RequisitionApproval');
?>
