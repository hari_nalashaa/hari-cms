<?php 
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$objPHPExcel = new Spreadsheet();


// Set document properties
$objPHPExcel->getProperties()->setCreator("David")
->setLastModifiedBy("David")
->setTitle("iRecruit Applicants History Reports")
->setSubject("Excel")
->setDescription("To view the applicants information those are filtered in reports applicants history page")
->setKeywords("phpExcel")
->setCategory("Applicants History Reports");

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

$al = 0;
$alphabets_list = $GenericLibraryObj->getAlphabets();
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Applicant Name');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Applicant ID');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Job Title');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Application Date');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Status');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Disposition Code');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Last Updated');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Comments / Notes');
$al++;
$objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Updated By');

if($OrgID == 'I20121215') { //United States Conference of Catholic Bishops
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('EEO Code');
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Ethnicity');
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('AA Veteran');
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('AA Veteran Status');
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Sage Hire Date');
    $al++;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$al]."1")->setValue('Department Office Secretariat');
}

for($ah = 0, $e = 2; $ah < count($list); $ah++, $e++) {
    $alpha = 0;
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicantName']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ApplicationID']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['JobTitle']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['EntryDate']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['ProcessOrder']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['DispositionCode']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue($list[$ah]['StatusEffectiveDate']);
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['Comments']));
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['UserID']));

    
    if($OrgID == 'I20121215') { //United States Conference of Catholic Bishops
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['EEOCode']));
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['AAEthnicity']));
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['AAVeteran']));
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['AAVeteranStatus']));
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['SageHireDate']));
    $objPHPExcel->getActiveSheet()->getCell($alphabets_list[$alpha++].$e)->setValue(strip_tags($list[$ah]['DOS']));

        
    }
}


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="applicants_history.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');
$writer = IOFactory::createWriter($objPHPExcel , 'Xlsx');
$writer->save('php://output');
exit();

?>
