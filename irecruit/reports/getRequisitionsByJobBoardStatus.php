<?php
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
    $TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title =   $title  =   "Job Board Status";

$FilterOrganization =   $_REQUEST['FilterOrganization'];

//Set active
$Active =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : 'Y';
$Limit  =   isset($_REQUEST['Limit']) ? $_REQUEST['Limit'] : $user_preferences['ReportsSearchResultsLimit'];

$IsRequisitionApproved = "Y";

require_once IRECRUIT_DIR . 'services/GetRequisitions.inc';

############        Export Code     ############
if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
    $requisitions_info = json_decode($requisitions_info, true);
    $list = $requisitions_info['results'];
    require_once IRECRUIT_DIR . 'reports/ExportRequisitionsByJobBoardStatus.inc';
}
############        Export Code     #############
?>
