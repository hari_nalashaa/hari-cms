<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

//Set columns
$columns = "date_format(min(DateEntered),'%m/%d/%Y') MinDateEntered";
$columns .= ", date_format(max(DateEntered),'%m/%d/%Y') MaxDateEntered";
//Set where condition
$where = array("OrgID = :OrgID","MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>Reports::$ORGID,":MultiOrgID"=>Reports::$MULTIORGID);
//Set Order by 
$order_by="";

//Execute Final Query
$req_search_results = $RequisitionsObj->getRequisitionInformation($columns, $where, "", $order_by, array($params)); 
$FinalDate=$req_search_results['results'][0]['MaxDateEntered'];
$BeginDate=$req_search_results['results'][0]['MinDateEntered'];

?>
myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

visualize(function(v){

v.login(myConfig);

var reportReq = "";
var reportApp = "";
var reportCost = "";

ReqReport("<?php echo Reports::$ORGID; ?>","<?php echo Reports::$MULTIORGID; ?>","R.Active = 'Y'","Title","ASC","<?php echo $BeginDate; ?>","<?php echo $FinalDate; ?>");

function AppReport (OrgID,MultiOrgID,RequestID,OrderBy,OrderDirection,FromDate,ToDate) {

	var td = new Date(ToDate).toLocaleDateString();
	var fd = new Date(FromDate).toLocaleDateString();

        $('input[name=OrgID]').val(OrgID);
        $('input[name=MultiOrgID]').val(MultiOrgID);
        $('input[name=RequestID]').val(RequestID);
	$('input[name=FromDate]').val(fd);
        $('input[name=ToDate]').val(td);
        $('option[value="'+OrderBy+'"]').attr('selected','selected');
        $('option[value="'+OrderDirection+'"]').attr('selected','selected');

    	reportApp = v.report({
        container: "#container",
	resource: "/reports/iRecruit_Reports/Applications",
        params: { "OrgID":OrgID, "MultiOrgID":MultiOrgID, "RequestID":RequestID, "OrderBy":[OrderBy + " " + OrderDirection], "FromDate":FromDate, "ToDate":ToDate },
	linkOptions: {
            beforeRender: function (linkToElemPairs) {
                linkToElemPairs.forEach(showCursor);
            },
            events: {
                "click": function(ev, link){
                   if (link.type == "Reference"){
			if (link.tooltip == "Link to summary page in iRecruit") {
			 	window.open([link.href],'_blank');
			}
                   }
                     console.log(link);
                }
            }
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsApp = totalPages;
	        displayApp();

           }
        },
	success: function () {
	   exportApp.removeAttribute("disabled");
	}
	});     

}

function ReqReport (OrgID,MultiOrgID,Filter,OrderBy,OrderDirection,FromDate,ToDate) {

        $('input[name=OrgID]').val(OrgID);
        $('input[name=MultiOrgID]').val(MultiOrgID);
        $('option[value="'+OrderDirection+'"]').attr('selected','selected');
        $('option[value="'+OrderBy+'"]').attr('selected','selected');
	$('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);
        $('option[value="'+Filter+'"]').attr('selected','selected');

        ToDate = new Date(ToDate).toISOString().slice(0, 10);
        FromDate = new Date(FromDate).toISOString().slice(0, 10);

    	reportReq = v.report({
	resource: "/reports/iRecruit_Reports/Requisitions",
        container: "#container",
        params: { "OrgID":[OrgID], "MultiOrgID":[MultiOrgID], "Filter":[Filter], "OrderBy":[OrderBy + " " + OrderDirection], "FromDate":[FromDate], "ToDate":[ToDate] },
        linkOptions: {
            beforeRender: function (linkToElemPairs) {
                linkToElemPairs.forEach(showCursor);
            },
            events: {
                "click": function(ev, link){
	
                     if (link.type == "ReportExecution"){
			if (link.tooltip == "Applicant Report by Requisition") {

				var appData = {};
        			params = $('#formApp').serializeArray();

				$.each(params, function(i, val) {
				    appData[val.name]=val.value;
				});

				 AppReport([link.parameters.OrgID],[link.parameters.MultiOrgID],[link.parameters.RequestID],[appData['OrderBy']],[appData['OrderDirection']],[link.parameters.FromDate],[link.parameters.ToDate]);
			}

                    }
                    console.log(link);
                }
            }
        },
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {

		tpgsReq = totalPages;
	        displayReq(totalPages);

            }
        },
	success: function () {
            exportReq.removeAttribute("disabled");
        }
    });

} // end Req Report


    function showCursor(pair){
           var el = pair.element;
               el.style.cursor = "pointer";
    }

    function displayCost(){

	document.getElementById('ReportControlsReq').style.display = "none";
	document.getElementById('previousPageReq').style.display = "none";
	document.getElementById('nextPageReq').style.display = "none";
	document.getElementById('exportReq').style.display = "none";

	document.getElementById('ReportControlsApp').style.display = "none";
	document.getElementById('previousPageApp').style.display = "none";
	document.getElementById('nextPageApp').style.display = "none";
	document.getElementById('exportApp').style.display = "none";
	document.getElementById('mainReportApp').style.display = "none";

	document.getElementById('ReportControlsCost').style.display = "block";
	document.getElementById('exportCost').style.display = "block";
	document.getElementById('mainReportCost').style.display = "block";

    }

    function displayApp(){

	document.getElementById('ReportControlsReq').style.display = "none";
	document.getElementById('previousPageReq').style.display="none";
	document.getElementById('nextPageReq').style.display = "none";
	document.getElementById('exportReq').style.display = "none";

	document.getElementById('ReportControlsApp').style.display = "block";
	document.getElementById('previousPageApp').style.display="none";
	document.getElementById('nextPageApp').style.display = "none";
	document.getElementById('exportApp').style.display = "block";
	document.getElementById('mainReportApp').style.display = "block";

	document.getElementById('ReportControlsCost').style.display = "none";
	document.getElementById('exportCost').style.display = "none";
	document.getElementById('mainReportCost').style.display = "none";

	var currentPage = reportApp.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageApp').style.display="block";
        }
        if (currentPage < tpgsApp) {
         document.getElementById('nextPageApp').style.display = "block";
        }


    }

    function displayReq(){

	document.getElementById('ReportControlsReq').style.display = "block";
        document.getElementById('previousPageReq').style.display = "none";
        document.getElementById('nextPageReq').style.display = "none";
	document.getElementById('exportReq').style.display = "block";

	document.getElementById('ReportControlsApp').style.display = "none";
        document.getElementById('previousPageApp').style.display = "none";
        document.getElementById('nextPageApp').style.display = "none";
	document.getElementById('exportApp').style.display = "none";
        document.getElementById('mainReportApp').style.display = "none";

	document.getElementById('ReportControlsCost').style.display = "none";
	document.getElementById('exportCost').style.display = "none";
        document.getElementById('mainReportCost').style.display = "none";

        var currentPage = reportReq.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageReq').style.display = "block";
        }

        if (currentPage < tpgsReq) {
         document.getElementById('nextPageReq').style.display = "block";
        }

	reportReq.run();

    }

    $("#previousPageReq").click(function() {
        var currentPage = reportReq.pages() || 1;

        document.getElementById('nextPageReq').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageReq').style.display = "none";
        }
 
        reportReq
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });
 
    $("#nextPageReq").click(function() {
        var currentPage = reportReq.pages() || 1;

        document.getElementById('previousPageReq').style.display = "block";

        if(currentPage >= tpgsReq-1) {
         document.getElementById('nextPageReq').style.display = "none";
        }
 
        reportReq
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#previousPageApp").click(function() {
        var currentPage = reportApp.pages() || 1;

        document.getElementById('nextPageApp').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageApp').style.display = "none";
        }
 
        reportApp
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });
 
    $("#nextPageApp").click(function() {
        var currentPage = reportApp.pages() || 1;

        document.getElementById('previousPageApp').style.display = "block";

        if(currentPage >= tpgsApp-1) {
         document.getElementById('nextPageApp').style.display = "none";
        }
 
        reportApp
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportReq").click(function () {

        reportReq.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
	   exportWindow = window.open(url,"Export");
           setTimeout(function(){
              exportWindow.close();
           },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("#exportApp").click(function () {

        reportApp.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
	   exportWindow = window.open(url,"Export");
           setTimeout(function(){
              exportWindow.close();
           },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("#exportCost").click(function () {

        reportCost.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
	   exportWindow = window.open(url,"Export");
           setTimeout(function(){
              exportWindow.close();
           },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("#mainReportApp").click(function () {
	displayReq();
    });

    $("#mainReportCost").click(function () {
	displayReq();
    });

    $("form[id='formApp']").submit(function () {

	var appData = {};
        params = $('#formApp').serializeArray();

	$.each(params, function(i, val) {
	    appData[val.name]=val.value;
	});

        var ToDate = new Date(appData['ToDate']).toISOString().slice(0, 10);
        var FromDate = new Date(appData['FromDate']).toISOString().slice(0, 10);

        reportApp
            .params({ "OrgID": [appData['OrgID']], "MultiOrgID":[appData['MultiOrgID']], "RequestID":[appData['RequestID']], "OrderBy":[appData['OrderBy'] + " " + appData['OrderDirection']], "FromDate":[FromDate], "ToDate":[ToDate]})
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });

    $("form[id='formReq']").submit(function () {

	var reqData = {};
        params = $('#formReq').serializeArray();

	$.each(params, function(i, val) {
	    reqData[val.name]=val.value;
	});

        var ToDate = new Date(reqData['ToDate']).toISOString().slice(0, 10);
        var FromDate = new Date(reqData['FromDate']).toISOString().slice(0, 10);

        reportReq
            .params({ "OrgID": [reqData['OrgID']], "MultiOrgID":[reqData['MultiOrgID']], "Filter":[reqData['Filter']], "OrderBy":[reqData['OrderBy'] + " " + reqData['OrderDirection']], "FromDate":[FromDate], "ToDate":[ToDate]})
            .run()
                .fail(function(err) { alert(err); } );


	return false;
    });

}, function(err) {
  alert("error: " + err);
});
