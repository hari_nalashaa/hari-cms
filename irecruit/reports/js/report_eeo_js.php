<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

$results = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
                $DR = $results['DefaultSearchDateRange'];
                if ($DR == "") {
                        $DR = "90";
                }
                //Get Dates List
                $columns = "date_format(now(),'%m/%d/%Y'),
                date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'),
                        date_format(now(),'%Y-%m-%d'),
                        date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%Y-%m-%d')";
                list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($MysqlHelperObj->getDatesList($columns));

?>
$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

visualize(function(v){


var reportEEO = "";

EEOReport("<?php echo Reports::$ORGID; ?>","<?php echo Reports::$MULTIORGID; ?>","<?php echo $StartDate; ?>","<?php echo $EndDate; ?>","EEO");

function EEOReport (OrgID,MultiOrgID,FromDate,ToDate,Report) {


        $('input[name=OrgID]').val(OrgID);
        $('input[name=MultiOrgID]').val(MultiOrgID);
        $('option[value="'+Report+'"]').attr('selected','selected');
        $('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);

	ToDate = new Date(ToDate).toISOString().slice(0, 10);
	FromDate = new Date(FromDate).toISOString().slice(0, 10);

    	reportEEO = v.report({
	resource: "/reports/iRecruit_Reports/EEO",
        container: "#container",
        params: { "OrgID":[OrgID], "MultiOrgID":[MultiOrgID], "FromDate":[FromDate], "ToDate":[ToDate], "Report":[Report]},
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsEEO = totalPages;
	        displayEEO();
            }
        },
	success: function () {
	    displayEEO();
            exportEEO.removeAttribute("disabled");
        }
    });


} // end EEO Report


    function displayEEO(){

	document.getElementById('ReportControlsEEO').style.display = "block";
        document.getElementById('previousPageEEO').style.display = "none";
        document.getElementById('nextPageEEO').style.display = "none";
	document.getElementById('exportEEO').style.display = "block";

        var currentPage = reportEEO.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageEEO').style.display = "block";
        }

        if (currentPage < tpgsEEO) {
         document.getElementById('nextPageEEO').style.display = "block";
        }

	reportEEO.run();

    }

    $("#previousPageEEO").click(function() {
        var currentPage = reportEEO.pages() || 1;

        document.getElementById('nextPageEEO').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageEEO').style.display = "none";
        }
 
        reportEEO
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });
 
    $("#nextPageEEO").click(function() {
        var currentPage = reportEEO.pages() || 1;

        document.getElementById('previousPageEEO').style.display = "block";

        if(currentPage >= tpgsEEO-1) {
         document.getElementById('nextPageEEO').style.display = "none";
        }
 
        reportEEO
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportEEO").click(function () {

        reportEEO.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formEEO']").submit(function () {

	var eeoData = {};
        params = $('#formEEO').serializeArray();

	$.each(params, function(i, val) {
	    eeoData[val.name]=val.value;
	});

	var ToDate = new Date(eeoData['ToDate']).toISOString().slice(0, 10);
	var FromDate = new Date(eeoData['FromDate']).toISOString().slice(0, 10);

        reportEEO
            .params({ "FromDate": [FromDate], "ToDate": [ToDate], "OrgID": [eeoData['OrgID']], "MultiOrgID": [eeoData['MultiOrgID']], "Report":[eeoData['Report']]})
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });

}, function(err) {
  alert("error: " + err);
});
