<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

$results = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
                $DR = $results['DefaultSearchDateRange'];
                if ($DR == "") {
                        $DR = "90";
                }
                //Get Dates List
                $columns = "date_format(now(),'%m/%d/%Y'),
                date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'),
                        date_format(now(),'%Y-%m-%d'),
                        date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%Y-%m-%d')";
                list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($MysqlHelperObj->getDatesList($columns));

?>
$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

visualize(function(v){

var reportApp = "";

AppReport("<?php echo Reports::$ORGID; ?>","<?php echo Reports::$MULTIORGID; ?>","<?php echo $_REQUEST['RequestID']; ?>","JA.ApplicantSortName","ASC","<?php echo $BeginDate; ?>","<?php echo $FinalDate; ?>");

function AppReport (OrgID,MultiOrgID,RequestID,OrderBy,OrderDirection,FromDate,ToDate) {

        $('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);
        $('input[name=OrgID]').val(OrgID);
        $('input[name=MultiOrgID]').val(MultiOrgID);
        $('option[value="'+RequestID+'"]').attr('selected','selected');
        $('option[value="'+OrderBy+'"]').attr('selected','selected');
        $('option[value="'+OrderDirection+'"]').attr('selected','selected');

        ToDate = new Date(ToDate).toISOString().slice(0, 10);
        FromDate = new Date(FromDate).toISOString().slice(0, 10);

        reportApp = v.report({
        container: "#container",
        resource: "/reports/iRecruit_Reports/Applications",
        params: { "OrgID":[OrgID], "MultiOrgID":[MultiOrgID], "RequestID":[RequestID], "OrderBy":[OrderBy + " " + OrderDirection], "FromDate":[FromDate], "ToDate":[ToDate] },
        linkOptions: {
            beforeRender: function (linkToElemPairs) {
                linkToElemPairs.forEach(showCursor);
            },
            events: {
                "click": function(ev, link){
                   if (link.type == "Reference"){
                        if (link.tooltip == "Link to summary page in iRecruit") {
                                window.open([link.href],'_blank');
                        }
                   }
                     console.log(link);
                }
            }
        },
        error: function (err) {
            alert(err.message);
        },
        events: {
            changeTotalPages: function(totalPages) {
                tpgsApp = totalPages;
                displayApp();

           }
        },
        success: function () {
           exportApp.removeAttribute("disabled");
        }
        });

}

    function displayApp(){

	document.getElementById('ReportControlsApp').style.display = "block";
        document.getElementById('previousPageApp').style.display = "none";
        document.getElementById('nextPageApp').style.display = "none";
	document.getElementById('exportApp').style.display = "block";

        var currentPage = reportApp.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageApp').style.display = "block";
        }

        if (currentPage < tpgsApp) {
         document.getElementById('nextPageApp').style.display = "block";
        }

	reportApp.run();

    }

    $("#previousPageApp").click(function() {
        var currentPage = reportApp.pages() || 1;

        document.getElementById('nextPageApp').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageApp').style.display = "none";
        }
 
        reportApp
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });
 
    $("#nextPageApp").click(function() {
        var currentPage = reportApp.pages() || 1;

        document.getElementById('previousPageApp').style.display = "block";

        if(currentPage >= tpgsApp-1) {
         document.getElementById('nextPageApp').style.display = "none";
        }
 
        reportApp
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportApp").click(function () {

        reportApp.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    function showCursor(pair){
           var el = pair.element;
               el.style.cursor = "pointer";
    }

    $("form[id='formApp']").submit(function () {

        var appData = {};
        params = $('#formApp').serializeArray();

        $.each(params, function(i, val) {
            appData[val.name]=val.value;
        });

        var ToDate = new Date(appData['ToDate']).toISOString().slice(0, 10);
        var FromDate = new Date(appData['FromDate']).toISOString().slice(0, 10);

        reportApp
            .params({ "OrgID": [appData['OrgID']], "MultiOrgID":[appData['MultiOrgID']], "RequestID":[appData['RequestID']], "OrderBy":[appData['OrderBy'] + " " + appData['OrderDirection']], "FromDate":[FromDate], "ToDate":[ToDate] })
            .run()
                .fail(function(err) { alert(err); } );

        return false;
    });


}, function(err) {
  alert("error: " + err);
});
