<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

$results = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
                $DR = $results['DefaultSearchDateRange'];
                if ($DR == "") {
                        $DR = "90";
                }
                //Get Dates List
                $columns = "date_format(now(),'%m/%d/%Y'),
                date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'),
                        date_format(now(),'%Y-%m-%d'),
                        date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%Y-%m-%d')";
                list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($MysqlHelperObj->getDatesList($columns));


?>
myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

visualize(function(v){

v.login(myConfig);

var reportActivity = "";

ActivityReport("<?php echo Reports::$ORGID; ?>","<?php echo $StartDate; ?>","<?php echo $EndDate; ?>");


function ActivityReport (OrgID,FromDate,ToDate) {

        $('input[name=OrgID]').val(OrgID);
        $('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);

        ToDate = new Date(ToDate).toISOString().slice(0, 10);
        FromDate = new Date(FromDate).toISOString().slice(0, 10);

    	reportActivity = v.report({
	resource: "/reports/iRecruit_Reports/iRecruitActivity",
        container: "#container",
        params: { "OrgID":[OrgID], "FromDate":[FromDate], "ToDate":[ToDate]},
	linkOptions: {
	    beforeRender: function (linkToElemPairs) {
		linkToElemPairs.forEach(showCursor);
	    },
            events: {
                "click": function(ev, link){
                    if (link.type == "ReportExecution"){

                    }
                    console.log(link);
	
                }
            }
        },
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsActivity = totalPages;
	        displayActivity();
            }
        },
	success: function () {
	    displayActivity();
            exportActivity.removeAttribute("disabled");
        }
    });



} // end Activity Report

    function showCursor(pair){
           var el = pair.element;
               el.style.cursor = "pointer";
    }

    function displayActivity(){

	document.getElementById('ReportControlsActivity').style.display = "block";
        document.getElementById('previousPageActivity').style.display = "none";
        document.getElementById('nextPageActivity').style.display = "none";
	document.getElementById('exportActivity').style.display = "block";

        var currentPage = reportActivity.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageActivity').style.display = "block";
        }

        if (currentPage < tpgsActivity) {
         document.getElementById('nextPageActivity').style.display = "block";
        }

	reportActivity.run();

    }


    $("#previousPageActivity").click(function() {
        var currentPage = reportActivity.pages() || 1;

        document.getElementById('nextPageActivity').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageActivity').style.display = "none";
        }

        reportActivity
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
 
    });
 
    $("#nextPageActivity").click(function() {
        var currentPage = reportActivity.pages() || 1;

        document.getElementById('previousPageActivity').style.display = "block";

        if(currentPage >= tpgsActivity-1) {
         document.getElementById('nextPageActivity').style.display = "none";
        }

        reportActivity
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportActivity").click(function () {

        reportActivity.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formActivity']").submit(function () {

	var activityData = {};
        params = $('#formActivity').serializeArray();

	$.each(params, function(i, val) {
	    activityData[val.name]=val.value;
	});

        var ToDate = new Date(activityData['ToDate']).toISOString().slice(0, 10);
        var FromDate = new Date(activityData['FromDate']).toISOString().slice(0, 10);

        reportActivity
            .params({ "FromDate": [FromDate], "ToDate": [ToDate], "OrgID": [activityData['OrgID']] })
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });


    $("#mainReportiRecruitActivity").click(function () {

	var activityData = {};
        params = $('#formActivity').serializeArray();

	$.each(params, function(i, val) {
	    activityData[val.name]=val.value;
	});

	ActivityReport(activityData['OrgID'],[activityData['FromDate']],[activityData['ToDate']]);
    });

}, function(err) {
  alert("error: " + err);
});

