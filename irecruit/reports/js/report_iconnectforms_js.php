<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

$results    =   G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
$DR         =   $results['DefaultSearchDateRange'];

if ($DR == "") {
        $DR = "90";
}

//Get Dates List
$columns = "date_format(now(),'%m/%d/%Y'), date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'), date_format(now(),'%Y-%m-%d'), date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%Y-%m-%d')";
list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($MysqlHelperObj->getDatesList($columns));
?>
myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

visualize(function(v){

v.login(myConfig);

var reportiConnectForms = "";

iConnectFormsReport("<?php echo Reports::$ORGID; ?>","<?php echo $StartDate; ?>","<?php echo $EndDate; ?>","JA.EntryDate","DESC");


function iConnectFormsReport (OrgID,FromDate,ToDate,OrderBy,OrderDirection) {

        $('input[name=OrgID]').val(OrgID);
        $('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);
	$('option[value="'+OrderBy+'"]').attr('selected','selected');
        $('option[value="'+OrderDirection+'"]').attr('selected','selected');

        ToDate = new Date(ToDate).toISOString().slice(0, 10);
        FromDate = new Date(FromDate).toISOString().slice(0, 10);

    	reportiConnectForms = v.report({
	resource: "/reports/iRecruit_Reports/iConnectForms",
        container: "#container",
        params: { "OrgID":[OrgID], "FromDate":[FromDate], "ToDate":[ToDate], "OrderBy":[OrderBy + " " + OrderDirection] },
	linkOptions: {
            beforeRender: function (linkToElemPairs) {
                linkToElemPairs.forEach(showCursor);
            },
            events: {
                "click": function(ev, link){
                   if (link.type == "Reference"){
                        if (link.tooltip == "Link to summary page in iRecruit") {
                                window.open([link.href],'_blank');
                        }
                   }
                     console.log(link);
                }
            }
        },
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsiConnectForms = totalPages;
	        displayiConnectForms();
            }
        },
	success: function () {
	    displayiConnectForms();
            //exportiConnectForms.removeAttribute("disabled");
        }
    });



} // end iConnect Forms Report

    function showCursor(pair){
           var el = pair.element;
               el.style.cursor = "pointer";
    }

    function displayiConnectForms(){

	document.getElementById('ReportControlsiConnectForms').style.display = "block";
        document.getElementById('previousPageiConnectForms').style.display = "none";
        document.getElementById('nextPageiConnectForms').style.display = "none";
	document.getElementById('exportiConnectForms').style.display = "block";

        var currentPage = reportiConnectForms.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageiConnectForms').style.display = "block";
        }

        if (currentPage < tpgsiConnectForms) {
         document.getElementById('nextPageiConnectForms').style.display = "block";
        }

	reportiConnectForms.run();

    }


    $("#previousPageiConnectForms").click(function() {
        var currentPage = reportiConnectForms.pages() || 1;

        document.getElementById('nextPageiConnectForms').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageiConnectForms').style.display = "none";
        }

        reportiConnectForms
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
 
    });
 
    $("#nextPageiConnectForms").click(function() {
        var currentPage = reportiConnectForms.pages() || 1;

        document.getElementById('previousPageiConnectForms').style.display = "block";

        if(currentPage >= tpgsiConnectForms-1) {
         document.getElementById('nextPageiConnectForms').style.display = "none";
        }

        reportiConnectForms
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportiConnectForms").click(function () {

        reportiConnectForms.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formiConnectForms']").submit(function () {

	var iconnectData = {};
        params = $('#formiConnectForms').serializeArray();

	$.each(params, function(i, val) {
	    iconnectData[val.name]=val.value;
	});

        var ToDate = new Date(iconnectData['ToDate']).toISOString().slice(0, 10);
        var FromDate = new Date(iconnectData['FromDate']).toISOString().slice(0, 10);

        reportiConnectForms
            .params({ "FromDate": [FromDate], "ToDate": [ToDate], "OrgID": [iconnectData['OrgID']], "OrderBy":[iconnectData['OrderBy'] + " " + iconnectData['OrderDirection']] })
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });

}, function(err) {
  alert("error: " + err);
});

