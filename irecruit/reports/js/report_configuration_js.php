<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

?>
myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

visualize(function(v){

v.login(myConfig);

var reportConfiguration= "";
var reportUserPortal = "";

ConfigurationReport("<?php echo Reports::$ORGID; ?>");

function UserPortalReport (OrgID,OrderBy,OrderDirection) {

        reportUserPortal = v.report({
        container: "#container",
        resource: "/reports/iRecruit_Reports/PortalUsers",
        params: { "OrgID": [OrgID],
	   "OrderBy":[OrderBy + " " + OrderDirection]
        },
        error: function (err) {
           alert(err.message);
        },
        events: {
                changeTotalPages: function(totalPages) {
		   tpgsUserPortal = totalPages;
                   displayUserPortal();
                }
        },
        success: function () {
                exportUserPortal.removeAttribute("disabled");
        }
        });

}

function ConfigurationReport (OrgID) {

        $('input[name=OrgID]').val(OrgID);

    	reportConfiguration = v.report({
	resource: "/reports/iRecruit_Reports/iRecruitConfiguration",
        container: "#container",
        params: { "OrgID":[OrgID] },
	linkOptions: {
	    beforeRender: function (linkToElemPairs) {
		linkToElemPairs.forEach(showCursor);
	    },
            events: {
                "click": function(ev, link){
                     if (link.type == "ReportExecution"){
                        if (link.tooltip == "User Report") {

                                var portalData = {};
                                params = $('#formUserPortal').serializeArray();

                                $.each(params, function(i, val) {
                                    portalData[val.name]=val.value;
                                });

                                UserPortalReport(link.parameters.OrgID,link.parameters.OrderBy,'ASC');

                        }

                    }
                    console.log(link);
	
                }
            }
        },
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsConfiguration = totalPages;
	        displayConfiguration();
            }
        },
	success: function () {
	    displayConfiguration();
            exportConfiguration.removeAttribute("disabled");
        }
    });



} // end Configuration Report

    function showCursor(pair){
           var el = pair.element;
               el.style.cursor = "pointer";
    }

    function displayConfiguration(){

	document.getElementById('ReportControlsUserPortal').style.display = "none";
        document.getElementById('previousPageUserPortal').style.display = "none";
        document.getElementById('nextPageUserPortal').style.display = "none";
	document.getElementById('exportUserPortal').style.display = "none";
	document.getElementById('mainReportiRecruitConfiguration').style.display = "none";

	document.getElementById('ReportControlsConfiguration').style.display = "block";
        document.getElementById('previousPageConfiguration').style.display = "none";
        document.getElementById('nextPageConfiguration').style.display = "none";
	document.getElementById('exportConfiguration').style.display = "block";

        var currentPage = reportConfiguration.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageConfiguration').style.display = "block";
        }

        if (currentPage < tpgsConfiguration) {
         document.getElementById('nextPageConfiguration').style.display = "block";
        }

	reportConfiguration.run();

    }


    $("#previousPageConfiguration").click(function() {
        var currentPage = reportConfiguration.pages() || 1;

        document.getElementById('nextPageConfiguration').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageConfiguration').style.display = "none";
        }

        reportConfiguration
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
 
    });
 
    $("#nextPageConfiguration").click(function() {
        var currentPage = reportConfiguration.pages() || 1;

        document.getElementById('previousPageConfiguration').style.display = "block";

        if(currentPage >= tpgsConfiguration-1) {
         document.getElementById('nextPageConfiguration').style.display = "none";
        }

        reportConfiguration
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportConfiguration").click(function () {

        reportConfiguration.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formConfiguration']").submit(function () {

	var configurationData = {};
        params = $('#formConfiguration').serializeArray();

	$.each(params, function(i, val) {
	    configurationData[val.name]=val.value;
	});

        reportConfiguration
            .params({ "OrgID": [configurationData['OrgID']] })
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });

    function displayUserPortal(){

	document.getElementById('ReportControlsConfiguration').style.display = "none";
        document.getElementById('previousPageConfiguration').style.display = "none";
        document.getElementById('nextPageConfiguration').style.display = "none";
	document.getElementById('exportConfiguration').style.display = "none";

	document.getElementById('ReportControlsUserPortal').style.display = "block";
        document.getElementById('previousPageUserPortal').style.display = "none";
        document.getElementById('nextPageUserPortal').style.display = "none";
	document.getElementById('exportUserPortal').style.display = "block";
	document.getElementById('mainReportiRecruitConfiguration').style.display = "block";

        var currentPage = reportUserPortal.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageUserPortal').style.display = "block";
        }

        if (currentPage < tpgsUserPortal) {
         document.getElementById('nextPageUserPortal').style.display = "block";
        }

    }


    $("#previousPageUserPortal").click(function() {
        var currentPage = reportUserPortal.pages() || 1;

        document.getElementById('nextPageUserPortal').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageUserPortal').style.display = "none";
        }
 
        reportUserPortal
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });
 
    $("#nextPageUserPortal").click(function() {
        var currentPage = reportUserPortal.pages() || 1;

        document.getElementById('previousPageUserPortal').style.display = "block";

        if(currentPage >= tpgsUserPortal-1) {
         document.getElementById('nextPageUserPortal').style.display = "none";
        }

        reportUserPortal
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportUserPortal").click(function () {

        reportUserPortal.export({
            //export options here
            outputFormat: "pdf"
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formUserPortal']").submit(function () {

	var portalData = {};
        params = $('#formUserPortal').serializeArray();

	$.each(params, function(i, val) {
	    portalData[val.name]=val.value;
	});

        reportUserPortal
            .params({ "OrgID": [portalData['OrgID']], "OrderBy": [portalData['OrderBy'] + " " + portalData['OrderDirection']]})
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });

    $("#mainReportiRecruitConfiguration").click(function () {

	var configurationData = {};
        params = $('#formConfiguration').serializeArray();

	$.each(params, function(i, val) {
	    configurationData[val.name]=val.value;
	});

	ConfigurationReport(configurationData['OrgID']);
    });

}, function(err) {
  alert("error: " + err);
});

