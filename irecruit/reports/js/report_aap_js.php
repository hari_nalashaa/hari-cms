<?php
header("Content-type: application/javascript");

require_once realpath(__DIR__ . '/../..') . "/Configuration.inc";

$results    =   G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "DefaultSearchDateRange");
$DR         =   $results['DefaultSearchDateRange'];

if ($DR == "") {
    $DR = "90";
}

//Get Dates List
$columns = "date_format(now(),'%m/%d/%Y'), date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%m/%d/%Y'), date_format(now(),'%Y-%m-%d'), date_format(DATE_SUB(now(), INTERVAL " . $DR . " DAY),'%Y-%m-%d')";
list($EndDate, $StartDate, $FinalDate, $BeginDate) = array_values($MysqlHelperObj->getDatesList($columns));
?>
myConfig = { auth: { token: encodeURIComponent("<?php echo Reports::$encrypttext; ?>"), preAuth:true, tokenName: "pp" } };
visualize.config(myConfig);

$(window).bind('beforeunload', function() {
      visualize(function(v) {
        v.logout();
      });
});

visualize(function(v){

v.login(myConfig);

var reportAAP= "";

AAPReport("<?php echo Reports::$ORGID; ?>","<?php echo Reports::$MULTIORGID; ?>","<?php echo $StartDate; ?>","<?php echo $EndDate; ?>","JA.EntryDate","DESC");


function AAPReport (OrgID,MultiOrgID,FromDate,ToDate,OrderBy,OrderDirection) {

        $('input[name=OrgID]').val(OrgID);
        $('input[name=MultiOrgID]').val(MultiOrgID);
        $('input[name=FromDate]').val(FromDate);
        $('input[name=ToDate]').val(ToDate);
	$('option[value="'+OrderBy+'"]').attr('selected','selected');
        $('option[value="'+OrderDirection+'"]').attr('selected','selected');

        ToDate = new Date(ToDate).toISOString().slice(0, 10);
        FromDate = new Date(FromDate).toISOString().slice(0, 10);

    	reportAAP = v.report({
	resource: "/reports/iRecruit_Reports/AAP",
        container: "#container",
        params: { "OrgID":[OrgID], "MultiOrgID":[MultiOrgID], "FromDate":[FromDate], "ToDate":[ToDate], "OrderBy":[OrderBy + " " + OrderDirection] },
	linkOptions: {
            beforeRender: function (linkToElemPairs) {
                linkToElemPairs.forEach(showCursor);
            },
            events: {
                "click": function(ev, link){
                   if (link.type == "Reference"){
                        if (link.tooltip == "Link to summary page in iRecruit") {
                                window.open([link.href],'_blank');
                        }
                   }
                     console.log(link);
                }
            }
        },
        error: function (err) {
            alert(err.message);
        },
	events: {
            changeTotalPages: function(totalPages) {
		tpgsAAP = totalPages;
	        displayAAP();
            }
        },
	success: function () {
	    displayAAP();
            exportAAP.removeAttribute("disabled");
        }
    });



} // end AAP Report

    function showCursor(pair){
           var el = pair.element;
               el.style.cursor = "pointer";
    }

    function displayAAP(){

	document.getElementById('ReportControlsAAP').style.display = "block";
        document.getElementById('previousPageAAP').style.display = "none";
        document.getElementById('nextPageAAP').style.display = "none";
	document.getElementById('exportAAP').style.display = "block";

        var currentPage = reportAAP.pages() || 1;

	if (currentPage > 1) {
         document.getElementById('previousPageAAP').style.display = "block";
        }

        if (currentPage < tpgsAAP) {
         document.getElementById('nextPageAAP').style.display = "block";
        }

	reportAAP.run();

    }


    $("#previousPageAAP").click(function() {
        var currentPage = reportAAP.pages() || 1;

        document.getElementById('nextPageAAP').style.display = "block";

        if(currentPage <= 2) {
         document.getElementById('previousPageAAP').style.display = "none";
        }

        reportAAP
            .pages(--currentPage)
            .run()
                .fail(function(err) { alert(err); } );
 
    });
 
    $("#nextPageAAP").click(function() {
        var currentPage = reportAAP.pages() || 1;

        document.getElementById('previousPageAAP').style.display = "block";

        if(currentPage >= tpgsAAP-1) {
         document.getElementById('nextPageAAP').style.display = "none";
        }

        reportAAP
            .pages(++currentPage)
            .run()
                .fail(function(err) { alert(err); } );
    });

    $("#exportAAP").click(function () {

        reportAAP.export({
            //export options here
            outputFormat: "pdf" // xls
            //exports all pages if not specified
            //pages: "1-2"
        }, function (link) {
           var url = link.href ? link.href : link;
           exportWindow = window.open(url,"Export");
	   setTimeout(function(){
	      exportWindow.close();
	   },1000); // 1 second
        }, function (error) {
            console.log(error);
        });

    });

    $("form[id='formAAP']").submit(function () {

	var aapData = {};
        params = $('#formAAP').serializeArray();

	$.each(params, function(i, val) {
	    aapData[val.name]=val.value;
	});

        var ToDate = new Date(aapData['ToDate']).toISOString().slice(0, 10);
        var FromDate = new Date(aapData['FromDate']).toISOString().slice(0, 10);

        reportAAP
            .params({ "FromDate": [FromDate], "ToDate": [ToDate], "OrgID": [aapData['OrgID']], "MultiOrgID": [aapData['MultiOrgID']], "OrderBy":[aapData['OrderBy'] + " " + aapData['OrderDirection']] })
            .run()
                .fail(function(err) { alert(err); } );

	return false;
    });


    $("#mainReportiRecruitAAP").click(function () {

	var aapData = {};
        params = $('#formAAP').serializeArray();

	$.each(params, function(i, val) {
	    aapData[val.name]=val.value;
	});

	AAPReport(aapData['OrgID'],aapData['MultiOrgID'],aapData['FromDate'],aapData['ToDate'],aapData['OrderBy'],aapData['OrderDirection']);
    });

}, function(err) {
  alert("error: " + err);
});

