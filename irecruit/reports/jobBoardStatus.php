<?php
require_once '../Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
    $TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title     =   $title  =   "Job Board Status";

//Set active
$Active =   'Y';
$Limit  =   isset($_REQUEST['Limit']) ? $_REQUEST['Limit'] : $user_preferences['ReportsSearchResultsLimit'];
$IsRequisitionApproved = "Y";

$TemplateObj->FilterOrganization    =   $FilterOrganization =   $_REQUEST['ddlOrganizationsList'];

// Assign add applicant datepicker variables
$script_vars_footer [] = 'var datepicker_ids = "#requisitions_from_date, #requisitions_to_date";';
$script_vars_footer [] = 'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer = $script_vars_footer;

require_once IRECRUIT_DIR . 'services/GetRequisitions.inc';

$requisitions_info = json_decode($requisitions_info, true);

$TemplateObj->requisition_search_results        =   $requisition_search_results         =   $requisitions_info['results'];
$TemplateObj->requisition_search_results_count  =   $requisition_search_results_count   =   $requisitions_info['count_per_page'];
$TemplateObj->total_count                       =   $total_count                        =   $requisitions_info['requisitions_count'];

$TemplateObj->Active    =   $Active;
$TemplateObj->Limit     =   $Limit;

echo $TemplateObj->displayIrecruitTemplate('views/reports/JobBoardStatus');
?>