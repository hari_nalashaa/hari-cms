<?php
require_once '../Configuration.inc';


// Set goto object, will be useful to track the page to redirect after login
if ($_SERVER['QUERY_STRING']) {
    $TemplateObj->goto  =   $_SERVER['QUERY_STRING'];
}

// Set page title
$TemplateObj->title     =   "Applicants By Distance";

// Assign add applicant datepicker variables
$script_vars_footer[]   =   'var datepicker_ids = "#from_date, #to_date";';
$script_vars_footer[]   =   'var date_format = "mm/dd/yy";';

$TemplateObj->scripts_vars_footer   =   $script_vars_footer;
$TemplateObj->FilterOrganization    =   $FilterOrganization = $_REQUEST['ddlOrganizationsList'];

if(isset($_REQUEST['from_date'])
    && $_REQUEST['from_date'] != ""
    && isset($_REQUEST['to_date'])
    && $_REQUEST['to_date'] != "") {
    $FromDate   =   $_REQUEST['from_date'];
    $ToDate     =   $_REQUEST['to_date'];
}
else {
    $FromDate   =   G::Obj('DateHelper')->getMdyFromYmd(date('Y-m-d', strtotime("-12 months", strtotime(date('Y-m-d')))), "-", "/");;
    $ToDate     =   G::Obj('DateHelper')->getMdyFromYmd(date('Y-m-d'));
}

$distance   =   '100';
$zip_code   =   '';

if(isset($_REQUEST['distance']) && $_REQUEST['distance'] != "") {
    $distance   =   $_REQUEST['distance'];
}
if(isset($_REQUEST['zip_code']) && $_REQUEST['zip_code'] != "") {
    $zip_code   =   $_REQUEST['zip_code'];
}


$params     =   array(":zipcode"=>$zip_code);
$query      =   "SELECT COUNT(*) AS Count FROM zipcodes WHERE zipcode = :zipcode";
$zip_info   =   G::Obj('GenericQueries')->getRowInfoByQuery($query, array($params));
$zip_cnt    =   $zip_info['Count'];

$params     =   array(
                    ":FromDate"     =>  G::Obj('DateHelper')->getYmdFromMdy($FromDate), 
                    ":ToDate"       =>  G::Obj('DateHelper')->getYmdFromMdy($ToDate), 
                    ":RCZipCode1"   =>  $zip_code,
                    ":RCZipCode2"   =>  $zip_code,
                    ":RCZipCode3"   =>  $zip_code,
                    ":RCZipCode4"   =>  $zip_code,
                    ":RCZipCode5"   =>  $zip_code,
                    ":RZipCode1"    =>  $zip_code,
                    ":RZipCode2"    =>  $zip_code,
                    ":RZipCode3"    =>  $zip_code,
                    ":RZipCode4"    =>  $zip_code,
                    ":RZipCode5"    =>  $zip_code
                );
$query      =   "SELECT JA.OrgID, JA.ApplicationID, JA.RequestID, JA.ProcessOrder, JA.DispositionCode, JA.StatusEffectiveDate, JA.EntryDate, LPAD(FORMAT((3958 * 3.1415926 * sqrt(((SELECT latitude FROM zipcodes WHERE zipcode = :RCZipCode1) - (select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) ) * ((select latitude from zipcodes where zipcode = :RCZipCode2) - (select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) ) + cos((select latitude from zipcodes where zipcode = :RCZipCode3) / 57.29578) * cos((select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) / 57.29578) * ((select longitude from zipcodes where zipcode = :RCZipCode4) - (select longitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip'))) * ((select longitude from zipcodes where zipcode = :RCZipCode5) - (select longitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip'))) ) / 180),2),6,'0') AS Distance";
$query      .=  " FROM JobApplications JA";
$query      .=  " WHERE 1 ";

if($FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
    
    $query      .=  " AND JA.OrgID = :OrgID";
    $query      .=  " AND JA.MultiOrgID = :MultiOrgID";
    $params[":OrgID"]       =   $FilterOrgID;
    $params[":MultiOrgID"]  =   $FilterMultiOrgID;
}
else {
    $query              .=  " AND JA.OrgID = :OrgID";
    $params[":OrgID"]   =   $OrgID;
}

$query      .=  " AND (3958 * 3.1415926 * sqrt(((SELECT latitude FROM zipcodes WHERE zipcode = :RZipCode1) - (select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) ) * ((select latitude from zipcodes where zipcode = :RZipCode2) - (select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) ) + cos((select latitude from zipcodes where zipcode = :RZipCode3) / 57.29578) * cos((select latitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip')) / 57.29578) * ((select longitude from zipcodes where zipcode = :RZipCode4) - (select longitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip'))) * ((select longitude from zipcodes where zipcode = :RZipCode5) - (select longitude from zipcodes where zipcode = (select Answer from ApplicantData where OrgID = JA.OrgID and ApplicationID = JA.ApplicationID and QuestionID ='zip'))) ) / 180) <= $distance";
$query      .=  " AND JA.EntryDate >= :FromDate";
$query      .=  " AND JA.EntryDate <= :ToDate";
$query      .=  " ORDER BY JA.ApplicationID DESC LIMIT 100";
$app_results    =   G::Obj('GenericQueries')->getInfoByQuery($query, array($params));

$totcnt     =   0;
$subcnt     =   0;

$TemplateObj->FromDate  =   $FromDate;
$TemplateObj->ToDate    =   $ToDate;

$TemplateObj->zip_cnt   =   $zip_cnt;
$TemplateObj->zip_code  =   $zip_code;

$TemplateObj->app_results   =   $app_results;
$TemplateObj->list      =   $list   =   $app_results;
$TemplateObj->totcnt    =   $totcnt;
$TemplateObj->subcnt    =   $subcnt;
$TemplateObj->distance  =   $distance;

if ($zip_code != ""
    && $FromDate != ""
    && $ToDate
    && count($app_results) > 0
    && $_REQUEST['export_apps'] == 'Yes') {
    
    require_once IRECRUIT_DIR . 'reports/ApplicantsByDistanceExport.inc';

}

echo $TemplateObj->displayIrecruitTemplate('views/reports/ApplicantsByDistance');
?>
