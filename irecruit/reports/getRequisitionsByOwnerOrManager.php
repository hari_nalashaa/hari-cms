<?php
require_once '../Configuration.inc';

$FilterOrganization =   $_REQUEST['FilterOrganization'];

$limit = $user_preferences['ReportsSearchResultsLimit'];
if(isset($_REQUEST['RecordsLimit']) && $_REQUEST['RecordsLimit'] != "") $limit = $_REQUEST['RecordsLimit'];

//Set active
$Active =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : 'Y';
$IsRequisitionApproved = "Y";

if($Active == "NA") {
    $Active = "N";
    $IsRequisitionApproved = "N";
}

$start = 0;
if(isset($_REQUEST['IndexStart'])) $start = $_REQUEST['IndexStart'];

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

if (isset ( $_REQUEST ['to_sort'] )) {

    if ($_REQUEST ['to_sort'] == "date_opened") {
        $order_by           =   " Requisitions.PostDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_id") {
        $order_by           =   " RequisitionID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "job_id") {
        $order_by           =   " JobID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_title") {
        $order_by           =   " Title $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "status") {
        $order_by           =   " Active $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "requisition_stage") {
        $order_by           =   " getRequisitionStageDescription(Requisitions.OrgID, Requisitions.RequisitionStage) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "days_open") {
        $order_by           =   " IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate))  $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "expire_date") {
        $order_by           =   " DATEDIFF(NOW(), Requisitions.ExpireDate) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "requisition_owner") {
        $order_by           =   " Owner $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "requisition_manager") {
        $order_by           =   " getRequisitionManagers(Requisitions.OrgID, Requisitions.RequestID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "applicants_count") {
        $order_by           =   " ApplicantsCount $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "org_title") {
        $order_by           =   " getOrgnizationName(Requisitions.OrgID, Requisitions.MultiOrgID) $sort_type";
    }
    else {
        $order_by           =   " Requisitions.PostDate DESC";
    }

    $sort_by_key            =   $_REQUEST ['to_sort'];
} else {
    $order_by           =   " Requisitions.PostDate DESC";
    $sort_by_key        =   "Requisitions.PostDate";
}

if(!isset($_REQUEST['Export'])) {
    $order_by .= " LIMIT $start, $limit";
}

// Set columns
$columns = "NOW() AS CurrentDateTime, DATE_FORMAT(Requisitions.PostDate,'%m/%d/%Y') PostDate, DATE_FORMAT(Requisitions.ExpireDate,'%m/%d/%Y') ExpireDate,";

$columns .= "IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate)) Open, DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate) Closed,";

$columns .= "DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current,
            Requisitions.RequestID, Requisitions.Title, Requisitions.MultiOrgID, 
            Requisitions.RequisitionID, Requisitions.JobID, Requisitions.Active, 
            getRequisitionStageDescription(Requisitions.OrgID, Requisitions.RequisitionStage) AS RequisitionStage,PresentOn";

$columns_count = "COUNT(Requisitions.RequestID) AS RequestIDsCount";

if(($_REQUEST['RequisitionManager'] != "" && $_REQUEST['RequisitionOwner'] != "")
   || ($_REQUEST['RequisitionManager'] != "" && $_REQUEST['RequisitionOwner'] == ""))
{
    // Get Requisition Information
    $requsition_search_results_info = $RequisitionsObj->getRequisitionsByRequisitionManagers($columns, $FilterOrganization, $_REQUEST['Keyword'], $_REQUEST['RequisitionManager'], $_REQUEST['RequisitionOwner'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['RequisitionStage'], $_REQUEST['RequisitionStageDate'], $IsRequisitionApproved, $Active, $order_by);
    // Get Requisition Information
    $req_count_info     =   G::Obj('Requisitions')->getRequisitionsByRequisitionManagers($columns_count, $FilterOrganization, $_REQUEST['Keyword'], $_REQUEST['RequisitionManager'], $_REQUEST['RequisitionOwner'], $_REQUEST['FromDate'], $_REQUEST['ToDate'], $_REQUEST['RequisitionStage'], $_REQUEST['RequisitionStageDate'], $IsRequisitionApproved, $Active, "Requisitions.PostDate DESC");
}
else if($_REQUEST['RequisitionManager'] == "" && $_REQUEST['RequisitionOwner'] != "") {
    
    if($FilterOrganization != "") {
        list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
        
        // Set condition
        $where          =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
        // Set parameters
        $params         =   array (":OrgID"=>$FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
    }   
    else {
        list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
        
        // Set condition
        $where          =   array ("OrgID = :OrgID");
        // Set parameters
        $params         =   array (":OrgID" => $OrgID);
    }
    
    $where[]            =   "Owner = :Owner";
    $where[]            =   "Approved = 'Y'";
    $params[":Owner"]   =   $_REQUEST['RequisitionOwner'];
    
    //Set Date
    if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])) {
        $where[] = "DateEntered >= :FromDate AND DateEntered <= :ToDate";
        $params[":FromDate"]    =   $DateHelperObj->getYmdFromMdy($_REQUEST['FromDate']);
        $params[":ToDate"]      =   $DateHelperObj->getYmdFromMdy($_REQUEST['ToDate']);
    }
    
    if(isset($_REQUEST['Keyword']) && $_REQUEST['Keyword'] != "") {
        $where[] = "(Title LIKE :Keyword1 OR RequisitionID LIKE :Keyword2 OR JobID LIKE :Keyword3)";
        $params[":Keyword1"]    =   "%".$_REQUEST['Keyword']."%";
        $params[":Keyword2"]    =   "%".$_REQUEST['Keyword']."%";
        $params[":Keyword3"]    =   "%".$_REQUEST['Keyword']."%";
    }
    
    if(isset($_REQUEST['RequisitionStage']) && $_REQUEST['RequisitionStage'] != "") {
        $where[] = "(RequisitionStage = :RequisitionStage)";
        $params[":RequisitionStage"]   =  $_REQUEST['RequisitionStage'];
    }

 
    
    if(isset($_REQUEST['RequisitionStageDate']) && $_REQUEST['RequisitionStageDate'] != "") {
        $where[] = "(RequisitionStageDate = :RequisitionStageDate)";
        $params[":RequisitionStageDate"]   =  $DateHelperObj->getYmdFromMdy($_REQUEST['RequisitionStageDate']);
    }
    
    if ($feature ['RequisitionApproval'] != "Y") {
        if($Active == "A") {
            $where[]            =   "Active != :Active";
            $params[":Active"]  =   'R';
        }
        else {
            $where[]            =   "Active = :Active";
            $params[":Active"]  =   $Active;
        }
    }
    else if ($feature ['RequisitionApproval'] == "Y") {
        if($Active != "A") {
            $where[]            =   "Active = :Active";
            $params[":Active"]  =   $Active;
        }
    }
    
    if(isset($IsRequisitionApproved) && $IsRequisitionApproved == "N") {
        $where[]                =   "Approved = 'N'";
    }
    
    if($Active == "Y") $where[] = "Approved = 'Y'";
    
    // Get Requisition Information
    $requsition_search_results_info = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", $order_by, array ($params) );
    
    // Get Requisition Information
    $req_count_info  =   $RequisitionsObj->getRequisitionInformation ( $columns_count, $where, "", "Requisitions.PostDate DESC", array ($params) );
}
else {

    if($FilterOrganization != "") {
        list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
        
        // Set condition
        $where  =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
        // Set parameters
        $params =   array (":OrgID"=>$FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
    }
    else {
        list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
        
        // Set condition
        $where  =   array ("OrgID = :OrgID");
        // Set parameters
        $params =   array (":OrgID" => $OrgID);
    }
    
    $where[]    =   "Approved = 'Y'";

    //Set Date
    if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])) {
        $where[] = "DateEntered >= :FromDate AND DateEntered <= :ToDate";
        $params[":FromDate"]    =   $DateHelperObj->getYmdFromMdy($_REQUEST['FromDate']);
        $params[":ToDate"]      =   $DateHelperObj->getYmdFromMdy($_REQUEST['ToDate']);
    }

    if(isset($_REQUEST['Keyword']) && $_REQUEST['Keyword'] != "") {
        $where[] = "(Title LIKE :Keyword1 OR RequisitionID LIKE :Keyword2 OR JobID LIKE :Keyword3)";
        $params[":Keyword1"]    =   "%".$_REQUEST['Keyword']."%";
        $params[":Keyword2"]    =   "%".$_REQUEST['Keyword']."%";
        $params[":Keyword3"]    =   "%".$_REQUEST['Keyword']."%";
    }
    
    if(isset($_REQUEST['RequisitionStage']) && $_REQUEST['RequisitionStage'] != "") {
        $where[] = "(RequisitionStage = :RequisitionStage)";
        $params[":RequisitionStage"]   =  $_REQUEST['RequisitionStage'];
    }
    
    if(isset($_REQUEST['RequisitionStageDate']) && $_REQUEST['RequisitionStageDate'] != "") {
        $where[] = "(RequisitionStageDate = :RequisitionStageDate)";
        $params[":RequisitionStageDate"]   =  $DateHelperObj->getYmdFromMdy($_REQUEST['RequisitionStageDate']);
    }
    
    if ($feature ['RequisitionApproval'] != "Y") {
        if($Active == "A") {
            $where[]            =   "Active != :Active";
            $params[":Active"]  =   'R';
        }
        else {
            $where[]            =   "Active = :Active";
            $params[":Active"]  =   $Active;
        }
    }
    else if ($feature ['RequisitionApproval'] == "Y") {
        if($Active != "A") {
            $where[]            =   "Active = :Active";
            $params[":Active"]  =   $Active;
        }
    }
    
    if(isset($IsRequisitionApproved) && $IsRequisitionApproved == "N") {
        $where[]                =   "Approved = 'N'";
    }
    
    if($Active == "Y") $where[] = "Approved = 'Y'";
    
    // Get Requisition Information
    $requsition_search_results_info = $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", $order_by, array ($params) );

    // Get Requisition Information
    $req_count_info  =   $RequisitionsObj->getRequisitionInformation ( $columns_count, $where, "", "Requisitions.PostDate DESC", array ($params) );
}

$requisitions_list  = $requsition_search_results_info['results'];
$requisition_search_results_count   = $requsition_search_results_info['count'];

$requisitions_count = $req_count_info['results'][0]['RequestIDsCount'];

$list = array();
$i = 0;
if (is_array($requisitions_list)) {

    foreach ($requisitions_list as $REQS) {
        
        $RequisitionIDJobID = "";

        $PostDate                   =   $REQS['PostDate'];

        //Have to bind for testing
        $requisitions_list[$i]["PostDate"] = $PostDate;
        
        if ($Active == "N") {
            $REQS['Open'] = $REQS['Closed'];
        }

        if ($REQS['Open'] < 0) $REQS['Open'] = 0;

        $requisitions_list[$i]["RequisitionOwnersName"]     =   $RequisitionDetailsObj->getRequisitionOwnersName($OrgID, $REQS['RequestID']);
        $requisitions_list[$i]["RequisitionManagersNames"]  =   $RequisitionsObj->getRequisitionManagersNames($OrgID, $REQS['RequestID']);
        
        //Have to bind for testing
        $requisitions_list[$i]["Open"]          =   $REQS['Open'];
        //Have to bind for testing
        $requisitions_list[$i]["ExpireDate"]    =   $REQS['ExpireDate'];
        
        // Set where condition
        $where      =   array("OrgID = :OrgID", "RequestID = :RequestID");
        // Set parameters
        $params     =   array(":OrgID" => $OrgID, ":RequestID" => $REQS['RequestID']);
        // Get Job Applications Information
        $resultsCNT =   $ApplicationsObj->getJobApplicationsInfo("count(*) as cnt", $where, '', '', array($params));
        $ttcnt      =   $resultsCNT['results'][0]['cnt'];

        $requisitions_list[$i]["ApplicantsCount"] = '<a href="'.IRECRUIT_HOME.'requisitionsSearch.php?RequestID='.$REQS['RequestID'].'&active_tab=applicants" target="_blank">' . $ttcnt . '</a>';

        //Get Organization Title
        $requisitions_list[$i]["OrganizationTitle"] = $OrganizationsObj->getOrgTitle($OrgID, $REQS['MultiOrgID']);
        
        
        $i++;

    } // end foreach
}

$left_nav_info = $PaginationObj->getPageNavigationInfo($start, $limit, $requisitions_count, '', '');

if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "requisitions_list"         =>  $requisitions_list,
        "previous"                  =>  $left_nav_info['previous'],
        "next"                      =>  $left_nav_info['next'],
        "total_pages"               =>  $left_nav_info['total_pages'],
        "current_page"              =>  $left_nav_info['current_page'],
        "requisitions_count"        =>  $requisitions_count,
        "count_per_page"            =>  $requisition_search_results_count,
        "feature_multi_org_id"      =>  $feature['MultiOrg'],
    ));
}

############        Export Code     ############
if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES") {
    $list = $requisitions_list;
    require_once IRECRUIT_DIR . 'reports/ExportRequisitionsByOwnerOrManager.inc';
}
############        Export Code     #############
?>
