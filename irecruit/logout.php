<?php
$PAGE_TYPE	= "Default";
require_once 'Configuration.inc';

// Set page title
$TemplateObj->title	=	"iRecruit Logout";

if ($USERID) {
	
	// remove the session key from the database
	$IrecruitUsersObj->unsetUserSessionByUserID ( $USERID );
	
	// remove the session key from the users browser
	if (isset ( $_COOKIE ['ID'] )) {
		setcookie ( "ID", "", time () - 3600, "/" );
	}
	if (isset ( $_COOKIE ['UR'] )) {
		setcookie ( "UR", "", time () - 3600, "/" );
	}
	
	//Don't destroy the entire session, Unset only the irecruit values.
	unset($_SESSION['I']);
	$_SESSION['I'] = array();
	
} // end USERID


// fixes cookie login
header ( 'P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"' );

echo $TemplateObj->displayIrecruitTemplate ( 'views/Logout' );
?>
