<?php
global $PAGE_TYPE;

if($PAGE_TYPE == "PopUp") echo "<br><br><br><br>";

if(isset($lst) && $lst != "") {
	?>
		<script>
		var lst					=	'<?php echo $lst?>';
		var validate_dates_info	=	'<?php echo $validate_dates_info;?>';
		validate_dates_info		=	JSON.parse(validate_dates_info);

		var date_split_ids		=	lst.split(",");
		var date_objs			=	new Array();

		var i = 0;
		var date_id = "";
		var year_range = "";
		for(date_id_key in date_split_ids) {
			date_id		=	date_split_ids[i];
			date_id		=	$.trim(date_id);
			year_range	=	getYearRange(validate_dates_info, date_id);
			date_picker(date_id, 'mm/dd/yy', year_range, '');
			i++;
		}
		</script>
	<?php
}

if (preg_match ( '/reports.php$/', $_SERVER ["SCRIPT_NAME"] )) {
    ?><script>date_picker('#FromDate,#ToDate', 'mm/dd/yy');</script><?php
} 

//To set the javascript variables
if(is_array($this->scripts_vars_footer) && count($this->scripts_vars_footer) > 0) {
	echo '<script type="text/javascript">';
	foreach($this->scripts_vars_footer as $script_variable) {
		echo "\n" . $script_variable;
	}
	echo "\n";

	echo 'if(typeof(datepicker_ids) != "undefined")';
	echo '{'."\n";
	echo 'date_picker(datepicker_ids, date_format);'."\n";
	echo '}'."\n";
	echo '</script>'."\n";
}

//Scripts to include default scripts
if(isset($default_scripts["footer"])) {
	foreach ($default_scripts["footer"] as $script_name) {
		
		echo '<script type="text/javascript"';
		echo ' src="' . IRECRUIT_HOME . $script_name.'">';
		echo '</script>'."\n";

	}
}

//Scripts to include scripts based on page
if(isset($page_scripts_footer)) {
	foreach ($page_scripts_footer as $script_name) {

		echo '<script type="text/javascript"';
		echo ' src="' . IRECRUIT_HOME . $script_name.'">';
		echo '</script>'."\n";

	}
}
?>
</body>
</html>
