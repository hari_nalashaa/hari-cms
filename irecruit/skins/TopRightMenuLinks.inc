<ul class="nav navbar-top-links navbar-right">
	<!-- /.dropdown -->
	<li class="dropdown">
		<a href="#" data-toggle="dropdown" class="dropdown-toggle fa-user-icon" style="padding:0px;">
			<?php
				if($user_preferences['DashboardAvatar'] != "") {
					?><img src="<?php echo IRECRUIT_HOME . "vault/".$OrgID."/".$USERID."/profile_avatars/".$user_preferences['DashboardAvatar'];?>" width="50" height="50"><?php
				}
				else {
					?><img src="<?php echo IRECRUIT_HOME . "images/no-intern.jpg";?>" width="50" height="50"><?php
				}
			?>
			&nbsp;<?php echo $user_info['FirstName']." ".$user_info['LastName']?>&nbsp;<i class="fa fa-caret-down"></i>
		</a>
		<ul class="dropdown-menu dropdown-user">
			<li><a href="<?php echo IRECRUIT_HOME . "configuration.php";?>"><i class="fa fa-user fa-fw"></i>User Profile & Settings</a></li>
			<li class="divider"></li>
			<li><a href="<?php echo IRECRUIT_HOME;?>logout.php">
				<i class="fa fa-sign-out fa-fw"></i> Logout</a>
			</li>
		</ul> 
	<!-- /.dropdown -->
</ul>
