	
	<?php
	$user_notifications_info = $UserNotificationsObj->getUserNotifications("*", array(), "CreatedDateTime DESC", array());
	$user_notifications_list = $user_notifications_info['results'];
	
	for($i = 0; $i < count($user_notifications_list); $i++) {
		
	    $notification_id = $user_notifications_list[$i]['NotificationID'];
		$notification_status_info 	= $UserNotificationsObj->getUserNotificationStatus($USERID, $notification_id);
		$notification_message 		= "";		
		
		if(stristr ( $_SERVER ["SCRIPT_NAME"], $user_notifications_list[$i]['NotificationPage'] )
			|| $user_notifications_list[$i]['NotificationPage'] == "All") {

		    if($user_notifications_list[$i]['NotificationType'] == "retainable" && $notification_status_info['NotificationID'] != ""  && $notification_status_info['NotificationStatus'] == "minimized")
		    {
		        $notification_class_name = "user_notification_msg_footer_short notification ".$user_notifications_list[$i]['NotificationMessageType'];
		        $notification_arrow_class = "close fa fa-angle-up";
		    }
		    else {
		        $notification_class_name = "user_notification_msg_footer notification ".$user_notifications_list[$i]['NotificationMessageType'];
		        $notification_arrow_class = "close fa fa-angle-down";
		    }
			$notification_message .= '<div class="'.$notification_class_name.'" id="notification_'.$notification_id.'">';

			if($user_notifications_list[$i]['NotificationType'] == 'dismissible') {
				$notification_message .= '<button aria-hidden=\'true\' data-dismiss=\'alert\' class=\'close\' type=\'button\' onclick=\'save_action_on_notification("'.$user_notifications_list[$i]['NotificationID'].'", "dismissible", "dismissed")\'>x</button>';
			}
			else if($user_notifications_list[$i]['NotificationType'] == 'retainable') {
				$notification_message .= '<i class="'.$notification_arrow_class.'" aria-hidden="true" onclick=\'minimize_notification(this, "notification_'.$notification_id.'", "'.$notification_id.'")\'></i>';
			}
			
			$notification_message .= $user_notifications_list[$i]['NotificationSubject'];
			$notification_message .= "<br>";
			$notification_message .= $user_notifications_list[$i]['NotificationMessage'];

			$notification_message .= '</div>';
			
			if($user_notifications_list[$i]['NotificationType'] == "dismissible"
				&&  $notification_status_info['NotificationID'] == "") {
				echo $notification_message;
			}
			else if($user_notifications_list[$i]['NotificationType'] == "retainable") {
				
			    $start_time      =   strtotime($user_notifications_list[$i]['NotificationStartDateTime']);
			    $end_time        =   strtotime($user_notifications_list[$i]['NotificationEndDateTime']);
			    $current_time    =   strtotime(date('Y-m-d H:i:s'));
			    
			    if($current_time >= $start_time && $current_time <= $end_time) {
			        echo $notification_message;
			    }
			}
		}
		
	}
	?>
	<script type="text/javascript">
		function save_action_on_notification(NotificationID, NotificationType, NotificationStatus) {
			
			$.ajax({
				async: true,   // this will solve the problem
				method: "POST",
		  		url: irecruit_home + "updateUserNotificationStatus.php?NotificationID="+NotificationID+'&NotificationType='+NotificationType+'&NotificationStatus='+NotificationStatus,
				type: "POST",
				success: function(data) {
					// Nothing to do here
		    	}
			});
					
		}

		function minimize_notification(arrow_obj, notification_div_id, notification_id) {
		    var current_class = $(arrow_obj).attr('class');
		    
			if(current_class == 'close fa fa-angle-down') {
				$(arrow_obj).attr('class', 'close fa fa-angle-up');

				var parents_class = $(arrow_obj).parents('div').attr('class');
				var classes_list = parents_class.split(" ");	

				var parent_class_name = 'user_notification_msg_footer_short';
				for(var i = 1; i < classes_list.length; i++) {
					parent_class_name += ' '+classes_list[i];
				}

				$('#'+notification_div_id).attr('class', parent_class_name);
				save_action_on_notification(notification_id, 'retainable', 'minimized');	
			}
			else if(current_class == 'close fa fa-angle-up') {
				$(arrow_obj).attr('class', 'close fa fa-angle-down');

				var parents_class = $(arrow_obj).parents('div').attr('class');
				var classes_list = parents_class.split(" ");	
				
				var parent_class_name = 'user_notification_msg_footer';
				for(var i = 1; i < classes_list.length; i++) {
					parent_class_name += ' '+classes_list[i];
				}

				$('#'+notification_div_id).attr('class', parent_class_name);
				save_action_on_notification(notification_id, 'retainable', 'maximized');
			}

		}

		function show_hide_notification() {
			$(".notification").hide();
			$("#"+notification_ids[j]).show();
			j++;
			if(notification_ids.length == j) j = 0;
		}

		var notification_ids = [];
		$(".notification").each(function() {
			notification_ids.push(this.id);
		});

		var j = 0;
		
		if(notification_ids.length > 1) {
			var notification_id_rand = notification_ids[Math.floor(Math.random() * notification_ids.length)];
			setInterval(show_hide_notification, 6000);
		}

		show_hide_notification();
	</script>
</div>