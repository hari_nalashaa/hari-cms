<div id="wrapper">
	<!-- Navigation -->
	<nav class="navbar navbar-default navbar-static-top" role="navigation"
		style="margin-bottom: 0">
		<?php include IRECRUIT_DIR . 'skins/Logo.inc';?>
		<!-- /.navbar-header -->
			
		<?php include IRECRUIT_DIR . 'skins/TopRightMenuLinks.inc';?>
        <!-- /.navbar-top-links -->
			
        <?php include IRECRUIT_DIR . 'skins/LeftSideBar.inc';?>
        <!-- /.navbar-static-side -->
	</nav>