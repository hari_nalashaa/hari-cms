<?php
if (preg_match ( '/reports.php$/', $_SERVER ["SCRIPT_NAME"] )) {
echo "<html>\n";
} else {
echo "<!DOCTYPE html>\n";
}
?>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html;">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>iRecruit - <?php echo htmlspecialchars($title);?></title>

<?php
/**
 * @tutorial	Load default style sheet of selected template, it will affect the irecruit folder
 */
if(isset($default_styles["header"])) {
	foreach ($default_styles["header"] as $style_sheet_name) {
		echo '<link rel="stylesheet" type="text/css" href="'.IRECRUIT_HOME.$style_sheet_name.'"'; 
		
		if(isset($default_styles["header_attributes"][$style_sheet_name])) {
			
			foreach ($default_styles["header_attributes"][$style_sheet_name] as $attr_name => $attr_value) {
				echo " ".$attr_name."='".$attr_value."'";
			}	
		}
		echo '>'."\n";
		
	}	
}

$togggle_box_color = "";
$navbar_color = "";

if(isset($user_preferences)) {
	$togggle_box_color = $user_preferences['TableHeader'];
	$navbar_color = $user_preferences['TableRowHeader'];
}

if($togggle_box_color != "" && $navbar_color != "") {
   	?>
    <style>
    <?php
    if($PAGE_TYPE == "PopUp" || $PAGE_TYPE == "Email" || $PAGE_TYPE == "Default") {
		echo 'body {';
		echo 'background: none;';
		echo '}'."\n";
	}
	?>
    .sidebar-toggle-box-menu {
		background: none repeat scroll 0 0 <?php echo $togggle_box_color;?>;
		color: #fff;
	}
	.navbar {
		background: none repeat scroll 0 0 <?php echo $navbar_color;?>;
		box-shadow:0 0 2px rgba(0, 0, 0, 0.3);
	}
	.navbar-default {
		background-color: <?php echo $navbar_color;?>;
		background-image: -moz-linear-gradient(top, <?php echo $navbar_color;?>, <?php echo $navbar_color;?>);
		background-image: -webkit-gradient(linear, 0 0, 0 100%, from(<?php echo $navbar_color;?>), to(<?php echo $navbar_color;?>));
		background-image: -webkit-linear-gradient(top, <?php echo $navbar_color;?>, <?php echo $navbar_color;?>);
		background-image: -o-linear-gradient(top, <?php echo $navbar_color;?>, <?php echo $navbar_color;?>);
		background-image: linear-gradient(to bottom, <?php echo $navbar_color;?>, <?php echo $navbar_color;?>);
		background-repeat: repeat-x;
		border-color: <?php echo $navbar_color;?>;
		color: #fff;
		filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff4a8bc2', endColorstr='#ff4a8bc2', GradientType=0);
		height: 60px;
	}
	.dropdown-menu > li > a {
		color:<?php echo $navbar_color;?>;
	}
	.dropdown-menu > li > a:hover {
		color:<?php echo $togggle_box_color;?>;
	}
	.navbar-right > li > a:hover, .navbar-right > li > a:focus {
	    text-decoration: none;
	    background-color: <?php echo $togggle_box_color;?>;
	}
	.navbar-right .open > a, .navbar-right .open > a:hover, .navbar-right .open > a:focus, .navbar-right a.active {
		background-color: <?php echo $togggle_box_color;?>;
	    border-color: <?php echo $togggle_box_color;?>;
	}
	#side-menu > li > a:hover, #side-menu > li > a:focus {
		text-decoration: none;
		background-color: <?php echo $togggle_box_color;?>;
	}
	#side-menu .open > a, #side-menu .open > a:hover, #side-menu .open > a:focus, #side-menu a.active {
		background-color: <?php echo $togggle_box_color;?>;
	    border-color: <?php echo $togggle_box_color;?>;
	}
	.btn-primary {
		background-color: <?php echo $navbar_color;?>;
	    border-color: <?php echo $navbar_color;?>;
	    color: #fff;
	}
	.btn {
	    -moz-user-select: none;
	    background-image: none;
	    border: 1px solid transparent;
	    border-radius: 4px;
	    cursor: pointer;
	    display: inline-block;
	    font-size: 14px;
	    font-weight: 400;
	    line-height: 1.42857;
	    margin-bottom: 0;
	    padding: 6px 12px;
	    text-align: center;
	    vertical-align: middle;
	    white-space: nowrap;
	}
	.btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
	    color: #fff;
	    background-color: <?php echo $togggle_box_color;?>;
	    border-color: <?php echo $togggle_box_color;?>;
	}
	.panel-default > .panel-heading-color {
		border-color: <?php echo $navbar_color;?>;
		color: #fff;
		background-color: <?php echo $navbar_color;?>;
		font-size: 15px;
	}
	</style>
	<?php
}

/**
 * @tutorial	Load page styles depend on page, it will affect to that page only
 */
if(isset($this->page_styles["header"])) {
	foreach ($this->page_styles["header"] as $style_sheet_name) {
		echo '<link rel="stylesheet" type="text/css" href="'.IRECRUIT_HOME.$style_sheet_name.'"';
		echo '>'."\n";
	}	
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME?>css/common.css">
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME?>css/jquery.rateyo.min.css">
<link rel="shortcut icon" href="<?php echo IRECRUIT_HOME?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo IRECRUIT_HOME?>favicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME?>css/buttons.dataTables.1.3.1.min.css">

<script type="text/javascript">
var irecruit_home   =   '<?php echo IRECRUIT_HOME;?>';
var user_id         =   '<?php echo $USERID;?>';
var tvcolors        =   [{"MonthHeader":"<?php echo $user_preferences['TableHeader']?>", "DayHeader":"<?php echo $user_preferences['TableRowHeader']?>", "WeekEnd":"<?php echo $user_preferences['TableColumnColor1']?>", "EventDayBackground":"<?php echo $user_preferences['TableColumnColor2']?>"}];
var nav_color       =   '<?php echo $navbar_color;?>';
var app_labels_list =   '<?php echo $organization_info['ApplicantConsiderationLabels'];?>';
<?php
if (preg_match ( '/administration.php$/', $_SERVER ["SCRIPT_NAME"] )) {
    echo 'var r_action = "purchases";'."\n";
}
else {
    echo 'var r_action = "requisitions";'."\n";
}
?>
</script>

<!-- 
Load default scripts for all the templates
-->

<script src="<?php echo IRECRUIT_HOME?>js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME?>js/jquery-browser-migration.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME?>js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME?>js/common.min.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME?>jscolor/jscolor.js" type="text/javascript"></script>
<?php 
//To set the javascript variables
if(is_array($this->script_vars_header) && count($this->script_vars_header) > 0) {
    echo '<script type="text/javascript">';
    foreach($this->script_vars_header as $script_variable) {
        echo "\n" . $script_variable;
    }
    echo "\n";

    echo 'if(typeof(datepicker_ids) != "undefined")';
    echo '{'."\n";
    echo 'date_picker(datepicker_ids, date_format);'."\n";
    echo '}'."\n";
    echo '</script>'."\n";
}

if(isset($page_scripts_header)) {
	foreach ($page_scripts_header as $script_name) {
		
		echo '<script type="text/javascript"';
		echo ' src="' . IRECRUIT_HOME . $script_name.'">';
		echo '</script>'."\n";

	}
}

if(isset($scripts_header_inline)) {
	foreach ($scripts_header_inline as $script_val_inline) {

		echo '<script type="text/javascript">'."\n";
		echo $script_val_inline."\n";
		echo '</script>'."\n";

	}
}
?>
</head>
<body>
