<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav navbar-collapse">
		<ul class="nav" id="side-menu">
			<?php
			include IRECRUIT_DIR . 'NavigationMenu.inc';
			
			$menu_category_icons["Home"] = "fa fa-dashboard fa-fw";
			$menu_category_icons["Applicant Management"] = "fa fa-file-text fa-fw";
			$menu_category_icons["Comparative Analysis"] = "fa fa-signal fa-fw";
			$menu_category_icons["Requisition Management"] = "fa fa-trello fa-fw";
			$menu_category_icons["Application Forms"] = "fa fa-files-o fa-fw";
			$menu_category_icons["iConnect"] = "fa fa-italic fa-fw";
			$menu_category_icons["Correspondence"] = "fa fa-puzzle-piece fa-fw";
			$menu_category_icons["WOTC Forms"] = "fa fa-wordpress fa-fw";
			$menu_category_icons["Reports"] = "fa fa-list-ul fa-fw";
			$menu_category_icons["Administration"] = "fa fa-adn fa-fw";
			$menu_category_icons["CMS WOTC"] = "fa fa-credit-card fa-fw";
			

			$menu_link_highlight["Home"] = "1";
			$menu_link_highlight["Applicant Management"] = "2";
			$menu_link_highlight["Requisition Management"] = "3";
			$menu_link_highlight["Application Forms"] = "4";
			$menu_link_highlight["iConnect"] = "5";
			$menu_link_highlight["Correspondence"] = "6";
			$menu_link_highlight["Reports"] = "7";
			$menu_link_highlight["Administration"] = "8";
			$menu_link_highlight["Comparative Analysis"] = "10";
			$menu_link_highlight["CMS WOTC"] = "11";
			
			$menu_category_index = 1;
			foreach ( $navigation_menu as $menu_category => $menu_info ) {
				
				echo "<li>";
				
				if(count($menu_info) == 1) {
					foreach ( $menu_info as $menu_name => $menu_link ) {
						echo "<a href='$menu_link' ";
					}
				}
				else {
					echo "<a href='#' ";
				}

								
				if(isset($_GET['menu']) && $_GET['menu'] != "" && $menu_link_highlight[$menu_category] == $_GET['menu']) {
					echo ' class="active"';
				}
				if ((! isset ( $_GET ['menu'] ) || $_GET ['menu'] == $menu_link_highlight[$menu_category]) && $menu_category == "Home") {
					echo ' class="active"';
				}
				
				
				echo "><i class='".$menu_category_icons[$menu_category]."'></i>";
				
				echo $menu_category;
				
				echo "<span class='fa fa-caret-right'></span></a>";
				
				if(count($menu_info) > 1) {
				
				    echo "<ul class='nav nav-second-level'>";

                    foreach ( $menu_info as $menu_name => $menu_link ) {
                        	
                        $menu_attributes = "";
                        	
                        if(isset($menu_info["Attributes"][$menu_name]) && is_array($menu_info["Attributes"][$menu_name])) {
                            foreach ($menu_info["Attributes"][$menu_name] as $attribute_name=>$attribute_value) {
                                $menu_attributes .= " ".$attribute_name."='".$attribute_value."'";
                            }
                        }
                        	
                        if($menu_name != "Attributes") {


                    
                            $menu_name_html  = '<li><a href="' . $menu_link . '"';
			      if ($menu_name == "Requisition Export") {
                                $menu_name_html .= ' onclick="return confirm(\'Are you sure you want to run this report?\n\n\')"';
			      }
                            $menu_name_html .= $menu_attributes;
                            $menu_name_html .= '>' . $menu_name;
                    
                            if(isset($navigation_sub_menu) && is_array($navigation_sub_menu[$menu_category][$menu_name])) {
                                $menu_name_html .= '<i class="fa fa-angle-down sub-menu-angle-down"></i>';
                            }
                    
                            $menu_name_html .= '</a>';
                    
                            if(isset($navigation_sub_menu) && is_array($navigation_sub_menu[$menu_category][$menu_name])) {
                                $menu_name_html .= '<ul class="nav nav-third-level">';
                    
                                foreach($navigation_sub_menu[$menu_category][$menu_name] as $sub_menu_name=>$sub_menu_link) {
                                    $menu_name_html .= '<li>';
                                    $menu_name_html .= '<a href="'.$sub_menu_link.'">'.$sub_menu_name.'</a>';
                                    $menu_name_html .= '</li>';
                                }
                                	
                                $menu_name_html .= '</ul>';
                            }
                    
                            $menu_name_html .= '</li>';
                            echo $menu_name_html;
                        }
                        	
                    }
                    
                    echo "</ul>";
				}
					
				echo "</li>";
				
				$menu_category_index ++;
			}
			?>
		</ul>
	</div>
	<!-- /.sidebar-collapse -->
</div>
