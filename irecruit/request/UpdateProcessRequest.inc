<?php
if ($_POST ['PostMeridian'] == "AM") {
	if ($PostHour == 12) {
		$PostHour = "00";
	}
}
if ($_POST ['PostMeridian'] == "PM") {
	if ($PostHour < 12) {
		$PostHour += 12;
	}
}
if ($_POST ['ExpireMeridian'] == "AM") {
	if ($ExpireHour == 12) {
		$ExpireHour = "00";
	}
}
if ($_POST ['ExpireMeridian'] == "PM") {
	if ($ExpireHour < 12) {
		$ExpireHour += 12;
	}
}

$PostDate = substr ( $PostDate, - 4 ) . '-' . substr ( $PostDate, 0, 2 ) . '-' . substr ( $PostDate, 3, 2 ) . ' ' . $PostHour . ':' . $PostMinute . ':00';
$ExpireDate = substr ( $ExpireDate, - 4 ) . '-' . substr ( $ExpireDate, 0, 2 ) . '-' . substr ( $ExpireDate, 3, 2 ) . ' ' . $ExpireHour . ':' . $ExpireMinute . ':00';

$howmany = $_REQUEST['howmany'];
if ($_REQUEST['howmany'] == "" || !isset($_REQUEST['howmany'])) {
	$howmany = 0;
}
if ($poe_from == "") {
	$poe_from = '00/00/0000';
}
if ($poe_to == "") {
	$poe_to = '00/00/0000';
}

$ADVERTISEOPTIONS = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
$AO = array ();
foreach ( $ADVERTISEOPTIONS as $option ) {
	$AO [$option] = $_POST [$option];
}

$SAO = serialize ( $AO );

$WORKWEEK = array (
		"SUN" => "Sunday",
		"MON" => "Monday",
		"TUE" => "Tuesday",
		"WED" => "Wednesday",
		"THU" => "Thursday",
		"FRI" => "Friday",
		"SAT" => "Saturday"
);

$WW = array_keys ( $WORKWEEK );
$WorkWeek = array ();
foreach ( $WW as $d ) {
	if ($_POST [$d] == "Y") {
		$WorkWeek [$d] = $_POST [$d];
	}
}

$workdays = serialize ( $WorkWeek );

if ($poe_from) {
	$POE_FROM = substr ( $poe_from, - 4 ) . '-' . substr ( $poe_from, 0, 2 ) . '-' . substr ( $poe_from, 3, 2 );
}
if ($poe_to) {
	$POE_TO = substr ( $poe_to, - 4 ) . '-' . substr ( $poe_to, 0, 2 ) . '-' . substr ( $poe_to, 3, 2 );
}


$set_info = array (
				"LastModified = NOW()",
				"Title = :Title",
				"PostDate = :PostDate",
				"ExpireDate = :ExpireDate",
				"MultiOrgID = :MultiOrgID",
				"RequisitionID = :RequisitionID",
				"JobID = :JobID",
				"Description = :Description",
				"Salary_Range_from = :Salary_Range_from",
				"Salary_Range_to = :Salary_Range_to",
				"HowMany = :HowMany",
				"WhenNeeded = :WhenNeeded"
			);

$params = array (
				":OrgID"=>$OrgID,
				":RequestID" => $RequestID,
				":Title" => $_REQUEST['requesttitle'],
				":PostDate" => $PostDate,
				":ExpireDate" => $ExpireDate,
				":MultiOrgID" => $_REQUEST['MultiOrgID'],
				":RequisitionID" => $_POST ['RequisitionID'],
				":JobID" => $_POST ['JobID'],
				":Description" => $_REQUEST['Description'],
				":Salary_Range_from" => $_REQUEST['salaryrangefrom'],
				":Salary_Range_to" => $_REQUEST['salaryrangeto'],
				":HowMany" => $_REQUEST['howmany'],
				":WhenNeeded" => $_REQUEST['whenneeded']
			);

if(isset($listingpriority)) {
	$set_info[] = "ListingPriority = :ListingPriority";
	$params[":ListingPriority"] =  $_POST ['listingpriority'];
}

if(isset($InternalFormID)) {
	$set_info[] = "InternalFormID = :InternalFormID";
	$params[":InternalFormID"] =  $_POST ['InternalFormID'];
}

if(isset($FormID)) {
	$set_info[] = "FormID = :FormID";
	$params[":FormID"] =  $_POST ['FormID'];
}

if(isset($EmpStatusID)) {
	$set_info[] = "EmpStatusID = :EmpStatusID";
	$params[":EmpStatusID"] =  $_POST ['EmpStatusID'];
}

if(isset($eeocode)) {
	$set_info[] = "EEOCode = :EEOCode";
	$params[":EEOCode"] =  $_POST ['eeocode'];
}

if(isset($JobGroupCode)) {
	$set_info[] = "JobGroupCode = :JobGroupCode";
	$params[":JobGroupCode"] =  $_POST ['JobGroupCode'];
}

if(isset($Exempt_Status)) {
	$set_info[] = "Exempt_Status = :Exempt_Status";
	$params[":Exempt_Status"] = $_POST['Exempt_Status'];
}

if(isset($education)) {
	$set_info[] = "Education = :Education";
	$params[":Education"] = $education;
}

if(isset($skills)) {
	$set_info[] = "Skills = :Skills";
	$params[":Skills"] = $skills;
}

if(isset($udf1)) {
	$set_info[] = "UDF1 = :UDF1";
	$params[":UDF1"] = $udf1;
}

if(isset($udf2)) {
	$set_info[] = "UDF2 = :UDF2";
	$params[":UDF2"] = $udf2;
}

if(isset($importancelevel)) {
	$set_info[] = "ImportanceID = :ImportanceID";
	$params[":ImportanceID"] = $importancelevel;
}

if(isset($SAO)) {
	$set_info[] = "AdvertisingOptions = :AdvertisingOptions";
	$params[":AdvertisingOptions"] = $SAO;
}

if(isset($workdays)) {
	$set_info[] = "WorkDays = :WorkDays";
	$params[":WorkDays"] = $workdays;
}

if(isset($hours)) {
	$set_info[] = "Hours = :Hours";
	$params[":Hours"] = $hours;
}

if(isset($weeks)) {
	$set_info[] = "Weeks = :Weeks";
	$params[":Weeks"] = $weeks;
}

if(isset($program)) {
	$set_info[] = "Program = :Program";
	$params[":Program"] = $program;
}

if(isset($replacement)) {
	$set_info[] = "Replacement = :Replacement";
	$params[":Replacement"] = $replacement;
}

if(isset($reportsto)) {
	$set_info[] = "ReportsTo = :ReportsTo";
	$params[":ReportsTo"] = $reportsto;
}

if(isset($POE_FROM)) {
	$set_info[] = "POE_from = :POE_from";
	$params[":POE_from"] = $POE_FROM;
}

if(isset($POE_TO)) {
	$set_info[] = "POE_to = :POE_to";
	$params[":POE_to"] = $POE_TO;
}

if(isset($isauditor)) {
	$set_info[] = "IsAuditor = :IsAuditor";
	$params[":IsAuditor"] = $isauditor;
}

if(isset($isapprover)) {
	$set_info[] = "IsApprover = :IsApprover";
	$params[":IsApprover"] = $isapprover;
}

if(isset($_POST['Owner'])) {
	$set_info[] = "Owner = :Owner";
	$params[":Owner"] = $_POST['Owner'];
}

if(isset($_POST ['City'])) {
	$set_info[] = "City = :City";
	$params[":City"] = $_POST ['City'];
}


if(isset($_POST ['State'])) {
	$set_info[] = "State = :State";
	$params[":State"] = $_POST ['State'];
}

if(isset($_POST ['ZipCode'])) {
	$set_info[] = "ZipCode = :ZipCode";
	$params[":ZipCode"] = $_POST ['ZipCode'];
}

if(isset($_POST ['Country'])) {
	$set_info[] = "Country = :Country";
	$params[":Country"] = $_POST ['Country'];
}

if(isset($_POST ['InternalDisplayAA'])) {
	$set_info[] = "InternalDisplayAA = :InternalDisplayAA";
	$params[":InternalDisplayAA"] = $_POST ['InternalDisplayAA'];
}

if(isset($_POST ['DisplayAA'])) {
	$set_info[] = "DisplayAA = :DisplayAA";
	$params[":DisplayAA"] = $_POST ['DisplayAA'];
}

if(isset($_POST ['Address1'])) {
	$set_info[] = "Address1 = :Address1";
	$params[":Address1"] = $_POST ['Address1'];
}

if(isset($_POST ['Address2'])) {
	$set_info[] = "Address2 = :Address2";
	$params[":Address2"] = $_POST ['Address2'];
}

if(isset($multicostcenter)) {
	$set_info[] = "MultiCostCenter = :MultiCostCenter";
	$params[":MultiCostCenter"] = $multicostcenter;
}

//Monster Information
if(isset($JobCategory)) {
	$set_info[] = "MonsterJobCategory = :MonsterJobCategory";
	$params[":MonsterJobCategory"] = $JobCategory;
}

if(isset($JobOccupations)) {
	$set_info[] = "MonsterJobOccupation = :MonsterJobOccupation";
	$params[":MonsterJobOccupation"] = $JobOccupations;
}

if(isset($Duration)) {
	$set_info[] = "Duration = :Duration";
	$params[":Duration"] = $Duration;
}

if(isset($MonsterJobIndustry)) {
	$set_info[] = "MonsterJobIndustry = :MonsterJobIndustry";
	$params[":MonsterJobIndustry"] = $MonsterJobIndustry;
}

$where = array (
	"OrgID = :OrgID",
	"RequestID = :RequestID"
);

if($feature['RequisitionApproval'] == "Y") {
	$set_info[] = "Active = :Active";
	$set_info[] = "Approved = :Approved";
	$params[":Active"] = "R";
	$params[":Approved"] = "N";
}

if($feature['RequisitionApproval'] != "Y") {
	$set_info[] = "Active = :Active";
	$set_info[] = "Approved = :Approved";
	$params[":Active"] = "Y";
	$params[":Approved"] = "Y";
}

// Update Requisitions Information
$RequisitionsObj->updRequisitionsInfo ('Requisitions', $set_info, $where, array ($params) );



// Set where condition
$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
// Set parameters information
$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Delete RequisitionManagers Information
$RequisitionsObj->delRequisitionsInfo ( 'RequisitionManagers', $where, array ($params) );

if (is_array ( $_POST ['Managers'] )) {
	while ( list ( $key, $val ) = each ( $_POST ['Managers'] ) ) {
			
		// Requisition Managers Information to insert
		$req_man_info = array (
				"OrgID" => $OrgID,
				"RequestID" => $RequestID,
				"UserID" => $val
		);
			
		if ($val != "") {
			// Insert RequisitionManagers Information
			$RequisitionsObj->insRequisitionsInfo ( 'RequisitionManagers', $req_man_info );
		}
	} // end while
} // end if Managers is array


// set where condition
$where = array ("OrgID = :OrgID", "RequestID = :RequestID");
// set parameters
$params = array (":OrgID" => $OrgID, ":RequestID" => $RequestID);
// Delete requisition org levels information
$RequisitionsObj->delRequisitionsInfo ( 'RequisitionOrgLevels', $where, array($params) );

$i = 1;
while ( $i <= 10 ) {
	if (is_array ( $_POST [$i] )) {

		while ( list ( $key, $val ) = each ( $_POST [$i] ) ) {
				
			if ($val) {
				// set org levels information
				$req_org_levels = array (
						"OrgID" => $OrgID,
						"RequestID" => $RequestID,
						"OrgLevelID" => $i,
						"SelectionOrder" => $val
				);

				// Insert Requisitions Informations
				$RequisitionsObj->insRequisitionsInfo ( 'RequisitionOrgLevels', $req_org_levels );
			} // end if val
		} // end while
	} // end is_array
		
	$i ++;
} // end while Levels

if (! $USERID) {
	$USERID = 'Requester';
	$Comments = 'Request has been updated';
	updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );
} else if($USERID) {
	updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );
}
?>