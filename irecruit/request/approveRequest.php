<?php
$PAGE_TYPE = "Email";
$IRECRUIT_USER_AUTH = FALSE;
require_once '../Configuration.inc';

$TemplateObj->title 		= $title 		= "Approve Request Form";
$TemplateObj->OrgID 		= $OrgID 		= isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
$TemplateObj->Comment 		= $Comment 		= isset($_REQUEST['Comment']) ? $_REQUEST['Comment'] : '';
$TemplateObj->EmailAddress 	= $EmailAddress = isset($_REQUEST['EmailAddress']) ? $_REQUEST['EmailAddress'] : '';
$TemplateObj->RequestID 	= $RequestID 	= isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->process 		= $process 		= isset($_REQUEST['process']) ? $_REQUEST['process'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/request/ApproveRequest');
?>