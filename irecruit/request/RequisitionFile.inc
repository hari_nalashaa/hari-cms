<?php
$dir = IRECRUIT_DIR . 'vault/' . $OrgID;

$requisitionfilesdir = $dir . '/requisitionfiles';

if (! file_exists ( $dir )) {
	mkdir ( $dir, 0700 );
	chmod ( $dir, 0777 );
}

if (! file_exists ( $requisitionfilesdir )) {
	mkdir ( $requisitionfilesdir, 0700 );
	chmod ( $requisitionfilesdir, 0777 );
}

$i = 0;
while ( $i < 3 ) {
	$i ++;
	
	$requestfile = "requestfile-" . $i;
	$filepurpose = "filepurpose-" . $i;
	$removefile = "remove-" . $i;
	
	if ($_POST [$removefile] == "Y") {
		//set where condition
		$where = array("OrgID = :OrgID", "RequestID = :RequestID", "FileID = :FileID");
		//set parameters
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":FileID"=>$i);
		//Delete requisitions files info
		$RequisitionsObj->delRequisitionsInfo('RequisitionFiles', $where, array($params));
	}
	
	if ($_POST [$filepurpose]) {
		
		$set_info = array("FilePurposeID = :FilePurposeID");
		$where 	  = array("OrgID = :OrgID", "RequestID = :RequestID", "FileID = :FileID");
		$params   = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":FileID"=>$i, ":FilePurposeID"=>$_POST[$filepurpose]);
		
		$RequisitionsObj->updRequisitionsFilesInfo($set_info, $where, array($params));
	}
	
	if ($_FILES [$requestfile] ['type']) {
		
		$fileTypeResume = $_FILES [$requestfile] ['type'];
		$data = file_get_contents ( $_FILES [$requestfile] ['tmp_name'] );
		$e = explode ( '.', $_FILES [$requestfile] ['name'] );
		$ecnt = count ( $e ) - 1;
		$ext = $e [$ecnt];
		
		//set where condition
		$where = array("OrgID = :OrgID", "RequestID = :RequestID", "FileID = :FileID");
		//set parameters
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":FileID"=>$i);
		//Delete requisitions files info
		$RequisitionsObj->delRequisitionsInfo('RequisitionFiles', $where, array($params));
		
		
		//Requisition Files Information
		$req_files_info = array("OrgID"=>$OrgID, "RequestID"=>$RequestID, "FileID"=>$i, "FilePurposeID"=>$_POST [$filepurpose], "FileType"=>$ext);
		//Insert into RequisitionFiles 
		$RequisitionsObj->insRequisitionsInfo('RequisitionFiles', $req_info);
		
		$filename = $requisitionfilesdir . '/' . $RequestID . '-' . $i . '.' . $ext;
		$fh = fopen ( $filename, 'w' );
		if (! $fh) {
			$ERROR = 'Cannot open file' . $filename;
			die ( include IRECRUIT_DIR . 'irecruit.err' );
		}
		fwrite ( $fh, $data );
		fclose ( $fh );
		
		chmod ( $filename, 0644 );
	}
} // end foreach
?>