<?php
function showExemptStatus($OrgID, $Exempt_Status) {
	$rtn = "";
	
	if ($Exempt_Status == "Y") {
		$rtn .= "Yes";
	} elseif ($Exempt_Status == "N") {
		$rtn .= "No";
	}
	
	return $rtn;
} // end function

function showEEOClassification($EEOCode) {
	global $OrganizationsObj;
	
	//set parameters
	$params    =   array(":Code"=>$EEOCode);
	//Set condition
	$where     =   array("Code = :Code");
	//Get EEO Classifications Information
	$results   =   $OrganizationsObj->getEEOClassificationsInfo('Description', $where, '', array($params));
	
	return $results['results'][0]['Description'];
} // end function

function showFilePurpose($OrgID, $filepurpose) {
	global $RequestObj;
	
	$purpose   =   "";
	
	//set where condition
	$where     =   array("OrgID = :OrgID", "FilePurposeID = :FilePurposeID");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID, ":FilePurposeID"=>$filepurpose);
	//Get file purpose info
	$results   =   $RequestObj->getFilePurposeInfo("PurposeTitle", $where, "", array($params));
	
	$title     =   $results['results'][0]['PurposeTitle'];	
	
	return $title;
} // end function

function showEmailApproverName($OrgID, $EmailAddress) {
	global $RequestObj;
	$name = "";
	
	//set columns
	$columns   =   "CONCAT(FirstName, ' ', LastName) as name";
	//set where condition
	$where     =   array("OrgID = :OrgID", "EmailAddress = :EmailAddress");
	//set params
	$params    =   array(":OrgID"=>$OrgID, ":EmailAddress"=>$EmailAddress);
	//get email approvers information
	$results   =   $RequestObj->getEmailApprovers($columns, $where, "", array($params));
	
	$name      =   $results['results'][0]['name'];
	
	return $name;
}
?>