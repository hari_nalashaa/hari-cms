<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

if ($permit ['Requisitions_Edit'] < 1) {
	die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
}


$TemplateObj->title = $title = 'Notify Approvers';
$TemplateObj->process = $process = isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->notify = $notify = isset($_REQUEST['notify']) ? $_REQUEST['notify'] : '';

echo $TemplateObj->displayIrecruitTemplate('views/request/ApproverStatus');
?>
