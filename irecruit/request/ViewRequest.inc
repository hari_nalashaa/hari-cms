<?php
include IRECRUIT_DIR . 'request/DisplayRequestValues.inc';
function displayRequestInfo($OrgID, $RequestID, $USERID, $EmailAddress) {
	global $feature, $USERROLE, $RequisitionsObj, $OrganizationsObj, $RequestObj, $RequisitionQuestionsObj, $TemplateObj, $FormSettingsObj, $OrganizationDetailsObj, $RequisitionFormsObj, $RequisitionDetailsObj;
	
    $req_questions_posix        =   array("MultiOrgID", "Address1", "Address2", "City", "State", "ZipCode", "Country");

	$MonsterObj 				= 	new Monster ();
	$MonsterInformation 		= 	$MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgIDRequest );
	$MonsterOrgInfo 			= 	$MonsterInformation ['row'];
	$MonsterJobIndustries 		= 	$MonsterObj->getMonsterJobIndustries();
	
	$columns 	= "*, date_format(PostDate,'%m/%d/%Y') as PostDate, date_format(ExpireDate,'%m/%d/%Y') as ExpireDate,
				  date_format(PostDate,'%h') as PostHour, date_format(ExpireDate,'%h') as ExpireHour,
				  date_format(PostDate,'%p') as PostMeridian, date_format(ExpireDate,'%p') as ExpireMeridian,
				  date_format(PostDate,'%i') as PostMinute, date_format(ExpireDate,'%i') as ExpireMinute,
				  date_format(POE_from,'%m/%d/%Y') POE_from, date_format(POE_to,'%m/%d/%Y') POE_to, ApprovalLevelRequired, ApprovalLevel";
	$results_row 	= 	$RequisitionsObj->getRequisitionsDetailInfo($columns, $OrgID, $RequestID);
	
	if(count($error_inputs) == 0) {
		$APPDATA		= 	array();
		foreach($results_row  as $edit_field_key=>$edit_field_value)
		{
		    $emp_status_levels    =   $RequisitionDetailsObj->getEmploymentStatusLevelsList($OrgID, $results_row["RequisitionFormID"]);
		    $job_group_codes      =   $RequisitionDetailsObj->getJobGroupCodesList($OrgID, $results_row["RequisitionFormID"]);
		    $sal_grade_codes      =   $RequisitionDetailsObj->getSalaryGradeInfo($OrgID, $results_row["RequisitionFormID"]);

			if($edit_field_key == "MonsterJobIndustry") {
				if($edit_field_value == "0" || $edit_field_value === 0) $edit_field_value = "All"; 
				else $edit_field_value = $MonsterJobIndustries[$edit_field_value];
			}
			else if($edit_field_key == "MonsterJobCategory") {
				$MonsterJobCategoryInfo	= $MonsterObj->getJobCategoryNameByCategoryId($edit_field_value);
				$edit_field_value = $MonsterJobCategoryInfo['JobCategoryAlias'];
			}
			else if($edit_field_key == "MonsterJobOccupation") {
				$MonsterJobOccupationInfo 	= 	$MonsterObj->getOccupationByOccupationId($edit_field_value);
				$edit_field_value = $MonsterJobOccupationInfo['OccupationAlias'];
			}
			else if($edit_field_key == "AdvertisingOptions") {
				$edit_field_value = $AdvertisingOptions = unserialize ( $edit_field_value );
			}
			else if($edit_field_key == "WorkDays") {
				$edit_field_value = $WorkWeek = unserialize ( $edit_field_value );
			}
			else if($edit_field_key == "MultiOrgID") {
				$edit_field_value = $OrganizationDetailsObj->getOrganizationNameByRequestID ( $OrgID, $RequestID );
			}
			else if($edit_field_key == "EmpStatusID") {
			    
				$edit_field_value = ($emp_status_levels[$edit_field_value] != "") ? $emp_status_levels[$edit_field_value] : 'Not Assigned';
			}
			else if($edit_field_key == "SalaryGradeID") {
				$edit_field_value = ($sal_grade_codes[$edit_field_value] != "") ? $sal_grade_codes[$edit_field_value] : 'Not Assigned';
			}
			else if($edit_field_key == "JobGroupCode") {
				$edit_field_value = ($job_group_codes[$edit_field_value] != "") ? $job_group_codes[$edit_field_value] : 'Not Assigned';
			}
			else if($edit_field_key == "Exempt_Status") {
				$edit_field_value = (showExemptStatus ( $OrgID, $edit_field_value )) ? showExemptStatus ( $OrgID, $edit_field_value ) : 'Not Assigned';
			}
			else if($edit_field_key == "IsAuditor"
					|| $edit_field_key == "IsApprover"
					|| $edit_field_key == "InternalDisplayAA"
					|| $edit_field_key == "DisplayAA") {
				$edit_field_value = ($edit_field_value == "Y") ? "Yes" : "No";
			}
			else if($edit_field_key == "EEOCode") {
				$results_eeo_code = $OrganizationsObj->getEEOClassificationsInfo ( "Code, Description", array(), 'ListOrder');
					
				if (is_array ( $results_eeo_code ['results'] )) {
					foreach ( $results_eeo_code ['results'] as $row_eeocode ) {
						if ($row_eeocode ['Code'] == $edit_field_value) {
							$edit_field_value = $row_eeocode ['Description'];
						}
					}
				}
			}
			else if($edit_field_key == "ApprovalLevelRequired") {
				$approvallevelrequired = $edit_field_value;
			}
			
			$TemplateObj->{$edit_field_key} = ${$edit_field_key} = $edit_field_value;
		
			$APPDATA[$edit_field_key]	=	$edit_field_value;
		}
		
		$requisitions_data 	= G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);
		
		foreach($requisitions_data as $req_data_que=>$req_data_ans) {
			$APPDATA[$req_data_que]	=	$req_data_ans;
		}
	}

	//Get RequisitionFormID
	$RequisitionFormID     =   $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);
	$requisition_details   =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $RequestID);
	if($requisition_details['RequisitionFormID'] != "" && $RequestID != "") {
	    $RequisitionFormID =   $requisition_details['RequisitionFormID'];
	}
	
	$where_info 	= 	array("OrgID = :OrgID", "RequisitionFormID = :RequisitionFormID", "(Request = 'Y' OR Requisition = 'Y')");
	$params_info 	= 	array(":OrgID"=>$OrgID, ":RequisitionFormID"=>$RequisitionFormID);
	$columns		=	"QuestionID, Question, value, QuestionTypeID, QuestionOrder, Request";
	$requisition_questions_info = $RequisitionQuestionsObj->getRequisitionQuestions($columns, $where_info, "QuestionOrder ASC", array($params_info));

    if($feature ['InternalRequisitions'] != "Y") {
      foreach ($requisition_questions_info['results'] as $ID=>$FQR) {
            if ($FQR['QuestionID'] == "InternalFormID") {
               unset($requisition_questions_info['results'][$ID]);
            }
            if ($FQR['QuestionID'] == "InternalDisplayAA") {
               unset($requisition_questions_info['results'][$ID]);
            }
      }
    }

	$TemplateObj->requisition_questions = $requisition_questions = $requisition_questions_info['results'];
	
    $req_que_list   =   array();
	
	if(is_array($requisition_questions)) {
		foreach ($requisition_questions as $rq_info) {
			echo $RequisitionQuestionsObj->printSection($OrgID, $RequestID, $rq_info['Question'], $rq_info['value'], $rq_info['QuestionID'], $rq_info['QuestionTypeID'], $APPDATA);
		}
	}
	
	$WorkWeeksList = array ("SUN" => "Sunday", "MON" => "Monday", "TUE" => "Tuesday", "WED" => "Wednesday", "THU" => "Thursday", "FRI" => "Friday", "SAT" => "Saturday");
	
	$TemplateObj->yes_no 		= $yes_no = array(""=>"Please Select", "Y"=>"Yes", "N"=>"No");
	$TemplateObj->WorkWeeksList = $WorkWeeksList;
	
	$TemplateObj->req_post_hours		=	$req_post_hours 		= array("12", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11");
	$TemplateObj->req_post_mins 		=	$req_post_mins 			= array("00", "15", "30", "45");
	
	
	if (count($results_row) < 1) {
		die ( include IRECRUIT_DIR . 'irecruit_permissions.err' );
	}
	
	
	$ANS = array ('Y' => 'Yes', 'N' => 'No');
	
	if (($salaryrangefrom) && ($salaryrangeto)) {
		$range = 'from ' . $salaryrangefrom . ' to ' . $salaryrangeto;
	}

	// set table name
	$table_name    =   "RequisitionOrgLevels R, OrganizationLevels O, OrganizationLevelData OD";
	// set columns
	$columns       =   "O.OrganizationLevel, OD.CategorySelection";
	// Set where condition
	$where         =   array(
                            "O.OrgID            =   R.OrgID", 
                            "O.OrgID            =   OD.OrgID", 
                            "O.OrgLevelID       =   R.OrgLevelID",
                            "O.OrgLevelID       =   OD.OrgLevelID",
                            "R.OrgLevelID       =   OD.OrgLevelID", 
                            "R.SelectionOrder   =   OD.SelectionOrder", 
                            "O.MultiOrgID       =   OD.MultiOrgID",
                            "R.OrgID            =   :OrgID", 
                            "R.RequestID        =   :RequestID", 
                            "O.MultiOrgID       =   :MultiOrgID"
                        );
	// set parameters
	$params        =   array(
                            ":OrgID"            =>  $OrgID,
                            ":RequestID"        =>  $RequestID,
                            ":MultiOrgID"       =>  $MultiOrgID 
                        );
	// Get Requisition and Organization Levels Information
	$results       =   $OrganizationsObj->getOrgAndReqLevelsInfo ( $table_name, $columns, $where, '', array ($params) );
	
	foreach ( $results ['results'] as $LOCATIONS ) {
		$locations .= '<tr><td align="right">' . $LOCATIONS ['OrganizationLevel'] . ':</td>';
		$locations .= '<td>' . $LOCATIONS ['CategorySelection'] . '</td></tr>';
	} // end while
	
	$i = 0;
	while ( $i < 3 ) {
		$i ++;
		
		// set where condition
		$where = array (
                "OrgID          = :OrgID",
                "RequestID      = :RequestID",
                "FileID         = :FileID" 
		);
		// set parameters
		$params = array (
                ":OrgID"        => $OrgID,
                ":RequestID"    => $RequestID,
                ":FileID"       => $i 
		);
		// Get Requisition Files Information
		$results = $RequisitionsObj->getRequisitionFilesInfo ( "FilePurposeID, FileType", $where, "", array ($params) );
		
		$RF               =   $results ['results'] [0];
		$filepurposeid    =   $RF ['FilePurposeID'];
		$filetype         =   $RF ['FileType'];
		
		if ($filepurposeid) {
			
			$file [$i] .= '<a href="' . IRECRUIT_HOME . 'display_requisitionfile.php?requestid=' . $RequestID . '&fileid=' . $i . '">';
			$file [$i] .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
			$file [$i] .= '&nbsp;&nbsp;';
			$file [$i] .= showFilePurpose ( $OrgID, $filepurposeid );
			$file [$i] .= '</a>';
		} // end filepurposeid
	} // end while
	
	$file1 = $file [1];
	$file2 = $file [2];
	$file3 = $file [3];
	
	// long display for verifying request process
	$request = '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered table-hover">' . "\n";
	
	$request .= '<tr><td colspan="2">';
	
	include IRECRUIT_DIR . 'request/ViewRequestHistory.inc';
	$request .= displayHistoryComments ( $OrgID, $RequestID );
	$request .= '</td></tr>';
	
	$request .= '<tr><td colspan="2"><hr></td></tr>';
	
	if ((preg_match ( '/viewReqRequirements.php$/', $_SERVER ["SCRIPT_NAME"] )) 
	   || (preg_match ( '/approveRequest.php$/', $_SERVER ["SCRIPT_NAME"] )) 
	   || (preg_match ( '/assign.php$/', $_SERVER ["SCRIPT_NAME"] ))) {
		
		// set where condition
		$where = array (
				"RA.OrgID           = EA.OrgID",
				"RA.EmailAddress    = EA.EmailAddress",
				"RA.OrgID           = :OrgID",
				"RA.RequestID       = :RequestID" 
		);
		// set parameters
		$params = array (
				":OrgID"        => $OrgID,
				":RequestID"    => $RequestID 
		);
		// set columns
		$columns = "RA.EmailAddress, RA.AuthorityLevel, date_format(RA.DateApproval,'%Y-%m-%d %H:%i') DA, EA.FirstName, EA.LastName";
		// /get approvers information
		$results = $RequestObj->getApproversInfo ( "RequestApprovers RA, EmailApprovers EA", $columns, $where, "RA.AuthorityLevel DESC", array (
				$params 
		) );

		$where = array (
                                "RA.OrgID           = :OrgID",
                                "RA.RequestID       = :RequestID",
                                "RA.DateApproval    = '0000-00-00 00:00:00'"
                );


		$MAX = $RequestObj->getApproversInfo ( "RequestApprovers RA", "min(RA.AuthorityLevel) AS Next", $where, "RA.AuthorityLevel DESC", array (
				$params 
		) );

		$currentapprovallevel = $MAX['results'][0]['Next'];
		
		$hit = $results ['count'];
		
		$request .= '<tr><td colspan="2">' . "\n";
		
		if ($hit > 0) {
			
			$request .= 'Approval Level Needed: ' . $approvallevelrequired . '<br><br>';
			
			$request .= '<table border="0" cellspacing="3" cellpadding="5" width="770" class="table table-striped table-bordered table-hover">';
			
			if ($Approved == "Y") {
				$rowcolor = "#eeeeee";
			} else {
				$rowcolor = "#ffffff";
			}
			
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $RA ) {
					
					$RA ['EmailAddress'] = trim ( $RA ['EmailAddress'] );
					$activateapprove [$RA ['EmailAddress']] = "N";
					
					if ($RA ['DA'] == '0000-00-00 00:00:00' || $RA ['DA'] == '0000-00-00 00:00' || is_null($RA ['DA'])) {
						$DA = "Approval Pending";
						
						if ($RA ['AuthorityLevel'] == $currentapprovallevel) {
							$rowcolor = "#88c98e"; // green
							if ($EmailAddress == $RA ['EmailAddress']) {
								$activateapprove [$RA ['EmailAddress']] = "Y";
							} // end email address
						} else { // else authority level
							if ($EmailAddress == $RA ['EmailAddress']) {
								$rowcolor = "#f7f4c9"; // yellow
								if ($_REQUEST['override'] == "Y") {
									$activateapprove [$RA ['EmailAddress']] = "Y";
								} elseif ($currentapprovallevel > $RA ['AuthorityLevel']) {
									$activateapprove [$RA ['EmailAddress']] = "N";
								} else {
									$activateapprove [$RA ['EmailAddress']] = "O";
								} // end override
								if ($hit == 1) {
									$rowcolor = "#88c98e"; // green
									$activateapprove [$RA ['EmailAddress']] = "Y";
								}
							} else { // else email
								if ($Approved == "N") {
									$rowcolor = "#f79e68"; // orange
								}
							} // end email
						} // end authority level
					} else {
						$DA = "Approved: " . $RA ['DA'];
					} // end else !RA/DA

					$request .= '<tr style="background-color:' . $rowcolor . ';">';
					$request .= '<td align="left">&nbsp;&nbsp;&nbsp;' . $RA ['LastName'] . ', ' . $RA ['FirstName'] . '&nbsp;&nbsp;&nbsp;</td>';
					$request .= '<td align="left">&nbsp;&nbsp;&nbsp;' . $RA ['EmailAddress'] . '&nbsp;&nbsp;&nbsp;</td>';
					$request .= '<td align="center">' . $RA ['AuthorityLevel'] . '</td>';
					$request .= '<td align="center">' . $DA . '</td>';
					$request .= '</tr>';
					
					if ($Approved == "Y") {
						if ($rowcolor == "#eeeeee") {
							$rowcolor = "#ffffff";
						} else {
							$rowcolor = "#eeeeee";
						}
					} else { // else Approved
						$rowcolor = "#ffffff"; // white
					} // end Approved
				} // end foreach
			}
			
			$request .= '</table>';
		} else { // else hit
			
			$request .= 'Approvers need to be assigned.<br><br>';
		} // end hit
		
		$request .= '<hr></td></tr>' . "\n";
	} // end viewReqReqirements
	
	if (strlen ( strstr ( $_SERVER ["SCRIPT_NAME"], 'approveRequest.php' ) ) > 0) {
		
		if ($activateapprove [$EmailAddress] == "O") {
			
			$request .= '<tr><td colspan="2" align="center"><a href="' . IRECRUIT_HOME . 'request/approveRequest.php?OrgID=' . $OrgID . '&RequestID=' . $RequestID . '&EmailAddress=' . $EmailAddress . '&override=Y" onclick="return confirm(\'Are you sure you want to override approval?\n\n\')">Requires Override</a></td></tr>';
		} elseif ($activateapprove [$EmailAddress] == "Y") {
			
			$request .= '<tr><td colspan="2" align="center">';
			
			$request .= '<script language="JavaScript" type="text/javascript">' . "\n";
			
			$request .= "function setProcess(fm,sb)" . "\n";
			$request .= "{" . "\n";
			$request .= "   fm.form.elements['process'].value=sb;" . "\n";
			$request .= "}" . "\n";
			$request .= '</script>' . "\n";
			
			$request .= '<form method="post" action="approveRequest.php">' . "\n";
			
			$request .= 'Comment: <input type="text" name="Comment" value="" size="60" maxlength="255"><br><br>' . "\n";
			
			$request .= '<input type="hidden" name="OrgID" value="' . $OrgID . '">' . "\n";
			$request .= '<input type="hidden" name="RequestID" value="' . $RequestID . '">' . "\n";
			$request .= '<input type="hidden" name="EmailAddress" value="' . $EmailAddress . '">' . "\n";
			$request .= '<input type="hidden" name="process" value="Y">' . "\n";
			$request .= '<input type="submit" value="Approve" onclick="setProcess(this,\'Approve\')">' . "\n";
			$request .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			$request .= '<input type="submit" value="Reject" onclick="setProcess(this,\'Reject\')">' . "\n";
			
			$request .= '</form>' . "\n";
			$request .= '</td></tr>' . "\n";
		} // end activateapprove
	} else {
		if (preg_match ( '/publicRequest.php$/', $_SERVER ["SCRIPT_NAME"] )) {
			
			$request .= '<tr><td colspan="2" align="center">';
			
			if ($Approved == "Y") {
				
				$request .= 'This request has been approved.';
			} else { // else ApprovalLevelRequired
				$request .= '<a href="' . IRECRUIT_HOME . 'publicEditRequest.php?OrgID=' . $OrgID . '&RequestID=' . $RequestID . '&action=edit">';
				$request .= '<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;">';
				$request .= '<b style="font-size:8pt;">Edit this Request</b>';
				$request .= '</a>';
			} // end ApprovalLevelRequired
			
			$request .= '</td></tr>' . "\n";
		} // end preg_match
	}
	
	$request .= '</table>' . "\n";
	$request .= '<br><br>' . "\n";
	
	return $request;
} // end function
?>
