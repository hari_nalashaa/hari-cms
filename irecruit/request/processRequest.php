<?php
require_once '../Configuration.inc';

$TemplateObj->title     = $title        = 'View Request Details';
$TemplateObj->bg_color 	= $bg_color 	= "yellow";
$TemplateObj->formtable = $formtable	= "RequisitionQuestions";
$TemplateObj->RequestID = $RequestID 	= isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->action 	= $action 		= isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

require_once IRECRUIT_DIR . 'request/EmailLinks.inc';
require_once IRECRUIT_DIR . 'request/UpdateHistory.inc';

// Load Graphs Related Scripts, Only for this page
$scripts_header [] = "js/loadAJAX.js";
$scripts_header [] = "js/irec_Display.js";
$scripts_header [] = "tiny_mce/tinymce.min.js";
$scripts_header [] = "js/irec_Textareas.js";

// Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

$TemplateObj->error_inputs                      =   $error_inputs                   =   array();
$TemplateObj->requisition_process_identifier    =   $requisition_process_identifier =   "Requisition";
$TemplateObj->button_name                       =   $button_name                    =   "Process Request";

//Get RequisitionFormID
$RequisitionFormID = $RequisitionFormsObj->getDefaultRequisitionFormID($OrgID);
$requisition_details   =   $RequisitionsObj->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $RequestID);
if($requisition_details['RequisitionFormID'] != "" && $RequestID != "") {
    $RequisitionFormID  =   $requisition_details['RequisitionFormID'];
}

$TemplateObj->RequisitionFormID =   $RequisitionFormID;

$where_info 	= 	array("OrgID = :OrgID", "(Requisition = :Requisition OR Request = :Request)", "RequisitionFormID = :RequisitionFormID");
$params_info 	= 	array(":OrgID"=>$OrgID, ":Requisition"=>'Y', ":Request"=>'Y', ":RequisitionFormID"=>$RequisitionFormID);
$columns		=	"OrgID, QuestionID, Question, QuestionTypeID, QuestionOrder, Request, Requisition, Required";
$requisition_questions_info = $RequisitionQuestionsObj->getRequisitionQuestions($columns, $where_info, "QuestionOrder ASC", array($params_info));
$TemplateObj->requisition_questions = $requisition_questions = $requisition_questions_info['results'];

$WorkWeeksList = array ("SUN" => "Sunday", "MON" => "Monday", "TUE" => "Tuesday", "WED" => "Wednesday", "THU" => "Thursday", "FRI" => "Friday", "SAT" => "Saturday");

$TemplateObj->yes_no 		= $yes_no = array(""=>"Please Select", "Y"=>"Yes", "N"=>"No");
$TemplateObj->WorkWeeksList = $WorkWeeksList;

// Get Affirmative Action Settings Information
$TemplateObj->req_post_hours		=	$req_post_hours 		= array("12", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11");
$TemplateObj->req_post_mins 		=	$req_post_mins 			= array("00", "15", "30", "45");

if(isset($_REQUEST['process_requisition']) && $_REQUEST['process_requisition'] == 'Y') {

	$ADVERTISEOPTIONS = preg_grep ( "/AdvertisingOption/", array_keys ( $_POST ) );
	$AO = array ();
	$SAO = serialize ( $AO );
	if(is_array($ADVERTISEOPTIONS)) {
		foreach ( $ADVERTISEOPTIONS as $option ) {
			$AO [$option] = $_POST [$option];
		}
		$SAO = serialize ( $AO );
	}

	$TemplateObj->AO = $AO;
	$TemplateObj->AdvertisingOptions = $AdvertisingOptions = unserialize ( $SAO );

	$ad_valid_status = "true";
	foreach ($AdvertisingOptions as $AOKeyName=>$AOValueName) {
		if($AdvertisingOptions[$AOKeyName] != "") $ad_valid_status = "false";
	}

	//Set columns
	$columns = "MAX(OrgLevelID) as MaxOrgLevelID";
	//Set where condition, it is common for below organization levels information function calls
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters, it is common for below organization levels information function calls
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$_REQUEST['MultiOrgID']);
	
	//Get organization levels information
	$results = $OrganizationsObj->getOrganizationLevelsInfo($columns, $where, '', array($params));
	$Levels  = $results['results'][0];
	
	$i = 1;
	$org_error = "true";
	while ( $i <= $Levels ['MaxOrgLevelID'] ) {
	    if(count($_REQUEST[$i]) > 0 && $_REQUEST[$i][0] != "") {
	        $org_error = "false";
	    }
	    $i++;
	}
	
	
	$WW = array_keys ( $WorkWeeksList );
	$WorkWeek = array ();
	foreach ( $WW as $d ) {
		if ($_POST [$d] == "Y") {
			$WorkWeek [$d] = $_POST [$d];
		}
	}

	$TemplateObj->WorkWeek = $WorkWeek;

	for($i = 0; $i < count ( $requisition_questions ); $i ++) {
		if($requisition_questions[$i]['Required'] == 'Y') {
			if(($requisition_questions[$i]['QuestionTypeID'] == "18" 
			    || $requisition_questions[$i]['QuestionTypeID'] == "1818")) {
					
				$QuestionIDcnt  = $_REQUEST[$requisition_questions[$i]['QuestionID']."cnt"];
		
				$error_answer = true;
				for($chkri = 1; $chkri <= $QuestionIDcnt; $chkri++) {
					if(isset($_POST [$requisition_questions[$i]['QuestionID']."-".$chkri])
					&& $_POST [$requisition_questions[$i]['QuestionID']."-".$chkri] != '') {
						$error_answer = false;
					}
				}
				if($error_answer === true) {
					$error_inputs[] = $requisition_questions[$i]['QuestionID'];
				}
					
			}
			else {
				if($_POST [$requisition_questions[$i]['QuestionID']] == ''
						&& $requisition_questions[$i]['QuestionID'] != "MultiOrgID") {
					$error_inputs[] = $requisition_questions[$i]['QuestionID'];
				}
			}
		}
	}
	
	if($org_error == "true") {
	    $error_inputs[] = "OrgLevel";
	}

	foreach ($error_inputs as $error_key=>$error_value) {
		if ($error_value == "AdvertisingOptions" && $ad_valid_status == "false") {
			unset($error_inputs[$error_key]);
		}
		if ($error_value == "WorkDays" && count($WorkWeek) > 0) {
			unset($error_inputs[$error_key]);
		}
		
		if ($error_value == "MonsterJobIndustry") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobCategory") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobOccupation") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "MonsterJobPostType") {
		    unset($error_inputs[$error_key]);
		}
		if ($error_value == "ZipRecruiterJobCategory") {
		    unset($error_inputs[$error_key]);
		}
		
		//This will be applicable only if InternalRequisitions feature is disabled
		if($feature ['InternalRequisitions'] != "Y") {
		
			if ($error_value == "InternalFormID"
				|| $error_value == "InternalDisplayAA") {
				unset($error_inputs[$error_key]);
			}
		}
	}
	
	$APPDATA	=	array();
	// Set post values to variables
	foreach ( $_POST as $pkey => $pvalue ) {

		if($pkey == "MultiOrgID") $pkey	= "MultiOrgIDRequest";
		if($pkey == "Address1") $pkey = "Address1Request";
		if($pkey == "Address2") $pkey = "Address2Request";
		if($pkey == "City") $pkey = "CityRequest";
		if($pkey == "State") $pkey = "StateRequest";
		if($pkey == "ZipCode") $pkey = "ZipCodeRequest";
		if($pkey == "Country") $pkey = "CountryRequest";
		$TemplateObj->{$pkey} = ${$pkey} = $pvalue;

		if ($pkey == 'shifts_schedule_time' || $pkey == 'LabelSelect') {

			foreach ( $_POST [$pkey] as $cquestion => $canswer ) {

				if ($pkey == 'LabelSelect') {
					$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
				}
				if ($pkey == 'shifts_schedule_time') {
					$qa ['from_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
					$qa ['to_time'] 	= $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
					$qa ['days'] 		= $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
					$answer 			= serialize ( $qa );
				}
					
				$APPDATA[$cquestion] = 	$answer;
			}
		}
		else {
			$APPDATA[$pkey]	= $pvalue;
		}
	}

	$TemplateObj->APPDATA = $APPDATA;

	if(count($error_inputs) > 0) {

		$TemplateObj->errors = $errors = "Please fill all the mandatory fields";
		$TemplateObj->error_inputs = $error_inputs;
	}
	else {
	    
		$TemplateObj->edit_insert_msg = $edit_insert_msg = "Successfully Updated";
		//Update requisition information
		require_once IRECRUIT_DIR . 'requisitions/ProcessEditRequisitionInformation.inc';

		if(isset($_REQUEST['page']) && $_REQUEST['page'] == "needsapproval") {
			header("Location:needsApproval.php?menu=3&msg=suc");
			exit;			
		} 
		else {
			if($feature['RequisitionApproval'] == "Y") {
				header("Location:needsApproval.php?menu=3&msg=suc_req_app");
				exit;
			}
			else {
				header("Location:requestsList.php?menu=3&msg=suc");
				exit;
			}
			
		}
	}
}


$columns 		= 	"*, date_format(PostDate,'%m/%d/%Y') as PostDate, date_format(ExpireDate,'%m/%d/%Y') as ExpireDate,
					  date_format(PostDate,'%h') as PostHour, date_format(ExpireDate,'%h') as ExpireHour,
					  date_format(PostDate,'%p') as PostMeridian, date_format(ExpireDate,'%p') as ExpireMeridian,
					  date_format(PostDate,'%i') as PostMinute, date_format(ExpireDate,'%i') as ExpireMinute,
					  date_format(POE_from,'%m/%d/%Y') POE_from, date_format(POE_to,'%m/%d/%Y') POE_to";
$results_row 	= 	$RequisitionsObj->getRequisitionsDetailInfo($columns, $OrgID, $RequestID);

if(count($error_inputs) == 0) {
	$APPDATA		= 	array();
	foreach($results_row  as $edit_field_key=>$edit_field_value)
	{
		if($edit_field_key == "MultiOrgID") {
			$edit_field_key	= "MultiOrgIDRequest";
		}
		if($edit_field_key == "Address1") {
			$edit_field_key = "Address1Request";
		}
		if($edit_field_key == "Address2") {
			$edit_field_key = "Address2Request";
		}
		if($edit_field_key == "City") {
			$edit_field_key = "CityRequest";
		}
		if($edit_field_key == "State") {
			$edit_field_key = "StateRequest";
		}
		if($edit_field_key == "ZipCode") {
			$edit_field_key = "ZipCodeRequest";
		}
		if($edit_field_key == "Country") {
			$edit_field_key = "CountryRequest";
		}
		if($edit_field_key == "AdvertisingOptions") {
			$edit_field_value = $AdvertisingOptions = unserialize ( $edit_field_value );
			$TemplateObj->AdvertisingOptions = $AdvertisingOptions;
		}
		if($edit_field_key == "WorkDays") {
			$edit_field_value = $WorkWeek = unserialize ( $edit_field_value );
			$TemplateObj->WorkWeek = $WorkWeek;
		}
		
		$TemplateObj->{$edit_field_key} = ${$edit_field_key} = $edit_field_value;

		$APPDATA[$edit_field_key]	=	$edit_field_value;
	}

	$requisitions_data 	= G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);

	foreach($requisitions_data as $req_data_que=>$req_data_ans) {
		$APPDATA[$req_data_que]	=	$req_data_ans;
	}
	
	$TemplateObj->APPDATA = $APPDATA; //This APPDATA variable is being used in DisplayQuestions, Don't change it.
}

$MonsterObj = new Monster ();
$MonsterInformation = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgIDRequest );
$MonsterOrgInfo = $MonsterInformation ['row'];

if((count($_POST) == 0)) 
{
	if ($OrgID == "I20140912") { // Community Research Foundation
	$po_exp_date_columns   =   "date_format(NOW(),'%m/%d/%Y') as RPD, date_format(DATE_ADD(NOW(), INTERVAL 30 DAY),'%m/%d/%Y') as RED";
	} else {
	$po_exp_date_columns   =   "date_format(NOW(),'%m/%d/%Y') as RPD, date_format(DATE_ADD(NOW(), INTERVAL 6 MONTH),'%m/%d/%Y') as RED";
	}
	$post_exp_date_row     =   $MysqlHelperObj->getDatesList ( $po_exp_date_columns );

	if($PostDate == "00/00/0000" && $ExpireDate == "00/00/0000") {
		$TemplateObj->PostDate 					= $PostDate 				= 	$post_exp_date_row ['RPD'];
		$TemplateObj->PostHour 					= $PostHour 				= 	"12";
		$TemplateObj->PostMinute 				= $PostMinute 				= 	"00";
		$TemplateObj->PostMeridian 				= $PostMeridian 			= 	"AM";
		$TemplateObj->ExpireDate 				= $ExpireDate 				= 	$post_exp_date_row ['RED'];
		$TemplateObj->ExpireHour 				= $ExpireHour 				= 	"11";
		$TemplateObj->ExpireMinute 				= $ExpireMinute 			= 	"00";
		$TemplateObj->ExpireMeridian 			= $ExpireMeridian 			= 	"PM";
	}
}


echo $TemplateObj->displayIrecruitTemplate('views/request/ProcessRequest');
?>
