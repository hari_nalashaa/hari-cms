<?php
require_once '../Configuration.inc';

if (($OrgID) && isset($_REQUEST['RequestID'])) {
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
	//$RequisitionsObj->insRequisitionsArchive(array($params));
	
	//Delete Requisitions Information
	$where = array("OrgID = :OrgID", "RequestID = :RequestID");
	$RequisitionsObj->delRequisitionsInfo('Requisitions', $where, array($params));
	
} // end OrgID RequestID

header ( 'Location: ' . IRECRUIT_HOME . 'request/needsApproval.php' );
exit ();
?>