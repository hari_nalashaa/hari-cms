<?php
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= $title = 'Requisitions Needing Approval';

//Set footer scripts
$scripts_footer[] = "js/requisition.js";

$TemplateObj->page_scripts_footer = $scripts_footer;

echo $TemplateObj->displayIrecruitTemplate('views/request/NeedsApproval');
?>