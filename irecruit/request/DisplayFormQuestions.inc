<?php
function displayExemptStatus($OrgID, $Exempt_Status) {
	$rtn = "";
	$rtn .= '<tr><td align="right" valign="top">Exempt Status:</td><td>';
	$rtn .= '<select name="Exempt_Status">';
	$rtn .= '<option value="N"';
	if ($Exempt_Status == "N") {
		$rtn .= ' selected';
	}
	$rtn .= '>No</option>';
	$rtn .= '<option value="Y"';
	if ($Exempt_Status == "Y") {
		$rtn .= ' selected';
	}
	$rtn .= '>Yes</option>';
	$rtn .= '</select>';
	$rtn .= '</td></tr>';
	
	return $rtn;
} // end function

function displayAutoForwardList($OrgID, $RequestID) {
	global $FormFeaturesObj, $RequisitionsObj;
	echo '<tr><td colspan="2">';
	echo '<strong>Who would you like this Requisition/JobID routed to:</strong>';
	echo '</td></tr>';
	
	//Get Auto Forward List
	$results   =   $FormFeaturesObj->getAutoForwardList($OrgID, "FirstName");
	
	echo '<input type="hidden" id="em" name="to[]" value="">';
	
	$i = 0;
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
			$Name    =   $row ['FirstName'] . ' ' . $row ['LastName'];
			$Email   =   $row ['EmailAddress'];
		
			echo '<tr>';
			echo '<td>&nbsp;&nbsp;&nbsp;' . $Name . '</td>';
			echo '<td>'.$Email.'</td>';
			echo '<td>';
		
			//Set where condition
			$where   =   array("OrgID = :OrgID", "RequestID = :RequestID", "EmailAddress = :EmailAddress");
			//Set parameters
			$params  =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":EmailAddress"=>$Email);
			//Get Requisition Forward Information
			$results =   $RequisitionsObj->getRequisitionForwardInfo("EmailAddress", $where, "", array($params));
			
			if(is_array($results['results'][0])) {
				list ( $em ) = array_values($results['results'][0]);
			}
			
			echo '<input type="checkbox" id="em" name="to[]" value="' . $Email . '"';
		
			if ($Email == $em) {
				echo ' checked';
			}
		
			echo '>';
			echo '</td>';
			echo '</tr>';
			$i ++;
		}
	}
	
} // end function

function displayEmailApprovers($OrgID, $EmailAddress, $i) {
	global $RequestObj;
	
	$params    =   array(":OrgID"=>$OrgID);
	$where     =   array("OrgID = :OrgID");
	$results   =   $RequestObj->getEmailApprovers("*", $where, "LastName, FirstName", array($params));
	
	$eapprove .=   '<tr><td align="right">Email Approver:</td><td colspan="2"><select name="approver-' . $i . '">';
	$eapprove .=   '<option value=""></option>';
	
	$startrange    =   '';
	$endrange      =   '';
	$cnt           =   0;
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
		
			$emailaddress = $row ['EmailAddress'];
			$firstname = $row ['FirstName'];
			$lastname = $row ['LastName'];
			$cnt ++;
		
			$eapprove .= '<option value="' . $emailaddress . '"';
		
			if (($emailaddress == $EmailAddress) || ($cnt == 0)) {
				$eapprove .= ' selected';
			}
		
			$eapprove .= '>' . $lastname . ', ' . $firstname . '</option>';
		}
	}
	
	$eapprove .= '</select>';
	$eapprove .= '</td></tr>';
	
	return $eapprove;
} // end function


function requestFiles($OrgID, $ReqestID) {
	$rtn = "here";
	
	return $rtn;
} // end function

function displayFilePurpose($OrgID, $i, $filepurpose) {
	global $RequestObj;
	
	$purpose   =   "";
	
	//set where condition
	$where     =   array("OrgID = :OrgID", "FilePurposeID = 99");
	//set parameters
	$params    =   array(":OrgID"=>$OrgID);
	//Get file purpose info
	$results   =   $RequestObj->getFilePurposeInfo("PurposeTitle", $where, "", array($params));

	$title     =   $results['results'][0]['PurposeTitle'];
	
	//set where condition
	$where     =   array("OrgID = :OrgID",  "FilePurposeID != 99");
	//get file purpose info
	$results   =   $RequestObj->getFilePurposeInfo("FilePurposeID, PurposeTitle", $where, "FilePurposeID", array($params));
	
	$purpose  .=    '<tr><td align="right">' . $title . ':</td><td colspan="2"><select name="filepurpose-' . $i . '">';
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
			$code        =   $row ['FilePurposeID'];
			$description =   $row ['PurposeTitle'];
		
			$purpose .= '<option value="' . $code . '"';
			if ($code == $filepurpose) {
				$purpose .= ' selected';
			}
			$purpose .= '>' . $description . '</option>';
		}
	}	
	
	$purpose .= '</select></td></tr>';
	
	return $purpose;
} // end function
?>