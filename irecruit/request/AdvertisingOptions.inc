<?php
if(is_array($AdvertisingOptions)) {
	$AOkeys = array_keys ( $AdvertisingOptions );
}

if (count($adv_options) > 0) {

	$advertising_options .= '<div class="row">';
    
	$advertising_options .= '<div class="col-lg-1 col-md-1 col-sm-1">';
	$advertising_options .= 'Select';
	$advertising_options .= '</div>';
	
	$advertising_options .= '<div class="col-lg-4 col-md-6 col-sm-6">';
	$advertising_options .= 'Job Posting Site';
	$advertising_options .= '</div>';
	
	$advertising_options .= '<div class="col-lg-3 col-md-3 col-sm-3">';
	$advertising_options .= 'Price';
	$advertising_options .= '</div>';
	
	$advertising_options .= '<div class="col-lg-2 col-md-2 col-sm-2">';
	$advertising_options .= 'Posting Period';
	$advertising_options .= '</div>';
	
	$advertising_options .= '</div>';
	
	$i = 1;
	
	if(is_array($adv_options)) {
    	for ($adr = 0; $adr < count($adv_options); $adr++) {
		
    	    $advertising_options .= '<div class="row">';
    	    
			$advertising_options .= '<div class="col-lg-1 col-md-1 col-sm-1">';
			$advertising_options .= '<input type="checkbox" name="AdvertisingOption-' . $i . '" value="' . $adv_options[$adr]['Site'] . '::' . $adv_options[$adr]['Price'] . '::' . $adv_options[$adr]['Period'] . '"';
			if (is_array($AOkeys) && in_array ( "AdvertisingOption-" . $i, $AOkeys )) {
				$advertising_options .= ' checked';
			}
			$advertising_options .= '>';
			$advertising_options .= '</div>';
		
            $advertising_options .= '<div class="col-lg-4 col-md-6 col-sm-6">';
			$advertising_options .= $adv_options[$adr]['Site'];
			$advertising_options .= '</div>';
		
            $advertising_options .= '<div class="col-lg-3 col-md-3 col-sm-3">';
			$advertising_options .= $adv_options[$adr]['Price'];
			$advertising_options .= '</div>';
		
            $advertising_options .= '<div class="col-lg-2 col-md-2 col-sm-2">';
			$advertising_options .= $adv_options[$adr]['Period'];
			$advertising_options .= '</div>';
		
			$advertising_options .= '</div>';
			
			$i ++;
		} // end foreach
	}	
	
	$advertising_options .= '<div class="row">';
	
	$advertising_options .= '<div class="col-lg-1 col-md-1 col-sm-1">';
	$advertising_options .= '<input type="checkbox" name="AdvertisingOption-Other" value="Y"';
	if ($AdvertisingOptions ['AdvertisingOption-Other'] == "Y") {
		$advertising_options .= ' checked';
	}
	$advertising_options .= '>';
	$advertising_options .= '</div>';
	
	$advertising_options .= '<div class="col-lg-4 col-md-6 col-sm-6">';
	$advertising_options .= 'Other - <input type="text" name="AdvertisingOption-Desc" value="' . $AdvertisingOptions ['AdvertisingOption-Desc'] . '" placeholder="Please Specify">';
	$advertising_options .= '</div>';
	
	$advertising_options .= '<div class="col-lg-3 col-md-3 col-sm-3">';
	$advertising_options .= '<input type="text" name="AdvertisingOption-Budget" value="' . $AdvertisingOptions ['AdvertisingOption-Budget'] . '" placeholder="Budget" size="10" maxlength="20">';
	$advertising_options .= '</div>';
	
	$advertising_options .= '<div class="col-lg-2 col-md-2 col-sm-2">';
	$advertising_options .= '<input type="text" name="AdvertisingOption-Period" value="' . $AdvertisingOptions ['AdvertisingOption-Period'] . '" placeholder="How Long" size="20" maxlength="60">';
	$advertising_options .= '</div>';
	
	$advertising_options .= '</div>';

	$advertising_options .= '<div class="row">';
	
	$advertising_options .= '<div class="col-lg-12 col-md-12 col-sm-12">';
	$advertising_options .= 'Special Instructions<br>';
	$advertising_options .= '<textarea cols="80" rows="6" name="AdvertisingOption-specialinstructions" class="AdvertisingOptionmceEditor">';
	$advertising_options .= $AdvertisingOptions ['AdvertisingOption-specialinstructions'];
	$advertising_options .= '</textarea>';
	$advertising_options .= '</div>';
	
	$advertising_options .= '</div>';
	
	
} // end raocnt
?>