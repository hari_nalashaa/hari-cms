<?php
//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Email Approvers Information
$results = $RequestObj->getEmailApprovers("*", $where, "", array($params));

$num_approvers = $results['count'];

if ($_REQUEST['process'] == "Y") {
	
	if ($OrgID != "" && $RequestID != "") {
		//Set condition
		$where = array("OrgID = :OrgID", "RequestID = :RequestID",  "DateApproval = ''");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
		
		//Delete Approvers Information
		$RequestObj->delApproversInfo('RequestApprovers', $where, array($params));
	}
	
	$i = 0;
	$ii = 0;
	$lst = "";
	$mlst = "";
	$emlst = "";
	$memlst = "";
	while ( $i < $num_approvers ) {
		$i ++;
		
		$approver = 'approver-' . $i;
		$authlevel = 'authlevel-' . $i;
		
		if ($_POST [$approver]) {
			$ii ++;
			
			//insert request approvers information
			$RequestObj->insRequestApprovers($OrgID, $RequestID, $_POST [$approver], $_POST [$authlevel]);
			
			if ($_POST [$authlevel] == '1') {
				sendApproveLink ( $OrgID, $RequestID, $_POST [$approver] );
				$emlst .= $_POST [$approver] . '<br>';
				$memlst .= $_POST [$approver] . '\\n';
			} // end if min
			
			$lst .= $_POST [$approver] . '<br>';
			$mlst .= $_POST [$approver] . '\\n';
		} // end if approver
	} // end num_approvers
	
	if ($ii > 0) {
		
		$Comments = 'Approvers Assigned<br>';
		$Comments .= $lst;
		if ($emlst) {
			$Comments .= "<br>Email notification for approval has been sent to:<br>" . $emlst;
		} // end emlst
		
		updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );
		
		$message = "Approvers Assigned.\\n";
		$message .= $mlst;
		
		if ($lst) {
			$message .= '\\nEmail notification for approval has been sent to:\\n' . $memlst;
		} // end lst
		
		$message .= "\\n";
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
	}
	
	$script = IRECRUIT_HOME . 'request/needsApproval.php';
	echo '<meta http-equiv="refresh" content="0;url=' . $script . '">';
	exit ();
} // end process

include IRECRUIT_DIR . 'request/DisplayFormQuestions.inc';

echo '<form method="post" action="' . IRECRUIT_HOME . 'request/assign.php">' . "\n";
echo '<br><table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered table-hover">' . "\n";

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);

//Get Request Aprovers Information
$results = $RequisitionsObj->getRequisitionInformation("*", $where, "", "", array($params));

$REQ = $results['results'][0];
$levelrequired = $REQ ['ApprovalLevelRequired'];

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get Request Aprovers Information
$results = $RequestObj->getRequestApproversInfo("*", $where, "AuthorityLevel, EmailAddress", array($params));

$i = 0;
$ii = 0;

if(is_array($results['results'])) {
	foreach($results['results'] as $RA) {
		$i ++;
		$ii ++;
	
		$EmailAddress = $RA ['EmailAddress'];
		$AuthorityLevel = $RA ['AuthorityLevel'];
	
		echo '<tr>';
		echo '<td>';
		echo displayEmailApprovers ( $OrgID, $EmailAddress, $i );
		echo '</td>';
		echo '<td align="right">Authority Level:</td><td><select name="authlevel-' . $i . '">' . authLevel ( $levelrequired, $AuthorityLevel ) . '</select><td></td>';
		echo '</tr>' . "\n";
	} // end foreach
}

while ( $i < $num_approvers ) {
	$i ++;
	
	echo '<tr>';
	echo '<td>';
	echo displayEmailApprovers ( $OrgID, '', $i );
	echo '</td>';
	echo '<td align="right">Authority Level:</td><td><select name="authlevel-' . $i . '">' . authLevel ( $levelrequired, '' ) . '</select><td></td>';
	echo '</tr>' . "\n";
}

if ($ii > 0) {
	$submit = 'Update Approvers';
} else {
	$submit = 'Enter Approvers';
}

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID", "DateApproval is not null");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$ri);
//Get Request Aprovers Information
$resultsIN2 = $RequestObj->getRequestApproversInfo("*", $where, "", array($params));


$approval_started = $resultsIN2['count'];

if ($approval_started > 0) {
	
	$onclickex = "onclick=\"javascript:window.open('" . IRECRUIT_HOME . "request/approverStatus.php?RequestID=" . $RequestID;
	if ($AccessCode != "") {
		$onclickex .= "&k=" . $AccessCode;
	}
	$onclickex .= "','_blank','location=yes,toolbar=no,height=400,width=900,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";
	
	echo '<tr><td colspan="100%" align="center" valign="middle" height="80">Approvers are locked. The Approval process has begun.<br><a href="#" ' . $onclickex . '>check approval status</a></td></tr>';
} else {
	
	echo '<tr><td colspan="100%" align="center"><input type="submit" value="' . $submit . '"></td></tr>' . "\n";
}
echo '</table>' . "\n";
echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
echo '<input type="hidden" name="action" value="' . $action . '">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="enteraction" value="' . $submit . '">';
echo '</form>' . "\n";

// / *** FUNCTIONS *** ///
function authLevel($levelrequired, $AuthorityLevel) {
	$rtn = "";
	
	$rtn .= '<option value=""></option>';
	$i = 1;
	while ( $i <= $levelrequired ) {
		$rtn .= '<option value="' . $i . '"';
		if ($i == $AuthorityLevel) {
			$rtn .= ' selected';
		}
		$rtn .= '>' . $i . '</option>';
		$i ++;
	}
	
	return $rtn;
} // end function
?>
