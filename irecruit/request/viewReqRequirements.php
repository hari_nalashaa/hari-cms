<?php
$PAGE_TYPE="PopUp";
require_once '../Configuration.inc';

$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->k = $action = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';
$TemplateObj->title = $title = 'Requisition Requirements';

if ($feature['RequisitionRequest'] != "Y") { die (include IRECRUIT_DIR . 'irecruit_permissions.err'); }

echo $TemplateObj->displayIrecruitTemplate('views/request/ViewReqRequirements');
?>