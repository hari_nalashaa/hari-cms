<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

//Set page title
$TemplateObj->title	= $title = "Requisition Override";

//set mostly using global variables
$TemplateObj->RequestID = $RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->k = $k = isset($_REQUEST['k']) ? $_REQUEST['k'] : '';

//Set OverrideRequisition
echo $TemplateObj->displayIrecruitTemplate('views/request/OverrideRequisition');
?>