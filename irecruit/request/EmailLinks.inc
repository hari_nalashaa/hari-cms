<?php
// *** FUNCTIONS *** //
function sendEditLink($OrgID, $RequestID) {
	global $RequisitionsObj, $OrganizationsObj, $OrganizationDetailsObj, $PHPMailerObj;
	
	//set where condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	//get requisitions information
	$results = $RequisitionsObj->getRequisitionInformation("RequesterEmail", $where, "", "", array($params));
	$REQUEST = $results['results'][0];
	
	$to = $REQUEST ['RequesterEmail'];
	
	$subject = "iRecruit Requisition Request";
	
	$site = IRECRUIT_HOME . 'publicRequest.php?OrgID=' . $OrgID . '&RequestID=' . $RequestID . '&action=view';
	$message = <<<END
You have submitted a requisition request to the iRecruit system. The following link
will allow you to view, edit, and monitor its progress.<br>
<br>
<a href="$site">Monitor Request</a>
<br>
END;
	
    //Get Email and EmailVerified
	$OE        =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
	//Get OrganiationName
	$OrgName   =   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);

    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
	
	// Set who the message is to be sent to
	$PHPMailerObj->addAddress ( $to );
	// Set the subject line
	$PHPMailerObj->Subject = $subject;
	// convert HTML into a basic plain-text alternative body
	$PHPMailerObj->msgHTML ( $message );
	// Content Type Is HTML
	$PHPMailerObj->ContentType = 'text/html';
	
	if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
		// Set who the message is to be sent from
		$PHPMailerObj->setFrom ( $OE ['Email'], $OrgName);
		// Set an alternative reply-to address
		$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrgName );
	}
	
	if ($to) {
		if (DEVELOPMENT != "Y") {
			// send php mail
           $PHPMailerObj->send ();
		}
	}
} // end function
function sendApproveLink($OrgID, $RequestID, $EmailAddress) {
	global $OrganizationsObj, $IrecruitUsersObj, $OrganizationDetailsObj, $PHPMailerObj;
	
	$site = IRECRUIT_HOME . 'request/approveRequest.php?OrgID=' . $OrgID . '&RequestID=' . $RequestID . '&EmailAddress=' . $EmailAddress;
	
	$to = $EmailAddress;
	$subject = "You have a New Requisition Request";
	$message = <<<END
Please review the following Requisition Request from iRecruit:<br>
<br>
<a href="$site">Review Requisition Request</a>
<br><br>
Thank you,<br>

iRecruit Support<br>
http://help.irecruit-us.com
	
END;

    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
		
	// Set who the message is to be sent to
	$PHPMailerObj->addAddress ( $to );
	// Set the subject line
	$PHPMailerObj->Subject = $subject;
	// convert HTML into a basic plain-text alternative body
	$PHPMailerObj->msgHTML ( $message );
	// Content Type Is HTML
	$PHPMailerObj->ContentType = 'text/html';
	
	//Get Email and EmailVerified
	$OE    =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
	//Get OrganiationName
	$OrgName   =   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);
	
	if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
		// Set who the message is to be sent from
		$PHPMailerObj->setFrom ( $OE ['Email'], $OrgName);
		// Set an alternative reply-to address
		$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrgName );
	}


	if ($to) {
		if (DEVELOPMENT != "Y") {
			// send php mail
            $PHPMailerObj->send();
		}
	}
	
} // end function
function sendAdminAlert($OrgID, $RequestID, $Type) {
	global $IrecruitUsersObj, $OrganizationsObj, $RequestObj, $OrganizationDetailsObj, $PHPMailerObj;
	
    //Clear properties
    $PHPMailerObj->clearCustomProperties();
    $PHPMailerObj->clearCustomHeaders();
		
	//Get notification config information
	$notification_config_info      =   $RequestObj->getRequestNotificationConfigInfo($OrgID, '');
	$admin_emails_list_selected    =   json_decode($notification_config_info['EmailsList'], true);
	
	foreach ($admin_emails_list_selected as $notification_email) {
		// Set who the message is to be sent to
		$PHPMailerObj->addAddress ( $notification_email );
	}
	
	//Get Email and EmailVerified
	$OE    =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
	//Get OrganiationName
	$OrgName   =   $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);	
	
	if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
		// Set who the message is to be sent from
		$PHPMailerObj->setFrom ( $OE ['Email'], $OrgName);
		// Set an alternative reply-to address
		$PHPMailerObj->addReplyTo ( $OE ['Email'], $OrgName);
	}
	
	if ($Type == "Start Request") {
		$subject = "iRecruit Requisition Request Started";
		$message = "A new requisition request has been started.<br>";
		$message .= "You can verify status by loging in here:<br>";
		$message .= IRECRUIT_HOME . "request/requestsList.php<br><br>";
	}

	// Set the subject line
	$PHPMailerObj->Subject = $subject;
	// convert HTML into a basic plain-text alternative body
	$PHPMailerObj->msgHTML ( $message );
	// Content Type Is HTML
	$PHPMailerObj->ContentType = 'text/html';
	
	//Send email to the configured emails
	if (count($admin_emails_list_selected) > 0) {
		if (DEVELOPMENT != "Y") {
			$PHPMailerObj->send ();
		}
	}
} // end function
?>
