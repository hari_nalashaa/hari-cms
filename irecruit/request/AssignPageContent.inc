<?php
echo '<table border="0" cellspacing="0" cellpadding="0" width="100%" class="table table-striped table-bordered table-hover">';
echo '<tr><td class="title" valign="top">';
echo $title;
echo '</td><td align="right">';

if ($permit ['RequisitionRequest'] == 0) {
	$script = 'request/needsApproval.php?menu=3';
} else {
	$script = 'request/needsApproval.php?menu=3';
}

echo '<a href="' . IRECRUIT_HOME . $script . '"><b style="FONT-SIZE:8pt;COLOR:#000000">Go Back</b><img src="' . IRECRUIT_HOME . 'images/icons/arrow_undo.png" border="0" title="Back" style="margin:0px 0px -4px 3px;"></a>';
echo '</td><tr>';
echo '</table>';

if ($_REQUEST ['enteraction'] == "Enter Request") {
	
	sendEditLink ( $OrgID, $RequestID );
	
	if ($permit ['RequisitionRequest'] == 0) {
		echo '<br><br>Your request has been entered.';
	} else {
		include IRECRUIT_DIR . 'request/ApproversRequestForm.inc';
	}
} else if ($_REQUEST ['enteraction'] == "Update Request") {
	
	include IRECRUIT_DIR . 'request/ViewRequest.inc';
	echo displayRequestInfo ( $OrgID, $RequestID, $USERID, '' );
} else if ($_REQUEST ['enteraction'] == "Assign Request") {
	
	include IRECRUIT_DIR . 'request/ApproversRequestForm.inc';
} else if ($action == "view") {
	include IRECRUIT_DIR . 'request/ViewRequest.inc';
	echo displayRequestInfo ( $OrgID, $RequestID, $USERID, '' );
} else if ($action == "assign") {
	
	include IRECRUIT_DIR . 'request/ApproversRequestForm.inc';
} else if ($action == "reassign") {
	
	if ($RequestID) {
		include IRECRUIT_DIR . 'request/ApproversRequestForm.inc';
	}
} else if ($action == "copy") {
	include IRECRUIT_DIR . 'request/CreateEditRequest.inc';
}
?>