<?php
require_once '../Configuration.inc';

if (($OrgID) && ($_REQUEST['updateid'])) {
	
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":UpdateID"=>$_REQUEST['updateid']);
	//Insert Request Archive Information
	$RequestObj->insRequestArchive(array($params));
	
	//Set where condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID", "UpdateID = :UpdateID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":UpdateID"=>$_REQUEST['updateid']);
	//Delete Request History
	$RequestObj->delRequestHistory($where, array($params));
}

header ( 'Location: ' . IRECRUIT_HOME . 'request/assign.php?RequestID=' . $_REQUEST['RequestID'] . '&action=edit' );
exit ();
?>