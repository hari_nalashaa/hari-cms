<?php 
require_once '../Configuration.inc';

$RequestID = $_REQUEST['RequestID'];
$ApproversLevel = $_REQUEST['ApproversLevel'];

$set_info = array("ApprovalLevelRequired = :ApprovalLevelRequired");
$params = array(":ApprovalLevelRequired"=>$ApproversLevel, ":RequestID"=>$RequestID, ":OrgID"=>$OrgID);
$where_info = array("OrgID = :OrgID", "RequestID = :RequestID");
$RequisitionsObj->updRequisitionsInfo("Requisitions", $set_info, $where_info, array($params));

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Email Approvers Information
$results = $RequestObj->getEmailApprovers("*", $where, "", array($params));

$num_approvers = $results['count'];

include IRECRUIT_DIR . 'request/DisplayFormQuestions.inc';

echo '<form method="post" name="frmApproversRequestForm" id="frmApproversRequestForm">';
echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered table-hover">';

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);

//Get Request Aprovers Information
$results = $RequisitionsObj->getRequisitionInformation("*", $where, "", "", array($params));

$REQ = $results['results'][0];
$levelrequired = $REQ ['ApprovalLevelRequired'];

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get Request Aprovers Information
$results = $RequestObj->getRequestApproversInfo("*", $where, "AuthorityLevel, EmailAddress", array($params));

$i = 0;

if((is_array($results['results'])) && ($levelrequired > 0)) {
	foreach($results['results'] as $RA) {
		$i ++;
		if ($i <= $levelrequired)  { 
		$EmailAddress = $RA ['EmailAddress'];
		$AuthorityLevel = $RA ['AuthorityLevel'];

		echo '<tr>';
		echo '<td width="10%">';
		echo displayEmailApprovers ( $OrgID, $EmailAddress, $i );
		echo '</td>';
		echo '<td align="right">Authority Level:</td><td><select name="authlevel-' . $i . '">' . authLevel ( $levelrequired, $AuthorityLevel ) . '</select><td></td>';
		echo '</tr>';
		} // end if $i <
	} // end foreach
}

while ( $i < $levelrequired) {
	$i ++;

	echo '<tr>';
	echo '<td>';
	echo displayEmailApprovers ( $OrgID, '', $i );
	echo '</td>';
	echo '<td align="right" width="20%">Authority Level:</td><td><select name="authlevel-' . $i . '">' . authLevel ( $levelrequired, '' ) . '</select><td></td>';
	echo '</tr>';
}

$submit = 'Notify Approvers';

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID", "DateApproval is not null");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$ri);
//Get Request Aprovers Information
$resultsIN2 = $RequestObj->getRequestApproversInfo("*", $where, "", array($params));


$approval_started = $resultsIN2['count'];

if ($approval_started > 0) {

	$onclickex = "onclick=\"javascript:window.open('" . IRECRUIT_HOME . "request/approverStatus.php?RequestID=" . $RequestID;
	if ($AccessCode != "") {
		$onclickex .= "&k=" . $AccessCode;
	}
	$onclickex .= "','_blank','location=yes,toolbar=no,height=400,width=900,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";

	echo '<tr><td colspan="100%" align="center" valign="middle" height="80">Approvers are locked. The Approval process has begun.<br><a href="#" ' . $onclickex . '>check approval status</a></td></tr>';
} else {

	echo '<tr><td colspan="100%" align="center"><input type="submit" value="' . $submit . '" class="btn btn-primary"></td></tr>';
}

echo '<tr>
		<td colspan="100%" align="left">
		Approvers are notified by email in order of their Authority Level. 
		When approved by #1, the approval email goes to the second approver. 
		When the approver with the highest Authority Level approves the position, 
		it will go live based on the date range that was set on the requisition.
		</td>
	  </tr>';  

echo '</table>';
echo '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="enteraction" value="' . $submit . '">';
echo '</form>';

// / *** FUNCTIONS *** ///
function authLevel($levelrequired, $AuthorityLevel) {
	$rtn = "";

	$rtn .= '<option value=""></option>';
	$i = 1;
	while ( $i <= $levelrequired ) {
		$rtn .= '<option value="' . $i . '"';
		if ($i == $AuthorityLevel) {
			$rtn .= ' selected';
		}
		$rtn .= '>' . $i . '</option>';
		$i ++;
	}

	return $rtn;
} // end function
?>
