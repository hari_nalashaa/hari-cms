<?php
require_once '../Configuration.inc';

$TemplateObj->title = $title = 'Process Requests';

//Set footer scripts
$scripts_footer[] = "js/requisition.js";

$TemplateObj->page_scripts_footer = $scripts_footer;

if(isset($_GET['sort']) && $_GET['sort'] != "") {
	if(isset($_GET['sort_type']) && ($_GET['sort_type'] == "DESC" || $_GET['sort_type'] == "ASC")) {
		$sort_type = $_GET['sort_type'];
	}
	if(isset($_GET['sort_type']) && $_GET['sort_type'] != "DESC" && $_GET['sort_type'] != "ASC") {
		$sort_type = "DESC";
	}
	if($_GET['sort'] == "title") {
		$order_by = "Title $sort_type";
	}
	if($_GET['sort'] == "email") {
		$order_by = "RequesterEmail $sort_type";
	}
	if($_GET['sort'] == "date") {
		$order_by = "DateEntered $sort_type";
	}
} else {
	$TemplateObj->sort_type = $sort_type = "DESC";
	$order_by = "PostDate DESC";
}

if(isset($_GET['sort_type']) && $_GET['sort_type'] == "DESC") {
	$TemplateObj->sort_type = $sort_type = "ASC";
}
if(isset($_GET['sort_type']) && $_GET['sort_type'] == "ASC") {
	$TemplateObj->sort_type = $sort_type = "DESC";
}

//Get Requests
$where_info = array("OrgID = :OrgID", "Active = 'R'");
$params_info = array(":OrgID"=>$OrgID);
$requests_list = $RequisitionsObj->getRequisitionInformation("*", $where_info, "", $order_by, array($params_info));

//Template Object
$TemplateObj->requests_list = $requests_list;

echo $TemplateObj->displayIrecruitTemplate('views/request/RequestsList');
?>
