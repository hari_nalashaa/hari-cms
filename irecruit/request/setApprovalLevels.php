<?php 
require_once '../Configuration.inc';
require_once IRECRUIT_DIR . 'request/EmailLinks.inc';
require_once IRECRUIT_DIR . 'request/UpdateHistory.inc';

$TemplateObj->title = $title = "Assign Approvers";

$RequestID = $_REQUEST['RequestID'];

// set where condition
$where = array ("OrgID = :OrgID");
// set parameters
$params = array (":OrgID" => $OrgID);
//Email Approvers Info
$results = $RequestObj->getEmailApprovers("*", $where, "", array($params));

$TemplateObj->cnt_approvers = $cnt_approvers = $results['count'];

if ($_REQUEST['process'] == "Y") {

	if ($OrgID != "" && $RequestID != "") {
		//Set condition
		$where = array("OrgID = :OrgID", "RequestID = :RequestID",  "DateApproval = '0000-00-00 00:00:00'");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);

		//Delete Approvers Information
		$RequestObj->delApproversInfo('RequestApprovers', $where, array($params));
	}

	$i = 0;
	$ii = 0;
	$lst = "";
	$mlst = "";
	$emlst = "";
	$memlst = "";
	while ( $i < $cnt_approvers ) {
		$i ++;

		$approver = 'approver-' . $i;
		$authlevel = 'authlevel-' . $i;

		if ($_POST [$approver]) {
			$ii ++;
				
			//insert request approvers information
			$RequestObj->insRequestApprovers($OrgID, $RequestID, $_POST [$approver], $_POST [$authlevel]);

			if ($_POST [$authlevel] == '1') {
				sendApproveLink ( $OrgID, $RequestID, $_POST [$approver] );
				$emlst .= $_POST [$approver] . '<br>';
				$memlst .= $_POST [$approver] . '\\n';
			}	
			$lst .= $_POST [$approver] . '<br>';
			$mlst .= $_POST [$approver] . '\\n';
		} // end if approver
	} // end num_approvers

	if ($ii > 0) {

		$Comments = 'Approvers Assigned<br>';
		$Comments .= $lst;
		if ($emlst) {
			$Comments .= "<br>Email notification for approval has been sent to:<br>" . $emlst;
		} // end emlst

		updateRequestHistory ( $OrgID, $RequestID, $USERID, $Comments );

		$message = "Approvers Assigned.\\n";
		$message .= $mlst;

		if ($lst) {
			$message .= '\\nEmail notification for approval has been sent to:\\n' . $memlst;
		} // end lst

		$message .= "\\n";

		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
	}

	$script = IRECRUIT_HOME . 'request/needsApproval.php';
	
	echo '<meta http-equiv="refresh" content="0;url=' . $script . '">';
	exit ();
} // end process

//Set condition
$where = array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
//Get Request Aprovers Information
$results = $RequisitionsObj->getRequisitionInformation("*", $where, "", "", array($params));
$TemplateObj->levelrequired = $levelrequired = $results['results'][0]['ApprovalLevelRequired'];


echo $TemplateObj->displayIrecruitTemplate('views/request/SetApprovalLevels');
?>
