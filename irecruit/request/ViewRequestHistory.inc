<?php
// / *** FUNCTIONS *** ///
function displayHistoryComments($OrgID, $RequestID) {
	global $permit, $action, $RequestObj;
	
	$rtn = '';
	
	$rtn .= '<table border="0" cellspacing="2" cellpadding="5" class="table table-striped table-bordered table-hover">';
	
	$rtn .= '<tr>';
	$rtn .= '<td align="left"><b>Date</b></td>';
	$rtn .= '<td align="left" width="80"><b>UserID</b></td>';
	$rtn .= '<td align="left" width="400"><b>Comments</b></td>';
	if (($action != "view") && ($permit ['Admin'] == 1)) {
		$rtn .= '<td><b>Delete</b></td>';
	} // end action=view and permit Admin
	
	$rtn .= '</tr>';
	
	$rowcolor = "#eeeeee";
	
	$cnt = 0;

	//Set condition
	$where = array("OrgID = :OrgID", "RequestID = :RequestID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
	//Set columns
	$columns = "*, date_format(Date,'%Y-%m-%d %H:%i EST') D";
	
	//Get Request History
	$results = $RequestObj->getRequestHistory($columns, $where, 'Date ASC', array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $HIS) {
			$cnt ++;
		
			$rtn .= '<tr bgcolor="' . $rowcolor . '">';
			$rtn .= '<td valign="top">' . $HIS ['D'] . '</td>';
			$rtn .= '<td valign="top">' . $HIS ['UserID'] . '</td>';
			$rtn .= '<td valign="top">' . $HIS ['Comments'] . '</td>';
		
			if (($action != "view") && ($permit ['Admin'] == 1)) {
				$rtn .= '<td align="left"><a href="' . IRECRUIT_HOME . 'request/deleteComment.php?RequestID=' . $RequestID . '&updateid=' . $HIS ['UpdateID'] . '" onclick="return confirm(\'Are you sure you want to delete this comment?\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete" style="margin:0px 3px -4px 0px;"></a></td>';
			} // end action=view and permit Admin
		
			$rtn .= '</tr>';
		
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach
	}
	
	
	if ($cnt == 0) {
		
		$rtn .= '<tr><td height="80" width="500" colspan="100%" align="center">There are no comments to display.</td></tr>';
	}
	
	$rtn .= '</table>';
	
	return $rtn;
} // end function
?>