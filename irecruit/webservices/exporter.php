<?php
require_once "../irecruitdb.inc";
require_once VENDOR_DIR . "econea/nusoap/src/nusoap.php";

//ini_set('memory_limit', '-1'); 

$nusoap_base_obj = new nusoap_base();
$nusoap_base_obj->setGlobalDebugLevel ( 0 );

function login($token) {
	$OrgID         =   "";
	
	// Set table name
	$table_name    =   "Users U, ApplicationPermissions AP";
	// Set where condition
	$where         =   array (
                			"U.UserID        =   AP.UserID",
                			"U.AccessCode    =   :AccessCode",
                			"AP.Active       =   1",
                    	);
	// Set parameters
	$params        =   array(":AccessCode" =>  $token);
	// Get ApplicationPermissions Inforamation
	$results       =   G::Obj('IrecruitApplicationFeatures')->getApplicationPermissionsInfo ( $table_name, "U.OrgID", $where, "", array ($params) );
	
	$OrgID         =   $results ['results'] [0] ['OrgID'];
	
	return $OrgID;
} // end function

function getAccessToken($user, $pass) {

	$AUTH  =   G::Obj('IrecruitUsers')->validateUserLogin($user, $pass);
	$OrgID =   login ($AUTH['AccessCode']);

    if ($OrgID != "") {
        return $AUTH['AccessCode'];
    }

} // end function

function getDBConfig($token) {
	
	list($t,$ext) = explode(":",$token);
	
	$OrgID = login ( $t);
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		if ($ext != "") {
		   $dborg = $OrgID . $ext;
		} else {
		   $dborg = $OrgID;
		}
	
		
		// Set columns
		$columns  =   "ExportType, DatabaseType, ConnectionString, DatabaseName, UserName, Verification, Uppercase, FileLocation, PayrollDBNames, SyncConnectionString, SyncDatabaseName, SyncUserName, SyncVerification";
		// Set where condition
		$where    =   array("OrgID = :OrgID");
		// Set parameters
		$params   =   array(":OrgID" => $dborg);
		// Get DataManager Information
		$results_data_manager =   G::Obj('ExporterDataManager')->getDataManagerInfo ( $columns, $where, "", array ($params) );
		
		$PDB      =   "";
		if (is_array ( $results_data_manager ['results'] [0] )) {
			list ( $exporttype, $dbtype, $connectionstring, $databasename, $username, $verification, $uppercase, $filelocation, $payrolldbnames, $syncconnectionstring, $syncdatabasename, $syncusername, $syncverification ) = array_values ( $results_data_manager ['results'] [0] );
			$PDB = unserialize ( $payrolldbnames );
		}

		if (($syncconnectionstring == "") && 
		    ($syncdatabasename == "") && 
		    ($syncusername == "") && 
		    ($syncverification == "")) {

			$syncconnectionstring = $connectionstring;
			$syncdatabasename = $databasename;
			$syncusername = $username;
			$syncverification = $verification;

		}
		
		// Set table name
		$table_name   =   "Users U, ApplicationPermissions AP";
		// Set where condition
		$where        =   array (
                				"AP.UserID      =   U.UserID",
                				"U.AccessCode   =   :AccessCode",
                				"AP.Active      =   1",
                				"U.Role IN ('', 'master_admin')" 
                    		);
		// Set parameters
		$params       =   array(":AccessCode" =>  $t);
		// Set columns
		$columns      =   "AP.UserID, AP.Active, U.Role";
		// Get ApplicationPermissions Inforamation
		$results      =   G::Obj('IrecruitApplicationFeatures')->getApplicationPermissionsInfo ( $table_name, $columns, $where, "", array ($params) );
		$authcnt      =   $results ['count'];

		$FEATURES     =   G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );
        $datamanagertype =   $FEATURES['DataManagerType'];
		
		if ($authcnt == 0) {
			$auth = "no";
		} else {
			$auth = "yes";
		}
	} // end OrgID
	
	return array (
			"orgid"                  =>  "$OrgID",
			"exporttype"             =>  "$datamanagertype",
			"dbtype"                 =>  "$dbtype",
			"connectionstring"       =>  "$connectionstring",
			"databasename"           =>  "$databasename",
			"username"               =>  "$username",
			"verification"           =>  "$verification",
			"syncconnectionstring"   =>  "$syncconnectionstring",
			"syncdatabasename"       =>  "$syncdatabasename",
			"syncusername"           =>  "$syncusername",
			"syncverification"       =>  "$syncverification",
			"uppercase"              =>  "$uppercase",
			"filelocation"           =>  "$filelocation",
			"auth"                   =>  "$auth",
			"payrolldbs"             =>  json_encode ( $PDB )
	);
} // end function

function configureOnboardQuestions($token, $QuestionID, $question, $values) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$total = count ( $values );

		// sort so descriptions are alphabetical
		$tmp = Array();

        foreach($values as &$ma)
           $tmp[] = &$ma["Description"];
        array_multisort($tmp, SORT_ASC|SORT_NATURAL|SORT_FLAG_CASE, $values);
		
		if ($total > 0) {
			$rtn = "";
			foreach ( $values as $key => $value ) {
			    if ($value ['Code'] != "***") {
                    	  	if($OrgID == "I20140912") { // remove enterprise for CRF
				  if (substr($value['Description'],0,3) != "***") {
                          	    $rtn .= $value ['Code'] . ":" . $value ['Description'] . "::";
                        	  }
                    	  	} else {
				  if(($OrgID == "I20200305") && ($QuestionID == "Sage_JobCode")) {
                      	 	    $rtn .= $value ['Code'] . ":" . $value ['Code'] . "-" . $value ['Description'] . "::";
				  } else {
                      	 	    $rtn .= $value ['Code'] . ":" . $value ['Description'] . "::";
				  }
                    	  	}

			    } // end Enterprise code strip
			} // end foreach
			$rtn = substr ( $rtn, 0, - 2 );
		} // end total
	
		$Active="Y";
		$Required="Y";
	
		if ($rtn == ":") {
			$rtn = "";
			$Active="";
			$Required="";
		}
		
		if ($QuestionID) {
			
			// Set where condition
			$where = array (
					"OrgID = :OrgID",
					"QuestionID = :QuestionID" 
			);
			// Set update information
			$set_info = array (
					"Question = :Question",
					"value = :value",
					"SageLock = 'Y'",
					"Active = :active",
					"Required = :required" 
			);
			// Set parameters
			$params = array (
					":OrgID" => $OrgID,
					":QuestionID" => $QuestionID,
					":Question" => $question,
					":value" => $rtn,
					":active" => $Active,
					":required" => $Required
			);
			
			// Update OnboardQuestions Information
			G::Obj('FormQuestions')->updQuestionsInfo ( 'OnboardQuestions', $set_info, $where, array ( $params ) );

		} // end QuestionID

		if ($OrgID == "I20140912") {

            $FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );

		    $RequisitionFormID =   G::Obj('RequisitionForms')->getDefaultRequisitionFormID($OrgID);  

		    $columns            =   "QuestionID, Question, value, defaultValue";
		    // Set where condition
		    $where              =   array (
                                        "OrgID           =   :OrgID",
                                        "OnboardFormID   =   :OnboardFormID",
                                        "QuestionID in ('Sage_JobCode','Sage_Rate','Sage_OrgLevel1','Sage_OrgLevel2', 'Sage_Supervisor', 'Sage_OrgLevel3','COB_WCgroup', 'COB_WCcode')"
	                                );
		    // Set parameters
		    $params             =   array (":OrgID" => $OrgID, ":OnboardFormID"=>$FEATURES['DataManagerType']);
		    $results            =   G::Obj('OnboardQuestions')->getOnboardQuestionsInformation($columns, $where, "", array ($params) );
    
 		    if (is_array ( $results ['results'] )) {
	                foreach ( $results ['results'] as $OQN ) {

			  $where_info =       array("OrgID = :OrgID", "QuestionID = :QuestionID", "RequisitionFormID = :RequisitionFormID");
			  $set_info   =       array("Question = :Question", "value = :value","defaultValue = :defaultValue","LastModifiedDateTime = NOW()");
			  $params     =       array (':OrgID'=>$OrgID, ':QuestionID'=>$OQN['QuestionID'], ":RequisitionFormID"=>$RequisitionFormID,':Question'=>$OQN['Question'],':value'=>$OQN['value'],':defaultValue'=>$OQN['defaultValue']);
			  $res_info   =       G::Obj('RequisitionQuestions')->updRequisitionQuestions($set_info, $where_info, array($params));

	                }
	            }


		}


	} // end OrgID
	
	return "Updated Entry: " . $QuestionID . " - " . $question;
} // end function

function createUserAccount($token,$userid,$password,$email,$role,$first,$last,$addr1,$addr2,$city,$state,$zip,$phone,$title) {
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return array (
			"Status"             =>  "Fail",
			"Message"            =>  "You don't have authorization to use this application.",
			"AccessCode"         =>  ""
		);
	} else {

        $ERROR = "";

        $ERROR .= G::Obj('Validate')->validate_userid ( $userid );
        $ERROR .= G::Obj('Validate')->validate_verification ( $password, '', 'N' );

        if (strlen($first) <= 3) {
            $ERROR .= "Please enter a First Name longer than 3 characters." . "\n";
        }

        if (strlen ( $last ) <= 3) {
            $ERROR .= "Please enter a Last Name longer than 3 characters." . "\n";
        }

        $ERROR .= G::Obj('Validate')->validate_email_address ( $email );

        // set where condition
        $where  =   array (
                        "OrgID != :OrgID",
                        "UserID = :UserID"
                    );
        // set parameters
        $params =   array (
                        ":OrgID" => $OrgID,
                        ":UserID" => $userid
                    );
        // Get UserInformation
        $results = G::Obj('IrecruitUsers')->getUserInformation ( "*", $where, "", array ($params) );
        $usrcnt = $results ['count'];

        if ($usrcnt > 0) {
            $ERROR .= "The user name already exists." . "\n";
        }

        if (!$ERROR) {

    		// Users Information
            $users_info = array (
                            "UserID"            =>  $userid,
                            "Verification"      =>  password_hash($password, PASSWORD_DEFAULT),
                            "OrgID"             =>  $OrgID,
                            "Role"              =>  $role,
                            "AccessCode"        =>  uniqid (),
                            "FirstName"         =>  $first,
                            "LastName"          =>  $last,
                            "Title"             =>  $title,
                            "Address1"          =>  $addr1,
                            "Address2"          =>  $addr2,
                            "City"              =>  $city,
                            "State"             =>  $state,
                            "ZipCode"           =>  $zip,
                            "Phone"             =>  $phone,
                            "EmailAddress"      =>  $email,
                            "ApplicationView"   =>  'M'
            );
    
            //Insert Users Information
	    G::Obj('IrecruitUsers')->insUsers ( $users_info );
    
            //Insert Application Permissions Information
            $info   =   array (
                            "UserID"    =>  $userid,
                            "Active"    =>  "1"
                        );
	    G::Obj('IrecruitApplicationFeatures')->insApplicationPermissions ( $info );
    
            $where      =   array("OrgID = :OrgID", "UserID = :UserID");
            $params     =   array(":OrgID"=>$OrgID, ":UserID"=>$userid);
            $results    =   G::Obj('IrecruitUsers')->getUserInformation("AccessCode", $where, "", array($params));
	    $Status	=   "Success";
	    $Message	=   "";
	    $AccessCode =   $results['results'][0]['AccessCode'];


        } else { // else if ERROR
        
            $ERROR =   preg_replace ( '/\\\\n/', "\n", $ERROR);
            $ERROR =   preg_replace ( '/\\n/', " ", $ERROR);
	    $Status	=   "Fail";
	    $Message	=   $ERROR;
	    $AccessCode =   "";
        
        } // end if ERROR

	return array (
			"Status"             =>  "$Status",
			"Message"            =>  "$Message",
			"AccessCode"         =>  "$AccessCode"
	);

	} // end OrgID

}

function getUsers($token) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$USERS    =   array ();
		$USERS[0] =   array (
                            "UserID"        =>  "",
                            "FirstName"     =>  "",
                            "LastName"      =>  "",
                            "EmailAddress"  =>  "",
                            "AccessCode"    =>  "" 
                        );
		
		// Get AccessCode Information
		$AccessCodeInfo   =   G::Obj('IrecruitUsers')->getUserInfoByAccessCode ( $token, "Role" );
		$Role             =   $AccessCodeInfo ["Role"];
		
		if ($Role == "master_admin") {
			
			// Set table name
            $table_name  =  "Users U, ApplicationPermissions AP";
			// Set where condition
			$where       =   array (
                                    "U.UserID   =   AP.UserID",
                                    "U.OrgID    =   :OrgID",
                                    "U.AccessCode != :AccessCode",
                                    "AP.Active  =   1" 
                                );
			// Set parameters
			$params      =   array (
                                    ":OrgID"        =>  $OrgID,
                                    ":AccessCode"   =>  $token 
                                );
			// Set columns
			$columns     =   "U.UserID, U.FirstName, U.LastName, U.EmailAddress, U.AccessCode";
			// Get ApplicationPermissions Inforamation
			$results     =   G::Obj('IrecruitApplicationFeatures')->getApplicationPermissionsInfo ( $table_name, $columns, $where, "U.UserID", array ($params) );

			$i = 0;
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $USR ) {
					
                    $UserID         =   $USR ['UserID'];
                    $FirstName      =   $USR ['FirstName'];
                    $LastName       =   $USR ['LastName'];
                    $EmailAddress   =   $USR ['EmailAddress'];
                    $AccessCode     =   $USR ['AccessCode'];
					
					$USERS [$i] = array (
                        "UserID"        =>  "$UserID",
                        "FirstName"     =>  "$FirstName",
                        "LastName"      =>  "$LastName",
                        "EmailAddress"  =>  "$EmailAddress",
                        "AccessCode"    =>  "$AccessCode" 
					);
					$i ++;
				} // end foreach
			}
		} // end Role
		
		return $USERS;
	} // end OrgID
} // end function

function getOnboardList($token) {
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$APPLICANTS       =   array ();
		$APPLICANTS [0]   =   array (
                                    "ApplicationID"     =>  "",
                                    "RequestID"         =>  "",
                                    "ApplicantSortName" =>  "" 
                                );
		// Set columns
		$columns  =   "ApplicationID, RequestID";
		// Set where condition
		$where    =   array (
                        	"OrgID = :OrgID",
                        	"Status = 'ready'" 
                        );
		// Set parameters
		$params   =   array (":OrgID" => $OrgID);
		// Get Applications Information
		$results  =   G::Obj('OnboardApplications')->getOnboardApplications("*", $where, "", "ApplicationID", array($params));
		
		$i = 0;
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $OA ) {
			    
				$ApplicationID      =   $OA ['ApplicationID'];
				$RequestID          =   $OA ['RequestID'];
				$ApplicantSortName  =   G::Obj('ApplicantDetails')->getApplicantSortName($OrgID, $ApplicationID, $RequestID);
				
				$APPLICANTS [$i]    =   array (
                                            "ApplicationID"     =>  $ApplicationID,
                                            "RequestID"         =>  $RequestID,
                                            "ApplicantSortName" =>  $ApplicantSortName
                                        );
				$i ++;
			} // end foreach
		}
		
		return $APPLICANTS;
	} // end OrgID
} // end function

function markApplicant($token, $applicationid, $requestid, $status) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$ok = 0;
		$rtn = "";
		
		if (($applicationid) && ($requestid) && ($status)) {

		    if(strstr($status, ':') !== false) {
		        //Overwrite the status variable
		        list($onboard_form_id, $status) = explode(":", $status);
		    }
		    else {
               		$FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );
               		$onboard_form_id  =   $FEATURES['DataManagerType'];

			if ($onboard_form_id == "N") {
			  if ($FEATURES['Lightwork'] == "Y") {
               			$onboard_form_id  =   "Lightwork";
			  }
			  if ($FEATURES['Abila'] == "Y") {
               			$onboard_form_id  =   "Abila";
			  }
			}
		    }
			
			// Set where condition
			$where       =   array (
                                    "OrgID              =   :OrgID",
                                    "ApplicationID      =   :ApplicationID",
                                    "RequestID          =   :RequestID",
                                    "OnboardFormID      =   :OnboardFormID"       
                                );
			// Set Information
			$set_info    =   array (
                                    "Status             =   :Status",
                                    "ProcessDate        =   NOW()"
                                );
			// Set Parameters
			$params      =   array (
                                    ":Status"           =>  $status,
                                    ":OnboardFormID"    =>  $onboard_form_id,
                                    ":OrgID"            =>  $OrgID,
                                    ":ApplicationID"    =>  $applicationid,
                                    ":RequestID"        =>  $requestid 
                                );
			// Update Applications Information
			$results     =   G::Obj('OnboardApplications')->updOnboardApplicationsInfo ( $set_info, $where, array ($params) );
			
			if ($results ['affected_rows'] > 0) {
				$ok ++;
			}
			
			$Comment = 'Applicant has been set to "' . $status . '. - "' . $onboard_form_id;
			
			// Insert Job Application History
			$job_app_history     =   array (
                                        	"OrgID"            =>  $OrgID,
                                        	"ApplicationID"    =>  $applicationid,
                                        	"RequestID"        =>  $requestid,
                                        	"ProcessOrder"     =>  "-2",
                                        	"Date"             =>  "NOW()",
                                        	"UserID"           =>  'Application',
                                        	"Comments"         =>  $Comment 
                                        );
			// Insert Job ApplicationHistory
			$results = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
			
			if ($results ['affected_rows'] > 0) {
				$ok ++;
			}
			
			if ($ok == 2) {
				$rtn = "Yes";
			} else {
				$rtn = "No";
			}
			
			return $rtn;
		} // end if appid, requestid, status
	} // end OrgID
} // end function

/* future for multiple onboard, replaces markApplicant */
function markOnboard($token, $applicationid, $requestid, $status, $onboardid, $returndata) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		$ok = 0;
		$rtn = "Failed";

		if (($applicationid) && ($requestid) && ($status)) {

			// Set where condition
			$where       =   array (
                                    "OrgID              =   :OrgID",
                                    "ApplicationID      =   :ApplicationID",
                                    "RequestID          =   :RequestID",
                                    "OnboardFormID      =   :OnboardFormID"       
                                );
			// Set Information
			$set_info    =   array (
                                    "Status             =   :Status",
                                    "ReturnData		=   :ReturnData",
                                    "ProcessDate        =   NOW()"
                                );
			// Set Parameters
			$params      =   array (
                                    ":Status"           =>  $status,
                                    ":ReturnData"       =>  $returndata,
                                    ":OnboardFormID"    =>  $onboardid,
                                    ":OrgID"            =>  $OrgID,
                                    ":ApplicationID"    =>  $applicationid,
                                    ":RequestID"        =>  $requestid 
                                );

			if ($onboardid == "") {
                	   unset($where[3]);
                	   unset($params[":OnboardFormID"]);
			}

			// Update Applications Information
			$results     =   G::Obj('OnboardApplications')->updOnboardApplicationsInfo ( $set_info, $where, array ($params) );
			
			if ($results ['affected_rows'] > 0) {
				$ok ++;
			}
			
			$Comment = 'Applicant has been set to "' . $status . '. - "' . $onboardid;
			$Comment .= "<br>\n" . 'Return Data:' . $returndata;
			
			// Insert Job Application History
			$job_app_history     =   array (
                                        	"OrgID"            =>  $OrgID,
                                        	"ApplicationID"    =>  $applicationid,
                                        	"RequestID"        =>  $requestid,
                                        	"ProcessOrder"     =>  "-2",
                                        	"Date"             =>  "NOW()",
                                        	"UserID"           =>  'Application',
                                        	"Comments"         =>  $Comment 
                                        );
			// Insert Job ApplicationHistory
			$results = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
			
			if ($results ['affected_rows'] > 0) {
				$ok ++;
			}
			
			if ($ok == 2) {
				$rtn = "Complete";
			} 



		} // end if appid, requestid, status

		return $rtn;

	} // end OrgID
} // end function

function getExporterConfiguration($token) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$EXPORTCONFIG     =   array ();
		$EXPORTCONFIG[0]  =   array (
                                    "ExporterID" => "",
                                    "ExportName" => "" 
                                );
		
		$params           =   array (":OrgID" => $OrgID);
		$results          =   G::Obj('ExporterDataManager')->getExporterInfo ( 'ExporterNames', "*", array ("OrgID = :OrgID"), "ExportName", array($params) );
		
		$i = 0;
		if (is_array ( $results ['results'] )) {
			foreach ( $results ['results'] as $EN ) {
				
				$EXPORTCONFIG [$i] = array (
					"ExporterID"   =>  $EN ['ExporterID'],
					"ExportName"   =>  $EN ['ExportName'] 
				);
				
				$i ++;
			} // end foreach
		}
		
		return $EXPORTCONFIG;
	} // end OrgID
} // end function

function getExporterApplicantList($token, $StartDate, $EndDate, $DataManagerStatus) {
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$EXPORTDATA       =   array ();
		$EXPORTDATA [0]   =   array (
                        			"ApplicationID"      => "",
                        			"RequestID"          => "",
                        			"OnboardFormID"      => "",
                        			"EntryDate"  	     => "",
                        			"ApplicantSortName"  => "",
                        			"JobTitle"           => "",
                        			"DataManagerStatus"  => "" 
                        		);

		$sel_info     =   "SELECT * FROM OnboardApplications OA";
        $sel_info    .=  " WHERE OA.OrgID = :OrgID
                           AND OA.ProcessDate BETWEEN :StartDate AND DATE_ADD(:EndDate, INTERVAL 1 DAY)";

        $params       =   array(":OrgID"=>$OrgID, ":StartDate"=>$StartDate, ":EndDate"=>$EndDate);
        
        if(strstr($DataManagerStatus, ':') !== false) {
            //Overwrite the status variable
            list($OnboardFormID, $Status) = explode(":", $DataManagerStatus);
            //Set OnboardFormID, Status
            $sel_info    .=  " AND OA.OnboardFormID = :OnboardFormID";
            $params[':OnboardFormID']   =   $OnboardFormID;
            $sel_info    .=  " AND OA.Status = :Status";
            $params[':Status']   =   $Status;
	    $DataManagerStatus = "";
	}  else {
            $sel_info    .=  " AND OA.Status = :Status";
            $params[':Status']   =   $DataManagerStatus;

	}
        
        $sel_info    .=  " ORDER BY OA.ApplicationID";
        $sel_info    .=  " LIMIT 100";

		$results      =   G::Obj('Applications')->getInfo($sel_info, array($params));
		
		if (is_array ( $results ['results'] )) {
			$i = 0;
			foreach ( $results ['results'] as $OA ) {
					
				$multiorgid_req     =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $OA['RequestID']);
				// Get JobTitle Based on RequestID
				$JobTitle           =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $OA['RequestID'] );
				$ApplicantSortName  =   G::Obj('ApplicantDetails')->getApplicantSortName($OrgID, $OA['ApplicationID'], $OA['RequestID']);
					
				$EXPORTDATA [$i]    =   array (
                        					"ApplicationID"      => $OA ['ApplicationID'],
                        					"RequestID"          => $OA ['RequestID'],
                        					"OnboardFormID"      => $OA ['OnboardFormID'],
                        					"EntryDate"  	     => $OA ['ProcessDate'],
                        					"ApplicantSortName"  => $ApplicantSortName,
                        					"JobTitle"           => $JobTitle,
                        					"DataManagerStatus"  => $OA ['Status'] 
                        				);
					
                $i ++;
			} // end foreach
		}

		return $EXPORTDATA;

	} // end OrgID

} // end function

function getExportList($token, $ExporterID, $StartDate, $EndDate, $ApplicationID, $RequestID, $case, $NameValue, $DataManagerStatus) {
    // Not working as of 1/15/2019.
    // Used by Exporter /reports/exporter/exportData.php
    // Recoded for offline processing updateExport.php 3/13/2020, no longer used
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$EXPORTDATA       =   array ();
		$EXPORTDATA [0]   =   array (
                                    "ApplicationID"     =>  "",
                                    "RequestID"         =>  "",
                                    "ApplicantSortName" =>  "",
                                    "JobTitle"          =>  "",
                                    "DataManagerStatus" =>  "",
                                    "OnboardFormID"     =>  "",
                        		);
		
		
		if ($ExporterID != "") {
	
            if ($DataManagerStatus != "") {

    		    //Get Job Applications based on Onboard Data
    			$params       =   array(":OrgID"=>$OrgID, ":StartDate"=>$StartDate, ":EndDate"=>$EndDate);
    			$sel_info     =   "SELECT * FROM OnboardApplications OA";
                $sel_info    .=  " WHERE OA.OrgID = :OrgID
                                    AND OA.ProcessDate <= DATE_ADD(:EndDate, INTERVAL 1 DAY)
                                    AND OA.ProcessDate >= DATE_SUB(:StartDate, INTERVAL 1 DAY)";
                
                if ($ApplicationID != "") {
                    $sel_info    .=  " AND OA.ApplicationID = :ApplicationID";
                    $params[":ApplicationID"] = $ApplicationID;
                }
                	
                if ($RequestID != "") {
                    $sel_info    .=  " AND OA.RequestID = :RequestID";
                    $params[":RequestID"] = $RequestID;
                }
    
                    
                if(strstr($DataManagerStatus, ':') !== false) {
                    //Overwrite the status variable
                    list($OnboardFormID, $Status) = explode(":", $DataManagerStatus);
                }
                else {
                    $FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );
                    $OnboardFormID  =   $FEATURES['DataManagerType'];
                    $Status         =   $DataManagerStatus;
                }
                    
                //Set OnboardFormID, Status
                $sel_info    .=  " AND OA.OnboardFormID = :OnboardFormID";
                $params[':OnboardFormID']   =   $OnboardFormID;
                $sel_info    .=  " AND OA.Status = :Status";
                $params[':Status']   =   $Status;
                
                $sel_info    .=  " ORDER BY OA.ApplicationID";

            } else {

                //Get Job Applications based on Onboard Data
                $params       =   array(":OrgID"=>$OrgID, ":StartDate"=>$StartDate, ":EndDate"=>$EndDate);
                $sel_info     =   "SELECT * FROM JobApplications JA";
                $sel_info    .=  " WHERE JA.OrgID = :OrgID AND JA.EntryDate <= DATE_ADD(:EndDate, INTERVAL 1 DAY) AND JA.EntryDate >= DATE_SUB(:StartDate, INTERVAL 1 DAY)";
                
                if ($ApplicationID != "") {
                    $sel_info    .=  " AND JA.ApplicationID = :ApplicationID";
                    $params[":ApplicationID"] = $ApplicationID;
                }
                	
                if ($RequestID != "") {
                    $sel_info    .=  " AND JA.RequestID = :RequestID";
                    $params[":RequestID"] = $RequestID;
                }
                
                $sel_info    .=  " ORDER BY JA.ApplicationID";
            
            } // end else DataManager

    		$results      =   G::Obj('Applications')->getInfo($sel_info, array($params));
						
            $i  =   0;
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $OA ) {
					
                    $multiorgid_req     =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $OA['RequestID']);
                    //Get JobTitle Based on RequestID
                    $JobTitle           =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $OA['RequestID'] );
                    $ApplicantSortName  =   G::Obj('ApplicantDetails')->getApplicantSortName($OrgID, $ApplicationID, $RequestID);
					
					$EXPORTDATA [$i] = array (
                            "ApplicationID"     =>  $OA ['ApplicationID'],
                            "RequestID"         =>  $OA ['RequestID'],
                            "ApplicantSortName" =>  $ApplicantSortName,
                            "JobTitle"          =>  $JobTitle,
                            "DataManagerStatus" =>  $OA ['Status'],
                            "OnboardFormID"     =>  $OnboardFormID,
					);
					
					$i ++;
				} // end foreach
			}

		} // end ExporterID

		return $EXPORTDATA;

	} // end OrgID
} // end function

function getExporterData($token, $ExporterID, $ApplicationID, $RequestID, $case, $NameValue) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$EXPORTDATA       =   array ();
		$EXPORTDATA [0]   =   array ("ExporterData" => "");
		
		if (($ExporterID != "") && ($ApplicationID != "") && ($RequestID != "")) {

			$EXPORT = G::Obj('ExporterDataManager')->getExportData ( $OrgID, $ApplicationID, $RequestID, $ExporterID, $case, $NameValue, $ExporterID );
					
			$EXPORTDATA [0] = array (
				"ExporterData" => json_encode ( $EXPORT [0] )
			);

		} // end ExporterID, ApplicationID, RequestID

		return $EXPORTDATA;

	} // end OrgID
} // end function

function getExport($token, $ExporterID, $StartDate, $EndDate, $ApplicationID, $RequestID, $case, $NameValue, $DataManagerStatus) {
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$EXPORTDATA = array ();
		$EXPORTDATA [0] = array (
			"ApplicationID"      => "",
			"RequestID"          => "",
			"ApplicantSortName"  => "",
			"JobTitle"           =>  "",
			"DataManagerStatus"  => "",
			"ExporterData"       => "",
			"ExporterValues"     => "" 
		);
		
		if ($ExporterID != "") {
			
			//Get Job Applications based on Onboard Data
			$params       =   array(":OrgID"=>$OrgID, ":StartDate"=>$StartDate, ":EndDate"=>$EndDate);
			$sel_info     =   "SELECT * FROM OnboardApplications OA";
			$sel_info    .=  " WHERE OA.OrgID = :OrgID
                    			AND OA.ProcessDate <= DATE_ADD(:EndDate, INTERVAL 1 DAY)
                    			AND OA.ProcessDate >= DATE_SUB(:StartDate, INTERVAL 1 DAY)";
			
            if ($ApplicationID != "") {
                $sel_info    .=  " AND OA.ApplicationID = :ApplicationID";
                $params[":ApplicationID"] = $ApplicationID;
            }
       
            if ($RequestID != "") {
                $sel_info    .=  " AND OA.RequestID = :RequestID";
                $params[":RequestID"] = $RequestID;
			}

            if ($DataManagerStatus != "") {
            
                if(strstr($DataManagerStatus, ':') !== false) {
                    //Overwrite the status variable
                    list($OnboardFormID, $Status) = explode(":", $DataManagerStatus);
                }
                else {
                    $FEATURES = G::Obj('IrecruitApplicationFeatures')->getApplicationFeaturesByOrgID ( $OrgID );
                    $OnboardFormID  =   $FEATURES['DataManagerType'];
                    $Status         =   $DataManagerStatus;
                }
            
                //Set OnboardFormID, Status
                $sel_info    .=  " AND OA.OnboardFormID = :OnboardFormID";
                $params[':OnboardFormID']   =   $OnboardFormID;
                $sel_info    .=  " AND OA.Status = :Status";
                $params[':Status']   =   $Status;
            }
    
            $sel_info    .=  " ORDER BY OA.ApplicationID";

            $results      =   G::Obj('Applications')->getInfo($sel_info, array($params));

			$i = 0;
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $OA) {
					
					$EXPORT            =   G::Obj('ExporterDataManager')->getExportData ( $OrgID, $OA ['ApplicationID'], $OA ['RequestID'], $ExporterID, $case, $NameValue, $OnboardFormID );
					$EXPORTVALUES      =   G::Obj('ExporterDataManager')->getExportValues ( $OrgID, $ExporterID );
					
					$multiorgid_req    =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $OA ['RequestID']);
					// Get JobTitle Based on RequestID
					$JobTitle          =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $OA ['RequestID'] );

                    $ApplicantSortName  =   G::Obj('ApplicantDetails')->getApplicantSortName($OrgID, $OA['ApplicationID'], $OA['RequestID']);
					
					$EXPORTDATA [$i] = array (
							"ApplicationID"      =>  $OA ['ApplicationID'],
							"RequestID"          =>  $OA ['RequestID'],
							"ApplicantSortName"  =>  $ApplicantSortName,
							"JobTitle"           =>  $JobTitle,
							"DataManagerStatus"  =>  $OA ['Status'],
							"ExporterData"       =>  json_encode ( $EXPORT [0] ),
							"ExporterValues"     =>  json_encode ( $EXPORTVALUES [0] ) 
					);
					
					$i ++;
				} // end foreach
			}
		} // end ExporterID
		
		return $EXPORTDATA;
	} // end OrgID
} // end function

function getFederalStateFormsList($token, $applicationid, $requestid) {
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$ACTIVEFORMS      =   array ();
		$ACTIVEFORMS [0]  =   array (
                                    "UniqueID"      =>  "",
                                    "TypeForm"      =>  "",
                                    "FormName"      =>  "",
                                    "LastUpdated"   =>  "",
                                    "Status"        =>  "" 
                        		);
		
		//Set where condition
		$where            =   array (
                                    "OrgID          =   :OrgID",
                                    "ApplicationID  =   :ApplicationID",
                                    "RequestID      =   :RequestID",
                                    "FormType       =   'PreFilled'",
                                    "Status < 5" 
                        		);
		//Set parameters condition
		$params           =   array (
                    				":OrgID"            =>  $OrgID,
                    				":ApplicationID"    =>  $applicationid,
                    				":RequestID"        =>  $requestid 
                        		);
		$results          =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo ( "*", $where, "", "FormType, FormName", array ($params) );
		
		$i = 0;
		if(is_array($results['results'])) {
			foreach($results['results'] as $INF) {
			
				$ACTIVEFORMS [$i]   =   array (
						"UniqueID"        =>  $INF ['UniqueID'],
						"TypeForm"        =>  $INF ['FormType'],
						"FormName"        =>  $INF ['FormName'],
						"LastUpdated"     =>  $INF ['LastUpdated'],
						"Status"          =>  $INF ['Status'] 
				);
				
				$i ++;
			} // end foreach
		}
		
		return $ACTIVEFORMS;

	} // end OrgID
} // end function

function getTaxFormData($token, $prefilledformid, $applicationid, $requestid, $case) {
	
	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		//Set where condition
		$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID");
		//Set parameters
		$params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$applicationid, ":RequestID"=>$requestid, ":PreFilledFormID"=>$prefilledformid);
		//Get PrefilledFormData
		$results  =   G::Obj('FormData')->getPreFilledFormData("*", $where, "", "", array($params));
		
		$DATA                     =   array ();
		$Scope                    =   array ();
		$FilingStatus             =   array ();
		$FilingType               =   array ();
		$FilingTypeValue          =   array ();
		$AdditionalAmount         =   array ();
		$Condition                =   array ();
		$ConditionValue           =   array ();
		$SecondaryCondition       =   array ();
		$SecondaryConditionValue  =   array ();
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $FORMDATA) {
				$DATA [$FORMDATA ['QuestionID']] = $FORMDATA ['Answer'];
			} // end foreach
		}

		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentW4 ($OrgID, $requestid, $applicationid);
	
		if ($prefilledformid == $CURRENT['W4']) {
			$Scope [0]                   =   "Federal";
			$FilingStatus [0]            =   $DATA ['status'];
			$FilingType [0]              =   "N";
			$FilingTypeValue [0]         =   $DATA ['allowances'];
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";
			
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] .= $DATA ['exempt-1'];
			}
			
			$ConditionValue [0]          =   "";
			$SecondaryCondition [0]      =   "";
			
			if ($DATA ['ssncheck-1'] == "Y") {
				$SecondaryCondition [0] = "ND";
			}
			
			$SecondaryConditionValue [0] =   "";
		} // end if CURRENT['W4']
		
		if ($prefilledformid == "AL-1") {
			$Scope [0]                   =   "State";
			$FilingStatus [0]            =   $DATA ['status'];
			$FilingType [0]              =   "N";
			$FilingTypeValue [0]         =   $DATA ['allowances'];
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";
			$ConditionValue [0]          =   "";
			$SecondaryCondition [0]      =   "";
			$SecondaryConditionValue [0] =   "";
		} // end if AL-1
		
		if ($prefilledformid == "AZ-1") {
			$Scope [0]                   =   "State";
			$FilingStatus [0]            =   "";
			$FilingType [0]              =   "P";
			$FilingTypeValue [0]         =   $DATA ['allowances'];
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";

			if ($DATA ['exempt-1'] != "") {
				$Condition [0]  =   $DATA ['exempt-1'];
			}
			
			$ConditionValue [0]          =   "";
			$SecondaryCondition [0]      =   "";
			$SecondaryConditionValue [0] =   "";
		} // end if AZ-1
		
		if ($prefilledformid == "AR-1") {
			$Scope [0]                   =   "State";
			$FilingStatus [0]            =   $DATA ['status'];
			$FilingType [0]              =   "N";
			
			$aa = 0;
			$ab = 0;
			$ac = 0;
			if ($DATA ['allowances'] == "A") {
				$aa = 1;
			} else if ($DATA ['allowances'] == "B") {
				$ab = 2;
			} else if ($DATA ['allowances'] == "C") {
				$ac = 2;
			}
			
			$FilingTypeValue [0]         =   $aa + $ab + $ac + $DATA ['dependents'];
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";
			$ConditionValue [0]          =   "";
			$SecondaryCondition [0]      =   "";

			if ($DATA ['lowrate-1'] == "Y") {
				$SecondaryCondition [0] = "LR";
			}
			
			$SecondaryConditionValue [0] =   "";
		} // end if AZ-1
		
		if ($prefilledformid == "CA-1") {
			$Scope [0]                   =   "State";
			$FilingStatus [0]            =   $DATA ['status'];
			$FilingType [0]              =   "N";
			
			$allowances                  =   $DATA ['allowances1'] + $DATA ['allowances2'];
			$FilingTypeValue [0]         =   $allowances;
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";
			
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			
			$ConditionValue [0]          =   "";
			$SecondaryCondition [0]      =   "";
			$SecondaryConditionValue [0] =   "";
		} // end if CA-1
		
		if ($prefilledformid == "CT-1") {
			$Scope [0]                   =   "State";
			$FilingStatus [0]            =   "";
			$FilingType [0]              =   "L";
			$FilingTypeValue [0]         =   $DATA ['allowances'];
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";
			
			if ($DATA ['exempt-1'] != "") {
				$Condition [0]  =   $DATA ['exempt-1'];
			}
			
			$ConditionValue [0]          =   "";
			
			if ($DATA ['exemptstate'] != "") {
				$ConditionValue [0] = $DATA ['exemptstate'];
			}
			
			$SecondaryCondition [0]      =   "";
			$SecondaryConditionValue [0] =   "";
		} // end if CT-1
		
		if ($prefilledformid == "DC-1") {
			$Scope [0]           = "State";
			$FilingStatus [0]    = $DATA ['status'];
			$FilingType [0]      = "M";
			$FilingTypeValue [0] = "";
			if ($DATA ['allowances-i'] != "") {
				$FilingTypeValue [0] .= $DATA ['allowances-i'];
			}
			$FilingTypeValue [0] .= ":";
			if ($DATA ['allowances-n'] != "") {
				$FilingTypeValue [0] .= $DATA ['allowances-n'];
			}
			$FilingTypeValue [0] .= ":";
			if ($DATA ['allowances-o'] != "") {
				$FilingTypeValue [0] .= $DATA ['allowances-o'];
			}
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			if ($DATA ['domicile'] == "Y") {
				$ConditionValue [0] .= "D";
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['domicilestate'] != "") {
				$ConditionValue [0] .= $DATA ['domicilestate'];
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['student'] == "Y") {
				$ConditionValue [0] .= "FS";
			}
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if DC-1
		
		if ($prefilledformid == "GA-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'] + $DATA ['dependents'] + $DATA ['additionalallowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			if ($DATA ['msrraexempt-1'] != "") {
				$SecondaryCondition [0] = $DATA ['msrraexempt-1'];
			}
			$SecondaryConditionValue [0] = "";
			if ($DATA ['msrraexemptstate1'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['msrraexemptstate1'];
			}
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['msrraexemptstate2'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['msrraexemptstate2'];
			}
		} // end if GA-1
		
		if ($prefilledformid == "HI-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if HI-1
		
		if ($prefilledformid == "IL-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "AE";
			$ConditionValue [0] = "";
			if ($DATA ['allowances2'] != "") {
				$ConditionValue [0] = $DATA ['allowances2'];
			}
			$SecondaryCondition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$SecondaryCondition [0] = $DATA ['exempt-1'];
			}
			$SecondaryConditionValue [0] = "";
		} // end if IL-1
		
		if ($prefilledformid == "IN-2") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			
			$d = 0;
			if ($DATA ['allowances4-1'] == "A") {
				$d ++;
			}
			if ($DATA ['allowances4-2'] == "B") {
				$d ++;
			}
			if ($DATA ['allowances4-3'] == "C") {
				$d ++;
			}
			if ($DATA ['allowances4-4'] == "D") {
				$d ++;
			}
			
			$allowances = $d + $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'];
			
			$FilingTypeValue [0] = $allowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "AE";
			$ConditionValue [0] = "";
			if ($DATA ['additionalallowances'] != "") {
				$ConditionValue [0] = $DATA ['additionalallowances'];
			}
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
			
			$Scope [1] = "County";
			$FilingStatus [1] = "";
			$FilingType [1] = "";
			$FilingTypeValue [1] = "";
			$AdditionalAmount [1] = number_format ( $DATA ['countyadditional'], 2 );
			$Condition [1] = "CRE";
			$ConditionValue [1] = "";
			if ($DATA ['countyresidence'] != "") {
				$ConditionValue [1] .= $DATA ['countyresidence'];
			}
			$ConditionValue [1] .= ":";
			if ($DATA ['countyemployment'] != "") {
				$ConditionValue [1] .= $DATA ['countyemployment'];
			}
			$SecondaryCondition [1] = "";
			$SecondaryConditionValue [1] = "";
		} // end if IN-2
		
		if ($prefilledformid == "IA-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			
			$totalallowances = $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'] + $DATA ['allowances4'] + $DATA ['allowances5'];
			
			$FilingTypeValue [0] = $totalallowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			if ($DATA ['exemptyear'] != "") {
				$ConditionValue [0] = $DATA ['exemptyear'];
			}
			$SecondaryCondition [0] = "";
			if ($DATA ['msrraexempt'] != "") {
				$SecondaryCondition [0] = $DATA ['msrraexempt'];
			}
			$SecondaryConditionValue [0] = "";
			if ($DATA ['msrrastate'] != "") {
				$SecondaryConditionValue [0] = $DATA ['msrrastate'];
			}
		} // end if IA-1
		
		if ($prefilledformid == "KA-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if KL-1
		
		if ($prefilledformid == "KY-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$allowances = $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3a'] + $DATA ['allowances3b'] + $DATA ['allowances4'] + $DATA ['allowances5'] + $DATA ['allowances6'];
			$FilingTypeValue [0] = $allowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if KY-1
		
		if ($prefilledformid == "LA-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "AE";
			$ConditionValue [0] = "";
			if ($DATA ['additionalallowances'] != "") {
				$ConditionValue [0] = $DATA ['additionalallowances'];
			}
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if LA-1
		
		if ($prefilledformid == "ME-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			for ($i = 1; $i <= $DATA['exemptcnt']; $i++) {
				$val="exempt-" . $i;
				if($DATA[$val] != "") {
				  $Condition [0] .= $DATA [$val] . ":";
			        }
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if ME-1
		
		if ($prefilledformid == "MD-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			if ($DATA ['exemptyear'] != "") {
				$ConditionValue [0] = $DATA ['exemptyear'];
			}
			$SecondaryCondition [0] = "ADD";
			$SecondaryConditionValue [0] = "";
			if ($DATA ['domicilestate'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['domicilestate'];
			}
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['domicileexempt'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['domicileexempt'];
			}
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['msrraexempt'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['msrraexempt'];
			}
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['msrrastate'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['msrrastate'];
			}
			
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['pastateexempt'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['pastateexempt'];
			}
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['palocalyaexempt'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['palocalyaexempt'];
			}
			$SecondaryConditionValue [0] .= ":";
			if ($DATA ['palocalexempt'] != "") {
				$SecondaryConditionValue [0] .= $DATA ['palocalexempt'];
			}
		} // end if MD-1
		
		if ($prefilledformid == "MA-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$totalallowances = $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'];
			$FilingTypeValue [0] = $totalallowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "ST";
			$ConditionValue [0] = "";
			if ($DATA ['status-1'] != "") {
				$ConditionValue [0] .= $DATA ['status-1'];
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['status-2'] != "") {
				$ConditionValue [0] .= $DATA ['status-2'];
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['status-3'] != "") {
				$ConditionValue [0] .= $DATA ['status-3'];
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['status-4'] != "") {
				$ConditionValue [0] .= $DATA ['status-4'];
			}
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if MA-1
		
		if ($prefilledformid == "MI-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			for ($i = 1; $i <= $DATA['exemptcnt']; $i++) {
				$val="exempt-" . $i;
				if($DATA[$val] != "") {
				  $Condition [0] .= $DATA [$val] . ":";
			        }
			}
			$ConditionValue [0] = "";
			if ($DATA ['explainexempt'] != "") {
				$ConditionValue [0] .= $DATA ['explainexempt'];
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['renaissanceexempt'] != "") {
				$ConditionValue [0] .= $DATA ['renaissanceexempt'];
			}
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if MI-1
		
		if ($prefilledformid == "MS-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "D";
			
			$One = 0;
			$TwoA = 0;
			$TwoB = 0;
			$Three = 0;
			$Four = 0;
			$Five = 0;
			
			if ($DATA ['status'] == "S") {
				$One = 6000;
			} else if ($DATA ['status'] == "H") {
				$Three = 9500;
			} else if ($DATA ['status'] == "SNE") {
				$TwoA = 12000;
			} else {
				$TwoB = substr ( $DATA ['status'], 2 );
			}
			
			$Four = 1500 * $DATA ['dependents'];
			
			$ageblind = 0;
			
			if ($DATA ['exceptionage-1'] == "H") {
				$ageblind ++;
			}
			if ($DATA ['exceptionage-2'] == "W") {
				$ageblind ++;
			}
			if ($DATA ['exceptionage-3'] == "S") {
				$ageblind ++;
			}
			if ($DATA ['exceptionblind-1'] == "H") {
				$ageblind ++;
			}
			if ($DATA ['exceptionblind-2'] == "W") {
				$ageblind ++;
			}
			if ($DATA ['exceptionblind-3'] == "S") {
				$ageblind ++;
			}
			
			$Five = 1500 * $ageblind;
			
			$totalexemption = $One + $TwoA + $TwoB + $Three + $Four + $Five;
			
			$FilingTypeValue [0] = number_format ( $totalexemption, 2 );
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = $DATA ['dependents'];
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if MS-1
		
		if ($prefilledformid == "MO-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$totalallowances = $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'] + $DATA ['allowances4'];
			$FilingTypeValue [0] = $totalallowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			if ($DATA ['msrraexempt'] != "") {
				$SecondaryCondition [0] = $DATA ['msrraexempt'];
			}
			$SecondaryConditionValue [0] = "";
		} // end if MO-1
		
		if ($prefilledformid == "NJ-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "L";
			$SecondaryConditionValue [0] = "";
			if ($DATA ['statuschart'] != "") {
				$SecondaryConditionValue [0] = $DATA ['statuschart'];
			}
		} // end if NJ-1
		
		if ($prefilledformid == "NY-3") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowancesnys'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
			
			$Scope [1] = "City";
			$FilingStatus [1] = "";
			$FilingType [1] = "N";
			$FilingTypeValue [1] = $DATA ['allowancesnyc'];
			$AdditionalAmount [1] = number_format ( $DATA ['additionalnyc'], 2 );
			$Condition [1] = "";
			if ($DATA ['residentnyc'] == "Y") {
				$Condition [1] = "RC";
			}
			$ConditionValue [1] = "";
			$SecondaryCondition [1] = "";
			$SecondaryConditionValue [1] = "";
			
			$Scope [2] = "Yonkers";
			$FilingStatus [2] = "";
			$FilingType [2] = "N";
			$FilingTypeValue [2] = $DATA ['allowancesnys'];
			$AdditionalAmount [2] = number_format ( $DATA ['additionalyonkers'], 2 );
			$Condition [2] = "";
			if ($DATA ['residentyonkers'] == "Y") {
				$Condition [2] = "RY";
			}
			$ConditionValue [2] = "";
			$SecondaryCondition [2] = "";
			$SecondaryConditionValue [2] = "";
		} // end if NY-3
		
		if ($prefilledformid == "NC-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = $DATA ['status'];
			$FilingType [0] = "N";
			$FilingTypeValue [0] = $DATA ['allowances'];
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if NC-1
		
		if ($prefilledformid == "OH-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$totalallowances = $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'];
			$FilingTypeValue [0] = $totalallowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "ADD";
			$ConditionValue [0] = "";
			if ($DATA ['schooldistrict'] != "") {
				$ConditionValue [0] .= $DATA ['schooldistrict'];
			}
			$ConditionValue [0] .= ":";
			if ($DATA ['districtnumber'] != "") {
				$ConditionValue [0] .= $DATA ['districtnumber'];
			}
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if OH-1
		
		if ($prefilledformid == "VA-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$totalallowances = $DATA ['allowances1'] + $DATA ['allowances2'];
			$FilingTypeValue [0] = $totalallowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			if ($DATA ['msrraexempt-1'] != "") {
				$SecondaryCondition [0] = $DATA ['msrraexempt-1'];
			}
			$SecondaryConditionValue [0] = "";
		} // end if VA-1
		
		if ($prefilledformid == "WV-1") {
			$Scope [0] = "State";
			$FilingStatus [0] = "";
			$FilingType [0] = "N";
			$totalallowances = $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'];
			$FilingTypeValue [0] = $totalallowances;
			$AdditionalAmount [0] = number_format ( $DATA ['additional'], 2 );
			$Condition [0] = "";
			if ($DATA ['lowerrate-1'] != "") {
				$Condition [0] = $DATA ['lowerrate-1'];
			}
			$ConditionValue [0] = "";
			$SecondaryCondition [0] = "";
			$SecondaryConditionValue [0] = "";
		} // end if WV-1
		
		if ($prefilledformid == "WI-1") {
			$Scope [0]                   =   "State";
			$FilingStatus [0]            =   $DATA ['status'];
			$FilingType [0]              =   "N";
			$totalallowances             =   $DATA ['allowances1'] + $DATA ['allowances2'] + $DATA ['allowances3'];
			$FilingTypeValue [0]         =   $totalallowances;
			$AdditionalAmount [0]        =   number_format ( $DATA ['additional'], 2 );
			$Condition [0]               =   "";
			
			if ($DATA ['exempt-1'] != "") {
				$Condition [0] = $DATA ['exempt-1'];
			}
			
			$ConditionValue [0]          =   "";
			$SecondaryCondition [0]      =   "";
			$SecondaryConditionValue [0] =   "";
		} // end if WI-1
		
		$TAXROW       =   array ();
		$TAXROW [0]   =   array (
                    			"Scope"                      =>  "",
                    			"FilingStatus"               =>  "",
                    			"FilingType"                 =>  "",
                    			"FilingTypeValue"            =>  "",
                    			"AdditionalAmount"           =>  "",
                    			"Condition"                  =>  "",
                    			"ConditionValue"             =>  "",
                    			"SecondaryCondition"         =>  "",
                    			"SecondaryConditionValue"    =>  "" 
                    		);
		for($i = 0; $i < count ( $Scope ); $i ++) {
			
		    $TAXROW [$i]  =   array (
                					"Scope"                    =>  "$Scope[$i]",
                					"FilingStatus"             =>  "$FilingStatus[$i]",
                					"FilingType"               =>  "$FilingType[$i]",
                					"FilingTypeValue"          =>  "$FilingTypeValue[$i]",
                					"AdditionalAmount"         =>  "$AdditionalAmount[$i]",
                					"Condition"                =>  "$Condition[$i]",
                					"ConditionValue"           =>  "$ConditionValue[$i]",
                					"SecondaryCondition"       =>  "$SecondaryCondition[$i]",
                					"SecondaryConditionValue"  =>  "$SecondaryConditionValue[$i]" 
                                );
			
			if (strtoupper ( $case ) == "UPPER") {
				foreach ( $TAXROW [$i] as $key => $value ) {
					$newvalue = strtoupper ( $value );
					$TAXROW [$i] [$key] = $newvalue;
				}
			}
		}
		
		return $TAXROW;
	} // end OrgID
} // end function

function geti9Data($token, $applicationid, $requestid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $requestid, $applicationid);
		
		//Set where condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$applicationid, ":RequestID"=>$requestid, ":PreFilledFormID"=>$CURRENT['I9']);
		//Get PrefilledFormData
		$results = G::Obj('FormData')->getPreFilledFormData("*", $where, "", "", array($params));
		
		
		$DATA = array ();
		
		if(is_array($results['results'])) {
			foreach ($results['results'] as $FORMDATA) {
					
				$DATA [$FORMDATA ['QuestionID']] = $FORMDATA ['Answer'];
					
				if ($FORMDATA ['QuestionID'] == "SSN") {
                    $Ans15  =   json_decode($FORMDATA ['Answer'], true);
                    $ssn1   =   $Ans15[0];
                    $ssn2   =   $Ans15[1];
                    $ssn3   =   $Ans15[2];
				}
					
				if ($FORMDATA ['QuestionID'] == "TelephoneNumber") {
					$telephonenumber = preg_replace ( "/[^0-9]/", "", $FORMDATA ['Answer'] );
				}
			} // end foreach
		}
		
		
		$DATA ['SSN']             =   $ssn1 . $ssn2 . $ssn3;
		$DATA ['TelephoneNumber'] =   $telephonenumber;
		
		$DATA ['EmploymentDate']  =   "";
		$DATA ['NewHire']         =   "";
		$DATA ['ValidationDate']  =   "";
		
		// Get Prefilled Form Data Question Information
		$pre_formdata_que_info    =   G::Obj('FormData')->getPreFilledFormDataQuestionInfo ( 'Answer', $OrgID, $applicationid, $requestid, $CURRENT['I9m'], 'employmentdate' );
		$employmentdate           =   $pre_formdata_que_info ['Answer'];
		
		if ($employmentdate != "") {
			$DATA ['EmploymentDate'] = $employmentdate;
		}

		// Get Prefilled Form Data Question Information
		$pre_formdata_que_info    =   G::Obj('FormData')->getPreFilledFormDataQuestionInfo ( 'Answer', $OrgID, $applicationid, $requestid, $CURRENT['I9m'], 'newhire' );
		$newhire                  =   $pre_formdata_que_info['Answer'];
		
		if ($newhire != "") {
			$DATA ['NewHire'] = $newhire;
		}
		
		// Get Prefilled Form Data Question Information
		$pre_formdata_que_info    =   G::Obj('FormData')->getPreFilledFormDataQuestionInfo ( 'Answer', $OrgID, $applicationid, $requestid, $CURRENT['I9m'], 'signature' );
		$validationdate           =   $pre_formdata_que_info['Answer'];
		
		if ($validationdate != "") {
			$DATA ['ValidationDate'] = $validationdate;
		}
		
		return $DATA;
	} // end OrgID
} // end function

function geti9mData($token, $applicationid, $requestid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

		$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $requestid, $applicationid);
		
		// Set parameters
		$params = array (
				":OrgID"            => $OrgID,
				":ApplicationID"    => $applicationid,
				":RequestID"        => $requestid,
				":PreFilledFormID"  => $CURRENT['I9m']
		);
		// Set where condition
		$where = array (
				"OrgID              = :OrgID",
				"ApplicationID      = :ApplicationID",
				"RequestID          = :RequestID",
				"PreFilledFormID    = :PreFilledFormID"
		);
		// Get PrefilledForm Data
		$results = G::Obj('FormData')->getPreFilledFormData ( "*", $where, "", "", array (
				$params
		) );
		
		$TITLE = array (
				'employmentdate'    => 'EmploymentDate',
				'newhire'           => 'NewHire',
				'listAdoctitle1'    => 'ListATitle',
				'listAdocument1'    => 'ListADocumentNumber',
				'listAissuingauth1' => 'ListAAuthority',
				'listAexpdate1'     => 'ListAExpDate',
				'listBdoctitle'     => 'ListBTitle',
				'listBsupdoc'       => 'ListBSupportingDocument',
				'listBdocument'     => 'ListBDocumentNumber',
				'listBissuingauth'  => 'ListBAuthority',
				'listBexpdate'      => 'ListBExpDate',
				'listCdoctitle'     => 'ListCTitle',
				'listCdocument'     => 'ListCDocumentNumber',
				'listCissuingauth'  => 'ListCAuthority',
				'listCexpdate'      => 'ListCExpDate',
				'ValidationDate' 
		);
		
        $DATA                       =   array ();

        // Get Prefilled Form Data Question Information
        $pre_formdata_que_info      =   G::Obj('FormData')->getPreFilledFormDataQuestionInfo ( 'Answer', $OrgID, $applicationid, $requestid, $CURRENT['I9m'], 'signature' );
        $validationdate             =   $pre_formdata_que_info['Answer'];

		if ($validationdate != "") {
			$DATA ['ValidationDate'] = $validationdate;
		}

		if(is_array($results['results'])) {
			foreach ($results['results'] as $FORMDATA) {
					
				if ($TITLE [$FORMDATA ['QuestionID']] != "") {
					$DATA [$TITLE [$FORMDATA ['QuestionID']]] = $FORMDATA ['Answer'];
				}
			} // end foreach
		}
		
		return $DATA;

	} // end OrgID
} // end function

function getCertifications($token, $applicationid, $case) {

	$OrgID = login ( $token );

        if ($OrgID == "") {
                return "You don't have authorization to use this application.";
        } else {

            $CERTIFICATIONS    =   array ();

        $ALPHA      =   G::Obj('GenericLibrary')->getAlphabets();
        $CERT       =   array ();
        $CERT[0]    =   array (
                            "Specialty" =>  "",
                            "LicenseNo" =>  "",
                            "Effective" =>  "",
                            "State"     =>  ""
                        );


        //Set where condition
        $where      =   array("OrgID = :OrgID", "FormID = 'STANDARD'", "SectionID = 7");
        //Set parameters
        $params     =   array(":OrgID"=>$OrgID);
        //Get FormQuestionsInformation
        $results    =   G::Obj('FormQuestions')->getFormQuestionsInformation("QuestionID", $where, "QuestionOrder", array($params));

        $cnt        =   0;

                if(is_array($results['results'])) {
                        foreach($results['results'] as $FQ) {
                          $CERTIFICATIONS [$FQ ['QuestionID']] = "";
                          if(strpos($FQ ['QuestionID'], 'crttype') !== false) {
                              $cnt++;
                          }
                        } // end foreach
                }

                foreach ( $CERTIFICATIONS as $key => $value ) {
                            $ans       =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, $key);
                            $CERTIFICATIONS [$key] = strip_tags($ans);
                } // end foreach

                for($x = 0; $x < $cnt; $x++) {
                    if ($CERTIFICATIONS [$ALPHA [$x].'crttype'] != "") {

                    //Get ApplicantAnswer
            $ans        =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, $key);

            $CERT[$x]     =   array(
                                "Specialty"   => $CERTIFICATIONS [$ALPHA[$x] . 'crttype'],
                                "LicenseNo"   => $CERTIFICATIONS [$ALPHA[$x] . 'crtno'],
                                "Effective"   => $CERTIFICATIONS [$ALPHA[$x] . 'crteffdate'],
                                "Expires"     => $CERTIFICATIONS [$ALPHA[$x] . 'crtexpdate'],
                                "State"       => $CERTIFICATIONS [$ALPHA[$x] . 'crtstate']
                                    );
                    }

                }

                return $CERT;
        } // end OrgID
} // end function

function getWorkExperience($token, $applicationid, $case) {

        $OrgID = login ( $token );

        if ($OrgID == "") {
                return "You don't have authorization to use this application.";
        } else {

        $ALPHA      =   G::Obj('GenericLibrary')->getAlphabets();
        $WORK       =   array ();
        $WORK[0]    =   array (
                            "CompanyName"           => "",
                            "Address"               => "",
                            "City"                  => "",
                            "State"                 => "",
                            "PostalCode"            => "",
                            "DateofEmployment_From" => "",
                            "DateofEmployment_To"   => "",
                            "Position"              => "",
                            "StartingSalary"        => "",
                            "StartingSalaryDefinition" => "",
                            "Salary"                => "",
                            "SalaryDefinition"      => "",
                            "LastSupervisor"        => "",
                            "Phone"                 => "",
                            "Ok2Contact"            => "",
                            "CurrentPosition"       => "",
                            "DescribePosition"      => "",
                            "ReasonForLeaving"      => "",
                            "Discharged"            => "",
                            "DischargedReason"      => ""
                        );

                $WORKEXPERIENCE = array ();

                //Set where condition
                $where = array("OrgID = :OrgID", "FormID = 'STANDARD'", "SectionID = 4");
                //Set parameters
                $params = array(":OrgID"=>$OrgID);
                //Get FormQuestionsInformation
                $results = G::Obj('FormQuestions')->getFormQuestionsInformation("QuestionID", $where, "QuestionOrder", array($params));

                $cnt = 0;
                if(is_array($results['results'])) {
                        foreach($results['results'] as $FQ) {
                                $WORKEXPERIENCE [$FQ ['QuestionID']] = "";
                                if(strpos($FQ ['QuestionID'], 'mrpcompanyname') !== false) {
                                    $cnt++;
                                }
                        } // end foreach
                }

                foreach ( $WORKEXPERIENCE as $key => $value ) {
                    if(strpos($key, "mrpphone") !== false) {
                            //Get ApplicantAnswer
                            $ans    =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, $key);
                            $ans    =   json_decode($ans, true);
                            $ans1   =   $ans[0];
                            $ans2   =   $ans[1];
                            $ans3   =   $ans[2];

                                $WORKEXPERIENCE [$key] = $ans1 . "-" . $ans2 . "-" . $ans3;
                        } else {
                                //Get ApplicantAnswer
                                $ans    =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, $key);
                                $WORKEXPERIENCE [$key] = $ans;
                        }
                } // end foreach

                foreach ($WORKEXPERIENCE as $key => $value) {
                   $WORKEXPERIENCE [$key]=strip_tags($value);
                   $WORKEXPERIENCE [$key]=preg_replace('/[^A-Za-z0-9-!@#$%\^&*()+={}?,.:;\"\'\/\s\_]/', '', $WORKEXPERIENCE [$key]);
                   $WORKEXPERIENCE [$key]=preg_replace('/\'/', '\'\'', $WORKEXPERIENCE [$key]);
                }

        for($x = 0; $x < $cnt; $x++) {

            if ($WORKEXPERIENCE [$ALPHA[$x].'mrpcompanyname'] != "") {
                $WORK [$x] = array (
                    "CompanyName"                 => $WORKEXPERIENCE [$ALPHA[$x].'mrpcompanyname'],
                    "Address"                     => $WORKEXPERIENCE [$ALPHA[$x].'mrpaddress'],
                    "City"                        => $WORKEXPERIENCE [$ALPHA[$x].'mrpcity'],
                    "State"                       => $WORKEXPERIENCE [$ALPHA[$x].'mrpstate'],
                    "PostalCode"                  => $WORKEXPERIENCE [$ALPHA[$x].'mrppostalcode'],
                    "DateofEmployment_From"       => $WORKEXPERIENCE [$ALPHA[$x].'mrpdatesofemploymentfrom'],
                    "DateofEmployment_To"         => $WORKEXPERIENCE [$ALPHA[$x].'mrpdatesofemploymentto'],
                    "Position"                    => $WORKEXPERIENCE [$ALPHA[$x].'mrpposition'],
                    "StartingSalary"              => $WORKEXPERIENCE [$ALPHA[$x].'mrpstartingsalary'],
                    "StartingSalaryDefinition"    => $WORKEXPERIENCE [$ALPHA[$x].'mrpstartingsalarydef'],
                    "Salary"                      => $WORKEXPERIENCE [$ALPHA[$x].'mrpsalary'],
                    "SalaryDefinition"            => $WORKEXPERIENCE [$ALPHA[$x].'mrpsalarydefinition'],
                    "LastSupervisor"              => $WORKEXPERIENCE [$ALPHA[$x].'mrplastsupervisor'],
                    "Phone"                       => $WORKEXPERIENCE [$ALPHA[$x].'mrpphone'],
                    "Ok2Contact"                  => $WORKEXPERIENCE [$ALPHA[$x].'mrpoktocontact'],
                    "CurrentPosition"             => $WORKEXPERIENCE [$ALPHA[$x].'mrpcurrentposition'],
                    "DescribePosition"            => $WORKEXPERIENCE [$ALPHA[$x].'mrpdescribeposition'],
                    "ReasonForLeaving"            => $WORKEXPERIENCE [$ALPHA[$x].'mrpreasonforleaving'],
                    "Discharged"                  => $WORKEXPERIENCE [$ALPHA[$x].'discharged'],
                    "DischargedReason"            => $WORKEXPERIENCE [$ALPHA[$x].'dischargedreason']
                );

                $i++;
            }
        }

                return $WORK;
        } // end OrgID
} // end function

function getSkills($token, $applicationid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$SKILLS = array ();
		$SKILLS [0] = array (
				"Skill" => "",
				"Years" => "",
				"Comment" => "" 
		);
		//Get ApplicantAnswer
		$Ans = json_decode(G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, 'skills'),true);

		$i=0;
            	foreach ($Ans as $kk => $vv) {
                if (strtoupper($case) == "UPPER") {
                    $SKILLS [$i] = array (
                        "Skill" => strtoupper ($vv["skills-" . $kk]),
                        "Years" => strtoupper ($vv["skills-" . $kk . "-yr"]),
                        "Comment" => strtoupper ($vv["skills-" . $kk . "-comments"])
                    );
                } else {
		    $SKILLS [$i] = array (
			"Skill" => $vv["skills-" . $kk],
			"Years" => $vv["skills-" . $kk . "-yr"],
			"Comment" => $vv["skills-" . $kk . "-comments"] 
		    );
		}
		$i++;
	        } // end foreach

		return $SKILLS;
	} // end OrgID
} // end function

function getEducation($token, $applicationid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$ALPHA = G::Obj('GenericLibrary')->getAlphabets();
		$EDU = array ();
		$EDU [0] = array (
				"Attended"          => "",
				"YearsCompleted"    => "",
				"Graduated"         => "",
				"Year"              => "",
				"GPA"               => "",
				"Degree"            => "",
				"Major"             => "",
				"Minor"             => "",
				"OtherDegree"       => "",
				"Phone"             => "",
				"Address"           => "" 
		);
		
		
		$EDUCATION    =   array ();
		//Set where condition
		$where        =   array("OrgID = :OrgID", "FormID = 'STANDARD'", "SectionID = 9");
		//Set parameters
		$params       =   array(":OrgID"=>$OrgID);
		//Get FormQuestionsInformation
		$results      =   G::Obj('FormQuestions')->getFormQuestionsInformation("QuestionID", $where, "QuestionOrder", array($params));
		
		$cnt          =   0;
		if(is_array($results['results'])) {
			foreach ($results['results'] as $FQ) {
				$EDUCATION [$FQ ['QuestionID']] = "";
				if(strpos($FQ ['QuestionID'], 'attended') !== false) {
				    $cnt++;
				}
			} // end foreach
		}
		
		foreach ( $EDUCATION as $key => $value ) {
		    if(strpos($key, 'eduphone') !== false) {
			    //Get ApplicantAnswer
			    $ans    =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, $key);
			    $ans    =   json_decode($ans, true);
			    $ans1   =   $ans[0];
			    $ans2   =   $ans[1];
			    $ans3   =   $ans[2];
				
				$EDUCATION [$key] = $ans1 . "-" . $ans2 . "-" . $ans3;
			} else {
				//Get ApplicantAnswer
			    $ans       =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $applicationid, $key);
				$EDUCATION [$key] = strip_tags($ans);
			}
		} // end foreach

		for($x = 0; $x < $cnt; $x ++) {
		    if ($EDUCATION [$ALPHA [$x].'attended'] != "") {
				$EDU [$x] = array (
				    "Attended"        => $EDUCATION [$ALPHA [$x].'attended'],
				    "YearsCompleted"  => $EDUCATION [$ALPHA [$x].'yearscompleted'],
				    "Graduated"       => $EDUCATION [$ALPHA [$x].'graduated'],
				    "Year"            => $EDUCATION [$ALPHA [$x].'year'],
				    "GPA"             => $EDUCATION [$ALPHA [$x].'gpa'],
				    "Degree"          => $EDUCATION [$ALPHA [$x].'degree'],
				    "Major"           => $EDUCATION [$ALPHA [$x].'major'],
				    "Minor"           => $EDUCATION [$ALPHA [$x].'minor'],
				    "OtherDegree"     => $EDUCATION [$ALPHA [$x].'otherdegree'],
				    "Phone"           => $EDUCATION [$ALPHA [$x].'eduphone'],
				    "Address"         => $EDUCATION [$ALPHA [$x].'eduaddress'] 
				);
			}
        } // end for loop
		
		return $EDU;
	} // end OrgID
} // end function

function getAttachments($token, $applicationid) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$AA2 = array ();
		$AA2 [0] = array (
				"PurposeName" => "",
				"FileType" => "",
				"URLLink" => ""
		);
		//Set where condition
		$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
		//Set parameters
		$params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$applicationid);
		//Get ApplicantAttachements Information
		$results  =   G::Obj('Attachments')->getApplicantAttachments("*", $where, '', array($params));


		$i = 0;
		if(is_array($results['results'])) {
			foreach($results['results'] as $AA) {

				$PurposeName = $AA ['PurposeName'];

				if ($PurposeName == "Resume-resumeupload") { $PurposeName = "resume"; }
				if ($PurposeName == "CoverLetter-coverletterupload") { $PurposeName = "coverletter"; }
				if ($PurposeName == "OtherDocument-otherupload") { $PurposeName = "other"; }
					
				$Purpose        = $PurposeName;
				$FileType       = $AA ['FileType'];
				$Link           = IRECRUIT_HOME . "display_attachment.php?OrgID=" . $OrgID;
				$Link          .= "&ApplicationID=" . $applicationid . "&Type=" . $AA ['TypeAttachment'];
					
				$AA2 [$i] = array (
						"PurposeName" => $Purpose,
						"FileType"    => "$FileType",
						"URLLink"     => "$Link"
				);
				$i ++;
			} // end foreach
		}

		return $AA2;
	} // end OrgID
} // end function

function getAADISVET($token, $applicationid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$AA = array ();
		
		//Set where condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "QuestionID in ('aa_genderselection','aa_ethnicgroup','aa_race','dis_preclassification','vet_preclassification')");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$applicationid);
		//Get ApplicantData Information
		$results = G::Obj('Applicants')->getApplicantDataInfo('QuestionID, Answer', $where, '', '', array($params));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $AD) {
				if ($case == "Upper") {
					$AA [$AD ['QuestionID']] = strtoupper ( $AD ['Answer'] );
				} else {
					$AA [$AD ['QuestionID']] = $AD ['Answer'];
				}
			} // end foreach					
		}
		
		return array (				
				"GenderSelection"   => $AA ['aa_genderselection'],
				"EthnicGroup"       => $AA ['aa_ethnicgroup'],
				"Race"              => $AA ['aa_race'],
				"Disabled"          => $AA ['dis_preclassification'],
				"Veteran"           => $AA ['vet_preclassification']
		);
	} // end OrgID

} // end function

function getAffirmativeAction($token, $applicationid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {
		
		$AA = array ();
		
		//Set where condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "QuestionID in ('aa_gender','aa_genderselection','aa_race','aa_ethnicity','aa_ethnicgroup','aa_disabled','aa_veteran','aa_veteranstatus','aa_veterandate','dis_preclassification','vet_preclassification','vet_enterdate')");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$applicationid);
		//Get ApplicantData Information
		$results = G::Obj('Applicants')->getApplicantDataInfo('QuestionID, Answer', $where, '', '', array($params));
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $AD) {
				if ($case == "Upper") {
					$AA [$AD ['QuestionID']] = strtoupper ( $AD ['Answer'] );
				} else {
					$AA [$AD ['QuestionID']] = $AD ['Answer'];
				}
			} // end foreach					
		}
		
		return array (				
				"Gender"            => $AA ['aa_gender'],
				"GenderSelection"   => $AA ['aa_genderselection'],
				"Race"              => $AA ['aa_race'],
				"Ethnicity"         => $AA ['aa_ethnicity'],
				"EthnicGroup"       => $AA ['aa_ethnicgroup'],
				"Disabled"          => $AA ['dis_preclassification'],
				"Veteran"           => $AA ['vet_preclassification'],
				"VeteranStatus"     => $AA ['aa_veteranstatus'],
				"VeteranDate"       => $AA ['vet_enterdate'] 
		);
	} // end OrgID

} // end function

function getEmergencyContacts($token, $applicationid, $requestid, $case) {

	$OrgID = login ( $token );
	
	if ($OrgID == "") {
		return "You don't have authorization to use this application.";
	} else {

                $EMC = array ();
                $results   =       G::Obj('ApplicantOnboardingInfo')->getEmergencyContactsFormData($OrgID, $applicationid, $requestid);

		foreach ($results['results'] AS $WFD) {

		  $ANSWER =   $WFD['Answer'];

		  if ($WFD['QuestionTypeID'] == "13") {
		    $Ans  =   json_decode($WFD['Answer'], true);
		    $ANSWER =   $Ans[0] . $Ans[1] . $Ans[2];
		  };

		  $EMC[$WFD['QuestionID']]=$ANSWER;

		}

		foreach($EMC as $name => $value) {
			if ($case == "Upper") {
			   $EMC [$name] = strtoupper ( $value );
			} else {
			   $EMC [$name] = $value;
			}
		} // end foreach					

		$cell_phone1 = "";
		$work_phone1 = "";
		$home_phone1 = "";

		$cell_phone2 = "";
		$work_phone2 = "";
		$home_phone2 = "";

		if ($EMC ['ecphone1atype'] == "2") {
		  $home_phone1 = $EMC ['ecphone1a'];
		}	
		if ($EMC ['ecphone1atype'] == "3") {
		  $work_phone1 = $EMC ['ecphone1a'];
		}	
		if ($EMC ['ecphone1atype'] == "4") {
		  $cell_phone1 = $EMC ['ecphone1a'];
		}	

		if ($EMC ['ecphone1btype'] == "2") {
		  $home_phone1 = $EMC ['ecphone1b'];
		}	
		if ($EMC ['ecphone1btype'] == "3") {
		  $work_phone1 = $EMC ['ecphone1b'];
		}	
		if ($EMC ['ecphone1btype'] == "4") {
		  $cell_phone1 = $EMC ['ecphone1b'];
		}	

		if ($EMC ['ecphone1ctype'] == "2") {
		  $home_phone1 = $EMC ['ecphone1c'];
		}	
		if ($EMC ['ecphone1ctype'] == "3") {
		  $work_phone1 = $EMC ['ecphone1c'];
		}	
		if ($EMC ['ecphone1ctype'] == "4") {
		  $cell_phone1 = $EMC ['ecphone1c'];
		}	

		if ($EMC ['ecphone2atype'] == "2") {
		  $home_phone2 = $EMC ['ecphone2a'];
		}	
		if ($EMC ['ecphone2atype'] == "3") {
		  $work_phone2 = $EMC ['ecphone2a'];
		}	
		if ($EMC ['ecphone2atype'] == "4") {
		  $cell_phone2 = $EMC ['ecphone2a'];
		}	

		if ($EMC ['ecphone2btype'] == "2") {
		  $home_phone2 = $EMC ['ecphone2b'];
		}	
		if ($EMC ['ecphone2btype'] == "3") {
		  $work_phone2 = $EMC ['ecphone2b'];
		}	
		if ($EMC ['ecphone2btype'] == "4") {
		  $cell_phone2 = $EMC ['ecphone2b'];
		}	

		if ($EMC ['ecphone2ctype'] == "2") {
		  $home_phone2 = $EMC ['ecphone2c'];
		}	
		if ($EMC ['ecphone2ctype'] == "3") {
		  $work_phone2 = $EMC ['ecphone2c'];
		}	
		if ($EMC ['ecphone2ctype'] == "4") {
		  $cell_phone2 = $EMC ['ecphone2c'];
		}	

		return array (				
				"EContact"            	=> $EMC ['ecname1'] . ' ' . $EMC ['eclname1'],
				"ECaddress1"   		=> $EMC ['ecaddress1'],
				"ECaddress2"            => $EMC ['ecaddress21'],
				"ECrelationship"        => $EMC ['ecrelationship1'],
				"ECcphone"       	=> $cell_phone1,
				"EChphone"          	=> $home_phone1,
				"ECwphone"           	=> $work_phone1,
				"SecEContact"           => $EMC ['ecname2'] . ' ' . $EMC ['eclname2'],
				"SecECaddress1"   	=> $EMC ['ecaddress2'],
				"SecECaddress2"         => $EMC ['ecaddress22'],
				"SecECrelationship"     => $EMC ['ecrelationship2'],
				"SecECcphone"       	=> $cell_phone2,
				"SecEChphone"          	=> $home_phone2,
				"SecECwphone"           => $work_phone2
		);

	} // end OrgID
} // end function


$namespace = IRECRUIT_HOME . "webservices";
$server = new soap_server ();
$server->configureWSDL ( "iRecruitExporter" );
$server->wsdl->schemaTargetNamespace = $namespace;

// getAccessToken
$server->register ( "getAccessToken", array (
		"User" => "xsd:string",
		"Password" => "xsd:string" 
), array (
		"return" => "xsd:string" 
), "urn:iRecruit", "urn:iRecruit#configureAccessToken", "rpc", "encoded", "Fetches Access Token for URL access" );

// getDBConfig
$server->register ( "getDBConfig", array (
		'token' => 'xsd:string' 
), array (
		'return' => 'tns:returnDBConfig' 
), 'urn:iRecruit', 'urn:iRecruit#getDBConfig' );

$server->wsdl->addComplexType ( 'returnDBConfig', 'conplexType', 'struct', 'all', '', array (
		'orgid' => array (
				'name' => 'orgid',
				'type' => 'xsd:string' 
		),
		'exporttype' => array (
				'name' => 'exporttype',
				'type' => 'xsd:string' 
		),
		'dbtype' => array (
				'name' => 'dbtype',
				'type' => 'xsd:string' 
		),
		'connectionstring' => array (
				'name' => 'connectionstring',
				'type' => 'xsd:string' 
		),
		'databasename' => array (
				'name' => 'databasename',
				'type' => 'xsd:string' 
		),
		'username' => array (
				'name' => 'username',
				'type' => 'xsd:string' 
		),
		'verification' => array (
				'name' => 'verification',
				'type' => 'xsd:string' 
		),
		'syncconnectionstring' => array (
				'name' => 'syncconnectionstring',
				'type' => 'xsd:string' 
		),
		'syncdatabasename' => array (
				'name' => 'syncdatabasename',
				'type' => 'xsd:string' 
		),
		'syncusername' => array (
				'name' => 'syncusername',
				'type' => 'xsd:string' 
		),
		'syncverification' => array (
				'name' => 'syncverification',
				'type' => 'xsd:string' 
		),
		'uppercase' => array (
				'name' => 'uppercase',
				'type' => 'xsd:string' 
		),
		'filelocation' => array (
				'name' => 'filelocation',
				'type' => 'xsd:string' 
		),
		'auth' => array (
				'name' => 'code',
				'type' => 'xsd:string' 
		),
		'payrolldbs' => array (
				'name' => 'payrolldbs',
				'type' => 'xsd:string' 
		)
) );

// configureOnboardQuestions
$server->register ( "configureOnboardQuestions", array (
		"Token" => "xsd:string",
		"QuestionID" => "xsd:string",
		"Question" => "xsd:string",
		"Values" => "tns:OnboardValues" 
), array (
		"return" => "xsd:string" 
), "urn:iRecruit", "urn:iRecruit#configureOnboardQuestions", "rpc", "encoded", "Configures Onboarding Questions" );

$server->wsdl->addComplexType ( 'OnboardValues', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:OnboardValue[]' 
		) 
), 'tns:onboardValues' );

$server->wsdl->addComplexType ( 'OnboardValue', 'complexType', 'struct', 'all', '', array (
		'Code' => array (
				'name' => 'Code',
				'type' => 'xsd:string' 
		),
		'Description' => array (
				'name' => 'Description',
				'type' => 'xsd:string' 
		) 
) );

// createUserAccount
$server->register ( "createUserAccount", array (
		"Token" => "xsd:string",
		"UserID" => "xsd:string",
		"Password" => "xsd:string",
		"EmailAddress" => "xsd:string",
		"Role" => "xsd:string", 
		"FirstName" => "xsd:string", 
		"LastName" => "xsd:string", 
		"Address1" => "xsd:string", 
		"Address2" => "xsd:string", 
		"City" => "xsd:string", 
		"State" => "xsd:string", 
		"Zip" => "xsd:string", 
		"Phone" => "xsd:string",
		"Title" => "xsd:string" 
), array (
		"return" => "tns:returnUserAccount" 
), "urn:iRecruit", "urn:iRecruit#createUserAccount");

$server->wsdl->addComplexType ( 'returnUserAccount', 'conplexType', 'struct', 'all', '', array (
                'Status' => array (
                                'name' => 'Status',
                                'type' => 'xsd:string'
                ),
                'Message' => array (
                                'name' => 'Message',
                                'type' => 'xsd:string'
                ),
                'AccessCode' => array (
                                'name' => 'AccessCode',
                                'type' => 'xsd:string'
                )
) );

// getUsers
$server->register ( "getUsers", array (
		"Token" => "xsd:string" 
), array (
		"return" => "tns:returnUserList" 
), "urn:iRecruit", "urn:iRecruit#getUsers", "rpc", "encoded", "Returns a list of available Users as an array" );

$server->wsdl->addComplexType ( 'returnUserList', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:User[]' 
		) 
), 'tns:User' );

$server->wsdl->addComplexType ( 'User', 'complexType', 'struct', 'all', '', array (
		'UserID' => array (
				'name' => 'UserID',
				'type' => 'xsd:string' 
		),
		'FirstName' => array (
				'name' => 'FirstName',
				'type' => 'xsd:string' 
		),
		'LastName' => array (
				'name' => 'LastName',
				'type' => 'xsd:string' 
		),
		'EmailAddress' => array (
				'name' => 'EmailAddress',
				'type' => 'xsd:string' 
		), 
		'AccessCode' => array (
				'name' => 'AccessCode',
				'type' => 'xsd:string' 
		) 
) );

// getOnboardList
$server->register ( "getOnboardList", array (
		"Token" => "xsd:string" 
), array (
		"return" => "tns:returnApplicantList" 
), "urn:iRecruit", "urn:iRecruit#getOnboardList", "rpc", "encoded", "Returns a list of available ApplicationIDs and RequestIDs as an array" );

$server->wsdl->addComplexType ( 'returnApplicantList', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:Applicant[]' 
		) 
), 'tns:Applicant' );

$server->wsdl->addComplexType ( 'Applicant', 'complexType', 'struct', 'all', '', array (
		'ApplicationID' => array (
				'name' => 'ApplicationID',
				'type' => 'xsd:string' 
		),
		'RequestID' => array (
				'name' => 'RequestID',
				'type' => 'xsd:string' 
		),
		'ApplicantSortName' => array (
				'name' => 'ApplicantSortName',
				'type' => 'xsd:string' 
		) 
) );

// getCertifications
$server->register ( "getCertifications", array (
		"Token" => "xsd:string",
		"ApplicationID" => "xsd:string",
		"case" => "xsd:string" 
), array (
		"return" => "tns:CertificationRows" 
), "urn:iRecruit", "urn:iRecruit#getCertifications", "rpc", "encoded", "Returns Certifications entries as an array" );

$server->wsdl->addComplexType ( 'CertificationRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:CertificationData[]' 
		) 
), 'tns:CertificationData' );

$server->wsdl->addComplexType ( 'CertificationData', 'complexType', 'struct', 'all', '', array (
		'Specialty' => array (
				'name' => 'Specialty',
				'type' => 'xsd:string' 
		),
		'LicenseNo' => array (
				'name' => 'LicenseNo',
				'type' => 'xsd:string' 
		),
		'Effective' => array (
				'name' => 'Effective',
				'type' => 'xsd:string' 
		),
		'Expires' => array (
				'name' => 'Expires',
				'type' => 'xsd:string' 
		),
		'State' => array (
				'name' => 'State',
				'type' => 'xsd:string' 
		) 
) );

// getWorkExperience
$server->register ( "getWorkExperience", array (
		"Token" => "xsd:string",
		"ApplicationID" => "xsd:string",
		"case" => "xsd:string" 
), array (
		"return" => "tns:WorkExperienceRows" 
), "urn:iRecruit", "urn:iRecruit#getWorkExperience", "rpc", "encoded", "Returns Work Experience entries as an array" );

$server->wsdl->addComplexType ( 'WorkExperienceRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:WorkExperienceData[]' 
		) 
), 'tns:WorkExperienceData' );

$server->wsdl->addComplexType ( 'WorkExperienceData', 'complexType', 'struct', 'all', '', array (
		'CompanyName' => array (
				'name' => 'CompanyName',
				'type' => 'xsd:string' 
		),
		'Address' => array (
				'name' => 'Address',
				'type' => 'xsd:string' 
		),
		'City' => array (
				'name' => 'City',
				'type' => 'xsd:string' 
		),
		'State' => array (
				'name' => 'State',
				'type' => 'xsd:string' 
		),
		'PostalCode' => array (
				'name' => 'PostalCode',
				'type' => 'xsd:string' 
		),
		'DateofEmployment_From' => array (
				'name' => 'DateofEmployment_From',
				'type' => 'xsd:string' 
		),
		'DateofEmployment_To' => array (
				'name' => 'DateofEmployment_To',
				'type' => 'xsd:string' 
		),
		'Position' => array (
				'name' => 'Position',
				'type' => 'xsd:string' 
		),
		'StartingSalary' => array (
				'name' => 'StartingSalary',
				'type' => 'xsd:string' 
		),
		'StartingSalaryDefinition' => array (
				'name' => 'StartingSalaryDefinition',
				'type' => 'xsd:string' 
		),
		'Salary' => array (
				'name' => 'Salary',
				'type' => 'xsd:string' 
		),
		'SalaryDefinition' => array (
				'name' => 'SalaryDefinition',
				'type' => 'xsd:string' 
		),
		'LastSupervisor' => array (
				'name' => 'LastSupervisor',
				'type' => 'xsd:string' 
		),
		'Phone' => array (
				'name' => 'Phone',
				'type' => 'xsd:string' 
		),
		'Ok2Contact' => array (
				'name' => 'Ok2Contact',
				'type' => 'xsd:string' 
		),
		'CurrentPosition' => array (
				'name' => 'CurrentPosition',
				'type' => 'xsd:string' 
		),
		'DescribePosition' => array (
				'name' => 'DescribePosition',
				'type' => 'xsd:string' 
		),
		'ReasonForLeaving' => array (
				'name' => 'ReasonForLeaving',
				'type' => 'xsd:string' 
		),
		'Discharged' => array (
				'name' => 'Discharged',
				'type' => 'xsd:string' 
		),
		'DischargedReason' => array (
				'name' => 'DischargedReason',
				'type' => 'xsd:string' 
		) 
) );

// getSkills
$server->register ( "getSkills", array (
		"Token" => "xsd:string",
		"ApplicationID" => "xsd:string",
		"case" => "xsd:string" 
), array (
		"return" => "tns:SkillRows" 
), "urn:iRecruit", "urn:iRecruit#getSkills", "rpc", "encoded", "Returns Skill entries as an array" );

$server->wsdl->addComplexType ( 'SkillRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:SkillData[]' 
		) 
), 'tns:SkillData' );

$server->wsdl->addComplexType ( 'SkillData', 'complexType', 'struct', 'all', '', array (
		'Skill' => array (
				'name' => 'Skill',
				'type' => 'xsd:string' 
		),
		'Years' => array (
				'name' => 'Years',
				'type' => 'xsd:string' 
		),
		'Comment' => array (
				'name' => 'Comment',
				'type' => 'xsd:string' 
		) 
) );

// getEducation
$server->register ( "getEducation", array (
		"Token" => "xsd:string",
		"ApplicationID" => "xsd:string",
		"case" => "xsd:string" 
), array (
		"return" => "tns:EducationRows" 
), "urn:iRecruit", "urn:iRecruit#getEducation", "rpc", "encoded", "Returns Education entries as an array" );

$server->wsdl->addComplexType ( 'EducationRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:EducationData[]' 
		) 
), 'tns:EducationData' );

$server->wsdl->addComplexType ( 'EducationData', 'complexType', 'struct', 'all', '', array (
		'Attended' => array (
				'name' => 'Attended',
				'type' => 'xsd:string' 
		),
		'YearsCompleted' => array (
				'name' => 'YearsCompleted',
				'type' => 'xsd:string' 
		),
		'Graduated' => array (
				'name' => 'Graduated',
				'type' => 'xsd:string' 
		),
		'Year' => array (
				'name' => 'Year',
				'type' => 'xsd:string' 
		),
		'GPA' => array (
				'name' => 'GPA',
				'type' => 'xsd:string' 
		),
		'Degree' => array (
				'name' => 'Degree',
				'type' => 'xsd:string' 
		),
		'Major' => array (
				'name' => 'Major',
				'type' => 'xsd:string' 
		),
		'Minor' => array (
				'name' => 'Minor',
				'type' => 'xsd:string' 
		),
		'OtherDegree' => array (
				'name' => 'OtherDegree',
				'type' => 'xsd:string' 
		),
		'Phone' => array (
				'name' => 'Phone',
				'type' => 'xsd:string' 
		),
		'Address' => array (
				'name' => 'Address',
				'type' => 'xsd:string' 
		) 
) );

// getAttachments
$server->register ( "getAttachments", array (
		"Token"           =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string" 
        ), 
        array (
    		"return" => "tns:AttachmentRows" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#getAttachments", 
        "rpc", 
        "encoded", 
        "Returns Attachment links as an array"
);

$server->wsdl->addComplexType ( 'AttachmentRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:AttachmentData[]' 
		) 
        ), 
        'tns:AttachmentData' );

$server->wsdl->addComplexType ( 'AttachmentData', 'complexType', 'struct', 'all', '', array (
		'PurposeName' => array (
				'name' => 'PurposeName',
				'type' => 'xsd:string' 
		),
		'FileType' => array (
				'name' => 'FileType',
				'type' => 'xsd:string' 
		),
		'URLLink' => array (
				'name' => 'URLLink',
				'type' => 'xsd:string' 
		) 
) );

// getAADISVET
$server->register ( "getAADISVET", array (
		"Token"           =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string",
		"case"            =>  "xsd:string" 
        ), 
        array (
		  'return' => 'tns:AADISVET' 
        ), 
        'urn:iRecruit', 
        'urn:iRecruit#getAADISVET', 
        "rpc", 
        "encoded", 
        "Returns AA, DIS & VET Section Data" 
);

$server->wsdl->addComplexType ( 'AADISVET', 'conplexType', 'struct', 'all', '', array (
		'GenderSelection' => array (
			'name' => 'GenderSelection',
			'type' => 'xsd:string' 
		),
		'Race' => array (
			'name' => 'Race',
			'type' => 'xsd:string' 
		),
		'EthnicGroup' => array (
			'name' => 'EthnicGroup',
			'type' => 'xsd:string' 
		),
		'Disabled' => array (
			'name' => 'Disabled',
			'type' => 'xsd:string' 
		),
		'Veteran' => array (
			'name' => 'Veteran',
			'type' => 'xsd:string' 
		)
) );

// getAffirmativeAction
$server->register ( "getAffirmativeAction", array (
		"Token"           =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string",
		"case"            =>  "xsd:string" 
        ), 
        array (
		  'return' => 'tns:AffirmativeAction' 
        ), 
        'urn:iRecruit', 
        'urn:iRecruit#getAffirmativeAction', 
        "rpc", 
        "encoded", 
        "Returns Affirmative Action Information" 
);


$server->wsdl->addComplexType ( 'AffirmativeAction', 'conplexType', 'struct', 'all', '', array (
		'Gender' => array (
			'name' => 'Gender',
			'type' => 'xsd:string' 
		),
		'GenderSelection' => array (
			'name' => 'GenderSelection',
			'type' => 'xsd:string' 
		),
		'Race' => array (
			'name' => 'Race',
			'type' => 'xsd:string' 
		),
		'Ethnicity' => array (
			'name' => 'Ethnicity',
			'type' => 'xsd:string' 
		),
		'EthnicGroup' => array (
			'name' => 'EthnicGroup',
			'type' => 'xsd:string' 
		),
		'Disabled' => array (
			'name' => 'Disabled',
			'type' => 'xsd:string' 
		),
		'Veteran' => array (
			'name' => 'Veteran',
			'type' => 'xsd:string' 
		),
		'VeteranStatus' => array (
			'name' => 'VeteranStatus',
			'type' => 'xsd:string' 
		),
		'VeteranDate' => array (
			'name' => 'VeteranDate',
			'type' => 'xsd:string' 
		) 
) );

// getEmergencyContacts
$server->register ( "getEmergencyContacts", array (
		"Token"           =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string",
		"RequestID"   	  =>  "xsd:string",
		"case"            =>  "xsd:string" 
        ), 
        array (
		  'return' => 'tns:EmergencyContacts' 
        ), 
        'urn:iRecruit', 
        'urn:iRecruit#getEmergencyContacts', 
        "rpc", 
        "encoded", 
        "Returns Emergency Contact Information" 
);

$server->wsdl->addComplexType ( 'EmergencyContacts', 'conplexType', 'struct', 'all', '', array (
		'EContact' => array (
			'name' => 'EContact',
			'type' => 'xsd:string' 
		),
		'ECaddress1' => array (
			'name' => 'ECaddress1',
			'type' => 'xsd:string' 
		),
		'ECaddress2' => array (
			'name' => 'ECaddress2',
			'type' => 'xsd:string' 
		),
		'ECrelationship' => array (
			'name' => 'ECrelationship',
			'type' => 'xsd:string' 
		),
		'ECcphone' => array (
			'name' => 'ECcphone',
			'type' => 'xsd:string' 
		),
		'EChphone' => array (
			'name' => 'EChphone',
			'type' => 'xsd:string' 
		),
		'ECwphone' => array (
			'name' => 'ECwphone',
			'type' => 'xsd:string' 
		),
		'SecEContact' => array (
			'name' => 'SecEContact',
			'type' => 'xsd:string' 
		),
		'SecECaddress1' => array (
			'name' => 'SecECaddress1',
			'type' => 'xsd:string' 
		),
		'SecECaddress2' => array (
			'name' => 'SecECaddress2',
			'type' => 'xsd:string' 
		),
		'SecECrelationship' => array (
			'name' => 'SecECrelationship',
			'type' => 'xsd:string' 
		),
		'SecECcphone' => array (
			'name' => 'SecECcphone',
			'type' => 'xsd:string' 
		),
		'SecEChphone' => array (
			'name' => 'SecEChphone',
			'type' => 'xsd:string' 
		),
		'SecECwphone' => array (
			'name' => 'SecECwphone',
			'type' => 'xsd:string' 
		)
) );

// markApplicant
$server->register ( "markApplicant", array (
    		"Token"           =>  "xsd:string",
    		"ApplicationID"   =>  "xsd:string",
    		"RequestID"       =>  "xsd:string",
    		"Status"          =>  "xsd:string" 
        ), 
        array (
    		"return" => "xsd:string" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#markApplicant", 
        "rpc", 
        "encoded", 
        "Marks the Applicant for 'download' or 'ready'"
);

// markOnboard
$server->register ( "markOnboard", array (
    		"Token"           =>  "xsd:string",
    		"ApplicationID"   =>  "xsd:string",
    		"RequestID"       =>  "xsd:string",
    		"Status"          =>  "xsd:string",
    		"OnboardID"       =>  "xsd:string",
    		"ReturnData"      =>  "xsd:string"
        ), 
        array (
    		"return" => "xsd:string" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#markOnboard", 
        "rpc", 
        "encoded", 
        "Marks the Onboard process for status"
);

// getExporterConfiguration
$server->register ( "getExporterConfiguration", array (
		"Token" => "xsd:string" 
        ), 
        array (
            "return" => "tns:ExporterConfig" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#getExporterConfiguration", 
        "rpc", 
        "encoded", 
        "Returns a list of available export configurations as an array"
);

$server->wsdl->addComplexType ( 'ExporterConfig', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
			'ref' => 'SOAP-ENC:arrayType',
			'wsdl:arrayType' => 'tns:Exporter[]' 
		)), 
        'tns:Exporter'
);

$server->wsdl->addComplexType ( 'Exporter', 'complexType', 'struct', 'all', '', array (
		'ExporterID' => array (
				'name' => 'ExporterID',
				'type' => 'xsd:string' 
		),
		'ExportName' => array (
				'name' => 'ExportName',
				'type' => 'xsd:string' 
		) 
) );

// getExporterApplicantList
$server->register ( "getExporterApplicantList", array (
		"Token"               =>  "xsd:string",
		"StartDate"           =>  "xsd:string",
		"EndDate"             =>  "xsd:string",
		"DataManagerStatus"   =>  "xsd:string" 
        ), array (
		  "return" => "tns:ExporterRows" 
        ),
        "urn:iRecruit", 
        "urn:iRecruit#getExporterApplicantList", 
        "rpc", 
        "encoded", 
        "Returns Exporter Data List as an array"
);

$server->wsdl->addComplexType ( 'ExporterRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:ExporterList[]' 
		) ), 'tns:ExporterList' );

$server->wsdl->addComplexType ( 'ExporterList', 'complexType', 'struct', 'all', '', array (
		'ApplicationID' => array (
				'name' => 'ApplicationID',
				'type' => 'xsd:string' 
		),
		'RequestID' => array (
				'name' => 'RequestID',
				'type' => 'xsd:string' 
		),
		'OnboardFormID' => array (
				'name' => 'OnboardFormID',
				'type' => 'xsd:string' 
		),
		'EntryDate' => array (
				'name' => 'EntryDate',
				'type' => 'xsd:string' 
		),
		'ApplicantSortName' => array (
				'name' => 'ApplicantSortName',
				'type' => 'xsd:string' 
		),
		'JobTitle' => array (
				'name' => 'JobTitle',
				'type' => 'xsd:string' 
		),
		'DataManagerStatus' => array (
				'name' => 'DataManagerStatus',
				'type' => 'xsd:string' 
		)
) );

// getExportList
$server->register ( "getExportList", array (
                                            "Token"             => "xsd:string",
                                            "ExporterID"        => "xsd:string",
                                            "StartDate"         => "xsd:string",
                                            "EndDate"           => "xsd:string",
                                            "ApplicationID"     => "xsd:string",
                                            "RequestID"         => "xsd:string",
                                            "Case"              => "xsd:string",
                                            "NameValue"         => "xsd:string",
                                            "DataManagerStatus" => "xsd:string"
                                    ), 
                                    array (
                                            "return"            => "tns:ExportListRows" 
                                    ), 
                                    "urn:iRecruit", 
                                    "urn:iRecruit#getExportList", 
                                    "rpc", 
                                    "encoded", 
                                    "Returns Export Data List as an array"
                    );

$server->wsdl->addComplexType ( 'ExportListRows', 'complexType', 'array', '', 'SOAP-ENC:Array', 
        array (), 
        array (
        	array (
        			'ref' => 'SOAP-ENC:arrayType',
        			'wsdl:arrayType' => 'tns:ExportListData[]' 
        	)
        ), 
        'tns:ExportListData' 
        );

$server->wsdl->addComplexType ( 'ExportListData', 'complexType', 'struct', 'all', '', array (
		'ApplicationID' => array (
				'name' => 'ApplicationID',
				'type' => 'xsd:string' 
		),
		'RequestID' => array (
				'name' => 'RequestID',
				'type' => 'xsd:string' 
		),
		'ApplicantSortName' => array (
				'name' => 'ApplicantSortName',
				'type' => 'xsd:string' 
		),
		'JobTitle' => array (
				'name' => 'JobTitle',
				'type' => 'xsd:string' 
		),
		'DataManagerStatus' => array (
				'name' => 'DataManagerStatus',
				'type' => 'xsd:string' 
		),
		'OnboardFormID' => array (
				'name' => 'OnboardFormID',
				'type' => 'xsd:string' 
		)
) );

// getExporterData
$server->register ( "getExporterData", array (
		"Token"           =>  "xsd:string",
		"ExporterID"      =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string",
		"RequestID"       =>  "xsd:string",
		"Case"            =>  "xsd:string",
		"NameValue"       =>  "xsd:string"
        ), 
        array (
		  "return" => "tns:ExporterDataRows" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#getExporterData", 
        "rpc", 
        "encoded", 
        "Returns Export Data as an array" 
);

$server->wsdl->addComplexType ( 'ExporterDataRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), 
        array (
    		array (
    				'ref' => 'SOAP-ENC:arrayType',
    				'wsdl:arrayType' => 'tns:ExporterValueData[]' 
    		) 
        ), 
        'tns:ExporterValueData'
);

$server->wsdl->addComplexType ( 'ExporterValueData', 'complexType', 'struct', 'all', '', array (
		'ExporterData' => array (
				'name' => 'ExporterData',
				'type' => 'xsd:string' 
		)
) );

// getExport
$server->register ( "getExport", array (
		"Token"               =>  "xsd:string",
		"ExporterID"          =>  "xsd:string",
		"StartDate"           =>  "xsd:string",
		"EndDate"             =>  "xsd:string",
		"ApplicationID"       =>  "xsd:string",
		"RequestID"           =>  "xsd:string",
		"Case"                =>  "xsd:string",
		"NameValue"           =>  "xsd:string",
		"DataManagerStatus"   =>  "xsd:string" 
        ), array (
    		"return" => "tns:ExportRows" 
        ), 
        "urn:iRecruit", "urn:iRecruit#getExport", "rpc", "encoded", "Returns Export Data as an array"
);

$server->wsdl->addComplexType ( 'ExportRows', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:ExportData[]' 
		) 
        ),
        'tns:ExportData' 
);

$server->wsdl->addComplexType ( 'ExportData', 'complexType', 'struct', 'all', '', array (
		'ApplicationID' => array (
				'name' => 'ApplicationID',
				'type' => 'xsd:string' 
		),
		'RequestID' => array (
				'name' => 'RequestID',
				'type' => 'xsd:string' 
		),
		'ApplicantSortName' => array (
				'name' => 'ApplicantSortName',
				'type' => 'xsd:string' 
		),
		'JobTitle' => array (
				'name' => 'JobTitle',
				'type' => 'xsd:string' 
		),
		'DataManagerStatus' => array (
				'name' => 'DataManagerStatus',
				'type' => 'xsd:string' 
		),
		'ExporterData' => array (
				'name' => 'ExporterData',
				'type' => 'xsd:string' 
		),
		'ExporterValues' => array (
				'name' => 'ExporterValues',
				'type' => 'xsd:string' 
		) 
) );

// getFederalStateFormsList
$server->register ( "getFederalStateFormsList", array (
		"Token"           => "xsd:string",
		"ApplicationID"   => "xsd:string",
		"RequestID"       => "xsd:string" 
        ), 
        array (
		  "return" => "tns:PreFilledFormsList" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#getFederalStateFormsList", 
        "rpc", 
        "encoded", 
        "Returns a list of available Federal and State Forms as an array"
);

$server->wsdl->addComplexType ( 'PreFilledFormsList', 'complexType', 'array', '', 'SOAP-ENC:Array', array (), array (
		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:PreFilledForm[]' 
		) 
), 'tns:PreFilledForm' );

$server->wsdl->addComplexType ( 'PreFilledForm', 'complexType', 'struct', 'all', '', array (
		'UniqueID' => array (
				'name' => 'UniqueID',
				'type' => 'xsd:string' 
		),
		'TypeForm' => array (
				'name' => 'TypeForm',
				'type' => 'xsd:string' 
		),
		'FormName' => array (
				'name' => 'FormName',
				'type' => 'xsd:string' 
		),
		'LastUpdated' => array (
				'name' => 'LastUpdated',
				'type' => 'xsd:string' 
		),
		'Status' => array (
				'name' => 'Status',
				'type' => 'xsd:string' 
		) 
) );

// getTaxFormData
$server->register ( "getTaxFormData", array (
		"Token"           =>  "xsd:string",
		"PreFilledFormID" =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string",
		"RequestID"       =>  "xsd:string",
		"Case"            =>  "xsd:string" 
        ), array (
            "return"        =>  "tns:TaxFormRow" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#getTaxFormData", 
        "rpc", 
        "encoded", 
        "Returns values of Federal and State Forms as an array"
);

$server->wsdl->addComplexType ( 'TaxFormRow', 'complexType', 'array', '', 'SOAP-ENC:Array', 
        array (), 
        array (
    		array (
				'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:TaxData[]' 
    		) 
        ), 
        'tns:TaxData'
);

$server->wsdl->addComplexType ( 'TaxData', 'complexType', 'struct', 'all', '', array (
		'Scope' => array (
				'name' => 'Scope',
				'type' => 'xsd:string' 
		),
		'FilingStatus' => array (
				'name' => 'FilingStatus',
				'type' => 'xsd:string' 
		),
		'FilingType' => array (
				'name' => 'FilingType',
				'type' => 'xsd:string' 
		),
		'FilingTypeValue' => array (
				'name' => 'FilingTypeValue',
				'type' => 'xsd:string' 
		),
		'AdditionalAmount' => array (
				'name' => 'AdditionalAmount',
				'type' => 'xsd:string' 
		),
		'Condition' => array (
				'name' => 'Condition',
				'type' => 'xsd:string' 
		),
		'ConditionValue' => array (
				'name' => 'ConditionValue',
				'type' => 'xsd:string' 
		),
		'SecondaryCondition' => array (
				'name' => 'SecondaryCondition',
				'type' => 'xsd:string' 
		),
		'SecondaryConditionValue' => array (
				'name' => 'SecondaryConditionValue',
				'type' => 'xsd:string' 
		) 
) );

// getI9Data
$server->register ( "geti9Data", array (
		"Token"           =>  "xsd:string",
		"ApplicationID"   =>  "xsd:string",
		"RequestID"       =>  "xsd:string",
		"Case"            =>  "xsd:string" 
        ),
        array (
		  "return"          =>  "tns:i9Data" 
        ), 
        "urn:iRecruit", 
        "urn:iRecruit#geti9Data", 
        "rpc", "encoded", 
        "Returns i9 Form Data for an applicant" 
);

$server->wsdl->addComplexType ( 'i9Data', 'complexType', 'struct', 'all', '', array (
		'LastName' => array (
				'name' => 'LastName',
				'type' => 'xsd:string' 
		),
		'FirstName' => array (
				'name' => 'FirstName',
				'type' => 'xsd:string' 
		),
		'MiddleName' => array (
				'name' => 'MiddleName',
				'type' => 'xsd:string' 
		),
		'Address1' => array (
				'name' => 'Address1',
				'type' => 'xsd:string' 
		),
		'Address2' => array (
				'name' => 'Address2',
				'type' => 'xsd:string' 
		),
		'City' => array (
				'name' => 'City',
				'type' => 'xsd:string' 
		),
		'State' => array (
				'name' => 'State',
				'type' => 'xsd:string' 
		),
		'ZipCode' => array (
				'name' => 'ZipCode',
				'type' => 'xsd:string' 
		),
		'OtherNamesUsed' => array (
				'name' => 'OtherNamesUsed',
				'type' => 'xsd:string' 
		),
		'EmailAddress' => array (
				'name' => 'EmailAddress',
				'type' => 'xsd:string' 
		),
		'TelephoneNumber' => array (
				'name' => 'TelephoneNumber',
				'type' => 'xsd:string' 
		),
		'DOB' => array (
				'name' => 'DOB',
				'type' => 'xsd:string' 
		),
		'SSN' => array (
				'name' => 'SSN',
				'type' => 'xsd:string' 
		),
		'Status' => array (
				'name' => 'Status',
				'type' => 'xsd:string' 
		),
		'AlienExpDate' => array (
				'name' => 'AlienExpDate',
				'type' => 'xsd:string' 
		),
		'AlienNumber' => array (
				'name' => 'AlienNumber',
				'type' => 'xsd:string' 
		),
		'FormI94' => array (
				'name' => 'FormI94',
				'type' => 'xsd:string' 
		),
		'ForeignPassport' => array (
				'name' => 'ForeignPassport',
				'type' => 'xsd:string' 
		),
		'PassportCountry' => array (
				'name' => 'PassportCountry',
				'type' => 'xsd:string' 
		),
		'EmploymentDate' => array (
				'name' => 'EmploymentDate',
				'type' => 'xsd:string' 
		),
		'NewHire' => array (
				'name' => 'NewHire',
				'type' => 'xsd:string' 
		),
		'ValidationDate' => array (
				'name' => 'ValidationDate',
				'type' => 'xsd:string' 
		) 
) );

// getI9mData
$server->register ( "geti9mData", array (
		"Token" => "xsd:string",
		"ApplicationID" => "xsd:string",
		"RequestID" => "xsd:string",
		"Case" => "xsd:string" 
        ), array (
    		"return" => "tns:i9mData" 
        ),
        "urn:iRecruit", 
        "urn:iRecruit#geti9mData", 
        "rpc", 
        "encoded", 
        "Returns i9 Form Data for an applicant"
);

$server->wsdl->addComplexType ( 'i9mData', 'complexType', 'struct', 'all', '', array (
		'EmploymentDate' => array (
				'name' => 'EmploymentDate',
				'type' => 'xsd:string' 
		),
		'NewHire' => array (
				'name' => 'NewHire',
				'type' => 'xsd:string' 
		),
		'ListATitle' => array (
				'name' => 'ListATitle',
				'type' => 'xsd:string' 
		),
		'ListADocumentNumber' => array (
				'name' => 'ListADocumentNumber',
				'type' => 'xsd:string' 
		),
		'ListAAuthority' => array (
				'name' => 'ListAAuthority',
				'type' => 'xsd:string' 
		),
		'ListAExpDate' => array (
				'name' => 'ListAExpDate',
				'type' => 'xsd:string' 
		),
		'ListBTitle' => array (
				'name' => 'ListBTitle',
				'type' => 'xsd:string' 
		),
		'ListBSupportingDocument' => array (
				'ListBSupportingDocument' => 'State',
				'type' => 'xsd:string' 
		),
		'ListBDocumentNumber' => array (
				'name' => 'ListBDocumentNumber',
				'type' => 'xsd:string' 
		),
		'ListBAuthority' => array (
				'name' => 'ListBAuthority',
				'type' => 'xsd:string' 
		),
		'ListBExpDate' => array (
				'name' => 'ListBExpDate',
				'type' => 'xsd:string' 
		),
		'ListCTitle' => array (
				'name' => 'ListCTitle',
				'type' => 'xsd:string' 
		),
		'ListCDocumentNumber' => array (
				'ListCDocumentNumber' => 'ZipCode',
				'type' => 'xsd:string' 
		),
		'ListCAuthority' => array (
				'name' => 'ListCAuthority',
				'type' => 'xsd:string' 
		),
		'ListCExpDate' => array (
				'name' => 'ListCExpDate',
				'type' => 'xsd:string' 
		),
		'ValidationDate' => array (
				'name' => 'ValidationDate',
				'type' => 'xsd:string' 
		) 
) );

$POST_DATA = file_get_contents('php://input');

$server->service ( $POST_DATA );

exit ();
?>
