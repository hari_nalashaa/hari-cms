<?php
require_once "../irecruitdb.inc";
require_once VENDOR_DIR . "econea/nusoap/src/nusoap.php";

/*
 * Build for CRF to incorporate listing locally into their website
 * Not used as of 1/1/2023
 * */


$nusoap_base_obj = new nusoap_base();
$nusoap_base_obj->setGlobalDebugLevel ( 0 );

function login($apikey) {

	$query	= "SELECT OrgID, MultiOrgID FROM IrecruitAPIkeys WHERE APIkey = :APIkey";
	$params =   array(':APIkey'    =>  $apikey);
	$KEY	=   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
	
	return array($KEY[0]['OrgID'],$KEY[0]['MultiOrgID']);

} // end function


function getCategoryConfiguration($apikey) {
	
	list($OrgID,$MultiOrgID) = login ( $apikey);

	$DATA       =   array ();
	$DATA [0]   =   array (
                              "CategoryID"    	 =>  "",
                              "Category"         =>  "",
                              "SelectionID"	 =>  "",
                              "Selection"      	 =>  "",
                              "SetCount"      =>  ""
                              );

	if ($OrgID == "") {

	  $DATA [0] ['CategoryID']   =   "ERROR";
	  $DATA [0] ['Category']     =   "Your API key is invalid or has expired.";

	} else {


	$query	= "SELECT OrganizationLevelData.OrgLevelID as CategoryID,";
	$query .= " OrganizationLevels.OrganizationLevel as Category,";
	$query .= " OrganizationLevelData.SelectionOrder as SelectionID,";
	$query .= " OrganizationLevelData.CategorySelection as Selection";
	$query .= " FROM OrganizationLevelData";
	$query .= " JOIN OrganizationLevels";
	$query .= " ON OrganizationLevelData.OrgID = OrganizationLevels.OrgID";
	$query .= " AND OrganizationLevelData.MultiOrgID = OrganizationLevels.MultiOrgID";
	$query .= " AND OrganizationLevelData.OrgLevelID = OrganizationLevels.OrgLevelID";
	$query .= " WHERE OrganizationLevelData.OrgID = :OrgID";
	$query .= " AND OrganizationLevelData.MultiOrgID = :MultiOrgID";
	$params =   array(':OrgID'    =>  $OrgID, ':MultiOrgID'    =>  $MultiOrgID);
	$LISTINGS =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

	if (count($LISTINGS) < 1) {
	  $DATA [0] ['CategoryID']   =   "NO DATA";
	  $DATA [0] ['Category']     =   "";
	}

	$i=0;
	foreach ( $LISTINGS AS $L) {

		$query  = "SELECT count(*) AS CNT";
		$query .= " FROM Requisitions";
		$query .= " WHERE OrgID = :OrgID and MultiOrgID = :MultiOrgID";
		$query .= " AND Active = 'Y' AND NOW() BETWEEN PostDate AND ExpireDate ";
		$query .= " AND RequestID in (";
		$query .= "SELECT RequestID FROM RequisitionOrgLevels WHERE OrgID = :OrgIDr";
		$query .= " AND OrgLevelID = :OrgLevelID AND SelectionOrder = :SelectionOrder";
		$query .= ")";
		$params =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':OrgIDr'=>$OrgID, ':OrgLevelID'=>$L['CategoryID'], ':SelectionOrder'=>$L['SelectionID']);
		$JOBS   =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));
		$cnt = $JOBS[0]['CNT'];

		$DATA [$i] = array (
                            "CategoryID"     	=>  $L['CategoryID'],
                            "Category"          =>  $L['Category'],
                            "SelectionID" 	=>  $L['SelectionID'],
                            "Selection"         =>  $L['Selection'],
                            "SetCount"          =>  $cnt
		);

	$i ++;
	} // end foreach

	} // end OrgID

	return $DATA;

} // end function


function getJobListings($apikey,$CategoryID,$SelectionID,$Internal) {
	
	list($OrgID,$MultiOrgID) = login ( $apikey);

	$DATA       =   array ();
	$DATA [0]   =   array (
                              "Title"	         =>  "",
                              "JobURL"		 =>  "",
                              "HighlightImage"	 =>  "",
                              "HTML"      	 =>  ""
                              );

	if ($OrgID == "") {

	  $DATA [0] ['Title']      =   "ERROR";
	  $DATA [0] ['JobURL']     =   "Your API key is invalid or has expired.";

	} else {

        $InternalVCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );

	$new_job_image = '';
	$hot_job_image = '';

	$highlight_req_settings = G::Obj('HighlightRequisitions')->getHighlightRequisitionSettings($OrgID);

	if($highlight_req_settings['HighlightNewJob'] == "Yes") {

	    $new_job_days = $highlight_req_settings['NewJobDays'];
	    $new_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['NewJobIcon']);

	    if($new_job_icon != "") {
	        $new_job_image = IRECRUIT_HOME . 'vault/highlight/' . $new_job_icon;
	    }
	}

	if($highlight_req_settings['HighlightHotJob'] == "Yes") {

	    $hot_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['HotJobIcon']);

	    if($hot_job_icon != "") {
	        $hot_job_image = IRECRUIT_HOME . 'vault/highlight/' . $hot_job_icon;
	    }
	}

	$query  = "SELECT RequestID, Title";
	if($new_job_days > 0) {
            $query .= ", IF((Highlight != 'Hot' AND DATEDIFF(NOW(), DATE(PostDate)) <= $new_job_days), 'New', Highlight) AS Highlight";
        }
        else {
            $query .= ", Highlight";
        }
	$query .= " FROM Requisitions";
	$query .= " WHERE OrgID = :OrgID and MultiOrgID = :MultiOrgID";
	$query .= " AND Active = 'Y' AND NOW() BETWEEN PostDate AND ExpireDate ";
	if ($Internal == 'Y') {
	  $query .= " AND (PresentOn = 'INTERNALONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	} else {
	  $query .= " AND (PresentOn = 'PUBLICONLY' OR PresentOn = 'INTERNALANDPUBLIC')";
	}
	$query .= " AND RequestID in (";
	$query .= "SELECT RequestID FROM RequisitionOrgLevels WHERE OrgID = :OrgIDr";
	$query .= " AND OrgLevelID = :OrgLevelID AND SelectionOrder = :SelectionOrder";
	$query .= ")";
	$query .= " ORDER BY Title";
	$params =   array(':OrgID'=>$OrgID, ':MultiOrgID'=>$MultiOrgID, ':OrgIDr'=>$OrgID, ':OrgLevelID'=>$CategoryID, ':SelectionOrder'=>$SelectionID);
	$JOBS   =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));


	if (count($JOBS) < 1) {
	  $DATA [0] ['Title']      =   "ERROR";
	  $DATA [0] ['JobURL']     =   "NO DATA";
	} else {

	  $i=0;
	  foreach ( $JOBS AS $J) {

		$title = $J['Title'];

		$url  = PUBLIC_HOME;
		$url .= "index.php";
		$url .= "?OrgID=" . $OrgID;

		if ($MultiOrgID != "") {
		  $url .= "&MultiOrgID=" . $MultiOrgID;
		}

		$url .= "&RequestID=" . $J['RequestID'];

		if ($Internal == "Y") {
		  if ($InternalVCode) {
                    $url .= '&InternalVCode=' . $InternalVCode;
                  }
		}

                $highlight = "";
		if ($J['Highlight'] == "New") {
                     $highlight = $new_job_image;
                } 
		else if($J['Highlight'] == "Hot") {
                     $highlight = $hot_job_image;
                }

		$html = "<div id=\"joblink\">";

		$html .= "<div class=\"title\">";
		$html .= "<a href=\"";
		$html .= $url;
		$html .= "\">";
		$html .= $title;
		$html .= "</a>";
		$html .= "</div>";

		if ($highlight != "") {
		  $html .= "<div class=\"image\">";
		  $html .= "<img src=\"";
		  $html .= $highlight;
		  $html .= "\">";
		  $html .= "</div>";
		}

		$html .= "</div>";


		$DATA [$i] = array (
                            "Title"          	=>  $title,
                            "JobURL" 		=>  $url,
                            "HighlightImage" 	=>  $highlight,
                            "HTML"         	=>  $html
		);

	  $i ++;
	  } // end foreach

	} // end if Jobs
	} // end OrgID

	return $DATA;

} // end function

$namespace = IRECRUIT_HOME . "webservices";
$server = new soap_server ();
$server->configureWSDL ( "iRecruitListings" );
$server->wsdl->schemaTargetNamespace = $namespace;

// getCategoryConfiguration
$server->register ( "getCategoryConfiguration", array (
                                            "APIkey"             => "xsd:string"
                                    ), 
                                    array (
                                            "return"            => "tns:CategoryRows" 
                                    ), 
                                    "urn:iRecruit", 
                                    "urn:iRecruit#getCategoryConfiguration", 
                                    "rpc", 
                                    "encoded", 
                                    "Returns Configured categories as an array"
                    );

$server->wsdl->addComplexType ( 'CategoryRows', 'complexType', 'array', '', 'SOAP-ENC:Array', 
        array (), 
        array (
        	array (
        			'ref' => 'SOAP-ENC:arrayType',
        			'wsdl:arrayType' => 'tns:CategoryData[]' 
        	)
        ), 
        'tns:CategoryData' 
        );

$server->wsdl->addComplexType ( 'CategoryData', 'complexType', 'struct', 'all', '', array (
		'CategoryID' => array (
				'name' => 'CategoryID',
				'type' => 'xsd:string' 
		),
		'Category' => array (
				'name' => 'Category',
				'type' => 'xsd:string' 
		),
		'SelectionID' => array (
				'name' => 'SelectionID',
				'type' => 'xsd:string' 
		),
		'Selection' => array (
				'name' => 'Selection',
				'type' => 'xsd:string' 
		),
		'SetCount' => array (
				'name' => 'SetCount',
				'type' => 'xsd:string' 
		)
) );


// getJobListings
$server->register ( "getJobListings", array (
                                            "APIkey"             => "xsd:string",
                                            "CategoryID"         => "xsd:string",
                                            "SelectionID"        => "xsd:string",
                                            "Internal"           => "xsd:string"
                                    ), 
                                    array (
                                            "return"            => "tns:ActiveJobRows" 
                                    ), 
                                    "urn:iRecruit", 
                                    "urn:iRecruit#getJobListings", 
                                    "rpc", 
                                    "encoded", 
                                    "Returns Active jobs assigned to a selectionid in an array"
                    );

$server->wsdl->addComplexType ( 'ActiveJobRows', 'complexType', 'array', '', 'SOAP-ENC:Array', 
        array (), 
        array (
        	array (
        			'ref' => 'SOAP-ENC:arrayType',
        			'wsdl:arrayType' => 'tns:ActiveJobData[]' 
        	)
        ), 
        'tns:ActiveJobData' 
        );

$server->wsdl->addComplexType ( 'ActiveJobData', 'complexType', 'struct', 'all', '', array (
		'Title' => array (
				'name' => 'Title',
				'type' => 'xsd:string' 
		),
		'JobURL' => array (
				'name' => 'URL',
				'type' => 'xsd:string' 
		),
		'HighlightImage' => array (
				'name' => 'HighlightImage',
				'type' => 'xsd:string' 
		),
		'HTML' => array (
				'name' => 'HTML',
				'type' => 'xsd:string' 
		)
) );

$POST_DATA = file_get_contents('php://input');

$server->service ( $POST_DATA );


exit ();
?>
