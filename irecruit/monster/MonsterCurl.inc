<?php
$jobref = rand () . time ();
$date = strtotime ( date ( 'd-m-Y' ) );
$timestamp = gmDate ( "Y-m-d\TH:i:s.z\Z", $date );

if (! isset ( $_REQUEST['MultiOrgID'] ) || $_REQUEST['MultiOrgID'] == "" || is_null ( $_REQUEST['MultiOrgID'] )) {
	$MultiOrgID = "";
}	
else {
	$MultiOrgID = $_REQUEST['MultiOrgID'];
}
	
// Get Monster Detail Information
$requisitioninfo = $RequisitionsObj->getReqDetailInfo ( "*", $_REQUEST ['OrgID'], $MultiOrgID, $_REQUEST ['RequestID'] );
$JobRefId = $_REQUEST ['RequestID'];

// Get Columns
$columns = "OrganizationName, OrganizationDescription, Address1, Address2, City, State, ZipCode, Country";
$roworginfo = $OrganizationDetailsObj->getOrganizationInformation ( $_REQUEST ['OrgID'], $MultiOrgID, $columns );

/**
 * Get User Information
 */
$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $requisitioninfo ['Owner'], "*" );

$MonsterRecords = $MonsterObj->getMonsterInfoByOrgID ( $OrgID, $MultiOrgID );
$MonsterInfo = $MonsterRecords ['row'];
$FlagAwm = $MonsterRecords ['flagawm'];

// This is the default configuration value
if ($MonsterRecords ['Duration'] != "")
	$Duration = $MonsterRecords ['Duration'];
	
// This is overriden value, if incase this doesn't exist default one will be taken
if ($requisitioninfo ['Duration'] != "" && $requisitioninfo ['Duration'] > 0)
	$Duration = $requisitioninfo ['Duration'];
	
// Generating, packing and encoding a random number
$prefix = gethostname ();
$nonce = base64_encode ( substr ( md5 ( uniqid ( $prefix . '_', true ) ), 0, 16 ) );

$user_id = $MonsterInfo ['RecruiterReferenceUserName'];
$password = $MonsterInfo ['Password'];
// $user_id = 'xtestxftp'; //'xrtpjobsx01'
// $password = 'ftp12345'; //'rtp987654'

// Set where condition
$where = array (
		"OrgID = :OrgID",
		"MultiOrgID = :MultiOrgID",
		"RequestID = :RequestID" 
);
// Set parameters
$params = array (
		":OrgID" => $OrgID,
		":MultiOrgID" => $REQS ['MultiOrgID'],
		":RequestID" => $REQS ['RequestID'] 
);
// Get Post Requisition Information
$respostinfo = $RequisitionsObj->getPostRequisitionsInformation ( "PostingId", $where, "", array (
		$params 
) );
// Get Posting Information
$rowpostinfo = $respostinfo ['results'] [0];

$postinfocount = $respostinfo ['count'];
$PostingId = $rowpostinfo ['PostingId'];

//If PostingId is null make it empty
if(is_null($PostingId)) $PostingId = '';

if ($requisitioninfo ['MonsterJobPostType'] == "POST")
	$DeliveryMethod = '1';
else if ($requisitioninfo ['MonsterJobPostType'] == "REQUEST")
	$DeliveryMethod = '3';
if ($DeliveryMethod == "") {
	$DeliveryMethod = ($MonsterInfo ['PostType'] == "POST") ? '1' : '3';
}

if ($PostingId == "" || ! isset ( $PostingId )) {
	
	if ($requisitioninfo ['MonsterJobCategory'] == 0 || $requisitioninfo ['MonsterJobCategory'] == "")
		$requisitioninfo ['MonsterJobCategory'] = '11';
	if ($requisitioninfo ['MonsterJobOccupation'] == 0 || $requisitioninfo ['MonsterJobOccupation'] == "")
		$requisitioninfo ['MonsterJobOccupation'] = '11892';
	if ($requisitioninfo ['MonsterJobIndustry'] == 0 || $requisitioninfo ['MonsterJobIndustry'] == "")
		$requisitioninfo ['MonsterJobIndustry'] = '0';
	
	$monster_job_inventory_type = '';
	if ($_REQUEST ['job_inventory_type'] != "transactional")
		$monster_job_inventory_type = ' inventoryType="' . $_REQUEST ['job_inventory_type'] . '"';
	
	$vendorfield = $OrgID . "*" . $MultiOrgID;
	if ($MultiOrgID != "") {
		$monsterredirectlink = 'jobRequest.php?OrgID=' . $OrgID . '&source=MONST&MultiOrgID=' . $MultiOrgID . '&RequestID=' . $JobRefId;
	} else {
		$monsterredirectlink = 'jobRequest.php?OrgID=' . $OrgID . '&source=MONST&RequestID=' . $JobRefId;
	}
	
	if (isset ( $_REQUEST ['job_has_bold'] ) && ($_REQUEST ['job_has_bold'] == "false" || $_REQUEST ['job_has_bold'] == "true"))
		$is_bold = $_REQUEST ['job_has_bold'];
	else
		$is_bold = "false";
	
	if (isset ( $_REQUEST ['job_auto_refresh_status'] ) && ($_REQUEST ['job_auto_refresh_status'] == "false" || $_REQUEST ['job_auto_refresh_status'] == "true"))
		$job_auto_refresh_status = $_REQUEST ['job_auto_refresh_status'];
	else
		$job_auto_refresh_status = "false";
	
	if (isset ( $_REQUEST ['job_auto_refresh_duration'] ) && ($_REQUEST ['job_auto_refresh_duration'] != ""))
		$job_auto_refresh_duration = $_REQUEST ['job_auto_refresh_duration'];
	else
		$job_auto_refresh_duration = "0";
	
	if (isset ( $_REQUEST ['can_info'] ) && $_REQUEST ['can_info'] != "") {
		$can_value = explode ( ":", $_REQUEST ['can_info'] );
		$can_duration_status = "true";
		$can_duration = $can_value [1];
	} else {
		$can_duration_status = "false";
		$can_duration = "0";
	}
	
	$single_job_refresh = '';
	if (isset ( $_REQUEST ['has_job_single_refresh'] ) && $_REQUEST ['has_job_single_refresh'] != "") {
		$single_job_refresh = ' jobPostingAction="refresh"';
	}
	
	$single_refresh_status = ($single_job_refresh != "") ? "true" : "false";
	
	$Duration = $_REQUEST ['job_posting_duration'];
	
	if (isset ( $_REQUEST ['job_board_id'] ) && $_REQUEST ['job_board_id'] != "")
		$job_board_id = $_REQUEST ['job_board_id'];
	else
		$job_board_id = 1;
	
	$soap_request = '<?xml version="1.0" encoding="UTF-8"?>
					<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
					  <SOAP-ENV:Header>
					    <mh:MonsterHeader xmlns:mh="http://schemas.monster.com/MonsterHeader">
					      <mh:MessageData>
					        <mh:MessageId>Company Jobs created on ' . date ( 'm/d/y h:i:s A' ) . '</mh:MessageId>
					        <mh:Timestamp>' . $timestamp . '</mh:Timestamp>
					      </mh:MessageData>
					    </mh:MonsterHeader>
					    <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext">
					      <wsse:UsernameToken>
					        <wsse:Username>' . $user_id . '</wsse:Username>
					        <wsse:Password>' . $password . '</wsse:Password>
					      </wsse:UsernameToken>
					    </wsse:Security>
					  </SOAP-ENV:Header>
					  <SOAP-ENV:Body>';
	
	$soap_request .= '<Job jobRefCode="' . $requisitioninfo ['RequestID'] . '" jobAction="addOrUpdate" jobComplete="true"';
	
	$soap_request .= $monster_job_inventory_type;
	
	$soap_request .= ' xmlns="http://schemas.monster.com/Monster" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://schemas.monster.com/Monster http://schemas.monster.com/Current/xsd/Monster.xsd">
					      <RecruiterReference>
					        <UserName>' . $user_id . '</UserName>
					      </RecruiterReference>
					      <JobInformation>
					        <JobTitle><![CDATA[' . $requisitioninfo ['Title'] . ']]></JobTitle>
					        <Contact hideAll="false" hideAddress="true" hideStreetAddress="true" hideCity="true"
					        hideState="true" hidePostalCode="true" hideCountry="true" hideContactInfoField="false"
					        hideCompanyName="false" hideEmailAddress="true" hideFax="true"
					        hideName="false" hidePhone="true">
					          <Name><![CDATA[' . $userinfo ['FirstName'] . " " . $userinfo ['LastName'] . ']]></Name>
					          <CompanyName><![CDATA[' . $roworginfo ['OrganizationName'] . ']]></CompanyName>
					          <Address>
					            <StreetAddress>' . $requisitioninfo ['Address1'] . '</StreetAddress>
					            <StreetAddress2>' . $requisitioninfo ['Address2'] . '</StreetAddress2>
					            <City>' . $requisitioninfo ['City'] . '</City>
					            <State>' . $requisitioninfo ['State'] . '</State>
					            <CountryCode>' . $requisitioninfo ['Country'] . '</CountryCode>
					            <PostalCode>' . $requisitioninfo ['ZipCode'] . '</PostalCode>
					          </Address>
					          <Phones>
					            <Phone phoneType="work">' . $userinfo ['Phone'] . '</Phone>
					          </Phones>
					          <E-mail>' . $userinfo ['EmailAddress'] . '</E-mail>
					        </Contact>
					        <PhysicalAddress>
					          <StreetAddress>' . $requisitioninfo ['Address1'] . '</StreetAddress>
					          <StreetAddress2>' . $requisitioninfo ['Address2'] . '</StreetAddress2>
					          <City>' . $requisitioninfo ['City'] . '</City>
					          <State>' . $requisitioninfo ['State'] . '</State>
					          <CountryCode>' . $requisitioninfo ['Country'] . '</CountryCode>
					          <PostalCode>' . $requisitioninfo ['ZipCode'] . '</PostalCode>
					        </PhysicalAddress>
					        <DisableApplyOnline>false</DisableApplyOnline>
					        <HideCompanyInfo>false</HideCompanyInfo>
					        <JobBody><![CDATA[' . $requisitioninfo ['Description'] . ']]></JobBody>
					        <ApplyWithMonster>
					          <DeliveryMethod monsterId="' . $DeliveryMethod . '"/>
					          <DeliveryFormat monsterId="1"/>
					          <EmailAddress>' . $userinfo ['EmailAddress'] . '</EmailAddress>
					          <VendorText><![CDATA[' . $vendorfield . ']]></VendorText>';
	
	if ($DeliveryMethod == '1')
		$soap_request .= '<PostURL><![CDATA[' . IRECRUIT_HOME . 'monster/saveMonsterApplicantInfo.php]]></PostURL>';
	else
		$soap_request .= '<OnContinueURL><![CDATA[' . PUBLIC_HOME . $monsterredirectlink . ']]></OnContinueURL>';
	
	$soap_request .= '<ApiKey>' . $MonsterObj->ApiKey . '</ApiKey>
					        </ApplyWithMonster>
					      </JobInformation>';
	
	$soap_request .= '<JobPostings>';
	
	$soap_request .= '<JobPosting desiredDuration="' . $Duration . '" bold="' . $is_bold . '"' . $single_job_refresh . '>';
	
	$soap_request .= '<InventoryPreference>';
	
	$soap_request .= '<Autorefresh desired="' . $job_auto_refresh_status . '">
					     	<Frequency>' . $job_auto_refresh_duration . '</Frequency>
					     </Autorefresh>';
	
	$soap_request .= '<CareerAdNetwork desired="' . $can_duration_status . '">
							<Duration>' . $can_duration . '</Duration>
					     </CareerAdNetwork>';
	
	$soap_request .= '</InventoryPreference>';
	
	$soap_request .= '<Location>
					            <City>' . $requisitioninfo ['City'] . '</City>
					            <State>' . $requisitioninfo ['State'] . '</State>
					            <CountryCode>' . $requisitioninfo ['Country'] . '</CountryCode>
					            <PostalCode>' . $requisitioninfo ['ZipCode'] . '</PostalCode>
					     </Location>
					     <JobCategory monsterId="' . $requisitioninfo ['MonsterJobCategory'] . '"/>
					     <JobOccupations>
					     	<JobOccupation monsterId="' . $requisitioninfo ['MonsterJobOccupation'] . '"/>
					     </JobOccupations>';
	
	$soap_request .= '<BoardName monsterId="' . $job_board_id . '"/>';
	
	$soap_request .= '<DisplayTemplate monsterId="1"/>';
	
	if ($_REQUEST ['job_video'] != "" && $_REQUEST ['job_video_monster_id'] != "") {
		$soap_request .= '<Video monsterId="' . $_REQUEST ['job_video_monster_id'] . '" refCode="' . $_REQUEST ['job_video'] . '"></Video>';
	}
	
	$soap_request .= '<Industries>
					            <Industry>
					              <IndustryName monsterId="' . $requisitioninfo ['MonsterJobIndustry'] . '"/>
					            </Industry>
					          </Industries>
					        </JobPosting>
					      </JobPostings>
					    </Job>
					  </SOAP-ENV:Body>
					</SOAP-ENV:Envelope>';
	
	$header = array (
			"Content-type: text/xml;charset=\"utf-8\"",
			"Accept: text/xml",
			"Cache-Control: no-cache",
			"Pragma: no-cache",
			"SOAPAction: \"run\"",
			"Content-length: " . strlen ( $soap_request ) 
	);
	
	$soap_do = curl_init ();
	curl_setopt ( $soap_do, CURLOPT_URL, "https://gateway.monster.com:8443/bgwBroker" );
	curl_setopt ( $soap_do, CURLOPT_CONNECTTIMEOUT, 10 );
	curl_setopt ( $soap_do, CURLOPT_TIMEOUT, 10 );
	curl_setopt ( $soap_do, CURLOPT_RETURNTRANSFER, true );
	curl_setopt ( $soap_do, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt ( $soap_do, CURLOPT_SSL_VERIFYHOST, false );
	curl_setopt ( $soap_do, CURLOPT_POST, true );
	curl_setopt ( $soap_do, CURLOPT_POSTFIELDS, $soap_request );
	curl_setopt ( $soap_do, CURLOPT_HTTPHEADER, $header );
	
	$result = curl_exec ( $soap_do );

	$result = str_replace ( "SOAP-ENV:", "", $result );
	
	if (trim ( $result ) == "") {
		$msg = '<span style="color:red">Unable to post job. Please try after some time.</span>';
		
		echo "<br><h4>".$msg."</h4>";
	}
	
	if (curl_exec ( $soap_do ) === false) {
		$err = curl_error ( $soap_do );
		$msg = $err;
		
		echo "<br><h4>".$msg . " Please try again.</h4>";
	}
	
	curl_close ( $soap_do );
	
	//Log the monster request and response
	Logger::writeMessage ( ROOT . "logs/monster/" . date ( 'Y-m-d-H-i-s' ) . ".log", $soap_request . "<br>" . $result, "a+", false );
	//Log the monster information to database
	Logger::writeMessageToDb($OrgID, "MonsterRequestResponse", $soap_request . "<br>" . $result);
	
	$xml = simplexml_load_string ( $result );
	$jobsr = $xml->Body->JobsResponse;
	$jobr = $jobsr->JobResponse;
	
	if (is_object ( $jobr )) {
		
		foreach ( $jobr->Status->ReturnCode->attributes () as $RK => $RV ) {
			$ReturnTypeCode = $RV;
		}
		foreach ( $jobr->CompanyReference->attributes () as $CK => $CV ) {
			$CompanyId = $CV;
		}
		foreach ( $jobr->RecruiterReference->attributes () as $RRK => $RRV ) {
			$RecruiterReference = $RRV;
		}
		foreach ( $jobr->JobPostingResponse->attributes () as $PK => $PV ) {
			$PostingId = $PV;
		}
		$ResourceIdsChargeQuantity = array ();
		
		if (isset ( $jobr->JobCharges->JobCharge ) && is_array ( $jobr->JobCharges->JobCharge )) {
			foreach ( $jobr->JobCharges->JobCharge as $JobCharge ) {
				$JobChargesList ['ResourceLicenseId'] [] = ( string ) $JobCharge->ResourceLicenseId;
				$JobChargesList ['ChargeQuantity'] [] = ( string ) $JobCharge->ChargeQuantity;
			}
			$ResourceIdsChargeQuantity = serialize ( $JobChargesList );
		}
		
		$ResourceIdsChargeQuantity = serialize ( $JobChargesList );
		
		$Descriptions = array ();
		for($d = 0; $d < count ( $jobr->Status->Descriptions->Description ); $d ++) {
			foreach ( $jobr->Status->Descriptions->Description [$d]->attributes () as $DV => $D ) {
				$Key = ( string ) $D; // Use That $D as Key For Description
			}
			
			if ($DV == "descriptionType" && $D == "error") {
				$Description = ( string ) $jobr->Status->Descriptions->Description [$d];
				$Descriptions [$Key] = $Description;
			}
		}
		
		$DescriptionStatus = serialize ( $Descriptions );
		
		/**
		 * Insert Record
		 */
		// Set parameters
		$params = array (":RequestID" => $JobRefId);
		// Set where condition
		$where = array ("RequestID = :RequestID");
		// Get PostRequisitions Information
		$respostreq = $RequisitionsObj->getPostRequisitionsInformation ( "*", $where, "", array ($params) );
		$numpostreq = $respostreq ['count'];
		
		if ($numpostreq > 0) {
			$JobLocation = $roworginfo ['City'] . ", " . $roworginfo ['State'] . " " . $roworginfo ['ZipCode'];
			
			// Set where condition
			$where = array (
					"RequestID = :RequestID"
			);
			// Set parameters
			$params = array (
					":RequestID" => $_REQUEST ['RequestID'],
					":OrgID" => $_REQUEST ['OrgID'],
					":MultiOrgID" => $MultiOrgID,
					":PostingId" => $PostingId,
					":CompanyId" => $CompanyId,
					":RecruiterReference" => $RecruiterReference,
					":ReturnTypeCode" => $ReturnTypeCode,
					":ResourceIdsChargeQuantity" => $ResourceIdsChargeQuantity,
					":CompanyName" => $roworginfo ['OrganizationName'],
					":JobTitle" => $requisitioninfo ['Title'],
					":JobLocation" => $JobLocation,
					":Email" => $userinfo ['EmailAddress'],
					":Description" => $DescriptionStatus,
					":PostType" => $MonsterInfo ['PostType'],
					":InventoryType" => $_REQUEST ['job_inventory_type'],
					":HasBold" => $is_bold,
					":HasSingleRefresh" => $single_refresh_status,
					":JobBoardId" => $job_board_id,
					":AutoRefreshStatus" => $job_auto_refresh_status,
					":AutoRefreshDuration" => $job_auto_refresh_duration,
					":CANStatus" => $can_duration_status,
					":CANDuration" => $can_duration,
					":Duration" => $Duration,
					":VideoRefCode" => $_REQUEST ['job_video'] 
			);
			// Set information
			$set_info = array (
					"OrgID = :OrgID",
					"MultiOrgID = :MultiOrgID",
					"PostingId = :PostingId",
					"CompanyId = :CompanyId",
					"RecruiterReference = :RecruiterReference",
					"ReturnTypeCode = :ReturnTypeCode",
					"ResourceIdsChargeQuantity = :ResourceIdsChargeQuantity",
					"CompanyName = :CompanyName",
					"JobTitle = :JobTitle",
					"JobLocation = :JobLocation",
					"Email = :Email",
					"Description = :Description",
					"Source = '1'",
					"PostType = :PostType",
					"InventoryType = :InventoryType",
					"HasBold = :HasBold",
					"HasSingleRefresh = :HasSingleRefresh",
					"JobBoardId = :JobBoardId",
					"AutoRefreshStatus = :AutoRefreshStatus",
					"AutoRefreshDuration = :AutoRefreshDuration",
					"CANStatus = :CANStatus",
					"CANDuration = :CANDuration",
					"Duration = :Duration",
					"VideoRefCode = :VideoRefCode",
					"PostedDateTime = NOW()" 
			);
			// Update Requisitions Information
			$RequisitionsObj->updRequisitionsInfo ( "PostRequisitions", $set_info, $where, array (
					$params 
			) );
			
			$MonsterObj->updateRequisitionPostedDate ( $_REQUEST ['OrgID'], $MultiOrgID, $_REQUEST ['RequestID'] );
			
			// Set where condition
			$where = array (
					"RequestID = :RequestID",
					"OrgID = :OrgID",
					"MultiOrgID = :MultiOrgID" 
			);
			// Set parameters
			$params = array (
					":RequestID" => $_REQUEST ['RequestID'],
					":OrgID" => $_REQUEST ['OrgID'],
					":MultiOrgID" => $MultiOrgID,
					":Duration" => $Duration 
			);
			// Set information
			$set_info = array (
					"Duration = :Duration" 
			);
			// Update Requisitions Information
			$RequisitionsObj->updRequisitionsInfo ( "Requisitions", $set_info, $where, array (
					$params 
			) );
		} else {
			$JobLocation = $roworginfo ['City'] . ", " . $roworginfo ['State'] . " " . $roworginfo ['ZipCode'];
			
			if($PostingId != "") {
				// Insert PostRequisitions Information
				$ins_set_info = array (
						"OrgID" => $_REQUEST ['OrgID'],
						"MultiOrgID" => $MultiOrgID,
						"RequestID" => $_REQUEST ['RequestID'],
						"PostingId" => $PostingId,
						"CompanyId" => $CompanyId,
						"RecruiterReference" => $RecruiterReference,
						"ReturnTypeCode" => $ReturnTypeCode,
						"ResourceIdsChargeQuantity" => $ResourceIdsChargeQuantity,
						"CompanyName" => $roworginfo ['OrganizationName'],
						"JobTitle" => $requisitioninfo ['Title'],
						"JobLocation" => $JobLocation,
						"Email" => $userinfo ['EmailAddress'],
						"Description" => $DescriptionStatus,
						"Source" => '1',
						"PostType" => $MonsterInfo ['PostType'],
						"InventoryType" => $_REQUEST ['job_inventory_type'],
						"HasBold" => $is_bold,
						"HasSingleRefresh" => $single_refresh_status,
						"JobBoardId" => $job_board_id,
						"AutoRefreshStatus" => $job_auto_refresh_status,
						"AutoRefreshDuration" => $job_auto_refresh_duration,
						"CANStatus" => $can_duration_status,
						"CANDuration" => $can_duration,
						"Duration" => $Duration,
						"VideoRefCode" => $_REQUEST ['job_video'],
						"PostedDateTime" => "NOW()"
				);
				$RequisitionsObj->insRequisitionsInfo ( "PostRequisitions", $ins_set_info );
					
				$MonsterObj->updateRequisitionPostedDate ( $_REQUEST ['OrgID'], $MultiOrgID, $_REQUEST ['RequestID'] );
					
				// Update Requisitions Inforamtion
				$set_info = array (
						"Duration = :Duration"
				);
				$where = array (
						"RequestID = :RequestID",
						"OrgID = :OrgID",
						"MultiOrgID = :MultiOrgID"
				);
				$params = array (
						":RequestID" => $_REQUEST ['RequestID'],
						":OrgID" => $_REQUEST ['OrgID'],
						":MultiOrgID" => $MultiOrgID,
						":Duration" => $Duration
				);
				$RequisitionsObj->updRequisitionsInfo ( "Requisitions", $set_info, $where, array (
						$params
				) );
			}
			
			
		}
	}
	
	if ($ReturnTypeCode == "success") {
		$msg = "<span style='color:$navbar_color'>Job Successfully Posted To Monster</span>";
	} else if ($ReturnTypeCode == "failure") {
		$msg = "<span style='color:red'>Unable To Post Job Please See The Message Below.</span>";
	} else {
		$msg = "<span style='color:red'>Sorry there is a problem while posting this job.</span>";
	}
	
	
	echo "<br><h4>".$msg."</h4>";
	
}