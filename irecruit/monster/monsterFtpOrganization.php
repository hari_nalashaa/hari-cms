<?php
//Get Date Time
$TS = $MysqlHelperObj->getDateTime('%a, %d %b %Y %H:%i:%s EST');

//Get requisitions foreach organization
$results = $MonsterObj->monsterOrgRequisitions($OrgID, $MultiOrgID);

$MonsterRecords = $MonsterObj->getMonsterInfoByOrgID($OrgID, $MultiOrgID);
$MonsterInfo = $MonsterRecords['row'];
$vendorfield = $OrgID."*".$MultiOrgID;

$link = "";

if(is_array($results['results'])) {
	foreach ($results['results'] as $REQ) {
		
		$JobRefId = $REQ ['RequestID'];
		if ($MultiOrgID != "") {
			$link = 'jobRequest.php?OrgID=' . $OrgID.'&MultiOrgID=' . $MultiOrgID.'&RequestID='.$JobRefId.'&source=MONST';
		} else {
			$link = 'jobRequest.php?OrgID=' . $OrgID.'&RequestID='.$JobRefId.'&source=MONST';
		}
	
		//Set parameters
		$params = array(":OrgID"=>$REQ ['OrgID'], ":MultiOrgID"=>$REQ ['MultiOrgID']);
		//Set condition
		$where  = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
		//Set results
		$resultsIN = $OrganizationsObj->getOrgDataInfo("OrganizationName, DemoAccount", $where, '', array($params));
		list ( $OrganizationName, $DemoAccount ) = array_values($resultsIN['results'][0]);
		
		$req_info           =   $RequisitionsObj->getRequisitionsDetailInfo("FreeJobBoardLists", $OrgID, $REQ['RequestID']);
		$free_jobboard_list =   json_decode($req_info['FreeJobBoardLists'], true);
		
		// Required
		if (($REQ ['Title']) && ($REQ ['PostDate']) && ($REQ ['RequestID']) && ($REQ ['OrgLevelID']) && ($REQ ['SelectionOrder']) && ($REQ ['Description']) && ($DemoAccount != 'Y')) {

		    if(is_array($free_jobboard_list) && in_array("Monster", $free_jobboard_list)) {

		        $data_exists = "true";
		        
		        $xml .= "<job>\n";
		        
		        $xml .= "   <title><![CDATA[";
		        $xml .= $REQ ['Title'];
		        $xml .= "]]></title>\n";
		        
		        $xml .= "   <refcode><![CDATA[";
		        $xml .= $REQ ['RequestID'];
		        $xml .= "]]></refcode>\n";
		        
		        $xml .= "   <url><![CDATA[";
		        
		        $xml .= PUBLIC_HOME . "jobRequest.php?";
		        $xml .= "OrgID=" . $REQ ['OrgID'];
		        if ($REQ ['MultiOrgID'] != "") {
		            $xml .= "&MultiOrgID=" . $REQ ['MultiOrgID'];
		        } 
		        $xml .= "&RequestID=";
		        $xml .= $REQ ['RequestID'];
		        
		        $xml .= "&source=MONST";
		        
		        $xml .= "]]></url>\n";
		        
		        if ($OrganizationName) {
		            $xml .= "   <company><![CDATA[";
		            $xml .= $OrganizationName;
		            $xml .= "]]></company>\n";
		        }
		        
		        if ($REQ ['City']) {
		            $xml .= "   <city><![CDATA[";
		            $xml .= $REQ ['City'];
		            $xml .= "]]></city>\n";
		        }
		        
		        if ($REQ ['State']) {
		            $xml .= "   <state><![CDATA[";
		            $xml .= $REQ ['State'];
		            $xml .= "]]></state>\n";
		        }
		        
		        if ($REQ ['ZipCode']) {
		            $xml .= "   <postalcode><![CDATA[";
		            $xml .= $REQ ['ZipCode'];
		            $xml .= "]]></postalcode>\n";
		        }
		        
		        $xml .= "<country><![CDATA[";
		        $xml .= $REQ ['Country'];
		        $xml .= "]]></country>\n";
		        
		        $xml .= "   <description><![CDATA[";
		        $xml .= $REQ ['Description'];
		        $xml .= "]]></description>\n";
		        
		        $xml .= "<posteddate>";
		        $xml .= date("Y-m-d\TH:i:s");
		        $xml .= "</posteddate>";
		        
		        //Set AWM Parameters
		        $xml .= "<awm>\n";
		        
		        if($MonsterInfo['PostType'] == 'POST')
		            $xml .= "<method>POST</method>\n";
		        else
		            $xml .= "<method>REQUEST</method>\n";
		        
		        
		        $xml .= "<format>JSON</format>\n";
		        $xml .= "<apikey>\n";
		        $xml .= $MonsterObj->ApiKey;
		        $xml .= "</apikey>\n";
		        
		        if($MonsterInfo['PostType'] == 'POST')
		        {
		            $xml .= "<posturl>\n";
		            $xml .= IRECRUIT_HOME."monster/saveMonsterApplicantInfo.php";
		            $xml .= "</posturl>\n";
		        }
		        else {
		            $xml .= "<oncontinueurl><![CDATA[\n";
		            $xml .= PUBLIC_HOME.$link;
		            $xml .= "]]></oncontinueurl>\n";
		        }
		        
		        $xml .= "<vendortext><![CDATA[".$vendorfield."]]></vendortext>\n";
		        
		        $xml .= "</awm>\n";
		        
		        $xml .= "</job>\n";
		        
		        $datasizeinbytes = mb_strlen($xml, '8bit');
		        $datasizeinmb = $datasizeinbytes / $OneMB;
		        
		        if($datasizeinmb > 48 && $datasizeinmb < 50) {
		            //if($datasizeinmb > 0.001 && $datasizeinmb < 0.009) {
		            $filesdata[] = $xml;
		            $xml = "";
		            $flag = "true";
		        }
		        
		    }
	
		} // end if all required fields are populated
	} // end foreach
} // end if
?>
