<?php
require_once '../Configuration.inc';

$JobOccupations = $MonsterObj->jobOccupationsByCategory($_POST['cat_id']);
?>
<select name="MonsterJobOccupation" id="MonsterJobOccupation" class="form-control width-auto-inline" onchange='saveMonsterInfo(this, "<?php echo $_REQUEST['RequestID'];?>", "", "paid")'>
<option value="">Select</option>
<?php
for($i = 0; $i < count($JobOccupations); $i++) {
	$selected = ($JobOccupations[$i]['OccupationId'] == $_POST['joboccupation']) ? "selected='selected'" : "";
	?><option value="<?php echo $JobOccupations[$i]['OccupationId'];?>" <?php echo $selected;?>><?php echo $JobOccupations[$i]['OccupationAlias'];?></option><?php
	unset($selected);
}
?>
</select>