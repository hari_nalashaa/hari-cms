<?php 
$user_id = $MonsterOrgInfo['RecruiterReferenceUserName'];//'xrtpjobsx01';
$password = $MonsterOrgInfo['Password'];//'rtp987654';
$timestamp = date ("Y-m-d\TH:i:s.z\Z");


if($user_id != "" && $password != "") {
	
	include IRECRUIT_DIR . 'monster/InventoryRequestResponse.inc';
	
	$inventoryxml = simplexml_load_string ( $result, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS);
	
	$base_inventories = array("Local_Posting", "Standard Reserved Jobs");
	
	$inventory_details = array();
	$fault_string = 'True';
	$ai = 0;
	if(!isset($inventoryxml->Body->Fault->faultstring) && $inventoryxml->Body->Fault->faultstring == "") {
		$inventories = $inventoryxml->Body->InventoriesQueryResponse->Inventories->Inventory;
		$inventorycount = count($inventoryxml->Body->InventoriesQueryResponse->Inventories->Inventory);
		
		$rowcolor = "#ffffff";
		for($i = 0; $i < $inventorycount; $i++) {
			
			
			foreach($inventories[$i]->ResourceLicenseInfo->ResourceAclID->attributes() as $license_attribute => $license_id) {
				$license_id = trim($license_id);
				$license_name = trim((string)$inventories[$i]->ResourceLicenseInfo->ResourceAclID);
			}
			
				
			$total_purchased = trim((string) $inventories[$i]->Quantity->TotalPurchased);
			$total_available = trim((string) $inventories[$i]->Quantity->TotalAvailable);
			
			if(in_array($license_name, $base_inventories))
			{
				
				if($total_available > 0) {
					$fault_string = 'False';
				}
					
				/**
				 * @tutorial Create an array of inventory options only if inventory is available
				 */
				if($total_available > 0) {
				
					foreach($inventories[$i]->Quantity->attributes() as $a => $b) {
						$inventory_details[$license_id]['isUnlimited'][] = (string)$b;
					}
				
					$inventory_details[$license_id]['TotalPurchased'][] = $total_purchased;
					$inventory_details[$license_id]['TotalAvailable'][] = $total_available;
					$inventory_details[$license_id]['LicenseName'][] = $license_name;
				
					$inventoryattrs = $inventories[$i]->InventoryAttrs->InventoryAttr;
					$inventoryattrcount = count($inventories[$i]->InventoryAttrs->InventoryAttr);
				
					for($j = 0; $j < $inventoryattrcount; $j++) {
				
						$AttrTypeId = trim($inventoryattrs[$j]->AttrTypeId);
							
						if($AttrTypeId != "LicenseProvisionMethod"
							&& $AttrTypeId != "MaxCategoryCount"
							&& $AttrTypeId != "MaxPostingLocationGroups"
							&& $AttrTypeId != "ExtraBoards"
							&& $AttrTypeId != "HasRecommendedResumes"
							&& $AttrTypeId != "OriginalQuantity") {
						
							if($AttrTypeId == "JobBoardID") {
								$job_board_id = (string) $inventoryattrs[$j]->AttrValue->Numeric1->Value;
								$inventory_details[$license_id]['JobBoardID'][] = $job_board_id;
							}
								
							if($AttrTypeId == "PostingLocationGroupID") {
								$location = (string) $inventoryattrs[$j]->AttrValue->Numeric1->Name;
								$inventory_details[$license_id]['Location'][] = $location;
							}
								
							if($AttrTypeId == "PostingDuration") {
								$inventory_details[$license_id]['PostingDuration'][] = (string) $inventoryattrs[$j]->AttrValue->Numeric1->Value;
							}
								
							if($AttrTypeId == "HasAutoRefresh") {
								$inventory_details[$license_id]['HasAutoRefresh']['Status'][] = (string) $inventoryattrs[$j]->AttrValue->Numeric1->Value;
								$inventory_details[$license_id]['HasAutoRefresh']['Duration'][] = (string) $inventoryattrs[$j]->AttrValue->Numeric2->Value;
							}
								
							if($AttrTypeId == "HasBolding") {
								$inventory_details[$license_id]['HasBolding'][] = (string) $inventoryattrs[$j]->AttrValue->Numeric1->Value;
							}
								
						}
				
					}
				
				}
				
				
			}
			else if($license_name == "JOBINVENTORY_CAN") {
				
				$inventoryattrs = $inventories[$i]->InventoryAttrs->InventoryAttr;
				$inventoryattrcount = count($inventories[$i]->InventoryAttrs->InventoryAttr);
				
				for($jc = 0; $jc < $inventoryattrcount; $jc++) {
				
					$AttrTypeId = trim($inventoryattrs[$jc]->AttrTypeId);
					
					if($AttrTypeId == "PostingLocationGroupID") {
						$can_location = (string) $inventoryattrs[$jc]->AttrValue->Numeric1->Name;
					}
					
					if($AttrTypeId == "ApplicableACLID") {
						$can_ref_licence_id = (string) $inventoryattrs[$jc]->AttrValue->Numeric1->Value;
						$can_ref_licence_name = (string) $inventoryattrs[$jc]->AttrValue->Numeric1->Name;
					}
						
					if($AttrTypeId == "CANDuration") {
						$can_duration = (string) $inventoryattrs[$jc]->AttrValue->Numeric1->Value;
						
					}
				}
				
				$inventory_details[$can_ref_licence_id][$can_location]['CANDuration'][] = $can_duration;
				
				
				if(is_array($inventory_details[$can_ref_licence_id]['CANLocations']) && !in_array($can_location, $inventory_details[$can_ref_licence_id]['CANLocations'])) {
					$inventory_details[$can_ref_licence_id]['CANLocations'][] = $can_location;
				}
				
			}
			else if($license_name == "JOBINVENTORY_REFRESH") {
			
				$inventoryattrs = $inventories[$i]->InventoryAttrs->InventoryAttr;
				$inventoryattrcount = count($inventories[$i]->InventoryAttrs->InventoryAttr);
			
				for($jr = 0; $jr < $inventoryattrcount; $jr++) {
			
					$AttrTypeId = trim($inventoryattrs[$jr]->AttrTypeId);
						
					if($AttrTypeId == "ApplicableACLID") {
						$refresh_licence_id = (string) $inventoryattrs[$jr]->AttrValue->Numeric1->Value;
						$refresh_licence_name = (string) $inventoryattrs[$jr]->AttrValue->Numeric1->Name;
					}
					
					if($AttrTypeId == "PostingLocationGroupID") {
						$refresh_location = (string) $inventoryattrs[$jr]->AttrValue->Numeric1->Name;
					}
			
					if($AttrTypeId == "HasJobSingleRefresh") {
						$has_refresh = (string) $inventoryattrs[$jr]->AttrValue->Numeric1->Value;
					}
				}
			
				$inventory_details[$refresh_licence_id][$refresh_location]['HasJobSingleRefresh'] = $has_refresh;
				if(is_array($inventory_details[$refresh_licence_id]['JobRefreshLocations']) && !in_array($refresh_location, $inventory_details[$refresh_licence_id]['JobRefreshLocations'])) {
					$inventory_details[$refresh_licence_id]['JobRefreshLocations'][] = $refresh_location;
				}
			
			}
			else if($license_name == "JOBINVENTORY_VIDEO") {
				
				$inventoryattrs = $inventories[$i]->InventoryAttrs->InventoryAttr;
				$inventoryattrcount = count($inventories[$i]->InventoryAttrs->InventoryAttr);
					
				for($jv = 0; $jv < $inventoryattrcount; $jv++) {
						
					$AttrTypeId = trim($inventoryattrs[$jv]->AttrTypeId);
				
					if($AttrTypeId == "ApplicableACLID") {
						$video_licence_id = (string) $inventoryattrs[$jv]->AttrValue->Numeric1->Value;
						$video_licence_name = (string) $inventoryattrs[$jv]->AttrValue->Numeric1->Name;
					}
						
					if($AttrTypeId == "PostingLocationGroupID") {
						$video_location = (string) $inventoryattrs[$jv]->AttrValue->Numeric1->Name;
					}
						
				}
				
				$inventory_details[$video_licence_id]["JobVideoLocation"] = $video_location;
				$inventory_details[$video_licence_id]["JobVideoMonsterId"] = $license_id;
					
			}
 			
			
		}
			
	}
	else {
		$fault_string = 'True';
	}
}

?>