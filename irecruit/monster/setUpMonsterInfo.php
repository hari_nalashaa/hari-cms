<?php
require_once '../Configuration.inc';

$title = 'Set Monster Information For Your Organization';

//Set page title
$TemplateObj->title	=	$title;

$MonsterRecords 	=	G::Obj('Monster')->getMonsterInfoByOrgID($OrgID, $MultiOrgID);

if($MonsterRecords['numrows'] == 0 || $MonsterRecords['numrows'] == "") {
    G::Obj('Monster')->insertMonsterInfoForOrg($USERID, $OrgID, $MultiOrgID, $_POST['RecruiterReferenceUserName'], $_POST['Password'], $_POST['ddlFreeListings'], $_POST['ddlDuration']);
}

if(count($_POST) > 0) {
    G::Obj('Monster')->updateMonsterInfoForOrg($OrgID, $MultiOrgID, $_POST['RecruiterReferenceUserName'], $_POST['Password'], $_POST['ddlFreeListings'], $_POST['ddlAWM'], $_POST['ddlDuration'], $_POST['ddlPostingMethodType']);

    echo "<script>location.href='setUpMonsterInfo.php?msg=suc';</script>";
}

echo $TemplateObj->displayIrecruitTemplate('views/monster/SetUpMonster');
?>