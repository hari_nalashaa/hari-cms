<form method="post" name="frmJobPostDetails" id="frmJobPostDetails">

	<input type="hidden" name="job_auto_refresh_status" id="job_auto_refresh_status">
	<input type="hidden" name="job_auto_refresh_duration" id="job_auto_refresh_duration"> 
	<input type="hidden" name="job_posting_duration" id="job_posting_duration"> 
	<input type="hidden" name="job_inventory_license_id" id="job_inventory_license_id"> 
	<input type="hidden" name="job_location" id="job_location"> 
	<input type="hidden" name="job_board_id" id="job_board_id"> 
	<input type="hidden" name="job_has_bold" id="job_has_bold">
	<input type="hidden" name="job_video_monster_id" id="job_video_monster_id">
	<input type="hidden" name="job_inventory_type" id="job_inventory_type">
	<input type="hidden" name="postjobto" id="postjobto" value="monster"> 
	<input type="hidden" name="action" id="action" value="<?php echo htmlspecialchars($_REQUEST['action']);?>"> 
	<input type="hidden" name="OrgID" id="OrgID" value="<?php echo htmlspecialchars($OrgID);?>">
	<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo htmlspecialchars($_REQUEST['MultiOrgID']);?>">
	<input type="hidden" name="Active" id="Active" value="<?php echo htmlspecialchars($_REQUEST['Active']);?>">
	<input type="hidden" name="RequestID" id="RequestID" value="<?php echo htmlspecialchars($_REQUEST['RequestID']);?>">

	<?php
	$style_inventory_info = ' class="inventory_info" style="border-collapse: collapse; border-color: #E4EBED"';
	if (preg_match ( '/validateMonsterJobPost.php$/', $_SERVER ["SCRIPT_NAME"] )) {
		$style_inventory_info = ' class="inventory_info table table-bordered"';
	}
	?>
	<table border="1" cellpadding="2" cellspacing="5" width="100%" <?php echo $style_inventory_info;?>>
		<?php
		if($ServerInformationObj->getRequestSource() != 'ajax') {
			?>
			<tr>
				<td colspan="8" align="left">Please select the inventory type</td>
				<td colspan="2" align="right">
					<p style="text-align:right;">
						<a title="Close Window" href="#closeForm">
							<img border="0" style="margin:0px 3px -4px 0px;" title="Close Window" src="<?php echo IRECRUIT_HOME;?>images/icons/photo_delete.png">
								<b style="FONT-SIZE:8pt;COLOR:#000000">Close Window</b>
						</a>
					</p>
				</td>
			</tr>
			<?php
		}
		?>
		<tr>
			<td colspan="10" align="left"><strong>Note:*</strong> <br>
				1) Please find the details of your monster inventory, select(last column, type of inventory) any one of that combination, to post this job.<br>
				2) If you want to select "CAN Duration" and "Has Single Refresh", It should be related to same license type.<br>
				3) CAN Duration location and Has Single Refresh location should be same as Type of inventory location.<br> 
				4) For videos, Please provide the Reference code(refCode) in the text box, only that video is related to selected Job Board Id,
				   Location. If You don't know about video Reference code please leave it blank, wrong or unrelated Reference code will be failed to post that job.
			</td>
		</tr>
		<tr>
			<td valign="top"><strong>Serial No</strong></td>
			<td valign="top"><strong>License Type / Job Board Id</strong></td>
			<td valign="top"><strong>Has Bold</strong></td>
			<td valign="top"><strong>Location</strong></td>
			<td valign="top"><strong>Has Auto Refresh<br>(Status - Duration)</strong></td>
			<td valign="top"><strong>Post Duration</strong></td>
			<td valign="top"><strong>CAN Inventory Duration (optional)</strong></td>
			<td valign="top"><strong>Has Job Single Refresh (optional)</strong></td>
			<td valign="top"><strong>Job Video (optional)</strong></td>
			<td valign="top">
				<strong>Type of Inventory<br>(License Type)</strong>
			</td>
		</tr>	
		<?php
	  	$inventory_types = array_keys($inventory_details);
	  	$sirno = 0;
	  	$inventory_ids_list = array();
	  	$has_refresh_license_ids = array();
	  	
	  	for($r = 0; $r < count($inventory_types); $r++) {
			
			$inventory_details[$inventory_types[$r]]['RowSpan'] = count($inventory_details[$inventory_types[$r]]['TotalAvailable']);
			
			if(isset($inventory_details[$inventory_types[$r]]['CANLocations']) && is_array($inventory_details[$inventory_types[$r]]['CANLocations'])) {
				foreach ($inventory_details[$inventory_types[$r]]['CANLocations'] as $inventory_can_location) {
					$inventory_details[$inventory_types[$r]][$inventory_can_location]['CANDuration'] = array_unique($inventory_details[$inventory_types[$r]][$inventory_can_location]['CANDuration']);
				}	
			}
			
		}
	  	
		$inventory_exist_status = "false";
	  	foreach($inventory_types as $inventory_license_id) {
	  		
			for($iti = 0; $iti < count($inventory_details[$inventory_license_id]['TotalAvailable']); $iti++)
			{
				$inventory_exist_status = "true";
				$has_bolding = "0";
				$has_autorefresh_status = "0";
				$has_autorefresh_duration = "0";
				$posting_duration = "0";
				
				$inventory_job_board_id = $inventory_details[$inventory_license_id]['JobBoardID'][$iti];
				$inventory_license_name = $inventory_details[$inventory_license_id]['LicenseName'][$iti];
				
				if(isset($inventory_details[$inventory_license_id]['HasBolding'][$iti])) {
					$has_bolding = $inventory_details[$inventory_license_id]['HasBolding'][$iti];
				}
				if(isset($inventory_details[$inventory_license_id]['HasAutoRefresh']['Status'][$iti])) {
					$has_autorefresh_status = $inventory_details[$inventory_license_id]['HasAutoRefresh']['Status'][$iti];
				}
				if(isset($inventory_details[$inventory_license_id]['HasAutoRefresh']['Duration'][$iti])) 
				{
					$has_autorefresh_duration = $inventory_details[$inventory_license_id]['HasAutoRefresh']['Duration'][$iti];
				}
				if(isset($inventory_details[$inventory_license_id]['PostingDuration'][$iti])) 
				{
					$posting_duration = $inventory_details[$inventory_license_id]['PostingDuration'][$iti];
				}
	
				$location = $inventory_details[$inventory_license_id]['Location'][$iti];
				
				$has_bolding_job = ($has_bolding == "0" || $has_bolding == 0) ? "false" : "true";
				$has_bolding_display = ($has_bolding == "0" || $has_bolding == 0) ? "no" : "yes";
				
				$has_autorefresh_status_job = ($has_autorefresh_status == "0" || $has_autorefresh_status == 0) ? "false" : "true";
				$has_autorefresh_status_display = ($has_autorefresh_status == "0" || $has_autorefresh_status == 0) ? "no" : "yes";
				?>
				<tr>
					<td><?php echo ++$sirno;?></td>
					<td><?php echo $inventory_license_name;?> / <?php echo $inventory_job_board_id;?></td>
					<td><?php echo ucfirst($has_bolding_display);?></td>
					<td><?php echo ucfirst($location);?></td>
					<td><?php echo ucfirst($has_autorefresh_status_display);?> - <?php echo $has_autorefresh_duration;?></td>
					<td><?php echo $posting_duration;?></td>
					<?php
					$rowspan = $inventory_details[$inventory_license_id]['RowSpan'];
					if(!in_array($inventory_license_id, $inventory_ids_list)) {
						$inventory_ids_list[] = $inventory_license_id;
						?>
						<td rowspan="<?php echo $rowspan;?>">
							<?php
							if(isset($inventory_details[$inventory_license_id]['CANLocations']) && is_array($inventory_details[$inventory_license_id]['CANLocations'])) {
								foreach ($inventory_details[$inventory_license_id]['CANLocations'] as $inventory_can_location) {

									for($c = 0; $c < count($inventory_details[$inventory_license_id][$inventory_can_location]['CANDuration']); $c++) {
										if(isset($inventory_details[$inventory_license_id][$inventory_can_location]['CANDuration'][$c])) {
											$inventory_can_duration = $inventory_details[$inventory_license_id][$inventory_can_location]['CANDuration'][$c];
												
											$can_duration_value[$inventory_license_id] = $inventory_can_duration;
												
											echo "<input type='radio' name='can_info' value='$inventory_license_id:$inventory_can_duration:$inventory_can_location'>";
											echo $inventory_can_duration."&nbsp;".$inventory_can_location."<br>";
										}
									}
								
								}
					
							}
							?>
						</td>
						<?php
					}
					
					if(!in_array($inventory_license_id, $has_refresh_license_ids)) {
						$has_refresh_license_ids[] = $inventory_license_id;
						?>
						<td rowspan="<?php echo $rowspan;?>">
						<?php
						if(isset($inventory_details[$inventory_license_id]['JobRefreshLocations']) && is_array($inventory_details[$inventory_license_id]['JobRefreshLocations'])) {
							foreach ($inventory_details[$inventory_license_id]['JobRefreshLocations'] as $inventory_refresh_location) {
		
								if($inventory_details[$inventory_license_id][$inventory_refresh_location]['HasJobSingleRefresh'] == 1) {
									$has_job_refresh_value = $inventory_license_id.":1:".$inventory_refresh_location;
									?><input type="radio" name="has_job_single_refresh" value="<?php echo $has_job_refresh_value;?>">&nbsp;Yes - <?php echo $inventory_refresh_location;?><?php
								}
							}	
						}	
						?>											
						</td>
						<?php
					}	
					?>
					<td><input type="text" name="job_video"/></td>
					<td>
						<input type="radio" name="inventory_info" value="<?php echo $inventory_license_id;?>" onclick="set_inventory_details('<?php echo $has_bolding_job;?>', '<?php echo $has_autorefresh_status_job;?>', '<?php echo $has_autorefresh_duration;?>', '<?php echo $posting_duration;?>', '<?php echo $inventory_license_id;?>', '<?php echo $location;?>', '<?php echo $inventory_job_board_id;?>', '<?php echo $inventory_details[$inventory_license_id]["JobVideoMonsterId"];?>', '<?php echo (string)$inventory_license_name;?>')">
					</td>
				</tr>
				<?php
			}
	  	} 
	  	
	  	if($inventory_exist_status == "false") {
	  		?>
	  		<tr>
	  			<td colspan="10" align="center">
	  				There is no inventory related to your credentials.
	  			</td>
	  		</tr>
	  		<?php
	  	}
	  	else {
	  		?>
	  		<tr>
	  			<td colspan="10" align="left">
	  				<u>Note:</u> Job posting sites reserve the right to reject requisitions pending content review. These items can only be cancelled until midnight on the same day of purchase. After that they are subject to our web site policies.
	  			</td>
	  		</tr>
			<tr>
				<td colspan="10">
				<br>
				<?php 
					if($ServerInformationObj->getRequestSource() == 'ajax') {
						?>
						<input type="button" name="post_monster_job" id="post_monster_job" value="Post job to monster" onclick="post_job(this)" class="btn btn-primary">
						<?php
					}
					else {
						?>
						<input type="button" name="post_monster_job" id="post_monster_job" value="Post job to monster" onclick="post_job_to_monster()">
						<?php
					}
				?>
				<span id="post_job_status_message"></span>
				<br><br>
				</td>
			</tr>
			<tr>
    		   <td colspan="10">
	               <font color="#FC1F1F">Is this position over 30 days old?  We recommend <u>ONLY</u> sponsoring brand new positions.</font>';
	           </td>
	        </tr>
	  		<?php
	  	}
		?>
	</table>
</form>