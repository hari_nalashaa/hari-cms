<form method="post" name="frmMonsterInfo" id="frmMonsterInfo">
<input type="hidden" name="ddlFreeListings" id="ddlFreeListings" value="Yes">

<table width="100%" cellpadding="3" cellspacing="3" class="table table-striped table-bordered table-hover">
	<tr>
		<td colspan="2"><h3 style="margin:0px;padding:0px">Setup Monster Information:</h3></td>
	</tr>
	<?php
	if($fault_string_msg == "") {
		if($_GET['msg'] == "suc")
		{
			?>
			<tr>
				<td colspan="2" style="color:red">Successfully Updated</td>
			</tr>		
			<?php
		}	
	}
	
	if($fault_string_msg != "") {
		?>
		<tr>
			<td colspan="2">
				<?php echo $fault_string_msg;?>
			</td>
		</tr>
		<?php 
	}
	if($fault_string_msg == "" && is_array($inventory_details)) {
		?>
		<tr><td><a href="javascript:void(0);" onclick="$('.inventory_info').toggle('slow')">Click here to see inventory details</a></td></tr>
		<tr>
			<td colspan="2">
		<table border="1" cellpadding="2" cellspacing="5" width="100%" class="inventory_info"
		style="border-collapse: collapse; border-color: #E4EBED;display:none;">
		<tr>
			<td valign="top" width="5%"><strong>Serial No</strong></td>
			<td valign="top" width="15%"><strong>License Type / Job Board Id</strong></td>
			<td valign="top" width="15%"><strong>Total Available</strong></td>
			<td valign="top" width="15%"><strong>Has Bold</strong></td>
			<td valign="top" width="15%"><strong>Location</strong></td>
			<td valign="top" width="15%"><strong>Has Auto Refresh<br>(Status - Duration)</strong></td>
			<td valign="top" width="15%"><strong>Post Duration</strong></td>
			<td valign="top" width="15%"><strong>CAN Inventory Duration (optional)</strong></td>
			<td valign="top" width="15%"><strong>Has Job Single Refresh (optional)</strong></td>
		</tr>
		<?php
	  	$inventory_types = array_keys($inventory_details);
	  	$sirno = 0;
	  	$inventory_ids_list = array();
	  	$has_refresh_license_ids = array();

	  	for($r = 0; $r < count($inventory_types); $r++) {
		
			$inventory_details[$inventory_types[$r]]['RowSpan'] = count($inventory_details[$inventory_types[$r]]['TotalAvailable']);
		
			if(isset($inventory_details[$inventory_types[$r]]['CANLocations']) && is_array($inventory_details[$inventory_types[$r]]['CANLocations']))
			{
				foreach ($inventory_details[$inventory_types[$r]]['CANLocations'] as $inventory_can_location) {
					$inventory_details[$inventory_types[$r]][$inventory_can_location]['CANDuration'] = array_unique($inventory_details[$inventory_types[$r]][$inventory_can_location]['CANDuration']);
				}	
			}
		}

	  	foreach($inventory_types as $inventory_license_id) {
	  
			for($iti = 0; $iti < count($inventory_details[$inventory_license_id]['TotalAvailable']); $iti++)
			{
				$has_bolding = "0";
				$has_autorefresh_status = "0";
				$has_autorefresh_duration = "0";
				$posting_duration = "0";

				$inventory_job_board_id = $inventory_details[$inventory_license_id]['JobBoardID'][$iti];
				$inventory_license_name = $inventory_details[$inventory_license_id]['LicenseName'][$iti];

				if(isset($inventory_details[$inventory_license_id]['HasBolding'][$iti])) {
					$has_bolding = $inventory_details[$inventory_license_id]['HasBolding'][$iti];
				}
				if(isset($inventory_details[$inventory_license_id]['HasAutoRefresh']['Status'][$iti])) {
					$has_autorefresh_status = $inventory_details[$inventory_license_id]['HasAutoRefresh']['Status'][$iti];
				}
				if(isset($inventory_details[$inventory_license_id]['HasAutoRefresh']['Duration'][$iti]))
				{
					$has_autorefresh_duration = $inventory_details[$inventory_license_id]['HasAutoRefresh']['Duration'][$iti];
				}
				if(isset($inventory_details[$inventory_license_id]['PostingDuration'][$iti]))
				{
					$posting_duration = $inventory_details[$inventory_license_id]['PostingDuration'][$iti];
				}

				$location = $inventory_details[$inventory_license_id]['Location'][$iti];

				$has_bolding_job = ($has_bolding == "0" || $has_bolding == 0) ? "false" : "true";
				$has_bolding_display = ($has_bolding == "0" || $has_bolding == 0) ? "no" : "yes";

				$has_autorefresh_status_job = ($has_autorefresh_status == "0" || $has_autorefresh_status == 0) ? "false" : "true";
				$has_autorefresh_status_display = ($has_autorefresh_status == "0" || $has_autorefresh_status == 0) ? "no" : "yes";
				?>
				<tr>
					<td><?php echo ++$sirno;?></td>
					<td><?php echo $inventory_license_name;?> / <?php echo $inventory_job_board_id;?></td>
					<td valign="top" width="15%"><?php echo $inventory_details[$inventory_license_id]['TotalAvailable'][$iti];?></td>
					<td><?php echo ucfirst($has_bolding_display);?></td>
					<td><?php echo ucfirst($location);?></td>
					<td><?php echo ucfirst($has_autorefresh_status_display);?> - <?php echo $has_autorefresh_duration;?></td>
					<td><?php echo $posting_duration;?></td>
					<?php
					$rowspan = $inventory_details[$inventory_license_id]['RowSpan'];
					if(!in_array($inventory_license_id, $inventory_ids_list)) {
						$inventory_ids_list[] = $inventory_license_id;
						?>
						<td rowspan="<?php echo $rowspan;?>">
							<?php
							if(isset($inventory_details[$inventory_license_id]['CANLocations']) && is_array($inventory_details[$inventory_license_id]['CANLocations'])) {
								foreach ($inventory_details[$inventory_license_id]['CANLocations'] as $inventory_can_location) {

									for($c = 0; $c < count($inventory_details[$inventory_license_id][$inventory_can_location]['CANDuration']); $c++) {
										if(isset($inventory_details[$inventory_license_id][$inventory_can_location]['CANDuration'][$c])) {
											$inventory_can_duration = $inventory_details[$inventory_license_id][$inventory_can_location]['CANDuration'][$c];
								
											$can_duration_value[$inventory_license_id] = $inventory_can_duration;
								
											echo $inventory_can_duration."&nbsp;".$inventory_can_location."<br>";
										}
									}
								
								}
	
							}
							?>
						</td>
						<?php
					}
			
					if(!in_array($inventory_license_id, $has_refresh_license_ids)) {
						$has_refresh_license_ids[] = $inventory_license_id;
						?>
						<td rowspan="<?php echo $rowspan;?>">
						<?php
						if(isset($inventory_details[$inventory_license_id]['JobRefreshLocations']) && is_array($inventory_details[$inventory_license_id]['JobRefreshLocations']))
						{
							foreach ($inventory_details[$inventory_license_id]['JobRefreshLocations'] as $inventory_refresh_location) {
									
								if($inventory_details[$inventory_license_id][$inventory_refresh_location]['HasJobSingleRefresh'] == 1) {
									$has_job_refresh_value = $inventory_license_id.":1:".$inventory_refresh_location;
									?>&nbsp;Yes - <?php echo $inventory_refresh_location;?><?php
								}
												
							}	
						}
						?>
						</td>
						<?php
					}
					?>
				</tr>
				<?php
			}
	  	} 
	  	?>
	  	</table>
	  	</td>
	  	</tr>
	  	<?php
	}
	?>
	<tr>
		<td colspan="2"><strong>Monster Account Information for paid listings:</strong></td>
	</tr>
	<tr>
		<td width="10%">RecruiterReference\UserName</td>
		<td>
			<input type="text" name="RecruiterReferenceUserName" id="RecruiterReferenceUserName" value="<?php echo $MonsterInfo['RecruiterReferenceUserName'];?>">
		</td>
	</tr>
	<tr>
		<td width="10%">Password</td>
		<td>
			<input type="password" name="Password" id="Password" value="<?php echo $MonsterInfo['Password'];?>">
		</td>
	</tr>
	<tr>
		<td colspan="2"><strong>Free Monster Settings:</strong></td>
	</tr>
	<tr>	
		<td width="50%">Duration</td>
		<td>
			<select name="ddlDuration" id="ddlDuration">
				<option value="30" <?php if($MonsterInfo['Duration'] == "30") echo "selected='selected'";?>>30</option>
				<option value="60" <?php if($MonsterInfo['Duration'] == "60") echo "selected='selected'";?>>60</option>
				<option value="90" <?php if($MonsterInfo['Duration'] == "90") echo "selected='selected'";?>>90</option>
			</select>
		</td>
	</tr>
	<tr>	
		<td width="50%">Job Posting Method Type</td>
		<td>
			<select name="ddlPostingMethodType" id="ddlPostingMethodType">
				<option value="POST" <?php if($MonsterInfo['PostType'] == "POST") echo "selected='selected'";?>>POST - Minimal</option>
				<option value="REQUEST" <?php if($MonsterInfo['PostType'] == "REQUEST") echo "selected='selected'";?>>REQUEST - Maximum</option>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2"><strong>iRecruit Application:</strong></td>
	</tr>
	<tr>	
		<td width="50%">Do you want to display the "Apply with Monster" button on the iRecruit application form? (Functionality will be POST method)</td>
		<td>
			<select name="ddlAWM" id="ddlAWM">
				<option value="1" <?php if($MonsterInfo['FlagApplyWithMonster'] == "1") echo "selected='selected'";?>>Yes</option>
				<option value="0" <?php if($MonsterInfo['FlagApplyWithMonster'] == "0") echo "selected='selected'";?>>No</option>
			</select>
		</td>
	</tr>

	
	<tr>	
		<td colspan="2"><input type="submit" name="freelistings" id="freelistings" value="Submit" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton'?>"></td>
	</tr>

	<tr>
		<td colspan="2">
			<b><i>*Note1:</i></b>
			Please provide your Employer/RecruiterReference Username, Password to post jobs to Monster. 
			<br>It is mandatory for paid listings and charges will be applicable from Monster.
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<strong>About "POST" Method:</strong>&nbsp;&nbsp;<i>(minimal information captured)</i><br> 
			1) A user applies for a position using their Monster login and information only. If user has successfully applied for a job in Monster their details will be saved to your iRecruit Account.
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<strong>About "REQUEST" Method:</strong>&nbsp;&nbsp;<i>(maximum information captured)</i><br>
			1) A user applies for a position using their Monster login and information. Once they have successfully entered their Monster information they are redirected to the iRecruit job application form. <br>
			2) The user will need to fill out the iRecruit application form with additional information and submit it before their information is saved to your iRecruit Account.
		</td>
	</tr>
	<tr>

	</table>
	<input type="hidden" name="hidefreelistings" id="hidefreelistings" value="yes">
</form>
