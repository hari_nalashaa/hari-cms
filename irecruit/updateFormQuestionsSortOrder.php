<?php
require_once 'Configuration.inc';

for($c = 0; $c < count($_REQUEST['forms_info']); $c++) {
    
    $FormID     =   $_REQUEST['forms_info'][$c]['FormID'];
    $SectionID  =   $_REQUEST['forms_info'][$c]['SectionID'];
    $QuestionID =   $_REQUEST['forms_info'][$c]['QuestionID'];
    $SortOrder  =   $_REQUEST['forms_info'][$c]['SortOrder'];
    
    $set_info   =   array("QuestionOrder = :QuestionOrder");
    $where_info =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "QuestionID = :QuestionID");
    $params     =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$SectionID, ":QuestionID"=>$QuestionID, ":QuestionOrder"=>$SortOrder);
    
    //Update Question Order
    G::Obj('FormQuestions')->updQuestionsInfo("FormQuestions", $set_info, $where_info, array($params));
}
?>