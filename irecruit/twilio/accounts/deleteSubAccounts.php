<?php
require_once '../../irecruitdb.inc';

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use Twilio\Exceptions\TwilioException;

// Find your Account Sid and Auth Token at twilio.com/console
// and set the environment variables. See http://twil.io/secure
$sid        =   "ACf4724c6fc1f251929845443be42fd688";   // Main Account SID
$token      =   "";     // Main Account Token
$twilio     =   new Client($sid, $token);

$sub_account_sid    =   ""; //Update the sub account number

try {
    //Code to close the account
    $account    =   $twilio->api->v2010->accounts($sub_account_sid)->update(["status" => "closed"]);
}
catch (TwilioException $e) {
    echo "<pre>";
    print_r($e);
}
catch (Exception $e) {
    echo "<pre>";
    print_r($e);
}
    
/*
$accounts   =   $twilio->api->v2010->accounts->read();

foreach ($accounts as $record) {
    echo $record->sid . " - " . $record->friendlyName . "<br>";
}
*/
?>
