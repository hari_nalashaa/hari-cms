<?php
require_once '../../irecruitdb.inc';

// Posted Data
$data   =   file_get_contents('php://input');
//$data   =   file_get_contents(ROOT . "logs/wotc_autopilot/2021-03-02-03-wotc-auto-pilot.txt");
$POST   =   json_decode($data, true);

// Get Address States List
$results    =   G::Obj('Address')->getAddressStateList ();
$states_res =   $results['results'];
$states     =   array();

foreach ($states_res as $state_info) {
    $states[strtolower($state_info['Description'])] = $state_info['Abbr'];
}

Logger::writeMessage(ROOT."logs/wotc_autopilot/". date('Y-m-d-H') . "-wotc-auto-pilot.txt", $data, "a+", false);

$call_response_data =   array(
                            "Source"            =>  "Twilio Autopilot",
                            "RawInputText"      =>  $data,
                            "PostData"          =>  json_encode($_POST),
                            "GetData"           =>  json_encode($_GET),
                            "CreatedDateTime"   =>  "NOW()",
                            "ModifiedDateTime"  =>  "NOW()"
                        );
//Insert Raw Input
G::Obj('GenericQueries')->insRowsInfo("RawInput", $call_response_data);

//Collections List
$collections_list   =   array(
                            "wotc_application_questions",
                            "wotc_social_security",
                            "wotc_application_questions_after_ssn",
                            "wotc_have_you_worked_questions",
                            "wotc_snap_questions",
                            "wotc_tanf_questions",
                            "wotc_veteran_us_armed_forces_questions",
                            "wotc_veteran_us_snap_questions",
                            "wotc_ssi_questions",
                            "wotc_convicted_of_felony_questions",
                            "wotc_spouse_enrollment"
                        );
$memory             =   json_decode($POST['memory'], true);

$SAVED_WOTCID       =   G::Obj('WOTCTwilioPhoneNumbers')->getWotcIDByPhoneNumber($memory['twilio']['voice']['To'], "ivr");

if(isset($memory) && count($memory) > 0) {

    //Answers List
    $answers_list       =   array();
    foreach ($collections_list as $collection_id) {
        if(isset($memory['twilio']['collected_data'][$collection_id])) {
            $answers_list[] =   $memory['twilio']['collected_data'][$collection_id]['answers'];
        }
    }
    
    //Answers Info
    $answers        =   array();
    
    foreach($answers_list as $key=>$value_array) {
        if(is_array($value_array)) {
            foreach($value_array as $question_id=>$question_info) {
                if(trim($question_id) == "") $question_id = "city";
                @$answers[$question_id] = $question_info["answer"];
            }
        }
    }
    
    $ssn            =   @$answers['SSN'];
    $ssn            =   preg_replace("/[^0-9]/", "", $ssn );
    $ssn            =   str_replace(" ", "", preg_replace("/[^0-9]/", "", $ssn ));
    
    //Update ApplicationID
    $set_info       =   array("ApplicationID = LAST_INSERT_ID(ApplicationID+1)");
    $where_info     =   array("wotcID = :wotcID");
    $params         =   array(":wotcID"=>$SAVED_WOTCID);
    G::Obj('WOTCOrganization')->updOrgDataInfo($set_info, $where_info, array($params));
    
    G::Obj('GenericQueries')->conn_string =   "WOTC";
    $query          =   "SELECT LAST_INSERT_ID() AS LAST_INSERT_ID";
    $last_ins_info  =   G::Obj('GenericQueries')->getRowInfoByQuery($query);
    $ID             =   $last_ins_info['LAST_INSERT_ID'];
    $ApplicationID  =   'W' . substr('0000000' . $ID, - 8);
    
    $org_info   	=   G::Obj('WOTCOrganization')->getOrganizationInfo("WotcFormID", $SAVED_WOTCID);
    $WotcFormID 	=   $org_info['WotcFormID'];

    
    //Insert Applicant Data
    $info                           =   array();
    $info['wotcID']                 =   $SAVED_WOTCID;
    $info['ApplicationID']          =   $ApplicationID;
    $info['WotcFormID']             =   $WotcFormID;
    $info['EntryDate']              =   "NOW()";
    $info['FirstName']              =   @$answers['FirstName'];
    $info['MiddleName']             =   @$answers['MiddleName'];
    $info['LastName']               =   @$answers['LastName'];
    
    $ssn1                           =   substr($ssn, 0, 3);
    $ssn2                           =   substr($ssn, 3, 2);
    $ssn3                           =   substr($ssn, 5, 4);
    
    $info['Social1']                =   $ssn1;
    $info['Social2']                =   $ssn2;
    $info['Social3']                =   $ssn3;
    
    $info['Address']                =   @$answers['Address'];
    $info['City']                   =   @$answers['City'];
    $info['State']                  =   $states[strtolower(@$answers['State'])];
    $info['ZipCode']                =   @$answers['ZipCode'];
    $info['County']                 =   @$answers['County'];
    
    $info['DOB']                    =   @$answers['DOB'];;
    $info['DOB_Date']               =   @$answers['DOB'];
    $info['TANF']                   =   @$answers['TANF'];
    $info['TANF_Recipient']         =   @$answers['TANF_Recipient'];
    $info['TANF_City']              =   @$answers['TANF_City'];
    $info['TANF_State']             =   $states[strtolower($answers['TANF_State'])];
    $info['SNAP']                   =   @$answers['SNAP'];
    $info['SNAP_Recipient']         =   @$answers['SNAP_Recipient'];
    $info['SNAP_City']              =   @$answers['SNAP_City'];
    $info['SNAP_State']             =   $states[strtolower($answers['SNAP_State'])];
    
    $info['SSI']                    =   @$answers['SSI'];
    
    $felony2                        =   "";
    if(@$answers['was_this_a_federal_state_conviction'] == "Federal")
    {
        $felony2 = "F";
    }
    if(@$answers['was_this_a_federal_state_conviction'] == "State")
    {
        $felony2 = "S";
    }
    
    $info['Felony']                 =   @$answers['Felony'];
    $info['Felony2']                =   $felony2;
    $info['FelonyConviction_Date']  =   @$answers['FelonyConviction_Date'];
    $info['FelonyRelease_Date']     =   @$answers['FelonyRelease_Date'];
    
    $info['Veteran']                =   @$answers['Veteran'];
    $info['Veteran2']               =   @$answers['Veteran2'];
    $info['Veteran3']               =   @$answers['Veteran3'];
    $info['Veteran4']               =   @$answers['Veteran4'];
    $info['Veteran5']               =   @$answers['Veteran5'];
    $info['Veteran6']               =   @$answers['Veteran6'];
    $info['VET_Recipient']          =   @$answers['VET_Recipient'];
    $info['VET_City']               =   @$answers['VET_City'];
    $info['VET_State']              =   $states[strtolower(@$answers['VET_State'])];
    $info['Veteran7']      	        =   @$answers['Veteran7'];
    
    $info['SWA2']                   =   @$answers['SWA2'];
    $info['SWA3']                   =   @$answers['SWA3'];
    $info['SWA4']                   =   @$answers['SWA4'];
    
    $info['Worked']                 =   @$answers['Worked'];
    $info['Worked_Date']            =   @$answers['Worked_Date'];
    $info['LTU']                    =   @$answers['LTU'];
    $info['LTU_State']              =   $states[strtolower(@$answers['LTU_State'])];
    $info['iRecruitID']             =   @$answers['irecruitlookup'];
    $info['Initiated']              =   "ivr";
    
    $info['Processed']              =   'N';
    $info['Signature']              =   'ONFILE';
    $info['NativeAmerican']	        =   '';
    $info['ApplicationStatus']      =   "H";
    
    G::Obj('WOTCApplicantData')->insApplicantDataInfo($info);
    
    /* Process Data */
    
    // Insert complete, now read data and analyse to finish processing
    list ($PROCESSOR, $ORG, $APPDATA, $PRS, $Signature, $Date, $One, $Two, $Three, $Four, $Five, $Six, $Seven, $workedA, $workedAN, $ck12A, $ck12AN, $ck13A, $ck13AN, $ck13B, $ck13BN, $ck13C, $ck13CN, $ck13D, $ck13DN, $ck13E, $ck13EN, $ck14A, $ck14AN, $ck14B, $ck14BN, $ck15A, $ck15AN, $ck15B, $ck15BN, $ck15C, $ck15CN, $ck16A, $ck16AN, $ck16B, $ck16BN, $ck16C, $ck16CN, $ck16D, $ck16DN, $ck17A, $ck17AN, $ck18A, $ck18AN, $ck19A, $ck19AN, $ck20A, $ck20AN, $ck21A, $ck21AN, $ck22A, $ck22AN, $ck23A, $ck23AN, $items) = G::Obj('WOTCPdf')->getData($SAVED_WOTCID, $ApplicationID);
    
    $set_info   =   array("DocumentsNeeded = :DocumentsNeeded");
    $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
    $params     =   array(":DocumentsNeeded"=>$items, ":wotcID"=>$SAVED_WOTCID, ':ApplicationID'=>$ApplicationID);
    G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
    
    // preliminary approval
    if (($One == "Yes")
            || ($Two == "Yes")
            || ($Three == "Yes")
            || ($Four == "Yes")
            || ($Five == "Yes")
            || ($Six == "Yes")
            || ($Seven == "Yes")) {
    
        $set_info   =   array("Processed = 'R'", "Processed_Date = NOW()");
        $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
        $params     =   array(":wotcID"=>$SAVED_WOTCID, ':ApplicationID'=>$ApplicationID);
        G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
    
    } // end if
    
    // New 2/26/2013
    $RCEZType       =   "";
    $RCEZName       =   "";
    $where_info     =   array("ZipCode = :ZipCode");
    $params_info    =   array(':ZipCode'=>$_POST['ZipCode']);
    $rrc_ez_info    =   G::Obj('WOTCRRC_EZ_2013')->getRRC_EZ_2013Info("*", $where_info, "ZipCode", array($params_info));
    $ZC             =   $rrc_ez_info['results'];
    $ENThit         =   sizeof($ZC);
    
    foreach ($ZC as $RCE) {
        $RCEZType = $RCE['ZoneType'];
        if ($RCE['ZoneType'] == 'Rural Renewal County') {
            $RCEZName = $RCE['County'] . " - " . $RCE['CityState'];
        } else {
            $RCEZName = $RCE['ZoneName'];
        }
    } // end foreach
    
    if ($ENThit > 0) {
        $set_info   =   array("EnterpriseZone = 'Y'", "Processed_Date = NOW()", "RCEZName = :RCEZName", "RCEZType = :RCEZType");
        $where_info =   array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
        $params     =   array(":wotcID"=>$SAVED_WOTCID, ":ApplicationID"=>$ApplicationID, ":RCEZName"=>$RCEZName, ":RCEZType"=>$RCEZType);
        G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
    } // end if
    
    G::Obj('WOTCApp')->processThankYou($SAVED_WOTCID, $ApplicationID);
    
    /* Process Data */

    ###########################
    //Clear properties
    G::Obj('PHPMailer')->clearCustomProperties();
    G::Obj('PHPMailer')->clearCustomHeaders();
    
    ############## Send Mail To Applicant #####################
    if (($OE['Email']) && ($OE['EmailVerified'] == 1)) {
        // Set who the message is to be sent from
        G::Obj('PHPMailer')->setFrom("WebApps@iRecruit-us.com", "iRecruit");
        // Set an alternative reply-to address
        G::Obj('PHPMailer')->addReplyTo("WebApps@iRecruit-us.com", "iRecruit");
    }
    
    // Set who the message is to be sent to
    G::Obj('PHPMailer')->addAddress ( 'dedgecomb@irecruit-software.com' );
    // Content Type Is HTML
    G::Obj('PHPMailer')->ContentType = 'text/html';
    
    $message    =   "New Application Information Received: $SAVED_WOTCID";
    $message    .=  print_r($last_ins_info, true);
    $message    .=  print_r($data, true);
    
    // Set the subject line
    G::Obj('PHPMailer')->Subject = "WOTC Information";
    // convert HTML into a basic plain-text alternative body
    G::Obj('PHPMailer')->msgHTML ( $message );
    
    ###########################
    
    if($ApplicationID != "") {
        //Send Mail
        G::Obj('PHPMailer')->send ();
        
        //Get CCID
        $CCID   =   G::Obj('WOTCOrganization')->getCCID($SAVED_WOTCID, $ApplicationID);

    	if ($CCID != "") {	
              $json_response  =   array("Status"=>"Success", "UserMessage"=>"Please give this ID to your supervisor. Your ID is: " . $CCID . ".", "CCID"=>$CCID);
    	} else {
              $json_response  =   array("Status"=>"Success", "UserMessage"=>"", "CCID"=>"");
    	}
        
        echo json_encode($json_response);
    }
    else {
        echo json_encode(array("Status"=>"Failed", "UserMessage"=>"Failed to generate application id"));
    }
}
else {
    echo json_encode(array("Status"=>"Failed", "UserMessage"=>"There is an issue with the data"));
}
?>
