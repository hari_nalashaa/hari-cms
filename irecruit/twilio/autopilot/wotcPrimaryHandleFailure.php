<?php
require_once '../../irecruitdb.inc';

// Posted Data
$data   =   file_get_contents('php://input');
$POST   =   json_decode($data, true);

//Clear properties
G::Obj('PHPMailer')->clearCustomProperties();
G::Obj('PHPMailer')->clearCustomHeaders();

$message    =   "Dear Team, <br><br> There is a failure in WOTC IVR Call. Please find the information below.<br><br>Thanks<br>Irecruit Support Team";
$message    .=  "DATA: " . print_r($data)."<br>";
$message    .=  "POST: " . print_r($POST, true)."<br>";

$to         =   'dedgecomb@irecruit-software.com';

$subject    =   "WOTC IVR - Failure";
$from       =   $POST ['EmailAddress'];

// Set who the message is to be sent to
G::Obj('PHPMailer')->addAddress ( $to );
// Set the subject line
G::Obj('PHPMailer')->Subject = $subject;
// convert HTML into a basic plain-text alternative body
G::Obj('PHPMailer')->msgHTML ( $message );
// Attach an coverletter file
G::Obj('PHPMailer')->addAttachment ( $coverLetter );
// Attach an resume file
G::Obj('PHPMailer')->addAttachment ( $resumeDocument );
// Content Type Is HTML
G::Obj('PHPMailer')->ContentType = 'text/html';

// send the message, check for errors, anyway we will not execute this file in browser
G::Obj('PHPMailer')->send();
?>
