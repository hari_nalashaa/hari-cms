<?php
require_once '../../Configuration.inc';

/**
 * @tutorial	This code should be updated eventually after testing.
 */

$conversations		=	G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $_REQUEST['ResourceID']);
$conversation_res	=	$conversations['Response'];
$messages			=	array();

for($c = 0; $c < count($conversation_res); $c++) {
	
	$message_obj	=	$conversation_res[$c];
	
	//Get the Protected properties from SMS object, by extending the object through ReflectionClass
	$reflection     =   new ReflectionClass($message_obj);
	$property       =   $reflection->getProperty("properties");
	$property->setAccessible(true);
	$messages[]		=   $property->getValue($message_obj);
}
?>
<table border="1" cellpadding="2" cellspacing="2">
<tr>
	<td>Author</td>
	<td>Message</td>
	<td>Date Sent</td>
</tr>
<?php
//Get messages list
for($m = 0; $m < count($messages); $m++) {
	$message_info	=	$messages[$m];
	?>
	<tr>
		<td><?php echo $message_info['author'];?></td>
		<td><?php echo $message_info['body'];?></td>
		<td>
			<?php 
			//Here there is a conflict with Predefined dateobject in php, 
			//So use the get_object_vars or some other functiion to retrieve the data
			$date_sent_info	=	get_object_vars($message_info['dateCreated']);
			echo $date_sent_info['date'];
			?>
		</td>
	</tr>
	<?php
}
?>
</table>