<?php
require_once '../../Configuration.inc';

/**
 * @tutorial	This code should be updated eventually after testing.
 */
$conversation_info	=	G::Obj('TwilioConversationInfo')->getConversationResourceInfo($OrgID, $USERID, $_REQUEST['ResourceID']);
$mobile_numbers		=	json_decode($conversation_info['MobileNumbers'], true);

if(isset($_REQUEST['smsMessage']) && $_REQUEST['smsMessage'] != "") {

	$author					=	$_REQUEST['NUM']['NID1'];
	$conversation_msg_res	=	G::Obj('TwilioConversationApi')->createConversationMessage($OrgID, $_REQUEST['ResourceID'], $author, $_REQUEST['smsMessage']);
	
	header("Location:sendConversationMessage.php?ResourceID=".$_REQUEST['ResourceID']."&msg=msg_suc");
	exit;
}
?>
<form name="frmConversationMsg" id="frmConversationMsg" method="post">
<table width="100%" border="0" cellpadding="3" cellspacing="1">
	<tr>
		<td colspan="2">Send Text.</td>
	</tr>
	<?php 
		if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'msg_suc') {
			?>
			<tr>
				<td colspan="2" style="color:blue"><?php echo "Successfully sent the message.";?></td>
			</tr>
			<?php
		}
	?>
	<tr>
		<td width="10%"><strong>From Number: </strong></td>
		<td>
			<input type="text" name="NUM[NID1]" id="NID1" value="<?php echo $mobile_numbers[0][0];?>" readonly>
		</td>
	</tr>
	<tr>
		<td width="10%"><strong>To Number: </strong></td>
		<td>
			<input type="text" name="NUM[NID2]" id="NID2" value="<?php echo $mobile_numbers[0][1];?>" readonly>
		</td>
	</tr>
	<tr>
		<td width="10%"><strong>Message:</strong></td>
		<td>
			<textarea name="smsMessage" id="smsMessage"></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="left">
			<input type="submit" name="btnSendSMS" id="btnSendSMS" value="Send Sms">
		</td>
	</tr>
</table>
</form>