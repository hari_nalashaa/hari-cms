<?php
require_once '../Configuration.inc';

//Get all messages
$messages_response	=	G::Obj('TwilioSmsApi')->getAllMessages($OrgID);
$messages_list		=	$messages_response['Response'];

?>
<table border="1" cellpadding="2" cellspacing="2">
<tr>
	<td>From</td>
	<td>To</td>
	<td>Message</td>
	<td>Date Sent</td>
</tr>
<?php
//Get messages list
for($m = 0; $m < count($messages_list); $m++) {
	$message_obj	=	$messages_list[$m];
	
	//Get the Protected properties from SMS object, by extending the object through ReflectionClass
	$reflection     =   new ReflectionClass($message_obj);
	$property       =   $reflection->getProperty("properties");
	$property->setAccessible(true);
	$message_info	=   $property->getValue($message_obj);

	?>
	<tr>
		<td><?php echo $message_info['from'];?></td>
		<td><?php echo $message_info['to'];?></td>
		<td><?php echo $message_info['body'];?></td>
		<td>
			<?php 
			//Here there is a conflict with Predefined dateobject in php, 
			//So use the get_object_vars or some other functiion to retrieve the data
			$date_sent_info	=	get_object_vars($message_info['dateSent']);
			echo $date_sent_info['date'];
			?>
		</td>
	</tr>
	<?php
}
?>
</table>