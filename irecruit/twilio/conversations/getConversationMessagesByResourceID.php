<?php
require_once '../../Configuration.inc';

$conversation_resources_info	=	G::Obj('TwilioConversationApi')->fetchAllConversationResources($OrgID);
$conversation_resources			=	$conversation_resources_info['Response'];

$conversation_msgs  =   array();

for($cr = 0; $cr < count($conversation_resources); $cr++) {
    $ResourceID         =   $conversation_resources[$cr]->sid;
    
    $conversations		=	G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $ResourceID);
    $conversation_res	=	$conversations['Response'];
    
    $messages           =	array();
    for($c = 0; $c < count($conversation_res); $c++) {
        
        $message_obj	=	$conversation_res[$c];
        
        //Get the Protected properties from SMS object, by extending the object through ReflectionClass
        $reflection     =   new ReflectionClass($message_obj);
        $property       =   $reflection->getProperty("properties");
        $property->setAccessible(true);
        $message_prop   =   $property->getValue($message_obj);
        
        $messages[$message_prop['sid']] = $property->getValue($message_obj);
    }
    
    $conversation_msgs[$ResourceID] = $messages;    
}

foreach($conversation_msgs as $ResourceID=>$messages) {
    //OrgID is mandatory to pull the account information
    $OrgID  =   G::Obj('TwilioConversationInfo')->getOrgIDByResourceID($ResourceID);
    
    foreach ($messages as $message_id=>$message_info) {
        
        if($OrgID != "") {
            
            $media_count    =   0;
            if(is_array($message_info['media'])) {
                $media_count	=	count($message_info['media']);
            }
            
            for($m = 0; $m < $media_count; $m++) {
                
                $media_info             =	$message_info['media'][$m];
                $media_sid              =	$media_info['sid'];
                
                $media_resource_json	=	G::Obj('TwilioConversationMediaApi')->getConversationMediaResource($OrgID, $media_sid);
                $media_resource_info	=	json_decode($media_resource_json, true);
                
                $media_image_url		=	$media_resource_info['links']['content_direct_temporary'];
                
                //Directory does not exist, so lets create it.
                if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID)) {
                    mkdir(IRECRUIT_DIR . "vault/".$OrgID, 0777);
                    chmod(IRECRUIT_DIR . "vault/".$OrgID, 0777);
                }
                
                //Directory does not exist, so lets create it.
                if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/")) {
                    mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/", 0777);
                    chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/", 0777);
                }
                
                //Directory does not exist, so lets create it.
                if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID)) {
                    mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID, 0777);
                    chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID, 0777);
                }
                
                //Directory does not exist, so lets create it.
                if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id)) {
                    mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id, 0777);
                    chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id, 0777);
                }
                
                //Directory does not exist, so lets create it.
                if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid)) {
                    mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid, 0777);
                    chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid, 0777);
                }
                
                //Get the file
                $content = file_get_contents($media_image_url);
                //Store in the filesystem.
                $fp = fopen(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid."/".$media_info['filename'], "w");
                fwrite($fp, $content);
                fclose($fp);
                
                $info       =   array(
                                    "ConversationResourceID"    =>  $ResourceID,
                                    "MessageSID"                =>  $message_id,
                                    "MediaSID"                  =>  $media_info['sid'],
                                    "FileName"                  =>  $media_info['filename'],
                                    "CreatedDateTime"           =>  "NOW()",
                                    "LastModifiedDateTime"      =>  "NOW()"
                                );
                
                $skip       =   array("CreatedDateTime");
                
                G::Obj('TwilioConversationMediaApi')->insConversationMediaResource($info, $skip);
            }
        
        }
        
    }
}


echo "End";
?>