<?php
require_once '../../irecruitdb.inc';

//Get OrgID by Conversation ID //OrgID is mandatory to pull the account information
$OrgID          =	G::Obj('TwilioConversationInfo')->getOrgIDByResourceID($_POST['ConversationSid']);
$ResourceID     =   $_POST['ConversationSid'];

if($_POST['EventType'] == 'onMessageAdded') {
    
    //Log error messages
    Logger::writeMessage(ROOT."logs/twilio/$OrgID_post_web_hook_".date('Y-m-d')."_db_errors.log", json_encode($_POST), 'a+', false, 'POST-WEB-HOOK', $e);
    
	$author				=	$_POST['Author'];
	
	$participant_sid	=	$_POST['ParticipantSid'];
	$account_sid		=	$_POST['AccountSid'];
	$conversation_sid	=	$_POST['ConversationSid'];

	$participant_detail	=	G::Obj('TwilioConversationParticipantApi')->fetchConversationParticipantInfo($OrgID, $conversation_sid, $participant_sid);
	$participant_info	=	$participant_detail['Response'];
	
	$attributes_info	=	json_decode($participant_info->attributes, true);

	if(isset($attributes_info[0])) {
		$first_name		=	$attributes_info[0]["First"];
		$last_name		=	$attributes_info[0]["Last"];
	}
	
	if($author) {
		$author			=	$last_name . " " . $first_name;
	}

	$body				=	$author.": ". $_POST['Body'];
	
	$updated_post		=	array("body"=>$body);

    $message_id         =   $_POST['MessageSid'];
    $media_info         =   json_decode($_POST['Media'], true);
	
    for($p = 0; $p < count($media_info); $p++) {
        
        $media_sid              =   $media_info[$p]['Sid'];
        $media_resource_json	=	G::Obj('TwilioConversationMediaApi')->getConversationMediaResource($OrgID, $media_info[$p]['Sid']);
        $media_resource_info	=	json_decode($media_resource_json, true);
        
        $media_image_url		=	$media_resource_info['links']['content_direct_temporary'];
        
        #####################
        //Directory does not exist, so lets create it.
        if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID)) {
            mkdir(IRECRUIT_DIR . "vault/".$OrgID, 0777);
            chmod(IRECRUIT_DIR . "vault/".$OrgID, 0777);
        }
        
        //Directory does not exist, so lets create it.
        if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/")) {
            mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/", 0777);
            chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/", 0777);
        }
        
        //Directory does not exist, so lets create it.
        if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID)) {
            mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID, 0777);
            chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID, 0777);
        }
        
        //Directory does not exist, so lets create it.
        if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id)) {
            mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id, 0777);
            chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id, 0777);
        }
        
        //Directory does not exist, so lets create it.
        if(!is_dir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid)) {
            mkdir(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid, 0777);
            chmod(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid, 0777);
        }
        
        //Get the file
        $content = file_get_contents($media_image_url);
        //Store in the filesystem.
        $fp = fopen(IRECRUIT_DIR . "vault/".$OrgID."/twilio/".$ResourceID."/".$message_id."/".$media_sid."/".$media_info[$p]['Filename'], "w");
        fwrite($fp, $content);
        fclose($fp);
        #####################
        
	    $info               =   array(
                        	        "ConversationResourceID"    =>  $ResourceID,
                        	        "MessageSID"                =>  $message_id,
                                    "MediaSID"                  =>  $media_sid,
                            	    "FileName"                  =>  $media_info[$p]['Filename'],
                            	    "CreatedDateTime"           =>  "NOW()",
                            	    "LastModifiedDateTime"      =>  "NOW()"
                            	);
    	$skip              =   array("CreatedDateTime");
    	G::Obj('TwilioConversationMediaApi')->insConversationMediaResource($info, $skip);
	}
	
    
	header("Content-Type: text/json");
	echo json_encode($updated_post);
}