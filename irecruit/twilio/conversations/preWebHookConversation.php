<?php
require_once '../../irecruitdb.inc';

//Get OrgID by Conversation ID	//OrgID is mandatory to pull the account information
$OrgID	=	G::Obj('TwilioConversationInfo')->getOrgIDByResourceID($_POST['ConversationSid']);

if($_POST['EventType'] == 'onMessageAdd') {

    //Log error messages
    Logger::writeMessage(ROOT."logs/twilio/$OrgID_pre_web_hook_".date('Y-m-d')."_db_errors.log", json_encode($_POST), 'a+', false, 'PRE - WEB HOOK', $e);
    
	$author				=	$_POST['Author'];
	
	$participant_sid	=	$_POST['ParticipantSid'];
	$account_sid		=	$_POST['AccountSid'];
	$conversation_sid	=	$_POST['ConversationSid'];
	
	$participant_detail	=	G::Obj('TwilioConversationParticipantApi')->fetchConversationParticipantInfo($OrgID, $conversation_sid, $participant_sid);
	$participant_info	=	$participant_detail['Response'];
	
	$attributes_info	=	json_decode($participant_info->attributes, true);

	if(isset($attributes_info[0])) {
		$first_name		=	$attributes_info[0]["First"];
		$last_name		=	$attributes_info[0]["Last"];
	}
	
	if($author) {
		$author			=	$last_name . " " . $first_name;
	}

	$body				=	$author.": ". $_POST['Body'];
	
	$updated_post		=	array("body"=>$body);
	
	
	header("Content-Type: text/json");
	echo json_encode($updated_post);
}