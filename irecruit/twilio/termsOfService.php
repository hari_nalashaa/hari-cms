<?php
require_once '../Configuration.inc';

$TemplateObj->title =   $title  =   "Terms Of Service";

if(G::Obj('ServerInformation')->getRequestSource() == 'ajax') {
    echo file_get_contents("http://help.irecruit-us.com/wp-content/uploads/2020/05/iRecruit-TXT-Terms-of-Service.htm");
}
else {
    echo $TemplateObj->displayIrecruitTemplate ( 'views/twilio/TermsOfService' );
}
?>