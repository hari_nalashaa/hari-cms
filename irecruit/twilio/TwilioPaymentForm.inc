<?php
echo '<form method="post">';

//Card Types List
$card_types_list    =   array("visa"=>"Visa", "mc"=>"Master Card", "amex"=>"American Express", "discover"=>"Discover");

$cards_information  =   G::Obj('IntuitManageCards')->getAllCardsInfoFromApi($OrgID);
$cards_information  =   json_decode($cards_information, true);

echo '<div class="form-group row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12">';
echo 'The submitted card will be considered as a default payment option';
echo '</div>';
echo '</div>';

echo '<div class="form-group">';
echo '<label class="question_name labels_auto">';
echo '<img src="' . IRECRUIT_HOME . 'images/creditcards_sm.jpg">';
echo '</label>';
echo '</div>';

if(count($cards_information) > 0) {
    echo '<div class="form-group">';
    echo '<label class="question_name">';
    echo 'Saved Cards List:';
    echo '</label>';
    
    echo '<select name="CCSavedCards" id="CCSavedCards" class="form-control width-auto-inline" onchange="hasSavedCard(this.value)">';
    echo '<option value="" '.$saved_card_selected.'>New Card</option>';
    unset($saved_card_selected);
    for($acc = 0; $acc < count($cards_information); $acc++) {
        if($cards_information[$acc]['id'] == $texting_agreement_info['IntuitCardID']) $saved_card_selected = ' selected="selected"';
        echo '<option value="'.$cards_information[$acc]['id'].'" '.$saved_card_selected.'>'.$cards_information[$acc]['name'] . ' - ' . $cards_information[$acc]['number'] .'</option>';
        unset($saved_card_selected);
    }
    echo '</select>';
    echo '</div>';
}

echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if ($CCtype_error == "Y") {
    echo $missing;
}
echo '>Credit Card Type:</i>';
echo '</label>';
echo '<select name="CCtype" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';
foreach($card_types_list as $card_type_key=>$card_type_value) {
    if($card_type_key == $_REQUEST['CCtype']) $card_type_selected = ' selected="selected"';
    echo '<option value="'.$card_type_key.'" '.$card_type_selected.'>'.$card_type_value.'</option>';
    unset($card_type_selected);
}
echo '</select>';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if ($CCnumber_error == "Y") {
    echo $missing;
}
echo '>Credit Card Number:</i>';
echo '</label>';
echo '<input type="text" name="CCnumber" value="' . $_REQUEST['CCnumber'] . '"  class="form-control width-auto-inline" size="20" maxlength="20">';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if (($CCexpmo_error == "Y") || ($CCexpyear_error == "Y")) {
    echo $missing;
}
echo '>Credit Card Exp:</i>';
echo '</label>';
echo '<select name="CCexpmo" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';
for($i = 1; $i <= 12; $i ++) {
    $ii = "0" . $i;
    $ii = substr ( $ii, - 2 );
    echo '<option value="' . $ii . '"';
    if ($ii == $_REQUEST['CCexpmo']) {
        echo ' selected';
    }
    echo '>' . $ii . '</option>';
} // end for
echo '</select>';
echo '/';
echo '<select name="CCexpyear" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';
for($i = $year; $i <= ($year + 10); $i ++) {
    echo '<option value="' . $i . '"';
    if ($i == $_REQUEST['CCexpyear']) {
        echo ' selected';
    }
    echo '>' . $i . '</option>';
} // end for
echo '</select>';
echo '&nbsp;&nbsp;';

$onclickcsc = " onclick=\"javascript:window.open('" . IRECRUIT_HOME . "shopping/cscexplain.php";
if ($_REQUEST['AccessCode'] != "") {
    $onclickcsc .= "?k=" . $AccessCode;
}
$onclickcsc .= "','_blank','location=yes,toolbar=no,height=600,width=550,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";

echo '<a href="#"' . $onclickcsc . ' style="text-decoration: underline;"><i';
if ($CCidentifier1_error == "Y") {
    echo $missing;
}
echo '>CSC:</i></a> <input type="text" name="CCidentifier1" value="' . $_REQUEST['CCidentifier1'] . '" class="form-control width-auto-inline" size="4" maxlength="4">';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name labels_auto">';
echo 'Please enter information as it appears on the credit card.';
echo '</label>';
echo '</div>';

echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if ($FullName_error == "Y") {
    echo $missing;
}
echo '>Name:</i>';
echo '</label>';
echo '<input type="text" name="FullName" value="' . $_REQUEST['FullName'] . '" size="45" maxlength="105" class="form-control width-auto-inline">';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if ($Address1_error == "Y") {
    echo $missing;
}
echo '>Address:</i>';
echo '</label>';
echo '<input type="text" name="Address1" value="' . $_REQUEST['Address1'] . '" size="40" maxlength="45" class="form-control width-auto-inline">';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '&nbsp;';
echo '</label>';
echo '<input type="text" name="Address2" value="' . $_REQUEST['Address2'] . '" size="40" maxlength="45" class="form-control width-auto-inline">';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if ($City_error == "Y") {
    echo $missing;
}
echo '>City:</i>';
echo '</label>';
echo '<input type="text" name="City" value="' . $_REQUEST['City'] . '" size="20" maxlength="45" class="form-control width-auto-inline">';
echo '&nbsp;&nbsp;<i';
if ($State_error == "Y") {
    echo $missing;
}
echo '>State:</i> ';
echo '<select name="State" class="form-control width-auto-inline">';
echo '<option value="">Please Select';

//Get states list
$results = $AddressObj->getAddressStateList();

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        	
        echo "<option value=\"" . $row ['Abbr'] . "\"";
        if ($_REQUEST['State'] == $row ['Abbr']) {
            echo " selected";
        }
        echo ">" . $row ['Description'];
    } // end foreach
}

echo '</select>';

echo '&nbsp;&nbsp;<i';
if ($ZipCode_error == "Y") {
    echo $missing;
}
echo '>ZIP Code</i>: <input type="text" name="ZipCode" value="' . $_REQUEST['ZipCode'] . '" size="10" maxlength="10" class="form-control width-auto-inline">';
echo '</div>';


echo '<div class="form-group has-saved-card">';
echo '<label class="question_name">';
echo '<i';
if ($Country_error == "Y") {
    echo $missing;
}
echo '>Country:</i>';
echo '</label>';
echo '<select name="Country" class="form-control width-auto-inline">';
echo '<option value="">Please Select';

if (($_REQUEST['Country'] == "") && ($_REQUEST['process'] == "")) {
    $Country = "US";
}

//Get countries list
$results = $AddressObj->getCountries();

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        	
        echo "<option value=\"" . $row ["Abbr"] . "\"";
        if ($Country == $row ["Abbr"] || ($_REQUEST['Country'] == $row ["Abbr"])) {
            echo " selected";
        }
        echo ">" . $row ["Description"];
        echo "</option>\n";
    } // end foreach
}

echo '</select>';
echo '</div>';

echo '<div class="form-group">';
echo '<label class="question_name labels_auto">';
echo 'By clicking "Purchase" I am subscribing to iRecruit TXT services and my card will be charged <span style="font-weight:bold" id="payment_form_formatted_amt"></span> per month plus any calculated amount in excess of the monthly base credits.';
echo '</label>';
echo '</div>';


echo '<div class="form-group">';
echo '<label class="question_name labels_auto">';
echo '<input type="hidden" name="amount" id="amount" value="' . $_REQUEST['amount'] . '">';
echo '</label>';
echo '</div>';

echo '<input type="hidden" name="Email" value="' . $user_details['EmailAddress'] . '">';
echo '<input type="hidden" name="Phone" value="' . $user_details['Phone'] . '">';

echo '</form>';

echo '<script>';
echo 'function hasSavedCard(saved_card_val) {
    if(saved_card_val != "") {
        $(".has-saved-card").hide();
    }
    else {
        $(".has-saved-card").show();
    }
}';
echo '</script>';

if(isset($IntuitCardID) && $IntuitCardID != "") {
    echo '<script>';
    echo '$(".has-saved-card").hide();';
    echo '</script>';
}
?>
