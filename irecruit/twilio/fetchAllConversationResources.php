<?php
require_once '../irecruitdb.inc';

//Get Twilio Accounts
$twilio_accounts_info   =   G::Obj('TwilioAccounts')->getTwilioAccountsList("OrgID ASC");
$twilio_accounts_res    =   $twilio_accounts_info['results'];

$OrgID  =   (isset($_REQUEST['TwilioAccounts']) && $_REQUEST['TwilioAccounts'] != "") ? $_REQUEST['TwilioAccounts'] : 'B12345467';

if(isset($_REQUEST['ResourceID']) 
    && $_REQUEST['ResourceID'] != ""
    && isset($_REQUEST['action'])
    && $_REQUEST['action'] == 'delete') {
        
    $conversation_res_info  =   G::Obj('TwilioConversationInfo')->getConversationInfoByResourceID($_REQUEST['ResourceID']);
    
    $UserID =   $conversation_res_info['UserID'];
    
    if($OrgID != ""
        && $_REQUEST['ResourceID'] != "") {
            
        //Delete Conversation Resource
        $del_res  =   G::Obj('TwilioConversationApi')->deleteConversationResource($OrgID, $_REQUEST['ResourceID'], "MANUAL");
        
        if($del_res['Code'] == "Success") {
            header("Location:fetchAllConversationResources.php?msg=del_success&ResourceID=".$_REQUEST['ResourceID']."&TwilioAccounts=".$OrgID);
            exit();
        }
        else {
            header("Location:fetchAllConversationResources.php?msg=del_failed&ResourceID=".$_REQUEST['ResourceID']."&TwilioAccounts=".$OrgID);
            exit();
        }
    }
    else {
        header("Location:fetchAllConversationResources.php?msg=del_failed&ResourceID=".$_REQUEST['ResourceID']."&TwilioAccounts=".$OrgID);
        exit();
    }
}

$limited_days           =   30; //Update this value based on Archived Days Count

$archive_conversations  =   array();
$conversation_messages  =   array();
$con_res_info           =   array();

//for($cl = 0; $cl < count($conversations_list); $cl++) {

    $conversation_resources_info	=	G::Obj('TwilioConversationApi')->fetchAllConversationResources($OrgID);
    $conversation_resources			=	$conversation_resources_info['Response'];
    $conversation_resources_cnt     =   count($conversation_resources);
    
    $insert_info                    =   array();
    $conversation_ids               =   array();
    $all_conversation_ids           =   array();
    
    for($cr = 0; $cr < $conversation_resources_cnt; $cr++) {
        
        $ResourceID                 =	$conversation_resources[$cr]->sid;
        $all_conversation_ids[]     =   $ResourceID;
        
        //Get the Protected properties from SMS object, by extending the object through ReflectionClass
        $reflection         =   new ReflectionClass($conversation_resources[$cr]);
        $property           =   $reflection->getProperty("properties");
        $property->setAccessible(true);
        $conversation_resource_info     =   $property->getValue($conversation_resources[$cr]);
        
        $con_date_from_obj  =   $conversation_resource_info['dateCreated'];
        $con_date_to_obj    =   $conversation_resource_info['dateUpdated'];
        
        $con_date_from_info =   (array)$conversation_resource_info['dateCreated'];
        $con_date_to_info   =   (array)$conversation_resource_info['dateUpdated'];
        
        $con_earlier        =   new DateTime($con_date_from_obj->format("Y-m-d"));
        $con_later          =   new DateTime(date("Y-m-d"));
        
        $con_days           =   $con_later->diff($con_earlier)->format("%a");

        $conversations_info[$ResourceID]["DateCreated"]  =   $con_date_from_obj->format("Y-m-d");
        
        if($con_days > $limited_days) {
            
            $conversation_res_info  =   G::Obj('TwilioConversationInfo')->getConversationInfoByResourceID($ResourceID);
            $conversation_ids[]     =   $ResourceID;
            
            $conversations		    =	G::Obj('TwilioConversationMessagesApi')->listAllConversationMessages($OrgID, $ResourceID);
            $conversation_res       =	$conversations['Response'];
            $conversation_res_cnt   =   (isset($conversation_res) && (is_object($conversation_res) || is_array($conversation_res))) ? count($conversation_res) : 0;
            
            $messages			    =	array();
            for($c = 0; $c < $conversation_res_cnt; $c++) {
                
                $message_obj        =	$conversation_res[$c];
                
                //Get the Protected properties from SMS object, by extending the object through ReflectionClass
                if(is_object($message_obj)) {
                    $reflection     =   new ReflectionClass($message_obj);
                    $property       =   $reflection->getProperty("properties");
                    $property->setAccessible(true);
                    $messages[]		=   $msg_info   =   $property->getValue($message_obj);
                    
                    $message_info   =   array();
                    
                    $date_from_obj  =   $msg_info['dateCreated'];
                    $date_to_obj    =   $msg_info['dateUpdated'];
                    
                    $date_from_info =   (array)$msg_info['dateCreated'];
                    $date_to_info   =   (array)$msg_info['dateUpdated'];
                    
                    $message_info['ConversationResourceID']     =   $msg_info['conversationSid'];
                    $message_info['ConversationMessageID']      =   $msg_info['sid'];
                    $message_info['Author']                     =   $msg_info['author'];
                    $message_info['Body']                       =   utf8_decode($msg_info['body']);
                    $message_info['DateCreatedDate']            =   $date_from_info['date'];
                    $message_info['DateCreatedTimeZone_Type']   =   $date_from_info['timezone_type'];
                    $message_info['DateCreatedTimeZone']        =   $date_from_info['timezone'];
                    $message_info['DateUpdatedDate']            =   $date_to_info['date'];
                    $message_info['DateUpdatedTimeZone_Type']   =   $date_to_info['timezone_type'];
                    $message_info['DateUpdatedTimeZone']        =   $date_to_info['timezone'];
                    $message_info['CreatedDateTime']            =   "NOW()";
                    
                    //Skip fields
                    $skip_fields    =   array_keys($message_info);
                    array_pop($skip_fields);
                    
                    $earlier    =   new DateTime($date_from_obj->format("Y-m-d"));
                    $later      =   new DateTime(date("Y-m-d"));
                    
                    $days       =   $later->diff($earlier)->format("%a");
                    
                    $conversation_days[$ResourceID][]   =   $days;
                    $conversation_messages[$ResourceID][$msg_info['sid']]           =   $message_info;
                    $conversation_msg_skip_fields[$ResourceID][$msg_info['sid']]    =   $skip_fields;
                    $con_res_info[$ResourceID]  =   $conversation_res_info;
                }
                
            }
            
        }
        
    }
    
    
    //Archived Conversations
    foreach ($conversation_ids as $conversation_resource_id) {
        
        $archive_conv_flag  =   "true";
        
        $con_messages       =   $conversation_messages[$conversation_resource_id];
        $con_skip_fields    =   $conversation_msg_skip_fields[$conversation_resource_id];
        $con_msg_days       =   $conversation_days[$conversation_resource_id];
        $con_arch_info      =   $con_res_info[$conversation_resource_id];
        
        foreach ($con_msg_days as $msg_days) {
            if($msg_days <= $limited_days) {
                $archive_conv_flag  =   "false";
            }
        }
        
        if($archive_conv_flag == "true") {
            $archive_conversations[] =  $conversation_resource_id;
        }
    }
//}

?>
<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>css/plugins/metisMenu/metisMenu.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>css/plugins/dataTables.bootstrap.css">

<script src="<?php echo IRECRUIT_HOME;?>js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME;?>js/jquery-browser-migration.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME;?>js/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>font-awesome-4.1.0/css/font-awesome.min.css">
</head>

<body>
<div id="wrapper">
<?php 
if(isset($_GET['msg']) && $_GET['msg'] != "") {
    ?>
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<?php
			if(isset($_GET['msg']) && $_GET['msg'] == "del_failed") {
                echo "<span style='color:red;'>Failed to delete this conversation</span>";
			}
			else if(isset($_GET['msg']) && $_GET['msg'] == "del_success") {
			    echo "<span style='color:red;'>Successfully deleted this conversation</span>";
			}
			?>
		</div>
	</div>
    <?php
}
?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
    	Conversations Count: <?php echo $conversation_resources_cnt;?>
	</div>
</div>	    
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<br>
		Twilio Accounts: 
		<select name="TwilioAccounts" id="TwilioAccounts" onchange="location.href='?TwilioAccounts='+this.value;">
		<?php
		for($ti = 0; $ti < count($twilio_accounts_res); $ti++) {
		    $TOrgID   =   $twilio_accounts_res[$ti]['OrgID'];
		    $selected =   (isset($OrgID) && $OrgID == $TOrgID) ? ' selected="selected"' :  '';
		    ?>
			<option value="<?php echo $TOrgID;?>" <?php echo $selected;?>>
				<?php echo $TOrgID; ?>
			</option>		    	
		    <?php
		}
		?>
		</select>
		<br><br>
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<table border="1" width="100%" id="conversation-resources" class="table table-bordered">
        	<thead>
            	<tr>
                	<td><strong>Conversation ID</strong></td>
                	<td><strong>Message ID</strong></td>
                	<td><strong>Author</strong></td>
                	<td><strong>Body</strong></td>
                	<td><strong>Created Date</strong></td>
                	<td><strong>Action</strong></td>
                </tr>
        	</thead>
        	<tbody>
            	<?php 
            	foreach ($all_conversation_ids as $conversation_id) {
                    
            	    $messages  =   (isset($conversation_messages[$conversation_id]) && is_array($conversation_messages[$conversation_id])) ? $conversation_messages[$conversation_id] : array();
            	    
            	    if(isset($messages) && count($messages) > 0) {
            	            
        	            foreach ($messages as $message_id=>$conv_message_info) {
        	                $to_time       =   strtotime($conv_message_info['DateCreatedDate']);
        	                ?>
                    	    <tr>
                    	    	<td <?php if(in_array($conversation_id, $archive_conversations)) echo ' style="background-color:grey;"';?>><?php echo $conversation_id;?></td>
                				<td><?php echo $message_id; ?></td>
                    	    	<td><?php echo $conv_message_info['Author']; ?></td>
                    	    	<td><?php echo $conv_message_info['Body']; ?></td>
                    	    	<td data-sort="<?php echo $to_time;?>"><?php echo $conv_message_info['DateCreatedDate']; ?></td>
                    	    	<td>
                    	    		<?php
                    	    		if(in_array($conversation_id, $archive_conversations)) {
                    	    		    ?>
                    	    		    <a href="javascript:void(0);" onclick="deleteConversation('?action=delete&ResourceID=<?php echo $conversation_id;?>&TwilioAccounts=<?php echo $_REQUEST['TwilioAccounts'];?>')">Delete</a>
                    	    		    <?php
                    	    		}
                    	    		?>
    							</td>
                    	    </tr>
                    	    <?php
                	    }
            	    }
            	    else {
            	        ?>
                	    <tr>
                	    	<td <?php if(in_array($conversation_id, $conversation_ids)) echo ' style="background-color:grey;"';?>><?php echo $conversation_id;?></td>
            				<td>-</td>
                	    	<td>-</td>
                	    	<td>-</td>
                	    	<td>
                	    		Conversation Created Date: 
                	    		<?php echo $conversations_info[$conversation_id]["DateCreated"];?>
							</td>
                	    	<td>
            	    		    <a href="javascript:void(0);" onclick="deleteConversation('?action=delete&ResourceID=<?php echo $conversation_id;?>&TwilioAccounts=<?php echo $_REQUEST['TwilioAccounts'];?>')">Delete</a>
							</td>
                	    </tr>
                	    <?php
            	    }
            	    
            	}
            	?>
        	</tbody>
        </table>
	</div>
</div>


</div>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/metisMenu/metisMenu.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/flot/excanvas.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/dataTables/jquery.dataTables.1.10.15.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
function deleteConversation(delete_url) {
	let delete_confirm = confirm("Are you sure, Do you want to delete this conversation!");
	
	if (delete_confirm == true) {
		location.href = delete_url;
	}
}

$(document).ready(function() {
    $('#conversation-resources').DataTable({
        responsive: true,
        aLengthMenu: [
          [50, 100, -1],
          [50, 100, "All"]
        ]
    });
});
</script>
</body>
</html>