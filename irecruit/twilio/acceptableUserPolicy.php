<?php
require_once '../Configuration.inc';

$TemplateObj->title =   $title  =   "Acceptable User Policy";

if(G::Obj('ServerInformation')->getRequestSource() == 'ajax') {
    echo file_get_contents("http://help.irecruit-us.com/wp-content/uploads/2020/05/iRecruit-TXT-Acceptable-Use-Policy.htm");
}
else {
    echo $TemplateObj->displayIrecruitTemplate ( 'views/twilio/AcceptableUserPolicy' );
}
?>