<?php
//Code to save the card - For now it is true, eventually it will be updated.
//isset($_REQUEST['chkDoYouWantToSave']) && $_REQUEST['chkDoYouWantToSave'] == "Y"
$save_card = true;
if($save_card) {
    
    //Change your Access Token Here
    $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
    //Add your request ID here
    $intuit_request_id    =   uniqid(str_replace(".", "", microtime(true)));

    $card_info = array(
        "number"    =>  $_REQUEST['CCnumber'],
        "expMonth"  =>  $_REQUEST['CCexpmo'],
        "expYear"   =>  $_REQUEST['CCexpyear'],
        "name"      =>  $_REQUEST['FullName'],
        "cvc"       =>  $_REQUEST['CCidentifier1'],
        "address"   =>  array(
            "streetAddress"  => $_REQUEST['Address1'],
            "city"           => $_REQUEST['City'],
            "region"         => $_REQUEST['Country'],
            "country"        => $_REQUEST['Country'],
            "postalCode"     => $_REQUEST['ZipCode']
        )
    );
    
    $http_header = array(
        'Accept'          =>  'application/json',
        'Request-Id'      =>  $intuit_request_id,
        'Authorization'   =>  $access_token,
        'Content-Type'    =>  'application/json;charset=UTF-8'
    );
    
    $card_on_file_url       =   G::Obj('IntuitManageCards')->customers_url.$OrgID.'/cards';
    $intuit_save_card_resp  =   $CurlClientObj->makeAPICall($card_on_file_url, "POST", $http_header, json_encode($card_info), null, false);
    
    $req_data  = "\nRequest:".json_encode($http_header)."<br>";
    $req_data .= "\nResponse:".json_encode($intuit_save_card_resp);
    
    Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-card-on-file.txt", $req_data, "a+", false);
    Logger::writeMessageToDb($OrgID, "Intuit", $req_data);
    
    $intuit_save_card_resp  =   json_decode($intuit_save_card_resp, true);
    
    $IntuitCardID = $intuit_save_card_resp['id'];
}
?>
