<?php
require_once '../../Configuration.inc';

$phone_numbers_response	=	G::Obj('TwilioPhoneNumbersApi')->getAvailablePhoneNumbers($OrgID);
$phone_numbers			=	$phone_numbers_response['Response'];

if(count($phone_numbers) > 0) {
    ?>
    <table>
      <tr>
        <th>Phone Number</th>
        <th>Action</th>
      </tr>
      <tr>
        <td>
        	<?php echo $phone_numbers[$p];?>
		</td>
        <td>
        		
        </td>
      </tr>
    </table>
    <?php
}
else {
    echo "No phone numbers assigned to this account";
}

?>