<?php
$TXT_AGREEMENT_ERROR = "";
if($_POST['chkSubscribe'] == "") {
    $TXT_AGREEMENT_ERROR .=   " - Please check the subscribe option.<br>";
}

if($_POST['chkUnderstandIrecruitText'] == "") {
    $TXT_AGREEMENT_ERROR .=   " - Please accept the iRecruit Text additional paid service.<br>";
}

if($_POST['chkAgreeToTermsOfService'] == "") {
    $TXT_AGREEMENT_ERROR .=   " - Please accept the Terms of Service.<br>";
}

if($_POST['chkAgreeToAcceptableUsePolicy'] == "") {
    $TXT_AGREEMENT_ERROR .=   " - Please accept the Acceptable Use Policy.<br>";
}

require_once IRECRUIT_DIR . 'shopping/ValidateBasketInfo.inc';

$ERROR  =   $TXT_AGREEMENT_ERROR . $ERROR;
?>