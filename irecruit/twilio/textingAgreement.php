<?php
require_once '../Configuration.inc';

$TemplateObj->title         =   $title      =   "Texting Agreement";

$texting_agreement_info     =   G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($OrgID);
$twilio_account_info        =	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
$user_details               =   G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "EmailAddress, Phone");
$TemplateObj->IntuitCardID  =   $IntuitCardID   =   $texting_agreement_info['IntuitCardID'];

// Get year
$TemplateObj->year          =   $year       =   G::Obj('MysqlHelper')->getDateTime ('%Y');

if ($USERID == "dave") {
//$test = "Y";
}

if (isset($test) && $test == "Y") {
    $_REQUEST['CCtype']             =   "visa";
    $_REQUEST['CCnumber']           =   "4112344112344113";
    $_REQUEST['CCexpmo']            =   "02";
    $_REQUEST['CCexpyear']          =   "2024";
    $_REQUEST['CCidentifier1']      =   "123";
    $_REQUEST['FullName']           =   "David Edgecomb";
    $_REQUEST['Address1']           =   "462 Long Bow Lane East";
    $_REQUEST['Address2']           =   "";
    $_REQUEST['City']               =   "Becket";
    $_REQUEST['State']              =   "MA";
    $_REQUEST['ZipCode']            =   "01223";
    $_REQUEST['Country']            =   "US";
    $_REQUEST['Email']              =   "dedgecomb@irecruit-software.com";
    $_REQUEST['Phone']              =   "(413) 426-8215";
} // end test

if(isset($_SESSION['I']['payment_information'][$USERID]) && count($_SESSION['I']['payment_information'][$USERID]) > 0)
{
    $_REQUEST['CCSavedCards']       =   $_SESSION['I']['payment_information'][$USERID]['CCSavedCards'];
    $_REQUEST['CCtype']             =   $_SESSION['I']['payment_information'][$USERID]['CCtype'];
    $_REQUEST['CCnumber']           =   $_SESSION['I']['payment_information'][$USERID]['CCnumber'];
    $_REQUEST['CCexpmo']            =   $_SESSION['I']['payment_information'][$USERID]['CCexpmo'];
    $_REQUEST['CCexpyear']          =   $_SESSION['I']['payment_information'][$USERID]['CCexpyear'];
    $_REQUEST['CCidentifier1']      =   $_SESSION['I']['payment_information'][$USERID]['CCidentifier1'];
    $_REQUEST['FullName']           =   $_SESSION['I']['payment_information'][$USERID]['FullName'];
    $_REQUEST['Address1']           =   $_SESSION['I']['payment_information'][$USERID]['Address1'];
    $_REQUEST['Address2']           =   $_SESSION['I']['payment_information'][$USERID]['Address2'];
    $_REQUEST['City']               =   $_SESSION['I']['payment_information'][$USERID]['City'];
    $_REQUEST['State']              =   $_SESSION['I']['payment_information'][$USERID]['State'];
    $_REQUEST['ZipCode']            =   $_SESSION['I']['payment_information'][$USERID]['ZipCode'];
    $_REQUEST['Country']            =   $_SESSION['I']['payment_information'][$USERID]['Country'];
    $_REQUEST['Email']              =   $_SESSION['I']['payment_information'][$USERID]['Email'];
    $_REQUEST['Phone']              =   $_SESSION['I']['payment_information'][$USERID]['Phone'];
}


if(isset($_POST['btnSave']) && $_POST['btnSave'] == 'Purchase') {

    require_once IRECRUIT_DIR . 'twilio/ValidateTwilioAgreementForm.inc';

    $TemplateObj->ERROR = str_replace("\\n", "<br>", $ERROR);
    
    if($ERROR != "") {
        $texting_agreement_info['AgreeToIrecruitPaidText']      =   $_POST['chkUnderstandIrecruitText'];
        $texting_agreement_info['AgreeToTermsOfService']        =   $_POST['chkAgreeToTermsOfService'];
        $texting_agreement_info['AgreeToAcceptableUsePolicy']   =   $_POST['chkAgreeToAcceptableUsePolicy'];
        $texting_agreement_info['Active']                       =   $_POST['chkSubscribe'];
    }
    
    //Have to ask about when it should be triggerd
    if($ERROR == "") {

        //Get Intuit Access Information
        $intuit_access_info   =   G::Obj('Intuit')->getUpdatedAccessTokenInfo();

        if($_REQUEST['CCSavedCards'] == "") {
            //Save Twilio Card Information If it is new card.
            require_once IRECRUIT_DIR . 'twilio/SaveTwilioPaymentCardInfo.inc';
        }
        else {
            $IntuitCardID   =   $_REQUEST['CCSavedCards'];
        }

        if($IntuitCardID != "") {
            require_once IRECRUIT_DIR . 'twilio/ProcessTwilioPaymentInformation.inc';
        }
        else {
            header("Location:textingAgreement.php?menu=8&msg=failedcard");
            exit();
        }
    }
}

$TemplateObj->texting_agreement_info    =   $texting_agreement_info;
$TemplateObj->twilio_account_info       =   $twilio_account_info;

if ($twilio_account_info['Licenses'] > 0) {
  echo $TemplateObj->displayIrecruitTemplate ( 'views/twilio/TextingAgreement' );
} else {
  echo $TemplateObj->displayIrecruitTemplate ( 'views/twilio/TextAgreement' );
}
?>
