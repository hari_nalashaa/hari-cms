<?php
// set where condition
$where      =   array ("substring(PurchaseNumber, 1, 4) = :PurchaseNumber");
// set parameters
$params     =   array (":PurchaseNumber" => $year);
// Get purchases information
$results    =   G::Obj('Purchases')->getPurchasesInfo ( "*", $where, "", array ($params) );
$invcnt     =   $results ['count'];
 
$invcnt ++;
$invcnt     =   "000" . $invcnt;
$invcnt     =   substr ( $invcnt, - 4, 4 );
 
$PurchaseNumber = $year . $invcnt;
 
$digitccnumber = preg_replace ( "/[- ]/", '', $_REQUEST['CCnumber'] );
for($i = 1; $i <= strlen ( substr ( $digitccnumber, 0, - 4 ) ); $i ++) {
    $hide .= "X";
}
$dbccnum = $hide . substr ( $digitccnumber, - 4, 4 );
$dbccexp = $_REQUEST['CCexpyear'] . "-" . $_REQUEST['CCexpmo'] . "-01";
 
//Change your Access Token Here
$access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
//Add your request ID here
$intuit_request_id    =   uniqid(str_replace(".", "", microtime(true)));
 
$CurlClientObj = new CurlClient();
 
$body = [
        "amount"    =>  $_REQUEST['amount'],
        "currency"  =>  "USD",
        "context"   =>  [
            "mobile"        =>  "false",
            "isEcommerce"   =>  "true"
        ]
    ];

if(isset($IntuitCardID) && $IntuitCardID != "") {
    $body["cardOnFile"]   =   $IntuitCardID;
}
 
$http_header = array(
    'Accept'          =>  'application/json',
    'Request-Id'      =>  $intuit_request_id,
    'Authorization'   =>  $access_token,
    'Content-Type'    =>  'application/json;charset=UTF-8'
);
 
$intuit_response = G::Obj('CurlClient')->makeAPICall($IntuitObj->create_charge_url, "POST", $http_header, json_encode($body), null, false);
 
$req_data  = "\nRequest:".json_encode($http_header)."<br>";
$req_data .= json_encode($body)."<br>";
$req_data .= "\nResponse:".json_encode($intuit_response);
 
Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-payment-response.txt", $req_data, "a+", false);
Logger::writeMessageToDb($OrgID, "Intuit", $req_data);

//$intuit_response        =   "{\"errors\":[{\"code\":\"PMT-4000\",\"type\":\"invalid_request\",\"message\":\"card.number is invalid.\",\"detail\":\"card.number\",\"infoLink\":\"https:\/\/developer.intuit.com\/v2\/docs?redirectID=PayErrors\"}]}";
$intuit_payment_info    =   json_decode($intuit_response, true);

if($intuit_payment_info['id'] != "") {
    $intuit_info        =   array(
                                "OrgID"             =>  $OrgID,
                                "PurchaseNumber"    =>  $PurchaseNumber,
                                "Created"           =>  $intuit_payment_info['created'],
                                "Status"            =>  $intuit_payment_info['status'],
                                "Amount"            =>  $intuit_payment_info['amount'],
                                "Currency"          =>  $intuit_payment_info['currency'],
                                "CardInfo"          =>  json_encode($intuit_payment_info['card']),
                                "AvsStreet"         =>  $intuit_payment_info['avsStreet'],
                                "AvsZip"            =>  $intuit_payment_info['avsZip'],
                                "CSCMatch"          =>  $intuit_payment_info['cardSecurityCodeMatch'],
                                "Id"                =>  $intuit_payment_info['id'],
                                "Context"           =>  json_encode($intuit_payment_info['context']),
                                "AuthCode"          =>  $intuit_payment_info['authCode'],
                                "PaymentCategory"   =>  "TWILIO",
                                "CreatedDateTime"   =>  "NOW()"
                            );

    //Always it is card on file
    $intuit_info["CardOnFileID"]    =   $IntuitCardID;

    //Insert intuit payment information
    G::Obj('IntuitPaymentInformation')->insIntuitPaymentInformation($intuit_info);
    
    //Purchase items information
    $purchase_items_info            =   array(
                                            "OrgID"             =>  $OrgID,
                                            "PurchaseNumber"    =>  $PurchaseNumber,
                                            "LineNo"            =>  1,
                                            "ServiceType"       =>  "Twilio Subscription",
                                            "RequestID"         =>  "",
                                            "InvoiceNo"         =>  "",
                                            "BudgetTotal"       =>  $intuit_payment_info['amount'],
                                            "Comment"           =>  "Twilio Subscription"
                                        );
    //Insert purchase information
    G::Obj('Purchases')->insPurchaseInfo ( 'PurchaseItems', $purchase_items_info );
}
 
if ($intuit_payment_info['status'] == "CAPTURED") {
    
    //Save Twilio Texting Agreement Information
    $user_info  =   G::Obj('IrecruitUsers')->getUserInfoByUserID($USERID, "Phone, EmailAddress");
    
    // Set info
    $info       =   array(
                        "OrgID"                         =>  $OrgID,
                        "UserID"                        =>  $USERID,
                        "IntuitCardID"                  =>  $IntuitCardID,
                        "SubscriptionDate"              =>  "NOW()",
                        "LastBilledDateTime"            =>  "NOW()",
                        "Active"                        =>  ($_POST['chkSubscribe'] != "") ? "Y" : "",
                        "TotalAmount"                   =>  $_POST['monthly_fee'],
                        "AgreeToIrecruitPaidText"       =>  $_POST['chkUnderstandIrecruitText'],
                        "AgreeToTermsOfService"         =>  $_POST['chkAgreeToTermsOfService'],
                        "AgreeToAcceptableUsePolicy"    =>  $_POST['chkAgreeToAcceptableUsePolicy'],
                        "UserEmail"                     =>  $user_info['EmailAddress'],
                        "LastModifiedDateTime"          =>  "NOW()",
                    );
    G::Obj('TwilioTextingAgreement')->insTextingAgreementByOrgID($info);
    
    //Unset Payment Information Session
    unset($_SESSION['I']['payment_information']);
    
    $purchases_info     =   array (
                                "OrgID"             =>  $OrgID,
                                "PurchaseNumber"    =>  $PurchaseNumber,
                                "UserID"            =>  $USERID,
                                "PurchaseDate"      =>  "NOW()",
                                "TransactionID"     =>  $intuit_payment_info['id'],
                                "ApprovalCode"      =>  $intuit_payment_info['status'],
                                "AuthAmount"        =>  $intuit_payment_info["amount"],
                                "Processed"         =>  "Y",
                                "CreatedDateTime"   =>  "NOW()",
                                "PaymentCategory"   =>  "TWILIO"
                            );
    
    if(isset($_REQUEST['CCSavedCards']) && $_REQUEST['CCSavedCards'] != "") {
        $purchases_info["FullName"]     =   $intuit_payment_info['card']['name'];
        $purchases_info["Address1"]     =   $intuit_payment_info['card']['address']['streetAddress'];
        $purchases_info["Address2"]     =   '';
        $purchases_info["City"]         =   $intuit_payment_info['card']['address']['city'];
        $purchases_info["State"]        =   '';
        $purchases_info["ZipCode"]      =   $intuit_payment_info['card']['address']['postalCode'];
        $purchases_info["Country"]      =   $intuit_payment_info['card']['address']['country'];
        $purchases_info["Phone"]        =   $_REQUEST['Phone'];
        $purchases_info["Email"]        =   $_REQUEST['Email'];
        $purchases_info["CCtype"]       =   $intuit_payment_info['card']['cardType'];
        $purchases_info["CCnumber"]     =   $intuit_payment_info['card']['number'];
        $purchases_info["CCexp"]        =   $intuit_payment_info['card']['expYear'] . "-" . $intuit_payment_info['card']['expMonth'] . "-01";;
    }
    else {
        $purchases_info["FullName"]     =   $_REQUEST['FullName'];
        $purchases_info["Address1"]     =   $_REQUEST['Address1'];
        $purchases_info["Address2"]     =   $_REQUEST['Address2'];
        $purchases_info["City"]         =   $_REQUEST['City'];
        $purchases_info["State"]        =   $_REQUEST['State'];
        $purchases_info["ZipCode"]      =   $_REQUEST['ZipCode'];
        $purchases_info["Country"]      =   $_REQUEST['Country'];
        $purchases_info["Phone"]        =   $_REQUEST['Phone'];
        $purchases_info["Email"]        =   $_REQUEST['Email'];
        $purchases_info["CCtype"]       =   $_REQUEST['CCtype'];
        $purchases_info["CCnumber"]     =   $dbccnum;
        $purchases_info["CCexp"]        =   $dbccexp;
    }
    
    //Insert purchase information
    G::Obj('Purchases')->insPurchaseInfo ( 'Purchases', $purchases_info );
     
    if (isset($_REQUEST['Email']) && $_REQUEST['Email'] != "") {
        
        $to         =   $_REQUEST['Email'];
        $subject    =   "Your iRecruit Order # " . $PurchaseNumber;

        $message .= '<img src="' . PUBLIC_HOME . 'images/iRecruit.png"><br><br>';
        
        $message .= "This email is to confirm your recent purchase with iRecruit." . "<br><br>";

        $message .= "Purchase Date: " . $MysqlHelperObj->getDateTime('%m/%d/%Y') . "<br>";
        $message .= "Purchase Number: " . $PurchaseNumber . "<br>";
        $message .= "Purchaser's Name: " . $intuit_payment_info['card']['name'] . "<br>";
        $message .= "Payment Details: $" . $intuit_payment_info ["amount"] . " was charged to card ending in " . substr($intuit_payment_info['card']['number'],-4) . "<br>";
        $message .= "Merchant Name: COST MANAGEMENT SERVICE LLC" . "<br><br>";
         
        $message .= "Information regarding your order can also be found under iRecruit Administration on the ";
        $message .= "<a href=\"" . IRECRUIT_HOME . "administration.php?action=purchases\">Purchases</a> page." . "<br><br>";
        $message .= "Thank you," . "<br>";
        $message .= "iRecruit" . "<br>";
        $message .= "<a href=\"https://www.irecruit-software.com\">www.irecruit-software.com</a>" . "<br>";
        
        $message  = nl2br($message);
        
        //Clear properties
        G::Obj('PHPMailer')->clearCustomProperties();
        G::Obj('PHPMailer')->clearCustomHeaders();
         
        // Set who the message is to be sent to
        G::Obj('PHPMailer')->addAddress ( $to );
        // Set the subject line
        G::Obj('PHPMailer')->Subject = $subject;
        // convert HTML into a basic plain-text alternative body
        G::Obj('PHPMailer')->msgHTML ( $message );
        // Content Type Is HTML
        G::Obj('PHPMailer')->ContentType = 'text/html';
        // send mail
        G::Obj('PHPMailer')->send ();
    } // end email

    header("Location:textingAgreement.php?menu=8&msg=suc&PurchaseNumber=$PurchaseNumber");
    exit();
} else { // else approved

    $_SESSION['I']['payment_information'][$USERID]['CCSavedCards']   =   $_REQUEST['CCSavedCards'];
    $_SESSION['I']['payment_information'][$USERID]['CCtype']         =   $_REQUEST['CCtype'];
    $_SESSION['I']['payment_information'][$USERID]['CCnumber']       =   $_REQUEST['CCnumber'];
    $_SESSION['I']['payment_information'][$USERID]['CCexpmo']        =   $_REQUEST['CCexpmo'];
    $_SESSION['I']['payment_information'][$USERID]['CCexpyear']      =   $_REQUEST['CCexpyear'];
    $_SESSION['I']['payment_information'][$USERID]['CCidentifier1']  =   $_REQUEST['CCidentifier1'];
    $_SESSION['I']['payment_information'][$USERID]['FullName']       =   $_REQUEST['FullName'];
    $_SESSION['I']['payment_information'][$USERID]['Address1']       =   $_REQUEST['Address1'];
    $_SESSION['I']['payment_information'][$USERID]['Address2']       =   $_REQUEST['Address2'];
    $_SESSION['I']['payment_information'][$USERID]['City']           =   $_REQUEST['City'];
    $_SESSION['I']['payment_information'][$USERID]['State']          =   $_REQUEST['State'];
    $_SESSION['I']['payment_information'][$USERID]['ZipCode']        =   $_REQUEST['ZipCode'];
    $_SESSION['I']['payment_information'][$USERID]['Country']        =   $_REQUEST['Country'];
    $_SESSION['I']['payment_information'][$USERID]['Email']          =   $_REQUEST['Email'];  
    $_SESSION['I']['payment_information'][$USERID]['Phone']          =   $_REQUEST['Phone'];

    $message  = $OrgID . "\n";
    $message .= $USERID . "\n";

    if(count($intuit_payment_info) > 0) {
        $message .= print_r($intuit_payment_info, true)."\n";
    }
    
    $err_message = "";
    if(isset($intuit_payment_info['errors']) && count($intuit_payment_info['errors']) > 0) {
        for($e = 0; $e < count($intuit_payment_info['errors']); $e++) {
            $err_message .= $intuit_payment_info['errors'][$e]['message']."\n";
        }
    }

    $message .= $err_message;
    $message  = nl2br($message);
    
    //Clear properties
    G::Obj('PHPMailer')->clearCustomProperties();
    G::Obj('PHPMailer')->clearCustomHeaders();
        
    //$to = 'dedgecomb@irecruit-software.com';
    $to = $_REQUEST['Email'];
    
    // Set who the message is to be sent to
    G::Obj('PHPMailer')->addAddress ( $to );
    // Set the subject line
    G::Obj('PHPMailer')->Subject = 'Response from Payment Gateway';
    // convert HTML into a basic plain-text alternative body
    G::Obj('PHPMailer')->msgHTML ( $message );
    // Content Type Is HTML
    G::Obj('PHPMailer')->ContentType = 'text/html';
    // send mail
    G::Obj('PHPMailer')->send ();
    
    header("Location:textingAgreement.php?menu=8&msg=failed");
    exit();
} // end else approved
?>
