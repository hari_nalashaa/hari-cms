<?php
require_once '../irecruitdb.inc';

//Get Twilio Accounts
$twilio_accounts_info   =   G::Obj('TwilioAccounts')->getTwilioAccountsList();
$twilio_accounts_res    =   $twilio_accounts_info['results'];

$OrgID                  =   (isset($_REQUEST['TwilioAccounts']) && $_REQUEST['TwilioAccounts'] != "") ? $_REQUEST['TwilioAccounts'] : 'B12345467';

//Delete Phone Number
if(isset($_GET['action']) 
    && $_GET['action'] == "delete"
    && isset($_GET['sid'])
    && $_GET['sid'] != "") {
    $del_res =  G::Obj('TwilioPhoneNumbersApi')->deletePhoneNumber($OrgID, $_GET['sid']);
    
    if ($del_res['Code'] == 'Success') {
        header("Location:?msg=del_success&TwilioAccounts=".$_REQUEST['TwilioAccounts']);
        exit();
    }
    else if ($del_res['Code'] == 'Failed') {
        header("Location:?msg=del_failed&TwilioAccounts=".$_REQUEST['TwilioAccounts']);
        exit();
    }
}

$where                  =   array();
$params                 =   array();

if(isset($_REQUEST['TwilioAccounts'])
    && $_REQUEST['TwilioAccounts'] != "") {
    $where              =   array("OrgID = :OrgID");
    $params             =   array(":OrgID"=>$_REQUEST['TwilioAccounts']);
}
    
$account_phone_numbers	=	G::Obj('TwilioPhoneNumbersApi')->getAccountIncomingNumbers($OrgID);

$incoming_phone_numbers	=	$account_phone_numbers['Response'];
$phone_numbers_info     =   $account_phone_numbers['PhoneNumbersInfo'];
?>
<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>css/plugins/metisMenu/metisMenu.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>css/plugins/dataTables.bootstrap.css">

<script src="<?php echo IRECRUIT_HOME;?>js/jquery-1.12.4.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME;?>js/jquery-browser-migration.js" type="text/javascript"></script>
<script src="<?php echo IRECRUIT_HOME;?>js/jquery-ui.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<?php echo IRECRUIT_HOME;?>font-awesome-4.1.0/css/font-awesome.min.css">
</head>

<body>
<div id="wrapper">

    <div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12">
    		<br>
    		Twilio Accounts: 
    		<select name="TwilioAccounts" id="TwilioAccounts" onchange="location.href='?TwilioAccounts='+this.value;">
    		<?php
    		for($ti = 0; $ti < count($twilio_accounts_res); $ti++) {
    		    $TOrgID   =   $twilio_accounts_res[$ti]['OrgID'];
    		    $selected =   (isset($OrgID) && $OrgID == $TOrgID) ? ' selected="selected"' :  '';
    		    ?>
    			<option value="<?php echo $twilio_accounts_res[$ti]['OrgID']?>" <?php echo $selected;?>>
    				<?php echo $twilio_accounts_res[$ti]['OrgID']?>
    			</option>		    	
    		    <?php
    		}
    		?>
    		</select>
    		<br><br>
    	</div>
    </div>
    
    <?php 
    if(isset($_GET['msg']) && $_GET['msg'] != "") {
        ?>
        <div class="row">
    		<div class="col-lg-12 col-md-12 col-sm-12">
    			<?php
    			if(isset($_GET['msg']) && $_GET['msg'] == "del_failed") {
                    echo "<span style='color:red;'>Failed to delete this phone number</span>";
    			}
    			else if(isset($_GET['msg']) && $_GET['msg'] == "del_success") {
    			    echo "<span style='color:blue;'>Successfully deleted this phone number</span>";
    			}
    			?>
    		</div>
    	</div>
        <?php
    }
    ?>

    <div class="row">
    	<div class="col-lg-12 col-md-12 col-sm-12">
        <?php
        if(count($incoming_phone_numbers) > 0) {
            ?>
            <table class="table table-bordered">
              <tr>
              	<td width="2%">#</td>
                <th>Phone Number</th>
                <th>Action</th>
              </tr>
              
              <?php
              $number = 0;
              for($i = 0; $i < count($phone_numbers_info); $i++) {
                  
                  $phone_number =   $phone_numbers_info[$i]['phoneNumber'];
                  $where_info   =   array("MobileNumbers LIKE :MobileNumbers");
                  $params_info  =   array(":MobileNumbers"=>"%".$phone_number."%");
                  
                  $twilio_con_res   =   G::Obj('TwilioConversationInfo')->getAllConversations("ResourceID", $where_info, "","", array($params_info));
                  $twilio_con_list  =   $twilio_con_res['results'];
                  $twilio_con_cnt   =   $twilio_con_res['count'];
                  ?>
                    <tr>
                    	<td>
                        	<?php echo ++$number;?>
                        </td>
                        <td>
                        	<?php echo $phone_numbers_info[$i]['phoneNumber'];?>
                        </td>
                        <td>
                        	<?php
                        	if($twilio_con_cnt == 0) {
                        	    ?>
                        	    <a href="?action=delete&sid=<?php echo $phone_numbers_info[$i]['sid'];?>">Delete</a>
                        	    <?php
                        	}
                        	?>
                        </td>
                    </tr>
                  <?php
              }
              ?>
            </table>
            <?php
        }
        else {
            echo "No phone numbers assigned to this account";
        }
        ?>
        </div>
    </div>
    
</div>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/metisMenu/metisMenu.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/flot/excanvas.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/dataTables/jquery.dataTables.1.10.15.min.js"></script>
<script type="text/javascript" src="<?php echo IRECRUIT_HOME;?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#conversation-resources').DataTable({
        responsive: true,
        aLengthMenu: [
          [50, 100, -1],
          [50, 100, "All"]
        ]
    });
});
</script>
</body>
</html>