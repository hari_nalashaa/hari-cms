<?php
require_once '../../Configuration.inc';

$TemplateObj->title	= "Text Applicants";

//Load scripts in header
$scripts_header[] = "js/loadAJAX.js";
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$scripts_header[] = "js/twilio.js";
$TemplateObj->page_scripts_header = $scripts_header;

//Load scripts in footer
$scripts_footer[] = "js/index.js";
$scripts_footer[] = "js/irec_Display.js";
$scripts_footer[] = "js/forms-internal.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

$script_vars_footer[]	= 'var datepicker_ids = "#from_date, #to_date";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';
$TemplateObj->scripts_vars_footer = $script_vars_footer;

$TemplateObj->FilterOrganization = $FilterOrganization = $_REQUEST['ddlOrganizationsList'];

//Get Twilio Account Information
$account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

//Redirect if account not exist
if($account_info['AccountSid'] == '') {
    header("Location:".IRECRUIT_HOME);
    exit();
}

echo $TemplateObj->displayIrecruitTemplate('views/twilio/ConversationsReport');
?>