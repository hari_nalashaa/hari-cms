<div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse"
		data-target=".navbar-collapse">
		<span class="sr-only">Toggle navigation</span> <span
			class="icon-bar"></span> <span class="icon-bar"></span> <span
			class="icon-bar"></span>
	</button>
	<div class="sidebar-toggle-box-menu hidden-xs">
		<div class="fa fa-bars" data-placement="right" data-original-title="Toggle Navigation"></div>
    </div>
	<a class="navbar-brand" href="<?php echo IRECRUIT_HOME;?>">
	<span size="5" color="#000000"><?php echo $CompanyName;?></span>
	<?php
	if ($DemoAccount == 'Y') {
		echo '<span style="color:red;font-size:12pt;">Demo Account</span><br>';
	}
	?>
	</a>
</div>