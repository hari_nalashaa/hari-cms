<?php
require_once '../Configuration.inc';

//Set action variable
$TemplateObj->action        = $action           = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->ApplicationID = $ApplicationID    = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$TemplateObj->RequestID     = $RequestID        = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$TemplateObj->RequisitionID = $RequisitionID    = isset($_REQUEST['RequisitionID']) ? $_REQUEST['RequisitionID'] : '';
$TemplateObj->FormID        = $FormID           = isset($_REQUEST['FormID']) ? $_REQUEST['FormID'] : '';
$TemplateObj->OrgID         = $OrgID            = isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';

//$userThemeInfo
$TemplateObj->user_preferences = $user_preferences;

date_default_timezone_set($user_preferences['DefaultTimeZone']);

require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
require_once IRECRUIT_DIR . "appointments/ProcessAppointment.inc";

//Load Graphs Related Scripts, Only for this page
$scripts_header[] = "js/loadAJAX.js";
$scripts_header[] = "js/appointmentscheduling.js";
$scripts_header[] = "js/irec_Display.js";
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";

//Set these scripts to header scripts objects
$TemplateObj->page_scripts_header = $scripts_header;

//Set non responsive css for 
$page_styles["header"][] = 'css/non-responsive.css';
//Set page styles information
$TemplateObj->page_styles =  $page_styles;

//Set page title
$TemplateObj->title	= $title = "Schedule Appointment";

echo $TemplateObj->displayIrecruitTemplate('views/appointments/ScheduleAppointment');
?>