<?php
require_once '../irecruitdb.inc';

//If not valid Appointment id - Redirect to home page
$appointmentid = (int)$_GET['appid'];

//Get ScheduleInterview UserPreferences
$userpreferences = $AppointmentsObj->getScheduleIntwUserPreferences($appointmentid);

//Get duration of from userpreferences
$duration = $userpreferences['AppointmentDuration'];

//Get appointment information from scheduleinterview table
$appinfo = $AppointmentsObj->userAppointmentInformation($appointmentid, $duration);

//Set the created appointment timezone
date_default_timezone_set($appinfo['SITimeZone']);

if(!is_int($appointmentid)) {
	header("Location:".IRECRUIT_HOME);
	exit;
}

//Get application information
$APPDATA = $ApplicantsObj->getAppData ( $appinfo['SIOrgID'], $appinfo['SIApplicationID'] );

$Email = $APPDATA ['email'];
$First = $APPDATA ['first'];
$Last = $APPDATA ['last'];
$Name = $First." ".$Last;

$Description = $appinfo[SIDescription];
$Description = str_replace("{Date}", rtrim($appinfo['SIDate']).'\n', rtrim($Description));
$Description = str_replace("{Time}", rtrim($appinfo['SITime']).'\n', rtrim($Description));
$Description = str_replace("{CalendarSection}", "", $Description);
$Description = str_replace("\r\t", "\\n", $Description);
$Description = preg_replace('/<p(.+?)>/', '', trim($Description));
$Description = preg_replace('/<\/p>/', '\\n', trim($Description));
$Description = preg_replace('/<span(.+?)>/', '', trim($Description));
$Description = preg_replace('/<\/span>/', '', trim($Description));
$Description = preg_replace('/<br(.+?)>/', '\\n', trim($Description));
$Description = str_replace("&nbsp;", "", trim($Description));
$Description = str_replace("\r\n", "\\n", $Description);
$Description = addcslashes($Description, ",");

$Location = addcslashes($appinfo[SILocation], ",");
$UID = time().rand(1, 4);
$timestamp = date("Ymd\THis");

$TZOFFSETFROM="-0500";
$TZOFFSETTO="-0400";
$TZNAME="EST";

if ($appinfo[SITimeZone] == "America/Los_Angeles") {
$TZOFFSETFROM="-0800";
$TZOFFSETTO="-0700";
$TZNAME="PDT";
}
if ($appinfo[SITimeZone] == "America/Denver") {
$TZOFFSETFROM="-0700";
$TZOFFSETTO="-0600";
$TZNAME="MDT";
}
if ($appinfo[SITimeZone] == "America/Chicago") {
$TZOFFSETFROM="-0600";
$TZOFFSETTO="-0500";
$TZNAME="CST";
}
if ($appinfo[SITimeZone] == "America/New_York") {
$TZOFFSETFROM="-0500";
$TZOFFSETTO="-0400";
$TZNAME="EST";
}
if ($appinfo[SITimeZone] == "America/Anchorage") {
$TZOFFSETFROM="-0800";
$TZOFFSETTO="-0900";
$TZNAME="AKST";
}
if ($appinfo[SITimeZone] == "America/Adak") {
$TZOFFSETFROM="-1030";
$TZOFFSETTO="-1000";
$TZNAME="HST";
}


$CAL = <<<USERAPPOINTMENTEND
BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//TYPO3/NONSGML Calendar Base (cal) V1.3.2//EN
METHOD:PUBLISH
BEGIN:DAYLIGHT
TZOFFSETFROM:$TZOFFSETFROM
TZOFFSETTO:$TZOFFSETTO
TZNAME:$TZNAME
DTSTART:19700308T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
END:DAYLIGHT
BEGIN:VTIMEZONE
TZID:$appinfo[SITimeZone]
TZURL:http://tzurl.org/zoneinfo-outlook/$appinfo[SITimeZone]
X-LIC-LOCATION:$appinfo[SITimeZone]
END:VTIMEZONE
BEGIN:VEVENT
DTSTAMP:$timestamp
DTSTART;TZID={$appinfo[SITimeZone]}:{$appinfo[begindate]}
DTEND;TZID={$appinfo[SITimeZone]}:{$appinfo[enddate]}
STATUS:CONFIRMED
SUMMARY;LANGUAGE=en-us:$appinfo[SISubject] - $Name
DESCRIPTION:$Description
CLASS:PRIVATE
CREATED:{$appinfo[begindate]}Z
LOCATION:{$Location}
SEQUENCE:1
LAST-MODIFIED:$timestamp
UID:$UID
END:VEVENT
END:VCALENDAR
USERAPPOINTMENTEND;

$filename ="event.ics";

header("Content-Type: text/Calendar; charset=utf-8");
header("Content-Disposition: inline; filename=$filename"); 
echo $CAL;
?>