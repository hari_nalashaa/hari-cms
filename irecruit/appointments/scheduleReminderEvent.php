<?php
require_once '../irecruitdb.inc';

// If not valid Appointment id - Redirect to home page
$reminderid = ( int ) $_GET ['rid'];
if (! is_int ( $reminderid )) {
	header ( "Location:" . IRECRUIT_HOME );
	exit ();
}
//Set columns
$columns = "date_format(DateTime,'%Y%m%dT%H%i00') as begindate, date_format(DATE_ADD(DateTime, INTERVAL 2 HOUR),'%Y%m%dT%H%i00') as enddate, Subject";
$date = $RemindersObj->getAppointmentReminderDetailInfo($columns, $reminderid);

$CAL = <<<REMINDEREVENTEND
BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VEVENT
STATUS:BUSY
DTSTART:$date[begindate]
DTEND:$date[enddate]
SUMMARY;LANGUAGE=en-us:$date[Subject]
DESCRIPTION:
CLASS:PRIVATE
LOCATION:
PRIORITY:5
END:VEVENT
END:VCALENDAR
REMINDEREVENTEND;

$filename = "event.ics";
header ( "Content-Type: text/Calendar" );
header ( "Content-Disposition: inline; filename=$filename" );
echo $CAL;
?>