<?php
if($_REQUEST['Year'] != "" && $_REQUEST['Month'] != "") {
	$SYear = $_REQUEST['Year'];
	$SMonth = (int)$_REQUEST['Month'];
	
	if($_REQUEST['Month'] < 10) $SMonth = '0'.$SMonth;
	$SDay = '01';
}
else if($SIDate != "") {
	$SIDateRow = explode("-", $SIDate);
	$SYear = $SIDateRow[0];
	$SMonth = (int)$SIDateRow[1];
	
	if($SIDateRow[1] < 10) $SMonth = '0'.$SMonth;
	$SDay = '01';
}
else {
	$SYear = date('Y');
	$SMonth = (int)date('m');
	if(date('m') < 10) $SMonth = '0'.$SMonth;
	$SDay = '01';
}

//UserTheme Information
$userThemeInfo = G::Obj('IrecruitUserPreferences')->getUserTheme();

?>
<div style="text-align: center;color:red">
<?php 
if($_GET['msg'] == 'ins' && $_GET['rid'] != '') {
	echo "Reminder Created Successfully";
}
?>
</div>
<?php 
if($_GET['msg'] == 'ins' && $_GET['rid'] != '') {
	?><iframe src="scheduleReminderEvent.php?rid=<?php echo $_REQUEST['rid'];?>" style="height: 1px;border: 0px"></iframe><?php
}	
?>
<div style="vertical-align: top;margin-bottom:6px;">
<a href="applicants.php?action=remindappointments" style="color: black">
<img src="<?php echo IRECRUIT_HOME;?>images/blue-bell-1-16.png" alt="Click Here To Create Reminder" style="vertical-align: bottom">
<strong>Create Reminder</strong></a>
</div>
<div id="CalendarDayView" style="text-align: center">
	<?php echo $AppointmentsObj->calendarListView($SYear, $SMonth, $SDay); ?>
</div>
<div style="border: 1px solid #ffffff;text-align:center;margin-top:8px;">
	<img src="<?php echo IRECRUIT_HOME;?>images/blue-bell-1-16.png" alt="Reminder">&nbsp; Reminder &nbsp;
	<img src="<?php echo IRECRUIT_HOME;?>images/EventReminderSmall.png" alt="Reminder">&nbsp; Appointment &nbsp;
	<img src="<?php echo IRECRUIT_HOME;?>images/icons/application_edit.png" alt="Reminder">&nbsp; Internal Forms &nbsp;
</div>
<script>
var lvcolors = [{"MonthHeader":"<?php echo $userThemeInfo['TableHeader']?>", "WeekEnd":"<?php echo $userThemeInfo['TableColumnColor1']?>", "EventDayBackground":"<?php echo $userThemeInfo['TableColumnColor2']?>"}];
function ChangeViewColor(sc) {
	var MonthHeadingColor = lvcolors[sc].MonthHeader;
	var WeekEnd = lvcolors[sc].WeekEnd;
	var EventDayBackground = lvcolors[sc].EventDayBackground;
	
	$( ".calendar_top_navigation_list tr th" ).css("background-color", MonthHeadingColor);
	$( ".weekend" ).css("background-color", WeekEnd);
	$( ".eventday" ).css("background-color", EventDayBackground);
}
//By Default it is First Color
ChangeViewColor(0);
</script>