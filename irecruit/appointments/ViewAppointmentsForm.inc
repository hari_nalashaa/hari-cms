<?php 
$columns = "YEAR(SIDateTime) as SIYear, MONTH(SIDateTime) as SIMonth";
$resultinterviews = $AppointmentsObj->getScheduleInterviewInfo($columns, array(), 'YEAR(SIDateTime), MONTH(SIDateTime)', '', array());
?>
<form action="applicants.php">
	<input type="hidden" name="Month" id="Month"/>
	<input type="hidden" name="Year" id="Year"/>
	<table>
		<tr><th>View Appointments</th></tr>
		<tr>
			<td>
				<select name="action" id="action">
					<option value="calendartableview">Calendar Table View</option>
					<option value="calendarlistview">Calendar List View</option>
				</select>
			</td>
		
			<td>
				<select name="SIDate" id="SIDate" onchange="set_month_year(this.value);">
					<?php
					if(is_array($resultinterviews['results'])) {
						foreach($resultinterviews['results'] as $row) {
							?><option value="<?php echo $row['SIYear']."-".$row['SIMonth']?>"><?php echo $row['SIYear']." ".$months[$row['SIMonth']]?></option><?php
						}
					}
					?>
				</select>
			</td>
			
			<td>
				<input type="submit" name="scheduledappointments" id="scheduledappointments" value="View Appointments" style="background-color:#f5f5f5;background:none;color:black;border:1px solid grey;">&nbsp;
			</td>
		</tr>
	</table>
	
	<input type="hidden" name="ApplicationID" id="ApplicationID" value="<?php echo $ApplicationID;?>">
	<input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID?>">
	<input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID?>">
</form>