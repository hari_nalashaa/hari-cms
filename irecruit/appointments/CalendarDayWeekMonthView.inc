<style>
.dashbaord_cal_active_view {
    background-color: #ded9d9 !important;
    color: black !important;
}
</style>
<div style="vertical-align: top;margin-bottom:6px;">
<a href="applicants.php?action=remindappointments" style="color: black">
<img src="<?php echo IRECRUIT_HOME;?>images/blue-bell-1-16.png" alt="Click Here To Create Reminder" style="vertical-align: bottom">
<strong>Create Reminder</strong></a>
</div>
<div style="width: 100%;">
	<form name="frmDashboardCalendarNavigation" id="frmDashboardCalendarNavigation" method="post">
		<input type="hidden" name="selected_year" id="selected_year" value="<?php echo date('Y');?>">
		<input type="hidden" name="selected_month" id="selected_month" value="<?php echo date('m');?>">
		<input type="hidden" name="selected_day" id="selected_day" value="<?php echo date('d');?>">
		<input type="hidden" name="selected_week_start_date" id="selected_week_start_date" value="<?php echo date('Y-m-d');?>">
		<input type="hidden" name="selected_calendar_view" id="selected_calendar_view" value="month">
		<input type="hidden" name="today_date" id="today_date" value="<?php echo date('Y-m-d');?>">
		
		<div class='table-responsive'>
		<table width='100%' border='0' cellpadding='5' cellspacing='0' class='table table-bordered table-hover calendar_top_navigation'>
			<tr>
				<th align="center" id="tcviewprevious" onclick="loadCalendarView('previous')">
					<a href="javascript:void(0)"><<</a>
				</th>
				<th align="center" id="tcviewnext" onclick="loadCalendarView('next')">
					<a href="javascript:void(0)">>></a>
				</th>
				<th align="center" id="tcviewtoday" onclick="loadTodayCalendar();"><span>Today</span></th>
				<th align="center" align="center" width="40%">
					<span id="calendar_date_label" class="dashboard_calendar_date_label">
					<?php echo date('M')." ".date('Y');?>
					</span>
				</th>
				<th align="center" class="dashbaord_cal_active_view" id="tcviewmonth" onclick="dashboardCalendarDataView(this.id)">
				<span>Month</span></th>
				<th align="center" id="tcviewweek" onclick="dashboardCalendarDataView(this.id)"><span>Week</span></th>
				<th align="center" id="tcviewday" onclick="dashboardCalendarDataView(this.id)"><span>Day</span></th>
			</tr>
		</table>
		</div>
	</form>


	<div id="cdatamonth" class='calendardataview'>
	<?php echo $AppointmentsObj->calendarTableView ( date ( 'Y' ), date ( 'm' ), date ( 'd' ), 'NonResponsive' ); ?>
	</div>

	<div id="cdataweek" class='calendardataview' style="display: none">
	<?php echo $AppointmentsObj->calendarWeekView ( date ( 'Y' ), date ( 'm' ), date ( 'd' ) );?>
	</div>

	<div id="cdataday" class='calendardataview' style="display: none">
		<?php echo $AppointmentsObj->calendarDayView ( '2015', '05', '15' ); ?>
	</div>
</div>
<div style="border: 1px solid #ffffff;text-align:center;margin-top:8px;">
	<img src="<?php echo IRECRUIT_HOME;?>images/blue-bell-1-16.png" alt="Reminder">&nbsp; Reminder &nbsp;
	<img src="<?php echo IRECRUIT_HOME;?>images/EventReminderSmall.png" alt="Reminder">&nbsp; Appointment &nbsp;
	<img src="<?php echo IRECRUIT_HOME;?>images/icons/application_edit.png" alt="Reminder">&nbsp; Internal Forms &nbsp;
</div>

<script type="text/javascript" src="<?php echo IRECRUIT_HOME . 'js/index.js'?>"></script>
<script>
//By Default it is First Color
ChangeViewColor(0);
</script>