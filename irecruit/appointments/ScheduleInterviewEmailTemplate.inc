<?php
/**
 * Retrieve Template Information
 */
$where              =   array ("OrgID = :OrgID");
$params             =   array (":OrgID" => $OrgID);

$results_template   =   $EmailObj->getEmailTemplateInfo ( "*", $where, "", array ($params) );
$templateinfo       =   $results_template ['results'] [0];
$isbcc              =   $templateinfo ['IsBcc'];

if(isset($RequestID) && $RequestID != "") {
    $multiorgid_req =   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
    $jobtitle       =   $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $RequestID );
}

$message_bottom     =   "{CalendarSection}";

/**
 * @tutorial This if and else condition will be executed before clicking on Schedule Interview
 */
if (isset($_REQUEST ['sidescription']) &&  $_REQUEST ['sidescription'] != '') {
	$message       =   html_entity_decode ( $_REQUEST ['sidescription'] );
}
else if (isset($_REQUEST ['emailbodytemplate']) && $_REQUEST ['emailbodytemplate'] == 'at') {
	$message       =   html_entity_decode ( $templateinfo ['Body'] );
	$message      .=   $message_bottom;
} else if (isset($_REQUEST ['emailbodytemplate']) 
            && $_REQUEST ['emailbodytemplate'] != 'at' 
            && isset($_REQUEST ['emailbodytemplate'])
            && $_REQUEST ['emailbodytemplate'] != '') {
	// Set columns
	$columns       =   "Subject, Body";
	// Set where condition
	$where         =   array (
                			"OrgID       =   :OrgID",
                			"UpdateID    =   :UpdateID" 
                       );
	// Set parameters
	$params        =   array (
                			":OrgID"     =>  $OrgID,
                			":UpdateID"  =>  $_REQUEST ['emailbodytemplate'] 
                       );
	// Get Correspondence Letters
	$resetemplatebody = $CorrespondenceObj->getCorrespondenceLetters ( $columns, $where, 'Classification, Subject', array (
			$params 
	) );
	$rowetemplatebody = $resetemplatebody ['results'] [0];
	
	$message = html_entity_decode ( $rowetemplatebody ['Body'] );
	$message .= $message_bottom;
}
else {
	$message = html_entity_decode ( $templateinfo ['Body'] );
	$message .= $message_bottom;
}

$viewmessage = $message;

if(!isset($_REQUEST['emailbodytemplate'])) $_REQUEST['emailbodytemplate'] = '';

if((isset($_REQUEST['emailbodytemplate']) && $_REQUEST['emailbodytemplate'] == 'at') 
    || ($_REQUEST['emailbodytemplate'] == ''))
{
	$calendarsection = "<p><span style=\"font-size: 10pt;\">Please click on icon below to add this appointment to your calendar.</span></p><p>{CalendarIcons}</p><p>or</p><p>{Location}</p><p>{RequestNewTime}</p><p>&nbsp;</p>";
}
else {
	$calendarsection = '<p><span style="color: #7aa1e6; font-weight: bold;">Appointment Confirmation:</span> <br /><br /><span style="font-size: 10pt;">Your appointment is confirmed</span><br /> <br /><span style="font-size: 10pt;">Date:</span> {Date}<br /><span style="font-size: 10pt;">Time:</span> {Time}<br /><span style="font-size: 10pt;">Instructions:</span> Please give me a call at the appointed time.<br /><br /></p>
							<p><span style="font-size: 10pt;">Please click on icon below to add this appointment to your calendar.</span></p>
							<p><span style="display: block;">{CalendarIcons}<br /> </span></p>
							<p><span style="font-size: 10pt;">&nbsp;</span></p>
							<p><span id="codevalue">{Location}</span></p>
							<p><span id="codevalue">{RequestNewTime}</span></p>';
}

$viewmessage = str_replace("{CalendarSection}", $calendarsection, $viewmessage);

if($templateinfo['GoogleCalendar'] == '1') $calendaricons['{AddToGoogleCalendar}'] = '<a href={AddToGoogleCalendar}><img src="'.IRECRUIT_HOME.'images/google.png" alt="Google" style="margin-right:10px;" /></a>';
if($templateinfo['OutlookCalendar'] == '1') $calendaricons['{AddToOutlookCalendar}'] = '<a href={AddToOutlookCalendar}><img src="'.IRECRUIT_HOME.'images/outlook.png" alt="Outlook" style="margin-right:10px;" /></a>';
if($templateinfo['IcalCalendar'] == '1') $calendaricons['{AddToIcalCalendar}'] = '<a href={AddToLotusCalendar}><img src="'.IRECRUIT_HOME.'images/lotus.png" alt="Lotus" style="margin-right:10px;" /></a>';
if($templateinfo['LotusCalendar'] == '1') $calendaricons['{AddToLotusCalendar}'] = '<a href={AddToIcalCalendar}><img src="'.IRECRUIT_HOME.'images/ical.png" alt="Ical" style="margin-right:10px;" /></a>';

if(is_array($calendaricons)) $selectedcicons = implode("\n", $calendaricons);

$viewmessage = str_replace("{CalendarIcons}", $selectedcicons, $viewmessage);

$viewmessage = str_replace("{RequestNewTime}", '<a href={RequestNewTime}>Request New Time</a>', $viewmessage);
$viewmessage = str_replace("{Location}", '<a href={Location}>Click Here To See The Location In Google Map</a>', $viewmessage);

if(is_object($TemplateObj)) {
	$TemplateObj->templateinfo =   $templateinfo;
	$TemplateObj->isbcc        =   $isbcc;
	$TemplateObj->jobtitle     =   isset($jobtitle) ? $jobtitle : '';
	$TemplateObj->message      =   $message;
	$TemplateObj->viewmessage  =   $viewmessage;
}
?>