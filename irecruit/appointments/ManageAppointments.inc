<?php
$userThemeInfo = G::Obj('IrecruitUserPreferences')->getUserTheme ();
date_default_timezone_set ( $userThemeInfo ['DefaultTimeZone'] );

$months     = $GenericLibraryObj->getMonths();

$APPDATA    = $ApplicantsObj->getAppData ( $OrgID, $ApplicationID );

$ToEmail    = $APPDATA ['email'];
$First      = $APPDATA ['first'];
$Last       = $APPDATA ['last'];

// Set where condition
$where_org = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
// Set parameters
$params_org = array(":OrgID"=>$OrgID, ":MultiOrgID" => $MultiOrgID);
//Results OrgData Information
$results_org_info = $OrganizationsObj->getOrgDataInfo ( "*", $where_org, '', array ($params_org) );
$OrgData = $results_org_info ['results'] [0];                                                              

//Get User Information
$userinfo = $IrecruitUsersObj->getUserInfoByUserID($USERID, "*");

$FromEmail = $userinfo ['EmailAddress'];

$MAddress = array (
		$OrgData ['Address1'],
		$OrgData ['Address2'],
		$OrgData ['City'],
		$OrgData ['State'],
		$OrgData ['ZipCode'] 
);
$CAddress = array (
		$OrgData ['ContactAddress'],
		$OrgData ['ContactAddress2'],
		$OrgData ['City'],
		$OrgData ['State'],
		$OrgData ['ZipCode'] 
);

// It will unset the null values, it will make a clean array
$MAddress = array_filter ( $MAddress );
$CAddress = array_filter ( $CAddress );

$MyOfficeAddress = implode ( ",", $MAddress );
$CorporateAddress = implode ( ",", $CAddress );


//Get ScheduledAppointment Information
$columns = "SID, SISubject, SICc, SILocation, SIDescription, DATE(SIDateTime) as SIDate, 
		   date_format(SIDateTime,'%h') as Hours, date_format(SIDateTime,'%i') as Minutes,
		   date_format(SIDateTime,'%p') as AmPm, SIRequestID, SIApplicationID";
$rowappointment = $AppointmentsObj->getScheduledAppointment($_REQUEST['sid'], $columns);

// ########################################################################################
// #### Mail Code
// ########################################################################################

$msg = '';

/**
 * Retrieve Template Information
 */
$where  = array("OrgID = :OrgID");
$params = array(":OrgID"=>$OrgID);
$results_template_info = $EmailObj->getEmailTemplateInfo("*", $where, "", array($params));
$templateinfo = $results_template_info['results'][0];
$isbcc = $templateinfo ['IsBcc'];

// Get application information
$APPDATA = $ApplicantsObj->getAppData ( $OrgID, $rowappointment ['SIApplicationID'] );
$multiorgid_req = $RequisitionDetailsObj->getMultiOrgID($OrgID, $rowappointment ['SIRequestID']);
$jobtitle = $RequisitionDetailsObj->getJobTitle ( $OrgID, $multiorgid_req, $rowappointment ['SIRequestID'] );

if ($_REQUEST ['sicc'] != "") {
	for($z = 0; $z < count ( $_REQUEST ['sicc'] ); $z ++) {
		$requestcc = explode ( "**-**", $_REQUEST ['sicc'] [$z] );

		// Set parameters
		$params = array (
				":OrgID" => $requestcc [0],
				":UserID" => $requestcc [1],
				":EmailAddress" => $requestcc [2]
		);
		// Set where condition
		$where = array (
				"OrgID = :OrgID",
				"UserID = :UserID",
				"EmailAddress = :EmailAddress"
		);
		// Get UserEmailLists
		$result_user_email_list = $IrecruitUsersObj->getUserEmailLists ( "*", $where, 'FirstName', array (
				$params
		) );
		$rowcclist = $result_user_email_list ['results'] [0];

		$ccslist [] = $rowcclist ['FirstName'] . " " . $rowcclist ['LastName'] . "<" . $rowcclist ['EmailAddress'] . ">";
	}
}

if ($_REQUEST ['sid'] != '' && isset($_REQUEST ['sidescription']) && $_REQUEST ['sidescription'] != '') {
	$message = $_REQUEST ['sidescription'];
}
else {
	$message = html_entity_decode ( $templateinfo ['Body'] );
}

$message_bottom = "<p><span style=\"font-size: 10pt;\">Please click on icon below to add this appointment to your calendar.</span></p><p>{CalendarIcons}</p><p>&nbsp;</p><p>{Location}</p><p>{RequestNewTime}</p><p>&nbsp;</p><p>{Signature}</p>";
$message_bottom = str_replace ( "{Signature}", '', $message_bottom );

if ($templateinfo ['GoogleCalendar'] == '1')
	$calendaricons ['{AddToGoogleCalendar}'] = '<a href="{AddToGoogleCalendar}"><img src="images/google.png" alt="Google" style="margin-right:10px;" /></a>';
if ($templateinfo ['OutlookCalendar'] == '1')
	$calendaricons ['{AddToOutlookCalendar}'] = '<a href="{AddToOutlookCalendar}"><img src="images/outlook.png" alt="Outlook" style="margin-right:10px;" /></a>';
if ($templateinfo ['IcalCalendar'] == '1')
	$calendaricons ['{AddToIcalCalendar}'] = '<a href="{AddToLotusCalendar}"><img src="images/lotus.png" alt="Lotus" style="margin-right:10px;" /></a>';
if ($templateinfo ['LotusCalendar'] == '1')
	$calendaricons ['{AddToLotusCalendar}'] = '<a href="{AddToIcalCalendar}"><img src="images/ical.png" alt="Ical" style="margin-right:10px;" /></a>';

if (is_array ( $calendaricons ))
	$selectedcicons = implode ( " ", $calendaricons );
$message_bottom = str_replace ( "{CalendarIcons}", $selectedcicons, $message_bottom );
$message_bottom = str_replace ( "{Location}", '<a href="{Location}">Click Here To See The Location In Google Map</a>', $message_bottom );
$message_bottom = str_replace ( "{RequestNewTime}", '<a href="{RequestNewTime}">Request New Time</a>', $message_bottom );

$googlecalendarurl = "http://www.google.com/calendar/event?action=TEMPLATE";
$googlecalendarurl .= "&text=" . $REQUEST ['sisubject'];
$googlecalendarurl .= "&dates=$eventstartdate/$eventenddate";
$googlecalendarurl .= "&details=";
$googlecalendarurl .= "&location=" . $REQUEST ['silocation'];
$googlecalendarurl = $googlecalendarurl;

$message = str_replace ( "{first}", $First, $message );
$message = str_replace ( "{middle}", '', $message );
$message = str_replace ( "{last}", $Last, $message );
$message = str_replace ( "{JobTitle}", $jobtitle, $message );
$message = str_replace ( "{address}", $APPDATA ['address'], $message );
$message = str_replace ( "{address2}", $APPDATA ['address'], $message );
$message = str_replace ( "{city}", $APPDATA ['city'], $message );
$message = str_replace ( "{state}", $APPDATA ['state'], $message );
$message = str_replace ( "{zip}", $APPDATA ['zip'], $message );
$message = str_replace ( "{country}", $APPDATA ['country'], $message );
$message = str_replace ( "{ApplicationID}", $APPDATA ['ApplicationID'], $message );

$message_bottom = str_replace ( "{RequestNewTime}", "mailto:" . $FromEmail, $message_bottom );
$message_bottom = str_replace ( "images/google.png", IRECRUIT_HOME . "images/google.png", $message_bottom );
$message_bottom = str_replace ( "images/lotus.png", IRECRUIT_HOME . "images/lotus.png", $message_bottom );
$message_bottom = str_replace ( "images/outlook.png", IRECRUIT_HOME . "images/outlook.png", $message_bottom );
$message_bottom = str_replace ( "images/ical.png", IRECRUIT_HOME . "images/ical.png", $message_bottom );
$message_bottom = str_replace ( "{AddToGoogleCalendar}", $googlecalendarurl, $message_bottom );

if ($_REQUEST ['dba'] == 'fu' && $_REQUEST ['sid'] != '') {
	
	// Filter Input Values
	$REQUEST = $_REQUEST;
	
	$sidatetime = $DateHelperObj->getYmdFromMdy ( $REQUEST ['sidate'] );
	
	// Calculate PostDate Hours
	$hrs = $REQUEST ['sihrs'];
	if ($REQUEST ['siampm'] == "AM") {
		if ($hrs == 12)
			$hrs = "00";
	} else if ($REQUEST ['siampm'] == "PM") {
		$hrs += 12;
		if ($hrs == 24)
			$hrs -= 12;
	}
	
	// Calculate PostDate Minutes
	$mins = $REQUEST ['simins'];
	if ($REQUEST ['simins'] == 0) {
		$mins = "00";
	}
	$sidatetime .= " " . $hrs . ":" . $mins . ":00";
	
	//Update ScheduleInterview Information
	$resappointment = $AppointmentsObj->updScheduleInterview($sidatetime, $REQUEST ['sisubject'], $REQUEST ['silocation'], $_REQUEST ['sidescription'], $_REQUEST ['sid']);
	
	/**
	 * Php Mail Code
	 */
	$to        =   $ToEmail;
	$subject   =   $_REQUEST ['sisubject'];
	$bcc       =   trim ( $_REQUEST ['sibcc'] );
	$location  =   $_REQUEST ['silocation'];
	$location  =   "http://maps.google.com/maps?q=$location&hl=en";
	$message_bottom = str_replace ( "{Location}", $location, $message_bottom );
	$message   =   html_entity_decode ( $message );
    
	$message_bottom = str_replace ( "{AddToOutlookCalendar}", IRECRUIT_HOME . "appointments/scheduleOutlookUserAppointment.php?appid=" . $_REQUEST ['sid'], $message_bottom );
	$message_bottom = str_replace ( "{AddToLotusCalendar}", IRECRUIT_HOME . "appointments/scheduleUserAppointment.php?appid=" . $_REQUEST ['sid'], $message_bottom );
	$message_bottom = str_replace ( "{AddToIcalCalendar}", IRECRUIT_HOME . "appointments/scheduleUserAppointment.php?appid=" . $_REQUEST ['sid'], $message_bottom );
	$message_bottom = str_replace ( "{Signature}", '<p><span style="font-family: arial; font-size: 10pt;">Sincerely,</span><br /><span style="font-family: arial; font-size: 10pt;">Hiring Manager</span></p>', $message_bottom );
	$message = str_replace ( "{Date}", $REQUEST ['sidate'], $message );
	$message = str_replace ( "{Time}", $REQUEST ['sihrs'] . ":" . $mins . ":00 " . $REQUEST ['siampm'], $message );
	
	$message = str_replace ( "{CalendarSection}", $message_bottom, $message );

	//Clear custom headers
	$PHPMailerObj->clearCustomHeaders();
	//Clear custom properties
	$PHPMailerObj->clearCustomProperties();

	//Add From
	$PHPMailerObj->setFrom($FromEmail);
	
	//Add Bcc
	if ($isbcc == "Y") {
	    $PHPMailerObj->addCustomHeader("BCC", $bcc);
	}
	//Add CC
	if(count($ccslist) > 0) {
	    $PHPMailerObj->addCustomHeader("Cc", implode(", ", $ccslist));
	}
	
	// Set who the message is to be sent to
	$PHPMailerObj->addAddress ( $to );
	// Set the subject line
	$PHPMailerObj->Subject = $subject;
	// convert HTML into a basic plain-text alternative body
	$PHPMailerObj->msgHTML ( $message );
	// Content Type Is HTML
	$PHPMailerObj->ContentType = 'text/html';
	// Mail Send
	$PHPMailerObj->send ();
	// ##########################################################################
	
	echo "<script>location.href='applicants.php?action=manageappointments&msg=upd&sid=" . htmlspecialchars($_REQUEST ['sid']) . "&dba=fa&OrgID=".htmlspecialchars($_REQUEST ['OrgID'])."&ApplicationID=".htmlspecialchars($_REQUEST ['ApplicationID'])."'</script>";
	exit ();
}
if ($_REQUEST ['dba'] == 'delete' && $_REQUEST ['sid'] != '') {
	$res = $AppointmentsObj->delScheduleInterview($_REQUEST ['sid']);
	
	echo "<script>location.href='applicants.php?action=manageappointments&msg=del'</script>";
	exit ();
}
?>
<br>
<br>
<?php
if ($_GET ['msg'] == 'del') {
	echo '<div style="text-align:center">';
	echo '<span style="color: red; font-size: 19px;">Successfully Deleted</span><br><br>';
	echo '<span style="font-size:19px;">&nbsp;&nbsp;<a href="applicants.php?action=calendartableview" style="font-size:19px;">Click here</a> to see appointments</span>';
	echo '</div>';
}
if ($_GET ['msg'] == 'upd') {
	echo '<span style="color: red;padding-left:10px;">Successfully Updated</span>';
}
if ($_REQUEST ['dba'] == 'fa' && $_REQUEST ['sid'] != '') {
	?>
<form name="updateappointment" id="updateappointment" method="post">
	<table border="0" width="100%" cellpadding="5" cellspacing="5"
		class="table table-striped table-bordered table-hover">
		<tr>
			<td width="15%">Recipient</td>
			<td><?php echo $First." ".$Last;?></td>
		</tr>
		<tr>
			<td>Email Address</td>
			<td><a href="mailto:<?php echo $ToEmail;?>"><?php echo $ToEmail;?></a></td>
		</tr>
		<tr>
			<td>Date & Time</td>
			<td><input name="sidate" id="sidate"
				value="<?php echo $DateHelperObj->getMdyFromYmd($rowappointment['SIDate'])?>">
				<select name="sihrs" id="sihrs">
				<?php
					for($h = 1; $h <= 12; $h ++) {
						$h = ($h < 10) ? '0' . $h : $h;
						?>
						  <option value="<?php echo $h;?>" <?php if($rowappointment['Hours'] == $h) echo 'selected="selected"'; ?>>
							<?php echo $h;?>
						  </option>
						<?php
					}
				?>
				</select> 
				<select name="simins" id="simins">
				<?php
				for($m = 0; $m <= 60; $m += 5) {
						$m = ($m == 0) ? '00' : $m;
						?>
						  <option value="<?php echo str_pad($m,2,"0",STR_PAD_LEFT);?>" <?php if($rowappointment['Minutes'] == $m) echo 'selected="selected"'; ?>>
							<?php echo str_pad($m,2,"0",STR_PAD_LEFT);?>
						  </option>
						<?php
				}
				?>
				</select> 
				<select name="siampm" id="siampm">
					<option value="AM"
						<?php if($rowappointment['AmPm'] == "AM") echo "selected='selected'";?>>AM</option>
					<option value="PM"
						<?php if($rowappointment['AmPm'] == "PM") echo "selected='selected'";?>>PM</option>
			</select>&nbsp;
			<?php
			$timezoneinfo = $AppointmentsObj->defaultTimeZone ( $userThemeInfo ['DefaultTimeZone'] );
			echo $timezoneinfo [2];
			?>
			</td>
		</tr>
		<tr>
			<td>Subject</td>
			<td><input type="text" name="sisubject" id="sisubject"
				value="<?php echo $rowappointment['SISubject']?>" /></td>
		</tr>
		<tr>
			<td>Location</td>
			<td><select name="ddlLocation" id="ddlLocation"
				onchange="$('#silocation').val(this.value)">
					<option value="">Select</option>
					<option value="<?php echo $MyOfficeAddress?>">My Office</option>
					<option value="<?php echo $CorporateAddress?>">Corporate Office</option>
					<option value="">Other</option>
			</select> <input type="text" name="silocation" id="silocation"
				value="<?php echo $rowappointment['SILocation']?>" /></td>
		</tr>
		<tr>
			<td valign="top">Email Body</td>
			<td><textarea rows="4" cols="31" name="sidescription"
					id="sidescription" class="mceEditor"><?php echo html_entity_decode(str_replace("&nbsp;", " ", $rowappointment['SIDescription']));?></textarea>
			</td>
		</tr>

		<tr>
			<td colspan="2" align="right"><a href="javascript:void(0)"
				onclick="$('#viewmessage').toggle()">View Message</a></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<div id="viewmessage" style="display: none">
					<?php
					echo html_entity_decode ( str_replace ( "&nbsp;", " ", $message . $message_bottom ) );
					?>
				</div>
			</td>
		</tr>

		<tr>
			<td align="right">
				<?php 
				if(isset($rowappointment ['SICc']) && $rowappointment ['SICc'] != "") {
					$cc_list = explode(",", $rowappointment ['SICc']);
					
					if(is_array($cc_list)) {
						foreach($cc_list as $cc_value) {
							?><input type="hidden" name="sicc[]" value="<?php echo $cc_value;?>"><?php
						}
					}
				}
				?>
				<input type="submit" name="scheduleinterview" id="scheduleinterview" value="Submit" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary btn-sm'; else 'custombutton';?>">
			</td>
			<td align="left">
				<input type="button" name="deletethisappointment" id="deletethisappointment" value="Delete This Appointment" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-danger'; else 'custombutton1';?> " onclick="deleteAppointment('<?php echo htmlspecialchars($_REQUEST['sid']);?>')" />
			</td>
		</tr>
	</table>
	<input type="hidden" name="sid" id="sid" value="<?php echo htmlspecialchars($_REQUEST['sid']);?>"> 
	<input type="hidden" name="dba" id="dba" value="fu">
</form>
<?php
}
?>
<script>
	function deleteAppointment(sid) {
		var c = confirm("Do you want to delete this appointment");
		if (c == true) {
		    location.href = '<?php echo IRECRUIT_HOME;?>applicants.php?action=manageappointments&dba=delete&sid='+sid;
		}
	}
</script>