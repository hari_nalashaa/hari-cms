<?php
$userThemeInfo = G::Obj('IrecruitUserPreferences')->getUserTheme();
date_default_timezone_set($userThemeInfo['DefaultTimeZone']);

if($_REQUEST['arid'] != "") require_once IRECRUIT_DIR . 'appointments/EditDeleteReminder.inc';
else require_once IRECRUIT_DIR . 'appointments/AddReminder.inc';
?>
<form name="frmFlags" id="frmFlags" action="applicants.php?action=remindappointments" method="post" onsubmit="return validateReminderForm(this);">
<input type="hidden" name="SID" id="SID" value="<?php echo htmlspecialchars($SID)?>"/>
<input type="hidden" name="arid" id="arid" value="<?php echo htmlspecialchars($_REQUEST['arid'])?>">
<input type="hidden" name="dba" id="dba" value="fu">
<?php 
if($_GET['msg'] == 'del') {
	echo '<span style="color: red;padding-left:10px;">Successfully Deleted</span>';
}
if($_GET['msg'] == 'upd') {
	echo '<span style="color: red;">Successfully Updated</span>';
}
?>
<table border="0" cellpadding="5" cellspacing="5" class="table table-striped table-bordered table-hover">
		<tr>
			<td colspan="2"><h4 style="padding: 0px; margin:0px;">Create Reminder</h4></td>
		</tr>
		<tr>
		<td colspan="2">
			<?php 
			$timezoneinfo = $AppointmentsObj->defaultTimeZone($userThemeInfo['DefaultTimeZone']);
			?>
			<strong>Note*:</strong> Based on your user preferences settings, your timezone is <?php echo $timezoneinfo[2];?>.
			So your appointment is going to be created with <?php echo $timezoneinfo[2];?> timezone.</td>
		</tr>		
		<tr>
			<td>Date & Time</td>
			<td>
			<input name="sidate" id="sidate" value="<?php echo $DateHelperObj->getMdyFromYmd($rowreminder['Date'])?>" onblur="if(this.value != '') { $('#sidate').removeAttr('style'); }">
			<select name="sihrs" id="sihrs">
				<?php
				for($h = 1; $h <= 12; $h ++) {
					$h = ($h < 10) ? '0'.$h : $h;
					?><option value="<?php echo $h;?>" <?php if($rowreminder['Hours'] == $h) echo 'selected="selected"'; ?>><?php echo $h;?></option><?php
				}
				?>
			</select>
			<select name="simins" id="simins">
			<?php
			for($m = 0; $m <= 60; $m += 5) {
				$m = ($m == 0) ? '00' : $m;
				?><option value="<?php echo str_pad($m,2,"0",STR_PAD_LEFT);?>" <?php if($rowreminder['Minutes'] == $m) echo 'selected="selected"'; ?>><?php echo str_pad($m,2,"0",STR_PAD_LEFT);?></option><?php
			}
			?>
			</select>
			<select name="siampm" id="siampm">
				<option value="AM" <?php if($rowreminder['AmPm'] == "AM") echo "selected='selected'";?>>AM</option>
				<option value="PM" <?php if($rowreminder['AmPm'] == "PM") echo "selected='selected'";?>>PM</option>
			</select>&nbsp;<?php echo $timezoneinfo[2];?></td>
		</tr>
		<tr>
			<td>Subject</td>
			<td><input type="text" name="sisubject" id="sisubject" onblur="if(this.value != '') { $('#sisubject').removeAttr('style'); }" value="<?php if($_REQUEST['arid'] != "") { echo $rowreminder['Subject']; } else { echo $Name; } ?>" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" name="RemindMe" id="RemindMe" value="Remind Me" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton';?>"/>
				&nbsp;&nbsp;
				<?php 
				if($_REQUEST['arid'] != "") {
					?><input type="button" name="deletethisreminder" id="deletethisreminder" value="Delete This Reminder" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-danger'; else echo 'custombutton1';?>" onclick="deleteReminder('<?php echo htmlspecialchars($_REQUEST['arid']);?>')"/><?php	
				}?>
			</td>
		</tr>
	</table>
</form>
<script>
	function validateReminderForm(frmObj) {
		var error = false;
		if($("#sidate").val() == "") {
			$("#sidate").css({"border": "1px solid red"});
			error = true;
		}
		if($("#sisubject").val() == "") {
			$("#sisubject").css({"border": "1px solid red"});
			error = true;
		}
		if(error == true) return false;
		return true;
	}
				
	function deleteReminder(arid) {
		var c = confirm("Do you want to delete this reminder");
		if (c == true) {
		    location.href = '<?php echo IRECRUIT_HOME;?>applicants.php?action=remindappointments&dba=delete&arid='+arid;
		}
	}
</script>
