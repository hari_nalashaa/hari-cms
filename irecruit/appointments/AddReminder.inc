<?php 
$Name = "";
if($SID != "") {
	$appinfo = $AppointmentsObj->getScheduledAppointment($SID);
	$APPDATA = $ApplicantsObj->getAppData ( $appinfo['SIOrgID'], $appinfo['SIApplicationID'] );
	$Email = $APPDATA ['email'];
	$First = $APPDATA ['first'];
	$Last = $APPDATA ['last'];
	$Name = $First." ".$Last;
}

$msg = '';

if($_REQUEST['RemindMe'] == 'Remind Me') {
	$sidatetime = $DateHelperObj->getYmdFromMdy( $_REQUEST['sidate'] );

	//Calculate PostDate Hours
	$hrs = $_REQUEST['sihrs'];
	if($_REQUEST['siampm'] == "AM") {
		if($hrs == 12) $hrs = "00";
	}
	else if($_REQUEST['siampm'] == "PM") {
		$hrs += 12;
		if($hrs == 24) $hrs -= 12;
	}

	//Calculate PostDate Minutes
	$mins = $_REQUEST['simins'];
	if($_REQUEST['simins'] == 0) {
		$mins = "00";
	}
	$sidatetime .= " ".$hrs.":".$mins.":00";

	global $USERID;
	$lastinsertid = $RemindersObj->createReminder($_REQUEST['SID'], $_REQUEST['sisubject'], $sidatetime, $USERID);
	
	$rowpref  = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, $columns = "*");
	
	if($rowpref['DefaultCalendarView'] == "Table") $cview = 'calendartableview';
	if($rowpref['DefaultCalendarView'] == "List") $cview = 'calendarlistview';
	echo "<script>location.href='applicants.php?action=$cview&rid=$lastinsertid&msg=ins'</script>";
	exit;
}
?>