<?php
require_once '../Configuration.inc';

//$userThemeInfo
$TemplateObj->user_preferences = $user_preferences;

date_default_timezone_set($user_preferences['DefaultTimeZone']);

if(isset($_REQUEST['ApplicationID'])) $ApplicationID =  $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID'])) $RequestID = $_REQUEST['RequestID'];

require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
require_once IRECRUIT_DIR . "appointments/ProcessAppointment.inc";
?>