<?php
if ($_REQUEST ['sicc'] != "") {
	for($z = 0; $z < count ( $_REQUEST ['sicc'] ); $z ++) {
		$requestcc = explode ( "**-**", $_REQUEST ['sicc'] [$z] );
		
		// Set parameters
		$params   =   array(
                        ":OrgID"        =>  $requestcc [0],
                        ":UserID"       =>  $requestcc [1],
                        ":EmailAddress" =>  $requestcc [2] 
            		  );
		// Set where condition
		$where    =   array (
                        "OrgID          =   :OrgID",
                        "UserID         =   :UserID",
                        "EmailAddress   =   :EmailAddress" 
		              );
		// Get UserEmailLists
		$result_user_email_list   =   G::Obj('IrecruitUsers')->getUserEmailLists ( "*", $where, 'FirstName', array ($params) );
		$rowcclist                =   $result_user_email_list ['results'] [0];
		
		$ccslist [] = $rowcclist ['FirstName'] . " " . $rowcclist ['LastName'] . "<" . $rowcclist ['EmailAddress'] . ">";//$rowcclist ['EmailAddress']; 
	}
}

$months = G::Obj('GenericLibrary')->getMonths();

// Get application information
$APPDATA = $ApplicantsObj->getAppData ( $OrgID, $ApplicationID );
$TemplateObj->ApplicantEmail = $ApplicantEmail = $APPDATA ['email'];
$TemplateObj->First = $First = $APPDATA ['first'];
$TemplateObj->Last  = $Last = $APPDATA ['last'];

//Have to add applicant and organization information
include_once IRECRUIT_DIR . 'organizations/OrganizationAndApplicantDetails.inc';

// Filter Input Values
$REQUEST = $_REQUEST;

$sidatetime = $DateHelperObj->getYmdFromMdy ( $REQUEST ['sidate'] );

// Calculate PostDate Hours
$hrs = $REQUEST ['sihrs'];
if ($REQUEST ['siampm'] == "AM") {
	if ($hrs == 12)
		$hrs = "00";
} else if ($REQUEST ['siampm'] == "PM") {
	$hrs += 12;
	if ($hrs == 24)
		$hrs -= 12;
}

// Calculate PostDate Minutes
$mins = $REQUEST ['simins'];
if ($REQUEST ['simins'] == 0) {
	$mins = "00";
}
$sidatetime .= " " . $hrs . ":" . $mins . ":00";
$eventstartdate = date ( 'Ymd\THis', strtotime ( $sidatetime ) );
$eventenddate = date ( 'Ymd\THis', strtotime ( "+2 hours", strtotime ( $eventstartdate ) ) );

$msg = '';

include_once IRECRUIT_DIR . 'appointments/ScheduleInterviewEmailTemplate.inc';

// Please click on icon below to add this appointment to your calendar.
$message = str_replace ( "{first}", $First, $message );
$message = str_replace ( "{middle}", '', $message );
$message = str_replace ( "{last}", $Last, $message );
$message = str_replace ( "{JobTitle}", $jobtitle, $message );
$message = str_replace ( "{address}", $APPDATA ['address'], $message );
$message = str_replace ( "{address2}", $APPDATA ['address'], $message );
$message = str_replace ( "{city}", $APPDATA ['city'], $message );
$message = str_replace ( "{state}", $APPDATA ['state'], $message );
$message = str_replace ( "{zip}", $APPDATA ['zip'], $message );
$message = str_replace ( "{country}", $APPDATA ['country'], $message );
$message = str_replace ( "{ApplicationID}", $APPDATA ['ApplicationID'], $message );

$TemplateObj->message = $message;

if ($_REQUEST ['sisubject'] != "") {
	
	if ($_REQUEST ['emailbodytemplate'] == 'at') {
		$calendarsection = "<p><span style=\"font-size: 10pt;\">Please click on icon below to add this appointment to your calendar.</span></p><p>{CalendarIcons}</p><p>or</p><p>{Location}</p><p>{RequestNewTime}</p><p> </p>";
	} else {
		$calendarsection = '<p><span style="color: #7aa1e6; font-weight: bold;">Appointment Confirmation:</span> <br /><br /><span style="font-size: 10pt;">Your appointment is confirmed</span><br /> <br /><span style="font-size: 10pt;">Date:</span> {Date}<br /><span style="font-size: 10pt;">Time:</span> {Time}<br /><span style="font-size: 10pt;">Instructions:</span> Please give me a call at the appointed time.<br /><br /></p>
							<p><span style="font-size: 10pt;">Please click on icon below to add this appointment to your calendar.</span></p>
							<p><span style="display: block;">{CalendarIcons}</span></p>
							<p><span style="font-size: 10pt;"> </span></p>
							<p><span id="codevalue">{Location}</span></p>
							<p><span id="codevalue">{RequestNewTime}</span></p>';
	}
	
	$message = str_replace ( "{CalendarSection}", $calendarsection, $message );
	
	if ($templateinfo ['GoogleCalendar'] == '1')
		$calendaricons ['{AddToGoogleCalendar}'] = '<a href={AddToGoogleCalendar}><img src="' . IRECRUIT_HOME . 'images/google.png" alt="Google" style="margin-right:10px;" /></a>';
	if ($templateinfo ['OutlookCalendar'] == '1')
		$calendaricons ['{AddToOutlookCalendar}'] = '<a href={AddToOutlookCalendar}><img src="' . IRECRUIT_HOME . 'images/outlook.png" alt="Outlook" style="margin-right:10px;" /></a>';
	if ($templateinfo ['IcalCalendar'] == '1')
		$calendaricons ['{AddToIcalCalendar}'] = '<a href={AddToLotusCalendar}><img src="' . IRECRUIT_HOME . 'images/lotus.png" alt="Lotus" style="margin-right:10px;" /></a>';
	if ($templateinfo ['LotusCalendar'] == '1')
		$calendaricons ['{AddToLotusCalendar}'] = '<a href={AddToIcalCalendar}><img src="' . IRECRUIT_HOME . 'images/ical.png" alt="Ical" style="margin-right:10px;" /></a>';
	
	if (is_array ( $calendaricons ))

	$selectedcicons = implode ( "\n", $calendaricons );
	$message = str_replace ( "{CalendarIcons}", $selectedcicons, $message );
	
	// Get Requisition Detail Information
	$requisition_info = G::Obj('Requisitions')->getReqDetailInfo("Owner, RequisitionID", $OrgID, $MultiOrgID, $RequestID);
	
	$message = str_replace ( "{RequestNewTime}", '<a href={RequestNewTime}>Request New Time</a>', $message );
	$message = str_replace ( "{Location}", '<a href={Location}>Click Here To See The Location In Google Map</a>', $message );
	
	$googlecalendarurl = "http://www.google.com/calendar/event?action=TEMPLATE";
	$googlecalendarurl .= "&text=" . urlencode($REQUEST ['sisubject']);
	$googlecalendarurl .= "&dates=$eventstartdate/$eventenddate";
	$googlecalendarurl .= "&details=";
	$googlecalendarurl .= "&location=" . urlencode ( $REQUEST ['silocation'] );
	
	if($user_preferences ['DefaultTimeZone'] != "") {
		$googlecalendarurl .= "&ctz=" . urlencode ( $user_preferences ['DefaultTimeZone'] );
	}
		
	$message = str_replace ( "{RequestNewTime}", "mailto:" . $REQUEST ['sifrom'], $message );
	
	$message = str_replace ( "../images/google.png", IRECRUIT_HOME . "images/google.png", $message );
	$message = str_replace ( "../images/lotus.png", IRECRUIT_HOME . "images/lotus.png", $message );
	$message = str_replace ( "../images/outlook.png", IRECRUIT_HOME . "images/outlook.png", $message );
	$message = str_replace ( "../images/ical.png", IRECRUIT_HOME . "images/ical.png", $message );
	$message = str_replace ( "{AddToGoogleCalendar}", $googlecalendarurl, $message );
	
	/**
	 * Php Mail Code
	 */
	//$ApplicantEmail = 'dedgecomb@irecruit-software.com';
	
	$subject   =   $_REQUEST ['sisubject'];
	$from      =   $_REQUEST ['sifrom'];
	$bcc       =   trim ( $_REQUEST ['sibcc'] );
	$location  =   "http://maps.google.com/maps?q=" . urlencode ( $_REQUEST ['silocation'] ) . "&hl=en";
	
	$message   =   str_replace ( "{Location}", $location, $message );
	
	$message   =   html_entity_decode ( $message );
	
	/**
	 * Insert the data to database
	 */
	$si_cc_request_info = '';
	if(is_array($_REQUEST ['sicc'])) $si_cc_request_info = implode ( ",", $_REQUEST ['sicc'] );
	
	$schedule_interview_info = array(
                                    "SIApplicationID"       =>  $ApplicationID,
                                    "SIRequisitionOwner"    =>  $requisition_info ['Owner'],
                                    "SIUserID"              =>  $USERID,
                                    "SIRequestID"           =>  $RequestID,
                                    "SIRequisitionID"       =>  $requisition_info ['RequisitionID'],
                                    "SIOrgID"               =>  $OrgID,
                                    "SIRecipient"           =>  $First . " " . $Last,
                                    "SIEmail"               =>  $ApplicantEmail,
                                    "SIDateTime"            =>  $sidatetime,
                                    "SITimeZone"            =>  $user_preferences ['DefaultTimeZone'],
                                    "SISubject"             =>  $REQUEST ['sisubject'],
                                    "SILocation"            =>  $REQUEST ['silocation'],
                                    "SIDescription"         =>  $_REQUEST ['sidescription'],
                                    "SIStatus"              =>  $REQUEST ['sistatus'],
                                    "SIDispositionCode"     =>  $REQUEST ['sidispositioncode'],
                                    "SIFrom"                =>  $REQUEST ['sifrom'],
                                    "SICc"                  =>  $si_cc_request_info,
                                    "SIBcc"                 =>  $REQUEST ['sibcc'],
                                    "SICreateDateTime"      =>  date ( 'Y-m-d H:i:s' ),
                                    "SIModifiedDateTime"    =>  date ( 'Y-m-d H:i:s' )
	                               );
	$res_sched_info    =   G::Obj('Appointments')->insScheduleInterviewInfo($schedule_interview_info);

	$lastinsertid      =   $res_sched_info['insert_id'];
	
	//Insert Job Application History	
	$job_app_history = array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "ProcessOrder"=>$REQUEST['sistatus'], "DispositionCode"=>$REQUEST ['sidispositioncode'], "Date"=>"NOW()", "StatusEffectiveDate"=>"NOW()", "UserID"=>$USERID, "Comments"=>'Appointment Scheduled');
	G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
	
	// Set Update Information
	$upd_set_info = array (
        "DispositionCode    =   :DispositionCode",
        "LastModified       =   NOW()"
	);
	
	// Set where condition
	$upd_where_info = array (
        "OrgID              =   :OrgID",
        "RequestID          =   :RequestID",
        "ApplicationID      =   :ApplicationID"
	);
	// Set parameters
	$upd_params = array (
        ":DispositionCode"  =>  $REQUEST ['sidispositioncode'],
        ":OrgID"            =>  $OrgID,
        ":RequestID"        =>  $RequestID,
        ":ApplicationID"    =>  $ApplicationID
	);
	
	if($REQUEST['sistatus'] != "") {

        $upd_set_info[] = "ProcessOrder = :ProcessOrder";
		$upd_params[":ProcessOrder"] = $REQUEST['sistatus'];
		
		// Set where condition
		$where    =   array ("OrgID = :OrgID", "ProcessOrder = :ProcessOrder");
		// Set parameters
		$params   =   array (":OrgID" => $OrgID, ":ProcessOrder" => $REQUEST['sistatus']);
		$results  =   G::Obj('Applicants')->getApplicantProcessFlowInfo ( "Searchable", $where, '', array ($params) );
		$APF      =   $results ['results'] [0] ['Searchable'];

		$upd_set_info[] =  "Searchable = :Searchable";
		$upd_params[":Searchable"] = $APF;
		
	}
	
	//Update Status and Disposition Code in JobApplications table
	G::Obj('Applications')->updApplicationsInfo ( 'JobApplications', $upd_set_info, $upd_where_info, array($upd_params) );

	// Insert Applicant Status Logs Information
	G::Obj('ApplicantStatusLogs')->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, $REQUEST['sistatus'], $REQUEST ['sidispositioncode']);
	
	$ProcessOrder      =   $REQUEST['sistatus'];
	$paperless_info    =   $ApplicationsObj->getJobApplicationsDetailInfo("Paperless", $OrgID, $ApplicationID, $RequestID);
	
	$trigger_status    =   $FormsInternalObj->getAssignedFormsTriggerStatus($OrgID, $ApplicationID, $RequestID, $ProcessOrder);
	
	if($paperless_info['Paperless'] == "No" 
       && $trigger_status == "Yes") {
	    include IRECRUIT_DIR . 'formsInternal/AssignForm.inc';
	}
	
	$message = str_replace ( "{AddToOutlookCalendar}", IRECRUIT_HOME . "appointments/scheduleOutlookUserAppointment.php?appid=" . $lastinsertid, $message );
	$message = str_replace ( "{AddToLotusCalendar}", IRECRUIT_HOME . "appointments/scheduleUserAppointment.php?appid=" . $lastinsertid, $message );
	$message = str_replace ( "{AddToIcalCalendar}", IRECRUIT_HOME . "appointments/scheduleUserAppointment.php?appid=" . $lastinsertid, $message );
	
	$message = str_replace ( "{Date}", $REQUEST ['sidate'], $message );
	$message = str_replace ( "{Time}", $REQUEST ['sihrs'] . ":" . $mins . ":00 " . $REQUEST ['siampm'], $message );

	//Clear custom headers
	$PHPMailerObj->clearCustomHeaders();
	//Clear properties
	$PHPMailerObj->clearCustomProperties();

	//Add From
	$PHPMailerObj->setFrom($from);

	//Add Bcc
	if ($isbcc == "Y") {
	    $PHPMailerObj->addCustomHeader("BCC", $bcc);
	}
	//Add CC
	if(count($ccslist) > 0) {
	    $PHPMailerObj->addCustomHeader("Cc", implode(", ", $ccslist));
	}
	
	// Content Type Is HTML
	$PHPMailerObj->ContentType = 'text/html';
	
	// Set who the message is to be sent to
	$PHPMailerObj->addAddress ( $ApplicantEmail );
	// Set the subject line
	$PHPMailerObj->Subject = $subject;
	// convert HTML into a basic plain-text alternative body
	
	$message = utf8_encode(preg_replace("/[^A-Z0-9a-z_ <>!#\-()\/%-&;?:=\".\\\\\n'@$+,]/i", '', $message));

	$PHPMailerObj->msgHTML ( $message );

	// Mail Send
	$PHPMailerObj->send ();

    //Get user preferences	
	$userpreferences = G::Obj('IrecruitUserPreferences')->getUserPreferences($USERID, "*");

	$default_cal = '';
	if ($userpreferences ['ReminderTypeStatus'] == "Yes" && $userpreferences ['ReminderType'] == "Google") {
		$default_cal = 'gog';
	
		$duration = $user_preferences['AppointmentDuration'];
		if($duration == "" || $duration == "0" || $duration == 0) $duration = 60;
		
		//Set columns
		$columns  =   "SIDateTime as begindate, SIApplicationID, SIOrgID, SICc,
					   DATE_FORMAT(DATE_ADD(SIDateTime, INTERVAL $duration MINUTE),'%Y-%m-%d %H:%i:00') as enddate,
					   SISubject, SILocation, SIDescription";
		//Get ScheduledAppointment Information
		$date     =   G::Obj('Appointments')->getScheduledAppointment($lastinsertid, $columns);
		
		$eventstartdate   =   date ( 'Ymd\THis', strtotime ( $date['begindate'] ) );
		$eventenddate     =   date ( 'Ymd\THis', strtotime ( $date['enddate'] )  );
		
		$event_url = "http://www.google.com/calendar/event?action=TEMPLATE";
		$event_url .= "&text=" . urlencode($REQUEST ['sisubject']);
		$event_url .= "&dates=$eventstartdate/$eventenddate";
		$event_url .= "&details=";
		$event_url .= "&location=" . urlencode ( $date ['SILocation'] );
		
		if($user_preferences ['DefaultTimeZone'] != "") {
			$event_url .= "&ctz=" . urlencode ( $user_preferences ['DefaultTimeZone'] );
		}
		
	}
	else if($userpreferences ['ReminderTypeStatus'] == "Yes") {
		$default_cal = 'ics';
		$event_url = IRECRUIT_HOME . "appointments/scheduleAppointmentEvent.php?appid=$lastinsertid";
	}
	
	echo json_encode(array("ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "FormID"=>"STANDARD", "OrgID"=>$OrgID, "msg"=>"schedulapp", "appid"=>$lastinsertid, "defcal"=>$default_cal, "event_url"=>$event_url));
}
?>
