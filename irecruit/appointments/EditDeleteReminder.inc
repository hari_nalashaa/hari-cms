<?php 
if($_REQUEST['dba'] == 'fa' && $_REQUEST['arid'] != '') {
	$columns = "SID, Subject, DATE(DateTime) as Date,
				date_format(DateTime,'%h') as Hours, date_format(DateTime,'%i') as Minutes,
				date_format(DateTime,'%p') as AmPm";
	//Get Appointment Reminder Detail Information
	$rowreminder = $RemindersObj->getAppointmentReminderDetailInfo($columns, $_REQUEST['arid']);	
}
if($_REQUEST['dba'] == 'fu' && $_REQUEST['arid'] != '') {
	//Convert DateTime Format
	$sidatetime = $DateHelperObj->getYmdFromMdy ( $_REQUEST ['sidate'] );

	//Calculate PostDate Hours
	$hrs = $_REQUEST['sihrs'];
	if($_REQUEST['siampm'] == "AM") {
		if($hrs == 12) $hrs = "00";
	}
	else if($_REQUEST['siampm'] == "PM") {
		$hrs += 12;
		if($hrs == 24) $hrs -= 12;
	}

	//Calculate PostDate Minutes
	$mins = $_REQUEST['simins'];
	if($_REQUEST['simins'] == 0) {
		$mins = "00";
	}
	$sidatetime .= " ".$hrs.":".$mins.":00";

	//Update Appointment Reminder Information
	$resappointment = $RemindersObj->updAppointmentReminderInfo($sidatetime, $_REQUEST['sisubject'], $_REQUEST['arid']);
	
	echo "<script>location.href='applicants.php?action=remindappointments&msg=upd'</script>";
	exit;
}
if($_REQUEST['dba'] == 'delete' && $_REQUEST['arid'] != '') {
	//Delete Appointment Reminder Information
	$RemindersObj->delAppointmentReminderInfo($_REQUEST['arid']);

	echo "<script>location.href='applicants.php?action=remindappointments&msg=del'</script>";
	exit;
}
?>