<?php
require_once '../Configuration.inc';

if($_REQUEST['Year'] != "" && $_REQUEST['Month'] != "") {
	$SYear = $_REQUEST['Year'];
	$SMonth = (int)$_REQUEST['Month'];

	if($_REQUEST['Month'] < 10) $SMonth = '0'.$SMonth;
	$SDay = '01';
}
else if($_REQUEST['SIDate'] != "") {
	$SIDateRow = explode("-", $_REQUEST['SIDate']);
	$SYear = $SIDateRow[0];
	$SMonth = (int)$SIDateRow[1];

	if($SIDateRow[1] < 10) $SMonth = '0'.$SMonth;
	$SDay = '01';
}
else {
	$SYear = date('Y');
	$SMonth = (int)date('m');
	if(date('m') < 10) $SMonth = '0'.$SMonth;
	$SDay = '01';
}
if($_REQUEST['View'] == 'Table') {
	echo $AppointmentsObj->calendarTableView($SYear, $SMonth, $SDay);
}
if($_REQUEST['View'] == 'List') {
	echo $AppointmentsObj->calendarListView($SYear, $SMonth, $SDay);
}
?>