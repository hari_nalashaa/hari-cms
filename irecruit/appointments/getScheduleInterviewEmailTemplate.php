<?php 
require_once '../Configuration.inc';

$RequestID = $_REQUEST['RequestID'];

include_once IRECRUIT_DIR . 'appointments/ScheduleInterviewEmailTemplate.inc';

$email_template_info = array();
$email_template_info['viewmessage'] = html_entity_decode(str_replace("&nbsp;", " ", $viewmessage));
$email_template_info['message'] = $message;

echo json_encode($email_template_info);
?>