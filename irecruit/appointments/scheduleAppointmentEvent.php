<?php
require_once '../Configuration.inc';

//If not valid Appointment id - Redirect to home page
$appointmentid      =   (int)$_GET['appid'];
//Get ScheduleInterview UserPreferences by appointmentid
$userpreferences    =   G::Obj('Appointments')->getScheduleIntwUserPreferences($appointmentid);
//Get UserTheme Information
$userThemeInfo      =   G::Obj('IrecruitUserPreferences')->getUserTheme();

date_default_timezone_set($userThemeInfo['DefaultTimeZone']);

$timestamp = date('Ymd\THis\Z');

$duration = $userpreferences['AppointmentDuration'];

if(!is_int($appointmentid)) {
	header("Location:".IRECRUIT_HOME);
	exit;
}

//Set columns
$columns = "date_format(SIDateTime,'%Y%m%dT%H%i00') as begindate, SIApplicationID, SIOrgID, SICc, 
		   date_format(DATE_ADD(SIDateTime, INTERVAL $duration MINUTE),'%Y%m%dT%H%i00') as enddate, 
		   SISubject, SILocation, SIDescription";
//Get ScheduledAppointment Information
$date = $AppointmentsObj->getScheduledAppointment($appointmentid, $columns);

$siccs = explode(",", $date['SICc']);
for($z = 0; $z < count($siccs); $z++) {
	$requestcc = explode("**-**", $siccs[$z]);
	
	if(isset($requestcc[0]) && isset($requestcc[1]) && isset($requestcc[2]) && $requestcc[0] != "" && $requestcc[1] != "" && $requestcc[2] != "") {
		//Set where condition
		$where = array("OrgID = :OrgID", "UserID = :UserID", "EmailAddress = :EmailAddress");
		//Set parameters
		$params = array(":OrgID"=>$requestcc[0], ":UserID"=>$requestcc[1], ":EmailAddress"=>$requestcc[2]);
		//Get UserEmail Lists
		$rescclist = $IrecruitUsersObj->getUserEmailLists("*", $where, 'FirstName', array($params));
		$rowcclist = $rescclist['results'][0];
		
		if($rowcclist['EmailAddress'] != "") {
			$ccslist[] = $rowcclist['FirstName']." ".$rowcclist['LastName']."<".$rowcclist['EmailAddress'].">";
		}
	}
}
if(is_array($ccslist)) $cc = implode(",", $ccslist);

//Get application information
$APPDATA = $ApplicantsObj->getAppData ( $date['SIOrgID'], $date['SIApplicationID'] );

$Email = $APPDATA ['email'];
$First = $APPDATA ['first'];
$Last = $APPDATA ['last'];
$Name = $First." ".$Last;

$TZOFFSETFROM="-0500";
$TZOFFSETTO="-0400";
$TZNAME="EST";

if ($userThemeInfo[DefaultTimeZone] == "America/Los_Angeles") {
$TZOFFSETFROM="-0800";
$TZOFFSETTO="-0700";
$TZNAME="PDT";
}
if ($userThemeInfo[DefaultTimeZone] == "America/Denver") {
$TZOFFSETFROM="-0700";
$TZOFFSETTO="-0600";
$TZNAME="MDT";
}
if ($userThemeInfo[DefaultTimeZone] == "America/Chicago") {
$TZOFFSETFROM="-0600";
$TZOFFSETTO="-0500";
$TZNAME="CST";
}
if ($userThemeInfo[DefaultTimeZone] == "America/New_York") {
$TZOFFSETFROM="-0500";
$TZOFFSETTO="-0400";
$TZNAME="EST";
}
if ($userThemeInfo[DefaultTimeZone] == "America/Anchorage") {
$TZOFFSETFROM="-0800";
$TZOFFSETTO="-0900";
$TZNAME="AKST";
}
if ($userThemeInfo[DefaultTimeZone] == "America/Adak") {
$TZOFFSETFROM="-1030";
$TZOFFSETTO="-1000";
$TZNAME="HST";
}


$UID = time().rand(1, 4);
$CAL = <<<APPOINTMENTCAL
BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VTIMEZONE
TZID:$userThemeInfo[DefaultTimeZone]
TZURL:http://tzurl.org/zoneinfo-outlook/$userThemeInfo[DefaultTimeZone]
X-LIC-LOCATION:$userThemeInfo[DefaultTimeZone]
BEGIN:DAYLIGHT
TZOFFSETFROM:$TZOFFSETFROM
TZOFFSETTO:$TZOFFSETTO
TZNAME:$TZNAME
DTSTART:19700308T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VEVENT
DTSTAMP:$timestamp
DTSTART;TZID={$userThemeInfo[DefaultTimeZone]}:{$date[begindate]}
DTEND;TZID={$userThemeInfo[DefaultTimeZone]}:{$date[enddate]}
STATUS:CONFIRMED
SUMMARY;LANGUAGE=en-us:$date[SISubject] - $Name
DESCRIPTION:{$cc}
CLASS:PRIVATE
CREATED:{$date[begindate]}Z
LOCATION:{$date[SILocation]}
SEQUENCE:1
LAST-MODIFIED:$timestamp
UID:$UID
END:VEVENT
END:VCALENDAR
APPOINTMENTCAL;

$filename ="event.ics";
header("Content-Type: text/Calendar"); 
header("Content-Disposition: inline; filename=$filename"); 
echo $CAL;
?>