<?php
require_once '../irecruitdb.inc';

//Set columns
$columns = "date_format(DATE_ADD(now(),INTERVAL 5 HOUR),'%Y%m%dT%H%i00Z'), date_format(DATE_ADD(now(),INTERVAL 6 HOUR),'%Y%m%dT%H%i00Z')";
//Get Dates List
$date = $MysqlHelperObj->getDatesList($columns);
list($start_date, $end_date) = array_values($date);

$CAL = <<<SCHEDULEEVENTEND
BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VEVENT
STATUS:BUSY
DTSTART:$start_date
DTEND:$end_date
SUMMARY;LANGUAGE=en-us:iRecruit Event
DESCRIPTION:
CLASS:PRIVATE
LOCATION:https://apps.irecruit-us.com
PRIORITY:5
END:VEVENT
END:VCALENDAR
SCHEDULEEVENTEND;

$filename = "event.ics";
header ( "Content-Type: text/Calendar" );
header ( "Content-Disposition: inline; filename=$filename" );
echo $CAL;
?>