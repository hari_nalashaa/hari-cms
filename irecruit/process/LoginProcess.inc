<?php
if (isset($_GET ['k']) && $_GET['k'] != "") {
  $AUTH = $IrecruitUsersObj->getUserInfoByAccessCode($_GET ['k'],'UserID');
  unset($_COOKIE ['ID']);
} else {
  $AUTH = $IrecruitUsersObj->validateUserLogin($_POST ['user'], $_POST ['password']);
}

// if we have a good user and password match
if ($AUTH ['UserID']) {
	

//2fa process
         if($AUTH['TwofEnable'] == 1){ 
			//$AUTH['EmailAddress']
			//random number generate for 2fa
			 $a = mt_rand(100000,999999); 

             		 $message    =   "\n Hello ".$AUTH ['FirstName'].",<br/><br/>";
			 $message   .=   "\n\n  Your verification code is: <b>".$a."</b><br><br>";
			 $link = "<a href='".IRECRUIT_HOME. "twofauth.php'>two-factor login page</a>";
			 $message    .=   "\n\n Please enter the code on iRecruit's " . $link . ".<br>";

			 //$message    .=   "\n\n\n ".IRECRUIT_HOME."twofauth.php<br>";
			 $message    .=   "<br><br>";
			 $message    .=   "\n\n\n Thanks,"."<br>";
			 $message    .=   "\n\n iRecruit Support"."<br>";
			 $message    .=   "\n http://help.irecruit-us.com/2FA";
			 
			 //save otp into table  
			 $IrecruitUsersObj->updateUserOtp($AUTH['UserID'],$a);  
			 $PHPMailerObj->clearCustomProperties();
			 $PHPMailerObj->addAddress ($AUTH['EmailAddress']);
			 $PHPMailerObj->setFrom('support@irecruit-software.com');
			 $PHPMailerObj->Subject = 'iRecruit - Account Verification';
			 $PHPMailerObj->msgHTML ($message);
			 $PHPMailerObj->ContentType = 'text/plain';
			 //Send email
			 $PHPMailerObj->send (); 
                         session_start();
                         $_SESSION['userid_2fa']=$AUTH['UserID'];

                         $_SESSION['verification_email']=$AUTH['EmailAddress'];

			 $_SESSION['goto']=$_REQUEST['goto'];
                          
			 header ( 'Location: ' .IRECRUIT_HOME.'twofauth.php');
			 exit ();
           }
	// randomize a session key
	$SESSIONID = $GenericLibraryObj->generateRandomSessionKey();

	if (isset($_COOKIE ['ID']) && $_COOKIE ['ID'] != "") {
		$SESSIONID = $_COOKIE ['ID'];
	}

	if (isset($keep) && $keep == "yes") {
		$keeper = ", Persist = 1";
	} else {
		$keeper = '';
	}
    
	if($SESSIONID != "") {
	    // add to database
	    $IrecruitUsersObj->updateUserAccessSession($SESSIONID, $AUTH ['UserID'], $keeper);
	    
	    // set session key on users browser
	    if ($AUTH ['Persist'] == 1) {
	        setcookie ( "ID", $SESSIONID, time () + 2592000, "/" );
	    } else {
	        setcookie ( "ID", $SESSIONID, time () + 7200, "/" );
	    }
	}
	


	if (isset($_REQUEST['goto']) && $_REQUEST['goto'] != "") {
		$loc = $_REQUEST['goto'];
	} else {
		$loc = IRECRUIT_HOME;
	}
    
	// go to application
	header ( 'Location: ' . $loc );
	exit ();
	
} // end AUTH
?>
