<?php
require_once 'Configuration.inc';

include COMMON_DIR . 'formsInternal/ApplicantFormStatus.inc';
include IRECRUIT_DIR . 'applicants/EmailApplicant.inc';

//set page title
include IRECRUIT_DIR . 'formsInternal/FormsInternalTitle.inc';

//Load scripts in header
$scripts_header[] = "js/loadAJAX.js";
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$TemplateObj->page_scripts_header = $scripts_header;

//Load scripts in footer
$scripts_footer[] = "js/index.js";
$scripts_footer[] = "js/irec_Display.js";
$scripts_footer[] = "js/forms-internal.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

$script_vars_footer[]	= 'var datepicker_ids = "#from_date, #to_date";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';
$TemplateObj->scripts_vars_footer = $script_vars_footer;

//Send notification to applicants
if(isset($_POST['chkSendReminder']) && count($_POST['chkSendReminder']) > 0)
{
    for($csr = 0; $csr < count($_POST['chkSendReminder']); $csr++) {
        $app_req_id         =   explode("-", $_POST['chkSendReminder'][$csr]);
        $ApplicationID      =   $app_req_id[0];
        $RequestID          =   $app_req_id[1];
        
        if($ApplicationID != "" && $RequestID != "") {
            //Set where condition
            $where              =   array("OrgID = :OrgID", "PresentedTo = 1", "Status < 3", "RequestID = :RequestID", "ApplicationID = :ApplicationID");
            //Set parameters
            $params             =   array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID, ":ApplicationID"=>$ApplicationID);
            //Get InternalFormsAssigned Information Count
            $results            =   $FormsInternalObj->getInternalFormsAssignedInfo("OrgID, ApplicationID, RequestID", $where, "OrgID, ApplicationID", "", array($params));
            $apps_list          =   $results['results'];
            $apps_count         =   $results['count'];
            
            $em                 =   internalFormTickler ( $OrgID, $ApplicationID, $RequestID, "" );
            $alertmessage       =   'iConnect forms reminder email sent to: ' . $em;
            
            // Insert into ApplicantHistory
            $UpdateID = $USERID;
            $Comments = 'iConnect forms reminder email sent to: ' . $em;

            if($alertmessage != 'An email has been sent to: no one') {
                $job_application_history = array (
                    "OrgID"                 =>  $OrgID,
                    "ApplicationID"         =>  $ApplicationID,
                    "RequestID"             =>  $RequestID,
                    "ProcessOrder"          =>  "-6",
                    "StatusEffectiveDate"   =>  "DATE(NOW())",
                    "Date"                  =>  "NOW()",
                    "UserID"                =>  $UpdateID,
                    "Comments"              =>  $Comments
                );
                
                G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );
            }
            
        }
    }
    

    header("Location:formsStatus.php?msg=notificationsucc");
    exit;
}

echo $TemplateObj->displayIrecruitTemplate('views/formsInternal/FormsStatus');
?>