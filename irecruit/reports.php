<?php
require_once 'Configuration.inc';

$TemplateObj->title =   $title  =   "Reports";

if($modal == "Y")
{
 	//Assign add applicant datepicker variables
    $script_vars_header[]   = 'var datepicker_ids = "'.$lst.'";';
    $script_vars_header[]   = 'var date_format = "mm/dd/yy";';

    $TemplateObj->script_vars_header = $script_vars_header;

    if(isset($lst)) {
		//Declare datepicker function
        $scripts_header_inline[]	= 'function again_cal() { date_picker(datepicker_ids, date_format); }';
        $TemplateObj->scripts_header_inline = $scripts_header_inline;
	}
}

G::Obj('Reports')->setMultiOrgReporting($OrgID, $_REQUEST['MultiOrgID']);

$scripts_header[] = "js/loadAJAX.js";
$TemplateObj->page_scripts_header = $scripts_header;

$TemplateObj->fromdate  =   $fromdate   =   isset($_REQUEST['fromdate']) ? $_REQUEST['fromdate'] : '';
$TemplateObj->todate    =   $todate     =   isset($_REQUEST['todate']) ? $_REQUEST['todate'] : '';
$TemplateObj->action    =   $action     =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';

include_once 'reports/ReportsPageTitles.inc';

echo $TemplateObj->displayIrecruitTemplate('views/reports/Reports');
?>