<?php
//Get AppData
$APPDATA = $ApplicantsObj->getAppData($OrgID, $ApplicationID);

$positions = '';
$date = '';

//Set columns
$columns = "RequestID, MultiOrgID, EntryDate";
//Set where condition
$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
//Set params
$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
//Get JobApplications information
$results = $ApplicationsObj->getJobApplicationsInfo($columns, $where, '', '', array($params));

$i=0;

if(is_array($results['results'])) {
	foreach($results['results'] as $JA) {
		$positions .=  'Req/JobID: ' . $RequisitionsObj->getReqJobIDs($OrgID,$JA['RequestID']) . ' ' . $RequisitionsObj->getJobTitle($OrgID,$JA['RequestID']) . "\n";
		$date = $JA['EntryDate'];
		$MultiOrgID = $JA['MultiOrgID'];
		$i++;
	} // end foreach
}

//Get Text from text blocks	// get display text from main job id
$row_text = $FormFeaturesObj->getTextFromTextBlocks($OrgID, $FormID, 'ResponseEmail');
$responsemessage = strip_tags($row_text);

$name = $APPDATA['first'] . ' ' . $APPDATA['middle'] . ' ' . $APPDATA['last'];
$email = $APPDATA['email'];

$to      = $APPDATA['email'];
$subject = "Thank you from " . strip_tags($OrganizationName);
$message = <<<END
$responsemessage
<br>
-------------------------------------------
<br>Application ID: $ApplicationID
<br>Application Date: $date
<br>Name: $name
<br>Email: $email
$positions
END;

$message .= <<<END
<br>-------------------------------------------
<br>           ** IMPORTANT **
<br>Please do not try to Reply-To this email.
<br>-------------------------------------------
<br>This is an automatically generated email with a
non-functional email address. Please refer back
to our website for contact email addresses and
phone numbers.

<br>Thank You.
<br>Have a Great Day!

END;

//Get Email and Email Verified
$OE = $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);
//Get OrganizationName
$OrgName = $OrganizationDetailsObj->getOrganizationName($OrgID, $MultiOrgID);

##################################################################################################
############################## Send Mail To Applicant ############################################

G::Obj('PHPMailer')->clearCustomProperties();

if (($OE['Email']) && ($OE['EmailVerified'] == 1)) {
	// Set who the message is to be sent from
	G::Obj('PHPMailer')->setFrom ( $OE['Email'], $OrgName );
	// Set an alternative reply-to address
	G::Obj('PHPMailer')->addReplyTo ( $OE['Email'], $OrgName );
}

// Set who the message is to be sent to
G::Obj('PHPMailer')->addAddress ( $to );
// Set the subject line
G::Obj('PHPMailer')->Subject = $subject;
// convert HTML into a basic plain-text alternative body
G::Obj('PHPMailer')->msgHTML ( $message );
// Content Type Is HTML
G::Obj('PHPMailer')->ContentType = 'text/html';
//Send email
G::Obj('PHPMailer')->send ();

##################################################################################################
############################# Send Mail To Hiring Manager ########################################

// Get Requisition Owner 
$requisitioninfo = $RequisitionsObj->getReqDetailInfo ( "Title, Owner", $OrgID, $MultiOrgID, $RequestID );
//Get Requisition Owner Information
$userinfo = $IrecruitUsersObj->getUserInfoByUserID ( $requisitioninfo ['Owner'], "EmailAddress" );

//UserPreferences by requisition owner
if($requisitioninfo ['Owner'] != "") {
	$req_owner_pref = G::Obj('IrecruitUserPreferences')->getUserPreferencesByUserID($requisitioninfo ['Owner']);
}

$MJobTitle = $requisitioninfo ['Title'];

$message = "Dear Hiring Manager, <br><br> You have received an application for " . $MJobTitle . "<br><br>Thanks<br>Irecruit Support Team";

$to = $userinfo ['EmailAddress'];

if($to != "") {
	
	$subject = "Notification - You got an application for " . $MJobTitle;
	$from = $POST ['EmailAddress'];
	
	G::Obj('PHPMailer')->clearCustomProperties();

	// Set who the message is to be sent from
	G::Obj('PHPMailer')->setFrom ( $from, "" );
	// Set an alternative reply-to address
	G::Obj('PHPMailer')->addReplyTo ( $from, "" );
	
	// Set who the message is to be sent to
	G::Obj('PHPMailer')->addAddress ( $to );
	
	// Set the subject line
	G::Obj('PHPMailer')->Subject = $subject;
	
	// convert HTML into a basic plain-text alternative body
	G::Obj('PHPMailer')->msgHTML ( $message );
	
	// Attach an coverletter file
	G::Obj('PHPMailer')->addAttachment ( $coverLetter );
	
	// Attach an resume file
	G::Obj('PHPMailer')->addAttachment ( $resumeDocument );
	
	// Content Type Is HTML
	G::Obj('PHPMailer')->ContentType = 'text/html';
	
	// send the message, check for errors, anyway we will not execute this file in browser
	if (! G::Obj('PHPMailer')->send ()) {
		Logger::writeMessage(ROOT_DIR."logs/mail-errors/".date('Y-m-d')."_errors.log", "Unable to send mail to user Email:$to. FileName: SendEmailThankYou.inc", "a+");
	}
}