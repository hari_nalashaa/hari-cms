<?php 
require_once '../irecruitdb.inc';

set_time_limit(0);

//Get Date Time
$TS = G::Obj('MysqlHelper')->getDateTime('%a, %d %b %Y %H:%i:%s GMT');

//Objects for CommonMethods and MonsterInformation
$OrgsInfoResults    =   G::Obj('Organizations')->getOrgDataInfo("OrgID, MultiOrgID, OrganizationName");
$OrgsInfo           =   $OrgsInfoResults['results'];

$xmlstart        =   '<?xml version="1.0" encoding="UTF-8"?>';
$xmlstart       .=   '<source>';
$xmlstart       .=   '<lastBuildDate>'.$TS.'</lastBuildDate>';
$xmlstart       .=   '<publisherurl>https://apps.irecruit-us.com/ziprecruiter/zipRecruiterFreeFeed.php</publisherurl>';
$xmlstart       .=   '<publisher>iRecruit</publisher>';

for($oi = 0; $oi < count($OrgsInfo); $oi++) {

    $org_info               =   $OrgsInfo[$oi];
    
    $OrgID                  =   $OrgsInfo[$oi]['OrgID'];
    $MultiOrgID             =   $OrgsInfo[$oi]['MultiOrgID'];
    
    $requisition_results    =   G::Obj('ZipRecruiter')->getZipRecruiterOrgRequisitions($OrgID, $MultiOrgID);
    $requisitions_count     =   $requisition_results['count'];    

    $ChkStatusLevel         =   array();
    $ORG_REQ_STATUSLEVELS   =   array();
    
    for($r = 0; $r < $requisitions_count; $r++) {
        $REQ                =   $requisition_results['results'][$r];

        $RequisitionFormID  =   $REQ['RequisitionFormID'];
        
        if(!in_array($OrgID."-".$RequisitionFormID, $ChkStatusLevel)) {
            $STATUSLEVELS   =   G::Obj('RequisitionDetails')->getEmploymentStatusLevelsList($OrgID, $RequisitionFormID);
            $ORG_REQ_STATUSLEVELS[$OrgID."-".$RequisitionFormID]    =   $STATUSLEVELS;
            $ChkStatusLevel[]   =   $OrgID."-".$RequisitionFormID;
        }
        else {
            $STATUSLEVELS   =   $ORG_REQ_STATUSLEVELS[$OrgID."-".$RequisitionFormID];
        }

        $EmpStatusLevel     =   $STATUSLEVELS[$REQ['EmpStatusID']];        
        
        $free_jobboard_list =   json_decode($REQ['FreeJobBoardLists'], true);
        $is_paid            =   G::Obj('ZipRecruiter')->isPaidRequisition($OrgID, $REQ['RequestID']);
        
        if(is_array($free_jobboard_list) 
            && in_array("ZipRecruiter", $free_jobboard_list)
            && $is_paid == "No") {

            //Zip Recruiter Job Types
            $job_types                  =   array("fulltime"=>"Full-Time", "parttime"=>"Part-Time", "contract"=>"Contractor", "contractor"=>"Contractor", "temporary"=>"Temporary");
            $EmpStatusLevel             =   str_replace(' ', '', $EmpStatusLevel);
            $EmpStatusLevel             =   str_replace('-', '', $EmpStatusLevel);
            $EmpStatusLevel             =   strtolower($EmpStatusLevel);
            
            foreach ($job_types as $job_type_key=>$job_type_value) {
                if(stristr($EmpStatusLevel, $job_type_key) !== FALSE) {
                    $job_type   =   $job_type_value;
                }                	
            }
            
            if(!in_array($job_type, $job_types)) {
                $job_type       =   "Full-Time";
            }

            $xmlstart   .=   '<job>';
            $xmlstart   .=   '<title><![CDATA['.$REQ['Title'].']]></title>';
            $xmlstart   .=   '<date><![CDATA['.$REQ['PostDate'].']]></date>';
            $xmlstart   .=   '<referencenumber><![CDATA['.$REQ['RequestID'].']]></referencenumber>';
            $xmlstart   .=   '<company><![CDATA['.$org_info['OrganizationName'].']]></company>';
            
            $xmlstart   .=   '<address><![CDATA['.$REQ['Address1'].']]></address>';
            $xmlstart   .=   '<city><![CDATA['.$REQ['City'].']]></city>';
            $xmlstart   .=   '<state><![CDATA['.$REQ['State'].']]></state>';
            $xmlstart   .=   '<postalcode><![CDATA['.$REQ['ZipCode'].']]></postalcode>';
            $xmlstart   .=   '<country><![CDATA['.$REQ['Country'].']]></country>';

if ($r > 50) {
$REQ['Description']=strip_tags($REQ['Description']);
}            

            $xmlstart   .=   '<description><![CDATA['.$REQ['Description'].']]></description>';
            $xmlstart   .=   '<category><![CDATA['.$REQ['ZipRecruiterJobCategory'].']]></category>';

            $xmlstart   .=   '<jobtype><![CDATA['.$job_type.']]></jobtype>';
            $xmlstart   .=   '<compensation_min><![CDATA['.number_format($REQ['Salary_Range_From'],2,'.','').']]></compensation_min>';
            $xmlstart   .=   '<compensation_max><![CDATA['.number_format($REQ['Salary_Range_To'],2,'.','').']]></compensation_max>';
            $xmlstart   .=   '<compensation_interval><![CDATA[]]></compensation_interval>';
            $xmlstart   .=   '<compensation_currency><![CDATA[]]></compensation_currency>';
            $xmlstart   .=   '<show_compensation><![CDATA[]]></show_compensation>';
            $xmlstart   .=   '<compensation_has_commission><![CDATA[]]></compensation_has_commission>';
            $xmlstart   .=   '<benefits>';
            $xmlstart   .=   '<vision><![CDATA[]]></vision>';
            $xmlstart   .=   '<medical><![CDATA[]]></medical>';
            $xmlstart   .=   '<life_insurance><![CDATA[]]></life_insurance>';
            $xmlstart   .=   '<retirement_savings><![CDATA[]]></retirement_savings>';
            $xmlstart   .=   '<dental><![CDATA[]]></dental>';
            $xmlstart   .=   '</benefits>';
            $xmlstart   .=   '<resume_not_required><![CDATA[1]]></resume_not_required>';
            
            //Interview Questions
            $xmlstart   .=   '<interview_json>';
            $xmlstart   .=   '<![CDATA[';
            $xmlstart   .=   '[';
            
            $xmlstart   .=   G::Obj('ZipRecruiter')->generateInterviewQuestionsJSONString($OrgID, $REQ['ZipRecruiterLabelID']);
            
            $xmlstart   .=   ']';
            $xmlstart   .=   ']]>';
            $xmlstart   .=   '</interview_json>';
            
            $xmlstart   .=   '<partner_attributes>';
            $xmlstart   .=   '<OrgID>'.$OrgID.'</OrgID>';
            $xmlstart   .=   '<FormID>'.$REQ['ZipRecruiterLabelID'].'</FormID>';
            $xmlstart   .=   '<RequestID>'.$REQ['RequestID'].'</RequestID>';
            $xmlstart   .=   '<MultiOrgID>'.$MultiOrgID.'</MultiOrgID>';
            $xmlstart   .=   '</partner_attributes>';
            
            $xmlstart   .=   '</job>';
            
        }
    }

}

$xmlstart   .=   '</source>';

header('Content-Type: application/xml; charset=UTF-8');
echo $xmlstart;
exit;
?>
