<?php
require_once '../irecruitdb.inc';

// Posted Data
$data   =   file_get_contents('php://input');

//$data   =   file_get_contents(IRECRUIT_DIR . 'ziprecruiter_applicants_info/B12345467/2019-03-23-09-35-5015c963636be078.json'); 
//$data = file_get_contents(ROOT.'logs/applicant-data2019-01-15-00_25_27-5c3d2877e2f01.log');

// Decode json data
$data               =   str_replace("\'", "'", $data);
$POST               =   json_decode($data, true);

$education_records  =   $POST['profile']['education_records'];
$job_records        =   $POST['profile']['job_records'];

$alphabets          =   range('A', 'Z');

for($jf = (count($education_records) - 1), $a = 0; $jf >= 0; $jf--, $a++) {
    $edu_info[$alphabets[$a]."attended"]    =   $education_records[$jf]['school'];

    $start_date     =   $education_records[$jf]['start_date'];
    $end_date       =   $education_records[$jf]['end_date'];
    
    $date1          =   strtotime($start_date);
    $date2          =   strtotime($end_date);
    
    $completed_year =   "";
    if($end_date != "") {
        $completed_year =   date('Y', strtotime($end_date));
    }
    
    $years          =   '';
    if($start_date != "" && $start_date != "") {
        $datediff   =   $date2 - $date1;
        $days       =   round($datediff / (60 * 60 * 24));
        $years      =   round($days / 365);
    }
    
    $edu_info[$alphabets[$a]."attended"]        =   $education_records[$jf]['school'];
    $edu_info[$alphabets[$a]."yearscompleted"]  =   $years;
    $edu_info[$alphabets[$a]."year"]            =   $completed_year;
    
    $pos = strpos($edu_info[$alphabets[$a]."type"], 'high school');
    
    if($a == 0 && $pos >= 0) {
        $edu_info[$alphabets[$a]."graduated"]   =   "No";
    }
    else {
        $edu_info[$alphabets[$a]."graduated"]   =   "Yes";
    }
    
    if($a >= 1) {
        $edu_info[$alphabets[$a]."major"]   =   $education_records[$jf]['major'];
    }
}

$company_info = array();

for($jr = (count($job_records) - 1), $ab = 0; $jr >= 0; $jr--) {
    
    if($job_records[$jr]['current'] == 1) {
        $company_info['cur_companyname']        =   $job_records[$jr]['employer'];
        $company_info['cur_jobtitle']           =   $job_records[$jr]['position'];
        $company_info['cur_describeposition']   =   $job_records[$jr]['description'];
        $company_info['cur_employmentdate']     =   $DateHelperObj->getMdyFromYmd($job_records[$jr]['start_date'], "-", "/");
    }
    else {
        $company_info[$alphabets[$ab]."mrpcompanyname"]             =   $job_records[$jr]['employer'];
        $company_info[$alphabets[$ab]."mrpposition"]                =   $job_records[$jr]['position'];
        $company_info[$alphabets[$ab]."mrpdescribeposition"]        =   $job_records[$jr]['description'];
        $company_info[$alphabets[$ab]."mrpdatesofemploymentfrom"]   =   $DateHelperObj->getMdyFromYmd($job_records[$jr]['start_date'], "-", "/");
        $company_info[$alphabets[$ab]."mrpdatesofemploymentto"]     =   $DateHelperObj->getMdyFromYmd($job_records[$jr]['end_date'], "-", "/");
        $ab++;
    }
}

$OrgID              =   $POST['attributes']['OrgID'];
$RequestID          =   $POST['attributes']['RequestID'];
$FormID             =   $POST['attributes']['FormID'];
$MultiOrgID         =   $POST['attributes']['MultiOrgID'];

$req_info           =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID, Active", $OrgID, $RequestID);

//Get Form Questions
$where_info         =   array("OrgID = :OrgID", "FormID = :FormID", "Active = 'Y'");
$params_info        =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID);
$form_ques_results  =   G::Obj('FormQuestions')->getFormQuestionsInformation("*", $where_info, "QuestionOrder ASC", array($params_info));
$form_ques_info     =   $form_ques_results['results'];
$form_ques_list     =   array();

for($fqi = 0; $fqi < count($form_ques_info); $fqi++) {
    $form_ques_list[$form_ques_info[$fqi]['QuestionID']]   =   $form_ques_info[$fqi];
}

if($POST['email'] != "" && $OrgID != "" && $FormID != "") {
    
    // Set CoverLetterData
    $message        =   "";
    $fileContent    =   $POST['resume'];
    $date2          =   $MysqlHelperObj->getNow(); // Get Database DateTime
    
    $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
    if (! file_exists($dir)) {
        mkdir($dir, 0700);
        chmod($dir, 0777);
    }
    
    $apatdir = $dir . '/applicantattachments';
    
    if (! file_exists($apatdir)) {
        mkdir($apatdir, 0700);
        chmod($apatdir, 0777);
    }
    
    // #############################################################################
    // ### Insert data to application history related tables
    
    // Generate ApplicationID
    $ApplicationID  =   G::Obj('Applications')->getApplicationID($OrgID);
    
    $log_file_name  =   date('Y-m-d-H-i-s').uniqid(true);
    
    if(!is_dir(IRECRUIT_DIR . 'ziprecruiter_applicants_info')) {
        //Directory does not exist, so lets create it.
        mkdir(IRECRUIT_DIR . 'ziprecruiter_applicants_info', 0777);
        chmod(IRECRUIT_DIR . 'ziprecruiter_applicants_info', 0777);
    }
    if(!is_dir(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID) && $OrgID != "") {
        //Directory does not exist, so lets create it.
        mkdir(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID, 0777);
        chmod(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID, 0777);
    }
    if(!file_exists(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID.'/'.$log_file_name.'.json')) {
        @touch(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID.'/'.$log_file_name.'.json');
        @chmod(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID.'/'.$log_file_name.'.json', 0777);
    
        $file = fopen(IRECRUIT_DIR . 'ziprecruiter_applicants_info/'.$OrgID.'/'.$log_file_name.'.json',"w+");
        fwrite($file, $data);
        fclose($file);
    }
    
    $phone_info   =   G::Obj('Address')->getFormattedLeadPhoneNumber($POST['PhoneNumber']);
    
    // Set Zip Recruiter Information
    $info = array(
        "ResponseID"            =>  $POST['response_id'],
        "OrgID"                 =>  $OrgID,
        "MultiOrgID"            =>  $MultiOrgID,
        "FormID"                =>  $FormID,
        "JobID"                 =>  $POST['job_id'],
        "ApplicationID"         =>  $ApplicationID,
        "Name"                  =>  $POST['name'],
        "FirstName"             =>  $POST['first_name'],
        "LastName"              =>  $POST['last_name'],
        "Email"                 =>  $POST['email'],
        "Phone"                 =>  $POST['phone'],
        "Resume"                =>  $POST['resume'],
        "Response"              =>  $data,
        "CreatedDateTime"       =>  "NOW()"
    );
    // Insert Zip Recruiter Applicant Information
    $ziprecruiter_app_cand_res  =   G::Obj('ZipRecruiter')->insZipRecruiterApplicantData($info);
    
    if($req_info['Active'] == "") $req_info['Active'] = 'N';
    
    // Insert JobApplication Information
    $job_app_info = array(
        "OrgID"                 =>  $OrgID,
        "MultiOrgID"            =>  $MultiOrgID,
        "ApplicationID"         =>  $ApplicationID,
        "RequestID"             =>  $RequestID,
        "FormID"                =>  $FormID,
        "RequisitionStatus"     =>  $req_info['Active'],
        "ApplicantSortName"     =>  $POST['last_name'] . ', ' . $POST['first_name'],
        "Distinction"           =>  'P',
        "EntryDate"             =>  "NOW()",
        "LastModified"          =>  "NOW()",
        "ProcessOrder"          =>  "1",
        "LeadStatus"            =>  "N",
        "StatusEffectiveDate"   =>  "DATE(NOW())",
    );
    G::Obj('Applications')->insJobApplication($job_app_info);
    
    // Insert Applicant Status Logs Information
    G::Obj('ApplicantStatusLogs')->insApplicantStatusLog($OrgID, $ApplicationID, $RequestID, "1", "New Applicant");
    
    $lead_name                  =   'ZipRecruiter';
    //Update Lead Generator
    G::Obj('Applications')->updLeadGenerator($lead_name, $OrgID, $ApplicationID, $RequestID);    
    
    // Insert into ApplicantHistory
    $UpdateID                   =   'Applicant';
    $Comments                   =   'Application submitted for: ' . $RequisitionDetailsObj->getReqJobIDs($OrgID, $MultiOrgID, $RequestID);
    
    // Insert JobApplication History Data
    $job_application_history = array(
        "OrgID"                 =>  $OrgID,
        "ApplicationID"         =>  $ApplicationID,
        "RequestID"             =>  $RequestID,
        "ProcessOrder"          =>  "1",
        "StatusEffectiveDate"   =>  "DATE(NOW())",
        "Date"                  =>  "NOW()",
        "UserID"                =>  $UpdateID,
        "Comments"              =>  $Comments
    );
    // Insert JobApplicationsHistory
    G::Obj('JobApplicationHistory')->insJobApplicationHistory($job_application_history);
    
    // Get Organization Name
    $org_info = G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "OrganizationName");
    $OrganizationName = $org_info['OrganizationName'];
    
    // Insert Applicant Data
    $info = array();
    
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'first',
        ":Answer"           =>  $POST['first_name'],
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'last',
        ":Answer"           =>  $POST['last_name'],
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'email',
        ":Answer"           =>  $POST['email'],
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'homephone',
        ":Answer"           =>  $phone_info['Phone'],
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'source',
        ":Answer"           =>  'ZipRecruiter',
    );
    $info[] = array(
        ":OrgID"            =>  $OrgID,
        ":ApplicationID"    =>  $ApplicationID,
        ":QuestionID"       =>  'FormID',
        ":Answer"           =>  $FormID,
    );
    
    //Education records
    foreach($edu_info as $edu_que_id=>$edu_answer) {
        $info[] = array(
            ":OrgID"                =>  $OrgID,
            ":ApplicationID"        =>  $ApplicationID,
            ":QuestionID"           =>  $edu_que_id,
            ":Answer"               =>  $edu_answer,
        );
    }
    
    //Employment records
    foreach($company_info as $company_que_id=>$company_answer) {
        $info[] = array(
            ":OrgID"                =>  $OrgID,
            ":ApplicationID"        =>  $ApplicationID,
            ":QuestionID"           =>  $company_que_id,
            ":Answer"               =>  $company_answer,
        );
    }
    
    /**
     * Insert Answers Information
     */
    if(count($POST['answers']) > 0) {
        
        $answers_list       =   $POST['answers'];

        $json_que_results   =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID);
        $json_que_info      =   $json_que_results['json_ques'];
        
        for($a = 0; $a < count($answers_list); $a++) {
            
            $que_ans_info       =   $answers_list[$a];
            
            if($que_ans_info['id'] != 'address2') {

                //set parameters
                $params             =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":QuestionID"=>$que_ans_info['id']);
                //set where information
                $where              =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
                //Set data to insert
                $form_que_results   =   G::Obj('FormQuestions')->getFormQuestionsInformation("Question, QuestionID, QuestionTypeID, Required, value", $where, "", array($params));
                
                $form_que_info      =   $form_que_results['results'][0];
                
                if (array_key_exists("values", $que_ans_info))
                {
                    //Checkboxes
                    if($form_que_info['QuestionTypeID'] == 18) {
                        $chk_box_answers        =   $que_ans_info['values'];
                
                        $AnswersList            =   array();
                        for($ca = 0, $qai = 1; $ca < count($chk_box_answers); $ca++, $qai++) {
                            $AnswersList[$que_ans_info['id']."-".$qai]  =   $chk_box_answers[$ca];
                        }
                        $Answer                 =   json_encode($AnswersList);
                
                        $QI                     =   $form_ques_list[$que_ans_info['id']];
                        $QI['Answer']           =   $Answer;
                        $QI['ApplicationID']    =   $ApplicationID;
                        $QI['AnswerStatus']     =   1;
                
                        $ApplicantsObj->insUpdApplicantData($QI);
                    }
                    else if($QI['QuestionTypeID'] == 1818) {
                        $chk_box_answers       =   $que_ans_info['values'];
                
                        $AnswersList           =   array();
                        for($ca = 0, $qai = 1; $ca < count($chk_box_answers); $ca++, $qai++) {
                            $AnswersList[$que_ans_info['id']."-".$qai]  =   $chk_box_answers[$ca];
                        }
                        $Answer                 =   json_encode($AnswersList);
                        
                        $QI                     =   $form_ques_list[$que_ans_info['id']];
                        $QI['Answer']           =   $Answer;
                        $QI['ApplicationID']    =   $ApplicationID;
                        $QI['AnswerStatus']     =   1;
                        
                        $ApplicantsObj->insUpdApplicantData($QI);
                    }
                
                }
                else if (array_key_exists("value", $que_ans_info))
                {
                    if($form_que_info['QuestionTypeID'] == 3
                    || $form_que_info['QuestionTypeID'] == 6
                    || $form_que_info['QuestionTypeID'] == 2)
                    {
                        if($que_ans_info['id'] == "country") {
                            $que_ans_info['value'] =   "US";   //Set all countries to US
                        }
                        
                        $info[] = array(
                            ":OrgID"                =>  $OrgID,
                            ":ApplicationID"        =>  $ApplicationID,
                            ":QuestionID"           =>  $que_ans_info['id'],
                            ":Answer"               =>  $que_ans_info['value'],
                        );
                        
                    }
                    else if($form_que_info['QuestionTypeID'] == 5) {
                        $info[] = array(
                            ":OrgID"                =>  $OrgID,
                            ":ApplicationID"        =>  $ApplicationID,
                            ":QuestionID"           =>  $que_ans_info['id'],
                            ":Answer"               =>  $que_ans_info['value'],
                        );
                    }
                    else if($form_que_info['QuestionTypeID'] == 13
                            || $json_que_info[$que_ans_info['id']]['QuestionTypeID'] == 13) {
                        $phone_info       =   G::Obj('Address')->getFormattedLeadPhoneNumber($que_ans_info['value']);
                    
                        $info[] = array(
                            ":OrgID"                =>  $OrgID,
                            ":ApplicationID"        =>  $ApplicationID,
                            ":QuestionID"           =>  $que_ans_info['id'],
                            ":Answer"               =>  $phone_info['Phone'],
                        );
                    }
                    else if($form_que_info['QuestionTypeID'] == 14
                            || $json_que_info[$que_ans_info['id']]['QuestionTypeID'] == 14) {
                            $phone_info       =   G::Obj('Address')->getFormattedLeadPhoneNumber($que_ans_info['value']);
                
                        $info[] = array(
                            ":OrgID"                =>  $OrgID,
                            ":ApplicationID"        =>  $ApplicationID,
                            ":QuestionID"           =>  $que_ans_info['id'],
                            ":Answer"               =>  $phone_info['Phone'],
                        );
                    }
                    else if($form_que_info['QuestionTypeID'] == 17) {
                    
                        $date_zip_format            =   $que_ans_info['value'];
                    
                        $info[] = array(
                            ":OrgID"                =>  $OrgID,
                            ":ApplicationID"        =>  $ApplicationID,
                            ":QuestionID"           =>  $que_ans_info['id'],
                            ":Answer"               =>  $date_zip_format,
                        );
                    }
                    else if($form_que_info['QuestionTypeID'] == 8) {
                    
                        if($form_que_info['QuestionID'] == 'coverletterupload') {
                            
                            $file_info = explode('.', $que_ans_info['filename']);
                            $extension = end($file_info);
                            
                            $fileName = $apatdir . '/' . $ApplicationID . '-coverletter.'.$extension;
                    
                            touch($fileName);
                            $fp = fopen($fileName, 'w');
                            fwrite($fp, base64_decode($que_ans_info['value']));
                            fclose($fp);
                    
                            chmod($fileName, 0644);
                    
                            // Insert Applicant Attachments Information
                            $attachment_info = array (
                                "OrgID"             =>  $OrgID,
                                "ApplicationID"     =>  $ApplicationID,
                                "TypeAttachment"    =>  'coverletterupload',
                                "PurposeName"       =>  'coverletter',
                                "FileType"          =>  $extension
                            );
                    
                            G::Obj('Attachments')->insApplicantAttachments ( $attachment_info );
                    
                        }
                        else if($form_que_info['QuestionID'] == 'otherupload') {

                            $file_info = explode('.', $que_ans_info['filename']);
                            $extension = end($file_info);
                            
                            $fileName = $apatdir . '/' . $ApplicationID . '-other.'.$extension;
                    
                            touch($fileName);
                            $fp = fopen($fileName, 'w');
                            fwrite($fp, base64_decode($que_ans_info['value']));
                            fclose($fp);
                    
                            chmod($fileName, 0644);
                    
                            // Insert Applicant Attachments Information
                            $attachment_info = array (
                                "OrgID"             =>  $OrgID,
                                "ApplicationID"     =>  $ApplicationID,
                                "TypeAttachment"    =>  'otherupload',
                                "PurposeName"       =>  'other',
                                "FileType"          =>  $extension
                            );
                    
                            G::Obj('Attachments')->insApplicantAttachments ( $attachment_info );
                        }
                    
                    }
                    else {
                        $info[] = array(
                            ":OrgID"                =>  $OrgID,
                            ":ApplicationID"        =>  $ApplicationID,
                            ":QuestionID"           =>  $que_ans_info['id'],
                            ":Answer"               =>  $que_ans_info['value'],
                        );
                    }
                }
                
            }
            
        }
    }
    
    $params_list    =   array();
    $question_ids   =   array();
    $pi             =   0;

    $informed       =   '';
    for($ini = 0; $ini < count($info); $ini++) {
        
        if(!in_array($info[$ini][":QuestionID"], $question_ids)) {
            
            $params_list[$pi][":OrgID"]             =   $info[$ini][":OrgID"];
            $params_list[$pi][":ApplicationID"]     =   $info[$ini][":ApplicationID"];
            $params_list[$pi][":QuestionID"]        =   $info[$ini][":QuestionID"];
            $params_list[$pi][":Answer"]            =   $info[$ini][":Answer"];
            
            $question_ids[]                         =   $info[$ini][":QuestionID"];

            if($info[$ini][":QuestionID"] == 'informed') {
                $informed   =   $info[$ini][":Answer"];
            }
            
            $pi++;
        }
    }
    
    // Insert Applicant Data
    G::Obj('Applicants')->insApplicantData($params_list);

    $set_info               =   array("Informed = :Informed");
    $where_info             =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
    $params_info            =   array(":Informed"=>$informed, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
    $update_result          =   G::Obj('Applications')->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params_info));
    
    // Create resume based on binary data
    if ($POST['resume'] != "") {
        $fileName = $apatdir . '/' . $ApplicationID . '-resume' . '.pdf';
        touch($fileName);
        $fp = fopen($fileName, 'w');
        fwrite($fp, base64_decode($POST['resume']));
        fclose($fp);
    
        chmod($fileName, 0644);
    
        // Insert Applicant Attachments Information
        $attachment_info = array(
            "OrgID"             => $OrgID,
            "ApplicationID"     => $ApplicationID,
            "TypeAttachment"    => 'resumeupload',
            "PurposeName"       => 'resume',
            "FileType"          => 'pdf'
        );
        
        G::Obj('Attachments')->insApplicantAttachments($attachment_info);
    }
    
    // Set OnboardStatus to ready for this Application
    if ($OrgID == "I20091201") {
        //Get Applicant MultiOrgID
        $AppMultiOrgID  =   G::Obj('ApplicantDetails')->getMultiOrgID($OrgID, $ApplicationID, $RequestID);
        //Set Onboard Header Information
        $onboard_info   =   array(
            "OrgID"         =>  $OrgID,
            "MultiOrgID"    =>  $AppMultiOrgID,
            "ApplicationID" =>  $ApplicationID,
            "RequestID"     =>  $RequestID,
            "Status"        =>  'ready',
            "ProcessDate"   =>  "NOW()",
            "OnboardFormID" =>  'XML',
        );
        $on_update      =   " ON DUPLICATE KEY UPDATE Status = :UStatus, ProcessDate = NOW()";
        $update_info    =   array(":UStatus" =>  'ready');
        G::Obj('OnboardApplications')->insOnboardApplication($onboard_info, $on_update, $update_info);
    }
    
    // Bind parameters
    $params     =   array (':OrgID' => $OrgID);
    // Set condition
    $where      =   array ("OrgID = :OrgID", "Role IN ('','master_admin')");
    // Auto Forward //Get Userinformation based on role
    $results    =   G::Obj('IrecruitUsers')->getUserInformation ( "UserID", $where, "Role LIMIT 1", array ($params) );
    $admin      =   $results ['results'] [0] ['UserID'];
    
    //Assign Status, Presented To
    $ProcessOrder   =   $NPresentedTo   =   $NAssignStatus  =   '1';
    //Assign Internal Forms
    include COMMON_DIR . 'formsInternal/AssignInternalForms.inc';
    
    // Bind parameters
    $params     =   array (':OrgID' => $OrgID, ':RequestID' => $RequestID);
    // Set condition
    $where      =   array ("OrgID    = :OrgID", "RequestID = :RequestID");
    // Get Requisition Forward Information
    $results    =   $RequisitionsObj->getRequisitionForwardInfo ( "EmailAddress", $where, "", array ($params) );
    
    if ($results ['count'] > 0) {
    
        require_once IRECRUIT_DIR . 'applicants/EmailApplicant.inc';
    
        $ATTS               =   array();
        //Have to update it with forward applicant specs information
        $req_forward_specs  =   $RequisitionForwardSpecsObj->getRequisitionForwardSpecsDetailInfo($OrgID, $RequestID);
        $forward_specs_list =   json_decode($req_forward_specs ['ForwardSpecsList'], true);
        //Forward Specs List
        foreach($forward_specs_list as $spec_que_id) {
            $ATTS[$spec_que_id] =   'Y';
        }
    
        $EmailList     =   array();
        $status_index  =   0;
        if (is_array ( $results ['results'] )) {
            foreach ( $results ['results'] as $EM ) {
                $Attachments = forwardApplicant ( $OrgID, $ApplicationID, $RequestID, $EM ['EmailAddress'], $ATTS, 'Application Auto Forwarded', $admin, 'processforward', $status_index);
                $EmailList[] = $EM ['EmailAddress'];
                $status_index++;
            } // end foreach
        }
    
        updateApplicantStatus ( $OrgID, $ApplicationID, $RequestID, 'Application', implode(", ", $EmailList), $Attachments, 'Application Auto Forwarded' );
    } // end result > 0
    
    // ##################################### Mail Code ################################################
    // Send Thank you email to Applicant
    //require_once IRECRUIT_DIR . 'ziprecruiter/SendEmailThankYou.inc';
}
else {
    //Log the ziprecruiter response
    Logger::writeMessage ( ROOT . "logs/ziprecuiter_applicants_info/applicant-data" . date ( 'Y-m-d-H:i:s' ) . '-' . uniqid() . ".log", $data, "a+", true, 'Save Zip Recruiter' );
http_response_code(404);
//var_dump(http_response_code());
header('Status: 404', TRUE, 404);
echo 'Error';
exit;
}
