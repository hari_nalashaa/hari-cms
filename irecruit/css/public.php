<?php
header("Content-type: text/css"); 
require_once '../Configuration.inc';
?>
/* Generic Selectors */
 
body {
   margin: 0px;
   width:100%;
   font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
   font-size: 12px;
   background-color: #<?php echo $COLOR['Light']?>;
}
 
p {
<?php
if ($OrgID == "I20110901") {
echo '   font-size: 14px;';
} else {
echo '   font-size: 12px;';
}
?>
}

table {
<?php
if ($OrgID == "I20110901") {
echo '   font-size: 14px;';
} else {
echo '   font-size: 12px;';
}
?>
}
 
/**************** Pseudo classes ****************/

<?php

if ($OrgID == "I20110801") {

echo <<<END
a:link {
   color: blue;
   text-decoration: underline;
}
 
a:visited {
   color: blue;
   text-decoration: underline;
}
 
a:hover {
   color: blue;
   text-decoration: underline;
}

a:active {
   color: #555555;
   font-style: normal;
}
END;

} else {

echo <<<END
a:link {
   color: #555555;
   text-decoration: none;
}
 
a:visited {
   color: #555555;
   text-decoration: none;
}
 
a:hover {
   color: #555555;
   text-decoration: underline;
   font-weight: bold;
   font-style: normal;
}

a:active {
   color: #555555;
   font-weight: bold;
   font-style: normal;
}
END;

} // end else OrgID

?>
.title  {
   font-weight: bold;
   font-size: 14px;
   line-height: 200%;
}


/************************* ID's *************************/
#register {
   float:left;
   width:160px;
   padding:5px;
   position:relative;
   height:80px;
   margin-right:10px;
}
#login {
   float:left;
   width:160px;
   padding:5px;
   position:relative;
   height:80px;
   margin-right:10px;
}
#apply {
   float:left;
   width:160px;
   padding:5px;
   position:relative;
   height:80px;
}

#topDoc {
   padding: 10px 0px 0px 25px; /*top right bottom left*/
   background-color: #FFFFFF;
   border-bottom: 4px solid #<?php echo $COLOR['Heavy']?>;
}


#centerDoc {
   margin-top: 5px;
   margin-bottom: 10px;
   margin-right: 5px;
   margin-left: 5px;
   padding: 20px 20px 20px 20px; /*top right bottom left*/
   background-color: #<?php echo $COLOR['LightLight']?>;
}

#bottomDoc {
   clear: both;
   width: 100%;
   padding: 5px 0 5px 0; /*top right bottom left*/
   font-size: 11px;
   background-color: #<?php echo $COLOR['Light']?>;
}

#bottomDoc p {
   text-align: center;
   color: #000000;
}

#bottomDoc a {
   color: #000000;
}
#loading-div-background {
        display:none;
        position:fixed;
        top:0;
        left:0;
	background:#ffffff;
        width:100%;
        height:100%;
}

#loading-div {
         width: 100px;
         height: 100px;
         text-align:center;
         position:absolute;
         left: 50%;
         top: 50%;
         margin-left:-100px;
         margin-top: -300px;
}

