<?php
if (isset($_REQUEST['process'])
	&& $_REQUEST['process'] == 'Y'
	&& $_REQUEST['process_onboard'] == 'YES') {

	$ERROR              =   '';
	$REQUIRED           =   array ();
	$REQUIREDQUESTION   =   array ();

	$OnboardFormID      =   $_REQUEST['OnboardSource'];

	$columns            =   "QuestionID, Question";
	//Set where condition
	$where              =   array (
								"OrgID           =   :OrgID",
								"OnboardFormID   =   :OnboardFormID",
								"Required        =   'Y'"
							);
	//Set parameters
	$params             =   array (":OrgID" => $OrgID, ":OnboardFormID"=>$OnboardFormID);
	$results            =   $OnboardQuestionsObj->getOnboardQuestionsInformation($columns, $where, "", array ($params) );

	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $OQN ) {
			$REQUIRED [] = $OQN ['QuestionID'];
			$REQUIREDQUESTION [$OQN ['QuestionID']] = $OQN ['Question'];
		}
	}

	//Validate the information
	$ValidateOnboardFormObj->FORMDATA['REQUEST']   =   $_REQUEST;
	$ValidateOnboardFormObj->FORMDATA['FILES']     =   $_FILES;
	$ERRORS =  $ValidateOnboardFormObj->validateOnboardForm($OrgID, $OnboardFormID);

	foreach($ERRORS as $ErrorQueID=>$ErrorQuestion) {
		$ERROR .= " - " . $ErrorQuestion . ".<br>";
	}

	if ($ERROR) {

		$message = '<br>Required fields are not present.' . "<br>";
		$message .= $ERROR;

	} else { // we have good input

		//Skip fields on duplicate key
		$skip       =   array('wotcID', 'ApplicationID');
		//Insert Update Processing
		$info       =   array(
							'wotcID'        =>  $_POST['wotcID'],
							'ApplicationID' =>  $_POST['WotcApplicationID'],
							'OfferDate'     =>  date("Y-m-d",  strtotime($_POST['OfferDate'])),
							'HiredDate'     =>  date("Y-m-d",  strtotime($_POST['HiredDate'])),
							'StartDate'     =>  date("Y-m-d",  strtotime($_POST['StartDate'])),
							'StartingWage'  =>  $_POST['StartingWage'],
							'Position'      =>  $_POST['Position'],
							'Received'      =>  "NOW()",
							'LastUpdated'   =>  "NOW()"
						);
		G::Obj('WOTCProcessing')->insUpdProcessing($info, $skip);
		
		//Update Application Status
		$set_info	=	array("ApplicationStatus = :ApplicationStatus");
		$where_info	=	array("wotcID = :wotcID", "ApplicationID = :ApplicationID");
		$params		=	array(":wotcID"=>$_POST['wotcID'], ":ApplicationID"=>$_POST['WotcApplicationID'], ":ApplicationStatus"=>"H");
		G::Obj('WOTCApplicantData')->updApplicantDataInfo($set_info, $where_info, array($params));
		

		$ProcessOrder = 0;
		$Comment = "WOTC Onboard Form Processed.";
		$StatusDate = "NOW()";
		$DispositionCode = '';
		
		// Set JobApplication History Information
		$job_app_history = array (
				"OrgID"                  => $OrgID,
				"ApplicationID"          => $ApplicationID,
				"RequestID"              => $RequestID,
				"ProcessOrder"           => $ProcessOrder,
				"DispositionCode"        => $DispositionCode,
				"StatusEffectiveDate"    => $StatusDate,
				"Date"                   => "NOW()",
				"UserID"                 => $USERID,
				"Comments"               => $Comment
		);
		
		// Insert JobApplication History
		$ins_res_job_app_his = G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
		
		$message	=	"<br>Successfully added the hire information.";
	}
}	
?>