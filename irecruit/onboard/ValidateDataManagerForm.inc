<?php
G::Obj('ApplicationForm')->FORM_SOURCE	=	"ONBOARD";

$i      =   0;
$iFQ    =   0;
$iOB    =   0;
$iAA    =   0;
$iPF    =   0;

// Set all internal forms question ids to array
$all_internal_form_que_ids = array();

// Get AppData
$APPDATA            =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);
$OnboardFormData    =   G::Obj('OnboardFormData')->getOnboardFormData($OrgID, $ApplicationID, $RequestID, $_REQUEST['OnboardSource']);

//Get Requisition FormID
$CurrentRequisitionFormID		=	G::Obj('RequisitionDetails')->getRequisitionFormID($OrgID, $RequestID);
//Get common questions list
$common_req_onboard_ques		=	G::Obj('OnboardQuestions')->getOnboardAndRequisitionCommonQuestionsList($OrgID, $_REQUEST['OnboardSource'], $CurrentRequisitionFormID);
$common_req_onboard_ques_keys	=	array_keys($common_req_onboard_ques);

//Set APPDATA
$REQ_APPDATA					=	G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);

foreach ($REQ_APPDATA as $REQ_APPDATA_QUEID=>$REQ_APPDATA_ANS) {
    if(!isset($APPDATA[$REQ_APPDATA_QUEID]) && in_array($REQ_APPDATA_QUEID, $common_req_onboard_ques_keys)) {
        $APPDATA[$REQ_APPDATA_QUEID] = $REQ_APPDATA_ANS;
    }
}

$hired_status_info  			=   G::Obj('GenericQueries')->getRowInfo("ApplicantProcessFlow", "getHiredStatusProcessOrder('$OrgID') as HiredProcessOrder");
$hired_status       			=   $hired_status_info['HiredProcessOrder'];
$process_order					=	G::Obj('ApplicantDetails')->getProcessOrder($OrgID, $ApplicationID, $RequestID);
$effective_date_info			=	G::Obj('GenericQueries')->getRowInfoByQuery("SELECT getProcessOrderStatusEffectiveDate('$OrgID', '$RequestID', '$ApplicationID', '$process_order') as EffectiveDate");

if($hired_status == $process_order)
{
    $APPDATA['Sage_HireDate']	=	date('m/d/Y', strtotime($effective_date_info['EffectiveDate']));
}

//Loop post values
if(isset($_POST) && count($_POST) > 0) {
    foreach ($_POST as $PostKey=>$PostValue) {
        $APPDATA[$PostKey]   =   $PostValue;
    }
}

//Add the onboard data to APPDATA variable
foreach ($OnboardFormData as $OnboardFormKey=>$OnboardFormValue) {
    $APPDATA[$OnboardFormKey]   =   $OnboardFormValue;
}

$alphabet   =   array ('XX', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

if ($permit ['Internal_Forms'] == 1) {
    
    //Set PreFilledForm Questions
    if ($feature ['PreFilledForms'] == "Y") {
        
        //set where condition
        $where      =   array("OrgID = :OrgID");
        //set parameters
        $params     =   array(":OrgID"=>$OrgID);
        //Get PrefilledForms Information
        $results    =   G::Obj('FormsInternal')->getPrefilledFormsInfo("PreFilledFormID", $where, "SortOrder, TypeForm, Form", array($params));
        
        if(is_array($results['results'])) {
            foreach ($results['results'] as $PFF) {
                $all_internal_form_que_ids[] = $PFF ['PreFilledFormID'];
            } // end foreach
        }
        
    } // end feature
    
    //Set WebForm Questions
    if ($feature ['WebForms'] == "Y") {
        
        //set where condition
        $where      =   array("OrgID = :OrgID");
        //set parameters
        $params     =   array(":OrgID"=>$OrgID);
        //Get WebForms Information
        $results    =   G::Obj('FormsInternal')->getWebFormsInfo("WebFormID", $where, "FormName", array($params));
        
        if(is_array($results['results'])) {
            foreach($results['results'] as $WFQNT) {
                $all_internal_form_que_ids[] = $WFQNT ['WebFormID'];
            } // end foreach
        }
        
    } // end feature
    
    //Set Agreement Form Questions
    if ($feature ['AgreementForms'] == "Y") {
        //set where condition
        $where      =   array("OrgID = :OrgID");
        //set parameters
        $params     =   array(":OrgID"=>$OrgID);
        // Agreement Forms
        $results    =   G::Obj('FormsInternal')->getAgreementFormsInfo("AgreementFormID", $where, "FormName", array($params));
        
        if(is_array($results['results'])) {
            foreach($results['results'] as $AFN ) {
                $all_internal_form_que_ids[] = $AFN ['AgreementFormID'];
            } // end foreach
        }
        
    } // end feature
    
    //Set SpecificationForm Questions
    if ($feature ['SpecificationForms'] == "Y") {
        
        //set where condition
        $where      =   array("OrgID = :OrgID");
        //set parameters
        $params     =   array(":OrgID"=>$OrgID);
        //get specification forms
        $results    =   G::Obj('FormsInternal')->getSpecificationForms("SpecificationFormID", $where, "FormType, DisplayTitle", array($params));
        
        if(is_array($results['results']))
        {
            foreach($results['results'] as $WFQNT) {
                $all_internal_form_que_ids[] = $WFQNT ['SpecificationFormID'];
            } // end foreach
        }
        
    } // end feature
    
}

//Get FormID
$FormID             =   $ApplicantDetailsObj->getFormID($OrgID, $ApplicationID, $RequestID);

$CHECK_FIELDS       =   array();
$PREFILLED_FIELDS   =   array();
//Set columns
$columns            =   "iRecruitTable, FormName, iRecruitField, Section, SubSection";
//Set where condition
$where              =   array("OrgID = :OrgID");
//Set parameters
$params             =   array(":OrgID" => $OrgID);
//Get the sage abra map questions
$resultsDMM         =   $ExporterDataManagerObj->getSageAbraMapInfo($columns, $where, "", array($params));

$sub_sections       =   array('Work Experience', 'Education', 'Certifications');

if (is_array($resultsDMM['results'])) {
    foreach ($resultsDMM['results'] as $DMM) {
        if ($DMM['iRecruitTable'] == "PreFilledForm") {
            if(in_array($DMM['FormName'], $all_internal_form_que_ids)) {
                $PREFILLED_FIELDS[$DMM['FormName']][] = $DMM['iRecruitField'];
            }
        } else {
            $CHECK_FIELDS[] = $DMM['iRecruitField'];
        }
    }
}

$columns            =   "iRecruitTable, FormName, iRecruitField, Section, SubSection";
$where              =   array("OrgID = :OrgID");
$params             =   array(":OrgID" => $OrgID);
$resultsDMM         =   $ExporterDataManagerObj->getSageAbraMapInfo($columns, $where, "", array($params));

if (is_array($resultsDMM['results'])) {
    foreach ($resultsDMM['results'] as $DMM) {
        if ($DMM['iRecruitTable'] == "PreFilledForm") {
            if (! in_array($DMM['iRecruitField'], $PREFILLED_FIELDS[$DMM['FormName']])) {
                if(in_array($DMM['FormName'], $all_internal_form_que_ids)) {
                    $PREFILLED_FIELDS[$DMM['FormName']][] = $DMM['iRecruitField'];
                }
            }
        } else {
            if (! in_array($DMM['iRecruitField'], $CHECK_FIELDS) && $DMM['Section'] != 'Multiple Sections') {
                $CHECK_FIELDS[] = $DMM['iRecruitField'];
            }
        }
    }
}

$PREFILLED_FIELDS_FLIP = array();
foreach ($PREFILLED_FIELDS as $PREFILLED_FORM_ID => $PREFILLED_QUESTIONS) {
    $PREFILLED_FIELDS_FLIP[$PREFILLED_FORM_ID] = array_flip($PREFILLED_QUESTIONS);
}

$CHECK_FIELDS_FLIP  =   array_flip($CHECK_FIELDS);

// Set where condition
$where      =   array(
                    "OrgID      = :OrgID",
                    "FormID     = :FormID"
                );

// Set parameters
$params     =   array(
                    ":OrgID"    =>  $OrgID,
                    ":FormID"   =>  $FormID
                );
// Set columns
$columns    =   "QuestionID, Question, QuestionTypeID, Validate, Required";
// Get FormQuestions Information
$results    =   $FormQuestionsObj->getFormQuestionsInformation($columns, $where, "SectionID, QuestionOrder", array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $FQ) {
        $VALIDATE[$FQ['QuestionID']]['Question']        =   $FQ['Question'];
        $VALIDATE[$FQ['QuestionID']]['QuestionTypeID']  =   $FQ['QuestionTypeID'];
        $VALIDATE[$FQ['QuestionID']]['Validate']        =   $FQ['Validate'];
        $VALIDATE[$FQ['QuestionID']]['Section']         =   "Form Question";
    }
}

// Set columns
$columns    =   "QuestionID, Question, QuestionTypeID, Validate, Required";
// Set where condition
$where      =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID");
// Set parameters
$params     =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$feature ['DataManagerType']);
// Get OnboardQuestions
$results    =   G::Obj('FormQuestions')->getQuestionsInformation('OnboardQuestions', $columns, $where, "QuestionOrder", array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $FQ) {
        if (in_array($FQ['QuestionID'], $CHECK_FIELDS)) {
            $VALIDATE[$FQ['QuestionID']]['Question']        =   $FQ['Question'];
            $VALIDATE[$FQ['QuestionID']]['QuestionTypeID']  =   $FQ['QuestionTypeID'];
            $VALIDATE[$FQ['QuestionID']]['Validate']        =   $FQ['Validate'];
            $VALIDATE[$FQ['QuestionID']]['Section']         =   "Onboard Question";
        }
    }
}

// Set columns
$columns    =   "QuestionID, Question, QuestionTypeID, Validate, Required";
// Set where condition
$where      =   array("OrgID = :OrgID", "FormID = :FormID");
// Set parameters
$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$feature ['DataManagerType']);
// Get FormQuestions Information
$results    =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $FQ) {
        if (in_array($FQ['QuestionID'], $CHECK_FIELDS)) {
            $VALIDATE[$FQ['QuestionID']]['Question']        =   $FQ['Question'];
            $VALIDATE[$FQ['QuestionID']]['QuestionTypeID']  =   $FQ['QuestionTypeID'];
            $VALIDATE[$FQ['QuestionID']]['Validate']        =   $FQ['Validate'];
            $VALIDATE[$FQ['QuestionID']]['Section']         =   "Affirmative Action Question";
        }
    }
}

// Set columns
$columns    =   "QuestionID, Question, QuestionTypeID, Validate, Required";
// Set where condition
$where      =   array("OrgID = :OrgID", "FormID = :FormID");
// Set parameters
$params     =   array(":OrgID"=>$OrgID, ":FormID"=>$feature ['DataManagerType']);
// Get FormQuestions Veteran Information
$results    =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $FQ) {
        if (in_array($FQ['QuestionID'], $CHECK_FIELDS)) {
            $VALIDATE[$FQ['QuestionID']]['Question']        =   $FQ['Question'];
            $VALIDATE[$FQ['QuestionID']]['QuestionTypeID']  =   $FQ['QuestionTypeID'];
            $VALIDATE[$FQ['QuestionID']]['Validate']        =   $FQ['Validate'];
            $VALIDATE[$FQ['QuestionID']]['Section']         =   "Veteran Form Question";
        }
    }
}

// Set columns
$columns    =   "QuestionID, Question, QuestionTypeID, Validate, Required";
// Set where condition
$where      =   array("(OrgID = :OrgID)", "FormID = :FormID");
// Set parameters
$params     =   array(":OrgID" => $OrgID, ":FormID"=>$feature ['DataManagerType']);
// Get FormQuestions Disabled Information
$results    =   $FormQuestionsObj->getFormQuestionsInformation($columns, $where, "QuestionOrder", array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $FQ) {
        if (in_array($FQ['QuestionID'], $CHECK_FIELDS)) {
            $VALIDATE[$FQ['QuestionID']]['Question']        =   $FQ['Question'];
            $VALIDATE[$FQ['QuestionID']]['QuestionTypeID']  =   $FQ['QuestionTypeID'];
            $VALIDATE[$FQ['QuestionID']]['Validate']        =   $FQ['Validate'];
            $VALIDATE[$FQ['QuestionID']]['Section']         =   "Disabled Form Question";
        }
    }
}

$VALIDATE_KEYS = array_keys($VALIDATE);

// Set columns
$columns    =   "PreFilledFormID, QuestionID, Question, QuestionTypeID, Validate, Required";
// Set where condition
$where      =   array("OrgID = :OrgID");
// Set parameters
$params     =   array(":OrgID" => $OrgID);
// Get FormQuestions Information
$results    =   G::Obj('FormQuestions')->getQuestionsInformation("PreFilledFormQuestions", $columns, $where, "", array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $FQ) {
        
        if (is_array($PREFILLED_FIELDS[$FQ['PreFilledFormID']])) {
            if (in_array($FQ['QuestionID'], $PREFILLED_FIELDS[$FQ['PreFilledFormID']])) {
                $PREFILLED_VALIDATE[$FQ['PreFilledFormID']][$FQ['QuestionID']]['Question']          =   $FQ['Question'];
                $PREFILLED_VALIDATE[$FQ['PreFilledFormID']][$FQ['QuestionID']]['QuestionTypeID']    =   $FQ['QuestionTypeID'];
                $PREFILLED_VALIDATE[$FQ['PreFilledFormID']][$FQ['QuestionID']]['Validate']          =   $FQ['Validate'];
                $PREFILLED_VALIDATE[$FQ['PreFilledFormID']][$FQ['QuestionID']]['Section']           =   "Prefilled Form Question";
            }
        }
    }
}

$prefilled_errors_list  =   array();
$where_info             =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
$params_info            =   array(":OrgID" => $OrgID, ":ApplicationID" => $ApplicationID);
$prefilled_results      =   G::Obj('FormData')->getPreFilledFormData("*", $where_info, "", "", array($params_info));

if (is_array($prefilled_results['results'])) {
    foreach ($prefilled_results['results'] as $QueAnsInfo) {
        $QuestionID     =   $QueAnsInfo['QuestionID'];
        $value          =   $QueAnsInfo['Answer'];
        if($value != "") $PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']][$QuestionID] = $value;
    }
}

if (is_array($prefilled_results['results'])) {
    foreach ($prefilled_results['results'] as $QueAnsInfo) {
        
        $ierror         =   "N";
        $QuestionID     =   $QueAnsInfo['QuestionID'];
        $value          =   $QueAnsInfo['Answer'];
        
        if (is_array($PREFILLED_FIELDS[$QueAnsInfo['PreFilledFormID']])) {
            if (in_array($QuestionID, $PREFILLED_FIELDS[$QueAnsInfo['PreFilledFormID']])) {
                
                if ((! preg_match("/social\d$/", $QuestionID)) && (! preg_match("/phone\d$/", $QuestionID)) && (! preg_match("/^sage_/", $QuestionID))) {
                    $QuestionID = preg_replace("/[0-9]$/", "", $QuestionID);
                }
                
                if ($QuestionID == "email") {
                    $ck = $ValidateObj->validate_email_address($value);
                    
                    if ($ck) {
                        $ierror = "Y";
                    }
                }
                else if($QuestionID == "SSN") {
                    
                    $ssnck1 = $PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']]['SSN1']; // first three
                    $ssnck2 = $PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']]['SSN3']; // last four
                    $ssnck3 = $PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']]['SSN2']; // fourth and fith
                    
                    if ($ssnck1 == "000") {
                        $ierror = "Y";
                    }
                    if ($ssnck1 == "666") {
                        $ierror = "Y";
                    }
                    if (($ssnck1 >= "900") && ($ssnck1 <= "999")) {
                        $ierror = "Y";
                    }
                    if ($ssnck3 == "00") {
                        $ierror = "Y";
                    }
                    if ($ssnck2 == "0000") {
                        $ierror = "Y";
                    }
                    
                }
                else if (($PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['Question'] != "") && ($value != "")) {
                    
                    if($PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['QuestionTypeID'] != "30") {
                        // check dates
                        if (($PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['QuestionTypeID'] == "17")
                            || ($PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['QuestionTypeID'] == "1")) {
                                if (! preg_match("/^\d\d\/\d\d\/\d\d\d\d/", $value)) {
                                    $ierror = "Y";
                                }
                            } else { // else date
                                
                                // check for special characters in all fields
                                if (strpbrk($value, '~!#$%^&*()_+`=:;"\'<>?') != FALSE) {
                                    $ierror = "Y";
                                }
                                
                                // check numbers
                                if ($PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']]['Validate'] == "numeric") {
                                    if (! preg_match("/^\d+/", $value)) {
                                        $ierror = "Y";
                                    } // end preg_match
                                }
                                
                                // check decimals
                                if ($PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']]['Validate'] == "decimal") {
                                    $value = number_format($value, 2, '.', '');
                                    if (! preg_match("/^\d+\.\d+/", $value)) {
                                        $ierror = "Y";
                                    } // end preg_match
                                }
                            } // end else date
                    }
                    
                } // end in array
            } // end if VALIDATE
        }
        
        if ($ierror == "Y") {
            if($QuestionID == "SSN" || $QuestionID == "SSN1" || $QuestionID == "SSN2" || $QuestionID == "SSN3") {
                if(!in_array($QueAnsInfo['PreFilledFormID'] . "-SSN", $prefilled_errors_list)) {
                    $ERROR .= "<u>" . $PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['Section'] . "</u> - ";
                    $ERROR .= $QueAnsInfo['PreFilledFormID']."-".$PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['Question'] . "&nbsp;&nbsp;<b>";
                    $ERROR .= $PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']]['SSN1']."-".$PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']]['SSN2']."-".$PREFILLED_DATA[$QueAnsInfo['PreFilledFormID']]['SSN3'] . "</b><br><br>";
                    $i ++;
                    $iPF ++;
                }
            }
            else {
                $ERROR .= "<u>" . $PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['Section'] . "</u> - ";
                $ERROR .= $QueAnsInfo['PreFilledFormID'] . " - " . $PREFILLED_VALIDATE[$QueAnsInfo['PreFilledFormID']][$QuestionID]['Question'] . "&nbsp;&nbsp;<b>" . $value . "</b><br><br>";
                $i ++;
                $iPF ++;
            }
        }
        
        if(!in_array($QueAnsInfo['PreFilledFormID']."-".$QuestionID, $prefilled_errors_list)) {
            $prefilled_errors_list[] = $QueAnsInfo['PreFilledFormID']."-".$QuestionID;
        }
        
        if ($ierror == "N") {
            unset($PREFILLED_FIELDS_FLIP[$QueAnsInfo['PreFilledFormID']][$QuestionID]);
        }
    }
}

$VALIDATE_DATA          =   array();
$recent_position_from   =   1;
$recent_position_to     =   1;

foreach ($APPDATA as $QuestionID => $value) {
    
    $ierror             =   "N";
    
    // Get first letter
    $chk_letter         =   substr($QuestionID, 0, 1);
    $QuestionCondition  =   in_array($QuestionID, $VALIDATE_KEYS);
    
    if ($QuestionCondition) {
        
        $VALIDATE_DATA[$QuestionID] = $value;
        
        if ($value == "") {
            $ierror = "Y";
        }
        else {
            
            if ($QuestionID == "email") {
                
                $ck = $ValidateObj->validate_email_address($value);
                
                if ($ck) {
                    $ierror = "Y";
                }
                
            }
            else if ($QuestionID == "Sage_OrgID") {
                
                if ($value == "" || $value == "***") {
                    $ierror = "Y";
                }
                
            }
            else {
                
                if ((! preg_match("/social\d$/", $QuestionID)) && (! preg_match("/phone\d$/", $QuestionID)) && (! preg_match("/^Sage_/", $QuestionID))) {
                    $QuestionID = preg_replace("/[0-9]$/", "", $QuestionID);
                }
                
                if (($VALIDATE[$QuestionID]['Question'] != "") && ($value != "")) {
                    
                    // check dates
                    if (($VALIDATE[$QuestionID]['QuestionTypeID'] == "17") || ($VALIDATE[$QuestionID]['QuestionTypeID'] == "1")) {
                        
                        if (! preg_match("/^\d\d\/\d\d\/\d\d\d\d/", $value)) {
                            $ierror = "Y";
                        }
                    }
                    else if (($VALIDATE[$QuestionID]['QuestionTypeID'] == "13") || ($VALIDATE[$QuestionID]['QuestionTypeID'] == "14")) {
                        
                        if ($APPDATA [$QuestionID] == "") {
                            $ierror = "Y";
                        }
                        else if ($APPDATA [$QuestionID] != "") {
                            $phone_info = json_decode($APPDATA [$QuestionID], true);
                            if($phone_info[0] == ""
                                || $phone_info[1] == ""
                                || $phone_info[2] == "") {
                                    $ierror = "Y";
                                }
                                if($phone_info[0] != ""
                                    && $phone_info[1] != ""
                                    && $phone_info[2] != "") {
                                        
                                        if($VALIDATE[$QuestionID]['QuestionTypeID'] == "13") {
                                            if(strlen($phone_info[0]) != 3
                                                || strlen($phone_info[1]) != 3
                                                || strlen($phone_info[2]) != 4) {
                                                    $ierror = "Y";
                                                }
                                        }
                                        else if($VALIDATE[$QuestionID]['QuestionTypeID'] == "14") {
                                            if(strlen($phone_info[0]) != 3
                                                || strlen($phone_info[1]) != 3
                                                || strlen($phone_info[2]) != 4) {
                                                    $ierror = "Y";
                                                }
                                        }
                                    }
                        }
                        
                    }
                    else if ($VALIDATE[$QuestionID]['QuestionTypeID'] == "15") {
                        
                        if ($APPDATA [$QuestionID] == "") {
                            $ierror = "Y";
                        }
                        else if ($APPDATA [$QuestionID] != "") {
                            $ssn_info = json_decode($APPDATA [$QuestionID], true);
                            if($ssn_info[0] == ""
                                || $ssn_info[1] == ""
                                || $ssn_info[2] == "") {
                                    $ierror = "Y";
                                }
                                if($ssn_info[0] != ""
                                    && $ssn_info[1] != ""
                                    && $ssn_info[2] != "") {
                                        
                                        if(strlen($ssn_info[0]) != 3) {
                                            $ierror = "Y";
                                        }
                                        if(strlen($ssn_info[1]) != 2) {
                                            $ierror = "Y";
                                        }
                                        if(strlen($ssn_info[2]) != 4) {
                                            $ierror = "Y";
                                        }
                                    }
                        }
                        
                    }
                    else { // else date
                        
                        $spec_chars = array('~', '!', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '`', '=', ':', ';', '"', '\\', '<', '>', '?', "'");
                        // check for special characters in all fields
                        foreach($spec_chars as $sc) {
                            if(strpos(strtolower($value), $sc)) {
                                $ierror = "Y";
                            }
                        }
                        
                        // check numbers
                        if ($VALIDATE[$QuestionID]['Validate'] == "numeric") {
                            if (! preg_match("/^\d+/", $value)) {
                                $ierror = "Y";
                            } // end preg_match
                        }
                        
                        // check decimals
                        if ($VALIDATE[$QuestionID]['Validate'] == "decimal") {
                            $value = number_format($value, 2, '.', '');
                            if (! preg_match("/^\d+\.\d+/", $value)) {
                                $ierror = "Y";
                            } // end preg_match
                        }
                    } // end else date
                    
                } // end in array
                
            }
            
        }
        
        //Once error is set we will unset the check fields question id
        unset($CHECK_FIELDS_FLIP[$QuestionID]);
        
        include IRECRUIT_DIR . 'onboard/VerifyDataManagerErrorChecking.inc';
    }
} // end foreach APPDATA

foreach ($CHECK_FIELDS_FLIP as $ErrorQuestionID => $ErrorQuestionIndex) {
    if (in_array($ErrorQuestionID, $VALIDATE_KEYS)) {
        if(!isset($VALIDATE_DATA[$ErrorQuestionID]) || $VALIDATE_DATA[$ErrorQuestionID] == "") {
            $ERROR .= "<u>" . $VALIDATE[$ErrorQuestionID]['Section'] . "</u> - ";
            $ERROR .= $VALIDATE[$ErrorQuestionID]['Question'] . "&nbsp;&nbsp; <br><br>";
            $i ++;
        }
    }
}

foreach ($PREFILLED_FIELDS_FLIP as $PREFILLED_FORM_ID => $PREFILLED_FORM_INFO) {
    foreach ($PREFILLED_FORM_INFO as $PREFILLED_QUESTION_ID => $PREFILLED_QUESTION_INDEX) {
        
        $PREFILLED_VALIDATE_KEYS = array_keys($PREFILLED_VALIDATE[$PREFILLED_FORM_ID]);
        
        if(!isset($PREFILLED_DATA[$PREFILLED_FORM_ID][$PREFILLED_QUESTION_ID]) || $PREFILLED_DATA[$PREFILLED_FORM_ID][$PREFILLED_QUESTION_ID] == "") {
            if (in_array($PREFILLED_QUESTION_ID, $PREFILLED_VALIDATE_KEYS)) {
                if ($PREFILLED_QUESTION_ID != "") {
                    $ERROR .= "<u>" . $PREFILLED_VALIDATE[$PREFILLED_FORM_ID][$PREFILLED_QUESTION_ID]["Section"] . "</u> - ";
                    $ERROR .= $PREFILLED_FORM_ID . " - " . $PREFILLED_QUESTION_ID . " - ";
                    $ERROR .= $PREFILLED_VALIDATE[$PREFILLED_FORM_ID][$PREFILLED_QUESTION_ID]["Question"] . "&nbsp;&nbsp; <br><br>";
                    $i ++;
                }
            }
        }
    }
}
?>