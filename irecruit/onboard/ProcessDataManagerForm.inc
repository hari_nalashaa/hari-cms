<?php
function validateDate($date, $format = 'Y-m-d H:i:s')
{
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

if (isset($_REQUEST['process']) 
	&& $_REQUEST['process'] == 'Y'
	&& $_REQUEST['process_onboard'] == 'YES') {


    $ERROR              =   '';
    $REQUIRED           =   array ();
    $REQUIREDQUESTION   =   array ();

    $OnboardFormID      =   $_REQUEST['OnboardSource'];
    $current_onboard_setting =   $OnboardFormDataObj->getApplicantOnboardAnswer($OrgID, $OnboardFormID, $ApplicationID, 'onboard');
    
    $columns            =   "QuestionID, Question";
    // Set where condition
    $where              =   array (
                    			"OrgID           =   :OrgID",
                    			"OnboardFormID   =   :OnboardFormID",
                    			"Required        =   'Y'"
                        	);
    // Set parameters
    $params             =   array (":OrgID" => $OrgID, ":OnboardFormID"=>$OnboardFormID);
    $results            =   $OnboardQuestionsObj->getOnboardQuestionsInformation($columns, $where, "", array ($params) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $OQN ) {
			$REQUIRED [] = $OQN ['QuestionID'];
			$REQUIREDQUESTION [$OQN ['QuestionID']] = $OQN ['Question'];
		}
	}

	//Validate the information
	$ValidateOnboardFormObj->FORMDATA['REQUEST']   =   $_REQUEST;
	$ValidateOnboardFormObj->FORMDATA['FILES']     =   $_FILES;
	$ERRORS =  $ValidateOnboardFormObj->validateOnboardForm($OrgID, $OnboardFormID);

	foreach($ERRORS as $ErrorQueID=>$ErrorQuestion) {
	    $ERROR .= " - " . $ErrorQuestion . ".<br>";
	}
	
	if ($OrgID == "I20111205") { // Del Mar Fairgrounds
		$hire_age = 14;
	} else if ($OrgID == "I20201101") { // Polly Management
		$hire_age = 13;
	} else {
		$hire_age = 16;
	}

    if(isset($_REQUEST['Sage_HireDate'])) {
		if(validateDate($_REQUEST['Sage_HireDate'], 'm/d/Y') == false) {
			$ERROR .= " - Correct the Hire Date format.<br>";
		}
	}
	else if(isset($_REQUEST['hire_date'])) {
		if(validateDate($_REQUEST['hire_date'], 'm/d/Y') == false) {
			$ERROR .= " - Correct the Hire Date format.<br>";
		}
	}

	if ($_POST ['dob'] != "") {
		$parts    =   explode ( '/', $_POST [dob] );
		$newdate  =   "$parts[2]-$parts[0]-$parts[1]";
		$dobck    =   $MysqlHelperObj->getDateDiffWithNow ( $newdate );
		$age      =   365 * $hire_age;

		if ($dobck < $age) {
			$ERROR .= " - Date of Birth needs to be at least " . $hire_age . " years ago.<br>";
		}
	} // end dob

	if (($_POST [social1] != "") && ($_POST [social2] != "") && ($_POST [social3] != "")) {

		// take out everything except numbers and trim it.
		$ssnck        =   trim ( preg_replace ( "/[^0-9]/", "", $_POST [social1] . $_POST [social2] . $_POST [social3] ) );
		$ssnlength    =   strlen ( $ssnck );

		if ($ssnck == "") {
			$ERROR .= " - Social Security Number is missing.<br>";
		} else if ($ssnlength < 9) {
			$ERROR .= " - Social Security Number is missing digits.<br>";
		} else {
				
			$ssnck1 = $_POST [social1]; // first three
			$ssnck2 = $_POST [social2]; // last four
			$ssnck3 = $_POST [social3]; // fourth and fith
				
			if ($ssnck1 == "000") {
				$erck ++;
			}
			if ($ssnck1 == "666") {
				$erck ++;
			}
			if (($ssnck1 >= "900") && ($ssnck1 <= "999")) {
				$erck ++;
			}
			if ($ssnck3 == "00") {
				$erck ++;
			}
			if ($ssnck2 == "0000") {
				$erck ++;
			}
			if ($erck > 0) {
				$ERROR .= " - Social Security Number is invalid.<br>";
			}
		}
	} // end social security check

	if ($ERROR) {

		$message = '<br>Required fields are not present.' . "<br>";
		$message .= $ERROR;

	} else { // we have good input

	    /**
	     * This is a temporary code until the final upgrade.
	     * Upto that time, we have to keep on adding different conditions to it.
	     */
	    $message = '';
	    $GetFormPostAnswerObj->POST =   $_POST;    //Set Post Data
	    
	    $form_que_list         =   $OnboardQuestionsObj->getOnboardQuestionsList($OrgID, $OnboardFormID);
	     
	    $ques_to_skip          =   array();
	    foreach($form_que_list as $QuestionID=>$QuestionInfo) {
	    
	        $QI                =   $QuestionInfo;
	        	
	        if($QI['QuestionTypeID'] == 13) {
	            //After the complete upgrade have to remove this.
	            $ques_to_skip[]        =   $QI['QuestionID']."1";
	            $ques_to_skip[]        =   $QI['QuestionID']."2";
	            $ques_to_skip[]        =   $QI['QuestionID']."3";
	             
	            $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	            $QI['ApplicationID']   =   $ApplicationID;
	            $QI['RequestID']       =   $RequestID;
	            $QI['OnboardFormID']   =   $OnboardFormID;
	            $QI['AnswerStatus']    =   1;
	            
	            $OnboardFormDataObj->insUpdOnboardFormData($QI);
	        }
	        else if($QI['QuestionTypeID'] == 14) {
	            //After the complete upgrade have to remove this.
	            $ques_to_skip[]        =   $QI['QuestionID']."1";
	            $ques_to_skip[]        =   $QI['QuestionID']."2";
	            $ques_to_skip[]        =   $QI['QuestionID']."3";
	            $ques_to_skip[]        =   $QI['QuestionID']."ext";
	        
	            $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	            $QI['ApplicationID']   =   $ApplicationID;
	            $QI['RequestID']       =   $RequestID;
	            $QI['OnboardFormID']   =   $OnboardFormID;
	            $QI['AnswerStatus']    =   1;
	            
	            $OnboardFormDataObj->insUpdOnboardFormData($QI);
	        }
	        else if($QI['QuestionTypeID'] == 15) {
	            //After the complete upgrade have to remove this.
	            $ques_to_skip[]        =   $QI['QuestionID']."1";
	            $ques_to_skip[]        =   $QI['QuestionID']."2";
	            $ques_to_skip[]        =   $QI['QuestionID']."3";
	            $ques_to_skip[]        =   $QI['QuestionID'];
	        
	            $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
	            $QI['ApplicationID']   =   $ApplicationID;
	            $QI['RequestID']       =   $RequestID;
	            $QI['OnboardFormID']   =   $OnboardFormID;
	            $QI['AnswerStatus']    =   1;
	             
	            $OnboardFormDataObj->insUpdOnboardFormData($QI);
	        }
            else if($QI['QuestionTypeID'] == 9) {
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
                	        
                $fd_skills_info        =   $QI['Answer'];
                foreach($fd_skills_info as $fd_skill_key=>$fd_skill_value) {
                    $fd_skill_keys     =   array_keys($fd_skill_value);
                    $ques_to_skip[]    =   $fd_skill_keys[0];
                    $ques_to_skip[]    =   $fd_skill_keys[1];
                    $ques_to_skip[]    =   $fd_skill_keys[2];
                }
            
                $ques_to_skip[]         =   $QI['QuestionID'];
                $OnboardFormDataObj->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 18) {
                //After the complete upgrade have to remove this.
                $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
            
                for($c18 = 1; $c18 <= $cnt; $c18++) {
                    $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
                }
                $ques_to_skip[]        =   $QI['QuestionID'];
                $ques_to_skip[]        =   $QI['QuestionID']."cnt";
                 
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
            
                $OnboardFormDataObj->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 1818) {
                //After the complete upgrade have to remove this.
                $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
            
                for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
                    $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c1818;
                }
                $ques_to_skip[]        =   $QI['QuestionID'];
                $ques_to_skip[]        =   $QI['QuestionID']."cnt";
                 
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
            
                $OnboardFormDataObj->insUpdOnboardFormData($QI);
            }
	    }
	    
	    //Questions to skip
	    $ques_to_skip[]    =   'process';
	    $ques_to_skip[]    =   'process_onboard';
	    $ques_to_skip[]    =   'action';

        // Recording last update to data table
        if ($_REQUEST['onboard'] == 'No') {
            $OnboardStatus    =   "hold";
        } else {
            $OnboardStatus    =   "ready";
        }
        
        //Get Applicant MultiOrgID
        $AppMultiOrgID     =   $ApplicantDetailsObj->getMultiOrgID($OrgID, $ApplicationID, $RequestID);
        //Set Onboard Header Information
        $onboard_info       =   array(
            "OrgID"         =>  $OrgID,
            "MultiOrgID"    =>  $AppMultiOrgID,
            "ApplicationID" =>  $ApplicationID,
            "RequestID"     =>  $RequestID,
            "Status"        =>  $OnboardStatus,
            "ProcessDate"   =>  "NOW()",
            "OnboardFormID" =>  $OnboardFormID,
        );
        $on_update    =   " ON DUPLICATE KEY UPDATE Status = :UStatus, ProcessDate = NOW()";
        $update_info  =   array(":UStatus" =>  $OnboardStatus);
        $OnboardApplicationsObj->insOnboardApplication($onboard_info, $on_update, $update_info);
                        	    
		// dynamic update
		foreach ( $_POST as $QuestionID => $value ) {
		    if(!in_array($QuestionID, $ques_to_skip)) {
		        if($QuestionID == "Sage_EmployeeNumber") {
		            // Insert and update applicant data
		            $QI  =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "OnboardFormID"=>$OnboardFormID, "QuestionID"=>$QuestionID, "Answer"=>$value);
		            $OnboardFormDataObj->insUpdOnboardFormData($QI);
		        }
		        else {
		            // Insert information
		            $QI = array ("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "OnboardFormID"=>$OnboardFormID, "QuestionID"=>$QuestionID, "Answer"=>$value);
		            // Insert and update applicant data
		            $OnboardFormDataObj->insUpdOnboardFormData($QI);
		        }
		    }
		} // end foreach

		if (($_REQUEST['onboard'] == 'No') && ($_REQUEST['onboard'] != $current_onboard_setting)) {
				
			//Set Update Information
			$set_info   =   array("LastModified = NOW()");
			//Set where condition
			$where       =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
			//Set parameters
			$params      =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
			//Update JobApplications Information
			$ApplicationsObj->updApplicationsInfo('JobApplications', $set_info, $where, array($params));
				
			$Comment     =   'Set applicant NOT to onboard. - Data Manager';
				
			//Insert JobApplication History
			$job_app_history = array (
					"OrgID"            =>  $OrgID,
					"ApplicationID"    =>  $ApplicationID,
					"RequestID"        =>  $RequestID,
					"ProcessOrder"     =>  "-2",
					"Date"             =>  "NOW()",
					"UserID"           =>  $USERID,
					"Comments"         =>  $Comment
			);
			G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );
				
			$message .= "<br>" . 'Applicant has been set to NOT onboard.';
		} // end onboard = No

		if (($_REQUEST['onboard'] == 'Yes') && ($_REQUEST['onboard'] != $current_onboard_setting)) {
		    $Comment = 'Set applicant to onboard. - Data Manager';
		    include IRECRUIT_DIR . 'applicants/SetOnBoardUserData.inc';
		    $message .= "<br>";
		    $message .= $DMStatusTitles ['downloaded'];
		} // end onboard = Yes

	} // end else ERROR
} // end if process
?>
