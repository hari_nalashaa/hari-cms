<?php
function validateDate($date, $format = 'Y-m-d H:i:s')
{
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) == $date;
}

if (isset($_REQUEST['process']) 
	&& $_REQUEST['process'] == 'Y'
	&& $_REQUEST['process_onboard'] == 'YES') {

    $ERROR              =   '';
    $REQUIRED           =   array ();
    $REQUIREDQUESTION   =   array ();

    $OnboardFormID      =   $_REQUEST['OnboardSource'];
    
    $columns            =   "QuestionID, Question";
    // Set where condition
    $where              =   array (
                    			"OrgID           =   :OrgID",
                    			"OnboardFormID   =   :OnboardFormID",
                    			"Required        =   'Y'"
                        	);
    // Set parameters
    $params             =   array (":OrgID" => $OrgID, ":OnboardFormID"=>$OnboardFormID);
    $results            =   G::Obj('OnboardQuestions')->getOnboardQuestionsInformation($columns, $where, "", array ($params) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $OQN ) {
			$REQUIRED [] = $OQN ['QuestionID'];
			$REQUIREDQUESTION [$OQN ['QuestionID']] = $OQN ['Question'];
		}
	}

	//Validate the information
	$ValidateOnboardFormObj->FORMDATA['REQUEST']   =   $_REQUEST;
	$ValidateOnboardFormObj->FORMDATA['FILES']     =   $_FILES;
	$ERRORS =  G::Obj('ValidateOnboardForm')->validateOnboardForm($OrgID, $OnboardFormID);

	foreach($ERRORS as $ErrorQueID=>$ErrorQuestion) {
	    $ERROR .= " - " . $ErrorQuestion . ".<br>";
	}

	if($feature ['MatrixCare'] == "Y" && $_REQUEST['OnboardSource'] == "MatrixCare") {
	    if ($_POST ['ddlMatrixCareCompaniesList'] == "") {
	        $ERROR .= " - Please select matrix care company id.<br>";
	    }
	    if ($_POST ['matrixcare_OfficeID'] == "") {
	        $ERROR .= " - Please select matrix care office id.<br>";
	    }
	}
	
	if ($ERROR) {

		$message = '<br>Required fields are not present.' . "<br>";
		$message .= $ERROR;

	} else { // we have good input

	    /**
	     * This is a temporary code until the final upgrade.
	     * Upto that time, we have to keep on adding different conditions to it.
	     */
	    
	    $message = '';
	    $GetFormPostAnswerObj->POST =   $_POST;    //Set Post Data
	    
	    $form_que_list         =   G::Obj('OnboardQuestions')->getOnboardQuestionsList($OrgID, $OnboardFormID);
	     
	    $ques_to_skip          =   array();
	    foreach($form_que_list as $QuestionID=>$QuestionInfo) {
	    
            $QI                =   $QuestionInfo;
            	
            if($QI['QuestionTypeID'] == 13) {
                //After the complete upgrade have to remove this.
                $ques_to_skip[]        =   $QI['QuestionID']."1";
                $ques_to_skip[]        =   $QI['QuestionID']."2";
                $ques_to_skip[]        =   $QI['QuestionID']."3";
                 
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
                
                G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 14) {
                //After the complete upgrade have to remove this.
                $ques_to_skip[]        =   $QI['QuestionID']."1";
                $ques_to_skip[]        =   $QI['QuestionID']."2";
                $ques_to_skip[]        =   $QI['QuestionID']."3";
                $ques_to_skip[]        =   $QI['QuestionID']."ext";
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
                
                G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 15) {
                //After the complete upgrade have to remove this.
                $ques_to_skip[]        =   $QI['QuestionID']."1";
                $ques_to_skip[]        =   $QI['QuestionID']."2";
                $ques_to_skip[]        =   $QI['QuestionID']."3";
                $ques_to_skip[]        =   $QI['QuestionID'];
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
                 
                G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 9) {
                 
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
            
                $fd_skills_info        =   $QI['Answer'];
                foreach($fd_skills_info as $fd_skill_key=>$fd_skill_value) {
                    $fd_skill_keys     =   array_keys($fd_skill_value);
                    $ques_to_skip[]    =   $fd_skill_keys[0];
                    $ques_to_skip[]    =   $fd_skill_keys[1];
                    $ques_to_skip[]    =   $fd_skill_keys[2];
                }
                 
                $ques_to_skip[]         =   $QI['QuestionID'];
                G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 18) {
                //After the complete upgrade have to remove this.
                $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
            
                for($c18 = 1; $c18 <= $cnt; $c18++) {
                    $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
                }
                $ques_to_skip[]        =   $QI['QuestionID'];
                $ques_to_skip[]        =   $QI['QuestionID']."cnt";
                 
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
            
                G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
            }
            else if($QI['QuestionTypeID'] == 1818) {
                //After the complete upgrade have to remove this.
                $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
            
                for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
                    $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c1818;
                }
                $ques_to_skip[]        =   $QI['QuestionID'];
                $ques_to_skip[]        =   $QI['QuestionID']."cnt";
                 
            
                $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
                $QI['ApplicationID']   =   $ApplicationID;
                $QI['RequestID']       =   $RequestID;
                $QI['OnboardFormID']   =   $OnboardFormID;
                $QI['AnswerStatus']    =   1;
            
                G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
            }
	    }

	    //Questions to skip
	    $ques_to_skip[]    =   'process';
	    $ques_to_skip[]    =   'process_onboard';
	    $ques_to_skip[]    =   'action';

        // Recording last update to data table
        $OnboardStatus     =   "transferred";
        //Get Applicant MultiOrgID
        $AppMultiOrgID     =   G::Obj('ApplicantDetails')->getMultiOrgID($OrgID, $ApplicationID, $RequestID);
        //Set Onboard Header Information
        $onboard_info       =   array(
            "OrgID"         =>  $OrgID,
            "MultiOrgID"    =>  $AppMultiOrgID,
            "ApplicationID" =>  $ApplicationID,
            "RequestID"     =>  $RequestID,
            "Status"        =>  $OnboardStatus,
            "ProcessDate"   =>  "NOW()",
            "OnboardFormID" =>  $OnboardFormID,
        );
        $on_update    =   " ON DUPLICATE KEY UPDATE Status = :UStatus, ProcessDate = NOW()";
        $update_info  =   array(":UStatus" =>  $OnboardStatus);
        G::Obj('OnboardApplications')->insOnboardApplication($onboard_info, $on_update, $update_info);
                        
		// dynamic update
		foreach ( $_POST as $QuestionID => $value ) {
		    if(!in_array($QuestionID, $ques_to_skip)) {
		        if($QuestionID == "Sage_EmployeeNumber") {
		            // Insert and update applicant data
		            $QI  =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "OnboardFormID"=>$OnboardFormID, "QuestionID"=>$QuestionID, "Answer"=>$value);
		            G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
		        }
		        else {
		            // Insert information
		            $QI = array ("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "OnboardFormID"=>$OnboardFormID, "QuestionID"=>$QuestionID, "Answer"=>$value);
		            // Insert and update applicant data
		            G::Obj('OnboardFormData')->insUpdOnboardFormData($QI);
		        }
		    }
		} // end foreach

				
        if ($_REQUEST['OnboardSource'] == "MatrixCare" && $feature ['MatrixCare'] == "Y") {
                
                $matrixcare_applicant = true;
                
                // Send employee to matrixcare api
                include_once IRECRUIT_DIR . 'onboard/AddEmployeeToMatrixCare.inc';
                
                if (isset($matrixcaregiver_info ['id']) &&  $matrixcaregiver_info ['id'] != "") {
                    
                    $Comment = 'Sent applicant to MatrixCare';
                    include IRECRUIT_DIR . 'applicants/SetOnBoardUserData.inc';
                    
                    if(count($contact_ids_list) == 3) {
                        $message .= "<br>";
                        $message .= 'Transferred to MatrixCare';
                    }
                    else {
                        $message .= "<br>";
                        $message .= 'Transferred to MatrixCare<br>';
                        $message .= 'Unable to process your emergency contacts information to matrix care. Please update by using Update button.';
                    }
                    
                } else {
                    $message .= "Sorry unable to onboard this applicant to MatrixCare";
                }
        }			

	} // end else ERROR
} // end if process
?>
