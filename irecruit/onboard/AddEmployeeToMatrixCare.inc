<?php
$params_web_form_id                                 =   array(":OrgID"=>$OrgID,":orgid"=>$OrgID);
$sel_web_form_id                                    =   "SELECT WebFormID FROM WebForms WHERE OrgID = :OrgID and WebFormID IN (
                                                        SELECT DISTINCT(WebFormID) FROM WebFormQuestions WHERE OrgID = :orgid AND FormID = 'emergencycontact') and FormStatus = 'Active' LIMIT 1";
$res_web_form_id                                    =   G::Obj('GenericQueries')->getRowInfoByQuery($sel_web_form_id, array($params_web_form_id));

$params_que_info                                    =   array(":OrgID"=>$OrgID, ":WebFormID"=>$res_web_form_id['WebFormID']);
$where_que_info                                     =   array("OrgID = :OrgID", "Required = 'Y'", "WebFormID = :WebFormID");
$res_que_results                                    =   G::Obj('FormQuestions')->getQuestionsInformation("WebFormQuestions", "QuestionID, Required", $where_que_info, "", array($params_que_info));
$res_web_que_results                                =   $res_que_results['results'];
$res_web_que_count                                  =   $res_que_results['count'];

$web_form_data_contact_map["ecaddress1"]            =   "address1";
$web_form_data_contact_map["ecaddress21"]           =   "address2";
$web_form_data_contact_map["ecaddress2"]            =   "address1";
$web_form_data_contact_map["ecaddress22"]           =   "address2";
$web_form_data_contact_map["ecaddress3"]            =   "address1";
$web_form_data_contact_map["ecaddress23"]           =   "address2";

$web_form_data_contact_map["eccity1"]               =   "city";
$web_form_data_contact_map["eccity2"]               =   "city";
$web_form_data_contact_map["eccity3"]               =   "city";

$web_form_data_contact_map["eccountry1"]            =   "country";
$web_form_data_contact_map["eccountry2"]            =   "country";
$web_form_data_contact_map["eccountry3"]            =   "country";

$web_form_data_contact_map["eccounty1"]             =   "county";
$web_form_data_contact_map["eccounty2"]             =   "county";
$web_form_data_contact_map["eccounty3"]             =   "county";

$web_form_data_contact_map["ecemailaddress1"]       =   "email";
$web_form_data_contact_map["ecemailaddress2"]       =   "email";
$web_form_data_contact_map["ecemailaddress3"]       =   "email";

$web_form_data_contact_map["eclname1"]              =   "lastName";
$web_form_data_contact_map["eclname2"]              =   "lastName";
$web_form_data_contact_map["eclname3"]              =   "lastName";

$web_form_data_contact_map["ecname1"]               =   "firstName";
$web_form_data_contact_map["ecname2"]               =   "firstName";
$web_form_data_contact_map["ecname3"]               =   "firstName";

$web_form_data_contact_map["ecphone1a"]             =   "phone1";
$web_form_data_contact_map["ecphone1atype"]         =   "phone1Type";
$web_form_data_contact_map["ecphone1b"]             =   "phone1";
$web_form_data_contact_map["ecphone1btype"]         =   "phone1Type";
$web_form_data_contact_map["ecphone1c"]             =   "phone1";
$web_form_data_contact_map["ecphone1ctype"]         =   "phone1Type";

$web_form_data_contact_map["ecphone2a"]             =   "phone2";
$web_form_data_contact_map["ecphone2atype"]         =   "phone2Type";
$web_form_data_contact_map["ecphone2b"]             =   "phone2";
$web_form_data_contact_map["ecphone2btype"]         =   "phone2Type";
$web_form_data_contact_map["ecphone2c"]             =   "phone2";
$web_form_data_contact_map["ecphone2ctype"]         =   "phone2Type";

$web_form_data_contact_map["ecrelationship1"]       =   "relation";
$web_form_data_contact_map["ecrelationship2"]       =   "relation";
$web_form_data_contact_map["ecrelationship3"]       =   "relation";

$web_form_data_contact_map["ecstate1"]              =   "state";
$web_form_data_contact_map["ecstate2"]              =   "state";
$web_form_data_contact_map["ecstate3"]              =   "state";

$web_form_data_contact_map["eczipcode1"]            =   "postalCode";
$web_form_data_contact_map["eczipcode2"]            =   "postalCode";
$web_form_data_contact_map["eczipcode3"]            =   "postalCode";

//Get matrixcare countries information
$matrixcare_countries                               =   G::Obj('MatrixCare')->getMatrixCareCountries();
//Get matrixcare matital status
$matrixcare_maritalstatus                           =   G::Obj('MatrixCare')->getMatrixCareMaritalStatus();
//Get matrixcare race information
$matrixcare_raceinfo                                =   G::Obj('MatrixCare')->getMatrixCareRaceInformation();
//Get matrix care contact information
$matrixcare_contactinfo                             =   G::Obj('MatrixCare')->getMatrixCareContactInfo($OrgID, $RequestID, $ApplicationID);
$matrixcare_contact_results                         =   $matrixcare_contactinfo['results'];
$matrixcare_contact_count                           =   count($matrixcare_contactinfo['results']);

$input_data                                         =   array();

//Get I9 Form Data
$i9form_info  = G::Obj('ApplicantOnboardingInfo')->getI9FormData($OrgID, $ApplicationID, $RequestID);
//Get Internal Forms Assigned Status I9

$CURRENT = G::Obj('PreFilledFormData')->determineCurrentI9 ($OrgID, $RequestID, $ApplicationID);

$i9form_int_forms_assign = G::Obj('ApplicantOnboardingInfo')->getInternalFormsAssigned($OrgID, $ApplicationID, $RequestID, 'PreFilled', $CURRENT['I9']);

/**
 * @tutorial Check Internal Forms Assigned Status,
 * 			 based on the status assign values to input_data array
 * 			 I9 Form Status
 */
if(isset($i9form_int_forms_assign['Status']) && $i9form_int_forms_assign['Status'] == "4") {

    for($fi = 0; $fi < count($i9form_info['results']); $fi++) {
        if($i9form_info['results'][$fi]['QuestionID'] == "SSN") {
            $Answer15   =   ($i9form_info['results'][$fi]['Answer'] != "") ? json_decode($i9form_info['results'][$fi]['Answer'], true) : "";
            $SSN1       =   $Answer15[0];
            $SSN2       =   $Answer15[1];
            $SSN3       =   $Answer15[2];
        }
        if($i9form_info['results'][$fi]['QuestionID'] == "DOB") {
            $DOB = $i9form_info['results'][$fi]['Answer'];
        }
    }

    if($SSN1 != "" && $SSN2 != "" && $SSN3 != "") {
        $input_data["payroll"]["ssn"]   =   $SSN1.$SSN2.$SSN3;
    }

    if($DOB != "") {
        $input_data["payroll"]["birthdate"] = G::Obj('DateHelper')->getYmdFromMdy($DOB);
    }
}


$mc_que_ans = array();
for($mc = 0; $mc < $matrixcare_contact_count; $mc++) {
    $MQueID     =   $matrixcare_contact_results[$mc]['QuestionID'];
    $MAnswer    =   $matrixcare_contact_results[$mc]['Answer'];
    $MQueTypeID =   $matrixcare_contact_results[$mc]['QuestionTypeID'];
    
    if($MQueTypeID == 13) {
        $Answer13  =   ($MAnswer != "") ? json_decode($MAnswer, true) : "";
        
        $mc_que_ans[$MQueID."1"]    =   $Answer13[0];
        $mc_que_ans[$MQueID."2"]    =   $Answer13[1];
        $mc_que_ans[$MQueID."3"]    =   $Answer13[2];
    }
    else if($MQueTypeID == 14) {
        $Answer14  =   ($MAnswer != "") ? json_decode($MAnswer, true) : "";
    
        $mc_que_ans[$MQueID."1"]    =   $Answer14[0];
        $mc_que_ans[$MQueID."2"]    =   $Answer14[1];
        $mc_que_ans[$MQueID."3"]    =   $Answer14[2];
        $mc_que_ans[$MQueID."ext"]  =   $Answer14[3];
    }
    else {
        $mc_que_ans[$MQueID]        =   $MAnswer;
    }
}

$mc_contact_info            =   array();

$mci = 0;
for($mc = 0; $mc < $matrixcare_contact_count; $mc++) {

    $mc_question = $web_form_data_contact_map[$matrixcare_contact_results[$mc]['QuestionID']];
    
    if($mc % 15 == 0 && $mc > 14) {
        $mci++;
    }
    
    if($mc % 15 == 0) {
        $mc_contact_info[$mci]["typeId"] = 1;    	
    }
    
    if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1a") {
        $mc_contact_info[$mci]["phone1"] = "(".$mc_que_ans["ecphone1a1"].")".$mc_que_ans["ecphone1a2"]."-".$mc_que_ans["ecphone1a3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1b") {
        $mc_contact_info[$mci]["phone1"] = "(".$mc_que_ans["ecphone1b1"].")".$mc_que_ans["ecphone1b2"]."-".$mc_que_ans["ecphone1b3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1c") {
        $mc_contact_info[$mci]["phone1"] = "(".$mc_que_ans["ecphone1c1"].")".$mc_que_ans["ecphone1c2"]."-".$mc_que_ans["ecphone1c3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2a") {
        $mc_contact_info[$mci]["phone2"] = $mc_que_ans["ecphone2a1"].$mc_que_ans["ecphone2a2"].$mc_que_ans["ecphone2a3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2b") {
        $mc_contact_info[$mci]["phone2"] = $mc_que_ans["ecphone2b1"].$mc_que_ans["ecphone2b2"].$mc_que_ans["ecphone2b3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2c") {
        $mc_contact_info[$mci]["phone2"] = $mc_que_ans["ecphone2c1"].$mc_que_ans["ecphone2c2"].$mc_que_ans["ecphone2c3"];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1atype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1btype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone1ctype"                
            ) {
        $mc_contact_info[$mci]["phone1Type"] = $mc_que_ans[$matrixcare_contact_results[$mc]['QuestionID']];
    }
    else if($matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2atype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2btype"
            || $matrixcare_contact_results[$mc]['QuestionID'] == "ecphone2ctype"
            ) {
        $mc_contact_info[$mci]["phone2Type"] = $mc_que_ans[$matrixcare_contact_results[$mc]['QuestionID']];
    }
    else {
        if(trim($mc_question) != '') {
            $mc_contact_info[$mci][$mc_question] = $matrixcare_contact_results[$mc]['Answer'];
        }
    }
}


$input_data["firstName"]        =   $APPDATA['first'];
$input_data["lastName"]         =   $APPDATA['last'];
$input_data["middleName"]       =   substr($APPDATA['middle'], 0, 1);
if ($APPDATA['nickname'] != "") {
    $input_data["nickName"]     =   $APPDATA['nickname'];
} else {
    $input_data["nickName"]     =   $APPDATA['last'] . " " . $APPDATA['first'];
}
if ($OrgID == "I20180701") {
    $input_data["role"]         =   "3";
} else {
    $input_data["role"]         =   "1";
}
$input_data["statusId"]         =   "1";
$input_data["email1"]           =   $APPDATA['email'];
$input_data["email2"]           =   "";

if($APPDATA['aa_genderselection'] == 'F') {
    $input_data["genderId"]     =   "1";
}
else if($APPDATA['aa_genderselection'] == 'M') {
    $input_data["genderId"]     =   "2";
}

$phones = array();
if($APPDATA['homephone'] != "") {
    $Answer13   =   ($APPDATA['homephone'] != "") ? json_decode($APPDATA['homephone'], true) : "";
    if($Answer13[0].$Answer13[1].$Answer13[2] != "") {
        $phones[]   =   array("phone"=>$Answer13[0].$Answer13[1].$Answer13[2], "phoneTypeId"=>"2");
    }
}
if($APPDATA['cellphone'] != "") {
    $Answer13   =   ($APPDATA['cellphone'] != "") ? json_decode($APPDATA['cellphone'], true) : "";
    if($Answer13[0].$Answer13[1].$Answer13[2] != "") {
        $phones[]   =   array("phone"=>$Answer13[0].$Answer13[1].$Answer13[2], "phoneTypeId"=>"4");
    }
}

$input_data["address"]          =   array(
                                        "address1"      =>  $APPDATA['address'],
                                        "address2"      =>  $APPDATA['address2'],
                                        "city"          =>  $APPDATA['city'],
                                        "state"         =>  $APPDATA['state'],
                                        "postalCode"    =>  $APPDATA['zip'],
                                        "county"        =>  $APPDATA['county'],
                                        "country"       =>  "240"
                                    );

if($MatrixCareInfo['UpperCase'] == "Y") {
    foreach ($input_data as $input_data_key=>$input_data_value) {
        if(is_string($input_data_value)) {
            $input_data[$input_data_key] = strtoupper($input_data_value);
        }
        else if(is_array($input_data_value)) {
            foreach($input_data_value as $input_data_value_key=>$input_data_value_val) {
                $input_data[$input_data_key][$input_data_value_key] = strtoupper($input_data_value_val);
            }
        }
    }
}

if($matrixcare_info['access_token'] != "") {
    if($_REQUEST['matrixcare_OfficeID'] != "") {
        
        $headers_list                   =   array("access_token"=>$matrixcare_info['access_token'], "token_type"=>$matrixcare_info['token_type']);
        $matrixcaregiver_url            =   $MatrixCareObj->api_router_url.$matrixcare_info['tenant']."/".$_REQUEST['matrixcare_OfficeID']."/caregivers";
        $matrixcaregiver_info           =   $MatrixCareObj->setMatrixCareGiverInfo($matrixcaregiver_url, $input_data, $headers_list);
        $matrixcaregiver_info           =   json_decode($matrixcaregiver_info, true);
        
        $json_res_entities              =   $matrixcaregiver_info['entities'];
        
        for($jei = 0; $jei < count($json_res_entities); $jei++) {
            if(isset($json_res_entities[$jei]['fields'])) {
                $json_fields    =   $json_res_entities[$jei]['fields'];
                $fields_count   =   count($json_res_entities[$jei]['fields']);
        
                for($jfi = 0; $jfi < $fields_count; $jfi++) {
                    $message .= implode("<br>", $json_fields[$jfi]['errors'])."<br>";
                }
            }
            else if(!isset($json_res_entities[$jei]['fields'])) {
                $message .= $json_res_entities[$jei]['error']."<br>";
            }
        }
        
        $data  = $matrixcaregiver_url."-*-";
        $data .= json_encode($input_data)."-*-";
        $data .= json_encode($headers_list)."-*-";
        $data .= "\nResponse:".json_encode($matrixcaregiver_info);
        
        Logger::writeMessage(ROOT."logs/matrixcare/". $OrgID . "-" . date('Y-m-d-H') . "-addmatrixcare.txt", $data, "a+", false);
        Logger::writeMessageToDb($OrgID, "MatrixCare", $data);
        
        if($matrixcaregiver_info['id']) {
            $matrixcaregiver_phone_url  =   $MatrixCareObj->api_router_url.$matrixcare_info['tenant']."/caregivers/".$matrixcaregiver_info['id']."/phones";
            $matrixcaregiver_phones     =   $MatrixCareObj->setCaregiverPhones($matrixcaregiver_phone_url, $phones, $headers_list);
        
            $matrixcaregiver_phones_res =   json_decode($matrixcaregiver_phones, true);
            $json_res_entities          =   @$matrixcaregiver_phones_res['entities'];
            
            for($jei = 0; $jei < count($json_res_entities); $jei++) {
                if(isset($json_res_entities[$jei]['fields'])) {
                    $json_fields    =   $json_res_entities[$jei]['fields'];
                    $fields_count   =   count($json_res_entities[$jei]['fields']);
            
                    for($jfi = 0; $jfi < $fields_count; $jfi++) {
                        $message .= implode("<br>", $json_fields[$jfi]['errors'])."<br>";
                    }
                }
                else if(!isset($json_res_entities[$jei]['fields'])) {
                    $message .= $json_res_entities[$jei]['error']."<br>";
                }
            }
            
            $data  = $matrixcaregiver_phone_url."-*-";
            $data .= json_encode($phones)."-*-";
            $data .= json_encode($headers_list)."-*-";
            $data .= "\nResponse:".json_encode($matrixcaregiver_phones);
        
            Logger::writeMessage(ROOT."logs/matrixcare/". $OrgID . "-" . date('Y-m-d-H') . "-addphones.txt", $data, "a+", false);
            Logger::writeMessageToDb($OrgID, "MatrixCare", $data);
        }

        if($res_web_que_count > 0) {
            $contact_ids_list = array();

            if($matrixcaregiver_info['id']) {
                for($k = 0, $c = 1; $k <= $mci; $k++, $c++) {
                    $mc_contact_info[$k]['country'] = 240;
                    $contact_data = $mc_contact_info[$k];
                    
                    if(($contact_data['firstName'] && $contact_data['lastName']) && ($contact_data['phone1'] != "" || $contact_data['phone2'] != "")) {

                        $matrixcaregiver_url        =   $MatrixCareObj->api_router_url.$matrixcare_info['tenant']."/"."caregivers/".$matrixcaregiver_info['id']."/contacts";
                        $matrixcaregiver_contacts   =   $MatrixCareObj->setMatrixCareContactInfo($matrixcaregiver_url, $contact_data, $headers_list);
                        $matrixcaregiver_contacts   =   json_decode($matrixcaregiver_contacts, true);
                        
                        $json_res_entities          =   $matrixcaregiver_contacts['entities'];
                        
                        for($jei = 0; $jei < count($json_res_entities); $jei++) {
                            if(isset($json_res_entities[$jei]['fields'])) {
                                $json_fields    =   $json_res_entities[$jei]['fields'];
                                $fields_count   =   count($json_res_entities[$jei]['fields']);
                        
                                for($jfi = 0; $jfi < $fields_count; $jfi++) {
                                    $message    .=  implode("<br>", $json_fields[$jfi]['errors'])."<br>";
                                }
                            }
                            else if(!isset($json_res_entities[$jei]['fields'])) {
                                $message .= $json_res_entities[$jei]['error']."<br>";
                            }
                        }
                        
                        
                        $data  = $matrixcaregiver_url."-*-";
                        $data .= json_encode($contact_data)."-*-";
                        $data .= json_encode($headers_list)."-*-";
                        $data .= "\nResponse:".json_encode($matrixcaregiver_contacts);
                         
                        $contact_ids_list["C".$c] = $matrixcaregiver_contacts['id'];
                        
                        Logger::writeMessage(ROOT."logs/matrixcare/". $OrgID . "-" . date('Y-m-d-H') . "-addmatrixcare.txt", $data, "a+", false);
                        Logger::writeMessageToDb($OrgID, "MatrixCare", $data);
                    }
                }
            }
            
            $params_info    = array(":MatrixCaregiverContactStatus"=>"1", ":MatrixCaregiverContacts"=>json_encode($contact_ids_list), ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
            $set_info       = array("MatrixCaregiverContactStatus = :MatrixCaregiverContactStatus", "MatrixCaregiverContacts = :MatrixCaregiverContacts");
            $where_info     = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
            $ApplicationsObj->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params_info));
        }
        
    }
}
else {
    $headers_list                   =   array("access_token"=>$matrixcare_info['access_token'], "token_type"=>$matrixcare_info['token_type']);
    $matrixcaregiver_url            =   $MatrixCareObj->api_router_url.$matrixcare_info['tenant']."/".$_REQUEST['matrixcare_OfficeID']."/caregivers";
    $matrixcaregiver_info           =   $MatrixCareObj->setMatrixCareGiverInfo($matrixcaregiver_url, $input_data, $headers_list);
    $matrixcaregiver_info           =   json_decode($matrixcaregiver_info, true);
    
    $json_res_entities              =   $matrixcaregiver_info['entities'];
    
    for($jei = 0; $jei < count($json_res_entities); $jei++) {
        if(isset($json_res_entities[$jei]['fields'])) {
            $json_fields    =   $json_res_entities[$jei]['fields'];
            $fields_count   =   count($json_res_entities[$jei]['fields']);
    
            for($jfi = 0; $jfi < $fields_count; $jfi++) {
                $message .= implode("<br>", $json_fields[$jfi]['errors'])."<br>";
            }
        }
        else if(!isset($json_res_entities[$jei]['fields'])) {
            $message .= $json_res_entities[$jei]['error']."<br>";
        }
    }
    
    $data  = $matrixcaregiver_url."-*-";
    $data .= json_encode($input_data)."-*-";
    $data .= json_encode($headers_list)."-*-";
    $data .= "\nResponse:".json_encode($matrixcaregiver_info);
    
    Logger::writeMessage(ROOT."logs/matrixcare/". $OrgID . "-" . date('Y-m-d-H') . "-failmatrixcare.txt", $data, "a+", true);
    Logger::writeMessageToDb($OrgID, "MatrixCare", $data);
}
?>