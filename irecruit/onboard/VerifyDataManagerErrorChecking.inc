<?php
global $GenericLibraryObj;
if ($ierror == "Y") {
    $ERROR .= "<u>" . $VALIDATE [$QuestionID] ['Section'] . "</u> - ";
    $ERROR .= $VALIDATE [$QuestionID] ['Question'] . "&nbsp;&nbsp;<b>";
    
    if($VALIDATE [$QuestionID] ['QuestionTypeID'] == 13) {

        $value  =   $APPDATA[$QuestionID];
        if($GenericLibraryObj->isJSON($value) === true) {
            $Answer13  =   ($value != "") ? json_decode($value, true) : "";
            $ERROR .= $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $Answer13[0], $Answer13[1], $Answer13[2], '' );
        }
        else if(is_string($APPDATA [$QuestionID]) && $APPDATA [$QuestionID] != "") {
            $ERROR .= $value;
        }
        
    }
    else if($VALIDATE [$QuestionID] ['QuestionTypeID'] == 14) {
    
        $value  =   $APPDATA[$QuestionID];
        if($GenericLibraryObj->isJSON($value) === true) {
            $Answer14  =   ($value != "") ? json_decode($value, true) : "";
            $ERROR .= $AddressObj->formatPhone ( $OrgID, $APPDATA ['country'], $Answer14[0], $Answer14[1], $Answer14[2], $Answer14[3] );
        }
        else if(is_string($APPDATA [$QuestionID]) && $APPDATA [$QuestionID] != "") {
            $ERROR .= $value;
        }
    
    }
    else if($VALIDATE [$QuestionID] ['QuestionTypeID'] == 15) {
    
        $value  =   $APPDATA[$QuestionID];
        if($GenericLibraryObj->isJSON($value) === true) {
            $Answer15  =   ($value != "") ? json_decode($value, true) : "";
            $ERROR .= $Answer15[0].'-'.$Answer15[1].'-'.$Answer15[2];
        }
        else if(is_string($APPDATA [$QuestionID]) && $APPDATA [$QuestionID] != "") {
            $ERROR .= $value;
        }
        
    }
    else {
        $value  =   $APPDATA[$QuestionID];
        $ERROR .=   $value;
    }
        
    $ERROR .= "</b><br><br>";
    $i ++;
    if ($VALIDATE [$QuestionID] ['Section'] == "Form Question") {
        $iFQ ++;
    }
    if ($VALIDATE [$QuestionID] ['Section'] == "Affirmative Action Question") {
        $iAA ++;
    }
    if ($VALIDATE [$QuestionID] ['Section'] == "Onboard Question") {
        $iOB ++;
    }
}
?>