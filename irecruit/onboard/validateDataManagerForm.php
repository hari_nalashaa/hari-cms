<?php
require_once '../Configuration.inc';

//Set page title
$ApplicationID  =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$RequestID      =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

if (($OrgID) && ($ApplicationID) && ($RequestID)) {

    include IRECRUIT_DIR . 'onboard/ValidateDataManagerForm.inc';

    echo '<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">';
    echo '<tr><td>';

    if ($i > 0) {
        echo 'The following fields have data that should be reviewed.<br><br>';
        echo $ERROR;
        echo '<table border="0" cellspacing="0" cellpadding="0" width="300" align="center">';
        echo '<tr><td>';
        if ($iFQ > 0) {
            echo '<a href="javascript:void(0);" onClick=\'setTab("#edit-application");\'><img src="' . IRECRUIT_HOME . '/images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Edit Application</b></a><br>';
        }
        if ($iOB > 0) {
            echo '<a href="javascript:void(0);" onClick=\'setTab("#onboard-applicant");\'><img src="' . IRECRUIT_HOME . '/images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Edit Onboard Questions</b></a><br>';
        }
        if ($iAA > 0) {
            echo '<a href="javascript:void(0);" onClick="getAffirmativeActionForm();"><img src="' . IRECRUIT_HOME . '/images/icons/pencil.png" border="0" title="Edit" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Edit Affirmative Action Questions</b></a><br>';
        }
        echo '</td></tr>';
        echo '</table>';
    } else {
        echo '<br>All fields for this applicant are ok.';
    }

    echo '</td></tr>';
    echo '</table>';
} else { // end if OrgID, RequestID, ApplicationID

    echo '<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered">';
    echo '<tr><td>';
    echo '<br><font style="color:red">This page loaded in error.</font>';
    echo '</td></tr>';
    echo '</table>';
}
?>