<?php
require_once '../Configuration.inc';

$ApplicationID = isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$RequestID = isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';

$FORMDATA = G::Obj('ExporterDataManager')->getExportData($OrgID, $ApplicationID, $RequestID, "60b644e94aba6", "", "N", "");
$GAFORMDATA = $FORMDATA[0];

// Create Directories
$dir = IRECRUIT_DIR . 'vault/' . $OrgID;
if (! file_exists($dir)) {
   mkdir($dir, 0700);
   chmod($dir, 0777);
}

$dir .= "/applicantvault";
if (! file_exists($dir)) {
   mkdir($dir, 0700);
   chmod($dir, 0777);
}

$dir .= "/internalforms";
if (! file_exists($dir)) {
   mkdir($dir, 0700);
   chmod($dir, 0777);
}

// PDF Form
$pdfform = IRECRUIT_DIR . 'formsInternal/PDFForms/Georgia/GA_New_Hire_Form.pdf';

// Temp file for merged data
$txfdffile = $dir . '/' . $ApplicationID . '_GA_New_Hire_Form.xfdf';

// Applicants Output file
$filename = $ApplicationID . '-' . $RequestID . '-GA_New_Hire_Form.pdf';
$outputpdf = $dir . '/' . $filename;
//$linktopdf = IRECRUIT_HOME . 'vault/' . $OrgID . '/applicantvault/internalforms/' . $filename;

	$XFDF = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$XFDF .= "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n";
	$XFDF .= "<f href=\"GA_New_Hire_Form.pdf\"/>\n";
	$XFDF .= "<fields>\n";

                $XFDF .= "<field name=\"FEIN\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['FEIN']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Employer_Name\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Employer_Name']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Employer_Address\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Employer_Address']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Employer_City\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Employer_City']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Employer_State\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Employer_State']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Employer_Zip\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Employer_Zip']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Contact\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Contact']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Employer_Phone\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Employer_Phone']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Contact_Email\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Contact_Email']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"SSN\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['SSN']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"FirstName\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['FirstName']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"LastName\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['LastName']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"MiddleName\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['MiddleName']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Address1\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Address1']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Address2\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Address2']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"City\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['City']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"State\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['State']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"ZipCode\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['ZipCode']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Start_Date\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Start_Date']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"DOB\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['DOB']) . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"MedicalInsuranceYes\">";
                $XFDF .= "<value>" . $GAFORMDATA['Medical_Insurance'] . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"MedicalInsuranceNo\">";
                $XFDF .= "<value>" . $GAFORMDATA['Medical_Insurance'] . "</value>";
                $XFDF .= "</field>\n";

                $XFDF .= "<field name=\"Medical_Provider\">";
                $XFDF .= "<value>" . strtoupper($GAFORMDATA['Medical_Provider']) . "</value>";
                $XFDF .= "</field>\n";

        $XFDF .= "</fields>\n";
        $XFDF .= "</xfdf>\n";

// Write temp data file
$fh = fopen($txfdffile, 'w');
if (! $fh) {
     die('Cannot open file ' . $txfdffile);
}
fwrite($fh, $XFDF);
fclose($fh);

// Merge temp file with PDF and write to applicants file
$command = 'pdftk ' . escapeshellarg($pdfform) . ' fill_form ' . escapeshellarg($txfdffile) . ' output ' . escapeshellarg($outputpdf);

system($command);
// remove temp file
system('rm ' . escapeshellarg($txfdffile));
@chmod($outputpdf, 0666);

/*
header("Location:" . $linktopdf);
*/

// Used for triggering download instead of display
if (file_exists ( $outputpdf )) {
                $browser = get_browser( null, true );


                if ($browser['browser'] == "IE") {

                        header ( 'Content-Description: File Transfer' );
                        header ( 'Content-Type: application/octet-stream' );
                        header ( 'Content-Disposition: inline; filename="' . $filename . '"' );
                        header ( 'Content-Transfer-Encoding: binary' );
                        header ( 'Expires: 0' );
                        header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
                        header ( 'Pragma: public' );
                        header ( 'Content-Length: ' . filesize ( $outputpdf) );
                        ob_clean();
                        flush();
                        readfile ( $outputpdf );
                        exit ();
                } else {
                        header ( 'Content-Description: File Transfer' );
                        header ( 'Content-Type: application/octet-stream' );
                        header ( 'Content-Disposition: attachment; filename=' . basename ( $filename ) );
                        header ( 'Content-Transfer-Encoding: binary' );
                        header ( 'Expires: 0' );
                        header ( 'Cache-Control:' );
                        header ( 'Pragma: cache' );
                        header ( 'Content-Length: ' . filesize ( $outputpdf ) );
                        readfile ( $outputpdf );
                        exit ();
                }
}


?>
