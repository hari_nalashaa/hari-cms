<?php 
G::Obj('ApplicationForm')->FORM_SOURCE	=	"ONBOARD";

//Get default companies information on matrix care, cloud payroll
$def_matrixcare_comp_info		=   $MatrixCareObj->getDefaultCompanyInfo($OrgID);

// Get the cloud application details again after processing
$cloud_application_info			=   $ApplicationsObj->getCloudApplicationDetails ( $ApplicationID, $RequestID, $OrgID );
$APPDATA						=   $ApplicantsObj->getAppData ( $OrgID, $ApplicationID );
$OnboardFormData				=   $OnboardFormDataObj->getOnboardFormData($OrgID, $ApplicationID, $RequestID, $_REQUEST['OnboardSource']);
$OnboardDataCount				=	count($OnboardFormData);

//Get Requisition FormID
$CurrentRequisitionFormID		=	G::Obj('RequisitionDetails')->getRequisitionFormID($OrgID, $RequestID);
//Get common questions list
$common_req_onboard_ques		=	G::Obj('OnboardQuestions')->getOnboardAndRequisitionCommonQuestionsList($OrgID, $_REQUEST['OnboardSource'], $CurrentRequisitionFormID);
$common_req_onboard_ques_keys	=	array_keys($common_req_onboard_ques);

//Set APPDATA
$REQ_APPDATA					=	G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);
$REQ_DEF_DATA					=	array();

$req_form_details				=	G::Obj('Requisitions')->getRequisitionsDetailInfo("RequisitionFormID", $OrgID, $RequestID);
$additional_req_ques			=	G::Obj('RequisitionQuestions')->getAdditionalRequisitionFields($OrgID, $req_form_details['RequisitionFormID']);

//Get the requisition questions default value data
foreach ($additional_req_ques as $additional_req_que_id) {
	$req_que_info_detail		=	G::Obj('RequisitionQuestions')->getRequisitionQuestionInfo("defaultValue", $OrgID, $additional_req_que_id, $req_form_details['RequisitionFormID']);
	$REQ_DEF_DATA[$additional_req_que_id]	=	$req_que_info_detail['defaultValue'];
}

G::Obj('ApplicationForm')->REQ_DEF_DATA	= $REQ_DEF_DATA;
G::Obj('ApplicationForm')->OnboardDataCount	= $OnboardDataCount;

//Set Requisition data to app data
foreach ($REQ_APPDATA as $REQ_APPDATA_QUEID=>$REQ_APPDATA_ANS) {
	if(!isset($APPDATA[$REQ_APPDATA_QUEID]) && in_array($REQ_APPDATA_QUEID, $common_req_onboard_ques_keys)) {
		$APPDATA[$REQ_APPDATA_QUEID] = $REQ_APPDATA_ANS;
	}
}

//Add the onboard data to APPDATA variable //Overwrite the Requisition data with onboard data
foreach ($OnboardFormData as $OnboardFormKey=>$OnboardFormValue) {
	$APPDATA[$OnboardFormKey]   =   $OnboardFormValue;
}

$hired_status_info  	=   G::Obj('GenericQueries')->getRowInfo("ApplicantProcessFlow", "getHiredStatusProcessOrder('$OrgID') as HiredProcessOrder");
$hired_status       	=   $hired_status_info['HiredProcessOrder'];
$process_order			=	G::Obj('ApplicantDetails')->getProcessOrder($OrgID, $ApplicationID, $RequestID);
$effective_date_info	=	G::Obj('GenericQueries')->getRowInfoByQuery("SELECT getProcessOrderStatusEffectiveDate('$OrgID', '$RequestID', '$ApplicationID', '$process_order') as EffectiveDate");

if($hired_status == $process_order)
{	
	$APPDATA['Sage_HireDate']	=	date('m/d/Y', strtotime($effective_date_info['EffectiveDate']));
}

//Get Requisition FormID
$CurrentRequisitionFormID		=	G::Obj('RequisitionDetails')->getRequisitionFormID($OrgID, $RequestID);
//Get common questions list
$common_req_onboard_ques		=	G::Obj('OnboardQuestions')->getOnboardAndRequisitionCommonQuestionsList($OrgID, $_REQUEST['OnboardSource'], $CurrentRequisitionFormID);
$common_req_onboard_ques_keys	=	array_keys($common_req_onboard_ques);

//Set APPDATA
$REQ_APPDATA	=	G::Obj('RequisitionsData')->getRequisitionsData($OrgID, $RequestID);

foreach ($REQ_APPDATA as $REQ_APPDATA_QUEID=>$REQ_APPDATA_ANS) {
	if(!isset($APPDATA[$REQ_APPDATA_QUEID]) && in_array($REQ_APPDATA_QUEID, $common_req_onboard_ques_keys)) {
		$APPDATA[$REQ_APPDATA_QUEID] = $REQ_APPDATA_ANS;
	}
}

//Set for wotc form
if($_REQUEST['OnboardSource'] == "WOTC") {
	$wotc_processing_info	=	G::Obj('WOTCProcessing')->getProcessingInfo("*", $wotcid_info['wotcID'], $WotcApplicationID);
	
	$APPDATA['OfferDate']		=	G::Obj('DateHelper')->getMdyFromYmd($wotc_processing_info['OfferDate']);
	$APPDATA['HiredDate']     	=	G::Obj('DateHelper')->getMdyFromYmd($wotc_processing_info['HiredDate']);
	$APPDATA['StartDate']     	=	G::Obj('DateHelper')->getMdyFromYmd($wotc_processing_info['StartDate']);
	$APPDATA['StartingWage']  	=	$wotc_processing_info['StartingWage'];
	$APPDATA['Position']      	=	$wotc_processing_info['Position'];
}

//Loop post values
if(isset($_POST) && count($_POST) > 0) {
    foreach ($_POST as $PostKey=>$PostValue) {
        $APPDATA[$PostKey]   =   $PostValue;
    }
}

//Set The Matrix Care CompanyID
if($def_matrixcare_comp_info['CompanyIsDefault'] == 'Y') {
    $MatrixCareCompanyID	=   $def_matrixcare_comp_info['CompanyID'];
}

if($matrix_care_comp_cnt == 1) {
    $MatrixCareCompanyID	=   $matrix_care_companies['results'][0]['CompanyID'];
}

if(isset($_REQUEST['ddlMatrixCareCompaniesList']) && $_REQUEST['ddlMatrixCareCompaniesList'] != "") {
    $MatrixCareCompanyID	=   $_REQUEST['ddlMatrixCareCompaniesList'];
}

if($cloud_application_info ['MatrixCareCompanyID'] != "") {
    $MatrixCareCompanyID	=   $cloud_application_info ['MatrixCareCompanyID'];
}
?>
