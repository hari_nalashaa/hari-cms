<?php
require_once '../Configuration.inc';

//Onboard applicant params
$DataManagerStatus          =   isset($_REQUEST['DataManagerStatus']) ? $_REQUEST['DataManagerStatus'] : '';
$onboard                    =   isset($_REQUEST['onboard']) ? $_REQUEST['onboard'] : '';

$ApplicationID              =   isset($_REQUEST['ApplicationID']) ? $_REQUEST['ApplicationID'] : '';
$RequestID                  =   isset($_REQUEST['RequestID']) ? $_REQUEST['RequestID'] : '';
$DisplayApplicantSummary    =   "no";

//Wotc Form Information
//Get MultiOrgID
$multiorgid_req         	=   $RequisitionDetailsObj->getMultiOrgID($OrgID, $RequestID);
//Get Wotc Ids list
$wotcid_info				=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $multiorgid_req);
//Get Wotc Application Header Information
$wotc_app_header_info		=	G::Obj('WOTCIrecruitApplications')->getWotcApplicationInfo("*", $wotcid_info['wotcID'], $ApplicationID, $RequestID);
//Get Wotc ApplicationID
$WotcApplicationID			=	$wotc_app_header_info['ApplicationID'];

//Get Wotc Ids list
$wotcid_main_org_info		=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, "");
//Copy master questions
if($wotcid_main_org_info['wotcID'] != "" && $_REQUEST['OnboardSource'] == "WOTC") {
	G::Obj('OnboardQuestions')->copyOnboardMasterQuestions($OrgID, $_REQUEST['OnboardSource']);
}

$cloud_req_det_info         =   $RequisitionsObj->getRequisitionsDetailInfo("MultiOrgID", $OrgID, $RequestID);
$cloud_payroll_multiorgid   =   $cloud_req_det_info['MultiOrgID'];
$CloudPayRollSettingsLink   =   "administration.php?action=setupcloudpayroll&menu=8&OrgID=".$OrgID;

if($cloud_payroll_multiorgid != "") {
	$CloudPayRollSettingsLink .= "&MultiOrgID=".$cloud_payroll_multiorgid;
}

//set onboard company after processing it and also data
include IRECRUIT_DIR . 'onboard/SetOnboardCompanyID.inc';

if ($feature['MatrixCare'] == 'Y') {
    //include matrix care login
    list($MatrixCareInfo, $MatrixCount)     =   $MatrixCareObj->getMatrixCareLoginInformation($OrgID, $MatrixCareCompanyID);
    require_once IRECRUIT_DIR . 'onboard/MatrixCareLogin.inc';
}

$DMStatusTitles =   array (
    'ready'       => 'Applicants ready for onboarding.',
    'downloaded'  => 'Applicants that have been onboarded.'
);
if ($_REQUEST['OnboardSource'] == 'MatrixCare') {
    $DMStatusTitles ['downloaded'] = 'Transferred to MatrixCare';
}
else {
    $DMStatusTitles ['downloaded'] = 'Successfully set to Onboard';
}


$matrix_care_companies  =   $MatrixCareObj->getMatrixCareCompanies($OrgID);
$matrix_care_comp_cnt   =   $MatrixCareObj->getMatrixCareCompaniesCount($OrgID);

//Process Onboard Information
if(isset($_REQUEST['OnboardSource']) 
    && $_REQUEST['OnboardSource'] != ""
    && isset($_REQUEST['process']) 
	&& $_REQUEST['process'] == 'Y'
	&& $_REQUEST['process_onboard'] == 'YES') {
    
    if($_REQUEST['OnboardSource'] == 'MatrixCare') {
        require_once IRECRUIT_DIR . 'onboard/ProcessMatrixCareForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'WebABA') {
        require_once IRECRUIT_DIR . 'onboard/ProcessWebABAForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'Lightwork') {
        require_once IRECRUIT_DIR . 'onboard/ProcessLightworkForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'Abila') {
        require_once IRECRUIT_DIR . 'onboard/ProcessAbilaForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == 'Mercer') {
        require_once IRECRUIT_DIR . 'onboard/ProcessMercerForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == "WOTC") {
    	require_once IRECRUIT_DIR . 'onboard/ProcessWOTCForm.inc';
    }
    else if($_REQUEST['OnboardSource'] == $feature['DataManagerType']) {
        require_once IRECRUIT_DIR . 'onboard/ProcessDataManagerForm.inc';
    }
}

//Onboard applicant forms depend on the source
require_once IRECRUIT_VIEWS . 'onboard/OnboardApplicant.inc';

//Code to set date pickers year range
$onboard_ques_res		=	G::Obj('OnboardQuestions')->getOnboardQuestionsListByQueTypeID($OrgID, $_REQUEST['OnboardSource'], '17');
$onboard_ques_list		=	$onboard_ques_res['results'];

$validate_dates_info	=	array();
for($o = 0; $o < count($onboard_ques_list); $o++) {
	$validate_dates_info[$onboard_ques_list[$o]['QuestionID']] 	=	json_decode($onboard_ques_list[$o]['Validate'], true);
}

$validate_dates_info	=	json_encode($validate_dates_info);

if(isset($lst)) {
	?>
	<script>
	var lst					=	'<?php echo htmlspecialchars($lst);?>';
	var validate_dates_info	=	'<?php echo $validate_dates_info;?>';
	validate_dates_info		=	JSON.parse(validate_dates_info);

	var date_split_ids		=	lst.split(",");
	var date_objs			=	new Array();

	var i = 0;
	var date_id = "";
	var year_range = "";
	for(date_id_key in date_split_ids) {
		date_id		=	date_split_ids[i];
		date_id		=	$.trim(date_id);
		year_range	=	getYearRange(validate_dates_info, date_id);
		date_picker(date_id, 'mm/dd/yy', year_range, '');
		i++;
	}
	</script>
	<?php
}
?>
