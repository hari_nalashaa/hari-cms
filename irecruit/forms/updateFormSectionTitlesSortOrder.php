<?php
require_once '../Configuration.inc';

$forms_info = $_REQUEST['forms_info'];
$forms_info_cnt = count($_REQUEST['forms_info']);

$sort_order     =   1;
$update_status  =   false; 
for($i = 0; $i < $forms_info_cnt; $i++) {
	$set_info      =   array("SortOrder = :SortOrder");
	$where_info    =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID");
	$params_info   =   array(":OrgID"=>$OrgID, ":FormID"=>$forms_info[$i]['FormID'], ":SectionID"=>$forms_info[$i]['SectionID'], ":SortOrder"=>$sort_order);
	$upd_results   =   G::Obj('ApplicationFormSections')->updApplicationFormSections($set_info, $where_info, array($params_info));
	if($upd_results['affected_rows'] == 1) $update_status = true;
	$sort_order++;
}

if($update_status) echo "Updated successfully";
else echo "Failed to update the status";
?>