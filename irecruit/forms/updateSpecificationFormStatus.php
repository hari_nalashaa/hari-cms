<?php
require_once '../Configuration.inc';

//set where condition
$where = array("OrgID = :OrgID", "SpecificationFormID = :SpecificationFormID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":SpecificationFormID"=>$_REQUEST ['SpecificationFormID'], ":FormStatus"=>$_REQUEST['FormStatus']);
//set information
$set_info = array("FormStatus = :FormStatus");
//update agreement forms
$FormsInternalObj->updSpecificationForms($set_info, $where, array($params));
?>