<?php
require_once '../Configuration.inc';

$set_info      =   array("ViewPrintStatus = :ViewPrintStatus");
$where_info    =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID");
$params_info   =   array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST['FormID'], ":SectionID"=>$_REQUEST['SectionID'], ":ViewPrintStatus"=>$_REQUEST['ViewPrintStatus']);

if($_REQUEST['Active'] == "Y") {
    $QuestionsCount    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsCountBySectionID($OrgID, $_REQUEST['FormID'], $_REQUEST['SectionID'], array('99'));
    if($QuestionsCount == 0) {
        echo "Kindly activate at-least one question to activate this section.";
    }
    else {
        $upd_results   =   G::Obj('ApplicationFormSections')->updApplicationFormSections($set_info, $where_info, array($params_info));
        echo "Updated successfully";
    }
}
else {
    $upd_results   =   G::Obj('ApplicationFormSections')->updApplicationFormSections($set_info, $where_info, array($params_info));
    echo "Updated successfully";
}
?>