<?php
require_once '../Configuration.inc';

//set where condition
$where = array("OrgID = :OrgID", "AgreementFormID = :AgreementFormID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":AgreementFormID"=>$_REQUEST ['AgreementFormID'], ":FormStatus"=>$_REQUEST['FormStatus']);
//set information
$set_info = array("FormStatus = :FormStatus");
//update agreement forms
$FormsInternalObj->updAgreementForms($set_info, $where, array($params));
?>