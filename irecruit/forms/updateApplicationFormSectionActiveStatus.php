<?php
require_once '../Configuration.inc';

if(isset($_REQUEST['Active'])) {

$set_info      =   array("Active = :Active");
$where_info    =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID");
$params_info   =   array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST['FormID'], ":SectionID"=>$_REQUEST['SectionID'], ":Active"=>$_REQUEST['Active']);

    if ($_REQUEST['Active'] == "N") {
        array_push($set_info,'ViewPrintStatus = ""');
    } 

    G::Obj('ApplicationFormSections')->updApplicationFormSections($set_info, $where_info, array($params_info));

}

$where_info     =   array("OrgID = :OrgID", "FormID = :FormID", "IsDeleted = :IsDeleted", "Active = 'Y'");
$params_info    =   array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST['FormID'], ":IsDeleted"=>"No");
$form_sec_info  =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsInfo("SectionID, SectionTitle", $where_info, "", "SortOrder", array($params_info));

$sections_list  =   array(
                        "x:x"               =>  "Available Forms",
                        "formquestions:AFS" =>  "Application Form Sections",
                    );


foreach ($form_sec_info['results'] as $section_info) {
    $key        =   "formquestions:" . $section_info['SectionID'];
    $value      =   $section_info['SectionTitle'];
    
    $sections_list[$key]    =   $value;
}

//Get all text block sections
$results    =   G::Obj('FormSections')->getAllTextBlockSections();

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $key        =   'textblocks:' . $row ['TextBlockID'];
        $value      =   $row ['Section'];
        
        $sections_list[$key]    =   $value;
    }
}

echo json_encode($sections_list);
?>
