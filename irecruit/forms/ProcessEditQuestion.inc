<?php
// handle Question Detail updates
if ($process) {
    $cnt        =   $_POST['cnt'];
    $Question   =   $_POST['Question'];
    $Active     =   $_POST['Active'];
    $Required   =   $_POST['Required'];
    
    if(isset($_REQUEST['date_field']) && $_REQUEST['date_field'] != "") {
        //Set information
        $set_info		=	array("Validate = :Validate");
        //Where information
        $where_info		=	array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
        //Set parameters
        $params			=	array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST['form'], ":QuestionID"=>$_REQUEST['questionid'], ":Validate"=>json_encode($_POST['date_field']));
        //Update the table information based on bind and set values
        $res_que_info	=   G::Obj('FormQuestions')->updQuestionsInfo ( "FormQuestions", $set_info, $where_info, array ($params) );
    }
    
    if(isset($_REQUEST['ddlPrefilledQueData']) && $_REQUEST['ddlPrefilledQueData'] != "") {
        
        if($_REQUEST['ddlPrefilledQueData'] ==  "States")
        {
            $address_states_results =   $AddressObj->getAddressStateList();
            $address_states_count   =   $address_states_results['count'];
            $address_states         =   $address_states_results['results'];
            
            $def                    =   '';
            
            $address_states_list    =   array();
            $address_states_list[]  =   "Select";
            $answers_info[]         =   "" . ":" . "Select";
            for($asl = 0; $asl < $address_states_count; $asl++) {
                $address_states_list[$address_states[$asl]['Abbr']] = $address_states[$asl]['Description'];
                if($address_states[$asl]['Description'] != "") {
                    $answers_info[] =  $address_states[$asl]['Abbr'] . ':' . $address_states[$asl]['Description'];
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] ==  "Days")
        {
            $days_list              =   G::Obj('QuestionPreFilledInfo')->getDays();
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($days_list as $day_key=>$day_value) {
                if($day_value != "") {
                    $answers_info[] =   $day_key . ':' . $day_value;
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] ==  "Months")
        {
            $months_list            =   G::Obj('QuestionPreFilledInfo')->getMonths();
            
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($months_list as $month_key=>$month_value) {
                if($month_value != "") {
                    $answers_info[] =   $month_key . ':' . $month_value;
                }
            }
        }
        else if($_REQUEST['ddlPrefilledQueData'] == 'SocialMedia') {
            
            $social_media           =   G::Obj('QuestionPreFilledInfo')->getSocialMedia();
            
            $def                    =   '';
            $answers_info[]         =   "" . ":" . "Select";
            foreach($social_media as $socialmedia_key=>$socialmedia_value) {
                if($socialmedia_value != "") {
                    $answers_info[] =   $socialmedia_key . ':' . $socialmedia_value;
                }
            }
        }
        
        $answer = implode("::", $answers_info);
    }
    else {
        $def    =   '';
        $i      =   0;
        $ii     =   0;
        
        for($i; $i < $cnt; $cnt) {
            $i ++;
            $v  =   'value-' . $i;
            $d  =   'display-' . $i;
            $de =   'default-' . $i;
            
            if ($_POST [$d] != '') {
                
                if ($default == $de) {
                    $def = $_POST [$v];
                }
                
                $answer .= $_POST [$v] . ':' . $_POST [$d] . '::';
                $ii ++;
                
                $data [] = array (
                    'value' =>  $_POST [$v],
                    'name'  =>  $_POST [$d]
                );
            }
        }
        
        if ($_POST ['AttachmentName']) {
            if ($_FILES ['AttachmentFile'] ['type'] != "") {
                
                $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
                
                if (! file_exists ( $dir )) {
                    mkdir ( $dir, 0700 );
                    chmod ( $dir, 0777 );
                }
                
                $apatdir = $dir . '/formattachments';
                
                if (! file_exists ( $apatdir )) {
                    mkdir ( $apatdir, 0700 );
                    chmod ( $apatdir, 0777 );
                }
                
                $data   =   file_get_contents ( $_FILES ['AttachmentFile'] ['tmp_name'] );
                $e      =   explode ( '.', $_FILES ['AttachmentFile'] ['name'] );
                $ecnt   =   count ( $e ) - 1;
                $ext    =   $e [$ecnt];
                $ext    =   preg_replace ( "/\s/i", '', $ext );
                $ext    =   substr ( $ext, 0, 5 );
                
                $answer =   $_POST ['AttachmentName'] . "::" . $ext;
                
                $filename = $apatdir . '/' . $FormID . '-' . $QuestionID . '.' . $ext;
                $fh = fopen ( $filename, 'w' );
                fwrite ( $fh, $data );
                fclose ( $fh );
                
                chmod ( $filename, 0644 );
            } // end if FILES
        } // end if AttachmentName
        
        if ($sort == 'Y') {
            
            if($_REQUEST ['QuestionTypeID'] != "100") {
                $answer = "";
                
                foreach ( $data as $key => $row ) {
                    if($row ['name'] == "Select") $row ['name'] = "1Select";
                    $value [$key] = $row ['value'];
                    $name [$key] = $row ['name'];
                }
                array_multisort ( $name, SORT_ASC, $data );
                
                foreach ( $data as $key => $row ) {
                    if($row ['name'] == "1Select")  $row ['name'] = "Select";
                    $answer .= $row ['value'] . ':' . $row ['name'] . '::';
                    $ii ++;
                }
            }
            
        }
        
        if ($ii > 0) {
            $answer =   substr ( "$answer", 0, - 2 );
        }
        
        if ($Clear == 'Y') {
            $def    =   '';
        }
        
        if (($questionid == "Sage_HireDate") || ($questionid == "Sage_PayPeriodHRS") || ($questionid == "Sage_Rate")) {
            $def    =   $default;
        } // end questionid
        
        if ($questionid == "COB_WCgroup") {
            $def    =   $default;
        } // end questionid
        
        $params     =   array ();
        $set_info   =   array ();
        
        if ($_REQUEST ['QuestionTypeID'] == "100") {
            $question_answers ['RVal'] = $_REQUEST ['RVal'];
            $question_answers ['LabelValRow'] = $_REQUEST ['LabelValRow'];
            
            if ($sort == 'Y') {
                //Sort based on alphabetical order
                asort($question_answers ['LabelValRow']);
                
                if(is_array($question_answers ['LabelValRow'])) {
                    $q100_i = 1;
                    foreach($question_answers ['LabelValRow'] as $lable_key_name=>$label_val_name) {
                        $question_answers ['LabelValRow']["LabelValRow".$q100_i] = $label_val_name;
                        $q100_i++;
                    }
                }
            }
            
            if (is_array ( $question_answers )) {
                $answer = serialize ( $question_answers );
            }
            
            if ($_REQUEST ['rows'] != "" && $_REQUEST ['cols'] != "") {
                $params [':rows'] = $_REQUEST ['rows'];
                $params [':cols'] = $_REQUEST ['cols'];
                $set_info [] = "rows = :rows";
                $set_info [] = "cols = :cols";
            }
        }
        
        if ($_REQUEST ['QuestionTypeID'] == "120") {
            $question_answers ['day_names'] = $_REQUEST ['day_names'];
            if (is_array ( $question_answers )) {
                $answer = serialize ( $question_answers );
            }
            
            if ($_REQUEST ['number_of_days'] != "") {
                $params [':rows'] = $_REQUEST ['number_of_days'];
                $set_info [] = "rows = :rows";
            }
        }
    }
    
    //Bind Parameters
    $params[':OrgID']          =   $OrgID;
    $params[':FormID']         =   $form;
    $params[':QuestionID']     =   $questionid;
    
    //set where condition
    $where_info                =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
    
    $params[':Question']       =   $Question;
    $params[':value']          =   $answer;
    $params[':defaultValue']   =   $def;
    
    //Set the parameters those are going to update
    $set_info[]                =   "Question       =   :Question";
    $set_info[]                =   "value          =   :value";
    $set_info[]                =   "defaultValue   =   :defaultValue";
    
    //Update the table information based on bind and set values
    $res_que_info              =   G::Obj('FormQuestions')->updQuestionsInfo ( $formtable, $set_info, $where_info, array ($params) );
    
    $QuestionIDOrig            =   $QuestionID; // preserve original $QuestionID
    $QuestionID                =   $QuestionIDOrig; // preserve original $QuestionID
    
    echo '<script language="JavaScript" type="text/javascript">';
    echo "alert('Update completed!')";
    echo '</script>';
}
?>