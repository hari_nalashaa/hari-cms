<?php
//Declare mostly using globals over here
$TemplateObj->formscript        =   $formscript     =   "forms.php";

//Declare mostly using globals over here
$TemplateObj->process           =   $process        =   isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->questionid        =   $questionid     =   isset($_REQUEST['questionid']) ? $_REQUEST['questionid'] : '';
$TemplateObj->new               =   $new            =   isset($_REQUEST['new']) ? $_REQUEST['new'] : '';

//Set Add Flag
$TemplateObj->add               =   $add            =   isset($_REQUEST['add']) ? $_REQUEST['add'] : '';
$TemplateObj->answer            =   $answer         =   isset($_REQUEST['answer']) ? $_REQUEST['answer'] : '';
$TemplateObj->Active            =   $Active         =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->Required          =   $Required       =   isset($_REQUEST['Required']) ? $_REQUEST['Required'] : '';
$TemplateObj->default           =   $default        =   isset($_REQUEST['default']) ? $_REQUEST['default'] : '';
$TemplateObj->cnt               =   $cnt            =   isset($_REQUEST['cnt']) ? $_REQUEST['cnt'] : '';
$TemplateObj->finish            =   $finish         =   isset($_REQUEST['finish']) ? $_REQUEST['finish'] : '';
$TemplateObj->Clear             =   $Clear          =   isset($_REQUEST['Clear']) ? $_REQUEST['Clear'] : '';
$TemplateObj->sort              =   $sort           =   isset($_REQUEST['sort']) ? $_REQUEST['sort'] : '';
$TemplateObj->mrpCount          =   $mrpCount       =   isset($_REQUEST['mrpCount']) ? $_REQUEST['mrpCount'] : '';
//
$TemplateObj->direction         =   $direction      =   isset($_REQUEST['direction']) ? $_REQUEST['direction'] : '';
$TemplateObj->Question          =   $Question       =   isset($_REQUEST['Question']) ? $_REQUEST['Question'] : '';
$TemplateObj->QuestionTypeID    =   $QuestionTypeID =   isset($_REQUEST['QuestionTypeID']) ? $_REQUEST['QuestionTypeID'] : '';

//Declare mostly using globals over here
$TemplateObj->section           =   $section        =   isset($_REQUEST['section']) ? $_REQUEST['section'] : '';
$TemplateObj->action            =   $action         =   isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$TemplateObj->form              =   $form           =   isset($_REQUEST['form']) ? $_REQUEST['form'] : '';
$TemplateObj->formtable         =   $formtable      =   isset($_REQUEST['formtable']) ? $_REQUEST['formtable'] : 'FormQuestions';

if (isset($_REQUEST['actionsection'])) {
	list ( $action, $section ) = explode ( ":", $_REQUEST['actionsection'] );
	
	$TemplateObj->action	=	$action;
	$TemplateObj->section	=	$section;
    $TemplateObj->formtable =  $formtable  =   "FormQuestions";
}

if (($action == 'x') && ($section == 'x')) {
	$TemplateObj->action   =   $action = '';
	$TemplateObj->section  =   $section = '';
	$TemplateObj->form     =   $form = '';
}

if ($form) {
	$title = 'Form - ' . $form;
} else {
	$title = 'Application Forms';
}
?>