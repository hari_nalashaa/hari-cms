<?php
require_once '../Configuration.inc';

//set where condition
$where = array("OrgID = :OrgID", "WebFormID = :WebFormID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$_REQUEST ['WebFormID'], ":FormStatus"=>$_REQUEST['FormStatus']);
//set information
$set_info = array("FormStatus = :FormStatus");
//update agreement forms
$FormsInternalObj->updWebForms($set_info, $where, array($params));
?>