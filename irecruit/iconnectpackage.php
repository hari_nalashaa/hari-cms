<?php 
require_once 'Configuration.inc';

 if(isset($_REQUEST['IconnectPackagesHeaderID'])){
     
     $getpackagename = G::Obj('IconnectPackagesHeader')->geticonnectPackagesHeader($OrgID, $_REQUEST['IconnectPackagesHeaderID']);
      $PackageName =  $getpackagename['PackageName'];
     $_SESSION['PackageName'] = '';
     $_SESSION['PackageName'] =  $PackageName;
    header("Location:".IRECRUIT_HOME."iconnectforms.php?IconnectPackagesHeaderID=".$_REQUEST['IconnectPackagesHeaderID']);

 }

if ($_REQUEST['active'] == "n") { 
	$Active = 'N'; 
	$a = 'n'; 
} else if ($_REQUEST['active'] == "p") { 
	$Active = 'P'; 
	$a = 'p'; 
} else { 
	$Active = 'Y'; 
	$a = 'y'; 
}

$checklist_res       =   array();
$package_info      =   G::Obj('IconnectPackagesHeader')->geticonnectPackagesHeaderByOrgID($OrgID,'Active');
$checklist_res       =   json_decode($checklist_info['StatusCategory'], true);
$column = '*';
$where          =   array ("OrgID = :OrgID","Active = 'Y'");
$params         =   array (":OrgID" => $OrgID);
$get_applicant_process_info      =   G::Obj('Applicants')->getApplicantProcessFlowInfo($columns, $where, 'ProcessOrder asc', array($params) );
$get_applicant_process_results = $get_applicant_process_info['results'];
$get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Add Package') {
    $PackageName =   $_REQUEST['PackageName'];
     $_SESSION['PackageName'] = '';
    //Insert PackageName Information
    G::Obj('IconnectPackagesHeader')->insiconnectPackagesHeader($OrgID, $PackageName);
     $_SESSION['PackageName'] =  $PackageName;
    //header("Location:".IRECRUIT_HOME."iconnectforms.php");
    header("Location:".IRECRUIT_HOME."formsInternal.php?typeform=packages");
    exit; 
}else if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Copy Form') {
    if (isset($_POST['action']) && $_POST['action'] == 'copyform') {
                        
        if (isset($_POST['NewChecklistName'])) {
            
            // Get Checklist ID & Name
            $new_name = $_POST['NewChecklistName'];
            $new_checklist_id = $_POST['NewChecklistID'];
            
            //Copy Application Form Sections
            G::Obj('Checklist')->copyChecklist($OrgID, $new_checklist_id, $new_name);
            
            header("Location:".IRECRUIT_HOME."administration/checklist.php?active=".$a);
        }
    }
}


$TemplateObj->package_info  =   $package_info['results'];
$TemplateObj->get_applicant_process_results  =   $get_applicant_process_results;
$TemplateObj->get_checklist_process_data  =   $get_checklist_process_data;
echo $TemplateObj->displayIrecruitTemplate ( 'views/requisitions/Iconnectpackage' );
?>
