<!-- Verified First - STAGING CODE --> 
<script type="text/javascript"src="https://widget1.verifiedfirst.com/Browser-Extension/src/app/widget.js" async></script>
<!-- End Verified First -->

<!-- Verified First - PRODUCTION CODE --> 
<!--script type="text/javascript"src="https://widget.verifiedfirst.com/Browser-Extension/src/app/widget.js" async></script-->
<!-- End Verified First -->

<div id="vf-widget-menu-wrap"></div>


<div id="vf-widget-button-wrap"></div>

<div id="verified-first"> 
  <div class="hidden"> 
    <input type="hidden" id="partner-applicant-id" value="IR0000600"> 
    <input type="hidden" id="individual-first-name" value="Hank"> 
    <input type="hidden" id="individual-last-name" value="Mess"> 
    <input type="hidden" id="individual-middle-name" value="F."> 
    <input type="hidden" id="individual-suffix" value=""> 
    <input type="hidden" id="individual-email" value="test@irecruit-software.com"> 
    <input type="hidden" id="individual-date-of-birth" value="07/07/1977"> 
    <input type="hidden" id="individual-ssan" value="333-22-1111"> 
    <input type="hidden" id="individual-mobile-phone" value="253-555-1212"> 
    <input type="hidden" id="individual-address-line-1" value="123 Main St."> 
    <input type="hidden" id="individual-address-line-2"> 
    <input type="hidden" id="individual-address-city" value="Houston"> 
    <input type="hidden" id="individual-address-state" value="Texas"> 
    <input type="hidden" id="individual-address-postal-code" value="77077"> 
    <input type="hidden" id="individual-address-postal-country" value="US"> 

    <input type="hidden" id="user-firstname" value="David"> 
    <input type="hidden" id="user-lastname" value="Edgecomb"> 
    <input type="hidden" id="user-email" value="dedgecomb@irecruit-software.com"> 
    <input type="hidden" id="user-phone" value="413-426-8215"> 
    <input type="hidden" id="user-company" value="United States of America"> 
  </div> 
</div>

<?php
/*
    <input type="hidden" id="user-misc1" value="Optional Data"> 
    <input type="hidden" id="user-misc2" value="Optional Data"> 
    <input type="hidden" id="user-misc3" value="Optional Data"> 
*/
?>
