<?php
require_once 'irecruitdb.inc';

$user_cell_number_sms	=	"false";

$user_cell_number		=	$_REQUEST['cell_number'];

$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $user_cell_number);

//Add plus one before number
if(substr($user_cell_number, 0, 2) != "+1") {
	$user_cell_number	=	"+1".$user_cell_number;
}

//Get LoopUp Phone number information
$lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $user_cell_number);

$sms_capable_info		=	array("sms_capable"=>$user_cell_number_sms);	

if($lookup_phone_num_info['Code'] == 'Success') {
	//Get the Protected properties from SMS object, by extending the object through ReflectionClass
	$reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
	$property       		=   $reflection->getProperty("properties");
	$property->setAccessible(true);
	$properties				=   $property->getValue($lookup_phone_num_info['Response']);
	
	if($properties['carrier']['type'] == 'mobile'
		|| $properties['carrier']['type'] == 'voip') {
		$user_cell_number_sms	=	"true";
	}
	
	$sms_capable_info		=	array("sms_capable"=>$user_cell_number_sms, "properties"=>$properties['carrier']);
}

echo json_encode($sms_capable_info);
?>