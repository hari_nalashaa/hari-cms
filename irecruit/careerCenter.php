<?php
require_once 'Configuration.inc';

//Set goto object, will be useful to track the page to redirect after login
if ($_SERVER ['QUERY_STRING']) {
	$TemplateObj->goto	=	$_SERVER ['QUERY_STRING'];
}

//Set page title
$TemplateObj->title	= $title = 'Career Center';

echo $TemplateObj->displayIrecruitTemplate('views/responsive/CareerCenter');
?>