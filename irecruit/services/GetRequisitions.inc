<?php 
// Set columns
$columns = "NOW() AS CurrentDateTime, DATE_FORMAT(PostDate,'%m/%d/%Y') PostDate, getOrgnizationName(Requisitions.OrgID, Requisitions.MultiOrgID) as OrgnizationName, DATE_FORMAT(ExpireDate,'%m/%d/%Y') ExpireDate,
            DATE_FORMAT(PostDate,'%Y-%m-%d %H:%i:%s') PostDateYMD, DATE_FORMAT(ExpireDate,'%Y-%m-%d %H:%i:%s') ExpireDateYMD,
			DATEDIFF(DATE_ADD(DATE(MonsterJobPostedDate),INTERVAL Duration DAY), NOW()) as RemainingDays,";

$columns .= "IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate)) Open, DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate) Closed,";

$columns .= "DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY),'%m/%d/%Y') Current,
			RequestID, Title, MultiOrgID, MonsterJobPostType, Duration, RequisitionID, JobID,
			Description, MonsterJobCategory, MonsterJobOccupation, MonsterJobIndustry, Active, 
            Approved, RequisitionStage, Address1, Address2, City, State, ZipCode, Country, FreeJobBoardLists, ZipRecruiterSubscription, ZipRecruiterTrafficBoost";

$columns_count = "COUNT(RequestID) AS RequisitionsCount";

if(isset($FilterOrganization) && $FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);

    // Set condition
    $where  =   array ("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
    // Set parameters
    $params =   array (":OrgID" => $FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
}
else {
    $where  =   array("OrgID = :OrgID");
    $params =   array(":OrgID" => $OrgID);
}


if(!isset($SubscribeZipRecruiter)) $SubscribeZipRecruiter = "";
if(!isset($PaidZipRecruiter)) $PaidZipRecruiter = "";
if(!isset($FreeZipRecruiter)) $FreeZipRecruiter = "";

if(isset($_REQUEST['SubscribeZipRecruiter'])) $SubscribeZipRecruiter = $_REQUEST['SubscribeZipRecruiter'];
if(isset($_REQUEST['PaidZipRecruiter'])) $PaidZipRecruiter = $_REQUEST['PaidZipRecruiter'];
if(isset($_REQUEST['FreeZipRecruiter'])) $FreeZipRecruiter = $_REQUEST['FreeZipRecruiter'];

if($SubscribeZipRecruiter == "Yes" && $PaidZipRecruiter == "Yes" && $FreeZipRecruiter == "Yes") {
    $where[] = "(FreeJobBoardLists LIKE '%ZipRecruiter%' OR ZipRecruiterSubscription != '' OR ZipRecruiterTrafficBoost != '')";
}
else {
    if($SubscribeZipRecruiter == "Yes") {
        $where[] = "ZipRecruiterSubscription != ''";
    }
    else if($PaidZipRecruiter == "Yes") {
        $where[] = "ZipRecruiterTrafficBoost != ''";
    }
    else if($FreeZipRecruiter == "Yes") {
        $where[] = "FreeJobBoardLists LIKE '%ZipRecruiter%'";
    }
}

//Set Date
if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])) {
    $where[] = "PostDate >= :FromDate AND PostDate <= :ToDate";
    $params[":FromDate"]    =   $DateHelperObj->getYmdFromMdy($_REQUEST['FromDate'], "/", "-");
    $params[":ToDate"]      =   $DateHelperObj->getYmdFromMdy($_REQUEST['ToDate'], "/", "-");
}
else {
    $where[] = "PostDate >= :FromDate AND PostDate <= :ToDate";
    $params[":FromDate"]    =   $DateHelperObj->getYmdFromMdy(date('m/d/Y', strtotime("-12 months", strtotime(date('Y-m-d')))), "/", "-");
    $params[":ToDate"]      =   $DateHelperObj->getYmdFromMdy(date('m/d/Y'), "/", "-");
}

if(isset($_REQUEST['Keyword']) && $_REQUEST['Keyword'] != "") {
    $where[] = "(Title LIKE :Keyword1 OR RequisitionID LIKE :Keyword2 OR JobID LIKE :Keyword3)";
    $params[":Keyword1"]    =   "%".$_REQUEST['Keyword']."%";
    $params[":Keyword2"]    =   "%".$_REQUEST['Keyword']."%";
    $params[":Keyword3"]    =   "%".$_REQUEST['Keyword']."%";
}

if(isset($_REQUEST['RequisitionStage']) && $_REQUEST['RequisitionStage'] != "") {
    $where[] = "(RequisitionStage = :RequisitionStage)";
    $params[":RequisitionStage"]   =  $_REQUEST['RequisitionStage'];
}

if(isset($_REQUEST['RequisitionStageDate']) && $_REQUEST['RequisitionStageDate'] != "") {
    $where[] = "(RequisitionStageDate = :RequisitionStageDate)";
    $params[":RequisitionStageDate"]   =  $DateHelperObj->getYmdFromMdy($_REQUEST['RequisitionStageDate']);
}

if ($feature ['RequisitionApproval'] != "Y") {
    if($Active == "A") {
        $where[]            =   "Active != :Active";
        $params[":Active"]  =   'R';
    }
    else {
        $where[]            =   "Active = :Active";
        $params[":Active"]  =   $Active;
    }
}
else if ($feature ['RequisitionApproval'] == "Y") {
    if($Active != "A") {
        $where[]            =   "Active = :Active";
        $params[":Active"]  =   $Active;
    }
}

if(isset($IsRequisitionApproved) && $IsRequisitionApproved == "N") {
    $where[]                =   "Approved = 'N'";
}

if($Active == "Y") $where[] = "Approved = 'Y'";


if (substr ( $USERROLE, 0, 21 ) == 'master_hiring_manager') {
    $params [":RMOrgID"]    =   $OrgID;
    $params [":RMUserID"]   =   $USERID;
    $where [] = " RequestID IN (SELECT RequestID FROM RequisitionManagers WHERE OrgID = :RMOrgID AND UserID = :RMUserID)";
} // end hiring manager



if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

if (isset ( $_REQUEST ['to_sort'] )) {

    if ($_REQUEST ['to_sort'] == "date_opened") {
        $order_by           =   " Requisitions.PostDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_id") {
        $order_by           =   " RequisitionID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "job_id") {
        $order_by           =   " JobID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_title") {
        $order_by           =   " Title $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "status") {
        $order_by           =   " Active $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "requisition_stage") {
        $order_by           =   " RequisitionStage $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "days_open") {
        $order_by       =   " IF((NOW() < Requisitions.ExpireDate), DATEDIFF(NOW(), Requisitions.PostDate), DATEDIFF(Requisitions.ExpireDate, Requisitions.PostDate))  $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "expire_date") {
        $order_by           =   " DATEDIFF(NOW(), Requisitions.ExpireDate) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "applicants_count") {
        $order_by =   " ApplicantsCount $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "company") {
        $order_by =   " getOrgnizationName(Requisitions.OrgID, Requisitions.MultiOrgID) $sort_type";
    }
    else {
        $order_by           =   " Requisitions.PostDate DESC";
    }

    $sort_by_key            =   $_REQUEST ['to_sort'];
} else {

    $order_by           =   " Requisitions.PostDate DESC";
    $sort_by_key        =   "Requisitions.PostDate";

    $sort_type              =   "DESC";
}

$Start      =   0;
if (isset ( $_REQUEST ['IndexStart'] ) && is_numeric ( $_REQUEST ['IndexStart'] )) {
    $Start  =   $_REQUEST ['IndexStart'];
}

if(!isset($_REQUEST['Export'])) {
    $req_limit  =   " LIMIT $Start, $Limit";
}

// Get Requisition Information
$req_search_results         =   $RequisitionsObj->getRequisitionInformation ( $columns, $where, "", $order_by . $req_limit, array ($params) );

// Get Requisitions Count
$total_results_count        =   $RequisitionsObj->getRequisitionInformation ( $columns_count, $where, "", "", array ($params) );

$total_requisitions_count   =   $total_results_count ['results'] [0] ['RequisitionsCount'];
$count_per_page             =   $req_search_results ['count'];

$req_search_list = array ();

/**
 * @full details information
*/
if (is_array ( $req_search_results ['results'] )) {

    $r = 0;
    foreach ( $req_search_results ['results'] as $REQS ) {

        $indeed_feed_info   =   $IndeedObj->getIndeedFeedInformation("Email", $OrgID, "", $REQS['RequestID']);
        $monster_req_info   =   $MonsterObj->getMonsterRequisitionInformation("RequestID", $OrgID, "", $REQS['RequestID']);
        $zip_recruiter_info =   $ZipRecruiterObj->getZipRecruiterFeed($OrgID, $REQS['RequestID']);
        
        $req_search_results ['results'] [$r] ['PaidIndeed'] = "";
        $req_search_results ['results'] [$r] ['PaidMonster'] = "";
        $req_search_results ['results'] [$r] ['PaidZipRecruiter'] = "";
        
        if(isset($zip_recruiter_info['RequestID']) && $zip_recruiter_info['RequestID'] != "") {
            $req_search_results ['results'] [$r] ['PaidZipRecruiter'] = "Yes";
        }
        
        if(isset($indeed_feed_info['Email']) && $indeed_feed_info['Email'] != "") {
            $req_search_results ['results'] [$r] ['PaidIndeed'] = "Yes";
        }
        
        if(isset($monster_req_info['RequestID']) && $monster_req_info['RequestID'] != "") {
            $req_search_results ['results'] [$r] ['PaidMonster'] = "Yes";
        }
        
        $req_search_results ['results'] [$r] ['OrganizationName'] = $REQS['OrgnizationName'];

        if($REQS['Active'] == "Y") {
            $req_search_results ['results'] [$r] ['ActiveStatus'] = "Active";
        }
        else if($REQS['Active'] == "N") {
            $req_search_results ['results'] [$r] ['ActiveStatus'] = "In-Active";
        }
        else if($REQS['Active'] == "R") {
            $req_search_results ['results'] [$r] ['ActiveStatus'] = "Pending";
        }
        
        $req_search_results ['results'] [$r] ['PostedInternal'] = "";
        
        $req_search_results ['results'] [$r] ['InternalRequisition'] = "Y";
        
        $req_search_results ['results'] [$r] ['PostDateByFeature'] = $REQS['PostDate'];
        $req_search_results ['results'] [$r] ['PostDateByFeatureYMD'] = $REQS['PostDateYMD'];
        $req_search_results ['results'] [$r] ['ExpireDateYMD'] = $REQS['ExpireDateYMD'];
        
        $free_jobboard_lists = json_decode($REQS['FreeJobBoardLists'], true);
        
        $req_search_results ['results'] [$r] ['FreeMonster'] = "";
        $req_search_results ['results'] [$r] ['FreeIndeed'] = "";
        $req_search_results ['results'] [$r] ['FreeZipRecruiter'] = "";
        
        if(is_array($free_jobboard_lists) && in_array("Indeed", $free_jobboard_lists)) {
            $req_search_results ['results'] [$r] ['FreeIndeed'] = "Yes";
        }
        if(is_array($free_jobboard_lists) && in_array("Monster", $free_jobboard_lists)) {
            $req_search_results ['results'] [$r] ['FreeMonster'] = "Yes";
        }
        if(is_array($free_jobboard_lists) && in_array("ZipRecruiter", $free_jobboard_lists)) {
            $req_search_results ['results'] [$r] ['FreeZipRecruiter'] = "Yes";
        }

        if(!isset($REQS['Province'])) $REQS['Province'] = '';
        $req_search_results ['results'] [$r] ['AddressInformation'] = $AddressObj->formatAddress($OrgID, $REQS['Country'], $REQS['City'], $REQS['State'], $REQS['Province'], $REQS['ZipCode'], '');
        
        if ($Active == "N") {
            $req_search_results ['results'] [$r] ['Open'] = $REQS ['Closed'];
        }

        // Set where condition
        $where = array (
            "OrgID          =   :OrgID",
            "RequestID      =   :RequestID"
        );
        // Set parameters
        $params = array (
            ":OrgID"        =>  $OrgID,
            ":RequestID"    =>  $REQS ['RequestID']
        );
        // Get Job Applications Information
        $resultsCNT = $ApplicationsObj->getJobApplicationsInfo ( "COUNT(*) AS cnt", $where, '', '', array ($params) );
        $req_search_results ['results'] [$r] ['ApplicantsCount'] = $resultsCNT ['results'] [0] ['cnt'];

        if ($permit ['Reports'] == 1) {
             
            $reportlink = "reports.php?";
            $reportlink .= "action=ap";
            $reportlink .= "OrgID=" . $OrgID;
            if($REQS ['MultiOrgID'] != "") $reportlink .= "&MultiOrgID=" . $REQS ['MultiOrgID'];
            $reportlink .= "&RequestID=" . $REQS['RequestID'];
            $reportlink .= "&menu=7";
             
            $req_search_results ['results'] [$r] ['ReportLink'] = $reportlink;
        }

        if ($permit ['Requisitions_Edit'] == 1) {
             
            $linkc = IRECRUIT_HOME . 'request/assign.php?RequestID=' . $REQS ['RequestID'] . '&action=copy&Active=' . $Active;
             
            $req_search_results ['results'] [$r] ['Linkc'] = $linkc;
        } else { // else action != Requisition_Edit
            $link = IRECRUIT_HOME . "requisitions/viewListing.php?RequestID=" . $REQS ['RequestID'];
            if ($AccessCode != "") {
                $link .= "&k=" . $AccessCode;
            }
            $req_search_results ['results'] [$r] ['ViewListing'] = $link;
        } // end else Requisition_Edit

        $r ++;
    } // end foreach
}

$left_nav_info = $PaginationObj->getPageNavigationInfo ( $Start, $Limit, $total_requisitions_count, '', '' );

$requisitions_info  =   json_encode (
                                        array (
                                            'results'                       =>  $req_search_results ['results'],
                                            'permit_Requisitions_Edit'      =>  $permit ['Requisitions_Edit'],
                                            'permit_Reports'                =>  $permit ['Reports'],
                                            'permit_Admin'                  =>  $permit ['Admin'],
                                            'feature_RequisitionRequest'    =>  $feature ['RequisitionRequest'],
                                            'feature_InternalRequisitions'  =>  $feature ['InternalRequisitions'],
                                            'count_per_page'                =>  $count_per_page,
                                            'previous'                      =>  $left_nav_info ['previous'],
                                            'next'                          =>  $left_nav_info ['next'],
                                            'total_pages'                   =>  $left_nav_info ['total_pages'],
                                            'current_page'                  =>  $left_nav_info ['current_page'],
                                            'requisitions_count'            =>  $total_requisitions_count
                                        )
                                    );

if ($ServerInformationObj->getRequestSource () == 'ajax') {
    echo $requisitions_info;           
}
else {
	return $requisitions_info;
}
?>
