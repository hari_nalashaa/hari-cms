<?php 
// Get Applicants based on RequestID
$columns_count  =   "COUNT(ApplicationID) AS ApplicantsCount";
$columns        =   "(SELECT Title FROM Requisitions WHERE OrgID = JobApplications.OrgID AND RequestID = JobApplications.RequestID) as Title, (SELECT RequisitionID FROM Requisitions WHERE OrgID = JobApplications.OrgID AND RequestID = JobApplications.RequestID) AS RequisitionID, (SELECT JobID FROM Requisitions WHERE OrgID = JobApplications.OrgID AND RequestID = JobApplications.RequestID) AS JobID, ApplicationID, RequestID, ApplicantSortName, ProcessOrder, DispositionCode, EntryDate, LastModified, IsInternalApplication";

if($FilterOrganization != "") {
    list($FilterOrgID, $FilterMultiOrgID) = explode("-", $FilterOrganization);
    
    $where_info     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
    $params_info    =   array(":OrgID" => $FilterOrgID, ":MultiOrgID"=>$FilterMultiOrgID);
}
else {
    $where_info     =   array("OrgID = :OrgID");
    $params_info    =   array(":OrgID" => $OrgID);
}

if (isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "ASC") {
    $sort_type              =   "ASC";
} else if ((isset ( $_GET ['sort_type'] ) && $_GET ['sort_type'] != "" && $_GET ['sort_type'] == "DESC")) {
    $sort_type              =   "DESC";
}

if (isset ( $_REQUEST ['to_sort'] )) {

    if ($_REQUEST ['to_sort'] == "applicant_name") {
        $order_by           =   " ApplicantSortName $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "application_id") {
        $order_by           =   " ApplicationID $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_title") {
        $order_by           =   " (SELECT Title FROM Requisitions WHERE OrgID = JobApplications.OrgID AND RequestID = JobApplications.RequestID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "req_id") {
        $order_by           =   " (SELECT RequisitionID FROM Requisitions WHERE OrgID = JobApplications.OrgID AND RequestID = JobApplications.RequestID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "job_id") {
        $order_by           =   " (SELECT JobID FROM Requisitions WHERE OrgID = JobApplications.OrgID AND RequestID = JobApplications.RequestID) $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "status") {
        $order_by           =   " ProcessOrder $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "entry_date") {
        $order_by           =   " EntryDate $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "last_modified") {
        $order_by           =   " LastModified $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "referral") {
        $order_by           =   " Informed $sort_type";
    }
    else if ($_REQUEST ['to_sort'] == "is_internal_application") {
        $order_by           =   " IsInternalApplication $sort_type";
    }
    else {
        $order_by           =   " EntryDate DESC";
    }
    
} else {
    $order_by           =   " EntryDate DESC";
}

$start      =   0;
if (isset ( $_REQUEST ['IndexStart'] ) && is_numeric ( $_REQUEST ['IndexStart'] )) {
    $start  =   $_REQUEST ['IndexStart'];
}


if(!isset($_REQUEST['Export'])) {
    $order_by .= " LIMIT $start, $limit";
}

if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])) {
    $where_info[] = "DATE(EntryDate)  >=  :FromDate";
    $where_info[] = "DATE(EntryDate)  <=  :ToDate";
    
    $params_info[":FromDate"]   =   $DateHelperObj->getYmdFromMdy($_REQUEST['FromDate'], "/", "-");
    $params_info[":ToDate"]     =   $DateHelperObj->getYmdFromMdy($_REQUEST['ToDate'], "/", "-");
}

if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
    $RequestIDsList =   explode(",", $_REQUEST['RequestID']);
    
    $RequestIDs     =   "'" . implode ( "', '", $RequestIDsList ) . "'";
    
    $where_info[]   =   "RequestID IN (".$RequestIDs.")";
}

if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") {
    $where_info[] = "MultiOrgID = :MultiOrgID";
    $params_info[":MultiOrgID"] = $_REQUEST['MultiOrgID'];
}

if(isset($_REQUEST['Status']) && $_REQUEST['Status'] != "") {
    $where_info[] = "ProcessOrder = :ProcessOrder";
    $params_info[":ProcessOrder"] = $_REQUEST['Status'];
}

if(isset($_REQUEST['Keyword']) && $_REQUEST['Keyword'] != "") {
    $where_info[] = "(ApplicantSortname LIKE :Keyword OR Informed LIKE :Informed)";
    $params_info[":Keyword"] = "%".$_REQUEST['Keyword']."%";
    $params_info[":Informed"] = "%".$_REQUEST['Keyword']."%";
}

$app_results            =   G::Obj('Applications')->getJobApplicationsInfo($columns, $where_info, '', $order_by, array($params_info));
$applicants_list        =   $app_results['results'];

$applicants_count_info  =   G::Obj('Applications')->getJobApplicationsInfo($columns_count, $where_info, '', ' EntryDate DESC', array($params_info));
$applicants_count       =   $applicants_count_info['results'][0]['ApplicantsCount'];

for ($i = 0; $i < count($applicants_list); $i ++) {
    $ApplicationID      =   $applicants_list[$i]['ApplicationID'];
    $APPDATA            =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);
    $Answer13           =   ($APPDATA ['homephone'] != "") ? json_decode($APPDATA ['homephone'], true) : "";
    
    $applicants_list[$i]['ReferralSource']  =   $APPDATA['informed'];
    $applicants_list[$i]['ProcessOrder']    =   G::Obj('ApplicantDetails')->getProcessOrderDescription($OrgID, $applicants_list[$i]['ProcessOrder']);
    $applicants_list[$i]['Email']           =   $APPDATA['email'];
    $applicants_list[$i]['HomePhone']       =   G::Obj('Address')->formatPhone ( $OrgID, $APPDATA ['country'], $Answer13[0], $Answer13[1], $Answer13[2], '' );
    $applicants_list[$i]['EntryDateMDY']    =   date('m/d/Y H:i:s', strtotime($applicants_list[$i]['EntryDate']));
    $applicants_list[$i]['LastModifiedMDY'] =   date('m/d/Y H:i:s', strtotime($applicants_list[$i]['LastModified']));
    $applicants_list[$i]['DispositionCode'] =   $disposition_codes[$applicants_list[$i]['DispositionCode']];
}

$left_nav_info = $PaginationObj->getPageNavigationInfo($start, $limit, $applicants_count, '', '');

// Applications list
if(!isset($_REQUEST['Export'])) {
    echo json_encode(array(
        "applicants_list"           =>  $applicants_list,
        "previous"                  =>  $left_nav_info['previous'],
        "next"                      =>  $left_nav_info['next'],
        "total_pages"               =>  $left_nav_info['total_pages'],
        "current_page"              =>  $left_nav_info['current_page'],
        "applicants_count"          =>  $applicants_count,
        "count_per_page"            =>  $applicants_list['count'],
    ));
}
?>
