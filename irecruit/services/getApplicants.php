<?php
require_once '../Configuration.inc';

$FilterOrganization     =   $_REQUEST['FilterOrganization'];

//Set limit
$limit = $user_preferences['ReportsSearchResultsLimit'];
if(isset($_REQUEST['RecordsLimit']) && $_REQUEST['RecordsLimit'] != "") $limit = $_REQUEST['RecordsLimit'];
$limit  = 100;
$start  =   0;
if (isset ( $_REQUEST ['IndexStart'] ) && is_numeric ( $_REQUEST ['IndexStart'] )) {
    $start  =   $_REQUEST ['IndexStart'];
}

//Get ApplicantDispositionCodes
$disposition_results    = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'');
$disposition_rows       = $results['count'];
$disposition_codes      = array();

if ($disposition_rows > 0) {
    if(is_array($disposition_results['results'])) {
        foreach ($disposition_results['results'] as $row) {
            $disposition_codes[$row ['Code']] = $row ['Description'];
        }
    }
} // end if disposition set up

require_once IRECRUIT_DIR . 'services/GetApplicants.inc';

############        Export Code     ############
if(isset($_REQUEST['PageSource']) && $_REQUEST['PageSource'] != "") {
    if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES" && $_REQUEST['PageSource'] == "ApplicantsStatusByRequisition") {
        $list = $applicants_list;
        require_once IRECRUIT_DIR . 'reports/ExportApplicantsStatusByRequisition.inc';
    }
    else if(isset($_REQUEST['Export']) && $_REQUEST['Export'] == "YES" && $_REQUEST['PageSource'] == "ApplicantsByReferralSource") {
        $list = $applicants_list;
        require_once IRECRUIT_DIR . 'reports/ExportApplicantsByReferralSource.inc';
    }
}
############        Export Code     #############
?>
