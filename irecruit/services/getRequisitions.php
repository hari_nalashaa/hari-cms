<?php 
//include configuration
require_once '../Configuration.inc';

//Set active
$Active =   isset($_REQUEST['Active']) ? $_REQUEST['Active'] : 'Y';
$Limit  =   isset($_REQUEST['Limit']) ? $_REQUEST['Limit'] : '10';

$IsRequisitionApproved = "Y";
if($Active == "NA") {
    $Active = "N";
    $IsRequisitionApproved = "N";
}

require_once IRECRUIT_DIR . 'services/GetRequisitions.inc';
?>