<?php
$PAGE_TYPE	= "Default";
$IRECRUIT_USER_AUTH	=	FALSE;
require_once 'Configuration.inc';


if(isset($_REQUEST['type']) && $_REQUEST['type']=='resend'){ 

 				$columns = "TwofAuthToken,EmailAddress,FirstName";
				$where = array ("UserID = :UserID");
				$params = array (":UserID" =>$_SESSION['userid_2fa']);
				$results = $IrecruitUsersObj->getUserInformation ( $columns, $where, "", array ($params) );


                         //random number generate for 2fa
			 $a = mt_rand(100000,999999); 
			 

			 $message    =   "\n Hello ".$results['results'][0]['FirstName'].",<br/><br/>";
			 $message   .=   "\n\n  Your verification code is: <b>".$a."</b><br><br>";
			 $message    .=   "\n\n Please enter the code on iRecruit's login page."."<br>";
			 $message    .=   "\n\n\n ".IRECRUIT_HOME."twofauth.php<br>";
			 $message    .=   "<br><br>";
			 $message    .=   "\n\n\n Thanks,"."<br>";
			 $message    .=   "\n\n iRecruit Support"."<br>";
			 $message    .=   "\n http://help.irecruit-us.com/2FA";


			 //save otp into table  
			 $IrecruitUsersObj->updateUserOtp($_SESSION['userid_2fa'],$a);  
			 $PHPMailerObj->clearCustomProperties();
			 $PHPMailerObj->addAddress ($results['results'][0]['EmailAddress']);
			 $PHPMailerObj->setFrom('support@irecruit-software.com');
			 $PHPMailerObj->Subject = 'iRecruit - Account Verification';
			 $PHPMailerObj->msgHTML ($message);
			 $PHPMailerObj->ContentType = 'text/plain';
			 //Send email
			 $PHPMailerObj->send ();


	}
if (isset($_POST['otp']) && !empty($_POST['otp']) && isset($_POST['userid'])) {
  session_start(); $_SESSION['userid_2fa'];

                                $columns = "TwofAuthToken";
				$where = array ("UserID = :UserID");
				$params = array (":UserID" =>$_SESSION['userid_2fa']);
				$results = $IrecruitUsersObj->getUserInformation ( $columns, $where, "", array ($params) );


if(isset($results['results'][0]['TwofAuthToken']) && $results['results'][0]['TwofAuthToken'] == $_POST['otp']){ 
$IRECRUIT_USER_AUTH	=	TRUE; 
$IrecruitUsersObj->updateUserOtp($_SESSION['userid_2fa'],'');  

// randomize a session key
	$SESSIONID = $GenericLibraryObj->generateRandomSessionKey();

	if (isset($_COOKIE ['ID']) && $_COOKIE ['ID'] != "") {
		$SESSIONID = $_COOKIE ['ID'];
	}

	if (isset($keep) && $keep == "yes") {
		$keeper = ", Persist = 1";
	} else {
		$keeper = '';
	}
    
	if($SESSIONID != "") {
	    // add to database
	    $IrecruitUsersObj->updateUserAccessSession($SESSIONID, $_SESSION['userid_2fa'], $keeper);
	    
	    // set session key on users browser
	    if ($AUTH ['Persist'] == 1) {
	        setcookie ( "ID", $SESSIONID, time () + 2592000, "/" );
	    } else {
	        setcookie ( "ID", $SESSIONID, time () + 7200, "/" );
	    }
	} 
	if(isset($_SESSION['goto'])&& !empty($_SESSION['goto'])){

			header ( 'Location: ' .$_SESSION['goto']);
			exit ();


	}else{
			header ( 'Location: ' .IRECRUIT_HOME);
			exit ();
	}
}else{
 ?> <script>alert('Invalid OTP ');</script> <?php 
}

}


// Set page title
$TemplateObj->title	=	"iRecruit Twofactor Auth";
    
echo $TemplateObj->displayIrecruitTemplate('views/twofauth');
?>
