<?php
//Get Organization Information
$OrgData = $OrganizationDetailsObj->getOrganizationInformation($OrgID, "", "*");

$MAddress = array (
    $OrgData ['Address1'],
    $OrgData ['Address2'],
    $OrgData ['City'],
    $OrgData ['State'],
    $OrgData ['ZipCode']
);
$CAddress = array (
    $OrgData ['ContactAddress'],
    $OrgData ['ContactAddress2'],
    $OrgData ['City'],
    $OrgData ['State'],
    $OrgData ['ZipCode']
);

// It will unset the null values, it will make a clean array
$MAddress = array_filter ( $MAddress );
$CAddress = array_filter ( $CAddress );

$MyOfficeAddress = implode ( ",", $MAddress );
$CorporateAddress = implode ( ",", $CAddress );

// Set where condition
$where_app_process = array ("OrgID = :OrgID","Active = 'Y'");
// Set parameters
$params_app_process = array (":OrgID" => $OrgID);
// Get Applicant Process Flow Information
$result_app_process = $ApplicantsObj->getApplicantProcessFlowInfo ( "*", $where_app_process, 'ProcessOrder', array ($params_app_process) );
$statuslist = $result_app_process ['results'];

// Get Applicant Disposition Codes List
$result_dis_codes = $ApplicantsObj->getApplicantDispositionCodes ( $OrgID, 'Y' );
$dispositioncodes = $result_dis_codes ['results'];

// Get Login User information
$userinfo   = $IrecruitUsersObj->getUserInfoByUserID ( $USERID, "*" );
$UserEmail  = $userinfo ['EmailAddress'];

if(is_object($TemplateObj)) {
	$TemplateObj->statuslist       =   $statuslist;
	$TemplateObj->dispositioncodes =   $dispositioncodes;
	$TemplateObj->MyOfficeAddress  =   $MyOfficeAddress;
	$TemplateObj->CorporateAddress =   $CorporateAddress;
	$TemplateObj->userinfo         =   $userinfo;
	$TemplateObj->UserEmail        =   $UserEmail;
}
?>
