<?php 
require_once 'Configuration.inc';

//set page title
include 'formsInternal/FormsInternalTitle.inc';

//Load scripts in header
$scripts_header[] = "js/loadAJAX.js";
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$TemplateObj->page_scripts_header = $scripts_header;

//Load scripts in footer
$scripts_footer[] = "js/index.js";
$scripts_footer[] = "js/irec_Display.js";
$scripts_footer[] = "js/forms-internal.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

$script_vars_footer[]	= 'var datepicker_ids = "#CUST54512e0a1dfb4";';
$script_vars_footer[]	= 'var date_format = "mm/dd/yy";';
$TemplateObj->scripts_vars_footer = $script_vars_footer;

//Process Prefilled Forms
if ($typeform == "prefilledforms" && $_POST ['process'] == "Y") {

    //set where condition
    $where	=	array("OrgID = :OrgID");
    //set parameters
    $params =	array(":OrgID"=>$OrgID);
    //Delete PrefilledForms Information
    G::Obj('Forms')->delFormsInfo('PreFilledForms', $where, array($params));

    foreach ( $_POST ['form'] as $key => $value ) {
        if ($value != "") {
            //set columns
            $columns	=	"'$OrgID' OrgID, PreFilledFormID, TypeForm, Form, FormName, PresentedTo, '$statuslevel' AssignStatus, '$CompleteByInterval' CompleteByInterval, '$CompleteByFactor' CompleteByFactor, SortOrder";
            //set where condition
            $where		=	array("OrgID = 'MASTER'", "PreFilledFormID = :PreFilledFormID");
            //set parameters
            $params		=	array(":PreFilledFormID"=>$value);
            	
            //Insert PreFilled Forms Information
            G::Obj('FormQuestions')->insFormQuestionsMaster('PreFilledForms', $columns, $where, array($params));
        } // end if
    } // end foreach

    $TemplateObj->msg = $msg = "Your PreFilled form selections have been updated.";

} // end process

if($typeform == "packages"){ 
 
if ($_REQUEST['active'] == "n") { 
	$Active = 'N'; 
	$a = 'n'; 
} else if ($_REQUEST['active'] == "p") { 
	$Active = 'P'; 
	$a = 'p'; 
} else { 
	$Active = 'Y'; 
	$a = 'y'; 
}

$checklist_res       =   array();
$package_info      =   G::Obj('IconnectPackagesHeader')->geticonnectPackagesHeaderByOrgID($OrgID,'Active');
$checklist_res       =   json_decode($checklist_info['StatusCategory'], true);
$column = '*';
$where          =   array ("OrgID = :OrgID","Active = 'Y'");
$params         =   array (":OrgID" => $OrgID);
$get_applicant_process_info      =   G::Obj('Applicants')->getApplicantProcessFlowInfo($columns, $where, 'ProcessOrder asc', array($params) );
$get_applicant_process_results = $get_applicant_process_info['results'];
$get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);
$TemplateObj->package_info  =   $package_info['results'];
$TemplateObj->get_applicant_process_results  =   $get_applicant_process_results;
$TemplateObj->get_checklist_process_data  =   $get_checklist_process_data;

echo $TemplateObj->displayIrecruitTemplate ( 'views/requisitions/Iconnectpackage' );

}else{
echo $TemplateObj->displayIrecruitTemplate('views/formsInternal/FormsInternal');
}
?>
