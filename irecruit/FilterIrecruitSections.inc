<?php
$irecruit_sections_list	=   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

$section_forms          =   array(
                                "1"     =>  "frmPersonalInfo",
                                "2"     =>  "frmAvailabilityForWorkInfo",
                                "3"     =>  "frmBackgroundInfo",
                                "4"     =>  "frmMrpInfo",
                                "5"     =>  "frmDriversRecordInfo",
                                "6"     =>  "frmAttachmentsInfo",
                                "7"     =>  "frmCRTInfo",
                                "8"     =>  "frmREFInfo",
                                "9"     =>  "frmEDUInfo",
                                "10"    =>  "frmSecurityInfo",
                                "11"    =>  "frmCurrentEmploymentInfo",
                                "12"    =>  "frmSocialMediaProfilesInfo",
                                "13"    =>  "frmMilitaryExperienceInfo",
                            );

$section_forms_keys     =   array_keys($section_forms);

foreach($irecruit_sections_list as $irecruit_section_id=>$irecruit_section_info) {
    $section_title  =   preg_replace("/[^A-Za-z]/", '', $irecruit_section_info['SectionTitle']);
    
    if(!in_array($irecruit_section_id, $section_forms_keys)) {
        $section_forms[$irecruit_section_id] =   "frm".$section_title;
    }
}
?>