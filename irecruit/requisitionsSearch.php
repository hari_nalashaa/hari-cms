    <?php
require_once 'Configuration.inc';

$inventory_details = array ();

include_once IRECRUIT_DIR . 'requisitions/RequisitionsPageTitles.inc';
include_once COMMON_DIR . 'listings/SocialMedia.inc';
 $active_tab = "view-requisition";
if(isset($_REQUEST['active_tab']) && $_REQUEST['active_tab'] != "") {
	$active_tab = $_REQUEST['active_tab'];
}

$TemplateObj->active_tab = $active_tab;

//Set page title
$TemplateObj->title	= $title;
$TemplateObj->GuIDRequisition = $GuIDRequisition = $GenericLibraryObj->getGlobalUniqueID($OrgID);


$new_job_image = '';
$hot_job_image = '';
// update new job status
$highlight_req_settings =   G::Obj('HighlightRequisitions')->getHighlightRequisitionSettings($OrgID);

if($highlight_req_settings['HighlightNewJob'] == "Yes") {
    $new_job_days = $highlight_req_settings['NewJobDays'];
    $new_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['NewJobIcon']);

    if($new_job_icon != "") {
        $new_job_image = '<img src="'.IRECRUIT_HOME.'vault/highlight/'.$new_job_icon.'">';
    }
}

if($highlight_req_settings['HighlightHotJob'] == "Yes") {
    $hot_job_icon = $HighlightRequisitionsObj->getJobImage($highlight_req_settings['HotJobIcon']);

    if($hot_job_icon != "") {
        $hot_job_image = '<img src="'.IRECRUIT_HOME.'vault/highlight/'.$hot_job_icon.'">';
    }
}


//Include configuration related javascript in header
$scripts_header[] = "tiny_mce/tinymce.min.js";
$scripts_header[] = "js/irec_Textareas.js";
$scripts_header[] = "js/irec_Display.js";
$scripts_header[] = "js/loadAJAX.js";

// Assign add applicant datepicker variables
$script_vars_header [] = 'var org_id = "'.$OrgID.'";';
$TemplateObj->script_vars_header = $script_vars_header;

$TemplateObj->page_scripts_header = $scripts_header;

//Set footer scripts
$scripts_footer[] = "js/requisition.js";
$scripts_footer[] = "js/requisitions-search.js";

// Assign add applicant datepicker variables
$script_vars_footer [] = 'var datepicker_ids = "#txtRequisitionStageDate";';
$script_vars_footer [] = 'var date_format = "mm/dd/yy";';
$script_vars_footer [] = 'var highlight_hot_jobs = "'.$highlight_req_settings['HighlightHotJob'].'";';
$script_vars_footer [] = "var new_job_image = '".$new_job_image."';";
$script_vars_footer [] = "var hot_job_image = '".$hot_job_image."';";

$TemplateObj->scripts_vars_footer = $script_vars_footer;

$TemplateObj->page_scripts_footer = $scripts_footer;

$page_styles["header"][] = 'css/requisitions-search-print.css';

//Set page styles information
$TemplateObj->page_styles =  $page_styles;

$Active = 'Y';
if(isset($_REQUEST['Active']) && $_REQUEST['Active'] != "") {
	$Active = $_REQUEST['Active'];
}

$TemplateObj->Active = $Active;

$TemplateObj->MonsterJobIndustries 	= $MonsterJobIndustries = $MonsterObj->getMonsterJobIndustries();
$TemplateObj->JobCategories 		= $JobCategories 		= $MonsterObj->monsterJobCategories("yes");
$TemplateObj->JobOccupations 		= $JobOccupations 		= $MonsterObj->getMonsterJobCategoriesOccupations();

// update new job status
$highlight_req_settings                 = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
$TemplateObj->highlight_req_settings    = $highlight_req_settings;

$TemplateObj->new_job_image = $new_job_image;
$TemplateObj->hot_job_image = $hot_job_image;

if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") { 
	require_once IRECRUIT_DIR . 'requisitions/GetRequisitionInfo.inc';
} else { 
	require_once IRECRUIT_DIR . 'requisitions/GetRequisitions.inc';
}

echo $TemplateObj->displayIrecruitTemplate('views/requisitions/RequisitionsSearch');

?>
