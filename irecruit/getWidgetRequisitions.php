<?php
require_once 'Configuration.inc';

// Requisitions
$JSON['DataType'] = 'REQUISITIONS';
$JSON['Active'] = 'Y';
$JSON['LIMIT'] = " LIMIT 5";
if ($_GET['lbl'] != "") {
    if ($_GET['lbl'] == "jobtitle") {
        $JSON['ORDER_BY'] = "Title";
        $JSON['SORT'] = $_GET['sort'];
    }
    if ($_GET['lbl'] == "dateopened") {
        $JSON['ORDER_BY'] = "date_format(PostDate,'%m/%d/%Y')";
        $JSON['SORT'] = $_GET['sort'];
    }
}

$resjson = $RequisitionsObj->getRequisitions(json_encode($JSON));
$requisitions = json_decode($resjson, true);

for ($r = 0; $r < count($requisitions); $r ++) {
    ?>
    <tr>
    	<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?>><a href="<?php echo IRECRUIT_HOME;?>requisitionsSearch.php?menu=3&RequestID=<?php echo $requisitions[$r]['RequestID'];?>&Active=Y"><?php echo $requisitions[$r]['Title']?></td>
    	<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?> width="13%" align="center"><?php echo $requisitions[$r]['PostDate']?></td>
    	<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?> width="5%" align="center"><?php echo $requisitions[$r]['ApplicantsCount']?></td>
    	<td <?php if($requisitions[$r]['ExpireDays'] <= 7) echo 'style="color:red";';?> width="13%" align="center"><?php echo $requisitions[$r]['ExpireDays'];?></td>
    </tr>
    <?php
}

if (count($requisitions) > 0) {
    ?>
    <tr>
    	<td align="right" colspan="4"><a href="<?php echo IRECRUIT_HOME;?>requisitions.php?Active=Y">View More>></a></td>
    </tr>
    <?php
}
?>