<?php
require_once 'Configuration.inc';

require_once 'forms/FormsPageTitle.inc';

// Set page title
$TemplateObj->title = $title;

// Include configuration related javascript in footer
$scripts_footer [] = "js/forms.js";
$TemplateObj->page_scripts_footer = $scripts_footer;

// Include configuration related javascript in header
$scripts_header [] = "tiny_mce/tinymce.min.js";
$scripts_header [] = "js/irec_Textareas.js";
$TemplateObj->page_scripts_header = $scripts_header;

if ($_REQUEST ['form'] != '' && $_REQUEST ['action'] == 'formquestions' && $_REQUEST ['new'] == 'add') {
	
	// Set Columns
	$columns   =   array ('MAX(QuestionOrder) as QuestionOrder');
	// set condition
	$where     =   array ("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID");
	// set parameters
	$params    =   array (":OrgID" => $OrgID, ":FormID" => $_REQUEST ['form'], ":SectionID" => $section);
	// Get FormQuestionsInformation
	$results   =   G::Obj('FormQuestions')->getFormQuestionsInformation ( $columns, $where, '', array ($params) );
	
	$row_que_order = $results ['results'] [0];
	$QuestionOrder = $row_que_order ['QuestionOrder'] + 1;
	
	$QuestionID = "CUST" . uniqid ();
		
	// Set Insert Information
	$fq_info   =   array(
                        "OrgID"             =>  $OrgID,
                        "FormID"            =>  $_REQUEST ['form'],
                        "QuestionID"        =>  $QuestionID,
                        "Question"          =>  'New Question Added.',
                        "QuestionTypeID"    =>  '100',
                        "size"              =>  '0',
                        "maxlength"         =>  '0',
                        "rows"              =>  '2',
                        "cols"              =>  '2',
                        "wrap"              =>  '',
                        "value"             =>  '',
                        "defaultValue"      =>  '',
                        "Active"            =>  'Y',
                        "SectionID"         =>  $section,
                        "QuestionOrder"     =>  $QuestionOrder,
                        "Required"          =>  '',
                        "Validate"          =>  '' 
                    );
	
	// Insert Form Questions
	$result_custom_question = G::Obj('FormQuestions')->insFormQuestions ( 'FormQuestions', $fq_info );
	
	header ( "Location:forms.php?form=" . $_REQUEST ['form'] . "&action=formquestions&section=" . $section );
	exit ();
}

// EDIT A FORM
if (isset ( $_REQUEST ['form'] ) 
    && $_REQUEST ['form'] != "" 
    && isset ( $_REQUEST ['delete'] ) 
    && $_REQUEST ['delete'] != "") {
	
	// Delete Form Questions
	$where         =   array ("FormID = :FormID", "QuestionID = :QuestionID");
	// set parameters
	$params        =   array (":FormID" => $_REQUEST ['form'], ":QuestionID" => $_REQUEST ['delete']);
	// Delete FormQuestions Information
	$res_form_ques =   G::Obj('FormQuestions')->delQuestionsInfo ( 'FormQuestions', $where, array ($params) );
	
	header ( "Location:forms.php?form=" . $_REQUEST ['form'] . "&action=formquestions&section=" . $section );
	exit ();
}

echo $TemplateObj->displayIrecruitTemplate ( 'views/forms/Forms' );
?>