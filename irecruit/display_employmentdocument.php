<?php
require_once 'Configuration.inc';

if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") {
	$OrgID = isset($_REQUEST['OrgID']) ? $_REQUEST['OrgID'] : '';
}
$UpdateID = isset($_REQUEST['UpdateID']) ? $_REQUEST['UpdateID'] : '';

if (($OrgID) && ($UpdateID)) {
	//Set where condition
	$where		=	array("OrgID = :OrgID", "UpdateID = :UpdateID");
	//Set parameters
	$params		=	array(":OrgID"=>$OrgID, ":UpdateID"=>$UpdateID);
	//Get Correspondence Attachments Information
	$results	=	G::Obj('Correspondence')->getCorrespondenceAttachments('Filename', $where, '', array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $formdoc) {
		
			$filename = $formdoc ['Filename'];
		
			$dfile = IRECRUIT_DIR;
		
			$link = 'vault/' . $OrgID . '/employmentdocuments/attachments/';
			$link .= $filename;
		
			$dfile .= $link;
			$file  = IRECRUIT_HOME . $link;
		}
	}
	
	if (file_exists ( $dfile )) {
		header ( 'Location: ' . $file );
	}
} // end if OrgID, and UpdateID
?>