<?php
$ThrowAuth = 0;
$USERID = "";

// general lockout for time
$res_user_lockout_time = $IrecruitUsersObj->updateUserSessionLockOutTime();

if (!isset ( $_COOKIE ['ID'] )) {
	$ThrowAuth ++;
} elseif (isset ( $_COOKIE ['ID'] ) && ($_COOKIE ['ID'] == "")) {
	$ThrowAuth ++;
} else {
	$fields_list	=	array('UserID', 'CAST(Persist as unsigned int)');
	$AUTH 			= 	$IrecruitUsersObj->getUserInfoByCookie($fields_list);
	
	$USERID  = $AUTH [0];
	$Persist = $AUTH [1];
}

if (! $USERID) {
	$ThrowAuth ++;
}

if ($ThrowAuth >= 1) {
	$loc = $_SERVER ['SCRIPT_NAME'] . '?' . $_SERVER ['QUERY_STRING'];
	header ( 'Location: ' . IRECRUIT_HOME . 'login.php?' . $loc );
	exit ();
} else {
	$IrecruitUsersObj->updateUserAccess($USERID);

	if ($Persist == 1) {
		setcookie ( "ID", $_COOKIE ['ID'], time () + 2592000, "/" );
	} else {
		setcookie ( "ID", $_COOKIE ['ID'], time () + 7200, "/" );
	}
}
?>
