<?php
/**
 * Main Menu Category - Home
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 * 
 */
$navigation_menu ["Home"] ["Dashboard"] = IRECRUIT_HOME;

$navigation_menu ["Home"] ["Career Center"] = IRECRUIT_HOME . 'careerCenter.php';

$navigation_menu ["Home"] ["iRecruit News"] = 'https://www.irecruit-software.com/category/irecruit-blog/';
// Set attributes for menu
$navigation_menu ["Home"] ["Attributes"] ["iRecruit News"]  = array (
		"target" => "_blank" 
);

$navigation_menu ["Home"] ["Useful Links"] = 'https://help.irecruit-us.com/useful-links/';
// Set attributes for menu
$navigation_menu ["Home"] ["Attributes"] ["Useful Links"]  = array (
    "target" => "_blank"
);
$navigation_menu ["Home"] ["Email Support"] = 'mailto:lstrong@cmshris.com?subject=iRecruit Support';

$navigation_menu ["Home"] ["Help File"] = 'https://help.irecruit-us.com/';
// Set attributes for menu
$navigation_menu ["Home"] ["Attributes"] ["Help File"]  = array (
		"target" => "_blank"
);

$navigation_menu ["Home"] ["Custom Links"] = IRECRUIT_HOME . 'customLinks.php';	

/**
 * Main Menu Category - Applicant Management
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 * 
 */
if (isset ( $permit ['Applicants'] ) && $permit ['Applicants'] == 1) {
	
	$navigation_menu ["Applicant Management"] ["Add Applicant"]            = IRECRUIT_HOME . 'applicants/addApplicant.php?menu=2';
	$navigation_menu ["Applicant Management"] ["Applicant Search"]         = IRECRUIT_HOME . 'applicantsSearch.php?menu=2';
	$navigation_menu ["Applicant Management"] ["Applicants by Status"]     = IRECRUIT_HOME . 'applicants.php?action=status&menu=2';
	$navigation_menu ["Applicant Management"] ["Maintain Saved Searches"]  = IRECRUIT_HOME . 'applicants.php?action=maintainsearch&menu=2';
	
    if($feature['MatrixCare'] == 'Y') {
        $navigation_menu ["Applicant Management"] ["MatrixCare Onboard List"] = IRECRUIT_HOME . 'applicants.php?action=MatrixCare&OnboardSource=MatrixCare&menu=2';
    }	
    if($feature['WebABA'] == 'Y') {
        $navigation_menu ["Applicant Management"] ["WebABA Onboard List"] = IRECRUIT_HOME . 'applicants.php?action=WebABA&OnboardSource=WebABA&menu=2';
    }
    if($feature['Lightwork'] == 'Y') {
        $navigation_menu ["Applicant Management"] ["Lightwork Onboard List"] = IRECRUIT_HOME . 'applicants.php?action=Lightwork&OnboardSource=Lightwork&menu=2';
    }
    if($feature['Abila'] == 'Y') {
        $navigation_menu ["Applicant Management"] ["Abila Onboard List"] = IRECRUIT_HOME . 'applicants.php?action=Abila&OnboardSource=Abila&menu=2';
    }
    if($feature['Mercer'] == 'Y') {
        $navigation_menu ["Applicant Management"] ["Mercer Onboard List"] = IRECRUIT_HOME . 'applicants.php?action=Mercer&OnboardSource=Mercer&menu=2';
    }
    if($feature['DataManagerType'] != 'N') {
        $navigation_menu ["Applicant Management"] ["Data Manager Onboard List"] = IRECRUIT_HOME . 'applicants.php?action='.$feature['DataManagerType'].'&OnboardSource='.$feature['DataManagerType'].'&menu=2';
    }
	
	if ($BackgroundCheckLinksObj->displayNavigation == "on") {
		$navigation_menu ["Applicant Management"] [$BackgroundCheckLinksObj->navigationTitle] = IRECRUIT_HOME . 'applicants.php?action=backgroundcheck&menu=2';
		// Set attributes for menu
		$navigation_menu ["Applicant Management"] ["Attributes"] [$BackgroundCheckLinksObj->navigationTitle]  = array (
				"target" => $BackgroundCheckLinksObj->navigationTarget 
		);
	}
	
	$navigation_menu ["Applicant Management"] ["Appointment Calendar"]             = IRECRUIT_HOME . 'applicants.php?action=' . $calenderview . '&menu=2';
	
}



/**
 * Main Category - Requisition Management
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 * 
 */

if ($permit ['Requisitions_Edit'] == 1) {
	$disre = 'Manage';
} else {
	$disre = 'View';
}
if ($permit ['Requisitions'] == 1) {
	if ($feature ['RequisitionRequest'] == "Y") {
		$RequisitionType = "Request";
	} else { // else feature
		$RequisitionType = "Requisition";
	} // end permid feature
	

	if ($permit ['Requisitions_Edit'] == 1 && $feature ['RequisitionRequest'] != "Y") {
		$navigation_menu ["Requisition Management"] ["Create New " . $RequisitionType] = IRECRUIT_HOME . 'requisitions/createRequisition.php?action=new&menu=3';
	}
	if (($feature ['RequisitionRequest'] == "Y")) {
		$navigation_menu ["Requisition Management"] ["Create New " . $RequisitionType] = IRECRUIT_HOME . 'request/createRequest.php?action=new&menu=3';
	}

	if (($feature ['RequisitionRequest'] == "Y")) {
		$navigation_menu ["Requisition Management"] ["Process Requests"] = IRECRUIT_HOME . 'request/requestsList.php';
	}

	if ($feature ['RequisitionApproval'] == "Y") {
		$navigation_menu ["Requisition Management"] ["Requisition Approval"] = IRECRUIT_HOME . 'request/needsApproval.php?menu=3';
	} // end feature

	
	$navigation_menu ["Requisition Management"] [$disre . ' Requisitions'] = IRECRUIT_HOME . 'requisitionsSearch.php?menu=3';
	
	
	
	$navigation_menu ["Requisition Management"] ["Advertise Requisitions"] = IRECRUIT_HOME . 'jobBoardPosting.php?menu=3';
	
} // end permit Requisitions


/**
 * Main Category - Application Forms
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 * 
 */

if ($permit ['Forms_Edit'] == 1) {
	$disaf = 'Manage';
} else {
	$disaf = 'View';
}
if ($permit ['Forms'] == 1) {
	
	$navigation_menu ["Application Forms"] [$disaf . " Application Forms"] = IRECRUIT_HOME . 'forms.php?menu=4';
	if ($permit ['Forms_Edit'] == 1) {
		$navigation_menu ["Application Forms"] ["Create New Form"] = IRECRUIT_HOME . 'forms.php?form=STANDARD&action=copyform&menu=4';
		$navigation_menu ["Application Forms"] ["Edit Standard Form"] = IRECRUIT_HOME . 'forms.php?form=STANDARD&action=formquestions&section=0&form_home=yes';
	}
}


/**
 * Main Category - iConnect
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 * 
 */

if (($feature ['WebForms'] == "Y") || ($feature ['AgreementForms'] == "Y") || ($feature ['SpecificationForms'] == "Y") || ($feature ['PreFilledForms'] == "Y") || ($feature ['Everify'] == "Y")) {
	
	if ($permit ['Internal_Forms'] == 1) {
		
		$navigation_menu ["iConnect"] ["Manage Internal Forms"] = IRECRUIT_HOME . 'formsInternal.php?typeform=manage&menu=5';

	        //$navigation_menu ["iConnect"] ["Packages"] = IRECRUIT_HOME . 'formsInternal.php?typeform=packages&menu=5';

		if ($feature ['WebForms'] == "Y") {
			$navigation_menu ["iConnect"] ["Web Forms"] = IRECRUIT_HOME . 'formsInternal.php?typeform=webforms&menu=5';
		}
		if ($feature ['AgreementForms'] == "Y") {
			$navigation_menu ["iConnect"] ["Agreement Forms"] = IRECRUIT_HOME . 'formsInternal.php?typeform=agreementforms&menu=5';
		}
		if ($feature ['SpecificationForms'] == "Y") {
			$navigation_menu ["iConnect"] ["Specification Forms"] = IRECRUIT_HOME . 'formsInternal.php?typeform=specforms&menu=5';
		}
		if ($feature ['PreFilledForms'] == "Y") {
			$navigation_menu ["iConnect"] ["Federal & State Forms"] = IRECRUIT_HOME . 'formsInternal.php?typeform=prefilledforms&menu=5';
		}
		
		$navigation_menu ["iConnect"] ["Form Status"] = IRECRUIT_HOME . 'formsStatus.php?menu=5';


	}
}


/**
 * Main Category - Correspondence
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 * 
 */

if ($permit ['Correspondence'] == 1) {
    $navigation_menu ["Correspondence"] ["Email Templates"]        =   IRECRUIT_HOME . 'correspondence.php?action=letters&menu=6';
    $navigation_menu ["Correspondence"] ["Attachments"]            =   IRECRUIT_HOME . 'correspondence.php?action=attachments&menu=6';
    $navigation_menu ["Correspondence"] ["Communication Packages"] =   IRECRUIT_HOME . 'correspondence.php?action=packages&menu=6';
} // end Correspondence



/**
 * Main Category - Reports
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 */
if ($permit ['Reports'] == 1) {

		/**
		 * Main Category - Reports
		 * Add Exporter as a report type
		 */
        $navigation_menu ["Reports"] ["Requisitions"] = IRECRUIT_HOME . 'reports/requisitionsByManagerOrOwner.php?menu=7';
    
        if ($feature ['RequisitionApproval'] == "Y") {
            $navigation_menu ["Reports"] ["Requisition Approvals"] = IRECRUIT_HOME . 'reports/requisitionApproval.php?menu=7';
        }

        $navigation_menu ["Reports"] ["Applicants Count"] = IRECRUIT_HOME . 'reports/getApplicantsCountByMonthAndYear.php?menu=7';
        
        $navigation_menu ["Reports"] ["Applicants Status By Requisition"] = IRECRUIT_HOME . 'reports/getApplicantsStatusByRequisition.php?menu=7';
        
        $navigation_menu ["Reports"] ["Applicants By Distance"] = IRECRUIT_HOME . 'reports/getApplicantsByDistance.php?menu=7';
        
        $navigation_menu ["Reports"] ["Applicant History"] = IRECRUIT_HOME . 'reports/applicantHistory.php?menu=7';
        
        $navigation_menu ["Reports"] ["Applicants Referral Source"] = IRECRUIT_HOME . 'reports/getApplicantsByReferralSource.php?menu=7';

        
        $navigation_menu ["Reports"] ["Time to fill"] = IRECRUIT_HOME . 'reports/timeToFill.php?menu=7';
        
        $navigation_menu ["Reports"] ["AAP"] = IRECRUIT_HOME . 'reports/applicantFlowLog.php?menu=7';

// Turn off Jaspersoft Reports 7/8/2020
// Turn on for Matanuska 9/1/2020
if ($OrgID == "I20100703") { // Matanuska Electric Association
    // CANADA Jaspersoft Report's don't point to Canada DB        
    if (SERVER != 'CANADA') {

		if (($feature ['WebForms'] == "Y") || ($feature ['AgreementForms'] == "Y") || ($feature ['SpecificationForms'] == "Y") || ($feature ['PreFilledForms'] == "Y") || ($feature ['Everify'] == "Y")) {
		    $navigation_menu ["Reports"] ["iConnect Form Status"] = IRECRUIT_HOME . 'reports.php?action=if&menu=7';
		}
		
		$navigation_menu ["Reports"] ["iRecruit Activity"] = IRECRUIT_HOME . 'reports.php?action=ia&menu=7';
		$navigation_menu ["Reports"] ["iRecruit Configuration"] = IRECRUIT_HOME . 'reports.php?action=ic&menu=7';

    } // end if not CANADA
} // end Matanuska Electric Association

		if (($feature ['Exporter'] == "Y") && ($permit ['Exporter'] == 1)) {
		    $navigation_menu ["Reports"] ["Exporter"] = IRECRUIT_HOME . 'reports.php?action=exporter&menu=7#openFormWindow';
		}
		


		$navigation_menu["Reports"]["Job Board Status"] = IRECRUIT_HOME . 'reports/jobBoardStatus.php?menu=7';

		$navigation_menu["Reports"]["Complete and Incomplete Applications"] = IRECRUIT_HOME . "reports/completeIncompleteApplications.php?menu=7";

		
		//Get Twilio Account Information
		$account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
		
		if($account_info['AccountSid'] != "") {
		    $navigation_menu["Reports"]["Text Applicants"] = IRECRUIT_HOME . "twilio/reports/conversationsReport.php?menu=7";
		}

		if ($OrgID == 'I20200305') {
		  $navigation_menu["Reports"]["Requisition Export"] = IRECRUIT_HOME . "reports/piperExport.php?menu=7";
		}
		
} // end permit

if($feature['ComparativeAnalysis'] == "Y") {

    $navigation_menu ["Comparative Analysis"] ["Comparative Search"]        = IRECRUIT_HOME . 'applicants/comparativeSearch.php?menu=10';
    $navigation_menu ["Comparative Analysis"] ["Comparative Analysis"]      = IRECRUIT_HOME . 'applicants/comparativeAnalysis.php?menu=10';
    $navigation_menu ["Comparative Analysis"] ["Comparative Tags"]          = IRECRUIT_HOME . 'applicants/comparativeLabelApplicants.php?menu=10';
    $navigation_menu ["Comparative Analysis"] ["Configuration"]             = IRECRUIT_HOME . 'applicants/comparativeAnalysisConfig.php?menu=10';
    $navigation_menu ["Comparative Analysis"] ["Weighted Search"]           = IRECRUIT_HOME . 'applicants.php?action=weightedsearch&menu=10&wlink_from=leftnav';
    
}

/**
 * Main Category - Administration
 * @tutorial	Array key order as main category followed by subcategory and the value is treated as menu link
 */

if (substr ( $USERROLE, 0, 21 ) == 'master_admin') {
    $navigation_menu ["Administration"] ["Administration"] = IRECRUIT_HOME . 'adminDashboard.php?menu=8';
}

if ($feature ['MultiOrg'] == "Y") {

	// Set where condition
	$where		=	array("OrgID = :OrgID");
	// Set parameters
	$params		=	array(":OrgID" => $OrgID);
	// Get Organization Data Information
	$results	=	G::Obj('Organizations')->getOrgDataInfo("*", $where, 'MultiOrgID ASC', array($params));
	
	if (is_array($results['results'])) {
		foreach ($results['results'] as $OD) {
	
			//Get Wotc Ids list
			$wotcid_info = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID, OrganizationName", $OD['OrgID'], $OD['MultiOrgID']);

			if($wotcid_info['wotcID'] != "") {
		                $wotc_admin_link	=	WOTCADMIN_HOME.'index.php?wotcID='.$wotcid_info['wotcID'].'&acc=' . $_COOKIE['ID'];
				$navigation_menu["CMS WOTC"][$wotcid_info['OrganizationName'] . " Portal"] = $wotc_admin_link;
			}
	
		} // end foreach
	}
}
else {
	//Get Wotc Ids list
	$wotcid_info		=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID, OrganizationName", $OrgID, "");
	$wotc_admin_link	=	WOTCADMIN_HOME.'index.php?wotcID='.$wotcid_info['wotcID'].'&acc=' . $_COOKIE['ID'];

        if($wotcid_info['wotcID'] != "") {
          $navigation_menu ["CMS WOTC"]["CMS WOTC"] = $wotc_admin_link;
        }
}
?>
