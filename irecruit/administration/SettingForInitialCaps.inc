<?php
//Get onboard settings by orgid
$onboard_settings	=	G::Obj('OnboardSettings')->getOnboardSettingsInfoByOrgID($OrgID);

if(isset($_POST['btnInitialCaps']) && $_POST['btnInitialCaps'] == "Submit") {
	//Set to no if initial caps settings doesn't exist
	$initial_caps	=	($_POST['chkInitialCaps'] != "") ? $_POST['chkInitialCaps'] : "No";
	
	$info			=	array(
							"OrgID"			=>	$OrgID,
							"InitialCaps"	=>	$initial_caps,
						);
	
	//Insert and update onboard settings
	G::Obj('OnboardSettings')->insUpdOnboardSettings($info);
	
	//Redirect
	echo "<script type='text/javascript'>location.href='administration.php?action=settingforinitialcaps&menu=8&msg=suc';</script>";
	exit();
}
?>
<div class="table-responsive">
	<form name="frmSettingForInitialCaps" id="frmSettingForInitialCaps" method="post">
		<table cellspacing="3" cellpadding="5" class="table table-bordered">
			<?php
				if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
					?>
					<tr>
						<td colspan="2" style="color: blue">
							<?php echo "Successfully updated the settings.";?>
						</td>
					</tr>
					<?php
				}
			?>
			<tr>
				<td width="15%">
					Check to apply Initial Caps
				</td>
				<td>
					<input type="checkbox" name="chkInitialCaps" id="chkInitialCaps" <?php if($onboard_settings['InitialCaps'] == "Yes") echo 'checked="checked"';?> value="Yes">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="btnInitialCaps" id="btnInitialCaps" class="btn btn-primary" value="Submit">
				</td>
			</tr>
		</table>
		<input type="hidden" name="action" value="settingforinitialcaps">
	</form>
</div>