<?php
//Get Indeed Configuration information
$indeed_config_info =   $IndeedObj->getIndeedConfigInfo($OrgID, $_REQUEST['MultiOrgID']);

if(isset($_POST) 
    && count($_POST) > 0 
    && isset($action) 
    && $action == "indeedconfig") {

    $error  =   "no";
    if($_POST['ContactName'] != ""
        || $_POST['Email'] != ""
        || $_POST['Phone'] != "")
    {
        $error              =   "yes";
        $valid_required     =   "no";
        //Replace it with post values
        $indeed_config_info['Email']        =   $_POST['Email'];
        $indeed_config_info['Phone']        =   $_POST['Phone'];
        $indeed_config_info['ContactName']  =   $_POST['ContactName'];
        
        
        if(isset($_POST['ContactName'])
        && isset($_POST['Email'])
        && isset($_POST['Phone'])
        && $_POST['Phone'] != ""
            && $_POST['Email'] != ""
                && $_POST['ContactName'] != "") {
        
            $error  =   "no";
            $valid_required = "yes";
        }
        
        if(!filter_var($_POST['Email'], FILTER_VALIDATE_EMAIL)) {
            $error  =   "yes";
        }
    }
    
    //Update Indeed Configuration Information
    if($error == "no")
    {
        $IndeedObj->insUpdIndeedConfig($OrgID, $_REQUEST['MultiOrgID'], $_POST);
        
        echo '<script>';
        echo 'location.href="'.IRECRUIT_HOME.'administration.php?action=indeedconfig&menu=8&OrgID='.$OrgID.'&msg=suc"';
        echo '</script>';
    }
}
?>

<div class="row">
    <div class="col-lg-12">
        <?php 
            if(isset($_GET['msg']) && $_GET['msg'] != "" && !isset($error)) {
            	echo '<span style="color:blue">Indeed configuration updated successfully.</span>';
            	echo '<br><br>';
            }
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
	<span style="color:red;font-weight:bold">Important!</span>
        The use of Indeed Apply authorizes iRecruit to share the following data about the applicants status with Indeed. 
        Specifically, the Applicant Process Flow (Status) setting with date of change until a terminal (final) status is reached or the status hasn't changed for a minimum of 60 days. 
        This information is collected for Indeed's internal reporting purposes, it is not shared with the applicant.
        <br><br>
        <strong>Instructions:</strong><br>
	<div style="margin-left:20px;margin-top:10px;">
        <strong>Yes</strong> - Use Indeed Apply- will set all your job postings that are listed on Indeed to use the Indeed Apply option. 
        With Indeed Apply applicants don't need to leave Indeed to apply using their Indeed profiles. 
        It requires a short form of questions (rather than the STANDARD long application) that can list additional interview questions that you can ask. 
        Applicants that apply in this way can be filtered as a lead in iRecruit's search results.
        <br><br>
        <strong>No</strong> - will set up all your job posts that are listed on Indeed.com with a link that will point back to iRecruit for the applicant to fill out the iRecruit application form. "Apply on Company Site"
	</div>
        <br>
        <strong>Indeed is committed to the success of your advertising campaign. Please help us connect with you by sharing your phone number*.</strong>
	<br><br>
        <strong>Note:</strong><br>
        A member of Indeed's sales team will contact you to discuss your ad's performance and offer suggestions for optimization. <br>
        Please contact <a href="mailto:alliancessupport@indeed.com">sjisupport@indeed.com</a> with any questions or concerns.<br>
        
        <br>
        *By filling out the phone number field, and submitting the form: <br><br>
	<ul>
	<li>I confirm that I am the subscriber to the telephone number entered and I hereby consent to receive autodialed marketing and informational calls from Indeed at the telephone number provided above, including if this number is a wireless (cell phone) number.</li>
        <li>Agreement to the above is not a condition of purchase of any Indeed product.</li> 
        <li>I agree that this information will be dealt with in accordance with Indeed's <a href="https://www.indeed.com/legal?hl=en_US#cookies" target="_blank">Cookie</a> and <a href="https://www.indeed.com/legal?hl=en_US#privacy" target="_blank">Privacy Policy</a>, and <a href="https://www.indeed.com/legal?hl=en_US#tos" target="_blank">Terms of Service</a>.</li>
	</ul>
        <br><br>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
    
        <form method="post" name="frmIndeedConfigInfo" id="frmIndeedConfigInfo">
            <input type="hidden" name="action" id="action" value="indeedconfig">
            <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $_REQUEST['MultiOrgID'];?>">

            <div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="ddlIndeedConfig">Indeed Apply: </label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <select name="ddlIndeedConfig" id="ddlIndeedConfig" class="form-control width-auto-inline">
                        <option value="N" <?php if($indeed_config_info['IndeedApply'] == 'N') echo ' selected="selected"';?>>No</option>
                        <option value="Y" <?php if($indeed_config_info['IndeedApply'] == 'Y') echo ' selected="selected"';?>>Yes</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
            </div>
            
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <span style="font-size:14pt;font-weight:bold;">Indeed Account Information</span><br>
                    <i>Required in order to sponsor a job</i><br><br>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php
                    if(isset($error) && $error == "yes") {
                        if($valid_required == 'no') {
                        	echo '<span style="color:red">Required fields are missing.</span>';
                            echo '<br><br>';
                        }
                        else if($valid_required == 'yes') {
                        	echo '<span style="color:red">Valid email required.</span>';
                            echo '<br><br>';
                        }
                    }
                    ?>
                </div>
            </div>
            
            
            <div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="ContactName">Contact Name: <span style="color:red">*</span></label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
                    <input type="text" name="ContactName" id="ContactName" class="form-control width-auto-inline" value="<?php echo $indeed_config_info['ContactName'];?>">
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="Email">Email: <span style="color:red">*</span></label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">                    
                    <input type="text" name="Email" id="Email" class="form-control width-auto-inline" value="<?php echo $indeed_config_info['Email'];?>">
                </div>                    
            </div>
            
            <div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="Phone">Phone: <span style="color:red">*</span></label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">                    
                    <input type="text" name="Phone" id="Phone" class="form-control width-auto-inline" value="<?php echo $indeed_config_info['Phone'];?>">
                </div>
            </div>
            
            
            <div class="form-group"  style="margin-top:10px;">
                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Save">
            </div>
        </form>            
        
    </div>
</div>
