<?php
if ($_REQUEST['process'] == 'Y') {
	
	// Set Parameters
	$params = array (
			":OrgID" => $OrgID 
	);
	// Set where condition
	$where = array (
			"OrgID = :OrgID" 
	);
	// Delete AutoForwardList Information
	$FormFeaturesObj->delAutoForwardListInfo ( $where, array (
			$params 
	) );
	
	$i = 0;
	while ( $i <= 99 ) {
		$i ++;
		
		$email = "EmailAddress-" . $i;
		$first = "FirstName-" . $i;
		$last = "LastName-" . $i;
		
		if (isset($_POST [$email]) && $_POST [$email] != "") {
			
			$ins_email = is_null($_POST [$email]) ? '' : $_POST [$email];
			$ins_first = is_null($_POST [$first]) ? '' : $_POST [$first];
			$ins_last  = is_null($_POST [$last]) ? '' : $_POST [$last];
				
			
			// Set AutoforwardList Information
			$info = array (
					"OrgID" => $OrgID,
					"EmailAddress" => $ins_email,
					"FirstName" => $ins_first,
					"LastName" => $ins_last 
			);
			
			
			//Insert AutoforwardList Information
			$FormFeaturesObj->insAutoForwardListInfo ( $info );
		}
	} // end while
	
	
	//Set where condition
	$where = array("OrgID = :OrgID", "EmailAddress NOT IN (SELECT EmailAddress FROM AutoForwardList WHERE OrgID = :SOrgID)");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":SOrgID"=>$OrgID);
	//Delete RequisitionForward
	$RequisitionsObj->delRequisitionsInfo('RequisitionForward', $where, array($params));
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Auto Forward has been configured!')";
	echo '</script>';
} // end if process

echo '<form method="post" action="administration.php">';
echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';
echo '<tr><td colspan="100%" height="5"></td></tr>';

// Get AutoForwardList Information
$results = $FormFeaturesObj->getAutoForwardList ( $OrgID, "LastName, FirstName" );

$i = 1;
if (is_array ( $results ['results'] )) {
	foreach ( $results ['results'] as $row ) {
		echo '<tr>';
		echo '<td>Email Address: <input type="text" name="EmailAddress-' . htmlspecialchars($i) . '" value="' . $row ['EmailAddress'] . '" size=30 maxlength=60></td>';
		echo '<td>First Name: <input type="text" name="FirstName-' . htmlspecialchars($i) . '" value="' . $row ['FirstName'] . '" size=15 maxlength=30></td>';
		echo '<td>Last Name: <input type="text" name="LastName-' . htmlspecialchars($i) . '" value="' . $row ['LastName'] . '" size=30 maxlength=45></td>';
		echo '<td width="140">&nbsp;</td>';
		echo '</tr>';
		$i ++;
	}
}

$ii = $i + 2;
while ( $i < $ii ) {
	echo '<tr>';
	echo '<td>Email Address: <input type="text" name="EmailAddress-' . htmlspecialchars($i) . '" size=30 maxlength=60></td>';
	echo '<td>First Name: <input type="text" name="FirstName-' . htmlspecialchars($i) . '" size=15 maxlength=30></td>';
	echo '<td>Last Name: <input type="text" name="LastName-' . htmlspecialchars($i) . '" size=30 maxlength=45></td>';
	echo '<td>&nbsp;</td>';
	echo '</tr>';
	$i ++;
}
echo '<tr><td colspan="100%">';
echo '<p>To remove a selection; clear the "Email Address" field and select update.</p>
';
echo '</td></tr>';

echo '<tr><td align=center colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="autoforwardlist">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update Auto Forward List" class="btn btn-primary"></td></tr>';
echo '</form>';

echo '<tr><td colspan="100%">';
echo '<hr size=1>';
echo '<p><b>Preview</b></p>';

echo '<form method="post" action="administration.php">';
echo '<input type="hidden" name="action" value="autoforwardlist">';

echo '<table>';
include IRECRUIT_DIR . 'request/DisplayFormQuestions.inc';
echo displayAutoForwardList ( $OrgID, $RequestID );
echo '</table>';

echo '</form>';
echo '</td></tr>';
echo '</table></div>';
echo '<br><br>';
?>