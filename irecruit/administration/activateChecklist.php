<?php
include '../Configuration.inc';

if ($_GET['ChecklistID'] != "") {

    if ($_GET['activate'] == 'y') { $active = 'Y'; } else { $active = 'N'; }

    $ChecklistID    =   $_GET['ChecklistID'];
    $now            = date('Y-m-d H:i:s');
    $set_info       =   array("Active = :Active","LastUpdated = :LastUpdated");
    $where_info     =   array("OrgID = :OrgID", "ChecklistID = :ChecklistID");
    $params         =   array(":Active"=>$active, ":LastUpdated"=>$now, ":OrgID"=>$OrgID, ":ChecklistID"=>$ChecklistID);
    //Update Status Category Information
    G::Obj('Checklist')->updChecklist($set_info, $where_info, array($params));
}

    header("Location:".IRECRUIT_HOME."administration/checklist.php");
    exit;
?>
