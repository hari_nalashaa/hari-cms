<?php
if ($_REQUEST ['process'] == "Y") {
	
	// Set parameters
	$params = array (
			":OrgID" => $OrgID 
	);
	// Set where condition
	$where_info = array (
			"OrgID = :OrgID" 
	);
	// Delete EmailResponses Information
	$EmailObj->delEmailResponsesInfo ( $where_info, array (
			$params 
	) );
	
	// Form Comments
	$FormComments = is_null ( $_REQUEST ['FormComments'] ) ? '' : $_REQUEST ['FormComments'];
	
	// Information to insert into EmailResponses
	$info = array (
			"OrgID" => $OrgID,
			"ProcessOrder" => $_REQUEST ['approveProcessOrder'],
			"DispositionCode" => $_REQUEST ['approveDispositionCode'],
			"RejectProcessOrder" => $_REQUEST ['rejectProcessOrder'],
			"RejectDispositionCode" => $_REQUEST ['rejectDispositionCode'],
			"FormComments" => $FormComments 
	);
	// Insert EmailResponses Information
	$EmailObj->insEmailResponsesInfo ( $info );
} // end process
  
// Set parameters
$params = array (
		":OrgID" => $OrgID 
);
// Set where condition
$where_info = array (
		"OrgID = :OrgID" 
);
$results = $EmailObj->getEmailResponsesInfo ( "*", $where_info, "", array (
		$params 
) );
$ER = $results ['results'] [0];

echo '<br>Please select Codes for both the Approve and Reject settings for automatically updating applicants via email.<br>';

echo '<div class="table-responsive">';
echo '<table border="0" class="table table-striped table-bordered table-hover">';
echo '<form method="POST" action="administration.php">';
echo '<tr><td colspan="4" height="40" valign="bottom"><b>Approve</b></td></tr>';

$ADMINTYPE = "approve";
$ProcessOrder = $ER ['ProcessOrder'];
$DispositionCode = $ER ['DispositionCode'];

include IRECRUIT_VIEWS . 'applicants/StatusFormNoComment.inc';
echo '<tr><td colspan="4" height="40" valign="bottom"><b>Reject</b></td></tr>';

$ADMINTYPE = "reject";
$ProcessOrder = $ER ['RejectProcessOrder'];
$DispositionCode = $ER ['RejectDispositionCode'];

include IRECRUIT_VIEWS . 'applicants/StatusFormNoComment.inc';

echo '<tr><td colspan="4" height="40" valign="bottom">';
echo '<input type="checkbox" name="FormComments" value="Y"';
if ($ER ['FormComments'] == "Y") {
	echo ' checked';
}
echo '>';

echo '&nbsp;<b>Reply form with comments</b></td></tr>';

echo '<tr><td colspan="4" align="center" height="60" valign="bottom">';
echo '<input type="hidden" name="action" value="emailresponses">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update Email Responses" class="btn btn-primary">';
echo '</td></tr>';
echo '</table>';

echo '</div>';
?>