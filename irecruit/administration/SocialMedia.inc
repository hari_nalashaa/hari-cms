<?php
if ($_POST ["process"] == "Y") {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Delete Social Media Information
	$SocialMediaObj->delSocialMediaInfo($where, array($params));
	
	if ($_POST ["socialmediasharing"] != "") {
		
		if (($_POST ['facebook'] == "") && ($_POST ['twitter'] == "") && ($_POST ['linkedin'] == "")) {
			$_POST ['socialmediasharing'] = "N";
		}
		
		//Insert Social Media Information
		$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "Placement"=>$_POST ['socialmediasharing'], "Facebook"=>$_POST ['facebook'], "Twitter"=>$_POST ['twitter'], "LinkedIn"=>$_POST ['linkedin']);
		$SocialMediaObj->insSocialMediaInfo($info);
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('Social media settings configured!')";
		echo '</script>';
	} // end if
} // end process

// Get Social Media Information
$SM           =   G::Obj('SocialMedia')->getSocialMediaDetailInfo($OrgID, $MultiOrgID);
$socialmediasharing = $SM['Placement'];
$facebook = $SM['Facebook'];
$twitter = $SM['Twitter'];
$linkedin = $SM['LinkedIn'];


if (! $socialmediasharing) {
	$socialmediasharing = "N";
}
?>
<div class="table-responsive">
	<table width="100%" cellspacing="3" cellpadding="5"
		class="table table-striped table-bordered">
		<tr>
			<td height="5"></td>
		</tr>
		<tr>
			<td>
				<form method="post" action="administration.php">
					Where would you like to allow Social Media Sharing links: <br>&nbsp;&nbsp;
					<input type="radio" name="socialmediasharing" value="N"
						<?php if ($socialmediasharing == "N") { echo ' checked'; } ?>>None
					<input type="radio" name="socialmediasharing" value="P"
						<?php if ($socialmediasharing == "P") { echo ' checked'; } ?>>Public
					Listings <input type="radio" name="socialmediasharing" value="I"
						<?php if ($socialmediasharing == "I") { echo ' checked'; } ?>>iRecruit
					<input type="radio" name="socialmediasharing" value="B"
						<?php if ($socialmediasharing == "B") { echo ' checked'; } ?>>Both
					<br>
					<br> Which links would you like to display:<br> &nbsp;&nbsp;<input
						type="checkbox" name="facebook" value="Y"
						<?php if ($facebook == "Y") { echo ' checked'; } ?>>&nbsp;Facebook
					&nbsp;&nbsp;<input type="checkbox" name="twitter" value="Y"
						<?php if ($twitter == "Y") { echo ' checked'; } ?>>&nbsp;Twitter
					&nbsp;&nbsp;<input type="checkbox" name="linkedin" value="Y"
						<?php if ($linkedin == "Y") { echo ' checked'; } ?>>&nbsp;LinkedIn
					<br>
					<br>
					<br> <input type="hidden" name="action" value="socialmedia"> <input
						type="hidden" name="MultiOrgID" value="<?=$MultiOrgID?>"> <input
						type="hidden" name="process" value="Y"> <input type="submit"
						value="Update Social Media Settings" class="btn btn-primary">
				</form>
			</td>
		</tr>
	</table>
</div>
