<?php
require_once '../irecruitdb.inc';

if (($_REQUEST['OrgID']) && ($_REQUEST['updateid'])) {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "UpdateID = :UpdateID");
	//Set parameters
	$params = array(":OrgID"=>$_REQUEST['OrgID'], ":MultiOrgID"=>$_REQUEST['MultiOrgID'], ":UpdateID"=>$_REQUEST['updateid']);
	//Get Userportal Information
	$results = $UserPortalInfoObj->getUserPortalInfo("*", $where, 'SortOrder', array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $IMG) {
			$displaytitle = $IMG ['DisplayTitle'];
			$attfile = $IMG ['Attachment'];
			$atttype = $IMG ['AttachmentType'];
			$attext = $IMG ['AttachmentExt'];
		}
	}
	
	$filename = $displaytitle . "." . $attext;
	
	header ( 'Content-Description: File Transfer' );
	header ( "Content-type: $atttype" );
	header ( 'Content-Disposition: attachment; filename=' . $filename );
	header ( 'Content-Transfer-Encoding: binary' );
	header ( 'Expires: 0' );
	header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header ( 'Pragma: public' );
	echo $attfile;
	exit ();
} // end if OrgID, UpdateID
?>