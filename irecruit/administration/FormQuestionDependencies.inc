<?php
if(isset($_REQUEST["FormID"]) && $_REQUEST["FormID"] != '') {
    $personal_ques_info     =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $_REQUEST["FormID"], "1");
    $country_child_que_info =   $personal_ques_info['country']['ChildQuestionsInfo'];
}

if(isset($_POST['country']) && count($_POST['country']) > 0) {
	foreach($_POST['country'] as $QuestionID=>$ChildQuestionInformation) {
		$child_questions_info[$QuestionID] =   $ChildQuestionInformation;		
	}

	$child_questions_json_info =   json_encode($child_questions_info, true);

	//Update Child Questions Information
	$set_info     =   array("ChildQuestionsInfo = :ChildQuestionsInfo");
	$where_info   =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
	$params_info  =   array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST["FormID"], ":QuestionID"=>"country", ":ChildQuestionsInfo"=>$child_questions_json_info);
	
	$FormQuestionsObj->updQuestionsInfo("FormQuestions", $set_info, $where_info, array($params_info));
}

if(isset($_REQUEST["FormID"]) 
    && $_REQUEST["FormID"] != ''
	&& $country_child_que_info == '') {
    
    $country_child_que_info =   '{"US":{"state":"show","province":"hidden","county":"hidden"},"CA":{"state":"hidden","province":"show","county":"hidden"},"Other":{"state":"hidden","province":"hidden","county":"hidden"}}';
    
    $set_info           =   array("ChildQuestionsInfo = :ChildQuestionsInfo");
    $where_info         =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = 'country'");
    $params             =   array(":OrgID"=>$OrgID, ":FormID"=>$_REQUEST["FormID"], ":ChildQuestionsInfo"=>$country_child_que_info);
    G::Obj('FormQuestions')->updQuestionsInfo("FormQuestions", $set_info, $where_info, array($params));
        
    $child_ques_info    =   json_decode($country_child_que_info, true);
    
    $personal_ques_info =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $_REQUEST["FormID"], "1");
}
?>
<div class="table-responsive">
    <form name="frmFormQuestionsList" id="frmFormQuestionsList" method="get">
	<table cellspacing="3" cellpadding="5" class="table table-bordered">
	   <?php
	   $country_results    =   $AddressObj->getCountries();
	   $countries          =   $country_results['results'];
	   
	   $countries_list     =   array();
	   for($c = 0; $c < count($countries); $c++) {
	       $countries_list[$countries[$c]['Abbr']] =   $countries[$c]['Description'];
	   }
	   ?>  
       <tr>
	       <td colspan="5">
	           <strong>Configure Country Form Question Dependencies:</strong>
           </td>
	   </tr>
       <tr>
           <td><strong>FormID</strong></td>
	       <td colspan="4">
            <?php
            // Select Active Forms
            $resultforms = $WeightedSearchObj->FormQuestions($OrgID);
            if(is_array($resultforms['results'])) {
                foreach ($resultforms['results'] as $FQ) {
                    $FormNames[$FQ['FormID']] = $FQ['FormID'];
                }
            }
            ?>
            <select name="FormID" id="FormID" onchange="getFormIDChildQuestionsInfo(this.value)">
            <option value="">Select</option>
            <?php
        	foreach($FormNames as $fkey=>$fval) {
        		?><option value="<?php echo $fkey?>" <?php if ($fkey == $_REQUEST["FormID"]) echo 'selected="selected"'?>><?php echo $fval?></option><?php
        	}
            ?>
            </select>
           </td>
	   </tr>
    </table>
    <input type="hidden" name="action" value="formquestiondependencies">
    </form>
    <form name="frmQueDependencies" id="frmQueDependencies" method="post">
    <table cellspacing="3" cellpadding="5" class="table table-bordered">
       <?php
        if($_REQUEST["FormID"] != '') {

               $show_hidden_options    =   array("show", "hidden");
               $personal_ques_info     =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $_REQUEST["FormID"], "1");
    	       
    	       if($personal_ques_info != "") {
                    $child_ques_info    =   json_decode($personal_ques_info['country']['ChildQuestionsInfo'], true);
                    
                    foreach($child_ques_info as $country_code=>$dependency_que_info) {
                        
                        echo '<tr>';
                        
                        if($country_code != "Other") {
                            echo '<td>';
                            echo "<strong>".$countries_list[$country_code]."</strong>: ";
                            echo '</td>';
                            
                            echo '<td>';
                        	echo "&nbsp;State&nbsp;";
                        	echo '<select name="country['.$country_code.'][state]">';
                        	foreach($show_hidden_options as $sh_val) {
                        	    $state_selected    =   ($child_ques_info[$country_code]['state'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$state_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;Province&nbsp;";
                        	echo '<select name="country['.$country_code.'][province]">';
                            foreach($show_hidden_options as $sh_val) {
                        	    $province_selected =   ($child_ques_info[$country_code]['province'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$province_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;County&nbsp;";
                        	echo '<select name="country['.$country_code.'][county]">';
                            foreach($show_hidden_options as $sh_val) {
                                $county_selected    =   ($child_ques_info[$country_code]['county'] == $sh_val) ? ' selected="selected"' : "";
                                echo '<option value="'.$sh_val.'" '.$county_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        }
                        else {
                            echo '<td>';
                        	echo "<strong>".$country_code."</strong>: ";
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;State&nbsp;";
                        	echo '<select name="country['.$country_code.'][state]">';
                        	foreach($show_hidden_options as $sh_val) {
                        	    $state_selected    =   ($child_ques_info[$country_code]['state'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$state_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;Province&nbsp;";
                        	echo '<select name="country['.$country_code.'][province]">';
                            foreach($show_hidden_options as $sh_val) {
                        	    $province_selected =   ($child_ques_info[$country_code]['province'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$province_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;County&nbsp;";
                        	echo '<select name="country['.$country_code.'][county]">';
                            foreach($show_hidden_options as $sh_val) {
                                $county_selected    =   ($child_ques_info[$country_code]['county'] == $sh_val) ? ' selected="selected"' : "";
                                echo '<option value="'.$sh_val.'" '.$county_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        }
                        
                        echo '</tr>';
                    }
    	       }
    	       ?>
    	       </td>
    	   </tr>
    	   
    	   <tr>
               <td colspan="5">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit">
    	       </td>
    	   </tr>
    	   <?php
    }
    ?>
    </table>
    <input type="hidden" name="action" value="formquestiondependencies">
    </form>
    <script>
    function getFormIDChildQuestionsInfo(form_value) {
        document.forms['frmFormQuestionsList'].submit();    
    }
    </script>
</div>