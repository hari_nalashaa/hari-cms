<?php
$params = array(":OrgID" => $OrgID);
$where = array("OrgID = :OrgID","Active = 'Y'");
$process_orders_results = $ApplicantsObj->getApplicantProcessFlowInfo("ProcessOrder, Description", $where, '', array($params));
$process_orders = $process_orders_results['results'];

$process_orders_list = array();
foreach($process_orders as $process_order_info) {
    $process_orders_list[$process_order_info["ProcessOrder"]] = $process_order_info['Description'];
}

$hired_process_order_info = $ReportsObj->getInfo("getHiredStatusProcessOrder('$OrgID') as HiredProcessOrder");
$hired_process_order = $hired_process_order_info['HiredProcessOrder'];

$reports_settings = $ReportsObj->getReportsSettings($OrgID, "");

if ($process == 'Y') {
    
    $Column1ProcessOrder = $_POST['Column1ProcessOrder'];
    $Column2ProcessOrder = $_POST['Column2ProcessOrder'];
    $Column3ProcessOrder = $_POST['Column3ProcessOrder'];
    
    if($_POST['Column1Title'] == "") $Column1ProcessOrder = "";
    if($_POST['Column2Title'] == "") $Column2ProcessOrder = "";
    if($_POST['Column3Title'] == "") $Column3ProcessOrder = "";
    
    $settings_info = array(
        "OrgID"                 =>  $OrgID,
        "MultiOrgID"            =>  "",
        "Column1Title"          =>  $_POST['Column1Title'],
        "Column1ProcessOrder"   =>  $Column1ProcessOrder,
        "Column2Title"          =>  $_POST['Column2Title'],
        "Column2ProcessOrder"   =>  $Column2ProcessOrder,
        "Column3Title"          =>  $_POST['Column3Title'],
        "Column3ProcessOrder"   =>  $Column3ProcessOrder,
        "PreferredDate"         =>  $_POST['PreferredDate']
    );
    
    $on_update      =   " ON DUPLICATE KEY UPDATE Column1Title = :UColumn1Title, Column1ProcessOrder = :UColumn1ProcessOrder,";
    $on_update     .=   "Column2Title = :UColumn2Title, Column2ProcessOrder = :UColumn2ProcessOrder, ";
    $on_update     .=   "Column3Title = :UColumn3Title, Column3ProcessOrder = :UColumn3ProcessOrder, ";
    $on_update     .=   "PreferredDate = :UPreferredDate";
    
    $update_info    =   array(
        ":UColumn1Title"        =>  $_POST['Column1Title'],
        ":UColumn1ProcessOrder" =>  $Column1ProcessOrder,
        ":UColumn2Title"        =>  $_POST['Column2Title'],
        ":UColumn2ProcessOrder" =>  $Column2ProcessOrder,
        ":UColumn3Title"        =>  $_POST['Column3Title'],
        ":UColumn3ProcessOrder" =>  $Column3ProcessOrder,
        ":UPreferredDate"       =>  $_POST['PreferredDate']
    );
    
    $ReportsObj->insReportsSettings($settings_info, $on_update, $update_info);
    
    echo '<script type="text/javascript">';
    echo "location.href='administration.php?action=reports_settings&msg=suc'";
    echo '</script>';
} // end if process

echo '<form method="post" action="administration.php">';

echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-bordered">';

if (isset($_GET['msg']) && $_GET['msg'] == 'suc') {
    echo '<tr><td colspan="3" height="5">';
    echo '<span style="color:blue">Successfully Updated</span>';
    echo '</td></tr>';
}

echo '<tr><td colspan="3"><h4>Time To Fill Reports Settings:</h4></td></tr>';

echo '<tr><td colspan="3"><strong>Note*:</strong> Column titles are required to select the Process Order.</td></tr>';

echo '<tr><td colspan="3"><strong>Preferred Date:</strong></td></tr>';

echo '<tr><td colspan="3">';

echo '&nbsp;&nbsp;Post Date: <input type="radio" name="PreferredDate" id="PostDate" value="PostDate"';
if ($reports_settings['PreferredDate'] == 'PostDate')
    echo ' checked="checked"';
echo '>';
echo '</td></tr>';

echo '<tr><td colspan="100%"><strong>Time To Hire Columns:</strong></td></tr>';

echo '<tr>';

echo '<td>';
echo 'Column1 Title: ';
echo '<input type="text" name="Column1Title" id="Column1Title" value="'.$reports_settings['Column1Title'].'">';
echo '</td>';

echo '<td>';
echo 'Process Order: <select name="Column1ProcessOrder" id="Column1ProcessOrder" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';

foreach ($process_orders_list as $process_order => $process_order_desc) {
    
    if ($process_order != $hired_process_order && $process_order != "0" && $process_order != "999") {
        
        echo '<option value="' . $process_order . '"';
        
        if (isset($reports_settings['Column1ProcessOrder']) && $reports_settings['Column1ProcessOrder'] != "" && $reports_settings['Column1ProcessOrder'] == $process_order) {
            echo ' selected="selected"';
        }
        
        echo '>' . $process_order_desc . '</option>';
    }
}
echo '</select>';
echo '</td>';
echo '</tr>';

echo '<tr>';

echo '<td>';
echo 'Column2 Title: ';
echo '<input type="text" name="Column2Title" id="Column2Title" value="'.$reports_settings['Column2Title'].'">';
echo '</td>';

echo '<td>';
echo 'Process Order: <select name="Column2ProcessOrder" id="Column2ProcessOrder" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';

foreach ($process_orders_list as $process_order => $process_order_desc) {
    
    if ($process_order != $hired_process_order && $process_order != "0" && $process_order != "999") {
        
        echo '<option value="' . $process_order . '"';
        
        if (isset($reports_settings['Column2ProcessOrder']) && $reports_settings['Column2ProcessOrder'] != "" && $reports_settings['Column2ProcessOrder'] == $process_order) {
            echo ' selected="selected"';
        }
        
        echo '>' . $process_order_desc . '</option>';
    }
}
echo '</select>';
echo '</td>';

echo '</tr>';

echo '<tr>';

echo '<td>';
echo 'Column3 Title: ';
echo '<input type="text" name="Column3Title" id="Column3Title" value="'.$reports_settings['Column3Title'].'">';
echo '</td>';

echo '<td>';
echo 'Process Order: <select name="Column3ProcessOrder" id="Column3ProcessOrder" class="form-control width-auto-inline">';
echo '<option value="">Select</option>';

foreach ($process_orders_list as $process_order => $process_order_desc) {
    if ($process_order != $hired_process_order && $process_order != "0" && $process_order != "999") {
        
        echo '<option value="' . $process_order . '"';
        
        if (isset($reports_settings['Column3ProcessOrder']) && $reports_settings['Column3ProcessOrder'] != "" && $reports_settings['Column3ProcessOrder'] == $process_order) {
            echo ' selected="selected"';
        }
        
        echo '>' . $process_order_desc . '</option>';
    }
}

echo '</select>';
echo '</td>';

echo '</tr>';

echo '<tr>';
echo '<td align="left" colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="reports_settings">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update" class="btn btn-primary">';
echo '</td>';
echo '</tr>';

echo '</table>';
echo '</form>';
echo '<br><br>';
?>
