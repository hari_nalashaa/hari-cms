<?php
if ($_REQUEST['submit'] == 'Change Description') {
	
	//Delete PrescreenText Information
	G::Obj('PrescreenQuestions')->delPrescreenTextInfo($OrgID);
	
	$intro = (isset($_POST ['intro']) && $_POST ['intro'] != "") ? $_POST ['intro'] : '';
	$disclaimer = (isset($_POST ['disclaimer']) && $_POST ['disclaimer'] != "") ? $_POST ['disclaimer'] : '';
	$rejection = (isset($_POST ['rejection']) && $_POST ['rejection'] != "") ? $_POST ['rejection'] : '';
	
	//Insert PrescreenText Information
	$info = array("OrgID"=>$OrgID, "Intro"=>$intro, "Disclaimer"=>$disclaimer, "Rejection"=>$rejection);
	G::Obj('PrescreenQuestions')->insPrescreenTextInfo($info);
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Prescreen Text configured!')";
	echo '</script>';
} // end process
  
// get updated text from db
$desc = G::Obj('PrescreenQuestions')->getPrescreenTextInfo($OrgID, array("OrgID = :OrgID"));
?>
<div class="table-responsive">
	<form action="administration.php" method="post">
	<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">
			<tr>
				<td><b>Intro Text</b></td>
			<tr>
			
			<tr>
				<td align="center"><textarea name="intro" rows="5" cols="80" class="mceEditor"><?php echo $desc['Intro']?></textarea></td>
			</tr>
			
			<tr>
				<td><b>Disclaimer Text</b></td>
			<tr>
			
			
			<tr>
				<td align="center"><textarea name="disclaimer" rows="5" cols="80" class="mceEditor"><?php echo $desc['Disclaimer']?></textarea></td>
			</tr>
			
			<tr>
				<td><b>Rejection Text</b></td>
			<tr>
			
			<tr>
				<td align="center"><textarea name="rejection" rows="10" cols="80" class="mceEditor"><?php echo $desc['Rejection']?></textarea></td>
			</tr>
			
			<tr>
				<td height="60" valign="middle" align="center">
				<input type="hidden" name="action" value="<?php echo $action ?>" /> 
				<input type="submit" name="submit" value="Change Description" class="btn btn-primary" />
				</td>
			</tr>
			<tr>
				<td>
					<hr size=1>
					<p>
						<b>Preview</b>
					</p>
					<?php echo $desc['Intro']?>
					<br>
					<br> [Prescreen questions assigned within a requisition will appear here] <br>
					<br> <?php echo $desc['Disclaimer']?> <br>
					<br>
					<p>
						<b>Preview Rejection Text</b>
					</p>
					<?php echo $desc['Rejection']?>
				</td>
			</tr>
	</table>
	</form>
</div>
