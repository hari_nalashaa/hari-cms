<?php
require_once '../Configuration.inc';

$TemplateObj->title =   $title  =   "Checklist - Items";

//Insert Default Address
//G::Obj('Checklist')->insDefaultAddress($OrgID);

if ($_REQUEST['active'] == 'n') {
        $Active = "N";
} else if ($_REQUEST['active'] == 'p') {
        $Active = "P";
} else {
        $Active = "Y";
}

$ChecklistID = $_REQUEST['ChecklistID'];
$checklist_items     =   array();
$checklist_info      =   G::Obj('Checklist')->getCheckList($ChecklistID, $OrgID, $Active);
$checklist_items     =   json_decode($checklist_info['Checklist'], true);
$uniqid              =   uniqid();

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Update') {
    $count = count($_REQUEST["ChecklistItem"]);
    
    for($i=0;$i<$count;$i++){
        $passed_value = $_REQUEST["passed_value"];
        $ChecklistItem = $_REQUEST["ChecklistItem"];
        
        if($passed_value[$i]){
            $ChecklistItems_array[$i]['passed_value'] = $passed_value[$i];
        }else{
            $ChecklistItems_array[$i]['passed_value'] = uniqid();
        }
        $ChecklistItems_array[$i]['ChecklistItem'] = $ChecklistItem[$i];        
    }

    $ChecklistItems_json_data = json_encode($ChecklistItems_array);
    
    if(isset($ChecklistItems_json_data)) {
        if(isset($_REQUEST['ChecklistID'])){
            $ChecklistID    =   $_REQUEST['ChecklistID'];   
            $now            =   date('Y-m-d H:i:s');
            $set_info       =   array("Checklist = :Checklist","LastUpdated = :LastUpdated");
            $where_info     =   array("ChecklistID = :ChecklistID","OrgID = :OrgID");
            $params         =   array(":Checklist"=>$ChecklistItems_json_data, ":LastUpdated"=>$now, ":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID);
            //Update Status Category Information
            G::Obj('Checklist')->updChecklist($set_info, $where_info, array($params));
        }else{
            //Insert Status Category Information
            G::Obj('Checklist')->insChecklist($OrgID, $ChecklistItems_json_data, null, null);
        }
        
    }
    
    header("Location:".IRECRUIT_HOME."administration/checklistItems.php?ChecklistID=".$ChecklistID."&active=".$_REQUEST['active']);
    exit;
}

$TemplateObj->ChecklistID  =   $ChecklistID;
$TemplateObj->uniqid  =   $uniqid;
$TemplateObj->checklist_items  =   $checklist_items;
echo $TemplateObj->displayIrecruitTemplate ( 'views/administration/ChecklistItems' );
?>
