<?php
if ($_GET['delete'] != "") {

	G::Obj('HRToolbox')->delHRToolboxID($OrgID, $_GET['delete']);

	$message = 'Document successfully deleted.';
}

if ($_POST['process'] == "Y") {

     if (! is_dir ( IRECRUIT_DIR . "vault/" )) {
         @mkdir(IRECRUIT_DIR . "vault/", 0777);
         @chmod(IRECRUIT_DIR . "vault/", 0777);
     }

     if (! is_dir ( IRECRUIT_DIR . "vault/".$OrgID."/" )) {
         @mkdir(IRECRUIT_DIR . "vault/".$OrgID."/", 0777);
         @chmod(IRECRUIT_DIR . "vault/".$OrgID."/", 0777);
     }

     if (! is_dir ( IRECRUIT_DIR . "vault/".$OrgID."/HRToolbox/" )) {
         @mkdir(IRECRUIT_DIR . "vault/".$OrgID."/HRToolbox/", 0777);
         @chmod(IRECRUIT_DIR . "vault/".$OrgID."/HRToolbox/", 0777);
     }

     $HRToolboxDIR = IRECRUIT_DIR . "vault/".$OrgID."/HRToolbox/";

     $HRToolboxID = ($_POST['HRToolboxID'] == "") ? uniqid() : $_POST['HRToolboxID'];

     $skip   =   array("HRToolboxID");

     $info   =   array(
                 "HRToolboxID"       =>  $HRToolboxID,
                 "OrgID"    	     =>  $OrgID,
                 "Description"       =>  $_POST['description'],
                 "SortOrder"         =>  0,
                 "LastModified"      =>  "NOW()"
                 );

     if ($_FILES['document']['name'] != "") {

	$FileName = date("YmdHi") . '-' . preg_replace("/[^a-zA-Z.]/", "",$_FILES['document']['name']);
	$info['FileName']=$FileName;

        $data = file_get_contents ( $_FILES ['document'] ['tmp_name'] );
        $file = $HRToolboxDIR . $FileName;

        $fh = fopen ( $file, 'w' );
	if (! $fh) {
            $ERROR = 'Cannot open file' . $filename;
            die ( include IRECRUIT_DIR . 'irecruit.err' );
        }
        fwrite ( $fh, $data );
        fclose ( $fh );
        chmod ( $file, 0644 );

     }

     G::Obj('HRToolbox')->insUpdHRToolbox($info, $skip);
     
     $message = 'Successfully updated.';

} // end process

echo G::Obj('HRToolbox')->displayHRToolboxFiles($OrgID,'Y');

?>
<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" class="table table-striped table-bordered">
		<form enctype="multipart/form-data" method="post" action="administration.php">
			<input type="hidden" name="MAX_FILE_SIZE" value="10485760"> 
			<input type="hidden" name="action" value="hrtoolbox">
			<input type="hidden" name="process" value="Y">

			<tr><td>

<div style="margin-bottom:10px;">
<input type="hidden" name="HRToolboxID" value="<?php $_REQUST['HRToolboxID']; ?>">
<strong style="font-size:12pt;">Add Document</strong>
</div>
<div style="float:left;">
Description:&nbsp;<input type="text" name="description" value="" size="40">&nbsp;&nbsp;&nbsp; File:&nbsp;&nbsp; 
</div>
<div style="float:left;">
<input type="file" name="document"> 
</div>
<div style="clear:both;margin:50px 0 0 40px;">
<input type="submit" value="Upload Document" class="btn btn-primary"> 
&nbsp;&nbsp;&nbsp;&nbsp;
<span style="color:red;"><?php echo $message; ?></span>
</div>
			</td></tr>

		</form>
	</table>
</div>
