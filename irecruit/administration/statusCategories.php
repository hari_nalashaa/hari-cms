<?php
require_once '../Configuration.inc';

$TemplateObj->title =   $title  =   "Checklist - Status Categories";

//Insert Default Address
//G::Obj('Checklist')->insDefaultAddress($OrgID);
//

if ($_REQUEST['active'] == 'n') {
	$Active = "N";
} else if ($_REQUEST['active'] == 'p') {
	$Active = "P";
} else {
	$Active = "Y";
}

$ChecklistID = $_REQUEST['ChecklistID'];
$category_list       =   array();
$checklist_info      =   G::Obj('Checklist')->getCheckList($ChecklistID, $OrgID, $Active);
$category_list       =   json_decode($checklist_info['StatusCategory'], true);

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Update') {
    $count = count($_REQUEST["passed_value"]);
    
    for($i=0;$i<$count;$i++){
        $passed_value = $_REQUEST["passed_value"];
        $field_name = $_REQUEST["field_name"];
        $default = $_REQUEST["default"];
        
        $status_array[$i]['passed_value'] = $passed_value[$i];
        $status_array[$i]['field_name'] = $field_name[$i];
        if($status_array[$i]['field_name'] == $default[0]){
            $status_array[$i]['default'] = $default[0];
        }
        
    }
    $status_json_data = json_encode($status_array);

    if(isset($status_json_data)) {
        if(isset($_REQUEST['ChecklistID'])){
            $ChecklistID    =   $_REQUEST['ChecklistID'];   
            $now            = date('Y-m-d H:i:s');
            $set_info       =   array("StatusCategory = :StatusCategory","LastUpdated = :LastUpdated");
            $where_info     =   array("ChecklistID = :ChecklistID","OrgID = :OrgID");
            $params         =   array(":StatusCategory"=>$status_json_data, ":LastUpdated"=>$now, ":ChecklistID"=>$ChecklistID, ":OrgID"=>$OrgID);
            //Update Status Category Information
            G::Obj('Checklist')->updChecklist($set_info, $where_info, array($params));
        }else{
            //Insert Status Category Information
            G::Obj('Checklist')->insChecklist($OrgID, $status_json_data, null, null);
        }
        
    }

    
    header("Location:".IRECRUIT_HOME."administration/statusCategories.php?ChecklistID=".$ChecklistID."&active=".$_REQUEST['active']);
    exit;
}

$TemplateObj->ChecklistID  =   $ChecklistID;
$TemplateObj->category_list  =   $category_list;
echo $TemplateObj->displayIrecruitTemplate ( 'views/administration/StatusCategories' );
?>
