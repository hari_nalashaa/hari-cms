<?php
// Access Global Variables
$msg = "";
global $OrgID, $MultiOrgID, $USERID, $USERROLE;

// Set parameters
$params = array (
		":OrgID" => $OrgID 
);
// Set where condition
$where = array (
		"OrgID = :OrgID" 
);
// Get EmailTemplate Information
$template_info = $EmailObj->getEmailTemplateInfo ( "*", $where, "", array (
		$params 
) );


if ($template_info ['count'] == 0) {
	//Insert EmailTemplate Information
	$EmailObj->insEmailTemplateMasterInfo($OrgID);
}

// After submit the template
if ($_REQUEST ['sendtemplate'] == "yes") {
	$gc = (isset($_REQUEST ['GoogleCalendar']) && $_REQUEST ['GoogleCalendar'] == '1') ? '1' : '0';
	$oc = (isset($_REQUEST ['OutlookCalendar']) && $_REQUEST ['OutlookCalendar'] == '1') ? '1' : '0';
	$ic = (isset($_REQUEST ['IcalCalendar']) && $_REQUEST ['IcalCalendar'] == '1') ? '1' : '0';
	$lc = (isset($_REQUEST ['LotusCalendar']) && $_REQUEST ['LotusCalendar'] == '1') ? '1' : '0';
	
	$iscc = 'N';
	$isbcc = 'N';
	
	// if(strtolower($_REQUEST['IsCc']) == "on") $iscc = 'Y';
	if (strtolower ( $_REQUEST ['IsBcc'] ) == "on")
		$isbcc = 'Y';
	
	$description = $_REQUEST ['MailBody'];
	
	//Set Information
	$set_info = array(
					"Cc = :Cc", 
					"Bcc = :Bcc", 
					"IsCc = :IsCc",
					"IsBcc = :IsBcc",
					"GoogleCalendar = :GoogleCalendar",
					"OutlookCalendar = :OutlookCalendar",
					"IcalCalendar = :IcalCalendar",
					"LotusCalendar = :LotusCalendar",
					"Body = :Body",
					"LastModified = NOW()"
					);
	//Set where condition
	$where_info = array("OrgID = :OrgID");
	//Set parameters
	$params = array(
				":OrgID"=>$OrgID,
				":Cc"=>$_REQUEST ['Bcc'],
				":Bcc"=>$_REQUEST ['Bcc'],
				":IsCc"=>$iscc,
				":IsBcc"=>$isbcc,
				":GoogleCalendar"=>$gc,
				":OutlookCalendar"=>$oc,
				":IcalCalendar"=>$ic,
				":LotusCalendar"=>$lc,
				":Body"=>$_REQUEST ['MailBody']
				);
	//Email Template Information
	$result_email_temp_info = $EmailObj->updEmailTemplateInfo($set_info, $where_info, array($params));
	
	$msg = "Updated Successfully";
}

/**
 * Get Email Template Information Based on Organization
 * Retrieve the information after inserting template information
 * and after update the template information
 */
// Set parameters
$params = array (
		":OrgID" => $OrgID
);
// Set where condition
$where = array (
		"OrgID = :OrgID"
);
// Get EmailTemplate Information
$template_info = $EmailObj->getEmailTemplateInfo ( "*", $where, "", array (
		$params
) );

$tf = $template_info ['results'] [0];
//$mailbody = html_entity_decode ( $tf ['Body'] );
$mailbody = $tf ['Body'];

// Get Login User information
$userinfo = $IrecruitUsersObj->getUserInfoByUserID($USERID, "*");
?>
<form name="scheduleinterview" id="scheduleinterview" method="post">
	<input type="hidden" name="sendtemplate" id="sendtemplate" value="yes" />
	<input type="hidden" name="action" id="action" value="scheduleinterviewmail">
	<div class="table-responsive">
		<table border="0" width="100%" cellpadding="5" cellspacing="5"
			class="table table-striped table-bordered">
			<tr>
				<td colspan="2"><span style="color: red"><?php echo $msg;?></span></td>
			</tr>
			<!--  <tr>
			<td>CC</td>
			<td>
				<input type="text" name="Cc" id="Cc" value="<?php echo $tf['Cc'];?>" />
				<input type="checkbox" name="IsCc" id="IsCc" <?php if($tf['IsCc'] == "Y") echo 'checked="checked"';?>>
			</td>
		</tr>-->

			<tr>
				<td width="20%">BCC</td>
				<td><input type="text" name="Bcc" id="Bcc"
					value="<?php echo $tf['Bcc'];?>"
					<?php if($tf['IsBcc'] == "Y") echo 'checked="checked"';?> /> <input
					type="checkbox" name="IsBcc" id="IsBcc"
					<?php if($tf['IsBcc'] == "Y") echo 'checked="checked"';?>></td>
			</tr>
			<tr>
				<th colspan="2" align="left">Insert Code Generator</th>
			</tr>
			<tr>
				<td width="21%">Applicant Data</td>
				<td><div style="float: left">
						<select name="codegenrator" id="codegenrator"
							onchange="$('#codevalue').html(this.value);" class="form-control"
							style="width: 200px">
							<option value="">Select</option>
							<option value="{first}">First Name</option>
							<option value="{last}">Last Name</option>
							<option value="{JobTitle}">Job Title</option>
							<option value="{Date}">Date</option>
							<option value="{Time}">Time</option>
							<option value="{Location}">Location</option>
							<option value="{RequestNewTime}">Request New Time</option>
						</select>
					</div>
					<div id="codevalue" style="float:left;<?php if($BOOTSTRAP_SKIN == true) echo 'padding:6px 12px;';?>"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">Email Body</td>
				<td><textarea name="MailBody" id="MailBody" rows="10" cols="60"
						wrap="virtual" class="mceEditor"><?php echo $mailbody;?></textarea>
				</td>
			</tr>

			<tr>
				<td>Calendar Icons</td>
				<td align="left"><input type="checkbox" name="GoogleCalendar"
					id="GoogleCalendar" value="1"
					<?php if($tf['GoogleCalendar'] == '1') echo 'checked="checked"';?>>GoogleCalendar
					<input type="checkbox" name="OutlookCalendar" id="OutlookCalendar"
					value="1"
					<?php if($tf['OutlookCalendar'] == '1') echo 'checked="checked"';?>>OutlookCalendar
					<input type="checkbox" name="IcalCalendar" id="IcalCalendar"
					value="1"
					<?php if($tf['IcalCalendar'] == '1') echo 'checked="checked"';?>>IcalCalendar
					<input type="checkbox" name="LotusCalendar" id="LotusCalendar"
					value="1"
					<?php if($tf['LotusCalendar'] == '1') echo 'checked="checked"';?>>LotusCalendar
				</td>
			</tr>
			<tr>
				<td width="15%" colspan="2"><input type="submit" name="send"
					id="send" value="Update Template Email"
					class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton';?>" />
				</td>
			</tr>
		</table>
	</div>
</form>
