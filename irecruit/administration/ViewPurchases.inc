<?php
$pagewidth = "770";

// cancel order
if ($_REQUEST['process'] == "Y") {
	
	if ($cancelORDER != "") {
		
		//Set information
		$set_info = array("Canceled = NOW()");
		//Set where condition
		$where = array("OrgID = :OrgID", "PurchaseNumber = :PurchaseNumber");
		//Set paramters
		$params = array(":OrgID"=>$OrgID, ":PurchaseNumber"=>$cancelORDER);
		//Update purchases information
		G::Obj('Purchases')->updPurchaseInfo('PurchaseItems', $set_info, $where, array($params));
		
		$ERROR = " - Your order has been canceled." . "\\n";
		
		$MESSAGE = "The following entries are missing information.\\n\\n";
		$MESSAGE .= $ERROR;
		
		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('$MESSAGE')" . "\n";
		echo '</script>' . "\n\n";
	} // end cancelORDER
} // end process

echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="0" cellpadding="3" width="' . $pagewidth . '" class="table table-striped table-bordered">';
echo '<tr>';

echo '<td>&nbsp;</td>';

echo '<td align="right" valign="bottom" width="130">';
echo '<a href=\'javascript:void(0)\' onclick=\'viewPurchasesInfo()\'>';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">view purchases</b>';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View Purchases" style="margin:0px 0px -3px 3px;">';
echo '</a>';
echo '</td>';

echo '<td align="right" valign="bottom" height="30" width="130">';
echo '<a href="#" onclick=\'getInvoiceForm("'.$AccessCode.'")\'>';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">Pay Invoice</b>';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/basket_put.png" border="0" title="Add Item" style="margin:0px 0px -3px 3px;">';
echo '</a>';
echo '</td>';

echo '<td align="right" valign="bottom" width="130">';
echo '<a href=\'javascript:void(0)\' onclick=\'getBasketInfo("'.$AccessCode.'")\'>';
echo '<b style="FONT-SIZE:8pt;COLOR:#000000">view basket</b>';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/basket.png" border="0" title="Shopping Basket" style="margin:0px 0px -3px 3px;">';
echo '</a>';
echo '</td>';

echo '</tr>';
echo '</table>';

echo '</div>';

include IRECRUIT_DIR . 'administration/Purchases.inc';
?>