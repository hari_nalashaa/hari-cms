<?php 
if ($_REQUEST['process'] == "Y") {

    if (strlen ( $_REQUEST['OrganizationName'] ) > 5) {

        //Get User EmailAddress
        $user_info = $IrecruitUsersObj->getUserInfoByUserID($USERID, "EmailAddress");
        $EmailAddress = $user_info['EmailAddress'];

        $MultiOrgID = uniqid ();

        //Organization Data Information
        $org_data_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrganizationName"=>$_REQUEST['OrganizationName'], "ContactEmail"=>$EmailAddress, "ClientSince"=>"NOW()");
        //Insert OrgData Information
        $org_data_res = $OrganizationsObj->insOrganizationInfo('OrgData', $org_data_info);

        //OrganizationLevels Information
        $org_levels_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>"1", "OrganizationLevel"=>"Location");
        //Insert OrgData Information
        $OrganizationsObj->insOrganizationInfo('OrganizationLevels', $org_levels_info);


        //OrganizationLevelData Information
        $org_levels_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>"1", "SelectionOrder"=>"1", "CategorySelection"=>'Connecticut');
        //Insert OrganizationLevelData Information
        $OrganizationsObj->insOrganizationInfo('OrganizationLevelData', $org_levels_info);

        echo '<script language="JavaScript" type="text/javascript">';
        echo "alert('Organization successfully added.')";
        echo '</script>';
        
    } else {

        echo '<script language="JavaScript" type="text/javascript">';
        echo "alert('Please enter a Organization Name.')";
        echo '</script>';
    }
} // end process

echo '<form method="post" action="administration.php">';
echo 'Organization Name: <input type="text" name="OrganizationName" size="50" maxlength="60">';
echo '&nbsp;&nbsp;';
echo '<input type="hidden" name="action" value="addorganization">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Add Organization" class="btn btn-primary">';
echo '</form>';
?>