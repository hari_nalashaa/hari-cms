<?php
require_once '../Configuration.inc';

$req_que_data = $_REQUEST['request_que_data'];
$req_que_data_cnt = count($_REQUEST['request_que_data']);

$sort_order = 1;
$update_status = false; 
for($i = 0; $i < $req_que_data_cnt; $i++) {
	$set_info = array("QuestionOrder = :SortOrder");
	$where_info = array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
	$params = array(":OrgID"=>$OrgID, ":FormID"=>"STANDARD", ":QuestionID"=>$req_que_data[$i]['QuestionID'], ":SortOrder"=>$sort_order);
	$upd_results = $RequestObj->updRequestQuestions($set_info, $where_info, array($params));
	if($upd_results['affected_rows'] == 1) $update_status = true;
	$sort_order++;
}

if($update_status) echo "Updated successfully";
else echo "Failed to update the status";
?>