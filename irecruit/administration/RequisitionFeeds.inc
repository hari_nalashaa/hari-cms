<?php

$SOURCE = trim($_REQUEST['source']);
$SOURCE = preg_replace("/[^a-z _]+/i", "", $SOURCE);
$SOURCE = preg_replace("/\s+/", "_", $SOURCE);

?>
<div style="max-width:450px;margin-bottom:20px;padding-top:10px;">
These URL's can be given to job board websites so they can pull requisition information for their postings.
All active requisitions will be available on this feed.
A unique source indication is needed for each website in order to identify and track where applicants originate from.
</div>
<div style="margin-bottom:20px;">
<form method="POST" action="administration.php">
Source Indication: <input type="text" name="source" value="<?echo $SOURCE; ?>" size="10">
<input type="hidden" name="action" value="requisitionfeeds">
<input type="submit" value="Create Feed">
</form>
</div>
<?php

if ($SOURCE != "") {

echo '<div style="padding-left:10px;line-height:160%;">';

   $where = array();
   $params = array();

   $where[] = "OrgID = :OrgID";
   $params[":OrgID"] = $OrgID;
   $order    =   'MultiOrgID ASC';

   if($feature['MultiOrg'] != "Y") {
   	$where[] = "MultiOrgID = :MultiOrgID";
   	$params[":MultiOrgID"] = "";
   }

   $results = $OrganizationsObj->getOrgDataInfo("OrganizationName, OrgID, MultiOrgID", $where, $order, array($params));

   foreach ($results['results'] as $OD) {

       echo '<strong>' . $OD['OrganizationName'] . "</strong><br>";

       echo '&nbsp;&nbsp;&nbsp;&nbsp;';

       $link = IRECRUIT_HOME . 'feeds/xml.php?';
       $link .= "OrgID=" . $OD['OrgID'];
	if ($OD['MultiOrgID'] != "") {
	   $link .= "&MultiOrgID=" . $OD['MultiOrgID'];
	} 
        $link .= "&source=" . $SOURCE;

       echo '<a href="' . $link . '" target="_blank">';
       echo $link . "</a><br><br>";

   } // end foreach

echo '</div>';

} // end source

?>
