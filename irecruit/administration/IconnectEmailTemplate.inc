<?php 
	if(isset($_REQUEST['btnEmail']) && $_REQUEST['btnEmail'] == "Submit") {
		//To insert common email message data
		$IconnectEmailTemplateObj->insIconnectEmailTemplate($OrgID, $_REQUEST['Subject'], $_REQUEST['EmailMessage']);
	}
	
	//Get email message 
	$email_template_info = $IconnectEmailTemplateObj->getIconnectEmailTemplate($OrgID);
	
	if($email_template_info['Subject'] == "") {
		$email_template_info['Subject'] = "Employment Action Needed";
	}
	if($email_template_info['EmailMessage'] == "") {
		
		$email_template_info['EmailMessage'] = "Dear {last} {first},"."<br><br>";
		$email_template_info['EmailMessage'] .= "The following forms are available and need to be completed for application {ApplicationID}."."<br><br>";
		$email_template_info['EmailMessage'] .= "Please login in to our portal with the below link to complete."."<br><br>";
		$email_template_info['EmailMessage'] .= "{UserPortalLink}"."<br><br>";
		$email_template_info['EmailMessage'] .= "-----------"."<br><br>";
		$email_template_info['EmailMessage'] .= "{FormsList}";
		
	}
?>
<form name="frmIconnectEmailTemplate" id="frmIconnectEmailTemplate" method="post">
	Template Tags: {ApplicationID}, {UserPortalLink}, {FormsList}, {first}, {last}, {JobTitle}
	<br><br>
	Subject: <input type="text" name="Subject" id="Subject" class="form-control" style="max-width: 500px" maxlength="255" value="<?php echo $email_template_info['Subject'];?>"><br>
	Email Message:
	<textarea rows="30" cols="30" class="mceEditor" name="EmailMessage" id="EmailMessage"><?php echo $email_template_info['EmailMessage'];?></textarea>
	<br>
	<input type="submit" class="btn btn-primary" name="btnEmail" id="btnEmail" value="Submit">
</form>
