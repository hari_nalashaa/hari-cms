<?php
//$test = "Y";

$texting_agreement_info     =   G::Obj('TwilioTextingAgreement')->getTextingAgreementByOrgID($OrgID);
$cards_info		    =   G::Obj('IntuitManageCards')->getAllCardsInfoFromApi($OrgID);
$cards_information          =   json_decode($cards_info, true);

if(isset($_REQUEST['card_action']) && $_REQUEST['card_action'] == 'delete') {

    //Get Intuit Access Information
    $intuit_access_info   =   $IntuitObj->getUpdatedAccessTokenInfo();
    //Change your Access Token Here
    $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
    //Add your request ID here
    $intuit_request_id    =   uniqid();
    
    $http_header = array(
        'Accept'          =>  'application/json',
        'Request-Id'      =>  $intuit_request_id,
        'Authorization'   =>  $access_token,
        'Content-Type'    =>  'application/json;charset=UTF-8'
    );
    
    $card_on_file_url       =   $IntuitManageCardsObj->customers_url.$OrgID.'/cards/'.$_GET['CardInfoID'];
    $CurlClientObj->makeAPICall($card_on_file_url, "DELETE", $http_header, array(), null, false);

    header("Location: " . IRECRUIT_HOME . "administration.php?action=savecreditcardinfo&menu=8&msg=delsucc");
}

if ($test == "Y") {
    $_REQUEST['CCtype']           =   "visa";
    $_REQUEST['CCnumber']         =   "4111111111111111";
    $_REQUEST['CCexpmo']          =   "02";
    $_REQUEST['CCexpyear']        =   "2020";
    $_REQUEST['CCidentifier1']    =   "433";
    $_REQUEST['FullName']         =   "David Edgecomb";
    $_REQUEST['Address1']         =   "462 Long Bow Lane East";
    $_REQUEST['Address2']         =   "";
    $_REQUEST['City']             =   "Becket";
    $_REQUEST['State']            =   "MA";
    $_REQUEST['ZipCode']          =   "01223";
    $_REQUEST['Country']          =   "US";
    $_REQUEST['Email']            =   "dedgecomb@irecruit-software.com";
    $_REQUEST['Phone']            =   "(413) 426-8215";
} // end test

if(isset($_POST) && count($_POST) > 0) {
    //Get Intuit Access Information
    $intuit_access_info   =   $IntuitObj->getUpdatedAccessTokenInfo();
    //Change your Access Token Here
    $access_token         =   "Bearer " . $intuit_access_info['AccessTokenValue'];
    //Add your request ID here
    $intuit_request_id    =   uniqid(str_replace(".", "", microtime(true)));
    
    $card_info = array(
	"number"    =>  preg_replace('[\D]', '', $_REQUEST['CCnumber']),
        "expMonth"  =>  $_REQUEST['CCexpmo'],
        "expYear"   =>  $_REQUEST['CCexpyear'],
        "name"      =>  $_REQUEST['FullName'],
        "cvc"       =>  $_REQUEST['CCidentifier1'], 
        "address"   =>  array(
           "streetAddress"  => $_REQUEST['Address1'],
           "city"           => $_REQUEST['City'],
           "region"         => $_REQUEST['Country'],
           "country"        => $_REQUEST['Country'],
           "postalCode"     => $_REQUEST['ZipCode']
        )
    );
      
    $http_header = array(
        'Accept'          =>  'application/json',
        'Request-Id'      =>  $intuit_request_id,
        'Authorization'   =>  $access_token,
        'Content-Type'    =>  'application/json;charset=UTF-8'
    );
    
    $card_on_file_url       =   $IntuitManageCardsObj->customers_url.$OrgID.'/cards';
    $intuit_save_card_resp  =   $CurlClientObj->makeAPICall($card_on_file_url, "POST", $http_header, json_encode($card_info), null, false);
    
    $req_data  = "\nRequest:".json_encode($http_header)."<br>";
    $req_data .= "\nResponse:".json_encode($intuit_save_card_resp);
    
    Logger::writeMessage(ROOT."logs/intuit/". $OrgID . "-" . date('Y-m-d-H') . "-intuit-card-on-file.txt", $req_data, "a+", false);
    Logger::writeMessageToDb($OrgID, "Intuit", $req_data);
    
    $intuit_save_card_resp  =   json_decode($intuit_save_card_resp, true);
    
    if($intuit_save_card_resp['id'] == "") {
	header("Location: " . IRECRUIT_HOME . "administration.php?action=savecreditcardinfo&menu=8&msg=savefailed");
    } else {
	header("Location: " . IRECRUIT_HOME . "administration.php?action=savecreditcardinfo&menu=8&msg=savedsucc");
    }

}

$cards_info		    =   G::Obj('IntuitManageCards')->getAllCardsInfoFromApi($OrgID);
$cards_information          =   json_decode($cards_info, true);

echo '<h3>Manage Cards</h3>';

echo '<table class="table table-bordered">';
echo '<tr>';
echo '<td><strong>Name</strong></td>';
echo '<td><strong>Card Type</strong></td>';
echo '<td><strong>Card Number</strong></td>';
echo '<td><strong>Expires</strong></td>';
echo '<td style="text-align:center;"><strong>Action</strong></td>';
echo '</tr>';

if($cards_information[0]['id'] !="") {
    for($acc = 0; $acc < count($cards_information); $acc++) {
        echo '<tr>';
        echo '<td>'.$cards_information[$acc]['name'].'</td>';
        echo '<td>'.$cards_information[$acc]['cardType'].'</td>';
        echo '<td>'.$cards_information[$acc]['number'].'</td>';
	echo '<td>'.$cards_information[$acc]['expMonth'].'/'.$cards_information[$acc]['expYear'].'</td>';
        echo '<td style="text-align:center;">';
        //echo $cards_information[$acc]['id'];
        if($texting_agreement_info['IntuitCardID'] != $cards_information[$acc]['id']) {
            echo '<a href="administration.php?action=savecreditcardinfo&menu=8&card_action=delete&CardInfoID='.$cards_information[$acc]['id'].'">';
            echo '<img src="'.IRECRUIT_HOME.'images/icons/cross.png" title="Delete" style="margin:0px 3px -4px 0px;" border="0">';
            echo '</a>';
        }
        echo '</td>';
        echo '</tr>';

    }
}
else {
    echo "<tr>";
    if($cards_information['code'] != "AuthenticationFailed") {
      echo "<td colspan='4' align='center'>You haven't saved any cards.</td>";
    } else {
      echo "<td colspan='4' align='center'>Intuit authentication failed.</td>";
    }
    echo "</tr>";
}

echo '</table>';


echo '<h3>Save Card</h3>';
echo '<form method="post" name="frmCreditCardInfo" id="frmCreditCardInfo">';

echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-bordered">';

if(isset($_GET['msg']) && $_GET['msg'] == 'savedsucc') {
    echo '<tr><td colspan="2" style="color:blue">Successfully Saved</td></tr>';
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'savefailed') {
    echo '<tr><td colspan="2" style="color:blue">Unable to save the credit card information.</td></tr>';
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'delsucc') {
    echo '<tr><td colspan="2" style="color:blue">Deleted credit card information successfully.</td></tr>';
}

echo '<tr><td colspan="2" align="left" style="border-bottom: 5px solid #8a89a2;"><img src="' . IRECRUIT_HOME . 'images/creditcards_sm.jpg"></td></tr>';
echo '<tr><td align="right" valign="bottom" height="40" width="150">';
echo '<i';
if ($CCtype_error == "Y") {
    echo $missing;
}
echo '>Credit Card Type:</i></td><td valign="bottom">';
echo '<select name="CCtype">';
echo '<option value="">Select</option>';

echo '<option value="visa"';
if ($_REQUEST['CCtype'] == "visa") {
    echo ' selected';
}
echo '>Visa</option>';

echo '<option value="mc"';
if ($_REQUEST['CCtype'] == "mc") {
    echo ' selected';
}
echo '>Master Card</option>';

echo '<option value="amex"';
if ($_REQUEST['CCtype'] == "amex") {
    echo ' selected';
}
echo '>American Express</option>';

echo '<option value="discover"';
if ($_REQUEST['CCtype'] == "discover") {
    echo ' selected';
}
echo '>Discover</option>';

echo '</select>';
echo '</td></tr>';
echo '<tr><td align="right"><i';
if ($CCnumber_error == "Y") {
    echo $missing;
}
echo '>Credit Card Number:</i></td><td><input type="text" name="CCnumber" value="' . htmlspecialchars($_REQUEST['CCnumber']) . '" size="20" maxlength="20"></td></tr>';
echo '<tr><td align="right"><i';
if (($CCexpmo_error == "Y") || ($CCexpyear_error == "Y")) {
    echo $missing;
}
echo '>Credit Card Exp:</i></td><td>';
echo '<select name="CCexpmo">';
echo '<option value="">Select</option>';
for($i = 1; $i <= 12; $i ++) {
    $ii = "0" . $i;
    $ii = substr ( $ii, - 2 );
    echo '<option value="' . htmlspecialchars($ii) . '"';
    if ($ii == $_REQUEST['CCexpmo']) {
        echo ' selected';
    }
    echo '>' . htmlspecialchars($ii) . '</option>';
} // end for
echo '</select>';
echo '/';
echo '<select name="CCexpyear">';
echo '<option value="">Select</option>';

$year = $MysqlHelperObj->getDateTime ( '%Y' );
for($i = $year; $i <= ($year + 6); $i ++) {
    echo '<option value="' . htmlspecialchars($i) . '"';
    if ($i == $_REQUEST['CCexpyear']) {
        echo ' selected';
    }
    echo '>' . htmlspecialchars($i) . '</option>';
} // end for
echo '</select>';
echo '&nbsp;&nbsp;';

$onclickcsc = " onclick=\"javascript:window.open('" . IRECRUIT_HOME . "shopping/cscexplain.php";
if ($_REQUEST['AccessCode'] != "") {
    $onclickcsc .= "?k=" . $AccessCode;
}
$onclickcsc .= "','_blank','location=yes,toolbar=no,height=600,width=550,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";

echo '<a href="#"' . $onclickcsc . ' style="text-decoration: underline;"><i';
if ($CCidentifier1_error == "Y") {
    echo $missing;
}
echo '>CSC:</i></a> <input type="text" name="CCidentifier1" value="' . htmlspecialchars($_REQUEST['CCidentifier1']) . '" size="4" maxlength="4">';
echo '</td></tr>';

echo '<table border="0" cellspacing="0" cellpadding="3"  class="table table-bordered">';
echo '<tr><td colspan="2" align="left" valign="center" height="50">Please enter information as it appears on the credit card.</td></tr>';

echo '<tr><td align="right"><i';
if ($FullName_error == "Y") {
    echo $missing;
}
echo '>Name:</i></td><td><input type="text" name="FullName" value="' . htmlspecialchars($_REQUEST['FullName']) . '" size="45" maxlength="105"></td></tr>';
echo '<tr><td align="right"><i';
if ($Address1_error == "Y") {
    echo $missing;
}
echo '>Address:</i></td><td><input type="text" name="Address1" value="' . htmlspecialchars($_REQUEST['Address1']) . '" size="40" maxlength="45"></td></tr>';
echo '<tr><td align="right"></td><td><input type="text" name="Address2" value="' . htmlspecialchars($_REQUEST['Address2']) . '" size="40" maxlength="45"></td></tr>';
echo '<tr><td align="right"><i';
if ($City_error == "Y") {
    echo $missing;
}
echo '>City:</i></td><td><input type="text" name="City" value="' . htmlspecialchars($_REQUEST['City']) . '" size="20" maxlength="45">';
echo '&nbsp;&nbsp;<i';
if ($State_error == "Y") {
    echo $missing;
}
echo '>State:</i> ';
echo '<select name="State">';
echo '<option value="">Please Select';

//Get states list
$results = $AddressObj->getAddressStateList();

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
         
        echo "<option value=\"" . $row ['Abbr'] . "\"";
        if ($_REQUEST['State'] == $row ['Abbr']) {
            echo " selected";
        }
        echo ">" . $row ['Description'];
    } // end foreach
}

echo '</select>';

echo '&nbsp;&nbsp;<i';
if ($ZipCode_error == "Y") {
    echo $missing;
}
echo '>ZIP Code</i>: <input type="text" name="ZipCode" value="' . htmlspecialchars($_REQUEST['ZipCode']) . '" size="10" maxlength="10">';
echo '</td></tr>';
echo '<tr><td align="right"><i';
if ($Country_error == "Y") {
    echo $missing;
}
echo '>Country:</i></td><td>';

echo '<select name="Country">';
echo '<option value="">Please Select';

if (($_REQUEST['Country'] == "") && ($_REQUEST['process'] == "")) {
    $Country = "US";
}

//Get countries list
$results = $AddressObj->getCountries();

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
         
        echo "<option value=\"" . $row ["Abbr"] . "\"";
        if ($_REQUEST['Country'] == $row ["Abbr"]) {
            echo " selected";
        }
        echo ">" . $row ["Description"];
        echo "</option>\n";
    } // end foreach
}

echo '</select>';

echo '</td></tr>';

echo '<tr><td align="right" valign="top"><input type="checkbox" name="agree" value="Y"';
if ($_REQUEST['agree'] == "Y") {
    echo ' checked';
}
echo '>&nbsp;&nbsp;</td><td valign="top">Yes, <i';
if ($agree_error == "Y") {
    echo $missing;
}
echo '>I have read and agree to</i> the web sites policies:<br>';

$onclickpolicy1 = " onclick=\"javascript:window.open('" . IRECRUIT_HOME . "shopping/policies.php";
if ($_REQUEST['AccessCode'] != "") {
    $onclickpolicy1 .= "?k=" . $AccessCode . "&pg=";
} else {
    $onclickpolicy1 .= "?pg=";
}
$onclickpolicy2 = "','_blank','location=yes,toolbar=no,height=800,width=600,status=no,menubar=yes,resizable=yes,scrollbars=yes');\"";

echo '<a href="#"' . $onclickpolicy1 . 'terms' . $onclickpolicy2 . '>Terms of Use</a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#"' . $onclickpolicy1 . 'policy' . $onclickpolicy2 . '>Privacy Policy</a>';
echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;';
echo '<a href="#"' . $onclickpolicy1 . 'purchase' . $onclickpolicy2 . '>Purchase Policy</a>';
echo '</td></tr>';

echo '<tr><td align="center" valign="middle" height="40" colspan="2" style="border-bottom: 5px solid #8a89a2;">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="action" value="savecreditcardinfo">';

$user_details = $IrecruitUsersObj->getUserInfoByUserID($USERID, "EmailAddress, Phone");
echo '<input type="hidden" name="Email" value="' . $user_details['EmailAddress'] . '">';
echo '<input type="hidden" name="Phone" value="' . $user_details['Phone'] . '">';

echo '<input type=\'submit\' name=\'btnPurchase\' id=\'btnPurchase\' class=\'btn btn-primary\' value=\'Save\'>';

echo '</td></tr>';

echo '</table>';
echo '</form>';
?>
