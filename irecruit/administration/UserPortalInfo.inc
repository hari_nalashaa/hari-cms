<?php
// EDIT A FORM
if (($_GET ['UpdateID'] != "") & ($_GET ['direction'] != "")) {
	
	if ($direction == "up") {
		
		$minus = $_GET ['SortOrder'] - 1;
		
		// Set information
		$set_info = array (
				"SortOrder = :USortOrder" 
		);
		
		// Set parameters
		$params = array (
				":USortOrder" => '999',
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":UpdateID" => $_GET ['UpdateID'] 
		);
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"UpdateID = :UpdateID" 
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, array (
				$params 
		) );
		
		$info = array ();
		// Set parameters
		$info [] = array (
				":USortOrder" => $_GET ['SortOrder'],
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":SortOrder" => $minus 
		);
		// Set parameters
		$info [] = array (
				":USortOrder" => $minus,
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":SortOrder" => '999' 
		);
		
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"SortOrder = :SortOrder" 
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, $info );
	} elseif ($direction == "down") {
		
		$plus = $_GET ['SortOrder'] + 1;
		
		// Set information
		$set_info = array (
				"SortOrder = :USortOrder" 
		);
		
		// Set parameters
		$params = array (
				":USortOrder" => '999',
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":UpdateID" => $_GET ['UpdateID'] 
		);
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"UpdateID = :UpdateID" 
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, array (
				$params 
		) );
		
		$info = array ();
		// Set parameters
		$info [] = array (
				":USortOrder" => $_GET ['SortOrder'],
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":SortOrder" => $plus 
		);
		// Set parameters
		$info [] = array (
				":USortOrder" => $plus,
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":SortOrder" => '999' 
		);
		
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"SortOrder = :SortOrder" 
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, $info );
	} // end direction
} // end sort feature

$mainfunction = "Add";
$extfunction = "Add";
$attfunction = "Add";
$htmlfunction = "Add";

$maintype = "none";
$exttype = "none";
$atttype = "none";
$htmltype = "none";

$change = "N";

if ($_GET ['edit'] != "") {
	
	// Set parameters
	$params = array (
			":UpdateID" => $_GET ['edit'],
			":OrgID" => $OrgID,
			":MultiOrgID" => $MultiOrgID 
	);
	// Set where condition
	$where_info = array (
			"UpdateID = :UpdateID",
			"OrgID = :OrgID",
			"MultiOrgID = :MultiOrgID" 
	);
	// Get UserPortal Information
	$results = $UserPortalInfoObj->getUserPortalInfo ( "*", $where_info, '', array (
			$params 
	) );
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $PTL ) {
			
			if ($PTL ['TypeLink'] == "Main Section Title") {
				$maintype = "block";
				$mainfunction = "Change";
				$maindisplaytitle = $PTL ['DisplayTitle'];
				$change = "Y";
			}
			if ($PTL ['TypeLink'] == "External Link") {
				$exttype = "block";
				$extfunction = "Change";
				$extdisplaytitle = $PTL ['DisplayTitle'];
				$exturl = $PTL ['URL'];
			}
			if ($PTL ['TypeLink'] == "Attachment") {
				$atttype = "block";
				$attfunction = "Change";
				$attdisplaytitle = $PTL ['DisplayTitle'];
			}
			if ($PTL ['TypeLink'] == "HTML Page") {
				$htmltype = "block";
				$htmlfunction = "Change";
				$htmldisplaytitle = $PTL ['DisplayTitle'];
				$htmltext = $PTL ['HTML'];
			}
		} // end foreach
	}
} // end edit

if ($_GET ['delete'] != "") {
	// Set params
	$params = array (
			":UpdateID" => $_GET ['delete'],
			":OrgID" => $OrgID,
			":MultiOrgID" => $MultiOrgID 
	);
	// Set where condition
	$where = array (
			"UpdateID = :UpdateID",
			"OrgID = :OrgID",
			"MultiOrgID = :MultiOrgID" 
	);
	$UserPortalInfoObj->delUserPortalInfo ( $where, array (
			$params 
	) );
}

if ($_REQUEST['submit'] == "Add Section Title") {
	
	if ($_POST ['subtitle']) {
		
		// Information to insert userportal data
		$info = array (
				"OrgID" => $OrgID,
				"MultiOrgID" => $MultiOrgID,
				"DisplayTitle" => $_POST ['subtitle'],
				"TypeLink" => 'Main Section Title',
				"SortOrder" => '0' 
		);
		// Insert UserPortal Information
		$UserPortalInfoObj->insUserPortalInfo ( $info );
	} // end subtitle
} // end Add Section Title

if ($_REQUEST['submit'] == "Add External Link") {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Get Userportal Information
	$results = $UserPortalInfoObj->getUserPortalInfo("MAX(SortOrder)+1 'SO'", $where, '', array($params));
	$sortorder = $results['results'][0]['SO'];
	
	if ($sortorder == 0) {
		$sortorder = 1;
	}
	
	if (($_POST ['exttitle']) && ($_POST ['url'])) {
		
		// Information to insert userportal data
		$info = array (
				"OrgID" => $OrgID,
				"MultiOrgID" => $MultiOrgID,
				"DisplayTitle" => $_POST ['exttitle'],
				"TypeLink" => 'External Link',
				"SortOrder" => $sortorder,
				"URL" => $_POST ['url'] 
		);
		// Insert UserPortal Information
		$UserPortalInfoObj->insUserPortalInfo ( $info );
	} // end exttitle && url
} // end Add External Link

if ($_REQUEST['submit'] == "Add Attachment") {
	
	//Set where condition
	$where = array("OrgID = :OrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID);
	//Get Userportal Information
	$results = $UserPortalInfoObj->getUserPortalInfo("MAX(SortOrder)+1 'SO'", $where, '', array($params));
	$sortorder = $results['results'][0]['SO'];
	
	
	if (($_POST ['atttitle']) && ($_FILES ['attfile'] ['type'] != "")) {
		
		$fileType = $_FILES ['attfile'] ['type'];
		$data = file_get_contents ( $_FILES ['attfile'] ['tmp_name'] );
		
		$e = explode ( '.', $_FILES ['attfile'] ['name'] );
		$ecnt = count ( $e ) - 1;
		$ext = $e [$ecnt];
		
		// Information to insert userportal data
		$info = array (
				"OrgID" => $OrgID,
				"MultiOrgID" => $MultiOrgID,
				"DisplayTitle" => $_POST ['atttitle'],
				"TypeLink" => 'Attachment',
				"SortOrder" => $sortorder,
				"AttachmentType" => $fileType,
				"AttachmentExt" => $ext,
				"Attachment" => $data 
		);
		// Insert UserPortal Information
		$UserPortalInfoObj->insUserPortalInfo ( $info );
	} // end file att type
} // end Add Attachment

if ($_REQUEST['submit'] == "Add HTML") {
	
	//Set where condition
	$where = array("OrgID = :OrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID);
	//Get Userportal Information
	$results = $UserPortalInfoObj->getUserPortalInfo("MAX(SortOrder)+1 'SO'", $where, 'SortOrder', array($params));
	$sortorder = $results['results'][0]['SO'];
	
	
	if ($sortorder == 0) {
		$sortorder = 2;
	}
	
	if (($_POST ['htmltitle']) && ($_POST ['html'])) {
		
		// Information to insert userportal data
		$info = array (
				"OrgID" => $OrgID,
				"MultiOrgID" => $MultiOrgID,
				"DisplayTitle" => $_POST ['htmltitle'],
				"TypeLink" => 'HTML Page',
				"SortOrder" => $sortorder,
				"HTML" => $_POST ['html']
		);
		// Insert UserPortal Information
		$UserPortalInfoObj->insUserPortalInfo ( $info );
		
	} // end htmltitle and html
} // end Add HTML

if ($_REQUEST['submit'] == "Change Section Title") {
	
	if (($_POST ['subtitle']) && ($_POST ['updateid'])) {
		
		// Set information
		$set_info = array (
				"DisplayTitle = :DisplayTitle"
		);
		// Set parameters
		$params = array (
				":DisplayTitle" => $_POST ['subtitle'],
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":UpdateID" => $_POST ['updateid']
		);
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"UpdateID = :UpdateID"
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, array (
				$params
		) );
		
	} // end POST
} // end change Section Title

if ($_REQUEST['submit'] == "Change External Link") {
	
	if (($_POST ['exttitle']) && ($_POST ['url']) && ($_POST ['updateid'])) {
		
		// Set information
		$set_info = array (
				"DisplayTitle = :DisplayTitle",
				"URL = :URL"
		);
		// Set parameters
		$params = array (
				":DisplayTitle" => $_POST ['exttitle'],
				":URL"=>$_POST ['url'],
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":UpdateID" => $_POST ['updateid']
		);
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"UpdateID = :UpdateID"
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, array (
				$params
		) );
		
	} // end exttitle && url
} // end change External Link

if ($_REQUEST['submit'] == "Change Attachment") {
	
	if (($_POST ['atttitle']) && ($_FILES ['attfile'] ['type']) && ($_POST ['updateid'])) {
		
		$fileType = $_FILES ['attfile'] ['type'];
		$data = file_get_contents ( $_FILES ['attfile'] ['tmp_name'] );
		
		$e = explode ( '.', $_FILES ['attfile'] ['name'] );
		$ecnt = count ( $e ) - 1;
		$ext = $e [$ecnt];
		
		// Set information
		$set_info = array (
			"DisplayTitle = :DisplayTitle", 
			"AttachmentType = :AttachmentType", 
			"AttachmentExt = :AttachmentExt", 
			"Attachment = :Attachment"
		);
		// Set parameters
		$params = array (
			":DisplayTitle"=>$_POST ['atttitle'], 
			":AttachmentType"=>$fileType, 
			":AttachmentExt"=>$ext, 
			":Attachment"=>$data,
			":OrgID"=>$OrgID,
			":MultiOrgID"=>$MultiOrgID,
			":UpdateID"=>$_POST ['updateid']				
		);
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"UpdateID = :UpdateID"
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, array (
				$params
		) );
		
	} // end arrtitle && attfile
} // end change Attachment

if ($_REQUEST['submit'] == "Change HTML") {
	
	if (($_POST ['htmltitle']) && ($_POST ['html']) && ($_POST ['updateid'])) {

		// Set information
		$set_info = array (
				"DisplayTitle = :DisplayTitle",
				"HTML = :HTML"
		);
		// Set parameters
		$params = array (
				":DisplayTitle" => $_POST ['htmltitle'],
				":HTML"=> $_POST ['html'],
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":UpdateID" => $_POST ['updateid']
		);
		// Set where condition
		$where_info = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"UpdateID = :UpdateID"
		);
		// Update UserPortal Information
		$UserPortalInfoObj->updUserPortalInfo ( $set_info, $where_info, array (
				$params
		) );
		
	} // end POST
} // end change HTML
  
// end submits

echo '<form method="post" action="administration.php" enctype="multipart/form-data">' . "\n";

echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">' . "\n";

//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "SortOrder = 0");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get Userportal Information
$results = $UserPortalInfoObj->getUserPortalInfo("*", $where, 'SortOrder', array($params));
$maintitlecnt = $results['count'];

if (($maintitlecnt == 0) || ($change == "Y")) {
	echo '<tr><td><a href="javascript:ReverseDisplay(\'addsectiontitle\')">';
	echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
	echo $mainfunction . ' Main Section Title</b></a></td></tr>';
	echo '<tr><td><div id="addsectiontitle" style="display:' . $maintype . ';">';
	
	echo '<table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
	echo '<tr><td width="15"></td><td>Main Section Title: <input type="text" name="subtitle" size="30" maxlength="30" value="' . $maindisplaytitle . '">' . "</td></tr>";
	echo '<tr><td></td><td><input type="submit" name="submit" class="btn btn-primary" value="' . $mainfunction . ' Section Title"></td></tr>';
	echo '</table></div>';
	if ($mainfunction == "Change") {
		echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
	}
	echo '</div></td></tr>';
} // end maintitlecnt

echo '<tr><td><a href="javascript:ReverseDisplay(\'addlink\')">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
echo $extfunction . ' External Link</b></a></td></tr>';
echo '<tr><td><div id="addlink" style="display:' . $exttype . ';">';

echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr><td width="15"></td><td>Display Title: <input type="text" name="exttitle" size="30" maxlength="30" value="' . $extdisplaytitle . '">' . "</td></tr>";
echo '<tr><td></td><td>URL: <input type="text" name="url" size="40" maxlength="100" value="' . $exturl . '">' . " <i sytle=\"font-size:10pt;\">be sure to add (http:// or https://) if external.</i></td></tr>";
echo '<tr><td></td><td><input type="submit" name="submit" class="btn btn-primary" value="' . $extfunction . ' External Link"></td></tr>';
echo '</table></div>';
if ($extfunction == "Change") {
	echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
}

echo '</div></td></tr>';

echo '<tr><td><a href="javascript:ReverseDisplay(\'addattachment\')">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
echo $attfunction . ' Attachment</b></a></td></tr>';
echo '<tr><td><div id="addattachment" style="display:' . $atttype . ';">';

echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr><td width="15"></td><td>Display Title: <input type="text" name="atttitle" size="30" maxlength="30" value="' . $attdisplaytitle . '">' . "</td></tr>";
echo '<tr><td></td><td>File: <input type="file" name="attfile" size="20">' . "</td></tr>";
echo '<tr><td></td><td><input type="submit" name="submit" class="btn btn-primary" value="' . $attfunction . ' Attachment"></td></tr>';
echo '</table></div>';
if ($attfunction == "Change") {
	echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
}

echo '</div></td></tr>';

echo '<tr><td><a href="javascript:ReverseDisplay(\'addhtml\')">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">&nbsp;<b style="FONT-SIZE:8pt;COLOR:#000000">';
echo $htmlfunction . ' HTML Page</b></a></td></tr>';
echo '<tr><td><div id="addhtml" style="display:' . $htmltype . ';">';

echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">';
echo '<tr><td width="15"></td><td colspan="2">Display Title: <input type="text" name="htmltitle" size="30" maxlength="30" value="' . $htmldisplaytitle . '">' . "</td></tr>";
echo '<tr><td></td><td valign="top">Text:</td><td><textarea cols="80" rows="15" name="html" class="mceEditor">' . $htmltext . '</textarea>' . "</td></tr>";
echo '<tr><td></td><td colspan="2"><input type="submit" name="submit" value="' . $htmlfunction . ' HTML" class="btn btn-primary"></td></tr>';
echo '</table></div>';
if ($htmlfunction == "Change") {
	echo '<input type="hidden" name="updateid" value="' . $_GET ['edit'] . '">';
}

echo '</div></td></tr>';

echo '</table>' . "\n";
echo '<br>' . "\n";

//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get Userportal Information
$results = $UserPortalInfoObj->getUserPortalInfo("*", $where, 'SortOrder', array($params));
$ptlcnt = $results['count'];

if ($ptlcnt > 0) {
	// Display
	echo '<div class="table-responsive"><table border="0" cellspacing="0" cellpadding="3" class="table table-striped table-bordered">' . "\n";
	echo '<tr>';
	echo '<td align="left" width="200"><b>Type of Display</b></td>';
	echo '<td align="left" width="450"><b>Display Title</b></td>';
	echo '<td align="center"><b>Order</b></td>';
	echo '<td align="center" width="60"><b>Edit</b></td>';
	echo '<td align="center" width="60"><b>Delete</b></td>';
	echo '</tr>' . "\n";
	
	$cnt = 0;
	$rowcolor = "#eeeeee";
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $PTLINFO) {
			$cnt ++;
		
			echo '<tr>';
			echo '<tr bgcolor="' . $rowcolor . '">';
			echo '<td>' . $PTLINFO ['TypeLink'] . '</td>';
		
			echo '<td>';
			if ($PTLINFO ['TypeLink'] != "Main Section Title") {
				echo '<a href="';
					
				if ($PTLINFO ['TypeLink'] == "External Link") {
					echo $PTLINFO ['URL'];
				}
				if ($PTLINFO ['TypeLink'] == "Attachment") {
					echo IRECRUIT_HOME . 'administration/display_portalinfoattachment.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&updateid=' . $PTLINFO ['UpdateID'];
				}
				if ($PTLINFO ['TypeLink'] == "HTML Page") {
					echo IRECRUIT_HOME . 'administration.php?action=portalinfohtml&updateid=' . $PTLINFO ['UpdateID'];
				}
					
				echo '"';
					
				if ($PTLINFO ['TypeLink'] == "External Link") {
					echo ' target="_blank"';
				}
				echo '>';
			}
			echo $PTLINFO ['DisplayTitle'];
			if ($PTLINFO ['TypeLink'] != "Main Section Title") {
				echo '</a>';
			}
			echo '</td>';
			echo '<td align="center">';
		
			if ($PTLINFO ['SortOrder'] > 0) {
					
				if ($cnt > 2) {
					echo '<a href="' . IRECRUIT_HOME . 'administration.php?&action=' . $action . '&UpdateID=' . $PTLINFO ['UpdateID'] . '&SortOrder=' . $PTLINFO ['SortOrder'] . '&direction=up">';
					echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_up.png" width="20" height="16" title="Up" border="0" style="margin:0px 0px 0px 0px;">';
					echo '</a>';
				}
					
				if ($cnt < $ptlcnt) {
					echo '<a href="' . IRECRUIT_HOME . 'administration.php?&action=' . $action . '&UpdateID=' . $PTLINFO ['UpdateID'] . '&SortOrder=' . $PTLINFO ['SortOrder'] . '&direction=down">';
					echo '<img src="' . IRECRUIT_HOME . 'images/icons/bullet_go_down.png" width="20" height="16" title="Down" border="0" style="margin:0px 0px 0px 0px;">';
					echo '</a>';
				}
			} else {
				echo '<b>1</b>';
			}
			echo '</td>';
		
			echo '<td align="center"><a href="administration.php?action=portalinfo';
			if ($MultiOrgID != "") {
				echo '&MultiOrgID=' . $MultiOrgID;
			}
			echo '&edit=' . $PTLINFO ['UpdateID'] . '"><img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit"></a></td>';
			echo '<td align="center"><a href="administration.php?action=portalinfo';
			if ($MultiOrgID != "") {
				echo '&MultiOrgID=' . htmlspecialchars($MultiOrgID);
			}
			echo '&delete=' . $PTLINFO ['UpdateID'] . '" onclick="return confirm(\'Are you sure you want to delete this item?\n\nDisplay Title: ' . $PTLINFO ['DisplayTitle'] . '\nType of Display: ' . $PTLINFO ['TypeLink'] . '\n\n\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a></td>';
			echo '</tr>' . "\n";
		
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach
	}
	
	
	echo '</table></div>' . "\n";
} // end ptlcnt

echo '<input type="hidden" name="MAX_FILE_SIZE" value="10485760">';
echo '<input type="hidden" name="action" value="portalinfo">';
echo '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">';
echo '</form>' . "<br>";
?>