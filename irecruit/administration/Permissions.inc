<?php
include COMMON_DIR . 'process/FormFunctions.inc';

if ($process == "permissions") {
	
	//Set post values to same variables
	if(isset($_REQUEST['Active'])) $Active = $_REQUEST['Active'];
	if(isset($_REQUEST['Admin'])) $Admin =  $_REQUEST['Admin'];
	if(isset($_REQUEST['Requisitions'])) $Requisitions = $_REQUEST['Requisitions'];
	if(isset($_REQUEST['Requisitions_Edit'])) $Requisitions_Edit = $_REQUEST['Requisitions_Edit'];
	if(isset($_REQUEST['Requisitions_Costs'])) $Requisitions_Costs = $_REQUEST['Requisitions_Costs'];
	if(isset($_REQUEST['Correspondence'])) $Correspondence = $_REQUEST['Correspondence'];
	if(isset($_REQUEST['Forms'])) $Forms = $_REQUEST['Forms'];
	if(isset($_REQUEST['Forms_Edit'])) $Forms_Edit = $_REQUEST['Forms_Edit'];
	if(isset($_REQUEST['Internal_Forms'])) $Internal_Forms = $_REQUEST['Internal_Forms'];
	if(isset($_REQUEST['Internal_Forms_Edit'])) $Internal_Forms_Edit = $_REQUEST['Internal_Forms_Edit'];
	if(isset($_REQUEST['Reports'])) $Reports = $_REQUEST['Reports'];
	if(isset($_REQUEST['Exporter'])) $Exporter = $_REQUEST['Exporter'];
	if(isset($_REQUEST['Applicants'])) $Applicants = $_REQUEST['Applicants'];
	if(isset($_REQUEST['Applicants_Contact'])) $Applicants_Contact = $_REQUEST['Applicants_Contact'];
	if(isset($_REQUEST['Applicants_Forward'])) $Applicants_Forward = $_REQUEST['Applicants_Forward'];
	if(isset($_REQUEST['Applicants_Edit'])) $Applicants_Edit = $_REQUEST['Applicants_Edit'];
	if(isset($_REQUEST['Applicants_Delete'])) $Applicants_Delete = $_REQUEST['Applicants_Delete'];
	if(isset($_REQUEST['Applicants_Assign'])) $Applicants_Assign = $_REQUEST['Applicants_Assign'];
	if(isset($_REQUEST['Applicants_Onboard'])) $Applicants_Onboard = $_REQUEST['Applicants_Onboard'];
	if(isset($_REQUEST['Applicants_Update_Status'])) $Applicants_Update_Status = $_REQUEST['Applicants_Update_Status'];
	if(isset($_REQUEST['Applicants_Update_Final_Status'])) $Applicants_Update_Final_Status = $_REQUEST['Applicants_Update_Final_Status'];
	if(isset($_REQUEST['Applicants_Affirmative_Action'])) $Applicants_Affirmative_Action = $_REQUEST['Applicants_Affirmative_Action'];
	if(isset($_REQUEST['Interview_Admin'])) $Interview_Admin = $_REQUEST['Interview_Admin'];
	
	
	if ($Requisitions == 0) {
		$Requisitions = 0;
		$Requisitions_Edit = 0;
		$Requisitions_Costs = 0;
	}
	
	// if all forms reports off then set reports parent permission to off as well
	if ($Applicants == 0) {
		$Applicants = 0;
		$Applicants_Contact = 0;
		$Applicants_Forward = 0;
		$Applicants_Edit = 0;
		$Applicants_Delete = 0;
		$Applicants_Assign = 0;
		$Applicants_Onboard = 0;
		$Applicants_Update_Status = 0;
		$Applicants_Update_Final_Status = 0;
		$Applicants_Affirmative_Action = 0;
	}
	
	if ($Forms == 0) {
		$Forms = 0;
		$Forms_Edit = 0;
	}
	
	if ($Internal_Forms == 0) {
		$Internal_Forms = 0;
		$Internal_Forms_Edit = 0;
	}
	
	if ($Correspondence == 0) {
		$Correspondence = 0;
	}
	
	if ($Admin == 0) {
		$Admin = 0;
	}
	
	
	//Set Information
	$set_info = array("Active = :Active", 
		   "Admin = :Admin",
		   "Requisitions = :Requisitions",
		   "Requisitions_Edit = :Requisitions_Edit",
		   "Requisitions_Costs = :Requisitions_Costs",
		   "Correspondence = :Correspondence",
		   "Forms = :Forms",
		   "Forms_Edit = :Forms_Edit",
		   "Internal_Forms = :Internal_Forms",
		   "Internal_Forms_Edit = :Internal_Forms_Edit",
		   "Reports = :Reports",
		   "Exporter = :Exporter",
		   "Applicants = :Applicants",
		   "Applicants_Contact = :Applicants_Contact",
		   "Applicants_Forward = :Applicants_Forward",
		   "Applicants_Edit = :Applicants_Edit",
		   "Applicants_Delete = :Applicants_Delete",
		   "Applicants_Assign = :Applicants_Assign",
		   "Applicants_Onboard = :Applicants_Onboard",
		   "Applicants_Update_Status = :Applicants_Update_Status",
		   "Applicants_Update_Final_Status = :Applicants_Update_Final_Status",
		   "Applicants_Affirmative_Action = :Applicants_Affirmative_Action",
		   "Interview_Admin = :Interview_Admin");
	//Set parameters
	$params = array(
			":Active"=>$Active,
			":Admin"=>$Admin,
			":Requisitions"=>$Requisitions,
			":Requisitions_Edit"=>$Requisitions_Edit,
			":Requisitions_Costs"=>$Requisitions_Costs,
			":Correspondence"=>$Correspondence,
			":Forms"=>$Forms,
			":Forms_Edit"=>$Forms_Edit,
			":Internal_Forms"=>$Internal_Forms,
			":Internal_Forms_Edit"=>$Internal_Forms_Edit,
			":Reports"=>$Reports,
			":Exporter"=>$Exporter,
			":Applicants"=>$Applicants,
			":Applicants_Contact"=>$Applicants_Contact,
			":Applicants_Forward"=>$Applicants_Forward,
			":Applicants_Edit"=>$Applicants_Edit,
			":Applicants_Delete"=>$Applicants_Delete,
			":Applicants_Assign"=>$Applicants_Assign,
			":Applicants_Onboard"=>$Applicants_Onboard,
			":Applicants_Update_Status"=>$Applicants_Update_Status,
			":Applicants_Update_Final_Status"=>$Applicants_Update_Final_Status,
			":Applicants_Affirmative_Action"=>$Applicants_Affirmative_Action,
			":Interview_Admin"=>$Interview_Admin,
			":UserID"=>$UserIDedit
	);
	//Set where condition
	$where_info = array("UserID = :UserID");
	
	//Update Application Features based on UserID
	$IrecruitApplicationFeaturesObj->updApplicationPermissionsByUserID($set_info, $where_info, array($params));
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Permissions configured!')";
	echo '</script>';
} // end process
function selectPermission($name, $limit, $un, $viewonly) {
	global $IrecruitApplicationFeaturesObj, $IrecruitUsersObj;
	//Set where condition
	$where = array();
	if ($limit == "onoff") {
		$where = array("FunctionID < 2");
	} elseif ($limit == "nodelete") {
		$where = array("FunctionID < 3");
	}
	//Get ApplicationFunctions Information
	$results = $IrecruitApplicationFeaturesObj->getApplicationFunctionsInfo("FunctionID, Description", $where, "", array());
	
	// choose users specific setting
	$permission = $IrecruitApplicationFeaturesObj->getApplicationPermissionsByUserID($name, $un, 'fetchRow');
	// end user setting
	
	$val = "<select name=\"" . $name . "\">";
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
		
			if ($permission [0] == $row ['FunctionID']) {
				$valview = $row ['Description'];
				$selected = ' selected="selected"';
			} else {
				$selected = '';
			}
			$val .= "<option value=\"" . $row ['FunctionID'] . "\"" . $selected . ">" . $row ['Description'];
		}
	}
	
	
	$val .= "</select>";
	
	if ($viewonly == "1") {
		$val = $valview;
	}
	
	return $val;
} // end function
  
// check users role
$results_role = $IrecruitUsersObj->getUserInfoByUserID($UserIDedit, "Role");
$role = $results_role['Role'];

// if user has a role other than use role permissions other wise user user's permission

if ($role != "") {
	$permUserId = $role;
	$viewOnly = 1;
	$formHeader = "User Permissions for: <b>" . $UserIDedit . "</b><br />Role applied: <b>" . DisplayRole ( $role ) . "</b>";
} else {
	$permUserId = $UserIDedit;
	$viewOnly = 0;
	$formHeader = "Select User Permissions for: <b>" . $UserIDedit . "</b>";
	$formHeader .= "<br>Current Role: <b>" . DisplayRole ( $role ) . "</b>";
}
?>
<div class="table-responsive">
	<form method="post" action="administration.php">
		<table border="0" cellspacing="3" cellpadding="5" width="770"
			class="table table-striped table-bordered table-hover">
			<tr>
				<td colspan="100%" valign="middle">
					<p><?php echo $formHeader; ?></p>
				</td>
			</tr>
<?php
$sectionIndex = 1;

// remove the permissions to feature if not set
if (($feature ['WebForms'] != "Y") && ($feature ['AgreementForms'] != "Y") && ($feature ['SpecificationForms'] != "Y") && ($feature ['PreFilledForms'] != "Y")) {
	foreach ( $permissionList as $k => $v ) {
		if (($v == "Internal_Forms") || ($v == "Internal_Forms_Edit")) {
			unset ( $permissionList [$k] );
		} // end v
	} // end foreach
} // end feature Forms

foreach ( $permissionList as $s ) {
	$column = $s;
	
	$columndisplay = preg_replace ( '/\_/i', ' ', $s );
	
	// bold main categories
	if (($column == "Active") || ($column == "Admin") || ($column == "Requisitions") || ($column == "Correspondence") || ($column == "Forms") || ($column == "Internal_Forms") || ($column == "Reports") || ($column == "Exporter") || ($column == "Applicants") || ($column == "Interview_Admin")) {
		$hl = "<b>";
		$hl2 = "</b>";
		$sp = " valign=\"bottom\" height=\"50\"";
		$limit = "onoff";
	} else {
		$hl = "";
		$hl2 = "";
		$sp = "";
		$limit = "nodelete";
	}
	
	// restrict Reports permissions options to Off / View (On)
	if ($column == "Requisitions_Edit" or $column == "Requisitions_Costs" or $column == "Exporter") {
		$limit = "onoff";
	}
	
	// restrict permission options to Off / View (On)
	if ($column == "Applicants_Contact" or $column == "Applicants_Forward" or $column == "Applicants_Edit" or $column == "Applicants_Delete" or $column == "Applicants_Assign" or $column == "Applicants_Onboard" or $column == "Applicants_Update_Status" or $column == "Applicants_Update_Final_Status" or $column == "Applicants_Affirmative_Action") {
		$limit = "onoff";
	}
	
	if ($column == "Forms_Edit") {
		$limit = "onoff";
	}
	
	if ($column == "Internal_Forms_Edit") {
		$limit = "onoff";
	}
	if ($columndisplay == 'Active') {
		echo "<tr><td align=\"right\"" . $sp . " width=\"50%\">" . $hl . $columndisplay . $hl2 . ":</td><td width=\"50%\"" . $sp . ">" . selectPermission ( $column, $limit, $UserIDedit, '0' ) . "</td></tr>";
	} else {
		/*edge*/
		echo "<tr><td align=\"right\"" . $sp . " width=\"50%\">" . $hl . $columndisplay . $hl2 . ":</td><td width=\"50%\"" . $sp . ">" . selectPermission ( $column, $limit, $permUserId, $viewOnly ) . "</td></tr>";
	}

}
?>

			<tr>
				<td colspan="100%" align="center" valign="middle" height="60"><input
					type="hidden" name="UserIDedit" value="<?php echo $UserIDedit ?>" /> <input
					type="hidden" name="process" value="permissions" /> <input
					type="hidden" name="action" value="useredit" /> <input
					type="hidden" name="useraction" value="permissions" />
	<input type=submit value="Update User Permissions"
					class="btn btn-primary" />
			</td></tr>
		</table>
	</form>
</div>
