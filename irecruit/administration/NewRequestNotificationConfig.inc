<?php 
if(isset($_REQUEST['config_request_emails_list'])
	&& $_REQUEST['config_request_emails_list'] == "Submit")
{
	//Insert notification emails list
	$RequestObj->insRequestNotificationConfig($OrgID, '');
	echo '<script>location.href="administration.php?action=newrequestnotificationconfig&menu=8";</script>';
}

$admin_emails_list_selected = array();
//Get notification config information
$notification_config_info   = $RequestObj->getRequestNotificationConfigInfo($OrgID, '');
if($notification_config_info['EmailsList'] != "") {
    $admin_emails_list_selected = json_decode($notification_config_info['EmailsList'], true);
}

// tap administrators that there is an approval sent
//set where condition
$where = array("OrgID = :OrgID", "Role in ('master_admin','')");
//set parameters
$params = array(":OrgID"=>$OrgID);
//get user information
$resultsIN = $IrecruitUsersObj->getUserInformation("EmailAddress", $where, "", array($params));

if(is_array($resultsIN['results'])) {
	foreach ($resultsIN['results'] as $ADUSERS) {
		$admin_emails_list[] = $ADUSERS ['EmailAddress'];
	} // end foreach
}

$admin_list_count       =   count($admin_emails_list);
$admin_list_sel_count   =   count($admin_emails_list_selected);

if ($admin_list_sel_count == 0) {
    $admin_emails_list_selected = array();
}
?>
<h4>Admin Emails List:</h4>
<form name="frmAdminEmailsList" id="frmAdminEmailsList" method="post">
    <input type="hidden" name="action" value="newrequestnotificationconfig">
    <?php
    for($ael = 0; $ael < $admin_list_count; $ael++) {
    	echo "<input type='checkbox' name='admin_emails[]' value='".$admin_emails_list[$ael]."'";
    	if(in_array($admin_emails_list[$ael], $admin_emails_list_selected)) echo " checked='checked'";
    	echo ">&nbsp;".$admin_emails_list[$ael]."<br>";
    }
    ?>
    <br><input type="submit" class="btn btn-primary" name="config_request_emails_list" id="config_request_emails_list" value="Submit">
</form>