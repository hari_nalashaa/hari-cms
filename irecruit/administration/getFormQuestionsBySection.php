<?php
require_once '../Configuration.inc';

$FormID         =   "STANDARD";
//Get FormQuestions Information
$columns        =   "Question, QuestionID, QuestionTypeID";
$where_info     =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = :Active");
$params         =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$_REQUEST['SectionID'], ":Active"=>"Y");
$FormQuestions  =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where_info, "", array($params));
 

$select_options =   "";

$FormQuestionsCount =   $FormQuestions['count'];
$FQ                 =   $FormQuestions['results'];

for($r = 0; $r < $FormQuestionsCount; $r++) {

    $where          =   array("OrgID = :OrgID","QuestionID = :QuestionID");
    $params_info    =   array(":OrgID"=>$OrgID,"QuestionID" =>$FQ[$r]['QuestionID']);
    $app_sum_que_results = $ApplicantsObj->getComparativeAnalysisQuestionsInfo("*", $where, '', '', array($params_info));
    $app_sum_que = $app_sum_que_results['results'];
    if(!empty($app_sum_que)){
            $question_disp = $FQ[$r]['Question'].' (This question is already in the list)';
    }else{
           $question_disp =  $FQ[$r]['Question'];

    }

    $select_options .= "<option value='".$FQ[$r]['QuestionID']."'>".$question_disp."</option>";
}

$select_option_start = "<select name='ddlFormQuestions' id='ddlFormQuestions' style='width:300px' onchange='$(\"#Question\").val($(this).find(\"option:selected\").text())'>";
$select_option_start .= "<option value=''>Select Question</option>";

$select_option_end = "</select>";

echo $select_option_start . $select_options . $select_option_end . $input_questions_type;
?>