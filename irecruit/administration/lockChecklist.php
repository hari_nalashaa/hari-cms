<?php
include '../Configuration.inc';

if ($_GET['ChecklistID'] != "") {
    if ($_GET['unlock'] == "y") { $locked = "N"; } else { $locked = "Y"; }
    $ChecklistID    =   $_GET['ChecklistID'];
    $now            = date('Y-m-d H:i:s');
    $set_info       =   array("Locked = :Locked","LastUpdated = :LastUpdated");
    $where_info     =   array("OrgID = :OrgID", "ChecklistID = :ChecklistID");
    $params         =   array(":Locked"=>$locked, ":LastUpdated"=>$now, ":OrgID"=>$OrgID, ":ChecklistID"=>$ChecklistID);
    //Update Status Category Information
    G::Obj('Checklist')->updChecklist($set_info, $where_info, array($params));
}

    header("Location:".IRECRUIT_HOME."administration/checklist.php?active=".$_REQUEST['active']);
    exit;
?>
