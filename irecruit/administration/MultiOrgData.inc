<?php
if ($process == "Y") {
	
	if (strlen ( $_REQUEST['OrganizationName'] ) > 5) {
		
		//Get User EmailAddress
		$user_info = $IrecruitUsersObj->getUserInfoByUserID($USERID, "EmailAddress");
		$EmailAddress = $user_info['EmailAddress'];
		
		$MultiOrgID = uniqid ();
		
		//Organization Data Information
		$org_data_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrganizationName"=>$_REQUEST['OrganizationName'], "ContactEmail"=>$EmailAddress, "ClientSince"=>"NOW()");
		//Insert OrgData Information
		$OrganizationsObj->insOrganizationInfo('OrgData', $org_data_info);
		
		
		//OrganizationLevels Information
		$org_levels_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>"1", "OrganizationLevel"=>"Location");
		//Insert OrgData Information
		$OrganizationsObj->insOrganizationInfo('OrganizationLevels', $org_levels_info);
		
		
		//OrganizationLevelData Information
		$org_levels_info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>"1", "SelectionOrder"=>"1", "CategorySelection"=>'Connecticut');
		//Insert OrganizationLevelData Information
		$OrganizationsObj->insOrganizationInfo('OrganizationLevelData', $org_levels_info);
		
		$OrganizationName = "";
	} else {
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('Please enter a Organization Name.')";
		echo '</script>';
	}
} // end process

echo '<div class="table-responsive">';
echo '<br><a href="javascript:ReverseDisplay(\'addlink\')">';
echo '<img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add Organization</b></a>';

if ($BOOTSTRAP_SKIN == true) {
	echo '<br><br><a href="' . IRECRUIT_HOME . 'home.php?pg=orgcenter">Career Center</a>';
} else {
	echo '<br><br><a href="' . IRECRUIT_HOME . 'index.php?pg=orgcenter">Career Center</a>';
}

echo '<div id="addlink" style="display:none;padding:10px 10px 0px 10px;">';
echo '<form method="post" action="administration.php">';
echo 'Organization Name: <input type="text" name="OrganizationName" value="' . $OrganizationName . '" size="50" maxlength="60">';
echo '&nbsp;&nbsp;';
echo '<input type="hidden" name="action" value="multiorg">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Add Organization">';
echo '</form>';
echo '</div>';

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Organization Data Information
$results = $OrganizationsObj->getOrgDataInfo("*", $where, '', array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $OD) {
	
		echo '<br><br><b>' . $OD ['OrganizationName'] . "</b><br>";
		foreach ( $subtitle as $key => $desc ) {
	
			if ($key != "multiorg") {
				echo '&nbsp;&nbsp;&nbsp;';
				echo '<a href="administration.php?action=' . $key . '&OrgID=' . $OD ['OrgID'];
				if ($OD ['MultiOrgID'] != "") {
					echo '&MultiOrgID=' . $OD ['MultiOrgID'];
				}
				echo '">';
				echo $desc;
				echo '</a><br>' . "\n";
			} // end if key
		} // end foreach
	} // end foreach
}

echo '</div>';
?>