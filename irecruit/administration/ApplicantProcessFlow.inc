<?php
if ($_REQUEST ['process'] == 'Y') {

	//echo '<pre>' . print_r($_POST,true) . "</pre>";
	
	$i = 1;
	while ( $i <= 999) {
		
		$order        =   'order-' . $i;
		$display      =   'display-' . $i;
		$type         =   'type-' . $i;
		$searchable   =   'searchable-' . $i;
		$active       =   'active-' . $i;
		
		if (($_POST [$order]) && ($_POST [$display])) {
			
			$ins_order       = is_null($_POST [$order]) ? '' : $_POST [$order];
			$ins_display     = is_null($_POST [$display]) ? '' : $_POST [$display];
			$ins_type        = is_null($_POST [$type]) ? '' : $_POST [$type];
			$ins_searchable  = is_null($_POST [$searchable]) ? '' : $_POST [$searchable];
			$ins_active	 = is_null($_POST [$active]) ? 'N' : $_POST [$active];
			
			if(isset($_POST['rdFinalStage']) && $_POST['rdFinalStage'] == $ins_order) {
			    $ins_final_stage = 'Yes';
			}
			else {
			    $ins_final_stage = 'No';
			}
			
			//Applicant Process Flow Information
			$app_process_info = array(
                                    "OrgID"         =>  $OrgID, 
                                    "ProcessOrder"  =>  $ins_order, 
                                    "Description"   =>  $ins_display, 
                                    "Type"          =>  $ins_type, 
                                    "Searchable"    =>  $ins_searchable,
                                    "IsFinal"       =>  $ins_final_stage, 
                                    "Active"        =>  $ins_active
									);
			//On Duplicate Update the description, type, Searchable
			$on_duplicate    = " ON DUPLICATE KEY UPDATE Description = :UDescription, Type = :UType, Searchable = :USearchable, IsFinal = :UIsFinal, Active = :UActive";
			//Update Information
			$update_info     = array(":UDescription"=>$ins_display, ":UType"=>$ins_type, ":USearchable"=>$ins_searchable, ":UIsFinal"=>$ins_final_stage, ":UActive"=>$ins_active);
			//Insert Applicant Process Flow Information
			$ApplicantsObj->insApplicantProcessFlow($app_process_info, $on_duplicate, $update_info);
			
		} // end if order and display

		if ($_POST [$display] == "delete") {
			if(isset($_POST [$order]) && $_POST [$order] != "") {
				//set where condition
				$where = array("OrgID = :OrgID", "ProcessOrder = :ProcessOrder");
				//set parameters
				$params = array(":OrgID"=>$OrgID, ":ProcessOrder"=>$_POST [$order]);
				//delete applicant process
				$ApplicantsObj->delApplicantProcessFlow($where, array($params));
			}
		} // end display delete
		
		$i ++;
	} // end while
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Applicant Process Flow configured!')";
	echo '</script>';
}

?>
<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" width="770"
		class="table table-striped table-bordered table-hover">
		<tr>
			<td colspan="100%"><font style="color: red">Warning!</font> This is
				an application set up value. The process order does not need to be
				numbered consecutively. The processing order will remain attached to
				an applicant when an applicant is submitted and any new text value
				will be assigned to that processing value.</td>
		</tr>
	</table>
</div>

<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" align="left"
		class="table table-striped table-bordered table-hover">
		<form method=post action=administration.php>
			<tr>
				<td colspan="100%" height="6"></td>
			</tr>
			<tr>
    			<td width="60">&nbsp;</td>
    			<td valign="middle" align="center" width="60"><strong>Process<br>Order</strong></td>
    			<td valign="middle" align="left"><strong>Description</strong></td>
    			<td valign="middle" align="center" width="60"><strong>Type</strong></td>
    			<td valign="middle" align="center" width="60"><strong>Final<br>Process</strong></td>
    			<td valign="middle" align="center" width="60"><strong>Hired<br>Status</strong></td>
    			<td valign="middle" align="center" width="60"><strong>Active</strong></td>
			</tr>
<?php
//set where condition
$where = array("OrgID = :OrgID");
//set parameters
$params = array(":OrgID"=>$OrgID);
//Get Applicant Process Flow Information
$results = $ApplicantsObj->getApplicantProcessFlowInfo("*", $where, 'ProcessOrder', array($params));

if(is_array($results['results'])) {
	$i = 1;
	foreach($results['results'] as $row) {

		$hold=$row ['ProcessOrder'];

		echo '<tr>';
		echo '<td>';
		echo '&nbsp;';
		echo '</td>';

		if ($i == 1) {

			echo '<td align="center">';
			echo $row ['ProcessOrder'] . '<input type="hidden" name="order-' . $i . '" value="' . $row ['ProcessOrder'] . '">';
			echo '</td>';

			echo '<td>';
			echo '<input type="hidden" name="display-' . $i . '" value="' . $row ['Description'] . '">';
			echo $row ['Description'];
			echo '</td>';

			echo '<td align="center">';
			echo '<input type="hidden" name="type-' . $i . '" value="' . $row ['Type'] . '">';
			echo 'Application';
			echo '</td>';

			echo '<td align="center">';
			echo '<input type="hidden" name="searchable-' . $i . '" value="' . $row ['Searchable'] . '">';
			echo ($row['Searchable'] == "N") ? 'Yes' : 'No';
			echo '</td>';

			echo '<td align="center">';
			echo '<input type="hidden" name="rdFinalState-' . $i . '" value="' . $row['IsFinal']. '">';
			echo $row ['IsFinal']; 
			echo '</td>';

			echo '<td align="center">';
			echo '<input type="hidden" name="active-' . $i . '" value="' . $row ['Active'] . '">';
			echo ($row['Active'] == "N") ? 'No' : 'Yes';
			echo '</td>';

		} else {

		   echo '<td align="center">';
		   if ($row ['Active'] == "Y") {
			echo '<input type="text" name="order-' . $i . '" size=2 maxlength=3 value="' . $row ['ProcessOrder'] . '">';
		   } else {
			echo '<input type="hidden" name="order-' . $i . '" value="' . $row ['ProcessOrder'] . '">';
			echo $row ['ProcessOrder'];
		   }
		   echo '</td>';

		   echo '<td>';
		   if ($row ['Active'] == "Y") {
			echo '<input type="text" name="display-' . $i . '" size=30 maxlength=60 value="' . $row ['Description'] . '">';
		   } else {
		   	echo '<input type="hidden" name="display-' . $i . '" value="' . $row ['Description'] . '">';
		   	echo $row ['Description'];
		   }
		   echo '</td>';

		   $TYPE = array("S"=>"Status","C"=>"Corespondence");

		   echo '<td align="center">';
		   if ($row ['Active'] == "Y") {

			echo '<select name="type-' . $i . '">';
			foreach ($TYPE as $n=>$v) {
				echo '<option value="' . $n . '"';
				if ($n == $row['Type']) { echo ' selected'; }
				echo '>';
				echo $v;
				echo '</option>';
			} // end foreach
			echo '</select>';

		   } else {
			echo '<input type="hidden" name="type-' . $i . '" value="' . $row ['Type'] . '">';
			echo $TYPE[$row ['Type']];

		   }
		   echo '</td>';

		   echo '<td align="center">';
		   if ($row ['Active'] == "Y") {
			echo '<input type="checkbox" name="searchable-' . $i . '" value="N"';
			if ($row ['Searchable'] == "N") {
			   echo ' checked';
			}
			echo '>';

		   } else {
			echo '<input type="hidden" name="searchable-' . $i . '" value="' . $row ['Searchable'] . '">';
			echo ($row['Searchable'] == "N") ? 'Yes' : 'No';
		   }
		   echo '</td>';

		   echo '<td align="center">';
		   if ($row ['Active'] == "Y") {
			echo '<input type="radio" name="rdFinalStage" id="rdFinalStage'. $i .'" value="'. $row ['ProcessOrder'] .'"';
			if($row ['IsFinal'] == 'Yes') echo ' checked="checked"';
			echo '>';
		   } else {
			echo '<input type="hidden" name="rdFinalState-' . $i . '" value="' . $row['IsFinal']. '">';
			echo $row ['IsFinal']; 
		   }
		   echo '</td>';

		   echo '<td align="center">';
		   echo '<input type="checkbox" name="active-' . $i . '" value="Y"';
		   if ($row ['Active'] == "Y") { echo ' checked'; }
		   echo '>';
		   echo '</td>';

		} // end rows > 1

		echo '</tr>';
		$i ++;
	}
}

$i  =   $hold+10;
$ii =   $i + 1;
while ( $i < $ii ) {
	echo '<tr>';
	echo '<td>&nbsp;</td>';
	
	echo '<td align="center">';
	echo '<input type="text" name="order-' . $i . '" size=2 maxlength=3 value="' . $i . '">';
	echo '</td>';
	
	echo '<td>';
	echo '<input type="text" name="display-' . $i . '" size="30" maxlength="60">';
	echo '</td>';
	
	echo '<td align="center">';
	echo '<select name="type-' . $i . '">';
	echo '<option value="S">Status</option>';
	echo '<option value="C">Correspondence</option>';
	echo '</select>';
	echo '</td>';
	
	echo '<td align="center">';
	echo '<input type="checkbox" name="searchable-' . $i . '" value="N">';
	echo '</td>';

	echo '<td align="center">';
	echo '<input type="radio" name="rdFinalStage" id="rdFinalStage'. $i .'" value="'. $row ['ProcessOrder'] .'">';
	echo '</td>';

	echo '<td align="center">Yes';
	echo '<input type="hidden" name="active-' . $i . '" value="Y">';
	echo '</td>';
	
	echo '</tr>';
	$i ++;
}

$me = $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, '-2' );

echo '<tr>';
echo '<td>&nbsp;</td>';
echo '<td align="center">999</td>';
echo '<td>' . $ApplicantDetailsObj->getProcessOrderDescription ( $OrgID, '-2' ) . '</td>';
echo '<td align="center">Application</td>';
echo '<td align="center"><img src="' . IRECRUIT_HOME . 'images/checkmark.jpg" width="60"></td>';
echo '<td align="center">No</td>';
echo '<td align="center">Yes</td>';
echo '</tr>';
echo '</table>';
echo '</div>';

echo '<div style="clear:both"></div><div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="770" class="table table-striped table-bordered table-hover">';
echo '<tr><td width="60">&nbsp;</td><td>';
echo '<p><span style="color:red;">To remove a selection;</span> enter the word "delete" in the description field and update.</p>';
echo '<p><span style="color:red;">Marking a status as "Final Process"</span> will determine if the application is ';
echo 'searchable when set to this status code. The "Processed Applicants" ';
echo 'selection of the Search will be affected.</p>';
echo '</td><td width="60">&nbsp;</td></tr>';

echo '<tr><td align="center" colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="processflow">';
echo '<input type="hidden" name="process" value="Y">';
echo '<center><input type="submit" value="Update Process Flow" class="btn btn-primary"></center></td></tr>';
echo '</form>';
echo '</table>';
echo '</div>';
?>
