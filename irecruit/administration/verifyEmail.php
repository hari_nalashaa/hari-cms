<?php
$PAGE_TYPE = "PopUp";
require_once '../Configuration.inc';

$TemplateObj->title         =   $title      =   'Verify Email';
$TemplateObj->DLEmail       =   $DLEmail    =   isset ( $_REQUEST ['DLEmail'] ) ? $_REQUEST ['DLEmail'] : '';
$TemplateObj->MultiOrgID    =   $MultiOrgID =   isset ( $_REQUEST ['MultiOrgID'] ) ? $_REQUEST ['MultiOrgID'] : '';
$TemplateObj->User          =   $User       =   isset ( $_REQUEST ['User'] ) ? $_REQUEST ['User'] : '';
$TemplateObj->OrgEmail      =   $OrgEmail   =   isset ( $_REQUEST ['OrgEmail'] ) ? $_REQUEST ['OrgEmail'] : '';

if ($User) {
	
	// Set where condition
	$where = array (
			"U.UserID    =   AP.UserID",
			"AP.Active   =   1",
			"U.UserID    =   :UserID" 
	);
	// Set parameters
	$params = array (
			":UserID"    =>  $User 
	);
	// Irecruit Application Permissions Information
	$results = $IrecruitApplicationFeaturesObj->getApplicationPermissionsInfo ( "Users U, ApplicationPermissions AP", "U.OrgID", $where, "", array (
			$params 
	) );
	
	$OrgID = $results ['results'] [0] ['OrgID'];
}

if ($OrgID) {
	
	$i = 0;
	if ($OrgEmail) {
		
		// Set EmailVerified information
		$set_info = array (
				"EmailVerified = 1" 
		);
		// Set where condition
		$where = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"Email = :Email" 
		);
		// Set parameters
		$params = array (
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":Email" => $OrgEmail 
		);
		// Update Organization Email Information
		$OrganizationsObj->updOrganizationInfo ( 'OrganizationEmail', $set_info, $where, array (
				$params 
		) );
		
		$i ++;
	} elseif ($DLEmail) {
		
		// Set EmailVerified information
		$set_info = array (
				"DeadLetterEmailVerified = 1" 
		);
		// Set where condition
		$where = array (
				"OrgID = :OrgID",
				"MultiOrgID = :MultiOrgID",
				"DeadLetterEmail = :DeadLetterEmail" 
		);
		// Set parameters
		$params = array (
				":OrgID" => $OrgID,
				":MultiOrgID" => $MultiOrgID,
				":DeadLetterEmail" => $DLEmail 
		);
		// Update Organization Email Information
		$OrganizationsObj->updOrganizationInfo ( 'OrganizationEmail', $set_info, $where, array (
				$params 
		) );
		
		$i ++;
	}
	
	if ($i > 0) {
		echo "<div id=\"centerDoc\">";
		echo "Thank you.<br>Your email has been verified.<br><br>";
		$link = IRECRUIT_HOME . "administration.php?action=orgemail";
		if ($MultiOrgID != "") {
			$link .= "&MultiOrgID=" . $MultiOrgID;
		}
		echo "You may verify here: <a href=\"" . $link . "\">" . $link . "</a>";
		echo "</div>";
	}
} else {
	
	echo "You have reached this page in error. Please be sure the link you are entering is accurate.";
} // end hit
?>