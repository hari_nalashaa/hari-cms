<?php
$TemplateObj->form 			= $form 			= isset($_REQUEST['form']) ? $_REQUEST['form'] : '';
$TemplateObj->add 			= $add 				= isset($_REQUEST['add']) ? $_REQUEST['add'] : '';
$TemplateObj->cnt 			= $cnt 				= isset($_REQUEST['cnt']) ? $_REQUEST['cnt'] : '';
$TemplateObj->default 		= $default 			= isset($_REQUEST['default']) ? $_REQUEST['default'] : '';
$TemplateObj->new 			= $new 				= isset($_REQUEST['new']) ? $_REQUEST['new'] : '';
$TemplateObj->section 		= $section 			= isset($_REQUEST['section']) ? $_REQUEST['section'] : '';
$TemplateObj->Question 		= $Question 		= isset($_REQUEST['Question']) ? $_REQUEST['Question'] : '';
$TemplateObj->questionid 	= $questionid 		= isset($_REQUEST['questionid']) ? $_REQUEST['questionid'] : '';
$TemplateObj->QuestionOrder = $QuestionOrder 	= isset($_REQUEST['QuestionOrder']) ? $_REQUEST['QuestionOrder'] : '';
$TemplateObj->direction 	= $direction 		= isset($_REQUEST['direction']) ? $_REQUEST['direction'] : '';
$TemplateObj->process 		= $process 			= isset($_REQUEST['process']) ? $_REQUEST['process'] : '';
$TemplateObj->print 		= $print 			= isset($_REQUEST['print']) ? $_REQUEST['print'] : '';
$TemplateObj->useraction 	= $useraction 		= isset($_REQUEST['useraction']) ? $_REQUEST['useraction'] : '';
$TemplateObj->UserIDedit	= $UserIDedit 		= isset($_REQUEST['UserIDedit']) ? $_REQUEST['UserIDedit'] : '';
$TemplateObj->adduser 		= $adduser 			= isset($_REQUEST['adduser']) ? $_REQUEST['adduser'] : '';
$TemplateObj->ProcessOrder 	= $ProcessOrder 	= isset($_REQUEST['ProcessOrder']) ? $_REQUEST['ProcessOrder'] : '';
$TemplateObj->UserID 		= $UserID 			= isset($_REQUEST['UserID']) ? $_REQUEST['UserID'] : '';
$TemplateObj->Active 		= $Active 			= isset($_REQUEST['Active']) ? $_REQUEST['Active'] : '';
$TemplateObj->Required 		= $Required 		= isset($_REQUEST['Required']) ? $_REQUEST['Required'] : '';
$TemplateObj->Clear 		= $Clear 			= isset($_REQUEST['Clear']) ? $_REQUEST['Clear'] : '';
$TemplateObj->sort 			= $sort 			= isset($_REQUEST['sort']) ? $_REQUEST['sort'] : '';
$TemplateObj->D 			= $D 				= isset($_REQUEST['D']) ? $_REQUEST['D'] : '';
$TemplateObj->R 			= $R 				= isset($_REQUEST['R']) ? $_REQUEST['R'] : '';
$TemplateObj->config 		= $config 			= isset($_REQUEST['config']) ? $_REQUEST['config'] : '';

if(!isset($MultiOrgID) || is_null($MultiOrgID)) {
	$TemplateObj->MultiOrgID = $MultiOrgID = "";	
}

if(isset($_REQUEST['MultiOrgID'])) {
	$TemplateObj->MultiOrgID = $MultiOrgID = $_REQUEST['MultiOrgID'];
}

$subtitle = array (
		"useredit" 						=> "Manage Users",
		"userpreferences" 				=> "User Preferences",
		"applicantsummaryquestions" 	=> "Applicant Summary Questions",
		"applicantlabels" 				=> "Applicant Labels",
		"documentszipdownload"			=> "DocumentsZip Download Options",
		"multiorg" 						=> "Multiple Organizations",
		"orgdata" 						=> "Organization Data",
		"orglevels" 					=> "Organization Levels",
		"orgapplicationprocess" 		=> "Organization Application Process",
		"orgtaxnumbers" 				=> "Organization Tax Numbers",
		"orgdescription" 				=> "Organization Description",
		"orgcolors" 					=> "Organization Colors",
		"orgemail" 						=> "Organization Email",
		"orglogos" 						=> "Organization Logos",
		"entrymethods" 					=> "Career Center Setup",
		"portalinfo" 					=> "User Portal Information",
		"socialmedia" 					=> "Social Media Sharing",
		"publishreqs" 					=> "Publish Requisitions",
		"applicationnumbers" 			=> "Application ID",
		"processflow" 					=> "Applicant Process Flow",
		"disposition" 					=> "Disposition Codes",
		"emailapprovers" 				=> "Email Approvers",
		"emailresponses" 				=> "Email Responses",
		"iconnectemailtemplate" 		=> "iConnect Email Template",
		"newrequestnotificationconfig" 	=> "New Request Notification Configuration",
		"scheduleinterviewmail" 		=> "Appointment Email Message",
		"formsectiontitles" 			=> "Form Section Titles",
		"ofccptext" 					=> "Prescreening Questions",
		"autoforwardlist" 				=> "Auto Forward List",
		"activeminutes" 				=> "Active Minutes",
		"international" 				=> "International",
		"onboardquestions" 				=> "Onboard Questions",
		"datamanagersetup" 				=> "Data Manager Setup",
		"datamanager" 					=> "Download Data Manager",
        "setupmatrixcare"               => "Setup MatrixCare",
		"purchases" 					=> "Purchases",
        "userportalstepcolors"          => "User Portal Step Colors",
        "reports_settings" 				=> "Reports Settings",
        "updatelistingsview"            => "Update Listings View",
		"backgroundchecklinks" 			=> $BackgroundCheckLinksObj->navigationTitle,
        "savecreditcardinfo"            => "Save Credit Card Information",
        "userportalemailtemplate"       => "User Portal Email Template",
        "requisitionfeeds"              => "Active Requisition Feeds",
        "checklistcategories" 			=> "Checklist Categories",
        "comparativeanalysisquestions" 	=> "Comparative Analysis Questions"
);

$orgtitle = array (
		"multiorg" 						=> "Multiple Organizations",
		"orgdata" 						=> "Organization Data",
		"orglevels" 					=> "Organization Levels",
		"orgtaxnumbers" 				=> "Organization Tax Numbers",
		"orgdescription" 				=> "Organization Description",
		"orgcolors" 					=> "Organization Colors",
		"orgemail" 						=> "Organization Email",
		"orglogos" 						=> "Organization Logos",
        "entrymethods" 					=> "Career Center Setup",
        "portalinfo" 					=> "User Portal Information",
        "updatelistingsview"            => "Update Listings View",
);

$title = 'Administration';

if ($feature ['MultiOrg'] == "Y") {
	//set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//get organization data information
	$results = $OrganizationsObj->getOrgDataInfo('OrganizationName', $where, '', array($params));
	$MultiOrgTitle = $results['results'][0]['OrganizationName'];
} else { // end feature MultiOrg
	unset ( $subtitle ['multiorg'] );
}

if ($feature ['DataManagerType'] == "N") {
	unset ( $subtitle ['datamanagersetup'] );
	unset ( $subtitle ['datamanager'] );
}

if ($feature ['EmailRespond'] != "Y") {
	unset ( $subtitle ['emailresponses'] );
}

if ($feature ['RequisitionRequest'] != "Y") {
	unset ( $subtitle ['emailapprovers'] );
	unset ( $subtitle ['newrequestnotifications'] );
} // end feature
if ($feature ['EmailRespond'] != "Y") {
	unset ( $subtitle ['emailresponses'] );
}

$title = 'Administration - ';
if (($feature ['MultiOrg'] == "Y") && isset($MultiOrgTitle)) {
	$title .= " " . $MultiOrgTitle . "<br>";
} // end feature

if (isset($ss)) {
	$title .= ', ' . $ss;
}

$TemplateObj->title     = $title;
$TemplateObj->orgtitle  = $orgtitle;
$TemplateObj->subtitle  = $subtitle;
$TemplateObj->publish   = isset($publish) ? $publish : '';
?>
