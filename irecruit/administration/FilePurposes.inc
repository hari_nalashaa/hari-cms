<?php
if ($_REQUEST['process'] == 'Y') {
	
	if (($_POST ['label-1']) && ($_POST ['display-1'])) {
		
		//Set where condition
		$where = array("OrgID = :OrgID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID);
		//Delete FilePurpose Information
		$RequestObj->delFilePurposeInfo($where, array($params));
		
	} else {
		$message = 'A Section Label and the first Display Text area are required.';
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $message . "')";
		echo '</script>';
	}
	$i = 0;
	while ( $i <= 99 ) {
		$i ++;
		
		$display = "display-" . $i;
		$label = "label-" . $i;
		
		if ($_POST [$display]) {
			
			$ins_display = is_null($_POST [$display]) ? '' : $_POST [$display];
			
			if($ins_display != "") {
				//Insert FilePurpose Information
				$info = array("OrgID"=>$OrgID, "FilePurposeID"=>$i, "PurposeTitle"=>$ins_display);
				$RequestObj->insFilePurposeInfo($info);
			}
		}
		
		if ($_POST [$label]) {
			
			$ins_label = is_null($_POST [$label]) ? '' : $_POST [$label];
			
			if($ins_label != "") {
				//Insert FilePurpose Information
				$info = array("OrgID"=>$OrgID, "FilePurposeID"=>'99', "PurposeTitle"=>$ins_label);
				$RequestObj->insFilePurposeInfo($info);
			}
		}
	}
	
	if (! $message) {
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('File Purpose has been configured!')";
		echo '</script>';
	}
}

//Set where condition
$where = array("OrgID = :OrgID", "FilePurposeID = 99");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get FilePurpose Information
$results = $RequestObj->getFilePurposeInfo("PurposeTitle", $where, "", array($params));

$Label = $results['results'][0];

// Display Form
echo '<form method="post" action="administration.php">';
echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';
echo '<tr><td colspan="2" height="5"></td></tr>';

echo '<tr><td align="right">';
echo '<font style="color:red">*</font>';
echo '&nbsp;Section Label:</td>';
echo '<td width="600"><input type="text" name="label-1" value="' . $Label ['PurposeTitle'] . '" size="30" maxlength="100">';
echo '</td></tr>';


//Set where condition
$where = array("OrgID = :OrgID", "FilePurposeID != 99");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get FilePurpose Information
$results = $RequestObj->getFilePurposeInfo("PurposeTitle", $where, "FilePurposeID", array($params));

$i = 0;

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$i ++;
	
		if ($i == 1) {
			$required = '<font style="color:red">*</font>&nbsp;';
		} else {
			$required = '';
		}
	
		echo '<tr><td align="right">';
		echo $required . 'Display Text:</td><td><input type="text" name="display-' . $i . '" value="' . $row ['PurposeTitle'] . '" size="30" maxlength="100">';
		echo '</td></tr>';
	}
}


$ii = $i + 2;

while ( $i < $ii ) {
	$i ++;
	
	echo '<tr><td align=right>';
	echo 'Display Text:</td><td><input type="text" name="display-' . $i . '" value="" size="30" maxlength="100">';
	echo '</td></tr>';
}

echo '<tr><td colspan="100%">';
echo '<p>To remove a selection; clear the "Display Text" field and select update.</p>';
echo '</td></tr>';

echo '<tr><td colspan="100%" align="center" height="60" valign="middle">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="hidden" name="action" value="filepurpose">';
echo '<input type="submit" value="Update File Purpose" class="btn btn-primary">';
echo '</td></tr>';

echo '<tr><td colspan="100%">';
echo '<hr size=1>';
echo '<p><b>Preview</b></p>';

echo '<table>';
include IRECRUIT_DIR . 'request/DisplayFormQuestions.inc';
echo displayFilePurpose ( $OrgID, 1, $filepurpose );
echo '</table>';

echo '</td></tr>';

echo '</table>';
echo '</div>';
echo '</form>';
?>