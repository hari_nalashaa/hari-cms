<?php
require_once '../Configuration.inc';

//Get Twilio Accounts
$account_info		=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
//$users_limit		=	$account_info['Licenses'];

$TwilioUserID		=	$_REQUEST['UserID'];
$TwilioSmsStatus	=	$_REQUEST['TwilioSmsStatus'];
$UsersCell			=	$_REQUEST['UsersCell'];


$where_info			=	array("OrgID = :OrgID", "TwilioSms = :TwilioSms");
$params				=	array(":OrgID"=>$OrgID, ":TwilioSms"=>"Y");
$twilio_users_res	=	G::Obj('IrecruitUsers')->getUserInformation("OrgID, TwilioSms, UserID", $where_info, "", array($params));
$twilio_users_list	=	$twilio_users_res['results'];
$twilio_users_count	=	$twilio_users_res['count'];


$UserIDs            =   array();
for($u = 0; $u < $twilio_users_count; $u++) {
    $UserIDs[]      =   $twilio_users_list[$u]['UserID'];
}

#################################################################
//Check Start weather the phone number is sms capable or not

$where_info			=	array("OrgID = :OrgID", "UserID = :UserID");
$set_info			=	array("Cell = :Cell", "TwilioSms = :TwilioSms");
$params_info		=	array(
                            ":OrgID"		=>	$OrgID,
                            ":UserID"		=>	$TwilioUserID,
                            ":Cell"			=>	$UsersCell,
                            ":TwilioSms"	=>	$TwilioSmsStatus
                        );
//Update Users Information
$user_results		=	G::Obj('IrecruitUsers')->updUsersInfo($set_info, $where_info, array($params_info));

if($TwilioSmsStatus == "Y") {

	$user_cell_number_sms	=	"false";
	
	$user_cell_number		=	str_replace(array("-", "(", ")", " "), "", $UsersCell);
	
	if(strlen($user_cell_number) >= 10) {
	    //Add plus one before number
	    if(substr($UsersCell, 0, 2) != "+1") {
	        $user_cell_number	=	"+1".$user_cell_number;
	    }
	    
	    $lookup_phone_num_info	=	G::Obj('TwilioLookUpApi')->getLookUpPhoneNumberInfo($OrgID, $user_cell_number);
	    
	    //Get the Protected properties from SMS object, by extending the object through ReflectionClass
	    $reflection     		=   new ReflectionClass($lookup_phone_num_info['Response']);
	    $property       		=   $reflection->getProperty("properties");
	    $property->setAccessible(true);
	    $properties				=   $property->getValue($lookup_phone_num_info['Response']);
	    
	    if($properties['carrier']['type'] == 'mobile'
	        || $properties['carrier']['type'] == 'voip') {
            $user_cell_number_sms	=	"true";
        }
        
        if($user_cell_number_sms == "true") {
            echo 'Successfully updated the information.';
        }
        else {
            echo 'This phone number is not sms capable. So texting is disabled for this user.';
            
            //Overwrite the Twilio Sms Status
            $params_info[":TwilioSms"]	=	"N";
            //Update Users Information
            $user_results				=	G::Obj('IrecruitUsers')->updUsersInfo($set_info, $where_info, array($params_info));
        }
	        
	}
	else {
	    echo 'Sorry there is an issue with your mobile number. Please update it again.';
	}
}
else {
	echo 'Successfully updated the mobile number.';
}

//Check end weather the phone number is sms capable or not
#################################################################


?>
