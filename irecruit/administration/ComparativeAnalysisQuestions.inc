<?php
//Delete Comparative Analysis Question
if(isset($_REQUEST['que_action']) 
	&& $_REQUEST['que_action'] == 'delete'
	&& isset($_REQUEST['que_key'])
	&& $_REQUEST['que_key'] != "") {
	$ApplicantsObj->delComparativeAnalysisQuestionsInfo($OrgID, $_REQUEST['que_key']);
	
	header("Location:administration.php?action=comparativeanalysisquestions&menu=8");
	exit();
}

//Get all form sections
$FormSectionTitles  =   array();
$form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, "STANDARD", "Y");
foreach($form_sections_info as $form_section_id=>$form_section_info) {
    $FormSectionTitles[$form_section_id] = $form_section_info['SectionTitle'];
}

$where          =   array("OrgID = :OrgID");
$params_info    =   array(":OrgID"=>$OrgID);
//Add new question
if(isset($_REQUEST['btnAddQuestion']) 
	&& $_REQUEST['btnAddQuestion'] == "Add Question"
	&& $_REQUEST['ddlFormQuestions'] != "") { 

	$app_sum_que_results = $ApplicantsObj->getComparativeAnalysisQuestionsInfo("*", $where, '', '', array($params_info));
	$app_sum_que = $app_sum_que_results['results'];
	$app_sum_cnt = $app_sum_que_results['count'];



        $where_save          =   array("OrgID = :OrgID","QuestionID = :QuestionID");
        $params_info_save    =   array(":OrgID"=>$OrgID,"QuestionID" =>$_REQUEST['ddlFormQuestions']);
        $app_sum_que_results_save = $ApplicantsObj->getComparativeAnalysisQuestionsInfo("*", $where_save, '', '', array($params_info_save));
        $app_sum_que_save = $app_sum_que_results_save['results'];

	
		//set parameters
		$params           =   array(":OrgID"=>$OrgID, ":FormID"=>"STANDARD", ":QuestionID"=>$_REQUEST['ddlFormQuestions']);
		//set where information
		$where_info       =   array("OrgID = :OrgID", "FormID = :FormID", "QuestionID = :QuestionID");
		//Set data to insert
		$form_que_results =   $FormQuestionsObj->getFormQuestionsInformation("Question, QuestionTypeID", $where_info, "", array($params));

		$form_que_info    =   $form_que_results['results'][0];
		
		$ins_info         =   array(
	                              "OrgID"             =>  $OrgID,
        						  "SectionID"         =>  $_REQUEST['ddlFormSections'],
        						  "QuestionID"        =>  $_REQUEST['ddlFormQuestions'],
        						  "Question"          =>  $_REQUEST['Question'],
        						  "QuestionTypeID"    =>  $form_que_info['QuestionTypeID']
		                      );
                if(empty($app_sum_que_save)){
		$ApplicantsObj->insComparativeAnalysisQuestionsInfo($ins_info);
		}
		header("Location:administration.php?action=comparativeanalysisquestions&menu=8");
		exit();
}

$app_sum_que_results = $ApplicantsObj->getComparativeAnalysisQuestionsInfo("*", $where, '', '', array($params_info));
$app_sum_que = $app_sum_que_results['results'];
$app_sum_cnt = $app_sum_que_results['count'];?>
<table class="table table-bordered">
   
  <tr>
    <td><strong>Question</strong></td>
    <td width="10%">Action</td>
  </tr>
<?php
if($app_sum_cnt > 0) {
	for($k = 0; $k < $app_sum_cnt; $k++) {
		?>
		  <tr>
		    <td><?php echo $app_sum_que[$k]['Question'];?></td>
		    <td>
		    	<a href="administration.php?action=comparativeanalysisquestions&menu=8&que_action=delete&que_key=<?php echo $app_sum_que[$k]['QuestionID'];?>">
		    		<img src="<?php echo IRECRUIT_HOME;?>images/icons/cross.png" title="Delete" border="0">
		    	</a>
		    </td>
		  </tr>
		<?php
	}
}
else {
	?>
	<tr><td colspan="2" align="center">You haven't added any questions</td></tr>
	<?php
}
?>
</table>

<form name="frmComparativeAnalysisQuestions" id="frmComparativeAnalysisQuestions" method="post">
<input type="hidden" name="Question" id="Question">
<table class="table table-bordered">
	<tr>
		<td width="10%">Form Name</td>
		<td>STANDARD</td>
	</tr>
	<tr>
		<td>Section</td>
		<td>
			<select name="ddlFormSections" id="ddlFormSections" onchange="if(this.value != '') getSectionQuestions(this.value);">
				<option value="">Select Section</option>
				<?php 
					foreach($FormSectionTitles as $FormSectionID=>$FormSectionTitle) {
						?><option value="<?php echo $FormSectionID;?>"><?php echo $FormSectionTitle;?></option><?php
					}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td>Select Question</td>
		<td>
			<div id="divFormQuestions">
				<select name="ddlFormQuestions" id="ddlFormQuestions" style="width:300px" onchange="$('#Question').val($(this).find('option:selected').text())">
					<option value="">Select Question</option>
				</select>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<?php 
			echo '<input type="submit" class="btn btn-primary" name="btnAddQuestion" id="btnAddQuestion" value="Add Question">';
			?>
			
		</td>
	</tr>
</table>
</form>
<script>
	function getSectionQuestions() {
		var SectionID = document.frmComparativeAnalysisQuestions.ddlFormSections.value;

		if(SectionID != "") {
			$("#divFormQuestions").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/>');
			$.ajax({
				method: "POST",
		  		url: "administration/getFormQuestionsBySection.php?SectionID="+SectionID,
				type: "POST",
				success: function(data) {
					$("#divFormQuestions").html(data);
		    	}
			});
		}			
	}
</script>