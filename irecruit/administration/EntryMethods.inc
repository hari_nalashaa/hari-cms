<?php
if ($_POST ["process"] == "Y") {
		
	if(isset($_POST ['entrymethod']) && $_POST ['entrymethod'] != "") {
		//Information to insert
		$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "EntryMethod"=>$_POST ['entrymethod']);
		//On Update Set Information
		$on_update = " ON DUPLICATE KEY UPDATE EntryMethod = :UEntryMethod";
		//Update Information
		$update_info = array(":UEntryMethod"=>$_POST ['entrymethod']);
		//Insert Entry Methods Information
		G::Obj('EntryMethods')->insEntryMethodsInfo($info, $on_update, $update_info);
	}
		
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Entry Method configured!')";
	echo '</script>';
} // end process

//Set where condition
$where		=	array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params		=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get EntryMethods Information
$results	=	G::Obj('EntryMethods')->getEntryMethodsInfo("*", $where, "", array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$entrymethod  =   $row ['EntryMethod'];
	}	
}

//Get InternalCode
$internalcode   =   G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID($OrgID);

if (! $entrymethod) {
	$entrymethod = "P";
}

if ($feature ['UserPortal'] == "Y") {
	?>
<div class="table-responsive">
	<table width="100%" cellspacing="3" cellpadding="5"
		class="table table-striped table-bordered">
		<tr>
			<td height="5"></td>
		</tr>
		<tr>
			<td>
				<form method="post" action="administration.php">
					Which application entry methods would you like iRecuit to use:<br>&nbsp;&nbsp;
					<input type="radio" name="entrymethod" value="P" onclick="submit()" <?php if ($entrymethod == "P") { echo ' checked'; } ?>>Public 
					<input type="radio" name="entrymethod" value="UP" onclick="submit()" <?php if ($entrymethod == "UP") { echo ' checked'; } ?>>User Portal 
					<input type="radio" name="entrymethod" value="B" onclick="submit()" <?php if ($entrymethod == "B") { echo ' checked'; } ?>>Both 
					<input type="hidden" name="action" value="entrymethods"> 
					<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>"> 
					<input type="hidden" name="process" value="Y">
				</form>
			</td>
		</tr>
	</table>
</div>
<?php
} else {
	echo '<br>';
}

$location = 'OrgID=' . $OrgID;
if ($MultiOrgID != "") {
    $location .= '&MultiOrgID=' . $MultiOrgID;
}
?>
<br>
<b>iframe Code</b>
<div style="padding: 20px 0px 10px 10px;">
	Use the following iframe code to insert your listings into another web site.<br>
    <xmp>
    	<iframe style="overflow: hidden;" frameborder=0 src="<?php echo PUBLIC_HOME?>iFrameListing.php?<?php echo $location?>" scrolling="no" width="800"></iframe>
    </xmp>
</div>
<div style="padding: 10px 0px 10px 10px;">
	The display will look like this.<br>
	<iframe 
		style="overflow: hidden;" 
		frameborder=0
		src="<?php echo PUBLIC_HOME?>iFrameListing.php?<?php echo $location?>" scrolling="no"
		width="800"></iframe>
</div>
