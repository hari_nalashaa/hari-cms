<?php
// This is being converted to ClassPurchases.inc for the ADMIN site. Will review
$purchases  =   "";

$purchases .=   "<style>\n";
$purchases .=   "table td {\n";
$purchases .=   "padding:2px;\n";
$purchases .=   "}\n";
$purchases .=   "</style>\n";

//6 month old date
$date1      =   date('Y-m-d');
$date1      =   strtotime ( '-6 month' , strtotime ( $date1 ) ) ;
$date1_ymd  =   date ( 'Y-m-d' , $date1 );
$date1_mdy  =   date ( 'm/d/Y' , $date1 );

//current date
$date2_ymd  =   date('Y-m-d');
$date2_mdy  =   date('m/d/Y');

//Set columns
$columns    =   "*, date_format(CCexp,'%m/%Y') EXP";
//Set where condition
$where      =   array("OrgID = :OrgID", "(DATE(PurchaseDate) >= :Date1 AND DATE(PurchaseDate) <= :Date2)");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":Date1"=>$date1_ymd, ":Date2"=>$date2_ymd);
//Get Purchases Information
$results    =   G::Obj('Purchases')->getPurchasesInfo($columns, $where, "PurchaseDate DESC", array($params));
//Purchase Count
$purchasecnt    =   $results['count'];

$purchases_header   =   "";
if(is_array($results['results'])) {
	foreach($results['results'] as $PURCHASES) {

	    $purchases_header .= '<div style="margin:10px 0px 5px 5px;padding:10px;border:1px solid #c9c5c5;width:auto;display:inline-block;cursor:pointer" id="'.$PURCHASES ['OrgID'].'-'.$PURCHASES ['PurchaseNumber'].'" onclick="displayPurchaseInfo(this.id)">';
	    $purchases_header .= 'Purchase Number: ' . $PURCHASES ['PurchaseNumber']."<br>";
	    $purchases_header .= 'Purchase Date: ' . $PURCHASES ['PurchaseDate'];
	    $purchases_header .= '</div>';
	    
		$purchases .= '<div style="margin:10px 0 20px 0;display:none" id="'.'content-'.$PURCHASES ['OrgID'].'-'.$PURCHASES ['PurchaseNumber'].'" class="purchase_content">';
		$purchases .= '<table class="table table-bordered"><tr><td>' . "\n";
		$purchases .= "\n\n" . '<table border="0" cellspacing="0" cellpading="3" class="table table-bordered">' . "\n";

		if ($internal == "Y") {
			
			//Get Organization Name
			$resultsINOD = $OrganizationDetailsObj->getOrganizationInformation($PURCHASES ['OrgID'], '', "OrganizationName");
			$OrganizationName = $resultsINOD['OrganizationName'];
			
			$purchases .= '<tr>';
			$purchases .= '<td align="right" valign="bottom">Customer:</td><td valign="bottom">';
			$purchases .= $PURCHASES ['OrgID'] . "&nbsp;-&nbsp;" . $OrganizationName;
			$purchases .= '</td>';
			$purchases .= '</tr>' . "\n";

		} // end if
	
		$purchases .= '<tr>';
		$purchases .= '<td align="right" valign="bottom">Purchase Number:</td><td valign="bottom">';
		$purchases .= "<b>" . $PURCHASES ['PurchaseNumber'] . "</b>";
		$purchases .= '</td>';
		$purchases .= '</tr>' . "\n";

		if ($PURCHASES ['OrderNumber'] != "") {
			$purchases .= '<tr>';
			$purchases .= '<td align="right" valign="bottom">Order Number:</td><td valign="bottom">';
			$purchases .= "<b>" . $PURCHASES ['OrderNumber'] . "</b>";
			$purchases .= '</td>';
			$purchases .= '</tr>' . "\n";
		}
		
		if ($PURCHASES ['TransactionID'] != "") {
			$purchases .= '<tr>';
			$purchases .= '<td align="right" valign="bottom">TransactionID:</td><td valign="bottom">';
			$purchases .= "<b>" . $PURCHASES ['TransactionID'] . "</b>";
			$purchases .= "&nbsp;&nbsp;" . $PURCHASES ['ApprovalCode'] . "</b>";
			$purchases .= '</td>';
			$purchases .= '</tr>' . "\n";
		}
		
		if ($PURCHASES ['PostTransactionID'] != "") {
			$purchases .= '<tr>';
			$purchases .= '<td align="right" valign="bottom">Post TransactionID:</td><td valign="bottom">';
			$purchases .= "<b>" . $PURCHASES ['PostTransactionID'] . "</b>";
			$purchases .= "&nbsp;&nbsp;" . $PURCHASES ['PostApprovalCode'] . "</b>";
			$purchases .= '</td>';
			$purchases .= '</tr>' . "\n";
		}
		
		$purchases .= '<tr>';
		$purchases .= '<td align="right">Purchase Date:</td><td>';
		$purchases .= $PURCHASES ['PurchaseDate'];
		$purchases .= '</td>';
		$purchases .= '</tr>' . "\n";
	
		$purchases .= '<tr>';
		$purchases .= '<td align="right" valign="top">Purchaser\'s Name:</td><td valign="top">';
		$purchases .= $PURCHASES ['FullName'] . "<br>";
		$purchases .= $PURCHASES ['Address1'] . "<br>";
		if ($PURCHASES ['Address2'] != "") {
			$purchases .= $PURCHASES ['Address2'] . "<br>";
		}
		$purchases .= $PURCHASES ['City'] . ", ";
		$purchases .= $PURCHASES ['State'] . "&nbsp;&nbsp;";
		$purchases .= $PURCHASES ['ZipCode'] . "<br>";
		$purchases .= $PURCHASES ['Country'] . "<br><br>";
		$purchases .= '</td>';
		$purchases .= '</tr>' . "\n";
	
		$purchases .= '</table>' . "\n";
	
		$purchases .= '</td><td valign="top">';
	
		$purchases .= "\n\n" . '<table border="0" cellspacing="0" cellpading="3" class="table table-bordered">' . "\n";
		if ($PURCHASES ['Email'] != "") {
			$purchases .= "<tr><td align=\"right\">Email:</td><td>" . $PURCHASES ['Email'] . "</td></tr>\n";
		}
		if ($PURCHASES ['Phone'] != "") {
			$purchases .= "<tr><td align=\"right\">Phone:</td><td>" . $PURCHASES ['Phone'] . "</td></tr>\n";
		}
		$purchases .= "<tr><td align=\"right\">Partial Card #:</td><td>" . $PURCHASES ['CCnumber'] . "</td></tr>\n";
		$purchases .= "<tr><td align=\"right\">Expiration:</td><td>" . $PURCHASES ['EXP'] . "</td></tr>\n";

		$purchases .= "<tr><td align=\"right\">Placed by:</td><td>" . G::Obj('IrecruitUsers')->getUsersName ( $PURCHASES ['OrgID'], $PURCHASES ['UserID'] ) . "</td></tr>\n";

		$purchases .= '</table>' . "\n";

		$purchases .= '</td></tr></table>';

	    include IRECRUIT_DIR . 'shopping/BasketItems.inc';
		
		$purchases .= $basket;

		$purchases .= '</div>' . "\n";

	} // end foreach
}

echo '<form method="post" name="frmFilterPurchases" id="frmFilterPurchases">';
echo 'From: <input type="text" id="txtPurchaseFromDate" name="txtPurchaseFromDate" class="form-control width-auto-inline" value="'.$date1_mdy.'">&nbsp;';
echo 'To: <input type="text" id="txtPurchaseToDate" name="txtPurchaseToDate" class="form-control width-auto-inline" value="'.$date2_mdy.'">&nbsp;';
echo '<input type="button" id="btnSubmit" name="btnSubmit" class="btn btn-primary" value="Submit" onclick="getPurchases(this)">';
echo '</form>';

echo '<div id="basket_payment_info">';

if ($purchasecnt > 0) {
    echo $purchases_header . "\n" . $purchases . "\n";
} else {
    echo "\n\n" . '<div class="table-responsive"><table border="0" cellspacing="0" cellpading="3" width="770" class="table table-bordered">' . "\n";
    echo '<tr><td height="50" valign="middle" align="center"><br>There are currently no purchases for this account.<br><br></td></tr>' . "\n";
    echo '</table></div>' . "\n";
}

echo '</div>';
?>