<?php 
if(isset($_REQUEST['action']) 
   && ($_REQUEST['action'] == 'processleademailtemplate')
   && isset($_REQUEST['btnProcessLeadEmailTemplate'])
   && $_REQUEST['btnProcessLeadEmailTemplate'] == 'Update') {

    $RegUserSub     =   $_REQUEST['txtRegisteredUsersSubject'];
    $RegUserMsg     =   $_REQUEST['taRegisteredUsersMessage'];
    $NonRegUserSub  =   $_REQUEST['txtNonRegisteredUsersSubject'];
    $NonRegUserMsg  =   $_REQUEST['taNonRegisteredUsersMessage'];
    
    $LeadGeneratorEmailTemplateObj->insUpdProcessLeadTemplate($OrgID, $RegUserSub, $RegUserMsg, $NonRegUserSub, $NonRegUserMsg);
    
    echo '<script>';
    echo 'location.href="'.IRECRUIT_HOME.'administration.php?action=processleademailtemplate&menu=8&msg=suc"';
    echo '</script>';
    exit;
}

//Process Lead Template Information
$lead_template_info =   $LeadGeneratorEmailTemplateObj->getProcessLeadTemplateInfo($OrgID);

if($lead_template_info == "") {
    //Process Lead Template Information
    $lead_template_master   =   $LeadGeneratorEmailTemplateObj->getProcessLeadTemplateInfo('MASTER');
    $lead_template_info     =   $lead_template_master;
}
?>
<div class="table-responsive">
	<form method="post" name="frmProcessLeads" id="frmProcessLeads" action="administration.php?action=processleademailtemplate&menu=8">
	<table width="770" cellspacing="3" cellpadding="5" class="table table-bordered">
    
	   <tr>
            <td colspan="2"><strong>Email Template for Registered Users:</strong> Placeholders: {first}, {last}, {RequisitionTitle}, {Email}, {ApplicationDate}, {ApplicationID}, {EditApplicationLink}</td>
       </tr>

       <tr>
            <td colspan="2" style="color:blue">
           <?php
           if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'suc')
           {
               echo 'Successfully Updated';
           }
           ?>
            </td>
       </tr>

       <tr>
			<td width="150" align="right">Subject:</td>
			<td>
                <input type="text" name="txtRegisteredUsersSubject" id="txtRegisteredUsersSubject" class="form-control width-auto-inline" value="<?php echo $lead_template_info['RegisteredUserSubject']?>">
			</td>
		</tr>
		
		<tr>
			<td width="150" align="right">Message:</td>
			<td>
                <textarea rows="5" cols="25" class="mceEditor" name="taRegisteredUsersMessage" id="taRegisteredUsersMessage"><?php echo $lead_template_info['RegisteredUserMessage']?></textarea>
			</td>
		</tr>

		<tr>
            <td colspan="2"><strong>Email Template for Non Registered Users:</strong> Placeholders: {first}, {last}, {RequisitionTitle}, {Email}, {Password}, {ApplicationDate}, {ApplicationID}, {VerifyEmailLink}</td>
        </tr>
		
		<tr>
			<td width="150" align="right">Subject:</td>
			<td>
                <input type="text" name="txtNonRegisteredUsersSubject" id="txtNonRegisteredUsersSubject" class="form-control width-auto-inline" value="<?php echo $lead_template_info['NonRegisteredUserSubject']?>">
			</td>
		</tr>

		<tr>
			<td width="150" align="right">Message:</td>
			<td>
                <textarea rows="5" cols="25" class="mceEditor" name="taNonRegisteredUsersMessage" id="taNonRegisteredUsersMessage"><?php echo $lead_template_info['NonRegisteredUserMessage']?></textarea>
			</td>
		</tr>
		
		<tr>
			<td colspan="3">
				<input type="submit" name="btnProcessLeadEmailTemplate" class="btn btn-primary" value="Update">
				<input type="hidden" name="action" value="processleademailtemplate"> 
				<input type="hidden" name="process" value="Y">
			</td>
		</tr>
	</table>
	</form>
</div>
