<?php 
if(isset($_POST) && count($_POST) > 0) {
	if(isset($_POST['btnLead']) 
	   && $_POST['btnLead'] == 'Update'
       && $_POST['action'] == 'applicationleads'
       && $_POST['FormID'] != "") {
		
	    if(isset($_POST['txtLeadName']) && $_POST['txtLeadName'] != "") {
	        $ApplicationLeadsObj->insUpdApplicationLead($OrgID, $_POST['txtLeadName'], $_POST['FormID']);
	    }
	    else if(isset($_POST['txtLeadName']) && $_POST['txtLeadName'] == "") {
	        $ApplicationLeadsObj->delApplicationLead($OrgID, $_POST['FormID']);
	    }
	}
}

$job_app_leads = $ApplicationLeadsObj->getApplicationLeadsList($OrgID);

$application_leads =   array();
for($ja = 0; $ja < count($job_app_leads); $ja++) {
    $application_leads[$job_app_leads[$ja]['FormID']]  =   $job_app_leads[$ja]['LeadName'];
}
?>

<script>
var application_leads = <?php echo json_encode($application_leads)?>;

function setLeadName(FormID) {
	console.log(FormID);
	console.log(application_leads);
	if(typeof(application_leads[FormID]) == 'undefined') {
		document.getElementById('txtLeadName').value = '';
	}
	else {
		document.getElementById('txtLeadName').value = application_leads[FormID];
	}
}
</script>

<div class="table-responsive">
	<form method="post" action="administration.php?action=applicantlabels&menu=8">
	<table width="770" cellspacing="3" cellpadding="5" class="table table-bordered">
		<tr>
		  <td colspan="3">
		      <strong>Note1:* </strong> Assign lead to FormID. Only one lead should be assigned per each FormID.<br>
		      <strong>Note2:* </strong> Select the FormID to see the lead name, You can update it and it can be deleted by updating with empty value.
		  </td>
		</tr>
		<tr>
			<td width="150" align="right">Form ID's:</td>
			<td>
            <?php
			// set condition
			$where = array ("OrgID = :OrgID");
			// set parameters
			$params = array (":OrgID" => $OrgID);
			// set columns
			$columns = "DISTINCT(FormID) as DistinctFormID";
			// Get formquestions information
			$results = $FormQuestionsObj->getFormQuestionsInformation ( $columns, $where, 'FormID', array ($params) );

			$Formids = '';
			
			$Formids .= '<select name="FormID" id="FormID" class="form-control width-auto-inline" onchange="setLeadName(this.value)">';
			$Formids .= '<option value="">Select</option>';
			
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $row ) {
					$sel = ($FormID == $row ['DistinctFormID'] || (count($_POST) == 0 && ($FormID == "") && ($row ['DistinctFormID'] == $defaultValue))) ? " selected" : "";
					$Formids .= '<option value="' . $row ['DistinctFormID'] . '"' . $sel . '>' . $row ['DistinctFormID'] . '</option>';
				}
			}
			
			$Formids .= '</select>';
			
			echo $Formids;
            ?>
			</td>
		</tr>
		<tr>
			<td align="right">Lead Name:</td>
			<td><input type="text" name="txtLeadName" id="txtLeadName" class="form-control width-auto-inline"></td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="submit" name="btnLead" class="btn btn-primary" value="Update">
				<input type="hidden" name="action" value="applicationleads"> 
				<input type="hidden" name="process" value="Y">
			</td>
		</tr>
	</table>
	</form>
</div>