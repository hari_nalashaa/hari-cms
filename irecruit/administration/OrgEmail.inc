<?php
if ($_POST ["process"] == "Y") {
	
	if ($_REQUEST['type'] == "org") {
		$displaytype = "Correspondence";
	} elseif ($_REQUEST['type'] == "dl") {
		$displaytype = "Default Applicant";
	}
	
	if ($_POST ['organizationemail']) {
		$ERROR1 = $ValidateObj->validate_email_address ( $_POST ['organizationemail'] );
		if ($ERROR1) {
			$ERROR .= $displaytype . " " . $ERROR1;
		}
	}
	
	if ($_POST ['deadletteremail']) {
		$ERROR2 = $ValidateObj->validate_email_address ( $_POST ['deadletteremail'] );
		if ($ERROR2) {
			$ERROR .= $displaytype . " " . $ERROR2;
		}
	}
	
	if ($_POST ['reset'] == "Y") {
		$ERROR = "";
	}
	
	if ($ERROR) {
		
		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		$err = "The following error has occurred:\\n\\n" . $ERROR;
		echo "alert('$err');" . "\n";
		echo '</script>' . "\n";
	} else {
		
		if ($_POST ['reset'] == "Y") {
			if ($_REQUEST['type'] == "org") {
				$_POST ['organizationemail'] = "";
			} elseif ($_REQUEST['type'] == "dl") {
				$_POST ['deadletteremail'] = "";
			}
		}
		
		$OrgNameAlias = preg_replace("/[^a-z0-9\\s]+/i", "", $_POST['organizationname']);
		if ($_REQUEST['type'] == "org") {
			//Set Insert Information
			$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "Email"=>$_POST ['organizationemail'], "OrgName"=>$OrgNameAlias, "EmailVerified"=>'');
			//Set Duplicate Key
			$on_update = " ON DUPLICATE KEY UPDATE Email = :UEmail, OrgName = :UOrgName, EmailVerified = ''";
			//Update Information
			$update_info = array(":UEmail"=>$_POST ['organizationemail'], ":UOrgName"=>$OrgNameAlias);
			//Insert OrganizationEmail
			$OrganizationsObj->insOrganizationInfo('OrganizationEmail', $info, $on_update, $update_info);
			
		} elseif ($_REQUEST['type'] == "dl") {
			
			//Set Insert Information
			$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "DeadLetterEmail"=>$_POST ['deadletteremail'], "DeadLetterEmailVerified"=>'');
			//Set Duplicate Key
			$on_update = " ON DUPLICATE KEY UPDATE DeadLetterEmail = :UDeadLetterEmail, DeadLetterEmailVerified = ''";
			//Update Information
			$update_info = array(":UDeadLetterEmail"=>$_POST ['deadletteremail']);
			//Insert OrganizationEmail
			$OrganizationsObj->insOrganizationInfo('OrganizationEmail', $info, $on_update, $update_info);
			
		}
		
		if ($_POST ['reset'] == "Y") {
			$message = $displaytype . " Email Address has been reset to the default!";
		} else {
			$resend = "Y";
			$message = $displaytype . " Email Address has been updated!";
		}
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('$message')";
		echo '</script>';
	} // end organizationemail
} // end process

//Get Organization Email Information
$OE         =   $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, $MultiOrgID);

if ($resend == "Y") {
	
	if (($_REQUEST['type'] == "org") && ($OE ['Email'])) {
		
		$to       =   $OE ['Email'];
		$subject  =   "Email Verification";
		$message  =   "This email address has been requested to be verified." . "\n";
		$message .=   "Clicking on the following link will activate this address." . "\n";
		$message .=   "If you did not request this, no action is required." . "\n";
		$link     =   preg_replace ( '/administration.php/', 'administration/verifyEmail.php', $_SERVER ['PHP_SELF'] ) . "?User=" . $USERID;
		if (isset($_REQUEST ['MultiOrgID']) && $_REQUEST ['MultiOrgID'] != "") {
			$link .= "&MultiOrgID=" . $_REQUEST ['MultiOrgID'];
		}
		
		$link    .= "&OrgEmail=" . $OE ['Email'];
	    $urllink .= "https://" . $_SERVER ['SERVER_NAME'] . $link;

		$message .= "\n\nPlease visit: <a href=\"" . $urllink . "\">" . $urllink . "</a>";
		
		if (DEVELOPMENT != "Y") {
		    $message  =   nl2br($message);
		    //Clear custom properties
		    $PHPMailerObj->clearCustomProperties();
			$PHPMailerObj->clearCustomHeaders();
 
		    // Set who the message is to be sent to
		    $PHPMailerObj->addAddress ( $to );
		    // Set the subject line
		    $PHPMailerObj->Subject = $subject;
		    // convert HTML into a basic plain-text alternative body
		    $PHPMailerObj->msgHTML ( $message );
		    // Content Type Is HTML
		    $PHPMailerObj->ContentType = 'text/html';
		    //Send Mail
		    $PHPMailerObj->send ();
		}
		
		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('Email has been sent to: " . $OE ['Email'] . "\\nPlease respond to the link sent to that address.');" . "\n";
		echo '</script>' . "\n";
	} // end email check
	
	if (($_REQUEST['type'] == "dl") && ($OE ['DeadLetterEmail'])) {
		
		$to       =   $OE ['DeadLetterEmail'];
		$subject  =   "Email Verification";
		$message  =   "This email address has been requested to be verified." . "\n";
		$message .=   "Clicking on the following link will activate this address." . "\n";
		$message .=   "If you did not request this, no action is required." . "\n";
		$link     =   preg_replace ( '/administration.php/', 'administration/verifyEmail.php', $_SERVER ['PHP_SELF'] ) . "?User=" . $USERID . "&MultiOrgID=" . $_REQUEST['MultiOrgID'] . "&DLEmail=" . $OE ['DeadLetterEmail'];
		$urllink = "https://" . $_SERVER ['SERVER_NAME'] . $link;

		$message .=   "\n\nPlease visit: <a href=\"" . $urllink . "\">" . $urllink . "</a>";
		
		if (DEVELOPMENT != "Y") {
		    $message  =   nl2br($message);
		    //Clear custom properties
		    $PHPMailerObj->clearCustomProperties();
		    $PHPMailerObj->clearCustomHeaders();
		    
		    // Set who the message is to be sent to
		    $PHPMailerObj->addAddress ( $to );
		    // Set the subject line
		    $PHPMailerObj->Subject = $subject;
		    // convert HTML into a basic plain-text alternative body
		    $PHPMailerObj->msgHTML ( $message );
		    // Content Type Is HTML
		    $PHPMailerObj->ContentType = 'text/html';
		    //Send Mail
		    $PHPMailerObj->send ();
		}
		
		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "alert('Email has been sent to: " . $OE ['DeadLetterEmail'] . "\\nPlease respond to the link sent to that address.');" . "\n";
		echo '</script>' . "\n";
	} // end email check
} // end resend = Y

if ($OE ['Email']) {
	if ($OE ['EmailVerified'] == '') {
		$from			  =	  G::Obj('IrecruitSettings')->getValue("From");
		$emailverified    =   "Email needs verification. <a href=\"administration.php?action=orgemail&MultiOrgID=" . $MultiOrgID . "&resend=Y&type=org\" style=\"COLOR:red;\">Resend Notification</a><br>";
		$emailverified   .=   $from['Email'] . " will be used until verified for accuracy of accessibility.<br>";
	} else {
		$emailverified    =   "Email has been verified.<br>";
	}
	
	$emailverified .= '<br>Reset default application email address <input type="checkbox" value="Y" name="reset"><br>';
} // end if Email

if ($OE ['DeadLetterEmail']) {
	if ($OE ['DeadLetterEmailVerified'] == '') {
		$deadletteremailverified = "Email needs verification. <a href=\"administration.php?action=orgemail&MultiOrgID=" . $MultiOrgID . "&resend=Y&type=dl\" style=\"COLOR:red;\">Resend Notification</a><br>";
		$deadletteremailverified .= "No address will be used until verified for accuracy of accessibility.<br>";
	} else {
		$deadletteremailverified = "Email has been verified.<br>";
	}
	$deadletteremailverified .= '<br>Reset default application email address <input type="checkbox" value="Y" name="reset"><br>';
} // end if Email
?>
<div class="row">
    <div class="col-lg-12">
    </div>
</div>
<div class="table-responsive">
	<table border="0" width="770" cellspacing="3" cellpadding="5"
		class="table table-striped table-bordered">
		<form method="post" action="administration.php">
			<tr>
				<td height="5">
				    Please consult your IT department and add the following to your DNS records. <br>
                    This will allow your e-mail address to be identified with and sent from our web server. <br>
                    The "~all" part means even if e-mails fail the spf tests, they will still be allowed through but will be marked as failed. 
                    <br><br>
                    TXT Record - Host Name: leave blank (or enter @)<br>
                    Text: v=spf1 A MX a:qs2998.pair.com  ~all    
				</td>
			</tr>
			<tr>
				<td>
                    Enter an email address which will be the return address for all
                    email correspondence: <br>&nbsp;&nbsp; 
                    <input type="text" name="organizationemail" value="<?php echo $OE['Email']?>" size="40" maxlength="55"> <br>
                    <br>
                    Enter Organization alias name: <br>&nbsp;&nbsp; 
                    <input type="text" name="organizationname" value="<?php echo $OE['OrgName']?>" size="40" maxlength="55"> <br>
                    <br>
                    <?php echo $emailverified?><br> 
                    <input type="hidden" name="action" value="<?php echo $action;?>"> 
                    <input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>"> 
                    <input type="hidden" name="type" value="org"> 
                    <input type="hidden" name="process" value="Y">
                    <center>
                    	<input type="submit" value="Submit" class="btn btn-primary">
                    </center>
				</td>
			</tr>
		</form>
	</table>

	<hr>

	<table border="0" width="770" cellspacing="3" cellpadding="5"
		class="table table-striped table-bordered">
		<form method="post" action="administration.php">
			<tr>
				<td height="5"></td>
			</tr>
			<tr>
				<td>
                    Enter an email address which will be the default address for
                    applicants added internally: <br>&nbsp;&nbsp; 
                    <input type="text" name="deadletteremail" value="<?php echo $OE['DeadLetterEmail']?>" size="40" maxlength="55"> <br>
                    <br>
                    <?php echo $deadletteremailverified?><br> <br>
                    <br> 
                    <input type="hidden" name="action" value="<?php echo $action;?>"> 
                    <input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>"> 
                    <input type="hidden" name="type" value="dl"> 
                    <input type="hidden" name="process" value="Y">
                    <center>
                    	<input type="submit" value="Submit" class="btn btn-primary">
                    </center>
				</td>
			</tr>
		</form>
	</table>
</div>
