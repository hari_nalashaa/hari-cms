<?php
require_once '../Configuration.inc';

$TemplateObj->title =   $title  =   "Checklist";


if ($_REQUEST['active'] == "n") { 
	$Active = 'N'; 
	$a = 'n'; 
} else if ($_REQUEST['active'] == "p") { 
	$Active = 'P'; 
	$a = 'p'; 
} else { 
	$Active = 'Y'; 
	$a = 'y'; 
}

$checklist_res       =   array();
$checklist_info      =   G::Obj('Checklist')->getCheckListByOrgID($OrgID,$Active);
$checklist_res       =   json_decode($checklist_info['StatusCategory'], true);
$column = '*';
$where          =   array ("OrgID = :OrgID","Active = 'Y'");
$params         =   array (":OrgID" => $OrgID);
$get_applicant_process_info      =   G::Obj('Applicants')->getApplicantProcessFlowInfo($columns, $where, 'ProcessOrder asc', array($params) );
$get_applicant_process_results = $get_applicant_process_info['results'];
$get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);
//echo "<pre>";print_r($get_applicant_process_results);echo "</pre>";

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Add Checklist') {
    $ChecklistName    =   $_REQUEST['ChecklistName'];
    //Insert Checklist Name Information
    G::Obj('Checklist')->insChecklistName($OrgID, $ChecklistName);
    
    header("Location:".IRECRUIT_HOME."administration/checklist.php?active=p");
    exit;

}else if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Copy Form') {
    if (isset($_POST['action']) && $_POST['action'] == 'copyform') {
                        
        if (isset($_POST['NewChecklistName'])) {
            
            // Get Checklist ID & Name
            $new_name = $_POST['NewChecklistName'];
            $new_checklist_id = $_POST['NewChecklistID'];
            
            //Copy Application Form Sections
            G::Obj('Checklist')->copyChecklist($OrgID, $new_checklist_id, $new_name);
            
            header("Location:".IRECRUIT_HOME."administration/checklist.php?active=".$a);
        }
    }
}


$TemplateObj->checklist_info  =   $checklist_info['results'];
$TemplateObj->get_applicant_process_results  =   $get_applicant_process_results;
$TemplateObj->get_checklist_process_data  =   $get_checklist_process_data;
echo $TemplateObj->displayIrecruitTemplate ( 'views/administration/Checklist' );
?>
