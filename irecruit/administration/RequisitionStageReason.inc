<?php
//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results_stage_reasons = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));
$req_stage_reasons = array();

if($results_stage_reasons['count'] == 0) {
    $RequisitionsObj->insDefaultRequisitionStageReasonsInfo($OrgID);
    $results_stage_reasons = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));
}
if(is_array($results_stage_reasons['results'])) {
    foreach($results_stage_reasons['results'] as $row_stage_reason) {
        $stage_reason_code                      =   $row_stage_reason ['Code'];
        $stage_reason_description               =   $row_stage_reason ['Description'];
        $req_stage_reasons[$stage_reason_code]  =   $stage_reason_description;
    }
}

if ($process == 'Y') {
	
	if (isset($_POST['display'][0]) && $_POST['display'][0] != "") {
	    //Set parameters
	    $params = array(":OrgID"=>$OrgID);
	    //Set where condition
	    $where = array("OrgID = :OrgID");
	    //Delete Request Reasons
	    $RequisitionsObj->delRequisitionStage("RequisitionStageReason", $where, array($params));
	}
	
	for($i = 0; $i < count($_POST['display']); $i ++) {
	
	    $item     =   $_POST['item'][$i];
	    $display  =   $_POST['display'][$i];
	
	    if ($display != "" && $item != "") {
	        $info = array("OrgID"=>$OrgID, "Code"=>$item, "Description"=>$display);
	        $RequisitionsObj->insRequisitionStage("RequisitionStageReason", $info);
	    }
	} // end for loop
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Requisition Stage Reason Configured!')";
	echo '</script>';
} // end if process

echo '<form method="post" action="administration.php">';

echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-bordered">';
echo '<tr><td colspan="3"></td></tr>';
echo '<tr><td colspan="3"><strong>Note:* </strong>Stage Reasons are dispositions for the requisitions stages. Reason for closing etc.</td></tr>';
//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));

$i = 1;
if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		echo '<tr>';
		echo '<td align="right" width="35%">Passed Value:</td><td><input type="text" name="item[]" value="' . $row ['Code'] . '" size="20" maxlength="20"></td>';
		echo '<td width="65%">';
		echo '&nbsp;&nbsp;Display Text: <input type="text" name="display[]" value="' . $row ['Description'] . '" size="40" maxlength="60">';
		/*
		echo '&nbsp;&nbsp;<input type="radio" name="stage_final" value="' . $row ['Code'] . '"';
		if($row['IsFinal'] == "Y") {
		    echo ' checked="checked"';
		}
		echo '> Final Stage';
		*/
		echo '</td>';
		echo '</tr>';
		$i ++;
	}
}

echo '<tr>';
echo '<td align="right">Passed Value:</td><td><input type="text" name="item[]" size="20" maxlength="20"></td>';
echo '<td>&nbsp;&nbsp;Display Text: <input type="text" name="display[]" size="40" maxlength="60">';
//echo '&nbsp;&nbsp;<input type="radio" name="stage_final" value="' . $row ['Code'] . '">&nbsp;Final Stage';
echo '</td>';
echo '</tr>';

echo '<tr><td align="center" colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="requisitionstagereason">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update Requisition Stage Reason" class="btn btn-primary"></td></tr>';
echo '</form>';

echo '<tr><td colspan="100%">';
echo '<hr size=1>';
echo '<p><b>Preview</b></p>';


//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Request Reasons Information
$results = $RequisitionsObj->getRequisitionStageInfo("RequisitionStageReason", "Code, Description", $where, "Description", array($params));

echo 'Reason for Request: <select>';
echo '<option value="">Select a Reason</option>';

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$code = $row ['Code'];
		$description = $row ['Description'];
	
		echo '<option value="' . $code . '">' . $description . '</option>';
	}	
}

echo '</select>';
echo '</td></tr>';
echo '</table>';
echo '<br><br>';
?>