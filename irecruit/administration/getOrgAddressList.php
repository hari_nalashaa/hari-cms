<?php
require_once '../Configuration.inc';

$TemplateObj->title =   $title  =   "Organization Addresses List";

//Insert Default Address
G::Obj('OrgAddressList')->insDefaultAddress($OrgID);

$address_list       =   array();
$org_address_info   =   G::Obj('OrgAddressList')->getAddressList($OrgID);
$address_list       =   json_decode($org_address_info['AddressList'], true);

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == 'Submit') {
    
    if(isset($_POST['AddressList'])) {
        $address_list   =   $_POST['AddressList'];
        
        $address_info   =   array();
        $default        =   "N";
        $has_default    =   false;
        foreach($address_list as $address_key=>$address_text) {
            if($address_text != "") {
                if($address_key == $_REQUEST['AddressDefault']) {
                    $has_default    =   true;
                    $default        =   "Y";
                }
                
                $address_info[] = ["Address"=>$address_text, "Default"=>$default];
            }
        }
        
        if($has_default == false) {
            if(count($address_info) > 0) {
                $address_info[0]["Default"]  =  $default;   
            }
        }
        
        //Insert Address Information
    	G::Obj('OrgAddressList')->insAddressList($OrgID, json_encode($address_info));
    }
    
    header("Location:".IRECRUIT_HOME."administration/getOrgAddressList.php?menu=8");
    exit;
}

$TemplateObj->address_list  =   $address_list;
echo $TemplateObj->displayIrecruitTemplate ( 'views/administration/OrgAddressList' );
?>