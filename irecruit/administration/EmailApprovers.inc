<?php
if ($_REQUEST['process'] == 'Y') {
	
	//Set where condition
	$where_info = array("OrgID = :OrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID);
	//Delete EmailApprovers Information
	$RequestObj->delApproversInfo('EmailApprovers', $where_info, array($params));
	
	$i = 0;
	while ( $i <= 99 ) {
		$i ++;
		
		$email = "EmailAddress-" . $i;
		$first = "FirstName-" . $i;
		$last = "LastName-" . $i;
		
		if (isset($_POST [$email]) && $_POST [$email] != "") {
			
			$ins_first = is_null($_POST [$first]) ? '' : $_POST [$first];
			$ins_last  = is_null($_POST [$last]) ? '' : $_POST [$last];
			
			if($ins_first != "" && $ins_last != "") {
				//Insert Approvers Information
				$info = array("OrgID"=>$OrgID, "EmailAddress"=>$_POST [$email], "FirstName"=>$ins_first, "LastName"=>$ins_last);
				$RequestObj->insApproversInfo($info);
			}
		}
	} // end while
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Email Approvers have been configured!')";
	echo '</script>';
} // end if process

echo '<form method="post" action="administration.php">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%">';
echo '<tr><td colspan="100%" height="5"></td></tr>';

//Set where condition
$where_info = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Email Approvers Information
$results = $RequestObj->getEmailApprovers("EmailAddress, FirstName, LastName", $where_info, "LastName, FirstName", array($params));

$i = 1;
if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		echo '<tr>';
		echo '<td>Email Address: <input type="text" name="EmailAddress-' . $i . '" value="' . $row ['EmailAddress'] . '" size=30 maxlength=60></td>';
		echo '<td>First Name: <input type="text" name="FirstName-' . $i . '" value="' . $row ['FirstName'] . '" size=15 maxlength=30></td>';
		echo '<td>Last Name: <input type="text" name="LastName-' . $i . '" value="' . $row ['LastName'] . '" size=30 maxlength=45></td>';
		echo '<td width="140">&nbsp;</td>';
		echo '</tr>';
		$i ++;
	}	
}


$ii = $i + 2;
while ( $i < $ii ) {
	echo '<tr>';
	echo '<td>Email Address: <input type="text" name="EmailAddress-' . $i . '" size=30 maxlength=60></td>';
	echo '<td>First Name: <input type="text" name="FirstName-' . $i . '" size=15 maxlength=30></td>';
	echo '<td>Last Name: <input type="text" name="LastName-' . $i . '" size=30 maxlength=45></td>';
	echo '<td>&nbsp;</td>';
	echo '</tr>';
	$i ++;
}
echo '<tr><td colspan="100%">';
echo '<p>To remove a selection; clear the "Email Address" field and select update.</p>
';
echo '</td></tr>';

echo '<tr><td align=center colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="emailapprovers">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update Email Approvers" class="btn btn-primary"></td></tr>';
echo '</form>';

echo '<tr><td colspan="100%">';
echo '<hr size=1>';
echo '<p><b>Preview</b></p>';

echo '<form method="post" action="administration.php">';
echo '<input type="hidden" name="action" value="emailapprovers">';

echo '<table>';
include IRECRUIT_DIR . 'request/DisplayFormQuestions.inc';
echo displayEmailApprovers ( $OrgID, $EmailAddress, 1 );
echo '</table>';

echo '</form>';
echo '</td></tr>';
echo '</table>';
echo '<br><br>';
?>