<?php
if ($_REQUEST['process'] == 'Y') {

	//Delete Applicants Disposition Codes Information
	$ApplicantsObj->delApplicantsDispositionCodesInfo($OrgID);
	
	$i = 0;
	while ( $i <= 99 ) {
		$i ++;
		
		$item = "item-" . $i;
		$display = "display-" . $i;
		$active = "active-" . $i;
		
		if ($_POST [$item] && $_POST [$display] != "") {
			//Insert ApplicantsDispositionCodes
			$info = array("OrgID"=>$OrgID, "Code"=>$_POST [$item], "Description"=>$_POST [$display], "Active"=>$_POST [$active]);
			$ApplicantsObj->insApplicantsDispositionCodesInfo($info);
		}
	} // end while
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Disposition Codes configured!')";
	echo '</script>';
} // end if process

echo '<form method="post" action="administration.php">';
echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="770" class="table table-striped table-bordered table-hover">';
echo <<<END
<tr><td colspan="100%">
<font style="color:red">Warning!</font> This is an application set up value.
The passed value is assigned to an applicant.
The passed value will remain attached to an applicant and any new text value will be assigned
to that value. Differing disposition codes should have new and unique passed values.
</td></tr>
END;

echo '<tr><td colspan="100%" height="5"></td></tr>';

echo '<tr>';
echo '<td width="80" align="center"><strong>Active</strong></td>';
echo '<td width="80"><strong>Passed Value</strong></td>';
echo '<td width="80%"><strong>Display Text</strong></td>';
echo '</tr>';

//Get ApplicationDispositionCodes
$results = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'');

$i = 1;
if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		echo '<tr>';
		echo '<td align="center"><input type="checkbox" name="active-' . htmlspecialchars($i) . '" value="Y"';
		if ($row ['Active'] == "Y") {
			echo ' checked';
		}
		echo '></td>';
		echo '<td><input type="text" name="item-' . htmlspecialchars($i) . '" value="' . htmlspecialchars($row ['Code']) . '" size=10 maxlength=15></td>';
		echo '<td><input type="text" name="display-' . htmlspecialchars($i) . '" value="' . htmlspecialchars($row ['Description']) . '" size=40 maxlength=60></td>';
		echo '</tr>';
		$i ++;
	}
}

$ii = $i + 2;
while ( $i < $ii ) {
	echo '<tr>';
	echo '<td align="center"><input type="checkbox" name="active-' . htmlspecialchars($i) . '" value="Y"></td>';
	echo '<td><input type="text" name="item-' . htmlspecialchars($i) . '" size=10 maxlength=10></td>';
	echo '<td><input type="text" name="display-' . htmlspecialchars($i) . '" size=40 maxlength=60></td>';
	echo '</tr>';
	$i ++;
}

echo '<tr><td align="center" colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="disposition">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update Disposition Codes" class="btn btn-primary"></td></tr>';
echo '</form>';

echo '<tr><td colspan="3">';
echo '<hr size=1>';
echo '<p><b>Preview</b></p>';

//Get ApplicationDispositionCodes
$results = $ApplicantsObj->getApplicantDispositionCodes($OrgID,'Y');

echo 'Disposition Code: <select class="form-control" style="width:250px;">';
echo '<option value="">Select a Code</option>';

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$code = $row ['Code'];
		$description = $row ['Description'];
	
		echo '<option value="' . $code . '">' . $description . '</option>';
	}	
}
echo '</select>';

echo '</td></tr>';
echo '</table>';
echo '</div>';
echo '<br><br>';
?>
