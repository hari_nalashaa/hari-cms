<?php
$org_detail_info    =   G::Obj('OrganizationDetails')->getOrganizationInformation($_REQUEST['OrgID'], $_REQUEST['MultiOrgID'], "ListingsView");

if($process == "Y" 
    && isset($_REQUEST['ddlListingsView']) 
    && $_REQUEST['ddlListingsView'] != "") {

    $set_info           =   array("ListingsView = :ListingsView");
    $where_info         =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
    $params_info        =   array(":ListingsView"=>$_REQUEST['ddlListingsView'], ":OrgID"=>$_REQUEST['OrgID'], ":MultiOrgID"=>$_REQUEST['MultiOrgID']);
    G::Obj('Organizations')->updOrganizationInfo("OrgData", $set_info, $where_info, array($params_info));
    
    echo "<script>location.href='administration.php?action=updatelistingsview&OrgID=".htmlspecialchars($OrgID)."&MultiOrgID=".htmlspecialchars($_REQUEST['MultiOrgID'])."&msg=suc';</script>";
}

echo '<form method="post" action="administration.php">';
echo '<table border="0" cellspacing="3" cellpadding="5" class="table table-bordered">';
echo '<tr><td colspan="3" height="5">';
if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
	echo '<span style="color:blue">Successfully Updated</span>';
}
echo '</td></tr>';
?>
<tr>
<td>List View: 
<select name="ddlListingsView" id="ddlListingsView" class="form-control width-auto-inline">
<option value="initiallist" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "initiallist") echo 'selected="selected"';?>>Default</option>
<option value="split" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "split") echo 'selected="selected"';?>>Left-Right Column (single)</option>
<option value="cwcomm" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "cwcomm") echo 'selected="selected"';?>>Left-Right Column (multi)</option>
<option value="menu" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "menu") echo 'selected="selected"';?>>Pulldown Selection</option>
<option value="CRF" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "CRF") echo 'selected="selected"';?>>Position & Org Level 1</option>
<option value="Vista" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "Vista") echo 'selected="selected"';?>>All Org Levels w/Emp Type</option>
<option value="JSS" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "JSS") echo 'selected="selected"';?>>2 Org Levels w/Emp Type</option>
<option value="Wayne" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "Wayne") echo 'selected="selected"';?>>Position List</option>
<option value="Benedictine" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "Benedictine") echo 'selected="selected"';?>>2 Org Levels w/Apply button</option>
<option value="Auto" <?php if(isset($org_detail_info['ListingsView']) && $org_detail_info['ListingsView'] == "Auto") echo 'selected="selected"';?>>Position Type & Org Level 1</option>
</select>
</td>
</tr>
<?php
echo '<tr><td align="center" colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="updatelistingsview">';
echo '<input type="hidden" name="OrgID" value="'.htmlspecialchars($_REQUEST['OrgID']).'">';
echo '<input type="hidden" name="MultiOrgID" value="'.htmlspecialchars($_REQUEST['MultiOrgID']).'">';
echo '<input type="hidden" name="process" value="Y">';
echo '<input type="submit" value="Update Listings View" class="btn btn-primary"></td></tr>';
echo '</form>';

echo '</table>';
echo '<br><br>';
?>