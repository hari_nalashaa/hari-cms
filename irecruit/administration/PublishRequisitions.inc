<?php
if ($_POST ["process"] == "Y") {
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Delete JobBoards Information
	$JobBoardsObj->delJobBoardsInfo($where, array($params));
	
	if ($_POST ['Indeed']) {
		//JobBoards Information
		$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "Indeed"=>$_POST ['Indeed']);
		//Insert JobBoards Information
		$JobBoardsObj->insJobBoardsInfo($info);
	}
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Job Board settings configured!')";
	echo '</script>';
} // end process


//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get JobBoards Information
$results = $JobBoardsObj->getJobBoardsInfo("Indeed", $where, 'OrgID LIMIT 0,1', array($params));

$Indeed = $results['results'][0]['Indeed'];

?>
<div class="table-responsive">
	<table width="770" cellspacing="3" cellpadding="5"
		class="table table-striped table-bordered">
		<tr>
			<td height="5"></td>
		</tr>
		<tr>
			<td>
				<form method="post" action="administration.php">

					<br> Include my active requisitions on Indeed.com <u>FREE</u>
					listings.<br>
					<br> &nbsp;&nbsp;<input type="checkbox" name="Indeed" value="Y"
						<?php if ($Indeed == "Y") { echo ' checked'; } ?>> &nbsp;&nbsp;<img
						src="<?php echo IRECRUIT_HOME?>images/indeed.png" width="100"
						style="margin: 0px 0px -15px 0px;">&nbsp;&nbsp;&nbsp;Price <b>$0</b><br>
					<br>
					<br>
					<hr size="1">

					<p>Job posting sites reserve the right to reject requisitions
						pending content review.</p>

					<input type="hidden" name="action" value="publishreqs"> <input
						type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>"> <input
						type="hidden" name="process" value="Y"> <input type="submit"
						value="Update Job Board Settings" class="btn btn-primary">
				</form>
			</td>
		</tr>
	</table>
</div>


