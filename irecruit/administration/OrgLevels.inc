<?php
$NumberOfOrganizationalLevels = 6;
global $MultiOrgID;
if(is_null($MultiOrgID)) $MultiOrgID = '';

if ($process == 'pulldown-Y') {
	
	//set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Delete Organization Information
	$OrganizationsObj->delOrganizationInfo('OrganizationLevelData', $where, array($params));
	
	$i = 0;
	while ( $i <= $NumberOfOrganizationalLevels ) {
		$i ++;
		
		$ii = 1;
		while ( $ii <= 999 ) {
			
			$selection = $i . '-' . $ii;
			
			if ($_POST [$selection]) {
				
				//Insert OrganizationLevelData Information
				$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>$i, "SelectionOrder"=>$ii, "CategorySelection"=>$_POST [$selection]);
				$OrganizationsObj->insOrganizationInfo('OrganizationLevelData', $info);
				
			}
			$ii ++;
		}
	}
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Category Choices configured!')";
	echo '</script>';
}

if ($process == 'names-Y') {
	
	//set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Delete Organization Information
	$OrganizationsObj->delOrganizationInfo('OrganizationLevels', $where, array($params));
	
	$i = 0;
	while ( $i <= $NumberOfOrganizationalLevels ) {
		$i ++;
		
		$name = "names-" . $i;
		
		if ($_POST [$name]) {
			
			//Insert OrganizationLevelData Information
			$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>$i, "OrganizationLevel"=>$_POST [$name]);
			$OrganizationsObj->insOrganizationInfo('OrganizationLevels', $info);

		}
	}
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Category Names configured!')";
	echo '</script>';
}

if ($process == 'levels-Y') {
	
	$num = isset($_REQUEST['num']) ? $_REQUEST['num'] : '';
	
	if(isset($num)) {
		
		//set where condition
		$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID > $num");
		//set parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		//Delete OrganizationLevels Information
		$del_org_results = $OrganizationsObj->delOrganizationInfo('OrganizationLevels', $where, array($params));
		//echo "<pre>";print_r($del_org_results);
		
		//Insert OrganizationLevels Information
		$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "OrgLevelID"=>$num, "OrganizationLevel"=>'');
		//Update on duplicate key
		$on_update = " ON DUPLICATE KEY UPDATE OrganizationLevel = :UOrganizationLevel";
		$update_info = array (
				":UOrganizationLevel" => ''
		);
		$OrganizationsObj->insOrganizationInfo('OrganizationLevels', $info, $on_update, $update_info);
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('Organization Levels configured!')";
		echo '</script>';
	}
	
} // end levels-Y

//set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get Max OrganizationLevelID 
$results = $OrganizationsObj->getOrganizationLevelsInfo("MAX(OrgLevelID) AS MAXOrgLevelID", $where, '', array($params));
$Levels = $results['results'][0]['MAXOrgLevelID'];

echo '<form method="post" action="administration.php">';
echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';

echo '<tr><td>';
echo 'How many organization levels should be configured?&nbsp;';
echo '<select name="num">';
$i = 1;
while ( $i <= $NumberOfOrganizationalLevels ) {
	if ($Levels == $i) {
		$selected = ' selected';
	} else {
		$selected = '';
	}
	echo '<option value="' . htmlspecialchars($i) . '"' . $selected . '>' . htmlspecialchars($i) .'</option>';
	$i ++;
}
echo '</select>';
echo '</td></tr>';

echo '<tr><td align=center colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="orglevels">';
echo '<input type="hidden" name="process" value="levels-Y">';
echo '<input type="hidden" name="MultiOrgID" value="' . htmlspecialchars($MultiOrgID) . '">';
echo '<input type="submit" value="Create Organization Levels" class="btn btn-primary">';
echo '</td></tr>';
echo '</form>';

// Collect the name for each Level
echo '<form method="post" action="administration.php">';
echo '<tr><td colspan="100%" height="5"></td></tr>';

//set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get Max OrganizationLevelID
$results = $OrganizationsObj->getOrganizationLevelsInfo("OrganizationLevel", $where, 'OrgLevelID', array($params));

$ii = 0;
if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$ii ++;
		$Category [$ii] = $row ['OrganizationLevel'];
	}	
}

$i = 1;
while ( $i <= $Levels ) {
	echo '<tr><td align="left">Category Level Name - ' . htmlspecialchars($i) . ':&nbsp;<input type="text" name="names-' . htmlspecialchars($i) . '" value="' . htmlspecialchars($Category [$i]) . '" size="30" maxvalue="100"></td></tr>';
	$i ++;
}

echo '<tr><td align="center" colspan="100%" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="orglevels">';
echo '<input type="hidden" name="process" value="names-Y">';
echo '<input type="hidden" name="MultiOrgID" value="' . htmlspecialchars($MultiOrgID) . '">';
echo '<input type="submit" value="Create Category Names" class="btn btn-primary">';
echo '</td></tr>';

echo '</form>';
echo '</td></tr>';
echo '<tr><td colspan="100%" height="5"></td></tr>';

// Determine the pulldown values for each Category
echo '<form method="post" action="administration.php">';

// Form
$iii = 1;
while ( $iii <= $Levels ) {
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "OrgLevelID = :OrgLevelID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrgLevelID"=>$iii);
	//Get Organization Level Data Information
	$results = $OrganizationsObj->getOrganizationLevelDataInfo('CategorySelection', $where, 'SelectionOrder', array($params));
	
	echo '<tr><td>Category Choices for: <b>' . $Category [$iii] . '</b></td></tr>';
	
	$i = 0;
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
			$i ++;
		
			echo '<tr><td align="left">';
			echo htmlspecialchars($required) . 'Display Text: <input type="text" name="' . htmlspecialchars($iii) . '-' . htmlspecialchars($i) . '" value="' . htmlspecialchars($row ['CategorySelection']) . '" size="30" maxlength="100">';
			echo '</td></tr>';
		}
	}
	
	$ii = $i + 2;
	while ( $i < $ii ) {
		$i ++;
		
		echo '<tr><td align="left">';
		echo 'Display Text: <input type="text" name="' . htmlspecialchars($iii) . '-' . htmlspecialchars($i) . '" value="" size="30" maxlength="100">';
		echo '</td></tr>';
	}
	
	//
	$iii ++;
} // end iii

echo '<tr><td>';
echo '<p>To remove a selection; clear the "Display Text" field and select update.</p>';
echo '</td></tr>';

echo '<tr><td colspan="100%" align="center" height="60" valign="middle">';
echo '<input type="hidden" name="action" value="orglevels">';
echo '<input type="hidden" name="process" value="pulldown-Y">';
echo '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">';
echo '<input type="submit" value="Create Category Choices" class="btn btn-primary">';
echo '</td></tr>';
echo '</form>';

// Display Preview of Organization Levels
echo '<tr><td colspan="100%"><hr size="1">';
echo '<p><b>Preview</b></p>';

echo '<table>';
include IRECRUIT_DIR . 'requisitions/OrgLevelDisplay.inc';
echo '</table>';

echo '</td></tr>';
echo '</table>';
echo '</div>';
?>