<?php
if ($process == "Y") {
	
	// Remove Routines
	if (($_REQUEST['rmA'] == 'Y') || ($_REQUEST['rmB'] == 'Y')) {
		
		if ($_REQUEST['rmA'] == 'Y') {
			
			//Set information
			$set_info = array("PrimaryLogoType = ''", "PrimaryLogo = ''");
			//Set where condition
			$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
			//Set parameters
			$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
			//Update OrganizationLogos
			$OrganizationsObj->updOrganizationInfo('OrganizationLogos', $set_info, $where, array($params));
			
			
			echo '<script language="JavaScript" type="text/javascript">';
			echo "alert('Primary Logo has been removed!')";
			echo '</script>';
		}
		
		if ($_REQUEST['rmB'] == 'Y') {
			
			//Set information
			$set_info = array("SecondaryLogoType = ''", "SecondaryLogo = ''");
			//Set where condition
			$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
			//Set parameters
			$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
			//Update OrganizationLogos
			$OrganizationsObj->updOrganizationInfo('OrganizationLogos', $set_info, $where, array($params));
			
			echo '<script language="JavaScript" type="text/javascript">';
			echo "alert('Secondary Logo has been removed!')";
			echo '</script>';
		}
	}
	
	//Set where condition
	$where = array("PrimaryLogoType = ''", "PrimaryLogo = ''", "SecondaryLogoType = ''", "SecondaryLogo = ''");
	//Delete Organization Logos
	$OrganizationsObj->delOrganizationInfo('OrganizationLogos', $where, array());
}

//Set where condition	// File Upload Routines
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get OrganizationLogos Information
$results = $OrganizationsObj->getOrganizationLogosInformation("OrgID", $where, '', array($params));
$hit = $results['results'][0]['OrgID'];

$fileTypeA = '';
$fileTypeB = '';
$dataA = '';
$dataB = '';

if ($_FILES ['AttachmentA'] ['type']) {
	
	$fileTypeA = $_FILES ['AttachmentA'] ['type'];
	$dataA = file_get_contents ( $_FILES ['AttachmentA'] ['tmp_name'] );
	
	if ($hit) {
		
		//Set information
		$set_info = array("PrimaryLogoType = :PrimaryLogoType", "PrimaryLogo = :PrimaryLogo");
		//Set where condition
		$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":PrimaryLogoType"=>$fileTypeA, ":PrimaryLogo"=>$dataA);
		//Update OrganizationLogos
		$OrganizationsObj->updOrganizationInfo('OrganizationLogos', $set_info, $where, array($params));
		
	}
} // end if Attachment A

if ($_FILES ['AttachmentB'] ['type']) {
	
	$fileTypeB = $_FILES ['AttachmentB'] ['type'];
	$dataB = file_get_contents ( $_FILES ['AttachmentB'] ['tmp_name'] );
	
	if ($hit) {
		
		//Set information
		$set_info = array("SecondaryLogoType = :SecondaryLogoType", "SecondaryLogo = :SecondaryLogo");
		//Set where condition
		$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":SecondaryLogoType"=>$fileTypeB, ":SecondaryLogo"=>$dataB);
		//Update OrganizationLogos
		$OrganizationsObj->updOrganizationInfo('OrganizationLogos', $set_info, $where, array($params));
		
	}
} // end if Attachement B
  
// Preparing data to be used in MySQL query
if (! $hit) {
	//OrganizationLogos Information	
	$info = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "PrimaryLogoType"=>$fileTypeA, "PrimaryLogo"=>$dataA, "SecondaryLogoType"=>$fileTypeB, "SecondaryLogo"=>$dataB);
	//Insert OrganizationLogos
	$OrganizationsObj->insOrganizationInfo('OrganizationLogos', $info);
}

//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get OrganizationLogos Information
$results = $OrganizationsObj->getOrganizationLogosInformation("PrimaryLogoType, SecondaryLogoType", $where, '', array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$PrimaryLogoType = $row ['PrimaryLogoType'];
		$SecondaryLogoType = $row ['SecondaryLogoType'];
	}	
}
?>
<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" width="100%"
		class="table table-striped table-bordered">
		<form enctype="multipart/form-data" method="post"
			action="administration.php">
			<input type="hidden" name="MAX_FILE_SIZE" value="10485760"> 
			<input type="hidden" name="action" value="orglogos">
			<tr>
				<td><b>Primary Logo</b> - (Requisition List and Application Form)<br>
				<br>
<?php if ($PrimaryLogoType) { ?>
<img
					src="<?php echo IRECRUIT_HOME?>display_image.php?OrgID=<?php echo  $OrgID ?>&MultiOrgID=<?php echo  $MultiOrgID ?>&Type=Primary"><br>
				<br>
<?php } ?>
<input type="checkbox" name="rmA" value="Y"> (remove) 
<input type="file" name="AttachmentA" size="20"><br>
				<br> <font size="1">Upload .jpg files only. Recommended resolution:
						60x60 pixels.</font>
					<hr size=1> <br>
				<b>Secondary Logo</b> - (Thank you page)<br>
				<br>
<?php if ($SecondaryLogoType) { ?>
<img src="<?php echo IRECRUIT_HOME?>display_image.php?OrgID=<?php echo  $OrgID ?>&MultiOrgID=<?php echo  $MultiOrgID ?>&Type=Secondary"><br>
				<br>
<?php } ?>
<input type="checkbox" name="rmB" value="Y"> (remove) <input type="file"
					name="AttachmentB" size="20"><br>
				<br> <font size="1">Upload .jpg files only. Recommended resolution:
						60x60 pixels.</font>
					<hr size=1></td>
			</tr>
			<tr>
				<td align="center" height="60" valign="middle">
				<?php global $MultiOrgID;?>
				<input type="submit" value="Upload Logos" class="btn btn-primary"> 
				<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>"> 
				<input type="hidden" name="process" value="Y">
				</td>
			</tr>
		</form>
	</table>
</div>
