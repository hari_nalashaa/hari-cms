<?php 
if($_REQUEST['process'] == 'Y') {
	$app_org_labels = array();
	
	$app_arr_is_empty = true;
	for($i = 0; $i < count($_REQUEST['item']); $i++) {
		if(trim($_REQUEST['item'][$i]) != "") {
			$app_arr_is_empty = false;
			$app_org_labels[trim($_REQUEST['item'][$i])] = trim($_REQUEST['display'][$i]);
		}
	}
	
	if(isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'Y') {
		asort($app_org_labels);
	}
	
	if($app_arr_is_empty == true) {
		$app_org_labels = array();
	}
	
	//Set condition
	$where_info = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters for prepared query
	$params_upd_app_label = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":ApplicantConsiderationLabels"=>json_encode($app_org_labels));
	//Set values to update
	$set_info = array("ApplicantConsiderationLabels = :ApplicantConsiderationLabels");
	$OrganizationsObj->updOrganizationInfo('OrgData', $set_info, $where_info, array($params_upd_app_label));
}
//Get organization information
$results_labels_json  = $OrganizationDetailsObj->getOrganizationInformation($OrgID, $MultiOrgID, "ApplicantConsiderationLabels");
$results_labels  = json_decode($results_labels_json["ApplicantConsiderationLabels"], true);
?>

<div class="table-responsive">
	<form method="post" action="administration.php?action=applicantlabels&menu=8">
	<table width="770" cellspacing="3" cellpadding="5"
		class="table table-bordered">
		<tr><td colspan="3"><strong>Important Note:</strong> Please seperate the labels with comma (","). Don't use any special symbols. Only alphanumeric characters</td></tr>
		<?php
			if(is_array($results_labels)) {
				foreach ($results_labels as $labels_key=>$labels_value) {
					?>
					<tr>
						<td width="150" align="right">Passed Value:</td>
						<td>
							<input type="text" name="item[]" value="<?php echo $labels_key;?>" size="10" maxlength="10">
						</td>
						<td width="65%">
							Display Text:
							<input type="text" name="display[]" value="<?php echo $labels_value;?>" size="40" maxlength="60">
						</td>
					</tr>
					<?php
				}
			}
		?>
		<tr>
			<td width="150" align="right">Passed Value:</td>
			<td>
				<input type="text" name="item[]" size="10" maxlength="10" >
			</td>
			<td width="65%"> 
				Display Text: 
				<input type="text" name="display[]" size="40" maxlength="60">
			</td>
		</tr>
		<tr>
			<td width="150" align="right">Passed Value:</td>
			<td>
				<input type="text" name="item[]" size="10" maxlength="10" >
			</td>
			<td width="65%"> 
				Display Text: 
				<input type="text" name="display[]" size="40" maxlength="60">
			</td>
		</tr>
		<tr>
			<td colspan="3">
					Sort Display Text Alphabetically: <input name="sort" id="sort" value="Y" type="checkbox">
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<input type="submit" name="btnAddAppLabel" class="btn btn-primary" value="Submit">
				<input type="hidden" name="action" value="applicantlabels"> 
				<input type="hidden" name="process" value="Y">
			</td>
		</tr>
	</table>
	</form>
	<br><br>
	
	<table width="770" cellspacing="3" cellpadding="5"
		class="table table-bordered">
		<tr><td colspan="3"><strong>Preview:</strong></td></tr>
		<tr>
			<td>
				<select name="ddlOrgAppLabels" id="ddlOrgAppLabels">
				<option value="">Please select</option>
					<?php
						if(is_array($results_labels)) {
							foreach ($results_labels as $labels_key=>$labels_value) {
								?>
								<option value="<?php echo $labels_key;?>"><?php echo $labels_value;?></option>
								<?php
							}
						}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
