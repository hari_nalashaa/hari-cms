<style>
a {
	decoration: none;
	color: #000000;
}

a.tooltips {
	position: relative;
	display: inline;
}

a.tooltips span {
	position: absolute;
	color: #1611B8;
	background: #A6CFFF;
	line-height: 120%;
	text-align: left;
	visibility: hidden;
	border-radius: 10px;
	box-shadow: 2px 2px 4px #4E5A96;
}

a.tooltips span:after {
	content: '';
	position: absolute;
	top: 15px;
	right: 101%;
	margin-top: -6px;
	width: 0;
	height: 0;
	border-right: 10px solid #A6CFFF;
	border-top: 10px solid transparent;
	border-bottom: 10px solid transparent;
}

a:hover.tooltips span {
	visibility: visible;
	opacity: 1;
	left: 100%;
	top: 15px;
	margin-top: -23.5px;
	margin-left: 10px;
	z-index: 999;
	padding: 5px 20px;
}
</style>
<?php
$ExporterID = "DataManager";

$APPLICATION = array (
		'informed' => 'Informed' 
);

if ($_POST ['config'] == "Y") {
	$configmessage = updateExportConfig ($ExporterID,$APPLICATION);
} // end config

if ($_REQUEST['process'] == 'Y') {
	
	//Set where condition
	$where = array("OrgID = :OrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID);
	//Delete Exporter Information
	$ExporterDataManagerObj->delDataManagerInfo($where, array($params));
	
	$PayrollDBNames = array ();
	foreach ( $_POST as $k => $v ) {
		if (substr ( $k, 0, 14 ) == "PayrollDBName-") {
			$o = substr ( $k, 14, strlen ( $k ) + 1 );
			if ($v != "") {
				$PayrollDBNames [$o] = $v;
			}
		}
	}

	if (count($PayrollDBNames) > 0) { $PN = serialize ( $PayrollDBNames ); } else { $PN = ""; }
	
	//Information to insert into DataManager Information
	$info = array(
				"OrgID"=>$OrgID, 
				"ExportType"=>$_REQUEST['ExportType'], 
				"DatabaseType"=>$_REQUEST['DatabaseType'], 
				"ConnectionString"=>$_REQUEST['ConnectionString'], 
				"DatabaseName"=>$_REQUEST['DatabaseName'], 
				"UserName"=>$_REQUEST['UserName'],
				"Verification"=>$_REQUEST['Verification'],
				"SyncConnectionString"=>$_REQUEST['SyncConnectionString'], 
				"SyncDatabaseName"=>$_REQUEST['SyncDatabaseName'], 
				"SyncUserName"=>$_REQUEST['SyncUserName'],
				"SyncVerification"=>$_REQUEST['SyncVerification'],
				"Uppercase"=>$_REQUEST['Uppercase'], 
				"FileLocation"=>$_REQUEST['FileLocation'],
				"PayrollDBNames"=>$PN
			);
	//Insert DataManager Information
	$ExporterDataManagerObj->insDataManagerInfo($info);

	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Data Manager settings have been configured!')";
	echo '</script>';
} // end if process

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get DataManager Information
$results = $ExporterDataManagerObj->getDataManagerInfo("*", $where, "", array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $DM) {

		$ExportType = $DM ['ExportType'];
		$DatabaseType = $DM ['DatabaseType'];
		$ConnectionString = $DM ['ConnectionString'];
		$DatabaseName = $DM ['DatabaseName'];
		$UserName = $DM ['UserName'];
		$Verification = $DM ['Verification'];
		$SyncConnectionString = $DM ['SyncConnectionString'];
		$SyncDatabaseName = $DM ['SyncDatabaseName'];
		$SyncUserName = $DM ['SyncUserName'];
		$SyncVerification = $DM ['SyncVerification'];
		$Uppercase = $DM ['Uppercase'];
		$FileLocation = $DM ['FileLocation'];
		$PDB = unserialize ( $DM ['PayrollDBNames'] );

	} // end foreach
}


?>
<style>
table tr, td {
padding:3px;
}
</style>
<form method="post" action="administration.php">
	<table border="0" cellspacing="3" cellpadding="5" width="770">
		<tr>
			<td align="right" width="150">Export Type:</td>
			<td>Sage HRMS</td>
		</tr>

		<tr>
			<td align="right">Database Type:</td>
			<td><select name="DatabaseType">
					<option value="SQL"
						<?php if ($DatabaseType == "SQL") { echo ' selected'; } ?>>SQL</option>
					<option value="SQLQ2"
						<?php if ($DatabaseType == "SQLQ2") { echo ' selected'; } ?>>HRMS 2015/16 Q2</option>
			</select></td>
		</tr>

		<tr>
			<td align="right" valign="top">Connection String:</td>
			<td><input type="text" name="ConnectionString"
				value="<?php echo $ConnectionString; ?>" size="30" maxlength="50"> <i
				style="font-size: 8pt;">(IP Address, or System Name)</i></td>
		</tr>

		<tr>
			<td align="right" valign="top">Database Name:</td>
			<td><input type="text" name="DatabaseName"
				value="<?php echo $DatabaseName; ?>" size="25" maxlength="30"> <i
				style="font-size: 8pt;">(HRMS_Live, HRMS_Sample)</i></td>
		</tr>

		<?php
		//Set where condition
		$where = array("OrgID = :OrgID", "OnboardFormID = 'Exporter'", "QuestionID = 'Sage_OrgID'");
		//Set parameters
		$params = array(":OrgID"=>$OrgID);
		//Get OnboardQuestions Information
		$results = $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', 'value', $where, "", array($params));
		$Sage_OrgIDs = $results['results'][0]['value'];
		if (count($Sage_OrgIDs) > 0 ) {
		  $ORGS = explode ( '::', $Sage_OrgIDs );
		  foreach ( $ORGS as $org ) {
			$ORG = explode ( ':', $org );
			if ($ORG [0] != "***") {
				echo '<tr><td align="right" valign="top">' . $ORG [0] . ' Database Name:</td><td>';
				echo '<input type="text" name="PayrollDBName-' . $ORG [0] . '" value="' . $PDB [$ORG [0]] . '" size="25" maxlength="30">';
				echo '&nbsp;<i style="font-size:8pt;">(HRMS_' . $ORG [0] . ')</i>';
				echo '</td></tr>';
			}
		  }
		} // end count check
		?>

		<tr>
			<td align="right">User Name:</td>
			<td><input type="text" name="UserName"
				value="<?php echo $UserName ; ?>" size="20" maxlength="25"></td>
		</tr>

		<tr>
			<td align="right">Password:</td>
			<td><input type="password" name="Verification"
				value="<?php echo $Verification ; ?>" size="20" maxlength="50"></td>
		</tr>

                <tr>
                        <td align="right" valign="top">Separate Sync DB:</td>
                        <td align="left">

<?php if ($SyncConnectionString == "") { ?>

<div id="syncdb_configure" style="display:block">
<a href="javascript:void(0);" onclick="$('#syncdb_configure').hide();$('#syncdb_questions').show();">Configure</a>
</div>

<?php } else { ?>

<div id="syncdb_configure" style="display:block">
<a href="javascript:void(0);" onclick="$('#syncdb_configure').hide();$('#syncdb_questions').show();">Show</a>
</div>

<?php } ?>

<div id="syncdb_questions" style="display:none;">
<div>
<a href="javascript:void(0);" onclick="$('#syncdb_configure').show();$('#syncdb_questions').hide();">Hide</a>
</div>
<div style="text-align:right;width:130px;float:left;margin-right:5px;">Connection String:</div>
<div style="margin-bottom:5px;"><input type="text" name="SyncConnectionString" value="<?php echo $SyncConnectionString; ?>" size="30" maxlength="50"> <i style="font-size: 8pt;">(IP Address, or System Name)</i></div>
<div style="text-align:right;width:130px;float:left;margin-right:5px;">Database Name:</div>
<div style="margin-bottom:5px;"><input type="text" name="SyncDatabaseName" value="<?php echo $SyncDatabaseName; ?>" size="25" maxlength="30"> <i style="font-size: 8pt;">(HRMS_Live, HRMS_Sample)</i></div>
<div style="text-align:right;width:130px;float:left;margin-right:5px;">User Name:</div>
<div style="margin-bottom:5px;"><input type="text" name="SyncUserName" value="<?php echo $SyncUserName ; ?>" size="20" maxlength="25"></div>
<div style="text-align:right;width:130px;float:left;margin-right:5px;">Password:</div>
<div style="margin-bottom:5px;"><input type="password" name="SyncVerification" value="<?php echo $SyncVerification ; ?>" size="20" maxlength="50"></div> 
</div>
                        </td>
                </tr>

		<tr>
			<td align="right">Export to Uppercase:</td>
			<td><select name="Uppercase">
					<option value="Upper"
						<?php if ($Uppercase == "Upper") { echo ' selected'; } ?>>Yes</option>
					<option value=""
						<?php if ($Uppercase == "") { echo ' selected'; } ?>>No</option>
			</select></td>
		</tr>

		<tr>
			<td align="right">File Location:</td>
			<td><input type="text" name="FileLocation"
				value="<?php echo $FileLocation; ?>" size="30" maxlength="80"> <i
				style="font-size: 8pt;">(C:\\irecruit\iRecruit-Attachments)</i></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td height="60" valign="middle"><input type="hidden"
				name="ExportType" value="SageAbra"> <input type="hidden" name="process"
				value="Y"> <input type="hidden" name="action"
				value="datamanagersetup"> <input type="submit"
				value="Configure Data Manager"></td>
		</tr>

	</table>
</form>




<div id="openFormWindow" class="openForm">
	<div>
		<p style="text-align: right;">
			<a href="#closeForm" title="Close Window"> <img
				src="<?php echo IRECRUIT_HOME?>images/icons/photo_delete.png"
				title="Close Window" style="margin: 0px 3px -4px 0px;" border="0"> <b
				style="FONT-SIZE: 8pt; COLOR: #000000">Close Window</b></a>
		</p>

<?php
// HRMS Specific //Set where condition
$where = array("OrgID = 'MASTER'", "DB = 'HRMS'");
if ($feature ['PreFilledForms'] != "Y") {
	$where[] = "Section != 'Federal & State Forms'";
}

//Get SageAbraMap Information
// Exporter Map info should be here
$results = $ExporterDataManagerObj->getSageAbraMapInfo("*", $where, "Section, SubSection, FormName, SortOrder", array());
$customercnt = $results['count'];

if ($customercnt > 0) {
	$hrms = '<div style="margin-top:20px;background-color:#eee;padding:0 10px 5px 10px;border-radius:10px;">';
	$hrms .= '<br><strong style="font-size:12pt;">HRMS</strong><br>';

	if(is_array($results['results'])) {
		foreach($results['results'] as $CSAM) {

			if (($CSAM ['Section'] != "") && ($sec != $CSAM ['Section'])) {
				$hrms .= "<br>";
				$hrms .= "&nbsp;";
				$hrms .= "<b><u>" . $CSAM ['Section'] . "</u></b><br>";
			}
		
			if (($CSAM ['SubSection'] != "") && ($subsec != $CSAM ['SubSection'])) {
				$hrms .= "<br>";
				$hrms .= "&nbsp;&nbsp;";
				$hrms .= "<b>" . $CSAM ['SubSection'] . "</b><br>";
			}
		
			$hrms .= "&nbsp;&nbsp;-&nbsp;<a class=\"tooltips\" href=\"#\">" . $CSAM ['iRecruitField'];
			$hrms .= '<span><b>' . $CSAM ['DBTable'] . "</b><br><i style=\"font-size:8pt;\">" . $CSAM ['AbraField'] . '</i></span>';
			$hrms .= "</a><br>";
			$sec = $CSAM ['Section'];
			$subsec = $CSAM ['SubSection'];
		} // end foreach
	}
	
	$hrms .= '</div>';
} // end customercnt

  
// Payroll Specific
if (count ( $PDB ) > 0) {
	
	// HRMS Specific //Set where condition
	$where = array("OrgID = 'MASTER'", "DB = 'Payroll'");
	if ($feature ['PreFilledForms'] != "Y") {
		$where[] = "Section != 'Federal & State Forms'";
	}
	
	//Get SageAbraMap Information
	$results = $ExporterDataManagerObj->getSageAbraMapInfo("*", $where, "Section, SubSection, FormName, SortOrder", array());
	$customercnt = $results['count'];
	
	if ($customercnt > 0) {
		$payroll = '<div style="margin-top:20px;background-color:#eee;padding:0 10px 5px 10px;border-radius:10px;">';
		$payroll .= '<br><strong style="font-size:12pt;">Payroll</strong><br>';
		
		if(is_array($results['results'])) {
			foreach($results['results'] as $CSAM) {
				if (($CSAM ['Section'] != "") && ($sec != $CSAM ['Section'])) {
					$payroll .= "<br>";
					$payroll .= "&nbsp;";
					$payroll .= "<b><u>" . $CSAM ['Section'] . "</u></b><br>";
				}
					
				if (($CSAM ['SubSection'] != "") && ($subsec != $CSAM ['SubSection'])) {
					$payroll .= "<br>";
					$payroll .= "&nbsp;&nbsp;";
					$payroll .= "<b>" . $CSAM ['SubSection'] . "</b><br>";
				}
					
				$payroll .= "&nbsp;&nbsp;-&nbsp;<a class=\"tooltips\" href=\"#\">" . $CSAM ['iRecruitField'];
				$payroll .= '<span><b>' . $CSAM ['DBTable'] . "</b><br><i style=\"font-size:8pt;\">" . $CSAM ['AbraField'] . '</i></span>';
				$payroll .= "</a><br>";
				$sec = $CSAM ['Section'];
				$subsec = $CSAM ['SubSection'];
			} // end foreach
		}
		
		$payroll .= '</div>';
	} // end customercnt
}

// Customer Specific // HRMS Specific //Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
if ($feature ['PreFilledForms'] != "Y") {
	$where[] = "Section != 'Federal & State Forms'";
}

//Get SageAbraMap Information
$results = $ExporterDataManagerObj->getSageAbraMapInfo("*", $where, "Section, SubSection, FormName, SortOrder", array($params));
$customercnt = $results['count'];

if ($customercnt > 0) {
	$custom = '<div style="margin-top:20px;background-color:#eee;padding:0 10px 5px 10px;border-radius:10px;">';
	$custom .= '<br><strong style="font-size:12pt;">Custom</strong><br>';
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $CSAM) {

			if (($CSAM ['Section'] != "") && ($sec != $CSAM ['Section'])) {
				$custom .= "<br>";
				$custom .= "&nbsp;";
				$custom .= "<b><u>" . $CSAM ['Section'] . "</u></b><br>";
			}
		
			if (($CSAM ['SubSection'] != "") && ($subsec != $CSAM ['SubSection'])) {
				$custom .= "<br>";
				$custom .= "&nbsp;&nbsp;";
				$custom .= "<b>" . $CSAM ['SubSection'] . "</b><br>";
			}
		
			$custom .= "&nbsp;&nbsp;-&nbsp;<a class=\"tooltips\" href=\"#\">" . $CSAM ['iRecruitField'];
			$custom .= '<span><b>' . $CSAM ['DB'] . '<br>' . $CSAM ['DBTable'] . "</b><br><i style=\"font-size:8pt;\">" . $CSAM ['AbraField'] . '</i></span>';
			$custom .= "</a><br>";
			$sec = $CSAM ['Section'];
			$subsec = $CSAM ['SubSection'];
		} // end foreach
	}
	
	$custom .= '</div>';
} // end customercnt

?>
<table border="0" cellspacing="0" cellpadding="10" width="100%">
			<tr>
				<td valign="top"><?php echo $hrms?></td>
				<td valign="top"><?php echo $payroll?></td>
				<td valign="top"><?php echo $custom?></td>
			</tr>
		</table>
	</div>
</div>



<div style="float: left; margin-left: 45px; margin-top: 10px;">
	<a href="#openFormWindow">
		<button type="button">Sage Transfer Fields</button>
	</a>
</div>

<div style="float: left; margin-left: 45px; margin-top: 10px;">
	<form method="post" action="administration.php"
		onsubmit="return confirm('Are you sure you want to update the Exporter?\n\n')">
		<input type="hidden" name="action" value="datamanagersetup"> <input
			type="hidden" name="config" value="Y"> <input type="submit"
			value="Configure Exporter">
	</form>
<?php echo $configmessage;?>
</div>

<div style="clear: both;"></div>

<?php
function updateExportConfig($ExporterID, $APPLICATION) {

	global $OrgID, $feature, $ExporterDataManagerObj, $FormQuestionsObj, $FormSettingsObj, $IrecruitApplicationFeaturesObj;
	
	$message = "";
	
	//Set where condition
	$where = array("OrgID = :OrgID", "ExporterID = :ExporterID");
	//Set paramters
	$params = array(":OrgID"=>$OrgID, ":ExporterID"=>$ExporterID);
	
	//Delete ExporterScheduler Information
	//$ExporterDataManagerObj->delExporterInfo('ExporterScheduler', $where, array($params));
	
	//Delete ExporterScheduler Information
	$ExporterDataManagerObj->delExporterInfo('ExporterNames', $where, array($params));
	
	//Delete ExporterScheduler Information
	$ExporterDataManagerObj->delExporterInfo('Exporter', $where, array($params));
	
	//Set ExporterNames Information
	$info = array (
				"OrgID"=>$OrgID, 
				"ExporterID"=>$ExporterID, 
				"ExportName"=>$ExporterID, 
				"LastAccessed"=>"NOW()"
				);
	
	//Insert Exporter Information
	$ExporterDataManagerObj->insExporterInfo ( 'ExporterNames', $info );
	
	$i = 0;
	
	if(is_array($APPLICATION)) {
		// Add Personal Questions
		foreach ( $APPLICATION as $q => $t ) {
			$i ++;
			$QuestionTypeID = "6";
			$Section = "1";
			if ($q == "informed") {
				$QuestionTypeID = "3";
				$Section = "2";
			}
			insertRow ( $OrgID, $i, "ApplicationForm", "", $Section, $q, "", $QuestionTypeID, $t, $ExporterID);
		}
	}
	
	$FEATURES = $IrecruitApplicationFeaturesObj->getApplicationFeaturesByOrgID ( $OrgID );

    //Set where condition
    $where = array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "Active = 'Y'");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":OnboardFormID"=>$FEATURES['DataManagerType']);
	//Get FormQuestions Information
	$results = $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', '*', $where, "QuestionOrder", array($params));

	// Add OnboardQuestions
	if(is_array($results['results'])) {
		foreach($results['results'] as $OQ) {
			$i ++;
			insertRow ( $OrgID, $i, "OnboardForm", $FEATURES['DataManagerType'], 0, $OQ ['QuestionID'], "", $OQ ['QuestionTypeID'], $OQ ['Question'], $ExporterID );
		} // end foreach
	}
	
	//Set where condition
	$where = array("OrgID = 'MASTER'", "iRecruitTable = 'Requisition'");
	//Set parameters
	$params = array(":OrgID"=>$OrgID);
	//Get SageAbraMap Information
	$results = $ExporterDataManagerObj->getSageAbraMapInfo("*", $where, "Section, SubSection, FormName, SortOrder", array($params));
	
	// Add Requisition Questions
	if(is_array($results['results'])) {
		foreach($results['results'] as $SAMR) {
			$i ++;
			insertRow ( $OrgID, $i, $SAMR ['iRecruitTable'], "", 0, $SAMR ['iRecruitField'], "", 6, $SAMR ['iRecruitField'], $ExporterID );
		} // end foreach
	}
	  
	// Add Multiple Sections //Set where condition
	$where = array("FormType = 'DataManager'");
	$results = $ExporterDataManagerObj->getExporterInfo("ExporterDataMap", "*", $where, "SortOrder", array());
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $EDM) {

			$i ++;
			if ($EDM ['DataField'] == "FedStateForms") {
				if ($feature ['PreFilledForms'] == "Y") {
					insertRow ( $OrgID, $i, "DataManager", "", 0, $EDM ['DataField'], "", $EDM ['QuestionTypeID'], $EDM ['Description'], $ExporterID );
				}
			} else {
				insertRow ( $OrgID, $i, "DataManager", "", 0, $EDM ['DataField'], "", $EDM ['QuestionTypeID'], $EDM ['Description'], $ExporterID );
			}
		} // end foreach
	}
	
	  
	//Add Custom Data Questions //Set where condition
    $where      =   array("OrgID = :OrgID");
	//Set parameters
    $params     =   array(":OrgID"=>$OrgID);
	//Get SageAbraMap Information
    $results    =   $ExporterDataManagerObj->getSageAbraMapInfo("*", $where, "Section, SubSection, FormName, SortOrder", array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $SAM) {

			if ($SAM ['iRecruitTable'] == "OnboardForm") {
					
				//Set where condition
				$where  = array("OrgID = :OrgID", "OnboardFormID = 'Exporter'", "QuestionID = :QuestionID");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":QuestionID"=>$SAM ['iRecruitField']);
				//Get FormQuestions Information
				$results = $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', 'Question, QuestionTypeID', $where, "OrgID LIMIT 1", array($params));
					
				if(is_array($results['results'][0])) {
					list ( $Question, $QuestionTypeID ) = array_values($results['results'][0]);
				}
					
			} else if ($SAM ['iRecruitTable'] == "WebForm") {
		
				//Set where condition
				$where = array("OrgID = :OrgID", "WebFormID = :WebFormID", "QuestionID = :QuestionID");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":WebFormID"=>$SAM ['FormName'], ":QuestionID"=>$SAM ['iRecruitField']);
				//Get FormQuestions Information
				$results = $FormQuestionsObj->getQuestionsInformation('WebFormQuestions', 'Question, QuestionTypeID', $where, "OrgID LIMIT 1", array($params));
		
				if(is_array($results['results'][0])) {
					list ( $Question, $QuestionTypeID ) = array_values($results['results'][0]);
				}
					
			} else if ($SAM ['iRecruitTable'] == "ApplicationForm") {
		
				//Set where condition
				$where = array("OrgID = :OrgID", "QuestionID = :QuestionID");
				//Set parameters
				$params = array(":OrgID"=>$OrgID, ":QuestionID"=>$SAM ['iRecruitField']);
				//Get FormQuestions Information
				$results = $FormQuestionsObj->getQuestionsInformation('FormQuestions', 'Question, QuestionTypeID', $where, "OrgID LIMIT 1", array($params));
					
				if(is_array($results['results'][0])) {
					list ( $Question, $QuestionTypeID ) = array_values($results['results'][0]);
				}
			}
		
			$i ++;
			insertRow ( $OrgID, $i, $SAM ['iRecruitTable'], $SAM ['FormName'], 0, $SAM ['iRecruitField'], "", $QuestionTypeID, strip_tags ( $Question ), $ExporterID );
		} // end foreach
	}
	
	$message .= "&nbsp;&nbsp;";
	$message .= '<span style="font-size:10px;color:red;">Configuration Complete</span>';
	
	return $message;
} // end function
function insertRow($OrgID, $i, $FormType, $FormID, $SectionID, $QuestionID, $FriendlyName, $QuestionTypeID, $Question, $ExporterID) {
	global $ExporterDataManagerObj;
	//Exporter Information To Insert
	$info = array (
			"OrgID" => $OrgID,
			"ExporterID" => $ExporterID,
			"FormType"=>$FormType,
			"FormID"=>$FormID,
			"SectionID"=>$SectionID,
			"QuestionID"=>$QuestionID,
			"FriendlyName"=>$FriendlyName,
			"QuestionTypeID"=>$QuestionTypeID,
			"Question"=>$Question,
			"SortOrder"=>$i 
	);
	//Insert Exporter Information
	$ExporterDataManagerObj->insExporterInfo ( 'Exporter', $info );
} // end function
?>
