<?php
if ($UserIDedit) {
	$UserID = $UserIDedit;
}
if ($adduser) {
	if ($UserID == "") {
		$UserID = $USERID;
	}
}

// New User Process
if ($process == "NewUser") {
	
	$ERROR = "";
	
	$ERROR .= $ValidateObj->validate_userid ( $UserID );
	$ERROR .= $ValidateObj->validate_verification ( $_POST['UsersVerification'], '', 'N' );
	
	if (strlen ( $_POST['UsersFirstName'] ) <= 3) {
		$ERROR .= "Please enter a First Name." . "\\n";
	}
	
	if (strlen ( $_POST['UsersLastName'] ) <= 3) {
		$ERROR .= "Please enter a Last Name." . "\\n";
	}
	
	$ERROR .= $ValidateObj->validate_email_address ( $_POST['UsersEmailAddress'] );
	
	// set where condition
	$where 		=	array ("UserID = :UserID");
	// set parameters
	$params		=	array (":UserID" => $UserID );
	// Get UserInformation
	$results 	=	$IrecruitUsersObj->getUserInformation ( "*", $where, "", array ($params) );
	$usrcnt 	=	$results ['count'];
	
	if ($usrcnt > 0) {
		$ERROR .= "The user name already exists." . "\\n";
	}
	
	if ($ERROR) {
		
		$MSG = "The following errors have occured:\\n\\n" . $ERROR . "\\n\\n";
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $MSG . "')";
		echo '</script>';
		
		$adduser              	=   'R';
		$RUserID              	=   $_REQUEST['UserID'];
		$UserID               	=   $USERID;
		$RVerification        	=   $_POST['UsersVerification'];
		$RRole                	=   $_POST['Role'];
		$RFirstName           	=   $_POST['UsersFirstName'];
		$RLastName            	=   $_POST['UsersLastName'];
		$RTitle               	=   $_POST['UsersTitle'];
		$RAddress1            	=   $_POST['UsersAddress1'];
		$RAddress2            	=   $_POST['UsersAddress2'];
		$RCity                	=   $_POST['UsersCity'];
		$RState               	=   $_POST['UsersState'];
		$RZipCode             	=   $_POST['UsersZipCode'];
		$RPhone               	=   $_POST['UsersPhone'];
		$RCell                	=   $_POST['UsersCell'];
		$REmailAddress        	=   $_POST['UsersEmailAddress'];
		$RAppView             	=   $_POST['UsersAppView'];
		$RCostCenter	      	=   $_POST['CostCenter'];
		
	} else { // else error
	         
		// Users Information
		$users_info = array (
				"UserID"            =>  $UserID,
				"Verification"      =>  password_hash($_POST['UsersVerification'], PASSWORD_DEFAULT),
				"OrgID"             =>  $OrgID,
				"Role"              =>  $_POST['Role'],
				"AccessCode"        =>  uniqid (),
				"FirstName"         =>  $_POST['UsersFirstName'],
				"LastName"          =>  $_POST['UsersLastName'],
				"Title"             =>  $_POST['UsersTitle'],
				"Address1"          =>  $_POST['UsersAddress1'],
				"Address2"          =>  $_POST['UsersAddress2'],
				"City"              =>  $_POST['UsersCity'],
				"State"             =>  $_POST['UsersState'],
				"ZipCode"           =>  $_POST['UsersZipCode'],
				"Phone"             =>  $_POST['UsersPhone'],
				"Cell"              =>  $_POST['UsersCell'],
				"EmailAddress"      =>  $_POST['UsersEmailAddress'],
				"ApplicationView"   =>  $_POST['UsersAppView'],
				"CostCenter"   	    =>  $_POST['CostCenter'],
		);
		// Insert Users Information
		$IrecruitUsersObj->insUsers ( $users_info );
		
		// Insert Application Permissions Information
		$info = array (
				"UserID" => $UserID,
				"Active" => "1" 
		);
		$IrecruitApplicationFeaturesObj->insApplicationPermissions ( $info );
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('User information has been configured!')";
		echo '</script>';
	} // end error check else
} // end if new user
  
// Edit existing User Process
if ($process == "Users") {
	
	$ERROR = "";
	
	$ERROR .= $ValidateObj->validate_email_address ( $_REQUEST['UsersEmailAddress'] );
	
	if (strlen ( $_POST['UsersFirstName'] ) <= 3) {
		$ERROR .= "Please enter a First Name." . "\\n";
	}
	
	if (strlen ( $_POST['UsersLastName'] ) <= 3) {
		$ERROR .= "Please enter a Last Name." . "\\n";
	}
	
	if ($ERROR) {
		
		$MSG = "The following errors have occured:\\n\\n" . $ERROR . "\\n\\n";
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $MSG . "')";
		echo '</script>';
	} else { // else error
		
		//Set where condition
		$where = array("UserID = :UserID", "OrgID = :OrgID"); 
		//Set Update Information
		$set_info = array (
                    "Role               =   :Role",
                    "AccessCode         =   :AccessCode",
                    "FirstName          =   :FirstName",
                    "LastName           =   :LastName",
                    "Title              =   :Title",
                    "Address1           =   :Address1",
                    "Address2           =   :Address2",
                    "City               =   :City",
                    "State              =   :State",
                    "ZipCode            =   :ZipCode",
                    "Phone              =   :Phone",
					"Cell               =   :Cell",
                    "EmailAddress       =   :EmailAddress",
                    "LastAccess         =   NOW()",
                    "ApplicationView    =   :ApplicationView",
                    "CostCenter			=   :CostCenter"
		);
		
		if($_POST['UsersVerification'] != "") {
			$set_info[] = "Verification = :Verification";
		}
		
		//Set parameters
		$params = array(
                    ":Role"             =>  $_POST['Role'],
                    ":AccessCode"       =>  uniqid(),
                    ":FirstName"        =>  $_POST['UsersFirstName'],
                    ":LastName"         =>  $_POST['UsersLastName'],
                    ":Title"            =>  $_POST['UsersTitle'],
                    ":Address1"         =>  $_POST['UsersAddress1'],
                    ":Address2"         =>  $_POST['UsersAddress2'],
                    ":City"             =>  $_POST['UsersCity'],
                    ":State"            =>  $_POST['UsersState'],
                    ":ZipCode"          =>  $_POST['UsersZipCode'],
                    ":Phone"            =>  $_POST['UsersPhone'],
					":Cell"             =>  $_POST['UsersCell'],
					":EmailAddress"     =>  $_POST['UsersEmailAddress'],
                    ":ApplicationView"  =>  $_POST['UsersAppView'],
                    ":UserID"           =>  $_REQUEST['UserID'], 
                    ":OrgID"            =>  $_REQUEST['OrgID'],
                    ":CostCenter"       =>  $_REQUEST['CostCenter'],
		);

		if($_POST['UsersVerification'] != "") {
			$params[":Verification"] = password_hash($_POST['UsersVerification'], PASSWORD_DEFAULT);
		}
				
		
		//Update Users Information
		$IrecruitUsersObj->updUsersInfo ( $set_info, $where, array($params) );
		
		
		$Role = $_REQUEST['Role'];
		if (! $_REQUEST['Role']) {
			$Role = 'master_admin';
		}
		
		//Update Application Permissions
		$IrecruitApplicationFeaturesObj->updApplicationPermissions($UserID, $Role);
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('User information has been configured!')";
		echo '</script>';
	} // end error check else
} // end if process users

//Set columns
$columns    =   "UserID, Verification, OrgID, Role, FirstName, LastName, Title,
    			Address1,  Address2, City, State, ZipCode, Phone, Cell, EmailAddress, 
    			ApplicationView, CostCenter, TwilioSms, ActivationDate, LastAccess";
//Get UserInformation	// display user
$row        =   G::Obj('IrecruitUsers')->getUserInfoByUserID($UserID, $columns);
	
$UserID				=   $row ['UserID'];
$Verification		=   $row ['Verification'];
$OrgID				=   $row ['OrgID'];
$Role				=   $row ['Role'];
$FirstName			=   $row ['FirstName'];
$LastName			=   $row ['LastName'];
$Title				=   $row ['Title'];
$Address1			=   $row ['Address1'];
$Address2			=   $row ['Address2'];
$City				=   $row ['City'];
$State				=   $row ['State'];
$ZipCode			=   $row ['ZipCode'];
$Phone				=   $row ['Phone'];
$Cell				=   $row ['Cell'];
$EmailAddress		=   $row ['EmailAddress'];
$ActivationDate		=   $row ['ActivationDate'];
$LastAccess			=   $row ['LastAccess'];
$AppView			=   $row ['ApplicationView'];
$CostCenter			=   $row ['CostCenter'];
$TwilioSms			=	$row ['TwilioSms'];

// get roles
$where        =   array("UserID LIKE 'master_%'");
$roleresults  =   $IrecruitApplicationFeaturesObj->getApplicationPermissionsInfo('ApplicationPermissions', "UserID", $where, "", array());
	
// begin content for display
$rtn .= '';

$rtn .= '<table border="0" cellspacing="3" cellpadding="5" width="770" class="table table-striped table-bordered">';
$rtn .= '<tr><td colspan="100%" height="5" valign="middle">&nbsp;</td></tr>';
	
$rtn .= <<<END
<form name="frmUserInfo" id="frmUserInfo" method=post action="$SCRIPT_NAME">
<tr><td align="right" width="150">
<font color=red>*</font>
UserID:
</td><td>
END;
	
if ($adduser) {

	if ($adduser == "R") {
        $UserID             =   $RUserID;
        $Verification       =   $RVerification;
        $Role               =   $RRole;
        $FirstName          =   $RFirstName;
        $LastName           =   $RLastName;
        $Title              =   $RTitle;
        $Address1           =   $RAddress1;
        $Address2           =   $RAddress2;
        $City               =   $RCity;
        $State              =   $RState;
        $ZipCode            =   $RZipCode;
        $Phone              =   $RPhone;
        $Cell               =   $RCell;
        $EmailAddress       =   $REmailAddress;
        $AppView            =   $RAppView;
        $CostCenter	    	=   $RCostCenter;
        
	} else {
        $UserID             =   "";
        $Verification       =   "";
        $Role               =   "";
        $FirstName          =   "";
        $LastName           =   "";
        $Title              =   "";
        $Address1           =   "";
        $Address2           =   "";
        $City               =   "";
        $State              =   "";
        $ZipCode            =   "";
        $Phone              =   "";
        $Cell               =   "";
        $EmailAddress       =   "";
        $CostCenter	    	=   "";
	}
	
$rtn .= <<<END
<input type="text" size="20" maxlengh="45" name="UserID" value="$UserID"/> <font size=1>Special characters will be reformatted.</font>
END;
} else {
	
$rtn .= <<<END
<input type="hidden" name="UserID" value="$UserID" />$UserID
END;
}
	
$rtn .= <<<END
</td></tr>
	
<tr><td align="right">
<font color=red>*</font>
Password:
</td><td>
<input type="text" size="20" maxlength="45" name="UsersVerification" id="UsersVerification" />
END;
	
$rtn .= <<<END
</td></tr>
END;
	
/*
 * $rtn .= <<<END <tr><td align="right"> OrgID: </td><td> $OrgID </td></tr> END;
*/
	
if ($permit ['Admin'] < 1) { // user only
    $rtn .= '<input type="hidden" name="Role" value="' . $Role . '">';
} else {
	// Display Role Field
	$rtn .= <<<END
<tr><td align="right">
<font color=red>*</font>
Role:
</td><td>
END;
	
	$rtn .= <<<END
<select name=Role>
END;
	
if(is_array($roleresults['results'])) {
    foreach($roleresults['results'] as $row) {
						
        $rtn .= <<<END
<option value="$row[UserID]"
END;
						
        if ($Role == $row ['UserID']) {
            $rtn .= ' selected="selected"';
        }
						
        $roleval = DisplayRole ( $row ['UserID'] );
						
        $rtn .= <<<END
>$roleval</option>
END;
    }
}
			
	
$rtn .= <<<END
<option value=""
END;
	
if ($Role == "") {
    $rtn .= ' selected="selected"';
}
	
$rtn .= <<<END
>Other</option>
END;
	
$rtn .= <<<END
</select>
</td></tr>
END;
		
} // end permit user else
		 
		// Display Processing View
		$rtn .= <<<END
<tr><td align="right">
<font color=red>*</font>
Processing View:</td>
<td><select name="UsersAppView">
END;
	
		$rtn .= '<option value="M"';
		if ($AppView == "M") {
			$rtn .= ' selected';
		}
		$rtn .= '>Multiple</option>';
	
		$rtn .= '<option value="S"';
		if ($AppView == "S") {
			$rtn .= ' selected';
		}
		$rtn .= '>Single</option>';
	
		$rtn .= <<<END
</select>
</td></tr>
	
<tr><td align="right">
<font color=red>*</font>
First Name:
</td><td>
<input type=text size=45 maxlength=45 name=UsersFirstName value="$FirstName" />
</td></tr>
	
<tr><td align="right">
<font color=red>*</font>
Last Name:
</td><td>
<input type=text size=45 maxlength=60 name=UsersLastName value="$LastName" />
</td></tr>

<tr><td align="right">
Title:
</td><td>
<input type=text size=45 maxlength=45 name=UsersTitle value="$Title" />
</td></tr>
	
<tr><td align="right">
<font color=red>*</font>
Email Address:
</td><td>
<input type=text size=45 maxlength=60 name=UsersEmailAddress value="$EmailAddress" />
</td></tr>
	
<tr><td align="right">
Address 1:
</td><td>
<input type=text size=45 maxlength=45 name=UsersAddress1 value="$Address1" />
</td></tr>
	
<tr><td align="right">
Address 2:
</td><td>
<input type=text size=45 maxlength=45 name=UsersAddress2 value="$Address2" />
</td></tr>
	
<tr><td align="right">
City:
</td><td>
<input type=text size=45 maxlength=45 name=UsersCity value="$City" />
</td></tr>
	
<tr><td align="right">
State:
</td><td>
<input type=text size=2 maxlength=2 name=UsersState value="$State" />
</td></tr>
	
<tr><td align="right">
Zip Code:
</td><td>
<input type=text size=12 maxlength=10 name=UsersZipCode value="$ZipCode" />
</td></tr>
	
<tr><td align="right">
Phone:
</td><td>
<input type=text size=12 maxlength=20 name=UsersPhone value="$Phone" />
</td></tr>
END;

//Get Twilio Account Information
$account_info   =   G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);

if($account_info['AccountSid'] != "") {
    $rtn .= '<input type=hidden size=12 maxlength=20 name=UsersCell id=UsersCell value="'.$Cell.'" />';
}
else {
    $rtn .= '<tr>
			<td align="right">
			Cell:
			</td>
			<td>
			<input type=text size=12 maxlength=20 name=UsersCell id=UsersCell value="'.$Cell.'" />
			</td>
			</tr>';
}
	

if (! $adduser) {
	$rtn .= <<<END
<tr><td align="right">
Activation Date:
</td><td>
$ActivationDate
</td></tr>

<tr><td align="right">
Last Access:
</td><td>
$LastAccess
</td></tr>
END;
}

if ($OrgID == "I20120605") { // GOODWILL

$COSTCENTERS=array();

$COSTCENTERS[]="47041";
$COSTCENTERS[]="47061";
$COSTCENTERS[]="50100";
$COSTCENTERS[]="51500";
$COSTCENTERS[]="52001";
$COSTCENTERS[]="52101";
$COSTCENTERS[]="52102";
$COSTCENTERS[]="52103";
$COSTCENTERS[]="52104";
$COSTCENTERS[]="52105";
$COSTCENTERS[]="52109";
$COSTCENTERS[]="52113";
$COSTCENTERS[]="52115";
$COSTCENTERS[]="52116";
$COSTCENTERS[]="52117";
$COSTCENTERS[]="52301";
$COSTCENTERS[]="52307";
$COSTCENTERS[]="52308";
$COSTCENTERS[]="52501";
$COSTCENTERS[]="52503";
$COSTCENTERS[]="52504";
$COSTCENTERS[]="52506";
$COSTCENTERS[]="52507";
$COSTCENTERS[]="52508";
$COSTCENTERS[]="52701";
$COSTCENTERS[]="52702";
$COSTCENTERS[]="52703";
$COSTCENTERS[]="52704";
$COSTCENTERS[]="52708";
$COSTCENTERS[]="52902";
$COSTCENTERS[]="52904";
$COSTCENTERS[]="52906";
$COSTCENTERS[]="52908";
$COSTCENTERS[]="52909";
$COSTCENTERS[]="52909";
$COSTCENTERS[]="52910";
$COSTCENTERS[]="52911";
$COSTCENTERS[]="52912";
$COSTCENTERS[]="52913";
$COSTCENTERS[]="52914";
$COSTCENTERS[]="52915";
$COSTCENTERS[]="53310";
$COSTCENTERS[]="54010";
$COSTCENTERS[]="54016";
$COSTCENTERS[]="54017";
$COSTCENTERS[]="54030";
$COSTCENTERS[]="54031";
$COSTCENTERS[]="54037";
$COSTCENTERS[]="54302";
$COSTCENTERS[]="54303";
$COSTCENTERS[]="54535";
$COSTCENTERS[]="55063";
$COSTCENTERS[]="55064";
$COSTCENTERS[]="55066";
$COSTCENTERS[]="55067";
$COSTCENTERS[]="55069";
$COSTCENTERS[]="58006";
$COSTCENTERS[]="58010";
$COSTCENTERS[]="60106";
$COSTCENTERS[]="60600";
$COSTCENTERS[]="60102";
$COSTCENTERS[]="61102";
$COSTCENTERS[]="61400";
$COSTCENTERS[]="61603";
$COSTCENTERS[]="62600";

$rtn .= '<tr><td align="right">';
$rtn .= 'Cost Center:';
$rtn .= '</td><td>';
$rtn .= '<select name = "CostCenter">';
$rtn .= '<option value="">Please Select</option>';
foreach ($COSTCENTERS as $v => $CS) {

  	$rtn .= '<option value="' . $CS . '"';
  	
  	if ($CS == $CostCenter) {  $rtn .= ' selected'; }
		$rtn .= '>' . $CS . '</option>';
	}
	
	$rtn .= '</select>';
	$rtn .= '</td></tr>';

} // END Goodwill


$rtn .= <<<END
<td colspan="100%" height="60" valign="middle" align="center">
<input type="hidden" name="UserIDedit" value="$UserIDedit" />
<input type="hidden" name="OrgID" value="$OrgID" />
END;

if ($adduser) {
	$rtn .= '<input type="hidden" name="process" value="NewUser" />';
	$rtn .= '<input type="hidden" name="action" value="useredit" />';
	$rtn .= '<input type="hidden" name="useraction" value="profile" />';
	$rtn .= '<input type="submit" value="Add User" class="btn btn-primary"/>';
	$rtn .= '<span id="user_validate_processing_msg" style="color:blue"></span>';
} else {
	$rtn .= '<input type="hidden" name="process" value="Users" />';
	$rtn .= '<input type="hidden" name="action" value="useredit" />';
	$rtn .= '<input type="hidden" name="useraction" value="profile" />';
	$rtn .= '<input type="submit" value="Update User Information" class="btn btn-primary"/>';
	$rtn .= '<span id="user_validate_processing_msg" style="color:blue"></span>';
}

$rtn .= <<<END
</form>

</td></tr>
</table>
<br />
END;

return $rtn;
?>
