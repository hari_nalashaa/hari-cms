<?php
if(isset($_REQUEST['ddlCompaniesList'])
    && $_REQUEST['ddlCompaniesList'] != ""
    && isset($_REQUEST['matrx_care_action'])
    && $_REQUEST['matrx_care_action'] == 'delete') {
    
    //Delete Cloud Api Information
    $MatrixCareObj->delMatrixCareApiLoginInformation($OrgID, $_REQUEST['ddlCompaniesList']);

    $msg                 =   'msg=sucdel';
    $location_href_link  =   'location.href = "' . IRECRUIT_HOME . 'administration.php?action=setupmatrixcare';
    $location_href_link .=   '&OrgID='.$OrgID.'&'.$msg.'";';
    echo '<script>'.$location_href_link.'</script>';
}

$matrix_care_errors = '';
if((count($_POST) > 0)
    &&
    ($_REQUEST['CompanyName'] == "" 
    || $_REQUEST['MatrixCareUserName'] == "" 
    || $_REQUEST['MatrixCarePassword'] == "" 
    || $_REQUEST['MatrixCareTenant'] == ""
    )) {
    
    $matrix_care_errors = 'Please fill all the mandatory fields.';
}

if($matrix_care_errors == "") {

    if(isset($_POST['btnSubmit'])) {
        if(isset($_REQUEST['CompanyID'])
        && $_REQUEST['CompanyID'] != ""
            && isset($_POST['CompanyName'])
            && isset($_POST['MatrixCareUserName'])
            && isset($_POST['MatrixCarePassword'])
            && $_POST['MatrixCareUserName'] != ""
            && $_POST['MatrixCarePassword'] != ""
            && $_REQUEST['MatrixCareTenant'] != "") {
    
            //Update Cloud Api Information
            $MatrixCareObj->updateMatrixCareLoginInformation($OrgID, $_REQUEST['CompanyName'], $_REQUEST['CompanyID'], $_REQUEST['MatrixCareUserName'], $_REQUEST['MatrixCarePassword'], $_REQUEST['MatrixCareTenant'], $_REQUEST['ddlUpperCase']);
    
            $MatrixCareCompanyID    =   $_REQUEST['CompanyID'];
    
            //Update Company Is Default
            $MatrixCareObj->updateCompanyIsDefault($OrgID, $MatrixCareCompanyID);
    
            require_once IRECRUIT_DIR . 'onboard/MatrixCareLogin.inc';
            
            $office_ids_url         =   $MatrixCareObj->api_router_url . $_REQUEST['MatrixCareTenant'] . "/offices";
            $mc_office_result       =   $MatrixCareObj->getOfficeIdsByMatrxCareApi($office_ids_url, $matrixcare_info);
            $mc_office_ids_list     =   json_decode($mc_office_result, true);
             
            if(count($mc_office_ids_list) > 0) {
                for($j = 0; $j < count($mc_office_ids_list); $j++) {
                    $branches_list[$mc_office_ids_list[$j]['id']] = $mc_office_ids_list[$j]['nickName'] . " - " . $mc_office_ids_list[$j]['formalName'];
                }
                
                //Insert Matrix Care OfficeIds
                $MatrixCareObj->insMatrixCareOfficeIds($OrgID, $_REQUEST['CompanyID'], $branches_list);
            }
    
            $msg = 'msg=sucupd';
            if(!isset($matrixcare_info['access_token']) || $matrixcare_info['access_token'] == "") {
                $msg = 'msg=failupdtoken';
            }
             
            $location_href_link = 'location.href = "' . IRECRUIT_HOME . 'administration.php?action=setupmatrixcare';
            $location_href_link .= '&OrgID='.$OrgID;
            $location_href_link .= '&CompanyID='.$MatrixCareCompanyID.'&matrx_care_action=edit';
            $location_href_link .= '&'.$msg.'";';
            echo '<script>'.$location_href_link.'</script>';
        }
        else if(count($_POST) > 0 
                && $_REQUEST['CompanyID'] == ""
                && $_REQUEST['MatrixCareUserName'] != ""
                && $_REQUEST['MatrixCarePassword'] != ""
                && $_REQUEST['MatrixCareTenant'] != "") {
            //Update Cloud Api Information
            $MatrixCareCompanyID = $MatrixCareObj->insertMatrixCareLoginInformation($OrgID, $_REQUEST['CompanyName'], $_REQUEST['MatrixCareUserName'], $_REQUEST['MatrixCarePassword'], $_REQUEST['MatrixCareTenant'], $_REQUEST['ddlUpperCase']);
    
            //Update Company Is Default
            $MatrixCareObj->updateCompanyIsDefault($OrgID, $MatrixCareCompanyID);
    
            require_once IRECRUIT_DIR . 'onboard/MatrixCareLogin.inc';
             
            $msg = 'msg=sucins';
    
            if(!isset($matrixcare_info['access_token']) || $matrixcare_info['access_token'] == "") {
                $msg = 'msg=failinstoken';
            }
    
            $office_ids_url         =   $MatrixCareObj->api_router_url . $_REQUEST['MatrixCareTenant'] . "/offices";
            $mc_office_result       =   $MatrixCareObj->getOfficeIdsByMatrxCareApi($office_ids_url, $matrixcare_info);
            $mc_office_ids_list     =   json_decode($mc_office_result, true);
    
            if(count($mc_office_ids_list) > 0) {
                for($j = 0; $j < count($mc_office_ids_list); $j++) {
                    $branches_list[$mc_office_ids_list[$j]['id']] = $mc_office_ids_list[$j]['nickName'] . " - " . $mc_office_ids_list[$j]['formalName'];
                }
    
                //Insert Matrix Care OfficeIds
                $MatrixCareObj->insMatrixCareOfficeIds($OrgID, $MatrixCareCompanyID, $branches_list);
            }
    
            $location_href_link  = 'location.href = "' . IRECRUIT_HOME . 'administration.php?action=setupmatrixcare';
            $location_href_link .= '&OrgID='.$OrgID;
            $location_href_link .= '&'.$msg.'";';
            echo '<script>'.$location_href_link.'</script>';
    
        }
    }
}


//Set where condition
$where      =   array("OrgID = :OrgID", "OnboardFormID = 'STANDARD'", "QuestionID = 'matrixcare_OfficeID'");
//Set parameters
$params     =   array(":OrgID"=>$OrgID);
//Get Questions Information
$results    =   $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', "*", $where, "", array($params));

if($results['count'] == 0) {

    //Set Columns
    $columns            =   "'$OrgID', 'STANDARD', QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, (SELECT MAX(QuestionOrder) + 1 FROM OnboardQuestions WHERE OrgID = '$OrgID' AND OnboardFormID = 'STANDARD'), Required, Validate, SageLock";
    //Set where condition
    $where              =   array("OrgID = 'MASTER'", "OnboardFormID = 'STANDARD'", "QuestionID = 'matrixcare_OfficeID'");

    $results_copy       =   $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', "Question", $where, "", array());

    //Insert Default Onboard Question OfficeID
    $FormQuestionsObj->insFormQuestionsMaster('OnboardQuestions', $columns, $where, array($params));

} // end if OnboardQuestions



$office_ids_info        =   $MatrixCareObj->getMatrixCareOfficeIds($OrgID, $_REQUEST['CompanyID']);
$branches_info          =   json_decode($office_ids_info['BranchesInfo'], true);

list($MatrixCareInfo, $MatrixCount) = $MatrixCareObj->getMatrixCareLoginInformation($OrgID, $_REQUEST['CompanyID']);

$matrixcare_info = $MatrixCareObj->getMatrixCareLoginSessionID($OrgID, $_REQUEST['CompanyID']);

if(!isset($MatrixCareInfo['UserName'])) $MatrixCareInfo['UserName'] = '';
if(!isset($MatrixCareInfo['Password'])) $MatrixCareInfo['Password'] = '';
if(!isset($MatrixCareInfo['CompanyName'])) $MatrixCareInfo['CompanyName'] = '';

if(is_object($TemplateObj)) {
	$TemplateObj->MatrixCareInfo   =   $MatrixCareInfo;
	$TemplateObj->Count            =   $Count;
}

$matrix_care_companies_res      =   $MatrixCareObj->getMatrixCareCompanies($OrgID);
$matrix_care_companies_list     =   $matrix_care_companies_res['results'];
$matrix_api_companies_cnt       =   $matrix_care_companies_res['count'];
?>
<form method="post" name="frmMatrixCareLoginForm" id="frmMatrixCareLoginForm">
<input type="hidden" name="action" id="action" value="setupmatrixcare">
<table width="100%" cellpadding="3" cellspacing="3" class="table table-striped table-bordered table-hover">
	<tr>
		<td colspan="2"><h4 style="margin:0px;padding:0px">Setup MatrixCare Information:</h4></td>
	</tr>
	<?php
    	$error_msgs = array(
            "sucins"                =>  "Successfully Inserted",
            "failinstoken"          =>  "Failed to generate the token. Please check the details correct or not",
            "sucupd"                =>  "Successfully Updated",
            "failins"               =>  "Failed to insert",
            "failupd"               =>  "Failed to update",
            "failupdtoken"          =>  "Failed to generate the token. Please check the details correct or not",
            "sucdel"                =>  "Successfully Deleted",
            "sucdef"                =>  "Default Company Successfully Updated",
            "office_fail"           =>  "OfficeIDs failed to update",
            "office_succ"           =>  "OfficeIDs successfully updated",
            "def_office_succ_ins"   =>  "Default office id updated successfully",
            "def_office_succ_upd"   =>  "Default office id updated successfully",
            "office_fail"           =>  "Unable to update office ids",
            "office_succ"           =>  "Officeids updated successfully",
    	);
    ?>
    <tr>
        <td colspan="2" style="color:blue" id="log_message">
            <?php 
            if(isset($_GET['msg']) && $_GET['msg'] != "")
            {
                echo $error_msgs[$_GET['msg']];
            }
            else if($matrix_care_errors != "") {
            	echo "<span style='color:red'>".$matrix_care_errors."</span>";
            }
            ?>
        </td>
    </tr>
    <?php 
    if($matrix_api_companies_cnt > 0) {
        ?>
        <tr>
    		<td width="14%">Companies List</td>
    		<td>
                <select name="ddlCompaniesList" id="ddlCompaniesList" onchange="redirectToEdit('<?php echo $OrgID?>', this.value)">
                <option value="">Add New</option>
                <?php
                for($ca = 0; $ca < $matrix_api_companies_cnt; $ca++) {
    	           ?>
                    <option value="<?php echo $matrix_care_companies_list[$ca]['CompanyID'];?>" <?php if($_REQUEST['CompanyID'] == $matrix_care_companies_list[$ca]['CompanyID']) echo ' selected="selected"'; ?>>
                        <?php echo $matrix_care_companies_list[$ca]['CompanyName'];?>
                    </option>
                    <?php
                }
                ?>
                </select>
            </td>
         <tr>                                    
        <?php
    }
    ?>
	<tr>
		<td width="14%">Company Name <font color="red">*</font></td>
		<td>
			<input type="text" name="CompanyName" id="CompanyName" value="<?php echo $MatrixCareInfo['CompanyName'];?>" autocomplete="off">
		</td>
	</tr>
	
	<tr>
		<td width="10%">UserName <font color="red">*</font></td>
		<td>
			<input type="text" name="MatrixCareUserName" id="MatrixCareUserName" value="<?php echo $MatrixCareInfo['UserName'];?>" autocomplete="off">
		</td>
	</tr>
	<tr>
		<td width="10%">Password <font color="red">*</font></td>
		<td>
			<input type="password" name="MatrixCarePassword" id="MatrixCarePassword" value="<?php echo $MatrixCareInfo['Password'];?>" autocomplete="off">
		</td>
	</tr>
	<tr>
		<td width="10%">Tenant <font color="red">*</font></td>
		<td>
			<input type="text" name="MatrixCareTenant" id="MatrixCareTenant" value="<?php echo $MatrixCareInfo['Tenant'];?>" autocomplete="off">
		</td>
	</tr>
	<tr>
		<td width="10%">Upper Case</td>
		<td>
			<select name="ddlUpperCase" id="ddlUpperCase">
			     <option value="N" <?php if($MatrixCareInfo['UpperCase'] == "N") echo 'selected="selected"';?>>No</option>
                 <option value="Y" <?php if($MatrixCareInfo['UpperCase'] == "Y") echo 'selected="selected"';?>>Yes</option>
			</select>
		</td>
	</tr>
	<tr>
	   <td width="14%">Is Default:</td>
	   <td>
        <select name="ddlMatrixCareIsDefault" id="ddlMatrixCareIsDefault">
		     <option value="N" <?php if($MatrixCareInfo['CompanyIsDefault'] == "N") echo 'selected="selected"';?>>No</option>
             <option value="Y" <?php if($MatrixCareInfo['CompanyIsDefault'] == "Y") echo 'selected="selected"';?>>Yes</option>
        </select>
	   </td>
	</tr>
	<tr>
	   <td width="14%">Do you want to delete this?</td>
	   <td>
            <input type="checkbox" name="matrx_care_action" id="matrx_care_action" value="delete">
	   </td>
	</tr>
	<tr>
	   <td colspan="2">
	       <table class="table table-bordered">
           	<tr>
                <td>Office ID</td>
                <td>Name</td>
                <td>Default</td>
            </tr>
            <?php
            if(is_array($branches_info)) {
            	foreach ($branches_info as $branch_id=>$branch_name)
                {
                    $checked = '';
                    if($office_ids_info['DefaultValue'] == $branch_id) $checked = ' checked = "checked"';
                    ?>
                    <tr>
                        <td><?php echo $branch_id;?></td>
                        <td><?php echo $branch_name;?></td>
                        <td><input type="radio" name="matrix_care_office_id" value="<?php echo $branch_id;?>" onclick="updateMatrixCareOfficeID('<?php echo $OrgID;?>', '<?php echo $_REQUEST['CompanyID']?>', '<?php echo $branch_id;?>');" <?php echo $checked;?>></td>
                    </tr>
                    <?php
                }
            }
            ?>
	       </table>
	   </td>
	</tr>
	
	<tr>
		<td colspan="2">
			<input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" class="<?php if($BOOTSTRAP_SKIN == true) echo 'btn btn-primary'; else echo 'custombutton'?>">
		</td>
	</tr>
	</table>
</form>


<script>
function redirectToEdit(org_id, company_id) {
    var link = 'administration.php?action=setupmatrixcare&OrgID='+org_id+'&CompanyID='+company_id+'&matrx_care_action=edit';
    location.href = link;
}

function updateMatrixCareOfficeID(OrgID, CompanyID, OfficeID) {

	var request = $.ajax({
		method: "POST",
  		url: "applicants/updateMatrixCareDefaultOfficeID.php?OrgID="+OrgID+"&CompanyID="+CompanyID+"&OfficeID="+OfficeID,
		type: "POST",
		beforeSend: function() {
			$("#log_message").html('Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function() {
			$("#log_message").html('Updated successfully.');
    	}
	});
}

</script>
