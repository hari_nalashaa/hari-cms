<?php
include COMMON_DIR . 'process/FormFunctions.inc';

if ($process == 'NewUser' || $process == 'Users' || $UserIDedit || $adduser) { 
   echo include IRECRUIT_DIR . "administration/Processusers.inc";
}

if(!$adduser && !$UserID) {

	//set columns
	$columns	=	"UserID, FirstName, LastName, Role, getUserStatus(UserID) as Status, CostCenter, TwilioSms";
	//set where condition
	$where		=	array("OrgID = :OrgID");
	//set parameters
	$params		=	array(":OrgID"=>$OrgID);
	
	if(!isset($_GET['status'])) {
		$status = "1";
	}
	else {
		$status = $_GET['status'];
	}
	if($status == "1") {
	    $where[]            =   "getUserStatus(UserID) = '1'";
	}
	else if($status == "0") {
	    $where[]            =   "getUserStatus(UserID) = '0'";
	}
	
	//get users information
	$results = $IrecruitUsersObj->getUserInformation($columns, $where, "", array($params));

	$UserIDck = $USERID;

	if ($UserIDedit)
	{
		$UserIDck	=	$UserIDedit;
	}
    
	echo '<div class="table-responsive">';
	
	echo '<table border="0" cellspacing="0" cellpadding="5" class="table table-striped table-bordered table-hover">';

	echo '<tr><td colspan="100%"><a href="administration.php?action=useredit&useraction=profile&adduser=y"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;"><b style="FONT-SIZE:8pt;COLOR:#000000">Add</b></a></td></tr>';

	echo '<tr><td colspan="100%">';
   	echo '<b style="color:red;">Warning!</b><br>Please be aware that adding new user accounts for Administrators, Recruiters or Hiring Managers may affect your monthly billing.<br>If you have questions please call 1-800-517-9099.';
   	echo '</td></tr>';

   	echo '<tr><td colspan="100%">';
   	echo '<input type=\'radio\' name=\'Status\' id=\'Active\' value=\'1\' onclick=\'changeUsersStatus("1")\'';
   	
   	if($status == "1") {
		echo ' checked="checked"';
   	}
   	echo '>Active &nbsp;';
   	echo '<input type="radio" name="Status" id="Inactive" value="0" onclick=\'changeUsersStatus("0")\'';
   	if($status == "0") {
       	echo ' checked="checked"';
   	}
   	echo '>Inactive &nbsp;';
   	echo '<input type="radio" name="Status" id="All" value="" onclick=\'changeUsersStatus("")\'';
   	if($status == "") {
       	echo ' checked="checked"';
   	}
   	echo '>All &nbsp;';
   	echo '</td></tr>';
   
   	echo '<tr>';
   	echo '<td width="80" valign="bottom"><b>UserID</b></td>';
   	echo '<td width="140" valign="bottom"><b>Role</b></td>';
   	echo '<td width="410" valign="bottom"><b>Name</b></td>';
   	if ($OrgID == "I20120605") { // GOODWILL
     	echo '<td width="100" align="center" valign="bottom"><b>Cost Center</b></td>';
   	} // END GOODWILL
   	echo '<td align="center" width="40" valign="bottom"><b>Edit<br>User</b></td>';
   	echo '<td align="center" width="70" valign="bottom"><b>Edit<br>Permissions</b></td>';
   	echo '<td align="center" width="70" valign="bottom"><b>Status</b></td>';
   	echo '<td align="center" width="30" valign="bottom"><b>Delete</b></td>';
   	echo '</tr>';

   	$rowcolor="#eeeeee";
   
   	if(is_array($results['results'])) {
	   	foreach($results['results'] as $row) {
	   	
	   		$del_permission = ('<a href="administration.php?action=useredit&useraction=delete&UserIDedit=' . $row['UserID'] . '" onclick="return confirm(\'Are you sure you want to delete the following user?\n\nID: ' . $row['UserID'] . '\')"><img src="' . IRECRUIT_HOME . 'images/icons/cross.png" border="0" title="Delete"></a>');
	   	
	   		$type='<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit">';
	   		$edit_user = ('<a href="administration.php?action=useredit&useraction=profile&UserIDedit=' . $row['UserID'] . '">' . $type . '</a>');
	   	
	   		$type='<img src="' . IRECRUIT_HOME . 'images/icons/pencil.png" border="0" title="Edit">';
	   		$edit_permission = ('<a href="administration.php?action=useredit&useraction=permissions&UserIDedit=' . $row['UserID'] . '">' . $type . '</a>');
	   	
	   		echo '<tr bgcolor="' . $rowcolor . '">';
	   		echo '<td>' . $row['UserID'] . '</td>';
	   		echo '<td>' . DisplayRole($row['Role']) . '</td>';
	   		echo '<td>' . $row['FirstName'] . ' ' . $row['LastName'] . '</td>';
   			if ($OrgID == "I20120605") { // GOODWILL
   			  echo '<td align="center">' . $row['CostCenter'] . '</td>';
   			} // END GOODWILL
	   		echo '<td align="center">' . $edit_user . '</td>';
	   		echo '<td align="center">' . $edit_permission . '</td>';
	   		echo '<td align="center">';
	   		if($row['Status'] == '1') {
	   			echo "Active";
	   		}
	   		else if($row['Status'] == '0') {
	   		    echo "Inactive";
	   		}
	   		else {
	   			echo "N/A";
	   		}
	   		echo '</td>';
	   		
	   		echo '<td align="center">' . $del_permission . '</td>';
	   		
	   		echo '</tr>';
	   	
	   		if ($rowcolor == "#eeeeee") { $rowcolor="#ffffff"; } else { $rowcolor="#eeeeee"; }
	   	
	   	} // end foreach
   	}   

   	echo '</table>';
   
   	echo '</div>';
}
?>
<script>
function changeUsersStatus(status) {
    location.href = irecruit_home+"administration.php?action=useredit&status="+status;
}

function validateUserForm() {

	var twilio_sms_check	=	false;
	var frmObj 				=	document.forms['frmUserInfo'];

	if(typeof(frmObj.TwilioSms) != 'undefined') {
		twilio_sms_check	=	frmObj.TwilioSms.checked;
	}

	var users_cell			=	frmObj.UsersCell.value;
	
	if(twilio_sms_check == true) {
		var request = $.ajax({
			async: true,   // this will solve the problem
			method: "POST",
			url: irecruit_home + "applicants/getTwilioLookUpPhoneNumCheckInfo.php?cell_number="+users_cell,
			type: "POST",
			dataType: 'json',
			beforeSend: function() {
				$("#user_validate_processing_msg").html('&nbsp;Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
			},
			success: function(data) {
				if(data.sms_capable == "false") {
					$("#user_validate_processing_msg").html("Sorry this number doesn't support sms. Please enter the sms supported number.");
					return false;
				}
				else {
					return true;
				}
	    	}
		});
	}
	else {
		return true;
	}
}
</script>
