<?php
require_once '../Configuration.inc';

$FormID         =   "STANDARD";
//Get FormQuestions Information
$columns        =   "Question, QuestionID, QuestionTypeID";
$where_info     =   array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = :Active", "QuestionTypeID IN ('2', '3', '6', '8', '18')", "QuestionID != 'resumeupload'");
$params         =   array(":OrgID"=>$OrgID, ":FormID"=>$FormID, ":SectionID"=>$_REQUEST['SectionID'], ":Active"=>"Y");
$FormQuestions  =   G::Obj('FormQuestions')->getFormQuestionsInformation($columns, $where_info, "QuestionOrder", array($params));

$FormQuestionsCount    =   $FormQuestions['count'];
$FQ                    =   $FormQuestions['results'];
for($r = 0; $r < $FormQuestionsCount; $r++) {
    $select_options[] = array($FQ[$r]['QuestionID'], $FQ[$r]['QuestionID'], $FQ[$r]['QuestionTypeID'], $FQ[$r]['Question']);
}

echo json_encode($select_options);
?>