<?php
if(isset($_REQUEST['btnEmail']) && $_REQUEST['btnEmail'] == "Submit") {
	//To insert common email message data
	$UserPortalEmailTemplateObj->insUserPortalEmailTemplate($OrgID, $_REQUEST['Subject'], $_REQUEST['EmailMessage']);
	
	header("Location:administration.php?action=userportalemailtemplate&menu=8&msg=suc");
	exit;
}

//Get email message 
$email_template_info = $UserPortalEmailTemplateObj->getUserPortalEmailTemplate($OrgID);

if($email_template_info['Subject'] == "") {
	$email_template_info['Subject'] = 'Pending application is about to expire';
}
if($email_template_info['EmailMessage'] == "") {
	$email_template_info['EmailMessage']  = "Dear {last} {first},"."<br><br>";

        $email_template_info['EmailMessage'] .= "Please complete the application for this position {JobTitle}, on or before {ExpiryDate}.<br>";
        $email_template_info['EmailMessage'] .= "<br>{PortalURL}<br>";


	$email_template_info['EmailMessage'] .= '<br>Regards,<br>Hiring Manager.';
}

if(isset($_GET['msg']) && $_GET['msg'] != "") {
	echo '<span style="color:blue">Successfully Updated.</span>'; 
}
?>
<form name="frmIconnectEmailTemplate" id="frmIconnectEmailTemplate" method="post">
	Template Tags: {first}, {last}, {JobTitle}, {ExpiryDate}, {PortalURL}, {OrganizationName}
	<br><br>
	Subject: <input type="text" name="Subject" id="Subject" class="form-control" style="max-width: 500px" maxlength="255" value="<?php echo $email_template_info['Subject'];?>"><br>
	Email Message:
	<textarea rows="30" cols="30" class="mceEditor" name="EmailMessage" id="EmailMessage"><?php echo $email_template_info['EmailMessage'];?></textarea>
	<br>
	<input type="hidden" name="action" id="action" value="userportalemailtemplate">
	<input type="submit" class="btn btn-primary" name="btnEmail" id="btnEmail" value="Submit">
</form>
