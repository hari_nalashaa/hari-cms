<?php
include COMMON_DIR . 'application/CountrySelect.inc';

global $USERID;

//Set where condition
$where = array("UserID = :UserID");
//Set parameters
$params = array(":UserID"=>$USERID);
//Get UserInformation
$results = $IrecruitUsersObj->getUserInformation("OrgID", $where, "", array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$ORGID = $row ['OrgID'];
	}
}


if ($_REQUEST["process"] == "Y") {
	
	//Set where condition
	$where = array("OrgID = :OrgID");
	//Set parameters
	$params = array(":OrgID"=>$ORGID);
	//Delete Internationalization
	$AddressObj->delInternationalizationInfo($where, array($params));
	
	if (isset($_POST ["allow"]) && $_POST ["allow"] != "") {
		
		$country = is_null($_POST ["country"]) ? '' : $_POST ["country"];
		
		//Insert Internationalization Information
		$info = array("OrgID"=>$OrgID, "Allow"=>$_POST ['allow'], "DefaultCountry"=>$country);
		$AddressObj->insInternationalizationInfo($info);
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('International configured!')";
		echo '</script>';
	}
}

//Get Internationalization Information
$row = $AddressObj->getInternationalizationInfo($ORGID);
$Allow = $row ['Allow'];
$DefaultCountry = $row['DefaultCountry'];

if (!isset($DefaultCountry) || is_null($DefaultCountry) || $DefaultCountry == "") {
	$DefaultCountry = "US";
}

if ($Allow == "Y") {
	
	$international = <<<END
Default Country: <select size="1" name="country" onchange="submit()">
END;
	
	//Get Countries
	$results = $AddressObj->getCountries();
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
			$international .= "<option value=\"" . $row ['Abbr'] . "\"";
			if ($DefaultCountry == $row ['Abbr']) {
				$international .= " selected";
			}
			$international .= ">" . $row ['Description'] ."</option>";
		}
	}
	
	
	$international .= <<<END
</select>
END;
} // end allow = y

?>
<div class="table-responsive">
	<table width="770" cellspacing="3" cellpadding="5"
		class="table table-bordered">
		<tr>
			<td>
				<form method="post" action="administration.php">
					Use International Addresses:&nbsp;&nbsp;<select name="allow"
						onChange="submit()">
						<option value="N"
							<?php if ($Allow == "N") { echo ' selected'; } ?>>No
						
						<option value="Y"
							<?php if ($Allow == "Y") { echo ' selected'; } ?>>Yes
					
					</select><br>
					<br>
					<?php echo $international?>
					<input type="hidden" name="action" value="international"> 
					<input type="hidden" name="process" value="Y">
				</form>
			</td>
		</tr>
	</table>
</div>