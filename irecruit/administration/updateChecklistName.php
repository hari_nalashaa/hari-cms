<?php
include '../Configuration.inc';

if ($_POST['ChecklistID'] != "" && $_POST['ChecklistName'] != "") {
    //Update Checklist Name    
    $ChecklistID    =   $_POST['ChecklistID'];
    $ChecklistName    =   $_POST['ChecklistName'];
    $now            = date('Y-m-d H:i:s');
    $set_info       =   array("ChecklistName = :ChecklistName","LastUpdated = :LastUpdated");
    $where_info     =   array("ChecklistID = :ChecklistID");
    $params         =   array(":ChecklistName"=>$ChecklistName, ":LastUpdated"=>$now, ":ChecklistID"=>$ChecklistID);
    //Update Status Category Information
    G::Obj('Checklist')->updChecklist($set_info, $where_info, array($params));
}
