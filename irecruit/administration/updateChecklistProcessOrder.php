<?php
include '../Configuration.inc';

if ($_POST['ProcessOrder'] != "") {
    //Update Checklist Name    
    $ProcessOrder    =   $_POST['ProcessOrder'];
    $get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);
    if($get_checklist_process_data[ProcessOrder]){
        $set_info       =   array("ProcessOrder = :ProcessOrder");
        $where_info     =   array("OrgID = :OrgID");
        $params         =   array(":ProcessOrder"=>$ProcessOrder, ":OrgID"=>$OrgID);
        //Update Status Category Information
        G::Obj('Checklist')->updChecklistProcessOrder($set_info, $where_info, array($params));
    }else{
        G::Obj('Checklist')->insChecklistProcessOrder($OrgID, $ProcessOrder);
    }
    
    
}