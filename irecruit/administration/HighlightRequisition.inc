<?php 
    $new_highlight_requsitions = $HighlightRequisitionsObj->getHighlightRequisitionIcons("New");
    $new_highlight_requsitions_results = $new_highlight_requsitions['results'];
    $new_highlight_requsitions_count = $new_highlight_requsitions['count'];
    
    $hot_highlight_requsitions = $HighlightRequisitionsObj->getHighlightRequisitionIcons("Hot");
    $hot_highlight_requsitions_results = $hot_highlight_requsitions['results'];
    $hot_highlight_requsitions_count = $hot_highlight_requsitions['count'];
    
    if(isset($_REQUEST['btnSubmit']) && $_REQUEST['btnSubmit'] == "Submit") {
        $new_job_icon = isset($_POST['new_job_icon']) ? $_POST['new_job_icon'] : '';
        $hot_job_icon = isset($_POST['hot_job_icon']) ? $_POST['hot_job_icon'] : '';
        
        if($hot_job_icon == "No") {
            $highlight_hot_job  =   "No";
            $hot_job_icon       =   "";
        }
        else if($hot_job_icon != "No") {
            $highlight_hot_job  =   "Yes";
        }
        
        if($new_job_icon == "No") {
            $highlight_new_job  =   "No";
            $new_job_icon       =   "";
        }
        else if($new_job_icon != "No") {
            $highlight_new_job  =   "Yes";
        }
        
        //Insert or update highlight settings
        $HighlightRequisitionsObj->insUpdHighlightRequisitionSettings($OrgID, $highlight_hot_job, $highlight_new_job, $_REQUEST['ddlNewJobDays'], $new_job_icon, $hot_job_icon);
        
        echo '<script>location.href="administration.php?action=highlight&menu=8";</script>';
    }
    
    $highlight_req_settings = $HighlightRequisitionsObj->getHighlightRequisitionSettings($OrgID);
?>
<form name="frmHighlightRequisition" id="frmHighlightRequisition" method="post" enctype="multipart/form-data">
    
    <strong>Note:*</strong> All New job status will be overwritten by Hot job.<br><br>
    
    <div id="newjobdays">
        Select for how many days requisition should be displayed as New Requisition: 
        <select name="ddlNewJobDays" id="ddlNewJobDays">
            <?php 
                for($i = 1; $i < 15; $i++) {
                    $selected = ($highlight_req_settings['NewJobDays'] == $i) ? ' selected="selected"' : '';
                	?><option value="<?php echo $i;?>" <?php echo $selected;?>><?php echo $i;?></option><?php
                }
            ?>
        </select>
    </div>
    <br><br>

    <div class="row">
        
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tr>
                    <td align="center" width="5%">Off</td>
                    <td align="left"><input type="radio" name="new_job_icon" value="No"></td>
                </tr>
                <?php 
                if($new_highlight_requsitions_count > 0) {
                
                    for($j = 0; $j < $new_highlight_requsitions_count; $j++) {
                        ?>
                    	<tr>
                    	   <td align="left" width="5%">
                    	       <img alt="" src="<?php echo IRECRUIT_HOME . "/vault/highlight/".$new_highlight_requsitions_results[$j]['JobIcon']?>">
                    	   </td>
                    	   <td align="left">
                    	       <input type="radio" name="new_job_icon" value="<?php echo $new_highlight_requsitions_results[$j]['IconID'];?>" <?php if($highlight_req_settings['NewJobIcon'] == $new_highlight_requsitions_results[$j]['IconID']) echo ' checked="checked"';?>>
                    	   </td>
                    	</tr>
                    	<?php
                    }
            
                }
                ?>
            </table>
        </div>
        
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tr>
                    <td align="center" width="5%">Off</td>
                    <td align="left"><input type="radio" name="hot_job_icon" value="No"></td>
                </tr>
                <?php 
                if($hot_highlight_requsitions_count > 0) {
                
                    for($j = 0; $j < $hot_highlight_requsitions_count; $j++) {
                        ?>
                    	<tr>
                    	   <td align="left" width="5%"><img alt="" src="<?php echo IRECRUIT_HOME . "/vault/highlight/".$hot_highlight_requsitions_results[$j]['JobIcon']?>"></td>
                    	   <td align="left">
                    	       <input type="radio" name="hot_job_icon" value="<?php echo $hot_highlight_requsitions_results[$j]['IconID'];?>" <?php if($highlight_req_settings['HotJobIcon'] == $hot_highlight_requsitions_results[$j]['IconID']) echo ' checked="checked"';?>>
                    	   </td>
                    	</tr>
                    	<?php
                    }
            
                }
                ?>
            </table>
        </div>
        
    </div>
    <br><br>
    <input type="submit" class="btn btn-primary" name="btnSubmit" id="btnSubmit" value="Submit"> 
</form>   
