<?php
$columns		=	"UserID, OrgID, Role, FirstName, LastName, EmailAddress, Cell, TwilioSms";
$where_info		=	array("OrgID = :OrgID");
$params_info	=	array(":OrgID"=>$OrgID);
$users_results	=	G::Obj('IrecruitUsers')->getUserInformation($columns, $where_info, "", array($params_info));
$users_list		=	$users_results['results'];

$twilio_account_info	=	G::Obj('TwilioAccounts')->getTwilioAccountInfo($OrgID);
?>
<div class="table-responsive">
	<form name="frmTwilioSmsCapableUsers" id="frmTwilioSmsCapableUsers" method="post">
	<table width="770" cellspacing="3" cellpadding="5" class="table table-bordered">
		<tr>
			<td><span id="processing_message" style="color:blue"></span></td>
		</tr>
	</table>
	<br>
	<table width="770" cellspacing="3" cellpadding="5" class="table table-bordered">
		<tr>
			<th>User ID</th>
			<th>Name</th>
			<th>Email</th>
			<th>Cell</th>
			<th>Twilio Sms</th>
			<th>Update Info</th>
		</tr>
		
		<?php 
		$sms_capable_users_count = 0;
		for($ul = 0; $ul < count($users_list); $ul++) {
			if($users_list[$ul]['TwilioSms'] == "Y") {
				$sms_capable_users_count++;
			}
		}
		
		for($u = 0; $u < count($users_list); $u++) {
			$twilio_sms	=	$users_list[$u]['TwilioSms'];
			$user_id	=	$users_list[$u]['UserID'];
			$cell		=	$users_list[$u]['Cell'];
			?>
			<tr>
				<td><?php echo $user_id;?></td>
				<td><?php echo $users_list[$u]['FirstName'] . ' ' . $users_list[$u]['LastName'];?></td>
				<td><?php echo $users_list[$u]['EmailAddress'];?></td>
				<td><input type="text" name="txtUserCell-<?php echo $user_id;?>" id="txtUserCell-<?php echo $user_id;?>" value="<?php echo $cell;?>"></td>
				<td>
				<?php
					$checked = '';
					if($twilio_sms == "Y") $checked = ' checked="checked"';
				?>
				<input type="checkbox" name="chkTwilioSms-<?php echo $user_id;?>" id="chkTwilioSms-<?php echo $user_id;?>" <?php echo $checked;?>>
				</td>
				<td>
				<input type="button" name="btnUpdateInfo-<?php echo $user_id;?>" id="btnUpdateInfo-<?php echo $user_id;?>" class="btn btn-primary btn-small" onclick="updateUserTwilioSmsStatus('<?php echo $user_id;?>');" value="Update">
				</td>
			</tr>
			<?php
		}
		?>
	</table>
	</form>
</div>
<script type="text/javascript">
function updateUserTwilioSmsStatus(UserID) {

	var frmObj			=	document.forms['frmTwilioSmsCapableUsers'];
	var users_cell		=	document.getElementById('txtUserCell-'+UserID).value;
	var TwilioSmsCheck	=	document.getElementById('chkTwilioSms-'+UserID).checked;
	var TwilioSmsStatus	=	"N";
	var message			=	"";
	
	if(TwilioSmsCheck == true) {
		TwilioSmsStatus =	"Y";
	}
	else if(TwilioSmsCheck == false) {
		TwilioSmsStatus =	"N";
	}

	var request = $.ajax({
		async: true,   // this will solve the problem
		method: "POST",
		url: irecruit_home + "administration/updateUserTwilioSmsStatus.php?UserID="+UserID+"&TwilioSmsStatus="+TwilioSmsStatus+"&UsersCell="+users_cell,
		type: "POST",
		beforeSend: function() {
			$("#processing_message").html('&nbsp;Please wait.. <img src="'+irecruit_home+'images/icons/loading-small.gif"/><br><br>');
		},
		success: function(data) {
			$("#processing_message").html(data);
    	}
	});
}
</script>
