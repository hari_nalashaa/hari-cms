<?php
if ($_REQUEST['submit'] == "Update Color Values") {
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"MultiOrgID = :MultiOrgID" 
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":MultiOrgID" => $MultiOrgID 
	);
	// Delete OrganizationColors
	$OrganizationsObj->delOrganizationInfo ( 'OrganizationColors', $where, array (
			$params 
	) );
	
	//Insert Organization Colors Information
	$info = array (
			"OrgID" => $OrgID,
			"MultiOrgID" => $MultiOrgID,
			"Heavy" => $_POST ['hcolor'],
			"Medium" => $_POST ['mcolor'],
			"Light" => $_POST ['lcolor'],
			"LightLight" => $_POST ['llcolor'],
			"ErrorHighlight" => $_POST ['ehlcolor'] 
	);
	$OrganizationsObj->insOrganizationInfo ( 'OrganizationColors', $info );
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Organization Colors have been configured!')";
	echo '</script>';
}

if ($_REQUEST['submit'] == "Reset to Default") {
	// Set where condition
	$where = array (
			"OrgID = :OrgID",
			"MultiOrgID = :MultiOrgID" 
	);
	// Set parameters
	$params = array (
			":OrgID" => $OrgID,
			":MultiOrgID" => $MultiOrgID 
	);
	// Delete OrganizationColors
	$OrganizationsObj->delOrganizationInfo ( 'OrganizationColors', $where, array (
			$params 
	) );
}


// Set where condition
$where = array (
		"OrgID = :OrgID",
		"MultiOrgID = :MultiOrgID"
);
// Set parameters
$params = array (
		":OrgID" => $OrgID,
		":MultiOrgID" => $MultiOrgID
);
//Get Organization Colors Information
$results = $OrganizationsObj->getOrganizationColorsInfo("*", $where, '', array($params));
$color = $results['results'][0];

if (! $color) {
	// Set where condition
	$where = array (
			"OrgID = 'DEFAULT'",
	);
	//Get Organization Colors Information
	$results = $OrganizationsObj->getOrganizationColorsInfo("*", $where, '', array());
	$color = $results['results'][0];
}

?>
<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" width="100%"
		class="table table-striped table-bordered">
		<form method="post" action="">
			<tr>
				<td colspan="100%">Use this page to set the colors in Hexadecimal
					for your User Portal and Public pages.</td>
			</tr>
			<tr>
				<td align="right">Heavy Color: #</td>
				<td><input class="color" name="hcolor" value="<?php echo $color['Heavy']?>"
					size="6"></td>
				<td width="400"></td>
			</tr>
		<tr>
			<td align="right">Medium Color: #</td>
			<td><input class="color" name="mcolor" value="<?php echo $color['Medium']?>"
				size="6"></td>
			<td></td>
		</tr>
		<tr>
			<td align="right">Light Color: #</td>
			<td><input class="color" name="lcolor" value="<?php echo $color['Light']?>"
				size="6"></td>
			<td></td>
		</tr>
		<tr>
			<td align="right">Light Light Color: #</td>
			<td><input class="color" name="llcolor"
				value="<?php echo $color['LightLight']?>" size="6"></td>
			<td></td>
		</tr>
		<tr>
			<td align="right">Error Highlight Color: #</td>
			<td><input class="color" name="ehlcolor"
				value="<?php echo $color['ErrorHighlight']?>" size="6"></td>
			<td></td>
		</tr>
		<input type="hidden" name="action" value="orgcolors">
		<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>">
		<tr>
			<td colspan="100%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="submit" class="btn btn-primary" value="Update Color Values">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="submit" value="Reset to Default" class="btn btn-primary">
			</td>
		</tr>
		</form>
	</table>
</div>