<?php
include '../Configuration.inc';

if ($_POST['getVal'] != "") {
    //Update Checklist Name    
    $displayUserportal    =   $_POST['getVal'];
    $get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);
    if($get_checklist_process_data[OrgID]){
        $set_info       =   array("DisplayOnUserPortal = :DisplayOnUserPortal");
        $where_info     =   array("OrgID = :OrgID");
        $params         =   array(":DisplayOnUserPortal"=>$displayUserportal, ":OrgID"=>$OrgID);
        //Update Status Category Information
        G::Obj('Checklist')->updChecklistProcessOrder($set_info, $where_info, array($params));
    }else{
        G::Obj('Checklist')->insChecklistDisplayOnUserPortal($OrgID, $displayUserportal);
    }
    
    
}