<?php
$formscript =   "administration.php";
$formtable  =   "OnboardQuestions";
$form       =   $_REQUEST['OnboardSource'];
$action     =   'onboardquestions'; //'formquestions';
$section    =   1;

if ($_REQUEST['unlock'] != "") {
    //Set where condition
    $where      =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
    //Set parameters
    $params     =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$form, ":QuestionID"=>$_REQUEST['unlock']);
    //Set information
    $set_info   =   array("SageLock = ''");
    $FormQuestionsObj->updQuestionsInfo($formtable, $set_info, $where, array($params));
}

//Set where condition
$where      =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID");
//Set parameters
$params     =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$form);
//Get Questions Information
$results    =   $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', "*", $where, "", array($params));

if($results['count'] == 0) {
	
    //FormID
    $FormID     =   $feature ['OnboardSetting'];
    //Set Columns
    $columns    =   "'$OrgID', 'STANDARD', QuestionID, ChildQuestionsInfo, Question, QuestionTypeID, size, maxlength, rows, cols, wrap, value, defaultValue, Active, QuestionOrder, Required, Validate, SageLock";
    //Set where condition
    $where      =   array("OrgID = 'MASTER'", "OnboardFormID = :OnboardFormID");
    //Set parameters
    $params     =   array(":OnboardFormID"=>$FormID);
    
    $results_copy = $FormQuestionsObj->getQuestionsInformation('OnboardQuestions', "Question", $where, "", array($params));
	
	if($results_copy['count'] == 0) {
		//Insert OnboardQuestions
		$FormQuestionsObj->insFormQuestionsMaster('OnboardQuestions', $columns, $where, array($params));
	}	
		
} // end if OnboardQuestions


// Update Custom Question Information
if (isset ( $action ) && $action == 'onboardquestions') {
    if (isset ( $process ) && $process == 'Y') {
        foreach ( $_REQUEST as $custqkey => $custqval ) {
            if (strstr ( $custqkey, '-questiontypeid' )) {
                $cqtypeinfo = explode ( "-", $custqkey );
                $cquestionid = $cqtypeinfo [0];

                if (substr ( $cquestionid, 0, 4 ) == "CUST") {
                    if (isset ( $_REQUEST [$cquestionid . 'Question'] ) && $_REQUEST [$cquestionid . 'Question'] != "") {

                        // Get question type information based on QuestionTypeID
                        $que_type_def_val = G::Obj('QuestionTypes')->getQuestionTypeInfo ( $_REQUEST [$custqkey] );

                        if ($formtable == "OnboardQuestions") {
                            // List of parameters to bind the query
                            $params     =   array (
                                                ':Question'        =>  $_REQUEST [$cquestionid . 'Question'],
                                                ':QuestionTypeID'  =>  $_REQUEST [$custqkey],
                                                ':OrgID'           =>  $OrgID,
                                                ':OnboardFormID'   =>  $_REQUEST ['form'],
                                                ':QuestionID'      =>  $cquestionid
                                            );
                            // Set update information
                            $set_info   =   array (
                                                "Question          =   :Question",
                                                "QuestionTypeID    =   :QuestionTypeID"
                                            );
                            //If question is text area add rows and cols while updating
                            if($_REQUEST [$custqkey] != 100 && $_REQUEST [$custqkey] != 120) {
                                //Set parameters
                                $params[':rows']        =   $que_type_def_val['rows'];
                                $params[':cols']        =   $que_type_def_val['cols'];
                                $params[':size']        =   $que_type_def_val['size'];
                                $params[':maxlength']   =   $que_type_def_val['maxlength'];
                                //Set the default parameters
                                $set_info[]             =   'rows = :rows';
                                $set_info[]             =   'cols = :cols';
                                $set_info[]             =   'size = :size';
                                $set_info[]             =   'maxlength = :maxlength';
                            }
                            // Set where condition
                            $where = array (
                                "OrgID             =   :OrgID",
                                "OnboardFormID     =   :OnboardFormID",
                                "QuestionID        =   :QuestionID"
                            );
                            //Update Onboard Questions Information
                            $res_question = $FormQuestionsObj->updQuestionsInfo ( $formtable, $set_info, $where, array (
                                $params
                            ) );
                        }
                    }
                }
            }
        }
    }
}


if (isset ( $_REQUEST ['process'] ) && $_REQUEST ['process'] == 'Y') {
    	
    foreach ( $_POST as $key => $value ) {

        if ($value == 'D') {

            //Set where condition
            $where_info =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
            // Bind QuestionID
            $params     =   array (':OrgID'=>$OrgID, ':OnboardFormID'=>$form, ':QuestionID'=>$key);
            // Set the parameters those are going to update
            $set_info   =   array("Active = ''", "Required = ''");
            // Update the table information based on bind and set values
            $OnboardQuestionsObj->updateOnboardQuestionsInfo($set_info, $where_info, array ($params) );
        }

        if ($value == 'Y') {
            	
            $pieces = explode ( "-", $key );
            	
            if ($pieces [1] == 'A') {

                //Set where condition
                $where_info =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
                // Bind QuestionID
                $params     =   array (':OrgID'=>$OrgID, ':OnboardFormID'=>$form, ':QuestionID'=>$pieces [0]);
                // Set the parameters those are going to update
                $set_info   =   array("Active = 'Y'");
                // Update the table information based on bind and set values
                G::Obj('OnboardQuestions')->updateOnboardQuestionsInfo($set_info, $where_info, array ($params) );
                
            }
            else if ($pieces [2] == 'A') {
                
                //Set where condition
                $where_info =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
                // Bind QuestionID
                $params     =   array (':OrgID'=>$OrgID, ':OnboardFormID'=>$form, ':QuestionID'=>$pieces [0]."-".$pieces [1]);
                // Set the parameters those are going to update
                $set_info   =   array("Active = 'Y'");
                // Update the table information based on bind and set values
                G::Obj('OnboardQuestions')->updateOnboardQuestionsInfo($set_info, $where_info, array ($params) );
                
            }
            else if ($pieces [1] == 'R') {

                //Set where condition
                $where_info =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
                // Bind QuestionID
                $params     =   array (':OrgID'=>$OrgID, ':OnboardFormID'=>$form, ':QuestionID'=>$pieces [0]);
                // Set the parameters those are going to update
                $set_info   =   array("Required = 'Y'");
                // Update the table information based on bind and set values
                G::Obj('OnboardQuestions')->updateOnboardQuestionsInfo($set_info, $where_info, array ($params) );
                
            }
        }
    }
    	
    echo '<script language="JavaScript" type="text/javascript">';
    echo "alert('Form Questions have been completed!')";
    echo '</script>';
}

require_once IRECRUIT_VIEWS . 'onboard/EditOnboardForm.inc';
?>
