<?php
echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';

if ($process == "Y") {
	
	$ERROR = "";
	
	if (($_REQUEST['EIN1']) && (strlen ( $_REQUEST['EIN1'] ) < 2)) {
		$ERROR .= "Please enter a Employer Identification Tax Number." . "\\n";
	}
	
	if (($_REQUEST['StateTax']) && (strlen ( $_REQUEST['StateTax'] ) <= 3)) {
		$ERROR .= "Please enter a State Tax Number." . "\\n";
	}
	
	if ($ERROR) {
		
		$MSG = "The following errors have occured:\\n\\n" . $ERROR . "\\n\\n";
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $MSG . "')";
		echo '</script>';
	} else {
		//Organization Tax Numbers Information
		$ins_org_tax_num = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "EIN1"=>$_REQUEST['EIN1'], "EIN2"=>$_REQUEST['EIN2'], "StateTax"=>$_REQUEST['StateTax']);
		//Update Information
		$update_info = array(":UEIN1"=>$_REQUEST['EIN1'], ":UEIN2"=>$_REQUEST['EIN2'], ":UStateTax"=>$_REQUEST['StateTax']);
		//Update on Duplicate
		$on_update = " ON DUPLICATE KEY UPDATE EIN1 = :UEIN1, EIN2 = :UEIN2, StateTax = :UStateTax";
		//Organization Tax Numbers
		$OrganizationsObj->insOrganizationInfo('OrganizationTaxNumbers', $ins_org_tax_num, $on_update, $update_info);
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('Organization Tax Numbers configured!')";
		echo '</script>';
	} // end else/error
} // end process

//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
$results = $OrganizationsObj->getOrganizationTaxNumbers("*", $where, 'OrgID LIMIT 0, 1', array($params));
$OTN = $results['results'][0];
?>

<br>
<form action=administration.php method="post">
	<tr>
		<td align="right">Employer Identification Tax Number:</td>
		<td><input type="text" name="EIN1" value="<?php echo $OTN['EIN1']?>" size="2"
			maxlength="5">-<input type="text" name="EIN2"
			value="<?php echo $OTN['EIN2']?>" size="8" maxlength="20"></td>
	</tr>

	<tr>
		<td align="right">State Tax Number:</td>
		<td><input type="text" size="30" maxlength="45" name="StateTax"
			value="<?php echo $OTN['StateTax'] ?>"></td>
	</tr>

	<tr>
		<td colspan="100%" height="60" valign="bottom" align="center">
			<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID ?>"> 
			<input type="hidden" name="action" value="orgtaxnumbers"> 
			<input type="hidden" name="process" value="Y"> 
			<input type="submit" value="Update Tax Numbers" class="btn btn-primary">
		</td>
	</tr>
	</table>
	</div>
</form>