<?php
if(isset($_POST) && $_POST['btnSubmit'] == "Submit") {
    
    G::Obj('InternalRequisitionsSettings')->insInternalRequisitionsSettings($OrgID, $_POST);
    
    echo "<script>";
    echo "location.href='administration.php?action=internalreqtextques";
    echo "&OrgID=".$_REQUEST['OrgID'];
    if($_REQUEST['MultiOrgID'] != "") {
        echo "&MultiOrgID=".$_REQUEST['MultiOrgID'];
    }
    echo "&msg=suc';";
    echo "</script>";
}

$inernal_req_settings = G::Obj('InternalRequisitionsSettings')->getInternalRequisitionsSettingsInfo($OrgID);

if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
    echo '<span style="color:blue">Successfully updated.</span><br><br>';
}
?>
<form name="frmInternalReqText" id="frmInternalReqText" method="post">
	<table class="table table-bordered">
		<tr>
			<td>Definition of Internal Requisitions:</td>
			<td>
				<input type="text" name="InternalCode" value="<?php echo $inernal_req_settings['InternalCode'];?>" size="10" maxlength="15">
			</td>
		</tr>
		<tr>
			<td>Instruction:</td>
			<td>
				<textarea name="InternalRequisitionsText" id="InternalRequisitionsText" class="mceEditor" rows="5" cols="60"><?php echo $inernal_req_settings['InternalRequisitionsText'];?></textarea>
			</td>
		</tr>
		<tr>
			<td>Question:</td>
			<td>
				<input type="text" name="Question" id="Question" size="60" class="form-control width-auto-inline" value="<?php echo $inernal_req_settings['Question'];?>">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit">	
			</td>
		</tr>
	</table>
</form