<?php
if ($_POST ["process"] == "Y") {
	
	//Set where condition
	$where_info = array("OrgID = :OrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID);
	//Delete ActiveMinutes Information
	$RequestObj->delActiveMinutesInfo($where_info, array($params));
	
	if (isset($_POST ["minutes"]) && $_POST ["minutes"] != "") {
		
		//Set ActiveMinutes Information
		$info = array("OrgID"=>$OrgID, "Minutes"=>$_POST ["minutes"]);
		$RequestObj->insActiveMinutesInfo($info);
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('Active Minutes configured!')";
		echo '</script>';
	} // end if
} // end process


//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Active Minutes Information
$results = $RequestObj->getActiveMinutesInfo("Minutes", $where, "", array($params));

if(is_array($results['results'])) {
	foreach($results['results'] as $row) {
		$Allow = $row ['Minutes'];
	}
}
?>
<div class="table-responsive">
	<br>
	<table width="770" cellspacing="3" cellpadding="5"
		class="table table-bordered">
		<form method="post" action="administration.php">
			Set Active Minutes:&nbsp;&nbsp;<select name="minutes"
				onchange="submit()">
				<option value="5" <?php if ($Allow == "5") { echo ' selected'; } ?>>5</option>
				<option value="10" <?php if ($Allow == "10") { echo ' selected'; } ?>>10</option>
				<option value="20" <?php if ($Allow == "20") { echo ' selected'; } ?>>20</option>
				<option value="30" <?php if ($Allow == "30") { echo ' selected'; } ?>>30</option>
				<option value="60" <?php if ($Allow == "60") { echo ' selected'; } ?>>60</option>
				<option value="120" <?php if ($Allow == "120") { echo ' selected'; } ?>>120</option>
			</select>
			<br>
			<br>
			<input type="hidden" name="action" value="activeminutes"> 
			<input type="hidden" name="process" value="Y">
		</form>
		</td>
		</tr>
	</table>
</div>