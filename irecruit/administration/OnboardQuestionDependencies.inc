<?php
if(isset($_POST['country']) && count($_POST['country']) > 0) {
	foreach($_POST['country'] as $QuestionID=>$ChildQuestionInformation) {
		$child_questions_info[$QuestionID] =   $ChildQuestionInformation;		
	}
	
	$child_questions_json_info =   json_encode($child_questions_info, true);

	//Update Child Questions Information
	$set_info     =   array("ChildQuestionsInfo = :ChildQuestionsInfo");
	$where_info   =   array("OrgID = :OrgID", "OnboardFormID = :OnboardFormID", "QuestionID = :QuestionID");
	$params_info  =   array(":OrgID"=>$OrgID, ":OnboardFormID"=>$_REQUEST["FormID"], ":QuestionID"=>"country", ":ChildQuestionsInfo"=>$child_questions_json_info);
	$FormQuestionsObj->updQuestionsInfo("OnboardQuestions", $set_info, $where_info, array($params_info));
}

$country_results    =   $AddressObj->getCountries();
$countries          =   $country_results['results'];

$countries_list     =   array();
for($c = 0; $c < count($countries); $c++) {
    $countries_list[$countries[$c]['Abbr']] =   $countries[$c]['Description'];
}
?>
<div class="table-responsive">
    <form name="frmFormQuestionsList" id="frmFormQuestionsList" method="get">
	<table cellspacing="3" cellpadding="5" class="table table-bordered">
	   <?php
	   $FormIDs    =   $OnboardQuestionsObj->getOnboardFormIDs($OrgID);
	   ?>  
       <tr>
	       <td colspan="5">
	           <strong>Configure Country Onboard Question Dependencies:</strong>
           </td>
	   </tr>
       <tr>
           <td><strong>FormID</strong></td>
	       <td colspan="4">
            <select name="FormID" id="FormID" onchange="getFormIDChildQuestionsInfo(this.value)">
            <option value="">Select</option>
            <?php
            foreach ($FormIDs as $OnboardFormID) {
               if($OnboardFormID != "Exporter" && $feature[$OnboardFormID] == "Y") {
                    ?>
                   <option value="<?php echo $OnboardFormID;?>" <?php if ($OnboardFormID == $_REQUEST["FormID"]) echo 'selected="selected"'?>><?php echo $OnboardFormID;?></option>
                   <?php
               }
               else if($OnboardFormID == "Exporter" && $feature["DataManagerType"] != "N") {
               	    ?>
                   <option value="<?php echo $OnboardFormID;?>" <?php if ($OnboardFormID == $_REQUEST["FormID"]) echo 'selected="selected"'?>><?php echo $OnboardFormID;?></option>
                   <?php
               }
            }
            ?>
            </select>
           </td>
	   </tr>
	   </table>
        <input type="hidden" name="action" value="onboardquestiondependencies">
	   </form>
	   
       <form name="frmQueDependencies" id="frmQueDependencies" method="post">
	   <table cellspacing="3" cellpadding="5" class="table table-bordered">
       <?php
        if($_REQUEST["FormID"] != '') {
               
               $show_hidden_options    =   array("show", "hidden");
    	       $personal_ques_info     =   $OnboardQuestionsObj->getOnboardPersonalQuestionsInfo($OrgID, $_REQUEST["FormID"]);
    	       
    	       if($personal_ques_info != "") {
                    $child_ques_info    =   json_decode($personal_ques_info['country']['ChildQuestionsInfo'], true);
                    
                    foreach($child_ques_info as $country_code=>$dependency_que_info) {
                        
                        echo '<tr>';
                        
                        if($country_code != "Other") {
                            echo '<td>';
                            echo "<strong>".$countries_list[$country_code]."</strong>: ";
                            echo '</td>';
                            
                            echo '<td>';
                        	echo "&nbsp;State&nbsp;";
                        	echo '<select name="country['.$country_code.'][state]">';
                        	foreach($show_hidden_options as $sh_val) {
                        	    $state_selected    =   ($child_ques_info[$country_code]['state'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$state_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;Province&nbsp;";
                        	echo '<select name="country['.$country_code.'][province]">';
                            foreach($show_hidden_options as $sh_val) {
                        	    $province_selected =   ($child_ques_info[$country_code]['province'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$province_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;County&nbsp;";
                        	echo '<select name="country['.$country_code.'][county]">';
                            foreach($show_hidden_options as $sh_val) {
                                $county_selected    =   ($child_ques_info[$country_code]['county'] == $sh_val) ? ' selected="selected"' : "";
                                echo '<option value="'.$sh_val.'" '.$county_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        }
                        else {
                            echo '<td>';
                        	echo "<strong>".$country_code."</strong>: ";
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;State&nbsp;";
                        	echo '<select name="country['.$country_code.'][state]">';
                        	foreach($show_hidden_options as $sh_val) {
                        	    $state_selected    =   ($child_ques_info[$country_code]['state'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$state_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;Province&nbsp;";
                        	echo '<select name="country['.$country_code.'][province]">';
                            foreach($show_hidden_options as $sh_val) {
                        	    $province_selected =   ($child_ques_info[$country_code]['province'] == $sh_val) ? ' selected="selected"' : "";
                        	    echo '<option value="'.$sh_val.'" '.$province_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        	echo '<td>';
                        	echo "&nbsp;County&nbsp;";
                        	echo '<select name="country['.$country_code.'][county]">';
                            foreach($show_hidden_options as $sh_val) {
                                $county_selected    =   ($child_ques_info[$country_code]['county'] == $sh_val) ? ' selected="selected"' : "";
                                echo '<option value="'.$sh_val.'" '.$county_selected.'>'.$sh_val.'</option>';
                        	}
                        	echo '</select>';
                        	echo '</td>';
                        	
                        }
                        
                        echo '</tr>';
                    }
    	       }
    	       ?>
    	       </td>
    	   </tr>
    	   
    	   <tr>
               <td colspan="5">
                    <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit">
    	       </td>
    	   </tr>
    	   <?php
    }
    ?>
    </table>
    <input type="hidden" name="action" value="onboardquestiondependencies">
    </form>
    <script>
    function getFormIDChildQuestionsInfo(form_value) {
        document.forms['frmFormQuestionsList'].submit();    
    }
    </script>
</div>