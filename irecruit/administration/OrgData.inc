<?php
echo '<div class="table-responsive">';
echo '<table border="0" cellspacing="3" cellpadding="5" width="100%" class="table table-striped table-bordered">';

if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") $MultiOrgID = $_REQUEST['MultiOrgID'];

if ($process == "OrgData") {
	
	$ERROR = "";
	
	if (strlen ( $_REQUEST['OrgDataOrganizationName'] ) <= 3) {
		$ERROR .= "Please enter an Organization Name." . "\\n";
	}
	
	$ERROR .= $ValidateObj->validate_email_address ( $_REQUEST['OrgDataContactEmail'] );
	
	if ($ERROR) {
		
		$MSG = "The following errors have occured:\\n\\n" . $ERROR . "\\n\\n";
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('" . $MSG . "')";
		echo '</script>';
	} else {
		// Set Update Information
		$set_info = array (
				"OrganizationName       =   :OrganizationName",
				"Address1               =   :Address1",
				"Address2               =   :Address2",
				"City                   =   :City",
				"State                  =   :State",
				"ZipCode                =   :ZipCode",
				"Country                =   :Country",
		        "Phone                  =   :Phone",
		        "Fax                    =   :Fax",
				"ContactName            =   :ContactName",
				"ContactAddress         =   :ContactAddress",
				"ContactCity            =   :ContactCity",
				"ContactState           =   :ContactState",
				"ContactZipCode         =   :ContactZipCode",
		        "ContactPhone           =   :ContactPhone",
				"ContactEmail           =   :ContactEmail" 
		);
		// Set where condition
		$where = array (
				"OrgID                  =   :OrgID",
				"MultiOrgID             =   :MultiOrgID" 
		);
		$Phone = json_encode([$_REQUEST['OrgDataPhoneAreaCode'],$_REQUEST['OrgDataPhonePrefix'],$_REQUEST['OrgDataPhoneSuffix']]);
		$Fax = json_encode([$_REQUEST['OrgDataFaxAreaCode'],$_REQUEST['OrgDataFaxPrefix'],$_REQUEST['OrgDataFaxSuffix']]);
		$ContactPhone = json_encode([$_REQUEST['OrgDataContactPhoneAreaCode'],$_REQUEST['OrgDataContactPhonePrefix'],$_REQUEST['OrgDataContactPhoneSuffix']]);
		// Set parameters
		$params = array (
				":OrganizationName"     =>  $_REQUEST['OrgDataOrganizationName'],
				":Address1"             =>  $_REQUEST['OrgDataAddress1'],
				":Address2"             =>  $_REQUEST['OrgDataAddress2'],
				":City"                 =>  $_REQUEST['OrgDataCity'],
				":State"                =>  strtoupper ( $_REQUEST['OrgDataState'] ),
				":ZipCode"              =>  $_REQUEST['OrgDataZipCode'],
				":Country"              =>  $_REQUEST['OrgDataCountry'],
		        ":Phone"                =>  $Phone,
		        ":Fax"                  =>  $Fax,
				":ContactName"          =>  $_REQUEST['OrgDataContactName'],
				":ContactAddress"       =>  $_REQUEST['OrgDataContactAddress'],
				":ContactCity"          =>  $_REQUEST['OrgDataContactCity'],
				":ContactState"         =>  $_REQUEST['OrgDataContactState'],
				":ContactZipCode"       =>  $_REQUEST['OrgDataContactZipCode'],
		        ":ContactPhone"         =>  $ContactPhone,
				":ContactEmail"         =>  $_REQUEST['OrgDataContactEmail'],
				":OrgID"                =>  $OrgID,
				":MultiOrgID"           =>  $MultiOrgID 
		);
		//echo "<pre>";print_r($params);exit();
		// Update OrgData Information
		$OrganizationsObj->updOrganizationInfo ( 'OrgData', $set_info, $where, array (
				$params 
		) );
		
		echo '<script language="JavaScript" type="text/javascript">';
		echo "alert('Organization Data configured!')";
		echo '</script>';
	} // end else/error
} // end process
  
// Set columns
$columns = "*, date_format(ClientSince, '%M %Y') AS ClientSince";
// Set parameters
$params = array (
		":OrgID" => $OrgID,
		":MultiOrgID" => $MultiOrgID 
);
// Set where Condition
$where = array (
		"OrgID = :OrgID",
		"MultiOrgID = :MultiOrgID" 
);
// Get Organization Data Information
$results = $OrganizationsObj->getOrgDataInfo ( $columns, $where, '', array (
		$params 
) );
$OD = $results ['results'] [0];
list($OD['PhoneAreaCode'],$OD['PhonePrefix'],$OD['PhoneSuffix']) = json_decode($OD['Phone']);
list($OD['FaxAreaCode'],$OD['FaxPrefix'],$OD['FaxSuffix']) = json_decode($OD['Fax']);
list($OD['ContactPhoneAreaCode'],$OD['ContactPhonePrefix'],$OD['ContactPhoneSuffix']) = json_decode($OD['ContactPhone']);
?>

<form action=administration.php method=post'>
	<tr>
		<td align="right" width="200">OrgID:</td>
		<td>
			<?php echo $OrgID;?>
		</td>
	</tr>
	<?php
	if ($feature ['MultiOrg'] == "Y") {
		?>
		<tr>
			<td align="right" width="200">Multi-OrgID:</td>
			<td>
			<?php
				if ($OD ['MultiOrgID'] == "") {
					echo "<i>(set as blank)</i>";
				} else {
					echo $OD ['MultiOrgID'];
				}
			?>
			</td>
		</tr>
	<?php
	}
	?>

	<tr>
		<td align="right"><font style="color: red">*</font> Organization Name:
		</td>
		<td><input type="text" size="40" maxlength="60"
			name="OrgDataOrganizationName"
			value="<?php echo $OD['OrganizationName'];?>"></td>
	</tr>

	<tr>
		<td align="right">Address 1:</td>
		<td><input type=text size="40" maxlength="45" name="OrgDataAddress1"
			value="<?php echo $OD['Address1'];?>"></td>
	</tr>

	<tr>
		<td align="right">Address 2:</td>
		<td><input type=text size="40" maxlength="45" name="OrgDataAddress2"
			value="<?php echo $OD['Address2'];?>"></td>
	</tr>

	<tr>
		<td align="right">City:</td>
		<td><input type=text size="30" maxlength="45" name="OrgDataCity"
			value="<?php echo $OD['City'];?>"></td>
	</tr>

	<tr>
		<td align="right">State:</td>
		<td><select name="OrgDataState">
				<option value="">Please Select</option>
				<?php
				// Get Address States List
				$results = $AddressObj->getAddressStateList ();
				
				if (is_array ( $results ['results'] )) {
					foreach ( $results ['results'] as $row ) {
						
						echo "<option value=\"" . $row ['Abbr'] . "\"";
						if ($OD ['State'] == $row ['Abbr']) {
							echo " selected";
						}
						echo ">" . $row ['Description'] . "</option>";
					} // end foreach
				}
				?>	
			</select></td>
	</tr>

	<tr>
		<td align="right">Zip Code:</td>
		<td><input type="text" size="10" maxlength="10" name="OrgDataZipCode"
			value="<?php echo $OD['ZipCode'] ?>"></td>
	</tr>

	<tr>
		<td align="right">Country:</td>
		<td><select name="OrgDataCountry">
			<?php
			$results = $AddressObj->getCountries ();
			
			if (is_array ( $results ['results'] )) {
				foreach ( $results ['results'] as $row ) {
					
					echo "<option value=\"" . $row ['Abbr'] . "\"";
					if (trim ( $OD ['Country'] ) == $row ['Abbr']) {
						echo " selected";
					}
					echo ">" . $row ['Description'];
					echo "</option>\n";
				}
			}
			?>
			</select></td>
	</tr>

	<tr>
		<td align="right">Phone:</td>
		<td>(<input type="text" size="3" maxlength="3"
			name="OrgDataPhoneAreaCode"
			value="<?php echo $OD['PhoneAreaCode'];?>">) <input type=text
			size="3" maxlength="3" name="OrgDataPhonePrefix" 
			value="<?php echo $OD['PhonePrefix'];?>">-<input type=text size="4"
			maxlength="4" name="OrgDataPhoneSuffix"
			value="<?php echo $OD['PhoneSuffix'];?>">
		</td>
	</tr>

	<tr>
		<td align="right">Fax:</td>
		<td>(<input type=text size="3" maxlength="3" name="OrgDataFaxAreaCode"
			value="<?php echo $OD['FaxAreaCode'];?>">) <input type=text size="3"
			maxlength="3" name="OrgDataFaxPrefix"
			value="<?php echo $OD['FaxPrefix'];?>">-<input type=text size="4"
			maxlength="4" name="OrgDataFaxSuffix"
			value="<?php echo $OD['FaxSuffix'];?>">
		</td>
	</tr>

	<tr>
		<td align="right">Contact Name:</td>
		<td><input type=text size="40" maxlength="45"
			name="OrgDataContactName" value="<?php echo $OD['ContactName'];?>"></td>
	</tr>

	<tr>
		<td align="right">Contact Address:</td>
		<td><input type=text size="40" maxlength="45"
			name="OrgDataContactAddress"
			value="<?php echo $OD['ContactAddress'];?>"></td>
	</tr>

	<tr>
		<td align="right">Contact City:</td>
		<td><input type=text size="30" maxlength="45"
			name="OrgDataContactCity" value="<?php echo $OD['ContactCity'];?>"></td>
	</tr>

	<tr>
		<td align="right">Contact State:</td>
		<td><select name="OrgDataContactState">
				<option value="">Please Select</option>
				<?php
				// Get Address States List
				$results = $AddressObj->getAddressStateList ();
				
				if (is_array ( $results ['results'] )) {
					foreach ( $results ['results'] as $row ) {
						
						echo "<option value=\"" . $row ['Abbr'] . "\"";
						if ($OD ['ContactState'] == $row ['Abbr']) {
							echo " selected";
						}
						echo ">" . $row ['Description'] . "</option>";
					} // end foreach
				}
				?>
			</select></td>
	</tr>

	<tr>
		<td align="right">Contact Zip Code:</td>
		<td><input type=text size="10" maxlength="10"
			name="OrgDataContactZipCode"
			value="<?php echo $OD['ContactZipCode'];?>"></td>
	</tr>

	<tr>
		<td align="right"><font style="color: red">*</font> Contact Email:</td>
		<td><input type=text size="40" maxlength="60"
			name="OrgDataContactEmail" value="<?php echo $OD['ContactEmail'];?>">
		</td>
	</tr>

	<tr>
		<td align="right">Contact Phone:</td>
		<td>(<input type=text size="3" maxlength="3"
			name="OrgDataContactPhoneAreaCode"
			value="<?php echo $OD['ContactPhoneAreaCode'];?>">) <input type=text
			size="3" maxlength="3" name="OrgDataContactPhonePrefix"
			value="<?php echo $OD['ContactPhonePrefix'];?>">-<input type=text
			size="4" maxlength="4" name="OrgDataContactPhoneSuffix"
			value="<?php echo $OD['ContactPhoneSuffix'];?>">
		</td>
	</tr>

	<tr>
		<td align="right">Client Since:</td>
		<td><?php echo $OD['ClientSince'];?></td>
	</tr>

	<tr>
		<td colspan="100%" height="60" valign="bottom" align="center">
			<input type="hidden" name=edit value="y"> 
			<input type="hidden" name="OrgID" value="<?php echo $OrgID;?>"> 
			<input type="hidden" name="MultiOrgID" value="<?php echo $OD['MultiOrgID'];?>"> 
			<input type="hidden" name="action" value="orgdata"> 
			<input type="hidden" name="process" value="OrgData"> 
			<input type="submit" value="Update Organization Information" class="btn btn-primary">
		</td>
	</tr>
	</table>
	</div>
</form>