<?php
if ($process == "Y") {
	// Update Text here //Set Update Information
	$set_info = array("OrganizationDescription = :OrganizationDescription");
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":OrganizationDescription"=>$_REQUEST['description']);
	//Update OrgData Information
	$OrganizationsObj->updOrganizationInfo('OrgData', $set_info, $where, array($params));
	
	echo '<script language="JavaScript" type="text/javascript">';
	echo "alert('Organizational Description configured!')";
	echo '</script>';
} // end process
  
// get updated text from db
//Set where condition
$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
//Get Organization Data Information
$results = $OrganizationsObj->getOrgDataInfo("OrganizationDescription", $where, '', array($params));
$desc = $results['results'][0];
?>
<form action="administration.php" method="post">
<div class="table-responsive">
	<table border="0" cellspacing="3" cellpadding="5" width="100%"
		class="table table-striped table-bordered">
		
			<tr>
				<td><b>Edit Text</b></td>
			</tr>
			<tr>
				<td align="center">
					<textarea name="description" rows="20" cols="80" class="mceEditor"><?php echo $desc['OrganizationDescription']?></textarea>
				</td>
			</tr>
			<tr>
				<td height="60" valign="middle" align="center">
					<input type="hidden" name="action" value="<?php echo $action ?>" /> 
					<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID ?>" /> 
					<input type="hidden" name="process" value="Y" /> 
					<input type="submit" name="submit" value="Change Description" class="btn btn-primary" />
				</td>
			</tr>
			<tr>
			<td>
				<hr size=1>
				<p>
					<b>Preview</b>
				</p>
				<?php echo $desc['OrganizationDescription']?>
			</td>
		</tr>
	</table>
</div>
</form>