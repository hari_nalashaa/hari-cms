<?php 
//Get OrganizationName
$OrganizationInformation    =   $OrganizationDetailsObj->getOrganizationInformation($OrgID, $MultiOrgID, "OrganizationName");
$title                      =   $OrganizationInformation["OrganizationName"];

if (! isset($navpg)) {
	$navpg                 =   "profiles";
	$navsubpg              =   "status";
} // end no navpg

$navsection = array (
	"listings"             =>  "Search Job Listings",
	"status"               =>  "Check My Status",
);

if (($navpg == "profiles") && (! $navsubpg)) {
	$navsubpg = "";
}
$profiles_subsection = array (
	"appprofile"           =>  "My Application Profile",
	"logininfo"            =>  "My Login Information" 
);

$bottomnav = array (
	"terms"                =>  "Terms of Use",
	"policy"               =>  "Privacy Policy" 
);

$page_titles_list = array(
	"status"               =>  "Application Status",
	"listings"             =>  "Search Job Listings",
	"appprofile"           =>  "My Application Profile",
	"logininfo"            =>  "My Login Information",
	"portalinfohtml"       =>  "Portal Information"
);

if (isset($_GET['navsubpg']) && isset($page_titles_list [$_GET['navsubpg']])) {
	$title .= ' - ' . $page_titles_list [$_GET['navsubpg']];
}
if (isset($_GET['navpg']) && $page_titles_list [$_GET['navpg']]) {
	$title .= ' - ' . $page_titles_list [$_GET['navpg']];
}
?>
