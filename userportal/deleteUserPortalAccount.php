<?php
require_once 'userportal.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
  require_once USERPORTAL_DIR . 'Authenticate.inc';
  require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
}

if(isset($_GET['confirm_del']) && $_GET['confirm_del'] == 'yes') {
    
	//Delete entire user information 
    G::Obj('UserPortalAccountDelete')->delUsersInfo($UpUserID);
    
    //Unset Master Value
    $_SESSION['U']['UserIDMasterValue'] = '';
    unset($_SESSION['U']['UserIDMasterValue']);

    // remove the session key from the users browser
    if (isset ( $_COOKIE ['PID'] )) {
        setcookie ( "PID", "", time () - 3600, "/" );
    }
    
    header("Location:".USERPORTAL_HOME."login.php?OrgID=".$OrgID);
    exit;
}

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
  require_once USERPORTAL_DIR . 'Header.inc';
  require_once USERPORTAL_DIR . 'Navigation.inc';
}

// Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';// Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Delete Account Permanently</h3>';
echo '</div>';
echo '</div>'; // Row End


echo '<div class="page-inner">'; // Inner Page Starts
echo '<div class="row">'; // Row Start

echo '<div class="col-lg-12">';

echo '<h4>Are you sure you want to delete your account permanently?</h4>';
echo '<h4>This change cannot be undone. Your data will be removed and you will be logged out.</h4>';

echo '<input type="button" name="btnDeleteAccount" id="btnDeleteAccount" onclick="confirmDelete()" value="Delete Account">';

echo '</div>';

echo '</div>'; // Row End
echo '</div>'; // Inner Page End

echo '</div>'; // Page Wrapper End
echo '</div>'; // Page Container End

?>
<script>
function confirmDelete() {
  if (confirm(" This change cannot be undone")) {
	location.href = '?confirm_del=yes';
  }
}
</script>
<?php
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
  require_once USERPORTAL_DIR . 'Footer.inc';
}
?>