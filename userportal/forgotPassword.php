<?php
require_once 'userportal.inc';

$AUTHX = $UserPortalUsersObj->getUserDetailInfoBySessionID ( "UserID, FirstName, LastName", $_REQUEST ['ctact'] );

if (isset ( $_REQUEST ['OrgID'] ) && $_REQUEST ['OrgID'] != "")
	$OrgID = $_REQUEST ['OrgID'];
if (isset ( $_REQUEST ['MultiOrgID'] ) && $_REQUEST ['MultiOrgID'] != "")
	$MultiOrgID = $_REQUEST ['MultiOrgID'];

// Set Global Variables
$navpg      =   isset ( $_REQUEST ['navpg'] ) ? $_REQUEST ['navpg'] : '';
$pg         =   isset ( $_REQUEST ['pg'] ) ? $_REQUEST ['pg'] : '';


$OrgIDck    =   "";
// Set parameters
$params_org =   array (":OrgID" => $OrgID);
// Get OrgData Information
$results    =   $OrganizationsObj->getOrgDataInfo ( "OrgID", array ("OrgID = :OrgID"), '', array ($params_org) );
$OrgIDck    =   $results ['results'] [0] ['OrgID'];

    $login_link = "login.php?OrgID=".$_REQUEST['OrgID'];
    if($MultiOrgID != "") {
        $login_link .= "&MultiOrgID=".$MultiOrgID;
    }

if((isset($_REQUEST['process']) 
    && $_REQUEST['process'] == 'Forgot Password')
    && ($_REQUEST['password'] == $_REQUEST['confirmpassword'])) {
	
    $encrypt_password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    //Update password based on session id
    
    $UserPortalUsersObj->updPassword($_REQUEST['ctact'], $encrypt_password);
    
    $login_link .= "&msg=pwdsuc";
    header("Location:".$login_link);
    exit;
}

if ($OrgIDck == "") {
	echo '<html><head><title>iRecruit - blank</title></head><body>';
	echo '<br><br><br>';
	echo '<table border="0" cellspacing="0" cellpadding="0">';
	echo '<tr><td width="250">&nbsp;</td><td>';
	echo '<span style="font-family:Arial;font-size:10pt;">Powered by:</span><br>';
	echo '<a href="http://www.irecruit-software.com/" target="_blank">';
	echo '<img border="0" src="' . USERPORTAL_HOME . 'images/iRecruit.png" width="130">';
	echo '</a>';
	echo '</td></tr>';
	echo '</table>';
	echo '</body></html>';
	exit ();
}


$page_title = $title = "myiRecruit Login";
require_once 'Header.inc';
?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <?php
    		// Set where condition
    		$where = array (
    				"OrgID           =   :OrgID",
    				"MultiOrgID      =   :MultiOrgID",
    				"PrimaryLogoType !=  ''" 
    		);
    		// Set parameters
    		$params = array (
    				":OrgID"         =>  $OrgID,
    				":MultiOrgID"    =>  (string) $MultiOrgID 
    		);
    		$results = $OrganizationsObj->getOrganizationLogosInformation ( "OrgID", $where, '', array (
    				$params 
    		) );
    		
    		$ImagePresent = $results ['count'];
    		
    		if ($ImagePresent > 0) {
    		    if(defined('USERPORTAL_HOME')) {
    		        echo '<img src="' . USERPORTAL_HOME . 'display_image.php?OrgID=' . htmlspecialchars($OrgID) . '&MultiOrgID=' . htmlspecialchars($MultiOrgID) . '&Type=Primary"><br><br>';
    		    }
    		}
    		?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h2>
				Forgot Password
			</h2>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <?php 
                if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
                	echo '<span style="color:blue">Password successfully updated</span>';
                }
                if($AUTHX['UserID'] == '') {
                    echo '<span style="color:blue">The link you have followed has expired. ';
                    echo 'Please resend your request using the <a href="';
                    echo $login_link;
                    echo '">login page</a>.</span>';
                    echo '<br><br><br><br>';
                }
            ?>
        </div>
    </div>
<?php
if($AUTHX['UserID'] != '') {
?>
    <div class="row">
        <div class="col-lg-12">
    
		    <form name="frmForgotPassword" id="frmForgotPassword" method="post" action="forgotPassword.php">
		        <input type="hidden" name="ctact" id="ctact" value="<?php echo htmlspecialchars($_REQUEST['ctact']);?>">
		        <input type="hidden" name="OrgID" id="OrgID" value="<?php echo htmlspecialchars($_REQUEST['OrgID']);?>">
		        <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo htmlspecialchars($_REQUEST['MultiOrgID']);?>">
    			<div class="form-group row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="password" class="question_name">
                            Password: <span style="color:#FF0000">*</span>
                        </label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
    				   <input type="password" name="password" id="password">
                    </div>
                </div>
    			
                <div class="form-group row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="confirmpassword" class="question_name">
                            Confirm Password: <span style="color:#FF0000">*</span>
                        </label>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
    					   <input type="password" name="confirmpassword" id="confirmpassword"> 
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
    				   <input type="hidden" name="process" value="Forgot Password"> 
    				   <input type="submit" value="Submit" name="Submit" class="btn btn-primary">
                    </div>
                </div>
                
                <div class="form-group row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
    					Please enter your password and confirm password to update. 
                    </div>
                </div>
            </form>
            
        </div>
    </div>
    
 </div>
<?php
}
require_once USERPORTAL_DIR . 'Footer.inc';
?>
