<?php 
require_once 'userportal.inc';
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

$formtable              =   "FormQuestions";
$title                  =   "Thank you for applying for this postion";

$requisition_info       =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $OrgID, $_REQUEST['RequestID']);
$FormID                 =   $requisition_info['FormID'];
$MultiOrgID             =   $requisition_info['MultiOrgID'];
$HoldID                 =   $OrgID.$UpUserID.$RequestID;

$APPDATA                =   array ();
$APPDATAREQ             =   array ();

$results                =   G::Obj('UserPortalInfo')->getApplicationInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        $APPDATA [$row ['QuestionID']] = $row ['Answer'];
    } // end foreach
}

//Prefill the $FILES
$FILES                  =   $_FILES;
$attachment_types       =   array();
$attachments_info       =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[]     =   $attachments_info[$aai]['TypeAttachment'];
    
    $file_type              =   $attachments_info[$aai]['FileType'];
    $file_attachment_name   =   $attachments_info[$aai]['TypeAttachment'];
    $file_name              =   $file_attachment_name.".".$file_type;
    
    $FILES[$attachments_info[$aai]['TypeAttachment']]   =   array(
                                                                "name"  =>  $attachments_info[$aai]['TypeAttachment'],
                                                                "type"  =>  $file_type
                                                                );
}

$processed_sections     =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");
$processed_sections     =   unserialize($processed_sections);

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';
}

$user_sec_list          =   array();
foreach ($userportal_sections_list as $section_id=>$section_info) {
    $user_sec_list[]    =   $section_id;
}

$sign_application       =   "true";

foreach($user_sec_list as $user_sec_id) {
    if(!in_array($user_sec_id, $processed_sections)) {
        $sign_application   =   "false";
    }
}

if(count($errors_list) > 0) {
    $sign_application   =   "false";
}

if($sign_application == "true") {
    echo json_encode(array("errors_list"=>$errors_list, "success"=>"true", "section_id_names"=>$section_id_names));
    exit;
}
else {
    echo json_encode(array("errors_list"=>$errors_list, "success"=>"false", "section_id_names"=>$section_id_names));
    exit;
}
?>