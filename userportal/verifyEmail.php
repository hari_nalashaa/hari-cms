<?php
require_once 'userportal.inc';

if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") $MultiOrgID = $_REQUEST['MultiOrgID'];
if(isset($_REQUEST['acc']) && $_REQUEST['acc'] != "") $acc = $_REQUEST['acc'];

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

//Get International Translation Information
$INT = $FormFeaturesObj->getInternationalTranslation($OrgID);

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Verify Email</h3>';
echo '</div>';
echo '</div>';

echo '<div class="page-inner">';//Row End
echo '<div class="row">'; //Row Start

if ($INT ['InternationalTranslation'] == "Y") {
	?>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script>
<script
	src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div id="google_translate_element"></div>
<?php
}

if (($OrgID) && ($acc)) {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "SessionID = :acc");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":acc"=>$acc);
	//Get Users Information
	if ($acc != '') {
	  $results_users = $UserPortalUsersObj->getUsersInformation("*", $where, "", "", array($params));
	  $hit = $results_users['count'];
	}
	
}

if ($hit == 1) {
	
	//Set Information
	$set_info = array("EmailVerified = 1");
	//Set where condition
	$where = array("OrgID = :OrgID", "SessionID = :acc");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":acc"=>$acc);
	//Update Users Information
	$UserPortalUsersObj->updUsersInfo($set_info, $where, array($params));
	
	$link = "https://" . $_SERVER ['SERVER_NAME'] . preg_replace ( '/verifyEmail.php/', 'index.php', $_SERVER ['PHP_SELF'] );
	$link .= '?OrgID=' . $OrgID;
        if($MultiOrgID != "") {
          $link .= "&MultiOrgID=".$MultiOrgID;
        }

	$link .= "&navpg=profiles&navsubpg=status";

	echo "Thank you.<br>Your email has been verified.<br><br>";
	echo "You may log in here: <a href=\"" . $link . "\">" . $link . "</a>";

} else { // hit
	
	echo "You have reached this page in error. Please be sure the link you are entering is accurate.";

} // end hit

echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End

echo '</div>';
echo '</div>';
?>
