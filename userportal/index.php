<?php
require_once 'userportal.inc';

include USERPORTAL_DIR . 'JobApplicationRequestVars.inc';
if(isset($_REQUEST['ProfileID']) && $_REQUEST['ProfileID'] != "") $ProfileID = $_REQUEST['ProfileID'];

if (in_array(USERPORTAL_DIR, $whitelist)) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

$new_job_image = '';
$hot_job_image = '';

// update new job status
$highlight_req_settings =   G::Obj('HighlightRequisitions')->getHighlightRequisitionSettings($OrgID);

if($highlight_req_settings['HighlightNewJob'] == "Yes") {
    $new_job_days = $highlight_req_settings['NewJobDays'];
    $new_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['NewJobIcon']);

    if($new_job_icon != "") {
        $new_job_image = '<img src="'.IRECRUIT_HOME.'/vault/highlight/'.$new_job_icon.'">';
    }
}

if($highlight_req_settings['HighlightHotJob'] == "Yes") {
    $hot_job_icon = G::Obj('HighlightRequisitions')->getJobImage($highlight_req_settings['HotJobIcon']);

    if($hot_job_icon != "") {
        $hot_job_image = '<img src="'.IRECRUIT_HOME.'/vault/highlight/'.$hot_job_icon.'">';
    }
}

/**
 * @tutorial
 */
if(isset($_REQUEST['action']) && $_REQUEST['action'] == "clearprofile") {
    G::Obj('UserPortalUsers')->clearUserProfileData($UpUserID);
    
    header("Location:".USERPORTAL_HOME."index.php?OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID."&navpg=profiles&navsubpg=".$navsubpg."&msg=succclearprofile");
    exit;
}

if(isset($_REQUEST['action']) && $_REQUEST['action'] == "updateprofile") {
    //Clear the data before insertion
    G::Obj('UserPortalUsers')->clearUserProfileData($UpUserID);
    
    G::Obj('UserPortalUsers')->insertUserProfileWithApplicaitonInfo($UpUserID, $OrgID, $ApplicationID);
	header("Location:".USERPORTAL_HOME."index.php?OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID."&navpg=profiles&navsubpg=status&msg=succupdprofile");
	exit;
}

if (in_array(USERPORTAL_DIR, $whitelist)) {
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo $title;
if($navsubpg != 'status') {
    echo '<span style="float:right;font-size:13px;">';
    echo '<a href="'.USERPORTAL_HOME.'index.php?navpg=profiles&navsubpg=status">';
    echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Check My Status';
    echo '</a>';
    echo '</span>';
}
echo '</h3>';
echo '</div>';
echo '</div>'; //Row End

echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

if ($navpg == "listings") {
    //Get Organization Detail Information
    $org_detail_info    =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "ListingsView");
    $listings_view      =   ($org_detail_info['ListingsView'] == '') ? 'initiallist' : $org_detail_info['ListingsView'];

    //Display Listings by listings view variable
    if (in_array(COMMON_DIR, $whitelist) 
        && in_array($listings_view, $whitelist)) {
        require_once COMMON_DIR . 'listings/' . $listings_view . '.inc';
    }
} // end listings


if (($navpg == "profiles") && ($navsubpg == "status")) {
	echo '<style>';
	echo '.table>thead>tr>th,.table>tbody>tr>th,.table>tfoot>tr>th,.table>thead>tr>td,.table>tbody>tr>td,.table>tfoot>tr>td
	{
		border: 0 none;
		vertical-align: text-top !important;
		padding: 5px 5px;
	}';
	echo '</style>';
	require_once 'Status.inc';
}

if (($navpg == "profiles") && ($navsubpg == "appprofile")) {
	require_once 'AppProfile.inc';
}

if (($navpg == "profiles") && ($navsubpg == "logininfo")) {
	require_once 'LoginInfo.inc';
}

// for viewing applicaiton
if (($navpg == "status") && ($navsubpg == "view")) {
	$emailonly = "Y";

	echo "<style>";
	echo ".table-bordered {
	    border: 0px solid #ddd !important;
	}
	.table {
	    margin-bottom: 0px !important;
	    max-width: 100%;
	    width: 100%;
	}
	.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
	    border: 0px solid #ddd !important;
	}
	.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
	    border-top: 0px solid #ddd;
	    line-height: 1.42857;
	    padding: 0px !important;
	    vertical-align: top;
	}
	";
	echo "</style>";
	
	if (in_array(COMMON_DIR, $whitelist)) {
        require_once COMMON_DIR . 'application/ApplicationView.inc';
    }
}

// for viewing html pages etc from employer
if (($navpg == "portalinfohtml") && ($navsubpg == "")) {
	//Set where condition
	$where_html_pages_info = array("UpdateID = :UpdateID", "OrgID = :OrgID");
	//Set parameters
	$params_html_pages_info = array(":UpdateID"=>$_GET ['updateid'], ":OrgID"=>$OrgID);
	//Get UserPortal Html Pages Information
	$user_portal_info = $UserPortalInfoObj->getUserPortalInfo("*", $where_html_pages_info, '', array($params_html_pages_info));

	if(is_array($user_portal_info['results'])) {
		foreach ($user_portal_info['results'] as $PTLKey=>$PTL) {
			echo $PTL ['HTML'];
		}	
	}
} // end portalinfohtml

if (($navpg == "terms") && ($navsubpg == "")) {
    if (in_array(COMMON_DIR, $whitelist)) {
        require_once COMMON_DIR . 'terms.inc';
    }
}

if (($navpg == "policy") && ($navsubpg == "")) {
    if (in_array(COMMON_DIR, $whitelist)) {
        require_once COMMON_DIR . 'policy.inc';
    }
}

echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';

require_once USERPORTAL_DIR . 'Footer.inc';
?>
