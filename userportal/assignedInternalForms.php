<?php
require_once 'userportal.inc';

if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['ProcessOrder']) && $_REQUEST['ProcessOrder'] != "") $ProcessOrder = $_REQUEST['ProcessOrder'];
if(isset($_REQUEST['complete']) && $_REQUEST['complete'] != "") $complete = $_REQUEST['complete'];


require_once USERPORTAL_DIR . 'Authenticate.inc';
require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

$user_detail_info       =   G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "UserPortalThemeID");
$up_app_theme_info      =   G::Obj('UserPortalInfo')->getUserPortalApplicationThemeInfo($user_detail_info['UserPortalThemeID']);

if (isset ( $complete )) {

	list ( $AID, $RID, $UID ) = explode ( ":", $complete );

	//Set information
	$set_info      =   array("LastUpdated = NOW()", "Status = 3");
	//Set where condition
	$where_info    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = :UniqueID");
	//Set parameters information
	$params_info   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$AID, ":RequestID"=>$RID, ":UniqueID"=>$UID);
	//Set Information
	G::Obj('FormsInternal')->updInternalFormsAssigned($set_info, $where_info, array($params_info));

	// Add History
	$Comment   =   "Changed status to: Completed";
	//Information of Internal Form History
	$info      =   array("OrgID"=>$OrgID, "ApplicationID"=>$AID, "RequestID"=>$RID, "InternalFormID"=>$UID, "Date"=>"NOW()", "UserID"=>$UpUserID, "Comments"=>$Comment);
	//Insert Internal Forms History	
	G::Obj('FormsInternal')->insInternalFormHistory($info);

} // end complete

$where                  =   array("OrgID = :OrgID");
$params                 =   array(":OrgID"=>$OrgID);
$res_webform_sortorder  =   G::Obj('FormsInternal')->getWebFormsInfo("WebFormID, SortOrder", $where, "", array($params));

if(is_array($res_webform_sortorder['results'])) {
	foreach($res_webform_sortorder['results'] as $wrow) {
		$formrows["WebForm"][] = array("WebFormID"=>$wrow["WebFormID"], "SortOrder"=>$wrow["SortOrder"]);
	}
}

$where = array("OrgID = :OrgID");
$params = array(":OrgID"=>$OrgID);
$res_prefilledforms_sortorder = $FormsInternalObj->getPrefilledFormsInfo("PreFilledFormID, SortOrder", $where, "", array($params));

if(is_array($res_prefilledforms_sortorder['results'])) {
	foreach($res_prefilledforms_sortorder['results'] as $prow) {
		$formrows["PreFilledForm"][] = array("PreFilledFormID"=>$prow["PreFilledFormID"], "SortOrder"=>$prow["SortOrder"]);
	}
}

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get Specification Forms Information
$res_specforms_sortorder = $FormsInternalObj->getSpecificationForms("SpecificationFormID, SortOrder", $where, "", array($params));

if(is_array($res_specforms_sortorder['results'])) {
	foreach($res_specforms_sortorder['results'] as $srow) {
		$formrows["SpecificationForm"][] = array("SpecificationFormID"=>$srow["SpecificationFormID"], "SortOrder"=>$srow["SortOrder"]);
	}	
}

//Set where condition
$where = array("OrgID = :OrgID");
//Set parameters
$params = array(":OrgID"=>$OrgID);
//Get AgreementForms Information
$res_agreeforms_sortorder = $FormsInternalObj->getAgreementFormsInfo("AgreementFormID, SortOrder", $where, "", array($params));

if(is_array($res_agreeforms_sortorder['results'])) {
	foreach ($res_agreeforms_sortorder['results'] as $arow) {
		$formrows["AgreementForm"][] = array("AgreementFormID"=>$arow["AgreementFormID"], "SortOrder"=>$arow["SortOrder"]);
	}
}

foreach ($formrows as $formtype=>$formsubtypes) {
	foreach($formsubtypes as $fkey=>$fvalue) {
			
		if($formtype == "WebForm") {
			$FUniqueId		=	$fvalue["WebFormID"];
			$FSortOrder		=	$fvalue["SortOrder"];
		} else if($formtype == "PreFilledForm") {
			$FUniqueId		=	$fvalue["PreFilledFormID"];
			$FSortOrder		=	$fvalue["SortOrder"];
		} else if($formtype == "SpecificationForm") {
			$FUniqueId		=	$fvalue["SpecificationFormID"];
			$FSortOrder		=	$fvalue["SortOrder"];
		} else if($formtype == "AgreementForm") {
			$FUniqueId		=	$fvalue["AgreementFormID"];
			$FSortOrder		=	$fvalue["SortOrder"];
		}
			
		//Set information
		$set_info = array("SortOrder = :SortOrder");
		//Set where condition
		$where_info = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = :UniqueID", "PresentedTo = 1");
		//Set parameters information
		$params_info = array(":SortOrder"=>$FSortOrder, ":OrgID"=>$OrgID, ":ApplicationID"=>$_GET ['ApplicationID'], ":RequestID"=>$_GET ['RequestID'], ":UniqueID"=>$FUniqueId);
		//Set Information
		$result_forms_sort_order = G::Obj('FormsInternal')->updInternalFormsAssigned($set_info, $where_info, array($params_info));
	}
}

//Set where condition
$where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID");
//Set parameters
$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID);
//Get ApplicantData Information		
$application_data = $ApplicantsObj->getApplicantDataInfo("*", $where, '', '', array($params));

$app_data_count = $application_data['count'];
$app_data_results = $application_data['results'];

for($i = 0; $i < $app_data_count; $i++) {
	if($app_data_results[$i]['QuestionID'] == 'first') {
		$first_name = $app_data_results[$i]['Answer'];
	}
	if($app_data_results[$i]['QuestionID'] == 'last') {
		$last_name = $app_data_results[$i]['Answer'];
	}
}

echo '<style>';

echo '
.numberSideText {
    background: #'.$up_app_theme_info['PendingTab'].' none repeat scroll 0 0;
    border: 1px solid green;
    color: white !important;
    font-family: Arial;
    font-size: 13px;
    font-weight: bold;
    height: 40px;
    margin-right: 10px;
    text-align: left;
    min-width:100%;
    width:auto;
}
.numberSideText a {
    color: white !important;
}
.numberSideCompletedText {
    background: #'.$up_app_theme_info['CompletedTab'].' none repeat scroll 0 0;
    border: 1px solid grey;
    color: white;
    font-family: Arial;
    font-size: 13px;
    font-weight: bold;
    height: 40px;
    text-align: left;
    min-width:100%;
    width:auto;
}
.numberSideCompletedText a {
    color: white !important;
}
/*
.numberLine {
    margin-left:6px;
    width: 40px;
    font-weight: bold;
    color: green;
    text-align: left;
    line-height: 80%;
    clear: both;
}
*/
';

echo '</style>';

// Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';// Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Assigned Forms';
echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'index.php?navpg=profiles&navsubpg=status">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Check My Status';
echo '</a>';
echo '</span>';
echo '</h3>';
echo '</div>';
echo '</div>'; // Row End

echo '<div class="page-inner">'; // Inner Page Starts

if(isset($_GET['msg']) && $_GET['msg'] == "form_suc")
{
	echo '<div class="row">';// Row Start
	echo '<div class="col-lg-12">';
	echo '<br>';
	echo '<span style="color:green;">&nbsp;';
	echo 'You have successfully submitted the form. ';
	echo '</span>';
	echo '</div>';
	echo '</div>'; // Row End	
}

include 'ApplicationInformation.inc';

echo '<div class="row">';// Row Start

echo '<div class="col-lg-12 col-md-12 col-sm-12">';

//Set where condition
$where		=	array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = 1", "Status > 0", "Status < 5");
//Set parameters
$params		=	array(":OrgID"=>$OrgID, ":ApplicationID"=>$_GET ['ApplicationID'], ":RequestID"=>$_GET ['RequestID']);
//Get InternalForms Assigned Information
$resultsIFA =	G::Obj('FormsInternal')->getInternalFormsAssignedInfo("*", $where, "", "SortOrder ASC", array($params));
$hitIFA 	=	$resultsIFA['count'];

if ($hitIFA > 0) {
	echo '<tr>';
	echo '<td valign="top" colspan="2"><br><strong>Dear '. $first_name . ' ' . $last_name .', please review and complete the assigned forms and/or documents listed below</strong>.</td>';
	echo '</tr>';
} // end hitIFA
else {
	echo '<tr>';
	echo '<td valign="top" colspan="2"><br><span style="font-size:14px"><strong>There are no forms assigned to this application</strong></font></td>';
	echo '</tr>';
}

echo '<tr>';

echo '<td valign="top" colspan="2"><br>';

echo '<div class="row">';
echo '<div class="col-lg-6 col-md-6 col-sm-6">';

echo '<div>';
echo '<h3>To Do List</h3>';
echo '</div>';

echo '<div style="clear:both;"><br></div>';

//Assigned Forms List
$assigned_forms_list    =   G::Obj('FormsInternal')->getAssignedFormsInfo($OrgID, $_REQUEST['RequestID'], $_REQUEST['ApplicationID'], $_REQUEST['ProcessOrder']);

for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
    
    $assigned_forms_info    =   $assigned_forms_list[$afl];
    
    foreach($assigned_forms_info as $FormType=>$IFA) {
        
        if($IFA["Status"] == "Pending") {
            $form_status        =   $IFA["IFAInfo"]["Status"];
            $form_status_text   =   $IFA["FormStatusText"];
            $link               =   $IFA["Link"];

            echo '<div class="'.$form_status_text.'">';
            if($FormType == "External Link") {
                echo '<img src="'.IRECRUIT_HOME.'images/icons/link.png" title="External Link" style="margin: -7px 3px -4px 0px;" border="0">';
            }
            else if($FormType == "Attachment") {
                echo '<img src="'.IRECRUIT_HOME.'images/icons/attach.png" title="Attachment" style="margin: -7px 3px -4px 0px;" border="0">';
            }
            else if($FormType == "HTML Page") {
                echo '<img src="'.IRECRUIT_HOME.'images/icons/html.png" title="HTML Page" style="margin: -7px 3px -4px 0px;" border="0">';
            }
            else if($FormType == "WebForm" || $FormType == "WOTC") {
                echo '<img src="'.IRECRUIT_HOME.'images/icons/application_form.png" title="Web Form" style="margin: -7px 3px -4px 0px;" border="0">';
            }
            else if($FormType == "Agreement") {
                echo '<img src="'.IRECRUIT_HOME.'images/icons/page_white_paste.png" title="Agreement Form" style="margin: -7px 3px -4px 0px;" border="0">';
            }
            else if($FormType == "PreFilled") {
                echo '<img src="'.IRECRUIT_HOME.'images/icons/report.png" title="Federal & State Form" style="margin: -7px 3px -4px 0px;" border="0">';
            }
            echo '<a href="'.$link.'">'.$IFA ['FormName'].'</a> &nbsp;';
            
            if($IFA ['MarkComplete'] != "") {
                echo $IFA ['MarkComplete'];
            }
            
            echo '</div>';
            
            echo '<div style="clear:both;"><br></div>';
        }

    } // end foreach
}

echo '</div>';


echo '<div class="col-lg-6 col-md-6 col-sm-6">';

echo '<div>';
echo '<h3>Completed List</h3>';
echo '</div>';

echo '<div style="clear:both;"><br></div>';

for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
    
    $assigned_forms_info    =   $assigned_forms_list[$afl];
    
    if(is_array($assigned_forms_info)) {
        foreach($assigned_forms_info as $FormType=>$IFA) {
            
            if($IFA["Status"] == "Completed") {
                $form_status        =   $IFA["IFAInfo"]["Status"];
                $form_status_text   =   $IFA["FormStatusText"];
                $link               =   $IFA["Link"];
                
                echo '<div class="'.$form_status_text.'">';
                if($FormType == "External Link") {
                    echo '<img src="'.IRECRUIT_HOME.'images/icons/link.png" title="External Link" style="margin: -4px 3px -4px 0px;" border="0">';
                }
                else if($FormType == "Attachment") {
                    echo '<img src="'.IRECRUIT_HOME.'images/icons/attach.png" title="Attachment" style="margin: -4px 3px -4px 0px;" border="0">';
                }
                else if($FormType == "HTML Page") {
                    echo '<img src="'.IRECRUIT_HOME.'images/icons/html.png" title="HTML Page" style="margin: -4px 3px -4px 0px;" border="0">';
                }
                else if($FormType == "WebForm" || $FormType == "WOTC") {
                    echo '<img src="'.IRECRUIT_HOME.'images/icons/application_form.png" title="Web Form" style="margin: -7px 3px -4px 0px;" border="0">';
                }
                else if($FormType == "Agreement") {
                    echo '<img src="'.IRECRUIT_HOME.'images/icons/page_white_paste.png" title="Agreement Form" style="margin: -7px 3px -4px 0px;" border="0">';
                }
                else if($FormType == "PreFilled") {
                    echo '<img src="'.IRECRUIT_HOME.'images/icons/report.png" title="Federal & State Form" style="margin: -7px 3px -4px 0px;" border="0">';
                }
                echo '<a href="'.$link.'">'.$IFA ['FormName'].'</a>';
                echo '<br>';
                
                echo '</div>';
                
                echo '<div style="clear:both;"><br></div>';
                
            }
            
        } // end foreach
    }
}

echo '</div>';

echo '</div>';

echo '</td></tr>';

echo '</table>';

echo '</div>';

echo '</div>'; // Row End

echo '</div>';
echo '</div>';

echo '</div>'; // Page Wrapper End
echo '</div>'; // Page Container End

require_once USERPORTAL_DIR . 'Footer.inc';
?>
