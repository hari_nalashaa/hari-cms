<?php
require_once 'userportal.inc';

if (isset($OrgID) && isset($_REQUEST['updateid']) && $_REQUEST['updateid'] != "") {
	
	//Set columns
	$columns = "TypeAttachment, FileName";
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "UpdateID = :UpdateID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>(string)$MultiOrgID, ":UpdateID"=>$_REQUEST['updateid']);
	//Get UserPortla Information
	$results = $UserPortalInfoObj->getUserPortalInfo("*", $where, '', array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $IMG) {
			$displaytitle    =   $IMG ['DisplayTitle'];
			$attfile         =   $IMG ['Attachment'];
			$atttype         =   $IMG ['AttachmentType'];
			$attext          =   $IMG ['AttachmentExt'];
		}
	}
	
	$filename = $displaytitle . "." . $attext;
	
	header ( 'Content-Description: File Transfer' );
	header ( "Content-type: $atttype" );
	header ( 'Content-Disposition: attachment; filename=' . $filename );
	header ( 'Content-Transfer-Encoding: binary' );
	header ( 'Expires: 0' );
	header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header ( 'Pragma: public' );
	echo $attfile;
	exit ();
} // end if OrgID, UpdateID
?>