<?php
header("Content-type: text/css"); 
require_once '../userportal.inc';
?>
/* Generic Selectors */
 
body {
   margin:0px; 
   width:100%;
   font-family: "Verdana", Arial, Helvetica, sans-serif;
   font-size: 12px;
   font-color: #2C2C2C;	
   background-color: #<?php echo $COLOR['Light']?>;
}
 
p {
   font-size: 12px;
}

ol {
   margin: 0;
   padding: 0px 0px 0px 20px; /*top right bottom left*/
}

ol li {
   list-style-type: none;
}
ol li b {
   color: #0066CC; 
}

table {
   font-size: 12px;
}
 
/**************** Pseudo classes ****************/

<?php

if ($OrgID == "I20110801") {

echo <<<END
a:link {
   color: #0066CC; 
   text-decoration: underline;
   font-style: normal;
}
 
a:visited {
   color: blue;
   text-decoration: underline;
   font-style: normal;
}
 
a:hover {
   color: blue;
   text-decoration: underline;
   font-style: normal;
}

a:active {
   color: blue;
   text-decoration: underline;
   font-style: normal;
}
END;


} else {

echo <<<END
a:link {
   color: #0066CC;
   text-decoration: none;
}
 
a:visited {
   color: #0066CC;
   text-decoration: none;
}
 
a:hover {
   color: #0066CC;
   text-decoration: underline;
   font-style: normal;
}

a:active {
   color: #0066CC;
   font-weight: bold;
   font-style: normal;
}
END;
} // end else OrgID

?>
 
/************************* ID's *************************/

#topDoc {
   padding: 10px 0px 0px 25px; /*top right bottom left*/
   border-bottom: 4px solid #<?php echo $COLOR['Heavy']?>; 
   background-color: #FFFFFF;
}

#topDoc h1 {
   padding: 33px 0px 0px 220px; /*top right bottom left*/
   font-size: 20px;
}

#topDoc p {
   padding: 0px 0px 0px 200px; /*top right bottom left*/
}
 
#navigation {
   float: left;
   background-color: #<?php echo $COLOR['Medium']?>;
   padding: 10px 20px 20px 0px; /*top right bottom left*/
   margin-top: 5px;
   margin-right: 10px;
   font-size: 12pt;
}

#navigation ol {
   list-style: none;
   padding: 0px 0px 0px 20px; /*top right bottom left*/
   margin: 0;
}

#navigation li {
   line-height: 250%;
}

#navigation ol li.home {
   padding: 0px 0px 0px 10px; /*top right bottom left*/
   font-size: 13pt;
   font-weight: bold;
   color: #0066CC;
}

#navigation ol li.home b {
   color: #0066CC;
   font-size: 13pt;
}

#navigation ol li.home a {
   color: #263884;
   font-size: 13pt;
}

#navigation ol ol {
   list-style: none;
   padding: 0px 0px 0px 10px; /*top right bottom left*/
   margin: 0;
}

#navigation ol ol li {
   line-height: 110%;
   padding: 0px 0px 0px 10px; /*top right bottom left*/
   font-weight: normal;
}


#navigation ol ol li.home {
   padding: 0px 0px 0px 10px; /*top right bottom left*/
   font-size: 13px;
   font-weight: bold;
   color: #263884;
}


#centerDoc {
   margin-top: 5px;
   margin-bottom: 10px;
   margin-right: 5px;
   margin-left: 300px;
   padding: 20px 20px 20px 20px; /*top right bottom left*/
   background-color: #<?php echo $COLOR['LightLight']?>;
}

#centerDoc table {
   font-size: 12px;
}

#bottomDoc {
   clear: both;
   width: 100%;
   padding: 5px 0 5px 0; /*top right bottom left*/
   font-size: 11px;
   background-color: #<?php echo $COLOR['Light']?>;
}

#bottomDoc p {
   text-align: center;
   color: #2C2C2C;
}

#bottomDoc a {
   color: #2C2C2C;
}

.shadow {
   -moz-box-shadow: 3px 3px 4px #444444;
   -webkit-box-shadow: 3px 3px 4px #444444;
   box-shadow: 3px 3px 4px #444444;
   /* For IE 8 */
   -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#444444')";
   /* For IE 5.5 - 7 */
   filter: progid:DXImageTransform.Microsoft.Shadow(Strength=4, Direction=135, Color='#444444');
}
#loading-div-background {
        display:none;
        position:fixed;
        top:0;
        left:0;
	background:#ffffff;
        width:100%;
        height:100%;
}

#loading-div {
         width: 100px;
         height: 100px;
         text-align:center;
         position:absolute;
         left: 50%;
         top: 50%;
         margin-left:-100px;
         margin-top: -300px;
}

