<?php
$ProfileID = 'STANDARD';

	
if ($process == "Update Profile Information") {
	
	//Delete Profile Data Information based on profile id
	G::Obj('UserPortalUsers')->delProfileData($UpUserID, $ProfileID);
	
	//Clear the data before insertion
	G::Obj('UserPortalUsers')->clearUserProfileData($UpUserID);
	
	// Loop through form fields
	foreach ( $_POST as $question => $answer ) {
		
		//Insert Profile Data Information
		if ($question == 'shifts_schedule_time' || $question == 'LabelSelect') {
		
			foreach ( $_POST [$question] as $cquestion => $canswer ) {
					
				if ($question == 'LabelSelect') {
					$answer = serialize ( $_REQUEST ['LabelSelect'] [$cquestion] );
				}
				if ($question == 'shifts_schedule_time') {
					$qa ['from_time'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [from_time];
					$qa ['to_time'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [to_time];
					$qa ['days'] = $_REQUEST ['shifts_schedule_time'] [$cquestion] [days];
					$answer = serialize ( $qa );
				}
					
				if (($answer != '') && ($question != 'MAX_FILE_SIZE') && ($question != 'submit') && (! preg_match ( '/^countdown/', $question ))) {
					//Insert ProfileData Information
					$info = array("UserID"=>$UpUserID, "ProfileID"=>$ProfileID, "QuestionID"=>$cquestion, "Answer"=>$answer);
					G::Obj('UserPortalUsers')->insProfileData($info);
				}
			}
		} else {
			if (($answer != '') 
				&& ($question != 'MAX_FILE_SIZE') 
				&& ($question != 'OrgID') 
				&& ($question != 'process') 
				&& ($question != 'navpg') 
				&& ($question != 'navsubpg') 
				&& ($question != 'ProfileID') 
				&& ($question != 'PortalUserID') 
				&& ($question != 'FormID') 
				&& ($question != 'HoldID') 
				&& ($question != 'countryorig') 
				&& ($question != 'process') 
				&& (! preg_match ( '/^countdown/', $question ))) {
				//Insert ProfileData Information
				$info = array("UserID"=>$UpUserID, "ProfileID"=>$ProfileID, "QuestionID"=>$question, "Answer"=>$answer);
				G::Obj('UserPortalUsers')->insProfileData($info);
			}
		}
		
	} // end foreach
	
	echo '<script language="JavaScript" type="text/javascript">' . "\n";
	echo "location.href='".USERPORTAL_HOME."index.php?navpg=profiles&navsubpg=appprofile&msg=succ';";
	echo '</script>' . "\n";
	
} // end process Update Profile

if ($delete) {
	//Delete Profile Data Information based on profile id
    G::Obj('UserPortalUsers')->delProfileData($UpUserID, $delete);
} // end delete

	
//Set where condition
$where      =   array("UserID = :UserID", "ProfileID = :ProfileID");
//Set parameters
$params     =   array(":UserID"=>$UpUserID, ":ProfileID"=>$ProfileID);
//Get ProfileData Information
$results    =   G::Obj('UserPortalUsers')->getProfileDataInfo("QuestionID, Answer", $where, "", array($params));
$hit        =   $results['count'];

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$APPDATA [$row ['QuestionID']] = $row ['Answer'];
	}
}

echo '<p><strong>Note: </strong>This is not an application form. This form will allow you to enter and save your data so that application forms will be pre-filled.</p>';

echo '<p style="font-weight:bold">';
echo '<a href="'.USERPORTAL_HOME.'index.php?OrgID='.$OrgID.'&MultiOrgID='.$MultiOrgID.'&navpg=profiles&navsubpg=appprofile&action=clearprofile">Clear Default Profile</a>';
echo '</p>';

if(isset($_GET['msg']) && $_GET['msg'] == 'succclearprofile') {
    echo '<p>';
    echo '<span style="color:green">Default profile successfully cleared</span>';
    echo '</p>';
}

require_once USERPORTAL_DIR . 'ApplicationProfile.inc';
?>