<?php
//Application Information
$requisition_fields =   array("RequisitionID", "JobID", "Title", "City", "State", "ZipCode");
//Set where condition for requisition
$where_req_info     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID");
//Set parameters for requisition
$params_req_info    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);

$columns            =   "DATE_FORMAT(EntryDate,'%Y-%m-%d %H:%i EST') EntryDate, ApplicantSortName, FormID, ProcessOrder, DispositionCode, VeteranEditStatus, DisabledEditStatus, AAEditStatus";
//Get Application Information
$application_info   =   $ApplicationsObj->getApplicationInformation($columns, $OrgID, $MultiOrgID, $RequestID, $ApplicationID);

//Get Requisition Information
$results_info       =   $RequisitionsObj->getRequisitionInformation($requisition_fields, $where_req_info, "", "", array($params_req_info));
$requisition_info   =   $results_info['results'][0];

//Get Irecruit Users Information
$columns            =   "FirstName, LastName, EmailAddress";
//Set where condition
$where              =   array("OrgID = :OrgID", "UserID = (SELECT owner FROM Requisitions WHERE OrgID = :ROrgID AND RequestID = :RequestID)");
//Set parameters
$params             =   array(":OrgID"=>$OrgID, ":ROrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get UserInformation
$results_owner      =   $IrecruitUsersObj->getUserInformation($columns, $where, "", array($params));
if(is_array($results_owner['results'][0])) list ( $First, $Last, $Email ) = array_values($results_owner['results'][0]);

//Get uploaded documents count
$vault_info         =   G::Obj('ApplicantVault')->getUploadedDocumentsCount($OrgID, $ApplicationID, $RequestID);
$attached_documents_count = $vault_info['count'];

$locations_list[] = $requisition_info['City'];
$locations_list[] = $requisition_info['State'];
$locations_list[] = $requisition_info['ZipCode'];

$aa_sec_info    =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $application_info['FormID'], "AA");
$vet_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $application_info['FormID'], "VET");
$dis_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $application_info['FormID'], "DIS");

$VDisplayTitle  =   $vet_sec_info['SectionTitle'];
$VDisplayType   =   $vet_sec_info['Active'];
$ADisplayTitle  =   $aa_sec_info['SectionTitle'];
$ADisplayType   =   $aa_sec_info['Active'];
$DDisplayTitle  =   $dis_sec_info['SectionTitle'];
$DDisplayType   =   $dis_sec_info['Active'];


echo '<div class="row">';
echo '<div class="col-lg-6">';


echo '<table cellspacing="0" cellpadding="0" width="100%" border="0" class="table">';

echo '<tr>';
echo '<td valign="top"><br>';
echo '<strong>ApplicationID: </strong>' . $_GET ['ApplicationID'];
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>';
echo '<strong>Job Title:</strong> ' . $requisition_info["Title"];
echo '</td>';
echo '</tr>';

echo '<tr>';
echo '<td>';
echo '<strong>Application Date: </strong>' . $application_info ['EntryDate'];
echo '</td>';
echo '</tr>';

echo '</table>';

echo '</div>';
echo '</div>';
?>
