<?php
require_once '../userportal.inc';

function _generateRandom($length=6)
{
	$_rand_src = array(array(65,90));
	srand ((double) microtime() * 1000000);
	$random_string = "";
	
	for($i = 0; $i < $length; $i++){
		$i1 = rand(0,sizeof($_rand_src)-1);
		$random_string .= chr(rand($_rand_src[$i1][0],$_rand_src[$i1][1]));
	}
	
	return $random_string;
}

$im     =   @imagecreatefromjpeg("captcha.jpg"); 
$rand   =   _generateRandom(6);

sleep(1);
$_SESSION['U']['captcha'] = $rand;

ImageString($im, 5, 2, 2, $rand[0].$rand[1].$rand[2].$rand[3].$rand[4].$rand[5],ImageColorAllocate ($im, 0, 0, 0));


header ('Content-type: image/jpeg');
imagejpeg($im, NULL, 100);
ImageDestroy($im);
?>
