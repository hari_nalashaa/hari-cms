<?php
require_once 'userportal.inc';

//First file doesn't require for now, but just keep it.
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';
include USERPORTAL_DIR . 'JobApplicationRequestVars.inc';

$purpose    =   $AttachmentsObj->getPurposeNamesList($OrgID, $_REQUEST['FormID']);

require_once USERPORTAL_DIR . 'Authenticate.inc';


if ($_REQUEST ['QuestionID'] != "" && $_REQUEST['ApplicationID'] != "" && $UpUserID != "") {

    // Get DateTime based on given format
    $HLD        =   $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
    
    $udir       =   IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments/';

    $att_info   =   $AttachmentsObj->getApplicantAttachmentInfo($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['QuestionID']);
    $ext        =   $att_info['FileType'];

    $filenameA  =   $udir . $_REQUEST['ApplicationID'] . '-' . $purpose [$_REQUEST['QuestionID']] . '.' . $ext;
    $filenameB  =   $udir . $_REQUEST['ApplicationID'] . '-' . $purpose [$_REQUEST['QuestionID']] . '-' . $HLD . '.' . $ext;
    
    @rename($filenameA, $filenameB);
    
    $der_res    =   $AttachmentsObj->delApplicantAttachments($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['QuestionID']);

    if($der_res['count'] > 0) {
	$processed_sections     =   unserialize(G::Obj('ApplicantDetails')->getAnswer($OrgID, $_REQUEST['ApplicationID'], "ProcessedSections"));
	foreach ($processed_sections AS $key => $value) {
   	  if ($value == $_REQUEST['SectionID']) {
      	    unset($processed_sections[$key]);
   	  }
	}
	$QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$_REQUEST['ApplicationID'], "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
	G::Obj('Applicants')->insUpdApplicantData($QI);

        echo json_encode(array("Status"=>"Success"));
        exit;
    }
    else {
        echo json_encode(array("Status"=>"Failed"));
        exit;
    }

} // end if
?>
