<?php
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

$ApplicationID =   $_REQUEST['ApplicationID'];

if ($ApplicationID == "") {
header("Location:index.php?OrgID=".$OrgID."&navpg=profiles&navsubpg=status");
exit;
}

$signature_rand_id      =   time();
$_SESSION['U']['SignatureRandID'] = $signature_rand_id;
$FormID                 =   $_REQUEST['FormID'];
$RequestID		=   $req_details_info['RequestID'];
$MultiOrgID             =   $req_details_info['MultiOrgID'];
$HoldID                 =   $OrgID.$UpUserID.$RequestID;

$APPDATA = G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);

$sec_child_ques_status  =   "false";

$validateallformfields  =   require_once USERPORTAL_DIR . 'ValidateAllFormFields.inc';
$errors_list            =   $validateallformfields['errors_list'];
$section_id_names       =   $validateallformfields['section_id_names'];

$page_title             =   "Sign and complete the application";

//Existing Sections based on current requisition
$req_sections_list      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
$req_sections_ids       =   array_keys($req_sections_list);

G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']  =   $APPDATA;

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Signature</h3>';
echo '</div>';
echo '</div>'; 


//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';


if(count($errors_list) > 0) {

    $back_link = USERPORTAL_HOME . 'editApplicationForm.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID;
    if ($MultiOrgID != "") { $link .= '&MultiOrgID=' . $MultiOrgID; }
    ?>
    <p style="text-align: center; font-size: 15px;">
    	Unable to process your application as you missed some required fields. Please fill those and submit again.
	<a href="<?php echo $back_link; ?>">Edit Application</a>
    </p>
    <?php
}
else if(isset($_POST['ProcessApplication']) && $_POST['ProcessApplication'] == "Yes" && count($errors_list)  == 0) {
    // Query and Set Text Elements
    $params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
    //Get text blocks information
    $where      =   array("OrgID = :OrgID", "FormID = :FormID");
    $results    =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));
    
    if(is_array($results['results'])) {
        foreach ($results['results'] as $row) {
            $TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
        }
    }
    
    if ($TextBlocks ["ApplicantAuthorizations"]) {
        $ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
    } else {
        $ApplicantAuthorizations = "";
    }
    ?>
    <form name="frmProcessApplication" id="frmProcessApplication" method="post" action="thankyouEdit.php">
        <?php 
        $Signature  = '<td width="18%"><font style="color:red">*</font> Electronic Signature: <font style="color:red">X</font></td>';
        $Signature .= '<td><input type="text" name="signature" id="signature" size="33" value="' . $APPDATA ['signature'] . '"></td>';
        
        if($ApplicantAuthorizations) {
        	echo '<b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '<br>';
        }
        if ($TextBlocks ["Signature"]) {
            echo $TextBlocks ["Signature"];
            echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
            echo '<tr>';
            echo '<td colspan="2">';
            echo '<input type="checkbox" name="agree" id="agree" value="Y"';
            if ($APPDATA ['agree'] == "Y") {
                echo ' checked';
            }
            if ($OrgID == "I20100301") { // Meritan
                echo '> <font style="color:red">*</font> I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.</td></tr>';
            } else {
                echo '> <font style="color:red">*</font> I agree with the above statement.</td></tr>';
            }
            echo '</td>';
            echo '<tr>';
            echo $Signature;
            echo '</tr>';
            echo '</table>';
        }
        if ($TextBlocks ["FormFooter"]) {
            echo $TextBlocks ["FormFooter"];
        }
        ?>
        <input type="hidden" name="ApplicationID" id="RequestID" value="<?php echo $ApplicationID;?>">
        <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>">
        <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">

        <input type="hidden" name="ProcessApplication" id="ProcessApplication" value="<?php echo $_REQUEST['ProcessApplication'];?>">
        <input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
        <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $MultiOrgID;?>">
        <input type="hidden" name="SectionID" id="SectionID" value="SIGNATURE">
        <input type="hidden" name="SignatureRandIDTime" id="SignatureRandIDTime" value="<?php echo $signature_rand_id;?>">
        <?php
        if(count($errors_list) == 0) {
            ?>
            <input type="button" name="btnProcessApplicationForm" id="btnProcessApplicationForm" value="Submit Application" onclick="return validateSignature(this)">
            &nbsp;
            <input type="submit" name="btnGoBack" id="btnGoBack" value="Go Back">
            <?php
        }
        ?>
        
        <div id="process_message"></div>
    </form>
    <?php
}

echo '<script type="text/javascript" src="js/process-job-application.js"></script>';
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';

require_once USERPORTAL_DIR . 'Footer.inc';
?>
