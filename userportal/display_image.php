<?php
require_once 'userportal.inc';

$Type = isset($_REQUEST['Type']) ? $_REQUEST['Type'] : '';

if (($OrgID) && ($Type)) {
	
	//set columns
	$columns = "OrgID, PrimaryLogoType, PrimaryLogo, SecondaryLogoType, SecondaryLogo";
	//Set where condition
	$where = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID");
	//Set parameters
	$params = array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
	//Get Organization Logos Information
	$results = $OrganizationsObj->getOrganizationLogosInformation($columns, $where, '', array($params));
	
	if(is_array($results['results'])) {
		foreach ($results['results'] as $img) {
			$orgid = $img ['OrgID'];
			$type1 = $img ['PrimaryLogoType'];
			$data1 = $img ['PrimaryLogo'];
			$type2 = $img ['SecondaryLogoType'];
			$data2 = $img ['SecondaryLogo'];
		}
	}
	
	if (($type1) && ($Type == "Primary")) {
		header ( "Content-type: $type1" );
		echo $data1;
		exit ();
	}
	
	if (($type2) && ($Type == "Secondary")) {
		header ( "Content-type: $type2" );
		echo $data2;
		exit ();
	}
} else {
	
	echo <<<END
<html>
<head><title></title></head>
<body></body>
</html>
END;
}
?>