<?php
require_once 'userportal.inc';

//Delete Location Hold By TimeInterval
G::Obj('Organizations')->delLocationHoldIdByTimeInterval();

if (isset ( $_COOKIE ['LOC'] )) {
	$LocHoldID = $_COOKIE ['LOC'];
} else {
	$LocHoldID = uniqid ( G::Obj('MysqlHelper')->getDateTime('%Y%m%d%H%m%s') );
	setcookie ( "LOC", $LocHoldID, time () + 3600, "/" );
}	

//Information to insert location hold data
$info = array("HoldID"=>$LocHoldID, "OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "ApplicationID"=>$_REQUEST['ApplicationID'], "RequestID"=>$_REQUEST['RequestID'], "Task"=>$_REQUEST['Task'], "source"=>$_REQUEST['source']);
G::Obj('Organizations')->insLocationHoldIdInfo($info);

require_once PUBLIC_DIR . 'SetMonsterSessionCookies.inc';

if ($_REQUEST ['action'] == "register") {
	$link = "signup.php?";
} else {
	$link = "login.php?";
}

$link .= 'OrgID=' . $OrgID;
if ($MultiOrgID != "") {
	$link .= '&MultiOrgID=' . $MultiOrgID;
}

header ( 'Location: ' . USERPORTAL_HOME . $link );
exit ();
?>
