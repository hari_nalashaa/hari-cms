<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation"
	style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="javascript:void(0);">
			<?php
			$OrganizationInformation = $OrganizationDetailsObj->getOrganizationInformation ( $OrgID, $MultiOrgID, "OrganizationName" );
			echo $OrganizationInformation ["OrganizationName"];
			?>
		</a>
	</div>

	<ul class="nav navbar-top-links navbar-right">
		<li class="dropdown"><a href="#" data-toggle="dropdown"
			class="dropdown-toggle fa-user-icon">
				<?php
				$abs_path = USERPORTAL_DIR . "vault/".$UpUserID."/profile_avatars/";
				if (file_exists ( $abs_path . $ProfileAvatarPicture )) {
					?><img src="<?php echo USERPORTAL_HOME . "vault/".$UpUserID."/profile_avatars/".$ProfileAvatarPicture;?>" width="29" height="29"><?php
				} else {
					?><img src="<?php echo USERPORTAL_HOME . "images/no-intern.jpg";?>" width="30" height="30"><?php
				}
				?>
				&nbsp;<?php 
				$user_info_name = $UserPortalUsersObj->getUserDetailInfoByUserID("FirstName, LastName, AdminStatus", $UpUserID);
				
				if($user_info_name['LastName'] != "") {
					echo $user_info_name['LastName'].", ";
				}
				echo $user_info_name['FirstName'];?>&nbsp;<i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<?php
				foreach ( $profiles_subsection as $profilessection => $profilesdisplaytitle ) {
					$profile_menu = true;
					if ($profilessection == "appprofile") {
						$img_class = "fa fa-user";
					} else if ($profilessection == "logininfo") {
						$img_class = "fa fa-info-circle";
					}
					
					if($profilesdisplaytitle != "") {
                        echo '<li>';
                        echo '<a href="' . USERPORTAL_HOME . 'index.php?navpg=profiles&navsubpg=' . $profilessection . '&OrgID='.$OrgID.'&MultiOrgID='.$MultiOrgID.'">';
                        echo '<i class="' . $img_class . '" style="float:left;margin-top:5px;margin-right:2px"></i>';
                        echo '<div style="width: 240px;float:left">' . $profilesdisplaytitle . '</div>';
                        echo '<i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>';
                        echo '</a>';
                        echo '</li>';
					}
				}
				?>
				<li>
					<a href="https://help.myirecruit.com/" target="_blank">
						<i class="fa fa-question-circle" style="float:left;margin-top:5px;margin-right:2px"></i>
						<div style="width: 240px;float:left">Applicant Help</div>
						<i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>
					</a>
				</li>
				<li>
					<a href="<?php echo USERPORTAL_HOME?>logout.php?OrgID=<?php echo $OrgID;?>&MultiOrgID=<?php echo $MultiOrgID?>">
						<i class="fa fa-sign-out" style="float:left;margin-top:5px;margin-right:2px"></i>
						<div style="width: 240px;float:left">Logout</div>
						<i class="fa fa-chevron-circle-right" style="float:right;margin-top:5px;"></i>
					</a>
				</li>

			</ul> <!-- /.dropdown-user --></li>
	</ul>

	<!-- /.navbar-header -->
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">
				<?php
				$INT = $FormFeaturesObj->getInternationalTranslation ( $OrgID );
				
				if ($INT ['InternationalTranslation'] == "Y") {
					?>
					<li>
					<script>
					function googleTranslateElementInit() {
					  new google.translate.TranslateElement({
					    pageLanguage: 'en',
					    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
					  }, 'google_translate_element');
					}
					</script>
					<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					<div id="google_translate_element" style="margin-left: 20px"></div>
					</li>
					<?php
				}

				foreach ( $navsection as $section => $displaytitle ) {
                    if($displaytitle != "") {
                            echo '<li>';
    
                            if ($section == "listings") {
                            
                                echo '<a href="' . USERPORTAL_HOME . 'index.php?OrgID=' . $OrgID;
                                if ($MultiOrgID != "") {
                                    echo '&MultiOrgID=' . $MultiOrgID;
                                }
                                echo '&navpg=listings">';
                            
                                echo '<i class="fa fa-search"></i>&nbsp;' . $displaytitle . '<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
                            }
                            if ($section == "status") {
                            
                                echo '<a href="' . USERPORTAL_HOME . 'index.php?OrgID=' . $OrgID;
                                if ($MultiOrgID != "") {
                                    echo '&MultiOrgID=' . $MultiOrgID;
                                }
                                echo '&navpg=profiles&navsubpg=status">';
                            
                                echo '<i class="fa fa-check-square"></i>&nbsp;' . $displaytitle . '<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
                            }
                            	
                            echo '</a>';
                            echo '</li>';
                    }
				}

				$user_portal_info   =   G::Obj('UserPortalInfo')->getUserPortalInfoByOrgIDMultiOrgID ( $OrgID, $MultiOrgID );
				$UPTLINFO           =   $user_portal_info ['results'];
				$hitPTL             =   $user_portal_info ['count'];
				
				if (isset ( $UPTLINFO ) && is_array ( $UPTLINFO ))
				{	
                    if($UPTLINFO[0]['DisplayTitle'] != "") {
                    	echo '<li>';
                    
                        echo '<a href="javascript:void(0);">';
                        echo '<i class="fa fa-wordpress"></i>';
                        echo "&nbsp;&nbsp;" . $UPTLINFO[0]['DisplayTitle'] . '<span class="fa fa-chevron-circle-right" style="float:right;font-size:12px"></span>';
                        echo '</a>';
                        
                        $user_portal_info = $UserPortalInfoObj->getUserPortalInfoByOrgIDMultiOrgID ( $OrgID, $MultiOrgID, 1 );
                        $UPTLINFO         = $user_portal_info ['results'];
                        
                        echo '<ul class="nav nav-second-level">';
                        if (isset ( $UPTLINFO ) && is_array ( $UPTLINFO ))
                        {
                            foreach ( $UPTLINFO as $UPTLKEY => $PTLINFO ) {
                                if($PTLINFO ['DisplayTitle'] != "") {
                                    echo '<li>';
                        
                                    if ($PTLINFO ['TypeLink'] != "Main Section Title") {
                                         
                                        echo '<a href="';
                                         
                                        if ($PTLINFO ['TypeLink'] == "External Link") {
                                            echo $PTLINFO ['URL'];
                                        }
                                        if ($PTLINFO ['TypeLink'] == "Attachment") {
                                            echo 'display_portalinfoattachment.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&updateid=' . $PTLINFO ['UpdateID'];
                                        }
                                        if ($PTLINFO ['TypeLink'] == "HTML Page") {
                                            echo USERPORTAL_HOME . 'index.php?navpg=portalinfohtml&updateid=' . $PTLINFO ['UpdateID'] . '&OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID;
                                        }
                                         
                                        echo '"';
                                         
                                        if ($PTLINFO ['TypeLink'] == "External Link") {
                                            echo ' target="_blank"';
                                        }
                                         
                                        echo '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                    }
                                     
                                    echo $PTLINFO ['DisplayTitle'];
                                    // echo '</b>';
                                    if ($PTLINFO ['TypeLink'] != "Main Section Title") {
                                        echo '</a>';
                                    }
                                    echo '</li>';
                        
                                }
                            }
                        }
                        echo '</ul>';
                        echo '</li>';
                    }
				}				
						
						
				echo '<li>';
				
				echo '<a href="https://help.myirecruit.com/" target="_blank">';
				
				echo '<i class="fa fa-question-circle"></i>&nbsp;Applicant Help<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
				
				echo '</a>';
				echo '</li>';

				$current_login_user_info = $UserPortalUsersObj->getUserPortalUserInfo();
				
                if($current_login_user_info['AdminStatus'] == 'Y') {
                	echo '<li>';
                    echo '<a href="' . USERPORTAL_HOME . 'userPortalMaster.php">';
                    echo '<i class="fa fa-gear"></i>&nbsp;Master Access<i class="fa fa-chevron-circle-right" style="float:right;"></i>';
                    echo '</a>';
                    echo '</li>';
                }
				
				// botom navigation
				if (isset($bottomnav [$navpg])) {
					echo '<li class="home">' . $bottomnav [$navpg] . '</li>';
				} // end bottomnav
				?>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>
