<?php
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

//Get requisition details
$req_info               =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID, Active", $OrgID, $_REQUEST ['RequestID']);
$RequestID              =   $_REQUEST ['RequestID'];
$FormID                 =   $req_info['FormID'];
$MultiOrgID             =   $req_info['MultiOrgID'];
$RequisitionStatus      =   $req_info['Active'];
$HoldID                 =   $OrgID.$UpUserID.$_REQUEST ['RequestID'];

//Processed Sections
$processed_sections     =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");

//Get Text From TextBlocks
$Response               =   G::Obj('FormFeatures')->getTextFromTextBlocks($OrgID, $FormID, 'Response');

if($processed_sections  == NULL || $processed_sections == "" || empty($processed_sections) || !isset($processed_sections)) {
    $processed_sections =   array();
}
else {
    $processed_sections =   unserialize($processed_sections);
}

$APPDATA                =   array ();
$APPDATAREQ             =   array ();

$results                =   G::Obj('UserPortalInfo')->getApplicationInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST ['RequestID']);

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        $APPDATA [$row ['QuestionID']] = $row ['Answer'];
    } // end foreach
}

$attachment_types       =   array();
$attachments_info       =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST ['RequestID']);
for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[] =  $attachments_info[$aai]['TypeAttachment'];
}

//Existing Sections based on current requisition
$req_sections_list      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
$req_sections_ids       =   array_keys($req_sections_list);

G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']  =   $APPDATA;

//Prefill the $FILES
$FILES                      =   $_FILES;
$attachment_types           =   array();
$attachments_info           =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);

for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[]     =   $attachments_info[$aai]['TypeAttachment'];
    
    $file_type              =   $attachments_info[$aai]['FileType'];
    $file_attachment_name   =   $attachments_info[$aai]['TypeAttachment'];
    $file_name              =   $file_attachment_name.".".$file_type;
    
    $FILES[$attachments_info[$aai]['TypeAttachment']]   =   array(
                                                                "name"  =>  $attachments_info[$aai]['TypeAttachment'],
                                                                "type"  =>  $file_type
                                                            );
}

$sec_child_ques_status  =   "false";

$userportal_sections_list	=   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
$validateallformfields  =   require_once USERPORTAL_DIR . 'ValidateAllFormFields.inc';
$errors_list            =   $validateallformfields['errors_list'];
$section_id_names       =   $validateallformfields['section_id_names'];

if(isset($_POST['ProcessApplication']) 
    && $_POST['ProcessApplication'] == "Yes" 
    && count($errors_list)  == 0) {
	require_once USERPORTAL_DIR . 'ProcessUserPortalApplicationThankYou.inc';
}

//Get Wotc Ids list
$wotcid_info = G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $MultiOrgID);

$page_title = "Thank you for applying for this postion";

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

$LISTINGS_URL = strtolower ( USERPORTAL_HOME );

require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

echo '<style>
.modal {
  display: none;
  position: fixed;
  z-index: 9999;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: rgb(0,0,0);
  background-color: rgba(0,0,0,0.4);
}
.modal-content {
  background-color: #fefefe;
  margin: 15% auto;
  padding: 20px;
  border: 1px solid #888;
  width: 90%;
}
.user_portal_modal_close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}
.user_portal_modal_close:hover,
.user_portal_modal_close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
</style>
';

echo '<script>';
echo 'function showPopUp() {';
echo 'document.getElementById("user_portal_modal").style.display = "block";';
echo '}';

echo 'function closePopUp() {';
echo 'document.getElementById("user_portal_modal").style.display = "none";';
echo '}';
echo '</script>';

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
	echo '<div class="row">';//Row Start
	echo '<div class="col-lg-12">';
	echo '<h3 class="page-header">Thank you</h3>';
	echo '</div>';
	echo '</div>'; 
}

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

$show_popup	= "false";

if(isset($_GET['msg']) && $_GET['msg'] == 'reqexpired') {
    ?>
    <div style="height:200px;">
    <p style="text-align: center;padding-top:30px;">
        <?php 
        echo "This requisition has expired and is no longer active.";
        ?>   	
    </p>
    </div>
    <?php
}
else if(count($errors_list) > 0) {
    ?>
    <p style="text-align: center;">
    	Unable to process your application as you missed some required fields. Please fill those and submit again.
    	<a href="<?php echo USERPORTAL_HOME;?>jobApplication.php?RequestID=<?php echo $_REQUEST ['RequestID'];?>&OrgID=<?php echo $OrgID;?>&MultiOrgID=<?php echo $MultiOrgID;?>">Edit Application</a>
    </p>
    <?php
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'suc') {
	$show_popup = "true";
    ?>
    <p style="text-align: left !important;">
    	Thank you for applying for <strong><?php echo $req_info['Title'];?></strong>
    	position. 
    	<br>Your <strong>Application ID: </strong><?php echo htmlspecialchars($_REQUEST['ApplicationID']);?>.<br>
    	You will receive confirmation mail with ApplicationID. <br><br>
    	<?php
    	   echo $Response;
    	
    	   if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") {
    	       ?><br> <a href="<?php echo htmlspecialchars(USERPORTAL_HOME) . "index.php?OrgID=".htmlspecialchars($OrgID)."&MultiOrgID=".htmlspecialchars($_REQUEST['MultiOrgID'])."&navpg=profiles&navsubpg=status"; ?>">You can check your status here</a><?php
    	   }
    	   else {
    	       ?><br> <a href="<?php echo htmlspecialchars(USERPORTAL_HOME) . "index.php?OrgID=".htmlspecialchars($OrgID)."&navpg=profiles&navsubpg=status"; ?>">You can check your status here</a><?php
    	   }
    	?>
    </p>
    <?php
    echo '<div class="row">';
	echo '<div class="col-lg-12 col-md-12 col-sm-12">';

	if($wotcid_info['wotcID'] != "") {		
		//WOTC Form Popup Code
		echo '<a href="javascript:void(0);" onclick="showPopUp()">';
		echo 'There is a final form to submit. Please click here to submit the WOTC Form';
		echo '</a>';
	}
	
	echo '</div>';
	echo '</div>';
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'duplicate') {

    echo '<br><br><br><br>';
    echo '<h3 style="text-align:center">';
    echo 'You have already submitted the application for this requisition.';
    echo '<br>';
    echo '</h3>';
    
    if(isset($req_info['MultiOrgID']) && $req_info['MultiOrgID'] != "") {
        ?><h5 style="text-align:center"><a href="<?php echo USERPORTAL_HOME . "index.php?OrgID=".$_REQUEST['OrgID']."&MultiOrgID=".$req_info['MultiOrgID']."&navpg=profiles&navsubpg=status"; ?>">Click here to view the application</a></h5><?php
    }
    else {
       ?><h5 style="text-align:center"><a href="<?php echo USERPORTAL_HOME . "index.php?OrgID=".$_REQUEST['OrgID']."&navpg=profiles&navsubpg=status"; ?>">Click here to view the application</a></h5><?php
    }
    
    echo '<h5 style="text-align:center">';
    echo $req_info['Title'];
    echo '</h5>';
    echo '<br><br><br><br><br>';

}

if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $IrecruitApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $IrecruitRequestID = $_REQUEST['RequestID'];
?>

<div id="user_portal_modal" class="modal">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<div class="modal-content">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<span class="user_portal_modal_close" onclick="closePopUp()">close &times;</span>
					</div>
				</div>		
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12" id="div_wotc_form">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
if($wotcid_info['wotcID'] == ""
	|| is_null($wotcid_info['wotcID']))
{
	$show_popup	= "false";
}

if($show_popup == "true") {
	?>
	<script type="text/javascript">
	function loadWOTCForm(IrecruitApplicationID, IrecruitRequestID) {
		showPopUp();
	
		var request = $.ajax({
			async: true,   // this will solve the problem
			method: "POST",
	  		url: "wotcForm.php?IrecruitRequestID="+IrecruitRequestID+"&IrecruitApplicationID="+IrecruitApplicationID,
			type: "POST",
			beforeSend: function() {
				$("#div_wotc_form").html('Please wait.. <img src="images/loading-small.jpg"/><br><br>');
			},
			success: function(data) {
				$("#div_wotc_form").html(data);
	    	}
		});
	
	}
	
	loadWOTCForm('<?php echo htmlspecialchars($IrecruitApplicationID);;?>', '<?php echo htmlspecialchars($IrecruitRequestID);?>');
	</script>
	<?php
}

echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
require_once USERPORTAL_DIR . 'Footer.inc';
?>