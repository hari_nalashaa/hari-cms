<?php
require_once 'userportal.inc';

//First file doesn't require for now, but just keep it.
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';
include USERPORTAL_DIR . 'JobApplicationRequestVars.inc';

$req_details_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Active, DATE(ExpireDate) as ExpireDate", $_REQUEST['OrgID'], $_REQUEST ['RequestID']);
$dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);

if($req_details_info['Active'] == 'N'
    || $dates_diff > 0) {
   header("Location:thankyou.php?RequestID=".$_REQUEST ['RequestID']."&OrgID=".$_REQUEST['OrgID'].'&msg=reqexpired');
   exit;
}

if(!isset($_REQUEST['RequestID']) || $_REQUEST['RequestID'] == "") {
    if(isset($MultiOrgID) && $MultiOrgID != "") {
        header("Location:index.php?OrgID=".$OrgID."&navpg=listings&MultiOrgID=".$MultiOrgID);
        exit;
    }
    else {
        header("Location:index.php?OrgID=".$OrgID."&navpg=listings");
        exit;
    }
}

require_once USERPORTAL_DIR . 'Authenticate.inc';

$formtable          =   "FormQuestions";
$title              =   "Thank you for applying for this postion";

$APPDATA            =   array ();
$APPDATAREQ         =   array ();

//Update Default Theme
G::Obj('UserPortalInfo')->updOrgUserPortalMainDefaultTheme($OrgID);

//Get requisition details
$requisition_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $OrgID, $_REQUEST['RequestID']);
$FormID             =   $requisition_info['FormID'];
$HoldID             =   $OrgID.$UpUserID.$RequestID;

$personal_ques_info =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, "1");
$child_ques_info    =   G::Obj('ApplicationFormQuestions')->getFormChildQuesForParentQues($OrgID, $FormID, "1");

foreach($personal_ques_info as $personal_que_id=>$personal_que_info) {
    $que_types_list[$personal_que_id]   =   $personal_que_info['QuestionTypeID'];
}

//Get UserApplications
$user_apps_info         =   G::Obj('UserPortalInfo')->getUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
$user_applications      =   $user_apps_info['results'];
$user_applications_cnt  =   $user_apps_info['count'];

if($user_applications_cnt > 0) {
    if($user_applications[0]['Status'] == 'Finished') {
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            header("Location:duplicateApplication.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&MultiOrgID=".$MultiOrgID);
            exit;
        }
        else {
            header("Location:duplicateApplication.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']);
            exit;
        }
    }
}

// Query and Set Text Elements
$params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
//Get text blocks information
$where      =   array("OrgID = :OrgID", "FormID = :FormID");
$results    =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));

if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
    }
}

//Check requisition expired or not
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") {
   
    //Set condition
    $where 	 =  array("OrgID = :OrgID", "RequestID = :RequestID", "PostDate < NOW()", "ExpireDate > NOW()");
    //Set parameters
    $params  =  array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
    
    $reqs_info_results = G::Obj('Requisitions')->getRequisitionInformation("Title", $where, "", "", array($params));
    
    $req_info = $reqs_info_results['results'][0];
    
    if($req_info['Title'] == "") {
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            header("Location:expiredRequisition.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&MultiOrgID=".$MultiOrgID);
            exit;
        }
        else {
            header("Location:expiredRequisition.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']);
            exit;
        }
    }
}

//Validate Prescreen Questions Information
$prescreen_res_where    =   array("OrgID = :OrgID", "RequestID = :RequestID", "HoldID = :HoldID");
$prescreen_res_params   =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID'], ":HoldID"=>$OrgID.$UpUserID.$_REQUEST['RequestID']);
$prescreen_res_results  =   G::Obj('PrescreenQuestions')->getPrescreenResults("*", $prescreen_res_where, "SortOrder", array($prescreen_res_params));
$prescreen_results      =   $prescreen_res_results['results'];

$prescreen_answers      =   array();
for($pr = 0; $pr < count($prescreen_results); $pr++) {
    $prescreen_answers[$prescreen_results[$pr]['SortOrder']] = $prescreen_results[$pr]['Submission'];
}

$ReqMultiOrgID      =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

if ($feature ['InternalRequisitions'] == "Y") {
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
}

//Set parameters for prepared query
$params = array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Set condition
$where  = array("OrgID = :OrgID", "RequestID = :RequestID");

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
    $where[] = "Internal IN ('Y','B')";
} else {
    $where[] = "Internal IN ('N','B','')";
}

//Get Prescreen Screen Question Information
$pre_screen_que_info    =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation ("*", $where, "SortOrder", array($params));
$questionsavail         =   $pre_screen_que_info ["count"];

$prescreen_errors       =   "false";
if($questionsavail > 0) {

    // loop through all questions for a match. If one doesn't it will throw a not qualify
    if (is_array ( $pre_screen_que_info ['results'] )) {
        foreach ( $pre_screen_que_info ['results'] as $PRESCREEN ) {

            if ($PRESCREEN ['Answer'] != $prescreen_answers[$PRESCREEN['SortOrder']]) {
                $prescreen_errors = "true";
            }

        } // end foreach
    }

    if($prescreen_errors == "true") {
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            header("Location:prescreen.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&MultiOrgID=".$MultiOrgID);
            exit;
        }
        else {
            header("Location:prescreen.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']);
            exit;
        }
    }
}

$RESUME_PARSED_APPDATA = array();
//Check for resume parsing option
if($feature['ResumeParsing'] == "Y") {
    $parse_userportal_resume    =   $UserPortalInfoObj->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ParseResume");
    if($parse_userportal_resume == NULL || $parse_userportal_resume == "" || empty($parse_userportal_resume) || !isset($parse_userportal_resume)) {
        
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            header("Location:uploadResume.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&MultiOrgID=".$MultiOrgID);
            exit;
        }
        else {
            header("Location:uploadResume.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']);
            exit;
        }
        
    }
    else if($parse_userportal_resume == "Yes") {
    	
        $RESUME_PARSED_APPDATA  =   $SovrenObj->getUserPortalParsedResumeInfo($OrgID, $UpUserID, $_REQUEST['RequestID']);
    }
}

if(count($RESUME_PARSED_APPDATA) == 0) {
    //To Prefill the application form data with the profile data
    $profile_data_info = $UserPortalUsersObj->getProfileDataByUserIDProfileID($UpUserID, 'STANDARD');
    
    foreach($profile_data_info as $profile_data_key=>$userportal_profile_data) {
        $APPDATA [$userportal_profile_data ['QuestionID']] = $userportal_profile_data ['Answer'];
    }
}

//Overwrite the APPDATA empty values with resume parsed data
$APPDATA_KEYS = array();
if(is_array($APPDATA) && count($APPDATA) > 0) {
    $APPDATA_KEYS = array_keys($APPDATA);
    foreach($APPDATA as $APPDATA_QUE=>$APPDATA_ANS) {
        if($APPDATA_ANS == "") {
            $APPDATA[$APPDATA_QUE] = $RESUME_PARSED_APPDATA[$APPDATA_QUE];
        }
    }
}


//Add the questions that doesn't exist in APPDATA
if(is_array($RESUME_PARSED_APPDATA) && count($RESUME_PARSED_APPDATA) > 0) {
    foreach($RESUME_PARSED_APPDATA as $RP_APPDATA_QUE=>$RP_APPDATA_ANS) {
        if(!in_array($RP_APPDATA_QUE, $APPDATA_KEYS)) {
            $APPDATA[$RP_APPDATA_QUE] = $RESUME_PARSED_APPDATA[$RP_APPDATA_QUE];
        }
    }
}

//Get application information saved by applicant and it will overwrite the existing data
$results = G::Obj('UserPortalInfo')->getApplicationInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        $APPDATA [$row ['QuestionID']] = $row ['Answer'];
    } // end foreach
}

//Set applicant_name
$applicant_name         =   str_replace("  ", " ", $APPDATA['last'] . '  ' . $APPDATA['middle'] . ' ' . $APPDATA['first']);

//Get Applicant attachments information
$attachment_types       =   array();
$attachments_info       =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[] =   $attachments_info[$aai]['TypeAttachment'];
}

//Prefill the $FILES
$attachment_types           =   array();
$attachments_info           =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[]     =   $attachments_info[$aai]['TypeAttachment'];
    
    $file_type              =   $attachments_info[$aai]['FileType'];
    $file_attachment_name   =   $attachments_info[$aai]['TypeAttachment'];
    $file_name              =   $file_attachment_name.".".$file_type;
    
    $FILES[$attachments_info[$aai]['TypeAttachment']]   =   array(
        "name"  =>  $attachments_info[$aai]['TypeAttachment'],
        "type"  =>  $file_type
    );
}

//Validate the information
G::Obj('ValidateApplicationForm')->FORMDATA['FILES']     =   $FILES;

require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Application Form:</h3>';
echo '</div>';
echo '</div>';

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';
?>

<div>
    <h4><?php echo $requisition_info['Title'];?>:</h4>
    <span style="color:red">*</span>&nbsp;Please click on each section title and complete each section. Click on the Save/Next button to go to the next section. Click on the Sign Application button after all the sections are completed. All sections with required questions must be completed before the application can be submitted.
    <br><br>
    <?php
    if (isset($TextBlocks ["FormHeader"]) && $TextBlocks ["FormHeader"] != "") {
        echo "<br>".$TextBlocks ["FormHeader"]."<br>";
    }
    ?>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['CompletedTab'];?>;color: <?php echo "#".$up_app_theme_info['CompletedTab'];?>;"></div> 
    <div style="float:left;">&nbsp;Completed Section&nbsp;</div>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['PendingTab'];?>;color: <?php echo "#".$up_app_theme_info['PendingTab'];?>;"></div>
    <div style="float:left;">&nbsp;Pending Section&nbsp;</div>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['ActiveTab'];?>;color: <?php echo "#".$up_app_theme_info['ActiveTab'];?>;"></div> 
    <div style="float:left;">&nbsp;Active Section&nbsp;</div>
    <div style="clear: both"></div>
</div>

<ul class="application_steps" id="application_steps">
    <?php
        foreach ($userportal_sections_list as $section_id=>$section_info) {
            $section_title  =   $section_info['SectionTitle'];
            ?>
            <li>
                <a href="javascript:void(0);" class="<?php if(!in_array($section_id, $section_id_names)) echo 'application_step_tabs_filled'; else echo 'application_step_tabs';?>" id="section_id<?php echo $section_id;?>">
                    <?php echo $section_title;?>
                </a>
            </li>
            <?php
        }
    ?>
</ul>
<br>
<div class="application_process">
    <?php
    $keys   =   array_keys($userportal_sections_list);
    $uic    =   0;
    
    foreach($userportal_sections_list as $section_id=>$section_info) {
        
        $section_title      =   $userportal_sections_list[$section_id]['SectionTitle'];
        $next_section_id    =   "";

        if(($uic+1) != count($userportal_sections_list)) {
            $next_section_id    =   $keys[array_search($section_id, $keys) + 1];
        }
        ?>
        <div class="application_process_content" id="section_content<?php echo $section_id;?>">
            <form name="<?php echo $section_forms[$section_id];?>" id="<?php echo $section_forms[$section_id];?>" method="post" enctype="mutipart/form-data">
                <?php
                if($section_id == 6) {
                    $application_form = include COMMON_DIR . "application/Attachments.inc";
                    echo $application_form;
                }
                else if($section_id == 14) {
                    $application_form = include COMMON_DIR . "application/ApplicantProfilePicture.inc";
                    echo $application_form;
                }
                else {
                    $application_form = include COMMON_DIR . "application/ApplicationSectionInfo.inc";
                    echo $application_form;
                }
                ?>
                <br>
                <input type="hidden" name="SectionID" id="SectionID" value="<?php echo $section_id;?>">
                <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $_REQUEST['RequestID'];?>">
                <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
                <input type="hidden" name="process" id="process" value="Y">
                <?php 
                if($uic == 0) {
                	?>
                    <input type="hidden" name="ClientIPAddress" id="ClientIPAddress" value="<?php echo $ServerInformationObj->getIPAddress();?>">
                	<?php
                }
                ?>
                <input type="button" name="btnInfo" id="btnInfo" value="Save/Next" onclick="processApplicationForm('<?php echo $section_forms[$section_id];?>', 'section_id<?php echo $section_id;?>', '<?php echo $next_section_id;?>');">
            </form>
        </div>
        <?php
        $uic++;
    }
    ?>
    <div id="process_message"></div>
</div>
<br>
					
<div id="process_finish_button_validation"></div>

<br>
<div>
    <form name="frmProcessApplication" id="frmProcessApplication" method="post" action="signature.php">
        <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $_REQUEST['RequestID'];?>">
        <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
        <input type="hidden" name="ProcessApplication" id="ProcessApplication" value="Yes">
        <input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
        <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $MultiOrgID;?>">
        <?php 
        $user_sec_list          =   array();
        for($ui = 0; $ui < count($userportal_sections_list); $ui++) {
            $section_id         =   $userportal_sections_list[$ui]['SectionID'];
            $user_sec_list[]    =   $section_id;
        }
        
        $processed_sections     =   $UserPortalInfoObj->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");
        if($processed_sections == '') $processed_sections = serialize(array());
        $processed_sections     =   unserialize($processed_sections);

        $sign_application       =   "true";
        foreach($user_sec_list as $user_sec_id) {
        	if(is_array($processed_sections) && !in_array($user_sec_id, $processed_sections)) {
                $sign_application   =   "false";		
        	}
        }
        
        if(count($errors_list) == 0 && $sign_application == "true") {
            ?><input type="submit" name="btnProcessApplicationForm" id="btnProcessApplicationForm" value="Sign Application"><?php
        }
        else {
        	?><input type="button" name="btnProcessApplicationForm" id="btnProcessApplicationForm" class="btnDisabled" value="Sign Application"><?php
        }
        ?>
    </form>
    <?php
    if (isset($TextBlocks ["FormFooter"]) && $TextBlocks ["FormFooter"] != "") {
        echo "<br>".$TextBlocks ["FormFooter"]."<br>";
    }
    ?>
    <span style="color:red">*</span>&nbsp;Please click on each section title and complete each section. Click on the Save/Next button to go to the next section. Click on the Sign Application button after all the sections are completed. All sections with required questions must be completed before the application can be submitted.
</div>

<script type="text/javascript" src="js/process-job-application.js"></script>

<?php
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
require_once USERPORTAL_DIR . 'Footer.inc';
?>
<script>
function manageCoverLetterOther(ocval, at_type) {

	if(at_type == "coverletterupload") {
		if(ocval == "") {
			document.getElementById('div_new_coverletterupload').style.display = 'block';
		}
		else {
		    document.getElementById('div_new_coverletterupload').style.display = 'none';
		}
	}
	else if(at_type == "otherupload") {
		if(ocval == "") {
			document.getElementById('div_new_otherupload').style.display = 'block';
		}
		else {
			document.getElementById('div_new_otherupload').style.display = 'none';
		}
	}
	else if(at_type == "resumeupload") {
		if(ocval == "") {
			document.getElementById('div_new_resumeupload').style.display = 'block';
		}
		else {
			document.getElementById('div_new_resumeupload').style.display = 'none';
		}
	}
	
}

var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info	=	JSON.parse('<?php echo json_encode($child_ques_info);?>');
</script>
<script src="<?php echo PUBLIC_HOME;?>js/child-questions.js" type="text/javascript"></script>
