<?php
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';

$colwidth   =   "220";
$highlight  =   '#' . $COLOR ['ErrorHighlight'];
$formtable  =   "FormQuestions";

if ($HoldID) {
    $APPDATA    =   array ();
    $APPDATAREQ =   array ();
	
	$results   =   G::Obj('ApplicantDataTemp')->getApplicantDataTempInfo($OrgID, $HoldID);
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $row) {
			$APPDATA [$row ['QuestionID']] = $row ['Answer'];
			$APPDATAREQ [$row ['QuestionID']] = $row ['Required'];
		} // end foreach
	}	
	
    $RequestID 	=   $APPDATA ['RequestID'];
    $FormID    	=   $APPDATA['FormID'];
    $HoldID	   	=   $APPDATA ['HoldID'];
    $source    	=   $APPDATA ['source'];
    $MultiOrgID	=   $APPDATA ['MultiOrgID'];
} // end HoldID

$form_sections_info =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

foreach ( $form_sections_info as $form_section_id=>$form_section_info) {
    $TITLES [$form_section_id] = $form_section_info['SectionTitle'];
}
    
if (FROM_SRC == "USERPORTAL") {

	$SCRIPT = $_SERVER ['PHP_SELF'];
	
	if ($ProfileID) {
		$Submit = '<p align="center"><input type="hidden" name="ProfileID" value="' . $ProfileID . '">';
		$Submit .= '<input type="hidden" name="RequestID" value="' . $RequestID . '">';
		$Submit .= '<input type="hidden" name="process" value="Update Profile Information">';
		$Submit .= '<input type="hidden" name="navpg" value="profiles">';
		$Submit .= '<input type="hidden" name="navsubpg" value="appprofile">';
		$Submit .= '<input type="hidden" name="HoldID" value="' . $HoldID . '">';
		
		$Submit .= '<button id="show" onclick="ShowProgressAnimation();submit();">Create Profile</button></p>';
	} // end ProfileID
} // end pg = profile

if ($pg == "application") {
	$SCRIPT = $_SERVER ['PHP_SELF'];
	$Submit = '<input type="hidden" name="HoldID" value="' . $HoldID . '">';
	$Submit .= '<button id="show" onclick="ShowProgressAnimation();submit();">Submit Application Form</button></p>';
} // end pg = application

if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] ) || preg_match ( '/getApplicationEditForm.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	$SCRIPT = 'applicants.php';

	$APPDATA = $ApplicantsObj->getAppData ( $OrgID, $ApplicationID );
	
	$HoldID = $APPDATA ['HoldID'];
	$source = $APPDATA ['source'];
	$Submit .= '<br><p align="center">Reason for updating information.<br /><textarea name="Comment" class="mceEditor" rows="5" cols="50" wrap="yes"></textarea></p><p align="center"><input type="hidden" name="action" value="applicationedit"><input type="hidden" name="ApplicationID" value="' . $ApplicationID . '"><input type="hidden" name="RequestID" value="' . $RequestID . '"><input type="hidden" name="process" value="Y">';
	$Submit .= '<input type="hidden" name="HoldID" value="' . $HoldID . '">';
	if($action == "applicationedit") {
		$Submit .= '<input type="button" id="show" onclick="processApplicationInfo();" class="btn btn-primary" value="Save Changes"/></p>';
	}
	else {
		$Submit .= '<button id="show" onclick="ShowProgressAnimation();submit();">Submit Changes</button></p>';
	}
} // end preg_match applicants

if ($OrgID == 'I20090304') {
	
	echo <<<END
<style type="text/css">
body { 
  font-size:12pt; 
}
</style>
END;
}

$ReqMultiOrgID = G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

if ($feature ['InternalRequisitions'] == "Y") {
    //Get InternalCode
	$InternalCode   =   G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID($OrgID);
}

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
	$typeform = "InternalFormID";
} else {
	$typeform = "FormID";
}

$Form   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("InternalFormID, FormID", $OrgID, $RequestID);
$FormID =   $Form [$typeform];

// for internal view display of application
if (FROM_SRC == "IRECRUIT") {
	if ($_GET ['FormID']) {
		$FormID = $_GET ['FormID'];
	}
}

if (FROM_SRC == "USERPORTAL") {
	if ($navpg == "profiles") {
		$FormID = "STANDARD";
	}
}
  
// Query and Set Text Elements
$params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
//Get text blocks information
$where      =   array("OrgID = :OrgID", "FormID = :FormID");
$results    =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));

if(is_array($results['results'])) {
	foreach ($results['results'] as $row) {
		$TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
	}
}
?>
<script>
function processApplicationInfo() {
	
		$("#edit_application_progress").html('<img src="'+irecruit_home+'images/wait.gif"/> Please wait...');
		
		var input_data = new FormData($('#application')[0]);
		
		var request = $.ajax({
			method: "POST",
	  		url: 'applicants/processApplicationFormInfo.php', 
			data: input_data,
			type: "POST",
			processData: false,
			contentType: false,
			success: function(data) {
				console.log(data);
				$("#edit_application_progress").html(data);
	    	}
		});
}
</script>
<?php
if(isset($_GET['msg']) && $_GET['msg'] == 'succ') {
	echo "<span style='color:blue'>Profile updated successfully</span>";
}

echo '<form action="' . $SCRIPT . '" method="POST" name="application" id="application" enctype="multipart/form-data">' . "\n";
echo '<input type="hidden" name="pg" value="' . $pg . '">';
echo '<input type="hidden" name="subpg" value="' . $subpg . '">';

if (($OrgID == "I20130812") || ($OrgID == "xB12345467")) {
	// 10mb
	$max_file_size = "10485760";
} else {
	// 1mb
	$max_file_size = "1048576";
}

echo '<input type="hidden" name="MAX_FILE_SIZE" value="' . $max_file_size . '">';
echo '<input type="hidden" name="FormID" value="' . $FormID . '">' . "\n";
echo '<input type="hidden" name="HoldID" value="' . $HoldID . '">' . "\n";
// here
echo '<input type="hidden" name="process" value="Y">' . "\n";
echo '<input type="hidden" name="OrgID" value="' . $OrgID . '">';
// here
if ($MultiOrgID) {
	echo '<input type="hidden" name="MultiOrgID" value="' . $MultiOrgID . '">' . "\n";
}
echo '<input type="hidden" name="source" value="' . $source . '">' . "\n";
if ($USERID) {
	echo '<input type="hidden" name="PortalUserID" value="' . $USERID . '">' . "\n";
}

require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';

$keys   =   array_keys($userportal_sections_list);
$uic    =   0;

foreach($userportal_sections_list as $section_id=>$section_info) {
    
    $section_title      =   $userportal_sections_list[$section_id]['SectionTitle'];
    $next_section_id    =   "";
    
    if(($uic+1) != count($userportal_sections_list)) {
        $next_section_id    =   $keys[array_search($section_id, $keys) + 1];
    }
    ?>
    <strong><?php echo $userportal_sections_list[$section_id]['SectionTitle'];?></strong>
    <div class="application_process_content" id="section_content<?php echo $section_id;?>">
    <?php
        $application_form = include COMMON_DIR . "application/ApplicationSectionInfo.inc";
        echo $application_form;
    ?>
    </div>
    <?php
    $uic++;
}

if (preg_match ( '/applicants.php$/', $_SERVER ["SCRIPT_NAME"] )) {
	echo '<hr noshade color="#' . $COLOR ['Heavy'] . '">';
}

if ($View != "Y") {
	echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
	echo '<tr><td colspan="2">';
	
	if($_REQUEST['navsubpg'] == 'appprofile' && $_REQUEST['navsubpg'] == 'appprofile') {
		echo '<div style="float:left"><strong>Note: </strong>This is not an application form. This form will allow you to enter and save your data so that application forms will be pre-filled</div>.<br>';
	}
	echo '</td></tr>';
	echo '<tr><td width="' . $colwidth / 2 . '">&nbsp;</td><td height="60" valign="middle">';
	echo "" . $Submit;
	echo "<span style='color:#428bca' id='edit_application_progress'></span>";
	echo '</td></tr>' . "\n\n";
	echo '</table>' . "\n";
}

echo '</form>';
?>