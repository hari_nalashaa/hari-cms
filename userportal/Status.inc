<?php
if (isset ( $complete )) {
	
	list ( $AID, $RID, $UID ) = explode ( ":", $complete );
	
	//Set information
	$set_info 	=	array("LastUpdated = NOW()", "Status = 3");
	//Set where condition
	$where_info = 	array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = :UniqueID");
	//Set parameters
	$params		= 	array(":OrgID"=>$OrgID, ":ApplicationID"=>$AID, ":RequestID"=>$RID, ":UniqueID"=>$UID);
	//Update Internal FormsAssigned
	G::Obj('FormsInternal')->updInternalFormsAssigned($set_info, $where_info, $info);
	
	// Add History
	$Comment 	=	"Changed status to: Completed";
	
	//Insert Internal Forms History Information
	$info		=	array("OrgID"=>$OrgID, "ApplicationID"=>$AID, "RequestID"=>$RID, "InternalFormID"=>$UID, "Date"=>"NOW()", "UserID"=>$UpUserID, "Comments"=>$Comment);
	//Insert Internal Forms History
	$result 	=	G::Obj('FormsInternal')->insInternalFormHistory($info);
	
} // end complete

if ($WHO) {
	echo '<B>Welcome back ' . $WHO ['first'] . ' ' . $WHO ['last'] . '!</B><br><br><br>';
}

//Update anonymous applications to UserPortal Applications - Based on Email of login user
require_once 'addApplication.php';

$job_application_info   =   G::Obj('Applications')->getUserApplicationsAndData($OrgID, $MultiOrgID, $UpUserID);
$hit = $job_application_info['count'];

echo "<div class='table-responsive'>";

$pending_apps_info  =   G::Obj('UserPortalInfo')->getPendingUserApplicationsInfo($OrgID, $MultiOrgID, $UpUserID);
$pending_apps_list  =   $pending_apps_info['results'];

$pending_app_tbl_body .= '<div class="row">';
$pending_app_tbl_body .= '<div class="col-lg-12 col-md-12 col-sm-12">';
$pending_app_tbl_body .= '<a href="'.USERPORTAL_HOME.'index.php?OrgID='.$OrgID.'&MultiOrgID='.$MultiOrgID.'&navpg=profiles&navsubpg=status&action=clearprofile">Clear Default Profile</a><br><br>';
$pending_app_tbl_body .= '</div>';
$pending_app_tbl_body .= '</div>';

$pending_app_tbl_body .= '<div class="row">';
$pending_app_tbl_body .= '<div class="col-lg-12 col-md-12 col-sm-12">';
$pending_app_tbl_body .= '<h4><strong>My Applications</strong></h4>';
$pending_app_tbl_body .= '</div>';
$pending_app_tbl_body .= '</div>';

$pending_app_tbl_body .= '<div class="row">';
$pending_app_tbl_body .= '<div class="col-lg-12 col-md-12 col-sm-12">';
$pending_app_tbl_body .= '<h6><strong><u>Pending Applications:</u></strong></h6>';
$pending_app_tbl_body .= '</div>';
$pending_app_tbl_body .= '</div>';


$pending_app_tbl_body .= '<div class="row">';

//Get Wotc Ids list
$wotcid_info	=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID, AKA, OrganizationName", $OrgID, $MultiOrgID);

if(count($pending_apps_list) > 0) {

    $pending_apps_has_content = "no";
    for($j = 0; $j < count($pending_apps_list); $j++)
    {
        //RequestID
        $PRequestID         =   $pending_apps_list[$j]['RequestID'];
        //Pending Application Last Updated Date
        $PLastUpdatedDate   =   $pending_apps_list[$j]['LastUpdatedDate'];
        $PendingOrgID       =   $pending_apps_list[$j]['OrgID'];
        $PendingMultiOrgID  =   $pending_apps_list[$j]['MultiOrgID'];
        
        //Set condition
        $where 	 			=	array("OrgID = :OrgID", "RequestID = :RequestID", "PostDate < NOW()", "ExpireDate > NOW()");
        //Set parameters
        $params  			=	array(":OrgID"=>$OrgID, ":RequestID"=>$PRequestID);
        
        $reqs_info_results  =   G::Obj('Requisitions')->getRequisitionInformation("Title, DATE_FORMAT(ExpireDate,'%m/%d/%Y') AS ExpireDate", $where, "", "", array($params));
        $req_info           =   $reqs_info_results['results'][0];
    
        if($req_info['Title'] != "") {
            $pending_apps_has_content = "yes";
            
            $pending_app_tbl_body .= '<div class="col-lg-6 col-md-6 col-sm-6">';            
            $pending_app_tbl_body .= '<strong>Job Title: </strong>'.$req_info['Title'].'</strong><br>';
            $pending_app_tbl_body .= '<strong>Completed By: </strong>'.$req_info['ExpireDate'].'</strong><br>';
            $pending_app_tbl_body .= '<a href="jobApplication.php?RequestID='.$PRequestID.'&OrgID='.$PendingOrgID.'&MultiOrgID='.$PendingMultiOrgID.'">Continue Application</a><br><br>';
            $pending_app_tbl_body .= '</div>';
            
        }
    }
}

if($pending_apps_has_content == "yes") {
    echo $pending_app_tbl_body;
}

echo '</div>';

echo '<div class="row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12"><br><br>';
echo '</div>';
echo '</div>';


echo '<div class="row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12">';
echo '<h6><strong><u>Completed Applications:</u></strong></h6>';
echo '</div>';
echo '</div>';

if(isset($_GET['msg']) && $_GET['msg'] == 'succupdprofile') {
    echo '<div class="row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12" style="color:green">';
    echo 'Your profile successfully updated with the application information';
    echo '</div>';
    echo '</div>';
}
else if(isset($_GET['msg']) && $_GET['msg'] == 'succclearprofile') {
    echo '<div class="row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12" style="color:green">';
    echo 'Default profile successfully cleared';
    echo '</div>';
    echo '</div>';
}

echo '<div class="row">';

if(is_array($job_application_info)) {
	foreach ($job_application_info['results'] as $STATUS) {

	    $aa_sec_info    =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $STATUS['FormID'], "AA");
	    $vet_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $STATUS['FormID'], "VET");
	    $dis_sec_info   =   G::Obj("ApplicationFormSections")->getApplicationFormSectionInfo("*", $OrgID, $STATUS['FormID'], "DIS");
	    
        $multiorgid_req =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $STATUS ['RequestID']);
        //$status = $ApplicantsObj->getProcessOrderDescription ( $OrgID, $STATUS ['ProcessOrder'] );
        $jobtitle       =   G::Obj('RequisitionDetails')->getJobTitle ( $OrgID, $multiorgid_req, $STATUS ['RequestID'] );
        $reqjobids      =   G::Obj('RequisitionDetails')->getReqJobIDs ( $OrgID, $multiorgid_req, $STATUS ['RequestID'] );

		//Set columns
        $columns        =   "date_format(EntryDate,'%Y-%m-%d %H:%i EST') EntryDate, ApplicantSortName, ProcessOrder, DispositionCode, VeteranEditStatus, DisabledEditStatus, AAEditStatus";
		//Get Application Information
		$application_info =   G::Obj('Applications')->getApplicationInformation($columns, $OrgID, $multiorgid_req, $STATUS['RequestID'], $STATUS ['ApplicationID']);
		
		//Get uploaded documents count
        $vault_info     =   G::Obj('ApplicantVault')->getUploadedDocumentsCount($OrgID, $STATUS ['ApplicationID'], $STATUS ['RequestID']);
		$attached_documents_count = $vault_info['count'];
		
		//Set where condition
        $where          =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = 1", "(Status > 0 AND Status < 5)");
		//Set parameters
        $params         =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$STATUS ['ApplicationID'], ":RequestID"=>$STATUS ['RequestID']);
		
		//Get InternalFormsAssigned Information Count
        $results        =   G::Obj('FormsInternal')->getInternalFormsAssignedInfo("COUNT(*) AS count", $where, "", "", array($params));
		$assigned_forms_count = $results['results'][0]['count'];
		
		//Set where condition
		$where = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PresentedTo = 1", "Status >= 3");
		//Set parameters
		$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$STATUS ['ApplicationID'], ":RequestID"=>$STATUS ['RequestID']);
		
		//Get InternalFormsAssigned Information Count
		$results = G::Obj('FormsInternal')->getInternalFormsAssignedInfo("COUNT(*) AS count", $where, "", "", array($params));
		$completed_forms_count = $results['results'][0]['count'];
 
		echo '<div class="col-lg-6 col-md-6 col-sm-6" style="margin-bottom:10px;border:1px solid #f5f5f5;min-height:240px;">';
		echo "<strong>Job Title: </strong>". $jobtitle . "<br>";
		echo "<strong>Application Date: </strong>". $STATUS['EntryDate'] . "<br>";
		echo "<strong>ApplicationID: </strong>";
		echo '<a href="index.php?ApplicationID=' . $STATUS ['ApplicationID'] . '&RequestID=' . $STATUS ['RequestID'] . '&navpg=status&navsubpg=view" data-toggle="tooltip" title="View the application you submitted for this position" style="color:black">';
		echo $STATUS['ApplicationID'];
		echo '</a>' . "<br>";
		if($DefaultProfile == $STATUS ['ApplicationID']) {
		    echo '<span style="color:green;font-weight:bold">';
		    echo 'Current Profile';
		    echo '</span><br>';
		}
		else {
		    echo '<a href="' . USERPORTAL_HOME . 'index.php?ApplicationID='.$STATUS ['ApplicationID'].'&RequestID='.$STATUS ['RequestID'].'&navpg=profiles&navsubpg=status&action=updateprofile" style="color:red;font-weight:bold" data-toggle="tooltip" title="The information from this application will be stored in your profile to be used for future applications" data-placement="right" style="color:black">';
		    echo 'Save as Profile';
		    echo '</a><br>';
		}

		if($application_info['VeteranEditStatus'] == 'Y') {
		    echo '<a href="' . USERPORTAL_HOME . 'editForm.php?ApplicationID='.$STATUS ['ApplicationID'].'&RequestID='.$STATUS ['RequestID'].'&typeform=veteran">';
		    echo '<img src="'. IRECRUIT_HOME .'images/icons/pencil.png" title="Edit" border="0">';
		    echo '&nbsp;Edit Veteran Form';
		    echo '</a><br>';
		}
		else if($vet_sec_info['Active'] == "Y") {
		    echo '<a href="' . USERPORTAL_HOME . 'formsInternal/viewInternalForm.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'&typeform=veteran">';
		    echo 'View Veteran Form';
		    echo '</a><br>';
		}
		    
		if($application_info['AAEditStatus'] == 'Y') {
		    echo '<a href="' . USERPORTAL_HOME . 'editForm.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'&typeform=aa">';
		    echo '<img src="'. IRECRUIT_HOME .'images/icons/pencil.png" title="Edit" border="0">';
		    echo '&nbsp;Edit Affirmative Action';
		    echo '</a><br>';
		}
		else if($aa_sec_info['Active'] == "Y") {
		    echo '<a href="' . htmlspecialchars(USERPORTAL_HOME) . 'formsInternal/viewInternalForm.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'&typeform=aa">';
		    echo 'View Affirmative Action';
		    echo '</a><br>';
		}
		
		
		if($application_info['DisabledEditStatus'] == 'Y') {
		    echo '<a href="' . USERPORTAL_HOME . 'editForm.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'&typeform=disabled">';
		    echo '<img src="'. IRECRUIT_HOME .'images/icons/pencil.png" title="Edit" border="0">';
		    echo '&nbsp;Edit Disabled Form';
		    echo '</a><br>';
		}
		else if($dis_sec_info['Active'] == "Y") {
		    echo '<a href="' . htmlspecialchars(USERPORTAL_HOME) . 'formsInternal/viewInternalForm.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'&typeform=disabled">';
		    echo 'View Disabled Form';
		    echo '</a><br>';
		}

		echo '<a href="' . USERPORTAL_HOME . 'editApplicationForm.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'">';
		echo '<img src="'. IRECRUIT_HOME .'images/icons/pencil.png" title="Edit" border="0">';
		echo '&nbsp;Edit Application Form';
		echo '</a><br>';
		
		echo '<div>';
		echo '<a href="' . USERPORTAL_HOME . 'formsInternal/applicantVault.php?ApplicationID='.$STATUS ['ApplicationID'].'&RequestID='.$STATUS ['RequestID'].'" data-toggle="tooltip" data-placement="right" title="Upload your supporting documents here" style="color:black">';
		echo 'Upload Files <i class="fa fa-cloud-upload fa-2x"></i>'. ' ( '.$attached_documents_count.' )';
		echo '</a>';
		echo '</div>';
		
		//Get Wotc Ids list
		$wotcid_info	=	G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID, AKA, OrganizationName", $OrgID, $MultiOrgID);
		//Get Wotc application info
		$wotc_app_info	=	G::Obj('WOTCIrecruitApplications')->getWotcApplicationInfo("*", $wotcid_info['wotcID'], $STATUS ['ApplicationID'], $STATUS ['RequestID']);
		
		if($wotcid_info['wotcID'] != "") {
			$assigned_forms_count	=	$assigned_forms_count	+	1;
		
			if($wotc_app_info['IrecruitApplicationID'] != "") {
				$completed_forms_count	=	$completed_forms_count	+	1;
			}
		}
		
		if($assigned_forms_count > 0) {
		    echo '<a href="assignedInternalForms.php?ApplicationID='.$STATUS ['ApplicationID'].'&RequestID='.$STATUS ['RequestID'].'&ProcessOrder='.$STATUS ['ProcessOrder'].'&navpg=status&navsubpg=view" style="color:red" data-toggle="tooltip" data-placement="right" title="Additional forms requested to be submitted for this position" style="color:black">';
		    echo 'Assigned Forms ';
		    echo '<i class="fa fa-files-o fa-2x"></i>';
		    
		    echo ' ( ' . $completed_forms_count . ' / '. $assigned_forms_count .' )';
		    echo '</a><br>';
		}
		else {
			echo '<br>';
		}
		
		//Get Checklist Info
		$get_checklist_process_data      =   G::Obj('Checklist')->getCheckListProcess($OrgID);
		if($get_checklist_process_data['DisplayOnUserPortal'] == "Yes"){
		    $get_checklist_header =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $STATUS ['ApplicationID'], $STATUS ['RequestID']);
		    if($get_checklist_header['ChecklistID']){
		        echo '<div style="padding-top:5px;">';
		        echo '<a href="' . USERPORTAL_HOME . 'hiringStatus.php?ApplicationID='.htmlspecialchars($STATUS ['ApplicationID']).'&RequestID='.htmlspecialchars($STATUS ['RequestID']).'&OrgID='.htmlspecialchars($OrgID).'">';
			echo 'Onboard Status';
		        echo '</a><br>';
		        echo "</div>";
		    }
		}
		
		echo '</div>';
	}
}

echo '</div>';

if ($hit == 0) {
	echo "<div class='table-responsive'>";
        echo 'You currently have no applications available for view.<br><br>';
        
        if($EmailVerifiedStatus != 1) {
        	echo '<b style="color:red;font-size:12pt;">Important:</b> <span style="font-size:12pt;">In order to retrieve the status of any pending applications <a href="index.php?navpg=profiles&navsubpg=logininfo"><strong style="color:red;">please verify your email address</strong></a> and return to this page. Make sure that the email address you are using is the same as the application you filed with us.</span><br><br>';
        }
	echo "</div>";
} // end hit

echo '</div>';
?>
