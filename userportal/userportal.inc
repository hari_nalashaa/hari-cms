<?php
define('FROM_SRC', 'USERPORTAL');

require_once realpath(__DIR__ . '/..') . '/server/VariablesDefined.inc';
require_once USERPORTAL_DIR . 'SessionConfig.inc';
require_once VENDOR_DIR . 'autoload.php';

/*
//Clear cache if we upload the profile image
if (($_REQUEST['navpg'] == "profiles") && ($_REQUEST['navsubpg'] == "logininfo")) {
	if (isset ( $_FILES ['profile_avatar_picture'] ['name'] ) && $_FILES ['profile_avatar_picture'] ['name'] != "") {
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	}
}
*/

header('P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"');
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//Common Request Parameters
if(isset($_REQUEST['pg']) && $_REQUEST['pg'] != "") $pg = $_REQUEST['pg'];
if(isset($_REQUEST['navpg']) && $_REQUEST['navpg'] != "") $navpg = $_REQUEST['navpg'];
if(isset($_REQUEST['navsubpg']) && $_REQUEST['navsubpg'] != "") $navsubpg = $_REQUEST['navsubpg'];
if(isset($_REQUEST['subpg']) && $_REQUEST['subpg'] != "") $subpg = $_REQUEST['subpg'];
if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];

//Set Error Reporting
ini_set ('display_errors', 0);

//Include all required classes from common folder
require_once COMMON_DIR . 'ClassIncludes.inc';

// assure no spaces in URL - Indeed check and cut and paste issue
$OrgID		=	preg_replace ( "/\s/i", '', $_REQUEST ['OrgID'] );
$MultiOrgID =	(isset($_REQUEST ['MultiOrgID']) && $_REQUEST ['MultiOrgID'] != "") ? preg_replace ( "/\s/i", '', $_REQUEST ['MultiOrgID'] ) : "";

// MultiOrg Checks
if (isset($_REQUEST ['mOrgID'])) {
	
	$org_information	=	$OrganizationDetailsObj->getOrgIDMultiOrgIDByMultiOrgID ( $_REQUEST ['mOrgID'] );
	$OrgID				=	$org_information['OrgID'];
	$MultiOrgID			=	$org_information['MultiOrgID'];
} else if (isset($_REQUEST['OrgID']) && isset($_REQUEST['MultiOrgID'])) {

	$org_information   =   $OrganizationDetailsObj->getOrganizationInformation($_REQUEST ['OrgID'], $_REQUEST ['MultiOrgID'], "OrgID, MultiOrgID");
	$OrgID             =   $org_information['OrgID'];
	$MultiOrgID        =   $org_information['MultiOrgID'];
	
} else if (isset($_REQUEST['OrgID']) && isset($_REQUEST['RequestID'])) {

	$MultiOrgID        =   $RequisitionDetailsObj->getMultiOrgID ( $_REQUEST ['OrgID'], $_REQUEST ['RequestID'] );
	$OrgID             =   $_REQUEST['OrgID'];
}

$brand_info     = $OrganizationDetailsObj->getOrganizationInformation($OrgID, "", "BrandID");
if(!isset($brand_info['BrandID']) || $brand_info['BrandID'] == "") $brand_info['BrandID'] = "0";
$brand_org_info = $BrandsObj->getBrandInfo($brand_info['BrandID']);

###########################################################################
####### Update user session if the OrgID is different
###########################################################################

// Get Userportal Login User Information
$AUTH_USER_INFO = $UserPortalUsersObj->getUserPortalUserInfo ();

if(isset($AUTH_USER_INFO['UserID']) && $AUTH_USER_INFO['UserID'] != "") {
	
	if($OrgID != "" && ($OrgID != $AUTH_USER_INFO['OrgID'] || $MultiOrgID != $AUTH_USER_INFO['MultiOrgID'])) {
		
		// Set Users Information
		$upd_set_users_info = array (
				"LastAccess     =   NOW()",
				"OrgID          =   :OrgID",
				"MultiOrgID     =   :MultiOrgID"
		);
		// Set parameters information
		$upd_params_users_info = array (
				":OrgID"        =>  $OrgID,
				":MultiOrgID"   =>  $MultiOrgID,
				":UserID"       =>  $AUTH_USER_INFO['UserID']
		);
		// Set where users information
		$upd_where_users_info = array ("UserID = :UserID");
		
		// update login user OrgID, MultiOrgID
		$upd_users_info = $UserPortalUsersObj->updUsersInfo ( $upd_set_users_info, $upd_where_users_info, array ($upd_params_users_info) );
		
	}
}
###########################################################################

$NumProfiles = 1;

$user_detail_info       =   $OrganizationDetailsObj->getOrganizationInformation($AUTH_USER_INFO['OrgID'], $AUTH_USER_INFO['MultiOrgID'], "UserPortalThemeID");
$up_app_theme_info      =   $UserPortalInfoObj->getUserPortalApplicationThemeInfo($user_detail_info['UserPortalThemeID']);

require_once COMMON_DIR . 'color.inc';
?>
