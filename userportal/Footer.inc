    <?php 
    $user_detail_info = $UserPortalUsersObj->getUserDetailInfoByUserID("EmailVerified", $UpUserID);

    if(isset($UpUserID) && $UpUserID != "" && $user_detail_info['EmailVerified'] == 0 && !preg_match ( '/logout.php$/', $_SERVER ["SCRIPT_NAME"] )) {
        ?>
        <div id="myModal" class="modal">
          
          <div class="modal-content">
            <div id="email_verification_msg"></div>
            <span class="close" onclick="document.getElementById('myModal').style.display='none';">&times;</span>
            <p>
                The email address that is associated with this account has not been verified. <br>
                Please <a href="javascript:void(0);" onclick="resendEmailVerification()" style="color:red;">resend the verification email </a> and check your email account.<br>
            </p>
          </div>
        </div>
        <script type="text/javascript">
        var modal = document.getElementById('myModal');
        modal.style.display = "block";
        </script>
        <?php
    }
	
	if (isset($whitelist) 
		&& in_array(PUBLIC_DIR, $whitelist)
		&& in_array($OrgID, $whitelist)
		&& isset($OrgID)
		&& defined('PUBLIC_DIR')
		&& file_exists(PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc')) {
		include_once PUBLIC_DIR . 'images/' . $OrgID . '/FooterCode.inc';
	}
    ?>
    <footer class="footer">
		<div class="container">
		      <?php 
    		      if($brand_info['BrandID'] == "0") {
                     ?><a href="http://www.irecruit-software.com" target="_blank">Powered by iRecruit</a><?php
                  }
                  else {
                     if($brand_org_info['Url'] != "") {
                        ?><a href="<?php echo $brand_org_info['Url'];?>" target="_blank"><?php echo $brand_org_info['FooterText'];?></a><?php
                     }
                     else {
                     	echo $brand_org_info['FooterText'];
                     }
                  }
    		      
                  if(isset($bottomnav) && is_array($bottomnav)) {
                    foreach ($bottomnav as $section => $displaytitle) {
                        echo '&nbsp;&nbsp;&nbsp;&#149;&nbsp;&nbsp;&nbsp;<a href="index.php?navpg=' . $section . '">' . $displaytitle . '</a> ';
                    }	
                  }
		      ?>
		</div>
	</footer>
</div>
	<!-- /#wrapper -->

<!-- jQuery -->
<!-- <script src="js/jquery.js"></script> -->

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo USERPORTAL_HOME?>js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo USERPORTAL_HOME?>js/plugins/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo USERPORTAL_HOME?>js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo USERPORTAL_HOME?>js/plugins/dataTables/dataTables.bootstrap.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo USERPORTAL_HOME?>js/sb-admin-2.js"></script>
<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script type="text/javascript">
$(document).ready(function() {
	$("#ui-datepicker-div").hide();

	$("#users-search-box").bind("keyup click", function() {
		$.ajax({
			dataType: "JSON",
    		type: "POST",
    		url: "getUsersList.php",
    		data:'keyword='+$(this).val(),
    		beforeSend: function() {
    			$("#users-search-box").css("background-color","#F5F5F5");
    		},
    		success: function(data) {
        		var users_list = '';
        		users_list += '<ul id="users-list">';
        		for(key in data) {
        			users_list += '<li onclick=\'setUsersSearchBoxValue("'+data[key].Email+'")\'>'+data[key].Email+'</li>';
        		}
        		users_list += '</ul>';
    			$("#suggesstion-box").show();
    			$("#suggesstion-box").html(users_list);
    			$("#users-search-box").css("background","#FFF");
    		}
		});
	});
	
});
var OrgID       = '<?php echo $OrgID;?>';
var MultiOrgID  = '<?php echo $MultiOrgID;?>';
var RequestID   = '<?php echo $MRequestID;?>';
function monster_request_on_continue() {
	location.href = 'thankyou.php?OrgID='+OrgID+'&MultiOrgID='+MultiOrgID+'&RequestID='+RequestID;
}

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

function setUsersSearchBoxValue(selected_user) {
	$("#users-search-box").val(selected_user);
	$("#suggesstion-box").hide();
	$( "#frmUserportalMasterAccess" ).submit();
}

$('#side-menu li').click(function() {
	$("#side-menu li a span").attr('class', 'fa fa-chevron-circle-right');

	$('#side-menu li').each(function(i, li) {
		if($(this).attr('class') == 'active')
			 $(this).find("a span").attr('class', 'fa fa-chevron-circle-down');
	});
});

function resendEmailVerification() {
	$.ajax({
		method: "POST",
		url: "resendEmail.php?resend=Y",
		type: "POST",
		beforeSend: function(){
			$("#email_verification_msg").html('Please wait.. ');
		},
		success: function(data) {
			$("#email_verification_msg").html("");
			alert(data);

			var modal = document.getElementById('myModal');
	        modal.style.display = "none";
		}
	});
}
</script>

<?php
if (DEVELOPMENT != "Y") {
echo <<<END
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-25955565-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
END;
}


if ($OrgID == "I20120420") { // Horizon Home Care & Hospice, Inc.
echo <<<END
<script src="https://tracking.pandoiq.com/8763" async></script>
<noscript><img src="https://tracking.pandoiq.com/8763?noscript" height="0" width="0"></img></noscript>
END;
}
?>
</body>
</html>
