<?php
require_once 'userportal.inc';

//First file doesn't require for now, but just keep it.
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';
include USERPORTAL_DIR . 'JobApplicationRequestVars.inc';

$page_title = "Edit Application";
$formtable = "FormQuestions";

require_once COMMON_DIR . 'color.inc';
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

//Get requisition details
$requisition_info       =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, MultiOrgID, RequestID", $OrgID, $_REQUEST['RequestID']);

$Title = $requisition_info['Title'];
$MultiOrgID = $requisition_info['MultiOrgID'];
$RequestID = $requisition_info['RequestID'];

$application_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("ApplicationID, FormID, ApplicantSortName, VeteranEditStatus, DisabledEditStatus, AAEditStatus", $OrgID, $_REQUEST['ApplicationID'], $RequestID);

$ApplicationID = $application_info['ApplicationID'];
$FormID = $application_info['FormID'];
$HoldID = $OrgID.$UpUserID.$RequestID.$ApplicationID;
$ApplicantSortName = $application_info['ApplicantSortName'];
$VeteranEditStatus = $application_info['VeteranEditStatus'];
$DisabledEditStatus = $application_info['DisabledEditStatus'];
$AAEditStatus = $application_info['AAEditStatus'];

if (($ApplicationID == "") || ($RequestID == "")) {
	header("Location:index.php?OrgID=".$OrgID."&navpg=profiles&navsubpg=status");
        exit;
}

$personal_ques_info     =   G::Obj('ApplicationFormQuestions')->getFormQuestionsListBySectionID($OrgID, $FormID, "1");
$child_ques_info        =   G::Obj('ApplicationFormQuestions')->getFormChildQuesForParentQues($OrgID, $FormID, "1");

$que_types_list=array();
foreach($personal_ques_info as $personal_que_id=>$personal_que_info) {
    $que_types_list[$personal_que_id]   =   $personal_que_info['QuestionTypeID'];
}

//load initial personal page data
$params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
//Get text blocks information
$where      =   array("OrgID = :OrgID", "FormID = :FormID");
$results    =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));

// Query and Set Text Elements
$TextBlocks=array();
if(is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
    }
}

$APPDATA = G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);
  
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo 'Application Form: '.$ApplicationID. ' - ' . $ApplicantSortName;

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'index.php?navpg=profiles&navsubpg=status">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Check My Status';
echo '</a>';
echo '</span>';

echo '</h3>';

echo '</div>';
echo '</div>';

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';
?>

<div>
    <h4><?php echo $Title;?>:</h4>
    <span style="color:red">*</span>&nbsp;Please click on each section title and complete each section. Click on the Save/Next button to go to the next section. Click on the Sign Application button after all the sections are completed. All sections with required questions must be completed before the application can be submitted.
    <br><br>
    <?php
    if (isset($TextBlocks ["FormHeader"]) && $TextBlocks ["FormHeader"] != "") {
        echo "<br>".$TextBlocks ["FormHeader"]."<br>";
    }
    ?>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['CompletedTab'];?>;color: <?php echo "#".$up_app_theme_info['CompletedTab'];?>;"></div> 
    <div style="float:left;">&nbsp;Completed Section&nbsp;</div>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['PendingTab'];?>;color: <?php echo "#".$up_app_theme_info['PendingTab'];?>;"></div>
    <div style="float:left;">&nbsp;Pending Section&nbsp;</div>
    <div style="float:left;width:20px;height:20px;background-color:<?php echo "#".$up_app_theme_info['ActiveTab'];?>;color: <?php echo "#".$up_app_theme_info['ActiveTab'];?>;"></div> 
    <div style="float:left;">&nbsp;Active Section&nbsp;</div>
    <div style="clear: both"></div>
</div>

<ul class="application_steps" id="application_steps">
    <?php
	if ($VeteranEditStatus != "Y") { unset ($userportal_sections_list['VET']); }
	if ($AAEditStatus != "Y") { unset ($userportal_sections_list['AA']); }
	if ($DisabledEditStatus != "Y") { unset ($userportal_sections_list['DIS']); }

        foreach ($userportal_sections_list as $section_id=>$section_info) {
            $section_title  =   $section_info['SectionTitle'];
            ?>
            <li>
                <a href="javascript:void(0);" class="<?php if(in_array($section_id, $processed_sections)) echo 'application_step_tabs_filled'; else echo 'application_step_tabs';?>" id="section_id<?php echo $section_id;?>">
                    <?php echo $section_title;?>
                </a>
            </li>
            <?php
        }
    ?>
</ul>

<br>
<div class="application_process">
    <?php
    $keys   =   array_keys($userportal_sections_list);
    
    foreach($keys AS $section_id) {
        
        $section_title      =   $userportal_sections_list[$section_id]['SectionTitle'];
        $next_section_id    =   $keys[array_search($section_id, $keys) + 1];
        
        ?>
        <div class="application_process_content" id="section_content<?php echo $section_id;?>">
            <form name="<?php echo $section_forms[$section_id];?>" id="<?php echo $section_forms[$section_id];?>" method="post" enctype="mutipart/form-data">
                <?php
		if($section_id == 6) {
                    $application_form   =   include COMMON_DIR . "application/EditAttachments.inc";
                }
                else if($section_id == 14) {
                    $application_form = include COMMON_DIR . "application/ApplicantProfilePicture.inc";
                }
                else {
                    $application_form   =   include COMMON_DIR . "application/ApplicationSectionInfo.inc";
		}
                    
                echo $application_form;
                ?>
                <br>
                <input type="hidden" name="ApplicationID" id="ApplicationID" value="<?php echo $ApplicationID;?>">
                <input type="hidden" name="SectionID" id="SectionID" value="<?php echo $section_id;?>">
                <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>">
                <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
                <input type="hidden" name="process" id="process" value="Y">
                <input type="button" name="btnInfo" id="btnInfo" value="Save/Next" onclick="processEditApplicationForm('<?php echo $section_forms[$section_id];?>', 'section_id<?php echo $section_id;?>', '<?php echo $next_section_id;?>', '<?php echo $ApplicationID;?>');">

            </form>
        </div>
        <?php
    }
    ?>
    <div id="process_message"></div>
</div>
<br>
					
<div id="process_finish_button_validation"></div>

<br>
<div>
    <form name="frmProcessApplication" id="frmProcessApplication" method="post" action="editSignature.php">
        <input type="hidden" name="ApplicationID" id="ApplicationID" value="<?php echo $ApplicationID;?>">
        <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $RequestID;?>">
        <input type="hidden" name="FormID" id="FormID" value="<?php echo $FormID;?>">
        <input type="hidden" name="ProcessApplication" id="ProcessApplication" value="Yes">
        <input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
        <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $MultiOrgID;?>">
        <?php 

        $sign_application       =   "true";
	foreach ($keys AS $sec) {
        	if(in_array($sec, $processed_sections)) {
                  $sign_application   =   "true";		
		} else {
                  $sign_application   =   "false";		
		}
	}
	?><input type="button" name="btnProcessApplicationForm" id="btnProcessApplicationForm" class="btnDisabled" value="Sign Application">
    </form>
    <?php
    if (isset($TextBlocks ["FormFooter"]) && $TextBlocks ["FormFooter"] != "") {
        echo "<br>".$TextBlocks ["FormFooter"]."<br>";
    }
    ?>
    <span style="color:red">*</span>&nbsp;Please click on each section title and complete each section. Click on the Save/Next button to go to the next section. Click on the Sign Application button after all the sections are completed. All sections with required questions must be completed before the application can be submitted.
</div>

<script type="text/javascript" src="js/edit-job-application.js"></script>

<?php
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
echo "here" . print_r($errors_list,true);
?>
<script>
function manageCoverLetterOther(ocval, at_type) {

	if(at_type == "coverletterupload") {
		if(ocval == "") {
			document.getElementById('div_new_coverletterupload').style.display = 'block';
		}
		else {
		    document.getElementById('div_new_coverletterupload').style.display = 'none';
		}
	}
	else if(at_type == "otherupload") {
		if(ocval == "") {
			document.getElementById('div_new_otherupload').style.display = 'block';
		}
		else {
			document.getElementById('div_new_otherupload').style.display = 'none';
		}
	}
	else if(at_type == "resumeupload") {
		if(ocval == "") {
			document.getElementById('div_new_resumeupload').style.display = 'block';
		}
		else {
			document.getElementById('div_new_resumeupload').style.display = 'none';
		}
	}
	
}

var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info	=	JSON.parse('<?php echo json_encode($child_ques_info);?>');
</script>
<script src="<?php echo PUBLIC_HOME;?>js/child-questions.js" type="text/javascript"></script>
