<?php
if ($process == "Change Information") {
	
	//Get UserDetails Information based on UserID
	$USER          =   $UserPortalUsersObj->getUserDetailInfoByUserID("UserID", $UpUserID);
	$where_info    =   array("UserID != :UserID", "Email = :Email");
	$params_info   =   array(":Email"=>$_REQUEST['email'], ":UserID"=>$UpUserID);
	
	$user_emails_info = $UserPortalUsersObj->getUsersInformation("Email", $where_info, "", "", array($params_info));
	
	$email_exist = "true";
	if($user_emails_info['count'] == 0) {
	    $email_exist = "false";
	}

	$ERROR .= $ValidateObj->validate_name ( $_REQUEST ['first_name'], "First" );
	$ERROR .= $ValidateObj->validate_name ( $_REQUEST ['last_name'], "Last" );
	
	if (isset($_REQUEST['pass']) && $_REQUEST['pass'] != "") {
		$ERROR .= $ValidateObj->validate_verification ( $_REQUEST['pass'], $_REQUEST['pass2'], 'Y' );
	}
	
	$ERROR .= $ValidateObj->validate_email_address ( $_REQUEST['email'] );
	
	if ($_REQUEST['minutes'] < 10) {
		$ERROR .= "Minutes cannot be set less than 10." . "\\n";
	}
	
	if ($ERROR) {
		echo '<script language="JavaScript" type="text/javascript">' . "\n";
		echo "errorMessage('$ERROR');" . "\n";
		echo '</script>' . "\n";
	} else {

	// Upload Photo
	if (isset ( $_FILES ['profile_avatar_picture'] ['name'] ) && $_FILES ['profile_avatar_picture'] ['name'] != "") {
				if ($_FILES ['profile_avatar_picture'] ['size'] > 60000) {
					echo "<script>location.href='index.php?navpg=profiles&navsubpg=logininfo&msg=maxsize';</script>";
					exit ();
				} else {
					$path_info = pathinfo($_FILES['profile_avatar_picture']['name']);
					$extension = $path_info['extension'];

					//Create vault directory if not exists
					if (! is_dir ( USERPORTAL_DIR . "vault/" )) {
						@mkdir(USERPORTAL_DIR . "vault/", 0777);
						@chmod(USERPORTAL_DIR . "vault/", 0777);
					}
					
					//Create profile avatars directory if not exist
					if (! is_dir ( USERPORTAL_DIR . "vault/".$UpUserID."/" )) {
						@mkdir(USERPORTAL_DIR . "vault/".$UpUserID."/", 0777);
						@chmod(USERPORTAL_DIR . "vault/".$UpUserID."/", 0777);
					}
					
					//Create profile avatars directory if not exist
					if (! is_dir ( USERPORTAL_DIR . "vault/".$UpUserID."/profile_avatars/" )) {
						@mkdir(USERPORTAL_DIR . "vault/".$UpUserID."/profile_avatars/", 0777);
						@chmod(USERPORTAL_DIR . "vault/".$UpUserID."/profile_avatars/", 0777);
					}
					
					//To create a directory based on UserID
					$PROFILE_DIR = USERPORTAL_DIR . "vault/".$UpUserID."/profile_avatars/";
					
					if (! is_dir ( $PROFILE_DIR )) {
						@mkdir($PROFILE_DIR, 0777);
						@chmod($PROFILE_DIR, 0777);
					}
					
					if ($ProfileAvatarPicture != "") {
						@unlink ( $PROFILE_DIR . "/" . $ProfileAvatarPicture );
					}
						
					$file_name = "profile_photo." . $extension;

					@chmod($PROFILE_DIR . "/" . $file_name, 0777);
						
					if (@move_uploaded_file ( $_FILES ['profile_avatar_picture'] ['tmp_name'], $PROFILE_DIR . "/" . $file_name )) {
						$UserPortalUsersObj->updateUserProfileAvatarPhoto ( $file_name, $UpUserID );
					}
				
				}
		}
		
		$user_detail_info = $UserPortalUsersObj->getUserDetailInfoByUserID("*", $UpUserID);

		//Common where condition to below update queries
		$where_info = array("UserID = :UserID");
		//Set information
		$set_info = array("Minutes = :Minutes");
		//Set parameters
		$params = array(":Minutes"=>$_REQUEST['minutes'], ":UserID"=>$UpUserID);
		//Update User Minutes
		$UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
		
		if($user_detail_info['Email'] != $_REQUEST['email'] && $email_exist == "false") {
			//Set information
			$set_info = array("Email = :Email", "EmailVerified = ''");
			//Set parameters
			$params = array(":Email"=>$_REQUEST['email'], ":UserID"=>$UpUserID);
			//Update User Email, EmailVerified
			$UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
		}
		
		if ($_REQUEST['pass']) {
			//Set information
			$set_info = array("Verification = :Verification");
			//Set parameters
			$params = array(":Verification"=>password_hash($_REQUEST['pass'], PASSWORD_DEFAULT), ":UserID"=>$UpUserID);
			//Update User Email, EmailVerified
			$UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
		}
		
		//Set information
		$set_info = array("FirstName = :FirstName");
		//Set parameters
		$params = array(":FirstName"=>$_REQUEST['first_name'], ":UserID"=>$UpUserID);
			
		//Update User Email, EmailVerified
		$UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
	
	
	    //Set information
	    $set_info = array("LastName = :LastName");
	    //Set parameters
	    $params = array(":LastName"=>$_REQUEST['last_name'], ":UserID"=>$UpUserID);
	
	    //Update User Email, EmailVerified
	    $UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
		
		if($email_exist == "true") {
		    echo "<script>location.href='index.php?navpg=profiles&navsubpg=logininfo&msg=ufailemail';</script>";
		    exit();
		}
		else {
		    echo "<script>location.href='index.php?navpg=profiles&navsubpg=logininfo&msg=usuc';</script>";
		    exit();
		}
	}
} // end process


$USERS = $UserPortalUsersObj->getUserDetailInfoByUserID("*", $UpUserID);


if ($USERS ['EmailVerified'] == '') {
	$emailverified = "Email needs verification. <a href=\"javascript:void(0);\" onclick=\"document.getElementById('myModal').style.display = 'block';\" style=\"COLOR:red;\">Resend Notification</a>";
} else {
	$emailverified = "Email has been verified.";
}
?>
<p>Use this page to change your login information.</p>
<p>If you want to delete your account permanently please <a href="deleteUserPortalAccount.php">click here</a>.</p>
<table border="0" cellspacing="3" cellpadding="3" width="500"
	class="table table-striped">
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
		<tr>
			<td colspan="2">
				<?php
				if(isset($_GET ['msg']) && $_GET ['msg'] != "") {
					
					if ($_GET ['msg'] == "maxsize") {
						echo '<div class="row" style="color:red;">';
						echo '&nbsp;&nbsp;&nbsp;Your file size should not be greater than 12000<br><br>';
						echo '</div>';
					}
					
					if ($_GET ['msg'] == "ufailemail") {
					    echo '<div class="row" style="color:red;">';
					    echo '&nbsp;&nbsp;&nbsp;Sorry email already exists.<br><br>';
					    echo '</div>';
					}
						
					if ($_GET ['msg'] == "usuc") {
						echo '<div class="row" style="color:blue;">';
						echo '&nbsp;&nbsp;&nbsp;Your settings updated successfully<br><br>';
						echo '</div>';
					}	
						
				}
				?>
			</td>
		</tr>
		<tr>
			<td width="200">
			 First Name:<span style='color:red'>*</span>
			</td>
			<td><input type="text" name="first_name" id="first_name" value="<?php echo $USERS['FirstName']?>" size="15" maxlength="45"></td>
		</tr>
		<tr>
			<td width="200">Last Name:<span style='color:red'>*</span></td>
			<td><input type="text" name="last_name" id="last_name" value="<?php echo $USERS['LastName']?>" size="15" maxlength="45"></td>
		</tr>
		<tr>
			<td>Email Address:<span style='color:red'>*</span></td>
			<td><input type="text" name="email" value="<?php echo $USERS['Email']?>">
				<?php echo $emailverified?>
			</td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="pass" value="" size="20" maxlength="45" autocomplete="off">
		</tr>
		<tr>
			<td>Password Again:</td>
			<td><input type="password" name="pass2" value="" size="20" maxlength="45" autocomplete="off"></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>Email will be used to recover your password.<br> Verified email
				will be used to access your previous applications.
			</td>
		</tr>

		<tr>
			<td>Active Minutes:</td>
			<td><input type="text" name="minutes" value="<?php echo $USERS['Minutes']?>" size="2" maxlength="3">&nbsp;Number of minutes before being automatically logged out.</td>
		</tr>
		
		<tr>
			<td>Profile Avatar Picture:(29 x 29)</td>
			<td><input class="form-control-file" name="profile_avatar_picture" id="profile_avatar_picture" type="file"></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td>
				<input type="hidden" name="navsubpg" value="<?php echo $navsubpg?>">
				<input type="hidden" name="navpg" value="<?php echo $navpg?>">
				<input type="hidden" name="process" value="Change Information">
				<input type="submit" value="Change Information">
			</td>
		</tr>

	</form>
</table>
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="200"></td>
	</tr>
</table>
