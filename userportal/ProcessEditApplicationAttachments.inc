<?php
// FILEHANDLING
$dir = IRECRUIT_DIR . 'vault/' . $OrgID;

if (! file_exists ( $dir )) {
	mkdir ( $dir, 0700 );
	chmod ( $dir, 0777 );
}

$apatdir = $dir . '/applicantattachments';

if (! file_exists ( $apatdir )) {
	mkdir ( $apatdir, 0700 );
	chmod ( $apatdir, 0777 );
}

$purpose        =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

//Set parameters
$params         =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID);
// Set Form Question Fields
$where          =   array ("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID = 8", "Active = 'Y'");
//Get FormQuestions Information
$resultsIN      =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder", array ($params) );

if (is_array ( $resultsIN ['results'] )) {
	foreach ( $resultsIN ['results'] as $FIL ) {
		
	    $file_id = "";
	    if($FIL ['QuestionID'] == 'coverletterupload') {
            $file_id    =   "new_file_coverletterupload";
	    }
	    else if($FIL ['QuestionID'] == 'otherupload') {
            $file_id    =   "new_file_otherupload";
	    }
	    else if($FIL ['QuestionID'] == 'resumeupload') {
            $file_id    =   "new_file_resumeupload";
	    }
	    else {
            $file_id    =   $FIL ['QuestionID'];
	    }

	    $q_results = G::Obj('FormQuestions')->getQuestionDetails("Question", "FormQuestions", $OrgID, $FormID, $FIL ['QuestionID']);
	    $ATTACHMENT =   G::Obj('Attachments')->getApplicantAttachmentInfo($OrgID, $ApplicationID, $FIL['QuestionID']);
	    if ($ATTACHMENT['TypeAttachment'] != "") { $file_present = "File Present."; } else { $file_present = ""; }

		if ($_FILES [$FIL ['QuestionID']] ['type'] != "") {

		    // Set where condition
		    $where     =   array (
                		        "OrgID             =   :OrgID",
                		        "ApplicationID     =   :ApplicationID",
                		        "TypeAttachment    =   :TypeAttachment"
                            );
		    // Set parameters
		    $params    =   array (
                		        ":OrgID"           =>  $OrgID,
                		        ":ApplicationID"   =>  $_REQUEST['ApplicationID'],
                		        ":TypeAttachment"  =>  $FIL['QuestionID']
                            );
		    // Get Applicant Attachments
		    $results   =   G::Obj('Attachments')->getApplicantAttachments ( "*", $where, '', array ($params) );
		    $AA        =   $results ['results'] [0];
		    
		    // Get DateTime based on given format
		    $HLD       =   G::Obj('MysqlHelper')->getDateTime ( '%Y%m%d%H%m%s' );
		    
		    $filenameA =   $apatdir . '/' . $_REQUEST['ApplicationID'] . '-' . $AA ['PurposeName'] . '.' . $AA ['FileType'];
		    $filenameB =   $apatdir . '/' . $_REQUEST['ApplicationID'] . '-' . $AA ['PurposeName'] . '-' . $HLD . '.' . $AA ['FileType'];
		    
		    @rename($filenameA, $filenameB);
		    
			$e       =   explode ( '.', $_FILES [$FIL ['QuestionID']] ['name'] );
			$ecnt    =   count ( $e ) - 1;
			$ext     =   $e [$ecnt];
			$ext     =   preg_replace ( "/\s/i", '', $ext );
			$ext     =   substr ( $ext, 0, 5 );
			
			//Applicant Attachments Information
			$applicant_attach_info   =   array (
                                            "OrgID"             =>  $OrgID,
                                            "ApplicationID"     =>  $_REQUEST['ApplicationID'],
                                            "TypeAttachment"    =>  $FIL ['QuestionID'],
                                            "PurposeName"       =>  $purpose [$FIL ['QuestionID']],
                                            "FileType"          =>  $ext 
                            			 );
			
			//Update Applicant Attachments Information
			G::Obj('Attachments')->insApplicantAttachments($applicant_attach_info);

			$COMPARE[$FIL['QuestionID']]['Question']         =   $q_results['Question'];
        		$COMPARE[$FIL['QuestionID']]['QuestionID']       =   $FIL ['QuestionID'];
        		$COMPARE[$FIL['QuestionID']]['QuestionTypeID']   =   "6";
        		$COMPARE[$FIL['QuestionID']]['values']           =   "";
			$COMPARE[$FIL['QuestionID']]['APPDATA']          =   $file_present;
        		$COMPARE[$FIL['QuestionID']]['REQUEST']          =   "A new file has been uploaded.";
			
			$filename    =  $apatdir . '/' . $_REQUEST['ApplicationID'] . '-' . $purpose [$FIL ['QuestionID']] . '.' . $ext;
			$tmp_name    =  $_FILES [$FIL ['QuestionID']] ['tmp_name'];
			move_uploaded_file($tmp_name, $filename);

			if($FIL ['QuestionID'] == 'resumeupload' && $feature['ResumeParsing'] == "Y") {
			    //User Portal Resume
			    G::Obj('Sovren')->processUserPortalResume($OrgID, $UpUserID, $_REQUEST['RequestID'], $_REQUEST['ApplicationID'], $filename);
			}
			
			chmod ( $filename, 0666 );
		}
		else if ($_FILES [$file_id] ['type'] != "") {

		    // Set where condition
		    $where     =   array (
                		        "OrgID             =   :OrgID",
                		        "ApplicationID     =   :ApplicationID",
                		        "TypeAttachment    =   :TypeAttachment"
                		   );
		    // Set parameters
		    $params    =   array (
                		        ":OrgID"           =>  $OrgID,
                		        ":ApplicationID"   =>  $_REQUEST['ApplicationID'],
                		        ":TypeAttachment"  =>  $FIL['QuestionID']
                		   );
		    // Get Applicant Attachments
		    $results   =   G::Obj('Attachments')->getApplicantAttachments ( "*", $where, '', array ($params) );
		    $AA        =   $results ['results'] [0];
		    
		    // Get DateTime based on given format
		    $HLD       =   G::Obj('MysqlHelper')->getDateTime ( '%Y%m%d%H%m%s' );
		    
		    $filenameA =   $apatdir . '/' . $_REQUEST['ApplicationID'] . '-' . $AA ['PurposeName'] . '.' . $AA ['FileType'];
		    $filenameB =   $apatdir . '/' . $_REQUEST['ApplicationID'] . '-' . $AA ['PurposeName'] . '-' . $HLD . '.' . $AA ['FileType'];
		    
		    @rename($filenameA, $filenameB);
		    
		    
		    $e       =   explode ( '.', $_FILES [$file_id] ['name'] );
		    $ecnt    =   count ( $e ) - 1;
		    $ext     =   $e [$ecnt];
		    $ext     =   preg_replace ( "/\s/i", '', $ext );
		    $ext     =   substr ( $ext, 0, 5 );

		    //Applicant Attachments Information
		    $applicant_attach_info = array (
                                        "OrgID"             =>  $OrgID,
                                        "ApplicationID"     =>  $_REQUEST['ApplicationID'],
                                        "TypeAttachment"    =>  $FIL ['QuestionID'],
                                        "PurposeName"       =>  $purpose [$FIL ['QuestionID']],
                                        "FileType"          =>  $ext
                        		    );
		    
			//Update Applicant Attachments Information
			G::Obj('Attachments')->insApplicantAttachments($applicant_attach_info);

			$COMPARE[$FIL['QuestionID']]['Question']         =   $q_results['Question'];
        		$COMPARE[$FIL['QuestionID']]['QuestionID']       =   $FIL ['QuestionID'];
        		$COMPARE[$FIL['QuestionID']]['QuestionTypeID']   =   "6";
        		$COMPARE[$FIL['QuestionID']]['values']           =   "";
			$COMPARE[$FIL['QuestionID']]['APPDATA']          =   $file_present;
        		$COMPARE[$FIL['QuestionID']]['REQUEST']          =   "A new file has been uploaded.";

		    $filename =   $apatdir . '/' . $_REQUEST['ApplicationID'] . '-' . $purpose [$FIL ['QuestionID']] . '.' . $ext;
		    $tmp_name =   $_FILES [$file_id] ['tmp_name'];
		    move_uploaded_file($tmp_name, $filename);
		    
		    if($FIL ['QuestionID'] == 'resumeupload' && $feature['ResumeParsing'] == "Y") {
		        //User Portal Resume
		        G::Obj('Sovren')->processUserPortalResume($OrgID, $UpUserID, $_REQUEST['RequestID'], $_REQUEST['ApplicationID'], $filename);
		    }
		    
		    chmod ( $filename, 0666 );
		}
	} // end foreach
} 
// END FILEHANDLING
?>
