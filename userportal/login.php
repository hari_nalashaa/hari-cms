<?php
require_once 'userportal.inc';

if (isset ( $_REQUEST ['OrgID'] ) && $_REQUEST ['OrgID'] != "")
	$OrgID = $_REQUEST ['OrgID'];
if (isset ( $_REQUEST ['MultiOrgID'] ) && $_REQUEST ['MultiOrgID'] != "")
	$MultiOrgID = $_REQUEST ['MultiOrgID'];

if (isset ( $_REQUEST ['lookup'] ) && $_REQUEST ['lookup'] != "")
	$lookup = $_REQUEST ['lookup'];


// Set Global Variables
$navpg  = isset ( $_REQUEST ['navpg'] ) ? $_REQUEST ['navpg'] : '';
$pg     = isset ( $_REQUEST ['pg'] ) ? $_REQUEST ['pg'] : '';


$OrgIDck    =   "";
// Set parameters
$params_org =   array (":OrgID" => $OrgID);
// Get OrgData Information
$results    =   $OrganizationsObj->getOrgDataInfo ( "OrgID", array ("OrgID = :OrgID"), '', array ($params_org) );
$OrgIDck    =   $results ['results'] [0] ['OrgID']; 
  if (isset ($_POST['process'])&&  $_POST['process'] == "otp") { 
    	$otpvalidate = $UserPortalUsersObj->getUserDetailInfoByEmail("*", $_SESSION['userportal_logged_email']);
       if($otpvalidate['TwofAuthToken'] === $_POST['otp']){

		// set a new Session ID
		// randomize a session key
		$SESS = $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
		$SESSIONID = uniqid ( $SESS );
		
		if ($_COOKIE ['PID']) {
			$SESSIONID = $_COOKIE ['PID'];
		}
		
		// Set Users Information
		$set_users_info = array (
				"LastAccess     =   NOW()",
				"SessionID      =   :SessionID",
				"OrgID          =   :OrgID",
				"MultiOrgID     =   :MultiOrgID" 
		);
		// Set parameters information

 		$params_users_info = array (
				":SessionID"    =>  $SESSIONID,
				":OrgID"        =>  $_SESSION['userportal_OrgID'],
				":MultiOrgID"   =>  $_SESSION['userportal_MultiOrgID'],
				":UserID"       =>  $_SESSION['userportal_authuserid'] 
		);
		// Set where users information
		$where_users_info = array ("UserID = :UserID");
		
		if ($keep == "yes") {
			$set_users_info [] = "Persist = 1";
		}
		
		// add to database
		$upd_users_info = $UserPortalUsersObj->updUsersInfo ( $set_users_info, $where_users_info, array (
				$params_users_info 
		) );
		
		// set session key on users browser
		if ($AUTH ['Persist'] == 1) {
			setcookie ( "PID", $SESSIONID, time () + 2592000, "/" );
		} else {
			setcookie ( "PID", $SESSIONID, time () + 7200, "/" );
		}

		// forward to section recorded in LOC
		$forwardlink = G::Obj('Organizations')->linkToLocationHoldTask(G::Obj('Organizations')->getLocationHoldIdByCookie());
                if ($forwardlink == "") {
                        $forwardlink = USERPORTAL_HOME . "index.php";
                }
		echo header ( 'Location: ' . $forwardlink );

             
       }else{ 
                   session_start();
                 $_SESSION['invalid_otp']="Invalid OTP";
                 $twofauth= USERPORTAL_HOME . "twofauth.php";
		 echo header ( 'Location: ' . $twofauth); die;
	}

}

if (isset ($_REQUEST['type']) &&  $_REQUEST['type'] == "resend") { 

    	                $getuserdata = $UserPortalUsersObj->getUserDetailInfoByEmail("*", $_SESSION['userportal_logged_email']);
	                  // OTP generate
				//random number generate for 2fa
			 $a = mt_rand(100000,999999); 

                          $where_info = array("UserID = :UserID");
			 //Set information
			 $set_info = array("TwofAuthToken= :TwofAuthToken");
			 //Set parameters
			 $params = array(":TwofAuthToken"=>$a, ":UserID"=>$_SESSION['userportal_authuserid']);
			 //Update User Minutes                      
			 $UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
		     $link = USERPORTAL_HOME . "twofauth.php";
             $helplink = "<a href='https://help.myirecruit.com/2fa'>https://help.myirecruit.com/2fa</a>";

			 $message    =   "\n Hello ".$getuserdata['FirstName'].",<br/><br/>";
			 $message   .=   "\n\n  Your verification code is: <b>".$a."</b><br><br>";
			 $message    .=   "\n\n Please enter the code on ".$link."<br>";
			//$message    .=   "\n\n\n ".IRECRUIT_HOME."twofauth.php<br>";
			 $message    .=   "<br><br>";
			 $message    .=   "\n\n\n Thanks,"."<br>";
			 $message    .=   "\n\n iRecruit Support"."<br>";
			 $message    .=   "\n".$helplink;
			 
			 //save otp into table   
			 $IrecruitUsersObj->updateUserOtp($AUTH['UserID'],$a);  
			 $PHPMailerObj->clearCustomProperties();
			 $PHPMailerObj->addAddress ($getuserdata['Email']);
			 $PHPMailerObj->setFrom('support@irecruit-software.com');
			 $PHPMailerObj->Subject = 'iRecruit UserPortal - Account Verification';
			 $PHPMailerObj->msgHTML ($message);
			 $PHPMailerObj->ContentType = 'text/plain';
			 //Send email
			 $PHPMailerObj->send (); 

                         $twofauth= USERPORTAL_HOME . "twofauth.php";
                        echo header ( 'Location: ' . $twofauth); die;


}
if ($OrgIDck == "") {
	echo '<html><head><title>iRecruit - blank</title></head><body>';
	echo '<br><br><br>';
	echo '<table border="0" cellspacing="0" cellpadding="0">';
	echo '<tr><td width="250">&nbsp;</td><td>';
	echo '<span style="font-family:Arial;font-size:10pt;">Powered by:</span><br>';
	echo '<a href="http://www.irecruit-software.com/" target="_blank">';
	echo '<img border="0" src="' . USERPORTAL_HOME . 'images/iRecruit.png" width="130">';
	echo '</a>';
	echo '</td></tr>';
	echo '</table>';
	echo '</body></html>';
	exit ();
}
 
if ($_COOKIE ['PID']) {

	// Get UserDetail Information
	$AUTH1 = $UserPortalUsersObj->getUserDetailInfoBySessionID ( "UserID, FirstName, LastName", $_COOKIE ['PID'] );

	// User already logged in	
	if ($AUTH1 ['UserID'] != "") {
		// forward to section recorded in LOC
		$forwardlink = G::Obj('Organizations')->linkToLocationHoldTask(G::Obj('Organizations')->getLocationHoldIdByCookie());
		if ($forwardlink == "") {
			$forwardlink = USERPORTAL_HOME . "index.php";
		}
		echo header ( 'Location: ' . $forwardlink );
	}

} // end if COOKIE

if (isset ( $_POST ['process'] ) == "Login") { 
	
	// Get UserDetail Information By Username and Password
	$AUTH = $UserPortalUsersObj->getUserDetailInfoByUserAndPassword ( "FirstName,UserID, Verification, Email, Persist,TwofEnable,TwofAuthToken", $_REQUEST ['email'], $_REQUEST ['password'] );
	 
	// if we have a good user and password match
         if($AUTH['TwofEnable'] == 1){  
                         session_start();
                         $_SESSION['userportal_OrgID']=$OrgID;
                         $_SESSION['userportal_MultiOrgID']=$MultiOrgID; 
			 $_SESSION['userportal_logged_email']=$AUTH['Email']; 
 			 $_SESSION['userportal_authuserid']=$AUTH['UserID']; 

                         // OTP generate
				//random number generate for 2fa
			 $a = mt_rand(100000,999999); 

                          $where_info = array("UserID = :UserID");
			 //Set information
			 $set_info = array("TwofAuthToken= :TwofAuthToken");
			 //Set parameters
			 $params = array(":TwofAuthToken"=>$a, ":UserID"=>$AUTH ['UserID']);
			 //Update User Minutes
			 $UserPortalUsersObj->updUsersInfo($set_info, $where_info, array($params));
			 $link = "<a href='".USERPORTAL_HOME . "twofauth.php'>two-factor login page</a>";
             		 $helplink = "<a href='https://help.myirecruit.com/2fa'>https://help.myirecruit.com/2fa</a>";

             		 $message    =   "\n Hello ".$AUTH ['FirstName'].",<br/><br/>";
			 $message   .=   "\n\n  Your verification code is: <b>".$a."</b><br><br>";
			 $message    .=   "\n\n Please enter the code on ".$link."<br>";
			//$message    .=   "\n\n\n ".IRECRUIT_HOME."twofauth.php<br>";
			 $message    .=   "<br><br>";
			 $message    .=   "\n\n\n Thanks,"."<br>";
			 $message    .=   "\n\n iRecruit Support"."<br>";
			 $message    .=   "\n".$helplink;
			 
			 //save otp into table  
			 $IrecruitUsersObj->updateUserOtp($AUTH['UserID'],$a);  
			 $PHPMailerObj->clearCustomProperties();
			 $PHPMailerObj->addAddress ($AUTH['Email']);
			 $PHPMailerObj->setFrom('support@irecruit-software.com');
			 $PHPMailerObj->Subject = 'iRecruit UserPortal - Account Verification';
			 $PHPMailerObj->msgHTML ($message);
			 $PHPMailerObj->ContentType = 'text/plain';
			 //Send email
			 $PHPMailerObj->send (); 
                      
         	         $twofauth= USERPORTAL_HOME . "twofauth.php";
		        echo header ( 'Location: ' . $twofauth); die;

	 }else if ($AUTH ['UserID'] && $AUTH['TwofEnable'] == 0) {
		
	    //$org_assigned_forms_count = $FormsInternalObj->assignedInternalFormsCount($OrgID, $MultiOrgID, $AUTH ['UserID']);
	    
		// set a new Session ID
		// randomize a session key
		$SESS = $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
		$SESSIONID = uniqid ( $SESS );
		
		if ($_COOKIE ['PID']) {
			$SESSIONID = $_COOKIE ['PID'];
		}
		
		// Set Users Information
		$set_users_info = array (
				"LastAccess     =   NOW()",
				"SessionID      =   :SessionID",
				"OrgID          =   :OrgID",
				"MultiOrgID     =   :MultiOrgID" 
		);
		// Set parameters information
		$params_users_info = array (
				":SessionID"    =>  $SESSIONID,
				":OrgID"        =>  $OrgID,
				":MultiOrgID"   =>  $MultiOrgID,
				":UserID"       =>  $AUTH ['UserID'] 
		);
		// Set where users information
		$where_users_info = array ("UserID = :UserID");
		
		if ($keep == "yes") {
			$set_users_info [] = "Persist = 1";
		}
		
		// add to database
		$upd_users_info = $UserPortalUsersObj->updUsersInfo ( $set_users_info, $where_users_info, array (
				$params_users_info 
		) );
		
		// set session key on users browser
		if ($AUTH ['Persist'] == 1) {
			setcookie ( "PID", $SESSIONID, time () + 2592000, "/" );
		} else {
			setcookie ( "PID", $SESSIONID, time () + 7200, "/" );
		}

		// forward to section recorded in LOC
		$forwardlink = G::Obj('Organizations')->linkToLocationHoldTask(G::Obj('Organizations')->getLocationHoldIdByCookie());
                if ($forwardlink == "") {
                        $forwardlink = USERPORTAL_HOME . "index.php";
                }
		echo header ( 'Location: ' . $forwardlink );

	} else {

		$ERROR = 'Login Failed. Please try again.';

	}

}
 // end process

// Display Login Page with forget password functions
$page_title = $title = "myiRecruit Login";
require_once 'Header.inc';

// Get InternationalTranslation Information
$INT = $FormFeaturesObj->getInternationalTranslation ( $OrgID );

if ($INT ['InternationalTranslation'] == "Y") {
	?>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<div class="row" style="margin:0 auto !important;padding: 10px 0;align-items: center;justify-content: space-around;display: flex;float: none;">
    <div class="col-lg-12">
        <div id="google_translate_element"></div>
    </div>
</div>
<?php } ?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <?php
    		// Set where condition
    		$where = array (
    				"OrgID           =   :OrgID",
    				"MultiOrgID      =   :MultiOrgID",
    				"PrimaryLogoType !=  ''" 
    		);
    		// Set parameters
    		$params = array (
    				":OrgID"         =>  $OrgID,
    				":MultiOrgID"    =>  (string) $MultiOrgID 
    		);
    		$results = $OrganizationsObj->getOrganizationLogosInformation ( "OrgID", $where, '', array (
    				$params 
    		) );
    		
    		$ImagePresent = $results ['count'];
    		
    		if ($ImagePresent > 0) {
    		    echo '<img src="' . USERPORTAL_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br><br>';
    		}
    		?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h2>
				Returning Applicants Login
			</h2>
        </div>
    </div>
    <?php
    if (isset ( $_GET ['process'] ) == "Lookup") {
    	
    	// Set columns
    	$columns = "Email, FirstName, LastName, UserID, Verification, OrgID, MultiOrgID";
    	// Set params
    	$params_lookup = array (
    			":Email"         =>  $lookup
    	);
    	// Set where condition
    	$where_lookup = array (
    			"Email           =   :Email"
    	);
    
    	// Get users information based on above condition
    	$results   =   $UserPortalUsersObj->getUsersInformation ( $columns, $where_lookup, "", "", array ($params_lookup) );
    	$AUTH      =   $results ['results'] [0];
    	
    	if ($AUTH ['Email']) {
    		
    		$to       =   $AUTH ['Email'];
    		$subject  =   $title . " Reminder";
    		
    		//Get Email and EmailVerified
    		$OE = $OrganizationDetailsObj->getEmailAndEmailVerified($_REQUEST ['OrgID'], $_REQUEST ['MultiOrgID']);
    		//Get OrganizationName
    		$OrgName = $OrganizationDetailsObj->getOrganizationName($_REQUEST ['OrgID'], $_REQUEST ['MultiOrgID']);

    		$user_details = $UserPortalUsersObj->getUserDetailInfoByEmail("*", $AUTH ['Email']);
    		
            //Clear properties
            $PHPMailerObj->clearCustomProperties();
            $PHPMailerObj->clearCustomHeaders();
    			
    		if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
    			// Set who the message is to be sent from
    			$PHPMailerObj->setFrom ( $OE['Email'], $OrgName );
    			// Set an alternative reply-to address
    			$PHPMailerObj->addReplyTo ( $OE['Email'], $OrgName );
    		}
    		

    		$random_session = crypt(time().rand(), rand().time().crypt(time(), rand(1, 20)));
    		$random_session = preg_replace("/[^a-zA-Z0-9]/", "", $random_session);


    		$UserPortalUsersObj->updUsersSessionID($random_session, $user_details['UserID']);

    		
    		$message = "Dear " . $user_details['FirstName'] . " " . $user_details['LastName']  . ",<br><br>";
    		$message .= "We received a request to reset your userportal password.<br><br>";
    		
    		if($_REQUEST ['MultiOrgID'] != "")
    		  $message .= "<a href='".USERPORTAL_HOME . "forgotPassword.php?OrgID=".$_REQUEST ['OrgID']."&MultiOrgID=".$_REQUEST ['MultiOrgID']."&ctact=".$random_session."'>Click here to reset password</a><br><br>";
    		else
    		  $message .= "<a href='".USERPORTAL_HOME . "forgotPassword.php?OrgID=".$_REQUEST ['OrgID']."&ctact=".$random_session."'>Click here to reset password</a><br><br>";
    		
    		$message .= "<strong>Password Tips: </strong>Passwords should have at least eight characters and include at least one uppercase character and at least one number.<br><br>";
    		$message .= "If you didn't request a password reset. Please ignore this message.<br><br>";
    		$message .= "Thank you,<br>";
    		$message .= $OrgName . "<br>";

    		if (DEVELOPMENT != "Y") {
                $message    =   nl2br($message);
                // Set who the message is to be sent to
                $PHPMailerObj->addAddress ( $to );
		        //$PHPMailerObj->addBCC("dedgecomb@irecruit-software.com", "David Edgecomb");

                // Set the subject line
                $PHPMailerObj->Subject = $subject;
                // convert HTML into a basic plain-text alternative body
                $PHPMailerObj->msgHTML ( $message );
                // Content Type Is HTML
                $PHPMailerObj->ContentType = 'text/html';
                //Send email
                $PHPMailerObj->send ();
    		}
    		
    		echo "<p style='color:blue'>Recovery email has been sent to your account.</p>";
    	} else {
    		
    		echo "<p style='color:blue'>Information for that email address was not found.</p>";
    		
    	}
    }
    
    if(isset($_GET['msg']) && $_GET['msg'] == 'pwdsuc') {
    	echo "<p style='color:blue'>Password successfully updated.</p>";
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
    
		    <form method="post" action="login.php">
		    <?php 
			if($ERROR != "") {
				?>
				<div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12" style="color: red">
                          <?php echo $ERROR?>
                        </div>                          
                </div>
				<?php
			}
			?>
			<div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="first_name" class="question_name">
                        Email: <span style="color:#FF0000">*</span>
                    </label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
				   <input type="text" name="email" size="30" maxlength="45" style="border: 1px solid #C0C0C0; padding: 4px" value="<?php echo $_REQUEST['email']?>">
                </div>
            </div>
			
            <div class="form-group row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <label for="first_name" class="question_name">
                        Password: <span style="color:#FF0000">*</span>
                    </label>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9">
					   <input type="password" name="password" size="30" style="border: 1px solid #C0C0C0; padding: 4px"> 
					   <input type="hidden" name="OrgID" value="<?php echo $OrgID?>"> 
					   <input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>">
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
					 <a href="javascript:ReverseDisplay('emailcheck')">
					     <font color="#0066CC">Forgot your username or password?</font>
					 </a>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
				   <input type="hidden" name="process" value="Login"> 
				   <input type="submit" value="Login" name="Login" class="btn btn-primary">
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-lg-12 col-md-12 col-sm-12">
					Please enter your email and password to log in to your account. If you do not have an account, please 
					<strong>
    					<a href="<?php echo USERPORTAL_HOME?>signup.php?OrgID=<?php echo $OrgID?>&MultiOrgID=<?php echo $MultiOrgID?>">
    					   click here to register
    					</a>
					</strong>
                </div>
            </div>
            
            </form>
            
        </div>
    </div>
    
    <div class="row" id="emailcheck" style="display: none;">
        <div class="col-lg-12">
                <br><br>
			    Please enter the email address associated to your account. We will send your login information to your email address.
				<form method="get" action="login.php">
				   <p>
						 <input type="text" name="lookup" size="30" value="<?php echo $lookup?>" style="border: 1px solid #C0C0C0; padding: 4px"><br> 
						 <input type="hidden" name="OrgID" value="<?php echo $OrgID?>"> 
						 <input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID?>"> 
						 <input type="hidden" name="process" value="Lookup"> <br> 
						 <input type="submit" value="Look Up" class="btn btn-primary">
					</p>
				</form>			
        </div>
    </div>
</div>
<?php
require_once USERPORTAL_DIR . 'Footer.inc';
?>
