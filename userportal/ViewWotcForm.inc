<?php
// get WOTC OrgData
$columns    		=   "OrganizationName";
$where      		=   array("wotcID = :wotcid");
$params     		=   array($WOTCID);
$ORGS_RES   		=   G::Obj('WOTCOrganization')->getOrgDataInfo($columns, $where, '', '', array($params));
$ORGS       		=   $ORGS_RES['results'];

//Get Applicant Data
$WOTCAPPDATA		=	G::Obj('WOTCApplicantData')->getApplicantDataInfo("*", $WOTCID, $WotcFormID, $ApplicationID);

//Get WOTC Applicant Data
$WOTCQUEANS			=	G::Obj('WOTCFormQueAns')->getWotcFormQueAnswersInfo($WOTCID, $WotcFormID, $ApplicationID);

$parent_ques		=	G::Obj('WOTCFormQuestions')->getWotcFormParentQuesForChildQues("MASTER", $WotcFormID);

//Row End
echo '<div class="page-inner">';

echo '<div class="row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12" style="margin:0px;padding:0px;color:blue">';
if($rtn_duplicate_info != "") {
	echo $rtn_duplicate_info;
}

//Get CCID
$CCID = G::Obj('WOTCOrganization')->getCCID($WOTCID, $ApplicationID);
echo "&nbsp;Please give this ID to your supervisor: " . $CCID;
echo '<br><br>';

echo '</div>';
echo '</div>';

//Row Start
echo '<div class="row">';

foreach ($WOTCQUEANS as $QuestionID=>$QuestionAnswerInfo) {


	$ParentQueID	=	$parent_ques[$QuestionID];

	if($QuestionAnswerInfo['QuestionTypeID'] != 99
    	&& $QuestionAnswerInfo['QuestionTypeID'] != 45
    	&& $QuestionID != 'Signature'
		&& $QuestionID != 'verify'
		&& $QuestionID != 'captcha') {

		if($ParentQueID != "") {

			$ChildQuestionsInfo		=	json_decode($WOTCQUEANS[$ParentQueID]['ChildQuestionsInfo'], true);
			$parent_que_val_info	=	$ChildQuestionsInfo[$WOTCAPPDATA[$ParentQueID]];

			if($parent_que_val_info[$QuestionID] == "show") {
				echo "<div class='row' style='margin-bottom:1px;padding:5px;'>";

				echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
				echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
				echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";

				echo "</div>";
			}
		}
		else {

			if ($QuestionID == "County") {
				$QuestionAnswerInfo['Answer']=$WOTCAPPDATA['County'];
			}

			echo "<div class='row' style='margin-bottom:1px;padding:5px;'>";

			echo "<div class='col-lg-3 col-md-4 col-sm-3'>".$QuestionAnswerInfo['Question']."&nbsp;&nbsp;</div>";
			echo "<div class='col-lg-9 col-md-8 col-sm-9'><strong>".$QuestionAnswerInfo['Answer']."</strong></div>";
			echo "<div class='col-lg-12 col-md-12 col-sm-12' id='question_space'></div>";

			echo "</div>";
		}
	}

}
?>

<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info =   JSON.parse('<?php echo json_encode($child_que_list);?>');
</script>
<script type="text/javascript" src="<?php echo WOTC_HOME;?>js/child-questions.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	getFormIDChildQuestionsInfo();

	$( ".row" ).click(function() {
		$("div[id^='divQue-']").css( "background", "#ffffff" );
	  	var next_divs	=	$(this).nextAll( ".form-group" );

	  	for(i = 0; i < next_divs.length; i++) {

	  		if(document.getElementById(next_divs[i].id).style.display == 'block'
		  		|| document.getElementById(next_divs[i].id).style.display == '') {
	  			$("#"+next_divs[i].id).css( "background", "#f5f5f5" );

	  			i = next_divs.length;	//Break the loop if element found
			}
		}
	});
});
<?php
if($DATE_IDS != "") {
	?>
    var dates = $('<?php echo $DATE_IDS;?>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: 'button',
                    buttonImage: '<?php echo WOTCADMIN_HOME; ?>calendar/css/smoothness/images/calendar.gif',
                    yearRange: '-80:+5',
                    buttonImageOnly: true,
                    buttonText: 'Select Date'
                });
	<?php
}
?>
</script>
<?php
echo '</div>';	//Row End
echo '</div>';
?>
