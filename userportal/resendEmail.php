<?php 
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

if (isset($_REQUEST['resend']) && $_REQUEST['resend'] == "Y") {

    $USERS = $UserPortalUsersObj->getUserDetailInfoByUserID("*", $UpUserID);
    if ($USERS ['Email']) {
        $to       =   $USERS ['Email'];

        //Get Email and EmailVerified
        $OE = $OrganizationDetailsObj->getEmailAndEmailVerified($USERS ['OrgID'], $USERS ['MultiOrgID']);
        //Get OrganizationName
        $OrgName = $OrganizationDetailsObj->getOrganizationName($USERS ['OrgID'], $USERS ['MultiOrgID']);
        
        $subject  =   $OrgName . "  Registration Confirmation";
        
        //Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
        
        if (($OE ['Email']) && ($OE ['EmailVerified'] == 1)) {
            // Set who the message is to be sent from
            $PHPMailerObj->setFrom ( $OE['Email'], $OrgName );
            // Set an alternative reply-to address
            $PHPMailerObj->addReplyTo ( $OE['Email'], $OrgName );
        }

        $headers .= 'Content-type: text/html; charset=iso-8859-1';
        
        $message .=   "Please click on the link below (or copy and paste) in order to verify your email address to activate your account." . "\n";
        $link     =   'verifyEmail.php'. "?OrgID=" . $USERS ['OrgID'];
	if ($USERS ['MultiOrgID'] != '') {
	  $link    .= "&MultiOrgID=" . $USERS ['MultiOrgID'];
	}
	$link	 .= "&acc=" . $USERS['SessionID'];

        $message .=   "\n\nPlease visit: <a href='" . USERPORTAL_HOME . $link . "'>" . USERPORTAL_HOME . $link . "</a>\n\n";
        $message .=   "For additional help please visit: <a href='http://help.myirecruit.com'>http://help.myirecruit.com</a>" . "\n";
        
        if (DEVELOPMENT != "Y") {
            //Set message
            $message = nl2br($message);
            // Set who the message is to be sent to
            $PHPMailerObj->addAddress ( $to );
            // Set the subject line
            $PHPMailerObj->Subject = $subject;
            // convert HTML into a basic plain-text alternative body
            $PHPMailerObj->msgHTML ( $message );
            // Content Type Is HTML
            $PHPMailerObj->ContentType = 'text/html';
            //Send email
            $PHPMailerObj->send ();
        }

        echo 'Email has been sent to: ' . $USERS ['Email'] . "\n". "Please respond to the link sent to that address.";
    } // end email check
} // end resend = Y
?>
