<?php
$colwidth = "220";

echo '<div class="form-group row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12">';
echo "<strong> Note: </strong>Please be sure to click on the Upload File button after selecting your file.";
echo '</div>';
echo '</div>';


echo '<div class="form-group row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12">';
echo '<label class="question_name"><b>Applicant Vault</b></label>';
echo '</div>';
echo '</div>';
  
// Bind the parameters
$params = array (
		":OrgID"          =>  $OrgID,
		":ApplicationID"  =>  $ApplicationID,
		":RequestID"      =>  $RequestID 
);
// Set the condition
$where = array (
		"OrgID            =   :OrgID",
		"ApplicationID    =   :ApplicationID",
		"RequestID        =   :RequestID" 
);

if (FROM_SRC == "USERPORTAL") {
	$where [] = "CONCAT('', UserID * 1) = UserID";
}

// Set Columns
$columns    =   "FileName, FileType, date_format(Date,'%Y-%m-%d') Date, UpdateID, UserID";
// Get Applicant Vault Information
$results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( $columns, $where, 'FileName', array ($params) );
// Get Count Applicant Vault Information Records Count
$cnt        =   $results ['count'];

if(FROM_SRC == 'USERPORTAL')
	$applicant_vault_url = USERPORTAL_HOME . 'formsInternal/applicantVault.php';
if(FROM_SRC == 'IRECRUIT')
	$applicant_vault_url = IRECRUIT_HOME . 'applicants/applicantVault.php';

echo '<form method="POST" name="frmApplicantVault" id="frmApplicantVault" enctype="multipart/form-data">';

echo '<input type="hidden" name="ApplicationID" value="' . htmlspecialchars($ApplicationID) . '">';
echo '<input type="hidden" name="RequestID" value="' . htmlspecialchars($RequestID) . '">';
echo '<input type="hidden" name="process" value="' . htmlspecialchars($process) . '">';
echo '<input type="hidden" name="display_app_header" value="' . htmlspecialchars($_REQUEST['display_app_header']) . '">';
echo '<input type="hidden" name="UpdateID" value="' . htmlspecialchars($UpdateID) . '">';

if ($action != "") {
    echo '<input type="hidden" name="action" value="' . htmlspecialchars($action) . '">';
}
if ($PreFilledFormID != "") {
    echo '<input type="hidden" name="PreFilledFormID" value="' . htmlspecialchars($PreFilledFormID) . '">';
}

echo '<input type="hidden" name="MAX_FILE_SIZE" value="10485760">';

echo '<div class="form-group row">';

echo '<div class="col-lg-4 col-md-4 col-sm-5">';
echo '<input type="file" name="newfile" value="">';
echo '</div>';

echo '<div class="col-lg-8 col-md-8 col-sm-7">';
echo '<input type="submit" value="' . $submit . '" class="btn btn-primary">';
echo '</div>';

echo '</div>';

echo '</form>';

echo '<div class="form-group row"><div class="col-lg-12 col-md-12 col-sm-12"><br></div></div>';

if ($cnt > 0) {
    echo '<div class="form-group row">';
    echo '<div class="col-lg-12 col-md-12 col-sm-12">';
    
    echo '<div class="table-responsive">';
	echo '<table border="0" cellspacing="3" cellpadding="5" align="center" class="table table-striped table-bordered table-hover">';
	
	echo '<tr height="30">';
	
	echo '<td><strong>File Name</strong></td>';
	echo '<td><strong>Date</strong></td>';
	echo '<td><strong>Uploaded By</strong></td>';
	echo '<td align="center" width="60"><strong>View</strong></td>';
	echo '<tr>';
	
	$rowcolor = "#eeeeee";
	
	if (is_array ( $results ['results'] )) {
		foreach ( $results ['results'] as $AV ) {
			
			echo '<tr bgcolor="' . $rowcolor . '">';
			
			echo '<td>';
			echo $AV ['FileName'];
			echo '.';
			echo $AV ['FileType'];
			echo '</td>';
			
			echo '<td>';
			echo $AV ['Date'];
			echo '</td>';
			
			echo '<td>';
			
			if (is_numeric ( $AV ['UserID'] )) {
				
				echo "PORTAL:<br>";
				
				// Set condition
				$where  =   array("UserID = :UserID");
				// Bind the parameters
				$params =   array(":UserID" => $AV ['UserID']);
				// Get User Information
				$resultsUP = $UserPortalUsersObj->getUsersInformation("FirstName, LastName, Email", $where, "", "", array ($params) );
				$User   =   $resultsUP ['results'] [0] ['LastName'].", ".$resultsUP ['results'] [0] ['FirstName'];
				$Em     =   $resultsUP ['results'] [0] ['Email'];
				
				echo $User . "<br>";
				
				if ($permit ['Applicants_Contact'] == 1) { // if access to view email
					echo '<a href="mailto:' . $Em . '">' . $Em . '</a>';
				} else {
					echo $Em;
				}
			} else {
				echo $AV ['UserID'];
			}
			
			echo '</td>';
			
			echo '<td align="center">';
			echo '<a href="' . IRECRUIT_HOME . 'applicants/display_applicantVault_File.php?OrgID=' . $OrgID . '&ApplicationID=' . $ApplicationID . '&UpdateID=' . $AV ['UpdateID'];
			if ($action != "") {
				echo '&action=' . htmlspecialchars($action);
			}
			if ($PreFilledFormID != "") {
				echo '&PreFilledFormID=' . htmlspecialchars($PreFilledFormID);
			}
			echo '">';
			echo '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
			echo '</a>';
			echo '</td>';
			
			echo '</tr>' . "\n";
			
			if ($rowcolor == "#eeeeee") {
				$rowcolor = "#ffffff";
			} else {
				$rowcolor = "#eeeeee";
			}
		} // end foreach
	}
	
	echo '</td></tr>';
	echo '</table>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
} // end $cnt applicantVault items
?>