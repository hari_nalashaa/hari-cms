<?php
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

$FormID                 =   $_REQUEST['FormID'];
$ApplicationID		=   $_REQUEST['ApplicationID'];
$RequestID		=   $_REQUEST['RequestID'];

$requisition_info       =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $OrgID, $_REQUEST['RequestID']);

$processed_sections     =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, "ProcessedSections");

if($processed_sections  == NULL || $processed_sections == "" || empty($processed_sections) || !isset($processed_sections)) {
    $processed_sections =   array();
}
else {
    $processed_sections =   unserialize($processed_sections);
}

require_once USERPORTAL_DIR . 'ValidateRequiredFields.inc';

if(count($errors_list) == 0) {

   // Pull existing data before changes
   $APPDATA =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);

   // Section Processing
   if($_REQUEST['SectionID'] == '6') {

    require_once USERPORTAL_DIR . 'ProcessEditApplicationAttachments.inc';

   }
   else if($_REQUEST['SectionID'] == '14') {
        
      if ($_FILES ['applicant_profile_picture'] ['name'] != "") {
        // FILEHANDLING
        $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
        
        if (! file_exists ( $dir )) {
            mkdir ( $dir, 0700 );
            chmod ( $dir, 0777 );
        }
        
        $app_picture_dir = $dir . '/applicant_picture';
        
        if (! file_exists ( $app_picture_dir )) {
            mkdir ( $app_picture_dir, 0700 );
            chmod ( $app_picture_dir, 0777 );
        }
        
            
        $data       =   file_get_contents ( $_FILES ['applicant_profile_picture'] ['tmp_name'] );
        $e          =   explode ( '.', $_FILES ['applicant_profile_picture'] ['name'] );
        $ecnt       =   count ( $e ) - 1;
        $ext        =   $e [$ecnt];
        $ext        =   preg_replace ( "/\s/i", '', $ext );
        $ext        =   substr ( $ext, 0, 5 );
            
        $filename   =   $app_picture_dir . '/' . $ApplicationID . '-ApplicantPicture.' . $ext;
        $fh         =   fopen ( $filename, 'w' );
        fwrite ( $fh, $data );
        fclose ( $fh );
            
        chmod ( $filename, 0666 );
            
        $ProfileAnswer = $ApplicationID . '-ApplicantPicture.'.$ext;
            
        $app_data_aft_upd["ApplicantPicture"] = $ProfileAnswer;

        //Set Question Information
        $QI =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "QuestionID"=>"ApplicantPicture", "Answer"=>$ProfileAnswer);
        //Insert update applicant data
        G::Obj('Applicants')->insUpdApplicantData($QI);

	$COMPARE['ApplicantPicture']['Question']   	 =   "Applicant Profile Picture";
	$COMPARE['ApplicantPicture']['QuestionID']   	 =   "ApplicantPicture";
	$COMPARE['ApplicantPicture']['QuestionTypeID']   =   "6";
	$COMPARE['ApplicantPicture']['values']           =   "";

	if ($APPDATA[$QI['QuestionID']] != "") { $file_present = "File Present."; } else { $file_present = ""; }
	$COMPARE['ApplicantPicture']['APPDATA']          =   $file_present;
	$COMPARE['ApplicantPicture']['REQUEST']          =   "A new picture has been uploaded.";


      }
   }
   else { // else Section 6 & 14

    $QUESTIONS =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID, $_REQUEST['SectionID']);
    G::Obj('GetFormPostAnswer')->POST =   $_REQUEST;    //Set Post Data
    $COMPARE=array();

        foreach($QUESTIONS['json_ques'] as $QI) {
	    G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;

            $QI['UserID']          =   $UpUserID;
            $QI['RequestID']       =   $RequestID;
            $QI['MultiOrgID']      =   $MultiOrgID;
            $QI['Answer']          =   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
            $QI['ApplicationID']   =   $ApplicationID;
            $QI['AnswerStatus']    =   1;

		if ($QI['Answer'] != $APPDATA[$QI['QuestionID']]) {

	          $COMPARE[$QI['QuestionID']]['Question']   	  =   $QI['Question'];
	          $COMPARE[$QI['QuestionID']]['QuestionID']   	  =   $QI['QuestionID'];
	          $COMPARE[$QI['QuestionID']]['QuestionTypeID']   =   $QI['QuestionTypeID'];
		  $COMPARE[$QI['QuestionID']]['values']           =   $QI['value'];
	          $COMPARE[$QI['QuestionID']]['APPDATA']          =   $APPDATA[$QI['QuestionID']];
		  $COMPARE[$QI['QuestionID']]['REQUEST']          =   $QI['Answer'];

		}

            G::Obj('Applicants')->insUpdApplicantData($QI);
        }


   } // end all else Section 6 & 14
    
   // apply for all sections
   
   // Update ApplicantSortName on JobAplications for Section 1 - Personal Section	
   if($_REQUEST['SectionID'] == "1") {
       //Set Update Information
       $jobapp_set_info      =   array("LastModified = NOW()");
       $jobapp_set_info[]    =   "ApplicantSortName = :ApplicantSortName";
            
       //Set Update Where Condition
       $jobapp_upd_where     =   array(
                "OrgID                  =   :OrgID",
                "ApplicationID          =   :ApplicationID",
                "RequestID              =   :RequestID"
       );
       //Set Update Parameters
       $jobapp_upd_params    =  array(
                ":OrgID"                =>  $OrgID,
                ":ApplicationID"        =>  $ApplicationID,
                ":RequestID"            =>  $RequestID
       );
            
       $jobapp_upd_params[":ApplicantSortName"]  =   $_REQUEST ['last'] . ', ' . $_REQUEST ['first'];
            
       G::Obj('Applications')->updApplicationsInfo('JobApplications', $jobapp_set_info, $jobapp_upd_where, array($jobapp_upd_params));
   }

   // Turn off edit capability after sucessfull submit
   if($_REQUEST['SectionID'] == 'VET' || $$_REQUEST['SectionID'] == 'AA' || $_REQUEST['SectionID'] == 'DIS') {
    
      if($_REQUEST['SectionID'] == "VET") {
        $set_info   =   array("VeteranEditStatus    =   :Status");
      }
      else if($_REQUEST['SectionID'] == "DIS") {
        $set_info   =   array("DisabledEditStatus   =   :Status");
      }
      else if($_REQUEST['SectionID'] == "AA") {
        $set_info   =   array("AAEditStatus         =   :Status");
      }
    
      $where_info =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
      $params     =   array(":Status"=>'N', ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        
      G::Obj('Applications')->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));
   }

   // update processed sections
   if(!in_array($_REQUEST['SectionID'], $processed_sections)) {

           $processed_sections[]   =   $_REQUEST['SectionID'];

           $QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
           G::Obj('Applicants')->insUpdApplicantData($QI);

   }
    
   // Process History for changes
   if (count($COMPARE) > 0) {

	$CHANGEHISTORY = array();
	$CHANGES = array();

	$userportal_sections_list  = G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
	$section_title      =   $userportal_sections_list[$_REQUEST['SectionID']]['SectionTitle'];

	$CHANGEHISTORY['Section'] = $section_title;

        foreach ($COMPARE as $C) {

           $CHANGES[]=$C;	

        } // end foreach

	$CHANGEHISTORY['Updated Fields']=$CHANGES;

        // Job Application History Information
        $job_app_history_info   =   array (
                                    "OrgID"                 =>  $OrgID,
                                    "ApplicationID"         =>  $ApplicationID,
                                    "RequestID"             =>  $RequestID,
                                    "ProcessOrder"          =>  "-84",  //For update fields track in userportal
                                    "Date"                  =>  "NOW()",
                                    "StatusEffectiveDate"   =>  "NOW()",
                                    "UserID"                =>  $UpUserID,
                                    "Comments"              =>  'Application data updated.',
                                    "UpdatedFields"         =>  json_encode($CHANGEHISTORY)
                                    );
        //Insert Job Application History
        G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history_info );

   } // end if count COMPARE

   echo json_encode(array("errors_list"=>array(), "success"=>"true"));
   exit;

} else { // else errors > 0

   // unset the section processed if there is an error
   foreach ($processed_sections AS $key => $value) {
	  if ($value == $_REQUEST['SectionID']) {
	    unset($processed_sections[$key]);
	  }
   }

   $QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
   G::Obj('Applicants')->insUpdApplicantData($QI);

   echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
   exit;

}
?>
