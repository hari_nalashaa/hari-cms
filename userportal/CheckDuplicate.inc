<?php
// Delete applicant lock information based on below condition
$where      =   array("EntryDate < DATE_ADD(NOW(), INTERVAL -5 MINUTE)");
G::Obj('Applicants')->delApplicantLock($where);

// Set columns
$columns    =   "concat(FirstName, LastName, Address, City, Zip, Email, SelectedPosition) AS applicant_lock_info";
// Set where condition
$where      =   array(
                    "FirstName          =   :FirstName",
                    "LastName           =   :LastName",
                    "Address            =   :Address",
                    "City               =   :City",
                    "Zip                =   :Zip",
                    "Email              =   :Email",
                    "SelectedPosition   =   :SelectedPosition"
                );
// Set parameters
$params     =   array(
                    ":FirstName"        =>  $first,
                    ":LastName"         =>  $last,
                    ":Address"          =>  $address,
                    ":City"             =>  $city,
                    ":Zip"              =>  $zip,
                    ":Email"            =>  $email,
                    ":SelectedPosition" =>  $RequestID
                );
// Get ApplicantLockInformation
$results    =   G::Obj('Applicants')->getApplicantLockInfo($columns, $where, '', array($params));

if (is_array($results['results'])) {
    foreach ($results['results'] as $row) {
        $appentered .= $row['applicant_lock_info'];
    }
}

//Get UserApplications
$user_apps_info         =   G::Obj('UserPortalInfo')->getUserApplications($OrgID, $MultiOrgID, $UpUserID, $RequestID);
$user_applications      =   $user_apps_info['results'];
$user_applications_cnt  =   $user_apps_info['count'];

if($user_applications_cnt > 0) {
    if($user_applications[0]['Status'] == 'Finished') {
        $appentered     =   "Duplicate";
    }
}

if ($appentered) {
    $DUPERROR = "An application with this information has recently been submitted.\\n\\n";
    $DUPERROR .= "Please check your email for confirmation before submitting again.";
}
?>