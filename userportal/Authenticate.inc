<?php
$ThrowAuth  = 0;
$UpUserID   = "";
$OrgID      = "";
$MultiOrgID = "";

// general lockout for time
$lockout_time_status = $UserPortalUsersObj->updateUsersSessionID ();

if (! isset ( $_COOKIE ['PID'] )) {
	$ThrowAuth ++;
} else {

if(isset($_REQUEST['clear_user']) && $_REQUEST['clear_user'] == "Y") {

	$_SESSION['U']['UserIDMasterValue'] = '';
	unset($_SESSION['U']['UserIDMasterValue']);

        $AUTH = $UserPortalUsersObj->getUserPortalUserInfo ();

                // Set Users Information
                $upd_set_users_info = array (
                                "OrgID          =   'B12345467'",
                                "MultiOrgID     =   ''"
                );
                // Set where users information
                $upd_where_users_info = array ("UserID = :UserID");

                // Set parameters information
                $upd_params_users_info = array (
                                ":UserID"       =>  $AUTH['UserID']
                );

                // update login user OrgID, MultiOrgID
                $upd_users_info = $UserPortalUsersObj->updUsersInfo ( $upd_set_users_info, $upd_where_users_info, array ($upd_params_users_info) );

}

    if(isset($_SESSION['U']['UserIDMasterValue']) && $_SESSION['U']['UserIDMasterValue'] != "") {
        // Get User Information based on Master Value
        $AUTH = $UserPortalUsersObj->getUserDetailInfoByEmail("*", $_SESSION['U']['UserIDMasterValue']);
    }
    else {
        // Get Userportal Login User Information
        $AUTH = $UserPortalUsersObj->getUserPortalUserInfo ();
    }

	$UpUserID              =   $AUTH['UserID'];
	$OrgID                 =   $AUTH['OrgID'];
	$MultiOrgID            =   $AUTH['MultiOrgID'];
	$Persist               =   $AUTH['CastPersist'];
	$DefaultProfile        =   $AUTH['DefaultProfile'];
	$EmailVerifiedStatus   =   $AUTH['EmailVerified'];
	$ProfileAvatarPicture  =   $AUTH['ProfileAvatarPicture'];
	
	$profile_data_info = $UserPortalUsersObj->getProfileDataByUserIDProfileID($UpUserID, 'STANDARD');
	
	foreach($profile_data_info as $profile_data_key=>$userportal_profile_data) {
		$UserPortalUserInfo [$userportal_profile_data ['QuestionID']] = $userportal_profile_data ['Answer'];
	}
	
	$brand_info            =   $OrganizationDetailsObj->getOrganizationInformation($OrgID, "", "BrandID");
	if(!isset($brand_info['BrandID']) || $brand_info['BrandID'] == "") $brand_info['BrandID'] = "0";
	$brand_org_info = $BrandsObj->getBrandInfo($brand_info['BrandID']);
}

if (! $UpUserID) {
	$ThrowAuth ++;
}

if (! $OrgID) {
	$ThrowAuth ++;
}

if ($ThrowAuth >= 1) {
	
	$loc = "?";
	
	if ($_SERVER ['QUERY_STRING']) {
		$loc = $_SERVER ['SCRIPT_NAME'] . '?' . $_SERVER ['QUERY_STRING'];
	} elseif ($_POST) {
		foreach ( $_POST as $k => $j ) {
			$loc .= $k . '=' . $j . "&";
		}
		
		$loc = substr ( $loc, 0, - 1 );
	} // end if QUERY_STRING or POST
	
	header ( 'Location: login.php' . $loc );
	exit ();
} else {
	
	$AF = $IrecruitApplicationFeaturesObj->getApplicationFeatures ();
	$afi = 0;
	foreach ( $AF as $key => $value ) {
		if ($afi > 0) {
			$featureList [$afi] = $key;
		}
		$afi ++;
	}
	
	// set functionality for add on modules
	$row = $IrecruitApplicationFeaturesObj->getApplicationFeaturesByOrgID ( $OrgID );
	
	foreach ( $featureList as $s ) {
		$feature [$s] = $row [$s];
	}
	
	$InternalCode = "internal";
	if ($feature ['InternalRequisitions'] == "Y") {
		$InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
	}
	
	// Update UserPortal User Last Access
	$UserPortalUsersObj->updateUserPortalUserLastAccess ();
	
	if ($Persist == 1) {
		setcookie ( "PID", $_COOKIE ['PID'], time () + 2592000, "/" );
	} else {
		setcookie ( "PID", $_COOKIE ['PID'], time () + 7200, "/" );
	}
}
?>
