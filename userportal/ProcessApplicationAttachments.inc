<?php
// FILEHANDLING
$dir = IRECRUIT_DIR . 'vault/' . $OrgID;

if (! file_exists ( $dir )) {
	mkdir ( $dir, 0700 );
	chmod ( $dir, 0777 );
}

$apatdir = $dir . '/applicantattachments';

if (! file_exists ( $apatdir )) {
	mkdir ( $apatdir, 0700 );
	chmod ( $apatdir, 0777 );
}

$purpose    =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

//Set parameters
$params     =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID);
// Set Form Question Fields
$where      =   array ("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID = 8", "Active = 'Y'");
//Get FormQuestions Information
$resultsIN  =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder", array ($params) );

if (is_array ( $resultsIN ['results'] )) {
	foreach ( $resultsIN ['results'] as $FIL ) {
		
	    $file_id = "";
	    if($FIL ['QuestionID'] == 'coverletterupload') {
	        $file_id = "new_file_coverletterupload";
	    }
	    else if($FIL ['QuestionID'] == 'otherupload') {
	        $file_id = "new_file_otherupload";
	    }
	    else if($FIL ['QuestionID'] == 'resumeupload') {
	        $file_id = "new_file_resumeupload";
	    }
	    else {
	        $file_id  =   $FIL ['QuestionID'];
	    }
	    
	    if ($_FILES [$FIL ['QuestionID']] ['type'] != "") {
			
			$e       =   explode ( '.', $_FILES [$FIL ['QuestionID']] ['name'] );
			$ecnt    =   count ( $e ) - 1;
			$ext     =   $e [$ecnt];
			$ext     =   preg_replace ( "/\s/i", '', $ext );
			$ext     =   substr ( $ext, 0, 5 );
			
			//Applicant Attachments Information
			$applicant_attach_info = array (
				"OrgID"            =>  $OrgID,
                "MultiOrgID"       =>  $MultiOrgID,
				"UserID"           =>  $UpUserID,
		        "SectionID"        =>  $_REQUEST['SectionID'],
		        "RequestID"        =>  $_REQUEST['RequestID'],
				"TypeAttachment"   =>  $FIL ['QuestionID'],
				"PurposeName"      =>  $purpose [$FIL ['QuestionID']],
				"FileType"         =>  $ext 
			);
			//Insert applicant attachments information
			G::Obj('UserPortalInfo')->insApplicantAttachmentsTemp ( $applicant_attach_info );
			
			$filename = $apatdir . '/' . $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$FIL ['QuestionID']] . '.' . $ext;
			$tmp_name = $_FILES [$FIL ['QuestionID']] ['tmp_name'];
			move_uploaded_file($tmp_name, $filename);

			chmod ( $filename, 0666 );
		}
		else if ($_FILES [$file_id] ['type'] != "") {
		    	
		    $e       =   explode ( '.', $_FILES [$file_id] ['name'] );
		    $ecnt    =   count ( $e ) - 1;
		    $ext     =   $e [$ecnt];
		    $ext     =   preg_replace ( "/\s/i", '', $ext );
		    $ext     =   substr ( $ext, 0, 5 );

		    //Applicant Attachments Information
		    $applicant_attach_info = array (
		        "OrgID"            =>  $OrgID,
		        "MultiOrgID"       =>  $MultiOrgID,
		        "UserID"           =>  $UpUserID,
		        "SectionID"        =>  $_REQUEST['SectionID'],
		        "RequestID"        =>  $_REQUEST['RequestID'],
		        "TypeAttachment"   =>  $FIL ['QuestionID'],
		        "PurposeName"      =>  $purpose [$FIL ['QuestionID']],
		        "FileType"         =>  $ext
		    );
		    //Insert applicant attachments information
		    G::Obj('UserPortalInfo')->insApplicantAttachmentsTemp ( $applicant_attach_info );

		    $filename =   $apatdir . '/' . $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$FIL ['QuestionID']] . '.' . $ext;
		    $tmp_name =   $_FILES [$file_id] ['tmp_name'];
		    move_uploaded_file($tmp_name, $filename);

		    chmod ( $filename, 0666 );
		}
	} // end foreach
} 
// END FILEHANDLING
?>