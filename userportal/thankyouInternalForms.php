<?php
$title = 'View Internal Form';
require_once 'userportal.inc';

if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['PreFilledFormID']) && $_REQUEST['PreFilledFormID'] != "") $PreFilledFormID = $_REQUEST['PreFilledFormID'];
if(isset($_REQUEST['WebFormID']) && $_REQUEST['WebFormID'] != "") $WebFormID = $_REQUEST['WebFormID'];
if(isset($_REQUEST['AgreementFormID']) && $_REQUEST['AgreementFormID'] != "") $AgreementFormID = $_REQUEST['AgreementFormID'];
if(isset($_REQUEST['specformid']) && $_REQUEST['specformid'] != "") $specformid = $_REQUEST['specformid'];
if(isset($_REQUEST['ProcessOrder']) && $_REQUEST['ProcessOrder'] != "") $ProcessOrder = $_REQUEST['ProcessOrder'];

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
}

require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

// if not a listings area for the new app process then navigate to these pages
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

echo '<div id="page-wrapper">';			//Page Wrapper Start
echo '<div class="page-container">';	//Page Container End

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';

echo 'Thank You: ';    

echo "ApplicationID: ".$_REQUEST['ApplicationID'];

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'assignedInternalForms.php?ApplicationID='.htmlspecialchars($_REQUEST['ApplicationID']).'&RequestID='.htmlspecialchars($_REQUEST['RequestID']).'&ProcessOrder='.htmlspecialchars($_REQUEST['ProcessOrder']).'&navpg=status&navsubpg=view">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Assigned Forms';
echo '</a>';
echo '</span>';

echo '</h3>';

echo '</div>';
echo '</div>';


echo '<div class="page-inner">';

include 'ApplicationInformation.inc';

//Row Start
echo '<div class="row">';

echo '<div class="col-lg-12">';

echo '<br><br>';

//Assigned Forms List
$assigned_forms_list    =   G::Obj('FormsInternal')->getAssignedFormsInfo($OrgID, $RequestID, $ApplicationID, $_REQUEST['ProcessOrder']);

$link                   =   "";
$link_hit_status        =   false;

for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
    
    $assigned_forms_info    =   $assigned_forms_list[$afl];
    
    foreach($assigned_forms_info as $FormType=>$IFA) {
        
        if($IFA["Status"] == "Pending"
            && $FormType != "Attachment"
            && $FormType != "External Link"
            && $FormType != "HTML Page") {
                
                $form_status        =   $IFA["IFAInfo"]["Status"];
                $form_status_text   =   $IFA["FormStatusText"];
                $link               =   $IFA["Link"];
                $form_name	    =   $IFA["FormName"];
                
                $afl                =   count($assigned_forms_list);
                
                $link_hit_status    =   true;
            }
            
            if($link_hit_status == true) {
                break;
            }
    }
}

if($link_hit_status == true) {
echo '<font style="color:red;font-size:12pt;">You have successfully submitted the form.</font>';
echo '<br>';
echo 'Go to the next form: <a href="'.$link.'">'.$form_name;
echo '<img src="' . IRECRUIT_HOME . '/images/icons/arrow_right.png" border="0" title="Next Form" style="margin: 0px 0px 4px 4px;"></a>';
}
echo '</div>';

echo '</div>';	//Row End

echo '</div>';	//Page Inner End
echo '</div>';	//Page Container End
echo '</div>';	//Page Wrapper

echo "<div style='clear:both'><br><br></div>";

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>
