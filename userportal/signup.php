<?php
if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") $MultiOrgID = $_REQUEST['MultiOrgID'];

require_once 'userportal.inc';

$loc_info           =   G::Obj('Organizations')->getLocationHoldIdByCookie();
$requisition_title  =   G::Obj('RequisitionDetails')->getJobTitle($OrgID, $MultiOrgID, $loc_info['RequestID']);

$OrgIDck = "";

// Set where condition
$where      =   array ("OrgID = :OrgID");
// Set parameters
$params     =   array (":OrgID" => $OrgID);
// Get OrganizationData Information
$results    =   G::Obj('Organizations')->getOrgDataInfo ( "OrgID", $where, '', array ($params) );
$OrgIDck    =   @$results ['results'][0]['OrgID'];

if ($OrgIDck == "") {
	echo '<html><head><title>iRecruit - blank</title></head><body>';
	echo '<br><br><br>';
	echo '<table border="0" cellspacing="0" cellpadding="0">';
	echo '<tr><td width="250">&nbsp;</td><td>';
	echo '<span style="font-family:Arial;font-size:10pt;">Powered by:</span><br>';
	echo '<a href="http://www.irecruit-software.com/" target="_blank">';
	echo '<img border="0" src="' . USERPORTAL_HOME . 'images/iRecruit.png" width="130">';
	echo '</a>';
	echo '</td></tr>';
	echo '</table>';
	echo '</body></html>';
	exit ();
}

if ($_COOKIE ['PID']) {
	
	// Set columns
	$columns = "UserID, OrgID, MultiOrgID";
	// Set where condition
	$where = array ("SessionID = :SessionID");
	// Set parameters
	$params = array (":SessionID" => $_COOKIE ['PID']);
	// Get UserPortal Users Information
	$results = $UserPortalUsersObj->getUsersInformation ( $columns, $where, "", "", array ($params) );
	$AUTH = $results ['results'] [0];
	
	if ($AUTH ['UserID'] != "") {
		$link = USERPORTAL_HOME . "index.php";
		header ( 'Location: ' . $link );
		exit ();
	}
} // end if COOKIE

$ERROR = "";

if (isset ( $_POST ['process'] ) == "Join Now") {
	// Get UserDetails based on user name
	$USRS = $UserPortalUsersObj->getUserDetailInfoByEmail ( "Email", $_REQUEST ['email'] );
	
	$UserCK = $USRS ['Email'];
	
	if ($UserCK) {
		$ERROR .= "There is an account already exists with this email. Click on forgot password to recover your account." . "\\n";
	}
	
	$ERROR .= $ValidateObj->validate_email_address ( $_REQUEST ['email'] );
	$ERROR .= $ValidateObj->validate_name ( $_REQUEST ['first_name'], "First" );
	$ERROR .= $ValidateObj->validate_name ( $_REQUEST ['last_name'], "Last" );
	$ERROR .= $ValidateObj->validate_verification ( $_REQUEST ['pass'], $_REQUEST ['pass2'], 'Y' );
	
	if (isset ( $_POST ["captcha"] )) {
		if ($_SESSION['U']["captcha"] != $_POST ["captcha"]) {
			$ERROR .= "The 6 symbols entered could not be verified." . "\\n";
		}
	} // end captcha session
	
	if (! $ERROR) {
		// Get date format based on the given format
		$SESS = $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );
		$SESSIONID = uniqid ( $SESS );
		
		if ($_COOKIE ['PID']) {
			$SESSIONID = $_COOKIE ['PID'];
		}
		
		// set session key on users browser
		setcookie ( "PID", $SESSIONID, time () + 7200, "/" );
		
		// lock for a duplicate submission 
		$info = array (
		        "FirstName"         =>  $_REQUEST['first_name'],
		        "LastName"          =>  $_REQUEST['last_name'],
		        "Email"             =>  $_REQUEST ['email'],
				"Verification"      =>  password_hash( $_REQUEST ['pass'], PASSWORD_DEFAULT ),
				"Minutes"           =>  '120',
				"ActivationDate"    =>  "NOW()",
				"LastAccess"        =>  "NOW()",
				"SessionID"         =>  $SESSIONID,
				"OrgID"             =>  $OrgID,
				"MultiOrgID"        =>  (string)$MultiOrgID
		);
		//Insert Users Information
		$result_users_ins = $UserPortalUsersObj->insUsers ( $info );
		
		//Get User Detail Information
		$USER = $UserPortalUsersObj->getUserDetailInfoByEmail ( "UserID", $_REQUEST['email'] );

		//Send verification mail to registered user
		$subject = "Email Verification";
		
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		//Get Email and EmailVerified
		$OE = $OrganizationDetailsObj->getEmailAndEmailVerified($OrgID, (string)$MultiOrgID);
		//Get OrganizationName
		$OrgName = $OrganizationDetailsObj->getOrganizationName($OrgID, (string)$MultiOrgID);

        //Clear properties
        $PHPMailerObj->clearCustomProperties();
        $PHPMailerObj->clearCustomHeaders();
			
		if (($OE['Email']) && ($OE['EmailVerified'] == 1)) {
		    // Set who the message is to be sent from
		    $PHPMailerObj->setFrom ( $OE['Email'], $OrgName );
		    // Set an alternative reply-to address
		    $PHPMailerObj->addReplyTo ( $OE['Email'], $OrgName );
		} else {
			$support	=	G::Obj('IrecruitSettings')->getValue("Support");
    		// Set who the message is to be sent from
    		$PHPMailerObj->setFrom ( $support['Email'], $support['Name'] );
    		// Set an alternative reply-to address
    		$PHPMailerObj->addReplyTo ( $support['Email'], $support['Name'] );
 		}
		
		$message = "";
		$message .= "Please click on the following link to verify your iRecruit account. Verifying your account will allow you to access your applications, save your profile and apply to additional positions." . "<br><br>";
		$link_verify = preg_replace ( '/signup.php/', 'verifyEmail.php', $_SERVER ['PHP_SELF'] ) . "?OrgID=" . $OrgID . "&MultiOrgID=" . $MultiOrgID . "&acc=" . $SESSIONID;
		$message .= "\n\nYes, please <a href=\"https://" . $_SERVER ['SERVER_NAME'] . $link_verify  . "\">Verify My Account</a>.<br><br>";
		$message .= "Thank you," . "<br>";
		$message .= "iRecruit Support" . "<br>";
		$message .= "<a href=\"http://help.myirecruit.com\">http://help.myirecruit.com</a>" . "<br>";
		
		if (DEVELOPMENT != "Y") {
		    // Set who the message is to be sent to
		    $PHPMailerObj->addAddress ( $_REQUEST ['email'] );
		    // Set the subject line
		    $PHPMailerObj->Subject = $subject;
		    // convert HTML into a basic plain-text alternative body
		    $PHPMailerObj->msgHTML ( $message );
		    // Content Type Is HTML
		    $PHPMailerObj->ContentType = 'text/html';
		    //Send email
		    $PHPMailerObj->send ();
		}

		// forward to section recorded in LOC
		$forwardlink = G::Obj('Organizations')->linkToLocationHoldTask(G::Obj('Organizations')->getLocationHoldIdByCookie());
                if ($forwardlink == "") {
                        $forwardlink = USERPORTAL_HOME . "index.php";
                }
		echo header ( 'Location: ' . $forwardlink );
	}
} // end process

$title = "myiRecruit";
require_once 'Header.inc';

if ($ERROR) {
	echo '<script language="JavaScript" type="text/javascript">' . "\n";
	echo "errorMessage('$ERROR');" . "\n";
	echo '</script>' . "\n";
}

// be sure the OrgID entered is a valid iRecruit ID
if (isset ( $OrgID )) {
	//Get Organization Information
	$results = G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "OrgID");
	$OrgOK   = $results['OrgID'];
}

if ($OrgOK) {
	//Get InternationalTranslation Information
	$INT = $FormFeaturesObj->getInternationalTranslation($OrgID);
	?>
	<style>
	.tooltip {
    	background: none !important;
    	margin-left: 10px !important;
    }
	#sign-up-wrapper {
    		margin-left:2%;
    }
	@media ( min-width :768px) {
    	#sign-up-wrapper {
    		margin-left:20%;
    	}
    }
	</style>
    <?php 
    if ($INT ['InternationalTranslation'] == "Y") {
        ?>
        <script>
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({
            pageLanguage: 'en',
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE
          }, 'google_translate_element');
        }
        </script>
        <script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        <div class="row" style="margin:0 auto !important;padding: 10px 0;align-items: center;justify-content: space-around;display: flex;float: none;">
            <div class="col-lg-12 table-responsive">
                <div id="google_translate_element"></div>
            </div>
        </div>                    
        <?php 
    }
    ?>
    
<div class="container">
<div class="row">
    <div class="col-lg-12">
		<?php 
		//Set where condition
		$where    =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
		//Set parameters
		$params   =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID);
		
		$results  =   G::Obj('Organizations')->getOrganizationLogosInformation("OrgID", $where, '', array($params));
		
		$ImagePresent = $results['count'];
		
		if ($ImagePresent > 0) {
		    echo '<img src="' . USERPORTAL_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br><br>';
		}
		?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 table-responsive">
          <form name="frmSignUp" id="frmSignUp" method="post">
                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <?php if($requisition_title != "") echo "<strong>Applying to " . $requisition_title . "</strong>";?>
                        </div>                          
                    </div>
                    <?php
                    // Same Day Delivery - Amazaon Delivery Service Partner
                    if ($_REQUEST['MultiOrgID'] == "5a81bfee04db9") {
                      echo "<h4>Application Page One</h4>";
                    } else { // end Same Day Delivery
                      echo "<h4>Create a User Account</h4>";
                    }
                    ?>
                    <div class="form-group row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="first_name" class="question_name">
                            First Name: <span style="color:#FF0000">*</span>
                        </label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                        <input name="first_name" id="first_name" size="30" maxlength="55" type="text" value="<?php echo htmlspecialchars($_REQUEST['first_name']);?>" style="border: 1px solid #C0C0C0; padding: 4px">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="last_name" class="question_name">
                            Last Name: <span style="color:#FF0000">*</span>
                        </label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                        <input name="last_name" id="last_name" size="30" maxlength="55" type="text" value="<?php echo htmlspecialchars($_REQUEST['last_name']);?>" style="border: 1px solid #C0C0C0; padding: 4px">        
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="email" class="question_name">
                            Enter Email Address: <span style="color:#FF0000">*</span>
                        </label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <input name="email" size="30" maxlength="55" type="text" value="<?php echo htmlspecialchars($_REQUEST['email']);?>" style="border: 1px solid #C0C0C0; padding: 4px">
                        </div>
                    </div>
                                            
                    <div class="form-group row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="pass" class="question_name">
                            <a data-toggle="tooltip" 
								data-placement="right" 
								data-html="true" 
								title="<strong>Tip:</strong> Passwords at least 8 characters and must include one uppercase letter and one number. (Example: MSmith19!)"
								style="text-decoration:none;color:#333;">
							     Choose a Password: <span style="color:#FF0000">*</span>
							</a>		
                        </label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <input name="pass" size="30" maxlength="45" type="password" style="border: 1px solid #C0C0C0; padding: 4px">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="pass2" class="question_name">
                            <a data-toggle="tooltip" 
								data-placement="right" 
								data-html="true" 
								title="<strong>Tip:</strong> Passwords at least 8 characters and must include one uppercase letter and one number. (Example: MSmith19!)"
								style="text-decoration:none;color:#333;">
                                Enter Password Again: <span style="color:#FF0000">*</span>
                            </a>		
                        </label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <input name="pass2" size="30" maxlength="45" type="password" style="border: 1px solid #C0C0C0; padding: 4px">
                        </div>                            
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-lg-3 col-md-3 col-sm-3">
                        <label for="captcha" class="question_name">
                            Please Enter the 6 symbols <br> as they appear:
                            <img src="captcha/captcha.php" alt="captcha image" border="0" title="security code" style="margin: 0px 3px -4px 0px;">
                        </label>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                            <input type="text" name="captcha" size="6" maxlength="6" style="border: 1px solid #C0C0C0; padding: 4px">
                        </div>
                    </div>    

                    <div class="form-group row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                        <label for="captcha">
					       <input type="submit" value="Continue" class="btn btn-primary">
                        </label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>
					       Already have an account? Please  
                    	   <a href="login.php?OrgID=<?php echo $OrgID;?>&MultiOrgID=<?php echo $MultiOrgID;?>">
                    		     <span style="color:#0066CC">
                    		         <strong>click here to sign in.</strong>
                    		     </span>
                    	   </a>
                        </label>
                    </div>
                    
					<input type="hidden" name="OrgID" value="<?php echo $OrgID?>"> 
					<input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID;?>"> 
					<input type="hidden" name="process" value="Join Now"> 
			</form>
    </div>
</div>
</div>
<?php
} else {
	echo '<br><br><p align="center">You have reached this page in error.</p>';
}

require_once USERPORTAL_DIR . 'Footer.inc';
?>
