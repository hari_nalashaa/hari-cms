<?php
if(isset($_REQUEST['OrgID']) && $_REQUEST['OrgID'] != "") $OrgID = $_REQUEST['OrgID'];
if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") $MultiOrgID = $_REQUEST['MultiOrgID'];

require_once 'userportal.inc';
require_once 'Authenticate.inc';

if ($UpUserID) {
	// remove the session key from the database
	$UserPortalUsersObj->updUsersSessionID('', $UpUserID);
	$_SESSION['U']['UserIDMasterValue'] = '';
	unset($_SESSION['U']['UserIDMasterValue']);

	// remove the session key from the users browser
	if (isset ( $_COOKIE ['PID'] )) {
		setcookie ( "PID", "", time () - 3600, "/" );
	}
}

$page_title =  "Logout";
require_once 'Header.inc';
?>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
  }, 'google_translate_element');
}
</script>
<script
	src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<br>

<div class="table-responsive">
<table border="0" width="70%" cellspacing=5 " cellpadding="5"
	align="center" style="margin:0 auto">
	<tr>
		<td colspan="2" align="left">
			<?php 
			//Set where condition
			$where   = array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "PrimaryLogoType != ''");
			//Set parameters
			$params  = array(":OrgID"=>$OrgID, ":MultiOrgID"=>(string)$MultiOrgID);
			$results = $OrganizationsObj->getOrganizationLogosInformation("OrgID", $where, '', array($params));
			
			$ImagePresent = $results['count'];
			
			if ($ImagePresent > 0) {
			    echo '<img src="' . USERPORTAL_HOME . 'display_image.php?OrgID=' . $OrgID . '&MultiOrgID=' . $MultiOrgID . '&Type=Primary"><br><br>';
			}
			?>
		</td>
	</tr>
	<tr>
		<td valign="top">
            <b>
                <font face="Verdana" size="4" color="#000066">
                      You have successfully logged out.
                </font>
            </b>
            <p>
				<font face="Verdana" color="#2C2C2C">Pages viewed during this session may still be seen in the browser's cache.</font>
			</p>
			<p>
				<font face="Verdana" color="#2C2C2C">Please close your browser completely to ensure data privacy if using a public computer.</font>
			</p>
			<p>
			<font face="Verdana" color="#2C2C2C">
			<?php
			$forward = "?OrgID=" . $OrgID;
			if ($MultiOrgID) {
				$forward .= "&MultiOrgID=" . $MultiOrgID;
			} 
			?>
			<p>
				<b><a href="login.php<?php echo $forward?>"><font color="#3366FF">Click here</font></a></b> to log back in.
			</p>
			</font>
		</td>
	</tr>
</table>
</div>
<?php
require_once USERPORTAL_DIR . 'Footer.inc';
?>
