<?php
if ($ApplicationID != "") {
	$processed_sections     =   G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, "ProcessedSections");
} else {
	$processed_sections     =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");
}

if($processed_sections  == NULL 
    || $processed_sections == "" 
    || empty($processed_sections) 
    || !isset($processed_sections)) {
    $processed_sections =   array();
}
else {
    $processed_sections =   unserialize($processed_sections);
}

$up_sections_list   =   array();
foreach ($userportal_sections_list as $section_id=>$section_info) {
    $up_sections_list[] = $section_id;
}

$errors_list        =   array();
$section_id_names   =   array();

//Prefill the $FILES
$FILES                      =   $_FILES;


if ($ApplicationID != "") {
    // Set attachment where condition
    $attach_where   =   array (
            "OrgID            =   :OrgID",
            "ApplicationID    =   :ApplicationID",
    );
    // Set attachment parameters
    $attach_params  =   array (
            ":OrgID"          =>  $OrgID,
            ":ApplicationID"  =>  $ApplicationID,
    );
    $results =   $AttachmentsObj->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );

    foreach ($results['results'] AS $ATT) {
      $FILES[$ATT['TypeAttachment']]   =   array(
        "name"  =>  $ATT['TypeAttachment'],
        "type"  =>  $ATT['FileType']
      );
    }

} else { // ApplicationID

$attachment_types           =   array();
$attachments_info           =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
   for($aai = 0; $aai < count($attachments_info); $aai++) {

      $attachment_types[]     =   $attachments_info[$aai]['TypeAttachment'];

      $file_type              =   $attachments_info[$aai]['FileType'];
      $file_attachment_name   =   $attachments_info[$aai]['TypeAttachment'];
      $file_name              =   $file_attachment_name.".".$file_type;

      $FILES[$attachments_info[$aai]['TypeAttachment']]   =   array(
        "name"  =>  $attachments_info[$aai]['TypeAttachment'],
        "type"  =>  $file_type
      );
   }

} // end else ApplicationID


//Validate the information
G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']   =   $APPDATA;
G::Obj('ValidateApplicationForm')->FORMDATA['FILES']     =   $FILES;

$errors_info        =   G::Obj('ValidateApplicationForm')->validateApplicationForm($OrgID, $FormID,'',$HoldID);

$errors_list        =   $errors_info['ERRORS'];
$errors_que_info    =   $errors_info['QuestionInfo'];

if(is_array($errors_list)) {
    foreach ($errors_list as $error_que_id=>$error_que_text) {
        if(!in_array($errors_que_info[$error_que_id]['SectionID'], $section_id_names)) {
            $section_id_names[] =   $errors_que_info[$error_que_id]['SectionID'];
        }
    }
}

$msections_list         =   array_merge($up_sections_list, $section_id_names);
$msections_list         =   array_unique($msections_list);
$msections_unique_list  =   array();

foreach($msections_list as $muslkey=>$muslvalue) {
    if(!in_array($muslvalue, $processed_sections)) {
        $msections_unique_list[] = $muslvalue;
    }
}

return array("errors_list"=>$errors_list, "section_id_names"=>$msections_unique_list);
?>
