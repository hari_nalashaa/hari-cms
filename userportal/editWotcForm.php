<?php
require_once 'userportal.inc';

$page_title = "Wotc Form";

require_once USERPORTAL_DIR . 'Authenticate.inc';

if(isset($_REQUEST['IrecruitApplicationID']) && $_REQUEST['IrecruitApplicationID'] != "") $IrecruitApplicationID = $_REQUEST['IrecruitApplicationID'];
if(isset($_REQUEST['wotcID']) && $_REQUEST['wotcID'] != "") $WOTCID = $_REQUEST['wotcID'];
if(isset($_REQUEST['IrecruitRequestID']) && $_REQUEST['IrecruitRequestID'] != "") $IrecruitRequestID = $_REQUEST['IrecruitRequestID'];
if(isset($_REQUEST['IrecruitProcessOrder']) && $_REQUEST['IrecruitProcessOrder'] != "") $IrecruitProcessOrder = $_REQUEST['IrecruitProcessOrder'];

//Get Wotc Ids list
$wotcid_info    =       G::Obj('WOTCOrganization')->getWotcIdInfoByOrgIDMultiOrgID("wotcID", $OrgID, $MultiOrgID);
$WOTCID                 =       $wotcid_info['wotcID'];

$PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
$WotcFormID     =   $PARENT['WotcFormID'];
$OrganizationName       =       $PARENT['CompanyName'];

//Get irecruit application information
if ($IrecruitApplicationID != "") {

	$IRECRUITDATA				=   G::Obj('Applicants')->getAppData($OrgID, $IrecruitApplicationID);
	
	$APPDATA['FirstName']       =   $IRECRUITDATA['first'];
	$APPDATA['LastName']        =   $IRECRUITDATA['last'];
	$APPDATA['MiddleName']      =   $IRECRUITDATA['middle'];
	$APPDATA['Social1']         =   $IRECRUITDATA['social1'];
	$APPDATA['Social2']         =   $IRECRUITDATA['social2'];
	$APPDATA['Social3']         =   $IRECRUITDATA['social3'];
	$APPDATA['Address']         =   $IRECRUITDATA['address'];
	$APPDATA['City']            =   $IRECRUITDATA['city'];
	$APPDATA['State']           =   $IRECRUITDATA['state'];
	$APPDATA['ZipCode']         =   $IRECRUITDATA['zip'];
	$APPDATA['County']          =   $IRECRUITDATA['county'];

	$IRECRUITLOOKUP         	=   "Y";
}

require_once USERPORTAL_DIR . 'formsInternal/ProcessWotcForm.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
	require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
	require_once USERPORTAL_DIR . 'Header.inc';
	require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo 'Work Opportunity Tax Credit Form';

$back_to_assigned_forms =   'assignedInternalForms.php?ApplicationID='.$IrecruitApplicationID.'&RequestID='.$IrecruitRequestID.'&ProcessOrder='.$IrecruitProcessOrder.'&navpg=status&navsubpg=view';

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.$back_to_assigned_forms.'">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Assigned Forms';
echo '</a>';
echo '</span>';

echo '</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';

echo '<div class="row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12" style="margin:0px;padding:0px;color:blue">';
if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'suc') {
	echo 'Successfully processed the form.';
}
if($rtn_duplicate_info != "") {
	echo $rtn_duplicate_info;
	exit;
}	
echo '<br><br>';
echo '</div>';
echo '</div>';

//Wotc Organization Information
$OrgLetter =   G::Obj('WOTCDisplayText')->getOrganizationLetter('MASTER');

//Row Start
echo '<div class="row">';
echo "<div class=\"row\">\n";
echo "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
echo str_replace("{OrganizationName}", $OrganizationName, $OrgLetter);
echo "<br><br>";
echo "</div>\n";
echo "</div>\n";
echo "</div>\n";

//Row Start
echo '<div class="row">';
?>
<form id="editWotcForm" name="editWotcForm" method="post">
	<input type="hidden" name="OrgID" value="<?php echo htmlspecialchars($OrgID); ?>">
	<input type="hidden" name="MultiOrgID" value="<?php echo htmlspecialchars($MultiOrgID); ?>"> 
	<input type="hidden" name="IrecruitApplicationID" value="<?php echo htmlspecialchars($IrecruitApplicationID); ?>">
	<input type="hidden" name="IrecruitRequestID" value="<?php echo htmlspecialchars($IrecruitRequestID); ?>">
	<input type="hidden" name="IrecruitProcessOrder" value="<?php echo htmlspecialchars($_REQUEST['IrecruitProcessOrder']); ?>">
	<input type="hidden" name="wotcID" value="<?php echo htmlspecialchars($_REQUEST['wotcID']); ?>">
	<input type="hidden" name="ApplicationStatus" value="A">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
        	<?php
			$WOTCID     =   G::Obj('WOTCApp')->isWotcIDExists();	//Alternate for validateWotcID

			if($WOTCID != "") {
					
				require_once WOTC_DIR . 'forms/Form.inc';
				
				$DATE_IDS               =   "";
				$child_que_list         =   array();
				$que_types_list         =   array();
				$special_ques           =   array('verify', 'captcha', 'declaration');
					
				for($wfq = 0; $wfq < $wotc_form_ques_list['count']; $wfq++) {
						
					if($ques_info['QuestionTypeID'] == 17) {
						$DATE_IDS       .=  "#".$ques_info['QuestionID'].", ";
					}
					 
					$QuestionID         =   $wotc_form_ques[$wfq]['QuestionID'];
					$QuestionTypeID     =   $wotc_form_ques[$wfq]['QuestionTypeID'];
					 
					$ques_info          =   $wotc_form_ques[$wfq];
					$ques_info['name']  =   $wotc_form_ques[$wfq]['QuestionID'];
					$que_types_list[$wotc_form_ques[$wfq]['QuestionID']]   =   $wotc_form_ques[$wfq]['QuestionTypeID'];
					 
					if($wotc_form_ques[$wfq]['ChildQuestionsInfo'] != "") {
						$child_que_list[$wotc_form_ques[$wfq]['QuestionID']]   =   json_decode($wotc_form_ques[$wfq]['ChildQuestionsInfo'], true);
					}
					 
					//Set errors related css
					$requiredck =   '';
					$highlight  =   'yellow';
					$ERROR_KEYS	=	array();
					if(is_array($ERRORS)) {
						$ERROR_KEYS =   array_keys($ERRORS);
					}
					 
					if(in_array($QuestionID, $ERROR_KEYS))
						$requiredck = ' style="background-color:' . $highlight . ';"';
					 
					$lbl_class = ' class="question_name"';
					if($QuestionTypeID == 99 || $QuestionTypeID == 5) {
						$lbl_class = " class='question_name labels_auto'";
					}
					 
					$ques_info ['lbl_class']  =   $lbl_class;
					$ques_info ['requiredck'] =   $requiredck;
					 
					//Print the Question Preview
					$que_view = call_user_func(array(G::Obj('ApplicationForm'), 'getQuestionView' . $ques_info['QuestionTypeID']), $ques_info);
						
					if(in_array($QuestionID, $special_ques)) {
						if ($_POST['Initiated'] == "callcenter") {
							if($QuestionID == "verify") {
								echo $que_view;
							}
						}
						else {
							if($QuestionID != "verify") {
								echo str_replace("{Organization}", $OrganizationName, $que_view);
							}
						}
					}
					else {
						echo $que_view;
					}
					 
				}
					
				$DATE_IDS   =   trim($DATE_IDS, ", ");
				?>
			    <div class="row" style="margin-bottom:10px;">
			        <div class="col-lg-3 col-md-3 col-sm-3">
			            <input type="hidden" name="Initiated" value="<?php echo htmlspecialchars($_POST['Initiated']); ?>"> 
			            <input type="hidden" name="Nonprofit" value="<?php echo htmlspecialchars($Nonprofit); ?>"> 
			            <input type="hidden" name="irecruitlookup" value="<?php echo htmlspecialchars($IRECRUITLOOKUP); ?>">
						<div id="QDONE">
			                <input type="submit" name="btnSubmit" id="btnSubmit" class="btn btn-primary" value="Submit Form">
						</div>
					</div>
				</div>
				<?php

			}
			else {
				echo "<div style=\"line-height:200%;padding:10px;\">Dear Employee,<br>Please tell your Hiring Manager, HR representative or Employer to <span style=\"color:red;\">Contact CMS</span> directly to resolve this issue regarding WOTC screening.</div>\n";
			}
        	?>
        </div>
    </div>
</form>

<?php
$company 		=	"Cost Management Services, LLC";

$PARTNER = G::Obj('WOTCcrm')->getPartnerInfo($WOTCID);
$PartnerName    =       $PARTNER['CompanyName'];

if ($PartnerName) {
	$company = $PartnerName . " & CMS";
	if ($PartnerName == "Paycor") {
		$company = $PartnerName;
	}
	if ($PartnerName == "iNetHR") {
		// $company = "CMS";
	}
	if ($PartnerName == "NEEPAA") {
		$company = $PartnerName;
	}
}

echo "<div class=\"row\">\n";
echo "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
echo "<br>";
echo "<b>" . $company . "</b> has been retained and is responsible for administering this program. All information disclosed by you will be handled by this service. The information is confidential and will be used only by $company and your employer in strict confidence with the Department of Labor to determine eligibility for available job initiation programs. It will not be included or impact any decision involving your employment.<br><br>\n";

echo "Your time and cooperation with this effort is greatly appreciated.<br><br>\n";

echo "Sincerely,<br>\n";
echo "<b>" . $company . "</b><br><br>\n";

echo "</div>\n";
echo "</div>\n";

$RESTRICT = G::Obj('WOTCcrm')->getWOTCIDs('62f41708245691'); // Dycom Industries

if (! in_array($WOTCID, $RESTRICT)) {
	$rtn .= "<div class=\"row\">\n";
	$rtn .= "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
	$rtn .= "<a href=\"http://www.eeoc.gov/\" target=\"_blank\" onClick=\"window.open('http://www.eeoc.gov/','mywindow','width=1000'); return false;\"><b>Equal Opportunity Employer</b></a><br>\n";

	// $PartnerName; May 23, 2017
	$rtn .= $OrganizationName . " is an Equal Opportunity Employer. It is the policy of the Company to provide equal opportunity for all employees and applicants for employment without regard to race, color, creed, religion, gender, sexual orientation, national origin, age, marital status, mental or physical disability, pregnancy, military or veteran status, or any other basis prohibited by state or federal law. This policy also prohibits employees from harassing any other employees for any reason including, but not limited to, race, religion, sex, national origin, age, or disabled status.\n";
	$rtn .= "<br><br>";
	$rtn .= "</div>\n";
	$rtn .= "</div>\n";
} // end RESTRICT
?>

<script type="text/javascript">
var que_types_list  =   JSON.parse('<?php echo json_encode($que_types_list);?>');
var child_ques_info =   JSON.parse('<?php echo json_encode($child_que_list);?>');
</script>
<script type="text/javascript" src="<?php echo WOTC_HOME;?>js/child-questions.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	getFormIDChildQuestionsInfo();

	$( ".row" ).click(function() {
		$("div[id^='divQue-']").css( "background", "#ffffff" );
	  	var next_divs	=	$(this).nextAll( ".form-group" );

	  	for(i = 0; i < next_divs.length; i++) {

	  		if(document.getElementById(next_divs[i].id).style.display == 'block'
		  		|| document.getElementById(next_divs[i].id).style.display == '') {
	  			$("#"+next_divs[i].id).css( "background", "#f5f5f5" );

	  			i = next_divs.length;	//Break the loop if element found
			}
		}
	});
});
var state=document.getElementById("State").value;
getCounties(state,'','');
<?php 
if ($_REQUEST['County'] != "") { 
	$county = preg_replace('/\'/', '\\\'', $_REQUEST['County']);
?>
setCounty('<?php echo $county; ?>');
<?php
}
if($DATE_IDS != "") {
	?>
    var dates = $('<?php echo $DATE_IDS;?>').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    showButtonPanel: true,
                    showOn: 'button',
                    buttonImage: '<?php echo WOTCADMIN_HOME; ?>calendar/css/smoothness/images/calendar.gif',
                    yearRange: '-80:+5',
                    buttonImageOnly: true,
                    buttonText: 'Select Date'
                });
	<?php
}
?>
</script>
<?php
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
	require_once USERPORTAL_DIR . 'Footer.inc';
}
?>
