<?php
//Get Email, EmailVerified From Users table
$user_detail_email  =   $UserPortalUsersObj->getUserDetailInfoByUserID("Email, cast(EmailVerified as unsigned int) as EmailVerified", $UpUserID);
$email              =   $user_detail_email['Email'];
$EmailVerified      =   $user_detail_email['EmailVerified'];

if (($email) && ($EmailVerified == 1)) {
	
	//Get ApplicationID's related to QuestionID email //Set where condtion
	$where	=	array("OrgID = :OrgID", "QuestionID = 'email'", "Answer = :Answer");
	//Set parameters
	$params =	array(":OrgID"=>$OrgID, ":Answer"=>$email);
	//Applicants Data Information
	$rows	=	$ApplicantsObj->getApplicantDataInfo("ApplicationID", $where, '', '', array($params));
	
	if($rows['count'] > 0) {
		foreach ($rows['results'] as $rkey=>$row) {
			//Insert Information
			$insert_info	=	array("OrgID"=>$OrgID, "ApplicationID"=>$row ['ApplicationID'], "QuestionID"=>'PortalUserID', "Answer"=>$UpUserID);
			//On Update
			$on_update   	=	" ON DUPLICATE KEY UPDATE Answer = :UAnswer";
			//Update Information
			$update_info	=	array(":UAnswer"=>$UpUserID);
			//Insert ApplicantData
			$results		=	G::Obj('Applicants')->insUpdApplicantData($insert_info, $on_update, $update_info);
			
			//Set Wotc Application Information
			$set_info		=	array("UserID = :UserID");
			$where_info		=	array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "IrecruitApplicationID = :IrecruitApplicationID");
			$params_info	=	array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":IrecruitApplicationID"=>$row ['ApplicationID'], ":UserID"=>$UpUserID);
			G::Obj('WOTCIrecruitApplications')->updWotcApplicationInfo($set_info, $where_info, array($params_info));
		}
	}
	
} // end if email
?>