<?php
require_once '../userportal.inc';
$title = 'Application Vault';

if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['process']) && $_REQUEST['process'] != "") $process = $_REQUEST['process'];
if(isset($_REQUEST['PreFilledFormID']) && $_REQUEST['PreFilledFormID'] != "") $PreFilledFormID = $_REQUEST['PreFilledFormID'];

$UpdateID = isset($_REQUEST['UpdateID']) ? $_REQUEST['UpdateID'] : '';

if ($_GET ['edit']) {

    $typefile  =   "Replace File: <font style=\"color:red;\">" . $AV ['FileName'] . '.' . $AV ['FileType'] . "</fon>";
    $submit    =   "Replace File";
    $add       =   ' <a href="' . IRECRUIT_HOME . 'applicants/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">Add File</a>';
    $process   =   "Replace";
    $UpdateID  =   $AV ['UpdateID'];

} else {

    $typefile  =   "Upload File:";
    $submit    =   "Upload File";
    $add       =   "";
    $process   =   "New";
    $UpdateID  =   "";

}

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
}

if(isset($_POST) && count($_POST) > 0) {
    require_once USERPORTAL_DIR . 'ProcessApplicantVault.inc';
}

// if not a listings area for the new app process then navigate to these pages
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

echo '<div id="page-wrapper">';			//Page Wrapper Start
echo '<div class="page-container">';	//Page Container End

//Row Start
echo '<div class="row">';
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo 'Please upload the supporting documents';

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'index.php?navpg=profiles&navsubpg=status">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Check My Status';
echo '</a>';
echo '</span>';

echo '</h3>';
echo '</div>';
echo '</div>'; //Row End

echo '<div class="page-inner">';

include 'ApplicationInformation.inc';
echo '<br><br>';

//Row Start
echo '<div class="row">';
echo '<div class="col-lg-12">';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'ApplicantVault.inc';
}
	
echo '</div>';
echo '</div>';	//Row End

echo '</div>';	//Page Inner End
echo '</div>';	//Page Container End

echo '</div>';	//Page Wrapper

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>