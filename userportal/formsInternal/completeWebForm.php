<?php
require_once '../userportal.inc';

$title = 'Complete Internal Form';

if(isset($_REQUEST['WebFormID']) && $_REQUEST['WebFormID'] != "") $WebFormID = $_REQUEST['WebFormID'];
if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['ProcessOrder']) && $_REQUEST['ProcessOrder'] != "") $ProcessOrder = $_REQUEST['ProcessOrder'];
if(isset($_REQUEST['HoldID']) && $_REQUEST['HoldID'] != "") $HoldID = $_REQUEST['HoldID'];
if(isset($_REQUEST['process']) && $_REQUEST['process'] != "") $process = $_REQUEST['process'];
if(isset($_REQUEST['edit']) && $_REQUEST['edit'] != "") $edit = $_REQUEST['edit'];

if (in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
}

//Process the submitted web form
require_once USERPORTAL_DIR . 'formsInternal/ProcessWebForm.inc';

require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

// if not a listings area for the new app process then navigate to these pages
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

echo '<style>
#question_space {
    padding: 0px 0px 3px 0px;
}
@media(max-width:768px) {
    #question_space {
    padding: 0px 0px 3px 0px;
    border-bottom: 1px solid #f5f5f5 !important;
}
}
@media print {
    .col-sm-3 {
        width: 35%;
        padding: 0px;
    }
    .col-sm-9 {
        width: 65%;
    }
    .col-sm-12 {
        width: 70%;
        padding: 0px;
    }
    #question_space {
        padding: 0px;
    }
}
</style>';

echo '<div id="page-wrapper">';			//Page Wrapper Start
echo '<div class="page-container">';	//Page Container End

//Row Start
echo '<div class="row">';
echo '<div class="col-lg-12">';

echo '<h3 class="page-header">';

echo 'Web Form';

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'assignedInternalForms.php?ApplicationID='.htmlspecialchars($_REQUEST['ApplicationID']).'&RequestID='.htmlspecialchars($_REQUEST['RequestID']).'&ProcessOrder='.htmlspecialchars($_REQUEST['ProcessOrder']).'&navpg=status&navsubpg=view">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Assigned Forms';
echo '</a>';
echo '</span>';

echo '</h3>';

echo '</div>';
echo '</div>'; //Row End

echo '<div class="page-inner">';

include 'ApplicationInformation.inc';

//Row Start
echo '<div class="row">';

echo '<div class="col-lg-12">';

require_once COMMON_DIR . 'formsInternal/CompleteWebForm.inc';

echo '</div>';

echo '</div>';	//Row End

echo '</div>';	//Page Inner End
echo '</div>';	//Page Container End
echo '</div>';	//Page Wrapper

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>