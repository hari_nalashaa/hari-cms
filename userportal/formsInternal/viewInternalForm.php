<?php
$title = 'View Internal Form';
require_once '../userportal.inc';

if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];
if(isset($_REQUEST['PreFilledFormID']) && $_REQUEST['PreFilledFormID'] != "") $PreFilledFormID = $_REQUEST['PreFilledFormID'];
if(isset($_REQUEST['WebFormID']) && $_REQUEST['WebFormID'] != "") $WebFormID = $_REQUEST['WebFormID'];
if(isset($_REQUEST['AgreementFormID']) && $_REQUEST['AgreementFormID'] != "") $AgreementFormID = $_REQUEST['AgreementFormID'];
if(isset($_REQUEST['specformid']) && $_REQUEST['specformid'] != "") $specformid = $_REQUEST['specformid'];
if(isset($_REQUEST['ProcessOrder']) && $_REQUEST['ProcessOrder'] != "") $ProcessOrder = $_REQUEST['ProcessOrder'];

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
}

require_once COMMON_DIR . 'formsInternal/DisplayApplicantHeader.inc';

// if not a listings area for the new app process then navigate to these pages
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

echo '<div id="page-wrapper">';			//Page Wrapper Start
echo '<div class="page-container">';	//Page Container End

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';

if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'veteran') {
    echo 'Veteran Form: ';    
}
else if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'aa') {
    echo 'Affirmative Action Form: ';
}
else if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'disabled') {
    echo 'Disabled Form: ';
}

echo "ApplicationID: ".$_REQUEST['ApplicationID'];

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'assignedInternalForms.php?ApplicationID='.htmlspecialchars($_REQUEST['ApplicationID']).'&RequestID='.htmlspecialchars($_REQUEST['RequestID']).'&ProcessOrder='.htmlspecialchars($_REQUEST['ProcessOrder']).'&navpg=status&navsubpg=view">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Assigned Forms';
echo '</a>';
echo '</span>';

echo '</h3>';

echo '</div>';
echo '</div>';


echo '<div class="page-inner">';

include 'ApplicationInformation.inc';

//Row Start
echo '<div class="row">';

echo '<div class="col-lg-12">';

if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "specforms") {
	require_once COMMON_DIR . 'formsInternal/SpecificationFormView.inc';
} else if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "webform") {
	require_once COMMON_DIR . 'formsInternal/WebFormView.inc';
} else if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "prefilledform") {
	require_once COMMON_DIR . 'formsInternal/PreFilledFormView.inc';
} else if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "agreementform") {
	require_once COMMON_DIR . 'formsInternal/AgreementFormView.inc';
} else if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "veteran") {
    require_once USERPORTAL_DIR . 'formsInternal/DocumentView.inc';
} else if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "aa") {
    require_once USERPORTAL_DIR . 'formsInternal/DocumentView.inc';
} else if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "disabled") {
    require_once USERPORTAL_DIR . 'formsInternal/DocumentView.inc';
}

echo '</div>';

echo '</div>';	//Row End

echo '</div>';	//Page Inner End
echo '</div>';	//Page Container End
echo '</div>';	//Page Wrapper

echo "<div style='clear:both'><br><br></div>";

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>