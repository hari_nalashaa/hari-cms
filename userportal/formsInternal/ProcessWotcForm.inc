<?php
if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == "Submit Form") {

	$APPDATA    =   $_POST;
	G::Obj('WOTCValidateForm')->FORMDATA['REQUEST'] =   $_REQUEST;
	G::Obj('ApplicationForm')->APPDATA              =   $_POST;

	$ERRORS     =  G::Obj('WOTCValidateForm')->validateWotcForm("MASTER", $WotcFormID); //Replace it with WotcID

	if($_POST['captcha'] != $_SESSION['U']['captcha']) {
		$ERRORS['captcha']  =   "Captcha";
	}
	
	if(count($ERRORS) == 0) {
		// Check duplicate entry
		$rtn_duplicate_info	=	G::Obj('WOTCIrecruitApplications')->validateWotcApplicationDuplicate();

		if($rtn_duplicate_info == "") {
			
			require_once WOTC_DIR . 'forms/Process.inc';
			
			//Insert wotc application header information
			$info	=	array(
							"OrgID"					=>	$OrgID,
							"MultiOrgID"			=>	$MultiOrgID,
							"WotcFormID"			=>	$WotcFormID,
							"wotcID"				=>	$WOTCID,
							"UserID"				=>	$UpUserID,
							"IrecruitApplicationID"	=>	$IrecruitApplicationID,	
							"IrecruitRequestID"		=>	$IrecruitRequestID,
							"ApplicationID"			=>	$ApplicationID,
							"CreatedDateTime"		=>	"NOW()",
							"Source"				=>	"USERPORTAL"
						);
			G::Obj('WOTCIrecruitApplications')->insWotcApplicationInfo($info);
			
			
			//Assigned Forms List
			$assigned_forms_list    =   G::Obj('FormsInternal')->getAssignedFormsInfo($OrgID, $IrecruitRequestID, $IrecruitApplicationID, $IrecruitProcessOrder);
			
			$link                   =   "";
			$link_hit_status        =   false;
			
			for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
			    
			    $assigned_forms_info    =   $assigned_forms_list[$afl];
			    
			    foreach($assigned_forms_info as $FormType=>$IFA) {
			        
			        if($IFA["Status"] == "Pending"
			            && $FormType != "Attachment"
			            && $FormType != "External Link"
			            && $FormType != "HTML Page") {
			                
			            $form_status        =   $IFA["IFAInfo"]["Status"];
			            $form_status_text   =   $IFA["FormStatusText"];
			            $link               =   $IFA["Link"];
			            
			            $afl                =   count($assigned_forms_list);
			            
			            $link_hit_status    =   true;
			        }
			        
			        if($link_hit_status == true) {
			            break;
			        }
			    }
			}

		    header('Location: ' . USERPORTAL_HOME . 'wotcThankYou.php?wotcID='.$WOTCID.'&ApplicationID='.$ApplicationID.'&IrecruitApplicationID='.$IrecruitApplicationID.'&IrecruitRequestID='.$IrecruitRequestID.'&IrecruitProcessOrder='.$IrecruitProcessOrder.'&redirect_url='.urlencode($link).'&msg=suc');
		    exit();
			
		}

	}
}
?>
