<?php
//Condition to check RequestID and ApplicationID exists or not
$app_req_exist_status   =   (isset($_REQUEST['ApplicationID']) && ($_REQUEST['ApplicationID'] != "") 
                            && isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "");

if ($app_req_exist_status) {
	$APPDATA = G::Obj('Applicants')->getAppData ( $OrgID, $_REQUEST['ApplicationID'] );	
}

echo '<div class="row">';
echo '<div class="col-lg-12 col-md-12 col-sm-12">';

if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "aa") {
	
	// Get AA section Title
	$results       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $_REQUEST['FormID'], "AA");
	$AATitle       =   $results['SectionTitle'];
	
	if ($AATitle) {
		echo '<b class="title">' . $AATitle . '</b><br><br>';
	}

	if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
	    echo getFormData ($OrgID, $_REQUEST['FormID'], $_REQUEST['ApplicationID'], 'AA', $APPDATA );
	} else {
		include COMMON_DIR . 'application/AA.inc';
		echo AffirmativeActionSection ($OrgID, $_REQUEST['FormID'], $_REQUEST['ApplicationID'], $APPDATA, $colwidth);
	}
	
} // end aa

if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "veteran") {	
	
	// Get Veteran section Title
    $results       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $_REQUEST['FormID'], "VET");
	$VTitle        =   $results['SectionTitle'];
	
	if ($VTitle) {
		echo '<b class="title">' . $VTitle . '</b><br><br>';
	}
	
	if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
	    echo getFormData ($OrgID, $_REQUEST['FormID'], $_REQUEST['ApplicationID'], 'VET', $APPDATA );
	} else {	
		include COMMON_DIR . 'application/Veteran.inc';
		echo VeteranSection ($OrgID, $_REQUEST['FormID'], $_REQUEST['ApplicationID']);
	}
	
} // end veteran

if (isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == "disabled") {
	
	// Get Disabled section Title
    $results    =   G::Obj('ApplicationFormSections')->getApplicationFormSectionInfo("*", $OrgID, $_REQUEST['FormID'], "DIS");
    $DTitle     =   $results['SectionTitle'];
	
	if ($DTitle) {
		echo '<div style="float:left;">';
		echo '<b class="title">' . $DTitle . '</b>';
		echo '</div>';
	}
	
	if (($_REQUEST['ApplicationID'] != "") && ($_REQUEST['RequestID'] != "")) {
		echo getFormData ($OrgID, $_REQUEST['FormID'], $_REQUEST['ApplicationID'], 'DIS', $APPDATA );
	} else {
		include COMMON_DIR . 'application/Disabled.inc';
		echo DisabledSection ($OrgID, $_REQUEST['FormID'], $_REQUEST['ApplicationID']);
	}
	
} // end disabled

echo '</div>';
echo '</div>';

function getFormData($OrgID, $FormID, $ApplicationID, $SectionID, $APPDATA) {
    
    $rtn = "";
    
    //Set parameters
    $params = array(':OrgID'=>$OrgID, ':FormID'=>$FormID, ':SectionID'=>$SectionID);
    //Set where condition
    $where  = array("OrgID = :OrgID", "FormID = :FormID", "SectionID = :SectionID", "Active = 'Y'");
    //Get QuestionsInformation
    $results = G::Obj('FormQuestions')->getQuestionsInformation("FormQuestions", "*", $where, "QuestionOrder", array($params));
    
    foreach ($results['results'] as $FORM) {
        
        if ($FORM ['QuestionTypeID'] == "99") {
            
            $rtn .= '<div style="margin-bottom:5px;margin-left:3px;">';
            $rtn .= $FORM ['Question'];
            $rtn .= '</div>' . "\n";
        } else {
            
            if ($FORM ['QuestionTypeID'] == "10") {
                
                $name = G::Obj('ApplicantDetails')->getApplicantName($OrgID, $ApplicationID);
                
                $rtn .= '<div class="row">';
                
                $rtn .= '<div class="col-lg-3 col-md-3 col-sm-3">' . "\n";
                $rtn .= 'Name:';
                $rtn .= '</div>';
                
                $rtn .= '<div class="col-lg-9 col-md-9 col-sm-9">' . "\n";
                $rtn .= '<strong> ' . ($name ? $name : 'N/A') . '</strong>';
                $rtn .= '</div>';
                
                $rtn .= '</div>' . "\n\n";
                
            }
            
            if (($FORM ['QuestionTypeID'] != "10") && ($FORM ['value'] != "")) {
                $APPDATA [$FORM ['QuestionID']] = G::Obj('ApplicationFormQueAns')->getDisplayValue ( $APPDATA [$FORM ['QuestionID']], $FORM ['value'] );
            }
            
            $rtn .= '<div class="row">';
            
            $rtn .= '<div class="col-lg-3 col-md-3 col-sm-3">' . "\n";
            $rtn .= $FORM ['Question'];
            $rtn .= '</div>' . "\n";
            
            $rtn .= '<div class="col-lg-9 col-md-9 col-sm-9">' . "\n";
            $rtn .= '<strong>' . ($APPDATA [$FORM ['QuestionID']] ? $APPDATA [$FORM ['QuestionID']] : 'N/A') . '</strong>';
            $rtn .= '<strong>' . $ans . '</strong>';
            $rtn .= '</div>' . "\n";
            
            $rtn .= '</div>' . "\n";
            
        }
    } // end while
    
    return $rtn;
} // end function
?>