<?php
// needs UpdateID as input
// example ?UpdateID=3
require_once '../userportal.inc';

if (($_REQUEST['OrgID']) && ($_REQUEST['specformid'])) {
	
	//Set where condition
	$where = array("OrgID = :OrgID", "SpecificationFormID = :SpecificationFormID");
	//Set parameters
	$params = array(":OrgID"=>$_REQUEST['OrgID'], ":SpecificationFormID"=>$_REQUEST['specformid']);
	//Get Specifications Forms Information
	$results = $FormsInternalObj->getSpecificationForms("*", $where, "", array($params));
	
	if(is_array($results['results'])) {
		foreach($results['results'] as $IMG) {
		
			$displaytitle = $IMG ['DisplayTitle'];
			$attfile = $IMG ['Attachment'];
			$atttype = $IMG ['AttachmentType'];
			$attext = $IMG ['AttachmentExt'];
		}
	}
	
	$filename = $displaytitle . "." . $attext;
	
	header ( 'Content-Description: File Transfer' );
	header ( "Content-type: $atttype" );
	header ( 'Content-Disposition: attachment; filename=' . $filename );
	header ( 'Content-Transfer-Encoding: binary' );
	header ( 'Expires: 0' );
	header ( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
	header ( 'Pragma: public' );
	echo $attfile;
	exit ();
} // end if OrgID, UpdateID
?>