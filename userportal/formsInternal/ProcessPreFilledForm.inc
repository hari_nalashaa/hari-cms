<?php
require_once COMMON_DIR . 'formsInternal/DisplayPreFilledForm.inc';
require_once COMMON_DIR . 'formsInternal/Signature.inc';

$complete_status                    =   "";
$GetFormPostAnswerObj->POST         =   $_POST;    //Set Post Data
G::Obj('ApplicationForm')->APPDATA  =   $_POST;    //Set APPDATA

$APPDATA    =   array ();
list ( $APPDATA, $APPDATAREQ, $HoldID ) = fillPreFilledFormData ( $USERID, $HoldID, $ApplicationID, $PreFilledFormID );

if (($ApplicationID == "") && ($APPDATA ['ApplicationID'] != "")) {
    $ApplicationID  =   $APPDATA ['ApplicationID'];
    $RequestID      =   $APPDATA ['RequestID'];
}

if ($_REQUEST['process'] == "Y") {
	
	//Delete ApplicantDataTemp Information
	G::Obj('ApplicantDataTemp')->delApplicantDataTemp($OrgID, $HoldID);

	include COMMON_DIR . 'formsInternal/ProcessPreFilledFormDataTemp.inc';
	
	include COMMON_DIR . 'formsInternal/PreFilledFormValidateRequired.inc';
	
	if($ERROR) {
		$complete_status = "failure";
		$MESSAGE = "The following entries are missing information.\\n";
		$MESSAGE .= $ERROR;
		$MESSAGE = str_replace(array("\n", "\\n"), "<br>", $MESSAGE);
	}
	else {
	    
		// if there are no errors there is no need to hold temp files
		G::Obj('ApplicantDataTemp')->delApplicantDataTemp($OrgID, $HoldID);
		$HoldID = "";
				
		/**
		 * This is a temporary code until the final upgrade.
		 * Upto that time, we have to keep on adding different conditions to it.
		 * After final upgrade we will have to remove the loop through $_POST
		 */
        $form_que_list         =   $PreFilledFormQuestionsObj->getPreFilledFormQuestionsList('MASTER', $PreFilledFormID);
        
        $define                =    "";
		$ques_to_skip          =   array();
		foreach($form_que_list as $QuestionID=>$QuestionInfo) {
		
            $QI                 =   $QuestionInfo;
            G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;
            $values             =   G::Obj('GetFormPostAnswer')->getDisplayValue($QI['value']);
            
            $QI['OrgID']        =   $OrgID;
		    
		    if($QI['QuestionTypeID'] == 13) {
		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		         
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;

		        
		        //Set the parameters
		        $params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QI['QuestionID']);
		        //Set condition
		        $where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
		        //Set the columns
		        $columns  =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
		        	
		        $results  =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
		        $WFDck    =   $results['results'][0];
		        	
		        if ($WFDck ['Answer'] != $QI['Answer']) {
		            
		            $phone_ans_bef    =   $WFDck ['Answer'];
		            $app_ans_bef      =   ($phone_ans_bef != "") ? json_decode($phone_ans_bef, true) : "";
		            $app_ans_bef_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], '' );
		             
		            $phone_ans_aft    =   $QI['Answer'];
		            $app_ans_aft      =   ($phone_ans_aft != "") ? json_decode($phone_ans_aft, true) : "";
		            $app_ans_aft_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], '' );
		            
		            
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }
		        
		        //Have to work on web form insert call
		        $PreFilledFormDataObj->insUpdPreFilledFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 14) {

		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		        $ques_to_skip[]        =   $QI['QuestionID']."ext";
		         
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //Set the parameters
		        $params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QI['QuestionID']);
		        //Set condition
		        $where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
		        //Set the columns
		        $columns  =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
		         
		        $results  =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
		        $WFDck    =   $results['results'][0];
		         
		        if ($WFDck ['Answer'] != $QI['Answer']) {
		    
		            $phone_ans_bef    =   $WFDck ['Answer'];
		            $app_ans_bef      =   ($phone_ans_bef != "") ? json_decode($phone_ans_bef, true) : "";
		            $app_ans_bef_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_bef[0], $app_ans_bef[1], $app_ans_bef[2], $app_ans_bef[3] );
		             
		            $phone_ans_aft    =   $QI['Answer'];
		            $app_ans_aft      =   ($phone_ans_aft != "") ? json_decode($phone_ans_aft, true) : "";
		            $app_ans_aft_ans  =   $AddressObj->formatPhone ( $OrgID, '', $app_ans_aft[0], $app_ans_aft[1], $app_ans_aft[2], $app_ans_aft[3] );
		        
		        
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }
		    
		        //Have to work on web form insert call
		        $PreFilledFormDataObj->insUpdPreFilledFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 15) {
		        
		        //After the complete upgrade have to remove this.
		        $ques_to_skip[]        =   $QI['QuestionID']."1";
		        $ques_to_skip[]        =   $QI['QuestionID']."2";
		        $ques_to_skip[]        =   $QI['QuestionID']."3";
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		        
		        
		        //Set the parameters
		        $params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QI['QuestionID']);
		        //Set condition
		        $where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
		        //Set the columns
		        $columns  =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
		         
		        $results  =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
		        $WFDck    =   $results['results'][0];

		        if ($WFDck ['Answer'] != $QI['Answer']) {
		        
		            $ssn_ans_bef      =   $WFDck ['Answer'];
		            $app_ans_bef      =   ($ssn_ans_bef != "") ? json_decode($ssn_ans_bef, true) : "";
		            $app_ans_bef_ans  =   $app_ans_bef[0].'-'.$app_ans_bef[1].'-'.$app_ans_bef[2];
		             
		            $ssn_ans_aft      =   $QI['Answer'];
		            $app_ans_aft      =   ($ssn_ans_aft != "") ? json_decode($ssn_ans_aft, true) : "";
		            $app_ans_aft_ans  =   $app_ans_aft[0].'-'.$app_ans_aft[1].'-'.$app_ans_aft[2];

		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }		            
		        
		        //Have to work on web form insert call
		        $PreFilledFormDataObj->insUpdPreFilledFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 9) {
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        //After the complete upgrade have to remove this.
		        $qis  =   0;
		        foreach ( $values as $v => $q ) {
		            $qis++;
		             
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis;
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-yr";
		            $ques_to_skip[]    =   $QI['QuestionID'] . "-" . $qis . "-comments";
		        }

		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		        		    
		        //Have to work on web form insert call
		        $PreFilledFormDataObj->insUpdPreFilledFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 18) {
		    
		        //After the complete upgrade have to remove this.
		        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
		    
		        for($c18 = 1; $c18 <= $cnt; $c18++) {
		            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c18;
		        }
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		    
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;
		    
		        $que_vals              =   explode ( '::', $QI['value'] );
		        $que_vals_cnt          =   count($que_vals);
		         
		        $que_values_info       =   array();
		        for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
		            $que_vals_info     =   explode(":", $que_vals[$qvc]);
		             
		            $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
		        }
		    
		        //Set the parameters
		        $params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QI['QuestionID']);
		        //Set condition
		        $where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
		        //Set the columns
		        $columns  =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
		         
		        $results  =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
		        $WFDck    =   $results['results'][0];
		         
		        if ($WFDck ['Answer'] != $QI['Answer']) {
		    
		            $app_ans_bef_ans    =   $WFDck ['Answer'];
		            $app_ans_aft_ans    =   $QI['Answer'];
		    
		            $ans_bef_ans_jd     =   json_decode($app_ans_bef_ans, true);
		            $ans_aft_ans_jd     =   json_decode($app_ans_aft_ans, true);
		    
		            foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
		                $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
		            }
		             
		            foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
		                $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
		            }
		    
		            $app_ans_bef_ans    =   @implode(", ", array_values($ans_bef_ans_jd));
		            $app_ans_aft_ans    =   @implode(", ", array_values($ans_aft_ans_jd));
		    
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }
		    
		        //Have to work on web form insert call
		        $PreFilledFormDataObj->insUpdPreFilledFormData($QI);
		    }
		    else if($QI['QuestionTypeID'] == 1818) {
		        //After the complete upgrade have to remove this.
		        $cnt                   =   $GetFormPostAnswerObj->POST[$QI['QuestionID'].'cnt'];
		    
		        for($c1818 = 1; $c1818 <= $cnt; $c1818++) {
		            $ques_to_skip[]    =   $QI['QuestionID'].'-'.$c1818;
		        }
		        $ques_to_skip[]        =   $QI['QuestionID'];
		        $ques_to_skip[]        =   $QI['QuestionID']."cnt";
		        	
		        $QI['Answer']          =   call_user_func( array( $GetFormPostAnswerObj, 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
		        $QI['ApplicationID']   =   $ApplicationID;
		        $QI['RequestID']       =   $RequestID;
		        $QI['AnswerStatus']    =   1;

		        $que_vals              =   explode ( '::', $QI['value'] );
		        $que_vals_cnt          =   count($que_vals);
		         
		        $que_values_info       =   array();
		        for($qvc = 0; $qvc < $que_vals_cnt; $qvc++) {
		            $que_vals_info     =   explode(":", $que_vals[$qvc]);
		             
		            $que_values_info[$que_vals_info[0]]    =   $que_vals_info[1];
		        }
		    
		        //Set the parameters
		        $params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QI['QuestionID']);
		        //Set condition
		        $where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
		        //Set the columns
		        $columns  =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
		         
		        $results  =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
		        $WFDck    =   $results['results'][0];
		         
		        if ($WFDck ['Answer'] != $QI['Answer']) {
		    
		            $app_ans_bef_ans    =   $WFDck ['Answer'];
		            $app_ans_aft_ans    =   $QI['Answer'];
		    
		            $ans_bef_ans_jd     =   json_decode($app_ans_bef_ans, true);
		            $ans_aft_ans_jd     =   json_decode($app_ans_aft_ans, true);
		    
		            foreach($ans_bef_ans_jd as $ans_bef_ans_jd_key=>$ans_bef_ans_jd_val) {
		                $ans_bef_ans_jd[$ans_bef_ans_jd_key]   =   $que_values_info[$ans_bef_ans_jd_val];
		            }
		             
		            foreach($ans_aft_ans_jd as $ans_aft_ans_jd_key=>$ans_aft_ans_jd_val) {
		                $ans_aft_ans_jd[$ans_aft_ans_jd_key]   =   $que_values_info[$ans_aft_ans_jd_val];
		            }
		    
		            $app_ans_bef_ans    =   @implode(", ", array_values($ans_bef_ans_jd));
		            $app_ans_aft_ans    =   @implode(", ", array_values($ans_aft_ans_jd));
		    
		            $define .= "Question: " . $QI['Question'] . "<br>";
		            $define .= "&nbsp;&nbsp;Old: " . $app_ans_bef_ans . "<br>";
		            $define .= "&nbsp;&nbsp;New: " . $app_ans_aft_ans . "<br>\n";
		        }
		    
		    
		        //Have to work on web form insert call
		        $PreFilledFormDataObj->insUpdPreFilledFormData($QI);
		    }
		}
		
		foreach ( $_POST as $pre_filled_que_id => $answer ) {
		    
            if(!in_array($pre_filled_que_id, $ques_to_skip)) {
            	
                //Set parameters
                $params         =   array(":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$pre_filled_que_id);
                //Set condition
                $where          =   array("OrgID = 'MASTER'", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
                //Set the columns
                $columns        =   array("Question", "QuestionID", "QuestionOrder", "QuestionTypeID", "value");
                //Get question information
                $results        =   $FormQuestionsObj->getQuestionsInformation("PreFilledFormQuestions", $columns, $where, "", array($params));
                
                //Results question information
                $res_que_info   =   $results['results'][0];
                
                //Set the information
                $question       =   isset($res_que_info['Question']) ? $res_que_info['Question'] : '';
                $questionid     =   $res_que_info['QuestionID'];
                $questionorder  =   isset($res_que_info['QuestionOrder']) ? $res_que_info['QuestionOrder'] : '';
                $questiontypeid =   isset($res_que_info['QuestionTypeID']) ? $res_que_info['QuestionTypeID'] : '';
                $value          =   isset($res_que_info['value']) ? $res_que_info['value'] : '';
    		
                //Bind the parameters
                $params         =   array(":PreFilledFormID"=>$PreFilledFormID, ":OrgID"=>$OrgID);
                //Set condition
                $where          =   array("PreFilledFormID = :PreFilledFormID", "OrgID = :OrgID");
                //Get PreFilled Forms Information
                $results        =   $FormsInternalObj->getPrefilledFormsInfo("FormName", $where, "", array($params));
                $IFformname     =   $results['results'][0]['FormName'];
    		
    			if ($_REQUEST['edit'] == "Y") {
    					
                    //Set the columns
                    $columns            =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
                    //Set the where condition
                    $where              =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
                    //Set the parameters
                    $params             =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$pre_filled_que_id);
                    	
    				//Get the prefilled form data
                    $results            =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
                    $pre_form_data      =   $results['results'][0];
                    	
                    $FDquestion         =   $pre_form_data['Question'];
                    $FDquestionID       =   $pre_form_data['QuestionID'];
                    $FDquestiontypeid   =   $pre_form_data['QuestionTypeID'];
                    $FDanswer 		    =   $pre_form_data['Answer'];
                    $FDvalue            =   $pre_form_data['value'];
    					
    				if ($value != $FDvalue) {
    					$pre_form_data['value']    =   $value;
    					//Update on duplicate key
    					$PreFilledFormDataObj->insUpdPreFilledFormData($pre_form_data);
    				}
    					
    				if (($questiontypeid == 18) || ($questiontypeid == 1818)) {
    		
    					$valueA = array ();
    					$valueA = explode ( "::", $FDvalue );
    		
    					$newans = array ();
    					foreach ( $valueA as $val => $ans ) {
    						$newansplit = explode ( ":", $ans );
    							
    						$newans [$newansplit [0]] = $newansplit [1];
    					}
    		
    					for($i = 1; $i <= count ( $valueA ); $i ++) {
    							
    						$QID      =   $questionid . "-" . $i;
    							
    						//Set the parameters
    						$params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QID);
    						//Set condition
    						$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
    						//Set the columns
    						$columns  =   array("Question", "QuestionID", "QuestionTypeID", "Answer", "value");
    							
    						$results  =   $FormDataObj->getPreFilledFormData($columns, $where, "", "", array($params));
    						$WFDck    =   $results['results'][0];
    							
    						if ($WFDck ['Answer'] != $_POST [$QID]) {
    		
    							$define .= "Question: " . $question . "<br>";
    							$define .= "&nbsp;&nbsp;Old: " . $newans [$WFDck ['Answer']] . "<br>";
    							$define .= "&nbsp;&nbsp;New: " . $newans [$_POST [$QID]] . "<br>\n";
    		
    							if ((substr ( $pre_filled_que_id, 0, 9 ) != "countdown") && ($pre_filled_que_id != 'PreFilledFormID') && ($pre_filled_que_id != 'ApplicationID') && ($pre_filled_que_id != 'RequestID') && ($pre_filled_que_id != 'HoldID') && ($pre_filled_que_id != 'edit') && ($pre_filled_que_id != "process")) {
    								//Set Answer
    								$Answer1 = ($_POST [$QID] == NULL) ? '' : $_POST [$QID];
    									
    								//Data to insert into PreFilledFormData
    								$pre_filled_form_info   =   array(
                                                                    "OrgID"             =>  $OrgID, 
                                                                    "ApplicationID"     =>  $ApplicationID, 
                                                                    "RequestID"         =>  $RequestID, 
                                                                    "PreFilledFormID"   =>  $PreFilledFormID,
                                                                    "QuestionID"        =>  $QID, 
                                                                    "QuestionOrder"     =>  "0", 
                                                                    "QuestionTypeID"    =>  '', 
                                                                    "Answer"            =>  $Answer1, 
                                                                    "Question"          =>  '', 
                                                                    "value"             =>  ''
                        								            );
    								//Update on duplicate key
    								$PreFilledFormDataObj->insUpdPreFilledFormData($pre_filled_form_info);
    							}
    		
    							if ($USERID != "") {
    								//Set Answer
    								$Answer2 = ($USERID == NULL) ? '' : $USERID;
    									
    								$pre_filled_form_info   =   array(
                                                                    "OrgID"             =>  $OrgID, 
                                                                    "ApplicationID"     =>  $ApplicationID, 
                                                                    "RequestID"         =>  $RequestID, 
                                                                    "PreFilledFormID"   =>  $PreFilledFormID,
                                                                    "QuestionID"        =>  'User', 
                                                                    "QuestionOrder"     =>  "0", 
                                                                    "QuestionTypeID"    =>  '', 
                                                                    "Answer"            =>  $Answer2, 
                                                                    "Question"          =>  '', 
                                                                    "value"             =>  ''
    								                                );
    		
    								//Have to work on web form insert call
    								$PreFilledFormDataObj->insUpdPreFilledFormData($pre_filled_form_info);
    							}
    		
    							if ($_POST [$QID] == "") {
    								//set parameters
    								$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":PreFilledFormID"=>$PreFilledFormID, ":QuestionID"=>$QID);
    								//set condition
    								$where  = array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "PreFilledFormID = :PreFilledFormID", "QuestionID = :QuestionID");
    								//Delete prefilled form data
    								$FormsInternalObj->delFormData('PreFilledFormData', $where, array($params));
    							}
    						}
    					}
    				}
    					
    				if (($questionid == $pre_filled_que_id) && ($FDanswer != $answer)) {
    		
    					$define .= "Question: " . $question . "<br>";
    					$define .= "&nbsp;&nbsp;Old: " . $FDanswer . "<br>";
    					$define .= "&nbsp;&nbsp;New: " . $_POST [$pre_filled_que_id] . "<br>\n";
    		
    					if ((substr ( $pre_filled_que_id, 0, 9 ) != "countdown") && ($pre_filled_que_id != 'PreFilledFormID') && ($pre_filled_que_id != 'ApplicationID') && ($pre_filled_que_id != 'RequestID') && ($pre_filled_que_id != 'HoldID') && ($pre_filled_que_id != 'edit') && ($pre_filled_que_id != "process")) {
    							
    						$Answer3 = ($_POST [$pre_filled_que_id] == NULL) ? '' : $_POST [$pre_filled_que_id];
    							
    						//Set the prefilled form data to insert and update on duplicate key
    						$pre_filled_form_info = array(
    								"OrgID"				=>	$OrgID, 
    								"ApplicationID"		=>	$ApplicationID,
    								"RequestID"			=>	$RequestID, 
    								"PreFilledFormID"	=>	$PreFilledFormID,
    								"QuestionID"		=>	$pre_filled_que_id, 
    								"QuestionOrder"		=>	$questionorder,
    								"QuestionTypeID"	=>	$questiontypeid, 
    								"Answer"			=>	$Answer3,
    								"Question"			=>	$question, 
    								"value"				=>	$value
    						);
    							
    						//Have to work on web form insert call
    						$PreFilledFormDataObj->insUpdPreFilledFormData($pre_filled_form_info);
    					}
    		
    					if ($USERID != "") {
    						//Set the prefilled form data to insert and update on duplicate key
    						$pre_filled_form_info = array(
    						        "OrgID"             =>  $OrgID, 
    								"ApplicationID"		=>	$ApplicationID,
    								"RequestID"			=>	$RequestID, 
    								"PreFilledFormID"	=>	$PreFilledFormID,
    								"QuestionID"		=>	'User', 
    								"QuestionOrder"		=>	"0",
    								"QuestionTypeID"	=>	'', 
    								"Answer"			=>	$USERID,
    								"Question"			=>	'', 
    								"value"				=>	''
    						);
    						
    						//Have to work on web form insert call
    						$PreFilledFormDataObj->insUpdPreFilledFormData($pre_filled_form_info);
    					}
    				}
    			} else { // else Edit
    					
    				$Answer2 = ($answer == NULL) ? '' : $answer;
    					
    				//Set the prefilled form data to insert and update on duplicate key
    				$pre_filled_form_info = array(
    								"OrgID"				=>	$OrgID,
    								"ApplicationID"		=>	$ApplicationID,
    								"RequestID"			=>	$RequestID,
    								"PreFilledFormID"	=>	$PreFilledFormID,
    								"QuestionID"		=>	$pre_filled_que_id,
    								"QuestionOrder"		=>	$questionorder,
    								"QuestionTypeID"	=>	$questiontypeid,
    								"Answer"			=>	$Answer2,
    								"Question"			=>	$question,
    								"value"				=>	$value
    				);
    		
    				if ((substr ( $pre_filled_que_id, 0, 9 ) != "countdown") && ($pre_filled_que_id != 'PreFilledFormID') && ($pre_filled_que_id != 'ApplicationID') && ($pre_filled_que_id != 'RequestID') && ($pre_filled_que_id != 'HoldID') && ($pre_filled_que_id != 'edit') && ($pre_filled_que_id != "process")) {
    					//Have to work on web form insert call
    					$PreFilledFormDataObj->insUpdPreFilledFormData($pre_filled_form_info);
    				}
    					
    				//Set the prefilled form data to insert and update on duplicate key
    				$pre_filled_form_info = array(
    								"OrgID"				=>	$OrgID,
    								"ApplicationID"		=>	$ApplicationID,
    								"RequestID"			=>	$RequestID,
    								"PreFilledFormID"	=>	$PreFilledFormID,
    								"QuestionID"		=>	'User',
    								"QuestionOrder"		=>	"0",
    								"QuestionTypeID"	=>	'',
    								"Answer"			=>	$USERID,
    								"Question"			=>	'',
    								"value"				=>	''
    				);
    					
    				if ($USERID != "") {
    					//Have to work on web form insert call
    					$PreFilledFormDataObj->insUpdPreFilledFormData($pre_filled_form_info);
    				}
    			} // end else if edit
			
            }
		} // end foreach
		
		if ($_REQUEST['edit'] == "Y") {
			if (! $define) {
				$define = "No Changes";
			}
			$Comment = "The following form was updated <b>" . $IFformname . "</b><br>" . $define;
		} else {
			$Comment = "The following form was submited: <b>" . $IFformname . "</b><br>";
		}
		
		if ($_POST ['captcha'] != "") {
			//Set parameters
			$captcha_info    =   array("OrgID"=>$OrgID, "ApplicationID"=>$ApplicationID, "RequestID"=>$RequestID, "PreFilledFormID"=>$PreFilledFormID, "QuestionID"=>"captcha", "Question"=>"Captcha", "Answer"=>$_POST ['captcha']);
			//Have to work on web form insert call
			$PreFilledFormDataObj->insUpdPreFilledFormData($captcha_info);
		}
		
		//Set the parameters
		$params   =   array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID, ":UniqueID"=>$PreFilledFormID);
		//Set the condition
		$where    =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = :UniqueID");
		//Set the data to update
		$set_info =   array("LastUpdated = NOW()", "Status = 3");
		//Update Internal Forms Assigned
		$FormsInternalObj->updInternalFormsAssigned($set_info, $where, array($params));
		
		if ($PreFilledFormID == "FE-I9") {
			//Set the data to update
			$set_info = array("LastUpdated = NOW()", "AssignedDate = NOW()", "DueDate = DATE_ADD(NOW(), INTERVAL 1 day)", "Status = 2");
			//set parameters
			$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
			//set where condition
			$where 	= array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = 'FE-I9m'");
		
			//Update Internal Forms Assigned
			$FormsInternalObj->updInternalFormsAssigned($set_info, $where, array($params));
		}
		if ($PreFilledFormID == "FE-I9-2020") {
			//Set the data to update
			$set_info = array("LastUpdated = NOW()", "AssignedDate = NOW()", "DueDate = DATE_ADD(NOW(), INTERVAL 1 day)", "Status = 2");
			//set parameters
			$params = array(":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
			//set where condition
			$where 	= array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID", "UniqueID = 'FE-I9m-2020'");
		
			//Update Internal Forms Assigned
			$FormsInternalObj->updInternalFormsAssigned($set_info, $where, array($params));
		}
		
		if($define == "There were no changes") {
		    $define   =   "";
		}
		
		// History
		//Information to insert set parameters
		$internal_form_info = array(
				"OrgID"				=>	$OrgID, 
				"ApplicationID"		=>	$ApplicationID,
				"RequestID"			=>	$RequestID, 
				"InternalFormID"	=>	$PreFilledFormID,
				"Date"				=>	"NOW()", 
				"UserID"			=>	$USERID, 
				"Comments"			=>	$Comment,
                "UpdatedFields"     =>  $define	
		);
		
		//Insert internal form history
		$FormsInternalObj->insInternalFormHistory($internal_form_info);
		
		if ($_REQUEST['edit'] == "Y") {
			$MESSAGE = "The form " . $IFformname . " has been updated.";
		} else {
			$MESSAGE = "The form " . $IFformname . " has been submitted.";
		}
		
		if(FROM_SRC == "USERPORTAL")
		{
		    //Assigned Forms List
		    $assigned_forms_list    =   G::Obj('FormsInternal')->getAssignedFormsInfo($OrgID, $RequestID, $ApplicationID, $_REQUEST['ProcessOrder']);

            $link                   =   "";
            $link_hit_status        =   false;
            
		    for($afl = 0; $afl < count($assigned_forms_list); $afl++) {
		        
		        $assigned_forms_info    =   $assigned_forms_list[$afl];
		        
		        foreach($assigned_forms_info as $FormType=>$IFA) {
		            
		            if($IFA["Status"] == "Pending"
		                && $FormType != "Attachment"
		                && $FormType != "External Link"
		                && $FormType != "HTML Page") {
		                    
		                $form_status        =   $IFA["IFAInfo"]["Status"];
		                $form_status_text   =   $IFA["FormStatusText"];
		                $link               =   $IFA["Link"];
		                
                        $afl                =   count($assigned_forms_list);
                        
                        $link_hit_status    =   true;
		            }
		            
		            if($link_hit_status == true) {
		                break;
		            }
		        }
		    }
		    
	        $redirect_link = USERPORTAL_HOME . 'thankyouInternalForms.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '&ProcessOrder=' . $ProcessOrder . '&navpg=status&navsubpg=view&msg=form_suc';
	        header("Location:".$redirect_link);
	        exit();
		}
		else {
			$complete_status = "success";
		}
		
		echo '<br><br><br><br><br><br>';
		
	}
}
?>
