<?php
//Application Information
$requisition_fields =   array("RequisitionID", "JobID", "Title", "City", "State", "ZipCode");
//Set where condition for requisition
$where_req_info     =   array("OrgID = :OrgID", "MultiOrgID = :MultiOrgID", "RequestID = :RequestID");
//Set parameters for requisition
$params_req_info    =   array(":OrgID"=>$OrgID, ":MultiOrgID"=>$MultiOrgID, ":RequestID"=>$RequestID);

$columns            =   "date_format(EntryDate,'%Y-%m-%d %H:%i EST') EntryDate, ApplicantSortName, ProcessOrder, DispositionCode";
//Get Application Information
$application_info   =   $ApplicationsObj->getApplicationInformation($columns, $OrgID, $MultiOrgID, $RequestID, $ApplicationID);
//Get Requisition Information
$results_info       =   $RequisitionsObj->getRequisitionInformation($requisition_fields, $where_req_info, "", "", array($params_req_info));
$requisition_info   =   $results_info['results'][0];

//Get Irecruit Users Information
$columns            =   "FirstName, LastName, EmailAddress";
//Set where condition
$where              =   array("OrgID = :OrgID", "UserID = (SELECT owner FROM Requisitions WHERE OrgID = :ROrgID AND RequestID = :RequestID)");
//Set parameters
$params             =   array(":OrgID"=>$OrgID, ":ROrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Get UserInformation
$results_owner      =   $IrecruitUsersObj->getUserInformation($columns, $where, "", array($params));
if(is_array($results_owner['results'][0])) list ( $First, $Last, $Email ) = array_values($results_owner['results'][0]);

//Get uploaded documents count
$vault_info         =   G::Obj('ApplicantVault')->getUploadedDocumentsCount($OrgID, $ApplicationID, $RequestID);
$attached_documents_count = $vault_info['count'];

$locations_list[] 	=	$requisition_info['City'];
$locations_list[] 	=	$requisition_info['State'];
$locations_list[] 	=	$requisition_info['ZipCode'];

echo '<div class="row" style="margin-bottom:4px">';
    echo '<div class="col-lg-12">';
        echo '<strong>Applicant Name</strong>: ' . $application_info ['ApplicantSortName'];
    echo '</div>';
echo '</div>';
    
echo '<div class="row" style="margin-bottom:4px">';
    echo '<div class="col-lg-12">';
        echo '<strong>Job Title</strong>: ' . $requisition_info["Title"];
    echo '</div>';
echo '</div>';
    
echo '<div class="row" style="margin-bottom:4px">';
    echo '<div class="col-lg-12">';
        echo '<strong>ApplicationID</strong>: ' . $_GET ['ApplicationID'];
    echo '</div>';
echo '</div>';


echo '<div class="row" style="margin-bottom:4px">';
    echo '<div class="col-lg-12">';
        echo '<strong>Application Date</strong>: ' . $application_info ['EntryDate'];
    echo '</div>';
echo '</div>';

echo '<div class="row" style="margin-bottom:4px">';
    echo '<div class="col-lg-12">';
        echo '<strong>Requisition/JobID</strong>: ' . $requisition_info["RequisitionID"] . "/" . $requisition_info["RequisitionID"];
    echo '</div>';
echo '</div>';

echo '<div class="row" style="margin-bottom:4px">';
if(is_array($locations_list)) {
    echo '<div class="col-lg-12">';
    echo '<strong>Location</strong>: ';
	echo implode(", ", $locations_list);
    echo '</div>';
}
echo '</div>';

echo '<div class="row" style="margin-bottom:10px">';
echo '<br>';
echo '</div>';
?>
