<?php 
require_once 'userportal.inc';
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

$FormID = $_REQUEST['FormID'];
$RequestID = $_REQUEST['RequestID'];
$ApplicationID = $_REQUEST['ApplicationID'];
$APPDATA = G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);

// All sections need to be complete
$processed_sections = unserialize(G::Obj('ApplicantDetails')->getAnswer($OrgID, $ApplicationID, "ProcessedSections"));

$application_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("VeteranEditStatus, DisabledEditStatus, AAEditStatus", $OrgID, $ApplicationID, $RequestID);
$VeteranEditStatus = $application_info['VeteranEditStatus'];
$DisabledEditStatus = $application_info['DisabledEditStatus'];
$AAEditStatus = $application_info['AAEditStatus'];

$userportal_sections_list       =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

if ($AAEditStatus != "Y") { unset($userportal_sections_list['AA']); }
if ($VeteranEditStatus != "Y") { unset($userportal_sections_list['VET']); }
if ($DisabledEditStatus != "Y") { unset($userportal_sections_list['DIS']); }

$sign_application   =   "true";
if (count($userportal_sections_list) > count($processed_sections)) {
   $sign_application   =   "false";
   $errors_list	=   "";
}

echo json_encode(array("errors_list"=>$errors_list, "success"=>$sign_application, "section_id_names"=>$section_id_names));
exit;
?>
