<?php
require_once 'userportal.inc';
require_once USERPORTAL_DIR . 'Authenticate.inc';

$page_title = "Requisition Expired";

$req_info = $RequisitionsObj->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $_REQUEST['OrgID'], $_REQUEST['RequestID']);

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

$LISTINGS_URL = strtolower ( USERPORTAL_HOME );

require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">'.$page_title.'</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

echo '<br><br><br><br>';
echo '<h3 style="text-align:center">';
echo 'Sorry this requisition is expired.';
echo '<br>';
echo '</h3>';
echo '<h5 style="text-align:center">';
echo $req_info['Title'];
echo '</h5>';
echo '<br><br><br><br><br>';

echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
require_once USERPORTAL_DIR . 'Footer.inc';
?>