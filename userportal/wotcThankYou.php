<?php
require_once 'userportal.inc';

$page_title = "Wotc Form";

require_once USERPORTAL_DIR . 'Authenticate.inc';

if(isset($_REQUEST['IrecruitApplicationID']) && $_REQUEST['IrecruitApplicationID'] != "") $IrecruitApplicationID = $_REQUEST['IrecruitApplicationID'];
if(isset($_REQUEST['wotcID']) && $_REQUEST['wotcID'] != "") $WOTCID = $_REQUEST['wotcID'];
if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['IrecruitRequestID']) && $_REQUEST['IrecruitRequestID'] != "") $IrecruitRequestID = $_REQUEST['IrecruitRequestID'];

require_once USERPORTAL_DIR . 'formsInternal/ProcessWotcForm.inc';

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo 'Thank You';

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'assignedInternalForms.php?ApplicationID='.$_REQUEST['IrecruitApplicationID'].'&RequestID='.$_REQUEST['IrecruitRequestID'].'&ProcessOrder='.$_REQUEST['IrecruitProcessOrder'].'&navpg=status&navsubpg=view">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Assigned Forms';
echo '</a>';
echo '</span>';

echo '</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';

//Row Start
echo '<div class="row">';

echo "<div class=\"row\">\n";
echo "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";

//Process thank you. call center
echo G::Obj('WOTCApp')->processThankYou($WOTCID, $ApplicationID);

$wotc_app_details   =   G::Obj('WOTCIrecruitApplications')->getWotcApplicationByApplicationID("*", $WOTCID, $IrecruitApplicationID, $IrecruitRequestID, $ApplicationID);

$wotc_qualifies     =   G::Obj('WOTCApp')->getApplicationInfo($WOTCID, $ApplicationID);

$set_info           =	array("Qualifies = :Qualifies");
$where_info         =	array(
        					"OrgID						=	:OrgID",
        					"WotcFormID					=	:WotcFormID",
        					"wotcID						=	:wotcID",
        					"IrecruitApplicationID		=	:IrecruitApplicationID",
        					"IrecruitRequestID			=	:IrecruitRequestID",
        					"ApplicationID				=	:ApplicationID"
        				);
$params             =   array(
        					":Qualifies"				=>	$wotc_qualifies['Qualifies'],
        					":OrgID"					=>	$OrgID, 
        					":WotcFormID"				=>	$wotc_app_details['WotcFormID'], 
        					":wotcID"					=>	$wotc_app_details['wotcID'], 
        					":IrecruitApplicationID"	=>	$wotc_app_details['IrecruitApplicationID'], 
        					":IrecruitRequestID"		=>	$wotc_app_details['IrecruitRequestID'], 
        					":ApplicationID"			=>	$wotc_app_details['ApplicationID']
        				);

//Update wotc application qualifies information
G::Obj('WOTCIrecruitApplications')->updWotcApplicationInfo($set_info, $where_info, array($params));

echo "</div>\n";
echo "</div>\n";


echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
require_once USERPORTAL_DIR . 'Footer.inc';
?>
