<?php
//Validate the information
G::Obj('GetFormPostAnswer')->POST   =   $_REQUEST;
G::Obj('GetFormPostAnswer')->FILES  =   $_FILES;

//Get Modified POST Answers
$POSTANSWERS                =   G::Obj('GetFormPostAnswer')->getPostDataAnswersOfFormQuestions($OrgID, $FormID);

//Prefill the $FILES
$FILES                      =   $_FILES;

if ($ApplicationID != "") {
    // Set attachment where condition
    $attach_where   =   array (
            "OrgID            =   :OrgID",
            "ApplicationID    =   :ApplicationID",
    );
    // Set attachment parameters
    $attach_params  =   array (
            ":OrgID"          =>  $OrgID,
            ":ApplicationID"  =>  $ApplicationID,
    );
    $results =   $AttachmentsObj->getApplicantAttachments ( "*", $attach_where, '', array ($attach_params) );

    foreach ($results['results'] AS $ATT) {
      $FILES[$ATT['TypeAttachment']]   =   array(
        "name"  =>  $ATT['TypeAttachment'],
        "type"  =>  $ATT['FileType']
      );
    }

} else { // ApplicationID

$attachment_types           =   array();
$attachments_info           =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
   for($aai = 0; $aai < count($attachments_info); $aai++) {

      $attachment_types[]     =   $attachments_info[$aai]['TypeAttachment'];

      $file_type              =   $attachments_info[$aai]['FileType'];
      $file_attachment_name   =   $attachments_info[$aai]['TypeAttachment'];
      $file_name              =   $file_attachment_name.".".$file_type;

      $FILES[$attachments_info[$aai]['TypeAttachment']]   =   array(
        "name"  =>  $attachments_info[$aai]['TypeAttachment'],
        "type"  =>  $file_type
      );
   }

} // end else ApplicationID

//Validate the information
G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']   =   $POSTANSWERS;
G::Obj('ValidateApplicationForm')->FORMDATA['FILES']     =   $FILES;

//Validate Required Fields By SectionID
$errors_info    =   G::Obj('ValidateApplicationForm')->validateApplicationForm($OrgID, $FormID, $_REQUEST['SectionID'],$HoldID);
$errors_list    =   $errors_info['ERRORS'];
?>
