<?php
$title      =   'Edit Affirmitive Action';
$PAGE_TYPE  =   "PopUp";

require_once 'userportal.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
}

if (in_array(COMMON_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once COMMON_DIR . 'formsInternal/EditAA.inc';
}

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>