<?php
require_once 'userportal.inc';
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    include USERPORTAL_DIR . 'JobApplicationRequestVars.inc';
}    

if(!isset($_REQUEST['RequestID']) || $_REQUEST['RequestID'] == "") {
    header("Location:index.php?OrgID=".$OrgID."&navpg=listings");
    exit;
}

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}    

$formtable  =   "FormQuestions";
$page_title =   "Prescreen Questions";

//Requisition details
$req_info           =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title", $OrgID, $_REQUEST['RequestID']);

$ReqMultiOrgID      =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

//Is Internal Requisition or Not
if ($feature ['InternalRequisitions'] == "Y") {
    $InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
}

// Bind parameters
$params             =   array (':OrgID' => $OrgID, ':RequestID' => $_REQUEST['RequestID']);
// Set condition
$where              =   array ("OrgID = :OrgID", "RequestID = :RequestID");

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
    $where [] = "Internal IN ('Y','B')";
} else {
    $where [] = "Internal IN ('N','B','')";
}

// Get Prescreen Questions Information
$rslts  =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation ( "*", $where, "SortOrder", array($params) );

$rejection_text = '';
if(isset($_POST['prescreen_process']) && $_POST['prescreen_process'] == "Process") {
    $cnt = 0;

    // loop through all questions for a match. If one doesn't it will throw a not qualify
    if (is_array ( $rslts ['results'] )) {
        foreach ( $rslts ['results'] as $PRESCREEN ) {

            if ($PRESCREEN ['Answer'] != $_POST [$PRESCREEN ['SortOrder']]) {
                $cnt ++;
            }

            //Insert Prescreen Results
            $prescreen_result_info = array (
                "OrgID"         =>  $OrgID,
                "HoldID"        =>  $OrgID.$UpUserID.$_REQUEST['RequestID'],
                "RequestID"     =>  $_REQUEST['RequestID'],
                "Question"      =>  $PRESCREEN ['Question'],
                "Answer"        =>  $PRESCREEN ['Answer'],
                "SortOrder"     =>  $PRESCREEN ['SortOrder'],
                "EntryDate"     =>  "NOW()",
                "Submission"    =>  $_POST [$PRESCREEN ['SortOrder']]
            );

            G::Obj('PrescreenQuestions')->insUpdPrescreenResults ( $prescreen_result_info );

        } // end foreach
    }


    if ($cnt > 0) {

        //Get Prescreen Text Information
        $where = array("OrgID = :OrgID", "Rejection != ''");
        //Get PrescreenText Information
        $PRESCREENT = G::Obj('PrescreenQuestions')->getPrescreenTextInfo($OrgID, $where, "OrgID LIMIT 1");
        
        // this is a fail entry
        $rejection_text .= $PRESCREENT ['Rejection'] . "\n";
        $rejection_text .= '<br><br>' . "\n";
        $rejection_text .= 'Please see our full listings for other positions you may qualify for <a href="index.php?';
        if ($MultiOrgID) {
            $rejection_text .= 'MultiOrgID=' . $MultiOrgID . '&navpg=listings&pg=split';
        } else {
            $rejection_text .= 'OrgID=' . $OrgID . '&navpg=listings&pg=split';
        }
        $rejection_text .= '">here</a>.';
        $rejection_text .= '<br><br>' . "\n";
    }
    else {
        
        //Insert User Application Process
        $UserPortalInfoObj->insUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], '', $req_info['Title'], "Pending");
        
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            header("Location:jobApplication.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']."&MultiOrgID=".$MultiOrgID);
            exit;
        }
        else {
            header("Location:jobApplication.php?OrgID=".$OrgID."&RequestID=".$_REQUEST['RequestID']);
            exit;
        }
    }
}

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Application Form:</h3>';
echo '</div>';
echo '</div>';

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

if($rejection_text == '') {
    ?>
    <form name="frmPreScreenInfo" id="frmPreScreenInfo" method="post">
    <?php
    if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist)) {
        $prescreen_app_form = include USERPORTAL_DIR . "Prescreen.inc";
        echo $prescreen_app_form;
    }
    ?>
    <input type="hidden" name="prescreen_process" id="prescreen_process" value="Process">
    <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit">
    </form>
    <?php
}
else {
    echo $rejection_text;
}
 
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>