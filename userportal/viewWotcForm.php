<?php
require_once 'userportal.inc';

$page_title = "Wotc Form";

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

if(isset($_REQUEST['IrecruitApplicationID']) && $_REQUEST['IrecruitApplicationID'] != "") $IrecruitApplicationID = $_REQUEST['IrecruitApplicationID'];
if(isset($_REQUEST['wotcID']) && $_REQUEST['wotcID'] != "") $WOTCID = $_REQUEST['wotcID'];
if(isset($_REQUEST['IrecruitRequestID']) && $_REQUEST['IrecruitRequestID'] != "") $IrecruitRequestID = $_REQUEST['IrecruitRequestID'];
if(isset($_REQUEST['IrecruitProcessOrder']) && $_REQUEST['IrecruitProcessOrder'] != "") $IrecruitProcessOrder = $_REQUEST['IrecruitProcessOrder'];

$org_info   		=   G::Obj('WOTCOrganization')->getOrganizationInfo("WotcFormID", $WOTCID);
$WotcFormID 		=   $org_info['WotcFormID'];

$ApplicationID		=	$_REQUEST['ApplicationID'];

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo 'Work Opportunity Tax Credit Application';

$back_to_assigned_forms =   'assignedInternalForms.php?ApplicationID='.$IrecruitApplicationID.'&RequestID='.$IrecruitRequestID.'&ProcessOrder='.$IrecruitProcessOrder.'&navpg=status&navsubpg=view';

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.$back_to_assigned_forms.'">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Assigned Forms';
echo '</a>';
echo '</span>';

echo '</h3>';
echo '</div>';
echo '</div>';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'ViewWotcForm.inc';
}

echo '</div>';
echo '</div>';	//Page Wrapper End

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}