<?php
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

$req_details_info       =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Active, DATE(ExpireDate) as ExpireDate", $_REQUEST['OrgID'], $_REQUEST ['RequestID']);
$dates_diff             =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);

if($req_details_info['Active'] == 'N'
  || $dates_diff > 0) {
    header("Location:thankyou.php?RequestID=".$_REQUEST ['RequestID']."&OrgID=".$_REQUEST['OrgID'].'&msg=reqexpired');
    exit;
}
    
if($_REQUEST['RequestID'] == "") {
	header("Location:index.php?OrgID=".$OrgID."&navpg=profiles&navsubpg=status");
	exit;
}

$signature_rand_id      =   time();
$_SESSION['U']['SignatureRandID']  = $signature_rand_id;

$req_info               =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $OrgID, $_REQUEST['RequestID']);
$FormID                 =   $req_info['FormID'];
$MultiOrgID             =   $req_info['MultiOrgID'];
$HoldID                 =   $OrgID.$UpUserID.$_REQUEST['RequestID'];

$purpose                =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

$processed_sections     =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");
if($processed_sections  == NULL || $processed_sections == "" || empty($processed_sections) || !isset($processed_sections)) {
    $processed_sections =   array();
}
else {
    $processed_sections =   unserialize($processed_sections);
}

$APPDATA                =   array ();
$APPDATAREQ             =   array ();

$results                =   G::Obj('UserPortalInfo')->getApplicationInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);

if(is_array($results['results'])) {
    foreach($results['results'] as $row) {
        $APPDATA [$row ['QuestionID']] = $row ['Answer'];
    } // end foreach
}

$attachment_types       =   array();
$attachments_info       =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
$attachments_error      =   FALSE;
$resume_error           =   FALSE;
$coverletter_error      =   FALSE;
$other_error            =   FALSE;

for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[] =   $attachments_info[$aai]['TypeAttachment'];
    
    if($purpose[$attachments_info[$aai]['TypeAttachment']] == 'resume') {
        $resume_file_name       =   $attachments_info[$aai]['PurposeName'].".".$attachments_info[$aai]['FileType'];
        
        $APPID                  =   $UpUserID."*".$_REQUEST['RequestID'];
        $apatdir                =   IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
        $filename               =   $apatdir . '/' . $APPID . '-' . $resume_file_name;

        if (!file_exists($filename)) {
            $attachments_error  =   TRUE;
            $resume_error       =   TRUE;
        }
        else if (filesize($filename) == 0) {
            $attachments_error  =   TRUE;
            $resume_error       =   TRUE;
        }
    }
    else if($purpose[$attachments_info[$aai]['TypeAttachment']] == 'coverletter') {
        $coverletter_file_name  =   $attachments_info[$aai]['PurposeName'].".".$attachments_info[$aai]['FileType'];
        $APPID                  =   $UpUserID."*".$_REQUEST['RequestID'];
        $apatdir                =   IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
        $filename               =   $apatdir . '/' . $APPID . '-' . $coverletter_file_name;

        if (!file_exists($filename)) {
            $attachments_error  =   TRUE;
            $coverletter_error  =   TRUE;
        }
        else if (filesize($filename) == 0) {
            $attachments_error  =   TRUE;
            $coverletter_error  =   TRUE;
        }
    }
    else if($purpose[$attachments_info[$aai]['TypeAttachment']] == 'other') {
        $other_file_name        =   $attachments_info[$aai]['PurposeName'].".".$attachments_info[$aai]['FileType'];
        $APPID                  =   $UpUserID."*".$_REQUEST['RequestID'];
        $apatdir                =   IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
        $filename               =   $apatdir . '/' . $APPID . '-' . $other_file_name;

        if (!file_exists($filename)) {
            $attachments_error  =   TRUE;
            $other_error        =   TRUE;
        }
        else if (filesize($filename) == 0) {
            $attachments_error  =   TRUE;
            $other_error        =   TRUE;
        }
    }
}

//Existing Sections based on current requisition
$req_sections_list      =   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");
$req_sections_ids       =   array_keys($req_sections_list);

G::Obj('ValidateApplicationForm')->FORMDATA['REQUEST']  =   $APPDATA;

$POSTANSWERS            =   G::Obj('GetFormPostAnswer')->getPostDataAnswersOfFormQuestions($OrgID, $FormID);

//Prefill the $FILES
$FILES                      =   $_FILES;
$attachment_types           =   array();
$attachments_info           =   G::Obj('UserPortalInfo')->getApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);

for($aai = 0; $aai < count($attachments_info); $aai++) {
    $attachment_types[]     =   $attachments_info[$aai]['TypeAttachment'];
    
    $file_type              =   $attachments_info[$aai]['FileType'];
    $file_attachment_name   =   $attachments_info[$aai]['TypeAttachment'];
    $file_name              =   $file_attachment_name.".".$file_type;

    $FILES[$attachments_info[$aai]['TypeAttachment']]   =   array(
                                                                "name"  =>  $attachments_info[$aai]['TypeAttachment'],
                                                                "type"  =>  $file_type
                                                            );
}

$sec_child_ques_status  =   "false";

$userportal_sections_list	=   G::Obj('ApplicationFormSections')->getApplicationFormSectionsByFormID("*", $OrgID, $FormID, "Y");

$validateallformfields  =   require_once USERPORTAL_DIR . 'ValidateAllFormFields.inc';
$errors_list            =   $validateallformfields['errors_list'];
$section_id_names       =   $validateallformfields['section_id_names'];

$page_title             =   "Sign and complete the application";

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

$LISTINGS_URL           =   strtolower ( USERPORTAL_HOME );

require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Signature</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

if(count($errors_list) > 0) {
    ?>
    <p style="text-align: center; font-size: 15px;">
    	Unable to process your application as you missed some required fields. Please fill those and submit again.
    	<a href="<?php echo USERPORTAL_HOME;?>jobApplication.php?RequestID=<?php echo $_REQUEST ['RequestID'];?>&OrgID=<?php echo $OrgID;?>&MultiOrgID=<?php echo $_REQUEST['MultiOrgID'];?>">Edit Application</a>
    </p>
    <?php
}
else if($attachments_error == TRUE) {
    echo '<p style="text-align: left; color:red; font-size: 15px;">';
	echo "There is an error in below attachments. You have uploaded the file but it seems like the file is blank or it is missing. Please go back and reupload the files below.<br>";
	if($resume_error       ==   TRUE) echo "Resume<br>";
	if($coverletter_error  ==   TRUE) echo "Cover Letter<br>";
    if($other_error        ==   TRUE) echo "Other <br>";
    echo '</p>';
    
    $go_back = "jobApplication.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID."&MultiOrgID=".$_REQUEST['MultiOrgID'];
    echo '<input type=\'button\' name=\'btnGoBack\' id=\'btnGoBack\' value=\'Go Back\' onclick=\'location.href="'.$go_back.'"\'>';
}
else if(isset($_POST['ProcessApplication']) && $_POST['ProcessApplication'] == "Yes" && count($errors_list)  == 0) {
    // Query and Set Text Elements
    $params     =   array(':OrgID'=>$OrgID, ':FormID'=>$FormID);
    //Get text blocks information
    $where      =   array("OrgID = :OrgID", "FormID = :FormID");
    $results    =   G::Obj('FormFeatures')->getTextBlocksInfo(array("TextBlockID", "Text"), $where, "", array($params));
    
    if(is_array($results['results'])) {
        foreach ($results['results'] as $row) {
            $TextBlocks [$row ["TextBlockID"]] = $row ["Text"];
        }
    }
    
    if ($TextBlocks ["ApplicantAuthorizations"]) {
        $ApplicantAuthorizations = '&nbsp;&nbsp;&nbsp;<font style="font-size:8pt"><a href="' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '" target="_blank" onClick="window.open(\'' . PUBLIC_HOME . 'applicant_authorizations.php?OrgID=' . $OrgID . '&FormID=' . $FormID . '\',\'\',\'width=600,height=450,resizable=yes,scrollbars=yes\'); return false;">Disclosures</a></font>';
    } else {
        $ApplicantAuthorizations = "";
    }
    ?>
    <form name="frmProcessApplication" id="frmProcessApplication" method="post" action="thankyou.php">
        <?php 
        $Signature  = '<td width="18%"><font style="color:red">*</font> Electronic Signature: <font style="color:red">X</font></td>';
        $Signature .= '<td><input type="text" name="signature" id="signature" size="33" value="' . $APPDATA ['signature'] . '"></td>';
        
        if($ApplicantAuthorizations) {
        	echo '<b class="title">Applicant Authorizations</b>' . $ApplicantAuthorizations . '<br>';
        }
        if ($TextBlocks ["Signature"]) {
            echo $TextBlocks ["Signature"];
            echo '<table border="0" cellspacing="3" cellpadding="0" class="table table-striped table-bordered table-hover">';
            echo '<tr>';
            echo '<td colspan="2">';
            echo '<input type="checkbox" name="agree" id="agree" value="Y"';
            if ($APPDATA ['agree'] == "Y") {
                echo ' checked';
            }
            if ($OrgID == "I20100301") { // Meritan
                echo '> <font style="color:red">*</font> I have read, understood and agree with the above Applicant Authorizations and Arbitration Agreement.</td></tr>';
            } else {
                echo '> <font style="color:red">*</font> I agree with the above statement.</td></tr>';
            }
            echo '</td>';
            echo '<tr>';
            echo $Signature;
            echo '</tr>';
            echo '</table>';
        }
        if ($TextBlocks ["FormFooter"]) {
            echo $TextBlocks ["FormFooter"];
        }
        ?>
        <input type="hidden" name="RequestID" id="RequestID" value="<?php echo $_REQUEST['RequestID'];?>">
        <input type="hidden" name="FormID" id="FormID" value="<?php echo $_REQUEST['FormID'];?>">
        <input type="hidden" name="ProcessApplication" id="ProcessApplication" value="<?php echo $_REQUEST['ProcessApplication'];?>">
        <input type="hidden" name="OrgID" id="OrgID" value="<?php echo $OrgID;?>">
        <input type="hidden" name="MultiOrgID" id="MultiOrgID" value="<?php echo $_REQUEST['MultiOrgID'];?>">
        <input type="hidden" name="SectionID" id="SectionID" value="SIGNATURE">
        <input type="hidden" name="SignatureRandIDTime" id="SignatureRandIDTime" value="<?php echo $signature_rand_id;?>">
        
        <?php
        if(count($errors_list) == 0) {
            ?>
            <input type="button" name="btnProcessApplicationForm" id="btnProcessApplicationForm" value="Submit Application" onclick="return validateSignature(this)">
            &nbsp;
            <input type="button" name="btnGoBack" id="btnGoBack" value="Go Back" onclick="location.href='jobApplication.php?RequestID=<?php echo $_REQUEST['RequestID'];?>&OrgID=<?php echo $OrgID;?>&MultiOrgID=<?php echo $_REQUEST['MultiOrgID'];?>'">
            <?php
        }
        else {
        	?><input type="button" name="btnProcessApplicationForm" id="btnProcessApplicationForm" class="btnDisabled" value="Submit Application" onclick="return validateSignature(this)"><?php
        }
        ?>
        
        <div id="process_message"></div>
    </form>
    <?php
}

echo '<script type="text/javascript" src="js/process-job-application.js"></script>';
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
require_once USERPORTAL_DIR . 'Footer.inc';
?>
