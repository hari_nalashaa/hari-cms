function messageForm(user, pass, email, min) {
	var msgtxt = '';
	msgtxt += "User: " + user + "\n"
	if (pass) {
		msgtxt += "Password: " + pass + "\n"
	}
	msgtxt += "Email Address: " + email + "\n"
	msgtxt += "Timeout Minutes: " + min + "\n"

	msgtxt = "The following has been updated:\n\n" + msgtxt + "\nPlease record this for your records.";
	alert(msgtxt);
}

function errorMessage(error) {
	msgtxt = "The following error has occured:\n\n" + error + "\n";
	alert(msgtxt);
}

function HideContent(d) {
	document.getElementById(d).style.display = "none";
}

function ShowContent(d) {
	document.getElementById(d).style.display = "block";
}

function ReverseDisplay(d) {
	if (document.getElementById(d).style.display == "none") {
		document.getElementById(d).style.display = "block";
	} else {
		document.getElementById(d).style.display = "none";
	}
}