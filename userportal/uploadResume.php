<?php
require_once 'userportal.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

$requisition_info       =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $OrgID, $_REQUEST['RequestID']);
$FormID                 =   $requisition_info['FormID'];
$MultiOrgID             =   $requisition_info['MultiOrgID'];
$HoldID                 =   $OrgID.$UpUserID.$RequestID;

$purpose                =   G::Obj('Attachments')->getPurposeNamesList($OrgID, $FormID);

$where_info             =   array("FormID = :FormID", "QuestionID = :QuestionID", "SectionID = :SectionID", "OrgID = :OrgID");
$params_info            =   array(":FormID"=>$FormID, ":SectionID"=>"6", ":QuestionID"=>'resumeupload', ":OrgID"=>$OrgID);
$form_que_info          =   G::Obj('FormQuestions')->getFormQuestionsInformation("Required", $where_info, "", array($params_info));
$form_que_req           =   $form_que_info['results'][0];

$parse_userportal_resume = G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ParseResume");

$upload_error           =   "false";

if(isset($_POST['btnRedirectToNextPage']) && $_POST['btnRedirectToNextPage'] == "Next") {
    //Insert User Application Process
    G::Obj('UserPortalInfo')->insUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], '', $requisition_info['Title'], "Pending");

    // Bind Parameters
    $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ParseResume", "Answer"=>"Yes");
    //Insert temporary data
    G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
    
    if(isset($MultiOrgID) && $MultiOrgID != "") {
        header("Location:jobApplication.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID);
        exit;
    }
    else {
        header("Location:jobApplication.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID);
        exit;
    }
}
else if (isset($_POST['btnParseResume']) && $_POST['btnParseResume'] == "Save/Next") {

    //Insert User Application Process
    G::Obj('UserPortalInfo')->insUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], '', $requisition_info['Title'], "Pending");
    
    if($form_que_req['Required'] == "Y" && $_FILES['resumefile']['type'] == "" && $_POST['resumeupload'] == '')
    {
        $upload_error   =   "true";
    }
    
    if($upload_error == "false") {
        
        if($parse_userportal_resume == NULL 
            || $parse_userportal_resume == "" 
            || empty($parse_userportal_resume) 
            || !isset($parse_userportal_resume)) {
        
            // Bind Parameters
            $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ParseResume", "Answer"=>"Yes");
            //Insert temporary data
            G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
        
            if($_FILES['resumefile']['type'] != "") {
                
                //Set parameters
                $params     =   array (':OrgID'=>$OrgID, ':FormID'=>$FormID);
                // Set Form Question Fields
                $where      =   array ("OrgID = :OrgID", "FormID = :FormID", "SectionID = 6", "QuestionTypeID = 8", "Active = 'Y'");
                //Get FormQuestions Information
                $resultsIN  =   G::Obj('FormQuestions')->getFormQuestionsInformation ( "*", $where, "QuestionOrder", array ($params) );
                
                if (is_array ( $resultsIN ['results'] )) {
                    foreach ( $resultsIN ['results'] as $FIL ) {
                
                        if($FIL ['QuestionID'] == "resumeupload") {
                
                            $ext        =   "";
                            $files_info =   explode(".", $_FILES['resumefile']['name']);
                            if(is_array($files_info) && count($files_info) > 0) {
                                $ext    =   end($files_info);
                            }
                
                            $APPID     =   $UpUserID."*".$_REQUEST['RequestID'];
                            $apatdir   =   IRECRUIT_DIR . "vault/" . $OrgID . "/applicantattachments";
                            $filename  =   $apatdir . '/' . $APPID . '-' . $purpose [$FIL ['QuestionID']] . '.' . $ext;
                
                            if(!file_exists($filename)) {
                                $data      =   file_get_contents ( $_FILES['resumefile']['tmp_name'] );
                                $fp        =   fopen($filename, 'w+');
                                fwrite($fp, $data);
                                fclose($fp);
                                chmod($filename, 0666);
                            }
                
                            //Applicants Attachments Information
                            $applicant_attach_info = array (
                                "OrgID"             =>  $OrgID,
                                "MultiOrgID"        =>  $MultiOrgID,
                                "UserID"            =>  $UpUserID,
                                "SectionID"         =>  "6",
                                "RequestID"         =>  $_REQUEST['RequestID'],
                                "TypeAttachment"    =>  $FIL ['QuestionID'],
                                "PurposeName"       =>  $purpose [$FIL ['QuestionID']],
                                "FileType"          =>  $ext
                            );
                            
                            //Insert applicant attachments information
                            G::Obj('UserPortalInfo')->insApplicantAttachmentsTemp ( $applicant_attach_info );
                        }
                
                    }
                }

                //User Portal Resume
                G::Obj('Sovren')->processUserPortalResume($OrgID, $UpUserID, $_REQUEST['RequestID'], '', $filename);
            }
            
            if(isset($MultiOrgID) && $MultiOrgID != "") {
                header("Location:jobApplication.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID);
                exit;
            }
            else {
                header("Location:jobApplication.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID);
                exit;
            }
        }
    }
    else {        
        if(isset($MultiOrgID) && $MultiOrgID != "") {
            header("Location:uploadResume.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID."&msg=ue");
            exit;
        }
        else {
            header("Location:uploadResume.php?RequestID=".$_REQUEST['RequestID']."&OrgID=".$OrgID."&msg=ue");
            exit;
        }
    }
    
}


$page_title = "Upload Resume";

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
}

$LISTINGS_URL = strtolower ( USERPORTAL_HOME );

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';

//Get Name
$user_info_name =   G::Obj('UserPortalUsers')->getUserDetailInfoByUserID("FirstName, LastName", $UpUserID);

$req_info['Title']  =   '';
//Set condition
$where          =   array("OrgID = :OrgID", "RequestID = :RequestID");
//Set parameters
$params         =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST['RequestID']);
$req_result     =   G::Obj('Requisitions')->getRequisitionInformation("Title", $where, "", "", array($params));
$req_info       =   $req_result['results'][0];

$Name           =   $user_info_name["LastName"];
if($user_info_name["FirstName"]) {
    $Name   .= " " . $user_info_name["FirstName"];
}
    
echo "Welcome ".$Name.", please upload your resume to begin the ".$req_info['Title']." position:"; 
echo '</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';
?>
<form id="resumeparse" name="resumeparse" method="post" enctype="multipart/form-data" onsubmit="return validateFileUpload();">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <label for="resumefile" id="resumefile" <?php if(isset($_GET['msg']) && $_GET['msg'] == 'ue') echo 'style="background-color:yellow"';?>>
            Please upload your resume.
            <?php
            if($form_que_req['Required'] == "Y") {
            	?><span style="color:red">*</span><?php
            }
            ?>
            </label>
            <br>
            <input type="hidden" name="OrgID" value="<?php echo $OrgID;?>">
            <input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID;?>">
            <input type="hidden" name="RequestID" value="<?php echo $RequestID;?>">
            <div id="div_new_resume">
            <br>
            <input type="file" name="resumefile"><br><br>
            Please attach a Word or PDF document only. Image files can be uploaded in the attachment section (Max file size 5MB).<br>
            </div>
            <br><br>
            <input type="submit" class="btn btn-primary" name="btnParseResume" id="btnParseResume" value="Save/Next">
        </div>
    </div>
</form>

<form id="frmNext" name="frmNext" method="post">
    <input type="hidden" name="OrgID" value="<?php echo $OrgID;?>">
    <input type="hidden" name="MultiOrgID" value="<?php echo $MultiOrgID;?>">
    <input type="hidden" name="RequestID" value="<?php echo $RequestID;?>">
    <br><br>
    <?php 
    if($form_que_req['Required'] != "Y") {
        ?>
        Or go to next page.
        <input type="submit" class="btn btn-primary" name="btnRedirectToNextPage" id="btnRedirectToNextPage" value="Next">
        <?php
    }
    ?>
</form>

<script type="text/javascript">
function validateFileUpload() {
	var resume_file_val    =   document.forms['resumeparse'].resumefile.value;
	var resumeupload       =   document.getElementsByName('resumeupload');
	var resumeupload_value =   '';
	
	for(var i = 0; i < resumeupload.length; i++) {
	    if(resumeupload[i].checked) {
	    	resumeupload_value = resumeupload[i].value;
	    }
	}

	if(resume_file_val == '' && resumeupload_value == '') {
	    alert("Please upload resume.");
	    return false;
	}

	return true;
}
</script>
<?php
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>
