<?php
require_once 'userportal.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

$FormID                 =   $_REQUEST['FormID'];
$HoldID                 =   $OrgID.$UpUserID.$_REQUEST['RequestID'];

$requisition_info       =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $OrgID, $_REQUEST['RequestID']);
$processed_sections     =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");

if($processed_sections  == NULL 
	|| $processed_sections == "" 
	|| empty($processed_sections) 
	|| !isset($processed_sections)) {
    $processed_sections =   array();
}
else {
    $processed_sections =   unserialize($processed_sections);
}


//Get UserApplications
$user_apps_info         =   G::Obj('UserPortalInfo')->getUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID']);
$user_applications      =   $user_apps_info['results'];
$user_applications_cnt  =   $user_apps_info['count'];

if($user_applications_cnt > 0) {
    if($user_applications[0]['Status'] == 'Finished') {
        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
}

//Insert User Application Process
G::Obj('UserPortalInfo')->insUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], '', $requisition_info['Title'], "Pending");

require_once USERPORTAL_DIR . 'ValidateRequiredFields.inc';

if($_REQUEST['SectionID'] == '6') {
    if(count($errors_list) == 0) {
        require_once USERPORTAL_DIR . 'ProcessApplicationAttachments.inc';

        if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
            $processed_sections[]   =   $_REQUEST['SectionID'];
            
            // Bind Parameters
            $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ProcessedSections", "Question"=>"Processed Sections", "Answer"=>serialize($processed_sections));
            //Insert temporary data
            G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
        }
        
        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
    else {
        echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
        exit;
    }
}
else if($_REQUEST['SectionID'] == '14') {

    if ($_FILES ['applicant_profile_picture'] ['tmp_name'] != "" && count($errors_list) == 0) {
    
        // FILEHANDLING
        $dir = IRECRUIT_DIR . 'vault/' . $OrgID;
    
        if (! file_exists ( $dir )) {
            mkdir ( $dir, 0700 );
            chmod ( $dir, 0777 );
        }
    
        $app_picture_dir = $dir . '/applicant_picture';
    
        if (! file_exists ( $app_picture_dir )) {
            mkdir ( $app_picture_dir, 0700 );
            chmod ( $app_picture_dir, 0777 );
        }
    
        if ($_FILES ['applicant_profile_picture'] ['name'] != "") {
            	
            $data       =   file_get_contents ( $_FILES ['applicant_profile_picture'] ['tmp_name'] );
            $e          =   explode ( '.', $_FILES ['applicant_profile_picture'] ['name'] );
            $ecnt       =   count ( $e ) - 1;
            $ext        =   $e [$ecnt];
            $ext        =   preg_replace ( "/\s/i", '', $ext );
            $ext        =   substr ( $ext, 0, 5 );
    
            $filename   =   $app_picture_dir . '/' . $UpUserID . "*" . $_REQUEST['RequestID'] . '-ApplicantPicture.' . $ext;
            $fh         =   fopen ( $filename, 'w' );
            fwrite ( $fh, $data );
            fclose ( $fh );
    
            chmod ( $filename, 0666 );
    
            $ProfileAnswer = $UpUserID . "*" . $_REQUEST['RequestID'] . '-ApplicantPicture.'.$ext;
            
            if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
                $processed_sections[]   =   $_REQUEST['SectionID'];
            
                // Bind Parameters
                $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ProcessedSections", "Question"=>"Processed Sections", "Answer"=>serialize($processed_sections));
                //Insert temporary data
                $UserPortalInfoObj->insApplicationFormInfo($QI);
            }
            
            // Bind Parameters
            $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "SectionID"=>$_REQUEST['SectionID'], "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ApplicantPicture", "Answer"=>$ProfileAnswer);
            //Insert temporary data
            G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);

            if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
                $processed_sections[]   =   $_REQUEST['SectionID'];
            
                // Bind Parameters
                $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ProcessedSections", "Question"=>"Processed Sections", "Answer"=>serialize($processed_sections));
                //Insert temporary data
                G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
            }
            
            echo json_encode(array("errors_list"=>array(), "success"=>"true"));
            exit;
        }
    }
    else if ($_FILES ['applicant_profile_picture'] ['tmp_name'] == "" && count($errors_list) == 0) {
        
        if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
            $processed_sections[]   =   $_REQUEST['SectionID'];
        
            // Bind Parameters
            $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ProcessedSections", "Question"=>"Processed Sections", "Answer"=>serialize($processed_sections));
            //Insert temporary data
            G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
        }
        
        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
    else {
        echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
        exit;
    }
}
else {
    
    if(count($errors_list) == 0) {

        /**
         * This is a temporary code until the final upgrade.
         * Upto that time, we have to keep on adding different conditions to it.
         */
        $GetFormPostAnswerObj->POST =   $_POST;    //Set Post Data

        $json_form_que_list    =   G::Obj('ApplicationFormQuestions')->getFormQuestionsList($OrgID, $FormID, $_REQUEST['SectionID']);
        $form_que_list         =   $json_form_que_list['json_ques'];
        
        $ques_to_skip          =   array();
        foreach($form_que_list as $QuestionID=>$QI) {
            G::Obj('GetFormPostAnswer')->QueInfo    =   $QI;

            $QI['UserID']          =   $UpUserID;
            $QI['RequestID']       =   $_REQUEST['RequestID'];
            $QI['MultiOrgID']      =   $MultiOrgID;
            $QI['Answer']          =   call_user_func( array( G::Obj('GetFormPostAnswer'), 'getQuestionAnswer'.$QI['QuestionTypeID'] ), $QI['QuestionID']);
            $QI['ApplicationID']   =   "";
            
            G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
        }
        
        //Insert Other Questions To TempData
        $other_ques_list        =   array(
                                        'FormID'    =>  array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"FormID", "Answer"=>$FormID),
                                        'RequestID' =>  array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"RequestID", "Answer"=>$_REQUEST['RequestID']),
                                        'signature' =>  array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "SectionID"=>"SIGNATURE", "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"signature", "Answer"=>$_REQUEST['signature']),
                                        'agree'     =>  array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "SectionID"=>"SIGNATURE", "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"agree", "Answer"=>$_REQUEST['agree']),
                                    );
        //Insert Other Questions
        foreach ($other_ques_list as $other_que_id=>$QI) {
            if(isset($_REQUEST[$other_que_id])) {
                //Insert temporary data
                G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
            }
        }
        
        if(!in_array($_REQUEST['SectionID'], $processed_sections)) {
            $processed_sections[]   =   $_REQUEST['SectionID'];
            
            // Bind Parameters
            $QI = array("OrgID"=>$OrgID, "MultiOrgID"=>$MultiOrgID, "UserID"=>$UpUserID, "RequestID"=>$_REQUEST['RequestID'], "QuestionID"=>"ProcessedSections", "Answer"=>serialize($processed_sections));
            //Insert temporary data
            G::Obj('UserPortalInfo')->insApplicationFormInfo($QI);
        }
    
        $processed_sections     =   G::Obj('UserPortalInfo')->getApplicationQuestionInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], "ProcessedSections");
        $un_processed_sections  =   unserialize($processed_sections);
        
        $user_sec_list          =   array();
        for($ui = 0; $ui < count($userportal_sections_list); $ui++) {
            $section_id         =   $userportal_sections_list[$ui]['SectionID'];
            $user_sec_list[]    =   $section_id;
        }
        
        echo json_encode(array("errors_list"=>array(), "success"=>"true"));
        exit;
    }
    else {
        echo json_encode(array("errors_list"=>$errors_list, "success"=>"false"));
        exit;
    }	
}
?>
