<?php
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

$file_names             =   array();
$attachments_info       =   $UserPortalInfoObj->getApplicantAttachments($OrgID, $_REQUEST['MultiOrgID'], $UpUserID, $_REQUEST['RequestID']);
$FormID                 =   $ApplicantDetailsObj->getFormID($OrgID, $_REQUEST['ApplicationID'], $_REQUEST['RequestID']);
$purpose                =   $AttachmentsObj->getPurposeNamesList($OrgID, $FormID);

for($ka = 0; $ka < count($attachments_info); $ka++) {
    $file_names[$attachments_info[$ka]['TypeAttachment']] = IRECRUIT_DIR . 'vault/'. $OrgID . '/applicantattachments/'. $UpUserID . "*" . $_REQUEST['RequestID'] . '-' . $purpose [$attachments_info[$ka]['TypeAttachment']] . '.' . $attachments_info[$ka]['FileType'];
}

$dfile                  =   is_null($file_names[$_REQUEST['attachment_type']]) ? '' : $file_names[$_REQUEST['attachment_type']];

if (file_exists ( $dfile )) {
	
	header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($dfile));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($dfile));
    readfile($dfile);
    exit;
}
?>