<?php
require_once 'userportal.inc';

$page_title = "Wotc Form";

require_once USERPORTAL_DIR . 'Authenticate.inc';

if(isset($_REQUEST['IrecruitApplicationID']) && $_REQUEST['IrecruitApplicationID'] != "") $IrecruitApplicationID = $_REQUEST['IrecruitApplicationID'];
if(isset($_REQUEST['wotcID']) && $_REQUEST['wotcID'] != "") $WOTCID = $_REQUEST['wotcID'];
if(isset($_REQUEST['IrecruitRequestID']) && $_REQUEST['IrecruitRequestID'] != "") $IrecruitRequestID = $_REQUEST['IrecruitRequestID'];

$PARENT = G::Obj('WOTCcrm')->getParentInfo($WOTCID);
$WotcFormID     =   $PARENT['WotcFormID'];

if(isset($_POST['btnSubmit']) && $_POST['btnSubmit'] == "Submit Form") {

	$APPDATA    =   $_POST;
	G::Obj('WOTCValidateForm')->FORMDATA['REQUEST'] =   $_REQUEST;
	G::Obj('ApplicationForm')->APPDATA              =   $_POST;

	$ERRORS	= G::Obj('WOTCValidateForm')->validateWotcForm("MASTER", $WotcFormID); //Replace it with WotcID

	if($_POST['captcha'] != $_SESSION['U']['captcha']) {
		$ERRORS['captcha']  =   "Captcha";
	}
	
	if(count($ERRORS) == 0) {
		// Check duplicate entry
		$rtn_duplicate_info	=	G::Obj('WOTCIrecruitApplications')->validateWotcApplicationDuplicate();

		if($rtn_duplicate_info == "") {
			
			require_once WOTC_DIR . 'forms/Process.inc';
			
			//Insert wotc application header information
			$info	=	array(
							"OrgID"					=>	$OrgID,
							"MultiOrgID"			=>	$MultiOrgID,
							"WotcFormID"			=>	$WotcFormID,
							"wotcID"				=>	$WOTCID,
							"UserID"				=>	$UpUserID,
							"IrecruitApplicationID"	=>	$IrecruitApplicationID,	
							"IrecruitRequestID"		=>	$IrecruitRequestID,
							"ApplicationID"			=>	$ApplicationID,
							"CreatedDateTime"		=>	"NOW()",
							"Source"				=>	"USERPORTAL"
						);
			G::Obj('WOTCIrecruitApplications')->insWotcApplicationInfo($info);
			
			echo "<div class=\"row\">\n";
			echo "<div class=\"col-lg-12 col-md-12 col-sm-12\">\n";
			
			//Process thank you. call center
			echo G::Obj('WOTCApp')->processThankYou($WOTCID, $ApplicationID);
			
			$wotc_app_details = G::Obj('WOTCIrecruitApplications')->getWotcApplicationByApplicationID("*", $WOTCID, $IrecruitApplicationID, $IrecruitRequestID, $ApplicationID);
			
			$wotc_qualifies	=	G::Obj('WOTCApp')->getApplicationInfo($WOTCID, $ApplicationID);
			
			$set_info	=	array("Qualifies = :Qualifies");
			$where_info	=	array(
									"OrgID						=	:OrgID",
									"WotcFormID					=	:WotcFormID",
									"wotcID						=	:wotcID",
									"IrecruitApplicationID		=	:IrecruitApplicationID",
									"IrecruitRequestID			=	:IrecruitRequestID",
									"ApplicationID				=	:ApplicationID"
							);
			$params		=	array(
									":Qualifies"				=>	$wotc_qualifies['Qualifies'],
									":OrgID"					=>	$OrgID,
									":WotcFormID"				=>	$wotc_app_details['WotcFormID'],
									":wotcID"					=>	$wotc_app_details['wotcID'],
									":IrecruitApplicationID"	=>	$wotc_app_details['IrecruitApplicationID'],
									":IrecruitRequestID"		=>	$wotc_app_details['IrecruitRequestID'],
									":ApplicationID"			=>	$wotc_app_details['ApplicationID']
							);
			
			//Update wotc application qualifies information
			G::Obj('WOTCIrecruitApplications')->updWotcApplicationInfo($set_info, $where_info, array($params));
			
			echo "</div>\n";
			echo "</div>\n";
				
		}
	}
}
?>
