<?php
require_once 'userportal.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

$ApplicationID  =   $_REQUEST['ApplicationID'];
$RequestID      =   $_REQUEST['RequestID'];
$OrgID      =   $_REQUEST['OrgID'];
$get_checklist_header =   G::Obj('ChecklistData')->getCheckListHeader($OrgID, $ApplicationID, $RequestID);
$application_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("ApplicantSortName", $OrgID, $ApplicationID, $RequestID);

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';
echo 'Application Form: '.$ApplicationID. ' - ' . $application_info['ApplicantSortName'];

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'index.php?navpg=profiles&navsubpg=status">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Check My Status';
echo '</a>';
echo '</span>';

echo '</h3>';

echo '</div>';
echo '</div>';

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

echo G::Obj('ChecklistData')->getChecklistView($OrgID, $ApplicationID, $RequestID, $get_checklist_header['ChecklistID']);
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
