<?php
//Get Prescreen Text Information
$where      =   array("OrgID = :OrgID", "Rejection != ''");
//Get PrescreenText Information
$PRESCREENT =   G::Obj('PrescreenQuestions')->getPrescreenTextInfo($OrgID, $where, "OrgID LIMIT 1");

$ReqMultiOrgID      =   G::Obj('RequisitionDetails')->getMultiOrgID($OrgID, $RequestID);

//Is Internal Requisition or Not
if ($feature ['InternalRequisitions'] == "Y") {
	$InternalCode = G::Obj('InternalRequisitionsSettings')->getInternalCodeByOrgID ( $OrgID );
}

//Set parameters for prepared query
$params				=	array(":OrgID"=>$OrgID, ":RequestID"=>$RequestID);
//Set condition
$where   			=	array("OrgID = :OrgID", "RequestID = :RequestID");

if (($feature ['InternalRequisitions'] == "Y") && ($_REQUEST['InternalVCode'] == $InternalCode)) {
	$where[] = "Internal IN ('Y','B')";
} else {
	$where[] = "Internal IN ('N','B','')";
}

//Get Prescreen Screen Question Information
$pre_screen_que_info    =   G::Obj('PrescreenQuestions')->getPrescreenQuestionsInformation ("*", $where, "SortOrder", array($params));
$questionsavail         =   $pre_screen_que_info ["count"];

$prescreen_form         =   "";
if (isset($PRESCREENT['OrgID']) && ($PRESCREENT['OrgID'] != "") && ($questionsavail > 0)) {
	
    $REQ                =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, InternalFormID, FormID", $OrgID, $RequestID);
	
	$prescreen_form .= '<p>Application for <b>' . $REQ ['Title'] . '</b>.';
	$prescreen_form .= '<br><br>';
	
	$prescreen_form .= $PRESCREENT ['Intro'] . "\n";
	$prescreen_form .= '<table border="0" cellspacing="0" cellpadding="3">';
	
	if (is_array ( $pre_screen_que_info ["results"] )) {
		foreach ( $pre_screen_que_info ["results"] as $PreScreenKey => $PRESCREEN ) {
			$prescreen_form .= '<tr><td width="400">' . $PRESCREEN ['Question'] . '?';
			$prescreen_form .= '</td><td width="30"></td><td width="120" valign="top">';
			$prescreen_form .= '<input type="radio" name="' . $PRESCREEN ['SortOrder'] . '" value="Y"';
			if($prescreen_answers[$PRESCREEN['SortOrder']] == "Y") {
				$prescreen_form .= ' checked="checked"';
			}
			$prescreen_form .= '>&nbsp;Yes';
			$prescreen_form .= '&nbsp;&nbsp;';
			$prescreen_form .= '<input type="radio" name="' . $PRESCREEN ['SortOrder'] . '" value="N"';
			if($prescreen_answers[$PRESCREEN['SortOrder']] == "N") {
			    $prescreen_form .= ' checked="checked"';
			}
			$prescreen_form .= '>&nbsp;No';
			$prescreen_form .= '</td></tr>' . "\n";
		}
	}
	
	$prescreen_form .= "<br>\n";
	
	$prescreen_form .= '</table>';
	$prescreen_form .= '<br>';
	$prescreen_form .= $PRESCREENT ['Disclaimer'] . "<br><br>\n";
	$prescreen_form .= '<input type="hidden" name="OrgID" value="' . $OrgID . '">' . "\n";
	if ($MultiOrgID) {
		$prescreen_form .= '<input type="hidden" name="MultiOrgID" id="MultiOrgID" value="' . $MultiOrgID . '">' . "\n";
	}

	return $prescreen_form;
}
?>
