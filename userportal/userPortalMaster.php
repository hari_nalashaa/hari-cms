<?php
require_once 'userportal.inc';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}


if(isset($_REQUEST['users-search-box']) && $_REQUEST['users-search-box'] != "") {
    $_SESSION['U']['UserIDMasterValue'] = $_REQUEST['users-search-box'];
    header("Location:userPortalMaster.php?msg=succ");
    exit;
}

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'FilterUserPortalSections.inc';
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Set Master Access:</h3>';
echo '</div>';
echo '</div>';

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';
?>

<form name="frmUserportalMasterAccess" id="frmUserportalMasterAccess" method="get">
    <div style="min-height:500px">
        <?php 
            if(isset($_GET['msg']) && $_GET['msg'] == 'succ') {
                echo '<span style="color:blue">User level updated successfully.</span><br><br>';
            }
        ?>
        Users List:
        <input type="text" id="users-search-box" name="users-search-box" placeholder="Enter User Email" style="width:400px"/>
        <br>
        <div id="suggesstion-box"></div>
    </div>
    <div>
        <a href="userPortalMaster.php?clear_user=Y">Clear User</a>
    </div>
</form>

<?php
echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>
