<?php
require_once 'userportal.inc';
if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Authenticate.inc';
}

$page_title =   "Already Submitted";

$req_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title, FormID, MultiOrgID", $_REQUEST['OrgID'], $_REQUEST['RequestID']);

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
}

$LISTINGS_URL = strtolower ( USERPORTAL_HOME );

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Header.inc';
    require_once USERPORTAL_DIR . 'Navigation.inc';
}

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Already Submitted</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

echo '<br><br><br><br>';
echo '<h3 style="text-align:center">';
echo 'You have already submitted the application for this requisition.';
echo '<br>';
echo '</h3>';

if(isset($req_info['MultiOrgID']) && $req_info['MultiOrgID'] != "") {
    ?><h5 style="text-align:center"><a href="<?php echo USERPORTAL_HOME . "index.php?OrgID=".$_REQUEST['OrgID']."&MultiOrgID=".$req_info['MultiOrgID']."&navpg=profiles&navsubpg=status"; ?>">Click here to view the application</a></h5><?php
}
else {
   ?><h5 style="text-align:center"><a href="<?php echo USERPORTAL_HOME . "index.php?OrgID=".$_REQUEST['OrgID']."&navpg=profiles&navsubpg=status"; ?>">Click here to view the application</a></h5><?php
}

echo '<h5 style="text-align:center">';
echo $req_info['Title'];
echo '</h5>';
echo '<br><br><br><br><br>';

echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';

if (isset($whitelist) && in_array(USERPORTAL_DIR, $whitelist) && defined('USERPORTAL_DIR')) {
    require_once USERPORTAL_DIR . 'Footer.inc';
}
?>