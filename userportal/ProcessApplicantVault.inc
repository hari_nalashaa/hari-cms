<?php
if (($_REQUEST ['process'] == "New") || ($_REQUEST ['process'] == "Replace") || ($_GET ['delete']) || ($_GET ['replace'])) {

    $dir = IRECRUIT_DIR . 'vault/' . $OrgID;

    if (! file_exists ( $dir )) {
        mkdir ( $dir, 0700 );
        chmod ( $dir, 0777 );
    }

    $appvaultdir = $dir . '/applicantvault';

    if (! file_exists ( $appvaultdir )) {
        mkdir ( $appvaultdir, 0700 );
        chmod ( $appvaultdir, 0777 );
    }
}


if (($process == "Replace") && ($data != "")) {

    // Bind parameters
    $params = array (
        ':OrgID'         =>  $OrgID,
        ':ApplicationID' =>  $ApplicationID,
        ':UpdateID'      =>  $UpdateID
    );

    // Set condition
    $where = array (
        "OrgID           =   :OrgID",
        "ApplicationID   =   :ApplicationID",
        "UpdateID        =   :UpdateID"
    );

    // Get Applicant Vault Information
    $results = G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
    $AV = $results ['results'] [0];

    // Bind parameters
    $params = array (
        ":FileName"      =>  $FileName,
        ":FileType"      =>  $FileType,
        ":UserID"        =>  $UpUserID,
        ":OrgID"         =>  $OrgID,
        ":ApplicationID" =>  $ApplicationID,
        ":RequestID"     =>  $RequestID,
        "UpdateID"       =>  $UpdateID
    );
    // Update Applicant Vault
    G::Obj('ApplicantVault')->updApplicantVault ( array ($params) );

    // Get DateTime
    $HLD = $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );

    $filenameA = $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '.' . $AV ['FileType'];
    $filenameB = $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '-' . $HLD . 'R.' . $AV ['FileType'];

    $Comments = "Replaced File in Vault: " . $AV ['FileName'] . '.' . $AV ['FileType'] . ' with ' . $FileName . '.' . $FileType . "<br>";
    $Comments .= '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
    $Comments .= '<a href="' . IRECRUIT_HOME . 'applicants/display_applicantVault_File.php?OrgID=' . $OrgID . '&File=' . $filenameB . '">Download</a> previous version.';

    // Job Application History
    $job_app_history = array (
        "OrgID"                 =>  $OrgID,
        "ApplicationID"         =>  $ApplicationID,
        "RequestID"             =>  $RequestID,
        "ProcessOrder"          =>  "-3",
        "StatusEffectiveDate"   =>  "DATE(NOW())",
        "Date"                  =>  "NOW()",
        "UserID"                =>  $UpUserID,
        "Comments"              =>  $Comments
    );

    // Insert Job Application History
    G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );

    $command = 'mv ' . $appvaultdir . '/' . $filenameA . ' ' . $appvaultdir . '/' . $filenameB;
    system ( $command );

    $filename = $appvaultdir . '/' . $ApplicationID . '-' . $UpdateID . '-' . $FileName . '.' . $FileType;
    $fh = fopen ( $filename, 'w' );
    fwrite ( $fh, $data );
    fclose ( $fh );

    chmod ( $filename, 0644 );

    $MESSAGE = $AV ['FileName'] . '.' . $AV ['FileType'] . " replaced with: " . $FileName . '.' . $FileType;
} // end process Replace


if ($_GET ['delete']) {

    // Bind parameters
    $params     =   array (':OrgID'=>$OrgID, ':ApplicationID'=>$ApplicationID, ':UpdateID'=>$_GET ['delete']);
    // Set condition
    $where      =   array ("OrgID = :OrgID", "ApplicationID = :ApplicationID", "UpdateID = :UpdateID");
    // Get Applicant Vault Information
    $results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
    $AV         =   $results ['results'] [0];

    // Get Date Time
    $HLD        =   $MysqlHelperObj->getDateTime ( '%Y%m%d%H%m%s' );

    $filenameA  =   $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '.' . $AV ['FileType'];
    $filenameB  =   $ApplicationID . '-' . $AV ['UpdateID'] . '-' . $AV ['FileName'] . '-' . $HLD . 'D.' . $AV ['FileType'];

    $Comments   =   "Deleted File from Vault: " . $AV ['FileName'] . '.' . $AV ['FileType'] . "<br>";
    $Comments   .=  '<img src="' . IRECRUIT_HOME . 'images/icons/page_white_magnify.png" border="0" title="View" style="margin:0px 3px -4px 0px;">';
    $Comments   .=  '<a href="' . IRECRUIT_HOME . 'applicants/display_applicantVault_File.php?OrgID=' . $OrgID . '&File=' . $filenameB . '">Download</a> previous version.';

    // Job Application History
    $job_app_history = array (
        "OrgID"                  =>  $OrgID,
        "ApplicationID"          =>  $ApplicationID,
        "RequestID"              =>  $AV ['RequestID'],
        "ProcessOrder"           =>  "-3",
        "StatusEffectiveDate"    =>  "DATE(NOW())",
        "Date"                   =>  "NOW()",
        "UserID"                 =>  $UpUserID,
        "Comments"               =>  $Comments
    );

    // Insert Job Application History
    G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );

    $command = 'mv ' . $appvaultdir . '/' . $filenameA . ' ' . $appvaultdir . '/' . $filenameB;
    system ( $command );

    // Bind parameters
    $params = array (
        ':OrgID'                 =>  $OrgID,
        ':ApplicationID'         =>  $ApplicationID,
        ':UpdateID'              =>  $_GET ['delete']
    );
    // Set where condition
    $where = array (
        "OrgID                   =   :OrgID",
        "ApplicationID           =   :ApplicationID",
        "UpdateID                =   :UpdateID"
    );
    // Delete Applicant Vault Information
    G::Obj('ApplicantVault')->delApplicantVault ( $where, array ($params) );

    $MESSAGE = "File deleted: " . $AV ['FileName'] . '.' . $AV ['FileType'];
} // end GET[delete]

if ($_GET ['edit']) {

    // Bind the parameters
    $params     =   array(
                        ":OrgID" => $OrgID,
                        ":ApplicationID" => $ApplicationID,
                        ":UpdateID" => $_GET ['edit']
                    );
			    // Set the condition
	$where     =   array(
                        "OrgID = :OrgID",
                        "ApplicationID = :ApplicationID",
                        "UpdateID = :UpdateID"
                    );
	//Get Applicant Vault Information
	$results   =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( "*", $where, '', array ($params) );
    $AV        =   $results ['results'] [0];

	$typefile  =   "Replace File: <font style=\"color:red;\">" . $AV ['FileName'] . '.' . $AV ['FileType'] . "</fon>";
	$submit    =   "Replace File";
	$add       =   ' <a href="' . IRECRUIT_HOME . 'applicants/applicantVault.php?ApplicationID=' . $ApplicationID . '&RequestID=' . $RequestID . '"><img src="' . IRECRUIT_HOME . 'images/icons/add.png" border="0" title="Add" style="margin:0px 3px -4px 0px;">Add File</a>';
	$process   =   "Replace";
	$UpdateID  =   $AV ['UpdateID'];

} else {

    $typefile  =   "Upload File:";
    $submit    =   "Upload File";
    $add       =   "";
    $process   =   "New";
	$UpdateID  =   "";

} // end GET[edit]

if ($_FILES ['newfile'] ['type'] != "") {

    $e         =   explode ( '.', $_FILES ['newfile'] ['name'] );
    $ecnt      =   count ( $e ) - 1;
    $FileType  =   $e [$ecnt];
    $FileType  =   preg_replace ( "/\s/i", '', $FileType );
    $FileName  =   $e [$ecnt - 1];
    $FileName  =   preg_replace ( "/\s/i", '_', $FileName );
    $FileName  =   preg_replace ( "/,/i", '', $FileName );
	$FileName  =   preg_replace ( "/'/i", '', $FileName );
	$data      =   file_get_contents ( $_FILES ['newfile'] ['tmp_name'] );

} // end if FILES



if (($_REQUEST ['process'] == "New") && ($data != "")) {
    
    // Bind parameters for Applicant Vault Data
    $applicant_vault_info = array (
        "OrgID"          =>  $OrgID,
        "ApplicationID"  =>  $ApplicationID,
        "RequestID"      =>  $RequestID,
        "FileName"       =>  $FileName,
        "FileType"       =>  $FileType,
        "Date"           =>  "NOW()",
        "UpdateID"       =>  '',
        "UserID"         =>  $UpUserID
    );
    
    // Insert Applicant Vault Information
    G::Obj('ApplicantVault')->insApplicantVault ( $applicant_vault_info );

    // Get Applicant Vault Information
    $results    =   G::Obj('ApplicantVault')->getApplicantVaultInfo ( 'LAST_INSERT_ID() AS last_insert_id', array (), 'OrgID LIMIT 1', array () );
    $AVIR       =   $results ['results'] [0] ['last_insert_id'];

    $Comments   =   "Added File to Vault: " . $FileName . '.' . $FileType;

    // Job Application History
    $job_app_history = array (
        "OrgID"          =>  $OrgID,
        "ApplicationID"  =>  $ApplicationID,
        "RequestID"      =>  $RequestID,
        "ProcessOrder"   =>  "-3",
        "Date"           =>  "NOW()",
        "UserID"         =>  $UpUserID,
        "Comments"       =>  $Comments,
        "StatusEffectiveDate" => "DATE(NOW())"
    );

    // Insert Job Application History
    G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history );

    $filename = $appvaultdir . '/' . $ApplicationID . '-' . $AVIR . '-' . $FileName . '.' . $FileType;
    $fh = fopen ( $filename, 'w' );
    fwrite ( $fh, $data );
    fclose ( $fh );

    chmod ( $filename, 0644 );

    $MESSAGE = "File Added: " . $FileName . '.' . $FileType;
} // end process new
?>