<?php
require_once 'userportal.inc';

//First file doesn't require for now, but just keep it.
include IRECRUIT_DIR . 'linkedin/LinkedInSessionCheck.inc';
include USERPORTAL_DIR . 'JobApplicationRequestVars.inc';

$purpose = array (
    'resumeupload'        =>  'resume',
    'coverletterupload'   =>  'coverletter',
    'otherupload'         =>  'other'
);

require_once USERPORTAL_DIR . 'Authenticate.inc';

if ($_POST ['QuestionID'] != "" && $_POST['RequestID'] != "" && $_POST['QuestionID'] != "" && $UpUserID != "") {

    $udir       =   IRECRUIT_DIR . 'vault/' . $OrgID . '/applicantattachments/';
    $ufile      =   $udir . $_POST ['QuestionID'];

    $att_info   =   $UserPortalInfoObj->getApplicantAttachmentInfo($OrgID, $MultiOrgID, $_POST['RequestID'], $_POST['QuestionID'], $UpUserID);
    $ext        =   $att_info['FileType'];

    $filename   =   $udir . $UpUserID . "*" . $_POST['RequestID'] . '-' . $purpose [$_POST['QuestionID']] . '.' . $ext;
    @unlink($filename); //Supress error

    $der_res    =   $UserPortalInfoObj->delApplicantAttachmentsTemp($OrgID, $MultiOrgID, $_POST['RequestID'], $_POST ['QuestionID'], $UpUserID);

    if($der_res['affected_rows'] > 0) {
        
        echo json_encode(array("Status"=>"Success"));
        exit;
    }
    else {
        echo json_encode(array("Status"=>"Failed"));
        exit;
    }
} // end if
?>
