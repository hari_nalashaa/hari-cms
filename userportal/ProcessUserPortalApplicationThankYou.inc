<?php
$_SESSION['U']['SignatureRandID'] = time().microtime();

$app_process_info   =   G::Obj('UserPortalInfo')->getApplicationInformation($OrgID, $MultiOrgID, $UpUserID, $_REQUEST ['RequestID']);
$app_process_list   =   $app_process_info['results'];

$APPPROCESSDATA     =   array();
for($a = 0; $a < count($app_process_list); $a++) {
	$APPPROCESSDATA[$app_process_list[$a]['QuestionID']] = $app_process_list[$a]['Answer'];
}

$first              =   $APPPROCESSDATA['first'];
$last               =   $APPPROCESSDATA['last'];
$address            =   $APPPROCESSDATA['address'];
$city               =   $APPPROCESSDATA['city'];
$zip                =   $APPPROCESSDATA['zip'];
$email              =   $APPPROCESSDATA['email'];

// Check duplicate entry
require_once USERPORTAL_DIR . 'CheckDuplicate.inc';

if ($DUPERROR) {
	if(isset($MultiOrgID) && $MultiOrgID != "") {
		header("Location:thankyou.php?msg=duplicate&RequestID=".$_REQUEST ['RequestID']."&OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID);
		exit;
	}
	else {
		header("Location:thankyou.php?msg=duplicate&RequestID=".$_REQUEST ['RequestID']."&OrgID=".$OrgID);
		exit;
	}
} // end DUPERROR


$req_details_info   =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Active, DATE(ExpireDate) as ExpireDate", $OrgID, $_REQUEST ['RequestID']);
$dates_diff         =   G::Obj('MysqlHelper')->getDateDiffWithNow($req_details_info['ExpireDate']);

if($req_details_info['Active'] == 'N'
	|| $dates_diff > 0) {
	header("Location:thankyou.php?RequestID=".$_REQUEST ['RequestID']."&OrgID=".$OrgID.'&msg=reqexpired');
	exit;
}

// lock for a duplicate submission
// Data to insert, here keys are table column names
$ins_app_lock_info  =   array(
                    		"EntryDate"           =>  "NOW()",
                    		"FirstName"           =>  $APPPROCESSDATA['first'],
                    		"LastName"            =>  $APPPROCESSDATA['last'],
                    		"Address"             =>  $APPPROCESSDATA['address'],
                    		"City"                =>  $APPPROCESSDATA['city'],
                    		"Zip"                 =>  $APPPROCESSDATA['zip'],
                    		"Email"               =>  $APPPROCESSDATA['email'],
                    		"SelectedPosition"    =>  $_REQUEST ['RequestID']
                        );
// lock for a duplicate submission
G::Obj('Applicants')->insApplicantLock ( $ins_app_lock_info );

// Generate ApplicationID
$ApplicationID  =   G::Obj('Applications')->getApplicationID ($OrgID);

//Copy data from temporary table to ApplicantData table
G::Obj('UserPortalInfo')->insApplicantData($OrgID, $MultiOrgID, $UpUserID, $_REQUEST ['RequestID'], $ApplicationID);
G::Obj('UserPortalInfo')->insApplicantAttachments($OrgID, $MultiOrgID, $UpUserID, $_REQUEST ['RequestID'], $ApplicationID);

if($feature['ResumeParsing'] == "Y") {
	//User Portal Resume
    G::Obj('Sovren')->insUserPortalParsingInfo($OrgID, $UpUserID, $_REQUEST ['RequestID'], $ApplicationID);
}

$LeadID         =   G::Obj('ApplicationLeads')->getLeadIDFromFormID($OrgID, $req_info['FormID']);
$LeadStatus     =   ($LeadID == "") ? "" : "N";

// Insert data into DB (JobApplications)
$job_app_info = array (
		"OrgID"                 =>  $OrgID,
		"MultiOrgID"            =>  $MultiOrgID,
		"ApplicationID"         =>  $ApplicationID,
		"RequestID"             =>  $_REQUEST ['RequestID'],
		"ApplicantSortName"     =>  $APPDATA['last'] . ', ' . $APPDATA['first'],
		"Distinction"           =>  'P',
		"EntryDate"             =>  "NOW()",
		"LastModified"          =>  "NOW()",
		"ProcessOrder"          =>  "1",
		"StatusEffectiveDate"   =>  "DATE(NOW())",
		"ReceivedDate"          =>  "NOW()",
		"FormID"                =>  $req_info['FormID'],
        "Informed"              =>  $APPPROCESSDATA['informed'],
		"RequisitionStatus"     =>  $req_info['Active'],
		"LeadGenerator"         =>  $LeadID,
		"LeadStatus"            =>  $LeadStatus
);
G::Obj('Applications')->insJobApplication ( $job_app_info );

// Insert Applicant Status Logs Information
$ApplicantStatusLogsObj->insApplicantStatusLog($OrgID, $ApplicationID, $_REQUEST ['RequestID'], "1", "New Applicant");

// Insert into ApplicantHistory
$UpdateID = 'Applicant';
$Comments = 'Application submitted for: ' . $RequisitionDetailsObj->getReqJobIDs ( $OrgID, $MultiOrgID, $_REQUEST ['RequestID'] );

$job_application_history = array (
		"OrgID"                 =>  $OrgID,
		"ApplicationID"         =>  $ApplicationID,
		"RequestID"             =>  $_REQUEST ['RequestID'],
		"ProcessOrder"          =>  "1",
		"StatusEffectiveDate"   =>  "DATE(NOW())",
		"Date"                  =>  "NOW()",
		"UserID"                =>  $UpdateID,
		"Comments"              =>  $Comments
);
G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_application_history );

//Insert User Application Process
G::Obj('UserPortalInfo')->insUserApplications($OrgID, $MultiOrgID, $UpUserID, $_REQUEST['RequestID'], $ApplicationID, $req_info['Title'], "Finished");

if(DEVELOPMENT == 'N') {
	// Send Thank you email to Applicant
	$org_title_info = G::Obj('OrganizationDetails')->getOrganizationInformation($OrgID, $MultiOrgID, "OrganizationName");
	require_once COMMON_DIR . 'process/EmailThankYou.inc';
	sendThankYou ( $OrgID, $ApplicationID, $FormID, $org_title_info['OrganizationName'] );
}

//Set params
$params     =   array(":OrgID"=>$OrgID);
// Auto Forward
$where      =   array("OrgID = :OrgID", "Role IN ('', 'master_admin')");
//Get UserInformation
$results    =   G::Obj('IrecruitUsers')->getUserInformation("UserID", $where, "Role LIMIT 1", array($params));
//Admin
$user_id    =   $results['results'][0]['UserID'];

//Set params
$params     =   array(":OrgID"=>$OrgID, ":RequestID"=>$_REQUEST ['RequestID']);
//Set condition
$where      =   array("OrgID = :OrgID", "RequestID = :RequestID");
//Get Requisition Information Forward
$results    =   G::Obj('Requisitions')->getRequisitionForwardInfo("EmailAddress", $where, "", array($params));

if ($results['count'] > 0) {

	include IRECRUIT_DIR . 'applicants/EmailApplicant.inc';

	//Have to update it with forward applicant specs information
	$ATTS               =   array();
	//Have to update it with forward applicant specs information
	$req_forward_specs  =   G::Obj('RequisitionForwardSpecs')->getRequisitionForwardSpecsDetailInfo($OrgID, $_REQUEST ['RequestID']);
	$forward_specs_list =   json_decode($req_forward_specs ['ForwardSpecsList'], true);
	//Forward Specs List
	foreach($forward_specs_list as $spec_que_id) {
		$ATTS[$spec_que_id] =   'Y';
	}

	if (is_array ( $results ['results'] )) {
		$EmailList = array();
		$status_index = 0;
		foreach ( $results ['results'] as $EM ) {
			$Attachments = forwardApplicant ( $OrgID, $ApplicationID, $_REQUEST ['RequestID'], $EM ['EmailAddress'], $ATTS, 'Application Auto Forwarded', $user_id, 'processforward',  $status_index);
			$EmailList[] = $EM ['EmailAddress'];
			$status_index++;
		} // end foreach
	}

	updateApplicantStatus ( $OrgID, $ApplicationID, $_REQUEST ['RequestID'], 'Application', implode(", ", $EmailList), $Attachments, 'Application Auto Forwarded' );
} // end AutoForward

if(isset($MultiOrgID) && $MultiOrgID != "") {
	header("Location:thankyou.php?msg=suc&RequestID=".$_REQUEST['RequestID']."&ApplicationID=".$ApplicationID."&OrgID=".$OrgID."&MultiOrgID=".$MultiOrgID);
	exit;
}
else {
	header("Location:thankyou.php?msg=suc&RequestID=".$_REQUEST['RequestID']."&ApplicationID=".$ApplicationID."&OrgID=".$OrgID);
	exit;
}
?>
