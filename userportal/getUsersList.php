<?php 
require_once 'userportal.inc';
require_once USERPORTAL_DIR . 'Authenticate.inc';

$columns        =   "UserID, FirstName, LastName, Email";
$where          =   array(
                        "(Email LIKE :Email 
                          OR FirstName LIKE :FirstName 
                          OR LastName LIKE :LastName
                        )");
$params         =   array(
                        ":Email"        =>  "%".trim($_REQUEST['keyword'])."%",
                        ":FirstName"    =>  "%".trim($_REQUEST['keyword'])."%",
                        ":LastName"     =>  "%".trim($_REQUEST['keyword'])."%"
                    );
$users_results  =   $UserPortalUsersObj->getUsersInformation($columns, $where, "", " UserID LIMIT 10", array($params));
$users_list     =   $users_results['results'];

echo json_encode($users_list);
?>
