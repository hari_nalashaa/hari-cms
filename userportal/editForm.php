<?php
$title = 'View Internal Form';
require_once 'userportal.inc';

if(isset($_REQUEST['ApplicationID']) && $_REQUEST['ApplicationID'] != "") $ApplicationID = $_REQUEST['ApplicationID'];
if(isset($_REQUEST['RequestID']) && $_REQUEST['RequestID'] != "") $RequestID = $_REQUEST['RequestID'];

require_once USERPORTAL_DIR . 'Authenticate.inc';

if(isset($_POST['btnEditVetDisAA']) && $_POST['btnEditVetDisAA'] != "") {
    
    foreach ( $_POST as $question => $answer ) {

        // Default update information
        $on_update      =   ' ON DUPLICATE KEY UPDATE Answer = :UAnswer';
        // insert information
        $insert_info    =   array(
                                'OrgID'         => $OrgID,
                                'ApplicationID' => $ApplicationID,
                                'QuestionID'    => $question,
                                'Answer'        => $answer
                            );
        // update information
        $update_info    =   array(
                                ":UAnswer"      => $answer
                            );
        // Insert Update Applicant Data Temp
        G::Obj('Applicants')->insUpdApplicantData ( $insert_info, $on_update, $update_info );

    } // end foreach

    $status     =   'N';
    $type       =   $_REQUEST['typeform'];
    
    if($type == "veteran") {
        $set_info   =   array("VeteranEditStatus    =   :Status");
    }
    else if($type == "disabled") {
        $set_info   =   array("DisabledEditStatus   =   :Status");
    }
    else if($type == "aa") {
        $set_info   =   array("AAEditStatus         =   :Status");
    }
    
    if($type == 'veteran' || $type == 'aa' || $type == 'disabled') {
        $params     =   array(":Status"=>$status, ":OrgID"=>$OrgID, ":ApplicationID"=>$ApplicationID, ":RequestID"=>$RequestID);
        $where_info =   array("OrgID = :OrgID", "ApplicationID = :ApplicationID", "RequestID = :RequestID");
        G::Obj('Applications')->updApplicationsInfo("JobApplications", $set_info, $where_info, array($params));
    }
    
    header("Location: editForm.php?ApplicationID=".$ApplicationID."&RequestID=".$RequestID."&typeform=".$_REQUEST['typeform']."&msg=suc");
    exit;
}

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';

// if not a listings area for the new app process then navigate to these pages
require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';


echo '<div id="page-wrapper">';			//Page Wrapper Start
echo '<div class="page-container">';	//Page Container End

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">';

if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'veteran') {
    echo 'Veteran Form: ';    
}
else if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'aa') {
    echo 'Affirmative Action Form: ';
}
else if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'disabled') {
    echo 'Disabled Form: ';
}

echo $ApplicationID;

echo '<span style="float:right;font-size:13px;">';
echo '<a href="'.USERPORTAL_HOME.'index.php?navpg=profiles&navsubpg=status">';
echo '<img src="'.USERPORTAL_HOME.'images/arrow_undo.png">&nbsp;Back to Check My Status';
echo '</a>';
echo '</span>';

echo '</h3>';

echo '</div>';
echo '</div>';


echo '<div class="page-inner">';

//Row Start
echo '<div class="row">';

echo '<div class="col-lg-12" style="color:blue">';
if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == 'suc') {
	echo '<br><br><strong>Successfully Updated</strong><br><br>';
}
echo '</div>';

echo '</div>';

echo '<div class="row">';

echo '<div class="col-lg-12">';

echo '<form name="frmEditVetDisAA" id="frmEditVetDisAA" method="post">';

if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'veteran') {
    $app_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("ApplicantSortName", $OrgID, $ApplicationID, $RequestID);
    $applicant_name =   $app_info['ApplicantSortName'];
    $APPDATA        =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);
    $rtn_aa_dis_vet =   include_once COMMON_DIR . "application/Veteran.inc";
}
else if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'aa') {
    $app_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("ApplicantSortName", $OrgID, $ApplicationID, $RequestID);
    $applicant_name =   $app_info['ApplicantSortName'];
    $APPDATA        =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);
    $rtn_aa_dis_vet =   include_once COMMON_DIR . "application/AA.inc";
}
else if(isset($_REQUEST['typeform']) && $_REQUEST['typeform'] == 'disabled') {
    $app_info       =   G::Obj('Applications')->getJobApplicationsDetailInfo("ApplicantSortName", $OrgID, $ApplicationID, $RequestID);
    $applicant_name =   $app_info['ApplicantSortName'];
    $APPDATA        =   G::Obj('Applicants')->getAppData($OrgID, $ApplicationID);
    $rtn_aa_dis_vet =   include_once COMMON_DIR . "application/Disabled.inc";
}

if($rtn_aa_dis_vet != "") {
    echo $rtn_aa_dis_vet;
    
    echo '<br><br>';
    
    echo '<input type="submit" name="btnEditVetDisAA" id="btnEditVetDisAA" value="Submit">';
}

echo '</form>';

echo '</div>';

echo '</div>';	//Row End

echo '</div>';	//Page Inner End
echo '</div>';	//Page Container End
echo '</div>';	//Page Wrapper

echo "<div style='clear:both'><br><br></div>";
require_once USERPORTAL_DIR . 'Footer.inc';
?>