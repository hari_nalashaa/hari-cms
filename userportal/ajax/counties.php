<?php
require_once '../userportal.inc';

//Set the database as WOTC
G::Obj('GenericQueries')->conn_string =   "WOTC";

$query = "SELECT * FROM Counties WHERE State = :State";
$query .= " ORDER BY County";
$params =   array(':State'=>$_POST['state']);
$COUNTIES =   G::Obj('GenericQueries')->getInfoByQuery($query,array($params));

echo '<option value="">Select</option>';

foreach ($COUNTIES AS $C) {

echo '<option value="' . $C['County'] . '">' . $C['County'] . '</option>';

} // end foreach


?>
