<?php
require_once 'userportal.inc';

require_once USERPORTAL_DIR . 'Authenticate.inc';

if (isset($_REQUEST['btnGoBack'])) {
  $back_link = USERPORTAL_HOME . 'editApplicationForm.php?ApplicationID=' . $_REQUEST['ApplicationID'] . '&RequestID=' . $_REQUEST['RequestID'];
  if ($MultiOrgID != "") { $link .= '&MultiOrgID=' . $MultiOrgID; }
  header("Location:". $back_link);
}

$APPDATA = G::Obj('Applicants')->getAppData($OrgID, $_REQUEST['ApplicationID']);

$QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$_REQUEST['ApplicationID'], "QuestionID"=>"signature", "Answer"=>$_REQUEST['signature']);
G::Obj('Applicants')->insUpdApplicantData($QI);
$QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$_REQUEST['ApplicationID'], "QuestionID"=>"agree", "Answer"=>$_REQUEST['agree']);
G::Obj('Applicants')->insUpdApplicantData($QI);
$signdatetime =   G::Obj('MysqlHelper')->getDateTime ( '%Y-%m-%d %H:%m:%s' );
$QI    =   array("OrgID"=>$OrgID, "ApplicationID"=>$_REQUEST['ApplicationID'], "QuestionID"=>"signdatetime", "Answer"=>$signdatetime);
G::Obj('Applicants')->insUpdApplicantData($QI);

if ($APPDATA['signature'] != $_REQUEST['signature']) {
$COMPARE['signature']['Question']         =   'Electronic Signature:';
$COMPARE['signature']['QuestionID']       =   'signature';
$COMPARE['signature']['QuestionTypeID']   =   "30";
$COMPARE['signature']['values']           =   "";
$COMPARE['signature']['APPDATA']          =   $APPDATA['signature'];
$COMPARE['signature']['REQUEST']          =   $_REQUEST['signature'];
}

if ($APPDATA['agree'] != $_REQUEST['agree']) {
$COMPARE['agree']['Question']         =   'I agree with the above statement.';
$COMPARE['agree']['QuestionID']       =   'agree';
$COMPARE['agree']['QuestionTypeID']   =   "30";
$COMPARE['agree']['values']           =   "";
$COMPARE['agree']['APPDATA']          =   $APPDATA['agree'];
$COMPARE['agree']['REQUEST']          =   $_REQUEST['agree'];
}

if ($APPDATA['signdatetime'] != $signdatetime) {
$COMPARE['signdatetime']['Question']         =   'Signature Date and Time';
$COMPARE['signdatetime']['QuestionID']       =   'signdatetime';
$COMPARE['signdatetime']['QuestionTypeID']   =   "30";
$COMPARE['signdatetime']['values']           =   "";
$COMPARE['signdatetime']['APPDATA']          =   $APPDATA['signdatetime'];
$COMPARE['signdatetime']['REQUEST']          =   $signdatetime;
}

  // Process History for changes
   if (count($COMPARE) > 0) {

        $CHANGEHISTORY = array();
        $CHANGES = array();

	$section_title      =   "Signature";

        $CHANGEHISTORY['Section'] = $section_title;

        foreach ($COMPARE as $C) {

           $CHANGES[]=$C;

        } // end foreach

        $CHANGEHISTORY['Updated Fields']=$CHANGES;

        // Job Application History Information
        $job_app_history_info   =   array (
                                    "OrgID"                 =>  $OrgID,
                                    "ApplicationID"         =>  $_REQUEST['ApplicationID'],
                                    "RequestID"             =>  $_REQUEST ['RequestID'],
                                    "ProcessOrder"          =>  "-84",  //For update fields track in userportal
                                    "Date"                  =>  "NOW()",
                                    "StatusEffectiveDate"   =>  "NOW()",
                                    "UserID"                =>  $UpUserID,
                                    "Comments"              =>  'Application data updated.',
                                    "UpdatedFields"         =>  json_encode($CHANGEHISTORY)
                                    );
        //Insert Job Application History
        G::Obj('JobApplicationHistory')->insJobApplicationHistory ( $job_app_history_info );

   } // end if count COMPARE


//Get requisition details
$req_info               =   G::Obj('Requisitions')->getRequisitionsDetailInfo("Title", $OrgID, $_REQUEST ['RequestID']);

$page_title = "Thank you for signing your application";

require_once USERPORTAL_DIR . 'PageAndNavigationInfo.inc';
require_once USERPORTAL_DIR . 'Header.inc';
require_once USERPORTAL_DIR . 'Navigation.inc';

//Page Wrapper Start
echo '<div id="page-wrapper">';
echo '<div class="page-container">';

echo '<div class="row">';//Row Start
echo '<div class="col-lg-12">';
echo '<h3 class="page-header">Thank you</h3>';
echo '</div>';
echo '</div>'; 

//Row End
echo '<div class="page-inner">';
//Row Start
echo '<div class="row">';

echo '<p style="text-align: left !important;">';
echo 'Thank you for editing your application for the <strong>' . $req_info['Title'] . '</strong> position.';
echo '<br>Your <strong>Application ID: </strong>' . htmlspecialchars($_REQUEST['ApplicationID']) . '<br><br>';

$status_link = htmlspecialchars(USERPORTAL_HOME) . 'index.php?OrgID='.htmlspecialchars($OrgID);
if(isset($_REQUEST['MultiOrgID']) && $_REQUEST['MultiOrgID'] != "") {
		   $status_link .= '&MultiOrgID=' . $_REQUEST['MultiOrgID'];
}
$status_link .= '&navpg=profiles&navsubpg=status';
    	
echo '<br> <a href="' . $status_link . '">You can check your status here</a></p>';

echo '</div>';	//Row End
echo '</div>';	//Page Wrapper End
echo '</div>';
echo '</div>';
require_once USERPORTAL_DIR . 'Footer.inc';
?>
